package com.thel.analytics;

import android.app.Activity;

public interface IGoogleAnalytics {

    void uploadGoogleDatas(String event, Activity context);

}
