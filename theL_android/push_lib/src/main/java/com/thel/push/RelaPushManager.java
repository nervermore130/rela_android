package com.thel.push;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import cn.jpush.android.api.JPushInterface;

public class RelaPushManager {

    private final static String TAG = "RelaPushManager";

    private static RelaPushManager INSTANCE = new RelaPushManager();

    public OnPushListener mOnPushListener;

    private RelaPushManager() {

    }

    public static RelaPushManager getInstance() {
        return INSTANCE;
    }

    public void initJPush(Context context, OnPushListener mOnPushListener) {

        this.mOnPushListener = mOnPushListener;

        // 设置开启日志,发布时请关闭日志
        JPushInterface.setDebugMode(true);
        // 初始化 JPush
        JPushInterface.init(context);

        String registrationId = JPushInterface.getRegistrationID(context);

        Log.d("PushReceiver", " registrationId : " + registrationId);

        if (!TextUtils.isEmpty(registrationId)) {
            mOnPushListener.onPushToken(registrationId);
        }

    }

}
