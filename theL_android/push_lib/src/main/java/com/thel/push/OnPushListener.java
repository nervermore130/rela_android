package com.thel.push;

public interface OnPushListener {

    void onPushToken(String token);

    void onPushMessage(String message);

}
