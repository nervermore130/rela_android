package com.rela.pay;

import android.app.Activity;

public interface IPayProxy {

    void init();

    void pay(String payType, Activity activity, String result, OnPayStatusListener mOnPayStatusListener);

    void destroy();

}
