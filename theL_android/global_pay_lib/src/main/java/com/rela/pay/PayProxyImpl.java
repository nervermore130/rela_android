package com.rela.pay;

import android.app.Activity;

import com.rela.pay.google.GooglePayment;

public class PayProxyImpl implements IPayProxy {

    private final static PayProxyImpl INSTANCE = new PayProxyImpl();

    private IPayment payment;

    private PayProxyImpl() {

    }

    public static PayProxyImpl getInstance() {
        return INSTANCE;
    }


    @Override
    public void init() {

    }

    @Override
    public void pay(String payType, Activity activity, String result, OnPayStatusListener mOnPayStatusListener) {

        switch (payType) {
            case PayConstants.PAY_TYPE_GOOGLEPAY:
                payment = new GooglePayment();
                payment.pay(activity, result, mOnPayStatusListener);
                break;
            default:
                break;
        }

    }

    @Override public void destroy() {
        if(payment!=null){
            payment.destroy();
        }
    }
}
