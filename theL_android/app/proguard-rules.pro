# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/liuyun/Library/Android/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

# 腾讯相关的库混淆规则
-dontwarn com.tencent.**
-keep public class com.tencent.**{*;}

# 避免影响升级功能，需要keep住support包的类
-keep class android.support.**{*;}

# 声网sdk
-keep class io.agora.**{*;}

#不混淆facebook相关库
-dontwarn com.facebook.**
-keep class com.facebook.** {*;}

-keep class customview.ToolTipView

#不混淆扫描二维码的代码
-keep public class com.dlazaro66.qrcodereaderview.**{*;}

#不混淆fabric
-keep public class com.crashlytics.sdk.android.**{*;}

#不混淆firbase
-keep public class com.google.firebase.**{*;}

#不混淆eventbus
-dontwarn org.greenrobot.**
-keep public class org.greenrobot.**{*;}

-dontwarn com.networkbench.**
-keep public class com.networkbench.**{*;}

#不混淆友盟上报
-keep public class com.umeng.analytics.**{*;}

#不混淆PhotoView
-keep public class uk.co.senab.**{*;}

-keep public class com.tbruyelle.rxpermissions2.**{*;}

#不混淆七牛的所有的sdk
-keep public class com.qiniu.**{*;}

-keep public class fr.castorflex.android.verticalviewpager.VerticalViewPager

#不混淆growingio
-dontwarn com.growingio.android.**
-keep public class com.growingio.android.**{*;}

#不混淆jakewharton的相关库
-keep public class com.jakewharton.**{*;}

#不混淆butterknife
-keep public class butterknife.**{*;}

#不混淆filedownloader
-keep public class com.liulishuo.filedownloader.**{*;}

#不混淆glide
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public class * extends com.bumptech.glide.module.AppGlideModule
-keep public enum com.bumptech.glide.load.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}

#不混淆美团打包
-keep public class com.meituan.android.**{*;}

#不混淆金山播放器
-keep public class com.ksyun.media.**{*;}

#不混淆mp4parser
-dontwarn com.coremedia.**
-keep public class com.coremedia.**{*;}
-dontwarn com.googlecode.**
-keep public class com.googlecode.**{*;}
-dontwarn com.mp4parser.**
-keep public class com.mp4parser.**{*;}

#不混淆greendao
-keep public class org.greenrobot.**{*;}

-keep public class com.llew.**{*;}

-keep public class com.jcodecraeer.**{*;}

-keep public class android.arch.lifecycle.**{*;}

-keep public class com.kyleduo.switchbutton.SwitchButton

-dontwarn com.google.**
-keep public class com.google.**{*;}

-dontwarn com.alipay.**
-keep public class com.alipay.**{*;}
-keep public class org.json.alipay.**{*;}

-keep public class com.github.chrisbanes.**{*;}

-keep public class com.nostra13.universalimageloader.**{*;}

##---------------Begin: proguard configuration for Gson  ----------
# Gson uses generic type information stored in a class file when working with fields. Proguard
# removes such information by default, so configure it to keep all of it.
-keepattributes Signature

# For using GSON @Expose annotation
-keepattributes *Annotation*

# Gson specific classes
-keep class sun.misc.Unsafe { *; }
#-keep class com.google.gson.stream.** { *; }

# Application classes that will be serialized/deserialized over Gson
-keep class com.google.gson.examples.android.model.** { *; }

##---------------End: proguard configuration for Gson  ----------

-dontwarn javax.annotation.**
-dontwarn javax.inject.**
# OkHttp3
-dontwarn okhttp3.logging.**
-keep class okhttp3.internal.**{*;}
-dontwarn okio.**
# Retrofit
-dontwarn retrofit2.**
-keep class retrofit2.** {*;}
# RxJava RxAndroid
-dontwarn sun.misc.**
-keepclassmembers class rx.internal.util.unsafe.*ArrayQueue*Field* {
    long producerIndex;
    long consumerIndex;
}
-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueProducerNodeRef {
    rx.internal.util.atomic.LinkedQueueNode producerNode;
}
-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueConsumerNodeRef {
    rx.internal.util.atomic.LinkedQueueNode consumerNode;
}

#定位
-dontwarn com.amap.**
-keep class com.amap.api.location.**{*;}
-keep class com.loc.**{*;}
-keep class com.amap.api.fence.**{*;}
-keep class com.autonavi.aps.amapapi.model.**{*;}

-keep class com.thel.bean{*;}
-keep class com.thel.modules.live.bean.**{*;}
-keep class com.thel.db.daos.**{*;}
-keep class com.thel.db.table.**{*;}
-keep class com.thel.base.BaseDataBean

-keepattributes Exceptions,InnerClasses,...

-dontwarn net.sourceforge.pinyin4j.**
-keep class net.sourceforge.pinyin4j.**{*;}

-dontwarn cn.udesk.**

-dontwarn com.alibaba.**

-dontwarn io.netty.**

-dontwarn org.android.agoo.**

-dontwarn udesk.org.**

-dontwarn video.com.relavideolibrary.**

-dontwarn demo.Pinyin4jAppletDemo
-keep class demo.Pinyin4jAppletDemo

-dontwarn okhttp3.internal.**

-dontwarn java.**

-dontwarn javax.**

#基线包使用，生成mapping.txt
-printmapping mapping.txt
#生成的mapping.txt在app/buidl/outputs/mapping/release路径下，移动到/app路径下
#修复后的项目使用，保证混淆结果一致
#-applymapping mapping.txt
#hotfix
-keep class com.taobao.sophix.**{*;}
-keep class com.ta.utdid2.device.**{*;}
#防止inline
-dontoptimize

# 指定代码的压缩级别 0 - 7(指定代码进行迭代优化的次数，在Android里面默认是5，这条指令也只有在可以优化时起作用。)
-optimizationpasses 5
# 混淆时不会产生形形色色的类名(混淆时不使用大小写混合类名)
-dontusemixedcaseclassnames
# 指定不去忽略非公共的库类(不跳过library中的非public的类)
-dontskipnonpubliclibraryclasses
# 指定不去忽略包可见的库类的成员
-dontskipnonpubliclibraryclassmembers
#不进行优化，建议使用此选项，
-dontoptimize
 # 不进行预校验,Android不需要,可加快混淆速度。
-dontpreverify
# 屏蔽警告
-ignorewarnings
# 指定混淆是采用的算法，后面的参数是一个过滤器
# 这个过滤器是谷歌推荐的算法，一般不做更改
-optimizations !code/simplification/arithmetic,!field/*,!class/merging/*
# 保护代码中的Annotation不被混淆
-keepattributes *Annotation*
# 避免混淆泛型, 这在JSON实体映射时非常重要
-keepattributes Signature
# 抛出异常时保留代码行号
-keepattributes SourceFile,LineNumberTable
 #优化时允许访问并修改有修饰符的类和类的成员，这可以提高优化步骤的结果。
# 比如，当内联一个公共的getter方法时，这也可能需要外地公共访问。
# 虽然java二进制规范不需要这个，要不然有的虚拟机处理这些代码会有问题。当有优化和使用-repackageclasses时才适用。
#指示语：不能用这个指令处理库中的代码，因为有的类和类成员没有设计成public ,而在api中可能变成public
-allowaccessmodification
#当有优化和使用-repackageclasses时才适用。
-repackageclasses ''
 # 混淆时记录日志(打印混淆的详细信息)
 # 这句话能够使我们的项目混淆后产生映射文件
 # 包含有类名->混淆后类名的映射关系
-verbose

#继承activity,application,service,broadcastReceiver,contentprovider....不进行混淆
-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.support.multidex.MultiDexApplication
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class * extends android.app.backup.BackupAgentHelper
-keep public class * extends android.preference.Preference
-keep public class * extends android.view.View
-keep class android.support.** {*;}## 保留support下的所有类及其内部类

-keep public class com.google.vending.licensing.ILicensingService
-keep public class com.android.vending.licensing.ILicensingService
#表示不混淆上面声明的类，最后这两个类我们基本也用不上，是接入Google原生的一些服务时使用的。
#----------------------------------------------------

# 保留继承的
-keep public class * extends android.support.v4.**
-keep public class * extends android.support.v7.**
-keep public class * extends android.support.annotation.**

#表示不混淆任何包含native方法的类的类名以及native方法名，这个和我们刚才验证的结果是一致
-keepclasseswithmembernames class * {
    native <methods>;
}


#这个主要是在layout 中写的onclick方法android:onclick="onClick"，不进行混淆
#表示不混淆Activity中参数是View的方法，因为有这样一种用法，在XML中配置android:onClick=”buttonClick”属性，
#当用户点击该按钮时就会调用Activity中的buttonClick(View view)方法，如果这个方法被混淆的话就找不到了
-keepclassmembers class * extends android.app.Activity{
    public void *(android.view.View);
}

#表示不混淆枚举中的values()和valueOf()方法，枚举我用的非常少，这个就不评论了
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

#表示不混淆Parcelable实现类中的CREATOR字段，
#毫无疑问，CREATOR字段是绝对不能改变的，包括大小写都不能变，不然整个Parcelable工作机制都会失败。
-keep class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator *;
}
# 这指定了继承Serizalizable的类的如下成员不被移除混淆
-keepclassmembers class * implements java.io.Serializable {
    static final long serialVersionUID;
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}
# 保留R下面的资源
#-keep class **.R$* {
# *;
#}
#不混淆资源类下static的
-keepclassmembers class **.R$* {
    public static <fields>;
}

#
#----------------------------- WebView(项目中没有可以忽略) -----------------------------
#
#webView需要进行特殊处理
-keepclassmembers class fqcn.of.javascript.interface.for.Webview {
   public *;
}
-keepclassmembers class * extends android.webkit.WebViewClient {
    public void *(android.webkit.WebView, java.lang.String, android.graphics.Bitmap);
    public boolean *(android.webkit.WebView, java.lang.String);
}
-keepclassmembers class * extends android.webkit.WebViewClient {
    public void *(android.webkit.WebView, jav.lang.String);
}
#在app中与HTML5的JavaScript的交互进行特殊处理
#我们需要确保这些js要调用的原生方法不能够被混淆，于是我们需要做如下处理：
-keepclassmembers class com.ljd.example.JSInterface {
    <methods>;
}

-dontwarn org.lasque.tusdk.**
-keep public class org.lasque.tusdk.**{*;}

-dontwarn io.flutter.**
-keep class io.flutter.app.** { *; }
-keep class io.flutter.plugin.**  { *; }
-keep class io.flutter.util.**  { *; }
-keep class io.flutter.view.**  { *; }
-keep class io.flutter.**  { *; }
-keep class io.flutter.plugins.**  { *; }

-dontwarn com.umeng.**
-keep public class com.umeng.**{*;}

-dontwarn com.taobao.**
-keep public class com.taobao.**{*;}

-dontwarn anet.channel.**
-keep public class anet.channel.**{*;}

-dontwarn anetwork.channel.**
-keep public class anetwork.channel.**{*;}

-dontwarn org.android.**
-keep public class org.android.**{*;}

-dontwarn org.apache.thrift.**
-keep public class org.apache.thrift.**{*;}

-dontwarn com.xiaomi.**
-keep public class com.xiaomi.**{*;}

-dontwarn com.huawei.**
-keep public class com.huawei.**{*;}

-dontwarn com.meizu.**
-keep public class com.meizu.**{*;}

-keep public class android.text.TextUtils{*;}

