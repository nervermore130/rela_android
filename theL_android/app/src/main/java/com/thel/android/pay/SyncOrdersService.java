package com.thel.android.pay;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.text.TextUtils;
import android.widget.Toast;

import com.thel.app.TheLApp;
import com.thel.bean.GoogleIapNotifyResultBean;
import com.thel.db.MomentsDataBaseAdapter;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.RequestConstants;
import com.thel.utils.GooglePayUtils;
import com.thel.utils.ShareFileUtils;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


/**
 * 同步google wallet订单服务
 * Created by Setsail on 2015/3/19.
 */
public class SyncOrdersService extends Service {

    private MomentsDataBaseAdapter adapter;
    private Handler handler;
    public static boolean isServiceStarted = false;

    @Override
    public void onCreate() {
        adapter = MomentsDataBaseAdapter.getInstance(TheLApp.getContext(), ShareFileUtils.getString(ShareFileUtils.ID, "0"));
        handler = new Handler();
        super.onCreate();
    }

    public static void startSyncService(Context ctx) {
        try {
            if (!isServiceStarted && ShareFileUtils.isGlobalVersion() && !TextUtils.isEmpty(ShareFileUtils.getString(ShareFileUtils.KEY, ""))) {
                final String userId = ShareFileUtils.getString(ShareFileUtils.ID, "");
                if (!TextUtils.isEmpty(userId)) {
                    final Purchase purchase = MomentsDataBaseAdapter.getInstance(TheLApp.getContext(), userId).getFirstInAppOrder();
                    if (purchase != null) {
                        try {
                            Intent intent = new Intent(ctx, SyncOrdersService.class);
                            ctx.startService(intent);
                            isServiceStarted = true;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void stopSyncService(Context ctx) {
        try {
            Intent intent = new Intent(ctx, SyncOrdersService.class);
            ctx.stopService(intent);
        } catch (Exception e) {
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null) {
            stopSelf();
            return super.onStartCommand(intent, flags, startId);
        }

        syncOneOrder();

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        handler.removeCallbacksAndMessages(null);
        isServiceStarted = false;
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void syncOneOrder() {
        final Purchase purchase = MomentsDataBaseAdapter.getInstance(TheLApp.getContext(), ShareFileUtils.getString(ShareFileUtils.ID, "0")).getFirstInAppOrder();
        if (purchase != null) {
            if (!TextUtils.isEmpty(ShareFileUtils.getString(ShareFileUtils.KEY, "")))// 判断一下key是否为空，如果为空则有可能是账号被登出了
            {
                RequestBusiness.getInstance()
                        .iapNotify(purchase.getDeveloperPayload(), purchase.getOriginalJson(), purchase.getSignature())
                        .onBackpressureDrop()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new InterceptorSubscribe<GoogleIapNotifyResultBean>() {
                            @Override
                            public void onNext(GoogleIapNotifyResultBean googleIapNotifyResultBean) {
                                super.onNext(googleIapNotifyResultBean);
                                if (RequestConstants.RESPONSE_SUCCESS == Integer.parseInt(googleIapNotifyResultBean.result) && null != googleIapNotifyResultBean) {
                                    if (GooglePayUtils.getInstance().isNotified(googleIapNotifyResultBean, false))
                                        adapter.deleteInAppOrder(purchase.getDeveloperPayload());
                                }
                                postNextSyncTask();
                            }

                            @Override
                            public void onError(Throwable t) {
                                super.onError(t);
                                postNextSyncTask();
                            }
                        });
                Toast.makeText(TheLApp.context, "google wallet 购买成功后提交服务端验证", Toast.LENGTH_SHORT).show();
            } else
                stopSelf();
        } else
            stopSelf();
    }

    private void postNextSyncTask() {
        if (handler != null)
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    syncOneOrder();
                }
            }, 15000);
        else
            stopSelf();
    }
}
