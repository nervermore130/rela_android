package com.thel.ui.widget;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

/**
 * @author liuyun
 * @date 2018/5/14
 */
public class RoundRectIndicatorView extends View {

    private static final int CIRCLE_RADIUS = 5;
    private static final int DOT_INTERVAL = 5;

    private int mCount = 0;

    private Paint mPaint;

    private float oldX;

    private RectF rectF;

    private int unit;

    private int interval;

    public RoundRectIndicatorView(Context context) {
        super(context);
        init();
    }

    public RoundRectIndicatorView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        mPaint = new Paint();
        mPaint.setColor(0xffffffff);
        mPaint.setAntiAlias(true);
        rectF = new RectF();
    }

    public void setCurrent(int current) {
        int unit = dp2px(CIRCLE_RADIUS);
        int interval = dp2px(DOT_INTERVAL);
        final int start;
        if (current == 0) {
            start = current * unit;
        } else {
            start = current * unit + (current - 1) * interval + unit;
        }
        startAnimation(oldX, start);
        oldX = start;
    }

    public void setCount(int count) {
        mCount = count + 1;
        unit = dp2px(CIRCLE_RADIUS);
        interval = dp2px(DOT_INTERVAL);
        requestLayout();
    }

    int dp2px(float dp) {
        return (int) (dp * getResources().getDisplayMetrics().density + 0.5f);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int unit = dp2px(CIRCLE_RADIUS);
        int interval = dp2px(DOT_INTERVAL) + dp2px(1);
        widthMeasureSpec = MeasureSpec.makeMeasureSpec(unit * mCount + (mCount - 1) * interval + dp2px(1),
                MeasureSpec.EXACTLY);
        heightMeasureSpec = MeasureSpec.makeMeasureSpec(unit, MeasureSpec.EXACTLY);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        for (int i = 0; i < mCount; i++) {
            final int start = i * (unit + interval);
            mPaint.setColor(0x6affffff);
            mPaint.setStyle(Paint.Style.FILL);
            mPaint.setStrokeWidth(dp2px(1));
            canvas.drawCircle(start + unit / 2, unit / 2, unit / 2, mPaint);
        }
        mPaint.setColor(0xffffffff);
        mPaint.setStyle(Paint.Style.FILL);
        rectF.left = offSet;
        rectF.top = 0;
        rectF.right = offSet + unit * 3;
        rectF.bottom = unit;
        canvas.drawRoundRect(rectF, unit / 2, unit / 2, mPaint);
    }

    private ValueAnimator valueAnimator;

    private float offSet = 0;

    private void startAnimation(float fromX, float toX) {
        valueAnimator = ValueAnimator.ofFloat(fromX, toX);
        valueAnimator.setDuration(200);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                offSet = (float) animation.getAnimatedValue();
                invalidate();
            }
        });
        valueAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                valueAnimator.cancel();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        valueAnimator.start();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (valueAnimator != null) {
            valueAnimator.cancel();
        }
    }
}

