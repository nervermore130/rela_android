package com.thel.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import androidx.annotation.NonNull;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.utils.StringUtils;


/**
 * Created by waiarl on 2018/3/12.
 */

public class DefaultEmptyView extends RelativeLayout {
    private final Context mContext;
    private int mEmptyName = EMPTY_NULL;
    private ImageView img_default_empty;
    private TextView txt_default_empty;

    public DefaultEmptyView(Context context) {
        this(context, null);
    }

    public DefaultEmptyView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DefaultEmptyView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        final TypedArray arr = context.obtainStyledAttributes(attrs, R.styleable.DefaultEmptyView);
        mEmptyName = arr.getInt(R.styleable.DefaultEmptyView_empty_name, EMPTY_NULL);
        arr.recycle();
        init();
        initView();
    }

    private void init() {
        inflate(mContext, R.layout.default_empty_view, this);
        img_default_empty = findViewById(R.id.img_default_empty);
        txt_default_empty = findViewById(R.id.txt_default_empty);
        setOnClickListener(null);
    }

    private DefaultEmptyView initView() {
        if (mEmptyName == EMPTY_NULL) {
            clearView();
            return this;
        }
        final DefaultEmptyViewBean viewBean = defaultEmptyViewArr.get(mEmptyName);
        if (viewBean == null) {
            clearView();
            return this;
        }
        img_default_empty.setImageResource(viewBean.res);
        txt_default_empty.setText(viewBean.text);
        return this;
    }

    private void clearView() {
        img_default_empty.setImageDrawable(null);
        txt_default_empty.setText("");
    }

    public DefaultEmptyView initView(int emptyName) {
        this.mEmptyName = emptyName;
        return initView();
    }

    public int getEmptyName() {
        return mEmptyName;
    }

    public DefaultEmptyView setImageResource(int resId) {
        img_default_empty.setImageResource(resId);
        return this;
    }

    public DefaultEmptyView setText(CharSequence text) {
        txt_default_empty.setText(text);
        return this;
    }

    public DefaultEmptyView setImageClickListener(OnClickListener listener) {
        img_default_empty.setOnClickListener(listener);
        return this;
    }

    public DefaultEmptyView setTextClickListener(OnClickListener listener) {
        txt_default_empty.setOnClickListener(listener);
        return this;
    }

    static class DefaultEmptyViewBean {
        int id;
        int res;
        String text;

        public DefaultEmptyViewBean(int id, int res, String text) {
            this.id = id;
            this.res = res;
            this.text = text;
        }
    }

    public static SparseArray<DefaultEmptyViewBean> defaultEmptyViewArr = new SparseArray<DefaultEmptyViewBean>() {
        {
            put(EMPTY_FOLLOW_LIST, new DefaultEmptyViewBean(EMPTY_FOLLOW_LIST, R.mipmap.bg_default_follower, getString(R.string.fllow_here)));
            put(EMPTY_FANS_LIST_ME, new DefaultEmptyViewBean(EMPTY_FANS_LIST_ME, R.mipmap.bg_default_follower, getString(R.string.default_info_fans)));
            put(EMPTY_FANS_LIST_HER, new DefaultEmptyViewBean(EMPTY_FANS_LIST_HER, R.mipmap.bg_default_follower, getString(R.string.default_info_fans1)));
            put(EMPTY_WORLD_WALKING, new DefaultEmptyViewBean(EMPTY_WORLD_WALKING, R.mipmap.bg_default_world, getString(R.string.world_result_activity_default)));
            put(EMPTY_WINK_LIST_ME, new DefaultEmptyViewBean(EMPTY_WINK_LIST_ME, R.mipmap.bg_default_wink, getString(R.string.default_info_winklist)));
            put(EMPTY_WINK_LIST_HER, new DefaultEmptyViewBean(EMPTY_WINK_LIST_HER, R.mipmap.bg_default_wink, getString(R.string.default_info_winklist1)));
            put(EMPTY_VISIT_LIST, new DefaultEmptyViewBean(EMPTY_VISIT_LIST, R.mipmap.bg_default_visit, getString(R.string.default_info_whoseenme)));
            put(EMPTY_CHAT_LIST, new DefaultEmptyViewBean(EMPTY_CHAT_LIST, R.mipmap.bg_default_msg, getString(R.string.default_info_messages)));
            put(EMPTY_NEARBY, new DefaultEmptyViewBean(EMPTY_NEARBY, R.mipmap.bg_default_filter, getString(R.string.default_empty_txt_nearby)));
            put(EMPTY_NOTICE, new DefaultEmptyViewBean(EMPTY_NOTICE, R.mipmap.bg_default_notification, getString(R.string.nitification_moment)));
            put(EMPTY_FAVORITE, new DefaultEmptyViewBean(EMPTY_FAVORITE, R.mipmap.bg_default_sticker2, getString(R.string.favorite_list_empty)));
            put(EMPTY_STICKER, new DefaultEmptyViewBean(EMPTY_STICKER, R.mipmap.bg_default_sticker, getString(R.string.my_purchased_default)));
            put(EMPTY_SEARCH_FRIEND, new DefaultEmptyViewBean(EMPTY_SEARCH_FRIEND, R.mipmap.bg_default_searchfrd, getString(R.string.search_activity_result_empty)));
            put(EMPTY_MOMENT_HIDE, new DefaultEmptyViewBean(EMPTY_MOMENT_HIDE, R.mipmap.bg_default_block, getString(R.string.default_info_myblockusermoments)));
            put(EMPTY_TAG_LIST, new DefaultEmptyViewBean(EMPTY_TAG_LIST, R.mipmap.bg_default_tags, getString(R.string.search_tags_activity_result_empty)));
            put(EMPTY_TAG_INFO, new DefaultEmptyViewBean(EMPTY_TAG_INFO, R.mipmap.bg_default_tags, getString(R.string.topic_main_page_default_tip)));
            put(EMPTY_LIVE_LIST, new DefaultEmptyViewBean(EMPTY_LIVE_LIST, R.mipmap.bg_default_live, getString(R.string.no_live_streams)));
        }
    };

    public static final int EMPTY_NULL = -1;
    /***关注列表***/
    public static final int EMPTY_FOLLOW_LIST = 0;
    /***粉丝列表***/
    public static final int EMPTY_FANS_LIST_ME = 1;
    /***漫游世界***/
    public static final int EMPTY_WORLD_WALKING = 2;
    /***挤眼列表***/
    public static final int EMPTY_WINK_LIST_ME = 3;
    /***来访记录***/
    public static final int EMPTY_VISIT_LIST = 4;
    /***聊天消息列表***/
    public static final int EMPTY_CHAT_LIST = 5;
    /***附近的人***/
    public static final int EMPTY_NEARBY = 6;
    /***通知为空***/
    public static final int EMPTY_NOTICE = 7;
    /***无收藏***/
    public static final int EMPTY_FAVORITE = 8;
    /***购买表情控***/
    public static final int EMPTY_STICKER = 9;
    /***搜索朋友列表空***/
    public static final int EMPTY_SEARCH_FRIEND = 10;
    /***隐藏日志***/
    public static final int EMPTY_MOMENT_HIDE = 11;
    /***无标签***/
    public static final int EMPTY_TAG_LIST = 12;
    /***表情详情***/
    public static final int EMPTY_TAG_INFO = 13;
    /***直播列表为空***/
    public static final int EMPTY_LIVE_LIST = 14;
    /***她人粉丝列表为空***/
    public static final int EMPTY_FANS_LIST_HER = 15;
    /***她人挤眼列表为空***/
    public static final int EMPTY_WINK_LIST_HER = 16;


    private static String getString(@NonNull int resId) {
        return StringUtils.getString(resId);
    }


}
