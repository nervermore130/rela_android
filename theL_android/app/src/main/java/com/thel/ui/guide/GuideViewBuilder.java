package com.thel.ui.guide;

import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.Typeface;
import androidx.annotation.NonNull;
import android.view.Gravity;

import com.thel.R;
import com.thel.utils.Utils;

/**
 * Created by waiarl on 2018/4/18.
 */

public class GuideViewBuilder {

    public String[] contents;
    public int guideRes;
    public RectF targetRect = new RectF();
    public int contentColor = Color.WHITE;
    public int contentSize = 12;
    public Point contentBackgroundColor = new Point();
    public int backgroundColor;
    public PointF offsetPoint = new PointF();
    public int gravity = Gravity.TOP;
    public int contentsGravity = Gravity.CENTER;
    public float bgRadius = 5;
    public RectF contentsMarginRect = new RectF(bgRadius, bgRadius, bgRadius, bgRadius);
    public int suggestContentBgWidth;
    public RectF contentBackgroundMargin = new RectF();
    public PointF suggestTriangleSize = new PointF();
    public Typeface contentStyle = Typeface.create(Typeface.SANS_SERIF, Typeface.NORMAL);
    public int contentBackgroundGravity = Gravity.CENTER;
    public int contentDivider = 5;

    public GuideViewBuilder setContents(String[] contents) {
        this.contents = contents;
        return this;
    }

    public GuideViewBuilder setGuideRes(int guideRes) {
        this.guideRes = guideRes;
        return this;

    }

    public GuideViewBuilder setTargetRect(@NonNull RectF targetRect) {
        this.targetRect.set(targetRect);
        return this;

    }

    public GuideViewBuilder setContentColor(int contentColor) {
        this.contentColor = contentColor;
        return this;
    }

    public GuideViewBuilder setContentSize(int contentSize) {
        this.contentSize = contentSize;
        return this;
    }

    public GuideViewBuilder setContentBackgroundColor(@NonNull Point contentBackgroundColor) {
        this.contentBackgroundColor.set(contentBackgroundColor.x, contentBackgroundColor.y);
        return this;
    }

    public GuideViewBuilder setContentBackgroundColor(int color) {
        this.contentBackgroundColor.set(color, color);
        return this;
    }

    public GuideViewBuilder setBackgroundColor(int backgroundColor) {
        this.backgroundColor = backgroundColor;
        return this;

    }

    public GuideViewBuilder setOffsetPoint(@NonNull PointF offsetPoint) {
        this.offsetPoint.set(offsetPoint.x, offsetPoint.y);
        return this;

    }

    public GuideViewBuilder setGravity(int gravity) {
        this.gravity = gravity;
        return this;

    }

    public GuideViewBuilder setContentsGravity(int contentsGravity) {
        this.contentsGravity = contentsGravity;
        return this;
    }

    public GuideViewBuilder setContentsMarginRect(@NonNull RectF contentsMarginRect) {
        this.contentsMarginRect.set(contentsMarginRect);
        return this;
    }


    public GuideViewBuilder setSuggestContentBgWidth(int suggestContentBgWidth) {
        this.suggestContentBgWidth = suggestContentBgWidth;
        return this;
    }

    public GuideViewBuilder setContentBackgroundMargin(@NonNull RectF contentBackgroundMargin) {
        this.contentBackgroundMargin.set(contentBackgroundMargin);
        return this;
    }

    public Point getTriiangleBgColor(float startRadio, float endRadio) {
        return new Point(getGradientColor(startRadio), getGradientColor(endRadio));
    }

    public GuideViewBuilder setSuggestTriangleSize(PointF suggestTriangleSize) {
        this.suggestTriangleSize = suggestTriangleSize;
        return this;
    }

    public GuideViewBuilder setContentBackgroundGravity(int contentBackgroundGravity) {
        this.contentBackgroundGravity = contentBackgroundGravity;
        return this;
    }

    public GuideViewBuilder setContentDivider(int contentDivider) {
        this.contentDivider = contentDivider;
        return this;
    }

    public GuideViewBuilder setBgRadius(float bgRadius) {
        this.bgRadius = bgRadius;
        return this;
    }

    public GuideViewBuilder setContentStyle(Typeface contentStyle) {
        this.contentStyle = contentStyle;
        return this;
    }

    public int getGradientColor(float radio) {
        final int mStartColor = contentBackgroundColor.x;
        final int mEndColor = contentBackgroundColor.y;
        int redStart = Color.red(mStartColor);
        int blueStart = Color.blue(mStartColor);
        int greenStart = Color.green(mStartColor);
        int redEnd = Color.red(mEndColor);
        int blueEnd = Color.blue(mEndColor);
        int greenEnd = Color.green(mEndColor);

        int red = (int) (redStart + ((redEnd - redStart) * radio + 0.5));
        int greed = (int) (greenStart + ((greenEnd - greenStart) * radio + 0.5));
        int blue = (int) (blueStart + ((blueEnd - blueStart) * radio + 0.5));
        return Color.argb(255, red, greed, blue);
    }


    public static GuideViewBuilder createCommenBuilder() {
        final int cm = Utils.dip2px(5);

        return new GuideViewBuilder()
                .setGravity(Gravity.TOP)
                .setBgRadius(Utils.dip2px(5))
                .setContentColor(Utils.getColor(R.color.white))
                .setContentSize(Utils.dip2px(14))
                .setContentsGravity(Gravity.LEFT)
                .setContentDivider(Utils.dip2px(3))
                .setContentsMarginRect(new RectF(cm, cm, cm, cm))
                .setContentBackgroundColor(new Point(Utils.getColor(R.color.guide_color_1), Utils.getColor(R.color.guide_color_2)))
                .setSuggestTriangleSize(new PointF(Utils.dip2px(15), Utils.dip2px(10)));


    }

}
