package com.thel.ui.widget.moments;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.thel.R;

/**
 * Created by liuyun on 2017/9/22.
 */

public class MomentOperationsLayout extends RelativeLayout {
    public MomentOperationsLayout(Context context) {
        super(context);
        initView();
    }

    public MomentOperationsLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public MomentOperationsLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.layout_moment_operations, this, true);
    }

}
