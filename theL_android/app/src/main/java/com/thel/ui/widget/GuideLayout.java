package com.thel.ui.widget;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.modules.main.me.aboutMe.UpdateUserInfoActivity;
import com.thel.modules.main.settings.SettingActivity;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.thel.utils.ShareFileUtils.IS_JUMP_UPDATE_USERINFO_ACTIVITY;
import static com.thel.utils.ShareFileUtils.IS_RELEASE_MOMENT;

/**
 * @author liuyun
 * @date 2018/1/5
 */

public class GuideLayout extends RelativeLayout {

    @BindView(R.id.guide_moment_rl)
    RelativeLayout guide_moment_rl;

    @BindView(R.id.guide_user_info_rl)
    RelativeLayout guide_user_info_rl;

    @BindView(R.id.user_info_ratio_tv)
    TextView user_info_ratio_tv;

    private String oldUserRatio = "0";

    public GuideLayout(Context context) {
        super(context);
        init();
    }

    public GuideLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public GuideLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {

        View view = LayoutInflater.from(getContext()).inflate(R.layout.layout_guide, this, true);

        ButterKnife.bind(this, view);

        if (!ShareFileUtils.getBoolean(Utils.getMyUserId() + "_" + IS_RELEASE_MOMENT, false)) {
            guide_moment_rl.setVisibility(View.VISIBLE);
        } else {
            guide_moment_rl.setVisibility(View.GONE);
        }

        oldUserRatio = ShareFileUtils.getString(ShareFileUtils.NO_RATIO, "0");

        if (!ShareFileUtils.getBoolean(Utils.getMyUserId() + "_" + IS_JUMP_UPDATE_USERINFO_ACTIVITY, false) && !oldUserRatio.equals("100")) {
            String profile = TheLApp.context.getString(R.string.userinfo_activity_left_profile);

            user_info_ratio_tv.setText(profile + "\n" + TheLApp.context.getString(R.string.profile_completed) + oldUserRatio + "%");

            guide_user_info_rl.setVisibility(View.VISIBLE);
        } else {
            guide_user_info_rl.setVisibility(View.GONE);
        }


    }

    @OnClick(R.id.guide_user_info_rl)
    void jumpUserInfoActivity() {
        guide_user_info_rl.setVisibility(View.GONE);
        ShareFileUtils.setBoolean(Utils.getMyUserId() + "_" + IS_JUMP_UPDATE_USERINFO_ACTIVITY, true);
        Intent fristActivity = new Intent(getContext(), SettingActivity.class);
        Intent secondActivity = new Intent(getContext(), UpdateUserInfoActivity.class);
        ShareFileUtils.setBoolean(ShareFileUtils.needShowUpdateView, true);
        Intent[] intens = new Intent[2];
        intens[0] = fristActivity;
        intens[1] = secondActivity;
        getContext().startActivities(intens);
        ((Activity) getContext()).overridePendingTransition(0, 0);

    }

    public void updateUI() {
        if (!ShareFileUtils.getBoolean(Utils.getMyUserId() + "_" + IS_JUMP_UPDATE_USERINFO_ACTIVITY, false) && !oldUserRatio.equals("100")) {
            guide_user_info_rl.setVisibility(View.VISIBLE);
        } else {
            guide_user_info_rl.setVisibility(View.GONE);
        }

        if (!ShareFileUtils.getBoolean(Utils.getMyUserId() + "_" + IS_RELEASE_MOMENT, false)) {
            guide_moment_rl.setVisibility(View.VISIBLE);
        } else {
            guide_moment_rl.setVisibility(View.GONE);
        }

        String newUserRatio = ShareFileUtils.getString(ShareFileUtils.NO_RATIO, "");

        if (!newUserRatio.equals(oldUserRatio)) {
            guide_user_info_rl.setVisibility(View.GONE);
            ShareFileUtils.setBoolean(Utils.getMyUserId() + "_" + IS_JUMP_UPDATE_USERINFO_ACTIVITY, true);
        }
    }

}
