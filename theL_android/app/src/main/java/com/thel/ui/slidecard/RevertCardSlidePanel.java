package com.thel.ui.slidecard;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import androidx.annotation.IdRes;
import androidx.annotation.LayoutRes;
import android.text.SpannableString;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.request.RequestOptions;
import com.facebook.rebound.Spring;
import com.facebook.rebound.SpringListener;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.LikeResultBean;
import com.thel.bean.SuperlikeBean;
import com.thel.constants.TheLConstants;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.main.home.moments.ReportActivity;
import com.thel.modules.main.me.aboutMe.MatchActivity;
import com.thel.modules.main.me.aboutMe.UpdateUserInfoActivity;
import com.thel.modules.main.me.bean.LogInfoBean;
import com.thel.modules.main.me.bean.MatchBean;
import com.thel.modules.main.me.bean.MatchListBean;
import com.thel.modules.main.me.bean.MyInfoBean;
import com.thel.modules.main.me.bean.RoleBean;
import com.thel.modules.main.me.match.DetailedMatchUserinfoActivity;
import com.thel.modules.main.me.match.MatchSuccessActivity;
import com.thel.modules.main.me.match.eventcollect.collect.LiveLogUtils;
import com.thel.modules.main.me.match.eventcollect.collect.MatchLogUtils;
import com.thel.modules.main.userinfo.UserInfoUtils;
import com.thel.modules.others.VipConfigActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.ui.dialog.ActionSheetDialog;
import com.thel.ui.dialog.BuySuperlikeDialog;
import com.thel.ui.dialog.MatchSuperlikeDialog;
import com.thel.ui.transfrom.GlideRoundCornerTransform;
import com.thel.utils.DateUtils;
import com.thel.utils.DialogUtil;
import com.thel.utils.ImageUtils;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.ToastUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import video.com.relavideolibrary.Utils.DensityUtils;

/**
 * Created by chad
 * Time 18/9/11
 * Email: wuxianchuang@foxmail.com
 * Description: TODO 可以撤销的CardSlidePanel
 */
public class RevertCardSlidePanel extends RelativeLayout implements View.OnClickListener {

    private static final String TAG = "RevertCardSlidePanel";

    private CardSlidePanel slidePanel;

    private CardSlidePanel.CardSwitchListener cardSwitchListener;

    private List<MatchBean> dataList = new ArrayList<>();

    private List<MatchBean> removeData = new ArrayList<>();

    private int currentPos;

    private boolean currentSlideRightIsSuperlike = false;//当前右滑是否是超级喜欢

    private String superlikeContent = "";

    private int revertType = -1;

    private boolean isShowIgnoreDialog = false;

    private boolean usedOutToday;//今日速配次数是否用完

    private ArrayList<String> constellation_list;

    @LayoutRes
    private int itemLayoutId = R.layout.fling_card_item;

    private CardItemView[] revertViews = new CardItemView[2];//0为左,1为右
    private ImageView match_revert;
    private LottieAnimationView match_dislike;
    private LottieAnimationView match_like;
    private LottieAnimationView match_superlike_anim;
    private TextView superlike_count;

    private ArrayList<String> role_list = new ArrayList<String>(); // 角色

    private int ratio;

    private boolean hasAddProfile;

    private SuperlikeBroadCast superlikeBroadCast;
    private RelativeLayout match_revert_container;
    private RelativeLayout match_superlike_container;
    private String userid;
    private long time;
    private MatchListBean matchBean;
    private String latitude;
    private String longitude;
    private String pageId;
    private SparseArray<RoleBean> relationshipMap = new SparseArray<RoleBean>();
    private String from_page;
    private String from_page_id;
    private String ACTION_TYPE_LIKE;
    private int resultCode = 0;


    public RevertCardSlidePanel(Context context) {
        this(context, null, 0);
    }

    public RevertCardSlidePanel(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RevertCardSlidePanel(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        constellation_list = new ArrayList<>(Arrays.asList(context.getResources().getStringArray(R.array.filter_constellation)));

        String[] role_Array = this.getResources().getStringArray(R.array.userinfo_role);
        role_list = new ArrayList<String>(Arrays.asList(role_Array));

        initView();
        initSlidePanel();
        initRelationshipMap();

        initData();

        superlikeBroadCast = new SuperlikeBroadCast();
        IntentFilter intentFilter = new IntentFilter(TheLConstants.BROADCAST_ACTION_SUPERLIKE);
        context.registerReceiver(superlikeBroadCast, intentFilter);
    }

    @Override
    protected void onDetachedFromWindow() {
        getContext().unregisterReceiver(superlikeBroadCast);
        super.onDetachedFromWindow();
    }

    private void initData() {
        userid = ShareFileUtils.getString(ShareFileUtils.ID, "");
        time = System.currentTimeMillis();
        latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
        longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");
        int superlikeRetain = ShareFileUtils.getInt(ShareFileUtils.SUPER_LIKE, 0);
        if (superlikeRetain > 0) {
            superlike_count.setVisibility(VISIBLE);
            superlike_count.setText(String.valueOf(superlikeRetain));
        }
    }

    private void initRelationshipMap() {
        relationshipMap.put(1, new RoleBean(0, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[0]));
        relationshipMap.put(6, new RoleBean(1, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[1]));
        relationshipMap.put(7, new RoleBean(2, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[2]));
        relationshipMap.put(2, new RoleBean(3, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[3]));
        relationshipMap.put(3, new RoleBean(4, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[4]));
        relationshipMap.put(4, new RoleBean(5, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[5]));
        relationshipMap.put(5, new RoleBean(6, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[6]));
        relationshipMap.put(0, new RoleBean(7, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[7]));


    }

    private void initSlidePanel() {

        // 1. 左右滑动监听
        cardSwitchListener = new CardSlidePanel.CardSwitchListener() {

            @Override
            public void onShow(View changedView, int index) {
                Log.d("Card", "正在显示-" + index);

                currentPos = index;
                if (dataList.size() > currentPos && dataList.get(currentPos).dataType == 0) {
                    traceMatchData("match", "exposure", dataList.get(index));
                }
                //当数据快没有的时候，请求数据
                if (dataList.size() - index == 5 && flingCardListener != null) {
                    flingCardListener.loadMore();
                }
            }

            @Override
            public void onCardVanish(int index, int type) {

                Log.d("Card", "正在消失-" + index + " 消失type=" + type);
                if (dataList.size() > currentPos && dataList.get(currentPos).dataType == 0) {
                    if (type == CardSlidePanel.VANISH_TYPE_LEFT) {
                        if ("unlike".equals(ACTION_TYPE_LIKE)) {
                            traceLikeData();

                        } else {
                            if (resultCode < 11) {
                                ACTION_TYPE_LIKE = "left";
                                traceLikeData();
                            } else {
                                resultCode = 0;
                            }


                        }
                        likeOrNot(dataList.get(currentPos).userId, "unlike", dataList.get(currentPos).isRollback + "", dataList.get(currentPos));

                    } else {
                        if (currentSlideRightIsSuperlike) {
                            superlikeHttp(String.valueOf(dataList.get(currentPos).userId), superlikeContent);
                            currentSlideRightIsSuperlike = false;
                            superlikeContent = "";
                        } else {
                            likeOrNot(dataList.get(currentPos).userId, "like", dataList.get(currentPos).isRollback + "", dataList.get(currentPos));
                            if ("like".equals(ACTION_TYPE_LIKE)) {
                                traceLikeData();

                            } else {
                                if (resultCode < 11) {
                                    ACTION_TYPE_LIKE = "right";

                                    traceLikeData();
                                } else {
                                    resultCode = 0;

                                }


                            }
                        }
                    }
                }

                revertType = type;

                removeData.add(dataList.get(index));

                if (dataList.get(index).dataType == 0) {
                    match_revert.setImageResource(R.mipmap.btn_withdraw);
                } else {
                    match_revert.setImageResource(R.mipmap.btn_withdraw_ban);
                }

                Object tag = revertViews[type].getTag();
                ViewHolder viewHolder;
                if (null != tag) {
                    viewHolder = (ViewHolder) tag;
                } else {
                    viewHolder = new ViewHolder(revertViews[type]);
                    revertViews[type].setTag(viewHolder);
                }
                viewHolder.bindData(dataList.get(index));

                if (usedOutToday && dataList.size() - 1 == index && flingCardListener != null) {
                    flingCardListener.usedOutToday();
                }
            }

            @Override
            public void onScroll(View changedView, float scrollProgressPercent) {
                try {
                    L.d("scrollProgressPercent:", "走一个" + scrollProgressPercent);
                    changedView.findViewById(R.id.item_swipe_right_indicator).setAlpha(scrollProgressPercent < 0 ? -scrollProgressPercent : 0);
                    changedView.findViewById(R.id.item_swipe_left_indicator).setAlpha(scrollProgressPercent > 0 ? scrollProgressPercent : 0);

                    float min;
                    float max;
                    float minLike;
                    float maxLike;
                    if (scrollProgressPercent < 0) {
                        min = 1 - scrollProgressPercent * 0.2f;
                        max = 1.2f;
                        match_dislike.setScaleX(min < max ? min : max);
                        match_dislike.setScaleY(min < max ? min : max);
                        match_like.setScaleX(1);
                        match_like.setScaleY(1);
                        if (scrollProgressPercent == -5.0) {
                            match_dislike.playAnimation();
                        }

                    } else if (scrollProgressPercent > 0) {
                        minLike = 1 + scrollProgressPercent * 0.2f;
                        maxLike = 1.2f;
                        match_like.setScaleX(minLike < maxLike ? minLike : maxLike);
                        match_like.setScaleY(minLike < maxLike ? minLike : maxLike);
                        match_dislike.setScaleX(1);
                        match_dislike.setScaleY(1);
                        if (scrollProgressPercent == 5.0) {
                            match_like.playAnimation();
                        }

                    } else if (scrollProgressPercent == 0) {

                        match_dislike.setScaleX(1);
                        match_dislike.setScaleY(1);
                        match_like.setScaleX(1);
                        match_like.setScaleY(1);

                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void itemClick(View changedView, int index) {
                ViewUtils.preventViewMultipleClick(changedView, 2000);
                if (dataList.get(index).dataType != 0) return;
                Intent intent = new Intent(getContext(), DetailedMatchUserinfoActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("MatchBean", dataList.get(index));
                bundle.putString("from", "RevertCardSlidePanel");
                bundle.putString(ShareFileUtils.MATCH_FROM_PAGE_ID, pageId);
                bundle.putString(ShareFileUtils.MATCH_FROM_PAGE, "match");

                bundle.putString("rank_id", matchBean.data.rankId);
                bundle.putString("receiver_id", dataList.get(index).userId + "");

                intent.putExtras(bundle);
                if (getContext() instanceof Activity) {
                    ((Activity) getContext()).startActivityForResult(intent, TheLConstants.BUNDLE_CODE_MATCH_ACTIVITY);
                }

                LogInfoBean logInfoBean = new LogInfoBean();
                logInfoBean.page = "match";
                logInfoBean.page_id = pageId;
                logInfoBean.activity = "click";
                logInfoBean.lat = latitude;
                logInfoBean.lng = longitude;
                logInfoBean.from_page = from_page;
                logInfoBean.from_page_id = from_page_id;

                LogInfoBean.LogsDataBean logsDataBean = new LogInfoBean.LogsDataBean();
                logsDataBean.receiver_id = dataList.get(index).userId + "";
                if (matchBean != null && matchBean.data != null && !TextUtils.isEmpty(matchBean.data.rankId)) {
                    logsDataBean.rank_id = matchBean.data.rankId;

                }
                logInfoBean.data = logsDataBean;
                MatchLogUtils.getInstance().addLog(logInfoBean);
            }

            @Override
            public void superlike() {
                showIgnoreSuperlikeDialog();
            }
        };
        slidePanel.setCardSwitchListener(cardSwitchListener);


        // 2. 绑定Adapter
        slidePanel.setAdapter(new CardAdapter() {
            @Override
            public int getLayoutId() {
                return itemLayoutId;
            }

            @Override
            public int getCount() {
                return dataList.size();
            }

            @Override
            public void bindView(View view, int index) {
                try {
                    Object tag = view.getTag();
                    ViewHolder viewHolder;
                    if (null != tag) {
                        viewHolder = (ViewHolder) tag;
                    } else {
                        viewHolder = new ViewHolder(view);
                        view.setTag(viewHolder);
                    }

                    viewHolder.bindData(dataList.get(index));
                    ((CardItemView) view).setDataType(dataList.get(index).dataType);
                    ((CardItemView) view).setSuperlike(dataList.get(index).isSuperLike == 1);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public Object getItem(int index) {
                return dataList.get(index);
            }

            @Override
            public Rect obtainDraggableArea(View view) {
                // 可滑动区域定制，该函数只会调用一次
                View contentView = view.findViewById(R.id.card_item_content);
                int left = view.getLeft() + contentView.getPaddingLeft();
                int right = view.getRight() - contentView.getPaddingRight();
                int top = view.getTop() + contentView.getPaddingTop();
                int bottom = view.getBottom() - contentView.getPaddingBottom();
                return new Rect(left, top, right, bottom);
            }
        });
    }

    private void initView() {
        inflate(getContext(), R.layout.revert_card_slide_panel, this);
        slidePanel = findViewById(R.id.image_slide_panel);
        match_revert = findViewById(R.id.match_revert);
        match_revert_container = findViewById(R.id.match_revert_container);
        match_revert_container.setOnClickListener(this);
        match_dislike = findViewById(R.id.match_dislike);
        match_dislike.setOnClickListener(this);
        match_like = findViewById(R.id.match_like);
        match_like.setOnClickListener(this);
        match_superlike_container = findViewById(R.id.match_superlike_container);
        match_superlike_container.setOnClickListener(this);
        match_superlike_anim = findViewById(R.id.match_superlike_anim);
        superlike_count = findViewById(R.id.superlike_count);

        pageId = Utils.getPageId();
        FrameLayout revert_container = findViewById(R.id.revert_container);
        for (int i = 0; i < revertViews.length; i++) {
            revertViews[i] = new CardItemView(getContext());
            revertViews[i].bindLayoutResId(itemLayoutId);
            revert_container.addView(revertViews[i], new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        }
    }

    public void setData(MatchListBean matchListBean, List<MatchBean> data, boolean usedOutToday, String from_page, String from_page_id) {
        if (removeData.size() > 0) {
            dataList.removeAll(removeData);
            removeData.clear();
            currentPos = 0;
        }
        //test
//        for (int i = 0; i < data.size(); i++) {
//            data.get(i).isSuperLike = 1;
//            data.get(i).superLikeNote = "test";
//        }
        //test
//        ShareFileUtils.setBoolean(ShareFileUtils.IS_FIRST_MATCH, true);
        // 判断是否第一次使用配对功能
        if (ShareFileUtils.getBoolean(ShareFileUtils.IS_FIRST_MATCH, true)) {// 第一次进入匹配功能
            ShareFileUtils.setBoolean(ShareFileUtils.IS_FIRST_MATCH, false);
            //增加新手引导
            for (int i = 1; i < 4; i++) {
                MatchBean matchBean = new MatchBean();
                matchBean.dataType = i;
                dataList.add(matchBean);
            }
        }
        ratio = ShareFileUtils.getUserRatio();

        this.usedOutToday = usedOutToday;
        this.matchBean = matchListBean;
        this.from_page = from_page;
        this.from_page_id = from_page_id;
        if (ratio < 80 && !hasAddProfile) {
            hasAddProfile = true;
            MatchBean matchBean = new MatchBean();
            matchBean.dataType = 4;
            if (data.size() <= 5)
                data.add(matchBean);
            else
                data.add(4, matchBean);
        }
        dataList.addAll(data);
        slidePanel.getAdapter().notifyDataSetChanged();
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);

        L.d(TAG, "-------------onLayout------------");

        if (!slidePanel.getReverting()) {
            for (int i = 0; i < revertViews.length; i++) {
                if (revertViews[i] != null) {
                    int childWidth = revertViews[i].getMeasuredWidth();
                    int childHeight = revertViews[i].getMeasuredHeight();
                    int marginTop = slidePanel.getMarginTop();

                    if (i == 0) {
                        revertViews[i].layout(-revertViews[i].getWidth(), marginTop, 0, marginTop + childHeight);
                        //test
//                    revertViews[i].layout(-revertViews[i].getWidth() / 2, marginTop, revertViews[i].getWidth() / 2, marginTop + childHeight);
                    } else if (i == 1) {
                        revertViews[i].layout(getWidth(), marginTop, getWidth() + childWidth, marginTop + childHeight);
                    }
                }
            }
        }

    }

    private boolean revertClickEnable = true;
    private boolean superlikeClickEnable = true;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.match_revert_container:
                if (!revertClickEnable) return;
                revertClickEnable = false;
                match_revert_container.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        revertClickEnable = true;
                    }
                }, 2000);
                if (slidePanel.getReverting()) return;
                if (dataList.size() > currentPos && dataList.get(currentPos).dataType != 0) return;
                if (UserUtils.getUserVipLevel() > 0) {
                    revertCard();
                } else {
                    Intent intent = new Intent(getContext(), VipConfigActivity.class);
                    intent.putExtra("showType", VipConfigActivity.SHOW_MSG_MATCH_REVERT);
                    Bundle bundle = new Bundle();
                    bundle.putString(ShareFileUtils.MATCH_FROM_PAGE_ID, pageId);
                    bundle.putString(ShareFileUtils.MATCH_FROM_PAGE, "match");
                    intent.putExtra("fromPage", "match");
                    intent.putExtra("fromPageId", pageId);
                    intent.putExtras(bundle);
                    getContext().startActivity(intent);
                    traceMatchUserLog("repent");

                }
                break;
            case R.id.match_dislike:
                try {
                    if (slidePanel.getReverting()) return;
                    if (dataList.get(currentPos).dataType == 1) {
                        ToastUtils.showCenterToastShort(getContext(), getContext().getString(R.string.match_indicator_tip3));
                        return;
                    }
                    if (dataList.get(currentPos).isSuperLike == 1) {
                        showIgnoreSuperlikeDialog();
                        return;
                    }
                    /***
                     * 在用户的个人主页点击的喜欢
                     * */
                    if (resultCode < 11) {
                        ACTION_TYPE_LIKE = "unlike";

                    }

                    slidePanel.leftSlide();
                    match_dislike.playAnimation();


                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case R.id.match_like:
                try {
                    if (slidePanel.getReverting()) return;
                    if (dataList.get(currentPos).dataType == 2) {
                        ToastUtils.showCenterToastShort(getContext(), getContext().getString(R.string.match_indicator_tip5));
                        return;
                    }
                    if (resultCode < 11) {
                        ACTION_TYPE_LIKE = "like";

                    }

                    slidePanel.rightSlide();
                    match_like.playAnimation();

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.match_superlike_container:
                if (!superlikeClickEnable) return;
                superlikeClickEnable = false;
                match_superlike_container.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        superlikeClickEnable = true;
                    }
                }, 2000);
                if (slidePanel.getReverting()) return;
                try {

                    if (dataList.size() > currentPos && dataList.get(currentPos).dataType != 0)
                        return;

                    LogInfoBean logInfoBean = new LogInfoBean();
                    logInfoBean.page = "match";
                    logInfoBean.page_id = pageId;
                    logInfoBean.activity = "slike";
                    logInfoBean.lat = latitude;
                    logInfoBean.lng = longitude;
                    logInfoBean.from_page = from_page;
                    logInfoBean.from_page_id = from_page_id;
                    LogInfoBean.LogsDataBean logsDataBean = new LogInfoBean.LogsDataBean();
                    if (matchBean != null && matchBean.data != null && !TextUtils.isEmpty(matchBean.data.rankId)) {
                        logsDataBean.rank_id = matchBean.data.rankId;

                    }
                    logsDataBean.receiver_id = dataList.get(currentPos).userId + "";
                    logInfoBean.data = logsDataBean;
                    MatchLogUtils.getInstance().addLog(logInfoBean);

                    MatchActivity matchActivity = (MatchActivity) getContext();

                    if (superlike_count.getVisibility() == View.VISIBLE) {
                        MatchSuperlikeDialog matchSuperlikeDialog = MatchSuperlikeDialog.newInstance();
                        matchSuperlikeDialog.setSuperlikeListener(new MatchSuperlikeDialog.SuperlikeListener() {
                            @Override
                            public void superlike(String content) {
                                superlikeContent = content;
                                currentSlideRightIsSuperlike = true;
                                match_superlike_anim.playAnimation();
                                match_superlike_anim.addAnimatorListener(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        super.onAnimationEnd(animation);
                                        slidePanel.rightSlide();
                                    }
                                });
                            }
                        });
                        matchSuperlikeDialog.show(matchActivity.getSupportFragmentManager(), MatchSuperlikeDialog.class.getName());
                    } else {
                        BuySuperlikeDialog buySuperlikeDialog = BuySuperlikeDialog.newInstance();
                        buySuperlikeDialog.setBuySuperlikeListener(new BuySuperlikeDialog.BuySuperlikeListener() {
                            @Override
                            public void success(int count) {
                                superlike_count.setVisibility(VISIBLE);
                                superlike_count.setText(String.valueOf(count));
                                ShareFileUtils.setInt(ShareFileUtils.SUPER_LIKE, count);
                            }
                        });
                        buySuperlikeDialog.show(matchActivity.getSupportFragmentManager(), BuySuperlikeDialog.class.getName());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == TheLConstants.BUNDLE_CODE_MATCH_ACTIVITY) {
            switch (resultCode) {
                case 44:  //不喜欢
                    this.resultCode = resultCode;

                    match_dislike.postDelayed(() -> match_dislike.performClick(), 500);
                    break;
                case 11:  //喜欢
                    this.resultCode = resultCode;

                    match_like.postDelayed(() -> match_like.performClick(), 500);
                    break;
                case 22: //反悔
                    this.resultCode = resultCode;

                    match_revert_container.postDelayed(() -> match_revert_container.performClick(), 500);
                    break;
                case 33: //超级喜欢
                    this.resultCode = resultCode;

                    slidePanel.postDelayed(() -> slidePanel.rightSlide(), 500);
                    break;

            }
        }
    }

    private void revertCard() {


        if (revertType != -1 && removeData.size() > 0) {
            revertViews[revertType].animTo(slidePanel.getInitCenterViewX(), slidePanel.getInitCenterViewY());
            revertViews[revertType].addListener(new SpringListener() {
                @Override
                public void onSpringUpdate(Spring spring) {

                }

                @Override
                public void onSpringAtRest(Spring spring) {

                    slidePanel.setReverting(false);

                    slidePanel.getAdapter().notifyDataSetChanged();

                    revertViews[revertType].removeListener(this);

                }

                @Override
                public void onSpringActivate(Spring spring) {

                }

                @Override
                public void onSpringEndStateChange(Spring spring) {

                }
            });

            slidePanel.setReverting(true);

            match_revert.setImageResource(R.mipmap.btn_withdraw_ban);
            removeData.remove(removeData.size() - 1);
            dataList.removeAll(removeData);
            removeData.clear();
            currentPos = 0;

            LogInfoBean logInfoBean = new LogInfoBean();
            logInfoBean.page = "match";
            logInfoBean.page_id = pageId;
            logInfoBean.activity = "repent";
            logInfoBean.lat = latitude;
            logInfoBean.lng = longitude;
            logInfoBean.from_page = from_page;
            logInfoBean.from_page_id = from_page_id;

            LogInfoBean.LogsDataBean logsDataBean = new LogInfoBean.LogsDataBean();
            if (matchBean != null && matchBean.data != null && !TextUtils.isEmpty(matchBean.data.rankId)) {
                logsDataBean.rank_id = matchBean.data.rankId;

            }
            logInfoBean.data = logsDataBean;

            MatchLogUtils.getInstance().addLog(logInfoBean);
        }
    }

    private void showIgnoreSuperlikeDialog() {
        if (!isShowIgnoreDialog) {
            isShowIgnoreDialog = true;
            DialogUtil.showConfirmDialog(getContext(), null, TheLApp.getContext().getString(R.string.she_superlike_messgage), TheLApp.getContext().getString(R.string.cancel), TheLApp.getContext().getString(R.string.info_yes),
                    (dialog, which) -> {
                        dialog.dismiss();
                        isShowIgnoreDialog = false;
                    },
                    (dialog, which) -> {
                        dialog.dismiss();
                        isShowIgnoreDialog = false;
                        slidePanel.leftSlide();
                        match_dislike.playAnimation();
                    });
        }
    }

    private void likeOrNot(long userId, final String like, String s, final MatchBean matchBean) {
        Flowable<LikeResultBean> flowable = RequestBusiness.getInstance().likeOrNot(userId, like, s);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<LikeResultBean>() {
            @Override
            public void onNext(LikeResultBean likeResultBean) {
                super.onNext(likeResultBean);
                getLikeResult(likeResultBean, matchBean);
            }

        });

    }

    private void traceLikeData() {
        LogInfoBean logInfoBean = new LogInfoBean();
        logInfoBean.page = "match";
        logInfoBean.page_id = pageId;
        logInfoBean.activity = ACTION_TYPE_LIKE;
        logInfoBean.lat = latitude;
        logInfoBean.lng = longitude;
        logInfoBean.from_page = from_page;
        logInfoBean.from_page_id = from_page_id;

        LogInfoBean.LogsDataBean logsDataBean = new LogInfoBean.LogsDataBean();
        logsDataBean.receiver_id = dataList.get(currentPos).userId + "";
        if (matchBean != null && matchBean.data != null && !TextUtils.isEmpty(matchBean.data.rankId)) {
            logsDataBean.rank_id = matchBean.data.rankId;

        }
        logInfoBean.data = logsDataBean;
        MatchLogUtils.getInstance().addLog(logInfoBean);
    }

    private void getLikeResult(LikeResultBean likeResultBean, MatchBean matchBean) {
        if (likeResultBean != null && likeResultBean.data != null) {
            //互相喜欢
            try {
                int result = Integer.parseInt(likeResultBean.data.likeSuccess);
                if (result == 1) {
                    Intent intent = new Intent(getContext(), MatchSuccessActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(MatchSuccessActivity.MATCH_SUCCESS_TYPE, MatchSuccessActivity.MATCH_SUCCESS_FROM_CARD_SLIDE);
                    bundle.putString(TheLConstants.BUNDLE_KEY_USER_AVATAR, matchBean.avatar);
                    bundle.putString(TheLConstants.BUNDLE_KEY_USER_ID, String.valueOf(matchBean.userId));
                    bundle.putString(TheLConstants.BUNDLE_KEY_NICKNAME, matchBean.nickName);
                    bundle.putString(ShareFileUtils.MATCH_FROM_PAGE_ID, pageId);
                    bundle.putString(ShareFileUtils.MATCH_FROM_PAGE, "match");

                    intent.putExtras(bundle);
                    getContext().startActivity(intent);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void superlikeHttp(String userId, String note) {
        Flowable<SuperlikeBean> flowable = RequestBusiness.getInstance().superlike(userId, note);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<SuperlikeBean>() {
            @Override
            public void onNext(SuperlikeBean likeResultBean) {
                super.onNext(likeResultBean);
                if (likeResultBean != null && likeResultBean.data != null) {
                    ShareFileUtils.setInt(ShareFileUtils.SUPER_LIKE, likeResultBean.data.superlikeRetain);
                    if (likeResultBean.data.superlikeRetain > 0) {
                        superlike_count.setVisibility(VISIBLE);
                        superlike_count.setText(String.valueOf(likeResultBean.data.superlikeRetain));
                    } else {
                        superlike_count.setVisibility(INVISIBLE);
                    }
                }
            }

            @Override
            public void onError(Throwable t) {
            }

            @Override
            public void onComplete() {

            }
        });
    }

    /**
     * 感情状态
     */
    private SpannableString generateRalationship(MatchBean itemData) {
        String startstring = TheLApp.context.getString(R.string.updatauserinfo_activity_lovestatus);
        if (itemData.affection != null) {
            int affection = Integer.parseInt(itemData.affection);
            RoleBean roleBean = relationshipMap.get(affection);
            if (roleBean != null) {
                SpannableString spannaString = UserInfoUtils.getUserInfoSpannaString(startstring, roleBean.contentRes);
                return spannaString;

            }

        }

        return null;
    }

    private String generateRole(MatchBean itemData) {
        String selfMatch = "";
        if (!TextUtils.isEmpty(itemData.roleName)) {
            try {
                int index = Integer.parseInt(itemData.roleName);
                RoleBean roleBean = ShareFileUtils.getRoleBean(index);
                if (roleBean != null) {
                    selfMatch = getContext().getText(R.string.slef_identity) + ":  " + roleBean.contentRes;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        int otherRoleCount = 0;
        String interestMatch = "";
        StringBuilder sb = new StringBuilder();// 寻找角色内容字符串拼接
        ArrayList newRoleList = new ArrayList();

        try {
            if (!TextUtils.isEmpty(itemData.wantRole)) {
                String[] lookingForRole;
                if (itemData.wantRole.contains(",")) {
                    lookingForRole = itemData.wantRole.split(",");
                } else {
                    lookingForRole = new String[]{itemData.wantRole};
                }
                for (int i = 0; i < lookingForRole.length; i++) {
                    int index = Integer.parseInt(lookingForRole[i]);
                    if ((index == 6 || index == 7 || index == 5)) {
                        if (otherRoleCount < 1) {
                            index = 5;
                            otherRoleCount++;
                        } else {
                            continue;
                        }
                    }
                    RoleBean roleBean = ShareFileUtils.getRoleBean(index);
                    if (roleBean != null) {
                        int listPoi = roleBean.listPoi;
                        if (listPoi == 5) {
                            newRoleList.add(listPoi);
                        }
                        sb.append(role_list.get(listPoi));

                    } else {

                    }
                    if (i != lookingForRole.length - 1) {
                        sb.append("，");
                    }
                }
                String content = sb.toString();
                if (content != null && content.length() > 0) {
                    if (content.endsWith("，")) {
                        content = content.substring(0, content.length() - 1);
                    }
                    interestMatch = getContext().getText(R.string.userinfo_right_lookingfor_role) + " " + content;

                }

            }
        } catch (Exception e) {
            e.printStackTrace();
            interestMatch = "";
        }

        return selfMatch + "  " + interestMatch;
    }

    private String generateInfo(MatchBean matchBean) {
        StringBuilder infoSB = new StringBuilder();
        Resources res = TheLApp.getContext().getResources();

        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar birthday = Calendar.getInstance();
        // 新版年龄
        if (!TextUtils.isEmpty(matchBean.birthday)) {
            try {
                birthday.setTime(sdf.parse(matchBean.birthday));
                infoSB.append(getBirthdayStr(birthday));
                infoSB.append(", ");
            } catch (Exception e) {
            }
        } else {// 旧版本年龄
            if (matchBean.age < 18) matchBean.age = 18;
            infoSB.append(matchBean.age + res.getString(R.string.updatauserinfo_activity_age_unit));
            infoSB.append(", ");
        }

        // 新版星座
        try {
            if (!TextUtils.isEmpty(matchBean.birthday)) {
                infoSB.append(DateUtils.date2Constellation(birthday));
            } else {// 旧版星座
                infoSB.append(constellation_list.get(Integer.valueOf(matchBean.horoscope)));
            }
        } catch (Exception e) {
        }

        // 身高体重
        int heightUnits = ShareFileUtils.getInt(ShareFileUtils.HEIGHT_UNITS, 0); // 身高单位(0=cm, 1=Inches)
        int weightUnits = ShareFileUtils.getInt(ShareFileUtils.WEIGHT_UNITS, 0); // 体重单位(0=kg, 1=Lbs)
        try {
            String height = "";
            String weight = "";
            if (heightUnits == 0) {
                if (matchBean.height != 0)
                    height = Float.valueOf(matchBean.height).intValue() + " cm";
            } else {
                if (matchBean.height != 0)
                    height = Utils.cmToInches(matchBean.height + "") + " Inches";
            }
            if (weightUnits == 0) {
                if (matchBean.weight != 0)
                    weight = Float.valueOf(matchBean.weight).intValue() + " kg";
            } else {
                if (matchBean.weight != 0)
                    weight = Utils.kgToLbs(matchBean.weight + "") + " Lbs";
            }
            if (!TextUtils.isEmpty(height)) {
                infoSB.append(", ");
                infoSB.append(height);
            }
            if (!TextUtils.isEmpty(weight)) {
                infoSB.append(", ");
                infoSB.append(weight);
            }
        } catch (Exception e) {
        }
        if (infoSB.toString().startsWith(", ")) return infoSB.toString().substring(2);
        return infoSB.toString();
    }

    /**
     * 格式：12岁
     *
     * @param birth
     * @return
     */
    private String getBirthdayStr(Calendar birth) {
        if (null == birth) {
            return "";
        }
        Calendar today = Calendar.getInstance();
        StringBuilder sb = new StringBuilder();
        try {
            int years = 18;
            if (today.get(Calendar.YEAR) - birth.get(Calendar.YEAR) > 18)
                years = today.get(Calendar.YEAR) - birth.get(Calendar.YEAR);
            sb.append(years).append(TheLApp.getContext().getString(R.string.updatauserinfo_activity_age_unit));
        } catch (Exception e) {
            return "";
        }

        return sb.toString();
    }

    private String generateNickname(String nickName) {
        if (TextUtils.isEmpty(nickName)) {
            return "***";
        } else {
            char codePoint = nickName.charAt(0);
            //是否包含表情符号
            if (!((codePoint == 0x0) || (codePoint == 0x9) || (codePoint == 0xA) || (codePoint == 0xD) || ((codePoint >= 0x20) && (codePoint <= 0xD7FF)) || ((codePoint >= 0xE000) && (codePoint <= 0xFFFD)) || ((codePoint >= 0x10000) && (codePoint <= 0x10FFFF)))) {

                return "?***";
            }
            return nickName.charAt(0) + "***";
        }
    }

    private String generateActiveTime(MatchBean matchBean) {

        String timeStr = "";
        if (!TextUtils.isEmpty(matchBean.activeTime)) {
            String[] number = matchBean.activeTime.split("_");
            switch (number[0]) {
                case "1"://second
                    timeStr = TheLApp.getContext().getResources().getString(R.string.active_second, number[1]);
                    break;
                case "2"://minute
                    timeStr = TheLApp.getContext().getResources().getString(R.string.active_minute, number[1]);
                    break;
                case "3"://hour
                    timeStr = TheLApp.getContext().getResources().getString(R.string.active_hour, number[1]);
                    break;
                case "4"://day
                    timeStr = TheLApp.getContext().getResources().getString(R.string.active_day, number[1]);
                    break;
            }
        }
        if (!TextUtils.isEmpty(matchBean.distance)) return matchBean.distance + " | " + timeStr;
        return timeStr;
    }

    public class ViewHolder {

        ImageView mCardImageView;
        TextView picture_num;
        TextView distance;
        TextView nickname;
        TextView details;
        TextView interest;
        ImageView more;
        ImageView img_vip;
        RelativeLayout card_top;
        LinearLayout card_bottom;
        RelativeLayout indicator;
        ImageView indicator_img;
        TextView indicator_tip1;
        TextView indicator_tip2;
        TextView edit_profile;
        LinearLayout superlike_container;
        LinearLayout indicator_tip2_container;
        ImageView superlike_note;
        ImageView indicator_tip2_img;
        RelativeLayout top_container;
        TextView ralationship_state;

        public ViewHolder(View convertView) {
            mCardImageView = convertView.findViewById(R.id.image);
            picture_num = convertView.findViewById(R.id.picture_num);
            distance = convertView.findViewById(R.id.distance);
            interest = convertView.findViewById(R.id.interest);
            details = convertView.findViewById(R.id.details);
            nickname = convertView.findViewById(R.id.nickname);
            ralationship_state = convertView.findViewById(R.id.ralationship_state);
            more = convertView.findViewById(R.id.more);
            img_vip = convertView.findViewById(R.id.img_vip);
            card_top = convertView.findViewById(R.id.card_top);
            card_bottom = convertView.findViewById(R.id.card_bottom);
            indicator = convertView.findViewById(R.id.indicator);
            indicator_img = convertView.findViewById(R.id.indicator_img);
            indicator_tip1 = convertView.findViewById(R.id.indicator_tip1);
            indicator_tip2 = convertView.findViewById(R.id.indicator_tip2);
            edit_profile = convertView.findViewById(R.id.edit_profile);
            superlike_container = convertView.findViewById(R.id.superlike_container);
            indicator_tip2_container = convertView.findViewById(R.id.indicator_tip2_container);
            superlike_note = convertView.findViewById(R.id.superlike_note);
            indicator_tip2_img = convertView.findViewById(R.id.indicator_tip2_img);
            top_container = convertView.findViewById(R.id.top_container);
        }

        @SuppressLint("SetTextI18n")
        public void bindData(final MatchBean itemData) {
            if (getContext() == null) return;
            //新手引导
            if (itemData.dataType != 0) {
                top_container.setVisibility(GONE);
                indicator.setVisibility(VISIBLE);
                @IdRes
                int resId = 0;
                switch (itemData.dataType) {
                    case 1:
                        resId = R.mipmap.icn_match_indicator_like;
                        indicator_tip1.setText(R.string.match_indicator_tip1);
                        indicator_tip2_container.setVisibility(VISIBLE);
                        indicator_tip2.setText(R.string.match_indicator_tip2);
                        edit_profile.setVisibility(GONE);
                        indicator_tip2_img.setVisibility(GONE);
                        break;
                    case 2:
                        resId = R.mipmap.icn_match_indicator_dislike;
                        indicator_tip1.setText(R.string.match_indicator_tip4);
                        indicator_tip2_container.setVisibility(GONE);
                        edit_profile.setVisibility(GONE);
                        indicator_tip2_img.setVisibility(GONE);
                        break;
                    case 3:
                        resId = R.mipmap.icn_match_indicator_data;
                        indicator_tip1.setText(R.string.match_indicator_tip6);
                        indicator_tip2_container.setVisibility(VISIBLE);
                        indicator_tip2.setText(R.string.match_indicator_tip7);
                        edit_profile.setVisibility(GONE);
                        indicator_tip2_img.setVisibility(GONE);
                        break;
                    case 4:
                        resId = R.mipmap.icn_match_indicator_data2;
                        indicator_tip1.setText(TheLApp.context.getString(R.string.match_indicator_tip8) + ratio + "%");
                        indicator_tip2_container.setVisibility(VISIBLE);
                        indicator_tip2.setText(R.string.match_indicator_tip9);
                        edit_profile.setVisibility(VISIBLE);
                        indicator_tip2_img.setVisibility(VISIBLE);
                        break;
                }
                ImageLoaderManager.imageLoader(indicator_img, resId);
                edit_profile.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getContext().startActivity(new Intent(getContext(), UpdateUserInfoActivity.class));
                    }
                });
            } else {
                top_container.setVisibility(VISIBLE);
                indicator.setVisibility(GONE);

                if (itemData.picList.size() > 0) {

                    String thumb = itemData.picList.get(0).longThumbnailUrl;
                    if (!TextUtils.isEmpty(thumb)){
                        RequestOptions requestOptions =new RequestOptions().transform(new CenterCrop(), new GlideRoundCornerTransform(DensityUtils.dp2px(12))).placeholder(R.drawable.material_card_white);
                        Glide.with(mCardImageView).load(ImageUtils.buildNetPictureUrl(thumb, TheLConstants.MAX_PIC_WIDTH, TheLConstants.MAX_PIC_HEIGHT)).apply(requestOptions).into(mCardImageView);
                    }
                    picture_num.setText(String.valueOf(itemData.picList.size()));
                } else {
                    picture_num.setText(String.valueOf(0));
                }

                if (itemData.vipLevel > 0) {
                    img_vip.setVisibility(VISIBLE);
                    switch (itemData.vipLevel) {
                        case 1:
                            img_vip.setImageResource(R.mipmap.icn_vip_1);
                            break;
                        case 2:
                            img_vip.setImageResource(R.mipmap.icn_vip_2);
                            break;
                        case 3:
                            img_vip.setImageResource(R.mipmap.icn_vip_3);
                            break;
                        case 4:
                            img_vip.setImageResource(R.mipmap.icn_vip_4);
                            break;
                        default:
                            img_vip.setVisibility(GONE);
                            break;
                    }
                } else {
                    img_vip.setVisibility(GONE);
                }

                if (itemData.isSuperLike == 1) {
                    card_bottom.setBackgroundResource(R.drawable.gradient_superlike_bg);
                    superlike_container.setVisibility(VISIBLE);
                    if (TextUtils.isEmpty(itemData.superLikeNote)) {
                        superlike_note.setVisibility(GONE);
                    } else {
                        superlike_note.setVisibility(VISIBLE);
                    }
                } else {
                    card_bottom.setBackgroundResource(0);
                    superlike_container.setVisibility(GONE);
                }

                distance.setText(generateActiveTime(itemData));
                nickname.setText(generateNickname(itemData.nickName));
                details.setText(generateInfo(itemData));

                interest.setText(generateRole(itemData));
                if (generateRalationship(itemData) != null) {
                    ralationship_state.setText(generateRalationship(itemData));
                } else {
                    ralationship_state.setVisibility(GONE);
                }
                more.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new ActionSheetDialog(getContext()).builder().setCancelable(true).setCanceledOnTouchOutside(true)
                                .addSheetItem(getContext().getString(R.string.updatauserinfo_activity_detail), ActionSheetDialog.SheetItemColor.BLACK, new ActionSheetDialog.OnSheetItemClickListener() {
                                    @Override
                                    public void onClick(int which) {
                                        Intent intent = new Intent(getContext(), DetailedMatchUserinfoActivity.class);
                                        Bundle bundle = new Bundle();
                                        bundle.putSerializable("MatchBean", itemData);
                                        bundle.putString("from", "RevertCardSlidePanel");
                                        bundle.putString(ShareFileUtils.MATCH_FROM_PAGE_ID, pageId);
                                        bundle.putString(ShareFileUtils.MATCH_FROM_PAGE, "match");

                                        intent.putExtras(bundle);
                                        if (getContext() instanceof Activity) {

                                            ((Activity) getContext()).startActivityForResult(intent, TheLConstants.BUNDLE_CODE_MATCH_ACTIVITY);
                                        }

                                    }
                                })
                                .addSheetItem(getContext().getString(R.string.userinfo_activity_dialog_report_title), ActionSheetDialog.SheetItemColor.RED, new ActionSheetDialog.OnSheetItemClickListener() {
                                    @Override
                                    public void onClick(int which) {
                                        if (itemData != null && itemData.userId != 0)
                                            gotoReportActivity(getContext(), itemData.userId + "");

                                    }
                                })
                                .show();
                    }
                });
            }
        }
    }

    private void traceMatchData(String page, String activity, MatchBean matchBean) {
        String latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
        String longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");

        LogInfoBean logInfoBean = new LogInfoBean();
        logInfoBean.page = page;
        logInfoBean.page_id = pageId;
        logInfoBean.activity = activity;
        logInfoBean.from_page = from_page;
        logInfoBean.from_page_id = from_page_id;
        logInfoBean.lat = latitude;
        logInfoBean.lng = longitude;

        LogInfoBean.LogsDataBean logsDataBean = new LogInfoBean.LogsDataBean();
        if (matchBean != null && !TextUtils.isEmpty(this.matchBean.data.rankId)) {
            logsDataBean.receiver_id = matchBean.userId + "";
            logsDataBean.rank_id = this.matchBean.data.rankId;
        }
        logInfoBean.data = logsDataBean;

        MatchLogUtils.getInstance().addLog(logInfoBean);


    }

    private void gotoReportActivity(Context context, String userId) {
        String latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
        String longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");

        LogInfoBean logInfoBean4 = new LogInfoBean();
        logInfoBean4.page = "match";
        logInfoBean4.page_id = pageId;
        logInfoBean4.activity = "report";
        logInfoBean4.from_page = from_page;
        logInfoBean4.from_page_id = from_page_id;
        logInfoBean4.lat = latitude;
        logInfoBean4.lng = longitude;

        LogInfoBean.LogsDataBean logsDataBean = new LogInfoBean.LogsDataBean();
        logsDataBean.receiver_id = dataList.get(currentPos).userId + "";
        if (matchBean != null && matchBean.data != null && !TextUtils.isEmpty(matchBean.data.rankId)) {
            logsDataBean.rank_id = matchBean.data.rankId;

        }
        logInfoBean4.data = logsDataBean;

        MatchLogUtils.getInstance().addLog(logInfoBean4);


        Intent intent = new Intent(context, ReportActivity.class);
        intent.putExtra(TheLConstants.BUNDLE_KEY_REPORT_TYPE, ReportActivity.REPORT_TYPE_REPORT_MATCH_USER);
        intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, userId);
        Bundle bundle = new Bundle();
        bundle.putString(ShareFileUtils.MATCH_FROM_PAGE_ID, pageId);
        bundle.putString(ShareFileUtils.MATCH_FROM_PAGE, "match");
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    private FlingCardListener flingCardListener;

    public void setFlingCardListener(FlingCardListener flingCardListener) {
        this.flingCardListener = flingCardListener;
    }

    public interface FlingCardListener {
        void loadMore();

        void usedOutToday();//今日速配次数用完
    }

    public class SuperlikeBroadCast extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (TheLConstants.BROADCAST_ACTION_SUPERLIKE.equals(intent.getAction())) {
                int superlikeRetain = ShareFileUtils.getInt(ShareFileUtils.SUPER_LIKE, 0);
                if (superlikeRetain > 0) {
                    superlike_count.setVisibility(VISIBLE);
                    superlike_count.setText(String.valueOf(superlikeRetain));
                } else {
                    superlike_count.setVisibility(INVISIBLE);
                }
            }
        }
    }
    public void traceMatchUserLog(String activity) {
        try {

            LogInfoBean logInfoBean = new LogInfoBean();
            logInfoBean.page = "match";
            logInfoBean.page_id = pageId;
            logInfoBean.activity = activity;

            MatchLogUtils.getInstance().addLog(logInfoBean);

        } catch (Exception e) {

        }
    }


}
