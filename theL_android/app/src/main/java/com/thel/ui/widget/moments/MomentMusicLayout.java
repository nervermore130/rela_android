package com.thel.ui.widget.moments;

import android.content.Context;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.bean.moments.MomentsBean;
import com.thel.ui.widget.SquareSimpleDraweeView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by liuyun on 2017/9/29.
 */

public class MomentMusicLayout extends LinearLayout {

    @BindView(R.id.song_name) TextView song_name;

    @BindView(R.id.album_name) TextView album_name;

    @BindView(R.id.artist_name) TextView artist_name;

    @BindView(R.id.moment_content_music_pic) SquareSimpleDraweeView moment_content_music_pic;

    public MomentMusicLayout(Context context) {
        super(context);
        initView();
    }

    public MomentMusicLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public MomentMusicLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.layout_moment_music, this, true);

        ButterKnife.bind(this, view);
    }

    public void setData(MomentsBean momentsBean) {
        song_name.setText(momentsBean.songName);
        album_name.setText(momentsBean.albumName);
        artist_name.setText(momentsBean.artistName);
        moment_content_music_pic.setImageURI(momentsBean.albumLogo444);
    }

}
