package com.thel.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.live.bean.DanmuBean;
import com.thel.modules.live.danmu.CreateDanmu;
import com.thel.modules.live.danmu.DanmuFactory;
import com.thel.utils.SizeUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by liuyun on 2018/1/27.
 */

public class DanmuView extends RelativeLayout {

    public int itemPos = 0;

    public boolean isAdd = false;

    public DanmuView(Context context) {
        super(context);
    }

    public DanmuView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DanmuView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public View initView(DanmuBean danmuBean, int itemPos) {

        this.itemPos = itemPos;

        CreateDanmu createDanmu = DanmuFactory.createDanmuView(danmuBean);

        if (createDanmu == null) {
            return null;
        }

        return createDanmu.createDanmuView(this, danmuBean);
    }

}
