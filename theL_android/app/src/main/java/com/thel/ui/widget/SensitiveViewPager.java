package com.thel.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import androidx.viewpager.widget.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.Scroller;

import com.thel.R;

import java.lang.reflect.Field;

/**
 * Created by chad
 * Time 18/2/9
 * Email: wuxianchuang@foxmail.com
 * Description: TODO 可调节滑动灵敏度的ViewPager
 */

public class SensitiveViewPager extends ViewPager {
    private VelocityTracker tracker;
    private float snap_velocity = 600;
    private float snap_distance_percent = 0.3f;
    private int downX;
    private final String TAG = "SensitiveViewPager";
    private int snap_duration = 250;
    private FixedSpeedScroller scroller;

    public SensitiveViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.sensitive_viewpager);
        for (int i = 0; i < a.getIndexCount(); i++) {
            switch (a.getIndex(i)) {
                case R.attr.snap_velocity:
                    snap_velocity = a.getDimension(R.attr.snap_velocity, snap_velocity);
                    break;
                case R.attr.snap_distance_percent:
                    snap_distance_percent = a.getFloat(R.attr.snap_distance_percent, snap_distance_percent);
                    break;
                case R.attr.snap_duration:
                    snap_duration = a.getInteger(R.attr.snap_duration, snap_duration);
                    break;
                default:
                    break;
            }
        }
        a.recycle();
        //change snap duration
        try {
            Field field = ViewPager.class.getDeclaredField("mScroller");
            field.setAccessible(true);
            scroller = new FixedSpeedScroller(getContext(), new AccelerateInterpolator());
            field.set(this, scroller);
            scroller.setDuration(snap_duration);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    public static class FixedSpeedScroller extends Scroller {
        private int mDuration;

        public FixedSpeedScroller(Context context) {
            super(context);
        }

        public FixedSpeedScroller(Context context, Interpolator interpolator) {
            super(context, interpolator);
        }

        @Override
        public void startScroll(int startX, int startY, int dx, int dy, int duration) {
            // Ignore received duration, use fixed one instead
            super.startScroll(startX, startY, dx, dy, mDuration);
        }

        @Override
        public void startScroll(int startX, int startY, int dx, int dy) {
            // Ignore received duration, use fixed one instead
            super.startScroll(startX, startY, dx, dy, mDuration);
        }

        public void setDuration(int time) {
            mDuration = time;
        }
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                downX = (int) event.getX();
                if (!scroller.isFinished()) {
                    scroller.abortAnimation();
                }
                break;
            default:
                break;
        }
        return super.onInterceptTouchEvent(event);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int currentItem = getCurrentItem();
        if (tracker == null) {
            tracker = VelocityTracker.obtain();
        }
        tracker.addMovement(event);
        boolean isHandle = false;
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                downX = (int) event.getX();
                break;
            case MotionEvent.ACTION_UP:
                tracker.computeCurrentVelocity(1000);
                int pageCount = getAdapter() == null ? 0 : getAdapter().getCount();
                float XVelocity = tracker.getXVelocity();
                if (Math.abs(XVelocity) > snap_velocity) {
                    Log.d(TAG, "XVelocity:" + XVelocity + " currentItem:" + currentItem + " pageCount:" + pageCount);
                    if (XVelocity > 0 && currentItem > 0) {
                        setCurrentItem(currentItem - 1, true);
                        isHandle = true;
                    } else if (XVelocity < 0 && currentItem < pageCount - 1) {
                        setCurrentItem(currentItem + 1, true);
                        isHandle = true;
                    }
                } else {
                    int curX = (int) event.getX();
                    int snapDistance = curX - downX;
                    if (Math.abs(snapDistance) > getWidth() * snap_distance_percent) {
                        Log.d(TAG, "snapDistance:" + snapDistance + " currentItem:" + currentItem + " pageCount:"
                                + pageCount + " downX:" + downX + " curX:" + curX);
                        if (currentItem > 0 && snapDistance > 0) {
                            setCurrentItem(currentItem - 1, true);
                            isHandle = true;
                        } else if (snapDistance < 0 && currentItem < pageCount - 1) {
                            setCurrentItem(currentItem + 1, true);
                            isHandle = true;
                        }
                    }
                }
                if (tracker != null) {
                    tracker.recycle();
                    tracker = null;
                }
                break;
            case MotionEvent.ACTION_MOVE:
                break;
            case MotionEvent.ACTION_CANCEL:
                if (tracker != null) {
                    tracker.recycle();
                    tracker = null;
                }
                break;
            default:
                break;
        }
        return isHandle || super.onTouchEvent(event);
    }
}
