package com.thel.ui.dialog;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.thel.R;

import androidx.fragment.app.DialogFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DeleteMsgDialog extends DialogFragment {

    @BindView(R.id.content_tv)
    TextView content_tv;

    @BindView(R.id.confirm_tv)
    TextView confirm_tv;

    @BindView(R.id.cancel_tv)
    TextView cancel_tv;

    private OnDeleteMsgListener mOnDeleteMsgListener;

    @Nullable @Override public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);

        View view = inflater.inflate(R.layout.dialog_confirm, container, false);

        ButterKnife.bind(this, view);

        return view;
    }

    @Override public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    @OnClick(R.id.confirm_tv) void onConfirm() {
        if (mOnDeleteMsgListener != null) {
            mOnDeleteMsgListener.onConfirm();
        }
        dismiss();
    }

    @OnClick(R.id.cancel_tv) void onCancel() {

        if (mOnDeleteMsgListener != null) {
            mOnDeleteMsgListener.onCancel();
        }

        dismiss();
    }

    public void setOnDeleteMsgListener(OnDeleteMsgListener mOnDeleteMsgListener) {
        this.mOnDeleteMsgListener = mOnDeleteMsgListener;
    }

    public interface OnDeleteMsgListener {

        void onConfirm();

        void onCancel();

    }

}
