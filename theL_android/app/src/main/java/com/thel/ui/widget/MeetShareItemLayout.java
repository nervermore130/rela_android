package com.thel.ui.widget;

import android.content.Context;
import androidx.annotation.NonNull;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.bean.live.LiveMultiSeatBean;
import com.thel.manager.ImageLoaderManager;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MeetShareItemLayout extends RelativeLayout {

    @BindView(R.id.avatar_left_iv) ImageView avatar_left_iv;

    @BindView(R.id.avatar_right_iv) ImageView avatar_right_iv;

    @BindView(R.id.user_name_left_tv) TextView user_name_left_tv;

    @BindView(R.id.user_name_right_tv) TextView user_name_right_tv;
    private LiveMultiSeatBean.CoupleDetail leftData;
    private LiveMultiSeatBean.CoupleDetail rightData;

    public MeetShareItemLayout(Context context) {
        super(context);
        init(context);
    }

    public MeetShareItemLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public MeetShareItemLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {

        View view = LayoutInflater.from(context).inflate(R.layout.layout_meet_share_item, this, true);

        ButterKnife.bind(this, view);

    }

    public void setData(@NonNull LiveMultiSeatBean.CoupleDetail leftData, @NonNull LiveMultiSeatBean.CoupleDetail rightData) {

        this.leftData = leftData;

        this.rightData = rightData;

        ImageLoaderManager.imageLoaderCircle(avatar_left_iv, R.mipmap.icon_user, leftData.avatar);

        user_name_left_tv.setText(leftData.nickname);

        ImageLoaderManager.imageLoaderCircle(avatar_right_iv, R.mipmap.icon_user, rightData.avatar);

        user_name_right_tv.setText(rightData.nickname);

    }

}
