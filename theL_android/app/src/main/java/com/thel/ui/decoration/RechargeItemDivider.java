package com.thel.ui.decoration;

import android.graphics.Rect;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

/**
 * Created by liuyun on 2017/12/29.
 */

public class RechargeItemDivider extends RecyclerView.ItemDecoration {

    private int space;

    public RechargeItemDivider(int space) {
        this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {

        outRect.left = space;

        outRect.top = space;

        outRect.right = space;

        outRect.bottom = space;

    }
}
