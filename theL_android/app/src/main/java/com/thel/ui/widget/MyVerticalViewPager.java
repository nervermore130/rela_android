package com.thel.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import fr.castorflex.android.verticalviewpager.VerticalViewPager;

/**
 * Created by the L on 2017/2/13.
 */

public class MyVerticalViewPager extends VerticalViewPager {

    private boolean mScolllable = true;

    public MyVerticalViewPager(Context context) {
        this(context, null);
    }

    public MyVerticalViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (!mScolllable) {
            return false;
        }
        return super.onTouchEvent(ev);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (!mScolllable) {
            return false;
        }
        return super.onInterceptTouchEvent(ev);
    }

    public boolean isCanScrollable() {
        return mScolllable;
    }

    public void setCanScrollable(boolean scrollable) {
        this.mScolllable = scrollable;
    }
}