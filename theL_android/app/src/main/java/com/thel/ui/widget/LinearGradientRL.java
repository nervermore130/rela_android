package com.thel.ui.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.LinearLayout;

/**
 * @author liuyun
 */
public class LinearGradientRL extends LinearLayout {

    private static final String TAG = "LinearGradientRL";

    private Paint mPaint;

    private LinearGradient mLinearGradient = null;

    private int colorStart = -1;

    private int colorEnd = -1;

    private int width = 0;

    private int height = 0;

    private RectF mRectF;

    public LinearGradientRL(Context context) {
        this(context, null);
    }

    public LinearGradientRL(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LinearGradientRL(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mRectF = new RectF();
    }

    public void setBGColor(int colorStart, int colorEnd) {
        this.colorStart = colorStart;
        this.colorEnd = colorEnd;
        invalidate();
    }

    public void setBGColor(int color) {
        this.colorStart = color;
        this.colorEnd = color;
        mLinearGradient = new LinearGradient(0, 0, width, height, colorStart, colorEnd, Shader.TileMode.CLAMP);
        invalidate();
    }

    @SuppressLint("DrawAllocation") @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        width = getWidth();
        height = getHeight();
        mRectF.set(0, 0, width, height);
        mLinearGradient = new LinearGradient(0, 0, width, height, colorStart, colorEnd, Shader.TileMode.CLAMP);
        mPaint.setShader(mLinearGradient);
        canvas.drawRoundRect(mRectF, dip2px(getContext(), 12.5f), dip2px(getContext(), 12.5f), mPaint);
    }

    private int dip2px(Context context, float dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }
}