package com.thel.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Layout;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.ImgShareBean;
import com.thel.bean.comment.CommentBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.modules.main.home.moments.comment.MomentCommentActivity;
import com.thel.modules.preview_image.UserInfoPhotoActivity;
import com.thel.ui.widget.ExpandTextView;
import com.thel.ui.widget.LikeButtonView;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;
import com.thel.utils.AppInit;
import com.thel.utils.EmojiUtils;
import com.thel.utils.MomentUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;

import java.util.ArrayList;

import static android.view.View.GONE;
import static com.thel.constants.TheLConstantsExt.MOMENT_TEXT_CONTENT_MAX_LINE;

/**
 * 日志评论列表的adapter，包括普通日志和话题日志
 */
public class ThemeReplyRecyclerViewAdapter extends BaseRecyclerViewAdapter<CommentBean> {

    private OnClickListener mOnClickListener;

    private Context mContext;

    public ThemeReplyRecyclerViewAdapter(ArrayList<CommentBean> commentlist, OnClickListener onClickListener) {
        super(R.layout.theme_reply_listitem, commentlist);
        mOnClickListener = onClickListener;
    }

    public void setContext(Context context) {
        mContext = context;
    }

    @Override
    public void convert(final BaseViewHolder holdView, final CommentBean commentBean) {

        setViewSize(holdView);

        holdView.setVisibility(R.id.rv_likes, GONE);

        ((LikeButtonView) (holdView.getView(R.id.moment_opts_like))).setLikeAnimImage(holdView.getView(R.id.img_like_anim));

        hideAllPic(holdView);// 先隐藏所有照片，以免滑动时错位
        // 头像和昵称
        holdView.setImageUrl(R.id.img_thumb, commentBean.avatar, TheLConstants.AVATAR_MIDDLE_SIZE, TheLConstants.AVATAR_MIDDLE_SIZE);
        holdView.setTag(R.id.img_thumb, commentBean.userId);
        holdView.setOnClickListener(R.id.img_thumb, mOnClickListener);
        holdView.setText(R.id.moment_user_name, commentBean.nickname);
        // 发布时间
        holdView.setText(R.id.moment_release_time, MomentUtils.getReleaseTimeShow(commentBean.commentTime));
        //关注
        if (CommentBean.FOLLOW_TYPE_UNFOLLOWED == commentBean.followerStatus && !ShareFileUtils.getString(ShareFileUtils.ID, "").equals(commentBean.userId + "")) {
            holdView.setVisibility(R.id.txt_follow, View.VISIBLE);
        } else {
            holdView.setVisibility(R.id.txt_follow, GONE);
        }
        holdView.setTag(R.id.txt_follow, R.id.comment_tag, holdView.getAdapterPosition() - getHeaderLayoutCount());
        holdView.setOnClickListener(R.id.txt_follow, mOnClickListener);

        if (!TextUtils.isEmpty(commentBean.commentText)) {
            final ExpandTextView txt_content = holdView.getView(R.id.moment_content_text);
            txt_content.setVisibility(View.VISIBLE);
            // comment文字内容
            txt_content.setText(EmojiUtils.getInstace(EmojiUtils.DEFAULT_SIZE).getExpressionString(TheLApp.getContext(), commentBean.commentText));
            /** 2.19.2显示全文 start **/
            final TextView txt_whole = holdView.getView(R.id.txt_whole);
            if (commentBean.contentStatus == MomentsBean.CONTENT_STATUS_HANG) {//状态为收起状态
                txt_whole.setText(TheLApp.getContext().getString(R.string.whole_content));//显示为全文
                txt_content.setMaxLines(MOMENT_TEXT_CONTENT_MAX_LINE);
            } else {
                txt_whole.setText(TheLApp.getContext().getString(R.string.pack_up));//显示为收起
                txt_content.setMaxLines(Integer.MAX_VALUE);
            }
            txt_content.setTextChangedListener(new ExpandTextView.TextChangedListener() {
                @Override
                public void textChanged(TextView textView) {
                    if (View.VISIBLE == txt_content.getVisibility()) {
                        final Layout layout = txt_content.getLayout();
                        final int lineCount = layout.getLineCount();
                        commentBean.textLines = lineCount;
                        int eCount = 0;
                        if (lineCount >= MOMENT_TEXT_CONTENT_MAX_LINE) {
                            eCount = layout.getEllipsisCount(MOMENT_TEXT_CONTENT_MAX_LINE - 1);
                        }
                        if (eCount > 0 || lineCount > MOMENT_TEXT_CONTENT_MAX_LINE) {
                            txt_whole.setVisibility(View.VISIBLE);
                        } else {
                            txt_whole.setVisibility(GONE);
                        }
                    } else {
                        txt_whole.setVisibility(GONE);
                    }
                }
            });
            txt_whole.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (commentBean.textLines > MomentsBean.MAX_EXPAND_LINES) {
//                        Intent intent = new Intent();
//                        intent.setClass(TheLApp.getContext(), MomentCommentActivity.class);
//                        intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, commentBean.commentId);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        TheLApp.getContext().startActivity(intent);
                        FlutterRouterConfig.Companion.gotoMomentDetails(commentBean.commentId);
                    } else {
                        if (commentBean.contentStatus == MomentsBean.CONTENT_STATUS_HANG) {//当前状态是收起，显示全文
                            commentBean.contentStatus = MomentsBean.CONTENT_STATUS_WHOLE;
                            txt_content.setMaxLines(Integer.MAX_VALUE);
                            txt_whole.setText(TheLApp.getContext().getString(R.string.pack_up));//显示为收起
                        } else {//否者，收起
                            commentBean.contentStatus = MomentsBean.CONTENT_STATUS_HANG;
                            txt_content.setMaxLines(MOMENT_TEXT_CONTENT_MAX_LINE);
                            txt_whole.setText(TheLApp.getContext().getString(R.string.whole_content));//显示为全文
                        }
                    }
                }
            });
            /** 2.19.2显示全文 end**/
        } else {
            holdView.setVisibility(R.id.moment_content_text, GONE);
            holdView.setVisibility(R.id.txt_whole, GONE);
        }

        updateLikeStatus(commentBean, holdView);

        updateCommentNum(commentBean, holdView);

        // 去点赞
        holdView.setOnClickListener(R.id.moment_opts_like, mOnClickListener);
        holdView.setTag(R.id.moment_opts_like, R.id.moment_id_tag, commentBean.commentId);
        // 日志操作
        holdView.setOnClickListener(R.id.moment_opts_more, mOnClickListener);
        holdView.setTag(R.id.moment_opts_more, R.id.comment_tag, commentBean);
        // 日志评论
        holdView.setOnClickListener(R.id.moment_opts_comment, mOnClickListener);
        holdView.setTag(R.id.moment_opts_comment, R.id.moment_id_tag, commentBean.commentId);

        if (!TextUtils.isEmpty(commentBean.imageUrl)) {// moment图片内容
            String[] picUrls = commentBean.imageUrl.split(",");
            if (picUrls.length > 0) {
                if (picUrls.length == 1) {
                    holdView.setVisibility(R.id.pic, View.VISIBLE);
                    holdView.setImageUrl(R.id.pic, picUrls[0], TheLConstants.MOMENT_PIC_BIG_SIZE, TheLConstants.MOMENT_PIC_BIG_SIZE);
                } else {
                    holdView.setVisibility(R.id.pic, GONE);
                    holdView.setVisibility(R.id.pic1, View.VISIBLE);
                    holdView.setImageUrl(R.id.pic1, picUrls[0], TheLConstants.MOMENT_PIC_BIG_SIZE, TheLConstants.MOMENT_PIC_BIG_SIZE);
                    holdView.setVisibility(R.id.pic2, View.VISIBLE);
                    holdView.setImageUrl(R.id.pic2, picUrls[1], TheLConstants.MOMENT_PIC_SMALL_SIZE, TheLConstants.MOMENT_PIC_SMALL_SIZE);
                    if (picUrls.length > 2) {
                        holdView.setVisibility(R.id.pic3, View.VISIBLE);
                        holdView.setImageUrl(R.id.pic3, picUrls[2], TheLConstants.MOMENT_PIC_SMALL_SIZE, TheLConstants.MOMENT_PIC_SMALL_SIZE);
                        if (picUrls.length > 3) {
                            holdView.setVisibility(R.id.pic4, View.VISIBLE);
                            holdView.setImageUrl(R.id.pic4, picUrls[3], TheLConstants.MOMENT_PIC_SMALL_SIZE, TheLConstants.MOMENT_PIC_SMALL_SIZE);
                            if (picUrls.length > 4) {
                                holdView.setVisibility(R.id.pic5, View.VISIBLE);
                                holdView.setImageUrl(R.id.pic5, picUrls[4], TheLConstants.MOMENT_PIC_SMALL_SIZE, TheLConstants.MOMENT_PIC_SMALL_SIZE);
                                if (picUrls.length > 5) {
                                    holdView.setVisibility(R.id.pic6, View.VISIBLE);
                                    holdView.setImageUrl(R.id.pic6, picUrls[5], TheLConstants.MOMENT_PIC_SMALL_SIZE, TheLConstants.MOMENT_PIC_SMALL_SIZE);
                                }
                            }
                        }
                    }
                    setPicsLayoutParams(holdView, picUrls.length);
                }
            }
            holdView.setVisibility(R.id.moment_content_pic, View.VISIBLE);
            holdView.setOnClickListener(R.id.pic, new OnClickListener() {
                @Override
                public void onClick(View v) {
                    ViewUtils.preventViewMultipleClick(v, 1000);
                    openPhoto(commentBean.imageUrl, 0, commentBean.userName, commentBean);
                }
            });
            holdView.setOnClickListener(R.id.pic1, new OnClickListener() {
                @Override
                public void onClick(View v) {
                    ViewUtils.preventViewMultipleClick(v, 1000);
                    openPhoto(commentBean.imageUrl, 0, commentBean.userName, commentBean);
                }
            });
            holdView.setOnClickListener(R.id.pic2, new OnClickListener() {
                @Override
                public void onClick(View v) {
                    ViewUtils.preventViewMultipleClick(v, 1000);
                    openPhoto(commentBean.imageUrl, 1, commentBean.userName, commentBean);
                }
            });
            holdView.setOnClickListener(R.id.pic3, new OnClickListener() {
                @Override
                public void onClick(View v) {
                    ViewUtils.preventViewMultipleClick(v, 1000);
                    openPhoto(commentBean.imageUrl, 2, commentBean.userName, commentBean);
                }
            });
            holdView.setOnClickListener(R.id.pic4, new OnClickListener() {
                @Override
                public void onClick(View v) {
                    ViewUtils.preventViewMultipleClick(v, 1000);
                    openPhoto(commentBean.imageUrl, 3, commentBean.userName, commentBean);
                }
            });
            holdView.setOnClickListener(R.id.pic5, new OnClickListener() {
                @Override
                public void onClick(View v) {
                    ViewUtils.preventViewMultipleClick(v, 1000);
                    openPhoto(commentBean.imageUrl, 4, commentBean.userName, commentBean);
                }
            });
            holdView.setOnClickListener(R.id.pic6, new OnClickListener() {
                @Override
                public void onClick(View v) {
                    ViewUtils.preventViewMultipleClick(v, 1000);
                    openPhoto(commentBean.imageUrl, 5, commentBean.userName, commentBean);
                }
            });
        } else

        {
            holdView.setVisibility(R.id.moment_content_pic, GONE);
        }

        if (mContext != null) {
            // 最近三条评论
            LinearLayout lin_latest_comments = holdView.getView(R.id.lin_latest_comments);
            lin_latest_comments.removeAllViews();

            MomentUtils.addComments(holdView.convertView.getContext(), lin_latest_comments, commentBean.commentList, commentBean.momentsId, commentBean.commentType);
        }

    }

    private void openPhoto(String photoUrl, int position, String relaId, CommentBean commentBean) {
        Intent intent1 = new Intent(TheLApp.getContext(), UserInfoPhotoActivity.class);
        Bundle bundle = new Bundle();
        ImgShareBean bean = Utils.getThemeImageShareBean(commentBean);
        bundle.putSerializable(TheLConstants.BUNDLER_KEY_IMAGESHAREBEAN, bean);
        intent1.putExtras(bundle);
        String[] photoUrls = photoUrl.split(",");
        ArrayList<String> photos = new ArrayList<>();
        for (int i = 0; i < photoUrls.length; i++) {
            photos.add(photoUrls[i]);
        }
        intent1.putStringArrayListExtra(TheLConstants.BUNDLE_KEY_PHOTOS, photos);
        intent1.putExtra("position", position);
        intent1.putExtra(TheLConstants.IS_WATERMARK, true);
        intent1.putExtra(TheLConstants.BUNDLE_KEY_RELA_ID, relaId);
        intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        TheLApp.getContext().startActivity(intent1);
    }

    /**
     * 当图片数大于1张时，要分2~6张不同的数量展示不同的布局
     *
     * @param helper
     * @param count
     */
    private void setPicsLayoutParams(BaseViewHolder helper, int count) {
        float photoMargin = TheLApp.getContext().getResources().getDimension(R.dimen.moment_list_photo_margin);
        int bigSize = (int) (AppInit.displayMetrics.widthPixels - TheLApp.getContext().getResources().getDimension(R.dimen.moment_list_margin) * 2);
        int smallSize = (int) (bigSize - photoMargin) / 2;
        int tinySize = (int) ((bigSize - photoMargin * 2) / 3);
        if (count == 2 || count == 5) {
            final RelativeLayout.LayoutParams smallSizeParamsLeft = new RelativeLayout.LayoutParams(smallSize, smallSize);
            final RelativeLayout.LayoutParams smallSizeParamsRight = new RelativeLayout.LayoutParams(smallSize, smallSize);
            smallSizeParamsRight.leftMargin = (int) photoMargin;
            smallSizeParamsRight.addRule(RelativeLayout.RIGHT_OF, R.id.pic1);
            helper.getView(R.id.pic1).setLayoutParams(smallSizeParamsLeft);
            helper.getView(R.id.pic2).setLayoutParams(smallSizeParamsRight);
            if (count == 5) {
                final RelativeLayout.LayoutParams params3 = new RelativeLayout.LayoutParams(tinySize, tinySize);
                params3.addRule(RelativeLayout.BELOW, R.id.pic1);
                params3.topMargin = (int) photoMargin;
                helper.getView(R.id.pic3).setLayoutParams(params3);
                final RelativeLayout.LayoutParams params4 = new RelativeLayout.LayoutParams(tinySize, tinySize);
                params4.addRule(RelativeLayout.BELOW, R.id.pic1);
                params4.addRule(RelativeLayout.RIGHT_OF, R.id.pic3);
                params4.topMargin = (int) photoMargin;
                params4.leftMargin = (int) photoMargin;
                helper.getView(R.id.pic4).setLayoutParams(params4);
                final RelativeLayout.LayoutParams params5 = new RelativeLayout.LayoutParams(tinySize, tinySize);
                params5.addRule(RelativeLayout.BELOW, R.id.pic1);
                params5.addRule(RelativeLayout.RIGHT_OF, R.id.pic4);
                params5.topMargin = (int) photoMargin;
                params5.leftMargin = (int) photoMargin;
                helper.getView(R.id.pic5).setLayoutParams(params5);
            }
        } else if (count == 3) {
            final RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, smallSize);
            helper.getView(R.id.pic1).setLayoutParams(params1);
            final RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(smallSize, smallSize);
            params2.topMargin = (int) photoMargin;
            params2.addRule(RelativeLayout.BELOW, R.id.pic1);
            final RelativeLayout.LayoutParams params3 = new RelativeLayout.LayoutParams(smallSize, smallSize);
            params3.leftMargin = (int) photoMargin;
            params3.topMargin = (int) photoMargin;
            params3.addRule(RelativeLayout.BELOW, R.id.pic1);
            params3.addRule(RelativeLayout.RIGHT_OF, R.id.pic2);
            helper.getView(R.id.pic2).setLayoutParams(params2);
            helper.getView(R.id.pic3).setLayoutParams(params3);
        } else if (count == 4) {
            final RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(smallSize, smallSize);
            helper.getView(R.id.pic1).setLayoutParams(params1);
            final RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(smallSize, smallSize);
            params2.leftMargin = (int) photoMargin;
            params2.addRule(RelativeLayout.RIGHT_OF, R.id.pic1);
            helper.getView(R.id.pic2).setLayoutParams(params2);
            final RelativeLayout.LayoutParams params3 = new RelativeLayout.LayoutParams(smallSize, smallSize);
            params3.topMargin = (int) photoMargin;
            params3.addRule(RelativeLayout.BELOW, R.id.pic1);
            helper.getView(R.id.pic3).setLayoutParams(params3);
            final RelativeLayout.LayoutParams params4 = new RelativeLayout.LayoutParams(smallSize, smallSize);
            params4.topMargin = (int) photoMargin;
            params4.leftMargin = (int) photoMargin;
            params4.addRule(RelativeLayout.BELOW, R.id.pic2);
            params4.addRule(RelativeLayout.RIGHT_OF, R.id.pic3);
            helper.getView(R.id.pic4).setLayoutParams(params4);
        } else if (count == 6) {
            int pic1Size = (int) (tinySize * 2 + photoMargin);
            final RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(pic1Size, pic1Size);
            helper.getView(R.id.pic1).setLayoutParams(params1);
            final RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(tinySize, tinySize);
            params2.addRule(RelativeLayout.RIGHT_OF, R.id.pic1);
            params2.leftMargin = (int) photoMargin;
            helper.getView(R.id.pic2).setLayoutParams(params2);
            final RelativeLayout.LayoutParams params3 = new RelativeLayout.LayoutParams(tinySize, tinySize);
            params3.addRule(RelativeLayout.RIGHT_OF, R.id.pic1);
            params3.addRule(RelativeLayout.BELOW, R.id.pic2);
            params3.leftMargin = (int) photoMargin;
            params3.topMargin = (int) photoMargin;
            helper.getView(R.id.pic3).setLayoutParams(params3);
            final RelativeLayout.LayoutParams params4 = new RelativeLayout.LayoutParams(tinySize, tinySize);
            params4.addRule(RelativeLayout.BELOW, R.id.pic1);
            params4.topMargin = (int) photoMargin;
            helper.getView(R.id.pic4).setLayoutParams(params4);
            final RelativeLayout.LayoutParams params5 = new RelativeLayout.LayoutParams(tinySize, tinySize);
            params5.addRule(RelativeLayout.RIGHT_OF, R.id.pic4);
            params5.addRule(RelativeLayout.BELOW, R.id.pic1);
            params5.leftMargin = (int) photoMargin;
            params5.topMargin = (int) photoMargin;
            helper.getView(R.id.pic5).setLayoutParams(params5);
            final RelativeLayout.LayoutParams params6 = new RelativeLayout.LayoutParams(tinySize, tinySize);
            params6.addRule(RelativeLayout.RIGHT_OF, R.id.pic5);
            params6.addRule(RelativeLayout.BELOW, R.id.pic1);
            params6.leftMargin = (int) photoMargin;
            params6.topMargin = (int) photoMargin;
            helper.getView(R.id.pic6).setLayoutParams(params6);
        }
    }

    private void hideAllPic(BaseViewHolder holdView) {
        holdView.setVisibility(R.id.pic, GONE);
        holdView.setVisibility(R.id.pic1, GONE);
        holdView.setVisibility(R.id.pic2, GONE);
        holdView.setVisibility(R.id.pic3, GONE);
        holdView.setVisibility(R.id.pic4, GONE);
        holdView.setVisibility(R.id.pic5, GONE);
        holdView.setVisibility(R.id.pic6, GONE);
    }

    public void likeMomentOrNot(RecyclerView recyclerView, int position) {
        try {
            final CommentBean commentBean = getItem(position);
            // 注意这里需要加上header的count
            final View item = recyclerView.getLayoutManager().findViewByPosition(position + getHeaderLayoutCount());
            final BaseViewHolder helper = (BaseViewHolder) recyclerView.getChildViewHolder(item);
            updateLikeStatus(commentBean, helper);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 刷新点赞状态
     *
     * @param commentBean
     * @param helper
     */
    private void updateLikeStatus(CommentBean commentBean, BaseViewHolder helper) {
        // 是否已点过赞
        if (commentBean.winkFlag != MomentsBean.HAS_NOT_WINKED) {
            ((LikeButtonView) (helper.getView(R.id.moment_opts_like))).setChecked(true);
        } else {
            ((LikeButtonView) (helper.getView(R.id.moment_opts_like))).setChecked(false);
        }
        helper.setTag(R.id.moment_opts_like, R.id.moment_like_tag, commentBean.winkFlag);
        // 点赞数
        if (commentBean.winkNum <= 0) {
            helper.setVisibility(R.id.moment_opts_emoji_txt, GONE);
        } else {
            helper.setVisibility(R.id.moment_opts_emoji_txt, View.VISIBLE);
            helper.setTag(R.id.moment_opts_emoji_txt, R.id.moment_id_tag, commentBean.momentsId);
            helper.setText(R.id.moment_opts_emoji_txt, TheLApp.getContext().getString(commentBean.winkNum == 1 ? R.string.like_count_odd : R.string.like_count_even, commentBean.winkNum));
        }
    }

    private void updateCommentNum(CommentBean commentBean, BaseViewHolder helper) {
        helper.setText(R.id.moment_opts_comment_txt, commentBean.commentNum + "");
        // 评论数
        if (commentBean.commentNum <= 0) {
            helper.setVisibility(R.id.moment_opts_comment_txt, GONE);
        } else {
            helper.setVisibility(R.id.moment_opts_comment_txt, View.VISIBLE);
            helper.setText(R.id.moment_opts_comment_txt, TheLApp.getContext().getString(commentBean.commentNum <= 1 ? R.string.comment_count_odd : R.string.comment_count_even, commentBean.commentNum));
        }
    }

    private void setViewSize(BaseViewHolder helper) {
        int bigSize = (int) (AppInit.displayMetrics.widthPixels - TheLApp.getContext().getResources().getDimension(R.dimen.moment_list_margin) * 2);
        final RelativeLayout.LayoutParams bigSizeParams = new RelativeLayout.LayoutParams(bigSize, bigSize);
        bigSizeParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        helper.getView(R.id.pic).setLayoutParams(bigSizeParams);
    }
}
