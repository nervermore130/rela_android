package com.thel.ui.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.LinearInterpolator;

import com.thel.R;
import com.thel.utils.L;

/**
 * StaticLayout 构造方法参数
 * CharSequence source, //需要分行的字符串
 * int bufstart, //分行的字符串从第几的位置开始
 * int bufend, //分行的字符串从第几的位置结束
 * TextPaint paint,
 * int outerwidth, //宽度，字符串超出宽度时自动换行
 * Alignment align, //有ALIGN_CENTER， ALIGN_NORMAL， ALIGN_OPPOSITE 三种
 * float spacingmult, //相对行间距，相对字体大小，1.5f表示行间距为1.5倍的字体高度
 * float spacingadd,. //在基础行距上添加多少实际行间距等于这两者的和。
 * boolean includepad,
 * TextUtils.TruncateAt ellipsize, //从什么位置开始省略
 * int ellipsizedWidth //超过多少开始省略需要指出的是这layout是默认画在Canvas的(0,0)点的，如果需要调整位置只能在draw前移Canvas的起始坐标canvas.translate(x,y);
 */

/**
 * @author LiuYun
 * @Date 2018-08-27
 * email poikl369@qq.com
 */

public class GuideView extends View {

    private static final String TAG = "GuideView";

    private Paint mPaint;

    private TextPaint mTextPaint;

    private Path mPath;

    private RectF mRectF = new RectF();

    private Path mArrowPath;

    private int targetViewWidth = -1;

    private int targetViewHeight = -1;

    private int targetViewX = -1;

    private int targetViewY = -1;

    private StaticLayout mStaticLayout;

    private String text;

    private int radius;

    private int padding = 20;

    private int arrowHeight = 20;

    private int arrowWidth = 40;

    private int bgColor;

    private int arrowColor;

    private int textColor;

    private int animOffset;

    private int arrowDirection = 1;

    private int textSize = 35;

    private int textWidth = 0;

    private boolean isShowShadow = true;

    private ValueAnimator mCurrentAnimator;

    private int top;

    private boolean isShowing = false;

    private boolean arrawOffset;//add for first charge guide

    private View view;

    public GuideView(Context context) {
        super(context);
        init();
    }

    public GuideView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initAttributeSet(attrs);
        init();
    }

    public GuideView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAttributeSet(attrs);
        init();
    }

    private void initAttributeSet(AttributeSet attrs) {

        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.GuideView);

        text = typedArray.getString(R.styleable.GuideView_text);

        radius = typedArray.getDimensionPixelSize(R.styleable.GuideView_bg_radius, 15);

        padding = typedArray.getDimensionPixelSize(R.styleable.GuideView_bg_padding, 20);

        bgColor = typedArray.getColor(R.styleable.GuideView_guide_bg_color, getContext().getResources().getColor(R.color.rela_color));

        arrowColor = typedArray.getColor(R.styleable.GuideView_arrow_color, getContext().getResources().getColor(R.color.rela_color));

        arrowHeight = typedArray.getDimensionPixelSize(R.styleable.GuideView_arrow_height, 20);

        arrowWidth = typedArray.getDimensionPixelSize(R.styleable.GuideView_arrow_width, 10);

        textColor = typedArray.getColor(R.styleable.GuideView_text_color, getContext().getResources().getColor(R.color.white));

        animOffset = typedArray.getDimensionPixelSize(R.styleable.GuideView_anim_offset, 10);

        arrowDirection = typedArray.getInt(R.styleable.GuideView_arrow_direction, 1);

        isShowShadow = typedArray.getBoolean(R.styleable.GuideView_showShadow, true);

        textSize = typedArray.getDimensionPixelSize(R.styleable.GuideView_textSize, 35);

        typedArray.recycle();
    }

    private void init() {
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setColor(bgColor);
        if (isShowShadow) {
            mPaint.setMaskFilter(new BlurMaskFilter(20, BlurMaskFilter.Blur.SOLID));
        }
        setLayerType(View.LAYER_TYPE_SOFTWARE, mPaint);

        mTextPaint = new TextPaint();
        mTextPaint.setAntiAlias(true);
        mTextPaint.setColor(textColor);
        mTextPaint.setStyle(Paint.Style.FILL);
        mTextPaint.setTextSize(textSize);
        mPath = new Path();

        mArrowPath = new Path();
    }

    //
    public void setArrowOffset(boolean arrawoffset) {
        this.arrawOffset = arrawoffset;
    }

    public void show(View view, String text) {
//        L.d(TAG, "show  isShowing : " + isShowing+" "+text+" "+view);
        if (isShowing) {
            return;
        }

        if (text != null) {
            this.text = text;
        }

        this.view = view;

        if (view != null) {
            view.getRootView().getViewTreeObserver().addOnGlobalLayoutListener(mOnGlobalLayoutListener);
            view.requestLayout();
        }

    }

    public boolean isVisible() {
        return getVisibility() == View.VISIBLE;
    }

    public void reset() {
        if (mCurrentAnimator != null && (mCurrentAnimator.isRunning() || mCurrentAnimator.isStarted())) {
            mCurrentAnimator.cancel();
        }
        mCurrentAnimator.end();
        setVisibility(INVISIBLE);
        isShowing = false;
    }


    @SuppressLint("DrawAllocation")
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (targetViewWidth == -1 || targetViewHeight == -1 || mStaticLayout == null || textWidth == 0) {
            return;
        }

        canvas.save();

        Log.d(TAG, " text : " + text);

        //视图的高度
        int viewHeight = mStaticLayout.getHeight() + arrowHeight - getPaddingTop();

        //视图的宽度
        int viewWidth = textWidth - getPaddingLeft();

        Log.d(TAG, " viewWidth : " + viewWidth);

        //视图距离左边屏幕左边的距离
        int left = targetViewX + targetViewWidth / 2 - viewWidth / 2;

        //如果视图超出屏幕则需要这个偏移量
        int offset = 0;

        //当视图超出屏幕左边时
        if (left < 0) {

            offset = Math.abs(left) + 40;

        }

        //当视图超出屏幕右边时
        if (left + mStaticLayout.getWidth() > getRootView().getMeasuredWidth()) {

            offset = getRootView().getMeasuredWidth() - mStaticLayout.getWidth() - left - padding / 2;

            Log.d(TAG, " 引导右边超出了屏幕 : " + offset);

        }
        if (arrawOffset && offset == 0) {
            offset = - mStaticLayout.getWidth()/3;
        }

        //箭头向下时距离顶部的高度
        if (arrowDirection == 1) {
            //视图距离屏幕顶部的高度
            top = targetViewY - mStaticLayout.getHeight() - padding - arrowHeight;

        }

        //箭头向上时距离顶部的高度
        if (arrowDirection == 0) {
            top = targetViewY + targetViewHeight + arrowHeight;
        }

        mPath.reset();

        mArrowPath.reset();

//        if (offset) {
//
//        } else {
//
//        }
        mRectF.set(left + offset - padding / 2, top, viewWidth + left + padding / 2 - radius / 2 + offset, viewHeight - arrowHeight + top + padding);

        mPath.addRoundRect(mRectF, radius, radius, Path.Direction.CW);

        canvas.drawPath(mPath, mPaint);

        int arrowX = 0;

        int arrowY = 0;

        if (arrowDirection == 1) {
            //箭头的第一个点
            mArrowPath.moveTo((viewWidth - arrowWidth) / 2 + left, viewHeight - arrowHeight + top + padding);
            //箭头的第二个点
            mArrowPath.lineTo(viewWidth / 2 + arrowWidth / 2 + left, viewHeight - arrowHeight + top + padding);
            //箭头的第三个点
            mArrowPath.lineTo((viewWidth - arrowWidth) / 2 + arrowWidth / 2 + left, viewHeight + top + padding);

            arrowX = (viewWidth - arrowWidth) / 2 + arrowWidth / 2 + left;

            arrowY = viewHeight + top + padding;

        }

        if (arrowDirection == 0) {
            //箭头的第一个点
            mArrowPath.moveTo((viewWidth - arrowWidth) / 2 + left, top);
            //箭头的第二个点
            mArrowPath.lineTo(viewWidth / 2 + arrowWidth / 2 + left, top);
            //箭头的第三个点
            mArrowPath.lineTo((viewWidth - arrowWidth) / 2 + arrowWidth / 2 + left, top - arrowHeight);

            arrowX = (viewWidth - arrowWidth) / 2 + arrowWidth / 2 + left;

            arrowY = top - arrowHeight;
        }

        setPivotX(arrowX);

        setPivotY(arrowY);

        mArrowPath.close();

        canvas.drawPath(mArrowPath, mPaint);
        //移动文字
        canvas.translate(left + offset, top + padding / 2);

        mStaticLayout.draw(canvas);

        canvas.restore();


    }

    @Override protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (mCurrentAnimator != null && (mCurrentAnimator.isRunning() || mCurrentAnimator.isStarted())) {
            mCurrentAnimator.cancel();
        }
        if (mPaint != null) {
            setLayerType(LAYER_TYPE_NONE, mPaint);
        }
    }

    private int getTextWidth(int textWidth) {
        int line = (textWidth + 200) / getRootView().getMeasuredWidth();

        return textWidth / (line + 1);
    }

    private void startAnimator() {

        isShowing = true;

        final ValueAnimator valueAnimator = ValueAnimator.ofInt(0, animOffset, 0);
        valueAnimator.setDuration(1000);
        valueAnimator.setRepeatMode(ValueAnimator.RESTART);
        valueAnimator.setRepeatCount(5);
        valueAnimator.setInterpolator(new LinearInterpolator());
        valueAnimator.start();

        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override public void onAnimationUpdate(ValueAnimator animation) {
                int offset = (int) animation.getAnimatedValue();
                setY(offset);
            }
        });

        valueAnimator.addListener(new AnimatorListenerAdapter() {

            @Override public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);

                startScaleAnimator(1, 0);

            }
        });
        mCurrentAnimator = valueAnimator;
    }

    private void startScaleAnimator(final int start, int end) {
        final ValueAnimator mScaleAnimator = ValueAnimator.ofFloat(start, end);
        mScaleAnimator.setDuration(250);
        mScaleAnimator.setInterpolator(new LinearInterpolator());
        mScaleAnimator.start();

        mScaleAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override public void onAnimationUpdate(ValueAnimator animation) {
                float offset = (float) animation.getAnimatedValue();
                setScaleX(offset);
                setScaleY(offset);
            }
        });

        mScaleAnimator.addListener(new AnimatorListenerAdapter() {

            @Override public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                setLayerType(LAYER_TYPE_HARDWARE, null);
                setVisibility(VISIBLE);

            }

            @Override public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                setLayerType(LAYER_TYPE_NONE, null);
                if (start == 1) {
                    mScaleAnimator.cancel();
                    setVisibility(GONE);
                    isShowing = false;
                } else {
                    startAnimator();
                }
            }
        });
        mCurrentAnimator = mScaleAnimator;
    }

    private ViewTreeObserver.OnGlobalLayoutListener mOnGlobalLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {

        @Override public void onGlobalLayout() {
            if (view != null && mOnGlobalLayoutListener != null) {
                view.getRootView().getViewTreeObserver().removeOnGlobalLayoutListener(mOnGlobalLayoutListener);
            }

            if (getVisibility() == VISIBLE) return;

            setVisibility(VISIBLE);

            targetViewWidth = view.getWidth();

            targetViewHeight = view.getHeight();

            if (targetViewWidth == 0 || targetViewHeight == 0) {
                return;

            }

            //文本的原来的宽度
            textWidth = (int) mTextPaint.measureText(text, 0, text.length());

            //经过换行处理后文本的宽度
            textWidth = getTextWidth(textWidth);

            if (mStaticLayout != null) {
                mStaticLayout = null;
            }

            mStaticLayout = new StaticLayout(text, mTextPaint, textWidth, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);

            int[] location = new int[2];

            view.getLocationInWindow(location);

            targetViewX = location[0];

            targetViewY = location[1];

            invalidate();

            setScaleX(0);

            setScaleY(0);

            startScaleAnimator(0, 1);


        }
    };

}
