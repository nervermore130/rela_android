package com.thel.ui.widget;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;

import androidx.core.content.ContextCompat;

import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.ImgShareBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.moments.MomentsBeanNew;
import com.thel.bean.recommend.RecommendWebBean;
import com.thel.bean.video.VideoBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.growingio.GrowingIoConstant;
import com.thel.modules.live.surface.watch.LiveWatchActivity;
import com.thel.modules.live.surface.watch.horizontal.LiveWatchHorizontalActivity;
import com.thel.modules.main.home.moments.comment.MomentCommentActivity;
import com.thel.modules.main.home.moments.theme.ThemeDetailActivity;
import com.thel.modules.main.messages.bean.MsgBean;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.modules.preview_image.UserInfoPhotoActivity;
import com.thel.modules.video.UserVideoListActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.service.DefaultRequestService;
import com.thel.utils.AppInit;
import com.thel.utils.BusinessUtils;
import com.thel.utils.DialogUtil;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.GsonUtils;
import com.thel.utils.ImageUtils;
import com.thel.utils.L;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.thel.R.id.txt_intro;

/**
 * Created by waiarl on 2017/7/4.
 */

public class ChatRecommendMomentView extends LinearLayout {

    private static final String TAG = "ChatRecommendMomentView";

    private Context mContext;
    private LinearLayout lin_header_recommend;
    private SimpleDraweeView img_avatar_recommend;
    private TextView txt_nickname_recommend;
    private LinearLayout moment_content_parent;
    private RelativeLayout moment_content_pic_parent;
    private SimpleDraweeView pic1_parent;
    private SimpleDraweeView pic2_parent;
    private SimpleDraweeView pic3_parent;
    private SimpleDraweeView pic4_parent;
    private SimpleDraweeView pic5_parent;
    private SimpleDraweeView pic6_parent;
    private SimpleDraweeView pic_parent;
    private RelativeLayout rel_video_parent;
    private SimpleDraweeView img_cover_parent;
    private ProgressBar progressbar_vieo_parent;
    private SimpleDraweeView img_play_vide_parent;
    private TextView txt_play_times_parent;
    private LinearLayout moment_content_music_parent;
    private SimpleDraweeView moment_content_music_pic_parent;
    private RelativeLayout rel_play_parent;
    private SimpleDraweeView img_play_parent;
    private MomentsBean momentsBean;
    private LinearLayout lin_theme_recommend;
    private SimpleDraweeView img_theme_recommend;
    private TextView txt_theme_recommend;
    private TextView txt_content_recommend;
    private TextView song_name_parent;
    private TextView album_name_parent;
    private TextView artist_name_parent;
    private View line_green;
    private SimpleDraweeView img_usercard_avater;
    private TextView txt_usercard_name;
    private TextView txt_usercard_birthday;
    private TextView txt_usercard_constellation;
    private TextView txt_usercard_intro;
    private TextView tv_affection;
    private TextView txt_weight;
    private LinearLayout ll_content_recommend;
    private LinearLayout recommend_usercard;
    private TextView user_recommend_intro;
    private float photoMargin;
    private int bigSize;
    private TextView recommend_a_moment;
    private boolean isMyself;
    private RecommendWebView recommend_web_view_parent;
    private LinearLayout lin_chat_parent_layout;
    private View user_card_shape;
    private TextView txt_user_card_intro_parent;
    private TextView txt_user_card_info_parent;
    private LinearLayout ll_music_info;
    private String msgType;
    private TextView txt_music_moment_text;
    private View line_content;
    private ImageView iv_goto;
    private SimpleDraweeView img_background;
    private RecommendLiveUserView live_user_view;

    public ChatRecommendMomentView(Context context) {
        this(context, null);
    }

    public ChatRecommendMomentView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ChatRecommendMomentView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        initDefaultData();
        init();
        setListener();
    }

    private void initDefaultData() {
        photoMargin = TheLApp.getContext().getResources().getDimension(R.dimen.moment_list_photo_margin);
        /*
         * TODO: 请注意：这个地方后面的数字要和chat_listitem.xml中的相关的布局参数保持一致，如要修改,相应的地方也务必要修改
         * TODO: 其中 24dp 为layout/chat_listitem.xml:527的 img_type_right 的 layout_width 属性值
         * TODO: 其中 16dp 为layout/chat_listitem.xml:527的 img_type_right 的 layout_marginLeft 属性值
         * TODO: 其中 10dp 为layout/chat_listitem.xml:527的 img_type_right 的 layout_marginRight 属性值
         * TODO: 其中 40dp 为layout/chat_listitem.xml:543的 rel_img_thumb_right 的 layout_width 属性值
         * TODO: 其中 5dp 为layout/chat_listitem.xml:543的 rel_img_thumb_right 的 layout_marginLeft 属性值
         * TODO: 其中 17dp 为layout/chat_listitem.xml:543的 rel_img_thumb_right 的 layout_marginRight 属性值
         * TODO: 其中 20dp 为layout/chat_listitem.xml:574的 lin_msg_status_right 的 layout_width 属性值
         * */
        bigSize = AppInit.displayMetrics.widthPixels - Utils.dip2px(TheLApp.getContext(), 24 + 16 + 10 + 40 + 5 + 17 + 20);
    }

    private void init() {
        if (mContext == null) {
            mContext = TheLApp.getContext();
        }
        inflate(mContext, R.layout.chat_recommend_moment_view, this);
        lin_chat_parent_layout = findViewById(R.id.lin_chat_parent_layout);
        //头部
        lin_header_recommend = findViewById(R.id.lin_header_recommend);
        img_avatar_recommend = findViewById(R.id.img_avatar_recommend);
        txt_nickname_recommend = findViewById(R.id.txt_nickname_recommend);
        iv_goto = findViewById(R.id.iv_goto);
        //中间内容不符（图片，视频，直播）
        //图片区域
        moment_content_parent = findViewById(R.id.moment_content_parent);
        moment_content_pic_parent = findViewById(R.id.moment_content_pic_parent);
        pic1_parent = findViewById(R.id.pic1_parent);
        pic2_parent = findViewById(R.id.pic2_parent);
        pic3_parent = findViewById(R.id.pic3_parent);
        pic4_parent = findViewById(R.id.pic4_parent);
        pic5_parent = findViewById(R.id.pic5_parent);
        pic6_parent = findViewById(R.id.pic6_parent);
        //单个图片（直播，日志）
        pic_parent = findViewById(R.id.pic_parent);
        //视频
        rel_video_parent = findViewById(R.id.rel_video_parent);
        img_cover_parent = findViewById(R.id.img_cover_parent);
        progressbar_vieo_parent = findViewById(R.id.progressbar_video_parent);
        img_play_vide_parent = findViewById(R.id.img_play_video_parent);
        txt_play_times_parent = findViewById(R.id.txt_play_times_parent);
        //音乐
        moment_content_music_parent = findViewById(R.id.moment_content_music_parent);
        moment_content_music_pic_parent = findViewById(R.id.moment_content_music_pic_parent);
        song_name_parent = findViewById(R.id.song_name_parent);
        album_name_parent = findViewById(R.id.album_name_parent);
        artist_name_parent = findViewById(R.id.artist_name_parent);
        rel_play_parent = findViewById(R.id.rel_play_parent);
        img_play_parent = findViewById(R.id.img_play_parent);
        ll_music_info = findViewById(R.id.ll_music_info);
        txt_music_moment_text = findViewById(R.id.txt_music_moment_text);
        //  line_green = findViewById(R.id.line_green);
        //line_green.setVisibility(GONE);
        //话题区域
        lin_theme_recommend = findViewById(R.id.lin_theme_recommend);
        img_theme_recommend = findViewById(R.id.img_theme_recommend);
        txt_theme_recommend = findViewById(R.id.txt_theme_recommend);
        //文本
        ll_content_recommend = findViewById(R.id.ll_content_view);
        txt_content_recommend = findViewById(R.id.txt_content_recommend);
        recommend_a_moment = findViewById(R.id.recommend_a_moment);
        line_content = findViewById(R.id.line_content);

        //推荐名片
        recommend_usercard = findViewById(R.id.recommend_usercard_view);
        img_usercard_avater = findViewById(R.id.img_avatar);
        txt_usercard_name = findViewById(R.id.txt_name);
        txt_usercard_birthday = findViewById(R.id.tv_birthday);
        txt_usercard_constellation = findViewById(R.id.txt_constellation); //身高
        txt_usercard_intro = findViewById(txt_intro);  //底部黄色
        tv_affection = findViewById(R.id.tv_affection);   //情感
        txt_weight = findViewById(R.id.txt_weight);
        user_recommend_intro = findViewById(R.id.user_recommend_intro);
        user_card_shape = findViewById(R.id.user_card_shape);
        img_background = findViewById(R.id.img_background);
        //推荐网页
        recommend_web_view_parent = findViewById(R.id.recommend_web_view_parent);
        txt_user_card_intro_parent = findViewById(R.id.txt_user_card_intro_parent);
        txt_user_card_info_parent = findViewById(R.id.txt_user_card_info_parent);
        live_user_view = findViewById(R.id.live_user_view);
    }

    public ChatRecommendMomentView initView(MomentsBean momentsBean, boolean isMyself) {
        this.isMyself = isMyself;
        this.momentsBean = momentsBean;
        if (momentsBean == null) {
            setVisibility(GONE);
            return this;
        }
        setVisibility(VISIBLE);
        setDefaultState();
        setDefaultParam();
        setViews();
        return this;
    }

    /**
     * 早期苹果一些信息没有日志类型，所以消息显示又问题
     *
     * @param momentsBean
     * @param isMyself
     * @return
     */
    public ChatRecommendMomentView initView(MomentsBean momentsBean, boolean isMyself, String msgType) {
        this.isMyself = isMyself;
        this.momentsBean = momentsBean;
        L.d("ChatRecommendMomentView", " momentsBean toJson : " + GsonUtils.createJsonString(momentsBean));
        this.msgType = msgType;
        if (momentsBean == null) {
            setVisibility(GONE);
            return this;
        }
        if (!TextUtils.isEmpty(momentsBean.momentsType)) {//如果日志类型不为空
            return initView(momentsBean, isMyself);
        }
        setVisibility(VISIBLE);
        setDefaultState();
        setDefaultParam();
        setHeader();
        setMsgTypeViews(msgType);
        return this;
    }

    private void setListener() {
     /*   moment_content_music_parent.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });*/
        //点击头像跳转
        img_avatar_recommend.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (momentsBean != null && !TextUtils.isEmpty(momentsBean.userId + "")) {
                    jumpToUserPage(momentsBean.userId + "");

                }
            }
        });

        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (momentsBean != null && !TextUtils.isEmpty(momentsBean.momentsId)) {
                    jumpToUserMoment(momentsBean.momentsId);

                }
            }
        });
        rel_video_parent.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (momentsBean != null) {
                    jumpToVideo();
                }
            }
        });
        //未实现就先保持父控件的点击事件
        /*rel_play_parent.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

               *//* if (!TextUtils.isEmpty(XiamiSDKUtil.newInstance().songMomentId) && XiamiSDKUtil.newInstance().songMomentId.equals(momentsBean.momentsId) && XiamiSDKUtil.newInstance().isPlaying()) {
                    img_play_parent.setImageResource(R.drawable.btn_feed_pause_big);
                } else {
                    img_play_parent.setImageResource(R.drawable.btn_feed_play_big);
                }*//*
            }
        });*/
        pic_parent.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                L.d(TAG, " msgType : " + msgType);
                if (MsgBean.MSG_TYPE_MOMENT_LIVE.equals(msgType) || MomentsBean.MOMENT_TYPE_LIVE.equals(momentsBean.momentsType)) {//如果是直播日志
                    gotoLiveShowActivity(momentsBean.momentsId);
                } else {
                    openPhoto(v, momentsBean.userName, momentsBean.imageUrl, 0, momentsBean);
                }
            }
        });
        pic1_parent.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                openPhoto(v, momentsBean.userName, momentsBean.imageUrl, 0, momentsBean);
            }
        });
        pic2_parent.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                openPhoto(v, momentsBean.userName, momentsBean.imageUrl, 1, momentsBean);
            }
        });
        pic3_parent.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                openPhoto(v, momentsBean.userName, momentsBean.imageUrl, 2, momentsBean);
            }
        });
        pic4_parent.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                openPhoto(v, momentsBean.userName, momentsBean.imageUrl, 3, momentsBean);
            }
        });
        pic5_parent.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                openPhoto(v, momentsBean.userName, momentsBean.imageUrl, 4, momentsBean);
            }
        });
        pic6_parent.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                openPhoto(v, momentsBean.userName, momentsBean.imageUrl, 5, momentsBean);
            }
        });
    }

    /**
     * 跳转到视频
     */
    private void jumpToVideo() {

        VideoBean videoBean = Utils.getVideoFromMoment(momentsBean);
        Intent intent;
        intent = new Intent(getContext(), UserVideoListActivity.class);
        intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_BEAN, momentsBean);
        intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, momentsBean.userId + "");
        intent.putExtra(TheLConstants.BUNDLE_KEY_VIDEO_BEAN, videoBean);
        intent.putExtra(TheLConstants.BUNDLE_KEY_ENTRY, TheLConstants.EntryConstants.ENTRY_CHAT);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        TheLApp.getContext().startActivity(intent);
    }

    /**
     * 跳转到个日志详情
     *
     * @param momentsId
     */
    private void jumpToUserMoment(String momentsId) {
        if (MsgBean.MSG_TYPE_MOMENT_LIVE.equals(msgType) || MomentsBean.MOMENT_TYPE_LIVE.equals(momentsBean.momentsType)) {//如果是直播日志
            gotoLiveShowActivity(momentsBean.momentsId);
        } else if (MsgBean.MSG_TYPE_MOMENT_THEME.equals(msgType)) {//如果是话题日志，跳转到话题详情
//            Intent intent = new Intent();

            if (!TextUtils.isEmpty(momentsId)) {
//                intent.setClass(mContext, ThemeDetailActivity.class);
//                intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, momentsId);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                TheLApp.getContext().startActivity(intent);
                FlutterRouterConfig.Companion.gotoThemeDetails(momentsId);
            }
        } else {


            Intent intent = new Intent();

            if (!TextUtils.isEmpty(momentsId)) {
//                intent.setClass(mContext, MomentCommentActivity.class);
//                intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, momentsId);
//                //                intent.putExtra("fromPage", TheLConstants.BUNDLE_KEY_COMMENT_ON_TOP);
//                //                intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, momentsBean.userId);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                TheLApp.getContext().startActivity(intent);
                FlutterRouterConfig.Companion.gotoMomentDetails(momentsId);
            }
        }
    }

    private void gotoLiveShowActivity(String momentsId) {
        if (momentsBean == null || TextUtils.isEmpty(momentsBean.liveId)) {
            return;
        }

        L.d("ChatRecommendMomentView", " json : " + GsonUtils.createJsonString(momentsBean));

        if (BusinessUtils.canIntoLiveRoom(TheLApp.getContext(), momentsBean.userId + "")) {
            Intent intent;
            if (momentsBean.isLandscape == 1) {
                intent = new Intent(getContext(), LiveWatchHorizontalActivity.class);
            } else {
                intent = new Intent(getContext(), LiveWatchActivity.class);
            }

            L.d("ChatRecommendMomentView", " momentsBean.liveId : " + momentsBean.liveId);

            intent.putExtra(TheLConstants.BUNDLE_KEY_ID, momentsBean.liveId);
            intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, momentsBean.userId + "");
            intent.putExtra(LiveWatchActivity.FROM_PAGE, LiveWatchActivity.FROM_PAGE_MOMENT);
            getContext().startActivity(intent);
            GrowingIOUtil.postWatchLiveEntry(GrowingIoConstant.G_LiveEntry_Other);

        }
    }

    private void jumpToUserPage(String userId) {

        if (MomentsBean.IS_SECRET == momentsBean.secret) {
            DialogUtil.showToastShort(mContext, getString(R.string.user_not_available));
            return;
        }
//        Intent intent = new Intent(TheLApp.getContext(), UserInfoActivity.class);
//        intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, userId);
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        TheLApp.getContext().startActivity(intent);
        FlutterRouterConfig.Companion.gotoUserInfo(userId);
    }


    private void openPhoto(View v, String relaId, String urls, int position, MomentsBean momentBean) {
        ViewUtils.preventViewMultipleClick(v, 1000);
        String[] photoUrls = urls.split(",");
        Intent intent = new Intent(TheLApp.getContext(), UserInfoPhotoActivity.class);
        ImgShareBean bean = Utils.getImageShareBean(momentBean);
        Bundle bundle = new Bundle();
        bundle.putSerializable(TheLConstants.BUNDLER_KEY_IMAGESHAREBEAN, bean);
        intent.putExtras(bundle);
        ArrayList<String> photos = new ArrayList<>();
        for (int i = 0; i < photoUrls.length; i++) {
            photos.add(photoUrls[i]);
        }
        intent.putStringArrayListExtra(TheLConstants.BUNDLE_KEY_PHOTOS, photos);
        intent.putExtra(TheLConstants.BUNDLE_KEY_RELA_ID, relaId);
        intent.putExtra(TheLConstants.IS_WATERMARK, true);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("position", position);
        TheLApp.getContext().startActivity(intent);
    }

    private void setDefaultState() {
        moment_content_parent.setVisibility(GONE);
        moment_content_pic_parent.setVisibility(GONE);
        //图片
        pic_parent.setVisibility(GONE);
        live_user_view.setVisibility(GONE);
        lin_theme_recommend.setVisibility(GONE);
        setPicDefaultState();
        //视频
        rel_video_parent.setVisibility(GONE);
        img_cover_parent.setVisibility(GONE);
        progressbar_vieo_parent.setVisibility(GONE);
        img_play_vide_parent.setVisibility(GONE);
        txt_play_times_parent.setVisibility(GONE);
        //音乐
        moment_content_music_parent.setVisibility(GONE);

        ll_content_recommend.setVisibility(View.GONE);
        recommend_web_view_parent.setVisibility(View.GONE);
        recommend_usercard.setVisibility(View.GONE);

        recommend_a_moment.setVisibility(GONE);

        line_content.setVisibility(View.VISIBLE);

        iv_goto.setVisibility(View.VISIBLE);
        txt_music_moment_text.setVisibility(View.GONE);
        img_theme_recommend.setVisibility(View.VISIBLE);
    }

    private void setPicDefaultState() {
        pic_parent.setVisibility(GONE);
        pic1_parent.setVisibility(GONE);
        pic2_parent.setVisibility(GONE);
        pic3_parent.setVisibility(GONE);
        pic4_parent.setVisibility(GONE);
        pic5_parent.setVisibility(GONE);
        pic6_parent.setVisibility(GONE);

        pic_parent.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadii(0, 0, 0, 0));
        pic1_parent.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadii(0, 0, 0, 0));
        pic2_parent.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadii(0, 0, 0, 0));
        pic3_parent.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadii(0, 0, 0, 0));
        pic4_parent.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadii(0, 0, 0, 0));
        pic5_parent.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadii(0, 0, 0, 0));
        pic6_parent.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadii(0, 0, 0, 0));
        img_cover_parent.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadii(0, 0, 0, 0));

    }


    private void setDefaultParam() {
        final ViewGroup.LayoutParams param_parent = lin_chat_parent_layout.getLayoutParams();
        param_parent.width = bigSize;
        lin_chat_parent_layout.setLayoutParams(param_parent);

        final RelativeLayout.LayoutParams param_pic = (RelativeLayout.LayoutParams) pic_parent.getLayoutParams();
        param_pic.width = bigSize;
        param_pic.height = bigSize;

        LayoutParams txt_intro_params = (LayoutParams) recommend_a_moment.getLayoutParams();

        ViewGroup.LayoutParams layoutParams = moment_content_music_pic_parent.getLayoutParams();
        layoutParams.width = bigSize;
        layoutParams.height = bigSize;

        ViewGroup.LayoutParams video_params = img_cover_parent.getLayoutParams();
        video_params.width = bigSize;
        video_params.height = bigSize;

        //先移除反方向的align parent
        //先移除反isMyself方向的align parent
        if (isMyself) {
            txt_intro_params.gravity = Gravity.LEFT;
        } else {
            txt_intro_params.gravity = Gravity.RIGHT;
        }
        recommend_a_moment.setLayoutParams(txt_intro_params);
        ViewGroup.LayoutParams moment_content_pic_parent_param = moment_content_pic_parent.getLayoutParams();
        moment_content_pic_parent_param.width = bigSize;

    }

    private void setViews() {
        setHeader();
        setContentView();
    }

    /**
     * 通过消息类型来展示
     *
     * @param msgType
     */
    private void setMsgTypeViews(String msgType) {
        setRecommendText(msgType);
        if (MsgBean.MSG_TYPE_MOMENT_TEXT.equals(msgType) || MsgBean.MSG_TYPE_MOMENT_RECOMMEND.equals(msgType)) {//纯文本（推荐日志也是纯文本日志）
            setMomentContent();
        } else if (MsgBean.MSG_TYPE_MOMENT_IMAGE.equals(msgType)) {//图片日志(包括图片文本消息)
            setMomentContent();
            setPicsView();
        } else if (MsgBean.MSG_TYPE_MOMENT_LIVE.equals(msgType)) {//视频直播日志
            // setMomentContent();
            setLiveView();
        } else if (MsgBean.MSG_TYPE_MOMENT_VOICE.equals(msgType)) {//声音直播日志
            // setMomentContent();
            setLiveView();
        } else if (MsgBean.MSG_TYPE_MOMENT_THEME.equals(msgType)) {//话题日志
            setThemeView();
        } else if (MsgBean.MSG_TYPE_MOMENT_THEMEREPLY.equals(msgType)) {//话题回复日志(参考纯文本日志或者图片日志或者图片文本日志)
            setThemeReplyView();
        } else if (MsgBean.MSG_TYPE_MOMENT_VIDEO.equals(msgType)) {//视频日志
            setVideoView();
            setNotLineContent();
        } else if (MsgBean.MSG_TYPE_MOMENT_USERCARD.equals(msgType)) {//推荐用户名片
            setUserCardView();
        } else if (MsgBean.MSG_TYPE_MOMENT_MUSIC.equals(msgType)) {//音乐日志
            setMuscView();
        } else if (MsgBean.MSG_TYPE_MOMENT_WEB.equals(msgType)) {//推荐网页日志
            setWebView();
        } else {
            setMomentContent();

        }
    }

    private void setContentView() {
        if (MomentsBean.MOMENT_TYPE_TEXT.equals(momentsBean.momentsType) || MomentsBean.MOMENT_TYPE_RECOMMEND.equals(momentsBean.momentsType)) {//纯文本（推荐日志也是纯文本日志）
            setMomentContent();
            setRecommendText(MsgBean.MSG_TYPE_MOMENT_TEXT);
        } else if (MomentsBean.MOMENT_TYPE_IMAGE.equals(momentsBean.momentsType)) {//图片日志
            setPicsView();
            setRecommendText(MsgBean.MSG_TYPE_MOMENT_IMAGE);
        } else if (MomentsBean.MOMENT_TYPE_TEXT_IMAGE.equals(momentsBean.momentsType)) {//带文本的图片日志
            setNotLineContent();
            setPicsView();
            setRecommendText(MsgBean.MSG_TYPE_MOMENT_IMAGE);
        } else if (MomentsBean.MOMENT_TYPE_LIVE.equals(momentsBean.momentsType) || MomentsBean.MOMENT_TYPE_VOICE_LIVE.equals(momentsBean.momentsType)) {//直播日志
            setLiveView();
            setRecommendText(MsgBean.MSG_TYPE_MOMENT_LIVE);
        } else if (MomentsBean.MOMENT_TYPE_THEME.equals(momentsBean.momentsType)) {//话题日志
            setThemeView();
            setRecommendText(MsgBean.MSG_TYPE_MOMENT_THEME);
        } else if (MomentsBean.MOMENT_TYPE_THEME_REPLY.equals(momentsBean.momentsType)) {//话题回复日志(参考纯文本日志或者图片日志或者图片文本日志)
            //  setPicsView();
            // setMomentContent();
            setThemeReplyView();
            setRecommendText(MsgBean.MSG_TYPE_MOMENT_THEMEREPLY);
        } else if (MomentsBean.MOMENT_TYPE_VIDEO.equals(momentsBean.momentsType)) {//视频日志
            setVideoView();
            setNotLineContent();
            setRecommendText(MsgBean.MSG_TYPE_MOMENT_VIDEO);
        } else if (MomentsBean.MOMENT_TYPE_USER_CARD.equals(momentsBean.momentsType)) {//推荐用户名片
            setUserCardView();
            setRecommendText(MsgBean.MSG_TYPE_MOMENT_USERCARD);
        } else if (MomentsBean.MOMENT_TYPE_VOICE.equals(momentsBean.momentsType)) {//纯音乐日志
            setMuscView();
            setRecommendText(MsgBean.MSG_TYPE_MOMENT_MUSIC);
        } else if (MomentsBean.MOMENT_TYPE_TEXT_VOICE.equals(momentsBean.momentsType)) {//带文字的音乐日志
            setMuscView();
            setRecommendText(MsgBean.MSG_TYPE_MOMENT_MUSIC);
        } else if (MomentsBean.MOMENT_TYPE_WEB.equals(momentsBean.momentsType)) {//网页日志
            setWebView();
            setRecommendText(MsgBean.MSG_TYPE_MOMENT_WEB);
        } else if (MomentsBean.MOMENT_TYPE_LIVE_USER.equals(momentsBean.momentsType)) {
            setLiveView();
            setRecommendText(MsgBean.MSG_TYPE_MOMENT_LIVE);
        } else {
            setMomentContent();
            setRecommendText(MsgBean.MSG_TYPE_MOMENT_TEXT);
        }
    }

    private void setThemeReplyView() {
        if (momentsBean.imageUrl != null) {
            final String[] picUrls = momentsBean.imageUrl.split(",");
            final int count = picUrls.length;
            if (count > 0 && !TextUtils.isEmpty(picUrls[0])) {//如果带图片，用话题界面
                lin_theme_recommend.setVisibility(VISIBLE);
                setImageUrl(img_theme_recommend, picUrls[0], TheLConstants.MOMENT_PIC_BIG_SIZE, TheLConstants.MOMENT_PIC_BIG_SIZE);
                txt_theme_recommend.setText(momentsBean.momentsText + "");//话题回复带不带文本无所谓
            } else {//不带图片用普通的文本类型
                setMomentContent();
            }
        }
    }


    private void setMomentContent() {
        if (!TextUtils.isEmpty(momentsBean.momentsText)) {
            ll_content_recommend.setVisibility(View.VISIBLE);
            txt_content_recommend.setVisibility(View.VISIBLE);
            Utils.setTextWithUrl(txt_content_recommend, momentsBean.momentsText);
        }
    }

    private void setNotLineContent() {
        if (!TextUtils.isEmpty(momentsBean.momentsText)) {
            ll_content_recommend.setVisibility(View.VISIBLE);
            line_content.setVisibility(GONE);
            txt_content_recommend.setVisibility(View.VISIBLE);
            Utils.setTextWithUrl(txt_content_recommend, momentsBean.momentsText);
        }
    }


    /**
     * 设置网页日志view
     */
    private void setWebView() {
        final RecommendWebBean recommendWebBean = new RecommendWebBean();
        recommendWebBean.imageUrl = momentsBean.imageUrl;
        recommendWebBean.momentsText = momentsBean.momentsText;
        recommend_web_view_parent.initView(recommendWebBean);
    }

    private void setThemeView() {
        lin_theme_recommend.setVisibility(VISIBLE);
        if (TextUtils.isEmpty(momentsBean.imageUrl)) {// 话题日志有可能不带背景图，所以要显示默认背景
            setImageUrl(img_theme_recommend, TheLConstants.RES_PIC_URL + R.mipmap.bg_topic_default);//没背景
            img_theme_recommend.setBackgroundColor(ContextCompat.getColor(TheLApp.getContext(), R.color.black_transprent_30));
        } else {
            setImageUrl(img_theme_recommend, momentsBean.imageUrl, TheLConstants.MOMENT_PIC_BIG_SIZE, TheLConstants.MOMENT_PIC_BIG_SIZE);
            img_theme_recommend.setBackgroundColor(ContextCompat.getColor(TheLApp.getContext(), R.color.light_gray_mask));
        }
        if (!TextUtils.isEmpty(momentsBean.momentsText)) {//话题
            txt_theme_recommend.setText(momentsBean.momentsText);
        } else {
            txt_theme_recommend.setText("");
        }
    }

    private void setMuscView() {

        if (momentsBean.songId != 0) {
            moment_content_parent.setVisibility(VISIBLE);
            moment_content_music_parent.setVisibility(VISIBLE);
            if (momentsBean.albumLogo444 != null) {
                setImageUrl(moment_content_music_pic_parent, momentsBean.albumLogo444);
            }
            ll_music_info.setVisibility(GONE);
            txt_music_moment_text.setVisibility(GONE);
            if (TextUtils.isEmpty(momentsBean.momentsText)) {
                ll_music_info.setVisibility(VISIBLE);
                song_name_parent.setText(momentsBean.songName);
                album_name_parent.setText(TheLApp.getContext().getString(R.string.moments_album) + "《" + momentsBean.albumName + "》");
                artist_name_parent.setText(TheLApp.getContext().getString(R.string.moments_artist) + " " + momentsBean.artistName);
            } else {
                txt_music_moment_text.setVisibility(VISIBLE);
                txt_music_moment_text.setText(momentsBean.momentsText);
            }
        }
    }


    private void setUserCardView() {
        recommend_usercard.setVisibility(VISIBLE);
        moment_content_music_parent.setVisibility(GONE);

        img_usercard_avater.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(momentsBean.cardAvatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE))).build()).setAutoPlayAnimations(true).build());
        txt_usercard_name.setText(momentsBean.cardNickName);
        setImageUrl(img_background, momentsBean.imageUrl, TheLConstants.MOMENT_PIC_BIG_SIZE, TheLConstants.MOMENT_PIC_BIG_SIZE);
        // teuser.setText(getResources().getString(R.string.recommend_a, momentsBean.cardIntro));
        //        txt_usercard_intro.setVisibility(GONE);
        //        txt_weight.setVisibility(VISIBLE);
        //        txt_usercard_birthday.setText(momentsBean.cardAge + TheLApp.getContext().getResources().getString(R.string.updatauserinfo_activity_age_unit) + ",");
        //        txt_weight.setText(momentsBean.cardWeight + " cm,");
        //        txt_usercard_constellation.setText(momentsBean.cardHeight + " kg,");
        //        user_recommend_intro.setVisibility(VISIBLE);
        //        user_recommend_intro.setText(momentsBean.cardIntro.replaceAll("\n", " "));

        //        if (!TextUtils.isEmpty(momentsBean.cardAffection)) {
        //
        //            tv_affection.setText(momentsBean.cardAffection);
        //        } else {
        //            tv_affection.setText(TheLApp.getContext().getString(R.string.info_do_not_show));
        //        }
        txt_user_card_info_parent.setText(momentsBean.buildUserCardInfoStr());
        txt_user_card_intro_parent.setText(momentsBean.cardIntro.replaceAll("\n", " "));
        setNotLineContent();

    }

    private void setVideoView() {
        if (!TextUtils.isEmpty(momentsBean.videoUrl)) {
            moment_content_music_parent.setVisibility(GONE);
            moment_content_parent.setVisibility(VISIBLE);
            rel_video_parent.setVisibility(VISIBLE);
            img_play_vide_parent.setVisibility(VISIBLE);
            img_cover_parent.setVisibility(VISIBLE);
            setImageUrl(img_cover_parent, momentsBean.imageUrl, TheLConstants.MOMENT_PIC_BIG_SIZE, TheLConstants.MOMENT_PIC_BIG_SIZE);
            String text;
            if (momentsBean.playTime > 0) {
                text = Utils.formatTime(momentsBean.playTime) + " / " + TheLApp.getContext().getString(R.string.play_count, momentsBean.playCount);
            } else {
                text = TheLApp.getContext().getString(R.string.play_count, momentsBean.playCount);
            }
            txt_play_times_parent.setText(text);
           /* if (isMyself && TextUtils.isEmpty(momentsBean.momentsText)) {
                img_cover_parent.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadii(0, 0, 0, 15));
*/
            if (TextUtils.isEmpty(momentsBean.momentsText)) {
                img_cover_parent.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadii(0, 0, Utils.dip2px(TheLApp.getContext(), 5f), Utils.dip2px(TheLApp.getContext(), 5f)));
            }
        }
    }

    private void setLiveView() {
        setPicsView();
        //        setNotLineContent();
        if (!TextUtils.isEmpty(momentsBean.momentsText)) {
            ll_content_recommend.setVisibility(View.VISIBLE);
            line_content.setVisibility(GONE);
            txt_content_recommend.setVisibility(View.VISIBLE);
            Utils.setTextWithUrl(txt_content_recommend, momentsBean.nickname + getString(R.string.is_on_live) + ":" + momentsBean.momentsText);
        }
        moment_content_parent.setVisibility(VISIBLE);

        live_user_view.setVisibility(View.VISIBLE);

        if (MomentsBean.MOMENT_TYPE_LIVE.equals(momentsBean.momentsType) || MomentsBean.MOMENT_TYPE_VOICE_LIVE.equals(momentsBean.momentsType)) {

            live_user_view.initView(momentsBean);
        }
    }

    private void setPicsView() {
        try {
            if (momentsBean.imageUrl == null) {//如果为空，则返回，不然为空也会获取到的count=1
                return;
            }
            final String[] picUrls = momentsBean.imageUrl.split(",");
            final int count = picUrls.length;
            if (count > 0) {
                moment_content_parent.setVisibility(VISIBLE);
                moment_content_pic_parent.setVisibility(VISIBLE);
                if (picUrls.length == 1 && !TextUtils.isEmpty(picUrls[0])) {
                    pic_parent.setVisibility(VISIBLE);
                    setImageUrl(pic_parent, picUrls[0], TheLConstants.MOMENT_PIC_BIG_SIZE, TheLConstants.MOMENT_PIC_BIG_SIZE);
                } else {
                    pic1_parent.setVisibility(VISIBLE);
                    setImageUrl(pic1_parent, picUrls[0], TheLConstants.MOMENT_PIC_BIG_SIZE, TheLConstants.MOMENT_PIC_BIG_SIZE);
                    pic2_parent.setVisibility(VISIBLE);
                    setImageUrl(pic2_parent, picUrls[1], TheLConstants.MOMENT_PIC_SMALL_SIZE, TheLConstants.MOMENT_PIC_SMALL_SIZE);
                    if (picUrls.length > 2) {
                        pic3_parent.setVisibility(VISIBLE);
                        setImageUrl(pic3_parent, picUrls[2], TheLConstants.MOMENT_PIC_SMALL_SIZE, TheLConstants.MOMENT_PIC_SMALL_SIZE);
                        if (picUrls.length > 3) {
                            pic4_parent.setVisibility(VISIBLE);
                            setImageUrl(pic4_parent, picUrls[3], TheLConstants.MOMENT_PIC_SMALL_SIZE, TheLConstants.MOMENT_PIC_SMALL_SIZE);
                            if (picUrls.length > 4) {
                                pic5_parent.setVisibility(VISIBLE);
                                setImageUrl(pic5_parent, picUrls[4], TheLConstants.MOMENT_PIC_SMALL_SIZE, TheLConstants.MOMENT_PIC_SMALL_SIZE);
                                if (picUrls.length > 5) {
                                    pic6_parent.setVisibility(VISIBLE);
                                    setImageUrl(pic6_parent, picUrls[5], TheLConstants.MOMENT_PIC_SMALL_SIZE, TheLConstants.MOMENT_PIC_SMALL_SIZE);
                                }
                            }
                        }
                    }
                    setPicsLayoutParams(count);
                }
            }
            setPicCircular(count);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setPicCircular(int count) {
        if (!TextUtils.isEmpty(momentsBean.momentsText)) {
            return;
        }
        switch (count) {
            case 1:
                pic_parent.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadii(0, 0, Utils.dip2px(TheLApp.getContext(), 5), Utils.dip2px(TheLApp.getContext(), 5)));
                break;
            case 2:
                pic1_parent.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadii(0, 0, 0, Utils.dip2px(TheLApp.getContext(), 5)));
                pic2_parent.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadii(0, 0, Utils.dip2px(TheLApp.getContext(), 5), 0));

                break;
            case 3:
                pic2_parent.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadii(0, 0, 0, Utils.dip2px(TheLApp.getContext(), 5)));
                pic3_parent.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadii(0, 0, Utils.dip2px(TheLApp.getContext(), 5), 0));

                break;

            case 4:
                pic3_parent.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadii(0, 0, 0, Utils.dip2px(TheLApp.getContext(), 5)));
                pic4_parent.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadii(0, 0, Utils.dip2px(TheLApp.getContext(), 5), 0));

                break;
            case 5:
                pic3_parent.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadii(0, 0, 0, Utils.dip2px(TheLApp.getContext(), 5)));
                pic5_parent.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadii(0, 0, Utils.dip2px(TheLApp.getContext(), 5), 0));

                break;
            case 6:
                pic4_parent.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadii(0, 0, 0, Utils.dip2px(TheLApp.getContext(), 5)));
                pic6_parent.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadii(0, 0, Utils.dip2px(TheLApp.getContext(), 5), 0));

                break;
        }
        pic_parent.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadii(0, 0, Utils.dip2px(TheLApp.getContext(), 5), Utils.dip2px(TheLApp.getContext(), 5)));
    }

    private void setHeader() {
        //头像，昵称
        String nickName = "";
        String postfix = "";
        if (MomentsBean.IS_SECRET == momentsBean.secret) {//匿名
            setImageUrl(img_avatar_recommend, TheLConstants.RES_PIC_URL + R.mipmap.icon_user_anonymous);
            nickName = TheLApp.getContext().getString(R.string.moments_msgs_notification_anonymous);
            iv_goto.setVisibility(GONE);
        } else {
            nickName = momentsBean.nickname;
            setImageUrl(img_avatar_recommend, momentsBean.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE);
        }
        if (MomentsBean.MOMENT_TYPE_RECOMMEND.equals(momentsBean.momentsType) || MomentsBean.MOMENT_TYPE_TEXT.equals(momentsBean.momentsType) || MomentsBean.MOMENT_TYPE_THEME_REPLY.equals(momentsBean.momentsType) || MomentsBean.MOMENT_TYPE_IMAGE.equals(momentsBean.momentsType) || MomentsBean.MOMENT_TYPE_TEXT_IMAGE.equals(momentsBean.momentsType)) {
            postfix = TheLApp.getContext().getString(R.string.de_moment);
        } else if (MomentsBean.MOMENT_TYPE_THEME.equals(momentsBean.momentsType)) {//话题
            postfix = TheLApp.getContext().getString(R.string.de_theme);
        } else if (MomentsBean.MOMENT_TYPE_VOICE.equals(momentsBean.momentsType) || MomentsBean.MOMENT_TYPE_TEXT_VOICE.equals(momentsBean.momentsType)) {//音乐
            postfix = TheLApp.getContext().getString(R.string.de_music);
        } else if (MomentsBean.MOMENT_TYPE_VIDEO.equals(momentsBean.momentsType)) {//视频
            postfix = TheLApp.getContext().getString(R.string.de_video);
        } else if (MomentsBean.MOMENT_TYPE_LIVE.equals(momentsBean.momentsType) || MomentsBean.MOMENT_TYPE_VOICE_LIVE.equals(momentsBean.momentsType)) {//直播
            postfix = TheLApp.getContext().getString(R.string.de_live);
        } else {//其他，默认文本日志
            postfix = TheLApp.getContext().getString(R.string.de_moment);
        }
        txt_nickname_recommend.setText(nickName + postfix);
    }


    private void setImageUrl(SimpleDraweeView view, String url) {
        view.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(url))).build()).setAutoPlayAnimations(true).build());
    }

    private void setImageUrl(SimpleDraweeView view, String url, float width, float height) {
        view.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(url, width, height))).build()).setAutoPlayAnimations(true).build());
    }

    /**
     * 当图片数大于1张时，要分2~6张不同的数量展示不同的布局
     *
     * @param count
     */
    private void setPicsLayoutParams(int count) {

        int smallSize = (int) (bigSize - photoMargin) / 2;
        int tinySize = (int) ((bigSize - photoMargin * 2) / 3);
        if (count == 2 || count == 5) {
            final RelativeLayout.LayoutParams smallSizeParamsLeft = new RelativeLayout.LayoutParams(smallSize, smallSize);
            final RelativeLayout.LayoutParams smallSizeParamsRight = new RelativeLayout.LayoutParams(smallSize, smallSize);
            smallSizeParamsRight.leftMargin = (int) photoMargin;
            smallSizeParamsRight.addRule(RelativeLayout.RIGHT_OF, R.id.pic1_parent);
            pic1_parent.setLayoutParams(smallSizeParamsLeft);
            pic2_parent.setLayoutParams(smallSizeParamsRight);
            if (count == 5) {
                final RelativeLayout.LayoutParams params3 = new RelativeLayout.LayoutParams(tinySize, tinySize);
                params3.addRule(RelativeLayout.BELOW, R.id.pic1_parent);
                params3.topMargin = (int) photoMargin;
                pic3_parent.setLayoutParams(params3);
                final RelativeLayout.LayoutParams params4 = new RelativeLayout.LayoutParams(tinySize, tinySize);
                params4.addRule(RelativeLayout.BELOW, R.id.pic1_parent);
                params4.addRule(RelativeLayout.RIGHT_OF, R.id.pic3_parent);
                params4.topMargin = (int) photoMargin;
                params4.leftMargin = (int) photoMargin;
                pic4_parent.setLayoutParams(params4);
                final RelativeLayout.LayoutParams params5 = new RelativeLayout.LayoutParams(tinySize, tinySize);
                params5.addRule(RelativeLayout.BELOW, R.id.pic1_parent);
                params5.addRule(RelativeLayout.RIGHT_OF, R.id.pic4_parent);
                params5.topMargin = (int) photoMargin;
                params5.leftMargin = (int) photoMargin;
                pic5_parent.setLayoutParams(params5);
            }
        } else if (count == 3) {
            final RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, smallSize);
            pic1_parent.setLayoutParams(params1);
            final RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(smallSize, smallSize);
            params2.topMargin = (int) photoMargin;
            params2.addRule(RelativeLayout.BELOW, R.id.pic1_parent);
            final RelativeLayout.LayoutParams params3 = new RelativeLayout.LayoutParams(smallSize, smallSize);
            params3.leftMargin = (int) photoMargin;
            params3.topMargin = (int) photoMargin;
            params3.addRule(RelativeLayout.BELOW, R.id.pic1_parent);
            params3.addRule(RelativeLayout.RIGHT_OF, R.id.pic2_parent);
            pic2_parent.setLayoutParams(params2);
            pic3_parent.setLayoutParams(params3);
        } else if (count == 4) {
            final RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(smallSize, smallSize);
            pic1_parent.setLayoutParams(params1);
            final RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(smallSize, smallSize);
            params2.leftMargin = (int) photoMargin;
            params2.addRule(RelativeLayout.RIGHT_OF, R.id.pic1_parent);
            pic2_parent.setLayoutParams(params2);
            final RelativeLayout.LayoutParams params3 = new RelativeLayout.LayoutParams(smallSize, smallSize);
            params3.topMargin = (int) photoMargin;
            params3.addRule(RelativeLayout.BELOW, R.id.pic1_parent);
            pic3_parent.setLayoutParams(params3);
            final RelativeLayout.LayoutParams params4 = new RelativeLayout.LayoutParams(smallSize, smallSize);
            params4.topMargin = (int) photoMargin;
            params4.leftMargin = (int) photoMargin;
            params4.addRule(RelativeLayout.BELOW, R.id.pic2_parent);
            params4.addRule(RelativeLayout.RIGHT_OF, R.id.pic3_parent);
            pic4_parent.setLayoutParams(params4);
        } else if (count == 6) {
            int pic1Size = (int) (tinySize * 2 + photoMargin);
            final RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(pic1Size, pic1Size);
            pic1_parent.setLayoutParams(params1);
            final RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(tinySize, tinySize);
            params2.addRule(RelativeLayout.RIGHT_OF, R.id.pic1_parent);
            params2.leftMargin = (int) photoMargin;
            pic2_parent.setLayoutParams(params2);
            final RelativeLayout.LayoutParams params3 = new RelativeLayout.LayoutParams(tinySize, tinySize);
            params3.addRule(RelativeLayout.RIGHT_OF, R.id.pic1_parent);
            params3.addRule(RelativeLayout.BELOW, R.id.pic2_parent);
            params3.leftMargin = (int) photoMargin;
            params3.topMargin = (int) photoMargin;
            pic3_parent.setLayoutParams(params3);
            final RelativeLayout.LayoutParams params4 = new RelativeLayout.LayoutParams(tinySize, tinySize);
            params4.addRule(RelativeLayout.BELOW, R.id.pic1_parent);
            params4.topMargin = (int) photoMargin;
            pic4_parent.setLayoutParams(params4);
            final RelativeLayout.LayoutParams params5 = new RelativeLayout.LayoutParams(tinySize, tinySize);
            params5.addRule(RelativeLayout.RIGHT_OF, R.id.pic4_parent);
            params5.addRule(RelativeLayout.BELOW, R.id.pic1_parent);
            params5.leftMargin = (int) photoMargin;
            params5.topMargin = (int) photoMargin;
            pic5_parent.setLayoutParams(params5);
            final RelativeLayout.LayoutParams params6 = new RelativeLayout.LayoutParams(tinySize, tinySize);
            params6.addRule(RelativeLayout.RIGHT_OF, R.id.pic5_parent);
            params6.addRule(RelativeLayout.BELOW, R.id.pic1_parent);
            params6.leftMargin = (int) photoMargin;
            params6.topMargin = (int) photoMargin;
            pic6_parent.setLayoutParams(params6);
        }
    }

    public void setRecommendText(String msgType) {
        final MyTextStye textStye = myTextStyeMap.get(msgType);
        if (textStye != null) {
            recommend_a_moment.setVisibility(View.VISIBLE);
            recommend_a_moment.setText(textStye.text + "");
            // recommend_a_moment.setBackgroundColor(textStye.color);
            recommend_a_moment.getBackground().setColorFilter(textStye.color, PorterDuff.Mode.SRC);
        }
    }

    public static class MyTextStye {
        public int color;
        public String text;

        public MyTextStye() {
        }

        public MyTextStye(int color, String text) {
            this.color = color;
            this.text = text;
        }

        @Override
        public String toString() {
            return "MyTextStye{" + "color=" + color + ", text='" + text + '\'' + '}';
        }
    }

    public static Map<String, MyTextStye> myTextStyeMap = new HashMap<String, MyTextStye>() {
        {
            put(MsgBean.MSG_TYPE_MOMENT_TEXT, new MyTextStye(getColor(R.color.recommend_a_moment_color), getString(R.string.recommend_a_moment)));
            put(MsgBean.MSG_TYPE_MOMENT_IMAGE, new MyTextStye(getColor(R.color.recommend_a_moment_color), getString(R.string.recommend_a_moment)));
            put(MsgBean.MSG_TYPE_MOMENT_VIDEO, new MyTextStye(getColor(R.color.recommend_a_moment_color), getString(R.string.recommend_a_moment)));
            put(MsgBean.MSG_TYPE_MOMENT_RECOMMEND, new MyTextStye(getColor(R.color.recommend_a_moment_color), getString(R.string.recommend_a_moment)));
            put(MsgBean.MSG_TYPE_MOMENT_USERCARD, new MyTextStye(getColor(R.color.recommend_a_moment_color), getString(R.string.recommend_a_moment)));
            put(MsgBean.MSG_TYPE_MOMENT_THEMEREPLY, new MyTextStye(getColor(R.color.recommend_a_moment_color), getString(R.string.recommend_a_moment)));
            put(MsgBean.MSG_TYPE_MOMENT_THEME, new MyTextStye(getColor(R.color.recommend_a_theme_color), getString(R.string.shared_a_topic)));
            put(MsgBean.MSG_TYPE_WEB, new MyTextStye(getColor(R.color.recommend_a_web_color), getString(R.string.shared_a_website)));
            put(MsgBean.MSG_TYPE_MOMENT_WEB, new MyTextStye(getColor(R.color.recommend_a_web_color), getString(R.string.shared_a_website)));
            put(MsgBean.MSG_TYPE_MOMENT_MUSIC, new MyTextStye(getColor(R.color.recommend_a_song_color), getString(R.string.shared_a_song)));
            put(MsgBean.MSG_TYPE_MOMENT_LIVE, new MyTextStye(getColor(R.color.recommend_a_live_color), getString(R.string.shared_a_live_steam)));
        }
    };

    private static int getColor(int id) {
        return ContextCompat.getColor(TheLApp.getContext(), id);
    }

    private static String getString(int id) {
        if (id == 0) {
            return "";
        }
        return TheLApp.getContext().getString(id);
    }

    private void getMomentInfoV2(String momentsId) {
        Flowable<MomentsBeanNew> flowable = DefaultRequestService.createAllRequestService().getMomentInfoV2(momentsId);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<MomentsBeanNew>() {
            @Override
            public void onNext(MomentsBeanNew result) {
                super.onNext(result);

            }
        });
    }

}
