package com.thel.ui.widget.zoom;

/*
 * This file provided by Facebook is for non-commercial testing and evaluation
 * purposes only.  Facebook reserves all rights not expressly granted.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * FACEBOOK BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.drawable.Animatable;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.MediaStore;

import androidx.annotation.Nullable;

import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;

import com.facebook.common.executors.CallerThreadExecutor;
import com.facebook.common.internal.Preconditions;
import com.facebook.common.logging.FLog;
import com.facebook.common.references.CloseableReference;
import com.facebook.datasource.DataSource;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.AbstractDraweeController;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.drawable.ScalingUtils;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.generic.GenericDraweeHierarchyBuilder;
import com.facebook.drawee.generic.GenericDraweeHierarchyInflater;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.DraweeView;
import com.facebook.imagepipeline.datasource.BaseBitmapDataSubscriber;
import com.facebook.imagepipeline.image.CloseableImage;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseDataBean;
import com.thel.bean.ImgShareBean;
import com.thel.bean.SharePosterBean;
import com.thel.bean.user.UserInfoPicBean;
import com.thel.constants.TheLConstants;
import com.thel.data.local.FileHelper;
import com.thel.modules.post.MomentPosterActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.utils.DialogUtil;
import com.thel.utils.ImageUtils;
import com.thel.utils.L;
import com.thel.utils.PermissionUtil;
import com.thel.utils.ShareFileUtils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * DraweeView that has zoomable capabilities.
 * <p/>
 * Once the image loads, pinch-to-zoom and translation gestures are enabled.
 * 图片可根据手势缩放，且添加热拉水印图
 */
public class ZoomableDraweeView extends DraweeView<GenericDraweeHierarchy> {

    private static final Class<?> TAG = ZoomableDraweeView.class;

    private static final float HUGE_IMAGE_SCALE_FACTOR_THRESHOLD = 1.1f;
    private static final int SAVE_FAILD = 1;
    private static final int SAVE_SUCCESS = 2;
    private static final int LCOAL_IMAGE = 3;

    private final RectF mImageBounds = new RectF();
    private final RectF mViewBounds = new RectF();

    private DraweeController mHugeImageController;
    private ZoomableController mZoomableController;
    private GestureDetector mTapGestureDetector;
    private boolean singgleTouchFinish = true;
    private final ControllerListener mControllerListener = new BaseControllerListener<Object>() {
        @Override
        public void onFinalImageSet(String id, @Nullable Object imageInfo, @Nullable Animatable animatable) {
            ZoomableDraweeView.this.onFinalImageSet();
        }

        @Override
        public void onRelease(String id) {
            ZoomableDraweeView.this.onRelease();
        }
    };

    private final ZoomableController.Listener mZoomableListener = new ZoomableController.Listener() {
        @Override
        public void onTransformChanged(Matrix transform) {
            ZoomableDraweeView.this.onTransformChanged(transform);
        }
    };

    private final GestureListenerWrapper mTapListenerWrapper = new GestureListenerWrapper();
    private Handler mHandler;

    public ZoomableDraweeView(Context context, GenericDraweeHierarchy hierarchy) {
        super(context);
        setHierarchy(hierarchy);
        init();
    }

    public ZoomableDraweeView(Context context) {
        super(context);
        inflateHierarchy(context, null);
        init();
    }

    public ZoomableDraweeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflateHierarchy(context, attrs);
        init();
    }

    public ZoomableDraweeView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        inflateHierarchy(context, attrs);
        init();
    }

    protected void inflateHierarchy(Context context, @Nullable AttributeSet attrs) {
        Resources resources = context.getResources();
        GenericDraweeHierarchyBuilder builder = new GenericDraweeHierarchyBuilder(resources).setActualImageScaleType(ScalingUtils.ScaleType.FIT_CENTER);
        GenericDraweeHierarchyInflater.updateBuilder(builder, context, attrs);
        setAspectRatio(builder.getDesiredAspectRatio());
        setHierarchy(builder.build());
    }

    private void init() {
        mZoomableController = createZoomableController();
        mZoomableController.setListener(mZoomableListener);
        mTapGestureDetector = new GestureDetector(getContext(), mTapListenerWrapper);
        mHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case SAVE_SUCCESS:
                        DialogUtil.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.poster_download_success));
                        break;
                    case SAVE_FAILD:
                        DialogUtil.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.download_pic_failed));
                        break;
                    case LCOAL_IMAGE:
                        DialogUtil.showToastShort(TheLApp.getContext(), "本地图片，无需下载");
                        break;
                }
            }
        };
    }

    /**
     * 开启双击缩放
     *
     * @param mContext mContext == null表示不单击关闭页面，否则表示单击关闭页面
     * @param imgUrl
     * @param
     */
    public void enableDoubleTapZoom(final Context mContext, final String imgUrl, final String rela_id, final boolean isWaterMark, final ImgShareBean bean) {

        final AnimatedZoomableController zc = (AnimatedZoomableController) getZoomableController();
        setTapListener(new GestureDetector.SimpleOnGestureListener() {
            private final PointF mDoubleTapViewPoint = new PointF();
            private final PointF mDoubleTapImagePoint = new PointF();
            // 判断是否是自己
            String myId = ShareFileUtils.getString(ShareFileUtils.ID, "");

            //长按点击，弹出dialog，提示用户是否进行保存图片
            @Override
            public void onLongPress(MotionEvent e) {

                L.d("ZoomableDraweeView", " imgUrl : " + imgUrl);

                final ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithSource(Uri.parse(imgUrl)).setProgressiveRenderingEnabled(true).build();
                String[] array;
                if (bean == null) {
                    array = new String[]{TheLApp.getContext().getString(R.string.download_pic),TheLApp.getContext().getString(R.string.cancel)};
                }else {
                    array =new String[]{TheLApp.getContext().getString(R.string.share_poster), TheLApp.getContext().getString(R.string.download_pic),TheLApp.getContext().getString(R.string.cancel)};
                }
                DialogUtil.getInstance().showSelectionDialog((Activity) mContext, array, new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                        DialogUtil.getInstance().closeDialog();

                        if (array.length == 2) {
                            if (position == 0) {
                                PermissionUtil.requestStoragePermission((Activity) mContext, new PermissionUtil.PermissionCallback() {
                                    @Override
                                    public void granted() {
                                        final DataSource<CloseableReference<CloseableImage>> dataSource = Fresco.getImagePipeline().fetchDecodedImage(imageRequest, mContext);
                                        dataSource.subscribe(new BaseBitmapDataSubscriber() {
                                            @Override
                                            protected void onNewResultImpl(@Nullable Bitmap bitmap) {
                                                if (bitmap != null) {
                                                    try {
//                                                File imgDir = new File(TheLConstants.F_TAKE_PHOTO_ROOTPATH);
                                                        File imgDir = new File(FileHelper.getInstance().getCameraDir());
                                                        if (!imgDir.exists()) {
                                                            imgDir.mkdir();
                                                        }
                                                        //拿到当前系统的时间
                                                        String fileName = System.currentTimeMillis() + ".jpg";
                                                        File img_file = new File(imgDir, fileName);

                                                        //拿到水印图片的id
                                                        final int water_id = R.mipmap.rela_watermark;
                                                        if (isWaterMark) {

                                                            final Bitmap water_bitmap = ImageUtils.createWatermark(mContext, bitmap, "ID:" + rela_id, water_id);
                                                            MediaStore.Images.Media.insertImage(getContext().getContentResolver(), water_bitmap, "ID:" + rela_id, "ID:" + rela_id);
                                                            ImageUtils.savePic(water_bitmap, img_file.getPath(), 100);
                                                        } else {
                                                            try {
                                                                BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(img_file.getPath()));
                                                                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
                                                                bos.flush();
                                                                bos.close();
                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                        Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                                                        Uri uri = Uri.fromFile(img_file);
                                                        intent.setData(uri);
                                                        mContext.sendBroadcast(intent);
                                                        Message msg = Message.obtain();
                                                        msg.what = SAVE_SUCCESS;
                                                        msg.obj = img_file.getPath();
                                                        mHandler.sendMessage(msg);
                                                    } catch (Exception e1) {
                                                        e1.printStackTrace();
                                                        mHandler.sendEmptyMessage(SAVE_FAILD);
                                                    }
                                                } else {
                                                    mHandler.sendEmptyMessage(SAVE_FAILD);
                                                }
                                            }

                                            @Override
                                            protected void onFailureImpl(DataSource<CloseableReference<CloseableImage>> dataSource) {
                                                if (imgUrl.contains("/storage/emulated/")) {
                                                    mHandler.sendEmptyMessage(LCOAL_IMAGE);
                                                }else {
                                                    mHandler.sendEmptyMessage(SAVE_FAILD);
                                                }
                                            }
                                        }, CallerThreadExecutor.getInstance());
                                    }

                                    @Override
                                    public void denied() {

                                    }

                                    @Override
                                    public void gotoSetting() {

                                    }
                                });
                            }
                        }else if (array.length == 3) {
                            switch (position) {
                                case 0:
                                    gotoShare(bean, imgUrl);
                                    break;
                                case 1:
                                    PermissionUtil.requestStoragePermission((Activity) mContext, new PermissionUtil.PermissionCallback() {
                                        @Override
                                        public void granted() {
                                            final DataSource<CloseableReference<CloseableImage>> dataSource = Fresco.getImagePipeline().fetchDecodedImage(imageRequest, mContext);
                                            dataSource.subscribe(new BaseBitmapDataSubscriber() {
                                                @Override
                                                protected void onNewResultImpl(@Nullable Bitmap bitmap) {
                                                    if (bitmap != null) {
                                                        try {
//                                                File imgDir = new File(TheLConstants.F_TAKE_PHOTO_ROOTPATH);
                                                            File imgDir = new File(FileHelper.getInstance().getCameraDir());
                                                            if (!imgDir.exists()) {
                                                                imgDir.mkdir();
                                                            }
                                                            //拿到当前系统的时间
                                                            String fileName = System.currentTimeMillis() + ".jpg";
                                                            File img_file = new File(imgDir, fileName);

                                                            //拿到水印图片的id
                                                            final int water_id = R.mipmap.rela_watermark;
                                                            if (isWaterMark) {

                                                                final Bitmap water_bitmap = ImageUtils.createWatermark(mContext, bitmap, "ID:" + rela_id, water_id);
                                                                MediaStore.Images.Media.insertImage(getContext().getContentResolver(), water_bitmap, "ID:" + rela_id, "ID:" + rela_id);
                                                                ImageUtils.savePic(water_bitmap, img_file.getPath(), 100);
                                                            } else {
                                                                try {
                                                                    BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(img_file.getPath()));
                                                                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
                                                                    bos.flush();
                                                                    bos.close();
                                                                } catch (Exception e) {
                                                                    e.printStackTrace();
                                                                }
                                                            }
                                                            Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                                                            Uri uri = Uri.fromFile(img_file);
                                                            intent.setData(uri);
                                                            mContext.sendBroadcast(intent);
                                                            Message msg = Message.obtain();
                                                            msg.what = SAVE_SUCCESS;
                                                            msg.obj = img_file.getPath();
                                                            mHandler.sendMessage(msg);
                                                        } catch (Exception e1) {
                                                            e1.printStackTrace();
                                                            mHandler.sendEmptyMessage(SAVE_FAILD);
                                                        }
                                                    } else {
                                                        mHandler.sendEmptyMessage(SAVE_FAILD);
                                                    }
                                                }

                                                @Override
                                                protected void onFailureImpl(DataSource<CloseableReference<CloseableImage>> dataSource) {
                                                    if (imgUrl.contains("/storage/emulated/")) {
                                                        mHandler.sendEmptyMessage(LCOAL_IMAGE);
                                                    }else {
                                                        mHandler.sendEmptyMessage(SAVE_FAILD);
                                                    }
                                                }
                                            }, CallerThreadExecutor.getInstance());
                                        }

                                        @Override
                                        public void denied() {

                                        }

                                        @Override
                                        public void gotoSetting() {

                                        }
                                    });
                                    break;
                                default:
                                    break;

                            }
                        }

                    }
                }, false, 2, null);

                super.onLongPress(e);
            }

            @Override
            public boolean onSingleTapConfirmed(MotionEvent e) {
                if (mContext != null)
                    ((Activity) mContext).finish();
                return super.onSingleTapConfirmed(e);
            }

            @Override
            public boolean onDoubleTapEvent(MotionEvent e) {
                PointF vp = new PointF(e.getX(), e.getY());
                PointF ip = zc.mapViewToImage(vp);
                float maxScaleFactor = zc.getMaxScaleFactor();
                switch (e.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        mDoubleTapViewPoint.set(vp);
                        mDoubleTapImagePoint.set(ip);
                        break;
                    case MotionEvent.ACTION_UP:
                        if (zc.getScaleFactor() < 1.5f) {
                            zc.zoomToPoint(maxScaleFactor, ip, vp, DefaultZoomableController.LIMIT_ALL, 300, null);
                        } else {
                            zc.zoomToPoint(1.0f, ip, vp, DefaultZoomableController.LIMIT_ALL, 300, null);
                        }
                        break;
                }
                return true;
            }
        });
    }

    /**
     * 开启双击缩放
     *
     * @param mContext mContext == null表示不单击关闭页面，否则表示单击关闭页面
     * @param imgUrl
     * @param
     */
    public void enableDoubleTapZoomNotShare(final Context mContext, final String imgUrl, final String rela_id, final boolean isWaterMark, final ImgShareBean bean) {

        final AnimatedZoomableController zc = (AnimatedZoomableController) getZoomableController();
        setTapListener(new GestureDetector.SimpleOnGestureListener() {
            private final PointF mDoubleTapViewPoint = new PointF();
            private final PointF mDoubleTapImagePoint = new PointF();
            // 判断是否是自己
            String myId = ShareFileUtils.getString(ShareFileUtils.ID, "");

            @Override
            public boolean onSingleTapConfirmed(MotionEvent e) {
                if (mContext != null && singgleTouchFinish)
                    ((Activity) mContext).finish();
                return super.onSingleTapConfirmed(e);
            }

            @Override
            public boolean onDoubleTapEvent(MotionEvent e) {
                PointF vp = new PointF(e.getX(), e.getY());
                PointF ip = zc.mapViewToImage(vp);
                float maxScaleFactor = zc.getMaxScaleFactor();
                switch (e.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        mDoubleTapViewPoint.set(vp);
                        mDoubleTapImagePoint.set(ip);
                        break;
                    case MotionEvent.ACTION_UP:
                        if (zc.getScaleFactor() < 1.5f) {
                            zc.zoomToPoint(maxScaleFactor, ip, vp, DefaultZoomableController.LIMIT_ALL, 300, null);
                        } else {
                            zc.zoomToPoint(1.0f, ip, vp, DefaultZoomableController.LIMIT_ALL, 300, null);
                        }
                        break;
                }
                return true;
            }
        });
    }

    /**
     * Gets the original image bounds, in view-absolute coordinates.
     * <p>
     * <p> The original image bounds are those reported by the hierarchy. The hierarchy itself may
     * apply scaling on its own (e.g. due to scale logType) so the reported bounds are not necessarily
     * the same as the actual bitmap dimensions. In other words, the original image bounds correspond
     * to the image bounds within this view when no zoomable transformation is applied, but including
     * the potential scaling of the hierarchy.
     * Having the actual bitmap dimensions abstracted away from this view greatly simplifies
     * implementation because the actual bitmap may change (e.g. when a high-res image arrives and
     * replaces the previously set low-res image). With proper hierarchy scaling (e.g. FIT_CENTER),
     * this underlying change will not affect this view nor the zoomable transformation in any way.
     */
    protected void getImageBounds(RectF outBounds) {
        getHierarchy().getActualImageBounds(outBounds);
    }

    /**
     * Gets the bounds used to limit the translation, in view-absolute coordinates.
     * <p>
     * <p> These bounds are passed to the zoomable controller in order to limit the translation. The
     * image is attempted to be centered within the limit bounds if the transformed image is smaller.
     * There will be no empty spaces within the limit bounds if the transformed image is bigger.
     * This applies to each dimension (horizontal and vertical) independently.
     * <p> Unless overridden by a subclass, these bounds are same as the view bounds.
     */
    protected void getLimitBounds(RectF outBounds) {
        outBounds.set(0, 0, getWidth(), getHeight());
    }

    /**
     * Sets a custom zoomable controller, instead of using the default one.
     */
    public void setZoomableController(ZoomableController zoomableController) {
        Preconditions.checkNotNull(zoomableController);
        mZoomableController.setListener(null);
        mZoomableController = zoomableController;
        mZoomableController.setListener(mZoomableListener);
    }

    /**
     * Gets the zoomable controller.
     * <p>
     * <p> Zoomable controller can be used to zoom to point, or to map point from view to image
     * coordinates for instance.
     */
    public ZoomableController getZoomableController() {
        return mZoomableController;
    }

    /**
     * Sets the tap listener.
     */
    public void setTapListener(GestureDetector.SimpleOnGestureListener tapListener) {
        mTapListenerWrapper.setListener(tapListener);
    }

    /**
     * Sets whether long-press tap detection is enabled.
     * Unfortunately, long-press conflicts with onDoubleTapEvent.
     */
    public void setIsLongpressEnabled(boolean enabled) {
        mTapGestureDetector.setIsLongpressEnabled(enabled);
    }

    /**
     * Sets the image controller.
     */
    @Override
    public void setController(@Nullable DraweeController controller) {
        setControllers(controller, null);
    }

    /**
     * Sets the controllers for the normal and huge image.
     * <p>
     * <p> The huge image controller is used after the image gets scaled above a certain threshold.
     * <p>
     * <p> IMPORTANT: in order to avoid a flicker when switching to the huge image, the huge image
     * controller should have the normal-image-uri set as its low-res-uri.
     *
     * @param controller          controller to be initially used
     * @param hugeImageController controller to be used after the client starts zooming-in
     */
    public void setControllers(@Nullable DraweeController controller, @Nullable DraweeController hugeImageController) {
        setControllersInternal(null, null);
        mZoomableController.setEnabled(false);
        setControllersInternal(controller, hugeImageController);
    }

    private void setControllersInternal(@Nullable DraweeController controller, @Nullable DraweeController hugeImageController) {
        removeControllerListener(getController());
        addControllerListener(controller);
        mHugeImageController = hugeImageController;
        super.setController(controller);
    }

    private void maybeSetHugeImageController() {
        if (mHugeImageController != null && mZoomableController.getScaleFactor() > HUGE_IMAGE_SCALE_FACTOR_THRESHOLD) {
            setControllersInternal(mHugeImageController, null);
        }
    }

    private void removeControllerListener(DraweeController controller) {
        if (controller instanceof AbstractDraweeController) {
            ((AbstractDraweeController) controller).removeControllerListener(mControllerListener);
        }
    }

    private void addControllerListener(DraweeController controller) {
        if (controller instanceof AbstractDraweeController) {
            ((AbstractDraweeController) controller).addControllerListener(mControllerListener);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        try {
            int saveCount = canvas.save();
            canvas.concat(mZoomableController.getTransform());
            super.onDraw(canvas);
            canvas.restoreToCount(saveCount);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int a = event.getActionMasked();
        FLog.v(getLogTag(), "onTouchEvent: %d, view %x, received", a, this.hashCode());
        if (mTapGestureDetector.onTouchEvent(event)) {
            FLog.v(getLogTag(), "onTouchEvent: %d, view %x, handled by tap gesture detector", a, this.hashCode());
            return true;
        }
        if (mZoomableController.onTouchEvent(event)) {
            if (!mZoomableController.wasTransformCorrected() && mZoomableController.getScaleFactor() > 1.0f) {
                getParent().requestDisallowInterceptTouchEvent(true);
            }
            FLog.v(getLogTag(), "onTouchEvent: %d, view %x, handled by zoomable controller", a, this.hashCode());
            return true;
        }
        if (super.onTouchEvent(event)) {
            FLog.v(getLogTag(), "onTouchEvent: %d, view %x, handled by the super", a, this.hashCode());
            return true;
        }
        // None of our components reported that they handled the touch event. Upon returning false
        // from this method, our parent won't send us any more events for this gesture. Unfortunately,
        // some componentes may have started a delayed action, such as a long-press timer, and since we
        // won't receive an ACTION_UP that would cancel that timer, a false event may be triggered.
        // To prevent that we explicitly send one last cancel event when returning false.
        MotionEvent cancelEvent = MotionEvent.obtain(event);
        cancelEvent.setAction(MotionEvent.ACTION_CANCEL);
        mTapGestureDetector.onTouchEvent(cancelEvent);
        mZoomableController.onTouchEvent(cancelEvent);
        cancelEvent.recycle();
        return false;
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        FLog.v(getLogTag(), "onLayout: view %x", this.hashCode());
        super.onLayout(changed, left, top, right, bottom);
        updateZoomableControllerBounds();
    }

    private void onFinalImageSet() {
        FLog.v(getLogTag(), "onFinalImageSet: view %x", this.hashCode());
        if (!mZoomableController.isEnabled()) {
            updateZoomableControllerBounds();
            mZoomableController.setEnabled(true);
        }
    }

    private void onRelease() {
        FLog.v(getLogTag(), "onRelease: view %x", this.hashCode());
        mZoomableController.setEnabled(false);
    }

    protected void onTransformChanged(Matrix transform) {
        FLog.v(getLogTag(), "onTransformChanged: view %x, transform: %s", this.hashCode(), transform);
        maybeSetHugeImageController();
        invalidate();
    }

    protected void updateZoomableControllerBounds() {
        getImageBounds(mImageBounds);
        getLimitBounds(mViewBounds);
        mZoomableController.setImageBounds(mImageBounds);
        mZoomableController.setViewBounds(mViewBounds);
        FLog.v(getLogTag(), "updateZoomableControllerBounds: view %x, view bounds: %s, image bounds: %s", this.hashCode(), mViewBounds, mImageBounds);
    }

    protected Class<?> getLogTag() {
        return TAG;
    }

    protected ZoomableController createZoomableController() {
        return AnimatedZoomableController.newInstance();
    }

    public void setSingleTouchFinish(boolean finish) {
        singgleTouchFinish = finish;
    }

    public void enableDoubleTapZoomPhotoReport(final Context mContext, final String imgUrl, final String rela_id, final String userid, final boolean isWaterMark, final ImgShareBean bean, final UserInfoPicBean mUserinfoPicbean) {

        final AnimatedZoomableController zc = (AnimatedZoomableController) getZoomableController();
        setTapListener(new GestureDetector.SimpleOnGestureListener() {
            private final PointF mDoubleTapViewPoint = new PointF();
            private final PointF mDoubleTapImagePoint = new PointF();
            // 判断是否是自己

            //长按点击，弹出dialog，提示用户是否进行保存图片
            @Override
            public void onLongPress(final MotionEvent e) {

                L.d("Zoom", "-------onLongPress--------");

                String[] strings;

                if (userid == null) {
                    strings = new String[]{TheLApp.getContext().getString(R.string.share_poster), TheLApp.getContext().getString(R.string.download_pic)};

                } else {
                    strings = new String[]{TheLApp.getContext().getString(R.string.userinfo_activity_dialog_report_title), TheLApp.getContext().getString(R.string.share_poster), TheLApp.getContext().getString(R.string.download_pic)};
                }

                final ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithSource(Uri.parse(imgUrl)).setProgressiveRenderingEnabled(true).build();
                DialogUtil.getInstance().showSelectionDialog((Activity) mContext, strings, new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                        DialogUtil.getInstance().closeDialog();
                        if (userid == null) {
                            switch (position) {
                                case 0:
                                    gotoShare(bean, imgUrl);
                                    break;
                                case 1:
                                    final DataSource<CloseableReference<CloseableImage>> dataSource = Fresco.getImagePipeline().fetchDecodedImage(imageRequest, mContext);
                                    dataSource.subscribe(new BaseBitmapDataSubscriber() {
                                        @Override
                                        protected void onNewResultImpl(@Nullable Bitmap bitmap) {
                                            if (bitmap != null) {
                                                try {
                                                    File imgDir = new File(TheLConstants.F_TAKE_PHOTO_ROOTPATH);
                                                    if (!imgDir.exists()) {
                                                        imgDir.mkdir();
                                                    }
                                                    //拿到当前系统的时间
                                                    String fileName = System.currentTimeMillis() + ".jpg";
                                                    File img_file = new File(imgDir, fileName);

                                                    //拿到水印图片的id
                                                    final int water_id = R.mipmap.rela_watermark;
                                                    if (isWaterMark) {

                                                        final Bitmap water_bitmap = ImageUtils.createWatermark(mContext, bitmap, "ID:" + rela_id, water_id);
                                                        ImageUtils.savePic(water_bitmap, img_file.getPath(), 100);
                                                    } else {
                                                        try {
                                                            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(img_file.getPath()));
                                                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
                                                            bos.flush();
                                                            bos.close();
                                                        } catch (Exception e) {
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                    Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                                                    Uri uri = Uri.fromFile(img_file);
                                                    intent.setData(uri);
                                                    mContext.sendBroadcast(intent);
                                                    Message msg = Message.obtain();
                                                    msg.what = SAVE_SUCCESS;
                                                    msg.obj = img_file.getPath();
                                                    mHandler.sendMessage(msg);
                                                } catch (Exception e1) {
                                                    e1.printStackTrace();
                                                    mHandler.sendEmptyMessage(SAVE_FAILD);
                                                }
                                            } else
                                                mHandler.sendEmptyMessage(SAVE_FAILD);
                                        }

                                        @Override
                                        protected void onFailureImpl(DataSource<CloseableReference<CloseableImage>> dataSource) {

                                        }
                                    }, CallerThreadExecutor.getInstance());
                                    break;
                                default:
                                    break;
                            }
                        } else {
                            switch (position) {
                                case 0:
                                    if (userid != null && !TextUtils.isEmpty(userid) && mUserinfoPicbean != null)
                                        RequestBusiness.getInstance()
                                                .postReportImageOrUser(userid, "userimg", Integer.parseInt(mUserinfoPicbean.picId))
                                                .onBackpressureDrop().subscribeOn(Schedulers.io())
                                                .observeOn(AndroidSchedulers.mainThread())
                                                .subscribe(new InterceptorSubscribe<BaseDataBean>() {
                                                    @Override
                                                    public void onNext(BaseDataBean data) {
                                                        super.onNext(data);
                                                        DialogUtil.showToastShort(mContext, getResources().getString(R.string.userinfo_activity_report_success));
                                                    }
                                                });

                                    break;
                                case 1:
                                    gotoShare(bean, imgUrl);
                                    break;
                                case 2:
                                    final DataSource<CloseableReference<CloseableImage>> dataSource = Fresco.getImagePipeline().fetchDecodedImage(imageRequest, mContext);
                                    dataSource.subscribe(new BaseBitmapDataSubscriber() {
                                        @Override
                                        protected void onNewResultImpl(@Nullable Bitmap bitmap) {
                                            if (bitmap != null) {
                                                try {
                                                    File imgDir = new File(TheLConstants.F_TAKE_PHOTO_ROOTPATH);
                                                    if (!imgDir.exists()) {
                                                        imgDir.mkdir();
                                                    }
                                                    //拿到当前系统的时间
                                                    String fileName = System.currentTimeMillis() + ".jpg";
                                                    File img_file = new File(imgDir, fileName);

                                                    //拿到水印图片的id
                                                    final int water_id = R.mipmap.rela_watermark;
                                                    if (isWaterMark) {

                                                        final Bitmap water_bitmap = ImageUtils.createWatermark(mContext, bitmap, "ID:" + rela_id, water_id);
                                                        ImageUtils.savePic(water_bitmap, img_file.getPath(), 100);
                                                    } else {
                                                        try {
                                                            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(img_file.getPath()));
                                                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
                                                            bos.flush();
                                                            bos.close();
                                                        } catch (Exception e) {
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                    Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                                                    Uri uri = Uri.fromFile(img_file);
                                                    intent.setData(uri);
                                                    mContext.sendBroadcast(intent);
                                                    Message msg = Message.obtain();
                                                    msg.what = SAVE_SUCCESS;
                                                    msg.obj = img_file.getPath();
                                                    mHandler.sendMessage(msg);
                                                } catch (Exception e1) {
                                                    e1.printStackTrace();
                                                    mHandler.sendEmptyMessage(SAVE_FAILD);
                                                }
                                            } else
                                                mHandler.sendEmptyMessage(SAVE_FAILD);
                                        }

                                        @Override
                                        protected void onFailureImpl(DataSource<CloseableReference<CloseableImage>> dataSource) {

                                        }
                                    }, CallerThreadExecutor.getInstance());
                                    break;
                                default:
                                    break;
                            }
                        }

                        DialogUtil.getInstance().closeDialog();

                    }
                }, false, 2, null);
                super.onLongPress(e);

            }

        });
    }

    private void gotoShare(ImgShareBean imageShareBean, String imgUrl) {
        if (imageShareBean != null) {
            SharePosterBean sharePosterBean = new SharePosterBean();
            sharePosterBean.avatar = imageShareBean.picUserUrl;
            sharePosterBean.momentsText = imageShareBean.commentText;
            sharePosterBean.imageUrl = imgUrl;
            sharePosterBean.nickname = imageShareBean.nickName;
            sharePosterBean.userName = imageShareBean.userID;
            sharePosterBean.from = MomentPosterActivity.FROM_PHOTO;
            MomentPosterActivity.gotoShare(sharePosterBean);
        }
    }

    /**
     * Wrapper for SimpleOnGestureListener as GestureDetector does not allow changing its listener.
     */
    class GestureListenerWrapper extends GestureDetector.SimpleOnGestureListener {

        private GestureDetector.SimpleOnGestureListener mDelegate;

        public GestureListenerWrapper() {
            mDelegate = new GestureDetector.SimpleOnGestureListener();
        }

        public void setListener(GestureDetector.SimpleOnGestureListener listener) {
            mDelegate = listener;
        }

        @Override
        public void onLongPress(MotionEvent e) {
            mDelegate.onLongPress(e);
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            return mDelegate.onScroll(e1, e2, distanceX, distanceY);
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            return mDelegate.onFling(e1, e2, velocityX, velocityY);
        }

        @Override
        public void onShowPress(MotionEvent e) {
            mDelegate.onShowPress(e);
        }

        @Override
        public boolean onDown(MotionEvent e) {
            return mDelegate.onDown(e);
        }

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            return mDelegate.onDoubleTap(e);
        }

        @Override
        public boolean onDoubleTapEvent(MotionEvent e) {
            return mDelegate.onDoubleTapEvent(e);
        }

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            return mDelegate.onSingleTapConfirmed(e);
        }

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            return mDelegate.onSingleTapUp(e);
        }
    }
}
