package com.thel.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.InitRecommendUserBean;
import com.thel.bean.InitRecommendUserMomentBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.modules.main.home.moments.comment.MomentCommentActivity;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;
import com.thel.utils.AppInit;
import com.thel.utils.DeviceUtils;
import com.thel.utils.ImageUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;

import java.util.ArrayList;

/**
 * 新用户注册成功后，推荐一批关注用户
 */
public class RecommendUserRecyclerViewAdapter extends BaseRecyclerViewAdapter<InitRecommendUserBean> {

    private View.OnClickListener mOnclickListener;
    private OnFollowClickListener onFollowClickListener;

    public RecommendUserRecyclerViewAdapter(View.OnClickListener onClickListener, ArrayList<InitRecommendUserBean> data) {
        super(R.layout.recommend_user_listitem, data);
        mOnclickListener = onClickListener;
    }

    @Override
    public void convert(final BaseViewHolder holdView, final InitRecommendUserBean initRecommendUserBean) {

        if (initRecommendUserBean.moments != null) {

            ((GridView) holdView.getView(R.id.grid_photos)).setAdapter(new RecommendUsersGridAdapter(initRecommendUserBean.moments));
            ((GridView) holdView.getView(R.id.grid_photos)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    try {
//                        Intent intent = new Intent(TheLApp.getContext(), MomentCommentActivity.class);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, initRecommendUserBean.moments.get(position).momentsId);
//                        TheLApp.getContext().startActivity(intent);
                        FlutterRouterConfig.Companion.gotoMomentDetails(initRecommendUserBean.moments.get(position).momentsId);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        holdView.setText(R.id.txt_nickname, initRecommendUserBean.nickName);
        if ("zh_CN".equals(DeviceUtils.getLanguage()))
            holdView.setText(R.id.txt_desc, initRecommendUserBean.recommendReason);
        else if ("zh_TW".equals(DeviceUtils.getLanguage()) || "zh_HK".equals(DeviceUtils.getLanguage()))
            holdView.setText(R.id.txt_desc, initRecommendUserBean.twReason);
        else
            holdView.setText(R.id.txt_desc, initRecommendUserBean.enReason);
        if (initRecommendUserBean.followStatus == 0) {
            holdView.setText(R.id.txt_follow_user, TheLApp.getContext().getString(R.string.userinfo_activity_follow));
            holdView.setBackgroundRes(R.id.txt_follow_user, R.drawable.bg_follow_round_button_blue);
        } else {
            holdView.setText(R.id.txt_follow_user, TheLApp.getContext().getString(R.string.userinfo_activity_followed));
            holdView.setBackgroundRes(R.id.txt_follow_user, R.drawable.bg_followed_round_button_gray);
        }

        holdView.setImageUrl(R.id.img_avatar, initRecommendUserBean.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE);
        holdView.setOnClickListener(R.id.img_avatar, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
//                Intent intent = new Intent();
//                intent.setClass(TheLApp.getContext(), UserInfoActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, initRecommendUserBean.userId + "");
//                TheLApp.getContext().startActivity(intent);
                FlutterRouterConfig.Companion.gotoUserInfo(initRecommendUserBean.userId+"");
            }
        });

        holdView.setOnClickListener(R.id.txt_follow_user, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onFollowClickListener != null) {
                    onFollowClickListener.onFollowClick(initRecommendUserBean, holdView.getLayoutPosition() - getHeaderLayoutCount());
                }
            }
        });
        holdView.setOnClickListener(R.id.recommend_opts_more, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onFollowClickListener != null) {
                    onFollowClickListener.onMoreClick(initRecommendUserBean, holdView.getLayoutPosition() - getHeaderLayoutCount());
                }
            }
        });

    }

    public class RecommendUsersGridAdapter extends BaseAdapter {

        private LayoutInflater mInflater;

        private int viewSize; // 图片控件的尺寸

        private int photoSize;// 这是加载图片的尺寸，不是展示图片的控件的尺寸

        private ArrayList<InitRecommendUserMomentBean> moments;

        public RecommendUsersGridAdapter(ArrayList<InitRecommendUserMomentBean> data) {
            mInflater = (LayoutInflater) TheLApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            this.moments = data;
            int screenWidth = AppInit.displayMetrics.widthPixels;// 屏幕宽度

            // 这个数值是根据布局的margin、padding等计算出来的，具体要看recommend_user_listitem.xml布局
            viewSize = (screenWidth - TheLApp.getContext().getResources().getDimensionPixelSize(R.dimen.recommend_users_horizontal_margin) * 4) / 3;
            // 这个数值与UserInfoGridAdapter中的图片尺寸保持一致，这样可以省流量
            photoSize = (screenWidth - Utils.dip2px(TheLApp.getContext(), 46)) / 3;
        }

        @Override
        public int getCount() {
            return moments.size();
        }

        @Override
        public Object getItem(int position) {
            return moments.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            HoldView holdView = null;
            if (null == convertView) {
                convertView = mInflater.inflate(R.layout.init_recommend_user_moment, parent, false);

                holdView = new HoldView();

                holdView.image = convertView.findViewById(R.id.image);
                holdView.img_play = convertView.findViewById(R.id.img_play);
                holdView.lin_text = convertView.findViewById(R.id.lin_text);
                holdView.text = convertView.findViewById(R.id.text);

                convertView.setTag(holdView);

                AbsListView.LayoutParams lp = new AbsListView.LayoutParams(viewSize, viewSize);
                convertView.setLayoutParams(lp);
            } else {
                holdView = (HoldView) convertView.getTag();
            }

            final InitRecommendUserMomentBean initRecommendUserMomentBean = moments.get(position);

            if (MomentsBean.MOMENT_TYPE_TEXT.equals(initRecommendUserMomentBean.momentsType) || MomentsBean.MOMENT_TYPE_THEME.equals(initRecommendUserMomentBean.momentsType) || MomentsBean.MOMENT_TYPE_THEME_REPLY.equals(initRecommendUserMomentBean.momentsType)) {// 文字日志和话题参与以及话题日志，显示文字
                holdView.lin_text.setVisibility(View.VISIBLE);
                holdView.img_play.setVisibility(View.GONE);
                holdView.image.setVisibility(View.GONE);
                holdView.text.setText(initRecommendUserMomentBean.momentsText);
            } else {
                holdView.lin_text.setVisibility(View.GONE);
                if (MomentsBean.MOMENT_TYPE_TEXT_VOICE.equals(initRecommendUserMomentBean.momentsType) || MomentsBean.MOMENT_TYPE_VOICE.equals(initRecommendUserMomentBean.momentsType)) {
                    holdView.img_play.setVisibility(View.VISIBLE);
                    holdView.img_play.setImageResource(R.mipmap.btn_feed_play_big);
                } else if (MomentsBean.MOMENT_TYPE_VIDEO.equals(initRecommendUserMomentBean.momentsType)) {
                    holdView.img_play.setVisibility(View.VISIBLE);
                    holdView.img_play.setImageResource(R.mipmap.btn_play_video);
                } else if (MomentsBean.MOMENT_TYPE_LIVE.equals(initRecommendUserMomentBean.momentsType)) {
                    holdView.img_play.setVisibility(View.VISIBLE);
                    holdView.img_play.setImageResource(R.mipmap.btn_live_play);
                } else {
                    holdView.img_play.setVisibility(View.GONE);
                }
                holdView.image.setVisibility(View.VISIBLE);
                String[] pics = initRecommendUserMomentBean.imageUrl.split(",");
                if (pics.length > 0)
                    holdView.image.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(pics[0], photoSize, photoSize)));
            }

            return convertView;
        }

        class HoldView {
            SimpleDraweeView image;
            ImageView img_play;
            LinearLayout lin_text;
            TextView text;
        }

    }

    public void setOnFollowClickListener(OnFollowClickListener listener) {
        this.onFollowClickListener = listener;
    }

    public interface OnFollowClickListener {
        void onFollowClick(InitRecommendUserBean initRecommendUserBean, int position);

        void onMoreClick(InitRecommendUserBean initRecommendUserBean, int position);
    }


}