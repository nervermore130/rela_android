package com.thel.ui.widget;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;

import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;

import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.thel.R;

/**
 * Created by setsail on 2016/12/20.
 */

public class LikeButtonView extends FrameLayout {

    private static final LinearInterpolator LINEAR_INTERPOLATOR = new LinearInterpolator();

    private ImageView imgLike;

    private View likeAnimImage;

    private boolean isChecked;

    private AnimatorSet animatorSet;

    private int defaultBgRes = R.mipmap.ic_moment_like;

    public LikeButtonView(@NonNull Context context) {
        super(context);
        init();
    }

    public LikeButtonView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        intiAttrs(context, attrs);
        init();
    }

    public LikeButtonView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        intiAttrs(context, attrs);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public LikeButtonView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr, @StyleRes int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        intiAttrs(context, attrs);
        init();
    }

    private void intiAttrs(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.LikeButtonView);
        defaultBgRes = typedArray.getResourceId(R.styleable.LikeButtonView_default_bg, R.mipmap.ic_moment_like);
        typedArray.recycle();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.view_like_button, this, true);
        imgLike = findViewById(R.id.img_like);
        imgLike.setBackgroundResource(defaultBgRes);
    }

    public void setLikeAnimImage(View imgLike) {
        likeAnimImage = imgLike;
    }

    public void check() {
        if (animatorSet != null) {
            animatorSet.cancel();
        }

        if (likeAnimImage != null) {
            imgLike.setBackgroundResource(R.mipmap.home_feed_btn_like_sel);
            likeAnimImage.animate().cancel();
            cancelAnim(likeAnimImage);
            animatorSet = new AnimatorSet();

            ObjectAnimator translation = ObjectAnimator.ofFloat(likeAnimImage, TRANSLATION_Y, -200);
            translation.setDuration(1300);
            translation.setInterpolator(LINEAR_INTERPOLATOR);

            ObjectAnimator scaleX = ObjectAnimator.ofFloat(likeAnimImage, SCALE_X, 1f, 3f);
            scaleX.setDuration(1300);
            scaleX.setInterpolator(LINEAR_INTERPOLATOR);

            ObjectAnimator scaleY = ObjectAnimator.ofFloat(likeAnimImage, SCALE_Y, 1f, 3f);
            scaleY.setDuration(1300);
            scaleY.setInterpolator(LINEAR_INTERPOLATOR);

            ObjectAnimator rotate1 = ObjectAnimator.ofFloat(likeAnimImage, ROTATION, 0f, 40f);
            rotate1.setDuration(300);
            rotate1.setInterpolator(LINEAR_INTERPOLATOR);

            ObjectAnimator rotate2 = ObjectAnimator.ofFloat(likeAnimImage, ROTATION, 30f, -40f);
            rotate2.setDuration(1000);
            rotate2.setInterpolator(LINEAR_INTERPOLATOR);

            ObjectAnimator alpha = ObjectAnimator.ofFloat(likeAnimImage, ALPHA, 0f);
            alpha.setDuration(500);
            alpha.setStartDelay(700);
            alpha.setInterpolator(LINEAR_INTERPOLATOR);

            animatorSet.playTogether(translation, scaleX, scaleY, alpha);
            animatorSet.playSequentially(rotate1, rotate2);

            animatorSet.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                    likeAnimImage.setVisibility(VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    likeAnimImage.setVisibility(GONE);
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                    cancelAnim(likeAnimImage);
                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });

            animatorSet.start();
        }
    }

    private void cancelAnim(View view) {
        view.setTranslationY(0);
        view.setScaleX(1f);
        view.setScaleY(1f);
        view.setAlpha(1f);
        view.setRotation(0f);
    }

    public void setChecked(boolean checked, int... fromtype) {
        if (fromtype.length > 0) {
            defaultBgRes = R.mipmap.ic_moment_like;
        }
        isChecked = checked;
        imgLike.setBackgroundResource(isChecked ? R.mipmap.home_feed_btn_like_sel : defaultBgRes);
    }
}
