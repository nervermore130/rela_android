package com.thel.ui.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.thel.R;
import com.thel.base.BaseAdapter;
import com.thel.bean.topic.TopicFollowerBean;

import butterknife.ButterKnife;

/**
 * Created by liuyun on 2017/10/24.
 */

public class MomentMentionAdapter extends BaseAdapter<MomentMentionAdapter.MomentMentionViewHolder, TopicFollowerBean> {

    public MomentMentionAdapter(Context context) {
        super(context);
    }

    @Override
    public void onBindViewHolder(MomentMentionViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
    }

    @Override
    public MomentMentionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MomentMentionViewHolder(mLayoutInflater.inflate(R.layout.item_common_friend_list, null));
    }

    class MomentMentionViewHolder extends RecyclerView.ViewHolder {

        MomentMentionViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }

}
