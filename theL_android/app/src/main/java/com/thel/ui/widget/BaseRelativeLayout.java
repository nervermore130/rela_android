package com.thel.ui.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import com.thel.utils.L;

public class BaseRelativeLayout extends RelativeLayout {

    private boolean isDrawed = false;

    public BaseRelativeLayout(Context context) {
        super(context);
    }

    public BaseRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BaseRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        L.d("BaseRelativeLayout", "-------onMeasure------");

    }

    @Override protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        L.d("BaseRelativeLayout", "-------onLayout------");


    }

    @Override protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        L.d("BaseRelativeLayout", "-------onDraw------");


    }

    @Override public void draw(Canvas canvas) {

        super.draw(canvas);

        L.d("BaseRelativeLayout", "-------draw------");

    }

}
