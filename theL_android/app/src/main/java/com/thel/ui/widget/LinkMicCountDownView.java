package com.thel.ui.widget;

import android.content.Context;
import androidx.annotation.Nullable;
import android.util.AttributeSet;

import java.util.concurrent.TimeUnit;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by liuyun on 2017/12/17.
 */

public class LinkMicCountDownView extends androidx.appcompat.widget.AppCompatTextView {

    private int count = 1;

    private CompositeDisposable mCompositeDisposable;

    public LinkMicCountDownView(Context context) {
        super(context);
        init();
    }

    public LinkMicCountDownView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public LinkMicCountDownView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {

        mCompositeDisposable = new CompositeDisposable();

    }

    public void show() {

        hide();

        setText(String.valueOf(count));

        Disposable disposable = Flowable.interval(1, TimeUnit.SECONDS)
                .onBackpressureDrop()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Long>() {
                    @Override public void accept(Long aLong) {
                        count++;
                        setText(formatTime());
                    }
                });
        mCompositeDisposable.add(disposable);
    }

    public void hide() {
        count = 1;
        mCompositeDisposable.clear();
    }

    private StringBuffer mStringBuffer = new StringBuffer();

    private synchronized String formatTime() {
        mStringBuffer.setLength(0);
        int min = count / 60;//得到分钟数
        int seconds = count % 60;//得到秒

        if (min >= 10) {
            mStringBuffer.append(min + ":");
            if (seconds >= 10) {
                mStringBuffer.append(seconds);
            } else {
                mStringBuffer.append("0" + seconds);
            }
        } else {
            mStringBuffer.append("0" + min + ":");
            if (seconds >= 10) {
                mStringBuffer.append(seconds);
            } else {
                mStringBuffer.append("0" + seconds);
            }
        }

        return mStringBuffer.toString();
    }
}
