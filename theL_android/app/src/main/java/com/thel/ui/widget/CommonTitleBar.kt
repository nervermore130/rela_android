package com.thel.ui.widget

import android.app.Activity
import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.RelativeLayout
import androidx.core.content.res.getStringOrThrow
import com.thel.R
import kotlinx.android.synthetic.main.include_common_title_bar.view.*

class CommonTitleBar : RelativeLayout {

    private var mTitle: String = ""

    private var mIsShowMore: Boolean = false

    private var mImgRes: Int = 0

    lateinit var mOnMoreClickListener: OnMoreClickListener

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attributeSet: AttributeSet?) : this(context, attributeSet, 0)
    constructor(context: Context, attributeSet: AttributeSet?, defStyleAttr: Int) : super(context, attributeSet, defStyleAttr) {
        init(context, attributeSet)
    }

    fun init(context: Context, attrs: AttributeSet?) {

        attrs.let {

            val typedArray = context.obtainStyledAttributes(attrs, R.styleable.CommonTitleBar)

            if (typedArray.hasValue(R.styleable.CommonTitleBar_title)) {
                mTitle = typedArray.getString(R.styleable.CommonTitleBar_title)!!
            }

            mIsShowMore = typedArray.getBoolean(R.styleable.CommonTitleBar_isShowMore, false)

            mImgRes = typedArray.getResourceId(R.styleable.CommonTitleBar_imgRes, R.drawable.selector_more_btn)

            typedArray.recycle()

        }

        initView(context)

    }


    fun initView(context: Context) {

        LayoutInflater.from(context).inflate(R.layout.include_common_title_bar, this, true)

        back_iv.setOnClickListener {

            if (context is Activity) {
                context.finish()
            }
        }

        setTitle(mTitle)

        isShowMoreBtn(mIsShowMore)

        more_iv.setImageResource(mImgRes)

        more_iv.setOnClickListener {
            mOnMoreClickListener.onMoreClick(more_iv)
        }

    }

    fun setTitle(title: String) {
        title.let {
            txt_title.text = it
        }

    }

    fun isShowMoreBtn(isShowMore: Boolean) {
        isShowMore.let {
            if (it) {
                more_iv.visibility = View.VISIBLE
            } else {
                more_iv.visibility = View.INVISIBLE
            }
        }
    }

}

interface OnMoreClickListener {

    fun onMoreClick(view: View)

}