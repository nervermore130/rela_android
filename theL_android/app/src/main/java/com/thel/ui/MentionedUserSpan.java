package com.thel.ui;

import android.graphics.Color;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

/**
 * 预发送的日志的提及用户的样式，没有点击事件
 */
public class MentionedUserSpan extends ClickableSpan {

    public MentionedUserSpan() {
        super();
    }

    @Override
    public void updateDrawState(TextPaint ds) {
        ds.setColor(Color.parseColor("#80B2B5"));
        ds.setUnderlineText(false); // 去掉下划线
    }

    @Override
    public void onClick(View widget) {
    }
}
