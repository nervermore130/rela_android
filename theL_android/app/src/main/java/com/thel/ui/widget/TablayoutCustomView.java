package com.thel.ui.widget;

import android.content.Context;
import com.google.android.material.tabs.TabLayout;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thel.R;


/**
 * Created by the L on 2016/9/5.
 */
public class TablayoutCustomView extends LinearLayout {
    private final Context mContext;
    public TextView mTextView;
    public View mUnderline;
    private TabLayout mTablayout;

    public TablayoutCustomView(Context context) {
        this(context, null);
    }

    private TablayoutCustomView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    private TablayoutCustomView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }

    private void init() {
        inflate(mContext, R.layout.tablayout_custom_view, this);
        mTextView = findViewById(R.id.txt);
        mUnderline = findViewById(R.id.underline);
        mUnderline.setVisibility(View.INVISIBLE);
    }

    public TablayoutCustomView bindTablayout(TabLayout tabLayout) {
        mTablayout = tabLayout;
        mTextView.setTextColor(tabLayout.getTabTextColors());
        return this;
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);
        if (selected) {
            mUnderline.setVisibility(View.VISIBLE);
        } else {
            mUnderline.setVisibility(View.INVISIBLE);
        }
    }
}
