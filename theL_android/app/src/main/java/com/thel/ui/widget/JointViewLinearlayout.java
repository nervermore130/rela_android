package com.thel.ui.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import com.thel.utils.L;

/**
 * Created by test1 on 2017/8/11.
 */

public class JointViewLinearlayout extends LinearLayout {
    private View jonitView;

    public JointViewLinearlayout(Context context) {
        this(context, null);
    }

    public JointViewLinearlayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public JointViewLinearlayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setWillNotDraw(false);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    public JointViewLinearlayout initView(View jonitview) {
        if (getOrientation() == VERTICAL) {
            return this;
        }
        if (jonitview == null) {
            return this;
        }
        this.jonitView = jonitview;
        return this;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        initJonitView();
        super.onDraw(canvas);
    }

    private void initJonitView() {
        final int count = getChildCount();
        if (jonitView != null) {
            final int mWidth = getMeasuredWidth();
            L.i("refresh", "mWidth=" + mWidth);
            int limitWidth = mWidth;
            for (int i = 0; i < count; i++) {
                View view = getChildAt(i);
                if (view != jonitView) {
                    measureChild(view, MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
                    final int width = view.getMeasuredWidth();
                    final LayoutParams param = (LayoutParams) view.getLayoutParams();
                    limitWidth = limitWidth - width - param.leftMargin - param.rightMargin;
                    L.i("refresh", "count=" + count + ",width=" + width + ",limitWidth=" + limitWidth);
                }
            }
            final LayoutParams param = (LayoutParams) jonitView.getLayoutParams();
            limitWidth = limitWidth - param.leftMargin - param.rightMargin - getPaddingLeft() - getPaddingRight();
            if (limitWidth < 0) {
                limitWidth = 0;
            }
            param.width = Math.min(limitWidth, jonitView.getMeasuredWidth());
            jonitView.setLayoutParams(param);
        }
    }
}
