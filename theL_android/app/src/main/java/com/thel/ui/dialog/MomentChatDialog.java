package com.thel.ui.dialog;

import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.bean.message.MomentToChatBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.constants.TheLConstants;
import com.thel.db.DBUtils;
import com.thel.manager.ChatServiceManager;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.main.messages.bean.MsgBean;
import com.thel.modules.main.messages.utils.MsgUtils;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;
import com.thel.utils.SharedPrefUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;

import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by chad
 * Time 18/4/27
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */
public class MomentChatDialog extends DialogFragment implements View.OnClickListener, TextWatcher {

    private static final String TAG = "MomentChatDialog";

    @BindView(R.id.other_portrait)
    ImageView avatar;

    @BindView(R.id.txt_other_name)
    TextView txt_other_name;

    @BindView(R.id.img_pic)
    ImageView img_pic;

    @BindView(R.id.img_play)
    ImageView img_play;

    @BindView(R.id.rel_pic)
    RelativeLayout rel_pic;

    @BindView(R.id.txt_content)
    TextView txt_content;

    @BindView(R.id.edit_content)
    EditText edit_content;

    @BindView(R.id.send)
    TextView send;

    @BindView(R.id.cancel)
    TextView cancel;

    private MomentsBean momentsBean;

    public static MomentChatDialog newInstance(Bundle bundle) {
        MomentChatDialog dialog = new MomentChatDialog();
        dialog.setArguments(bundle);
        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCancelable(true);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.dialogScanleAnim);
        if (getArguments() != null)
            momentsBean = (MomentsBean) getArguments().getSerializable(TheLConstants.BUNDLE_KEY_MOMENT_BEAN);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        //dialog背景透明
        Objects.requireNonNull(getDialog().getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return inflater.inflate(R.layout.moment_chat_dialog, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        send.setOnClickListener(this);
        cancel.setOnClickListener(this);
        edit_content.addTextChangedListener(this);
        initMomentBeanView();
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {

            @Override
            public void run() {
                ViewUtils.showSoftInput(getActivity(), edit_content);
            }
        }, 300);
    }

    @Override
    public void dismiss() {
        ViewUtils.hideSoftInput(getActivity(), edit_content);
        super.dismiss();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.send:
                recommendMoment();
                break;
            case R.id.cancel:
                this.dismiss();
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        if (s == null) {
            send.setTextColor(ContextCompat.getColor(getActivity(), R.color.moment_chat_cancel_text));
            send.setEnabled(false);
            return;
        }

        int textLength = s.length();

        if (textLength > 0) {
            send.setTextColor(ContextCompat.getColor(getActivity(), R.color.text_color_green));
            send.setEnabled(true);
        } else {
            send.setTextColor(ContextCompat.getColor(getActivity(), R.color.moment_chat_cancel_text));
            send.setEnabled(false);
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    /**
     * 推荐日志
     */
    private void initMomentBeanView() {

//        L.d("ToChat", " momentsBean : " + momentsBean);

        if (momentsBean == null) {
            return;
        }

        L.d("ToChat", " momentsBean.momentsType : " + momentsBean.momentsType);

        L.d("ToChat", " momentsBean.momentsText : " + momentsBean.momentsText);

        L.d("ToChat", " momentsBean.imageUrl : " + momentsBean.imageUrl);

        L.d("ToChat", " momentsBean.thumbnailUrl : " + momentsBean.thumbnailUrl);

        ImageLoaderManager.imageLoaderCircle(avatar, R.mipmap.btn_avatar, momentsBean.avatar);
        txt_other_name.setText(momentsBean.nickname);

        if (momentsBean.momentsText != null) {
            txt_content.setVisibility(View.VISIBLE);
            txt_content.setText(momentsBean.momentsText);
        }

        if (momentsBean.imageUrl != null && momentsBean.imageUrl.length() > 0) {
            img_pic.setVisibility(View.VISIBLE);
            String[] urls = momentsBean.imageUrl.split(",");
            ImageLoaderManager.imageLoader(img_pic, R.mipmap.bg_topic_default, urls[0]);
        }

        if (momentsBean.momentsType.equals(MomentsBean.MOMENT_TYPE_VIDEO) || (momentsBean.themeReplyClass != null && momentsBean.themeReplyClass.equals("video"))) {
            img_play.setVisibility(View.VISIBLE);
            ImageLoaderManager.imageLoader(img_play, R.mipmap.btn_play_video);
        } else if (momentsBean.momentsType.equals(MomentsBean.MOMENT_TYPE_LIVE)) {
            img_play.setVisibility(View.VISIBLE);
            ImageLoaderManager.imageLoader(img_play, R.mipmap.btn_live_play);
        } else {
            img_play.setVisibility(View.GONE);
        }

//        if (MomentsBean.MOMENT_TYPE_TEXT.equals(momentsBean.momentsType) || MomentsBean.MOMENT_TYPE_RECOMMEND.equals(momentsBean.momentsType)) {//纯文本（推荐日志也是纯文本日志）
//            txt_content.setText(momentsBean.momentsText);
//        } else if (MomentsBean.MOMENT_TYPE_IMAGE.equals(momentsBean.momentsType)) {//图片日志
//            setPicView(false);
//        } else if (MomentsBean.MOMENT_TYPE_TEXT_IMAGE.equals(momentsBean.momentsType)) {//带文本的图片日志
//            setPicView(true);
//        } else if (MomentsBean.MOMENT_TYPE_LIVE.equals(momentsBean.momentsType)) {//直播日志
//            setPicView(true);
//        } else if (MomentsBean.MOMENT_TYPE_THEME.equals(momentsBean.momentsType)) {//话题日志
//            setThemeView();
//        } else if (MomentsBean.MOMENT_TYPE_THEME_REPLY.equals(momentsBean.momentsType)) {//话题回复日志
//            if (TextUtils.isEmpty(getImageUrl(momentsBean))) {//如果不带图片
//                txt_content.setText(momentsBean.momentsText);
//            } else {
//                setPicView(!TextUtils.isEmpty(momentsBean.momentsText));
//            }
//        } else if (MomentsBean.MOMENT_TYPE_VIDEO.equals(momentsBean.momentsType)) {//视频日志
//            setPicView(!TextUtils.isEmpty(momentsBean.momentsText));
//            img_play.setVisibility(View.VISIBLE);
//        } else if (MomentsBean.MOMENT_TYPE_USER_CARD.equals(momentsBean.momentsType)) {//推荐用户名片
//            txt_content.setText(momentsBean.momentsText);
//            if (TextUtils.isEmpty(momentsBean.imageUrl)) {//如果被推荐名片没有背景
//                img_pic.setImageResource(R.color.tab_normal);
//            } else {
//                setImage(momentsBean.imageUrl, img_pic);
//            }
//        } else if (MomentsBean.MOMENT_TYPE_WEB.equals(momentsBean.momentsType)) {//如果是网页日志
//            try {
//                final JSONObject obj = new JSONObject(momentsBean.momentsText);
//                RecommendWebBean bean = new RecommendWebBean();
//                bean.fromJson(obj);
//                txt_content.setText(bean.momentsText);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//
//        } else if (MomentsBean.MOMENT_TYPE_USER_CARD.equals(momentsBean.momentsType)) {//如果是推荐主播
//            txt_content.setText(momentsBean.momentsText);
//        } else {//默认当普通文本类型
//            txt_content.setText(momentsBean.momentsText);
//        }
    }

    /**
     * 带图片的日志
     */
    private void setPicView(boolean hasText) {
        rel_pic.setVisibility(View.VISIBLE);
        setImage(getImageUrl(momentsBean), img_pic);
        if (hasText) {
            txt_content.setText(momentsBean.momentsText);
        } else {
            txt_content.setVisibility(View.GONE);
        }
    }

    /**
     * 话题
     */
    private void setThemeView() {
        rel_pic.setVisibility(View.VISIBLE);
        txt_content.setText(momentsBean.momentsText);
        if (TextUtils.isEmpty(momentsBean.imageUrl)) {//没背景图片的话题
            setImage(TheLConstants.RES_PIC_URL + R.mipmap.bg_topic_default, img_pic);
        } else {
            setImage(momentsBean.imageUrl, img_pic);
        }
    }

    /**
     * 获取第一张缩略图
     */
    private String getImageUrl(MomentsBean bean) {
        String[] picUrls = bean.thumbnailUrl.split(",");
        if (picUrls.length > 0) {
            return picUrls[0];
        }
        return "";
    }

    /**
     * 设置图片
     */
    private void setImage(String imageUrl, ImageView img_pic) {
        if (!TextUtils.isEmpty(imageUrl)) {
            ImageLoaderManager.imageLoader(img_pic, imageUrl);
        }
    }

    private void recommendMoment() {
        String releaseText = edit_content.getText().toString().trim();

        int isOnlyText = SharedPrefUtils.getInt(SharedPrefUtils.FILE_LIVE, SharedPrefUtils.KEY_ONLY_TEXT, 1);

        //1表示只能发送文本消息，0表示支持发送带日志的消息
        if (isOnlyText == 1) {

            MsgBean msgBean = MsgUtils.createMsgBean(MsgBean.MSG_TYPE_TEXT, releaseText, 1, momentsBean.userId + "", momentsBean.nickname, momentsBean.avatar);

            ChatServiceManager.getInstance().sendMsg(msgBean);

            ChatServiceManager.getInstance().insertNewMsg(msgBean, DBUtils.getChatTableName(msgBean), 1);

        } else {

            MsgBean msgBean = MsgUtils.createMsgBean(MsgBean.MSG_TYPE_MOMENT_TO_CHAT, getMomentToChat(releaseText), 1, momentsBean.userId + "", momentsBean.nickname, momentsBean.avatar);

            ChatServiceManager.getInstance().sendMsg(msgBean);

            ChatServiceManager.getInstance().insertNewMsg(msgBean, DBUtils.getChatTableName(msgBean), 1);

        }

        Utils.playSound(R.raw.sound_send);

        dismiss();

    }

    private String getMomentToChat(@NonNull String chatText) {

        MomentToChatBean momentToChatBean = new MomentToChatBean();

        if (momentsBean.imageUrl != null && momentsBean.imageUrl.length() > 0) {

            String[] imageUrls = momentsBean.imageUrl.split(",");

            momentToChatBean.imgUrl = imageUrls[0];

        }

        momentToChatBean.chatText = chatText;

        momentToChatBean.momentText = momentsBean.momentsText;

        momentToChatBean.momentId = momentsBean.momentsId;

        momentToChatBean.momentType = momentsBean.momentsType;

        return GsonUtils.createJsonString(momentToChatBean);

    }

}
