package com.thel.ui.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.bean.LivePermitBean;
import com.thel.bean.me.MyCircleActivity;
import com.thel.bean.me.MyCircleFriendListBean;
import com.thel.constants.TheLConstants;
import com.thel.modules.live.Certification.ZhimaCertificationActivity;
import com.thel.modules.main.home.moments.ReleaseLiveMomentActivity;
import com.thel.modules.main.home.moments.ReleaseMomentActivity;
import com.thel.modules.main.home.moments.ReleaseThemeMomentActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.utils.DialogUtil;
import com.thel.utils.PermissionUtil;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.thel.utils.ShareFileUtils.IS_RELEASE_MOMENT;

/**
 * Created by liuyun on 2017/9/19.
 */

public class ReleaseMomentDialog extends Dialog {

    private final Context context;

    private String tag;

    @BindView(R.id.album)
    LinearLayout album;

    @BindView(R.id.video)
    LinearLayout video;

    @BindView(R.id.text)
    LinearLayout text;

    @BindView(R.id.topic)
    LinearLayout topic;

    @BindView(R.id.live)
    LinearLayout live;

    @BindView(R.id.lover)
    LinearLayout lover;

    public ReleaseMomentDialog(@NonNull Context context) {
        this(context, R.style.CustomDialog);
    }

    public ReleaseMomentDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        this.context = context;

        init();
    }

    private void init() {
        View view;
        if (TheLApp.enablePubLive == 1) {//可以发直播，显示直播按钮
            view = LayoutInflater.from(context).inflate(R.layout.do_more_dialog, null);
        } else {
            view = LayoutInflater.from(context).inflate(R.layout.do_more_dialog_without_live, null);
        }
        ButterKnife.bind(this, view);
        setContentView(view);
        setCanceledOnTouchOutside(true);

    }

    @Override
    public void show() {

        super.show();
    }


    @OnClick(R.id.album)
    void gotoAlbum(View v) {
        if (UserUtils.isVerifyCell()) {
            ShareFileUtils.setBoolean(Utils.getMyUserId() + "_" + IS_RELEASE_MOMENT, true);

            ViewUtils.preventViewMultipleClick(v, 2000);
            PermissionUtil.requestStoragePermission((Activity) context, new PermissionUtil.PermissionCallback() {
                @Override
                public void granted() {
                    Intent intent = new Intent(TheLApp.getContext(), ReleaseMomentActivity.class);
                    intent.putExtra(TheLConstants.BUNDLE_KEY_RELEASE_TYPE, ReleaseMomentActivity.RELEASE_TYPE_PHOTOS);
                    if (!TextUtils.isEmpty(tag)) {
                        intent.putExtra(TheLConstants.BUNDLE_KEY_TOPIC_NAME, tag);
                    }
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    TheLApp.getContext().startActivity(intent);
                    ReleaseMomentDialog.this.dismiss();
                }

                @Override
                public void denied() {
                    ReleaseMomentDialog.this.dismiss();
                }

                @Override
                public void gotoSetting() {
                    ReleaseMomentDialog.this.dismiss();
                }
            });
        } else
            ReleaseMomentDialog.this.dismiss();
    }

    @OnClick(R.id.video)
    void gotoVideo(final View v) {
        if (UserUtils.isVerifyCell()) {
            ShareFileUtils.setBoolean(Utils.getMyUserId() + "_" + IS_RELEASE_MOMENT, true);

            ViewUtils.preventViewMultipleClick(v, 2000);
            Intent intent = new Intent(TheLApp.getContext(), ReleaseMomentActivity.class);
            if (!TextUtils.isEmpty(tag)) {
                intent.putExtra(TheLConstants.BUNDLE_KEY_TOPIC_NAME, tag);
            }
            intent.putExtra(TheLConstants.BUNDLE_KEY_RELEASE_TYPE, ReleaseMomentActivity.RELEASE_TYPE_VIDEO);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            TheLApp.getContext().startActivity(intent);
            dismiss();
        } else
            ReleaseMomentDialog.this.dismiss();
    }

    @OnClick(R.id.text)
    void gotoText(View v) {
        if (UserUtils.isVerifyCell()) {
            ShareFileUtils.setBoolean(Utils.getMyUserId() + "_" + IS_RELEASE_MOMENT, true);

            ViewUtils.preventViewMultipleClick(v, 2000);
            Intent intent = new Intent(TheLApp.getContext(), ReleaseMomentActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            if (!TextUtils.isEmpty(tag)) {
                intent.putExtra(TheLConstants.BUNDLE_KEY_TOPIC_NAME, tag);
            }
            intent.putExtra(TheLConstants.BUNDLE_KEY_RELEASE_TYPE, ReleaseMomentActivity.RELEASE_TYPE_TEXT);
            TheLApp.getContext().startActivity(intent);
            dismiss();
        } else
            ReleaseMomentDialog.this.dismiss();
    }

    @OnClick(R.id.topic)
    void gotoTopic(View v) {
        if (UserUtils.isVerifyCell()) {
            ShareFileUtils.setBoolean(Utils.getMyUserId() + "_" + IS_RELEASE_MOMENT, true);

            ViewUtils.preventViewMultipleClick(v, 2000);
            Intent intent = new Intent(TheLApp.getContext(), ReleaseThemeMomentActivity.class);
            if (!TextUtils.isEmpty(tag)) {
                intent.putExtra(TheLConstants.BUNDLE_KEY_TOPIC_NAME, tag);
            }
            intent.putExtra(TheLConstants.BUNDLE_KEY_RELEASE_TYPE, ReleaseThemeMomentActivity.RELEASE_TYPE_TOPIC);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            TheLApp.getContext().startActivity(intent);
            dismiss();
        } else
            ReleaseMomentDialog.this.dismiss();
    }

    @OnClick(R.id.live)
    void gotoLive(final View v) {
        if (UserUtils.isVerifyCell()) {
            ShareFileUtils.setBoolean(Utils.getMyUserId() + "_" + IS_RELEASE_MOMENT, true);

            ViewUtils.preventViewMultipleClick(v, 2000);

            if (ShareFileUtils.getInt(TheLConstants.live_permit, 0) == 1) {
                PermissionUtil.requestCameraPermission((Activity) context, new PermissionUtil.PermissionCallback() {
                    @Override
                    public void granted() {
                        Intent intent = new Intent(TheLApp.getContext(), ReleaseLiveMomentActivity.class);
                        if (!TextUtils.isEmpty(tag)) {
                            intent.putExtra(TheLConstants.BUNDLE_KEY_TOPIC_NAME, tag);
                        }
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        TheLApp.getContext().startActivity(intent);
                        dismiss();
                    }

                    @Override
                    public void denied() {

                    }

                    @Override
                    public void gotoSetting() {

                    }
                });
            } else {
                RequestBusiness.getInstance().getLivePermit().onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<LivePermitBean>() {
                    @Override
                    public void onNext(LivePermitBean livePermitBean) {
                        super.onNext(livePermitBean);
                        if (livePermitBean != null && livePermitBean.data != null) {
                            ShareFileUtils.setInt(TheLConstants.live_permit, livePermitBean.data.perm);
                            if (livePermitBean.data.perm == 1) {
                                Intent intent = new Intent(TheLApp.getContext(), ReleaseLiveMomentActivity.class);
                                if (!TextUtils.isEmpty(tag)) {
                                    intent.putExtra(TheLConstants.BUNDLE_KEY_TOPIC_NAME, tag);
                                }
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                TheLApp.getContext().startActivity(intent);
                                ReleaseMomentDialog.this.dismiss();
                            } else {
                                DialogUtil.showConfirmDialog(getContext(), "", TheLApp.getContext().getString(R.string.no_live_permition_tip), TheLApp.getContext().getString(R.string.info_continue), TheLApp.getContext().getString(R.string.info_no), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        ReleaseMomentDialog.this.dismiss();
                                        Intent intent = new Intent(TheLApp.getContext(), ZhimaCertificationActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        TheLApp.getContext().startActivity(intent);

                                    }
                                });
                            }
                        }
                    }
                });
            }
        } else
            ReleaseMomentDialog.this.dismiss();
    }

    @OnClick(R.id.lover)
    void gotoLover(View v) {
        PermissionUtil.requestStoragePermission((Activity) context, new PermissionUtil.PermissionCallback() {
            @Override
            public void granted() {
                if (UserUtils.isVerifyCell()) {
                    ShareFileUtils.setBoolean(Utils.getMyUserId() + "_" + IS_RELEASE_MOMENT, true);


                    ViewUtils.preventViewMultipleClick(v, 2000);

                    ((BaseActivity) context).showLoading();
                    RequestBusiness.getInstance().getMyCircleData().onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<MyCircleFriendListBean>() {
                        @Override
                        public void onNext(MyCircleFriendListBean myCircelBean) {
                            super.onNext(myCircelBean);
                            ((BaseActivity) context).closeLoading();
                            if (myCircelBean != null && myCircelBean.data != null && myCircelBean.data.paternerd != null && 1 == myCircelBean.data.paternerd.requestStatus) {//有情侣
                                Intent intent = new Intent(TheLApp.getContext(), ReleaseMomentActivity.class);
                                if (!TextUtils.isEmpty(tag)) {
                                    intent.putExtra(TheLConstants.BUNDLE_KEY_TOPIC_NAME, tag);
                                }
                                intent.putExtra(TheLConstants.BUNDLE_KEY_RELEASE_TYPE, ReleaseMomentActivity.RELEASE_TYPE_LOVE);
                                Bundle bundle = new Bundle();
                                bundle.putSerializable("partner", myCircelBean.data.paternerd);
                                intent.putExtras(bundle);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                TheLApp.getContext().startActivity(intent);
                                dismiss();
                            } else {
                                DialogUtil.showConfirmDialog(getContext(), "", TheLApp.getContext().getString(R.string.have_no_lover), TheLApp.getContext().getString(R.string.bind_lover), TheLApp.getContext().getString(R.string.info_no), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent intent = new Intent(TheLApp.getContext(), MyCircleActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        TheLApp.getContext().startActivity(intent);
                                        dialog.cancel();
                                        ReleaseMomentDialog.this.dismiss();
                                    }
                                });
                            }
                        }
                    });
                } else
                    ReleaseMomentDialog.this.dismiss();
            }

            @Override
            public void denied() {

            }

            @Override
            public void gotoSetting() {

            }
        });
    }

}
