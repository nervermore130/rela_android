package com.thel.ui.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;
import android.view.Gravity;

import com.thel.R;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by the L on 2016/5/30.
 */
public class TimeTextView extends androidx.appcompat.widget.AppCompatTextView {
    private int delayed;//延迟时间
    private int period;//period
    Timer timer;//计时器
    int count;//计数
    boolean isCombo = false;//是否在连击
    String content = "";//text正常显示的内容
    private final int SHOW_COUNT = 1;
    private final int SHOW_CONTENT = 2;
    MyTimeTask task;
    private int totalcount;//总共要求倒计时总数

    public TimeTextView(Context context) {
        this(context, null);

    }

    public TimeTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TimeTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initText();
    }

    private void initText() {
        setGravity(Gravity.CENTER);
        showContent();
    }

    public TimeTextView getInstance(int count, int delayed, int period) {
        this.totalcount = count;
        this.delayed = delayed;
        this.period = period;
        return this;
    }

    public void count() {
        count = totalcount;
        showCount(count);
        if (task != null) {
            task.cancel();
        }
        task = new MyTimeTask();
        timer = new Timer();
        timer.scheduleAtFixedRate(task, delayed, period);
    }

    /**
     * 显示倒计时时间
     *
     * @param count
     */
    private void showCount(int count) {
        setTextColor(ContextCompat.getColor(getContext(), R.color.red));
        setText(count + "");
        isCombo = true;
    }

    /**
     * 设置正常显示内容
     *
     * @param content
     */
    public void setContent(String content) {
        this.content = content;
        showContent();
    }

    /**
     * 显示正常内容
     */
    public void showContent() {//显示正常内容，同时连击为false
        if (task != null) {
            task.cancel();
            task = null;
        }
        handler.removeMessages(SHOW_COUNT);
        count = totalcount;
        isCombo = false;
        setTextColor(ContextCompat.getColor(getContext(), R.color.text_color_gray));
        setText(content);
    }

    /**
     * 是否在连击
     *
     * @return
     */
    public boolean isCombo() {
        return isCombo;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @SuppressLint("HandlerLeak") Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case SHOW_COUNT:
                    showCount(count);//显示倒计时
                    break;
                case SHOW_CONTENT://显示正常内容
                    showContent();
                    break;
                default:
                    break;
            }
        }
    };

    class MyTimeTask extends TimerTask {

        @Override
        public void run() {
            if (count > 0) {
                count--;
                handler.sendEmptyMessage(SHOW_COUNT);
            }
            if (count <= 0) {
                handler.sendEmptyMessage(SHOW_CONTENT);
                cancel();
            }
        }
    }

    @Override protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        if (timer != null) {
            timer.cancel();
            timer.purge();
        }

        if (task != null) {
            task.cancel();
            task = null;
        }

    }
}
