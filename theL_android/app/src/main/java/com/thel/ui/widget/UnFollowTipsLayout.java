package com.thel.ui.widget;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.thel.R;
import com.thel.modules.others.VipConfigActivity;
import com.thel.utils.ShareFileUtils;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class UnFollowTipsLayout extends RelativeLayout {


    public UnFollowTipsLayout(Context context) {
        super(context);
        init();
    }

    public UnFollowTipsLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public UnFollowTipsLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {

        View view = LayoutInflater.from(getContext()).inflate(R.layout.layout_un_follow_tips, this, true);

        view.setVisibility(GONE);

        ButterKnife.bind(this, view);

    }

    @OnClick(R.id.close_iv)
    void onCloseClick() {
        this.setVisibility(GONE);
        ShareFileUtils.setBoolean(ShareFileUtils.CHAT_UN_FOLLOW_TIPS, true);
    }

    @OnClick(R.id.setting_tv)
    void onSettingClick() {
        Intent intent = new Intent(getContext(), VipConfigActivity.class);
        intent.putExtra("showType", VipConfigActivity.SHOW_UN_FOLLOW);
        getContext().startActivity(intent);
    }

}
