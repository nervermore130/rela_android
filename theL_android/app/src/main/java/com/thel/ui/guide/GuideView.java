package com.thel.ui.guide;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.Typeface;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;

import java.util.Arrays;

/**
 * Created by waiarl on 2018/4/10.
 */

public class GuideView extends View {
    private final Context mContext;
    private GuideViewBuilder builder = new GuideViewBuilder();
    private Paint mPaint;
    private int mWidth;
    private int mHeight;
    private TextPaint mTextPaint;


    public String[] contents;
    public int guideRes;
    public RectF targetRect = new RectF();
    public int contentColor = Color.WHITE;
    public int contentSize = 12;
    public Point contentBackgroundColor = new Point();
    public int backgroundColor;
    public PointF offsetPoint = new PointF();
    public int gravity = Gravity.TOP;
    public int contentsGravity = Gravity.CENTER;
    public RectF contentsMarginRect = new RectF();
    public int suggestContentBgWidth;
    public RectF contentBackgroundMargin = new RectF();
    public PointF suggestTriangleSize = new PointF();
    public Typeface contentStyle = Typeface.create(Typeface.SANS_SERIF, Typeface.NORMAL);
    public int contentBackgroundGravity = Gravity.CENTER;
    public int contentDivider = 5;
    private float bgRadius;

    public GuideView(Context context) {
        this(context, null);
    }

    public GuideView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public GuideView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }

    private void init() {
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mTextPaint = new TextPaint();
        mTextPaint.setAntiAlias(true);
    }

    public GuideView initView(@NonNull GuideViewBuilder builder) {
        this.builder = builder;
        build();
        return this;
    }

    private void initView() {
        contents = builder.contents;
        guideRes = builder.guideRes;
        targetRect = builder.targetRect;
        contentColor = builder.contentColor;
        contentSize = builder.contentSize;
        contentBackgroundColor = builder.contentBackgroundColor;
        backgroundColor = builder.backgroundColor;
        offsetPoint = builder.offsetPoint;
        gravity = builder.gravity;
        contentsGravity = builder.contentsGravity;
        contentsMarginRect = builder.contentsMarginRect;
        suggestContentBgWidth = builder.suggestContentBgWidth;
        contentBackgroundMargin = builder.contentBackgroundMargin;
        suggestTriangleSize = builder.suggestTriangleSize;
        contentBackgroundGravity = builder.contentBackgroundGravity;
        contentDivider = builder.contentDivider;
        bgRadius = builder.bgRadius;
    }

    public GuideViewBuilder getBuilder() {
        return builder;
    }

    public GuideView build() {
        initView();
        invalidate();
        return this;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (targetRect == null || targetRect.isEmpty()) {
            return;
        }
        mWidth = getMeasuredWidth();
        mHeight = getMeasuredHeight();
        saveLayer(canvas);
        drawBackground(canvas);
        drawTextAndBg(canvas);
    }


    private void drawBackground(Canvas canvas) {
        canvas.drawColor(builder.backgroundColor);
    }

    private void saveLayer(Canvas canvas) {
        canvas.saveLayer(0, 0, mWidth, mHeight, null, Canvas.ALL_SAVE_FLAG);
    }

    private void drawBg(Canvas canvas, RectF bgRect) {
        setLayerType(View.LAYER_TYPE_SOFTWARE, mPaint);
        /***由于ondraw不只执行一次，所以一些状态要清空***/
        clearPaint();
        Path path = new Path();
        path.addRoundRect(bgRect, bgRadius, bgRadius, Path.Direction.CW);
        final Path trianglePath = getTrianglePath();
        path.addPath(trianglePath);

        mPaint.setShadowLayer(10, 1f, 1f, Color.GRAY);
        mPaint.setColor(Color.GRAY);
        canvas.drawPath(path, mPaint);

        clearPaint();
        final LinearGradient gradient = new LinearGradient(bgRect.left, bgRect.top, bgRect.right, bgRect.top, contentBackgroundColor.x, contentBackgroundColor.y, Shader.TileMode.CLAMP);
        mPaint.setShader(gradient);
        canvas.drawPath(path, mPaint);

       /* clearPaint();
        mPaint.setStyle(Paint.Style.STROKE);
        canvas.drawRect(targetRect, mPaint);*/
    }

    private RectF drawTextAndBg(Canvas canvas) {
        if (contents == null || contents.length == 0 || contents.length == 1 && TextUtils.isEmpty(contents[0])) {
            return null;
        }
        float tbWidth;
        if (suggestContentBgWidth > 0) {
            tbWidth = Math.min(suggestContentBgWidth - getHorizeontalMargin(contentsMarginRect), mWidth);
        } else {
            tbWidth = mWidth - getHorizeontalMargin(contentBackgroundMargin);
        }
        final float maxTextWidth = tbWidth;
        if (maxTextWidth <= 0) {
            return null;
        }
        final RectF bgRect = new RectF();
        mTextPaint.setColor(contentColor);
        mTextPaint.setXfermode(null);
        mTextPaint.setTextSize(contentSize);
        mTextPaint.setTypeface(contentStyle);
        mTextPaint.setSubpixelText(true);

        final Paint.FontMetricsInt fm = mTextPaint.getFontMetricsInt();
        final int baseLineH = 0 - fm.top;//基线基于起点的高度

        final int length = contents.length;
        final Rect[] boundRect = new Rect[length];
        final int[] rectWidths = new int[length];
        final int[] heights = new int[length];

        for (int i = 0; i < length; i++) {
            boundRect[i] = new Rect();
        }
        for (int i = 0; i < length; i++) {
            final String txt = contents[i];
            mTextPaint.getTextBounds(txt, 0, txt.length(), boundRect[i]);
            rectWidths[i] = boundRect[i].width();
            heights[i] = boundRect[i].height();
        }
        final float baseTopOffset = boundRect[0].top;
        final float yOffset = Math.abs(fm.ascent - baseTopOffset);
        //文本总高度（纯文本）
        int totalTextHeight = getTotalHeight(heights);

        final int maxWidth = getMaxWidth(rectWidths);
        if (maxWidth > maxTextWidth) {//需要换行，无视内容重力，文本总高度重新计算
            final float bgStartX = getNoGravityBgStartX();
            float startX = bgStartX + contentsMarginRect.left;
            float bgStartY = 0;
            float startY = 0;
            if (gravity == Gravity.BOTTOM) {//如果内容局域目标下方
                bgStartY = getGravityBottomBgStartY();
                startY = bgStartY + contentsMarginRect.top;
            } else {//如果是内容上方，需要测量总体textview高度
                for (int i = 0; i < length; i++) {
                    final StaticLayout myStaticLayout = new StaticLayout(contents[i], mTextPaint, (int) maxTextWidth, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
                    final int height = myStaticLayout.getHeight();
                    heights[i] = height;
                }
                totalTextHeight = getTotalHeight(heights);//总体高度（仅限于文本呢）文本总高度重新计算
                startY = targetRect.top - offsetPoint.y - suggestTriangleSize.y - contentsMarginRect.bottom - totalTextHeight - contentDivider * (length - 1);
                bgStartY = startY - contentsMarginRect.top;
            }

            bgRect.left = bgStartX;
            bgRect.top = bgStartY;
            bgRect.right = bgRect.left + maxTextWidth + getHorizeontalMargin(contentsMarginRect);
            bgRect.bottom = bgRect.top + totalTextHeight + contentDivider * (length - 1) + getVerticalMargin(contentsMarginRect);

            drawBg(canvas, bgRect);

            final float baseStartY = startY;//- baseLineH;//加上基线高度后开始的绘制文本左上角的y值才为真正需要的y值
            drawContent(canvas, mTextPaint, maxTextWidth, startX, baseStartY, heights, yOffset);

        } else {//不需要换行,此时 y起点坐标可以确定，然而背景位置因为可能没有设置具体宽度或者别的属性而无法确定，需要一种情况一种情况考虑
            float bgStartX = 0;
            float startX = 0;
            float bgStartY = 0;
            float startY = 0;

            float bgWidth = getbgWidth(maxWidth);
            bgStartX = getBgStartX(bgWidth);
            startX = getStartXByGravity(bgStartX, bgWidth, maxWidth);
            if (gravity == Gravity.BOTTOM) {//如果内容局域目标下方
                bgStartY = getGravityBottomBgStartY();
                startY = bgStartY + contentsMarginRect.top;
            } else {
                totalTextHeight = getTotalHeight(heights);//无需换行的总体高度（仅限于文本呢）
                startY = targetRect.top - offsetPoint.y - suggestTriangleSize.y - contentsMarginRect.bottom - totalTextHeight - contentDivider * (length - 1);
                bgStartY = startY - contentsMarginRect.top;
            }


            bgRect.left = bgStartX;
            bgRect.top = bgStartY;
            bgRect.right = bgRect.left + bgWidth;
            bgRect.bottom = bgRect.top + totalTextHeight + contentDivider * (length - 1) + getVerticalMargin(contentsMarginRect);

            drawBg(canvas, bgRect);

            final float baseStartY = startY;//- baseLineH;//加上基线高度后开始的绘制文本左上角的y值才为真正需要的y值,使用StaticLayout无视基线
            drawContent(canvas, mTextPaint, maxTextWidth, startX, baseStartY, heights, yOffset);
        }
        return bgRect;
    }

    private void clearPaint() {
        mPaint.clearShadowLayer();
        mPaint.setShader(null);
        mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
    }

    private Path getTrianglePath() {
        final Path path = new Path();
        float lx;
        float ly;
        float cx;
        float cy;
        float rx;
        float ry;
        if (gravity == Gravity.BOTTOM) {
            lx = targetRect.centerX() - suggestTriangleSize.x / 2 + offsetPoint.x;
            ly = targetRect.bottom + suggestTriangleSize.y + offsetPoint.y;

            cx = lx + suggestTriangleSize.x / 2;
            cy = ly - suggestTriangleSize.y;

            rx = lx + suggestTriangleSize.x;
            ry = ly;
        } else {//重复一部分，为了代码清晰决定分开写
            lx = targetRect.centerX() - suggestTriangleSize.x / 2 + offsetPoint.x;
            ly = targetRect.top - suggestTriangleSize.y - offsetPoint.y;

            cx = lx + suggestTriangleSize.x / 2;
            cy = ly + suggestTriangleSize.y;

            rx = lx + suggestTriangleSize.x;
            ry = ly;
        }

        path.moveTo(lx, ly);
        path.lineTo(cx, cy);
        path.lineTo(rx, ry);

        return path;
    }

    private float getGravityBottomBgStartY() {
        return targetRect.bottom + offsetPoint.y + suggestTriangleSize.y;
    }

    private float getStartXByGravity(float bgStartX, float bgWidth, int maxWidth) {
        switch (contentsGravity) {
            case Gravity.LEFT:
                return bgStartX + contentsMarginRect.left;
            case Gravity.RIGHT:
                return bgStartX + bgWidth - contentsMarginRect.right - maxWidth;
            default:
                return bgStartX + (bgWidth - maxWidth) / 2;
        }
    }

    private float getBgStartX(float bgWidth) {
        switch (contentBackgroundGravity) {
            case Gravity.LEFT:
                return contentBackgroundMargin.left;
            case Gravity.RIGHT:
                return mWidth - contentsMarginRect.right - bgWidth;
            default://默认视为居中
                return getGravityCenterBgStartX(bgWidth);
        }
    }

    private float getbgWidth(float maxWidth) {
        if (suggestContentBgWidth <= 0 && IsEmptyRect(contentBackgroundMargin)) {//既没有设置背景宽度也没有设置背景边距，视为正好包裹,奶奶的都不想处理这种情况
            return maxWidth + getHorizeontalMargin(contentsMarginRect);
        } else if (suggestContentBgWidth <= 0 && !IsEmptyRect(contentBackgroundMargin)) {//只设置了外边距
            return mWidth - getHorizeontalMargin(contentsMarginRect);
        } else {
            return suggestContentBgWidth;
        }
    }

    /**
     * 使用 staticlayout的时候，绘画的起点不是从基线，而是从ascent，假设以y轴0点为基线绘制，
     * 那么实际上文字的最定点为getboundRect中的rect的 top，假设为-29，ascent为-33，基线距离top值为-39
     * 所以要让文字的最顶端为0，在使用staticlayout的时候，起点应为 ascent-rect.top(); 即为-4；
     */
    private void drawContent(Canvas canvas, TextPaint mTextPaint, float maxTextWidth, float startX, float baseStartY, int[] heights, float yOffset) {
        final int length = contents.length;
        baseStartY -= yOffset;
        for (int i = 0; i < length; i++) {
            canvas.save();
            final int x = (int) startX;
            final int y = getYByIndex(i, baseStartY, heights);
            final StaticLayout staticLayout = new StaticLayout(contents[i], mTextPaint, (int) maxTextWidth, Layout.Alignment.ALIGN_NORMAL, 1f, 0f, false);
            canvas.translate(x, y);
            staticLayout.draw(canvas);
            canvas.restore();
        }
    }

    private int getYByIndex(int index, float startY, int[] heights) {
        float y = startY;
        if (heights == null) {
            return (int) y;
        }
        int i = 1;
        final int length = heights.length;
        while (i <= index && i < length) {
            y += heights[i - 1] + contentDivider;
            i += 1;
        }
        return (int) y;
    }

    private int getTotalHeight(int[] heights) {
        int height = 0;
        if (heights == null) {
            return height;
        }
        final int length = heights.length;
        for (int i = 0; i < length; i++) {
            height += heights[i];
        }
        return height;
    }


    /**
     * 内容需要换行的时候，背景的起点x
     *
     * @return
     */
    private float getNoGravityBgStartX() {
        if (suggestContentBgWidth <= 0) {//无设置具体宽度，则为左边距
            return contentBackgroundMargin.left;
            //设置了具体宽度但是具体宽度加上边距总宽度大于 view总宽度，则无视边距，七点为总view宽度减去背景宽度差的一半
        } else if (suggestContentBgWidth + contentBackgroundMargin.width() >= mWidth && suggestContentBgWidth < mWidth) {
            return (mWidth - suggestContentBgWidth) / 2;
        } else if (suggestContentBgWidth < mWidth) {//具体背景宽度加上边距宽度依旧小于view宽度的时候，根据重力设置位置
            if (contentBackgroundGravity == Gravity.LEFT) {//左重力，返回左边距
                return contentBackgroundMargin.left;
            } else if (contentBackgroundGravity == Gravity.RIGHT) {//右重力，返回总宽度减去设置宽度再减去右边距
                return mWidth - suggestContentBgWidth - contentBackgroundMargin.right;
            } else {//否则视为居中，居中有两种情况，一种是相对view居中，一种是相对target居中，
                return getGravityCenterBgStartX(suggestContentBgWidth);
            }
        }
        return 0;
    }

    public float getGravityCenterBgStartX(float bgWidth) {
        if (Math.min(targetRect.left, mWidth - targetRect.right) * 2 >= bgWidth) {//此时，可以相对target居中
            return targetRect.centerX() - bgWidth / 2;
        }
        return (mWidth - bgWidth) / 2;
    }

    private int getMaxWidth(int[] rectWidths) {
        if (rectWidths == null) {
            return 0;
        }
        Arrays.sort(rectWidths);
        return rectWidths[rectWidths.length - 1];
    }

    private float getHorizeontalMargin(@NonNull RectF rectF) {
        return rectF.left + rectF.right;
    }

    private float getVerticalMargin(@NonNull RectF rectF) {
        return rectF.top + rectF.bottom;
    }

    private boolean IsEmptyRect(@NonNull RectF rectF) {
        return rectF.left == 0 && rectF.top == 0 && rectF.right == 0 && rectF.bottom == 0;
    }
/*
    *//**************************************************内部类*******************************************************************//*

    public static class Builder {
        public String[] contents;
        public int guideRes;
        public RectF targetRect = new RectF();
        public int contentColor = Color.WHITE;
        public int contentSize = 12;
        public Point contentBackgroundColor = new Point();
        public int backgroundColor;
        public PointF offsetPoint = new PointF();
        public int gravity = Gravity.TOP;
        public int contentsGravity = Gravity.CENTER;
        public float bgRadius = 5;
        public RectF contentsMarginRect = new RectF(bgRadius, bgRadius, bgRadius, bgRadius);
        public int suggestContentBgWidth;
        public RectF contentBackgroundMargin = new RectF();
        public PointF suggestTriangleSize = new PointF();
        public Typeface contentStyle = Typeface.create(Typeface.SANS_SERIF, Typeface.NORMAL);
        public int contentBackgroundGravity = Gravity.CENTER;
        public int contentDivider = 5;

        public Builder setContents(String[] contents) {
            this.contents = contents;
            return this;
        }

        public Builder setGuideRes(int guideRes) {
            this.guideRes = guideRes;
            return this;

        }

        public Builder setTargetRect(@NonNull RectF targetRect) {
            this.targetRect.set(targetRect);
            return this;

        }

        public Builder setContentColor(int contentColor) {
            this.contentColor = contentColor;
            return this;
        }

        public Builder setContentSize(int contentSize) {
            this.contentSize = contentSize;
            return this;
        }

        public Builder setContentBackgroundColor(@NonNull Point contentBackgroundColor) {
            this.contentBackgroundColor.set(contentBackgroundColor.x, contentBackgroundColor.y);
            return this;
        }

        public Builder setContentBackgroundColor(int color) {
            this.contentBackgroundColor.set(color, color);
            return this;
        }

        public Builder setBackgroundColor(int backgroundColor) {
            this.backgroundColor = backgroundColor;
            return this;

        }

        public Builder setOffsetPoint(@NonNull PointF offsetPoint) {
            this.offsetPoint.set(offsetPoint.x, offsetPoint.y);
            return this;

        }

        public Builder setGravity(int gravity) {
            this.gravity = gravity;
            return this;

        }

        public Builder setContentsGravity(int contentsGravity) {
            this.contentsGravity = contentsGravity;
            return this;
        }

        public Builder setContentsMarginRect(@NonNull RectF contentsMarginRect) {
            this.contentsMarginRect.set(contentsMarginRect);
            return this;
        }


        public Builder setSuggestContentBgWidth(int suggestContentBgWidth) {
            this.suggestContentBgWidth = suggestContentBgWidth;
            return this;
        }

        public Builder setContentBackgroundMargin(@NonNull RectF contentBackgroundMargin) {
            this.contentBackgroundMargin.set(contentBackgroundMargin);
            return this;
        }

        public Point getTriiangleBgColor(float startRadio, float endRadio) {
            return new Point(getGradientColor(startRadio), getGradientColor(endRadio));
        }

        public Builder setSuggestTriangleSize(PointF suggestTriangleSize) {
            this.suggestTriangleSize = suggestTriangleSize;
            return this;
        }

        public Builder setContentBackgroundGravity(int contentBackgroundGravity) {
            this.contentBackgroundGravity = contentBackgroundGravity;
            return this;
        }

        public Builder setContentDivider(int contentDivider) {
            this.contentDivider = contentDivider;
            return this;
        }

        public Builder setBgRadius(float bgRadius) {
            this.bgRadius = bgRadius;
            return this;
        }

        public Builder setContentStyle(Typeface contentStyle) {
            this.contentStyle = contentStyle;
            return this;
        }

        public int getGradientColor(float radio) {
            final int mStartColor = contentBackgroundColor.x;
            final int mEndColor = contentBackgroundColor.y;
            int redStart = Color.red(mStartColor);
            int blueStart = Color.blue(mStartColor);
            int greenStart = Color.green(mStartColor);
            int redEnd = Color.red(mEndColor);
            int blueEnd = Color.blue(mEndColor);
            int greenEnd = Color.green(mEndColor);

            int red = (int) (redStart + ((redEnd - redStart) * radio + 0.5));
            int greed = (int) (greenStart + ((greenEnd - greenStart) * radio + 0.5));
            int blue = (int) (blueStart + ((blueEnd - blueStart) * radio + 0.5));
            return Color.argb(255, red, greed, blue);
        }

    }*/

    /**************************************************非绘制方法*******************************************************************/


}
