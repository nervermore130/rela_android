package com.thel.ui.widget;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.thel.R;
import com.thel.bean.LiveAdInfoBean;
import com.thel.constants.BundleConstants;
import com.thel.modules.main.home.moments.web.WebViewActivity;
import com.thel.utils.SizeUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LiveAdLayout extends RelativeLayout {

    @BindView(R.id.ad_view) SimpleDraweeView ad_view;

    @BindView(R.id.top_tv) TextView top_tv;

    @BindView(R.id.offset_tv) TextView offset_tv;

    @BindView(R.id.ad_content) FrameLayout ad_content;

    @BindView(R.id.root_rl) RelativeLayout root_rl;

    // 1左边 2右边
    private int adSite = 1;

    //1下边 2上边
    private int tipsSite = 1;

    public LiveAdLayout(Context context) {
        super(context);
        initView(context, null);
    }

    public LiveAdLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context, attrs);
    }

    public LiveAdLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }


    private void initView(Context context, AttributeSet attrs) {

        if (attrs != null && context != null) {

            TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.LiveAdLayout);

            adSite = typedArray.getInteger(R.styleable.LiveAdLayout_ad_site, 1);

            tipsSite = typedArray.getInteger(R.styleable.LiveAdLayout_tips_site, 1);

            typedArray.recycle();

        }

        View view = LayoutInflater.from(context).inflate(R.layout.layout_live_ad, this, true);

        ButterKnife.bind(this, view);

    }

    public void setData(LiveAdInfoBean liveAdInfoBean) {

        updateUI(liveAdInfoBean);

        setLayout();
    }

    private void updateUI(LiveAdInfoBean liveAdInfoBean) {
        if (liveAdInfoBean != null) {

            setVisibility(View.VISIBLE);

            root_rl.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override public void onGlobalLayout() {

                    int width = 0;

                    if (liveAdInfoBean.offset != null && liveAdInfoBean.offset.length() > 0) {
                        offset_tv.setVisibility(View.VISIBLE);
                        offset_tv.setText(liveAdInfoBean.offset);
                        int startX;

                        int endX;

                        if (tipsSite == 2) {
                            startX = offset_tv.getWidth() + offset_tv.getPaddingLeft() + offset_tv.getPaddingRight();
                            endX = 0;
                        } else {
                            startX = -offset_tv.getWidth() - offset_tv.getPaddingLeft() - offset_tv.getPaddingRight();
                            endX = offset_tv.getPaddingLeft() + offset_tv.getPaddingRight();
                        }

                        width = offset_tv.getWidth() + offset_tv.getPaddingLeft() + offset_tv.getPaddingRight();

                        RelativeLayout.LayoutParams params = (LayoutParams) getLayoutParams();

                        params.width = width;

                        setLayoutParams(params);

                        startAnimator(startX, endX);

                    }

                    if (width == 0) {
                        width = SizeUtils.dip2px(getContext(), 67.5f);
                    }

                    RelativeLayout.LayoutParams rootParams = (LayoutParams) root_rl.getLayoutParams();

                    rootParams.width = width;

                    root_rl.setLayoutParams(rootParams);

                    root_rl.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                }
            });

            if (liveAdInfoBean.icon != null) {

                Uri uri = Uri.parse(liveAdInfoBean.icon);
                DraweeController controller = Fresco.newDraweeControllerBuilder().setUri(uri).setAutoPlayAnimations(true).build();
                ad_view.setController(controller);

            }

            if (liveAdInfoBean.rank != null && liveAdInfoBean.rank.length() > 0) {
                String title = "NO." + liveAdInfoBean.rank;
                top_tv.setText(title);
            }


            setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (liveAdInfoBean.url != null) {
                        Intent intent = new Intent(v.getContext(), WebViewActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(BundleConstants.URL, liveAdInfoBean.url);
                        intent.putExtras(bundle);
                        v.getContext().startActivity(intent);
                    }

                }
            });
        } else {
            setVisibility(GONE);
        }
    }

    private void setLayout() {

        if (adSite == 2) {

            LayoutParams layoutParams = (LayoutParams) ad_content.getLayoutParams();

            layoutParams.addRule(ALIGN_PARENT_RIGHT);

            layoutParams.bottomMargin = SizeUtils.dip2px(getContext(), 21);

            ad_content.setLayoutParams(layoutParams);

        }

        if (tipsSite == 2) {

            LayoutParams layoutParams = (LayoutParams) offset_tv.getLayoutParams();

            layoutParams.addRule(ALIGN_PARENT_RIGHT);

            offset_tv.setLayoutParams(layoutParams);
        }

    }

    private void startAnimator(int startX, int endX) {

        ValueAnimator valueAnimator = ValueAnimator.ofInt(startX, endX, endX, endX, endX, endX, startX);
        valueAnimator.setInterpolator(new LinearInterpolator());
        valueAnimator.setDuration(6000);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override public void onAnimationUpdate(ValueAnimator animation) {
                int offset = (int) animation.getAnimatedValue();
                offset_tv.setX(offset);
            }
        });
        valueAnimator.addListener(new Animator.AnimatorListener() {
            @Override public void onAnimationStart(Animator animation) {
                offset_tv.setLayerType(LAYER_TYPE_HARDWARE, null);
            }

            @Override public void onAnimationEnd(Animator animation) {
                offset_tv.setLayerType(LAYER_TYPE_NONE, null);
            }

            @Override public void onAnimationCancel(Animator animation) {

            }

            @Override public void onAnimationRepeat(Animator animation) {

            }
        });
        valueAnimator.start();

    }

}
