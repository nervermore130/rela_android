package com.thel.ui.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseAdapter;
import com.thel.bean.user.MyImageBean;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.preview_image.UserInfoPhotoActivity;
import com.thel.utils.AppInit;
import com.thel.utils.L;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyImageAdapter extends BaseAdapter<MyImageAdapter.MyImageViewHolder, MyImageBean> {

    public MyImageAdapter(Context context) {
        super(context);
    }

    @Override
    public MyImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyImageViewHolder(mLayoutInflater.inflate(R.layout.uploadimage_griditem, null));
    }

    @Override
    public void onBindViewHolder(final MyImageViewHolder holder, final int position) {
        super.onBindViewHolder(holder, position);

        final MyImageBean item = getData().get(position);

        int width = (AppInit.displayMetrics.widthPixels - Utils.dip2px(TheLApp.getContext(), 30)) / 3;

        // 是否是拍照按钮
        if (item.isCameraBtnFlag) {
            ImageLoaderManager.imageLoader(holder.img_thumb, R.drawable.stroke_gray_bg);
            holder.camera_iv.setVisibility(View.VISIBLE);
            holder.img_thumb.setOnClickListener((View.OnClickListener) mContext);
            holder.img_private.setVisibility(View.GONE);
        } else {
            holder.text.setVisibility(View.GONE);
            holder.camera_iv.setVisibility(View.GONE);
            // 是否隐私图片
            if (item.isPrivate == 0) {
                // 是否封面图片
                //  int coverFlag = Integer.parseInt(myImageslist.get(position).coverFlag);
//                if (item.coverFlag == 0) {
//                    holder.img_private.setVisibility(View.GONE);
//                } else {
//                    holder.img_private.setImageResource(R.mipmap.icn_photos_cover);
//                    holder.img_private.setVisibility(View.GONE);
//                }
            } else {
                holder.img_private.setImageResource(R.mipmap.icn_locked);
                holder.img_private.setVisibility(View.VISIBLE);
            }

            ImageLoaderManager.imageLoader(holder.img_thumb, R.color.pic_placeholder_color, item.longThumbnailUrl);

            holder.img_thumb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    L.d("MyImageAdapter", " position : " + position);

                    L.d("MyImageAdapter", " item url : " + item.longThumbnailUrl);

                    L.d("MyImageAdapter", " getData() : " + getData());

                    ViewUtils.preventViewMultipleClick(v, 2000);
                    Intent i = new Intent(mContext, UserInfoPhotoActivity.class);
                    i.putExtra("fromPage", "UploadImageActivity");
                    ArrayList<MyImageBean> imgs = new ArrayList<>();

                    if (getData().size() == 9) {
                        MyImageBean item = getData().get(8);

                        if (item.isCameraBtnFlag) {
                            imgs.addAll(getData().subList(0, getData().size() - 1));
                        } else {
                            imgs.addAll(getData());
                        }


                    } else {
                        imgs.addAll(getData().subList(0, getData().size() - 1));
                    }

                    i.putExtra("userinfo", imgs);
                    i.putExtra("position", position);
                    mContext.startActivity(i);
                }
            });
        }

        holder.img_thumb.setOnLongClickListener(new View.OnLongClickListener()

        {
            @Override
            public boolean onLongClick(View v) {

                if (mOnDragItemClickListener != null) {
                    mOnDragItemClickListener.OnDragItemClick(holder);
                }

                return true;
            }
        });

        holder.img_thumb.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, width));

    }

    public class MyImageViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_thumb)
        ImageView img_thumb;
        @BindView(R.id.img_private)
        ImageView img_private;
        @BindView(R.id.text)
        TextView text;
        @BindView(R.id.camera_iv)
        ImageView camera_iv;

        public MyImageViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
