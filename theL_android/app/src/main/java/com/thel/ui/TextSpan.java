package com.thel.ui;

import androidx.core.content.ContextCompat;
import android.text.TextPaint;
import android.text.style.CharacterStyle;

import com.thel.app.TheLApp;
import com.thel.utils.SizeUtils;

/**
 * Created by setsail on 15/12/2.
 */
public class TextSpan extends CharacterStyle {

    private int mTextSize;
    private int mTextColor;

    public TextSpan(int textSize, int textColor) {
        mTextSize = textSize;
        mTextColor = textColor;
    }

    @Override
    public void updateDrawState(TextPaint tp) {
        tp.setColor(ContextCompat.getColor(TheLApp.getContext(), mTextColor));
        tp.setTextSize(SizeUtils.sp2px(TheLApp.getContext(), mTextSize));
    }
}
