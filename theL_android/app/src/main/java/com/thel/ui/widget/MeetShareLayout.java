package com.thel.ui.widget;

import android.content.Context;

import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.thel.R;
import com.thel.bean.live.LiveMultiSeatBean;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MeetShareLayout extends LinearLayout {

    private List<LiveMultiSeatBean.CoupleDetail> coupleDetail;
    private int whoReslut;

    public MeetShareLayout(Context context) {
        super(context);
        init();
    }

    public MeetShareLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MeetShareLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.layout_meet_share, this, true);

        ButterKnife.bind(this, view);
    }

    public void setData(LiveMultiSeatBean liveMultiSeatBean, int whoReslt) {
        this.coupleDetail = liveMultiSeatBean.coupleDetail;
        this.whoReslut = whoReslt;
    }

    @OnClick(R.id.friends_circle_iv)
    void onFriendsCircleClick() {

//        MomentPosterActivity.gotoShare("friends_circle_iv", coupleDetail, whoReslut);

    }

    @OnClick(R.id.friends_we_chat_iv)
    void onWeChatClick() {

//        MomentPosterActivity.gotoShare("friends_we_chat_iv", coupleDetail, whoReslut);

    }

    @OnClick(R.id.weibo_iv)
    void onWeiboClick() {

//        MomentPosterActivity.gotoShare("weibo_iv", coupleDetail, whoReslut);

    }

    @OnClick(R.id.qq_zone_iv)
    void onQQZoneClick() {

//        MomentPosterActivity.gotoShare("qq_zone_iv", coupleDetail, whoReslut);

    }

    @OnClick(R.id.qq_iv)
    void onQQClick() {

//        MomentPosterActivity.gotoShare("qq_iv", coupleDetail, whoReslut);

    }

    @OnClick(R.id.facebook_iv)
    void onFacebookClick() {

//        MomentPosterActivity.gotoShare("facebook_iv", coupleDetail, whoReslut);

    }


}
