package com.thel.ui.adapter;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.reflect.TypeToken;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseAdapter;
import com.thel.base.BaseDataBean;
import com.thel.bean.WebMomentsBean;
import com.thel.bean.comment.CommentBean;
import com.thel.bean.moments.MomentParentBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.theme.ThemeClassBean;
import com.thel.bean.video.VideoBean;
import com.thel.constants.BundleConstants;
import com.thel.constants.MomentTypeConstants;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.growingio.GrowingIoConstant;
import com.thel.imp.black.BlackUtils;
import com.thel.imp.like.MomentLikeUtils;
import com.thel.imp.momentblack.MomentBlackUtils;
import com.thel.manager.ImageLoaderManager;
import com.thel.manager.ListVideoVisibilityManager;
import com.thel.modules.main.home.InputActivity;
import com.thel.modules.main.home.moments.SendCardActivity;
import com.thel.modules.main.home.moments.comment.MomentCommentActivity;
import com.thel.modules.main.home.moments.theme.ThemeDetailActivity;
import com.thel.modules.main.home.moments.web.WebViewActivity;
import com.thel.modules.main.home.moments.winkcomment.WinkCommentsActivity;
import com.thel.modules.main.home.tag.TagDetailActivity;
import com.thel.modules.main.me.bean.LogInfoBean;
import com.thel.modules.main.me.match.eventcollect.collect.LiveLogUtils;
import com.thel.modules.main.userinfo.MyLinearLayoutManager;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.modules.video.UserVideoListActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.RequestConstants;
import com.thel.ui.widget.BlockRelativeLayout;
import com.thel.ui.widget.ExpandTextView;
import com.thel.ui.widget.FollowView;
import com.thel.ui.widget.LikeButtonView;
import com.thel.ui.widget.MomentParentViewOld;
import com.thel.ui.widget.MultiImageViewGroup;
import com.thel.ui.widget.RecommendLiveUserView;
import com.thel.ui.widget.RecommendWebView;
import com.thel.ui.widget.VideoLoadingLayout;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;
import com.thel.utils.BusinessUtils;
import com.thel.utils.DeviceUtils;
import com.thel.utils.DialogUtil;
import com.thel.utils.DialogUtils;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;
import com.thel.utils.MomentUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SharedPrefUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;
import com.thel.utils.VideoUtils;
import com.thel.utils.ViewUtils;

import java.lang.reflect.Type;
import java.util.List;
import java.util.ListIterator;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * @author liuyun
 * @date 2017/9/21
 */

public class MomentsAdapter extends BaseRecyclerViewAdapter<MomentsBean> {

    private static final String TAG = "MomentsAdapter";
    private String fromPageId;
    private String fromPage;
    private String pageId;
    private String page;

    private boolean isShowRecommendView = true;

    private boolean isShowFollowBtn = true;

    private static final int MOMENT_TEXT_CONTENT_MAX_LINE = 5;

    private String pageFrom = GrowingIoConstant.ENTRY_FOLLOW_PAGE;

    private MyLinearLayoutManager mMyLinearLayoutManager;

    private BroadcastReceiver newCommentNotify = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();

            if (action == null) {
                return;
            }

            L.d(TAG, " action : " + action);

            try {
                if (action.equals(TheLConstants.BROADCAST_AUTO_RELEASE_NEW_RECOMMEND_COMMENT_ACTION) || action.equals(TheLConstants.BROADCAST_AUTO_RELEASE_NEW_COMMENT_ACTION)) {

                    String momentId = intent.getStringExtra(RequestConstants.I_MOMENTS_ID);

                    int currentPos = getPositionByMomentId(momentId);

                    List<MomentsBean> momentsBeans = getData();

                    if (currentPos < 0 || momentsBeans == null) {
                        return;
                    }

                    CommentBean commentBean = new CommentBean();
                    commentBean.commentText = intent.getStringExtra(RequestConstants.I_TEXT);
                    commentBean.commentType = intent.getStringExtra(RequestConstants.I_TYPE);
                    commentBean.momentsId = mData.get(currentPos).momentsId;
                    commentBean.userId = Integer.valueOf(ShareFileUtils.getString(ShareFileUtils.ID, ""));
                    commentBean.nickname = ShareFileUtils.getString(ShareFileUtils.USER_NAME, "");
                    commentBean.avatar = ShareFileUtils.getString(ShareFileUtils.AVATAR, "");
                    commentBean.commentTime = "0_0";
                    commentBean.sayType = CommentBean.SAY_TYPE_SAY;
                    momentsBeans.get(currentPos).commentNum++;
                    if (momentsBeans.get(currentPos).commentList.size() >= 3) {
                        momentsBeans.get(currentPos).commentList.remove(2);
                    }
                    momentsBeans.get(currentPos).commentList.add(0, commentBean);
                    notifyDataSetChanged();

                    if (mMyLinearLayoutManager != null) {

                        ListVideoVisibilityManager.getInstance().calculatorItemPercent(mMyLinearLayoutManager, RecyclerView.SCROLL_STATE_IDLE, MomentsAdapter.this, TheLConstants.EntryConstants.ENTRY_FOLLOWING);

                    }


                } else if (TheLConstants.BROADCAST_VIDEO_COUNT.equals(action)) {
                    VideoBean videoBean = (VideoBean) intent.getExtras().getSerializable(TheLConstants.BUNDLE_KEY_VIDEO_BEAN);
                    if (videoBean == null) {
                        return;
                    }

                    for (int i = 0; i < getData().size(); i++) {
                        final MomentsBean momentsBean = getData().get(i);
                        VideoBean video = Utils.getVideoFromMoment(momentsBean);
                        if (video.equals(videoBean)) {
                            getData().get(i).playCount = videoBean.playCount;
                            getData().get(i).winkFlag = videoBean.winkFlag;
                            getData().get(i).winkNum = videoBean.winkNum;
                            notifyDataSetChanged();
                            return;
                        }
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public void setShowRecommendView(boolean showRecommendView) {
        isShowRecommendView = showRecommendView;
    }

    public void setLayoutManager(MyLinearLayoutManager mMyLinearLayoutManager) {
        this.mMyLinearLayoutManager = mMyLinearLayoutManager;
    }

    public void setShowFollowBtn(boolean isShowFollowBtn) {
        this.isShowFollowBtn = isShowFollowBtn;
    }

    public MomentsAdapter(int layoutResId, List<MomentsBean> data) {
        super(layoutResId, data);
    }

    public interface ThemeListCommentListener {
        void onclick(MomentsBean item, int position);
    }

    private ThemeListCommentListener themeListCommentListener;

    /**
     * 话题日志页面专用构造器
     *
     * @param layoutResId
     * @param data
     * @param pageFrom
     * @param themeListCommentListener
     */
    public MomentsAdapter(int layoutResId, List<MomentsBean> data, String pageFrom, String page, String PageId, String fromPage, String fromPageId, ThemeListCommentListener themeListCommentListener) {
        super(layoutResId, data);
        this.pageFrom = pageFrom;
        this.page = page;
        this.pageId = PageId;
        this.fromPage = fromPage;
        this.fromPageId = fromPageId;
        this.themeListCommentListener = themeListCommentListener;
    }

    public MomentsAdapter(int layoutResId, List<MomentsBean> data, String pageFrom) {
        super(layoutResId, data);
        this.pageFrom = pageFrom;
    }

    public MomentsAdapter(int layoutResId, List<MomentsBean> data, String pageFrom, String page, String PageId, String fromPage, String fromPageId) {
        super(layoutResId, data);
        this.pageFrom = pageFrom;
        this.page = page;
        this.pageId = PageId;
        this.fromPage = fromPage;
        this.fromPageId = fromPageId;
    }

    public void registerNewCommentNotify(Context context) {
        IntentFilter intentFilter = new IntentFilter();
        //推荐评论和评论在其它地方需要区分开
        intentFilter.addAction(TheLConstants.BROADCAST_AUTO_RELEASE_NEW_RECOMMEND_COMMENT_ACTION);//推荐评论
        intentFilter.addAction(TheLConstants.BROADCAST_AUTO_RELEASE_NEW_COMMENT_ACTION);//评论
        intentFilter.addAction(TheLConstants.BROADCAST_VIDEO_COUNT);
        context.registerReceiver(newCommentNotify, intentFilter);
    }

    public void unregisterNewCommentNotify(Context context) {
        context.unregisterReceiver(newCommentNotify);
    }

    @Override
    protected BaseViewHolder onCreateDefViewHolder(ViewGroup parent, int viewType) {

        BaseViewHolder baseViewHolder = super.onCreateDefViewHolder(parent, viewType);
        return new MomentsViewHolder(baseViewHolder.convertView);
    }

    @Override
    protected void convert(final BaseViewHolder helper, final MomentsBean momentsBean) {

        L.d("MomentsAdapter", " momentsBean.momentsType : " + momentsBean.momentsType);

        final MomentsViewHolder holder = (MomentsViewHolder) helper;

        final Context context = helper.getConvertView().getContext();

        //预发布日志，禁止点击
        if (momentsBean.momentsId.contains(MomentsBean.SEND_MOMENT_FLAG))
            holder.root_ll.setBlock(true);
        else
            holder.root_ll.setBlock(false);

        holder.center_layout.setVisibility(GONE);

        holder.moment_parent_view.setVisibility(GONE);

        holder.lin_latest_comments.setVisibility(GONE);

        holder.layout_multi_image.setVisibility(GONE);

        holder.layout_moment_music.setVisibility(GONE);

        holder.layout_moment_video.setVisibility(GONE);

        holder.layout_moment_user_card.setVisibility(GONE);

        holder.live_user_view.setVisibility(GONE);

        holder.recommend_web_view.setVisibility(GONE);

        holder.lin_recommend_repeat_users.setVisibility(GONE);

        holder.layout_moment_live.setVisibility(GONE);

        holder.layout_moment_theme.setVisibility(View.GONE);

        holder.layout_moment_theme_participate.setVisibility(View.GONE);

        holder.moment_opts_emoji_txt.setVisibility(View.GONE);

        holder.videoLoading.setVisibility(View.GONE);

        holder.txt_theme_title.setVisibility(View.GONE);

        holder.txt_theme_desc.setVisibility(View.GONE);

        holder.time_machine_rl.setVisibility(GONE);

        holder.txt_update_now.setVisibility(GONE);

        // 是否匿名
        if (momentsBean.secret == MomentsBean.IS_SECRET) {

            holder.moment_user_name.setText(TheLApp.getContext().getString(R.string.moments_msgs_notification_anonymous));

            ImageLoaderManager.imageLoader(holder.img_thumb, R.mipmap.icon_user_anonymous);
            holder.moment_img_vip.setVisibility(GONE);
        } else {
            ImageLoaderManager.imageLoaderCircle(holder.img_thumb, R.mipmap.icon_user, momentsBean.avatar);

            holder.moment_user_name.setText(momentsBean.nickname);

            MomentUtils.setNicknameWithMomentType(helper.getView(R.id.moment_user_name), momentsBean);
            if (momentsBean.level > 0) {
                holder.moment_img_vip.setVisibility(VISIBLE);
                switch (momentsBean.level) {
                    case 1:
                        holder.moment_img_vip.setImageResource(R.mipmap.icn_vip_1);
                        break;
                    case 2:
                        holder.moment_img_vip.setImageResource(R.mipmap.icn_vip_2);
                        break;
                    case 3:
                        holder.moment_img_vip.setImageResource(R.mipmap.icn_vip_3);
                        break;
                    case 4:
                        holder.moment_img_vip.setImageResource(R.mipmap.icn_vip_4);
                        break;
                    default:
                        holder.moment_img_vip.setVisibility(GONE);
                        break;
                }
            } else {
                holder.moment_img_vip.setVisibility(GONE);

            }
        }

        updateShareTo(momentsBean, helper);

        // 推荐日志
        if (!TextUtils.isEmpty(momentsBean.tag)) {
            if (momentsBean.tag.equals(MomentsBean.TAG_TOP) && momentsBean.topicFlag == 0) {
                helper.setImageUrl(R.id.moments_tag, TheLConstants.RES_PIC_URL + R.mipmap.icn_feed_top);
                helper.setVisibility(R.id.moments_tag, View.VISIBLE);
            } else if (momentsBean.tag.equals(MomentsBean.TAG_RECOMMEND) && momentsBean.topicFlag == 0) {
                helper.setImageUrl(R.id.moments_tag, TheLConstants.RES_PIC_URL + R.mipmap.icn_feed_edited);
                helper.setVisibility(R.id.moments_tag, View.VISIBLE);
            } else {
                helper.setVisibility(R.id.moments_tag, GONE);
            }
        } else {
            helper.setVisibility(R.id.moments_tag, GONE);
        }

        // 是否是用户个人置顶(显示优先级高于推荐标签)
        if (momentsBean.userBoardTop == 1) {
            ImageLoaderManager.imageLoader(holder.moments_tag, R.mipmap.icn_feed_top);
            helper.setVisibility(R.id.moments_tag, View.VISIBLE);
        }

        if (isShowFollowBtn && momentsBean.myself == 0 && !UserUtils.getMyUserId().equals(momentsBean.userId + "") && momentsBean.secret == MomentsBean.IS_NOT_SECRET) {

            if (momentsBean.followerStatus == 0 || momentsBean.followerStatus == 2) {
                holder.txt_follow.setVisibility(View.VISIBLE);
                holder.txt_follow.setFollowData(String.valueOf(momentsBean.userId), momentsBean.nickname, momentsBean.avatar, momentsBean.followerStatus);
            } else {
                holder.txt_follow.setVisibility(View.GONE);
            }
        } else {
            holder.txt_follow.setVisibility(View.GONE);
        }


        if (momentsBean.winkNum > 0) {
            holder.moment_opts_emoji_txt.setVisibility(VISIBLE);
            holder.moment_opts_emoji_txt.setText(momentsBean.winkNum + " " + TheLApp.getContext().getString(R.string.like_msgs_title));
        } else {
            holder.moment_opts_emoji_txt.setVisibility(GONE);
        }

        if (momentsBean.commentNum > 0) {
            holder.moment_opts_comment_txt.setVisibility(VISIBLE);
            holder.moment_opts_comment_txt.setText(momentsBean.commentNum + " " + TheLApp.getContext().getString(R.string.moment_comments_title));
        } else {
            holder.moment_opts_comment_txt.setVisibility(GONE);
        }

        if (TextUtils.isEmpty(momentsBean.distance)) {
            holder.moment_release_time.setText(MomentUtils.getReleaseTimeShow(momentsBean.secret, momentsBean.momentsTime));
        } else {// 如果距离字段不为空，则要显示距离
            if ("附近".equals(momentsBean.distance)) {
                holder.moment_release_time.setText(MomentUtils.getReleaseTimeShow(momentsBean.secret, momentsBean.momentsTime) + "/" + TheLApp.getContext().getString(R.string.topic_followers_act_nearby));
            } else {
                holder.moment_release_time.setText(momentsBean.distance + "/" + MomentUtils.getReleaseTimeShow(momentsBean.secret, momentsBean.momentsTime));
            }
        }

        if (momentsBean.atUserList != null && momentsBean.atUserList.size() > 0 && momentsBean.shareTo != MomentsBean.SHARE_TO_ONLY_ME) {
            holder.layout_moment_mention.setVisibility(View.VISIBLE);
            if (!momentsBean.momentsId.contains(MomentsBean.SEND_MOMENT_FLAG)) {
                holder.moment_mentioned_text.setText(MomentUtils.getMetionedShowString(momentsBean.atUserList, momentsBean.momentsId));
            } else {
                holder.moment_mentioned_text.setText(MomentUtils.getMetionedShowStringForSendMoment(momentsBean.atUserList, momentsBean.momentsId));
            }
            holder.moment_mentioned_text.setMovementMethod(LinkMovementMethod.getInstance());
        } else {
            holder.layout_moment_mention.setVisibility(View.GONE);
        }

        if (momentsBean.shareTo != MomentsBean.SHARE_TO_ONLY_ME) {
            holder.iv_feed_recommend.setVisibility(VISIBLE);
        } else {
            holder.iv_feed_recommend.setVisibility(GONE);
        }

        if (MomentsBean.MOMENT_TYPE_AD.equals(momentsBean.momentsType)) {
            holder.txt_theme.setVisibility(GONE);
            holder.moment_release_time.setBackgroundResource(R.drawable.shape_ad_text_bg);
            holder.moment_release_time.setTextColor(TheLApp.context.getResources().getColor(R.color.white));
            holder.moment_release_time.setTypeface(null, Typeface.BOLD);
            holder.select_theme.setText(TheLApp.getContext().getString(R.string.click_to_see_more));
            holder.imageView.setVisibility(GONE);
            holder.topic_comma_right_iv.setVisibility(GONE);

            String adText = null;

            switch (momentsBean.adType) {
                case "official":
                    adText = TheLApp.context.getResources().getString(R.string.official);
                    break;
                case "ad":
                    adText = TheLApp.context.getResources().getString(R.string.AD);
                    break;
                default:
                    break;
            }

            holder.moment_release_time.setText(adText);

        } else {
            holder.moment_release_time.setTypeface(null, Typeface.NORMAL);
            holder.moment_release_time.setBackgroundResource(R.color.white);
            holder.moment_release_time.setTextColor(TheLApp.context.getResources().getColor(R.color.theme_class_title_tab));
        }

        final ExpandTextView txt_content = helper.getView(R.id.moment_content_text);
        final TextView txt_whole = helper.getView(R.id.txt_whole);
        //状态为收起状态
        if (momentsBean.contentStatus == MomentsBean.CONTENT_STATUS_HANG) {
            //显示为全文
            txt_whole.setText(context.getString(R.string.whole_content));
            txt_content.setMaxLines(MOMENT_TEXT_CONTENT_MAX_LINE);
        } else {
            //显示为收起
            txt_whole.setText(context.getString(R.string.pack_up));
            txt_content.setMaxLines(Integer.MAX_VALUE);
        }
        txt_content.setTextChangedListener(new ExpandTextView.TextChangedListener() {
            @Override
            public void textChanged(TextView textView) {
                if (View.VISIBLE == txt_content.getVisibility()) {
                    final Layout layout = txt_content.getLayout();
                    final int lineCount = layout.getLineCount();
                    momentsBean.textLines = lineCount;
                    int eCount = 0;
                    if (lineCount >= MOMENT_TEXT_CONTENT_MAX_LINE) {
                        eCount = layout.getEllipsisCount(MOMENT_TEXT_CONTENT_MAX_LINE - 1);
                    }
                    if (eCount > 0 || lineCount > MOMENT_TEXT_CONTENT_MAX_LINE) {
                        txt_whole.setVisibility(View.VISIBLE);
                    } else {
                        txt_whole.setVisibility(GONE);
                    }
                } else {
                    txt_whole.setVisibility(GONE);
                }
            }
        });

        txt_whole.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (momentsBean.textLines > MomentsBean.MAX_EXPAND_LINES) {
                    jumpToCommentPage(momentsBean);
                } else {
                    //当前状态是收起，显示全文
                    if (momentsBean.contentStatus == MomentsBean.CONTENT_STATUS_HANG) {
                        momentsBean.contentStatus = MomentsBean.CONTENT_STATUS_WHOLE;
                        txt_content.setMaxLines(Integer.MAX_VALUE);
                        //显示为收起
                        txt_whole.setText(context.getString(R.string.pack_up));
                    } else {
                        //否者，收起
                        momentsBean.contentStatus = MomentsBean.CONTENT_STATUS_HANG;
                        txt_content.setMaxLines(MOMENT_TEXT_CONTENT_MAX_LINE);
                        //显示为全文
                        txt_whole.setText(context.getString(R.string.whole_content));
                    }
                }
            }
        });

        if (!TextUtils.isEmpty(momentsBean.momentsText)) {

            holder.layout_moment_text.setVisibility(View.VISIBLE);

            MomentUtils.setMomentContent(holder.moment_content_text, momentsBean.momentsText);

        } else {
            holder.layout_moment_text.setVisibility(View.GONE);
        }

        if (momentsBean.winkFlag != MomentsBean.HAS_NOT_WINKED) {
            holder.moment_opts_like.setChecked(true);
        } else {
            holder.moment_opts_like.setChecked(false);
        }

        if (!TextUtils.isEmpty(momentsBean.momentsType))
            switch (momentsBean.momentsType) {

                case MomentTypeConstants.MOMENT_TYPE_TEXT:

                    break;
                case MomentTypeConstants.MOMENT_TYPE_IMAGE:

                    holder.center_layout.setVisibility(View.VISIBLE);

                    holder.layout_multi_image.setVisibility(View.VISIBLE);

                    holder.layout_multi_image.initUI(momentsBean.imageUrl, momentsBean.userName, Utils.getImageShareBean(momentsBean));

                    break;
                case MomentTypeConstants.MOMENT_TYPE_TEXT_IMAGE:

                    holder.center_layout.setVisibility(View.VISIBLE);

                    holder.layout_multi_image.setVisibility(View.VISIBLE);

                    holder.layout_multi_image.initUI(momentsBean.imageUrl, momentsBean.userName, Utils.getImageShareBean(momentsBean));

                    break;
                case MomentTypeConstants.MOMENT_TYPE_VOICE:

                    holder.center_layout.setVisibility(View.VISIBLE);

                    holder.layout_moment_music.setVisibility(View.VISIBLE);

                    holder.song_name.setText(momentsBean.songName);

                    holder.album_name.setText(momentsBean.albumName);

                    holder.artist_name.setText(momentsBean.artistName);

                    ImageLoaderManager.imageLoader(holder.moment_content_music_pic, R.drawable.bg_waterfull_item_shape, momentsBean.albumLogo444);


                    break;
                case MomentTypeConstants.MOMENT_TYPE_TEXT_VOICE:

                    holder.center_layout.setVisibility(View.VISIBLE);

                    holder.layout_moment_music.setVisibility(View.VISIBLE);

                    holder.song_name.setText(momentsBean.songName);

                    holder.album_name.setText(momentsBean.albumName);

                    holder.artist_name.setText(momentsBean.artistName);

                    ImageLoaderManager.imageLoader(holder.moment_content_music_pic, R.drawable.bg_waterfull_item_shape, momentsBean.albumLogo444);

                    break;

                case MomentTypeConstants.MOMENT_TYPE_THEME:

                    holder.center_layout.setVisibility(View.VISIBLE);

                    holder.layout_moment_theme.setVisibility(View.VISIBLE);

                    holder.moment_content_text.setVisibility(View.GONE);

                    holder.imageView.setVisibility(VISIBLE);

                    holder.topic_comma_right_iv.setVisibility(VISIBLE);

                    if (momentsBean.momentsText != null) {
                        holder.txt_theme.setVisibility(VISIBLE);
                        holder.txt_theme.setText(momentsBean.momentsText);
                    }
                    if (momentsBean.themeClass != null) {
                        final ThemeClassBean themeClassBean = MomentUtils.getThemeByType(momentsBean.themeClass);
                        if (themeClassBean != null) {
                            holder.select_theme.setVisibility(VISIBLE);
                            holder.select_theme.setText(themeClassBean.text + " >");
                        } else {
                            holder.select_theme.setVisibility(GONE);

                        }
                    }
                    holder.txt_theme.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View v) {
                            DialogUtil.getInstance().showSelectionDialog((Activity) mContext, new String[]{TheLApp.getContext().getString(R.string.info_copy)}, new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    DialogUtil.getInstance().closeDialog();
                                    switch (position) {
                                        case 0:
                                            // 复制
                                            DeviceUtils.copyToClipboard(TheLApp.getContext(), momentsBean.momentsText);
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }, false, 2, null);
                            return true;
                        }
                    });
                    holder.txt_theme.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
//                            Intent intent = new Intent();
//                            Bundle bundle = new Bundle();
//                            bundle.putString(ShareFileUtils.MATCH_FROM_PAGE_ID, pageId);
//                            bundle.putString(ShareFileUtils.MATCH_FROM_PAGE, page);
//                            intent.putExtras(bundle);
//                            intent.setClass(TheLApp.getContext(), ThemeDetailActivity.class);
//                            intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, momentsBean.momentsId);
//                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                            TheLApp.getContext().startActivity(intent);
                            FlutterRouterConfig.Companion.gotoThemeDetails(momentsBean.momentsId);
                        }
                    });
                    if (!TextUtils.isEmpty(momentsBean.themeTagName)) {
                        holder.moment_content_text.setVisibility(VISIBLE);
                        SpannableString sp = new SpannableString(momentsBean.themeTagName);
                        sp.setSpan(new ForegroundColorSpan(ContextCompat.getColor(TheLApp.context, R.color.tag_color)), 0, momentsBean.themeTagName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        holder.moment_content_text.setText(sp);
                        holder.moment_content_text.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
//                                Intent intent = new Intent(TheLApp.getContext().getApplicationContext(), TagDetailActivity.class);
//                                intent.putExtra(TheLConstants.BUNDLE_KEY_TOPIC_NAME, momentsBean.themeTagName);
//                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                TheLApp.getContext().getApplicationContext().startActivity(intent);
                                FlutterRouterConfig.Companion.gotoTagDetails(Integer.parseInt(momentsBean.topicId), momentsBean.themeTagName);
                            }
                        });
                    } else {
                        holder.moment_content_text.setVisibility(GONE);
                    }
                    // 话题日志有可能不带背景图，所以要显示默认背景
                    if (!TextUtils.isEmpty(momentsBean.imageUrl)) {
                        ImageLoaderManager.imageLoader(holder.img_theme, R.mipmap.bg_topic_default, momentsBean.imageUrl);
                        holder.rel_theme.setBackgroundColor(ContextCompat.getColor(TheLApp.context, R.color.black_transprent_30));
                    } else {
                        ImageLoaderManager.imageLoader(holder.img_theme, R.mipmap.bg_topic_default);
                        holder.rel_theme.setBackgroundColor(ContextCompat.getColor(TheLApp.context, R.color.black_transprent_30));
                    }

                    break;

                case MomentTypeConstants.MOMENT_TYPE_THEME_REPLY:

                    final MomentParentBean parentMomentBean = momentsBean.parentMoment;
                    if (parentMomentBean == null) {
                        return;
                    }

                    if (!parentMomentBean.momentsId.equals("0") && parentMomentBean.deleteFlag == 0) {

                        holder.layout_moment_theme_participate.setVisibility(View.VISIBLE);

                        holder.txt_theme_title.setVisibility(View.VISIBLE);
                        holder.txt_theme_desc.setVisibility(View.VISIBLE);
                        holder.txt_deleted.setVisibility(View.GONE);

                        ImageLoaderManager.imageLoader(holder.theme_iv, R.mipmap.bg_topicpost_default, parentMomentBean.imgUrl);
                        holder.txt_theme_title.setText(parentMomentBean.momentsText);
                        holder.txt_theme_desc.setText(MomentUtils.buildDesc(parentMomentBean.joinTotal));
                        holder.layout_moment_theme_participate.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
//                                Intent intent = new Intent(TheLApp.getContext(), ThemeDetailActivity.class);
//                                Bundle bundle = new Bundle();
//                                bundle.putString(ShareFileUtils.MATCH_FROM_PAGE_ID, pageId);
//                                bundle.putString(ShareFileUtils.MATCH_FROM_PAGE, page);
//                                intent.putExtras(bundle);
//                                intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, parentMomentBean.momentsId + "");
//                                intent.putExtra(BundleConstants.THEMEPARTICIPATES, MomentUtils.buildDesc(parentMomentBean.joinTotal));
//                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                TheLApp.getContext().startActivity(intent);
                                FlutterRouterConfig.Companion.gotoThemeDetails(parentMomentBean.momentsId);
                            }
                        });

                    } else if (parentMomentBean.deleteFlag == 1) {
                        holder.layout_moment_theme_participate.setVisibility(View.VISIBLE);
                        holder.txt_theme_title.setVisibility(View.GONE);
                        holder.txt_theme_desc.setVisibility(View.GONE);
                        holder.txt_deleted.setVisibility(View.VISIBLE);
                        ImageLoaderManager.imageLoader(holder.theme_iv, R.mipmap.icn_topic_delete);
                        holder.layout_moment_theme_participate.setOnClickListener(null);
                    }

                    if (momentsBean.themeReplyClass != null) {

                        switch (momentsBean.themeReplyClass) {
                            case "video":
                                holder.center_layout.setVisibility(View.VISIBLE);

                                holder.layout_moment_video.setVisibility(View.VISIBLE);

                                holder.videoLoading.setVisibility(View.VISIBLE);

                                ImageLoaderManager.imageLoader(holder.moment_content_video_pic, R.drawable.bg_waterfull_item_shape, momentsBean.thumbnailUrl);
                                break;
                            case "image":
                                holder.center_layout.setVisibility(View.VISIBLE);

                                holder.layout_multi_image.setVisibility(View.VISIBLE);

                                holder.layout_multi_image.initUI(momentsBean.imageUrl, momentsBean.userName, Utils.getImageShareBean(momentsBean));

                                break;
                            case "voice":
                                holder.center_layout.setVisibility(View.VISIBLE);

                                holder.layout_moment_music.setVisibility(View.VISIBLE);

                                holder.song_name.setText(momentsBean.songName);

                                holder.album_name.setText(momentsBean.albumName);

                                holder.artist_name.setText(momentsBean.artistName);

                                ImageLoaderManager.imageLoader(holder.moment_content_music_pic, R.drawable.bg_waterfull_item_shape, momentsBean.albumLogo444);
                                break;
                            default:
                                break;
                        }
                    }

                    break;

                case MomentTypeConstants.MOMENT_TYPE_VIDEO:

                    holder.center_layout.setVisibility(View.VISIBLE);

                    holder.layout_moment_video.setVisibility(View.VISIBLE);

                    holder.videoLoading.setVisibility(View.VISIBLE);

                    if (VideoUtils.willAutoPlayVideo()) {
                        holder.img_video_play.setVisibility(View.GONE);
                        holder.videoLoading.setVisibility(View.VISIBLE);
                    } else {
                        holder.img_video_play.setVisibility(View.VISIBLE);
                        holder.videoLoading.setVisibility(View.GONE);
                    }

                    ImageLoaderManager.imageLoader(holder.moment_content_video_pic, R.drawable.bg_waterfull_item_shape, momentsBean.thumbnailUrl);

                    String text;
                    if (momentsBean.playTime > 0) {
                        text = Utils.formatTime(momentsBean.playTime) + " / " + TheLApp.getContext().getString(R.string.play_count, momentsBean.playCount);
                    } else {
                        text = TheLApp.getContext().getString(R.string.play_count, momentsBean.playCount);
                    }
                    holder.txt_play_times.setText(text);

                    holder.layout_moment_video.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            gotoVideoPlayAct(momentsBean, v, holder.getAdapterPosition());
                        }
                    });

                    break;
                case MomentTypeConstants.MOMENT_TYPE_VOICE_LIVE:
                case MomentTypeConstants.MOMENT_TYPE_LIVE:

                    holder.center_layout.setVisibility(View.VISIBLE);

                    holder.live_user_view.setVisibility(View.VISIBLE);

                    holder.live_user_view.initView(momentsBean);

                    break;

                case MomentTypeConstants.MOMENT_TYPE_USER_CARD:

                    holder.center_layout.setVisibility(View.VISIBLE);

                    holder.layout_moment_user_card.setVisibility(View.VISIBLE);

                    holder.moment_content_text.setVisibility(View.GONE);

                    ImageLoaderManager.imageLoaderCircle(holder.img_user_card_avatar, R.mipmap.icon_user, momentsBean.cardAvatar);

                    holder.txt_user_card_nickname.setText(momentsBean.cardNickName);

                    if (momentsBean.cardIntro != null) {
                        holder.txt_user_card_intro.setText(momentsBean.cardIntro.replaceAll("\n", " "));
                    } else {
                        holder.txt_user_card_intro.setText("");
                    }

                    holder.txt_user_card_info.setText(MomentUtils.buildUserCardInfoStr(momentsBean.cardAge, momentsBean.cardAffection, momentsBean.cardHeight, momentsBean.cardWeight));

                    holder.mask_user_card.setBackgroundColor(ContextCompat.getColor(TheLApp.context, R.color.light_gray_mask));

                    // 被推荐用户可能不带背景图，所以要显示默认背景
                    if (!TextUtils.isEmpty(momentsBean.imageUrl)) {
                        ImageLoaderManager.imageLoader(holder.img_user_card, momentsBean.imageUrl);
                    }

                    holder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            MomentUtils.jumpToUserPage(momentsBean.cardUserId + "");
                        }
                    });

                    break;

                case MomentTypeConstants.MOMENT_TYPE_RECOMMEND:

                    holder.center_layout.setVisibility(View.VISIBLE);

                    holder.lin_recommend_repeat_users.setVisibility(View.GONE);

                    holder.moment_parent_view.initView(momentsBean);

                    if (momentsBean.recommendMoment != null && momentsBean.recommendMoment.count > 0) {
                        helper.setVisibility(R.id.lin_recommend_repeat_users, View.VISIBLE);
                        holder.txt_recommend_repeat_users.setText(MomentUtils.getRecommendUserListBySystem(momentsBean));
                        holder.txt_recommend_repeat_users.setMovementMethod(LinkMovementMethod.getInstance());
                    }

                    break;

                case MomentTypeConstants.MOMENT_TYPE_WEB:

                    holder.center_layout.setVisibility(View.VISIBLE);

                    holder.recommend_web_view.setVisibility(View.VISIBLE);

                    holder.recommend_web_view.initView(momentsBean);

                    WebMomentsBean webMomentsBean = GsonUtils.getObject(momentsBean.momentsText, WebMomentsBean.class);

                    holder.moment_content_text.setText(webMomentsBean.momentsText);
                    break;

                case MomentTypeConstants.MOMENT_TYPE_LIVE_USER:
                case MomentTypeConstants.MOMENT_TYPE_VOICE_LIVE_USER:

                    holder.center_layout.setVisibility(View.VISIBLE);

                    holder.live_user_view.setVisibility(View.VISIBLE);

                    holder.live_user_view.initView(momentsBean);

                    break;
                case MomentTypeConstants.MOMENT_TYPE_AD:
                    holder.center_layout.setVisibility(View.VISIBLE);
                    holder.layout_moment_theme.setVisibility(View.VISIBLE);
                    holder.moment_content_text.setVisibility(View.VISIBLE);
                    holder.moment_content_text.setText(momentsBean.momentsText);

                    // 话题日志有可能不带背景图，所以要显示默认背景
                    if (!TextUtils.isEmpty(momentsBean.imageUrl)) {
                        ImageLoaderManager.imageLoader(holder.img_theme, R.mipmap.bg_topic_default, momentsBean.imageUrl);
                        holder.rel_theme.setBackgroundColor(ContextCompat.getColor(TheLApp.context, R.color.black_transprent_30));
                    } else {
                        ImageLoaderManager.imageLoader(holder.img_theme, R.mipmap.bg_topic_default);
                        holder.rel_theme.setBackgroundColor(ContextCompat.getColor(TheLApp.context, R.color.black_transprent_30));
                    }
                    break;
                default:
                    holder.moment_content_text.setVisibility(VISIBLE);
                    holder.txt_update_now.setVisibility(VISIBLE);
                    String content = TheLApp.context.getResources().getString(R.string.info_version_outdate);
                    MomentUtils.setMomentContent(holder.moment_content_text, content);
                    break;
            }
        if (!pageFrom.equals(GrowingIoConstant.ENTRY_TIME_MACHINE) && momentsBean.commentList != null && momentsBean.commentList.size() > 0) {

            holder.lin_latest_comments.setVisibility(View.VISIBLE);

            holder.lin_latest_comments.removeAllViews();

            MomentUtils.addComments(mContext, holder.lin_latest_comments, momentsBean.commentList, momentsBean.momentsId, momentsBean.momentsType);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(holder.itemView, momentsBean, holder.getAdapterPosition());
                    /**
                     * 埋点上报 首页点击
                     * **/
                    if (pageFrom.equals(GrowingIoConstant.ENTRY_FOLLOW_PAGE)) {
                        traceMomentLog(page, "click", helper.getAdapterPosition(), momentsBean.momentsId, momentsBean.momentsType, momentsBean.rank_id);

                    }

                }

            }
        });

        holder.moment_opts_emoji_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                Intent intent = new Intent(mContext, WinkCommentsActivity.class);
                intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, momentsBean.momentsId);
                mContext.startActivity(intent);
            }
        });

        holder.moment_opts_like.setLikeAnimImage(holder.img_like_anim);

        holder.moment_opts_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);

                if (MomentUtils.isBlackOrBlock(momentsBean.userId + "")) {
                    return;
                }
                if (momentsBean.winkFlag == MomentsBean.HAS_NOT_WINKED) {
                    BusinessUtils.playSound(R.raw.sound_emoji);
                    holder.moment_opts_like.check();
                    likeMoment(momentsBean.momentsId);
                    momentsBean.winkFlag = MomentsBean.HAS_WINKED;
                    momentsBean.winkNum++;
                    /**
                     * 埋点上报 点赞
                     * **/
                    if (pageFrom.equals(GrowingIoConstant.ENTRY_FOLLOW_PAGE) || pageFrom.equals(GrowingIoConstant.ENTRY_NEARBY_PAGE)) {
                        traceMomentLog(page, "like", helper.getAdapterPosition(), momentsBean.momentsId, momentsBean.momentsType, momentsBean.rank_id);

                    } else if (pageFrom.equals(GrowingIoConstant.ENTRY_THEME_DETAIL_PAGE)) {
                        if (!TextUtils.isEmpty(momentsBean.commentId)) {
                            traceThemeReplyLog(page, "like_reply", momentsBean.momentsId, momentsBean.commentId);

                        } else {
                            traceThemeLog(page, "like_theme", momentsBean.momentsId);

                        }

                    }
                } else {
                    if (momentsBean.winkNum <= 0) {
                        holder.moment_opts_emoji_txt.setVisibility(View.GONE);
                    }
                    holder.moment_opts_like.setChecked(false);
                    momentsBean.winkFlag = MomentsBean.HAS_NOT_WINKED;
                    momentsBean.winkNum--;
                    unWinkMoment(momentsBean.momentsId);
                    /**
                     * 埋点上报 取消点赞
                     * **/
                    if (pageFrom.equals(GrowingIoConstant.ENTRY_FOLLOW_PAGE) || pageFrom.equals(GrowingIoConstant.ENTRY_NEARBY_PAGE)) {
                        traceMomentLog(page, "unlike", helper.getAdapterPosition(), momentsBean.momentsId, momentsBean.momentsType, momentsBean.rank_id);

                    } else if (pageFrom.equals(GrowingIoConstant.ENTRY_THEME_DETAIL_PAGE)) {
                        if (!TextUtils.isEmpty(momentsBean.commentId)) {
                            traceThemeReplyLog(page, "unlike_theme", momentsBean.momentsId, momentsBean.commentId);

                        } else {
                            traceThemeLog(page, "unlike_theme", momentsBean.momentsId);

                        }

                    }
                }

                if (momentsBean.winkNum > 0) {
                    holder.moment_opts_emoji_txt.setVisibility(VISIBLE);
                    holder.moment_opts_emoji_txt.setText(momentsBean.winkNum + " " + TheLApp.getContext().getString(R.string.like_msgs_title));
                } else {
                    holder.moment_opts_emoji_txt.setVisibility(GONE);
                }

            }
        });

        holder.moment_opts_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (themeListCommentListener != null) {
                    themeListCommentListener.onclick(momentsBean, holder.getLayoutPosition());
                } else {
                    if (UserUtils.isVerifyCell()) {
//                        Intent intent;

                        if (MomentsBean.MOMENT_TYPE_THEME.equals(momentsBean.momentsType)) {
//                            intent = new Intent(view.getContext(), ThemeDetailActivity.class);
//                            if (momentsBean.parentMoment != null) {
//                                intent.putExtra(BundleConstants.THEMEPARTICIPATES, String.valueOf(momentsBean.parentMoment.joinTotal));
//                            } else {
//                                intent.putExtra(BundleConstants.THEMEPARTICIPATES, String.valueOf(momentsBean.joinTotal));
//                            }
//                            Bundle bundle = new Bundle();
//                            bundle.putString(ShareFileUtils.MATCH_FROM_PAGE_ID, pageId);
//                            bundle.putString(ShareFileUtils.MATCH_FROM_PAGE, page);
//                            bundle.putSerializable(TheLConstants.BUNDLE_KEY_MOMENT_BEAN, momentsBean);
//                            intent.putExtras(bundle);
//                            ((Activity) mContext).startActivityForResult(intent, TheLConstants.BUNDLE_CODE_MOMENTS_ACTIVITY);
                            FlutterRouterConfig.Companion.gotoThemeDetails(momentsBean.momentsId);
                        } else {
//                            intent = new Intent(view.getContext(), MomentCommentActivity.class);
//                            intent.putExtra(BundleConstants.FROME_WRITE_COMMENT, MomentCommentActivity.WriteMomentComment);
                            FlutterRouterConfig.Companion.gotoMomentDetails(momentsBean.momentsId);
                        }

                        /**
                         * 埋点上报 评论
                         * **/
                        if (pageFrom.equals(GrowingIoConstant.ENTRY_FOLLOW_PAGE) || pageFrom.equals(GrowingIoConstant.ENTRY_NEARBY_PAGE)) {
                            traceMomentLog(page, "comment", helper.getAdapterPosition(), momentsBean.momentsId, momentsBean.momentsType, momentsBean.rank_id);

                        }
                    }
                }
            }
        });

        holder.moment_opts_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (momentsBean.momentsType.equals("ad")) {
                    showAdControlDialog(mContext, momentsBean);
                    filterAdMoment(getData());
                    notifyDataSetChanged();
                } else {
                    MomentUtils.clickMore(mContext, momentsBean, new PrivateOrPublicMomentSubscribe(momentsBean, MomentsAdapter.this));
                }

            }
        });

        holder.img_thumb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (momentsBean.secret != MomentsBean.IS_SECRET) {
                    ViewUtils.preventViewMultipleClick(v, 1000);
                    jumpToUserInfo(context, momentsBean.userId, "");
                }
            }
        });
        holder.iv_feed_recommend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (UserUtils.isVerifyCell()) {
                    recommendMoment(v.getContext(), momentsBean);

                    /**
                     * 埋点上报  分享
                     * **/
                    if (pageFrom.equals(GrowingIoConstant.ENTRY_FOLLOW_PAGE) || pageFrom.equals(GrowingIoConstant.ENTRY_NEARBY_PAGE)) {
                        traceMomentLog(page, "share", helper.getAdapterPosition(), momentsBean.momentsId, momentsBean.momentsType, momentsBean.rank_id);

                    } else if (pageFrom.equals(GrowingIoConstant.ENTRY_THEME_DETAIL_PAGE)) {
                        if (!TextUtils.isEmpty(momentsBean.commentId)) {
                            traceThemeReplyLog(page, "share_reply", momentsBean.momentsId, momentsBean.commentId);

                        } else {
                            traceThemeLog(page, "sharet_theme", momentsBean.momentsId);

                        }

                    }
                }
            }
        });

        holder.select_theme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(momentsBean.appSchemeUrl) && momentsBean.appSchemeUrl.contains(TheLConstants.SchemeConstant.SCHEME_PATH_THEMEDETAIL)) {
//                    Intent intent = new Intent();
//                    intent.setClass(TheLApp.getContext(), ThemeDetailActivity.class);
                    Uri uri = Uri.parse(momentsBean.appSchemeUrl);

                    String momentsId = uri.getQueryParameter("momentId");
//                    intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, momentsId);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    TheLApp.getContext().startActivity(intent);
                    FlutterRouterConfig.Companion.gotoThemeDetails(momentsId);
                } else {
                    if (momentsBean.momentsType.equals(MomentsBean.MOMENT_TYPE_AD)) {
                        final Intent intent = new Intent(mContext, WebViewActivity.class);
                        Bundle bundle = new Bundle();

                        if (!TextUtils.isEmpty(momentsBean.appSchemeUrl)) {
                            bundle.putString(BundleConstants.URL, momentsBean.appSchemeUrl);

                        } else {
                            bundle.putString(BundleConstants.URL, momentsBean.adUrl);
                        }
                        intent.putExtras(bundle);
                        mContext.startActivity(intent);
                    } else {
                        ThemeClassBean themeClassBean = MomentUtils.getThemeByType(momentsBean.themeClass);

                        Utils.gotoThemeTab(themeClassBean, mContext);
                    }
                }

            }
        });

        if (momentsBean.isMusicPlaying) {
            holder.img_play.setImageResource(R.mipmap.btn_feed_pause_big);
        } else {
            holder.img_play.setImageResource(R.mipmap.btn_feed_play_big);
        }

        holder.layout_moment_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.itemView.performClick();
            }
        });

        if (!isShowRecommendView || momentsBean.momentsType.equals(MomentsBean.MOMENT_TYPE_THEME) || pageFrom.equals(GrowingIoConstant.ENTRY_TIME_MACHINE)) {
            holder.edit_recommend.setVisibility(View.GONE);
        } else {
            holder.edit_recommend.setVisibility(View.VISIBLE);

        }

        holder.edit_recommend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (UserUtils.isVerifyCell()) {
                    Intent intent = new Intent(mContext, InputActivity.class);
                    intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_BEAN, momentsBean);
                    mContext.startActivity(intent);
                }
            }
        });

        holder.txt_update_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.checkUpdate(v.getContext());
            }
        });

        if (pageFrom.equals(GrowingIoConstant.ENTRY_TIME_MACHINE)) {
            holder.time_machine_rl.setVisibility(VISIBLE);
            holder.time_machine_tv.setText(momentsBean.expire);
            if (momentsBean.deleteFlag == 0) {//未恢复
                holder.time_machine_recover.setText(R.string.time_machine_vip_tips3);
                holder.time_machine_recover.setBackgroundResource(R.drawable.bg_follow_round_button_blue_time);
            } else {
                holder.time_machine_recover.setText(R.string.time_machine_vip_tips4);
                holder.time_machine_recover.setBackgroundResource(R.drawable.bg_follow_round_button_gray_time);
            }
            holder.time_machine_recover.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (timeMachineListener != null && momentsBean.deleteFlag == 0) {
                        timeMachineListener.onClick(holder.getAdapterPosition());
                    }
                }
            });
            holder.setVisibility(R.id.layout_moment_operations, GONE);
            holder.moment_opts_more.setVisibility(View.INVISIBLE);
        } else {
            holder.setVisibility(R.id.layout_moment_operations, VISIBLE);
            holder.moment_opts_more.setVisibility(VISIBLE);
        }
        /**
         * 埋点上报  曝光数据
         * **/
        if (pageFrom.equals(GrowingIoConstant.ENTRY_FOLLOW_PAGE) || pageFrom.equals(GrowingIoConstant.ENTRY_NEARBY_PAGE)) {
            traceMomentLog(page, "exposure", helper.getAdapterPosition(), momentsBean.momentsId, momentsBean.momentsType, momentsBean.rank_id);

        } else if (pageFrom.equals(GrowingIoConstant.ENTRY_THEME_DETAIL_PAGE)) {
            traceThemeReplyLog(page, "exposure_reply", momentsBean.momentsId, momentsBean.commentId);

        }

    }

    public void traceThemeLog(String page, String activity, String momentsId) {
        String latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
        String longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");
        try {
            LogInfoBean logInfoBean = new LogInfoBean();
            logInfoBean.page = page;
            logInfoBean.page_id = pageId;
            logInfoBean.activity = activity;
            logInfoBean.from_page = fromPage;
            logInfoBean.from_page_id = fromPageId;
            logInfoBean.lat = latitude;
            logInfoBean.lng = longitude;

            LogInfoBean.LogsDataBean logsDataBean = new LogInfoBean.LogsDataBean();
            logsDataBean.moment_id = momentsId;
            logInfoBean.data = logsDataBean;

            LiveLogUtils.getInstance().addLog(logInfoBean);

        } catch (Exception e) {

        }
    }

    public void traceThemeReplyLog(String page, String activity, String momentid, String from_theme_id) {
        String latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
        String longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");
        try {
            LogInfoBean logInfoBean = new LogInfoBean();
            logInfoBean.page = page;
            logInfoBean.page_id = pageId;
            logInfoBean.activity = activity;
            logInfoBean.from_page = fromPage;
            logInfoBean.from_page_id = fromPageId;
            logInfoBean.lat = latitude;
            logInfoBean.lng = longitude;

            LogInfoBean.LogsDataBean logsDataBean = new LogInfoBean.LogsDataBean();
            logsDataBean.from_theme_id = from_theme_id;
            logsDataBean.moment_id = momentid;
            logInfoBean.data = logsDataBean;

            LiveLogUtils.getInstance().addLog(logInfoBean);

        } catch (Exception e) {

        }
    }

    public void traceMomentLog(String page, String activity, int position, String momentsId, String momentsType, String rankid) {
        String latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
        String longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");
        try {
            LogInfoBean logInfoBean = new LogInfoBean();
            logInfoBean.page = page;
            logInfoBean.page_id = pageId;
            logInfoBean.activity = activity;
            logInfoBean.from_page = fromPage;
            logInfoBean.from_page_id = fromPageId;
            logInfoBean.lat = latitude;
            logInfoBean.lng = longitude;

            LogInfoBean.LogsDataBean logsDataBean = new LogInfoBean.LogsDataBean();
            logsDataBean.rank_id = rankid;
            logsDataBean.index = position;
            logsDataBean.moment_id = momentsId;
            logsDataBean.moment_type = momentsType;
            logInfoBean.data = logsDataBean;

            LiveLogUtils.getInstance().addLog(logInfoBean);

        } catch (Exception e) {

        }
    }

    public class MomentsViewHolder extends BaseViewHolder {

        @BindView(R.id.layout_moment_text)
        View layout_moment_text;

        @BindView(R.id.center_layout)
        RelativeLayout center_layout;

        @BindView(R.id.layout_multi_image)
        MultiImageViewGroup layout_multi_image;

        @BindView(R.id.layout_moment_music)
        View layout_moment_music;

        @BindView(R.id.layout_moment_video)
        View layout_moment_video;

        /**
         * head
         */
        @BindView(R.id.moment_user_name)
        TextView moment_user_name;

        @BindView(R.id.img_vip)
        ImageView moment_img_vip;

        @BindView(R.id.img_thumb)
        ImageView img_thumb;

        @BindView(R.id.moment_release_time)
        TextView moment_release_time;

        @BindView(R.id.txt_follow)
        FollowView txt_follow;

        @BindView(R.id.moment_opts_more)
        ImageView moment_opts_more;

        @BindView(R.id.moments_tag)
        ImageView moments_tag;

        /**
         * text
         */
        @BindView(R.id.moment_content_text)
        TextView moment_content_text;

        @BindView(R.id.txt_update_now)
        TextView txt_update_now;

        @BindView(R.id.txt_whole)
        TextView txt_whole;

        /**
         * music
         */
        @BindView(R.id.moment_content_music_pic)
        ImageView moment_content_music_pic;

        @BindView(R.id.img_play)
        ImageView img_play;

        @BindView(R.id.song_name)
        TextView song_name;

        @BindView(R.id.album_name)
        TextView album_name;

        @BindView(R.id.artist_name)
        TextView artist_name;

        /**
         * video
         */
        @BindView(R.id.moment_content_video_pic)
        ImageView moment_content_video_pic;

        @BindView(R.id.txt_play_times)
        TextView txt_play_times;

        @BindView(R.id.videoLoading)
        VideoLoadingLayout videoLoading;

        @BindView(R.id.img_video_play)
        ImageView img_video_play;

        /**
         * user card
         */
        @BindView(R.id.layout_moment_user_card)
        View layout_moment_user_card;

        @BindView(R.id.img_user_card)
        ImageView img_user_card;

        @BindView(R.id.img_user_card_avatar)
        ImageView img_user_card_avatar;

        @BindView(R.id.txt_user_card_nickname)
        TextView txt_user_card_nickname;

        @BindView(R.id.txt_user_card_info)
        TextView txt_user_card_info;

        @BindView(R.id.txt_user_card_intro)
        TextView txt_user_card_intro;

        @BindView(R.id.mask_user_card)
        LinearLayout mask_user_card;


        /**
         * comments
         */
        @BindView(R.id.lin_latest_comments)
        LinearLayout lin_latest_comments;

        /**
         * mention
         */
        @BindView(R.id.layout_moment_mention)
        View layout_moment_mention;

        @BindView(R.id.moment_mentioned_text)
        TextView moment_mentioned_text;

        /**
         * operations
         */
        //        @BindView(R.id.rv_likes)
        //        RecyclerView rv_likes;

        @BindView(R.id.moment_opts_emoji_txt)
        TextView moment_opts_emoji_txt;

        @BindView(R.id.moment_opts_comment_txt)
        TextView moment_opts_comment_txt;

        @BindView(R.id.moment_opts_comment)
        ImageView moment_opts_comment;

        @BindView(R.id.moment_opts_like)
        LikeButtonView moment_opts_like;

        @BindView(R.id.img_like_anim)
        ImageView img_like_anim;

        @BindView(R.id.iv_feed_recommend)
        ImageView iv_feed_recommend;

        /**
         * theme
         */
        @BindView(R.id.layout_moment_theme)
        View layout_moment_theme;

        @BindView(R.id.img_theme)
        ImageView img_theme;

        @BindView(R.id.txt_theme)
        TextView txt_theme;

        @BindView(R.id.select_theme)
        TextView select_theme;

        @BindView(R.id.rel_theme)
        RelativeLayout rel_theme;

        @BindView(R.id.imageView)
        ImageView imageView;

        @BindView(R.id.topic_comma_right_iv)
        ImageView topic_comma_right_iv;

        /**
         * theme participate
         */
        @BindView(R.id.layout_moment_theme_participate)
        View layout_moment_theme_participate;

        @BindView(R.id.txt_theme_title)
        TextView txt_theme_title;

        @BindView(R.id.txt_theme_desc)
        TextView txt_theme_desc;

        @BindView(R.id.txt_deleted)
        TextView txt_deleted;

        @BindView(R.id.theme_iv)
        ImageView theme_iv;

        /**
         * live user
         */
        @BindView(R.id.live_user_view)
        RecommendLiveUserView live_user_view;

        /**
         * recommend web
         */
        @BindView(R.id.recommend_web_view)
        RecommendWebView recommend_web_view;

        /**
         * MOMENT_TYPE_RECOMMEND
         */
        @BindView(R.id.txt_recommend_repeat_users)
        TextView txt_recommend_repeat_users;

        @BindView(R.id.lin_recommend_repeat_users)
        LinearLayout lin_recommend_repeat_users;

        /**
         * MOMENT_TYPE_LIVE
         */
        @BindView(R.id.layout_moment_live)
        View layout_moment_live;

        @BindView(R.id.img_live)
        ImageView img_live;

        @BindView(R.id.txt_livestatus)
        TextView txt_livestatus;

        @BindView(R.id.live_bg_ImageView)
        ImageView live_bg_ImageView;

        @BindView(R.id.moment_parent_view)
        MomentParentViewOld moment_parent_view;

        @BindView(R.id.root_ll)
        BlockRelativeLayout root_ll;

        @BindView(R.id.edit_recommend)
        TextView edit_recommend;

        @BindView(R.id.time_machine_rl)
        RelativeLayout time_machine_rl;

        @BindView(R.id.time_machine_tv)
        TextView time_machine_tv;

        @BindView(R.id.time_machine_recover)
        TextView time_machine_recover;

        public MomentsViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

        }
    }

    private BaseAdapter.OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(BaseAdapter.OnItemClickListener mOnItemClickListener) {
        this.mOnItemClickListener = mOnItemClickListener;
    }

    private TimeMachineListener timeMachineListener;

    public void setTimeMachineListener(TimeMachineListener timeMachineListener) {
        this.timeMachineListener = timeMachineListener;
    }

    public interface TimeMachineListener {
        void onClick(int position);
    }

    private void jumpToUserInfo(Context context, int userId, String filter) {
//        Intent intent = new Intent();
//        intent.setClass(context, UserInfoActivity.class);
//        intent.putExtra("fromPage", this.getClass().getName());
//        intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, userId + "");
//        context.startActivity(intent);
        FlutterRouterConfig.Companion.gotoUserInfo(userId + "");
    }

    private void likeMoment(String momentsId) {
        MomentLikeUtils.likeMoment(momentsId);
    }

    private void unWinkMoment(String momentsId) {
        MomentLikeUtils.unLikeMoment(momentsId);
    }

    private void gotoVideoPlayAct(MomentsBean momentsBean, View view, int adapterPosition) {
        if (view != null) {

            final VideoBean videoBean = getVideoFromMomentBean(momentsBean);

            if (momentsBean != null) {
                videoBean.winkFlag = momentsBean.winkFlag;
            }
            final int userId = momentsBean.userId;
            Bundle bundle = new Bundle();
            bundle.putSerializable(TheLConstants.BUNDLE_KEY_VIDEO_BEAN, videoBean);
            bundle.putString(TheLConstants.BUNDLE_KEY_USER_ID, userId + "");
            Intent intent = new Intent(view.getContext(), UserVideoListActivity.class);
            intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_BEAN, momentsBean);
            intent.putExtra(TheLConstants.BUNDLE_KEY_VIDEO_LIST_POSITION, adapterPosition);
            intent.putExtras(bundle);
            ((Activity) view.getContext()).startActivityForResult(intent, TheLConstants.RESULT_CODE_VIDEO_PLAY_COUNT);
        }

    }

    private VideoBean getVideoFromMomentBean(MomentsBean momentBean) {
        return Utils.getVideoFromMoment(momentBean);
    }

    private void updateShareTo(MomentsBean momentBean, BaseViewHolder helper) {
        if (momentBean.shareTo == MomentsBean.SHARE_TO_FRIENDS) {
            helper.setVisibility(R.id.share_to_img, View.VISIBLE);
            helper.setImageResource(R.id.share_to_img, R.mipmap.icn_post_friends);
            helper.setVisibility(R.id.iv_feed_recommend, View.VISIBLE);
        } else if (momentBean.shareTo == MomentsBean.SHARE_TO_ONLY_ME) {
            helper.setVisibility(R.id.share_to_img, View.VISIBLE);
            helper.setImageResource(R.id.share_to_img, R.mipmap.icn_post_privacy);
            helper.setVisibility(R.id.iv_feed_recommend, View.GONE);
        } else {
            helper.setVisibility(R.id.iv_feed_recommend, View.VISIBLE);
            helper.setVisibility(R.id.share_to_img, GONE);
        }
    }

    private void jumpToCommentPage(MomentsBean momentBean) {
        if (MomentsBean.MOMENT_TYPE_THEME.equals(momentBean.momentsType)) {
//            Intent intent = new Intent();
//            intent.setClass(TheLApp.getContext(), ThemeDetailActivity.class);
//            intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, momentBean.momentsId);
//            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            TheLApp.getContext().startActivity(intent);
            FlutterRouterConfig.Companion.gotoThemeDetails(momentBean.momentsId);
        } else {
//            intent.setClass(TheLApp.getContext(), MomentCommentActivity.class);
            FlutterRouterConfig.Companion.gotoMomentDetails(momentBean.momentsId);
        }
    }

    public static void filterUserMoments(List<MomentsBean> data) {
        if (data != null) {

            ListIterator<MomentsBean> iterator = data.listIterator();

            while (iterator.hasNext()) {

                MomentsBean momentsBean = iterator.next();

                if (BlackUtils.isUserBlock(String.valueOf(momentsBean.userId))) {

                    iterator.remove();

                }
            }
        }
    }

    public static void filterOneMoments(List<MomentsBean> data) {
        if (data != null) {

            ListIterator<MomentsBean> iterator = data.listIterator();

            while (iterator.hasNext()) {

                MomentsBean momentsBean = iterator.next();

                if (BlackUtils.isMomentBlock(String.valueOf(momentsBean.momentsId))) {

                    iterator.remove();

                }
            }
        }
    }

    public static void filterBlack(List<MomentsBean> data) {

        ListIterator<MomentsBean> iterator = data.listIterator();

        while (iterator.hasNext()) {

            MomentsBean momentsBean = iterator.next();

            if (BlackUtils.isBlack(String.valueOf(momentsBean.userId))) {
                iterator.remove();
            }
        }

    }

    public static void filterEquallyMoment(List<MomentsBean> oldData, List<MomentsBean> newData) {

        if (oldData != null && newData != null && oldData.size() > 0 && newData.size() > 0) {

            MomentsBean oldBean = oldData.get(oldData.size() - 1);

            MomentsBean newBean = newData.get(0);

            if (oldBean.momentsId.equals(newBean.momentsId)) {

                newData.remove(newBean);

            }
        }

    }

    public static void filterAdMoment(List<MomentsBean> data) {

        String oldJson = ShareFileUtils.getString(ShareFileUtils.AD_HIDE, "[]");

        L.d(TAG, " oldJson : " + oldJson);

        Type typeOfDest = new TypeToken<List<String>>() {
        }.getType();

        List<String> blackAdList = GsonUtils.getObjects(oldJson, typeOfDest);

        ListIterator<MomentsBean> iterator = data.listIterator();

        while (iterator.hasNext()) {

            MomentsBean momentsBean = iterator.next();

            for (int i = 0; i < blackAdList.size(); i++) {

                String momentId = blackAdList.get(i);

                if (momentsBean.momentsId.equals(momentId)) {

                    iterator.remove();

                }
            }
        }
    }

    private void recommendMoment(Context context, MomentsBean momentsBean) {
        final String userId = momentsBean.userId + "";
        if (MomentUtils.isBlackOrBlock(userId)) {
            return;
        }
        SharedPrefUtils.setBoolean(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.NEW_SHARE_MOMENT, true);
        final Intent intent = new Intent(context, SendCardActivity.class);
        intent.putExtra(TheLConstants.BUNDLE_KEY_INTENT_FROM, SendCardActivity.FROM_RECOMMEND_MOMENT);
        intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_BEAN, momentsBean);
        intent.putExtra(TheLConstants.BUNDLE_KEY_PAGE_FROM, pageFrom);

        context.startActivity(intent);
    }

    public void showAdControlDialog(final Context context, final MomentsBean momentsBean) {

        DialogUtils.showSelectionDialogWide((Activity) context, 3 / 4f, new String[]{"不感兴趣", "取消"}, new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                switch (position) {
                    case 0:
                        RequestBusiness.getInstance().postADIndifferent(momentsBean.momentsId);
                        MomentBlackUtils.blackThisMomentSuccess(momentsBean.momentsId);
                        break;
                    case 1:
                        break;
                    default:
                        break;
                }

                DialogUtils.dismiss();

            }
        }, false, 0, null);
    }

    private int getPositionByMomentId(String momentId) {

        List<MomentsBean> momentsBeans = getData();

        if (momentsBeans == null || momentsBeans.size() == 0) {
            return -1;
        }

        for (int i = 0; i < momentsBeans.size(); i++) {
            MomentsBean momentsBean = momentsBeans.get(i);

            if (momentsBean.momentsId.equals(momentId)) {
                return i;
            }
        }

        return -1;

    }

    public static class PrivateOrPublicMomentSubscribe extends InterceptorSubscribe<BaseDataBean> {

        private MomentsBean momentsBean;

        private RecyclerView.Adapter adapter;

        private boolean isPrivate;

        public PrivateOrPublicMomentSubscribe(MomentsBean momentsBean, RecyclerView.Adapter adapter) {
            this.momentsBean = momentsBean;
            this.adapter = adapter;
        }

        public void isPrivate(boolean isPrivate) {
            this.isPrivate = isPrivate;
        }

        @Override
        public void onNext(BaseDataBean data) {
            super.onNext(data);
            if (isPrivate) {
                momentsBean.shareTo = MomentsBean.SHARE_TO_ONLY_ME;
            } else {
                momentsBean.shareTo = MomentsBean.SHARE_TO_ALL;
            }
            adapter.notifyDataSetChanged();
        }
    }

}
