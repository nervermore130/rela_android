package com.thel.ui.widget.popupwindow;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.thel.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UnFollowPopupWindow extends PopupWindow {

    @BindView(R.id.content_tv)
    TextView content_tv;

    @BindView(R.id.ok_tv)
    TextView ok_tv;

    @BindView(R.id.cancel_tv)
    TextView cancel_tv;

    private Context mContext;

    private String nickName;

    private OnUnFollowListener mOnUnFollowListener;

    public UnFollowPopupWindow(Context context, String nickName) {
        this.mContext = context;
        this.nickName = nickName;
        init();
    }

    private void init() {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_un_follow_pw, null);
        ButterKnife.bind(this, view);
        setContentView(view);
        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        setFocusable(true);
        setAnimationStyle(R.style.dialogAnim);
        setBackgroundDrawable(new BitmapDrawable());
        setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        setTouchable(true);
        setOutsideTouchable(true);
        setBackgroundAlpha(0.5f);

        content_tv.setText(mContext.getString(R.string.un_follow_confirm, nickName));
    }


    @OnClick(R.id.cancel_tv) void onCancel() {
        dismiss();
    }

    @OnClick(R.id.ok_tv) void onConfirm() {
        if (mOnUnFollowListener != null) {
            mOnUnFollowListener.onUnFollow();
        }
        dismiss();
    }

    public void setOnUnFollowListener(OnUnFollowListener mOnUnFollowListener) {
        this.mOnUnFollowListener = mOnUnFollowListener;
    }

    public interface OnUnFollowListener {
        void onUnFollow();
    }

    @Override public void dismiss() {
        super.dismiss();
        setBackgroundAlpha(1.0f);
    }

    /**
     * 设置添加屏幕的背景透明度
     *
     * @param bgAlpha 屏幕透明度0.0-1.0 1表示完全不透明
     */
    public void setBackgroundAlpha(float bgAlpha) {
        WindowManager.LayoutParams lp = ((Activity) mContext).getWindow()
                .getAttributes();
        lp.alpha = bgAlpha;
        ((Activity) mContext).getWindow().setAttributes(lp);
    }

}
