package com.thel.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import androidx.appcompat.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.View;

import com.thel.R;
import com.thel.constants.TheLConstants;
import com.thel.utils.ShareFileUtils;

/**
 * Created by waiarl on 2018/2/1.
 */

public class UserLevelImageView extends AppCompatImageView {
    private final Context mContext;
    protected int level = -1;

    public UserLevelImageView(Context context) {
        this(context, null);
    }

    public UserLevelImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public UserLevelImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
       // getAttrs(attrs);

    }

    private void init() {

    }

    private void getAttrs(AttributeSet attrs) {
        final TypedArray ta = mContext.obtainStyledAttributes(attrs, R.styleable.UserLevelImageView);
        final int level = ta.getInteger(R.styleable.UserLevelImageView_user_level, -1);
        ta.recycle();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (level >= 0 && level < TheLConstants.VIP_LEVEL_RES.length) {
            final int height = getMeasuredHeight();
            final int widht = getLevelImageWidth(height, level);
            setMeasuredDimension(widht, height);
        }
    }

    public UserLevelImageView initView(int level) {
        if (level >= TheLConstants.USER_LEVEL_RES.length) {
            level = TheLConstants.USER_LEVEL_RES.length - 1;
        }
        if (level < 0) {
            setVisibility(View.GONE);
            return this;
        }

        int levelPrivileges = ShareFileUtils.getInt(com.thel.utils.ShareFileUtils.LEVEL_PRIVILEGES, 1);

        if (levelPrivileges == 1 && level > 35) {
            level = 35;
        }

        setImageResource(TheLConstants.USER_LEVEL_RES[level]);
        final int height = getLayoutParams().height;
        final int width = getLevelImageWidth(height, level);
        getLayoutParams().width = width;
        return this;
    }

    public UserLevelImageView initView(int level, int height) {
        getLayoutParams().height = height;
        return initView(level);
    }

    /**
     * 根据icon高度和用户等级计算 icon的宽度
     *
     * @param height
     * @param level
     * @return
     */
    public static int getLevelImageWidth(int height, int level) {
        float width = height * 152f / 72f;
        if (level >= 30) {
            width = height * 264 / 72;
        } else if (level >= 26) {
            width = height * 208 / 72;
        }else if (level==0){
            width =height*214/72;
        }
        return (int) width;
    }
}
