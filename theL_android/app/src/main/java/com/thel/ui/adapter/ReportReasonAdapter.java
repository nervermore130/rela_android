package com.thel.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReportReasonAdapter extends BaseAdapter {

    private List<ReasonBean> reasons;
    private LayoutInflater mInflater;
    private int selected = -1;

    public ReportReasonAdapter(String[] reasons) {
        mInflater = (LayoutInflater) TheLApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.reasons = new ArrayList<ReasonBean>();
        for (int i = 0; i < reasons.length; i++) {
            ReasonBean reasonBean = new ReasonBean(reasons[i],i+1);
            this.reasons.add(reasonBean);
        }
    }

    public ReportReasonAdapter(List<ReasonBean> reasons) {
        mInflater = (LayoutInflater) TheLApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.reasons = reasons;
    }


    @Override
    public int getCount() {
        return reasons.size();
    }

    @Override
    public Object getItem(int position) {
        return reasons.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        HoldView holdView = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.select_report_reason_listitem, parent, false);
            holdView = new HoldView();
            holdView.selection_box = convertView.findViewById(R.id.selection_box);
         /*   holdView.portrait = (ImageView) convertView.findViewById(R.id.portrait);
            holdView.portrait.setVisibility(View.GONE);*/
            holdView.nickname = convertView.findViewById(R.id.nickname);
            convertView.setTag(holdView); // 把holdview缓存下来
        } else {
            holdView = (HoldView) convertView.getTag();
        }

        holdView.nickname.setText(reasons.get(position).reasonDesc);
        if (position == selected) {
            holdView.selection_box.setImageResource(R.mipmap.icn_selected);
        } else {
            holdView.selection_box.setImageResource(R.mipmap.icn_selection);
        }
        return convertView;
    }

    public void select(int position) {
        selected = position;
        notifyDataSetChanged();
    }

    public String getReasonType(int position) {
        return String.valueOf(reasons.get(position).reasonType);
    }

    class HoldView {
        ImageView selection_box;
        // ImageView portrait;
        TextView nickname;
    }

    public static class ReasonBean {

        public ReasonBean(String reasonDesc,int reasonType) {
            this.reasonDesc = reasonDesc;
            this.reasonType = reasonType;
        }

        public String reasonDesc;

        public int reasonType;
    }

}
