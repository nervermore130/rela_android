package com.thel.ui.widget;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.RelativeLayout;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SizeUtils;

public class SendGiftMentionGuideLayout extends RelativeLayout {

    private static final String TAG = "SendGiftMentionGuideLayout";
    private AutoSplitTextView content_tv;

    public SendGiftMentionGuideLayout(Context context) {
        super(context);

        init(context);

    }

    public SendGiftMentionGuideLayout(Context context, AttributeSet attrs) {
        super(context, attrs);

        init(context);

    }

    public SendGiftMentionGuideLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init(context);

    }

    private void init(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_live_gift_send_guide, this, true);
        content_tv = view.findViewById(R.id.content_tv);
        setEmojiToTextView();
    }

    private ValueAnimator valueAnimator;

    private void startAnimator(final View view) {

        int startY = 0;

        int endY = SizeUtils.dip2px(getContext(), 10);

        valueAnimator = ValueAnimator.ofInt(startY, endY, startY);

        valueAnimator.setDuration(1000);

        valueAnimator.setInterpolator(new LinearInterpolator());

        valueAnimator.setRepeatCount(ValueAnimator.INFINITE);

        valueAnimator.setRepeatMode(ValueAnimator.RESTART);

        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int offset = (int) animation.getAnimatedValue();
                view.setY(offset);
            }
        });

        valueAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                if (view != null) {
                    view.setLayerType(LAYER_TYPE_HARDWARE, null);
                }
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (view != null) {
                    view.setLayerType(LAYER_TYPE_NONE, null);
                }

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        valueAnimator.start();

    }

    private void stopAnimator() {
        if (valueAnimator != null) {
            valueAnimator.cancel();
            valueAnimator = null;
        }
    }

    public void show() {

        SendGiftMentionGuideLayout.this.setVisibility(View.VISIBLE);

        startAnimator(this);

        postDelayed(new Runnable() {
            @Override
            public void run() {
                stopAnimator();
                SendGiftMentionGuideLayout.this.setVisibility(View.INVISIBLE);
                /***第一次显示连击礼物的离手引导*/
                 ShareFileUtils.setBoolean(ShareFileUtils.LIVE_GIFT_GIFT_LIAN, true);

            }
        }, 5000);

    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        L.d(TAG, " onDetachedFromWindow ");

        stopAnimator();
    }

    private void setEmojiToTextView() {
        int unicodeJoy = 0x1F381;
        String emojiString = getEmojiStringByUnicode(unicodeJoy);
        content_tv.setText(TheLApp.context.getText(R.string.support_the_broadcaster) + emojiString);
    }

    private String getEmojiStringByUnicode(int unicode) {
        return new String(Character.toChars(unicode));
    }
}
