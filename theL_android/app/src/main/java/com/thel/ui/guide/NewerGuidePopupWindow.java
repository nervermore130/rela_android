package com.thel.ui.guide;

import android.content.Context;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Build;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;

import com.thel.R;

/**
 * Created by waiarl on 2018/2/3.
 */

public class NewerGuidePopupWindow extends PopupWindow {
    private final Context mContext;
    private NewerGuideView guideView;

    public NewerGuidePopupWindow(Context context) {
        this(context, null);
    }

    public NewerGuidePopupWindow(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NewerGuidePopupWindow(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }

    private void init() {
        guideView = new NewerGuideView(mContext);
        setContentView(guideView);
        setOutsideTouchable(false);
        setFocusable(false);
        setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.color.transparent));
        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
    }

    public NewerGuidePopupWindow initPopup(final View targetView, int guideRes, int guideWidth, int guideHeight, RectF rectF, String[] guideText, PointF offsetPointF) {
        guideView.initView(guideRes, guideWidth, guideHeight, rectF, guideText, offsetPointF);
        guideView.setGuideClickListener(new NewerGuideView.GuideClickListener() {
            @Override
            public void clickTarget() {
                /***一下两行代码顺序最好不要调换***/
                dismiss();
                if (targetView != null) {
//                    targetView.callOnClick();
                }
            }

            @Override
            public void clickOutSide() {
                /***一下两行代码顺序最好不要调换***/
                dismiss();
                if (targetView != null) {
//                    targetView.callOnClick();
                }
            }
        });
        return this;
    }

    public NewerGuideView getContentView() {
        return guideView;
    }

    public NewerGuidePopupWindow initPopup(final View targetView, int guideRes, int guideWidth, int guideHeight, RectF rectF, String[] guideText, PointF offsetPointF, int guideGravity) {
        initPopup(targetView, guideRes, guideWidth, guideHeight, rectF, guideText, offsetPointF).setNewGuideGravity(guideGravity);
        return this;
    }

    public NewerGuidePopupWindow setNewGuideGravity(int gravity) {
        if (guideView != null) {
            guideView.setGravite(gravity);
        }
        return this;
    }

    public NewerGuidePopupWindow show(View view) {
        showAtLocation(view, Gravity.NO_GRAVITY, 0, 0);
        return this;
    }

    @Override
    public void showAsDropDown(View anchor) {
        if (Build.VERSION.SDK_INT == 24) {
            Rect rect = new Rect();
            anchor.getGlobalVisibleRect(rect);
            int h = anchor.getResources().getDisplayMetrics().heightPixels - rect.bottom;
            setHeight(h);
        }
        super.showAsDropDown(anchor);
    }

    @Override
    public void showAsDropDown(View anchor, int xoff, int yoff) {
        if (Build.VERSION.SDK_INT == 24) {
            Rect rect = new Rect();
            anchor.getGlobalVisibleRect(rect);
            int h = anchor.getResources().getDisplayMetrics().heightPixels - rect.bottom;
            setHeight(h);
        }
        super.showAsDropDown(anchor, xoff, yoff);
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }

    @Override
    public void setOnDismissListener(OnDismissListener onDismissListener) {
        super.setOnDismissListener(onDismissListener);
    }

}
