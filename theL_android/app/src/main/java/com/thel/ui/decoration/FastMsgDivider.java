package com.thel.ui.decoration;

import android.graphics.Rect;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.thel.modules.main.messages.adapter.FastEmojiAdapter;

/**
 * Created by liuyun on 2017/12/29.
 */

public class FastMsgDivider extends RecyclerView.ItemDecoration {

    private int space;

    public FastMsgDivider(int space) {
        this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {

        int position = parent.getChildLayoutPosition(view);

        FastEmojiAdapter fastEmojiAdapter = (FastEmojiAdapter) parent.getAdapter();

        int dataSize = fastEmojiAdapter.getData().size();

        outRect.left = space;

        if (position == dataSize - 1) {

            outRect.right = space;

        }

    }
}
