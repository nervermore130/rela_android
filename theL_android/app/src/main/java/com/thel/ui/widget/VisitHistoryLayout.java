package com.thel.ui.widget;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.Nullable;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.WhoSeenMeListBean;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.main.me.bean.LogInfoBean;
import com.thel.modules.main.me.match.eventcollect.collect.MatchLogUtils;
import com.thel.modules.others.VipConfigActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VisitHistoryLayout extends LinearLayout {

    @BindView(R.id.user_avatar_1) ImageView user_avatar_1;

    @BindView(R.id.user_avatar_2) ImageView user_avatar_2;

    @BindView(R.id.user_avatar_3) ImageView user_avatar_3;

    @BindView(R.id.user_avatar_4) ImageView user_avatar_4;

    @BindView(R.id.user_avatar_5) ImageView user_avatar_5;

    @BindView(R.id.image_rl) RelativeLayout image_rl;

    @BindView(R.id.content_tv) TextView content_tv;

    private String pageId;

    private String page;

    public VisitHistoryLayout(Context context) {
        super(context);
        initView(context);
    }

    public VisitHistoryLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public VisitHistoryLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_visit_history, this, true);

        ButterKnife.bind(this, view);

        showContent();
    }

    public void setImage(List<WhoSeenMeListBean.MoreGirlsBean> moreGirls) {

        if (moreGirls == null || moreGirls.size() == 0) {
            this.setVisibility(GONE);
            return;
        }

        image_rl.setVisibility(VISIBLE);

        user_avatar_5.setVisibility(VISIBLE);

        ImageLoaderManager.imageLoaderCircle(user_avatar_5, R.mipmap.bg_head_frame, moreGirls.get(0).avatar);

        if (moreGirls.size() >= 2) {
            user_avatar_4.setVisibility(VISIBLE);
            ImageLoaderManager.imageLoaderCircle(user_avatar_4, R.mipmap.bg_head_frame, moreGirls.get(1).avatar);
        }

        if (moreGirls.size() >= 3) {
            user_avatar_3.setVisibility(VISIBLE);
            ImageLoaderManager.imageLoaderCircle(user_avatar_3, R.mipmap.bg_head_frame, moreGirls.get(2).avatar);
        }

        if (moreGirls.size() >= 4) {
            user_avatar_2.setVisibility(VISIBLE);
            ImageLoaderManager.imageLoaderCircle(user_avatar_2, R.mipmap.bg_head_frame, moreGirls.get(3).avatar);
        }

        if (moreGirls.size() == 5) {
            user_avatar_1.setVisibility(VISIBLE);
            ImageLoaderManager.imageLoaderCircle(user_avatar_1, R.mipmap.bg_head_frame, moreGirls.get(4).avatar);
        }

    }

    public void showContent() {

        try {
            String content = TheLApp.context.getString(R.string.rela_premium_privileges);

            SpannableString spannableString = new SpannableString(content);

            int index = content.indexOf("50");

            spannableString.setSpan(new ForegroundColorSpan(TheLApp.context.getResources().getColor(R.color.live_gift_rank_header_sp_color)), index, index + 2, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

            content_tv.setText(spannableString);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @OnClick(R.id.rl_goto_recharge) void gotoBuyVip() {
        Intent intent = new Intent(getContext(), VipConfigActivity.class);
        intent.putExtra("fromPage", "visit.who");
        intent.putExtra("fromPageId", pageId);
        getContext().startActivity(intent);
        setReportLogs(pageId);

    }


    public void setReportPage(String pageId, String page) {
        this.pageId = pageId;
        this.page = page;

    }

    public void setReportLogs(String pageId) {
        LogInfoBean logInfoBean = new LogInfoBean();
        logInfoBean.page = "visit.who";
        logInfoBean.page_id = pageId;
        logInfoBean.activity = "visitclickvip";

        MatchLogUtils.getInstance().addLog(logInfoBean);


    }
}
