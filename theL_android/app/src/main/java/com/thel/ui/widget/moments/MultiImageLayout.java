package com.thel.ui.widget.moments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.ImgShareBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.constants.TheLConstants;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.main.home.moments.photo.UserInfoPhotoActivity;
import com.thel.ui.widget.HalfSquareImageView;
import com.thel.ui.widget.SquareImageView;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by liuyun on 2017/9/22.
 */

public class MultiImageLayout extends LinearLayout {

    @BindView(R.id.layout_top) LinearLayout layout_top;

    @BindView(R.id.layout_center) LinearLayout layout_center;

    @BindView(R.id.layout_bottom) LinearLayout layout_bottom;

    @BindView(R.id.half_iv) HalfSquareImageView half_iv;

    @BindView(R.id.layout_top_left) SquareImageView layout_top_left;

    @BindView(R.id.layout_top_right) SquareImageView layout_top_right;

    @BindView(R.id.layout_center_left) SquareImageView layout_center_left;

    @BindView(R.id.layout_center_right_top) SquareImageView layout_center_right_top;

    @BindView(R.id.layout_center_right_bottom) SquareImageView layout_center_right_bottom;

    @BindView(R.id.layout_bottom_left) SquareImageView layout_bottom_left;

    @BindView(R.id.layout_bottom_center) SquareImageView layout_bottom_center;

    @BindView(R.id.layout_bottom_right) SquareImageView layout_bottom_right;

    private MomentsBean momentsBean;

    private boolean isInit = false;

    public MultiImageLayout(Context context) {
        super(context);
        initView();
    }

    public MultiImageLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public MultiImageLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.layout_multi_image, this, true);
        ButterKnife.bind(this, view);

    }

    public void setData(MomentsBean momentsBean) {

        this.momentsBean = momentsBean;

        layout_top.setVisibility(View.GONE);

        layout_center.setVisibility(View.GONE);

        layout_bottom.setVisibility(View.GONE);

        half_iv.setVisibility(View.GONE);

        String[] urlArray = momentsBean.imageUrl.split(",");

        switch (urlArray.length) {
            case 1:
                layout_top_right.setVisibility(View.GONE);
                layout_top.setVisibility(View.VISIBLE);
                layout_top_left.setVisibility(View.VISIBLE);
                ImageLoaderManager.imageLoader(layout_top_left, urlArray[0]);
                break;
            case 2:
                layout_top.setVisibility(View.VISIBLE);
                layout_top_left.setVisibility(View.VISIBLE);
                layout_top_right.setVisibility(View.VISIBLE);
                ImageLoaderManager.imageLoader(layout_top_left, R.drawable.bg_waterfull_item_shape, urlArray[0]);
                ImageLoaderManager.imageLoader(layout_top_right, R.drawable.bg_waterfull_item_shape, urlArray[1]);

                break;
            case 3:
                layout_top.setVisibility(View.VISIBLE);
                layout_top_left.setVisibility(View.VISIBLE);
                layout_top_right.setVisibility(View.VISIBLE);
                ImageLoaderManager.imageLoader(layout_top_left, R.drawable.bg_waterfull_item_shape, urlArray[0]);
                ImageLoaderManager.imageLoader(layout_top_right, R.drawable.bg_waterfull_item_shape, urlArray[1]);

                half_iv.setVisibility(View.VISIBLE);

                ImageLoaderManager.imageLoader(half_iv, R.drawable.bg_waterfull_item_shape, urlArray[1]);

                break;
            case 4:
                layout_top.setVisibility(View.VISIBLE);
                layout_top_left.setVisibility(View.VISIBLE);
                layout_top_right.setVisibility(View.VISIBLE);
                ImageLoaderManager.imageLoader(layout_top_left, R.drawable.bg_waterfull_item_shape, urlArray[0]);
                ImageLoaderManager.imageLoader(layout_top_right, R.drawable.bg_waterfull_item_shape, urlArray[1]);

                layout_bottom.setVisibility(View.VISIBLE);
                layout_bottom_left.setVisibility(View.VISIBLE);
                layout_bottom_center.setVisibility(View.VISIBLE);

                ImageLoaderManager.imageLoader(layout_bottom_left, R.drawable.bg_waterfull_item_shape, urlArray[2]);
                ImageLoaderManager.imageLoader(layout_bottom_center, R.drawable.bg_waterfull_item_shape, urlArray[3]);


                break;
            case 5:
                layout_top.setVisibility(View.VISIBLE);
                layout_top_left.setVisibility(View.VISIBLE);
                layout_top_right.setVisibility(View.VISIBLE);
                ImageLoaderManager.imageLoader(layout_top_left, R.drawable.bg_waterfull_item_shape, urlArray[0]);
                ImageLoaderManager.imageLoader(layout_top_right, R.drawable.bg_waterfull_item_shape, urlArray[1]);

                layout_bottom.setVisibility(View.VISIBLE);
                layout_bottom_left.setVisibility(View.VISIBLE);
                layout_bottom_center.setVisibility(View.VISIBLE);
                layout_bottom_right.setVisibility(VISIBLE);

                ImageLoaderManager.imageLoader(layout_bottom_left, R.drawable.bg_waterfull_item_shape, urlArray[2]);
                ImageLoaderManager.imageLoader(layout_bottom_center, R.drawable.bg_waterfull_item_shape, urlArray[3]);
                ImageLoaderManager.imageLoader(layout_bottom_right, R.drawable.bg_waterfull_item_shape, urlArray[4]);

                break;
            case 6:
                layout_center.setVisibility(VISIBLE);
                layout_center_left.setVisibility(VISIBLE);
                layout_center_right_top.setVisibility(VISIBLE);
                layout_center_right_bottom.setVisibility(VISIBLE);

                ImageLoaderManager.imageLoader(layout_center_left, R.drawable.bg_waterfull_item_shape, urlArray[0]);
                ImageLoaderManager.imageLoader(layout_center_right_top, R.drawable.bg_waterfull_item_shape, urlArray[1]);
                ImageLoaderManager.imageLoader(layout_center_right_bottom, R.drawable.bg_waterfull_item_shape, urlArray[2]);


                layout_bottom.setVisibility(View.VISIBLE);
                layout_bottom_left.setVisibility(View.VISIBLE);
                layout_bottom_center.setVisibility(View.VISIBLE);
                layout_bottom_right.setVisibility(VISIBLE);

                ImageLoaderManager.imageLoader(layout_bottom_left, R.drawable.bg_waterfull_item_shape, urlArray[3]);
                ImageLoaderManager.imageLoader(layout_bottom_center, R.drawable.bg_waterfull_item_shape, urlArray[4]);
                ImageLoaderManager.imageLoader(layout_bottom_right, R.drawable.bg_waterfull_item_shape, urlArray[5]);
                break;

        }

        isInit = true;

    }

    @OnClick(R.id.half_iv) void onClickHalfImageView(View v) {
        gotoPhotoActivity(v, 2, momentsBean);
    }

    @OnClick(R.id.layout_top_left) void onClickTopLeft(View v) {
        gotoPhotoActivity(v, 0, momentsBean);
    }

    @OnClick(R.id.layout_top_right) void onClickTopRight(View v) {
        gotoPhotoActivity(v, 1, momentsBean);
    }

    @OnClick(R.id.layout_center_left) void onClickCenterLeft(View v) {
        gotoPhotoActivity(v, 0, momentsBean);
    }

    @OnClick(R.id.layout_center_right_top) void onClickCenterRight_top(View v) {
        gotoPhotoActivity(v, 1, momentsBean);
    }

    @OnClick(R.id.layout_center_right_bottom) void onClickCenterRightBottom(View v) {
        gotoPhotoActivity(v, 2, momentsBean);
    }

    @OnClick(R.id.layout_bottom_left) void onClickBottomLeft(View v) {
        gotoPhotoActivity(v, 3, momentsBean);
    }

    @OnClick(R.id.layout_bottom_center) void onClickBottomCenter(View v) {
        gotoPhotoActivity(v, 4, momentsBean);
    }

    @OnClick(R.id.layout_bottom_right) void onClickBottomRight(View v) {
        gotoPhotoActivity(v, 5, momentsBean);
    }

    private void gotoPhotoActivity(View v, int position, MomentsBean momentBean) {
        ViewUtils.preventViewMultipleClick(v, 1000);
        String[] photoUrls = momentBean.imageUrl.split(",");
        Intent intent = new Intent(TheLApp.getContext(), UserInfoPhotoActivity.class);
        ImgShareBean bean = Utils.getImageShareBean(momentBean);
        Bundle bundle = new Bundle();
        bundle.putSerializable(TheLConstants.BUNDLER_KEY_IMAGESHAREBEAN, bean);
        intent.putExtras(bundle);
        ArrayList<String> photos = new ArrayList<>();
        for (int i = 0; i < photoUrls.length; i++) {
            photos.add(photoUrls[i]);
        }
        intent.putStringArrayListExtra(TheLConstants.BUNDLE_KEY_PHOTOS, photos);
        intent.putExtra(TheLConstants.BUNDLE_KEY_RELA_ID, momentBean.nickname);
        intent.putExtra(TheLConstants.IS_WATERMARK, true);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("position", position);
        TheLApp.getContext().startActivity(intent);
    }


}
