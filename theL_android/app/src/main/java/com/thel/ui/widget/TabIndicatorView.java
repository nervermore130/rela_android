package com.thel.ui.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import com.thel.R;
import com.thel.utils.SizeUtils;
import com.thel.utils.Utils;

public class TabIndicatorView extends View {

    private Paint mPaint;

    private float mOffset;

    private int tabWidth;

    private int page = 3;

    public TabIndicatorView(Context context) {
        super(context);

        init();
    }

    public TabIndicatorView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        mPaint = new Paint();
        mPaint.setColor(getResources().getColor(R.color.tab_bottom_border_color));
        tabWidth = SizeUtils.dip2px(getContext(), 80);
        if (Utils.isChaneseLanguage()) {
            setVisibility(View.VISIBLE);
        } else {
            setVisibility(View.INVISIBLE);
        }
    }

    public void setPage(int page) {
        this.page = page;
    }

    public void setOffset(float offset) {
        if (offset != 0) {
            mOffset = offset;
            invalidate();
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float left = (getMeasuredWidth() / page - tabWidth) / 2;
        canvas.drawRect(left + computeScrollWidth(), 0, left + computeScrollWidth() + tabWidth, getMeasuredHeight(), mPaint);
    }

    private float computeScrollWidth() {
        return getMeasuredWidth() / page * mOffset;
    }

}


