package com.thel.ui.widget;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.utils.L;
import com.thel.utils.ScreenUtils;
import com.thel.utils.SizeUtils;

public class BubbleGuideLayout extends RelativeLayout {

    private TextView content_tv;

    public BubbleGuideLayout(Context context) {
        super(context);
        init(context);
    }

    public BubbleGuideLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public BubbleGuideLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }


    private void init(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_bubble_guide, this, true);
        content_tv = view.findViewById(R.id.content_tv);
    }

    private ValueAnimator valueAnimator;

    private void startAnimator(final View view) {

        final int y = ScreenUtils.getScreenHeight(TheLApp.context) - SizeUtils.dip2px(TheLApp.context, 115);

        int startY = 0;

        int endY = SizeUtils.dip2px(getContext(), 10);

        valueAnimator = ValueAnimator.ofInt(startY, endY, startY);

        valueAnimator.setDuration(1000);

        valueAnimator.setInterpolator(new LinearInterpolator());

        valueAnimator.setRepeatCount(ValueAnimator.INFINITE);

        valueAnimator.setRepeatMode(ValueAnimator.RESTART);

        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int offset = (int) animation.getAnimatedValue();
                view.setY(offset + y);
            }
        });

        valueAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                if (view != null) {
                    view.setLayerType(LAYER_TYPE_HARDWARE, null);
                }
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (view != null) {
                    view.setLayerType(LAYER_TYPE_NONE, null);
                }

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        valueAnimator.start();

    }

    private void stopAnimator() {
        if (valueAnimator != null) {
            valueAnimator.cancel();
            valueAnimator = null;
        }
    }

    public void show() {

        BubbleGuideLayout.this.setVisibility(View.VISIBLE);

        startAnimator(this);

        postDelayed(new Runnable() {
            @Override
            public void run() {
                stopAnimator();
                BubbleGuideLayout.this.setVisibility(View.INVISIBLE);
            }
        }, 5000);

    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        stopAnimator();
    }

}
