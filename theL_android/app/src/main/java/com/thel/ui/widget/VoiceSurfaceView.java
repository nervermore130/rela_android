package com.thel.ui.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.thel.utils.ScreenUtils;

import java.util.Random;

/**
 * Created by liuyun on 2018/1/30.
 */

public class VoiceSurfaceView extends SurfaceView implements SurfaceHolder.Callback {

    private static final String TAG = "SinCurveLine";

    private Paint mPaint;
    private SurfaceHolder mSurfaceHolder;
    private Canvas mCanvas;
    private Rect mRect = new Rect();
    private int measuredWidth = 0;
    private int measuredHeight = 0;
    private float y;
    private double scaleTime = 0.1;
    private float centerY = 0;
    private float centerX = 0;

    private float[] pointsRed;

    private float[] pointsYellow;

    private float[] pointsBlue;

    private float[] pointsViolet;

    private boolean isPause = false;

    public VoiceSurfaceView(Context context) {
        this(context, null);
        Log.d(TAG, "SinCurveLine");
    }

    public VoiceSurfaceView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    @Override protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        measuredWidth = getMeasuredWidth();
        measuredHeight = getMeasuredHeight();
        centerY = measuredHeight * 0.5f;
        centerX = measuredWidth * 0.5f;
        mRect.set(0, 0, measuredWidth, measuredHeight);
    }

    public VoiceSurfaceView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mSurfaceHolder = getHolder();
        mSurfaceHolder.setFormat(PixelFormat.TRANSLUCENT);
        mSurfaceHolder.addCallback(this);
        setZOrderOnTop(true);
    }

    private void init() {

        mPaint = new Paint();
        mPaint.setColor(Color.RED);
        mPaint.setAntiAlias(true);
        mPaint.setStrokeWidth(4);
        mPaint.setStyle(Paint.Style.FILL);

        int screenWidth = ScreenUtils.getScreenWidth(getContext());

        pointsRed = new float[screenWidth * 4];

        pointsYellow = new float[screenWidth * 4];

        pointsBlue = new float[screenWidth * 4];

        pointsViolet = new float[screenWidth * 4];

    }

    /**
     * 角度转换成弧度
     *
     * @param degree
     * @return
     */
    private double degreeToRad(double degree) {
        return degree * Math.PI / 360;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

        init();

        new DrawThread().start();

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    private boolean isRunning = true;

    private float time = 0;

    class DrawThread extends Thread {
        @Override public void run() {
            super.run();

            while (isRunning) {

                long startTime = System.currentTimeMillis();

                time += 0.50;

                if (!isPause) {

                    draw();

                }

                long endTime = System.currentTimeMillis();

                long frameRate = endTime - startTime;

                Log.d("VoiceSurfaceView", " frameRate : " + frameRate);

                if (frameRate < 20) {
                    frameRate = 20;
                }

                try {
                    Thread.sleep(frameRate);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void draw() {

        synchronized (this) {

            if (measuredWidth < 0 || measuredHeight < 0) {
                return;
            }

            mCanvas = mSurfaceHolder.lockCanvas();

            if (mCanvas == null) {
                return;
            }

            mCanvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);

            int x = 0;

            while (x < measuredWidth) {

                //角度转换成弧度
                double rad = degreeToRad(x);

                double xScale = -Math.pow(x / centerX - 1.0, 2.0) + 1;

                if (x % 2 == 0) {
                    pointsRed[x] = pointsBlue[x] = pointsYellow[x] = pointsViolet[x] = x;
                } else {
                    y = getYHeight(centerY, rad, xScale, 1);
                    pointsRed[x] = y;

                    y = getYHeight(centerY, rad, xScale, 0.75);
                    pointsBlue[x] = y;

                    y = getYHeight(centerY, rad, xScale, 0.5);
                    pointsYellow[x] = y;

                    y = getYHeight(centerY, rad, xScale, 0.25);
                    pointsViolet[x] = y;
                }

                x++;
            }

            mPaint.setColor(0XFFF23288);
            mCanvas.drawPoints(pointsRed, mPaint);

            mPaint.setColor(0XFFF7D446);
            mCanvas.drawPoints(pointsBlue, mPaint);

            mPaint.setColor(0XFF0043FF);
            mCanvas.drawPoints(pointsYellow, mPaint);

            mPaint.setColor(0XFFAE00FF);
            mCanvas.drawPoints(pointsViolet, mPaint);

            mPaint.setColor(0XFF66DADB);
            mCanvas.drawLine(0, centerY, measuredWidth, centerY, mPaint);

            mSurfaceHolder.unlockCanvasAndPost(mCanvas);

        }
    }

    public void setScaleTime(double scaleTime) {
        this.scaleTime = scaleTime;
    }

    private double getScaleTime() {
        return Math.abs(Math.sin(time));
    }

    private float getYHeight(double centerY, double rad, double xScale, double yScale) {
        return (float) (centerY - Math.sin(rad + time) * xScale * centerY * yScale * scaleTime);
    }

    public void pause() {
        isPause = true;
    }

    public void resume() {
        isPause = false;
    }

    public void release() {
        isRunning = false;
    }

}
