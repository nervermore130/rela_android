package com.thel.ui.widget

import android.animation.Animator
import android.animation.ValueAnimator
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import com.thel.R
import com.thel.manager.ImageLoaderManager
import com.thel.modules.live.bean.LiveRoomMsgBean
import com.thel.utils.L
import com.thel.utils.StringUtils
import kotlinx.android.synthetic.main.layout_video_bottom.view.avatar_iv
import kotlinx.android.synthetic.main.view_live_level_up.view.*

class LiveLevelUpView : LinearLayout {

    private val tagName: String = "LiveLevelUpView"

    private var animator: ValueAnimator? = ValueAnimator.ofFloat(0f, 1f, 1f, 1f, 0f)

    private var isViewShowing: Boolean = false


    constructor(context: Context?) : this(context, null)
    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        context?.let { initView(it) }
    }

    private val cacheData = ArrayList<LiveRoomMsgBean>()

    fun initView(context: Context) {

        LayoutInflater.from(context).inflate(R.layout.view_live_level_up, this, true)

    }

    fun showView(liveRoomMsgBean: LiveRoomMsgBean) {

        L.d(tagName, " showView : $isViewShowing")

        if (isViewShowing) {
            cacheData.add(liveRoomMsgBean)
            return
        }

        isViewShowing = true

        ImageLoaderManager.imageLoaderDefaultCircle(avatar_iv, R.mipmap.icon_user, liveRoomMsgBean.avatar)

        ImageLoaderManager.imageLoader(level_up_bg_iv, R.mipmap.all_upgrade_noti)

        level_iv.setLevel(liveRoomMsgBean.userLevel)

        context_tv.text = StringUtils.getString(R.string.promoted_to_level, liveRoomMsgBean.nickName, liveRoomMsgBean.userLevel)

        animator?.addUpdateListener {

            val offset = it.animatedValue as Float

            alpha = offset
        }

        animator?.addListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(animation: Animator?) {

            }

            override fun onAnimationEnd(animation: Animator?) {

                L.d(tagName, " onAnimationEnd : ")

                visibility = View.GONE

                isViewShowing = false

                if (cacheData.size > 0) {

                    L.d(tagName, " cacheData : ${cacheData.size}")

                    showView(cacheData.removeAt(cacheData.size - 1))

                }
            }

            override fun onAnimationCancel(animation: Animator?) {

            }

            override fun onAnimationStart(animation: Animator?) {

                L.d(tagName, " onAnimationStart : ")

                visibility = View.VISIBLE

            }

        })

        animator?.duration = 5000

        animator?.start()
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        animator?.cancel()
    }

}