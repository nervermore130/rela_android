package com.thel.ui.widget;

import android.content.Context;
import android.content.Intent;

import androidx.core.content.ContextCompat;
import androidx.appcompat.widget.AppCompatTextView;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.modules.main.home.moments.comment.MomentCommentActivity;
import com.thel.modules.main.home.moments.comment.MomentCommentReplyActivity;
import com.thel.modules.main.home.moments.theme.ThemeDetailActivity;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.ui.MyLinkMovementMethod;
import com.thel.utils.EmojiUtils;


/**
 * Created by setsail on 15/11/30.
 */
public class LatestCommentView extends AppCompatTextView {
    public LatestCommentView(Context context) {
        super(context);
    }

    public LatestCommentView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LatestCommentView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * @param nickName
     * @param comment
     * @param userId
     * @param id       当type=="commentReply"，则是评论id，当type=="moment"||"theme"，则就是日志id
     * @param type     moment:跳转到普通日志详情页 theme:跳转到话题详情页 commentReply:跳转到评论回复页
     */
    public void setText(String nickName, String comment, final String userId, final String id, final String type) {
        SpannableString str = EmojiUtils.getInstace(15).getExpressionString(TheLApp.getContext(), nickName + ": " + comment);

        str.setSpan(new ClickableSpan() {

            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setColor(ContextCompat.getColor(TheLApp.getContext(), R.color.text_color_green));
                ds.setUnderlineText(false); // 去掉下划线
            }

            @Override
            public void onClick(View widget) {
                // 通过tag来判断span的点击事件是否要执行，如果不执行，则要将tag置为true，以免下次点击失效
                if (widget.getTag() != null) {
                    if (!(Boolean) widget.getTag()) {
                        widget.setTag(true);
                        return;
                    }
                }
//                Intent intent = new Intent();
//                intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, userId);
//                intent.setClass(TheLApp.getContext(), UserInfoActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                TheLApp.getContext().startActivity(intent);
                FlutterRouterConfig.Companion.gotoUserInfo(userId);
            }
        }, 0, nickName.length() + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        str.setSpan(new ClickableSpan() {
            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setColor(ContextCompat.getColor(TheLApp.getContext(), R.color.moment_item_comment_color));
                ds.setUnderlineText(false); // 去掉下划线
            }

            @Override
            public void onClick(View widget) {
                // 通过tag来判断span的点击事件是否要执行，如果不执行，则要将tag置为true，以免下次点击失效
                if (widget.getTag() != null) {
                    if (!(Boolean) widget.getTag()) {
                        widget.setTag(true);
                        return;
                    }
                }
                if ("commentReply".equals(type)) {// 跳转到评论回复列表
//                    intent.setClass(TheLApp.getContext(), MomentCommentReplyActivity.class);
//                    intent.putExtra(TheLConstants.BUNDLE_KEY_COMMENT_ID, id);
//                    intent.putExtra(TheLConstants.BUNDLE_KEY_INTENT_FROM, MomentCommentReplyActivity.FROM_MOMENT);
                    FlutterRouterConfig.Companion.gotoCommentReply(id);
                } else {
                    if ("theme".equals(type)) {
//                        Intent intent = new Intent();
//
//                        intent.setClass(TheLApp.getContext(), ThemeDetailActivity.class);
//                        intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, id);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        TheLApp.getContext().startActivity(intent);
                        FlutterRouterConfig.Companion.gotoThemeDetails(id);
                    } else if ("moment".equals(type)) {
                        FlutterRouterConfig.Companion.gotoMomentDetails(id);
//                        intent.setClass(TheLApp.getContext(), MomentCommentActivity.class);
                    }
                }

            }
        }, nickName.length() + 1, str.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        setTag(true);
        super.setText(str);
        super.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
        super.setMaxLines(5);
        super.setEllipsize(TextUtils.TruncateAt.END);
        super.setMovementMethod(MyLinkMovementMethod.getInstance());
    }

    /**
     * 通过tag来控制span的点击事件，否则会出现span的click和view的longClick事件同时执行
     */
    public void disableSpanClick() {
        setTag(false);
    }
}
