package com.thel.ui.widget;

import android.content.Context;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.view.ViewCompat;
import androidx.core.view.ViewPropertyAnimatorListener;
import androidx.interpolator.view.animation.FastOutSlowInInterpolator;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;

/**
 * Created by the L on 2016/12/22.
 */

public class MyNearbyFABBehavior extends FloatingActionButton.Behavior {
    private static final Interpolator INTERPOLATOR = new FastOutSlowInInterpolator();

    public MyNearbyFABBehavior(Context context, AttributeSet attributeSet) {
        super();
    }

    @Override
    public boolean onStartNestedScroll(CoordinatorLayout coordinatorLayout, FloatingActionButton child, View directTargetChild, View target, int nestedScrollAxes) {
        return nestedScrollAxes == ViewCompat.SCROLL_AXIS_VERTICAL || super.onStartNestedScroll(coordinatorLayout, child, directTargetChild, target, nestedScrollAxes);
    }

    @Override
    public void onNestedScroll(CoordinatorLayout coordinatorLayout, FloatingActionButton child, View target, int dxConsumed, int dyConsumed, int dxUnconsumed, int dyUnconsumed) {
        super.onNestedScroll(coordinatorLayout, child, target, dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed);
        if (child instanceof MyFloatingActionButton) {
            final MyFloatingActionButton button = (MyFloatingActionButton) child;
            if (!button.isNeverShow) {
                if (dyConsumed > 0 && !button.mIsAnimatingOut && child.getVisibility() == View.VISIBLE) {
                    animateOut(button);
                    button.isFirstPageOut = false;
                } else if (dyConsumed < 0 && child.getVisibility() != View.VISIBLE) {
                    animateIn(button);
                }
            }

        }
    }

    //移出悬浮窗
    public static void animateOut(final MyFloatingActionButton child) {
        ViewCompat.animate(child).translationY(child.getHeight() + getMarginBottom(child)).setInterpolator(INTERPOLATOR).withLayer().setListener(new ViewPropertyAnimatorListener() {
            @Override
            public void onAnimationStart(View view) {
                child.mIsAnimatingOut = true;
            }

            @Override
            public void onAnimationEnd(View view) {
                child.mIsAnimatingOut = false;
                view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(View view) {
                child.mIsAnimatingOut = false;
            }
        });
    }

    //悬浮窗进来
    public static void animateIn(MyFloatingActionButton child) {
        child.setVisibility(View.VISIBLE);
        ViewCompat.animate(child).translationY(0).setInterpolator(INTERPOLATOR).withLayer().setListener(null);
    }

    static int getMarginBottom(View child) {
        int marginBottom = 0;
        final ViewGroup.LayoutParams param = child.getLayoutParams();
        if (param instanceof ViewGroup.MarginLayoutParams) {
            marginBottom = ((ViewGroup.MarginLayoutParams) param).bottomMargin;
        }
        return marginBottom;
    }
}
