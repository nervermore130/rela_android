package com.thel.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseDataBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.video.VideoBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.growingio.GrowingIoConstant;
import com.thel.imp.follow.FollowStatusChangedImpl;
import com.thel.imp.follow.FollowStatusChangedListener;
import com.thel.imp.like.MomentLikeUtils;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.main.home.moments.SendCardActivity;
import com.thel.modules.main.home.moments.comment.MomentCommentActivity;
import com.thel.modules.main.home.moments.winkcomment.WinkCommentsActivity;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestConstants;
import com.thel.network.service.DefaultRequestService;
import com.thel.ui.dialog.ActionSheetDialog;
import com.thel.ui.widget.LikeButtonView;
import com.thel.utils.BusinessUtils;
import com.thel.utils.DialogUtil;
import com.thel.utils.MD5Utils;
import com.thel.utils.MomentUtils;
import com.thel.utils.ScreenUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SharedPrefUtils;
import com.thel.utils.ViewUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.thel.utils.StringUtils.getString;

/**
 * @author liuyun
 * @date 2018/3/7
 */

public class VideoListAdapter extends RecyclerView.Adapter<VideoListAdapter.VideoListViewHolder> {

    private Context context;

    private LayoutInflater mLayoutInflater;

    private List<VideoBean> videoBeans = new ArrayList<>();

    public VideoListAdapter(Context context, List<VideoBean> videoBeans) {
        this.context = context;
        this.videoBeans.clear();
        this.videoBeans.addAll(videoBeans);
        mLayoutInflater = LayoutInflater.from(context);

    }


    @Override
    public VideoListViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = mLayoutInflater.inflate(R.layout.fragment_video3, viewGroup, false);
        return new VideoListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(VideoListViewHolder videoListViewHolder, int i) {

        VideoBean videoBean = videoBeans.get(i);

        initView(videoListViewHolder, videoBean);

        initListener(videoListViewHolder, videoBean);
    }

    @Override
    public int getItemCount() {
        return videoBeans.size();
    }

    private void initView(VideoListViewHolder videoListViewHolder, VideoBean videoBean) {

        final Activity activity = (Activity) context;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            videoListViewHolder.statusBar.setVisibility(View.VISIBLE);
        } else {
            videoListViewHolder.statusBar.setVisibility(View.GONE);
        }
        if (videoBean != null) {
            ImageLoaderManager.imageLoaderDefaultCircle(videoListViewHolder.avatar, R.mipmap.icon_user, videoBean.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE);
            videoListViewHolder.play_time.setText(getString(R.string.play_count, videoBean.playCount));
            videoListViewHolder.moment_opts_emoji_txt.setText(videoBean.winkNum + " " + TheLApp.getContext().getString(R.string.like_msgs_title));
            videoListViewHolder.moment_opts_comment_txt.setText(videoBean.commentNum + " " + TheLApp.getContext().getString(R.string.moment_comments_title));
            videoListViewHolder.txt_nickname.setText(videoBean.nickname);
            if (videoBean.text != null && videoBean.text.length() > 0) {
                MomentUtils.setMomentContent(videoListViewHolder.txt_desc, videoBean.text);
            }
            if (videoBean.winkFlag != MomentsBean.HAS_NOT_WINKED) {
                videoListViewHolder.moment_opts_like.setChecked(true, 1);
            } else {
                videoListViewHolder.moment_opts_like.setChecked(false, 1);
            }
            if (videoBean.isFollow != 0) {
                videoListViewHolder.tv_follow.setVisibility(View.GONE);
            }



            boolean isCollect = BusinessUtils.isCollected(videoBean.id);

            if (isCollect) {
                videoListViewHolder.img_collect.setImageResource(R.mipmap.icn_collection_press);
            } else {
                videoListViewHolder.img_collect.setImageResource(R.mipmap.icn_collection_normal);
            }

            float videoWidth = videoBean.pixelWidth;

            float videoHeight = videoBean.pixelHeight;

            float defaultScreenWidth = ScreenUtils.getScreenWidth(activity);

            float defaultScaleHeight = defaultScreenWidth / videoWidth * videoHeight;

            setVideoSize(videoListViewHolder, defaultScaleHeight, videoWidth, videoHeight);

            if (videoBean.playTime > 60) {//long video
                //                end_time.setText(DensityUtils.longToChronometer(videoBean.playTime * 1000));
                //                start_time.setVisibility(View.VISIBLE);
                //                end_time.setVisibility(View.VISIBLE);
                //                long_video_seek.setVisibility(View.VISIBLE);
            } else {//short video
                videoListViewHolder.short_video_seek.setVisibility(View.VISIBLE);
            }
            ImageLoaderManager.imageLoader(videoListViewHolder.thumb_iv, videoBean.image);

            if (videoWidth / videoHeight > 1) {
                videoListViewHolder.full_screen_iv.setVisibility(View.VISIBLE);
            } else {
                videoListViewHolder.full_screen_iv.setVisibility(View.GONE);
            }
        }
    }

    private void setVideoSize(VideoListViewHolder videoListViewHolder, float defaultScaleHeight, float videoWidth, float videoHeight) {

        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, (int) defaultScaleHeight);

        params.gravity = Gravity.CENTER;

        videoListViewHolder.texture_view.setLayoutParams(params);

        videoListViewHolder.video_rl.setLayoutParams(params);

        videoListViewHolder.content_ll.setVisibility(View.VISIBLE);

        videoListViewHolder.small_screen_iv.setVisibility(View.GONE);

        if (videoWidth / videoHeight > 1) {
            videoListViewHolder.full_screen_iv.setVisibility(View.VISIBLE);
        } else {
            videoListViewHolder.full_screen_iv.setVisibility(View.GONE);
        }
    }

    private void initListener(VideoListViewHolder videoListViewHolder, VideoBean videoBean) {

        MyOnClickListener myOnClickListener = new MyOnClickListener(videoListViewHolder, videoBean);

        videoListViewHolder.back.setOnClickListener(myOnClickListener);
        videoListViewHolder.more.setOnClickListener(myOnClickListener);
        videoListViewHolder.img_collect.setOnClickListener(myOnClickListener);
        videoListViewHolder.avatar.setOnClickListener(myOnClickListener);
        videoListViewHolder.moment_opts_comment_txt.setOnClickListener(myOnClickListener);
        videoListViewHolder.tv_follow.setOnClickListener(myOnClickListener);
        videoListViewHolder.iv_feed_recommend.setOnClickListener(myOnClickListener);
        videoListViewHolder.moment_opts_comment.setOnClickListener(myOnClickListener);
        videoListViewHolder.moment_opts_like.setOnClickListener(myOnClickListener);
        videoListViewHolder.video_rl.setOnClickListener(myOnClickListener);
        videoListViewHolder.full_screen_iv.setOnClickListener(myOnClickListener);
        videoListViewHolder.small_screen_iv.setOnClickListener(myOnClickListener);
        videoListViewHolder.moment_opts_emoji_txt.setOnClickListener(myOnClickListener);
        videoListViewHolder.pause_or_play_iv.setOnClickListener(myOnClickListener);
    }

    public class MyOnClickListener implements View.OnClickListener {

        private VideoListViewHolder videoListViewHolder;

        private VideoBean videoBean;

        public MyOnClickListener(VideoListViewHolder videoListViewHolder, VideoBean videoBean) {
            this.videoListViewHolder = videoListViewHolder;
            this.videoBean = videoBean;
        }

        @Override
        public void onClick(View v) {

            final Activity activity = (Activity) context;

            switch (v.getId()) {
                case R.id.pause_or_play_iv:
                    //                    play(isPlaying);
                    break;
                case R.id.video_rl:
                    //                    if (mKSYMediaPlayer != null && mKSYMediaPlayer.getDuration() != 0 && videoBean.playTime > 60) {
                    //                        if (videoListViewHolder.pause_or_play_iv.getVisibility() == View.VISIBLE) {
                    //                            videoListViewHolder.pause_or_play_iv.setVisibility(View.INVISIBLE);
                    //                            videoListViewHolder.long_video_seek.setVisibility(View.INVISIBLE);
                    //                            videoListViewHolder.start_time.setVisibility(View.INVISIBLE);
                    //                            videoListViewHolder.end_time.setVisibility(View.INVISIBLE);
                    //                        } else {
                    //                            videoListViewHolder.pause_or_play_iv.setVisibility(View.VISIBLE);
                    //                            videoListViewHolder.long_video_seek.setVisibility(View.VISIBLE);
                    //                            videoListViewHolder.start_time.setVisibility(View.VISIBLE);
                    //                            videoListViewHolder.end_time.setVisibility(View.VISIBLE);
                    //                        }
                    //                    }
                    break;
                case R.id.back:
                    activity.finish();
                    break;
                case R.id.more:
                    new ActionSheetDialog(activity)
                            .builder()
                            .setCancelable(true)
                            .setCanceledOnTouchOutside(true)
                            .addSheetItem(getString(R.string.stranger_report), ActionSheetDialog.SheetItemColor.RED,
                                    new ActionSheetDialog.OnSheetItemClickListener() {
                                        @Override
                                        public void onClick(int which) {
                                            MomentUtils.reportMoment(activity, videoBean.id);
                                        }
                                    })
                            .show();
                    break;
                case R.id.avatar:
                    if (videoBean != null) {
                        ViewUtils.preventViewMultipleClick(v, 1000);
                        jumpToUserInfo(activity, String.valueOf(videoBean.userId), "");
                    }
                    break;
                case R.id.tv_follow:
                    if (videoBean != null) {
                        FollowStatusChangedImpl.followUserWithNoDialog(videoBean.userId + "", FollowStatusChangedListener.ACTION_TYPE_FOLLOW, videoBean.nickname, videoBean.avatar);
                    }
                    break;
                case R.id.iv_feed_recommend:
                    if (videoBean != null) {
                        recommendMoment(activity, videoBean);
                    }
                    break;
                case R.id.moment_opts_comment:
                    if (videoBean != null) {
                        jumpToComment(activity, videoBean);
                    }
                    break;
                case R.id.moment_opts_like:
                    ViewUtils.preventViewMultipleClick(v, 1000);
                    if (MomentUtils.isBlackOrBlock(videoBean.id + "")) {
                        return;
                    }
                    if (videoBean.winkFlag == MomentsBean.HAS_NOT_WINKED) {
                        BusinessUtils.playSound(R.raw.sound_emoji);
                        videoListViewHolder.moment_opts_like.check();
                        videoListViewHolder.moment_opts_like.setChecked(true, 1);
                        videoListViewHolder.moment_opts_emoji_txt.setText(videoBean.winkNum++ + " " + TheLApp.getContext().getString(R.string.like_msgs_title));
                        MomentLikeUtils.likeMoment(videoBean.id);
                        videoBean.winkFlag = MomentsBean.HAS_WINKED;
                    } else {
                        videoListViewHolder.moment_opts_emoji_txt.setText(videoBean.winkNum-- + " " + TheLApp.getContext().getString(R.string.like_msgs_title));
                        MomentLikeUtils.unLikeMoment(videoBean.id);
                        videoBean.winkFlag = MomentsBean.HAS_NOT_WINKED;
                        videoListViewHolder.moment_opts_like.setChecked(false, 1);
                    }
                    break;
                case R.id.full_screen_iv:
                    //                    setOrientation();
                    break;
                case R.id.small_screen_iv:
                    //                    setOrientation();
                    break;
                case R.id.play_iv:
                    //                    play(isPlaying);
                    break;
                case R.id.moment_opts_emoji_txt:
                    ViewUtils.preventViewMultipleClick(v, 1000);
                    Intent intent = new Intent(activity, WinkCommentsActivity.class);
                    intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, videoBean.id);
                    activity.startActivity(intent);
                    break;
                case R.id.moment_opts_comment_txt:
                    if (videoBean != null) {
                        jumpToComment(activity, videoBean);
                    }
                    break;
                case R.id.img_collect:
                    if (videoBean != null) {
                        //是否被收藏,如果已经被收藏，则为true,要收藏，否则，要删除
                        final boolean isCollect = !BusinessUtils.isCollected(videoBean.id);
                        collect(isCollect, activity, videoListViewHolder, videoBean);
                    }
                    break;
                default:
                    break;
            }
        }
    }

    private void jumpToUserInfo(Context context, String userId, String filter) {
//        Intent intent = new Intent();
//        intent.setClass(context, UserInfoActivity.class);
//        intent.putExtra("fromPage", this.getClass().getName());
//        intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, userId + "");
//        context.startActivity(intent);
        FlutterRouterConfig.Companion.gotoUserInfo(userId);
    }

    private void recommendMoment(Context context, VideoBean videoBean) {
        final String userId = videoBean.userId + "";
        if (MomentUtils.isBlackOrBlock(userId)) {
            return;
        }
        SharedPrefUtils.setBoolean(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.NEW_SHARE_MOMENT, true);
        final Intent intent = new Intent(context, SendCardActivity.class);
        intent.putExtra(TheLConstants.BUNDLE_KEY_INTENT_FROM, SendCardActivity.FROM_VIDEO);
        intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_BEAN, videoBean);
        intent.putExtra(TheLConstants.BUNDLE_KEY_PAGE_FROM, GrowingIoConstant.ENTRY_VIDEO_LIST);
        context.startActivity(intent);
    }

    private void jumpToComment(Activity activity, VideoBean videoBean) {
        Intent intent = new Intent();

        if (!TextUtils.isEmpty(videoBean.id)) {
//            intent.setClass(activity, MomentCommentActivity.class);
//            intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, videoBean.id);
//            activity.startActivity(intent);
            FlutterRouterConfig.Companion.gotoMomentDetails(videoBean.id);
        }
    }

    /**
     * 收藏/取消收藏
     *
     * @param collect true为收藏，false为取消收藏
     */
    private void collect(boolean collect, final Activity activity, VideoListViewHolder videoListViewHolder, final VideoBean videoBean) {

        Map<String, String> map = new HashMap<>();

        //收藏
        if (collect) {
            videoListViewHolder.img_collect.setImageResource(R.mipmap.icn_collection_press);

            map.put(RequestConstants.FAVORITE_COUNT, videoBean.id);
            map.put(RequestConstants.FAVORITE_TYPE, RequestConstants.FAVORITE_TYPE_MOM);

            Flowable<BaseDataBean> flowable = DefaultRequestService.createMomentRequestService().getFavoriteCreate(MD5Utils.generateSignatureForMap(map));
            flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
                @Override
                public void onNext(BaseDataBean data) {
                    super.onNext(data);
                    String collectList = SharedPrefUtils.getString(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.FAVORITE_MOMENT_LIST, "");
                    String id = "[" + ShareFileUtils.getString(ShareFileUtils.ID, "") + "_" + videoBean.id + "]";
                    collectList += id;
                    SharedPrefUtils.setString(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.FAVORITE_MOMENT_LIST, collectList);
                }

                @Override
                public void onComplete() {
                    super.onComplete();
                    DialogUtil.showToastShort(TheLApp.context, getString(R.string.collection_success));

                }

            });
        } else {                  //取消收藏
            videoListViewHolder.img_collect.setImageResource(R.mipmap.icn_collection_normal);

            map.put(RequestConstants.FAVORITE_ID, "");
            map.put(RequestConstants.FAVORITE_COUNT, videoBean.id);
            map.put(RequestConstants.FAVORITE_TYPE, RequestConstants.FAVORITE_TYPE_MOM);

            Flowable<BaseDataBean> flowable = DefaultRequestService.createMomentRequestService().deleteFavoriteMoment(MD5Utils.generateSignatureForMap(map));
            flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
                @Override
                public void onNext(BaseDataBean data) {
                    super.onNext(data);
                    String collectList = SharedPrefUtils.getString(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.FAVORITE_MOMENT_LIST, "");
                    String id = "[" + ShareFileUtils.getString(ShareFileUtils.ID, "") + "_" + videoBean.id + "]";
                    collectList = collectList.replace(id, "");
                    SharedPrefUtils.setString(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.FAVORITE_MOMENT_LIST, collectList);
                    DialogUtil.showToastShort(activity, getString(R.string.collection_canceled));
                }
            });
        }
    }

    public class VideoListViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.avatar)
        ImageView avatar;
        @BindView(R.id.back)
        ImageView back;
        @BindView(R.id.more)
        ImageView more;
        @BindView(R.id.tv_follow)
        ImageView tv_follow;
        @BindView(R.id.iv_feed_recommend)
        ImageView iv_feed_recommend;
        @BindView(R.id.moment_opts_comment)
        ImageView moment_opts_comment;
        @BindView(R.id.moment_opts_like)
        LikeButtonView moment_opts_like;
        @BindView(R.id.txt_nickname)
        TextView txt_nickname;
        @BindView(R.id.txt_desc)
        TextView txt_desc;
        @BindView(R.id.moment_opts_emoji_txt)
        TextView moment_opts_emoji_txt;
        @BindView(R.id.moment_opts_comment_txt)
        TextView moment_opts_comment_txt;
        @BindView(R.id.start_time)
        TextView start_time;
        @BindView(R.id.end_time)
        TextView end_time;
        @BindView(R.id.play_time)
        TextView play_time;
        @BindView(R.id.texture_view)
        TextureView texture_view;
        @BindView(R.id.pause_or_play_iv)
        ImageView pause_or_play_iv;
        @BindView(R.id.img_collect)
        ImageView img_collect;
        @BindView(R.id.long_video_seek)
        SeekBar long_video_seek;
        @BindView(R.id.short_video_seek)
        ProgressBar short_video_seek;
        @BindView(R.id.translucent_view)
        View statusBar;
        @BindView(R.id.video_rl)
        FrameLayout video_rl;
        @BindView(R.id.thumb_iv)
        ImageView thumb_iv;
        @BindView(R.id.full_screen_iv)
        ImageView full_screen_iv;
        @BindView(R.id.small_screen_iv)
        ImageView small_screen_iv;
        @BindView(R.id.content_ll)
        LinearLayout content_ll;

        public VideoListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
