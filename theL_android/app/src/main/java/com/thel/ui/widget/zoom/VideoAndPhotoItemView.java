package com.thel.ui.widget.zoom;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.interfaces.DraweeController;
import com.pili.pldroid.player.PLOnCompletionListener;
import com.pili.pldroid.player.PLOnErrorListener;
import com.pili.pldroid.player.PLOnPreparedListener;
import com.pili.pldroid.player.widget.PLVideoTextureView;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseDataBean;
import com.thel.bean.ImgShareBean;
import com.thel.bean.user.UserInfoPicBean;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.utils.AppInit;
import com.thel.utils.DialogUtil;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.Utils;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by the L on 2016/6/29.
 * 图片视频控件
 */
public class VideoAndPhotoItemView extends RelativeLayout {
    public static final int TYPE_PHOTO = 1;//图片;
    public static final int TYPE_VIDEO = 2;//视频;

    private Context mContext;
    private int mType;//类型
    private String mUrl;//url;
    private ZoomableDraweeView mPhotoView;
    private PLVideoTextureView mVideoView;
    private ProgressBar mProgressBar;
    private LinearLayout mVideoLayout;
    private int mBarHeight = 0;
    private boolean isActive = false;//是否处于活动中
    private boolean isPlaying = false;//是否处于播放中
    private Handler mHandler;
    private MyPreparedListener mVideoPrepareListener;
    private DragListener dragListener;

    public VideoAndPhotoItemView(Context context) {
        this(context, null);
    }

    public VideoAndPhotoItemView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public VideoAndPhotoItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mContext = getContext();
    }

    public VideoAndPhotoItemView initView(int type, String url) {
        mType = type;
        mUrl = url;
        removeAllViews();
        if (mType == TYPE_VIDEO) {
            addVideoView();//添加视频
        } else {
            addPhotoView();//添加图片
        }
        return this;
    }

    /**
     * 添加图像控件
     */
    protected void addPhotoView() {
        mPhotoView = new ZoomableDraweeView(mContext);
        mPhotoView.setLayerType(View.LAYER_TYPE_NONE, null);
        mPhotoView.setSingleTouchFinish(false);
        addView(mPhotoView);
        mPhotoView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        LayoutParams param = (LayoutParams) mPhotoView.getLayoutParams();
        param.addRule(RelativeLayout.CENTER_IN_PARENT);
    }


    public void setController(DraweeController ctrl) {
        if (mType != TYPE_VIDEO && mPhotoView != null) {
            mPhotoView.setController(ctrl);
        }
    }

    public void setHierarchy(GenericDraweeHierarchy hierarchy) {
        if (mType != TYPE_VIDEO && mPhotoView != null) {
            mPhotoView.setHierarchy(hierarchy);
        }
    }

    public void enableDoubleTapZoom(Context context, String mRela_id, boolean isWaterMark, ImgShareBean mImageShareBean) {
        if (mType != TYPE_VIDEO && mPhotoView != null) {
            mPhotoView.enableDoubleTapZoom(context, mUrl, mRela_id, isWaterMark, mImageShareBean);
        }
    }

    public void enableDoubleTapZoomPhotoReport(Context context, String mRela_id, String mUserid, boolean isWaterMark, ImgShareBean mImageShareBean, UserInfoPicBean mUserinfoPicbean) {
        String myId = ShareFileUtils.getString(ShareFileUtils.USER_THEL_ID, "");
        if (myId.equals(mRela_id)) {
            if (mType != TYPE_VIDEO && mPhotoView != null) {
                mPhotoView.enableDoubleTapZoom(context, mUrl, mRela_id, isWaterMark, mImageShareBean);

            }
        } else {
            if (mType != TYPE_VIDEO && mPhotoView != null) {
                mPhotoView.enableDoubleTapZoomPhotoReport(context, mUrl, mRela_id, mUserid, isWaterMark, mImageShareBean, mUserinfoPicbean);

            }
        }
    }

    //举报视频
    public void enableDoubleTapZoomVideoReport(Context context, String mRela_id, String mUserid, boolean isWaterMark, ImgShareBean mImageShareBean, UserInfoPicBean mUserinfoPicbean) {

        if (mVideoView != null && mImageShareBean != null) {
            mVideoLayout.setOnLongClickListener(new OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {

                    DialogUtil.getInstance().showSelectionDialog((Activity) context, new String[]{TheLApp.getContext().getString(R.string.userinfo_activity_dialog_report_title), TheLApp.getContext().getString(R.string.info_no)}, new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                            DialogUtil.getInstance().closeDialog();

                            switch (position) {
                                case 0:
                                    RequestBusiness.getInstance()
                                            .postReportImageOrUser(mUserid, "userimg", Integer.parseInt(mUserinfoPicbean.picId))
                                            .onBackpressureDrop().subscribeOn(Schedulers.io())
                                            .observeOn(AndroidSchedulers.mainThread())
                                            .subscribe(new InterceptorSubscribe<BaseDataBean>() {
                                                @Override
                                                public void onNext(BaseDataBean data) {
                                                    super.onNext(data);
                                                    DialogUtil.showToastShort(mContext, getResources().getString(R.string.userinfo_activity_report_success));
                                                }
                                            });

                                    break;
                                case 1:
                                    DialogUtil.getInstance().closeDialog();

                                    break;
                                default:
                                    break;
                            }
                            DialogUtil.getInstance().closeDialog();

                        }
                    }, false, 2, null);
                    return true;

                }
            });

        }

    }

    public AnimatedZoomableController getZoomableController() {
        if (mType != TYPE_VIDEO && mPhotoView != null) {
            return (AnimatedZoomableController) mPhotoView.getZoomableController();
        }
        return null;
    }

    /**
     * 添加视频视图
     */
    protected void addVideoView() {
        mVideoView = new PLVideoTextureView(mContext);
        mProgressBar = new ProgressBar(mContext, null, android.R.style.Widget_Holo_Light_ProgressBar_Horizontal);
        mProgressBar.setProgressDrawable(ContextCompat.getDrawable(mContext, R.drawable.video_progressbar_drawable));
        mBarHeight = Utils.dip2px(mContext, 4);//progressbar高度为4dp
        mVideoLayout = new LinearLayout(mContext);
        mVideoLayout.setGravity(Gravity.CENTER);
        mVideoLayout.setOrientation(LinearLayout.VERTICAL);
        mVideoLayout.addView(mVideoView);
        mVideoLayout.addView(mProgressBar);
        mVideoView.setLayoutParams(new LinearLayout.LayoutParams(AppInit.displayMetrics.widthPixels, AppInit.displayMetrics.widthPixels));//video为名目宽度为边长正方形
        mProgressBar.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, mBarHeight));
        addView(mVideoLayout);
        mVideoLayout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        mProgressBar.setVisibility(View.INVISIBLE);
        initVideoView();
    }


    /**
     * 初始化videoView
     */
    private void initVideoView() {
        if (mType == TYPE_VIDEO && mVideoView != null && !TextUtils.isEmpty(mUrl) && mProgressBar != null && mUrl != null) {
            mHandler = new Handler();
            mVideoPrepareListener = new MyPreparedListener();
            mVideoView.setOnCompletionListener(new PLOnCompletionListener() {
                @Override
                public void onCompletion() {
                    if (mVideoView != null) {
                        mVideoView.seekTo(0);
                        mVideoView.start();

                    }
                }
            });
            mVideoView.setOnErrorListener(new PLOnErrorListener() {
                @Override
                public boolean onError(int i) {
                    stopPlayer();
                    setDefaultLayout();
                    return false;
                }
            });
        }
        mVideoView.setOnPreparedListener(mVideoPrepareListener);
        setDefaultLayout();
    }

    /**
     * 当视频播放出问题时候，设置默认界面
     */
    private void setDefaultLayout() {
    }

    public boolean isActive() {
        return isActive;
    }

    /**
     * 设置是否活动（播放）
     *
     * @param active
     */
    public void setActive(boolean active) {
        if (mType == TYPE_VIDEO && mVideoView != null && mProgressBar != null && mUrl != null) {
            if (!active) {
                isActive = active;
                stopPlayer();
            } else if (active && !isActive) {//要求播放视频并且处于不是处于活动状态
                isActive = active;
                startPlay();
            }
        }
    }

    /**
     * 开始播放
     */
    private void startPlay() {
        mVideoView.setVideoPath(mUrl);
        mVideoView.requestFocus();
        mVideoView.setOnPreparedListener(mVideoPrepareListener);
    }

    /**
     * 停止播放
     */
    private void stopPlayback() {
        isPlaying = false;
        mVideoView.seekTo(0);
        mVideoView.pause();
        mProgressBar.setProgress(0);
        mProgressBar.setVisibility(View.INVISIBLE);
        setDefaultLayout();
    }

    class MyPreparedListener implements PLOnPreparedListener {

        @Override
        public void onPrepared(int i) {
            if (mProgressBar != null && isActive && !mVideoView.isPlaying()) {
                mVideoView.setVolume(0.5f, 0.5f);
                mProgressBar.setVisibility(View.VISIBLE);
                mProgressBar.setMax((int) mVideoView.getDuration());
                mVideoView.start();
                isPlaying = true;
                mHandler.post(new mVideoRunnable());
            } else if (!isActive) {
                mVideoView.seekTo(0);
            } else {
                setDefaultLayout();
            }
        }
    }

    class mVideoRunnable implements Runnable {
        @Override
        public void run() {
            if (isPlaying) {
                mProgressBar.setProgress((int) mVideoView.getCurrentPosition());
                mHandler.postDelayed(this, 10);
            }
        }
    }

    long startTime = 0;
    long endTime = 0;

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                startTime = System.currentTimeMillis();
                break;
            case MotionEvent.ACTION_UP:
                endTime = System.currentTimeMillis();
                if (endTime - startTime > 0 && endTime - startTime < 100) {//按下点起少于100毫秒为点击
                    if (dragListener != null) {
                        dragListener.dismiss(true);
                    }
                }
                break;
        }
        return super.dispatchTouchEvent(event);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        stopPlayer();
    }

    private void stopPlayer() {
        if (mVideoView != null) {
            mVideoView.stopPlayback();
        }
    }

    public interface DragListener {
        void dismiss(boolean dismiss);
    }

    public void setDragListener(DragListener dragListener) {
        this.dragListener = dragListener;
    }
}
