package com.thel.ui.dialog;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;

import com.thel.R;

public class LoadingActivity extends Activity {

    public static LoadingActivity instance = null;

    /**
     * 0： 初始化 1：显示 -1：不显示
     */
    public static int isShowing = 0;

    private boolean noBack = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (isShowing == -1) {// 有时候close在Activity的oncreate执行之前就已经被调用了，这时候则需要马上关闭
            finish();
            return;
        }
        isShowing = 1;
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);// 在某些机器上光设置theme style不行，需要在这儿调用overridePendingTransition
        noBack = getIntent().getBooleanExtra("noBack", false);
        setContentView(R.layout.dialog_loading);
        instance = this;
    }

    public static void close() {
        if (instance != null)
            instance.finish();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);// 在某些机器上光设置theme style不行，需要在这儿调用overridePendingTransition
        instance = null;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (noBack)
                return true;
            else
                super.onKeyDown(keyCode, event);
        }
        return true;
    }
}