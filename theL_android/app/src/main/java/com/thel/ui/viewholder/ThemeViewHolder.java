package com.thel.ui.viewholder;

import android.view.View;

import butterknife.ButterKnife;

/**
 * Created by liuyun on 2017/11/12.
 */

public class ThemeViewHolder extends MomentDefaultViewHolder {
    public ThemeViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
