package com.thel.ui.widget;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.recommend.RecommendWebBean;
import com.thel.constants.MomentTypeConstants;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.main.home.moments.web.WebViewActivity;
import com.thel.utils.BeanUtils;
import com.thel.utils.ImageUtils;
import com.thel.utils.L;

/**
 * Created by waiarl on 2017/7/14.
 */

public class RecommendWebView extends LinearLayout {
    private final Context mContext;
    private LinearLayout lin_moment_web;
    private ImageView img_moment_web;
    private TextView txt_moment_web_title;
    private MomentsBean momentBean;
    private RecommendWebBean recommendWebBean;

    public RecommendWebView(Context context) {
        this(context, null);
    }

    public RecommendWebView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RecommendWebView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
        setListener();
    }


    private void init() {
        inflate(mContext, R.layout.moment_web_layout, this);
        lin_moment_web = findViewById(R.id.lin_moment_web);
        img_moment_web = findViewById(R.id.img_moment_web);
        txt_moment_web_title = findViewById(R.id.txt_moment_web_title);
    }

    public RecommendWebView initView(MomentsBean momentBean) {
        if (null == momentBean || !MomentTypeConstants.MOMENT_TYPE_WEB.equals(momentBean.momentsType)) {
            setVisibility(GONE);
            return this;
        }
        this.momentBean = momentBean;
        setVisibility(View.VISIBLE);
        return initView(BeanUtils.getRecommendWebBeanFromMoment(momentBean));
    }

    public RecommendWebView initView(RecommendWebBean recommendWebBean) {
        if (null == recommendWebBean) {
            setVisibility(GONE);
            return this;
        }
        this.recommendWebBean = recommendWebBean;
        setVisibility(View.VISIBLE);
        setViews();
        return this;
    }

    private void setListener() {
        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoWebActivity();
            }
        });
    }

    private void setViews() {
        if (!TextUtils.isEmpty(recommendWebBean.imageUrl)) {
//            setImageUrl(img_moment_web, recommendWebBean.imageUrl, TheLConstants.ICON_MIDDLE_SIZE, TheLConstants.ICON_MIDDLE_SIZE);
            ImageLoaderManager.imageLoader(img_moment_web, R.color.gray, recommendWebBean.imageUrl);
        } else {
            img_moment_web.setImageResource(R.mipmap.ic_launcher);
        }
        if (!TextUtils.isEmpty(recommendWebBean.title)) {
            txt_moment_web_title.setText(recommendWebBean.title);
        } else {
            txt_moment_web_title.setText(recommendWebBean.url);
        }
    }

    private void gotoWebActivity() {
        if (momentBean != null) {
            RecommendWebBean bean = BeanUtils.getRecommendWebBeanFromMoment(momentBean);
            if (bean == null || TextUtils.isEmpty(bean.url)) {
                return;
            }
            final Intent intent = new Intent();
            intent.setClass(mContext, WebViewActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("url", bean.url);
            intent.putExtras(bundle);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            TheLApp.getContext().startActivity(intent);
        } else if (recommendWebBean != null) {
            if (TextUtils.isEmpty(recommendWebBean.url)) {
                return;
            }
            final Intent intent = new Intent();
            intent.setClass(mContext, WebViewActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("url", recommendWebBean.url);
            intent.putExtras(bundle);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            TheLApp.getContext().startActivity(intent);
        }

    }

    private void setImageUrl(SimpleDraweeView view, String url) {
        view.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(url))).build()).setAutoPlayAnimations(true).build());
    }

    private void setImageUrl(SimpleDraweeView view, String url, float width, float height) {
        //view.setImageURI(url);
        final String uri = ImageUtils.buildNetPictureUrl(url, width, height);
        view.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(url, width, height))).build()).setAutoPlayAnimations(true).build());
    }
}
