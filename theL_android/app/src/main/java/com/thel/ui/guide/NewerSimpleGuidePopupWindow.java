package com.thel.ui.guide;

import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;

import com.thel.R;

/**
 * Created by waiarl on 2018/2/3.
 */

public class NewerSimpleGuidePopupWindow extends PopupWindow {
    private final Context mContext;
    private GuideView guideView;

    public NewerSimpleGuidePopupWindow(Context context) {
        this(context, null);
    }

    public NewerSimpleGuidePopupWindow(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NewerSimpleGuidePopupWindow(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
        setListener();
    }

    private void init() {
        guideView = new GuideView(mContext);
        setContentView(guideView);
        setOutsideTouchable(false);
        setFocusable(false);
        setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.color.transparent));
        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
    }

    private void setListener() {
        guideView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    public NewerSimpleGuidePopupWindow initPopup(GuideViewBuilder builder) {
        guideView.initView(builder);
        return this;
    }

    public GuideView getContentView() {
        return guideView;
    }


    public NewerSimpleGuidePopupWindow show(View view) {
        showAtLocation(view, Gravity.NO_GRAVITY, 0, 0);
        return this;
    }

    @Override
    public void showAsDropDown(View anchor) {
        if (Build.VERSION.SDK_INT == 24) {
            Rect rect = new Rect();
            anchor.getGlobalVisibleRect(rect);
            int h = anchor.getResources().getDisplayMetrics().heightPixels - rect.bottom;
            setHeight(h);
        }
        super.showAsDropDown(anchor);
    }

    @Override
    public void showAsDropDown(View anchor, int xoff, int yoff) {
        if (Build.VERSION.SDK_INT == 24) {
            Rect rect = new Rect();
            anchor.getGlobalVisibleRect(rect);
            int h = anchor.getResources().getDisplayMetrics().heightPixels - rect.bottom;
            setHeight(h);
        }
        super.showAsDropDown(anchor, xoff, yoff);
    }

    @Override
    public void dismiss() {
        try {
            super.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setOnDismissListener(OnDismissListener onDismissListener) {
        super.setOnDismissListener(onDismissListener);
    }

}
