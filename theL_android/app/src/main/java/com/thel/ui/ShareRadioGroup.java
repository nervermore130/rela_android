package com.thel.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;

import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.SharePosterBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.constants.TheLConstants;
import com.thel.constants.TheLConstantsExt;
import com.thel.growingio.GIOShareTrackBean;
import com.thel.growingio.GrowingIoConstant;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.live.surface.LiveShowPresenter;
import com.thel.modules.main.me.bean.LogInfoBean;
import com.thel.modules.main.me.match.eventcollect.collect.MatchLogUtils;
import com.thel.modules.post.MomentPosterActivity;
import com.thel.modules.post.PosterActivity;
import com.thel.utils.DialogUtil;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.GsonUtils;
import com.thel.utils.ImageUtils;
import com.thel.utils.L;
import com.thel.utils.PermissionUtil;
import com.thel.utils.QRUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.ToastUtils;
import com.thel.utils.UMShareUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;

import java.io.File;

import static com.thel.modules.post.MomentPosterActivity.FROM_ADDFRIEND;
import static com.thel.modules.post.MomentPosterActivity.URL_INVITE_FRIEND;
import static com.thel.modules.post.MomentPosterActivity.URL_SHARE_MOMENT;
import static com.thel.utils.StringUtils.getString;

/**
 * Created by chad
 * Time 17/11/30
 * Email: wuxianchuang@foxmail.com
 * Description: TODO 使用ShareRaidoGroup只需关注两个方法: setShareImagePath、onActivityResult
 */

public class ShareRadioGroup extends RelativeLayout implements View.OnClickListener {

    private String shareImagePath;
    private String zoomPath;//分享的图片缩略图保存的本地路径
    private final int IMG_QUALITY = 70;
    private final float THUMB_SCALE = 0.3f;//缩略图比例
    private boolean isDownload = false;
    private SharePosterBean sharePosterBean;
    private View shareView;

    private String title;
    private String shareTitle;
    private String shareContent;
    private String singleTitle;
    private String url;
    private String logo;
    private String gio_page_from;
    private GIOShareTrackBean gioTrackBean;
    private MomentsBean momentBean;
    private String myUid;
    private String newUrl;
    private boolean isJumpToPoster = false;

    public ShareRadioGroup(Context context) {
        super(context, null);
    }

    public ShareRadioGroup(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    private void initView() {
        View view = inflate(getContext(), R.layout.share_radio_button, this);
        myUid = ShareFileUtils.getString(ShareFileUtils.ID, "");

        view.findViewById(R.id.btn_wx_circle).setOnClickListener(this);
        view.findViewById(R.id.btn_wx).setOnClickListener(this);
        view.findViewById(R.id.btn_weibo).setOnClickListener(this);
        view.findViewById(R.id.btn_qq).setOnClickListener(this);
        view.findViewById(R.id.btn_qzone).setOnClickListener(this);
        view.findViewById(R.id.btn_facebook).setOnClickListener(this);
        view.findViewById(R.id.img_poster).setOnClickListener(this);

        gioTrackBean = new GIOShareTrackBean();

    }

    public void setShareImagePath(View view, SharePosterBean sharePosterBean, boolean isJumpToPoster) {
        this.isJumpToPoster = isJumpToPoster;
        setShareImagePath(view, sharePosterBean);
    }

    /**
     * 获取分享的图片
     *
     * @param view
     * @return
     */
    public void setShareImagePath(View view, SharePosterBean sharePosterBean) {
        this.sharePosterBean = sharePosterBean;
        this.shareView = view;

        ImageView img_bg = view.findViewById(R.id.img_bg);
        ImageView img_qrcode = view.findViewById(R.id.img_qrcode);
        ImageView img_avatar = view.findViewById(R.id.img_avatar);
        TextView txt_nickname = view.findViewById(R.id.txt_nickname);
        TextView txt_rela_id = view.findViewById(R.id.txt_rela_id);
        TextView txt_content = view.findViewById(R.id.txt_content);

        if (!TextUtils.isEmpty(sharePosterBean.userName))
            txt_rela_id.setText(getString(R.string.rela_id, sharePosterBean.userName));
        else//因未完善资料而没有relaId的时候，整个控件不显示
            txt_rela_id.setVisibility(View.GONE);
        if (!TextUtils.isEmpty(sharePosterBean.momentsText)) {
            txt_content.setText(sharePosterBean.momentsText.replace("\n", " "));
        } else {
            txt_content.setText(getString(R.string.moment_poster_default_txt));
        }
        txt_nickname.setText(sharePosterBean.nickname + "");
        ImageLoaderManager.imageLoaderCropCircleBorder(img_avatar, sharePosterBean.avatar);
        final String imageUrl = getImageUrl(sharePosterBean.imageUrl);
        if (!TextUtils.isEmpty(imageUrl)) {
            ImageLoaderManager.imageLoader(img_bg, imageUrl);
        } else {
            img_bg.setBackgroundResource(R.mipmap.rainbow_bg);
        }

        //二维码区分
        final String url = FROM_ADDFRIEND == sharePosterBean.from ? URL_INVITE_FRIEND : URL_SHARE_MOMENT;
        final Bitmap bitmapQr = QRUtils.createQRImage(url, Utils.dip2px(getContext(), 80), Utils.dip2px(getContext(), 80));
        img_qrcode.setImageBitmap(bitmapQr);
    }

    /**
     * 必须将回调设置给 UMShareAPI
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        UMShareAPI.get(getContext()).onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v) {
        //todo Activity context = (Activity) v.getContext(); 类型转换异常 android.support.v7.widget.TintContextWrapper cannot be cast to android.app.Activity
        PermissionUtil.requestStoragePermission((Activity) v.getContext(), new PermissionUtil.PermissionCallback() {
            @Override
            public void granted() {
                try {
                    String activity = "";
                    Activity context = (Activity) v.getContext();
                    switch (v.getId()) {
                        case R.id.btn_wx_circle:
                            if (!isJumpToPoster) {
                                MobclickAgent.onEvent(context, "start_share");
                                activity = "share_wechatmoments";
                                newUrl = getNewUrl(url, myUid);
                                UMShareUtils.share(context, SHARE_MEDIA.WEIXIN_CIRCLE, new UMImage(TheLApp.getContext(), ImageUtils.buildNetPictureUrl(logo, TheLConstants.WX_SHARE_IMAGE_SIZE, TheLConstants.WX_SHARE_IMAGE_SIZE)), newUrl, singleTitle, singleTitle, umShareListener, gioTrackBean);
                            } else {
                                PosterActivity.gotoShare(sharePosterBean.userId, PosterActivity.SHARE_TYPE_WX_CIRCLE);
                            }

                            break;
                        case R.id.btn_wx:
                            if (!isJumpToPoster) {
                                MobclickAgent.onEvent(context, "start_share");
                                activity = "share_wechat";
                                newUrl = getNewUrl(url, myUid);
                                UMShareUtils.share(context, SHARE_MEDIA.WEIXIN, new UMImage(TheLApp.getContext(), ImageUtils.buildNetPictureUrl(logo, TheLConstants.WX_SHARE_IMAGE_SIZE, TheLConstants.WX_SHARE_IMAGE_SIZE)), newUrl, shareTitle, shareContent, umShareListener, gioTrackBean);
                            } else {
                                PosterActivity.gotoShare(sharePosterBean.userId, PosterActivity.SHARE_TYPE_WX);
                            }
                            break;
                        case R.id.btn_weibo:
                            if (!isJumpToPoster) {
                                MobclickAgent.onEvent(context, "start_share");
                                newUrl = getNewUrl(url, myUid);
                                activity = "share_weibo";
                                UMShareUtils.share(context, SHARE_MEDIA.SINA, new UMImage(TheLApp.getContext(), logo), newUrl, singleTitle, singleTitle, umShareListener, gioTrackBean);
                            } else {
                                PosterActivity.gotoShare(sharePosterBean.userId, PosterActivity.SHARE_TYPE_WEIBO);
                            }
                            break;
                        case R.id.btn_qq:
                            if (!isJumpToPoster) {
                                MobclickAgent.onEvent(context, "start_share");
                                newUrl = getNewUrl(url, myUid);
                                activity = "share_qq";
                                UMShareUtils.share(context, SHARE_MEDIA.QQ, new UMImage(TheLApp.getContext(), logo), newUrl, shareTitle, shareContent, umShareListener, gioTrackBean);
                            } else {
                                PosterActivity.gotoShare(sharePosterBean.userId, PosterActivity.SHARE_TYPE_QQ);
                            }
                            break;
                        case R.id.btn_qzone:
                            if (!isJumpToPoster) {
                                newUrl = getNewUrl(url, myUid);
                                activity = "share_qzone";
                                UMShareUtils.share(context, SHARE_MEDIA.QZONE, new UMImage(TheLApp.getContext(), logo), url, shareTitle, shareContent, umShareListener, gioTrackBean);
                            } else {
                                PosterActivity.gotoShare(sharePosterBean.userId, PosterActivity.SHARE_TYPE_QZONE);
                            }
                            break;
                        case R.id.btn_facebook:
                            if (!isJumpToPoster) {
                                MobclickAgent.onEvent(context, "start_share");
                                if (ShareDialog.canShow(ShareLinkContent.class)) {
                                    newUrl = getNewUrl(url, myUid);
                                    ShareLinkContent contentS = new ShareLinkContent.Builder().setContentUrl(Uri.parse(newUrl)).setContentTitle(shareTitle).setContentDescription(shareContent).setImageUrl(Uri.parse(logo)).build();
                                    ShareDialog shareDialog = new ShareDialog(context);
                                    shareDialog.show(contentS);
                                    if (gioTrackBean != null) {
                                        gioTrackBean.shareBean.shareType = GrowingIoConstant.TYPE_FACEBOOK;
                                        GrowingIOUtil.shareTrack(gioTrackBean);
                                    }
                                }
                            } else {
                                PosterActivity.gotoShare(sharePosterBean.userId, PosterActivity.SHARE_TYPE_FACEBOOK);
                            }
                            return;
                        case R.id.img_poster:
                            MomentPosterActivity.gotoShare(sharePosterBean);
                            return;
                        default:
                            break;
                    }

                    LogInfoBean logInfoBean = new LogInfoBean();
                    logInfoBean.activity = activity;
                    logInfoBean.page = "share_page";
                    logInfoBean.page_id = Utils.getPageId();
                    logInfoBean.ua = getShareUA();

                    LogInfoBean.LogsDataBean dataBean = new LogInfoBean.LogsDataBean();
                    if (momentBean != null) {
                        dataBean.data_id = momentBean.momentsId;
                        dataBean.share_id = Utils.getPageId();
                        dataBean.data_type = momentBean.momentsType;
                    }

                    logInfoBean.data = dataBean;
                    String json = "[" + GsonUtils.createJsonString(logInfoBean) + "]";
                    MatchLogUtils.getInstance().send(json);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent();
                intent.setAction(LiveShowPresenter.BR_SHARE);
                LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
            }

            @Override
            public void denied() {
                ToastUtils.showToastShort(v.getContext(), "缺少文件读写权限，无法分享");
            }

            @Override
            public void gotoSetting() {

            }
        });


    }

    private String getNewUrl(String url, String addParm) {

        addParm = addParm + "&page_id=" + Utils.getPageId() + "&page=share_page";

        String newUrl;
        if (url.contentEquals("?")) {
            newUrl = url + "&shareUserId=" + addParm;
        } else {
            newUrl = url + "?shareUserId=" + addParm;
        }

        L.d("ShareRaidoGroup", " newUrl : " + newUrl);

        return newUrl;
    }

    public void shareMoment(Context context, MomentsBean momentBean) {

        if (momentBean != null) {
            this.momentBean = momentBean;
            GrowingIOUtil.setMomentShareTrack(momentBean, gioTrackBean);

            String logo = MomentsBean.MOMENT_TYPE_THEME.equals(momentBean.momentsType) ? TheLConstantsExt.SHARE_THEME_LOGO_URL : TheLConstantsExt.DEFAULT_SHARE_LOGO_URL;

            if (MomentsBean.MOMENT_TYPE_IMAGE.equals(momentBean.momentsType) || MomentsBean.MOMENT_TYPE_TEXT_IMAGE.equals(momentBean.momentsType) || MomentsBean.MOMENT_TYPE_THEME_REPLY.equals(momentBean.momentsType) || MomentsBean.MOMENT_TYPE_THEME.equals(momentBean.momentsType) || MomentsBean.MOMENT_TYPE_USER_CARD.equals(momentBean.momentsType)) {
                if (!TextUtils.isEmpty(momentBean.imageUrl)) {
                    String[] photoUrls = momentBean.imageUrl.split(",");
                    if (photoUrls.length > 0) {
                        logo = photoUrls[0];
                    }
                }
            } else if (MomentsBean.MOMENT_TYPE_VOICE.equals(momentBean.momentsType) || MomentsBean.MOMENT_TYPE_TEXT_VOICE.equals(momentBean.momentsType)) {
                if (!TextUtils.isEmpty(momentBean.albumLogo444)) {
                    logo = momentBean.albumLogo444;
                }
            } else if (MomentsBean.MOMENT_TYPE_VIDEO.equals(momentBean.momentsType) || MomentsBean.MOMENT_TYPE_LIVE.equals(momentBean.momentsType)) {
                if (!TextUtils.isEmpty(momentBean.thumbnailUrl)) {
                    logo = momentBean.thumbnailUrl;
                }
            }
            try {
                if (MomentsBean.MOMENT_TYPE_THEME_REPLY.equals(momentBean.momentsType)) {//话题回复
                    String defaults = context.getResources().getString(R.string.default_share_talk, momentBean.nickname);

                    this.title = context.getString(R.string.share_title_moment);
                    this.shareTitle = context.getString(R.string.share_theme_reply);
                    this.shareContent = TextUtils.isEmpty(momentBean.momentsText) ? defaults : momentBean.momentsText;
                    this.singleTitle = momentBean.momentsText;
                    this.url = TheLConstants.ALL_MOMENT_SHARE_URL(momentBean.momentsId);
                    this.logo = logo;

                } else if (MomentsBean.MOMENT_TYPE_THEME.equals(momentBean.momentsType)) {//话题
                    String defaults = context.getResources().getString(R.string.default_share_talk, momentBean.nickname);

                    if (String.valueOf(momentBean.userId).equals(UserUtils.getMyUserId())) {
                        this.title = context.getString(R.string.posted_a_new_topic);
                    } else {
                        this.title = UserUtils.getNickName() + context.getString(R.string.join_in_the_topic) + "-" + momentBean.momentsText;
                    }
                    this.shareTitle = context.getString(R.string.share_released_topic);
                    this.shareContent = TextUtils.isEmpty(momentBean.momentsText) ? defaults : momentBean.momentsText;
                    this.singleTitle = momentBean.momentsText;
                    this.url = TheLConstants.ALL_MOMENT_SHARE_URL(momentBean.momentsId);
                    this.logo = logo;


                } else if (MomentsBean.MOMENT_TYPE_VIDEO.equals(momentBean.momentsType)) {//视频
                    String defaults = context.getResources().getString(R.string.share_video_content, momentBean.nickname);

                    this.title = context.getString(R.string.share_title_moment);
                    this.shareTitle = context.getString(R.string.share_video_title);
                    this.shareContent = TextUtils.isEmpty(momentBean.momentsText) ? defaults : momentBean.momentsText;
                    this.singleTitle = defaults;
                    this.url = TheLConstants.VIDEO_MOMENT_SHARE_URL(momentBean.momentsId);
                    this.logo = logo;

                } else if (MomentsBean.MOMENT_TYPE_TEXT_IMAGE.equals(momentBean.momentsType) || MomentsBean.MOMENT_TYPE_IMAGE.equals(momentBean.momentsType) || MomentsBean.MOMENT_TYPE_TEXT.equals(momentBean.momentsType)) {
                    String defaults = context.getResources().getString(R.string.share_theme_content, momentBean.nickname);

                    if (String.valueOf(momentBean.userId).equals(UserUtils.getMyUserId())) {
                        this.title = context.getString(R.string.posted_a_moment);
                    } else {
                        this.title = context.getString(R.string.share_the_moment);
                    }

                    this.shareTitle = context.getString(R.string.share_theme_journal);
                    this.shareContent = TextUtils.isEmpty(momentBean.momentsText) ? defaults : momentBean.momentsText;
                    this.singleTitle = defaults;
                    this.url = TheLConstants.ALL_MOMENT_SHARE_URL(momentBean.momentsId);
                    this.logo = logo;


                } else if (MomentsBean.MOMENT_TYPE_LIVE.equals(momentBean.momentsType) || MomentsBean.MOMENT_TYPE_VOICE_LIVE.equals(momentBean.momentsType)) {//直播

                    this.title = context.getString(R.string.share_title_moment);
                    this.shareTitle = momentBean.momentsText;
                    this.shareContent = momentBean.title;
                    this.singleTitle = momentBean.momentsText;
                    this.url = TheLConstants.LIVE_MOMENT_SHARE_URL(momentBean.userId + "");
                    this.logo = momentBean.thumbnailUrl;


                } else if (MomentsBean.MOMENT_TYPE_USER_CARD.equals(momentBean.momentsType)) {//名片
                    String defaults = context.getResources().getString(R.string.share_card_title, momentBean.nickname);
                    String intro = context.getResources().getString(R.string.share_card_content);

                    this.title = context.getString(R.string.share_title_moment);
                    this.shareTitle = defaults;
                    this.shareContent = TextUtils.isEmpty(momentBean.cardIntro) ? intro : momentBean.momentsText;
                    this.singleTitle = momentBean.momentsText;
                    this.url = TheLConstants.ALL_MOMENT_SHARE_URL(momentBean.momentsId);
                    this.logo = logo;

                } else if (MomentsBean.MOMENT_TYPE_VOICE.equals(momentBean.momentsType) || MomentsBean.MOMENT_TYPE_TEXT_VOICE.equals(momentBean.momentsType)) {//音乐日志（音乐与文本音乐）
                    String defaults = context.getResources().getString(R.string.share_music_content, momentBean.nickname);

                    this.title = context.getString(R.string.share_title_moment);
                    this.shareTitle = context.getString(R.string.share_music_title);
                    this.shareContent = TextUtils.isEmpty(momentBean.momentsText) ? defaults : momentBean.momentsText;
                    this.singleTitle = defaults;
                    this.url = TheLConstants.ALL_MOMENT_SHARE_URL(momentBean.momentsId);
                    this.logo = logo;

                } else {//默认为文本
                    String defaults = context.getResources().getString(R.string.share_theme_content, momentBean.nickname);

                    if (String.valueOf(momentBean.userId).equals(UserUtils.getMyUserId())) {
                        this.title = context.getString(R.string.posted_a_moment);
                    } else {
                        this.title = context.getString(R.string.share_the_moment);
                    }
                    this.shareTitle = context.getString(R.string.share_theme_journal);
                    this.shareContent = TextUtils.isEmpty(momentBean.momentsText) ? defaults : momentBean.momentsText;
                    this.singleTitle = defaults;
                    this.url = TheLConstants.ALL_MOMENT_SHARE_URL(momentBean.momentsId);
                    this.logo = logo;

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        //删除缓存文件
        try {
            if (!TextUtils.isEmpty(shareImagePath) && !isDownload) {
                final File f = new File(shareImagePath);
                if (f.exists()) {
                    f.delete();
                }
            }
            if (!TextUtils.isEmpty(zoomPath)) {
                final File zoomF = new File(zoomPath);
                if (zoomF.exists()) {
                    zoomF.delete();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 下载海报
     */
    private void download() {

        getShareImagePath();

        if (!TextUtils.isEmpty(shareImagePath)) {
            isDownload = true;
            Toast.makeText(getContext(), getString(R.string.poster_download_success), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * facebook分享
     */
    private void shareFacebook() {//todo 修复跳转到facebook报错

        try {
            getShareImagePath();
            if (!TextUtils.isEmpty(shareImagePath)) {
                if (ShareDialog.canShow(SharePhotoContent.class)) {
                    final SharePhoto photo = new SharePhoto.Builder().setBitmap(BitmapFactory.decodeFile(shareImagePath)).build();
                    final SharePhotoContent content = new SharePhotoContent.Builder().addPhoto(photo).build();
                    ShareDialog shareDialog = new ShareDialog((Activity) getContext());
                    shareDialog.show(content);
                } else {
                    DialogUtil.showToastShort(getContext(), getString(R.string.install_facebook));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 朋友圈，微信，qq，qq空间，新浪微博 分享
     *
     * @param platform
     */
    private void share(SHARE_MEDIA platform) {

        getShareImagePath();

        MobclickAgent.onEvent(getContext(), "start_share");
        final String thumb_path = getZoomBitmapPath(shareImagePath, THUMB_SCALE, IMG_QUALITY);
        if (null != platform && !TextUtils.isEmpty(shareImagePath)) {
            final UMImage image = new UMImage(getContext(), BitmapFactory.decodeFile(shareImagePath));
            image.setThumb(new UMImage(getContext(), BitmapFactory.decodeFile(thumb_path)));
            new ShareAction((Activity) getContext()).setPlatform(platform).withMedia(image).setCallback(umShareListener).share();
        }
    }

    private String getZoomBitmapPath(String oldPath, float scale, int quality) {
        if (!TextUtils.isEmpty(zoomPath))
            return zoomPath;
        if (TextUtils.isEmpty(oldPath)) {
            zoomPath = null;
            return zoomPath;
        }
        final Bitmap bitmap = BitmapFactory.decodeFile(oldPath);
        if (null == bitmap) {
            return null;
        }
        try {
            final Bitmap bm = getZoomBitmap(bitmap, scale);
            zoomPath = TheLConstants.F_TAKE_PHOTO_ROOTPATH + System.currentTimeMillis() + TheLConstants.MOMENT_POSTER_SHARE_IMG_END + ".jpg";
            if (ImageUtils.savePic(bm, zoomPath, quality)) {
                return zoomPath;
            }
        } catch (Exception e) {
            bitmap.recycle();
            zoomPath = null;
            e.printStackTrace();
        }
        return zoomPath;
    }

    private Bitmap getZoomBitmap(Bitmap oldBitmap, float scale) {
        Bitmap bm = null;
        final float width = oldBitmap.getWidth();
        final float height = oldBitmap.getHeight();
        final Matrix matrix = new Matrix();
        matrix.postScale(scale, scale);
        bm = Bitmap.createBitmap(oldBitmap, 0, 0, (int) width, (int) height, matrix, true);
        return bm;
    }

    private void getShareImagePath() {
        if (!TextUtils.isEmpty(shareImagePath) || shareView == null)
            return;

        TextView tips = shareView.findViewById(R.id.click_qrcode_tips);
        tips.setVisibility(View.VISIBLE);
        shareView.setVisibility(VISIBLE);

        final Bitmap bitmap = Bitmap.createBitmap(shareView.getWidth(), shareView.getHeight(), Bitmap.Config.RGB_565);

        final Canvas canvas = new Canvas(bitmap);
        shareView.draw(canvas);
        File dir = new File(TheLConstants.F_TAKE_PHOTO_ROOTPATH);
        if (!dir.exists()) {
            dir.mkdirs();
        }

        tips.setVisibility(View.INVISIBLE);
        shareView.setVisibility(INVISIBLE);
        shareImagePath = TheLConstants.F_TAKE_PHOTO_ROOTPATH + System.currentTimeMillis() + TheLConstants.MOMENT_POSTER_SHARE_IMG_END + ".jpg";

        if (ImageUtils.savePic(bitmap, shareImagePath, IMG_QUALITY)) {
            Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            Uri uri = Uri.fromFile(dir);
            intent.setData(uri);
            getContext().sendBroadcast(intent);
        }
    }

    private String getImageUrl(String urls) {
        if (TextUtils.isEmpty(urls)) {
            return "";
        }
        return urls.split(",")[0];
    }

    private UMShareListener umShareListener = new UMShareListener() {

        @Override
        public void onStart(SHARE_MEDIA share_media) {

        }

        @Override
        public void onResult(SHARE_MEDIA platform) {
            MobclickAgent.onEvent(getContext(), "share_succeeded");
        }

        @Override
        public void onError(SHARE_MEDIA platform, Throwable t) {
            if (t != null && t.getMessage() != null) {
                DialogUtil.showToastShort(getContext(), t.getMessage());
            }
        }

        @Override
        public void onCancel(SHARE_MEDIA platform) {
            //这里注销掉cancel提示，分享成功点击返回也会走到onCancel
//            DialogUtil.showToastShort(getContext(), TheLApp.getContext().getString(R.string.my_circle_requests_act_canceled));
        }
    };

    public void initGIOPageFrom(String gio_page_from) {
        this.gio_page_from = gio_page_from;
        gioTrackBean.shareBean.shareEntry = gio_page_from;
    }

    private String getShareUA() {
        return android.os.Build.BRAND.toUpperCase() + "," + Build.MODEL.toUpperCase() + ",ANDROID" + Build.VERSION.RELEASE.toUpperCase();
    }
}
