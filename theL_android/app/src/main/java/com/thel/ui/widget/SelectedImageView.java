package com.thel.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import androidx.annotation.Nullable;
import android.util.AttributeSet;

import com.thel.R;

/**
 * Created by waiarl on 2018/3/23.
 */

public class SelectedImageView extends androidx.appcompat.widget.AppCompatImageView {
    private final Context mContext;
    public static final int DEFALUT = -1;
    private int styleName;

    public SelectedImageView(Context context) {
        this(context, null);
    }

    public SelectedImageView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SelectedImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        final TypedArray arr = context.obtainStyledAttributes(attrs, R.styleable.SelectedImageView);
        styleName = arr.getInt(R.styleable.SelectedImageView_selected_type, DEFALUT);
        arr.recycle();
        setPressed(isPressed());
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);
        setPressed(isPressed());
    }

    @Override
    public void setPressed(boolean pressed) {
        super.setPressed(pressed);
        if (isSelected() || pressed) {
            if (styleName != -1) {
                switch (styleName) {
                    case 0:

                        setImageResource(R.mipmap.btn_usermore_sentpremium_press);

                        break;
                    case 1:
                        setImageResource(R.mipmap.btn_usermore_secretlyfollow_press);

                        break;
                    case 2:
                        setImageResource(R.mipmap.btn_usermore_interested_press);

                        break;
                    case 3:
                        setImageResource(R.mipmap.btn_usermore_block_press);

                        break;
                    case 4:
                        setImageResource(R.mipmap.btn_usermore_report_press);

                        break;
                }
            }
        } else {
            if (styleName != -1) {
                switch (styleName) {
                    case 0:

                        setImageResource(R.mipmap.btn_usermore_sentpremium_normal);

                        break;
                    case 1:
                        setImageResource(R.mipmap.btn_usermore_secretlyfollow_normal);

                        break;
                    case 2:
                        setImageResource(R.mipmap.btn_usermore_interested_normal);

                        break;
                    case 3:
                        setImageResource(R.mipmap.btn_usermore_block_normal);

                        break;
                    case 4:
                        setImageResource(R.mipmap.btn_usermore_report_normal);

                        break;
                }
            }
        }
    }
}
