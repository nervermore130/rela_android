package com.thel.ui.widget;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import androidx.annotation.IdRes;
import androidx.annotation.Nullable;

import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.facebook.common.executors.CallerThreadExecutor;
import com.facebook.common.references.CloseableReference;
import com.facebook.datasource.DataSource;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.datasource.BaseBitmapDataSubscriber;
import com.facebook.imagepipeline.image.CloseableImage;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseImageViewerActivity;
import com.thel.bean.ImgShareBean;
import com.thel.bean.SharePosterBean;
import com.thel.constants.TheLConstants;
import com.thel.modules.post.MomentPosterActivity;
import com.thel.ui.imageviewer.ImageLoader;
import com.thel.ui.imageviewer.ViewData;
import com.thel.ui.imageviewer.dragger.ImageDraggerType;
import com.thel.ui.imageviewer.listener.OnItemLongClickListener;
import com.thel.ui.imageviewer.widget.ImageViewer;
import com.thel.ui.imageviewer.widget.ScaleImageView;
import com.thel.utils.DialogUtil;
import com.thel.utils.L;
import com.thel.utils.ScreenUtils;
import com.thel.utils.SizeUtils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by liuyun on 2017/9/21.
 */

public class MultiImageViewGroup extends ViewGroup {

    private static final String TAG = "MultiImageViewGroup";

    private int count = -1;

    private int space = 6;

    private int width = ScreenUtils.getScreenWidth(TheLApp.context) - SizeUtils.dip2px(TheLApp.context, 20);

    private String[] urlArray;

    protected List<ViewData> mViewList = new ArrayList<>();

    private static final int SAVE_FAILD = 1;
    private static final int SAVE_SUCCESS = 2;

    private Handler mHandler;

    public MultiImageViewGroup(Context context) {
        super(context);
        init();
    }

    public MultiImageViewGroup(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MultiImageViewGroup(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case SAVE_SUCCESS:
                        DialogUtil.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.poster_download_success));
                        break;
                    case SAVE_FAILD:
                        DialogUtil.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.download_pic_failed));
                        break;
                }
            }
        };
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int height;

        if (count == 2) {
            height = width / 2;
        } else if (count == 5) {
            height = width / 2 + width / 3;
        } else {
            height = width;
        }

        heightMeasureSpec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        final long startTime = System.currentTimeMillis();
        final int childCount = getChildCount();
        final int halfSpace = space / 2;
        for (int i = 0; i < childCount; i++) {
            if (count == 1) {

                getChildAt(0).layout(0, 0, getMeasuredWidth(), getMeasuredWidth());

            } else if (count == 2) {
                int imageWidth = getMeasuredWidth() / 2;

                getChildAt(0).layout(0, 0, imageWidth - halfSpace, imageWidth - halfSpace);
                getChildAt(1).layout(imageWidth + halfSpace, 0, getMeasuredWidth(), imageWidth - halfSpace);

            } else if (count == 3) {
                int imageWidth = getMeasuredWidth() / 2;
                getChildAt(0).layout(0, 0, getMeasuredWidth(), getMeasuredWidth() / 2 - halfSpace);
                getChildAt(1).layout(0, imageWidth + halfSpace, imageWidth - halfSpace, getMeasuredWidth() - halfSpace);
                getChildAt(2).layout(imageWidth + halfSpace, imageWidth + halfSpace, getMeasuredWidth(), getMeasuredWidth() - halfSpace);

            } else if (count == 4) {
                int imageWidth = getMeasuredWidth() / 2;
                getChildAt(0).layout(0, 0, imageWidth - halfSpace, imageWidth - halfSpace);
                getChildAt(1).layout(imageWidth + halfSpace, 0, getMeasuredWidth(), imageWidth - halfSpace);
                getChildAt(2).layout(0, imageWidth + halfSpace, imageWidth - halfSpace, getMeasuredWidth());
                getChildAt(3).layout(imageWidth + halfSpace, imageWidth + halfSpace, getMeasuredWidth(), getMeasuredWidth());

            } else if (count == 5) {
                int topImageWidth = getMeasuredWidth() / 2;
                int bottomImageWidth = getMeasuredWidth() / 3;
                getChildAt(0).layout(0, 0, topImageWidth - halfSpace, topImageWidth - halfSpace);
                getChildAt(1).layout(topImageWidth + halfSpace, 0, getMeasuredWidth(), topImageWidth - halfSpace);
                getChildAt(2).layout(0, topImageWidth + halfSpace, bottomImageWidth - halfSpace, topImageWidth + bottomImageWidth);
                getChildAt(3).layout(bottomImageWidth + halfSpace, topImageWidth + halfSpace, bottomImageWidth * 2 - halfSpace, topImageWidth + bottomImageWidth);
                getChildAt(4).layout(bottomImageWidth * 2 + halfSpace, topImageWidth + halfSpace, getMeasuredWidth(), topImageWidth + bottomImageWidth);

            } else if (count == 6) {
                int imageWidth = getMeasuredWidth() / 3;
                getChildAt(0).layout(0, 0, imageWidth * 2 - halfSpace, imageWidth * 2 - halfSpace);
                getChildAt(1).layout(imageWidth * 2 + halfSpace, 0, getMeasuredWidth(), imageWidth - halfSpace);
                getChildAt(2).layout(imageWidth * 2 + halfSpace, imageWidth + halfSpace, getMeasuredWidth(), imageWidth * 2 - halfSpace);
                getChildAt(3).layout(0, imageWidth * 2 + halfSpace, imageWidth - halfSpace, getMeasuredWidth());
                getChildAt(4).layout(imageWidth + halfSpace, imageWidth * 2 + halfSpace, imageWidth * 2 - halfSpace, getMeasuredWidth());
                getChildAt(5).layout(imageWidth * 2 + halfSpace, imageWidth * 2 + halfSpace, imageWidth * 3 + halfSpace, getMeasuredWidth());
            } else {
                getChildAt(0).layout(0, 0, getMeasuredWidth(), getMeasuredWidth());
            }
            long endTime = System.currentTimeMillis();
            Log.d("MultiImageViewGroup", " layout time : " + (endTime - startTime));
        }
    }

    public void initUI(final String imageUrl, final String nickName, final ImgShareBean imageShareBean) {
        initMatchNotShareUI(imageUrl, nickName, imageShareBean);
        try {
            BaseImageViewerActivity activity = (BaseImageViewerActivity) getContext();
            ImageViewer imageViewer = activity.getImageViewer();
            imageViewer.setOnItemLongClickListener(new OnItemLongClickListener() {
                @Override
                public void onItemLongClick(int clickPosition, View imageView) {
                    DialogUtil.getInstance().showSelectionDialog((Activity) getContext(), new String[]{TheLApp.getContext().getString(R.string.share_poster), TheLApp.getContext().getString(R.string.download_pic)}, new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                            DialogUtil.getInstance().closeDialog();

                            switch (position) {
                                case 0:
                                    SharePosterBean sharePosterBean = new SharePosterBean();
                                    sharePosterBean.avatar = imageShareBean.picUserUrl;
                                    sharePosterBean.momentsText = imageShareBean.commentText;
                                    sharePosterBean.imageUrl = imageUrl;
                                    sharePosterBean.nickname = imageShareBean.nickName;
                                    sharePosterBean.userName = imageShareBean.userID;
                                    sharePosterBean.from = MomentPosterActivity.FROM_PHOTO;
                                    MomentPosterActivity.gotoShare(sharePosterBean);
                                    break;
                                case 1:
                                    List list = imageViewer.getImageData();
                                    String imageUrl = (String) list.get(clickPosition);
                                    if (TextUtils.isEmpty(imageUrl)) return;
                                    ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithSource(Uri.parse(imageUrl)).setProgressiveRenderingEnabled(true).build();
                                    final DataSource<CloseableReference<CloseableImage>> dataSource = Fresco.getImagePipeline().fetchDecodedImage(imageRequest, getContext());
                                    dataSource.subscribe(new BaseBitmapDataSubscriber() {
                                        @Override
                                        protected void onNewResultImpl(@Nullable Bitmap bitmap) {
                                            if (bitmap != null) {
                                                try {
                                                    File imgDir = new File(TheLConstants.F_TAKE_PHOTO_ROOTPATH);
                                                    if (!imgDir.exists()) {
                                                        imgDir.mkdir();
                                                    }
                                                    //拿到当前系统的时间
                                                    String fileName = System.currentTimeMillis() + ".jpg";
                                                    File img_file = new File(imgDir, fileName);

                                                    try {
                                                        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(img_file.getPath()));
                                                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
                                                        bos.flush();
                                                        bos.close();
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                    Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                                                    Uri uri = Uri.fromFile(img_file);
                                                    intent.setData(uri);
                                                    getContext().sendBroadcast(intent);
                                                    Message msg = Message.obtain();
                                                    msg.what = SAVE_SUCCESS;
                                                    msg.obj = img_file.getPath();
                                                    mHandler.sendMessage(msg);
                                                } catch (Exception e1) {
                                                    e1.printStackTrace();
                                                    mHandler.sendEmptyMessage(SAVE_FAILD);
                                                }
                                            } else
                                                mHandler.sendEmptyMessage(SAVE_FAILD);
                                        }

                                        @Override
                                        protected void onFailureImpl(DataSource<CloseableReference<CloseableImage>> dataSource) {
                                            mHandler.sendEmptyMessage(SAVE_FAILD);
                                        }
                                    }, CallerThreadExecutor.getInstance());
                                    break;
                                default:
                                    break;
                            }
                            DialogUtil.getInstance().closeDialog();

                        }
                    }, false, 2, null);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initMatchNotShareUI(final String imageUrl, final String nickName, final ImgShareBean imageShareBean) {
        urlArray = imageUrl.split(",");
        this.count = urlArray.length;
        long startTime = System.currentTimeMillis();

        removeAllViews();

        for (int i = 0, len = urlArray.length; i < len; i++) {

            L.d(TAG, " urlArray imageUrl : " + urlArray[i]);

            ViewData viewData = new ViewData();
            mViewList.add(viewData);
        }

        for (int i = 0; i < count; i++) {
            SquareImageView imageView = new SquareImageView(getContext());
            imageView.setTag(i);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = (int) v.getTag();

                    try {
                        for (int i = 0; i < count; i++) {
                            int[] location = new int[2];
                            // 获取在整个屏幕内的绝对坐标
                            getChildAt(i).getLocationOnScreen(location);
                            ViewData viewData = mViewList.get(i);
                            viewData.setTargetX(location[0]);
                            // 此处注意，获取 Y 轴坐标时，需要根据实际情况来处理《状态栏》的高度，判断是否需要计算进去
                            viewData.setTargetY(location[1]);
                            viewData.setTargetWidth(getChildAt(i).getWidth());
                            viewData.setTargetHeight(getChildAt(i).getHeight());
                            mViewList.set(i, viewData);
                        }

                        BaseImageViewerActivity activity = (BaseImageViewerActivity) getContext();
                        ImageViewer imageViewer = activity.getImageViewer();
                        imageViewer.doDrag(true);
                        imageViewer.setDragType(ImageDraggerType.DRAG_TYPE_WX);
                        //直接Arrays.asList(urlArray)报错UnsupportedOperationException
                        imageViewer.setImageData(new ArrayList<>(Arrays.asList(urlArray)));
                        imageViewer.setImageLoader(new ImageLoader<String, ImageView>() {

                            @Override
                            public void displayImage(final int position, String imageUrl, ImageView imageView) {
                                final ScaleImageView scaleImageView = (ScaleImageView) imageView.getParent();

                                RequestBuilder<Drawable> requestBuilder = Glide.with(imageView).load(imageUrl);

                                scaleImageView.showProgess();

                                requestBuilder.diskCacheStrategy(DiskCacheStrategy.ALL).load(imageUrl).listener(new RequestListener<Drawable>() {
                                    @Override public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                        return false;
                                    }

                                    @Override public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, com.bumptech.glide.load.DataSource dataSource, boolean isFirstResource) {
                                        scaleImageView.removeProgressView();
                                        imageView.setImageDrawable(resource);
                                        return false;
                                    }
                                }).into(imageView);

                            }
                        });
                        imageViewer.setStartPosition(position);

                        imageViewer.setViewData(mViewList);
                        imageViewer.watch();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
            imageView.setBackgroundColor(0XFFE6E6E6);
            int finalI = i;

            RequestBuilder<Drawable> requestBuilder = Glide.with(imageView).load(imageUrl);

            requestBuilder.diskCacheStrategy(DiskCacheStrategy.ALL).load(urlArray[i]).listener(new RequestListener<Drawable>() {
                @Override public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    return false;
                }

                @Override public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, com.bumptech.glide.load.DataSource dataSource, boolean isFirstResource) {
                    imageView.setImageDrawable(resource);
                    mViewList.get(finalI).setImageWidth(resource.getIntrinsicWidth());
                    mViewList.get(finalI).setImageHeight(resource.getIntrinsicHeight());
                    return false;
                }
            }).into(imageView);

            addView(imageView);

        }

        long endTime = System.currentTimeMillis();
        Log.d("MultiImageViewGroup", " initUI time : " + (endTime - startTime));
    }

    @Override
    public LayoutParams generateLayoutParams(AttributeSet attrs) {
        return new MarginLayoutParams(getContext(), attrs);
    }
}
