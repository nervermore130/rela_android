package com.thel.ui.widget.verticalviewpager;

import androidx.viewpager.widget.ViewPager;
import android.view.View;

/**
 * Created by liuyun on 2018/2/3.
 */

public class DefaultTransformer implements ViewPager.PageTransformer {

    @Override
    public void transformPage(View view, float position) {
        view.setTranslationX(view.getWidth() * -position);
        float yPosition = position * view.getHeight();
        view.setTranslationY(yPosition);
    }
}
