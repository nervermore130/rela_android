package com.thel.ui.widget;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;

import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.animation.LinearInterpolator;

import com.thel.R;

/**
 * Created by liuyun on 2017/11/15.
 */

public class MusicRotationImageView extends androidx.appcompat.widget.AppCompatImageView {

    private ObjectAnimator icon_anim;

    public MusicRotationImageView(Context context) {
        super(context);

        init();
    }

    public MusicRotationImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MusicRotationImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setImageResource(R.drawable.selector_tab_music_play);
    }

    public void pause() {
        stopAnimator();
    }

    public void start() {
        startAnimator();
    }


    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        startAnimator();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        stopAnimator();
    }

    private void startAnimator() {
        icon_anim = ObjectAnimator.ofFloat(this, "rotation", 0.0F, 359.0F);
        icon_anim.setRepeatCount(ValueAnimator.INFINITE);
        icon_anim.setDuration(2000);
        icon_anim.setInterpolator(new LinearInterpolator());
        icon_anim.start();
    }

    private void stopAnimator() {

        if (icon_anim != null) {
            icon_anim.cancel();
            icon_anim = null;
        }
    }


}
