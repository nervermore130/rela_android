package com.thel.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.imp.follow.FollowStatusChangedImpl;
import com.thel.imp.follow.FollowStatusChangedListener;
import com.thel.utils.ShareFileUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChatMatchTipsLayout extends RelativeLayout {

    @BindView(R.id.icon_iv)
    ImageView icon_iv;

    @BindView(R.id.close_iv)
    ImageView close_iv;

    @BindView(R.id.txt_follow)
    TextView txt_follow;

    @BindView(R.id.content_tv)
    TextView content_tv;

    private String userId;

    private String name;

    private String avatar;

    public ChatMatchTipsLayout(Context context) {
        super(context);
        init(context);
    }

    public ChatMatchTipsLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ChatMatchTipsLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {

        View view = LayoutInflater.from(context).inflate(R.layout.layout_chat_match, this, true);

        ButterKnife.bind(this, view);

    }

    public void setData(String userId, String name, String avatar, int status) {
        this.userId = userId;
        this.name = name;
        this.avatar = avatar;

        switch (status) {
            case 0:
                icon_iv.setImageResource(R.mipmap.icn_match_un_follow);
                content_tv.setText(TheLApp.context.getResources().getString(R.string.prefect_match_un_follow_tips));
                break;
            case 2:
                icon_iv.setImageResource(R.mipmap.icn_match_other_followed);
                content_tv.setText(TheLApp.context.getResources().getString(R.string.prefect_match_other_follow_tips));
                break;
            default:
                icon_iv.setImageResource(R.mipmap.icn_match_un_follow);
                break;
        }

    }

    @OnClick(R.id.close_iv) void onClose() {
        this.setVisibility(GONE);
        ShareFileUtils.setBoolean(ShareFileUtils.CHAT_MATCH_FOLLOW_TIPS + userId, true);
    }

    @OnClick(R.id.txt_follow) void onFollow() {

        FollowStatusChangedImpl.followUser(userId, FollowStatusChangedListener.ACTION_TYPE_FOLLOW, name, avatar);

        ShareFileUtils.setBoolean(ShareFileUtils.CHAT_MATCH_FOLLOW_TIPS + userId, true);

        this.setVisibility(GONE);
    }

}
