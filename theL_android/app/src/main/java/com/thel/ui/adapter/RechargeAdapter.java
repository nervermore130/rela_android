package com.thel.ui.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.thel.R;
import com.thel.base.BaseAdapter;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.live.bean.SoftGold;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RechargeAdapter extends BaseAdapter<RechargeAdapter.RechargeViewHolder, SoftGold> {

    public RechargeAdapter(Context context) {
        super(context);
    }

    @Override public void onBindViewHolder(@NonNull RechargeViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);

        SoftGold softGold = getData().get(position);

        SpannableString ss = new SpannableString("x" + softGold.gold);

        ss.setSpan(new RelativeSizeSpan(0.6f), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        holder.gold_tv.setText(ss);

        holder.price_tv.setText(softGold.generatePriceShow());

        ImageLoaderManager.imageLoader(holder.icon_iv, softGold.icon);

        if (softGold.summary != null && softGold.summary.length() > 0) {
            holder.recharge_tips_tv.setVisibility(View.VISIBLE);
            holder.recharge_tips_tv.setText(softGold.summary);
        } else if (softGold.firstChargeDesc != null && softGold.firstChargeDesc.length() > 0) {
            holder.recharge_tips_tv.setVisibility(View.VISIBLE);
            holder.recharge_tips_tv.setText(softGold.firstChargeDesc);
        } else {
            holder.recharge_tips_tv.setVisibility(View.GONE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(v, softGold, position);
                }
            }
        });

    }

    @Override public RechargeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = mLayoutInflater.inflate(R.layout.item_recharge, parent, false);


        return new RechargeViewHolder(view);
    }

    public class RechargeViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.recharge_tips_tv) TextView recharge_tips_tv;

        @BindView(R.id.icon_iv) ImageView icon_iv;

        @BindView(R.id.price_tv) TextView price_tv;

        @BindView(R.id.gold_tv) TextView gold_tv;

        public RechargeViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }

}
