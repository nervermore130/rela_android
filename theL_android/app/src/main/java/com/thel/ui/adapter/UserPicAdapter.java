package com.thel.ui.adapter;

import android.content.Context;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseAdapter;
import com.thel.bean.user.UserInfoPicBean;
import com.thel.utils.AppInit;
import com.thel.utils.ImageUtils;

import java.util.ArrayList;
import java.util.List;


public class UserPicAdapter extends BaseAdapter<UserPicAdapter.UserPicViewHolder, UserInfoPicBean> {

    private List<UserInfoPicBean> picList = new ArrayList<>();

    private int size; // 图片宽度

    public UserPicAdapter(Context context) {
        super(context);

        int screenWidth = AppInit.displayMetrics.widthPixels;// 屏幕宽度

        // 这个数值是根据布局的margin、padding等计算出来的，具体要看user_info_main.xml布局
        size = (screenWidth - TheLApp.getContext().getResources().getDimensionPixelSize(R.dimen.moment_list_photo_margin) * 2) / 3;
    }

    public void addData(List<UserInfoPicBean> picList) {
        this.picList.clear();
        this.picList.addAll(picList);
        notifyDataSetChanged();
    }

    public List<UserInfoPicBean> getData() {
        return picList;
    }

    @NonNull @Override public UserPicViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new UserPicViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_user_info_pic_item, null));
    }

    @Override public void onBindViewHolder(@NonNull final UserPicViewHolder holder, final int position) {
        final UserInfoPicBean item = picList.get(position);

        holder.simpleDraweeView.setController(Fresco.
                newDraweeControllerBuilder().
                setImageRequest(ImageRequestBuilder
                        .newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(item.longThumbnailUrl, size, size)))
                        .build()).
                setAutoPlayAnimations(true).
                build());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(holder.itemView, item, position);
                }
            }
        });

    }

    @Override public int getItemCount() {
        return picList.size();
    }

    public class UserPicViewHolder extends RecyclerView.ViewHolder {

        public SimpleDraweeView simpleDraweeView;

        public UserPicViewHolder(View itemView) {
            super(itemView);
            simpleDraweeView = itemView.findViewById(R.id.img_thumb);
        }
    }

}
