package com.thel.ui.widget;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import androidx.annotation.NonNull;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.live.LiveMultiSeatBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.utils.SizeUtils;
import com.thel.utils.UserUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MeetItemLayout2 extends RelativeLayout {

    @BindView(R.id.avatar_left_iv)
    SquareImageView avatar_left_iv;

    @BindView(R.id.avatar_right_iv)
    SquareImageView avatar_right_iv;

    @BindView(R.id.user_name_left_tv)
    TextView user_name_left_tv;

    @BindView(R.id.user_name_right_tv)
    TextView user_name_right_tv;

    @BindView(R.id.content_rl)
    RelativeLayout content_rl;

    @BindView(R.id.content_left_ll)
    LinearLayout content_left_ll;

    @BindView(R.id.content_right_ll)
    LinearLayout content_right_ll;

    private int contentHeight;//单位为dp

    private int avatarHeight;//单位为dp

    private int textSize;//单位sp

    private LiveMultiSeatBean.CoupleDetail leftData;

    private LiveMultiSeatBean.CoupleDetail rightData;

    public MeetItemLayout2(Context context, AttributeSet attrs) {
        super(context, attrs);

        final TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.MeetItemLayout);

        contentHeight = typedArray.getInt(R.styleable.MeetItemLayout_content_height, 120);

        avatarHeight = typedArray.getInt(R.styleable.MeetItemLayout_avatar_height, 70);

        textSize = typedArray.getInt(R.styleable.MeetItemLayout_text_size, 10);

        typedArray.recycle();

        init(context);
    }

    private void init(Context context) {

        View view = LayoutInflater.from(context).inflate(R.layout.layout_meet_item_poster, this, true);

        ButterKnife.bind(this, view);

        int contentHeightToPX = SizeUtils.dip2px(context, contentHeight);

        int avatarHeightToPX = SizeUtils.dip2px(context, avatarHeight);

        content_rl.getLayoutParams().width = contentHeightToPX;

        content_left_ll.getLayoutParams().width = avatarHeightToPX;

        content_right_ll.getLayoutParams().width = avatarHeightToPX;

        avatar_left_iv.getLayoutParams().width = avatarHeightToPX;

        avatar_left_iv.getLayoutParams().height = avatarHeightToPX;

        avatar_right_iv.getLayoutParams().width = avatarHeightToPX;

        avatar_right_iv.getLayoutParams().height = avatarHeightToPX;

        user_name_left_tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);

        user_name_right_tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
    }

    public void setData(@NonNull LiveMultiSeatBean.CoupleDetail leftData, @NonNull LiveMultiSeatBean.CoupleDetail rightData) {

        this.leftData = leftData;

        this.rightData = rightData;

        ImageLoaderManager.imageLoaderCropCircleBorder(avatar_left_iv, leftData.avatar);

        user_name_left_tv.setText(leftData.nickname);

        ImageLoaderManager.imageLoaderCropCircleBorder(avatar_right_iv, rightData.avatar);

        user_name_right_tv.setText(rightData.nickname);

    }

    public void setFilureData() {
        ImageLoaderManager.imageLoaderCropCircleBorder(avatar_right_iv, UserUtils.getUserAvatar());

        String userName = UserUtils.getUserName();
        user_name_right_tv.setText(userName);
        avatar_left_iv.setImageResource(R.mipmap.default_avatra);
        user_name_left_tv.setTextSize(14);

        user_name_right_tv.setTextSize(14);
        user_name_left_tv.setText(TheLApp.context.getText(R.string.mysterious_female_voice));

    }

    @OnClick(R.id.avatar_left_iv)
    void onLeftAvatarClick() {
        if (leftData != null) {
            goToUserInfoActivity(leftData.userId);
        }
    }

    @OnClick(R.id.avatar_right_iv)
    void onRightAvatarClick() {
        if (rightData != null) {
            goToUserInfoActivity(rightData.userId);
        }
    }

    private void goToUserInfoActivity(String userId) {

        if (getContext() != null) {

//            Intent intent = new Intent(getContext(), UserInfoActivity.class);
//            intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, userId);
//            getContext().startActivity(intent);
            FlutterRouterConfig.Companion.gotoUserInfo(userId);
        }

    }

}
