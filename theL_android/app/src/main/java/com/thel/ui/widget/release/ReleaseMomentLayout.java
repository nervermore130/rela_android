package com.thel.ui.widget.release;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;

public class ReleaseMomentLayout extends ViewGroup {

    public ReleaseMomentLayout(Context context) {
        super(context);
    }

    public ReleaseMomentLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ReleaseMomentLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        measureChildren(widthMeasureSpec, heightMeasureSpec);
    }

    @Override protected void onLayout(boolean changed, int l, int t, int r, int b) {

        int childCount = getChildCount();

        for (int i = 0; i < childCount; i++) {
            getChildAt(i).layout(l, t, r, b);
        }
    }

}
