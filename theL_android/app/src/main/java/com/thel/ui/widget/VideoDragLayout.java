package com.thel.ui.widget;

import android.content.Context;
import androidx.core.view.ViewCompat;
import androidx.customview.widget.ViewDragHelper;

import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;


/**
 * Created by liuyun on 2017/6/29.
 */

public class VideoDragLayout extends ViewGroup {

    private final static String TAG = "VideoDragLayout";

    private static int SNAP_VELOCITY = 1000;//默认滑动速度

    private static int SNAP_DISTANCE = 200;//默认移动距离

    private boolean isScrollX = false, isInit = false;

    private float startX, startY;

    private int finalTop = 0;

    private View videoView;

    private View topLoadingView;

    private View bottomLoadingVIew;

    private View contentView;

    private ViewDragHelper mViewDragHelper;

    private ViewStatus viewStatus = ViewStatus.CENTER;

    private ViewDragStatus viewDragStatus = ViewDragStatus.STATE_IDLE;

    private enum ViewStatus {
        PREVIOUS, CENTER, NEXT
    }

    private enum ViewDragStatus {
        STATE_DRAGGING, STATE_SETTLING, STATE_IDLE
    }

    public VideoDragLayout(Context context) {
        super(context);
        init();
    }

    public VideoDragLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public VideoDragLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        measureChildren(widthMeasureSpec, heightMeasureSpec);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    private void init() {
        mViewDragHelper = ViewDragHelper.create(this, 1.0f, callback);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        videoView = getChildAt(0);
        topLoadingView = getChildAt(1);
        contentView = getChildAt(2);
        bottomLoadingVIew = getChildAt(3);
    }

    @Override
    protected void onLayout(boolean b, int left, int top, int right, int bottom) {
        if (contentView.getTop() == 0) {
            for (int i = 0; i < getChildCount(); i++) {
                final int height;
                if (i == 0) {
                    height = 0;
                } else {
                    height = getMeasuredHeight() * (i - 2);
                }

                getChildAt(i).layout(left, top + height, right, bottom + height);
            }
        } else {
            for (int i = 0; i < getChildCount(); i++) {
                View view = getChildAt(i);
                getChildAt(i).layout(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
            }
        }

    }

    ViewDragHelper.Callback callback = new ViewDragHelper.Callback() {
        @Override
        public boolean tryCaptureView(View child, int pointerId) {
            return viewDragStatus == ViewDragStatus.STATE_IDLE;
        }

        @Override
        public int clampViewPositionHorizontal(View child, int left, int dx) {
            if (!isScrollX) {
                return 0;
            }

            if (child == videoView && videoView.getLeft() >= 0) {
                return 0;
            }

            return child.getLeft() + (left - child.getLeft()) / 2;
        }

        @Override
        public int clampViewPositionVertical(View child, int top, int dy) {

            if (!isScrollX) {
                return 0;
            }

            return child.getTop() + (top - child.getTop()) / 2;
        }

        @Override
        public int getViewVerticalDragRange(View child) {
            return getMeasuredWidth();
        }

        @Override
        public int getViewHorizontalDragRange(View child) {
            return getMeasuredHeight();
        }

        @Override
        public void onViewPositionChanged(View changedView, int left, int top, int dx, int dy) {

            Log.d("VideoDragLayout", " dy : " + dy);

            onViewPosChanged(changedView, dy);
        }

        @Override
        public void onViewReleased(View releasedChild, float xvel, float yvel) {

            Log.d("VideoDragLayout", " xvel : " + xvel);

            if (isScrollX) {
                dragLeftOrRight(releasedChild, xvel);
            } else {

                dragToTopOrBottom(releasedChild, yvel);
            }

        }

        @Override
        public void onViewDragStateChanged(int state) {
            switch (state) {
                case ViewDragHelper.STATE_DRAGGING:  // 正在被拖动
                    viewDragStatus = ViewDragStatus.STATE_DRAGGING;
                    break;
                case ViewDragHelper.STATE_SETTLING: // fling完毕后被放置到一个位置
                    viewDragStatus = ViewDragStatus.STATE_SETTLING;
                    break;
                case ViewDragHelper.STATE_IDLE:  // view没有被拖拽或者 正在进行fling/snap
                    viewDragStatus = ViewDragStatus.STATE_IDLE;
                    switch (viewStatus) {
                        case NEXT:
                            if (contentView.getTop() == 0) {
                                topLoadingView.layout(topLoadingView.getLeft(), -getMeasuredHeight(), getRight(), 0);
                                contentView.layout(contentView.getLeft(), 0, contentView.getRight(), getMeasuredHeight());
                                bottomLoadingVIew.layout(bottomLoadingVIew.getLeft(), getMeasuredHeight(), bottomLoadingVIew.getRight(), getMeasuredHeight() * 2);
                            } else if (contentView.getTop() == -getMeasuredHeight()) {
                                topLoadingView.layout(topLoadingView.getLeft(), getMeasuredHeight(), getRight(), getMeasuredHeight() * 2);
                                contentView.layout(contentView.getLeft(), -getMeasuredHeight(), contentView.getRight(), 0);
                                bottomLoadingVIew.layout(bottomLoadingVIew.getLeft(), 0, bottomLoadingVIew.getRight(), getMeasuredHeight());

                            } else if (contentView.getTop() == -getMeasuredHeight() * 2) {
                                topLoadingView.layout(topLoadingView.getLeft(), 0, getRight(), getMeasuredHeight());
                                contentView.layout(contentView.getLeft(), getMeasuredHeight(), contentView.getRight(), getMeasuredHeight() * 2);
                                bottomLoadingVIew.layout(bottomLoadingVIew.getLeft(), -getMeasuredHeight(), bottomLoadingVIew.getRight(), 0);

                            } else if (contentView.getTop() == getMeasuredHeight()) {
                                topLoadingView.layout(topLoadingView.getLeft(), -getMeasuredHeight(), getRight(), 0);
                                contentView.layout(contentView.getLeft(), 0, contentView.getRight(), getMeasuredHeight());
                                bottomLoadingVIew.layout(bottomLoadingVIew.getLeft(), getMeasuredHeight(), bottomLoadingVIew.getRight(), getMeasuredHeight() * 2);

                            } else if (contentView.getTop() == getMeasuredHeight() * 2) {
                                topLoadingView.layout(topLoadingView.getLeft(), getMeasuredHeight(), getRight(), getMeasuredHeight() * 2);
                                contentView.layout(contentView.getLeft(), -getMeasuredHeight(), contentView.getRight(), 0);
                                bottomLoadingVIew.layout(bottomLoadingVIew.getLeft(), 0, bottomLoadingVIew.getRight(), getMeasuredHeight());
                            }
                            break;
                        case PREVIOUS:
                            if (contentView.getTop() == getMeasuredHeight()) {
                                topLoadingView.layout(topLoadingView.getLeft(), 0, getRight(), getMeasuredHeight());
                                contentView.layout(contentView.getLeft(), getMeasuredHeight(), contentView.getRight(), getMeasuredHeight() * 2);
                                bottomLoadingVIew.layout(bottomLoadingVIew.getLeft(), -getMeasuredHeight(), bottomLoadingVIew.getRight(), 0);
                            } else if (contentView.getTop() == getMeasuredHeight() * 2) {
                                topLoadingView.layout(topLoadingView.getLeft(), getMeasuredHeight(), getRight(), getMeasuredHeight() * 2);
                                contentView.layout(contentView.getLeft(), -getMeasuredHeight(), contentView.getRight(), 0);
                                bottomLoadingVIew.layout(bottomLoadingVIew.getLeft(), 0, bottomLoadingVIew.getRight(), getMeasuredHeight());
                            } else if (contentView.getTop() == 0) {
                                topLoadingView.layout(topLoadingView.getLeft(), -getMeasuredHeight(), getRight(), 0);
                                contentView.layout(contentView.getLeft(), 0, contentView.getRight(), getMeasuredHeight());
                                bottomLoadingVIew.layout(bottomLoadingVIew.getLeft(), getMeasuredHeight(), bottomLoadingVIew.getRight(), getMeasuredHeight() * 2);
                            }
                            break;

                    }
                    videoView.layout(videoView.getLeft(), 0, videoView.getRight(), getMeasuredHeight());
                    break;
            }
        }
    };

    private void onViewPosChanged(View changedView, int offSet) {

        for (int i = 0; i < getChildCount(); i++) {
            View view = getChildAt(i);
            if (changedView != view) {
                view.layout(view.getLeft(), view.getTop() + offSet, view.getRight(), view.getBottom() + offSet);
            }
        }

    }

    private void dragLeftOrRight(View releasedChild, float xvel) {
        if (releasedChild == contentView) {
            animRight(releasedChild, xvel);
        } else {
            if (xvel < -SNAP_VELOCITY) {
                animLeft(contentView);
            }
        }
    }

    private void animRight(View releasedChild, float xvel) {

        int finalLeft = 0;
        if (releasedChild == contentView) {
            if (contentView.getLeft() > SNAP_DISTANCE || xvel > SNAP_VELOCITY) {
                finalLeft = releasedChild.getMeasuredWidth();
            } else {
                finalLeft = 0;
            }
        }
        isInit = false;
        if (mViewDragHelper.smoothSlideViewTo(releasedChild, finalLeft, 0)) {
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    private void animLeft(View releasedChild) {
        if (mViewDragHelper.smoothSlideViewTo(releasedChild, 0, 0)) {
            ViewCompat.postInvalidateOnAnimation(this);
        }
        isInit = false;
        requestLayout();
    }

    private void dragToTopOrBottom(final View releasedChild, float yvel) {
        if (releasedChild.getTop() > SNAP_DISTANCE || yvel > SNAP_VELOCITY) {
            finalTop = getMeasuredHeight();
            viewStatus = ViewStatus.PREVIOUS;
        } else if (releasedChild.getTop() < -SNAP_DISTANCE || yvel < -SNAP_VELOCITY) {
            finalTop = -getMeasuredHeight();
            viewStatus = ViewStatus.NEXT;
        } else {
            finalTop = 0;
            viewStatus = ViewStatus.CENTER;
        }
        if (mViewDragHelper.smoothSlideViewTo(releasedChild, 0, finalTop)) {
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return mViewDragHelper.shouldInterceptTouchEvent(event);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        try {
            mViewDragHelper.processTouchEvent(event);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        int action = event.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                startX = event.getX();
                startY = event.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                float moveX = event.getX();
                float moveY = event.getY();
                if ((moveX - startX > 100 || moveX - startX < -100) && !isInit) {
                    isScrollX = false;
                    isInit = true;
                } else if ((moveY - startY > 100 || moveY - startY < -100) && !isInit) {
                    isScrollX = true;
                    isInit = true;
                } else {
                    return false;
                }
                break;
            case MotionEvent.ACTION_UP:
                float endX = event.getX();
                float endY = event.getY();
                if (Math.abs(endX - startX) < 8 && Math.abs(endY - startY) < 8 && !isInit) {

                }
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    public void computeScroll() {
        if (mViewDragHelper.continueSettling(true)) {
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

}

