package com.thel.ui.widget.emoji;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.StickerBean;
import com.thel.bean.StickerPackBean;
import com.thel.constants.TheLConstants;
import com.thel.imp.sticker.StickerContract;
import com.thel.imp.sticker.StickerUtils;
import com.thel.modules.main.me.aboutMe.MyStickersActivity;
import com.thel.modules.main.me.aboutMe.StickerStoreActivity;
import com.thel.utils.EmojiUtils;
import com.thel.utils.ImageUtils;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SharedPrefUtils;
import com.thel.utils.SizeUtils;
import com.thel.utils.ViewUtils;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import static androidx.viewpager.widget.PagerAdapter.POSITION_NONE;

/**
 * Created by setsail on 15/8/17.
 */
public class SendEmojiLayout extends RelativeLayout {

    private Context mContext;
    private SimpleDraweeView img_bg;
    private ViewPager view_pager;
    private RecyclerView mRecyclerView;
    private RelativeLayout rel_preview;
    private SendEmojiIconRecyclerViewAdapter mAdapter;
    private ArrayList<StickerPackBean> mDatas = new ArrayList<>();
    // 选中第几个表情包
    private int selection = -1;

    /**
     * 表情包-表情页集合
     */
    private ArrayList<ArrayList<View>> packPageViews = new ArrayList<>();

    public SendEmojiLayout(Context context) {
        super(context);
        this.mContext = context;
    }

    public SendEmojiLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
    }

    public SendEmojiLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
    }

    public void refreshPaddingTop() {
        // 表情区域的高度是（60+20）*2 = 160dp，底部recyclerView高度为send_emoji_recycler_height，小圆点是6dp并且marginBottom是10dp，根据这些数据再与键盘高度做比较，设置previewGridView相应的marginTop
        int softHeight = ShareFileUtils.getInt(ShareFileUtils.SOFT_KEYBOARD_HEIGHT, 0);
        if (softHeight <= SizeUtils.dip2px(mContext, 200)) {
            findViewById(R.id.rel_emoji).setPadding(0, 0, 0, 0);
        } else {
            int paddingTop = (int) ((softHeight - SizeUtils.dip2px(mContext, (6 + 10 + 10 + 160)) - getResources().getDimension(R.dimen.send_emoji_recycler_height)) / 2);
            if (paddingTop > 0) {
                findViewById(R.id.rel_emoji).setPadding(0, paddingTop, 0, 0);
            }
        }
    }

    //
    public void initViews(RelativeLayout preview) {
        setVisibility(GONE);
        this.rel_preview = preview;
        refreshPaddingTop();
        // 进入表情商店
        findViewById(R.id.rel_go_to_store).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                mContext.startActivity(new Intent(mContext, StickerStoreActivity.class));
            }
        });
        initPackDataViews();

        // initPackData();

       /* mDatas.get(1).isSelected = true;
        selection = 1;
        view_pager = (ViewPager) findViewById(R.id.view_pager);
        img_bg = (SimpleDraweeView) findViewById(R.id.img_bg);

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview_horizontal);
        //设置布局管理器
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        //如果可以确定每个item的高度是固定的，设置这个选项可以提高性能
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(null);

        lin_orals = (LinearLayout) findViewById(R.id.lin_orals);

        initViewPager(rel_preview);
        initOrals();
        initData();*/
    }

    private void initPackDataViews() {
        StickerUtils.getMySpStickFileList(new StickerContract.GetStickerListCallback<StickerPackBean>() {
            @Override
            public void getStickerList(List<StickerPackBean> stickerList) {
                L.d("表情列表获取"+stickerList.toString());
                initPackDataView(stickerList);
                initViews();
            }
        });
    }

    private void initPackDataView(List<StickerPackBean> stickerList) {
        this.mDatas.clear();
        // ArrayList<StickerPackBean> data = MomentsDataBaseAdapter.getInstance(mContext, ShareFileUtils.getString(ShareFileUtils.ID, "")).getStickerPacksInUseWithStickers();
        mDatas.addAll(stickerList);
        // 内置静态图
        StickerPackBean emojiPack = new StickerPackBean();
        emojiPack.id = 0;
        emojiPack.icon = TheLConstants.RES_PIC_URL + R.mipmap.btn_stickerkeyboard_emoji;
        emojiPack.stickers.addAll(EmojiUtils.getInstace(EmojiUtils.DEFAULT_SIZE).getEmojis());
        emojiPack.isEmojiPack = true;
        mDatas.add(0, emojiPack);
        // 历史表情
        StickerPackBean historyPack = new StickerPackBean();
        historyPack.id = -1;
        historyPack.icon = TheLConstants.RES_PIC_URL + R.mipmap.btn_stickerkeyboard_history;
        historyPack.stickers.addAll(getHistoryStickers());
        mDatas.add(0, historyPack);
        // 管理表情按钮
        StickerPackBean operateBtn = new StickerPackBean();
        operateBtn.icon = TheLConstants.RES_PIC_URL + R.mipmap.btn_stickerkeyboard_settings;
        this.mDatas.add(operateBtn);
    }

    private void initViews() {
        mDatas.get(1).isSelected = true;
        selection = 1;
        view_pager = findViewById(R.id.view_pager);
        img_bg = findViewById(R.id.img_bg);

        mRecyclerView = findViewById(R.id.recyclerview_horizontal);
        //设置布局管理器
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        //如果可以确定每个item的高度是固定的，设置这个选项可以提高性能
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(null);

        lin_orals = findViewById(R.id.lin_orals);

        initViewPager(rel_preview);
        initOrals();
        initData();
    }


    private void initViewPager(RelativeLayout rel_preview) {
        packPageViews.clear();
        /** 初始化贴图数据 **/
        for (StickerPackBean stickerPackBean : mDatas) {
            // 内置静态表情3行，动态贴图2行
            int numColumn = stickerPackBean.isEmojiPack ? 7 : 4;
            int numRow = stickerPackBean.isEmojiPack ? 3 : 2;

            // viewpager每一页上的贴图数据
            ArrayList<ArrayList<StickerBean>> stickerPages = new ArrayList<>();
            if (stickerPackBean.isEmojiPack) {// 内置静态表情包
                int index = 0;
                for (int i = 0; i < 8; i++) {
                    ArrayList<StickerBean> arr = new ArrayList<>();
                    for (int y = 0; y < numColumn * numRow; y++) {
                        if (y == numColumn * numRow - 1) {// 最后一个图标是删除按钮
                            StickerBean stickerBean = new StickerBean();
                            stickerBean.isDeleteBtn = true;
                            stickerBean.thumbnail = TheLConstants.RES_PIC_URL + R.mipmap.btn_stickerkeyboard_delete;
                            arr.add(stickerBean);
                        } else {
                            if (index < stickerPackBean.stickers.size()) {
                                arr.add(stickerPackBean.stickers.get(index));
                                index++;
                            } else {
                                break;
                            }
                        }
                    }
                    stickerPages.add(arr);
                }
            } else {
                int pageSize = (int) Math.ceil((double) (stickerPackBean.stickers.size()) / (numColumn * numRow));
                int index = 0;
                for (int i = 0; i < pageSize; i++) {
                    ArrayList<StickerBean> arr = new ArrayList<>();
                    for (int y = 0; y < numColumn * numRow; y++) {
                        if (index < stickerPackBean.stickers.size()) {
                            arr.add(stickerPackBean.stickers.get(index));
                            index++;
                        } else
                            break;
                    }
                    stickerPages.add(arr);
                }
            }

            // viewpager的每一页view
            ArrayList<View> pageViews = new ArrayList<>();

            // 中间添加表情页
            if (stickerPackBean.isEmojiPack) {
                for (int i = 0; i < stickerPages.size(); i++) {
                    GridView gridView = new PreviewGridView(mContext);
                    EmojiGridViewAdapter adapter = new EmojiGridViewAdapter(stickerPages.get(i));
                    gridView.setAdapter(adapter);
                    gridView.setOnItemClickListener((AdapterView.OnItemClickListener) mContext);
                    gridView.setNumColumns(numColumn);
                    gridView.setBackgroundColor(Color.TRANSPARENT);
                    gridView.setStretchMode(GridView.STRETCH_COLUMN_WIDTH);
                    gridView.setPadding((int) mContext.getResources().getDimension(R.dimen.margin_horizontal), 0, (int) mContext.getResources().getDimension(R.dimen.margin_horizontal), 0);
                    gridView.setCacheColorHint(0);
                    gridView.setSelector(new ColorDrawable(Color.TRANSPARENT));
                    gridView.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                    gridView.setGravity(Gravity.CENTER);
                    pageViews.add(gridView);
                }
            } else {
                for (int i = 0; i < stickerPages.size(); i++) {
                    addPageViews(pageViews, stickerPages.get(i), numColumn);
                }
                if (stickerPackBean.id == -1 && stickerPages.size() == 0) {// 一开始历史表情表是空的
                    addPageViews(pageViews, new ArrayList<StickerBean>(), numColumn);
                }
            }

            packPageViews.add(pageViews);
        }
    }

    private void addPageViews(ArrayList<View> pageViews, ArrayList<StickerBean> stickerBeans, int numColumn) {
        PreviewGridView previewGridView = new PreviewGridView(mContext);
        StickerGridViewAdapter adapter = new StickerGridViewAdapter(stickerBeans);
        previewGridView.setAdapter(adapter);
        previewGridView.setMyOnItemClickListener((PreviewGridView.MyOnItemClickListener) mContext);
        previewGridView.setNumColumns(numColumn);
        previewGridView.setBackgroundColor(Color.TRANSPARENT);
        previewGridView.setStretchMode(GridView.STRETCH_COLUMN_WIDTH);
        previewGridView.setCacheColorHint(0);
        previewGridView.setSelector(new ColorDrawable(Color.TRANSPARENT));
        previewGridView.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        previewGridView.setGravity(Gravity.CENTER);
        ArrayList<String> arr = new ArrayList<>();
        for (StickerBean stickerBean : stickerBeans) {
            arr.add(stickerBean.img);
        }
        previewGridView.setPreviewRelativeLayout(rel_preview, arr);
        pageViews.add(previewGridView);
    }

    private void refreshHistoryStickersPageView(ArrayList<View> pageViews) {
        try {
            PreviewGridView previewGridView = (PreviewGridView) pageViews.get(0);
            StickerGridViewAdapter adapter = (StickerGridViewAdapter) previewGridView.getAdapter();
            ArrayList<StickerBean> stickerBeans = getHistoryStickers();
            adapter.refreshAdapter(stickerBeans);
            adapter.notifyDataSetChanged();
            ArrayList<String> arr = new ArrayList<>();
            for (StickerBean stickerBean : stickerBeans) {
                arr.add(stickerBean.img);
            }
            previewGridView.refreshPreviewData(arr);
        } catch (Exception e) {
        }
    }

    private void initData() {
        /** 初始化recycview的数据 **/
        mAdapter = new SendEmojiIconRecyclerViewAdapter(mDatas, mContext);
        mAdapter.setOnRecyclerViewListener(new OnRecyclerViewListener() {
            @Override
            public void onItemClick(int position) {
                if (selection != position) {
                    if (position == packPageViews.size() - 1) {// 管理表情按钮
                        mContext.startActivity(new Intent(mContext, MyStickersActivity.class));
                        return;
                    }
                    mDatas.get(position).isSelected = true;
                    mDatas.get(selection).isSelected = false;
                    int lastSelection = selection;
                    selection = position;
                    mAdapter.notifyItemChanged(position);
                    mAdapter.notifyItemChanged(lastSelection);
                    if (selection == 0) {
                        refreshHistoryStickersPageView(packPageViews.get(selection));
                    }
                    view_pager.setAdapter(new ViewPagerAdapter(packPageViews.get(selection)));
                    if (!TextUtils.isEmpty(mDatas.get(selection).bgImg))
                        img_bg.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(mDatas.get(selection).bgImg)));
                    else {
                        img_bg.setImageURI(Uri.parse(TheLConstants.RES_PIC_URL + R.color.transparent));
                    }
                    view_pager.setCurrentItem(0);
                    initOrals();
                }
            }

            @Override
            public boolean onItemLongClick(int position) {
                return false;
            }
        });
        mRecyclerView.setAdapter(mAdapter);

        /** 初始化view_pager的数据 **/
        view_pager.setAdapter(new ViewPagerAdapter(packPageViews.get(selection)));
        view_pager.setCurrentItem(0);
        view_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int arg0) {
                // 描绘分页点
                drawOral(arg0);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int arg0) {

            }
        });

    }

    public void refreshUI() {
        // 当前选中了哪个表情包
        StickerPackBean selectedPack = mDatas.get(selection);
        if (mAdapter != null) {
            refreshUIDataViews(selectedPack);
        }
    }

    private void refreshUIDataViews(final StickerPackBean selectedPack) {
        StickerUtils.getMySpStickFileList(new StickerContract.GetStickerListCallback<StickerPackBean>() {
            @Override
            public void getStickerList(List<StickerPackBean> stickerList) {
                initPackDataView(stickerList);
                refreshUIData(selectedPack);
            }
        });
    }

    private void refreshUIData(StickerPackBean selectedPack) {
        int i = 0;
        boolean hasSelected = false;
        for (StickerPackBean stickerPackBean : mDatas) {
            if (selectedPack.id == stickerPackBean.id) {
                selection = i;
                hasSelected = true;
                stickerPackBean.isSelected = true;
                break;
            }
            i++;
        }
        if (!hasSelected) {
            selection = 1;
            mDatas.get(1).isSelected = true;
        }
        mAdapter.notifyDataSetChanged();

        initViewPager(rel_preview);

        if (selectedPack.id != mDatas.get(selection).id) {// 如果已选的包没被删除，则不刷新viewPager，否则刷新
            view_pager.setAdapter(new ViewPagerAdapter(packPageViews.get(selection)));
            if (!TextUtils.isEmpty(mDatas.get(selection).bgImg))
                img_bg.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(mDatas.get(selection).bgImg)));
            else {
                img_bg.setImageURI(Uri.parse(TheLConstants.RES_PIC_URL + R.color.transparent));
            }
            view_pager.setCurrentItem(0);
            initOrals();
        }
    }

    /**
     * 游标点集合
     */
    private ArrayList<SimpleDraweeView> pointViews = new ArrayList<SimpleDraweeView>();
    /**
     * 游标显示布局
     */
    private LinearLayout lin_orals;

    /**
     * 初始化游标
     */
    private void initOrals() {

        lin_orals.removeAllViews();
        pointViews.clear();
        SimpleDraweeView imageView;
        if (packPageViews.get(selection).size() <= 1) {
            return;
        }
        for (int i = 0; i < packPageViews.get(selection).size(); i++) {
            imageView = new SimpleDraweeView(mContext);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(new ViewGroup.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
            layoutParams.leftMargin = 10;
            layoutParams.rightMargin = 10;
            layoutParams.width = SizeUtils.dip2px(TheLApp.getContext(), 6);
            layoutParams.height = layoutParams.width;
            imageView.setBackgroundResource(R.drawable.circle_gray);
            lin_orals.addView(imageView, layoutParams);
            if (i == 0) {
                imageView.setBackgroundResource(R.drawable.circle_main_color);
            }
            pointViews.add(imageView);
        }
    }

    /**
     * 绘制游标背景
     */
    public void drawOral(int index) {
        for (int i = 0; i < pointViews.size(); i++) {
            if (index == i) {
                pointViews.get(i).setBackgroundResource(R.drawable.circle_main_color);
            } else {
                pointViews.get(i).setBackgroundResource(R.drawable.circle_gray);
            }
        }
    }

    /**
     * 获取历史表情
     *
     * @return
     */
    private ArrayList<StickerBean> getHistoryStickers() {
        ArrayList<StickerBean> result = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(SharedPrefUtils.getString(SharedPrefUtils.FILE_STICKERS, SharedPrefUtils.FILE_STICKERS, "[]"));
            for (int i = 0; i < jsonArray.length(); i++) {
                StickerBean stickerBean = new StickerBean();
                stickerBean.fromJson(jsonArray.getJSONObject(i));
                result.add(stickerBean);
            }
        } catch (Exception e) {
        }
        return result;
    }

    private class SendEmojiIconRecyclerViewAdapter extends RecyclerView.Adapter {

        private ArrayList<StickerPackBean> stickerPackBeans = new ArrayList<>();

        private LayoutInflater mInflater;
        private Context context;

        public SendEmojiIconRecyclerViewAdapter(ArrayList<StickerPackBean> stickerPackBeans, Context context) {
            this.context = context;
            mInflater = LayoutInflater.from(context);

            this.stickerPackBeans = stickerPackBeans;
        }

        public void setOnRecyclerViewListener(OnRecyclerViewListener onRecyclerViewListener) {
            this.onRecyclerViewListener = onRecyclerViewListener;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = mInflater.inflate(R.layout.send_emoji_icon_item, null);
            return new HoldView(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
            HoldView holdView = (HoldView) viewHolder;
            holdView.position = position;
            StickerPackBean stickerPackBean = stickerPackBeans.get(position);
            holdView.img_icon.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(stickerPackBean.icon))).build()).setAutoPlayAnimations(true).build());
            if (stickerPackBean.isSelected) {
                holdView.rel_main.setBackgroundColor(TheLApp.getContext().getResources().getColor(R.color.gray));
            } else {
                holdView.rel_main.setBackgroundColor(Color.TRANSPARENT);
            }
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemCount() {
            return stickerPackBeans.size();
        }

        private OnRecyclerViewListener onRecyclerViewListener;

        class HoldView extends RecyclerView.ViewHolder implements OnClickListener, View.OnLongClickListener {
            public RelativeLayout rel_main;
            public SimpleDraweeView img_icon;
            public int position;

            public HoldView(View itemView) {
                super(itemView);
                rel_main = itemView.findViewById(R.id.rel_main);
                img_icon = itemView.findViewById(R.id.img_icon);
                rel_main.setOnClickListener(this);
                rel_main.setOnLongClickListener(this);
            }

            @Override
            public void onClick(View view) {
                if (null != onRecyclerViewListener) {
                    onRecyclerViewListener.onItemClick(position);
                }
            }

            @Override
            public boolean onLongClick(View view) {
                if (null != onRecyclerViewListener) {
                    return onRecyclerViewListener.onItemLongClick(position);
                }
                return false;
            }
        }
    }

    private class ViewPagerAdapter extends PagerAdapter {

        private List<View> pageViews;

        public ViewPagerAdapter(List<View> pageViews) {
            super();
            this.pageViews = pageViews;
        }

        // 显示数目
        @Override
        public int getCount() {
            return pageViews.size();
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == arg1;
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public void destroyItem(View arg0, int arg1, Object arg2) {
            ((ViewPager) arg0).removeView(pageViews.get(arg1));
        }

        @Override
        public Object instantiateItem(View arg0, int arg1) {
            ((ViewPager) arg0).addView(pageViews.get(arg1));
            return pageViews.get(arg1);
        }
    }
}
