package com.thel.ui.widget.moments;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.RelativeLayout;

import com.facebook.drawee.view.SimpleDraweeView;
import com.thel.R;
import com.thel.bean.moments.MomentsBean;
import com.thel.player.video.VideoPlayer;
import com.thel.ui.widget.SquareTextureView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by liuyun on 2017/9/26.
 */

public class MomentVideoLayout extends RelativeLayout {

    private static final String TAG = "MomentVideoLayout";

//    @BindView(R.id.video_view) SquareTextureView video_view;

    @BindView(R.id.moment_content_video_pic) SimpleDraweeView moment_content_video_pic;

    private Surface mSurface;

    private boolean isAvailable = false;

    private String url;

    private String momentsId;

    private enum PlayerStatus {NONE, TRUE, FALSE}

    private PlayerStatus mPlayerStatus = PlayerStatus.NONE;

    public MomentVideoLayout(Context context) {
        super(context);
        initView();
    }

    public MomentVideoLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public MomentVideoLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {

        View view = LayoutInflater.from(getContext()).inflate(R.layout.layout_moment_video, this, true);

        ButterKnife.bind(this, view);

//        video_view.setSurfaceTextureListener(mSurfaceTextureListener);

    }

    public void setData(MomentsBean momentsBean) {

        moment_content_video_pic.setImageURI(momentsBean.thumbnailUrl);

    }

    public void startPlayer(String url, String momentsId) {
        this.url = url;
        this.momentsId = momentsId;
        mPlayerStatus = PlayerStatus.FALSE;
        if (isAvailable) {
            mPlayerStatus = PlayerStatus.TRUE;
            VideoPlayer.getInstance().startPlayer(url, momentsId, mSurface);
        }
    }


    private TextureView.SurfaceTextureListener mSurfaceTextureListener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i1) {

            if (mSurface != null) {
                mSurface.release();
            }

            mSurface = new Surface(surfaceTexture);

            if (mPlayerStatus == PlayerStatus.NONE) {
                if (VideoPlayer.getInstance().getMediaPlayer() != null && mPlayerStatus == PlayerStatus.FALSE) {
                    mPlayerStatus = PlayerStatus.TRUE;
                    VideoPlayer.getInstance().startPlayer(url, momentsId, mSurface);
                }
            }

            isAvailable = true;
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i1) {

            mSurface = new Surface(surfaceTexture);
            if (VideoPlayer.getInstance().getMediaPlayer() != null) {
                VideoPlayer.getInstance().getMediaPlayer().setSurface(mSurface);
            }

        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {

            if (mSurface != null) {
                mSurface.release();
                mSurface = null;
            }

            if (VideoPlayer.getInstance().getMediaPlayer() != null)
                VideoPlayer.getInstance().getMediaPlayer().setSurface(null);

            return false;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {

            mSurface = new Surface(surfaceTexture);
            if (VideoPlayer.getInstance().getMediaPlayer() != null)
                VideoPlayer.getInstance().getMediaPlayer().setSurface(mSurface);

        }
    };

}
