package com.thel.ui.decoration;

import android.graphics.Rect;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.thel.ui.adapter.RecommendUserPicAdapter;

/**
 * Created by liuyun on 2017/12/29.
 */

public class HorizontalItemDivider extends RecyclerView.ItemDecoration {

    private int space;

    public HorizontalItemDivider(int space) {
        this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {

        int position = parent.getChildLayoutPosition(view);

        RecommendUserPicAdapter recommendUserPicAdapter = (RecommendUserPicAdapter) parent.getAdapter();

        int dataSize = recommendUserPicAdapter.getData().size();

        for (int i = 0; i < dataSize; i++) {
            if (i != position) {
                outRect.right = space;
            } else {
                outRect.right = 0;
            }
        }

    }
}
