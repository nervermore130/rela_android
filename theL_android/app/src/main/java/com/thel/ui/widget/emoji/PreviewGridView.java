package com.thel.ui.widget.emoji;

import android.content.Context;
import android.graphics.drawable.Animatable;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.image.ImageInfo;
import com.thel.R;
import com.thel.utils.AppInit;
import com.thel.utils.ImageUtils;
import com.thel.utils.SizeUtils;

import java.util.ArrayList;

/**
 * 触摸预览大图的gridView
 * Created by setsail on 15/8/20.
 */
public class PreviewGridView extends GridView {

    private boolean isPreviewing = false;
    private long actionDownTime = 0;
    private int clickTime = 200;

    private RelativeLayout previewRelativeLayout;
    private RelativeLayout previewView;

    private int[][] arr = null;
    private int numRows = 0;
    private int showingChildIndex = -1;

    private ArrayList<String> previewImages = new ArrayList<String>();

    private Context context;

    private MyOnItemClickListener onItemClickListener = null;

    public PreviewGridView(Context context) {
        super(context);
        this.context = context;
        initPreviewView();
    }

    public PreviewGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initPreviewView();
    }

    public PreviewGridView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        initPreviewView();
    }

    private void initPreviewView() {
        previewView = (RelativeLayout) LinearLayout.inflate(context, R.layout.sticker_preview_view, null);
    }

    private void initArr() {
        numRows = (int) Math.ceil((double) getCount() / (double) getNumColumns());
        arr = new int[numRows][getNumColumns()];
        int index = 0;
        for (int i = 0; i < numRows; i++) {
            for (int y = 0; y < getNumColumns(); y++) {
                arr[i][y] = index;
                index += 1;
            }
        }
    }

    public void setPreviewRelativeLayout(RelativeLayout layout, ArrayList<String> images) {
        refreshPreviewData(images);
        previewRelativeLayout = layout;
        setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        actionDownTime = System.currentTimeMillis();
                        break;
                    case MotionEvent.ACTION_UP:
                        if (onItemClickListener != null && System.currentTimeMillis() - actionDownTime < clickTime) {
                            if (arr == null) {
                                initArr();
                            }
                            int column = (int) Math.floor(motionEvent.getX() / getChildAt(0).getMeasuredWidth());
                            int row = (int) Math.floor(motionEvent.getY() / getChildAt(0).getMeasuredHeight());
                            if (column >= 0 && column < getNumColumns() && row >= 0 && row < numRows && arr[row][column] < previewImages.size()) {
                                onItemClickListener.onItemClick(getChildAt(arr[row][column]));
                            }
                        }
                    case MotionEvent.ACTION_CANCEL:
                        requestDisallowInterceptTouchEvent(false);
                        isPreviewing = false;
                        actionDownTime = 0;
                        cleanShowing(-1);
                        break;
                }
                if (actionDownTime != 0 && System.currentTimeMillis() - actionDownTime >= clickTime) {
                    requestDisallowInterceptTouchEvent(true);
                    isPreviewing = true;
                }
                if (isPreviewing) {
                    showPreviewView(motionEvent);
                } else {
                    previewRelativeLayout.removeAllViews();
                }
                return true;
            }
        });
    }

    public void refreshPreviewData(ArrayList<String> images) {
        this.previewImages.clear();
        this.previewImages.addAll(images);
    }

    private void showPreviewView(MotionEvent motionEvent) {
        if (arr == null) {
            initArr();
        }
        int column = (int) Math.floor(motionEvent.getX() / getChildAt(0).getMeasuredWidth());
        int row = (int) Math.floor(motionEvent.getY() / getChildAt(0).getMeasuredHeight());
        if (column >= 0 && column < getNumColumns() && row >= 0 && row < numRows) {
            if (arr[row][column] >= previewImages.size()) {
                previewRelativeLayout.removeAllViews();
                cleanShowing(-1);
            } else if (showingChildIndex != arr[row][column]) {
                previewRelativeLayout.removeAllViews();
                cleanShowing(arr[row][column]);
                getChildAt(showingChildIndex).setBackgroundColor(context.getResources().getColor(R.color.gray));

                //设置Controller
                ((SimpleDraweeView) previewView.findViewById(R.id.image)).setController(Fresco.newDraweeControllerBuilder().setAutoPlayAnimations(true).setUri(Uri.parse(ImageUtils.buildNetPictureUrl(previewImages.get(showingChildIndex)))).setControllerListener(new BaseControllerListener<ImageInfo>() {
                    @Override
                    public void onSubmit(String id, Object callerContext) {
                        super.onSubmit(id, callerContext);
                        previewView.findViewById(R.id.progress).setVisibility(VISIBLE);
                    }

                    @Override
                    public void onFinalImageSet(String id, ImageInfo imageInfo, Animatable animatable) {
                        super.onFinalImageSet(id, imageInfo, animatable);
                        previewView.findViewById(R.id.progress).setVisibility(GONE);
                    }
                }).build());
                int[] location = new int[2];
                getChildAt(showingChildIndex).getLocationInWindow(location);
                float x = getChildAt(0).getMeasuredWidth() * column + getPaddingLeft() + getChildAt(0).getMeasuredWidth() / 2 - context.getResources().getDimension(R.dimen.sticker_preview_width) / 2;
                // preview超过屏幕右边界，则要调整x坐标，多向左偏移10px
                if (x + previewView.getMeasuredWidth() > AppInit.displayMetrics.widthPixels) {
                    x += AppInit.displayMetrics.widthPixels - (x + previewView.getMeasuredWidth() + 10);
                }
                previewView.setX(x);
                int y = location[1] - SizeUtils.dip2px(context, 150);
                // y坐标超过标题了则要限制
                if (y >= context.getResources().getDimension(R.dimen.title_height) / 2)
                    previewView.setY(y);
                else
                    previewView.setY(context.getResources().getDimension(R.dimen.title_height) / 2);
                previewRelativeLayout.addView(previewView);
            }
        } else {
            previewRelativeLayout.removeAllViews();
            cleanShowing(-1);
        }
    }

    private void cleanShowing(int index) {
        if (showingChildIndex >= 0 && showingChildIndex < getCount()) {
            getChildAt(showingChildIndex).setBackgroundColor(context.getResources().getColor(R.color.transparent));
        }
        showingChildIndex = index;
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, expandSpec);
    }

    public MyOnItemClickListener getMyOnItemClickListener() {
        return onItemClickListener;
    }

    public void setMyOnItemClickListener(MyOnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface MyOnItemClickListener {
        void onItemClick(View view);
    }
}

