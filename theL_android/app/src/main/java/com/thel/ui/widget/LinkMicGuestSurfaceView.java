package com.thel.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.SurfaceView;

public class LinkMicGuestSurfaceView extends SurfaceView {
    public LinkMicGuestSurfaceView(Context context) {
        super(context);
    }

    public LinkMicGuestSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LinkMicGuestSurfaceView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = getMeasuredWidth() / 3;
        int height = width * 16 / 9;
        setMeasuredDimension(width, height);
    }
}
