package com.thel.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.thel.R;
import com.thel.utils.AppInit;
import com.thel.utils.ToastUtils;

import androidx.fragment.app.DialogFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import video.com.relavideolibrary.Utils.DensityUtils;

/**
 * Created by chad
 * Time 18/9/12
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */
public class MatchSuperlikeDialog extends DialogFragment implements TextWatcher {

    @OnClick(R.id.close)
    void close() {
        this.dismiss();
    }

    @OnClick(R.id.cancel)
    void cancel() {
        this.dismiss();
    }

    @OnClick(R.id.superlike_btn)
    void superlike_btn() {
        String content = edit_superlike.getText().toString();
        if (superlikeListener != null) {
            superlikeListener.superlike(content);
        }
        this.dismiss();
    }

    @BindView(R.id.last_count)
    TextView last_count;

    @BindView(R.id.edit_superlike)
    EditText edit_superlike;

    public static MatchSuperlikeDialog newInstance() {
        MatchSuperlikeDialog matchSuperlikeDialog = new MatchSuperlikeDialog();
        return matchSuperlikeDialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.match_super_like_dialog_layout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        edit_superlike.addTextChangedListener(this);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), R.style.DialogFadeAnim);

        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER); //可设置dialog的位置
        window.getDecorView().setPadding(0, 0, 0, 0); //消除边距
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.width = AppInit.displayMetrics.widthPixels - DensityUtils.dp2px(50);   //设置宽度充满屏幕
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        return dialog;
    }

    @Override
    public void onDestroy() {
        if (getActivity() != null) {
            //拿到InputMethodManager
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            //如果window上view获取焦点 && view不为空
            if (imm != null && imm.isActive() && getActivity().getCurrentFocus() != null) {
                //拿到view的token 不为空
                if (getActivity().getCurrentFocus().getWindowToken() != null) {
                    //表示软键盘窗口总是隐藏，除非开始时以SHOW_FORCED显示。
                    imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                }
            }
        }
        super.onDestroy();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (s.length() > 50) {
            ToastUtils.showToastShort(getContext(), "最多填写50个字符");
        }
        last_count.setText(String.valueOf(50 - s.length()));
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    private SuperlikeListener superlikeListener;

    public void setSuperlikeListener(SuperlikeListener superlikeListener) {
        this.superlikeListener = superlikeListener;
    }

    public interface SuperlikeListener {
        void superlike(String content);
    }
}
