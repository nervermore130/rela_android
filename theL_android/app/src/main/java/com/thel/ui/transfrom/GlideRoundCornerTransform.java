package com.thel.ui.transfrom;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import androidx.annotation.NonNull;

import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;

import java.security.MessageDigest;

/**
 * Created by waiarl on 2017/10/18.
 */

public class GlideRoundCornerTransform extends BitmapTransformation {
    private static RoundRadii radii;

    public GlideRoundCornerTransform() {
        this(0);
    }

    public GlideRoundCornerTransform(float radius) {
        radii = new RoundRadii(radius);
    }

    public GlideRoundCornerTransform(float[] radius) {
        radii = new RoundRadii(radius);
    }

    @Override
    protected Bitmap transform(BitmapPool pool, Bitmap toTransform, int outWidth, int outHeight) {
        return roundCrop(pool, toTransform);
    }

    private static Bitmap roundCrop(BitmapPool pool, Bitmap source) {
        if (source == null)
            return null;

        Bitmap result = pool.get(source.getWidth(), source.getHeight(), Bitmap.Config.ARGB_8888);
        if (result == null) {
            result = Bitmap.createBitmap(source.getWidth(), source.getHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(result);
        Paint paint = new Paint();
        paint.setShader(new BitmapShader(source, BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP));
        paint.setAntiAlias(true);
        final RectF rectF = new RectF(0f, 0f, source.getWidth(), source.getHeight());
        final Path path = new Path();
        if (radii == null) {
            radii = new RoundRadii();
        }
        path.addRoundRect(rectF, radii.beArray(), Path.Direction.CW);
        canvas.drawPath(path, paint);
        return result;
    }

    @Override public void updateDiskCacheKey(@NonNull MessageDigest messageDigest) {

    }

    protected static class RoundRadii {
        float leftTopX;
        float leftTopY;
        float rightTopX;
        float rightTopY;
        float rightBottomX;
        float rightBottomY;
        float leftBottomX;
        float leftBottomY;

        public RoundRadii() {

        }

        public RoundRadii(float radius) {
            setRadii(radius);
        }

        public RoundRadii(float[] radii) {
            if (radii == null) {
                return;
            }
            if (radii.length == 1) {
                setRadii(radii[0]);
                return;
            }
            if (radii.length == 2) {
                setRadii(radii[0], radii[1]);
                return;
            }
            if (radii.length == 4) {
                setRadii(radii[0], radii[1], radii[2], radii[3]);
                return;
            }
            if (radii.length == 8) {
                setRadii(radii[0], radii[1], radii[2], radii[3], radii[4], radii[5], radii[6], radii[7]);
            }

        }

        private void setRadii(float leftTopX, float leftTopY, float rightTopX, float rightTopY, float rightBottomX, float rightBottomY, float leftBottomX, float leftBottomY) {
            this.leftTopX = leftTopX;
            this.leftTopY = leftTopY;
            this.rightTopX = rightTopX;
            this.rightTopY = rightTopY;
            this.rightBottomX = rightBottomX;
            this.rightBottomY = rightBottomY;
            this.leftBottomX = leftBottomX;
            this.leftBottomY = leftBottomY;
        }


        private void setRadii(float leftTopRadius, float rightTopRadius, float rightBottomRadius, float leftBottomRadius) {
            this.leftTopX = leftTopRadius;
            this.leftTopY = leftTopRadius;
            this.rightTopX = rightTopRadius;
            this.rightTopY = rightTopRadius;
            this.rightBottomX = rightBottomRadius;
            this.rightBottomY = rightBottomRadius;
            this.leftBottomX = leftBottomRadius;
            this.leftBottomY = leftBottomRadius;
        }

        private void setRadii(float x, float y) {
            this.leftTopX = x;
            this.leftTopY = y;
            this.rightTopX = x;
            this.rightTopY = y;
            this.rightBottomX = x;
            this.rightBottomY = y;
            this.leftBottomX = x;
            this.leftBottomY = y;
        }

        private void setRadii(float radius) {
            this.leftTopX = radius;
            this.leftTopY = radius;
            this.rightTopX = radius;
            this.rightTopY = radius;
            this.rightBottomX = radius;
            this.rightBottomY = radius;
            this.leftBottomX = radius;
            this.leftBottomY = radius;
        }

        public float[] beArray() {
            return new float[]{leftTopX, leftTopY, rightTopX, rightTopY, rightBottomX, rightBottomY, leftBottomX, leftBottomY};
        }
    }
}
