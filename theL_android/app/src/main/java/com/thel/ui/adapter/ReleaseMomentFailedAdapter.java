package com.thel.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseAdapter;
import com.thel.bean.ReleaseMomentListBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.constants.TheLConstants;
import com.thel.db.MomentsDataBaseAdapter;
import com.thel.modules.main.home.moments.ReleaseMomentActivity;
import com.thel.modules.main.home.moments.ReleaseThemeMomentActivity;
import com.thel.service.ReleaseMomentsService;
import com.thel.utils.DialogUtil;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.ViewUtils;

import java.util.ListIterator;

import javax.annotation.Nullable;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.thel.bean.moments.MomentsBean.MOMENT_TYPE_IMAGE;
import static com.thel.bean.moments.MomentsBean.MOMENT_TYPE_TEXT;
import static com.thel.bean.moments.MomentsBean.MOMENT_TYPE_TEXT_IMAGE;
import static com.thel.bean.moments.MomentsBean.MOMENT_TYPE_THEME;
import static com.thel.bean.moments.MomentsBean.MOMENT_TYPE_VIDEO;

public class ReleaseMomentFailedAdapter extends BaseAdapter<ReleaseMomentFailedAdapter.ReleaseMomentFailedViewHolder, MomentsBean> {

    public ReleaseMomentFailedAdapter(Context context) {
        super(context);
    }

    @Override public void onBindViewHolder(@Nullable ReleaseMomentFailedViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);

        MomentsBean moment = getData().get(position);

        holder.img_play.setVisibility(View.GONE);
        holder.resend.setVisibility(View.VISIBLE);
        holder.delete.setVisibility(View.VISIBLE);
        if (MOMENT_TYPE_IMAGE.equals(moment.momentsType) || MOMENT_TYPE_TEXT_IMAGE.equals(moment.momentsType) || MOMENT_TYPE_VIDEO.equals(moment.momentsType)) {//图片日志、视频日志
            if (!TextUtils.isEmpty(moment.imageUrl)) {
                String[] imgs = moment.imageUrl.split(",");
                if (imgs.length > 0) {
                    holder.preview_img.setVisibility(View.VISIBLE);
                    holder.preview_txt.setVisibility(View.GONE);
                    holder.preview_img.setImageURI(Uri.parse(TheLConstants.FILE_PIC_URL + imgs[0]));
                }
            }
            if (MOMENT_TYPE_VIDEO.equals(moment.momentsType)) {
                holder.img_play.setVisibility(View.VISIBLE);
                holder.img_play.setImageResource(R.mipmap.btn_play_video);
            }
        } else if (MomentsBean.MOMENT_TYPE_VOICE.equals(moment.momentsType) || MomentsBean.MOMENT_TYPE_TEXT_VOICE.equals(moment.momentsType)) {//音乐日志
            holder.preview_img.setVisibility(View.VISIBLE);
            holder.preview_txt.setVisibility(View.GONE);
            holder.preview_img.setImageURI(Uri.parse(moment.albumLogo100));
            holder.img_play.setVisibility(View.VISIBLE);
            holder.img_play.setImageResource(R.mipmap.btn_feed_play_big);
        } else {// 文字日志或话题日志
            holder.preview_txt.setVisibility(View.VISIBLE);
            holder.preview_img.setVisibility(View.GONE);
            holder.preview_txt.setText(moment.momentsText);
        }

        holder.text.setText(TheLApp.getContext().getString(R.string.post_moment_failed));

        holder.resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ViewUtils.preventViewMultipleClick(view, 1000);
                Intent intent = new Intent(TheLApp.getContext(), ReleaseMomentsService.class);
                // 要发的日志标识，用发布时间作为标识
                intent.putExtra(MomentsDataBaseAdapter.F_RELEASE_TIME, moment.releaseTime);
                TheLApp.getContext().startService(intent);

//                deleteMomentsBean(moment);

                getData().remove(moment);

                notifyDataSetChanged();

            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewUtils.preventViewMultipleClick(view, 1000);
                DialogUtil.showConfirmDialog((Activity) view.getContext(), "", TheLApp.getContext().getString(R.string.post_moment_failed_delete), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();

                        Intent intent = new Intent();
                        intent.setAction(TheLConstants.BROADCAST_RELEASE_MOMENT_DELETE_FAILED);
                        intent.putExtra(TheLConstants.BUNDLE_KEY_RELEASE_TIME, moment.releaseTime);
                        TheLApp.getContext().sendBroadcast(intent);

                        deleteMomentsBean(moment);

                        getData().remove(moment);

                        notifyDataSetChanged();
                    }
                });
            }
        });

        holder.rewrite_iv.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                Intent intent;
                switch (moment.momentsType) {
                    case MOMENT_TYPE_IMAGE:
                    case MOMENT_TYPE_TEXT_IMAGE:
                        intent = new Intent(TheLApp.getContext(), ReleaseMomentActivity.class);
                        intent.putExtra(TheLConstants.BUNDLE_KEY_RELEASE_TYPE, ReleaseMomentActivity.RELEASE_TYPE_PHOTOS);
                        intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_BEAN, moment);
                        v.getContext().startActivity(intent);
                        break;
                    case MOMENT_TYPE_TEXT:
                        intent = new Intent(TheLApp.getContext(), ReleaseMomentActivity.class);
                        intent.putExtra(TheLConstants.BUNDLE_KEY_RELEASE_TYPE, ReleaseMomentActivity.RELEASE_TYPE_TEXT);
                        intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_BEAN, moment);
                        v.getContext().startActivity(intent);
                        break;
                    case MOMENT_TYPE_VIDEO:
                        intent = new Intent(TheLApp.getContext(), ReleaseMomentActivity.class);
                        intent.putExtra(TheLConstants.BUNDLE_KEY_RELEASE_TYPE, ReleaseMomentActivity.RELEASE_TYPE_VIDEO);
                        intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_BEAN, moment);
                        v.getContext().startActivity(intent);
                        break;
                    case MOMENT_TYPE_THEME:
                        intent = new Intent(TheLApp.getContext(), ReleaseThemeMomentActivity.class);
                        intent.putExtra(TheLConstants.BUNDLE_KEY_RELEASE_TYPE, ReleaseThemeMomentActivity.RELEASE_TYPE_TOPIC);
                        intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_BEAN, moment);
                        v.getContext().startActivity(intent);
                        break;
                    default:
                        break;
                }

                deleteMomentsBean(moment);

                getData().remove(moment);

                notifyDataSetChanged();
            }
        });

    }

    @Override public ReleaseMomentFailedViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.send_failed_moments_listitem, null);

        return new ReleaseMomentFailedViewHolder(view);
    }


    public class ReleaseMomentFailedViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.preview) RelativeLayout preview;

        @BindView(R.id.preview_img) SimpleDraweeView preview_img;

        @BindView(R.id.img_play) ImageView img_play;

        @BindView(R.id.preview_txt) TextView preview_txt;

        @BindView(R.id.text) TextView text;

        @BindView(R.id.delete) ImageView delete;

        @BindView(R.id.resend) ImageView resend;

        @BindView(R.id.rewrite_iv) ImageView rewrite_iv;

        public ReleaseMomentFailedViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

        }
    }

    public void initData() {
        ReleaseMomentListBean releaseMomentListBean = getReleaseFailedMoment();

        if (releaseMomentListBean != null && releaseMomentListBean.list != null && releaseMomentListBean.list.size() > 0) {

            setData(releaseMomentListBean.list);
        }
    }


    private void deleteMomentsBean(MomentsBean moment) {
        ReleaseMomentListBean releaseMomentListBean = getReleaseFailedMoment();

        if (releaseMomentListBean != null && releaseMomentListBean.list != null && releaseMomentListBean.list.size() > 0) {
            ListIterator<MomentsBean> listIterator = releaseMomentListBean.list.listIterator();

            while (listIterator.hasNext()) {
                MomentsBean momentsBean = listIterator.next();
                if (momentsBean.momentsId.equals(moment.momentsId)) {
                    listIterator.remove();
                }
            }

            saveReleaseFailedMoment(releaseMomentListBean);
        }
    }

    private ReleaseMomentListBean getReleaseFailedMoment() {
        String content = ShareFileUtils.getString(TheLConstants.RELEASE_CONTENT_KEY, null);

        L.subLog("ReleaseMomentFailedAdapter", " content : " + content);

        if (content != null) {
            return GsonUtils.getObject(content, ReleaseMomentListBean.class);
        }
        return null;

    }

    public void saveReleaseFailedMoment(ReleaseMomentListBean releaseMomentListBean) {

        String json = GsonUtils.createJsonString(releaseMomentListBean);

        ShareFileUtils.setString(TheLConstants.RELEASE_CONTENT_KEY, json);
    }
}
