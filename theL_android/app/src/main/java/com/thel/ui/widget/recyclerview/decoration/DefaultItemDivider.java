package com.thel.ui.widget.recyclerview.decoration;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

/**
 * Created by setsail on 15/6/25.
 */
public class DefaultItemDivider extends RecyclerView.ItemDecoration {
    /*
          * RecyclerView的布局方向，默认先赋值
          * 为纵向布局
          * RecyclerView 布局可横向，也可纵向
          * 横向和纵向对应的分割想画法不一样
          * */
    private int mOrientation = LinearLayoutManager.VERTICAL;

    /**
     * item之间分割线的size，默认为1
     */
    private int mItemSize = 1;

    /**
     * 绘制item分割线的画笔，和设置其属性
     * 来绘制个性分割线
     */
    private Paint mPaint;

    /**
     * 是否显示头部divider
     */
    private boolean enableHeaderDivider = true;

    /**
     * 是否显示尾部divider
     */
    private boolean enableFooterDivider = true;
    /**
     * 尾部的上面是否绘制（footer上面）
     */
    private boolean enableFooterTop = true;


    /**
     * 构造方法传入布局方向，不可不传
     *
     * @param context
     * @param orientation
     */
    public DefaultItemDivider(Context context, int orientation, int color, int size, boolean enableHeaderDivider, boolean enableFooterDivider) {
        this.enableHeaderDivider = enableHeaderDivider;
        this.enableFooterDivider = enableFooterDivider;
        this.mItemSize = size;
        this.mOrientation = orientation;
        if (orientation != LinearLayoutManager.VERTICAL && orientation != LinearLayoutManager.HORIZONTAL) {
            throw new IllegalArgumentException("请传入正确的参数");
        }
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setColor(ContextCompat.getColor(context, color));
         /*设置填充*/
        mPaint.setStyle(Paint.Style.FILL);
    }

    /**
     * 构造方法传入布局方向，不可不传
     *
     * @param context
     * @param orientation
     */
    public DefaultItemDivider(Context context, int orientation, int color, int size, boolean enableHeaderDivider, boolean enableFooterDivider, boolean enableFooterTop) {//新增参数，footerTop是否绘制
        this.enableHeaderDivider = enableHeaderDivider;
        this.enableFooterDivider = enableFooterDivider;
        this.enableFooterTop = enableFooterTop;
        this.mItemSize = size;
        this.mOrientation = orientation;
        if (orientation != LinearLayoutManager.VERTICAL && orientation != LinearLayoutManager.HORIZONTAL) {
            throw new IllegalArgumentException("请传入正确的参数");
        }
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setColor(ContextCompat.getColor(context, color));
         /*设置填充*/
        mPaint.setStyle(Paint.Style.FILL);
    }

    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        if (mOrientation == LinearLayoutManager.VERTICAL) {
            drawVertical(c, parent);
        } else {
            drawHorizontal(c, parent);
        }
    }

    /**
     * 绘制纵向 item 分割线
     *
     * @param canvas
     * @param parent
     */
    private void drawVertical(Canvas canvas, RecyclerView parent) {
        final int left = parent.getPaddingLeft();
        final int right = parent.getMeasuredWidth() - parent.getPaddingRight();
        final int childSize = parent.getChildCount();
        boolean skipFirst = false;
        boolean skipLast = false;
        final RecyclerView.LayoutManager layoutManager = parent.getLayoutManager();
        if (layoutManager instanceof LinearLayoutManager) {
            if (((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition() == 0)
                skipFirst = true;
            if (((LinearLayoutManager) layoutManager).findLastVisibleItemPosition() + 1 == layoutManager.getItemCount()) {
                skipLast = true;
            }
        }
        for (int i = 0; i < childSize; i++) {
            final View child = parent.getChildAt(i);
            if (i == 0 && !enableHeaderDivider && skipFirst) {
                continue;
            }
            if (i == childSize - 1 && !enableFooterDivider && skipLast) {
                continue;
            }
            if (i == childSize - 2 && !enableFooterDivider) {
                continue;
            }
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) child.getLayoutParams();
            final int top = child.getBottom() + layoutParams.bottomMargin;
            final int bottom = top + mItemSize;
            canvas.drawRect(left, top, right, bottom, mPaint);
        }
    }

    /**
     * 绘制横向 item 分割线
     *
     * @param canvas
     * @param parent
     */
    private void drawHorizontal(Canvas canvas, RecyclerView parent) {
        final int top = parent.getPaddingTop();
        final int bottom = parent.getMeasuredHeight() - parent.getPaddingBottom();
        final int childSize = parent.getChildCount();
        for (int i = 0; i < childSize; i++) {
            final View child = parent.getChildAt(i);
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) child.getLayoutParams();
            final int left = child.getRight() + layoutParams.rightMargin;
            final int right = left + mItemSize;
            canvas.drawRect(left, top, right, bottom, mPaint);
        }
    }

    /**
     * 设置item分割线的size
     *
     * @param outRect
     * @param view
     * @param parent
     * @param state
     */
    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {

        final int itemPosition = parent.getChildAdapterPosition(view);
        final int itemCount = state.getItemCount();
        if (itemPosition == RecyclerView.NO_POSITION) {
            return;
        }
        if (!enableHeaderDivider && itemPosition == 0) {
            outRect.top = 0;
            outRect.setEmpty();
        } else if (!enableFooterDivider && itemCount > 0 && itemPosition == itemCount - 1) {
            outRect.setEmpty();
        } else if (!enableFooterTop && itemCount > 0 && itemPosition == itemCount - 2) {
            outRect.setEmpty();
        } else {
            if (mOrientation == LinearLayoutManager.VERTICAL) {
                outRect.set(0, 0, 0, mItemSize);
            } else {
                outRect.set(0, 0, mItemSize, 0);
            }
        }
    }
}
