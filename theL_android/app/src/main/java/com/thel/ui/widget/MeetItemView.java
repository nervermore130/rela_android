package com.thel.ui.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.thel.R;
import com.thel.bean.live.LiveMultiSeatBean;
import com.thel.constants.TheLConstants;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.live.bean.LiveRoomMsgBean;
import com.thel.modules.live.view.SoftGiftMsgItemCountView;
import com.thel.ui.transfrom.GlideCircleTransform;
import com.thel.utils.L;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by liuyun on 2018/5/21.
 */
public class MeetItemView extends LinearLayout {

    @BindView(R.id.user_is_talking)
    LottieAnimationView user_is_talking;

    @BindView(R.id.selected_her)
    RelativeLayout selected_her;

    @BindView(R.id.img_thumb_live)
    ImageView img_thumb_live;

    @BindView(R.id.image_ban_mic)
    ImageView image_ban_mic;

    @BindView(R.id.selected_heart)
    LottieAnimationView selected_heart;

    @BindView(R.id.user_name)
    TextView user_name;

    @BindView(R.id.rel_heart_num)
    RelativeLayout rel_heart_num;

    @BindView(R.id.heart)
    ImageView heart;

    @BindView(R.id.heart_num)
    TextView heart_num;

    @BindView(R.id.gift_iv)
    ImageView gift_iv;

    @BindView(R.id.lin_gift_count)
    SoftGiftMsgItemCountView lin_gift_count;

    @BindView(R.id.gift_ll)
    LinearLayout gift_ll;

    public MeetItemView(Context context) {
        super(context);
        initView(context);
    }

    public MeetItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public MeetItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {

        setGravity(Gravity.CENTER_HORIZONTAL);

        View view = LayoutInflater.from(context).inflate(R.layout.sound_multi_item, this, true);

        ButterKnife.bind(this, view);

        user_is_talking.addAnimatorListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                user_is_talking.setVisibility(INVISIBLE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

    }

    public void refreshHeartNum(int heartNum) {
        heart_num.setText(String.valueOf(heartNum));
    }

    public void refreshSeat(LiveMultiSeatBean item) {
        final int position = (int) getTag();

        if (item != null) {
            if (TextUtils.isEmpty(item.userId)) {
                user_name.setText(TheLConstants.defaultSeatNames[position]);
                rel_heart_num.setVisibility(View.INVISIBLE);
            } else {
                user_name.setText(item.nickname);
                rel_heart_num.setVisibility(View.VISIBLE);
            }
            heart_num.setText(String.valueOf(item.heartNum));
            if (TextUtils.isEmpty(item.micStatus) || item.micStatus.equals(LiveRoomMsgBean.TYPE_MIC_STATUS_ON)) {
                image_ban_mic.setVisibility(View.INVISIBLE);
            } else {
                image_ban_mic.setVisibility(View.VISIBLE);
            }

            ImageLoaderManager.imageLoaderCircle(img_thumb_live, R.mipmap.icn_multi_seat, item.avatar);

            if (item.isSelected) {
                selected_heart.setVisibility(View.VISIBLE);
                selected_heart.playAnimation();
            } else {
                selected_heart.setVisibility(View.INVISIBLE);
            }
        } else {
            image_ban_mic.setVisibility(INVISIBLE);
            rel_heart_num.setVisibility(INVISIBLE);
            ImageLoaderManager.imageLoader(img_thumb_live, R.mipmap.icn_multi_seat);
            user_name.setText(TheLConstants.defaultSeatNames[position]);
        }
    }

    /**
     * 音量动画
     */
    public void speakAnimation() {
        if (user_is_talking.getVisibility() == View.INVISIBLE) {
            user_is_talking.setVisibility(VISIBLE);
            user_is_talking.playAnimation();
        }
    }

    public void setGift(String giftIconUrl, int count) {

        L.d("MeetItemView", " giftIconUrl : " + giftIconUrl);

        L.d("MeetItemView", " count : " + count);


        if (gift_ll != null) {
            gift_ll.setVisibility(VISIBLE);
        }

        startScaleAnimator();

        if (gift_iv != null) {
            if (giftIconUrl != null) {
                ImageLoaderManager.imageLoader(gift_iv, giftIconUrl);
            }
            if (count > 0) {
                lin_gift_count.refreshCombo(count);
            }
        }
    }

    private void startScaleAnimator() {
        final ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0);
        valueAnimator.setDuration(2000);
        valueAnimator.setInterpolator(new LinearInterpolator());
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override public void onAnimationUpdate(ValueAnimator animation) {
                float offset = (float) animation.getAnimatedValue();
                if (gift_ll != null) {
                    gift_ll.setScaleX(offset);
                    gift_ll.setScaleY(offset);
                }
            }
        });

        valueAnimator.addListener(new AnimatorListenerAdapter() {
            @Override public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                if (gift_ll != null) {
                    gift_ll.setLayerType(LAYER_TYPE_SOFTWARE, null);
                }
            }

            @Override public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);

                if (gift_ll != null) {
                    gift_ll.setLayerType(LAYER_TYPE_NONE, null);
                    gift_ll.setVisibility(INVISIBLE);
                }

                valueAnimator.cancel();
            }
        });
        valueAnimator.start();
    }

}
