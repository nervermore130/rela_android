package com.thel.ui.widget;

import android.content.Context;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import com.thel.utils.SizeUtils;

public class GuideAvatarLayout extends LinearLayout {

    int padding = SizeUtils.dip2px(getContext(), 5);

    public GuideAvatarLayout(Context context) {
        super(context);
    }

    public GuideAvatarLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public GuideAvatarLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int measuredWidth = getMeasuredWidth();

        int measuredHeight = (measuredWidth - (padding * 2)) / 3;

        setMeasuredDimension(measuredWidth, measuredHeight);
    }

    @Override protected void onLayout(boolean changed, int left, int top, int right, int bottom) {

        int childViewCount = getChildCount();

        int measuredWidth = getMeasuredWidth();

        int viewWidth = (measuredWidth - padding - padding) / 3;

        for (int i = 0; i < childViewCount; i++) {

            View childView = getChildAt(i);

            childView.layout(viewWidth * i + padding * i, 0, viewWidth * (i + 1) + padding * i, viewWidth);

        }

    }
}
