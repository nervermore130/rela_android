package com.thel.ui.widget;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.live.LiveMultiSeatBean;
import com.thel.bean.live.MultiSpeakersBean;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.live.bean.LiveRoomMsgBean;
import com.thel.utils.L;
import com.thel.utils.SizeUtils;
import com.thel.utils.UserUtils;

import java.util.List;

/**
 * Created by liuyun on 2018/5/21.
 */
public class MeetItemViewGroup extends ViewGroup {

    private List<LiveMultiSeatBean> mData;

    public MeetItemViewGroup(Context context) {
        super(context);
        initView(context);
    }

    public MeetItemViewGroup(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public MeetItemViewGroup(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int childCount = getChildCount();       //获取子View的数量
        //使用遍历的方式通知子View对自身进行测量
        for (int i = 0; i < childCount; i++) {
            View childView = getChildAt(i);
            measureChild(childView, widthMeasureSpec, heightMeasureSpec);
        }
    }

    @Override
    public LayoutParams generateLayoutParams(AttributeSet attrs) {
        return new MarginLayoutParams(getContext(), attrs);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        final int childCount = getChildCount();
        int itemHeight = SizeUtils.dip2px(TheLApp.context, 120);
        int itemWidth = getMeasuredWidth() / 4;
        for (int i = 0; i < childCount; i++) {
            View view = getChildAt(i);
            int j;
            if (i > 3) {
                j = 1;
            } else {
                j = 0;
            }
            view.layout(i % 4 * itemWidth, itemHeight * j, (i % 4 + 1) * itemWidth, itemHeight * (j + 1));
        }
    }

    private void initView(Context context) {

        for (int i = 0; i < 8; i++) {
            MeetItemView meetItemView = new MeetItemView(context);
            meetItemView.setTag(i);
            ImageView avatar = meetItemView.findViewById(R.id.img_thumb_live);
            final int finalI = i;
            avatar.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(v, finalI);
                    }
                }
            });
            addView(meetItemView);
        }

    }

    public boolean isOnSeat(String uid) {
        boolean isOnSeat = false;
        for (int i = 0; i < getData().size(); i++) {
            if (getData().get(i).userId.equals(uid)) {
                //我已经在某个座位上
                isOnSeat = true;
                break;
            }
        }
        return isOnSeat;
    }

    /**
     * 刷新所有座位
     *
     * @param liveMultiSeatBeans
     */
    public void refreshAllSeats(List<LiveMultiSeatBean> liveMultiSeatBeans) {
        if (liveMultiSeatBeans != null) {
            for (int i = 0; i < liveMultiSeatBeans.size(); i++) {
                liveMultiSeatBeans.get(i).seatNum = i;
                MeetItemView meetItemView = (MeetItemView) getChildAt(i);
                meetItemView.refreshSeat(liveMultiSeatBeans.get(i));
            }
            this.mData = liveMultiSeatBeans;
        }
    }

    /**
     * 刷新单个座位
     *
     * @param liveMultiSeatBean
     */
    public void refreshSeat(LiveMultiSeatBean liveMultiSeatBean) {
        if (liveMultiSeatBean != null) {
            int seatNum = liveMultiSeatBean.seatNum;
            MeetItemView meetItemView = (MeetItemView) getChildAt(seatNum);
            switch (liveMultiSeatBean.method) {
                case LiveRoomMsgBean.TYPE_METHOD_ONSEAT:
                    mData.set(seatNum, liveMultiSeatBean);
                    meetItemView.refreshSeat(mData.get(seatNum));
                    break;
                case LiveRoomMsgBean.TYPE_METHOD_OFFSEAT:
                    mData.set(seatNum, new LiveMultiSeatBean());
                    meetItemView.refreshSeat(mData.get(seatNum));
                    break;
                case LiveRoomMsgBean.TYPE_METHOD_MUTE:
                    mData.get(seatNum).micStatus = liveMultiSeatBean.micStatus;
                    meetItemView.refreshSeat(mData.get(seatNum));
                    break;
                case LiveRoomMsgBean.TYPE_METHOD_GUEST_GIFT:
                    mData.get(seatNum).heartNum = liveMultiSeatBean.heartNum;
                    meetItemView.refreshHeartNum(mData.get(seatNum).heartNum);
                    break;
            }
        }
    }

    /**
     * 刷新指定座位音量动画
     */
    public void refreshSeatAnimation(MultiSpeakersBean speakersBean) {
        if (speakersBean != null && speakersBean.speakers != null) {
            for (int i = 0; i < speakersBean.speakers.size(); i++) {
                MultiSpeakersBean.Speakers speaker = speakersBean.speakers.get(i);
                if (speaker != null) {
                    for (int j = 0; j < mData.size(); j++) {
                        String seatUidStr = getItem(j).userId;
                        if (!TextUtils.isEmpty(seatUidStr) && seatUidStr.equals(String.valueOf(speaker.uid))) {
                            MeetItemView meetItemView = (MeetItemView) getChildAt(j);
                            if (speakersBean.speakers.get(i).volumn > 28 && getItem(j).micStatus.equals(LiveRoomMsgBean.TYPE_MIC_STATUS_ON)) {
                                meetItemView.speakAnimation();
                            }
                            break;
                        }
                    }
                }
            }
        }
    }

    public void meetConfirm() {
        for (int i = 0; i < getChildCount(); i++) {
            MeetItemView meetItemView = (MeetItemView) getChildAt(i);
            if (getItem(i).isSelected) {
                meetItemView.setAlpha(1.0f);
            } else {
                meetItemView.setAlpha(0.5f);
            }
        }
    }

    public LiveMultiSeatBean getItem(int position) {
        if (mData != null) return mData.get(position);
        return null;
    }

    public int getNotSeatPosition() {

        if (mData != null) {

            for (int i = 0; i < mData.size(); i++) {
                LiveMultiSeatBean liveMultiSeatBean = mData.get(i);
                if (liveMultiSeatBean.userId == null || liveMultiSeatBean.userId.length() == 0) {
                    return i;
                }
            }

        }

        return -1;
    }

    public int getUserSeatPositionByUserId(String userId) {

        int position = -1;

        L.d("MeetItemViewGroup", " userId : " + userId);

        L.d("MeetItemViewGroup", " mData : " + mData.toString());

        if (mData != null) {
            for (int i = 0; i < mData.size(); i++) {
                LiveMultiSeatBean liveMultiSeatBean = mData.get(i);
                if (liveMultiSeatBean.userId != null && liveMultiSeatBean.userId.equals(userId)) {
                    return i;
                }
            }
        }

        return position;
    }

    public List<LiveMultiSeatBean> getData() {
        return mData;
    }

    public boolean isGuest(String userId) {
        if (mData == null) {
            return false;
        }

        for (LiveMultiSeatBean liveMultiSeatBean : mData) {
            if (liveMultiSeatBean.userId.equals(userId)) {
                return true;
            }
        }

        return false;

    }

    public void clear() {
        removeAllViews();
    }

    public void clearAllView() {

        int childCount = getChildCount();

        for (int i = 0; i < childCount; i++) {
            MeetItemView meetItemView = (MeetItemView) getChildAt(i);
            meetItemView.refreshSeat(null);
        }

    }

    public boolean isGuest() {
        return isGuest(UserUtils.getMyUserId());
    }

    public void setGift(String giftIconUrl, int count, String toUserId) {
        int position = getUserSeatPositionByUserId(toUserId);
        if (position >= 0) {
            MeetItemView meetItemView = (MeetItemView) getChildAt(position);
            if (meetItemView != null) {
                meetItemView.setGift(giftIconUrl, count);
            }
        }

    }

    private OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(OnItemClickListener mOnItemClickListener) {
        this.mOnItemClickListener = mOnItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

}
