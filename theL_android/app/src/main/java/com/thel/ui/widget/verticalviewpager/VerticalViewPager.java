package com.thel.ui.widget.verticalviewpager;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.thel.ui.widget.SensitiveViewPager;

/**
 * Created by liuyun on 2018/2/3.
 */

public class VerticalViewPager extends SensitiveViewPager {

    private boolean isCanScroll = true;

    public VerticalViewPager(Context context) {
        this(context, null);
    }

    public VerticalViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);

        setPageTransformer(false, new DefaultTransformer());

    }

    private MotionEvent swapTouchEvent(MotionEvent event) {
        float width = getWidth();
        float height = getHeight();

        float swappedX = (event.getY() / height) * width;
        float swappedY = (event.getX() / width) * height;

        event.setLocation(swappedX, swappedY);

        return event;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {

        if (isCanScroll) {
            boolean intercept = super.onInterceptTouchEvent(swapTouchEvent(event));
            //If not intercept, touch event should not be swapped.
            swapTouchEvent(event);
            return intercept;
        } else {
            return false;
        }

    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (isCanScroll) {
            return super.onTouchEvent(swapTouchEvent(ev));
        } else {
            return false;
        }
    }

    public void isCanScroll(boolean isCanScroll) {
        this.isCanScroll = isCanScroll;
    }

}
