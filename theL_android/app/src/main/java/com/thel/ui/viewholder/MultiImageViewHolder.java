package com.thel.ui.viewholder;

import android.view.View;

import butterknife.ButterKnife;

/**
 * Created by liuyun on 2017/11/11.
 */

public class MultiImageViewHolder extends MomentDefaultViewHolder {

    public MultiImageViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

}
