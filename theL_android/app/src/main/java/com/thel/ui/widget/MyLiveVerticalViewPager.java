package com.thel.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;

import com.thel.utils.L;

import fr.castorflex.android.verticalviewpager.VerticalViewPager;

/**
 * Created by the L on 2017/2/13.
 */

public class MyLiveVerticalViewPager extends VerticalViewPager {
    private final String TAG = MyLiveVerticalViewPager.class.getSimpleName();
    private boolean mScolllable = true;
    private GestureDetector mGestureDetector;
    private ScrollPageChangedListener mScrollPageChangedListener;
    private final float minFling = 200;


    public enum Direction {
        LEFT, RIGHT
    }


    public MyLiveVerticalViewPager(Context context) {
        this(context, null);
    }

    public MyLiveVerticalViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        setListener();
    }

    private void setListener() {
        mGestureDetector = new GestureDetector(getContext(), new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                final float x = e2.getX() - e1.getX();
                L.i(TAG, "onFling:x=" + x + ",e1x=" + e1.getX() + ",e2x=" + e2.getX() + ",vx=" + velocityX + ",vy=" + velocityY);
                if (x > minFling) {//向右
                    if (mScrollPageChangedListener != null) {
                        mScrollPageChangedListener.onFling(Direction.RIGHT);
                    }
                } else if (x < -minFling) {//向左
                    if (mScrollPageChangedListener != null) {
                        mScrollPageChangedListener.onFling(Direction.LEFT);
                    }
                }
                return super.onFling(e1, e2, velocityX, velocityY);
            }
        });
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        mGestureDetector.onTouchEvent(ev);
        if (!mScolllable) {
            return false;
        }
        return super.onTouchEvent(ev);

    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (!mScolllable) {
            return false;
        }
        return super.onInterceptTouchEvent(ev);
    }

    public interface ScrollPageChangedListener {


        void onFling(Direction direction);

    }

    public void setOnScrollPageChangedListener(ScrollPageChangedListener listener) {
        mScrollPageChangedListener = listener;
    }

    public boolean isCanScrollable() {
        return mScolllable;
    }

    public void setCanScrollable(boolean scrollable) {
        this.mScolllable = scrollable;
    }
}
