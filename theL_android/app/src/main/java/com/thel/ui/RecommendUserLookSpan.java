package com.thel.ui;

import android.content.Intent;
import androidx.core.content.ContextCompat;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.constants.TheLConstants;
import com.thel.modules.main.home.moments.RecommendMomentInfoActivity;

public class RecommendUserLookSpan extends ClickableSpan {

    private final int count;
    private String momentId;

    public RecommendUserLookSpan(String momentId, int count) {
        super();
        this.momentId = momentId;
        this.count = count;
    }

    @Override
    public void updateDrawState(TextPaint ds) {
        ds.setColor(ContextCompat.getColor(TheLApp.getContext(), R.color.text_color_green));
        ds.setUnderlineText(false); // 去掉下划线
    }

    @Override
    public void onClick(View widget) {
        Intent intent = new Intent(TheLApp.getContext().getApplicationContext(), RecommendMomentInfoActivity.class);
        intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, momentId);
        intent.putExtra(TheLConstants.BUNDLE_KEY_RECOMMEND_COUNT, count);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        TheLApp.getContext().getApplicationContext().startActivity(intent);
    }
}
