package com.thel.ui.widget.moments;

import android.content.Context;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.thel.R;

/**
 * Created by liuyun on 2017/9/22.
 */

public class MomentMomentsLayout extends LinearLayout {
    public MomentMomentsLayout(Context context) {
        super(context);
    }

    public MomentMomentsLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public MomentMomentsLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void initView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.layout_moment_moments, this, true);
    }

}
