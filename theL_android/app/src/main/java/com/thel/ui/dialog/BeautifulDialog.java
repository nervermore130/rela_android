package com.thel.ui.dialog;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.SeekBar;

import com.thel.R;
import com.thel.base.BaseDialogFragment;
import com.thel.constants.TheLConstants;
import com.thel.tusdk.plain.CameraConfig;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by chad
 * Time 18/7/4
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */
public class BeautifulDialog extends BaseDialogFragment implements SeekBar.OnSeekBarChangeListener {

    @BindView(R.id.white_seekBar)
    SeekBar white_seekBar;

    @BindView(R.id.skin_grinding_seekBar)
    SeekBar skin_grinding_seekBar;

    @BindView(R.id.thin_face_seekBar)
    SeekBar thin_face_seekBar;

    @BindView(R.id.big_eyes_seekBar)
    SeekBar big_eyes_seekBar;

    private BeautyListener beautyListener;

    private CameraConfig cameraConfigBean = new CameraConfig();

    private boolean bgTrans;

    public void setBeautyListener(BeautyListener beautyListener) {
        this.beautyListener = beautyListener;
    }

    public static BeautifulDialog newInstance(CameraConfig cameraConfigBean, boolean bgTrans) {
        BeautifulDialog dialog = new BeautifulDialog();
        Bundle bundle = new Bundle();
        bundle.putSerializable(TheLConstants.BUNDLE_KEY_LIVE_ROOM_CAMERA_CONFIG, cameraConfigBean);
        bundle.putBoolean("background", bgTrans);
        dialog.setArguments(bundle);
        return dialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            cameraConfigBean = (CameraConfig) getArguments().getSerializable(TheLConstants.BUNDLE_KEY_LIVE_ROOM_CAMERA_CONFIG);
            bgTrans = getArguments().getBoolean("background");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (bgTrans)
            return inflater.inflate(R.layout.video_setting_bar, container, false);
        else
            return inflater.inflate(R.layout.video_setting_bar1, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        white_seekBar.setOnSeekBarChangeListener(this);
        skin_grinding_seekBar.setOnSeekBarChangeListener(this);
        thin_face_seekBar.setOnSeekBarChangeListener(this);
        big_eyes_seekBar.setOnSeekBarChangeListener(this);
        white_seekBar.setProgress((int) (cameraConfigBean.mWhitening * 100));
        skin_grinding_seekBar.setProgress((int) (cameraConfigBean.mSmoothing * 100));
        thin_face_seekBar.setProgress((int) (cameraConfigBean.mChinSize * 100));
        big_eyes_seekBar.setProgress((int) (cameraConfigBean.mEyeSize * 100));
    }


    @Override
    public void onStart() {
        // TODO Auto-generated method stub
        super.onStart();

        //设置背景透明
        Window window = getDialog().getWindow();
        WindowManager.LayoutParams windowParams = window.getAttributes();
        windowParams.dimAmount = 0.0f;

        window.setAttributes(windowParams);

    }

    @Override
    public void onDestroy() {
        if (beautyListener != null) beautyListener.dialogClose(cameraConfigBean);
        super.onDestroy();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        float progressTemp;
        progressTemp = (float) progress / 100;
        switch (seekBar.getId()) {
            case R.id.white_seekBar:
                cameraConfigBean.mWhitening = progressTemp;
                if (beautyListener != null)
                    beautyListener.adjustBeauty(CameraConfig.KEY_WHITENING, progressTemp);
                break;
            case R.id.skin_grinding_seekBar:
                cameraConfigBean.mSmoothing = progressTemp;
                if (beautyListener != null)
                    beautyListener.adjustBeauty(CameraConfig.KEY_SMOOTHING, progressTemp);
                break;
            case R.id.thin_face_seekBar:
                cameraConfigBean.mChinSize = progressTemp;
                if (beautyListener != null)
                    beautyListener.adjustBeauty(CameraConfig.KEY_CHIN_SIZE, progressTemp);
                break;
            case R.id.big_eyes_seekBar:
                cameraConfigBean.mEyeSize = progressTemp;
                if (beautyListener != null)
                    beautyListener.adjustBeauty(CameraConfig.KEY_EYE_SIZE, progressTemp);
                break;
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    public interface BeautyListener {

        void adjustBeauty(String key, float progress);

        void dialogClose(CameraConfig cameraConfig);
    }
}
