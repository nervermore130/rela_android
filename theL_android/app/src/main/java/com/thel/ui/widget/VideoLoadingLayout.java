package com.thel.ui.widget;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.thel.R;

/**
 * @author liuyun
 * @date 2017/11/15
 */

public class VideoLoadingLayout extends RelativeLayout {

    private ImageView btn_bg;

    private ObjectAnimator icon_anim;


    public VideoLoadingLayout(Context context) {
        super(context);
        init();
    }

    public VideoLoadingLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public VideoLoadingLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.layout_video_loading, this, true);

        btn_bg = view.findViewById(R.id.btn_bg);

        setVisibility(View.GONE);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        startAnimator();

    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        stopAnimator();
    }

    private void startAnimator() {

//        setVisibility(VISIBLE);

        if (btn_bg != null) {
            icon_anim = ObjectAnimator.ofFloat(btn_bg, "rotation", 0.0F, 359.0F);
            icon_anim.setRepeatCount(ValueAnimator.INFINITE);
            icon_anim.setDuration(1000);
            icon_anim.setInterpolator(new LinearInterpolator());
            icon_anim.start();
        }
    }

    private void stopAnimator() {

        setVisibility(GONE);

        if (icon_anim != null) {
            icon_anim.cancel();
            icon_anim = null;
        }

    }

}
