package com.thel.ui.widget.moments;

import android.content.Context;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.thel.R;

/**
 * Created by liuyun on 2017/9/22.
 */

public class MomentUserCardLayout extends LinearLayout {
    public MomentUserCardLayout(Context context) {
        super(context);
        initView();
    }

    public MomentUserCardLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public MomentUserCardLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        LayoutInflater.from(getContext()).inflate(R.layout.layout_moment_user_card, this, true);
    }

}
