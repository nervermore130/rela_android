package com.thel.ui.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;

import com.thel.utils.Utils;

/*
 *用来显示不规则图片，
 * 正方形
 * 上面两个是圆角，下面两个是直角
 * */
public class OvalSquareImageView extends androidx.appcompat.widget.AppCompatImageView {

    private float radius = Utils.dip2px(getContext(), 10);

    private RectF mRectF = new RectF();

    private Path mPath = new Path();

    /*圆角的半径，依次为左上角xy半径，右上角，右下角，左下角*/
    private float[] rids = {radius, radius, radius, radius, 0.0f, 0.0f, 0.0f, 0.0f};

    public OvalSquareImageView(Context context) {
        this(context, null);
    }


    public OvalSquareImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }


    public OvalSquareImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(getMeasuredWidth(), getMeasuredWidth());
    }

    /**
     * 画图
     * by Hankkin at:2015-08-30 21:15:53
     *
     * @param canvas
     */
    protected void onDraw(Canvas canvas) {
        int w = this.getWidth();
        int h = this.getHeight();
        /*向路径中添加圆角矩形。radii数组定义圆角矩形的四个圆角的x,y半径。radii长度必须为8*/
        mRectF.set(0, 0, w, h);
        mPath.addRoundRect(mRectF, rids, Path.Direction.CW);
        canvas.clipPath(mPath);
        super.onDraw(canvas);
    }
}
