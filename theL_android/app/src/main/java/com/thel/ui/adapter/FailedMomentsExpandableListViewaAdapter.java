package com.thel.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;

import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.ReleaseMomentListBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.constants.TheLConstants;
import com.thel.db.MomentsDataBaseAdapter;
import com.thel.service.ReleaseMomentsService;
import com.thel.utils.DialogUtil;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.ViewUtils;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Setsail on 2015/3/16.
 */
public class FailedMomentsExpandableListViewaAdapter extends BaseExpandableListAdapter {
    private List<MomentsBean> groupArray = new ArrayList<>();
    private List<MomentsBean> childArray = new ArrayList<>();

    private List<MomentsBean> sendFailedMoments;

    private Context ctx;

    public FailedMomentsExpandableListViewaAdapter(Context context) {
        ctx = context;
        initData();
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return childArray.get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        return getGenericView(convertView, childArray.get(childPosition), 2, groupPosition);
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return childArray.size();
    }

    /* ----------------------------Group */
    @Override
    public Object getGroup(int groupPosition) {
        return getGroup(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return groupArray.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        return getGenericView(convertView, groupArray.get(groupPosition), 1, groupPosition);
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    private View getGenericView(View convertView, final MomentsBean moment, final int type, final int position) {
        if (convertView == null) {
            convertView = RelativeLayout.inflate(TheLApp.getContext(), R.layout.send_failed_moments_listitem, null);
        }
        TextView preview_txt = convertView.findViewById(R.id.preview_txt);
        final TextView text = convertView.findViewById(R.id.text);
        SimpleDraweeView preview_img = convertView.findViewById(R.id.preview_img);
        ImageView img_play = convertView.findViewById(R.id.img_play);
        img_play.setVisibility(View.GONE);
        final ImageView toggle = convertView.findViewById(R.id.toggle);
        final ImageView resend = convertView.findViewById(R.id.resend);
        resend.setVisibility(View.VISIBLE);
        final ImageView delete = convertView.findViewById(R.id.delete);
        delete.setVisibility(View.VISIBLE);
        if (MomentsBean.MOMENT_TYPE_IMAGE.equals(moment.momentsType) || MomentsBean.MOMENT_TYPE_TEXT_IMAGE.equals(moment.momentsType) || MomentsBean.MOMENT_TYPE_VIDEO.equals(moment.momentsType)) {//图片日志、视频日志
            if (!TextUtils.isEmpty(moment.imageUrl)) {
                String[] imgs = moment.imageUrl.split(",");
                if (imgs.length > 0) {
                    preview_img.setVisibility(View.VISIBLE);
                    preview_txt.setVisibility(View.GONE);
                    preview_img.setImageURI(Uri.parse(TheLConstants.FILE_PIC_URL + imgs[0]));
                }
            }
            if (MomentsBean.MOMENT_TYPE_VIDEO.equals(moment.momentsType)) {
                img_play.setVisibility(View.VISIBLE);
                img_play.setImageResource(R.mipmap.btn_play_video);
            }
        } else if (MomentsBean.MOMENT_TYPE_VOICE.equals(moment.momentsType) || MomentsBean.MOMENT_TYPE_TEXT_VOICE.equals(moment.momentsType)) {//音乐日志
            preview_img.setVisibility(View.VISIBLE);
            preview_txt.setVisibility(View.GONE);
            preview_img.setImageURI(Uri.parse(moment.albumLogo100));
            img_play.setVisibility(View.VISIBLE);
            img_play.setImageResource(R.mipmap.btn_feed_play_big);
        } else {// 文字日志或话题日志
            preview_txt.setVisibility(View.VISIBLE);
            preview_img.setVisibility(View.GONE);
            preview_txt.setText(moment.momentsText);
        }

        if (type == 1) {// 第一个
            if (groupArray.size() + childArray.size() > 1) {
                text.setText(TheLApp.getContext().getString(R.string.post_moment_failed) + " (" + (groupArray.size() + childArray.size()) + TheLApp.getContext().getString(R.string.userinfo_activity_moments_num_even) + ")");
                toggle.setVisibility(View.VISIBLE);
            } else {
                text.setText(TheLApp.getContext().getString(R.string.post_moment_failed));
                toggle.setVisibility(View.GONE);
            }
            toggle.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {

                }
            });
        } else {// 其他
            text.setText(TheLApp.getContext().getString(R.string.post_moment_failed));
            toggle.setVisibility(View.GONE);
        }

        resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                L.d(" FailedMomentsExpandableListViewaAdapter ", "-----------onClick----------");

                L.d(" FailedMomentsExpandableListViewaAdapter ", " logType : " + type);

                ViewUtils.preventViewMultipleClick(view, 1000);
                Intent intent = new Intent(TheLApp.getContext(), ReleaseMomentsService.class);
                // 要发的日志标识，用发布时间作为标识
                intent.putExtra(MomentsDataBaseAdapter.F_RELEASE_TIME, moment.releaseTime);
                TheLApp.getContext().startService(intent);

                deleteMomentsBean(type, position, false);


            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewUtils.preventViewMultipleClick(view, 1000);
                DialogUtil.showConfirmDialog((Activity) ctx, "", TheLApp.getContext().getString(R.string.post_moment_failed_delete), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
//                        initData(false);
                        // 发送发布日志成功的广播
                        Intent intent = new Intent();
                        intent.setAction(TheLConstants.BROADCAST_RELEASE_MOMENT_DELETE_FAILED);
                        intent.putExtra(TheLConstants.BUNDLE_KEY_RELEASE_TIME, moment.releaseTime);
                        TheLApp.getContext().sendBroadcast(intent);

                        if (groupArray.isEmpty()) {// 如果全删完了，刷新tab界面
                            // 发刷新tab界面的广播
                            Intent intent1 = new Intent();
                            intent1.setAction(TheLConstants.BROADCAST_FAILED_MOMENTS_CHECK_ACTION);
                            TheLApp.getContext().sendBroadcast(intent1);
                        }

                        deleteMomentsBean(type, position, true);


                    }
                });
            }
        });

        return convertView;
    }

    private void deleteMomentsBean(int type, int position, boolean isDelete) {
        switch (type) {
            case 1:
                if (groupArray.size() > 0) {
                    groupArray.remove(position);
                    if (childArray.size() > 0) {
                        groupArray.add(childArray.get(0));
                        childArray.remove(0);
                    }

                    if (isDelete) {
                        saveReleaseFailedMoment();
                    }

                    FailedMomentsExpandableListViewaAdapter.this.notifyDataSetChanged();
                }
                break;
            case 2:
                if (groupArray.size() > 0) {

                    if (childArray.size() > position) {
                        childArray.remove(position);
                    }

                    if (isDelete) {
                        saveReleaseFailedMoment();
                    }

                    FailedMomentsExpandableListViewaAdapter.this.notifyDataSetChanged();
                }
                break;
        }
    }


    public void initData() {

        if (getReleaseFailedMoment() != null) {
            sendFailedMoments = getReleaseFailedMoment().list;

            groupArray.clear();
            childArray.clear();

            if (sendFailedMoments != null && !sendFailedMoments.isEmpty()) {
                groupArray.add(sendFailedMoments.get(0));
                for (int i = 1; i < sendFailedMoments.size(); i++) {
                    childArray.add(sendFailedMoments.get(i));
                }
            }
        }

    }


    private ReleaseMomentListBean getReleaseFailedMoment() {
        String content = ShareFileUtils.getString(TheLConstants.RELEASE_CONTENT_KEY, null);

        if (content != null) {
            return GsonUtils.getObject(content, ReleaseMomentListBean.class);
        }
        return null;

    }

    private void saveReleaseFailedMoment() {
        ReleaseMomentListBean releaseMomentListBean = new ReleaseMomentListBean();

        List<MomentsBean> list = new ArrayList<>();

        list.addAll(groupArray);

        list.addAll(childArray);

        releaseMomentListBean.list = list;

        String json = GsonUtils.createJsonString(releaseMomentListBean);

        ShareFileUtils.setString(TheLConstants.RELEASE_CONTENT_KEY, json);
    }

}
