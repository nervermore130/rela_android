package com.thel.ui.widget;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.moments.LiveUserMomentBean;
import com.thel.bean.moments.MomentParentBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.constants.TheLConstants;
import com.thel.growingio.GrowingIoConstant;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.live.surface.watch.LiveWatchActivity;
import com.thel.modules.live.surface.watch.horizontal.LiveWatchHorizontalActivity;
import com.thel.utils.BusinessUtils;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.L;
import com.thel.utils.SimpleDraweeViewUtils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by test1 on 2017/7/13.
 */

public class RecommendLiveUserView extends LinearLayout {

    private static final String TAG = "RecommendLiveUserView";

    @BindView(R.id.live_img_avater_live_user)
    ImageView live_img_avater;

    @BindView(R.id.img_live_live_user)
    SimpleDraweeView img_live_state;

    @BindView(R.id.txt_livestatus_live_user)
    TextView txt_livestatus;

    @BindView(R.id.lin_live_status_live_user)
    LinearLayout lin_live_status_live_user;

    @BindView(R.id.txt_livestatus_parent)
    TextView txt_livestatus_parent;

    @BindView(R.id.live_user_count_ll)
    LinearLayout live_user_count_ll;

    @BindView(R.id.live_user_count_tv)
    TextView live_user_count_tv;

    private String vedio = "icon_cameralive.gif";

    private String voice = "icon_miclive.gif";

    private String avatar;

    private String liveId;

    private String userId;

    private int liveStatus;

    private String momentsType;

    private String userName;

    private int isLandscape;

    private LiveUserMomentBean liveUserMoment;

    public RecommendLiveUserView(Context context) {
        this(context, null);
    }

    public RecommendLiveUserView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RecommendLiveUserView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
        setListener();
    }

    private void setListener() {
        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                if (BusinessUtils.canIntoLiveRoom(TheLApp.getContext(), userId)) {

                    MobclickAgent.onEvent(v.getContext(), "enter_live_room_from_moment");

                    Intent intent;
                    if (isLandscape == 1) {
                        intent = new Intent(v.getContext(), LiveWatchHorizontalActivity.class);
                    } else {
                        intent = new Intent(v.getContext(), LiveWatchActivity.class);
                    }
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(TheLConstants.BUNDLE_KEY_ID, liveId);
                    intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, userId);
                    intent.putExtra(LiveWatchActivity.FROM_PAGE, LiveWatchActivity.FROM_PAGE_MOMENT);
                    v.getContext().startActivity(intent);

                    GrowingIOUtil.postWatchLiveEntry(GrowingIoConstant.G_LiveEntry_Feed);

                }
            }
        });
    }

    private void init() {
        View view = inflate(getContext(), R.layout.layout_moment_live_user, this);
        ButterKnife.bind(this, view);

    }

    public RecommendLiveUserView initView(MomentsBean momentsBean) {
        initData(momentsBean);
        setVisibility(VISIBLE);
        setViews();
        return this;
    }

    public RecommendLiveUserView initView(MomentParentBean momentsBean) {
        initData(momentsBean);
        setVisibility(VISIBLE);
        setViews();
        return this;
    }

    private void initData(MomentsBean momentsBean) {

        userId = String.valueOf(momentsBean.userId);
        liveId = momentsBean.liveId;
        avatar = momentsBean.imageUrl;
        liveStatus = momentsBean.liveStatus;
        momentsType = momentsBean.momentsType;
        liveUserMoment = momentsBean.liveUserMoment;
        userName = momentsBean.userName;

        if (momentsBean.isLandscape == 1 || (momentsBean.liveUserMoment != null && momentsBean.liveUserMoment.isLandscape == 1)) {
            isLandscape = 1;
        } else {
            isLandscape = 0;
        }
    }

    private void initData(MomentParentBean momentsBean) {

        userId = String.valueOf(momentsBean.userId);
        liveId = momentsBean.liveId;
        avatar = momentsBean.imageUrl;
        liveStatus = momentsBean.liveStatus;
        momentsType = momentsBean.momentsType;
        liveUserMoment = momentsBean.liveUserMoment;
        userName = momentsBean.userName;
        isLandscape = momentsBean.isLandscape;
        if (momentsBean.isLandscape == 1 || (momentsBean.liveUserMoment != null && momentsBean.liveUserMoment.isLandscape == 1)) {
            isLandscape = 1;
        } else {
            isLandscape = 0;
        }
    }

    private void setViews() {

        ImageLoaderManager.imageLoader(live_img_avater, avatar);

        L.d(TAG, " liveStatus : " + liveStatus);

        if (liveUserMoment != null && liveUserMoment.userId != null) {
            userId = liveUserMoment.userId;
        }

        if (liveStatus == -1 || (liveUserMoment != null && liveUserMoment.liveStatus == -1)) {

            lin_live_status_live_user.setVisibility(GONE);

            if (MomentsBean.MOMENT_TYPE_LIVE.equals(momentsType)) {
                img_live_state.setImageResource(R.mipmap.icon_cameralive_close);
                txt_livestatus_parent.setText(TheLApp.getContext().getString(R.string.live_video));
            } else if (MomentsBean.MOMENT_TYPE_VOICE_LIVE.equals(momentsType)) {
                img_live_state.setImageResource(R.mipmap.icon_miclive_close);
                txt_livestatus_parent.setText(TheLApp.getContext().getString(R.string.live_voice));
            }
//            txt_livestatus_parent.setText(TheLApp.getContext().getString(R.string.live_ended));

        } else {

//            txt_livestatus_parent.setText(TheLApp.getContext().getString(R.string.is_on_live));

            lin_live_status_live_user.setVisibility(VISIBLE);

            live_user_count_ll.setVisibility(GONE);

            L.d(TAG, " momentsType : " + momentsType);

            if (MomentsBean.MOMENT_TYPE_LIVE.equals(momentsType)) {
                final String path = TheLConstants.ASSET_PIC_URL + "/" + vedio;
                SimpleDraweeViewUtils.setImageUri(img_live_state, Uri.parse(path));
                txt_livestatus.setText(TheLApp.getContext().getString(R.string.now_watching_live, liveStatus));

            } else if (MomentsBean.MOMENT_TYPE_VOICE_LIVE.equals(momentsType)) {
                final String path = TheLConstants.ASSET_PIC_URL + "/" + voice;
                SimpleDraweeViewUtils.setImageUri(img_live_state, Uri.parse(path));
                txt_livestatus.setText(TheLApp.getContext().getString(R.string.now_watching_live, liveStatus));

            } else if (MomentsBean.MOMENT_TYPE_LIVE_USER.equals(momentsType)) {

                L.d(TAG, " liveUserMoment.audioType : " + liveUserMoment.audioType);

                live_user_count_ll.setVisibility(VISIBLE);

                if (liveUserMoment.audioType == 0) {
                    final String path = TheLConstants.ASSET_PIC_URL + "/" + vedio;
                    SimpleDraweeViewUtils.setImageUri(img_live_state, Uri.parse(path));

                } else {
                    final String path = TheLConstants.ASSET_PIC_URL + "/" + voice;
                    SimpleDraweeViewUtils.setImageUri(img_live_state, Uri.parse(path));
                }

                live_user_count_tv.setText(TheLApp.getContext().getString(R.string.now_watching_live, liveUserMoment.liveStatus));

                String content = liveUserMoment.nickname + " " + TheLApp.getContext().getString(R.string.on_line) + ": " + liveUserMoment.description;

                txt_livestatus.setText(content);

            }

        }
    }
}
