package com.thel.ui.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Layout;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.comment.CommentBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.modules.main.home.moments.comment.MomentCommentActivity;
import com.thel.ui.widget.ExpandTextView;
import com.thel.ui.widget.LikeButtonView;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;
import com.thel.utils.EmojiUtils;
import com.thel.utils.MomentUtils;
import com.thel.utils.ShareFileUtils;

import java.util.List;

import static android.view.View.GONE;
import static com.thel.constants.TheLConstantsExt.MOMENT_TEXT_CONTENT_MAX_LINE;

/**
 * Created by waiarl on 2017/7/15.
 */
public class RecommendMomentAdapter extends BaseRecyclerViewAdapter<MomentsBean> {
    private View.OnClickListener mOnClickListener;

    public RecommendMomentAdapter(List<MomentsBean> list) {
        super(R.layout.item_recommend_moment, list);

    }

    public RecommendMomentAdapter(List<MomentsBean> list, View.OnClickListener onClickListener) {
        this(list);
        mOnClickListener = onClickListener;
    }

    public void setContext(Context context) {
        mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder holdView, final MomentsBean commentBean) {

        ((LikeButtonView) (holdView.getView(R.id.moment_opts_like))).setLikeAnimImage(holdView.getView(R.id.img_like_anim));

        // 头像和昵称
        holdView.setImageUrl(R.id.img_thumb, commentBean.avatar, TheLConstants.AVATAR_MIDDLE_SIZE, TheLConstants.AVATAR_MIDDLE_SIZE);
        holdView.setTag(R.id.img_thumb, commentBean.userId);
        holdView.setOnClickListener(R.id.img_thumb, mOnClickListener);
        //  holdView.setText(R.id.moment_user_name, commentBean.nickname);
        MomentUtils.setNicknameWithMomentType(holdView.getView(R.id.moment_user_name), commentBean);//设置姓名以及后缀
        // 发布时间

        holdView.setText(R.id.moment_release_time, MomentUtils.getReleaseTimeShow(commentBean.momentsTime));
        //关注
        if (CommentBean.FOLLOW_TYPE_UNFOLLOWED == commentBean.followerStatus && !ShareFileUtils.getString(ShareFileUtils.ID, "").equals(commentBean.userId + "")) {
            holdView.setVisibility(R.id.txt_follow, View.VISIBLE);
        } else {
            holdView.setVisibility(R.id.txt_follow, GONE);
        }
        holdView.setTag(R.id.txt_follow, R.id.comment_tag, holdView.getAdapterPosition() - getHeaderLayoutCount());
        holdView.setOnClickListener(R.id.txt_follow, mOnClickListener);

        if (!TextUtils.isEmpty(commentBean.momentsText)) {
            final ExpandTextView txt_content = holdView.getView(R.id.moment_content_text);
            txt_content.setVisibility(View.VISIBLE);
            // comment文字内容
            txt_content.setText(EmojiUtils.getInstace(EmojiUtils.DEFAULT_SIZE).getExpressionString(TheLApp.getContext(), commentBean.momentsText));
            /** 2.19.2显示全文 start **/
            final TextView txt_whole = holdView.getView(R.id.txt_whole);
            if (commentBean.contentStatus == MomentsBean.CONTENT_STATUS_HANG) {//状态为收起状态
                txt_whole.setText(TheLApp.getContext().getString(R.string.whole_content));//显示为全文
                txt_content.setMaxLines(MOMENT_TEXT_CONTENT_MAX_LINE);
            } else {
                txt_whole.setText(TheLApp.getContext().getString(R.string.pack_up));//显示为收起
                txt_content.setMaxLines(Integer.MAX_VALUE);
            }
            txt_content.setTextChangedListener(new ExpandTextView.TextChangedListener() {
                @Override
                public void textChanged(TextView textView) {
                    if (View.VISIBLE == txt_content.getVisibility()) {
                        final Layout layout = txt_content.getLayout();
                        final int lineCount = layout.getLineCount();
                        commentBean.textLines = lineCount;
                        int eCount = 0;
                        if (lineCount >= MOMENT_TEXT_CONTENT_MAX_LINE) {
                            eCount = layout.getEllipsisCount(MOMENT_TEXT_CONTENT_MAX_LINE - 1);
                        }
                        if (eCount > 0 || lineCount > MOMENT_TEXT_CONTENT_MAX_LINE) {
                            txt_whole.setVisibility(View.VISIBLE);
                        } else {
                            txt_whole.setVisibility(GONE);
                        }
                    } else {
                        txt_whole.setVisibility(GONE);
                    }
                }
            });
            txt_whole.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (commentBean.textLines > MomentsBean.MAX_EXPAND_LINES) {
//                        Intent intent = new Intent();
//                        intent.setClass(TheLApp.getContext(), MomentCommentActivity.class);
//                        intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, commentBean.momentsId);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        TheLApp.getContext().startActivity(intent);
                        FlutterRouterConfig.Companion.gotoMomentDetails(commentBean.momentsId);
                    } else {
                        if (commentBean.contentStatus == MomentsBean.CONTENT_STATUS_HANG) {//当前状态是收起，显示全文
                            commentBean.contentStatus = MomentsBean.CONTENT_STATUS_WHOLE;
                            txt_content.setMaxLines(Integer.MAX_VALUE);
                            txt_whole.setText(TheLApp.getContext().getString(R.string.pack_up));//显示为收起
                        } else {//否者，收起
                            commentBean.contentStatus = MomentsBean.CONTENT_STATUS_HANG;
                            txt_content.setMaxLines(MOMENT_TEXT_CONTENT_MAX_LINE);
                            txt_whole.setText(TheLApp.getContext().getString(R.string.whole_content));//显示为全文
                        }
                    }
                }
            });
            /** 2.19.2显示全文 end**/
        } else {
            holdView.setVisibility(R.id.moment_content_text, GONE);
            holdView.setVisibility(R.id.txt_whole, GONE);
        }

        updateLikeStatus(commentBean, holdView);

        updateCommentNum(commentBean, holdView);

        // 去点赞
        holdView.setOnClickListener(R.id.moment_opts_like, mOnClickListener);
        holdView.setTag(R.id.moment_opts_like, R.id.moment_id_tag, commentBean.momentsId);
        // 日志操作
        holdView.setOnClickListener(R.id.moment_opts_more, mOnClickListener);
        holdView.setTag(R.id.moment_opts_more, R.id.comment_tag, commentBean);
        // 日志评论
        holdView.setOnClickListener(R.id.moment_opts_comment, mOnClickListener);
        holdView.setTag(R.id.moment_opts_comment, R.id.moment_id_tag, commentBean.momentsId);

        LinearLayout lin_latest_comments = holdView.getView(R.id.lin_latest_comments);

        MomentUtils.addComments(lin_latest_comments.getContext(), lin_latest_comments, commentBean.commentList, commentBean.momentsId, commentBean.momentsType);

    }


    public void likeMomentOrNot(RecyclerView recyclerView, int position) {
        try {
            final MomentsBean commentBean = getItem(position);
            // 注意这里需要加上header的count
            final View item = recyclerView.getLayoutManager().findViewByPosition(position + getHeaderLayoutCount());
            final BaseViewHolder helper = (BaseViewHolder) recyclerView.getChildViewHolder(item);
            updateLikeStatus(commentBean, helper);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 刷新点赞状态
     *
     * @param commentBean
     * @param helper
     */
    private void updateLikeStatus(MomentsBean commentBean, BaseViewHolder helper) {
        // 是否已点过赞
        if (commentBean.winkFlag != MomentsBean.HAS_NOT_WINKED) {
            ((LikeButtonView) (helper.getView(R.id.moment_opts_like))).setChecked(true);
        } else {
            ((LikeButtonView) (helper.getView(R.id.moment_opts_like))).setChecked(false);
        }
        helper.setTag(R.id.moment_opts_like, R.id.moment_like_tag, commentBean.winkFlag);
        // 点赞数
        if (commentBean.winkNum <= 0) {
            helper.setVisibility(R.id.moment_opts_emoji_txt, GONE);
        } else {
            helper.setVisibility(R.id.moment_opts_emoji_txt, View.VISIBLE);
            helper.setTag(R.id.moment_opts_emoji_txt, R.id.moment_id_tag, commentBean.momentsId);
            helper.setText(R.id.moment_opts_emoji_txt, TheLApp.getContext().getString(commentBean.winkNum == 1 ? R.string.like_count_odd : R.string.like_count_even, commentBean.winkNum));
        }
    }

    private void updateCommentNum(MomentsBean commentBean, BaseViewHolder helper) {
        helper.setText(R.id.moment_opts_comment_txt, commentBean.commentNum + "");
        // 评论数
        if (commentBean.commentNum <= 0) {
            helper.setVisibility(R.id.moment_opts_comment_txt, GONE);
        } else {
            helper.setVisibility(R.id.moment_opts_comment_txt, View.VISIBLE);
            helper.setText(R.id.moment_opts_comment_txt, TheLApp.getContext().getString(commentBean.commentNum <= 1 ? R.string.comment_count_odd : R.string.comment_count_even, commentBean.commentNum));
        }
    }


}
