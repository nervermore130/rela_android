package com.thel.ui.widget.emoji;

/**
 * RecyclerView的点击事件
 * Created by setsail on 15/6/25.
 */
public interface OnRecyclerViewListener {

    void onItemClick(int position);

    boolean onItemLongClick(int position);
}
