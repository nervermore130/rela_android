package com.thel.ui.widget.moments;

import android.content.Context;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.thel.R;

/**
 * Created by liuyun on 2017/9/22.
 */

public class MomentThemeLayout extends LinearLayout {
    public MomentThemeLayout(Context context) {
        super(context);
        initView();
    }

    public MomentThemeLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public MomentThemeLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.layout_moment_theme, this, true);
    }
}
