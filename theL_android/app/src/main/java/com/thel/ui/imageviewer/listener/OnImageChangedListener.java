package com.thel.ui.imageviewer.listener;


import com.thel.ui.imageviewer.widget.ScaleImageView;

/**
 * 图片的切换监听事件
 */
public interface OnImageChangedListener {

    void onImageSelected(int position, ScaleImageView view);
}
