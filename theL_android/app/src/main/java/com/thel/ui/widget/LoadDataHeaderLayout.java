package com.thel.ui.widget;

import android.content.Context;
import androidx.annotation.Nullable;
import android.util.AttributeSet;

import com.jcodecraeer.xrecyclerview.ArrowRefreshHeader;

public class LoadDataHeaderLayout extends ArrowRefreshHeader {
    public LoadDataHeaderLayout(Context context) {
        super(context);
    }

    public LoadDataHeaderLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }
}
