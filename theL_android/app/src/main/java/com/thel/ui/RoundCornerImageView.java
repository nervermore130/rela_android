package com.thel.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;

import com.thel.utils.Utils;

/**
 * Created by chad
 * Time 18/9/7
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */
public class RoundCornerImageView extends androidx.appcompat.widget.AppCompatImageView {

    private float radius = Utils.dip2px(getContext(), 12);

    private RectF mRectF = new RectF();

    private Path mPath = new Path();

    /*圆角的半径，依次为左上角xy半径，右上角，右下角，左下角*/
    private float[] rids = {radius, radius, radius, radius, radius, radius, radius, radius};

    public RoundCornerImageView(Context context) {
        this(context, null);
    }


    public RoundCornerImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }


    public RoundCornerImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * 画图
     * by Hankkin at:2015-08-30 21:15:53
     *
     * @param canvas
     */
    protected void onDraw(Canvas canvas) {
        int w = this.getWidth();
        int h = this.getHeight();
        /*向路径中添加圆角矩形。radii数组定义圆角矩形的四个圆角的x,y半径。radii长度必须为8*/
        mRectF.set(0, 0, w, h);
        mPath.addRoundRect(mRectF, rids, Path.Direction.CW);
        canvas.clipPath(mPath);
        super.onDraw(canvas);
    }
}
