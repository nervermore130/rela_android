package com.thel.ui;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.utils.L;
import com.thel.utils.SizeUtils;

public class ChatGuideLayout extends RelativeLayout {

    private static final String TAG = "ChatGuideLayout";

    private TextView content_tv;

    public ChatGuideLayout(Context context) {
        super(context);

        init(context);

    }

    public ChatGuideLayout(Context context, AttributeSet attrs) {
        super(context, attrs);

        init(context);

    }

    public ChatGuideLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init(context);

    }

    @Override protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(getMeasuredWidth(), getMeasuredHeight() + SizeUtils.dip2px(getContext(), 10));
    }

    private void init(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_chat_guide, this, true);
        content_tv = view.findViewById(R.id.content_tv);
    }

    private ValueAnimator valueAnimator;

    private void startAnimator(final View view) {

        int startY = 0;

        int endY = SizeUtils.dip2px(getContext(), 10);

        valueAnimator = ValueAnimator.ofInt(startY, endY, startY);

        valueAnimator.setDuration(1000);

        valueAnimator.setInterpolator(new LinearInterpolator());

        valueAnimator.setRepeatCount(ValueAnimator.INFINITE);

        valueAnimator.setRepeatMode(ValueAnimator.RESTART);

        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override public void onAnimationUpdate(ValueAnimator animation) {
                int offset = (int) animation.getAnimatedValue();
                view.setY(offset);
            }
        });

        valueAnimator.addListener(new Animator.AnimatorListener() {
            @Override public void onAnimationStart(Animator animation) {
                if (view != null) {
                    view.setLayerType(LAYER_TYPE_HARDWARE, null);
                }
            }

            @Override public void onAnimationEnd(Animator animation) {
                if (view != null) {
                    view.setLayerType(LAYER_TYPE_NONE, null);
                }

            }

            @Override public void onAnimationCancel(Animator animation) {

            }

            @Override public void onAnimationRepeat(Animator animation) {

            }
        });

        valueAnimator.start();

    }

    private void stopAnimator() {
        if (valueAnimator != null) {
            valueAnimator.cancel();
            valueAnimator = null;
        }
    }

    public void show() {

        ChatGuideLayout.this.setVisibility(View.VISIBLE);

        startAnimator(this);

        postDelayed(new Runnable() {
            @Override public void run() {
                stopAnimator();
                ChatGuideLayout.this.setVisibility(View.GONE);
            }
        }, 5000);

    }


    public void show(String content) {
        show();
        if (content_tv != null) {
            content_tv.setText(content);
        }
    }

    @Override protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        L.d(TAG, " onDetachedFromWindow ");

        stopAnimator();
    }

}
