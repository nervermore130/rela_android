package com.thel.ui.adapter;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;

import java.util.List;

/**
 * 分享页面列表的adaper
 *
 * @author Setsail
 */
public class ShareGridAdapter extends BaseAdapter {

    private List<Integer> images;

    private List<String> texts;

    private LayoutInflater mInflater;

    private DisplayMetrics dm = TheLApp.getContext().getApplicationContext().getResources().getDisplayMetrics();
    private int width = dm.widthPixels;

    private GridView mGridview;

    public ShareGridAdapter(List<Integer> images, List<String> texts, GridView mGridview) {
        this.images = images;
        this.texts = texts;
        this.mGridview = mGridview;

        mInflater = (LayoutInflater) TheLApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public Object getItem(int position) {
        return "";
    }

    @Override
    public long getItemId(int arg0) {
        return 0;
    }

    private HoldView holdView = null;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        holdView = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_share_grid, parent, false);
            holdView = new HoldView();
            holdView.image = convertView.findViewById(R.id.image);
            holdView.text = convertView.findViewById(R.id.text);
            convertView.setTag(holdView); // 把holdview缓存下来

        } else {
            holdView = (HoldView) convertView.getTag();
        }


        holdView.image.setImageResource(images.get(position));
        holdView.text.setText(texts.get(position));

        return convertView;
    }

    class HoldView {
        ImageView image;
        TextView text;
    }
}
