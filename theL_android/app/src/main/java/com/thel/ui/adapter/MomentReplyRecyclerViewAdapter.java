package com.thel.ui.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.comment.CommentBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.modules.main.home.moments.comment.MomentCommentReplyActivity;
import com.thel.modules.main.messages.utils.MessagesUtils;
import com.thel.ui.widget.LatestCommentView;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;
import com.thel.utils.EmojiUtils;
import com.thel.utils.L;
import com.thel.utils.MomentUtils;
import com.thel.utils.SizeUtils;

import java.util.ArrayList;
import java.util.List;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;


/**
 * 日志评论列表的adapter，普通日志
 */
public class MomentReplyRecyclerViewAdapter extends BaseRecyclerViewAdapter<CommentBean> {

    private final int page_from;
    private OnClickListener mOnClickListener;
    private Context mContext;
    public static final int PAGE_MOMENT_COMMENT = 0;
    public static final int PAGE_MOMENT_COMMENT_REPLY = 1;

    public MomentReplyRecyclerViewAdapter(Context ctx, ArrayList<CommentBean> commentlist, OnClickListener onClickListener, int page_from) {
        super(R.layout.moment_comment_recyclerview_item, commentlist);
        mContext = ctx;
        mOnClickListener = onClickListener;
        this.page_from = page_from;
    }

    public void setContext(Context context) {
        mContext = context;
    }

    @Override
    public void convert(final BaseViewHolder holdView, final CommentBean commentBean) {
        if (holdView.getAdapterPosition() > 0 && mContext instanceof MomentCommentReplyActivity)
            holdView.setBackgroundColor(R.id.main, ContextCompat.getColor(mContext, R.color.light_gray1));
        else {
            holdView.setBackgroundColor(R.id.main, ContextCompat.getColor(mContext, R.color.white));
        }
        if (holdView.getAdapterPosition() == 0 && mContext instanceof MomentCommentReplyActivity) {//评论子评论界面，可以查看原日志
            holdView.setVisibility(R.id.txt_look_original_moment, View.VISIBLE);
            holdView.setTag(R.id.txt_look_original_moment, commentBean.commentId);
            holdView.setOnClickListener(R.id.txt_look_original_moment, mOnClickListener);
        } else {
            holdView.setVisibility(R.id.txt_look_original_moment, View.GONE);
        }

        // 头像和昵称
        holdView.setImageUrl(R.id.img_thumb, commentBean.avatar, TheLConstants.AVATAR_MIDDLE_SIZE, TheLConstants.AVATAR_MIDDLE_SIZE);
        holdView.setTag(R.id.img_thumb, commentBean.userId);
        holdView.setOnClickListener(R.id.img_thumb, mOnClickListener);
        holdView.setText(R.id.comment_user_name, commentBean.nickname);
        if (commentBean.level > 0) {
            holdView.setVisibility(R.id.img_vip, VISIBLE);
            switch (commentBean.level) {
                case 1:
                    holdView.setImageResource(R.id.img_vip, R.mipmap.icn_vip_1);
                    break;
                case 2:
                    holdView.setImageResource(R.id.img_vip, R.mipmap.icn_vip_2);

                    break;
                case 3:
                    holdView.setImageResource(R.id.img_vip, R.mipmap.icn_vip_3);

                    break;
                case 4:
                    holdView.setImageResource(R.id.img_vip, R.mipmap.icn_vip_4);

                    break;
            }
        } else {
            holdView.setVisibility(R.id.img_vip, GONE);

        }
        if (CommentBean.SAY_TYPE_TO.equals(commentBean.sayType)) {
            holdView.setVisibility(R.id.comment_say_to, View.VISIBLE);
            holdView.setText(R.id.comment_say_to, "@" + commentBean.toNickname);
        } else {
            holdView.setVisibility(R.id.comment_say_to, View.GONE);
        }
        // 发布时间
        holdView.setText(R.id.comment_release_time, MomentUtils.getReleaseTimeShow(commentBean.commentTime));
        //内容
        if (CommentBean.COMMENT_TYPE_TEXT.equals(commentBean.commentType)) {//如果是文本回复
            holdView.setVisibility(R.id.img_sticker, View.GONE);
            holdView.setVisibility(R.id.comment_content_text, View.VISIBLE);
            holdView.setText(R.id.comment_content_text, EmojiUtils.getInstace(EmojiUtils.DEFAULT_SIZE).getExpressionString(TheLApp.getContext(), commentBean.commentText));
        } else {//表情
            holdView.setVisibility(R.id.img_sticker, View.VISIBLE);
            holdView.setVisibility(R.id.comment_content_text, View.GONE);
            holdView.setImageUrl(R.id.img_sticker, MessagesUtils.getStickerUrl(commentBean.commentText));
        }
        /***4.7.4,如果是日志评论，显示评论的评论次数，如果是日志评论的评论，则不显示次数***/
        if (page_from == PAGE_MOMENT_COMMENT_REPLY) {
            holdView.getView(R.id.lin_latest_comments).setVisibility(View.GONE);
        } else {
            // 最近三条评论

            ((LinearLayout) holdView.getView(R.id.lin_latest_comments)).removeAllViews();

            if (commentBean.commentList != null) {
                List<CommentBean> commentBeans = commentBean.commentList;
                if (commentBeans.size() > 0) {
                    holdView.setVisibility(R.id.lin_latest_comments, View.VISIBLE);
                    holdView.setOnClickListener(R.id.lin_latest_comments, new OnClickListener() {
                        @Override
                        public void onClick(View v) {
//                            Intent intent = new Intent(TheLApp.getContext(), MomentCommentReplyActivity.class);
//                            intent.putExtra(TheLConstants.BUNDLE_KEY_COMMENT_ID, commentBean.commentId);
//                            intent.putExtra(TheLConstants.BUNDLE_KEY_INTENT_FROM, MomentCommentReplyActivity.FROM_MOMENT);
//                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                            TheLApp.getContext().startActivity(intent);
                            FlutterRouterConfig.Companion.gotoCommentReply(commentBean.commentId);
                        }
                    });
                } else
                    holdView.setVisibility(R.id.lin_latest_comments, View.GONE);
                for (int i = 0; i < commentBeans.size(); i++) {
                    final LatestCommentView latestCommentView = new LatestCommentView(mContext);
                    latestCommentView.setGravity(Gravity.LEFT);
                    if (i > 0) {
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        params.topMargin = SizeUtils.dip2px(TheLApp.getContext(), 5);
                        latestCommentView.setLayoutParams(params);
                    }
                    final String text;
                    L.d("MomentReplyRecyclerViewAdapter", "");
                    if (CommentBean.COMMENT_TYPE_STICKER.equals(commentBeans.get(i).commentType))
                        text = TheLApp.getContext().getString(R.string.message_info_sticker);
                    else
                        text = commentBeans.get(i).commentText;
                    latestCommentView.setText(commentBeans.get(i).nickname, text, commentBeans.get(i).userId + "", commentBean.commentId, "commentReply");
                    ((LinearLayout) holdView.getView(R.id.lin_latest_comments)).addView(latestCommentView);
                }
            }

            // 如果超过三条评论，则需要加一个『共xx条回复』的item
            if (commentBean.commentNum > 3) {
                final TextView textView = new TextView(mContext);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.topMargin = SizeUtils.dip2px(TheLApp.getContext(), 5);
                textView.setLayoutParams(params);
                textView.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_green));
                textView.setText(TheLApp.getContext().getString(R.string.total_comment_replies, commentBean.commentNum));
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
                textView.setMaxLines(1);
                textView.setEllipsize(TextUtils.TruncateAt.END);
                ((LinearLayout) holdView.getView(R.id.lin_latest_comments)).addView(textView);
            }

            if (commentBean.commentNum == 0) {
                holdView.getView(R.id.lin_latest_comments).setVisibility(View.GONE);
            } else {
                holdView.getView(R.id.lin_latest_comments).setVisibility(View.VISIBLE);
            }
            LinearLayout linearLayout = holdView.getView(R.id.lin_latest_comments);

            if (linearLayout.getChildCount() <= 0) {
                holdView.getView(R.id.lin_latest_comments).setVisibility(View.GONE);
            } else {
                holdView.getView(R.id.lin_latest_comments).setVisibility(View.VISIBLE);
            }
        }

    }
}
