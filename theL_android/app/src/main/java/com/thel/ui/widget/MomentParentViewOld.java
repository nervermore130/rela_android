package com.thel.ui.widget;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;

import androidx.core.content.ContextCompat;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.ImgShareBean;
import com.thel.bean.MusicInfoBean;
import com.thel.bean.moments.MomentParentBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.recommend.RecommendWebBean;
import com.thel.bean.theme.ThemeClassBean;
import com.thel.bean.video.VideoBean;
import com.thel.constants.BundleConstants;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.imp.follow.FollowStatusChangedImpl;
import com.thel.modules.main.home.moments.comment.MomentCommentActivity;
import com.thel.modules.main.home.moments.theme.ThemeDetailActivity;
import com.thel.modules.main.home.moments.web.WebViewActivity;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.modules.preview_image.UserInfoPhotoActivity;
import com.thel.modules.video.UserVideoListActivity;
import com.thel.ui.MyLinkMovementMethod;
import com.thel.ui.TextSpan;
import com.thel.ui.TopicClickSpan;
import com.thel.utils.AppInit;
import com.thel.utils.ImageUtils;
import com.thel.utils.L;
import com.thel.utils.MomentUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

/**
 * Created by waiarl on 2017/6/29.
 * 日志列表里推荐日志的View(ParentMomentView)
 */

public class MomentParentViewOld extends LinearLayout implements View.OnClickListener {
    private Context mContext;
    private LinearLayout moment_header;
    private SimpleDraweeView img_thumb;
    private TextView moment_user_name;
    private TextView moment_release_time;
    private FollowView txt_follow;
    private LinearLayout moment_mentioned;
    private TextView moment_mentioned_text;
    private LinearLayout lin_participate_theme;
    private SimpleDraweeView img;
    private TextView txt_theme_title;
    private TextView txt_theme_desc;
    private TextView txt_deleted;
    private LinearLayout moment_content;
    private MultiImageViewGroup multi_image_vg;
    private SimpleDraweeView img_cover;
    private ProgressBar progressbar_video;
    private SimpleDraweeView img_play_video;
    private TextView txt_play_times;
    private LinearLayout moment_content_music;
    private SimpleDraweeView moment_content_music_pic;
    private RelativeLayout rel_play;
    private SimpleDraweeView img_play;
    private TextView song_name;
    private TextView album_name;
    private TextView artist_name;
    private LinearLayout theme_content;
    private SimpleDraweeView img_theme;
    private RelativeLayout rel_theme;
    private TextView txt_theme;
    private RelativeLayout rel_select_theme;
    private TextView select_theme;
    private TextView txt_theme_tag;
    private RelativeLayout user_card_content;
    private RelativeLayout rel_user_card;
    private SimpleDraweeView img_user_card;
    private LinearLayout mask_user_card;
    private SimpleDraweeView img_user_card_avatar;
    private TextView txt_user_card_nickname;
    private TextView txt_user_card_info;
    private TextView txt_user_card_intro;
    private TextView moment_content_text;
    //网页
    private LinearLayout lin_moment_web;
    private SimpleDraweeView img_moment_web;
    private TextView txt_moment_web_title;

    private ImageView imageView;
    private ImageView topic_comma_right_iv;

    private MomentParentBean momentParentBean;

    private MomentsBean momentsBean;

    private Matcher topicMatcher = TheLConstants.TOPIC_PATTERN.matcher("");
    private Matcher urlMatcher = TheLConstants.URL_PATTERN.matcher("");
    private String topicName;
    private LinearLayout moment_delete_parent_ll;

    private int index = 0;//所在Adapter index下标
    private String momentId = "";//所在的moment的id
    private ParentMusicPlayListener parentMusicPlayListener;
    private ImageView moment_user_vip;
    private boolean isSpanClick = false;
    private RecommendLiveUserView live_user_view;
    private RelativeLayout rel_video_parent;

    public MomentParentViewOld(Context context) {
        this(context, null);
    }

    public MomentParentViewOld(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MomentParentViewOld(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }

    private void init() {
        if (mContext == null) {
            mContext = TheLApp.getContext();
        }
        inflate(mContext, R.layout.moment_parent_view, this);
        //头像、昵称、日志时间、日志类型区域
        moment_header = findViewById(R.id.moment_header_parent);
        img_thumb = findViewById(R.id.img_thumb_parent);
        moment_user_name = findViewById(R.id.moment_user_name_parent);
        moment_user_vip = findViewById(R.id.img_vip);
        moment_release_time = findViewById(R.id.moment_release_time_parent);
        txt_follow = findViewById(R.id.txt_follow);
        //提及用户区域
        moment_mentioned = findViewById(R.id.moment_mentioned_parent);
        moment_mentioned_text = findViewById(R.id.moment_mentioned_text_parent);
        //参与的话题区域
        lin_participate_theme = findViewById(R.id.lin_participate_theme_parent);
        img = findViewById(R.id.img_parent);
        txt_theme_title = findViewById(R.id.txt_theme_title_parent);
        txt_theme_desc = findViewById(R.id.txt_theme_desc_parent);
        txt_deleted = findViewById(R.id.txt_deleted_parent);
        //日志内容区域
        moment_content = findViewById(R.id.moment_content_parent);

        multi_image_vg = findViewById(R.id.multi_image_vg);
        //视频区域
        img_cover = findViewById(R.id.img_cover_parent);
        progressbar_video = findViewById(R.id.progressbar_video_parent);
        img_play_video = findViewById(R.id.img_play_video_parent);
        txt_play_times = findViewById(R.id.txt_play_times_parent);
        //音乐区域
        moment_content_music = findViewById(R.id.moment_content_music_parent);
        moment_content_music_pic = findViewById(R.id.moment_content_music_pic_parent);
        rel_play = findViewById(R.id.rel_play_parent);
        img_play = findViewById(R.id.img_play_parent);
        song_name = findViewById(R.id.song_name_parent);
        album_name = findViewById(R.id.album_name_parent);
        artist_name = findViewById(R.id.artist_name_parent);
        //话题区域
        theme_content = findViewById(R.id.theme_content_parent);
        img_theme = findViewById(R.id.img_theme_parent);
        rel_theme = findViewById(R.id.rel_theme_parent);
        txt_theme = findViewById(R.id.txt_theme_parent);
        rel_select_theme = findViewById(R.id.rel_select_theme_parent);
        select_theme = findViewById(R.id.select_theme_parent);
        txt_theme_tag = findViewById(R.id.txt_theme_tag_parent);
        //推荐用户卡片区域
        user_card_content = findViewById(R.id.user_card_content_parent);
        rel_user_card = findViewById(R.id.rel_user_card_parent);
        img_user_card = findViewById(R.id.img_user_card_parent);
        mask_user_card = findViewById(R.id.mask_user_card_parent);
        img_user_card_avatar = findViewById(R.id.img_user_card_avatar_parent);
        txt_user_card_nickname = findViewById(R.id.txt_user_card_nickname_parent);
        txt_user_card_info = findViewById(R.id.txt_user_card_info_parent);
        txt_user_card_intro = findViewById(R.id.txt_user_card_intro_parent);
        //网页
        lin_moment_web = findViewById(R.id.lin_moment_web_parent);
        img_moment_web = findViewById(R.id.img_moment_web_parent);
        txt_moment_web_title = findViewById(R.id.txt_moment_web_title_parent);
        //文本内容
        moment_content_text = findViewById(R.id.text_moment_content_parent);
        //日志已经被删除
        moment_delete_parent_ll = findViewById(R.id.moment_delete_parent_ll);

        imageView = findViewById(R.id.imageView);
        topic_comma_right_iv = findViewById(R.id.topic_comma_right_iv);
        live_user_view = findViewById(R.id.live_user_view);
        rel_video_parent = findViewById(R.id.rel_video_parent);

    }

    public MomentParentBean getMomentParentBean() {
        return momentParentBean;
    }

    public int getIndex() {
        return index;
    }

    public MomentParentViewOld initView(MomentsBean momentsBean) {
        this.momentsBean = momentsBean;
        this.momentParentBean = momentsBean.parentMoment;
        if (null == momentParentBean) {
            setVisibility(GONE);
            return this;
        }
        setVisibility(VISIBLE);
        setDefaultState();
        if (momentParentBean.deleteFlag != 0) {//如果日志已经被删除
            moment_delete_parent_ll.setVisibility(View.VISIBLE);
            moment_header.setVisibility(VISIBLE);
            setHeaderView();
            txt_follow.setVisibility(GONE);
            return this;
        }
        setViews();
        setListener();
        return this;
    }

    public MomentParentViewOld initView(MomentsBean momentsBean, int index, String momentId) {
        this.index = index;
        this.momentId = momentId;
        initView(momentsBean);
        return this;
    }


    private void setDefaultState() {

        txt_follow.setVisibility(VISIBLE);

        moment_header.setVisibility(View.GONE);
        moment_mentioned.setVisibility(GONE);
        lin_participate_theme.setVisibility(GONE);
        moment_content.setVisibility(GONE);
        theme_content.setVisibility(GONE);
        user_card_content.setVisibility(GONE);
        moment_content_text.setVisibility(GONE);

        hideAllPic();

        img_cover.setVisibility(GONE);
        progressbar_video.setVisibility(GONE);
        img_play_video.setVisibility(GONE);
        txt_play_times.setVisibility(GONE);
        lin_moment_web.setVisibility(View.GONE);
        rel_video_parent.setVisibility(GONE);

        moment_content_music.setVisibility(GONE);

        moment_delete_parent_ll.setVisibility(GONE);

        live_user_view.setVisibility(View.GONE);


    }

    private void hideAllPic() {
        if (multi_image_vg != null) {
            multi_image_vg.setVisibility(GONE);
        }
    }

    private void setViews() {

        setViewSize();
        setHeaderView();
        setmentionView();
        if (MomentsBean.MOMENT_TYPE_TEXT.equals(momentParentBean.momentsType) || MomentsBean.MOMENT_TYPE_RECOMMEND.equals(momentParentBean.momentsType)) {//纯文本（推荐日志也是纯文本日志）
            setMomentContet();
        } else if (MomentsBean.MOMENT_TYPE_IMAGE.equals(momentParentBean.momentsType)) {//图片日志
            setPicsView();
        } else if (MomentsBean.MOMENT_TYPE_TEXT_IMAGE.equals(momentParentBean.momentsType)) {//带文本的图片日志
            setMomentContet();
            setPicsView();
        } else if (MomentsBean.MOMENT_TYPE_LIVE.equals(momentParentBean.momentsType) || MomentsBean.MOMENT_TYPE_VOICE_LIVE.equals(momentParentBean.momentsType)) {//直播日志
            setMomentContet();
            setLiveView();
        } else if (MomentsBean.MOMENT_TYPE_LIVE_USER.equals(momentParentBean.momentsType) || MomentsBean.MOMENT_TYPE_VOICE_LIVE.equals(momentParentBean.momentsType)) {//直播用户
            live_user_view.setVisibility(View.VISIBLE);
            /***注意，下面这一句只是临时解决问题的方法，后期建议另做处理***/
            final MomentsBean liveMoment = MomentUtils.getMomentByLiveMoment(momentsBean.parentMoment.liveUserMoment, momentsBean.parentMoment.imageUrl);
            live_user_view.initView(liveMoment);
        } else if (MomentsBean.MOMENT_TYPE_THEME.equals(momentParentBean.momentsType)) {//话题日志
            setThemeView();
        } else if (MomentsBean.MOMENT_TYPE_THEME_REPLY.equals(momentParentBean.momentsType)) {//话题回复日志(参考纯文本日志或者图片日志或者图片文本日志)
            setMomentContet();
            if (momentParentBean.themeReplyClass != null) {
                switch (momentParentBean.themeReplyClass) {
                    case "video":
                        setVideoView();
                        break;
                    case "image":
                        setPicsView();
                        break;
                    case "voice":
                        setMuscView();
                        break;
                    default:
                        break;
                }
            }
        } else if (MomentsBean.MOMENT_TYPE_VIDEO.equals(momentParentBean.momentsType)) {//视频日志
            setMomentContet();
            setVideoView();
        } else if (MomentsBean.MOMENT_TYPE_USER_CARD.equals(momentParentBean.momentsType)) {//推荐用户名片
            setUserCardView();
        } else if (MomentsBean.MOMENT_TYPE_VOICE.equals(momentParentBean.momentsType)) {//纯音乐日志
            setMuscView();
        } else if (MomentsBean.MOMENT_TYPE_TEXT_VOICE.equals(momentParentBean.momentsType)) {//带文字的音乐日志
            setMomentContet();
            setMuscView();
        } else if (MomentsBean.MOMENT_TYPE_WEB.equals(momentParentBean.momentsType)) {//网页日志
            setWebView();
        } else if (MomentsBean.MOMENT_TYPE_USER_CARD.equals(momentParentBean.momentsType)) {//推荐主播
            setLiveView();
            setMomentContet();
        } else if (MomentsBean.MOMENT_TYPE_AD.equals(momentParentBean.momentsType)) {//广告日志
            setAdView();
        } else {
            String no_type_moment = TheLApp.context.getResources().getString(R.string.info_version_outdate);
            moment_content_text.setVisibility(VISIBLE);
            MomentUtils.setMomentContent(moment_content_text, no_type_moment);
        }
    }

    /**
     * 网页
     */
    private void setWebView() {
        JSONObject obj = new JSONObject();
        try {
            obj = new JSONObject(momentParentBean.momentsText);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RecommendWebBean bean = new RecommendWebBean();
        bean.fromJson(obj);
        if (!TextUtils.isEmpty(bean.imageUrl)) {
            setImageUrl(img_moment_web, bean.imageUrl, TheLConstants.ICON_MIDDLE_SIZE, TheLConstants.ICON_MIDDLE_SIZE);
        } else {
            img_moment_web.setImageResource(R.drawable.ic_launcher);
        }
        if (!TextUtils.isEmpty(bean.title)) {
            txt_moment_web_title.setText(bean.title);
        } else {
            txt_moment_web_title.setText(bean.url);
        }
        setMomentContet(bean.momentsText);
    }

    /**
     * 除了网页日志以外的文本日志设置
     */
    private void setMomentContet() {
        setMomentContet(momentParentBean.momentsText);
    }

    /**
     * 提及
     */
    private void setmentionView() {
        if (momentParentBean.atUserList != null) {
            if (momentParentBean.atUserList.size() > 0 && momentParentBean.shareTo != MomentsBean.SHARE_TO_ONLY_ME) {
                if (!momentParentBean.momentsId.contains(MomentsBean.SEND_MOMENT_FLAG)) {
                    moment_mentioned_text.setText(momentParentBean.getMetionedShowString());
                } else {
                    moment_mentioned_text.setText(momentParentBean.getMetionedShowStringForSendMoment());
                }
                moment_mentioned_text.setMovementMethod(LinkMovementMethod.getInstance());
                moment_mentioned.setVisibility(VISIBLE);
            }
        }
    }

    /**
     * 音乐
     */
    private void setMuscView() {

        if (momentParentBean.songId != 0) {
            moment_content.setVisibility(VISIBLE);
            moment_content_music.setVisibility(VISIBLE);

            MusicInfoBean musicInfoBean = new MusicInfoBean();
            musicInfoBean.songId = momentParentBean.songId;
            musicInfoBean.momentId = momentParentBean.momentsId;

            song_name.setText(momentParentBean.songName);
            album_name.setText(TheLApp.getContext().getString(R.string.moments_album) + "《" + momentParentBean.albumName + "》");
            artist_name.setText(TheLApp.getContext().getString(R.string.moments_artist) + " " + momentParentBean.artistName);

            if (momentParentBean.albumLogo444 != null) {
                setImageUrl(moment_content_music_pic, momentParentBean.albumLogo444);
            }
        }

    }

    /**
     * 名片
     */
    private void setUserCardView() {
        user_card_content.setVisibility(View.VISIBLE);
        rel_user_card.setVisibility(VISIBLE);
        L.d("MomentParentViewOld", " momentParentBean.cardAvatar : " + momentParentBean.cardAvatar);
        setImageUrl(img_user_card_avatar, momentParentBean.cardAvatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE);
        txt_user_card_nickname.setText(momentParentBean.cardNickName);
        txt_user_card_info.setText(momentParentBean.buildUserCardInfoStr());
        if (momentParentBean.cardIntro != null) {
            txt_user_card_intro.setText(momentParentBean.cardIntro.replaceAll("\n", " "));
        }
        mask_user_card.setBackgroundColor(ContextCompat.getColor(TheLApp.getContext(), R.color.light_gray_mask));
        if (!TextUtils.isEmpty(momentParentBean.imageUrl)) {// 被推荐用户可能不带背景图，所以要显示默认背景
            L.d("MomentParentViewOld", " momentParentBean.imageUrl : " + momentParentBean.imageUrl);
            setImageUrl(img_user_card, momentParentBean.imageUrl, TheLConstants.MOMENT_PIC_BIG_SIZE, TheLConstants.MOMENT_PIC_BIG_SIZE);
        } else {
            setImageUrl(img_user_card, null);
        }
    }

    /**
     * 视频
     */
    private void setVideoView() {
        L.d("MomentParentViewOld", "-------1------" + momentParentBean.videoUrl);
        if (!TextUtils.isEmpty(momentParentBean.videoUrl)) {
            L.d("MomentParentViewOld", "-------2------");
            moment_content.setVisibility(VISIBLE);
            rel_video_parent.setVisibility(VISIBLE);
            img_cover.setVisibility(VISIBLE);
            img_play_video.setVisibility(VISIBLE);
            txt_play_times.setVisibility(VISIBLE);
            setImageUrl(img_cover, momentParentBean.thumbnailUrl, TheLConstants.MOMENT_PIC_BIG_SIZE, TheLConstants.MOMENT_PIC_BIG_SIZE);
            String text;
            if (momentParentBean.playTime > 0) {
                text = Utils.formatTime(momentParentBean.playTime) + " / " + TheLApp.getContext().getString(R.string.play_count, momentParentBean.playCount);
            } else {
                text = TheLApp.getContext().getString(R.string.play_count, momentParentBean.playCount);
            }
            txt_play_times.setText(text);
        }
    }

    /**
     * 话题
     */
    private void setThemeView() {
        theme_content.setVisibility(VISIBLE);
        if (!TextUtils.isEmpty(momentParentBean.momentsText)) {//话题
            txt_theme.setText(momentParentBean.momentsText);
        } else {
            txt_theme.setText("");
        }
        rel_select_theme.setVisibility(GONE);
        if (!TextUtils.isEmpty(momentParentBean.themeClass)) {//话题类型
            final ThemeClassBean themeClassBean = MomentUtils.getThemeByType(momentParentBean.themeClass);
            if (themeClassBean != null) {
                rel_select_theme.setVisibility(VISIBLE);
                select_theme.setText(themeClassBean.text + " >");
            }
        }
        if (!TextUtils.isEmpty(momentParentBean.themeTagName)) {
            txt_theme_tag.setVisibility(VISIBLE);
            SpannableString sp = new SpannableString(momentParentBean.themeTagName);
            sp.setSpan(new ForegroundColorSpan(ContextCompat.getColor(TheLApp.getContext(), R.color.tag_color)), 0, momentParentBean.themeTagName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            txt_theme_tag.setText(sp);
        } else {
            txt_theme_tag.setVisibility(GONE);
        }
        if (TextUtils.isEmpty(momentParentBean.imageUrl)) {// 话题日志有可能不带背景图，所以要显示默认背景
            setImageUrl(img_theme, TheLConstants.RES_PIC_URL + R.mipmap.bg_topic_default);//没背景
            rel_theme.setBackgroundColor(ContextCompat.getColor(TheLApp.getContext(), R.color.black_transprent_30));
        } else {
            setImageUrl(img_theme, momentParentBean.imageUrl, TheLConstants.MOMENT_PIC_BIG_SIZE, TheLConstants.MOMENT_PIC_BIG_SIZE);
            rel_theme.setBackgroundColor(ContextCompat.getColor(TheLApp.getContext(), R.color.black_transprent_30));
        }

        rel_select_theme.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                final ThemeClassBean themeClassBean = MomentUtils.getThemeByType(momentParentBean.themeClass);
                Utils.gotoThemeTab(themeClassBean, getContext());
            }
        });
        select_theme.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (momentParentBean.momentsType.equals(MomentsBean.MOMENT_TYPE_AD)) {

                    final Intent intent = new Intent(mContext, WebViewActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(BundleConstants.URL, momentParentBean.adUrl);
                    intent.putExtras(bundle);
                    mContext.startActivity(intent);
                } else {
                    ThemeClassBean themeClassBean = MomentUtils.getThemeByType(momentParentBean.themeClass);

                    Utils.gotoThemeTab(themeClassBean, mContext);
                }
            }
        });
    }

    private void setAdView() {
        theme_content.setVisibility(VISIBLE);
        rel_select_theme.setVisibility(View.VISIBLE);
        moment_content_text.setVisibility(View.VISIBLE);
        moment_release_time.setBackgroundResource(R.drawable.shape_ad_text_bg);
        moment_release_time.setTextColor(TheLApp.context.getResources().getColor(R.color.white));
        moment_release_time.setTypeface(null, Typeface.BOLD);
        select_theme.setText(TheLApp.getContext().getString(R.string.click_to_see_more));
        imageView.setVisibility(GONE);
        topic_comma_right_iv.setVisibility(GONE);
        setImageUrl(img_theme, momentParentBean.imageUrl, TheLConstants.MOMENT_PIC_BIG_SIZE, TheLConstants.MOMENT_PIC_BIG_SIZE);
        moment_content_text.setText(momentParentBean.momentsText);

        String adText = null;

        switch (momentParentBean.adType) {
            case "official":
                adText = TheLApp.context.getResources().getString(R.string.official);
                break;
            case "ad":
                adText = TheLApp.context.getResources().getString(R.string.AD);
                break;
            default:
                break;
        }

        moment_release_time.setText(adText);
    }

    /**
     * 设置直播图片
     */
    private void setLiveView() {
        moment_content.setVisibility(VISIBLE);
        live_user_view.setVisibility(VISIBLE);
        live_user_view.initView(momentParentBean);
    }

    /**
     * 设置图片
     */
    private void setPicsView() {
        if (TextUtils.isEmpty(momentParentBean.thumbnailUrl)) {//如果为空，则返回，不然为空也会获取到的count=1
            return;
        }
        final String[] picUrls = momentParentBean.thumbnailUrl.split(",");
        final int count = picUrls.length;
        if (count > 0) {
            moment_content.setVisibility(VISIBLE);
            multi_image_vg.setVisibility(View.VISIBLE);
            multi_image_vg.initUI(momentParentBean.imageUrl, momentParentBean.userName, Utils.getImageShareBean(momentParentBean));
        }
    }

    /**
     * 文本内容
     */
    private void setMomentContet(String momentsText) {
        if (!TextUtils.isEmpty(momentsText)) {
            moment_content_text.setVisibility(VISIBLE);
        }
        // moment文字内容
        // 检查是否有输入话题，有的话把话题标红
        topicMatcher.reset(momentsText);
        List<Integer> startIndexs = new ArrayList<Integer>();
        List<Integer> endIndexs = new ArrayList<Integer>();
        while (topicMatcher.find()) {
            startIndexs.add(topicMatcher.start());
            endIndexs.add(topicMatcher.start() + topicMatcher.group().length());
        }
        // 检查是否有输入网址，有的话把网址替换样式
        urlMatcher.reset(momentsText);
        List<Integer> startIndexsUrl = new ArrayList<Integer>();
        List<Integer> endIndexsUrl = new ArrayList<Integer>();
        while (urlMatcher.find()) {
            startIndexsUrl.add(urlMatcher.start());
            endIndexsUrl.add(urlMatcher.start() + urlMatcher.group().length());
        }
        if (!startIndexs.isEmpty() || !startIndexsUrl.isEmpty()) {
            // 创建一个 SpannableString对象
            SpannableString sp = new SpannableString(momentsText);
            for (int i = 0; i < startIndexs.size(); i++) {
                // 设置高亮样式，话题主页不能点击话题只给高亮，所以加个标识区分一下
                if (TextUtils.isEmpty(topicName)) {
                    sp.setSpan(new TopicClickSpan(sp.subSequence(startIndexs.get(i), endIndexs.get(i))) {
                        @Override
                        public void onClick(View widget) {
                            isSpanClick = true;
                            super.onClick(widget);
                        }
                    }, startIndexs.get(i), endIndexs.get(i), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                } else {
                    if (sp.subSequence(startIndexs.get(i), endIndexs.get(i)).toString().equals(topicName)) {
                        sp.setSpan(new ForegroundColorSpan(ContextCompat.getColor(TheLApp.getContext(), R.color.tag_color)), startIndexs.get(i), endIndexs.get(i), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                    } else {
                        sp.setSpan(new TopicClickSpan(sp.subSequence(startIndexs.get(i), endIndexs.get(i))) {
                            @Override
                            public void onClick(View widget) {
                                isSpanClick = true;
                                super.onClick(widget);
                            }
                        }, startIndexs.get(i), endIndexs.get(i), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    }
                }

            }
            for (int i = 0; i < startIndexsUrl.size(); i++) {
                final String url = momentsText.substring(startIndexsUrl.get(i), endIndexsUrl.get(i));
                sp.setSpan(new ImageSpan(TheLApp.getContext(), createUrlDrawble()), startIndexsUrl.get(i), endIndexsUrl.get(i), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                sp.setSpan(new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        isSpanClick = true;
                        Intent intent = new Intent(TheLApp.getContext(), WebViewActivity.class);
                        intent.putExtra(BundleConstants.URL, url);
                        intent.putExtra("title", "");
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        TheLApp.getContext().startActivity(intent);
                    }
                }, startIndexsUrl.get(i), endIndexsUrl.get(i), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
            // SpannableString对象设置给TextView
            moment_content_text.setText(sp);
            moment_content_text.setMovementMethod(MyLinkMovementMethod.getInstance());
        } else {
            moment_content_text.setText(momentsText);
        }
    }

    private void setViewSize() {
        int bigSize = (int) (AppInit.displayMetrics.widthPixels - TheLApp.getContext().getResources().getDimension(R.dimen.moment_list_margin) * 2);
        final RelativeLayout.LayoutParams bigSizeParams = new RelativeLayout.LayoutParams(bigSize, bigSize);
        bigSizeParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        img_cover.setLayoutParams(bigSizeParams);
        moment_content_music_pic.setLayoutParams(bigSizeParams);
        rel_theme.setLayoutParams(bigSizeParams);
        img_theme.setLayoutParams(bigSizeParams);
        rel_user_card.setLayoutParams(bigSizeParams);
    }

    /**
     * 设置头部信息，包括昵称，日期时间，日志类型
     */
    private void setHeaderView() {
        //头像，昵称
        moment_header.setVisibility(View.VISIBLE);
        if (MomentsBean.IS_SECRET == momentParentBean.secret) {//是匿名
            setImageUrl(img_thumb, TheLConstants.RES_PIC_URL + R.mipmap.icon_user_anonymous);
            moment_user_name.setText(TheLApp.getContext().getString(R.string.moments_msgs_notification_anonymous));
            moment_user_vip.setVisibility(GONE);

        } else {
            setImageUrl(img_thumb, momentParentBean.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE);
            momentParentBean.setNicknameWithMomentType(moment_user_name, momentParentBean);
            if (momentParentBean.level > 0) {

                moment_user_vip.setVisibility(VISIBLE);
                switch (momentParentBean.level) {
                    case 1:
                        moment_user_vip.setImageResource(R.mipmap.icn_vip_1);

                        break;
                    case 2:
                        moment_user_vip.setImageResource(R.mipmap.icn_vip_2);

                        break;
                    case 3:
                        moment_user_vip.setImageResource(R.mipmap.icn_vip_3);

                        break;
                    case 4:
                        moment_user_vip.setImageResource(R.mipmap.icn_vip_4);

                        break;
                }
            } else {
                moment_user_vip.setVisibility(GONE);

            }
        }
        // 发布时间
        if (TextUtils.isEmpty(momentParentBean.distance)) {
            moment_release_time.setText(momentParentBean.getReleaseTimeShow());
        } else {// 如果距离字段不为空，则要显示距离
            if ("附近".equals(momentParentBean.distance)) {
                moment_release_time.setText(momentParentBean.getReleaseTimeShow() + "/" + TheLApp.getContext().getString(R.string.topic_followers_act_nearby));
            } else {
                moment_release_time.setText(momentParentBean.distance + "/" + momentParentBean.getReleaseTimeShow());
            }
        }

        if (momentsBean.myself == 0 && !UserUtils.getMyUserId().equals(momentsBean.userId + "") && momentsBean.secret == MomentsBean.IS_NOT_SECRET) {

            txt_follow.setVisibility(View.VISIBLE);

            txt_follow.setFollowData(String.valueOf(momentsBean.userId), momentsBean.nickname, momentsBean.avatar, momentsBean.followerStatus);

        } else {
            txt_follow.setVisibility(View.GONE);
        }

    }

    private void setImageUrl(SimpleDraweeView view, String url) {
        view.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(url))).build()).setAutoPlayAnimations(true).build());
    }

    private void setImageUrl(SimpleDraweeView view, String url, float width, float height) {
        view.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(url, width, height))).build()).setAutoPlayAnimations(true).build());
    }

    private SpannableString buildThemeHeaderStr(String nickName, String str) {
        SpannableString spannableString = new SpannableString(nickName + " " + str);
        spannableString.setSpan(new TextSpan(12, R.color.text_color_gray), nickName.length(), nickName.length() + str.length() + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannableString;
    }

    private Bitmap createUrlDrawble() {
        View view = LayoutInflater.from(TheLApp.getContext()).inflate(R.layout.layout_url_view, null);
        view.setDrawingCacheEnabled(true);
        view.measure(MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED), MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        return view.getDrawingCache();
    }

    /**
     * ********************************一下是个中接口和监听****************************************
     **/
    private void setListener() {
        img_thumb.setOnClickListener(this);
        img_play_video.setOnClickListener(this);
        user_card_content.setOnClickListener(this);
        lin_moment_web.setOnClickListener(this);
        moment_content_text.setOnClickListener(this);

        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoMomentActivity();
            }
        });
    }

    @Override
    public void onClick(View v) {
        ViewUtils.preventViewMultipleClick(v, 1500);
        switch (v.getId()) {
            case R.id.img_thumb_parent://头像
                gotoUserInfoActivity(momentParentBean.userId + "");
                break;
            case R.id.pic1_parent://图1
                showPicPreView(v, 0);
                break;
            case R.id.pic2_parent:
                showPicPreView(v, 1);
                break;
            case R.id.pic3_parent:
                showPicPreView(v, 2);
                break;
            case R.id.pic4_parent:
                showPicPreView(v, 3);
                break;
            case R.id.pic5_parent:
                showPicPreView(v, 4);
                break;
            case R.id.pic6_parent:
                showPicPreView(v, 5);
                break;
            case R.id.img_play_video_parent://视频播放
                playVideo();
                break;
            case R.id.img_play_parent://音乐播放
                playMusic();
                break;
            case R.id.user_card_content_parent://用户名片
                gotoUserInfoActivity(momentParentBean.cardUserId + "");
                break;
            case R.id.lin_moment_web_parent://网页
                gotoWebActivity();
                break;
            case R.id.text_moment_content_parent:
                if (isSpanClick) {
                    isSpanClick = false;
                    return;
                }
                gotoMomentActivity();
                break;
        }
    }

    private void gotoWebActivity() {
        RecommendWebBean bean = MomentUtils.getRecommendWebBeanFromMoment(momentParentBean);
        if (bean == null || TextUtils.isEmpty(bean.url)) {
            return;
        }
        final Intent intent = new Intent();
        intent.setClass(mContext, WebViewActivity.class);
        intent.putExtra("url", bean.url);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
    }

    private void gotoUserInfoActivity(String userId) {
//        final Intent intent = new Intent();
//        intent.setClass(mContext, UserInfoActivity.class);
//        intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, userId);
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        mContext.startActivity(intent);
        FlutterRouterConfig.Companion.gotoUserInfo(userId);
    }

    /**
     * 播放音乐
     */
    private void playMusic() {
        if (momentParentBean != null && MomentsBean.MOMENT_TYPE_TEXT_VOICE.equals(momentParentBean.momentsType) || MomentsBean.MOMENT_TYPE_VOICE.equals(momentParentBean.momentsType)) {
            if (parentMusicPlayListener != null) {
                if (!TextUtils.isEmpty(momentId)) {
                    parentMusicPlayListener.play(momentParentBean.songId, momentId, index, momentParentBean.momentsId, img_play);
                }
            }
        }
    }

    private void playVideo() {

        VideoBean videoBean = MomentUtils.getVideoFromMomentParent(momentParentBean);

        Intent intent;
        intent = new Intent(getContext(), UserVideoListActivity.class);
        intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_BEAN, momentParentBean);
        intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, momentParentBean.userId + "");
        intent.putExtra(TheLConstants.BUNDLE_KEY_VIDEO_BEAN, videoBean);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        TheLApp.getContext().startActivity(intent);
    }

    private void showPicPreView(View view, int index) {
        ViewUtils.preventViewMultipleClick(view, 1000);
        String[] photoUrls = momentParentBean.imageUrl.split(",");
        Intent intent = new Intent(TheLApp.getContext(), UserInfoPhotoActivity.class);
        ImgShareBean bean = Utils.getImageShareBean(momentParentBean);
        Bundle bundle = new Bundle();
        bundle.putSerializable(TheLConstants.BUNDLER_KEY_IMAGESHAREBEAN, bean);
        intent.putExtras(bundle);
        ArrayList<String> photos = new ArrayList<>();
        for (int i = 0; i < photoUrls.length; i++) {
            photos.add(photoUrls[i]);
        }
        intent.putStringArrayListExtra(TheLConstants.BUNDLE_KEY_PHOTOS, photos);
        intent.putExtra(TheLConstants.BUNDLE_KEY_RELA_ID, momentParentBean.userName);
        intent.putExtra(TheLConstants.IS_WATERMARK, true);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("position", index);
        TheLApp.getContext().startActivity(intent);
    }

    //关注用户
    private void followUser() {
       /* new RequestBussiness().followUser(new OneRequestNetworkHelper(new UIDataListener() {
            @Override
            public void onDataChanged(RequestCoreBean rcb) {
                // 成功数据
                if (RequestConstants.RESPONSE_SUCCESS == rcb.responseValue && null != rcb.responseDataObj) {
                    sendRefreshUserFollowStatusBroadCast(momentParentBean.userId + "", 1);
                    DialogUtil.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.userinfo_activity_followed));
                    // 发送关注消息
                    SendMsgUtils.getInstance().sendMessage(MsgUtils.createMsgBean(MsgBean.MSG_TYPE_FOLLOW, "", 1, momentParentBean.momentsId + "", momentParentBean.nickname, momentParentBean.avatar));
                } else {
                    NetworkUtils.handleErrorResponse(rcb, true);
                }
            }

            @Override
            public void onErrorHappened(VolleyError error, RequestCoreBean rcb) {
                NetworkUtils.handleErrorMessage(error);
            }
        }), momentParentBean.userId + "", "1");*/
        FollowStatusChangedImpl.followUser(momentParentBean.userId + "", FollowStatusChangedImpl.ACTION_TYPE_FOLLOW, momentParentBean.nickname, momentParentBean.avatar);
    }

    /**
     * 跳转到日志详情（被推荐的）
     */
    private void gotoMomentActivity() {
        if (momentParentBean == null || momentParentBean.deleteFlag != 0 || TextUtils.isEmpty(momentParentBean.momentsId)) {//日志为Null,删除，空日志id
            return;
        }
        if (MomentsBean.MOMENT_TYPE_THEME.equals(momentParentBean.momentsType)) {//如果是话题日志，跳转到话题详情
//            Intent intent = null;
//            intent = new Intent(mContext, ThemeDetailActivity.class);
//            intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, momentParentBean.momentsId);
//            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            TheLApp.getContext().startActivity(intent);
            FlutterRouterConfig.Companion.gotoThemeDetails(momentParentBean.momentsId);
        } else {
//            intent = new Intent(mContext, MomentCommentActivity.class);
//            intent.putExtra(TheLConstants.BUNDLE_KEY_COMMENT_ON_TOP, momentsBean.momentsText);
//            intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, momentsBean.userId);
            FlutterRouterConfig.Companion.gotoMomentDetails(momentParentBean.momentsId);
        }
    }

    private void sendRefreshUserFollowStatusBroadCast(String userId, int type) {
        final Intent intent = new Intent();
        intent.setAction(TheLConstants.BROADCAST_REFRESH_USER_FOLLOW_STATUS);
        intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, userId);
        intent.putExtra(TheLConstants.BUNDLE_KEY_FOLLOW_STATUS, type);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        TheLApp.getContext().sendBroadcast(intent);
    }

    /**
     * 播放音乐接口
     */
    public interface ParentMusicPlayListener {
        void play(long songId, String momentId, int index, String parentId, View img_play);
    }

    public void setParentMusicPlayListener(ParentMusicPlayListener listener) {
        this.parentMusicPlayListener = listener;
    }

}
