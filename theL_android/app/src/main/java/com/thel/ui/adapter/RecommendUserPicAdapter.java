package com.thel.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.thel.R;
import com.thel.bean.RecommendListBean;
import com.thel.constants.TheLConstants;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.preview_image.UserInfoPhotoActivity;
import com.thel.utils.SizeUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by liuyun on 2017/12/29.
 */

public class RecommendUserPicAdapter extends HeaderAndFooterAdapter<RecommendListBean.PicBean> {

    private int picWidth = 0;

    private float cornerWidth = 0;

    public RecommendUserPicAdapter(@NonNull Context context, int normalViewId) {
        super(context, normalViewId);

        picWidth = SizeUtils.dip2px(context, 50);
        cornerWidth = SizeUtils.dip2px(context, 5);

    }

    @Override public void onNormalBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {

        RecommendListBean.PicBean item = getData().get(position);

        RecommendUserPicViewHolder recommendUserPicViewHolder = (RecommendUserPicViewHolder) viewHolder;

        ImageLoaderManager.imageLoaderDefaultCorner(recommendUserPicViewHolder.pic_iv, R.color.gray, item.longThumbnailUrl.replace("-w204", ""), picWidth, picWidth, cornerWidth);

        recommendUserPicViewHolder.pic_iv.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), UserInfoPhotoActivity.class);
                ArrayList<String> photos = new ArrayList<>();

                List<RecommendListBean.PicBean> picBeans = getData();

                for (int i = 0; i < picBeans.size(); i++) {
                    RecommendListBean.PicBean picBean = picBeans.get(i);
                    photos.add(picBean.picUrl);
                }
                intent.putStringArrayListExtra(TheLConstants.BUNDLE_KEY_PHOTOS, photos);
                Bundle bundle = new Bundle();
                intent.putExtra("position", position);
                intent.putExtras(bundle);

                v.getContext().startActivity(intent);
            }
        });

    }

    @Override public RecyclerView.ViewHolder onNormalViewHolder(View view, ViewGroup viewGroup, int viewType) {
        return new RecommendUserPicViewHolder(view);
    }


    public class RecommendUserPicViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.pic_iv) ImageView pic_iv;

        public RecommendUserPicViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }

}
