package com.thel.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.GridView;

/**
 * Created by waiarl on 2017/10/18.
 * 自适应高度
 */

public class MyGridView extends GridView {
    private Context context;

    public MyGridView(Context context) {
        super(context);
        this.context = context;
    }

    public MyGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    public MyGridView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, expandSpec);
    }
}
