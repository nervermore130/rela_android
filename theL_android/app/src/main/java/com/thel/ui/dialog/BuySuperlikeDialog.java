package com.thel.ui.dialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.RelativeSizeSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.appevents.AppEventsLogger;
import com.rela.pay.OnPayStatusListener;
import com.rela.pay.PayConstants;
import com.rela.pay.PayProxyImpl;
import com.thel.BuildConfig;
import com.thel.R;
import com.thel.android.pay.IabHelper;
import com.thel.android.pay.IabResult;
import com.thel.android.pay.Inventory;
import com.thel.android.pay.Purchase;
import com.thel.android.pay.SkuDetails;
import com.thel.android.pay.SyncOrdersService;
import com.thel.app.TheLApp;
import com.thel.bean.GoogleIapNotifyResultBean;
import com.thel.bean.PayOrderBean;
import com.thel.bean.SuperlikeListBean;
import com.thel.constants.BundleConstants;
import com.thel.constants.TheLConstants;
import com.thel.db.MomentsDataBaseAdapter;
import com.thel.growingio.GIoGiftTrackBean;
import com.thel.growingio.GrowingIoConstant;
import com.thel.modules.main.home.moments.web.WebViewActivity;
import com.thel.modules.main.me.aboutMe.MySoftMoneyActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.RequestConstants;
import com.thel.utils.AppInit;
import com.thel.utils.DialogUtil;
import com.thel.utils.GooglePayUtils;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.GsonUtils;
import com.thel.utils.JsonUtils;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

import androidx.fragment.app.DialogFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import video.com.relavideolibrary.Utils.DensityUtils;

/**
 * Created by chad
 * Time 18/9/12
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */
public class BuySuperlikeDialog extends DialogFragment {

    @OnClick(R.id.close)
    void close() {
        this.dismiss();
    }

    @OnClick(R.id.cancel)
    void cancel() {
        this.dismiss();
    }

    @OnClick(R.id.buy_superlike)
    void buy_superlike() {
        if (list.size() > 0) {
            operate(list.get(selectbuyPos).id);
        }
    }

    @OnClick(R.id.txt_pay_qa)
    void question(View v) {
        ViewUtils.preventViewMultipleClick(v, 1000);
        Intent intent = new Intent(getContext(), WebViewActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(BundleConstants.URL, TheLConstants.BUY_SOFT_MONEY_HELP_PAGE_URL);
        bundle.putBoolean(WebViewActivity.NEED_SECURITY_CHECK, false);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @OnClick(R.id.item1)
    void choice1() {
        selectItem(0);
    }

    @OnClick(R.id.item2)
    void choice2() {
        selectItem(1);
    }

    @OnClick(R.id.item3)
    void choice3() {
        selectItem(2);
    }

    @BindView(R.id.item1)
    LinearLayout item1;

    @BindView(R.id.item2)
    LinearLayout item2;

    @BindView(R.id.item3)
    LinearLayout item3;

    @BindView(R.id.count1)
    TextView count1;

    @BindView(R.id.price1)
    TextView price1;

    @BindView(R.id.old_price1)
    TextView old_price1;

    @BindView(R.id.count2)
    TextView count2;

    @BindView(R.id.price2)
    TextView price2;

    @BindView(R.id.old_price2)
    TextView old_price2;

    @BindView(R.id.count3)
    TextView count3;

    @BindView(R.id.price3)
    TextView price3;

    @BindView(R.id.old_price3)
    TextView old_price3;

    private int selectbuyPos = 1;

    private IabHelper mHelper;

    private boolean iabSetuped = false;

    private String price_currency_code = "";//货币参数

    public List<SuperlikeListBean.Lists> list = new ArrayList<>();

    private PayResultReceiver payResultReceiver;

    public static BuySuperlikeDialog newInstance() {
        BuySuperlikeDialog buySuperlikeDialog = new BuySuperlikeDialog();
        return buySuperlikeDialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.buy_super_like_dialog_layout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        RequestBusiness.getInstance().superlikeList()
                .onBackpressureDrop().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new InterceptorSubscribe<SuperlikeListBean>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onNext(SuperlikeListBean data) {
                        super.onNext(data);
                        if (data != null && data.data != null && data.data.list != null && data.data.list.size() > 0) {
                            list = data.data.list;
                            for (int i = 0; i < data.data.list.size(); i++) {
                                SuperlikeListBean.Lists lists = data.data.list.get(i);
                                SpannableString ss = new SpannableString("x" + lists.count);
                                ss.setSpan(new RelativeSizeSpan(0.7f), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                                if (i == 0) {
                                    count1.setText(ss);
                                    price1.setText("¥ " + lists.realPrice / 100 + ".00");
                                } else if (i == 1) {
                                    count2.setText(ss);
                                    price2.setText("¥ " + lists.realPrice / 100 + ".00");
                                    old_price2.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                                    old_price2.setText(TheLApp.context.getString(R.string.superlike_original_price) + lists.price / 100 + ".00");
                                } else if (i == 2) {
                                    count3.setText(ss);
                                    price3.setText("¥ " + lists.realPrice / 100 + ".00");
                                    old_price3.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                                    old_price3.setText(TheLApp.context.getString(R.string.superlike_original_price) + lists.price / 100 + ".00");
                                }
                            }

                            if (RequestConstants.APPLICATION_ID_GLOBAL.equals(BuildConfig.APPLICATION_ID)) {
                                setupIap(data.data.list);
                            }
                        }
                    }
                });
        registerReceiver();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), R.style.DialogFadeAnim);

        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER); //可设置dialog的位置
        window.getDecorView().setPadding(0, 0, 0, 0); //消除边距
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.width = AppInit.displayMetrics.widthPixels - DensityUtils.dp2px(50);   //设置宽度充满屏幕
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        return dialog;
    }

    @Override
    public void onDestroy() {
        if (getActivity() != null) {
            //拿到InputMethodManager
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            //如果window上view获取焦点 && view不为空
            if (imm != null && imm.isActive() && getActivity().getCurrentFocus() != null) {
                //拿到view的token 不为空
                if (getActivity().getCurrentFocus().getWindowToken() != null) {
                    //表示软键盘窗口总是隐藏，除非开始时以SHOW_FORCED显示。
                    imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                }
            }

            try {
                getActivity().unregisterReceiver(payResultReceiver);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        super.onDestroy();
    }

    private void selectItem(int position) {
        selectbuyPos = position;
        switch (position) {
            case 0:
                item1.setBackgroundResource(R.drawable.bg_item_buy_superlike);
                item2.setBackgroundResource(0);
                item3.setBackgroundResource(0);

                count1.setTextColor(ContextCompat.getColor(TheLApp.getContext(), R.color.rela_color));
                count2.setTextColor(ContextCompat.getColor(TheLApp.getContext(), R.color.black));
                count3.setTextColor(ContextCompat.getColor(TheLApp.getContext(), R.color.black));

                price1.setTextColor(ContextCompat.getColor(TheLApp.getContext(), R.color.rela_color));
                price2.setTextColor(ContextCompat.getColor(TheLApp.getContext(), R.color.black));
                price3.setTextColor(ContextCompat.getColor(TheLApp.getContext(), R.color.black));

                old_price2.setTextColor(ContextCompat.getColor(TheLApp.getContext(), R.color.white_gray));
                old_price3.setTextColor(ContextCompat.getColor(TheLApp.getContext(), R.color.white_gray));
                break;
            case 1:
                item2.setBackgroundResource(R.drawable.bg_item_buy_superlike);
                item1.setBackgroundResource(0);
                item3.setBackgroundResource(0);

                count2.setTextColor(ContextCompat.getColor(TheLApp.getContext(), R.color.rela_color));
                count1.setTextColor(ContextCompat.getColor(TheLApp.getContext(), R.color.black));
                count3.setTextColor(ContextCompat.getColor(TheLApp.getContext(), R.color.black));

                price2.setTextColor(ContextCompat.getColor(TheLApp.getContext(), R.color.rela_color));
                price1.setTextColor(ContextCompat.getColor(TheLApp.getContext(), R.color.black));
                price3.setTextColor(ContextCompat.getColor(TheLApp.getContext(), R.color.black));

                old_price2.setTextColor(Color.parseColor("#00bfc1"));
                old_price3.setTextColor(ContextCompat.getColor(TheLApp.getContext(), R.color.white_gray));
                break;
            case 2:
                item3.setBackgroundResource(R.drawable.bg_item_buy_superlike);
                item2.setBackgroundResource(0);
                item1.setBackgroundResource(0);

                count3.setTextColor(ContextCompat.getColor(TheLApp.getContext(), R.color.rela_color));
                count2.setTextColor(ContextCompat.getColor(TheLApp.getContext(), R.color.black));
                count1.setTextColor(ContextCompat.getColor(TheLApp.getContext(), R.color.black));

                price3.setTextColor(ContextCompat.getColor(TheLApp.getContext(), R.color.rela_color));
                price2.setTextColor(ContextCompat.getColor(TheLApp.getContext(), R.color.black));
                price1.setTextColor(ContextCompat.getColor(TheLApp.getContext(), R.color.black));

                old_price3.setTextColor(Color.parseColor("#00bfc1"));
                old_price2.setTextColor(ContextCompat.getColor(TheLApp.getContext(), R.color.white_gray));
                break;
        }
    }

    private void operate(final int superlikeId) {
        if (RequestConstants.APPLICATION_ID_LOCAL.equals(BuildConfig.APPLICATION_ID))
            DialogUtil.getInstance().showSelectionDialogWithIcon((Activity) getContext(), getString(R.string.pay_options), new String[]{getString(R.string.pay_alipay), getString(R.string.pay_wx)}, new int[]{R.mipmap.icon_zhifubao, R.mipmap.icon_wechat}, new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                    DialogUtil.getInstance().closeDialog();
                    MobclickAgent.onEvent(TheLApp.getContext(), "click_price_button");
                    switch (position) {
                        case 0:// 支付宝
                            MobclickAgent.onEvent(getContext(), "start_alipay");

                            RequestBusiness.getInstance().createStickerPackOrder(TheLConstants.PRODUCT_TYPE_SOFT_SUPER_LIKE, superlikeId + "", PayConstants.PAY_TYPE_ALIPAY, PayConstants.ALIPAY_SOURCE).observeOn(AndroidSchedulers.mainThread()).onBackpressureDrop().subscribeOn(Schedulers.io()).subscribe(new InterceptorSubscribe<PayOrderBean>() {
                                @Override public void onNext(PayOrderBean data) {
                                    super.onNext(data);

                                    final GIoGiftTrackBean payTrackBean = new GIoGiftTrackBean(GrowingIoConstant.MEMBERSHIP_PURCHASE, data.data.outTradeNo, data.data.subject, PayConstants.PAY_TYPE_ALIPAY);
                                    GrowingIOUtil.payGiftTrack(payTrackBean);

                                    PayProxyImpl.getInstance().pay(PayConstants.PAY_TYPE_ALIPAY, getActivity(), GsonUtils.createJsonString(data.data), new OnPayStatusListener() {
                                        @Override public void onPayStatus(int payStatus, String errorInfo) {

                                            if (payStatus == 1) {
                                                MobclickAgent.onEvent(getContext(), "vip_pay_succeed");// 购买vip支付成功事件统计
                                                hasBoughtVip = true;
                                                DialogUtil.showToastShort(TheLApp.getContext(), getContext().getString(R.string.buy_superlike_succeed));
                                                if (buySuperlikeListener != null) {
                                                    buySuperlikeListener.success(list.get(selectbuyPos).count);
                                                }
                                                BuySuperlikeDialog.this.dismiss();

                                            }
                                        }

                                    });

                                }
                            });
                            break;
                        case 1: // 微信支付
                            MobclickAgent.onEvent(getContext(), "start_wx_pay");

                            RequestBusiness.getInstance().createStickerPackOrder(TheLConstants.PRODUCT_TYPE_SOFT_SUPER_LIKE, superlikeId + "", PayConstants.PAY_TYPE_WXPAY, PayConstants.WXPAY_SOURCE).observeOn(AndroidSchedulers.mainThread()).onBackpressureDrop().subscribeOn(Schedulers.io()).subscribe(new InterceptorSubscribe<PayOrderBean>() {
                                @Override public void onNext(PayOrderBean data) {
                                    super.onNext(data);

                                    final GIoGiftTrackBean payTrackBean = new GIoGiftTrackBean(GrowingIoConstant.MEMBERSHIP_PURCHASE, data.data.outTradeNo, data.data.subject, PayConstants.PAY_TYPE_WXPAY);
                                    GrowingIOUtil.payGiftTrack(payTrackBean);

                                    PayProxyImpl.getInstance().pay(PayConstants.PAY_TYPE_WXPAY, getActivity(), GsonUtils.createJsonString(data.data), new OnPayStatusListener() {
                                        @Override public void onPayStatus(int payStatus, String errorInfo) {

                                            if (payStatus == 1) {

                                                MobclickAgent.onEvent(getContext(), "vip_pay_succeed");// 购买vip支付成功事件统计

                                                L.d("showWhoSeenMeData", " onReceive hasBoughtVip : " + hasBoughtVip);

                                                hasBoughtVip = true;
                                                DialogUtil.showToastShort(TheLApp.getContext(), getString(R.string.buy_superlike_succeed));
                                                if (buySuperlikeListener != null) {
                                                    buySuperlikeListener.success(list.get(selectbuyPos).count);
                                                }
                                                try {
                                                    BuySuperlikeDialog.this.dismiss();
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }

                                        }
                                    });

                                }
                            });
                            break;
                        default:
                            break;
                    }
                }
            }, false, null);
        else {// 国际版，用google wallet
            RequestBusiness.getInstance().createStickerPackOrder(TheLConstants.PRODUCT_TYPE_SOFT_SUPER_LIKE, superlikeId + "", GooglePayUtils.GOOGLEPAY, GooglePayUtils.GOOGLEPAY_SOURCE).onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new PayConsumer(GooglePayUtils.GOOGLEPAY));
        }
    }

    private class IapNotifyConsumer extends InterceptorSubscribe<GoogleIapNotifyResultBean> {

        private String outTradeNo;

        public IapNotifyConsumer(String outTradeNo) {
            this.outTradeNo = outTradeNo;
        }

        @Override
        public void onNext(GoogleIapNotifyResultBean data) {
            super.onNext(data);
            if (GooglePayUtils.getInstance().isNotified(data, true)) {
                final Purchase purchase = MomentsDataBaseAdapter.getInstance(TheLApp.getContext(), ShareFileUtils.getString(ShareFileUtils.ID, "")).getInAppOrder(outTradeNo);
                mHelper.consumeAsync(purchase, new IabHelper.OnConsumeFinishedListener() {
                    @Override
                    public void onConsumeFinished(Purchase purchase, IabResult result) {

                    }
                });
                MomentsDataBaseAdapter.getInstance(TheLApp.getContext(), ShareFileUtils.getString(ShareFileUtils.ID, "")).deleteInAppOrder(outTradeNo);

                L.d("showWhoSeenMeData", " onNext hasBoughtVip : " + hasBoughtVip);

                hasBoughtVip = true;
                DialogUtil.showToastShort(TheLApp.getContext(), getContext().getString(R.string.buy_superlike_succeed));
                if (buySuperlikeListener != null) {
                    buySuperlikeListener.success(list.get(selectbuyPos).count);
                }
                BuySuperlikeDialog.this.dismiss();
            } else {
                notifyFailed(outTradeNo);
            }

        }
    }

    private class PayConsumer extends InterceptorSubscribe<PayOrderBean> {

        private String payType;

        public PayConsumer(String payType) {
            this.payType = payType;
        }

        @Override
        public void onNext(PayOrderBean result) {
            super.onNext(result);
            MobclickAgent.onEvent(getContext(), "create_order_succeed");
//            if (AlipayUtils.ALIPAY.equals(payType)) {
//                MobclickAgent.onEvent(getContext(), "start_alipay");
//                AlipayUtils.getInstance().pay((Activity) getContext(), AlipayUtils.getInstance().getOrderInfo(result.data.outTradeNo, result.data.subject, result.data.description, result.data.totalFee, result.data.alipayNotifyUrl, result.data.expire), mHandler);
//                final GIoGiftTrackBean payTrackBean = new GIoGiftTrackBean(GrowingIoConstant.MEMBERSHIP_PURCHASE, result.data.outTradeNo, result.data.subject, AlipayUtils.ALIPAY);
//                GrowingIOUtil.payGiftTrack(payTrackBean);
//            } else
//            if (WXPayUtils.WXPAY.equals(payType)) {
//                MobclickAgent.onEvent(getContext(), "start_wx_pay");
//                WXPayUtils.getInstance().pay(result.data.wxOrder);
//                final GIoGiftTrackBean payTrackBean = new GIoGiftTrackBean(GrowingIoConstant.MEMBERSHIP_PURCHASE, result.data.outTradeNo, result.data.subject, WXPayUtils.WXPAY);
//                GrowingIOUtil.payGiftTrack(payTrackBean);
//            } else
            if (GooglePayUtils.GOOGLEPAY.equals(payType)) {
                if (iabSetuped && mHelper != null) {
                    final String sku = result.data.iapId;
                    buyItem(sku, result.data.outTradeNo, result.data.totalFee);
                    final GIoGiftTrackBean payTrackBean = new GIoGiftTrackBean(GrowingIoConstant.MEMBERSHIP_PURCHASE, result.data.outTradeNo, result.data.subject, GooglePayUtils.GOOGLEPAY);
                    GrowingIOUtil.payGiftTrack(payTrackBean);
                }
            }
        }
    }

    private boolean hasBoughtVip = false;

    private void notifyFailed(final String outTradeNo) {
        DialogUtil.showConfirmDialog(getContext(), "", getString(R.string.synchronize_order_failed), getString(R.string.retry), getString(R.string.info_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Purchase pur = MomentsDataBaseAdapter.getInstance(TheLApp.getContext(), ShareFileUtils.getString(ShareFileUtils.ID, "")).getInAppOrder(outTradeNo);
                if (pur != null) {
                    RequestBusiness.getInstance().iapNotify(pur.getDeveloperPayload(), pur.getOriginalJson(), pur.getSignature()).observeOn(AndroidSchedulers.mainThread()).onBackpressureDrop().subscribeOn(Schedulers.io()).subscribe(new IapNotifyConsumer(pur.getDeveloperPayload()));
                } else {
                    startSyncOrders();
                }
            }
        }, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                startSyncOrders();
            }
        });
    }

    private void startSyncOrders() {
        SyncOrdersService.startSyncService(getContext());
    }

    private void buyItem(final String sku, final String outTradeNo, final double totalFee) {
        mHelper.launchPurchaseFlow((Activity) getContext(), sku, TheLConstants.REQUEST_CODE_ANDROID_PAY, new IabHelper.OnIabPurchaseFinishedListener() {
            @Override
            public void onIabPurchaseFinished(IabResult result, Purchase info) {
                if (result.isSuccess()) {
                    onPaid(info);
                    AppEventsLogger logger = AppEventsLogger.newLogger(getContext());
                    if (logger != null && !TextUtils.isEmpty(price_currency_code)) {//facebook记录购买成功
                        logger.logPurchase(BigDecimal.valueOf(totalFee), Currency.getInstance(price_currency_code));
                    }
                } else {
                    if (IabHelper.IABHELPER_USER_CANCELLED == result.getResponse()) {
                        DialogUtil.showToastShort(getContext(), getContext().getString(R.string.pay_canceled));
                    } else if (IabHelper.BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED == result.getResponse()) { // 已购买过该商品，但是没有消耗掉，此时需要查询本地是否有未同步的订单
                        List<String> skus = new ArrayList<>();
                        skus.add(sku);
                        mHelper.queryInventoryAsync(true, skus, new IabHelper.QueryInventoryFinishedListener() {
                            @Override
                            public void onQueryInventoryFinished(IabResult result, Inventory inv) {
                                if (result.isSuccess())
                                    mHelper.consumeAsync(inv.getPurchase(sku), new IabHelper.OnConsumeFinishedListener() {
                                        @Override
                                        public void onConsumeFinished(Purchase purchase, IabResult result) {
                                            if (result.isSuccess()) {
                                                buyItem(sku, outTradeNo, totalFee);
                                            } else {
                                                DialogUtil.showToastShort(getContext(), getContext().getString(R.string.google_play_connect_error));
                                            }
                                        }
                                    });
                                else {
                                    DialogUtil.showToastShort(getContext(), getContext().getString(R.string.google_play_connect_error) + "(" + result.getResponse() + ")");
                                }
                            }
                        });
                    } else {
                        DialogUtil.showToastShort(getContext(), getContext().getString(R.string.pay_failed) + "(" + result.getResponse() + ")");
                    }
                }
            }
        }, outTradeNo);
    }

    private void queryItems(final List<String> skuIds, final List<SuperlikeListBean.Lists> superlikeList) {
        mHelper.queryInventoryAsync(true, skuIds, new IabHelper.QueryInventoryFinishedListener() {
            @Override
            public void onQueryInventoryFinished(IabResult result, Inventory inv) {
                if (result.isSuccess()) {
                    for (SuperlikeListBean.Lists vipBean : superlikeList) {
                        SkuDetails skuDetails = inv.getSkuDetails(vipBean.iapId);
                        if (skuDetails != null) {
                            vipBean.isPendingPrice = false;
                            vipBean.googlePrice = skuDetails.getPrice();
                            try {
                                if (TextUtils.isEmpty(price_currency_code)) {//获取货币参数
                                    price_currency_code = JsonUtils.getString(new JSONObject(skuDetails.getJson()), "price_currency_code", "");
                                }
                                // google返回的price_amount_micros是*1000000，我们在这里只/10000，因为后面会再/100（为了跟国内版逻辑统一）
                                vipBean.price = (int) (JsonUtils.getLong(new JSONObject(skuDetails.getJson()), "price_amount_micros", 0) / 10000d);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        });
    }

    private void onPaid(Purchase purchase) {
        MomentsDataBaseAdapter.getInstance(TheLApp.getContext(), ShareFileUtils.getString(ShareFileUtils.ID, "")).saveInAppOrder(purchase);
        RequestBusiness.getInstance().iapNotify(purchase.getDeveloperPayload(), purchase.getOriginalJson(), purchase.getSignature()).observeOn(AndroidSchedulers.mainThread()).onBackpressureDrop().subscribeOn(Schedulers.io()).subscribe(new IapNotifyConsumer(purchase.getDeveloperPayload()));
    }

    private void setupIap(final List<SuperlikeListBean.Lists> superlikeList) {

        final List<String> skuIds = new ArrayList<>();
        for (SuperlikeListBean.Lists vipBean : superlikeList) {
            vipBean.isPendingPrice = true;
            skuIds.add(vipBean.iapId);
        }

        if (mHelper == null) {
            mHelper = new IabHelper(TheLApp.getContext(), GooglePayUtils.KEY);
            mHelper.enableDebugLogging(true);
        }
        if (!iabSetuped)
            mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
                @Override
                public void onIabSetupFinished(IabResult result) {
                    iabSetuped = result.isSuccess();
                    if (iabSetuped && mHelper != null) {
                        queryItems(skuIds, superlikeList);
                    } else {
                        DialogUtil.showToastShort(getContext(), getString(R.string.google_play_connect_error));
                    }
                }
            });
        else
            queryItems(skuIds, superlikeList);
    }

    private BuySuperlikeListener buySuperlikeListener;

    public void setBuySuperlikeListener(BuySuperlikeListener buySuperlikeListener) {
        this.buySuperlikeListener = buySuperlikeListener;
    }

    public interface BuySuperlikeListener {
        void success(int count);
    }

    // 注册广播接收器
    private void registerReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(TheLConstants.BROADCAST_PAY);
        payResultReceiver = new PayResultReceiver();
        getContext().registerReceiver(payResultReceiver, intentFilter);
    }

    // 微信支付成功接收器
    private class PayResultReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                String result = intent.getStringExtra("result");
                if (TextUtils.equals(result, "succeed")) {
                    MobclickAgent.onEvent(getContext(), "vip_pay_succeed");// 购买vip支付成功事件统计

                    L.d("showWhoSeenMeData", " onReceive hasBoughtVip : " + hasBoughtVip);

                    hasBoughtVip = true;
                    DialogUtil.showToastShort(TheLApp.getContext(), getString(R.string.buy_superlike_succeed));
                    if (buySuperlikeListener != null) {
                        buySuperlikeListener.success(list.get(selectbuyPos).count);
                    }
                    try {
                        BuySuperlikeDialog.this.dismiss();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
