package com.thel.ui.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import androidx.interpolator.view.animation.LinearOutSlowInInterpolator;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.utils.SizeUtils;

/**
 * @author liuyun
 * @date 2018/5/21
 */
public class ReleaseMomentViewGroup extends ViewGroup {

    public static final int GOTO_ALBUM = 0;

    public static final int GOTO_VIDEO = 1;

    public static final int GOTO_TEXT = 2;

    public static final int GOTO_TOPIC = 3;

    public static final int GOTO_LIVE = 4;

    public static final int GOTO_LOVER = 5;

    private static final long DELAY = 30;

    private static final long DURATION = 220;

    private int itemHeight = SizeUtils.dip2px(TheLApp.context, 130);

    private int[] imgRes = {R.mipmap.btn_post_photo, R.mipmap.btn_post_video, R.mipmap.btn_post_text, R.mipmap.btn_post_topic, R.mipmap.btn_post_live, R.mipmap.btn_post_couple};

    private int[] titleRes = {R.string.chat_activity_photo, R.string.info_video, R.string.info_text, R.string.info_topic, R.string.live, R.string.memorial_day};

    public ReleaseMomentViewGroup(Context context) {
        super(context);
        init();
    }

    public ReleaseMomentViewGroup(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ReleaseMomentViewGroup(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        measureChildren(widthMeasureSpec, widthMeasureSpec);

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

    }

    @Override
    public LayoutParams generateLayoutParams(AttributeSet attrs) {
        return new MarginLayoutParams(getContext(), attrs);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        final int childCount = getChildCount();
        int itemWidth = getMeasuredWidth() / 3;
        int parentViewHeight = getMeasuredHeight();
        int marginTop = parentViewHeight / 2 - itemHeight;

        for (int i = 0; i < childCount; i++) {
            View view = getChildAt(i);
            int j;
            if (i > 2) {
                j = 1;
            } else {
                j = 0;
            }
            view.layout(i % 3 * itemWidth, itemHeight * j + marginTop, (i % 3 + 1) * itemWidth, itemHeight * (j + 1) + marginTop);
        }


    }

    private void init() {
        for (int i = 0; i < imgRes.length; i++) {
            View childView = LayoutInflater.from(getContext()).inflate(R.layout.item_release, null);
            childView.setOnClickListener(mOnClickListener);
            ImageView imageView = childView.findViewById(R.id.icon_iv);
            imageView.setImageResource(imgRes[i]);
            TextView title_tv = childView.findViewById(R.id.title_tv);
            title_tv.setText(titleRes[i]);
            childView.setTag(i);
            addView(childView);
        }
    }

    boolean isIn = true;

    public void in() {

        isIn = true;

        int childCount = getChildCount();

        long delay = 0;

        for (int i = 0; i < childCount; i++) {
            delay += DELAY;
            final View view = getChildAt(i);
            postDelayed(new Runnable() {
                @Override public void run() {
                    startAnimator(view);
                }
            }, delay);
        }

    }

    public void out() {

        isIn = false;

        int childCount = getChildCount();

        long delay = 0;

        for (int i = childCount - 1; i >= 0; i--) {
            delay += DELAY;
            final View view = getChildAt(i);
            postDelayed(new Runnable() {
                @Override public void run() {
                    endAnimator(view);
                }
            }, delay);
        }

    }

    private void startAnimator(final View view) {
        int startY = getMeasuredHeight() + view.getHeight();
        int endY = view.getTop();
        ValueAnimator valueAnimator = ValueAnimator.ofFloat(startY, endY);
        valueAnimator.setInterpolator(new LinearOutSlowInInterpolator());
        valueAnimator.setDuration(DURATION);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override public void onAnimationUpdate(ValueAnimator animation) {
                float y = (float) animation.getAnimatedValue();
                view.setY(y);
            }
        });
        valueAnimator.addListener(new AnimatorListenerAdapter() {

            @Override public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                view.setVisibility(VISIBLE);
                view.setLayerType(LAYER_TYPE_HARDWARE, null);
            }

            @Override public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                view.setLayerType(LAYER_TYPE_NONE, null);
                int tag = (int) view.getTag();
                if (tag == getChildCount() - 1) {
                    if (mOnFinishListener != null) {
                        mOnFinishListener.onShown();
                    }
                }
            }
        });
        valueAnimator.start();
    }

    private void endAnimator(final View view) {
        int endY = getMeasuredHeight() + view.getHeight();
        int startY = view.getTop();
        ValueAnimator valueAnimator = ValueAnimator.ofFloat(startY, endY);
        valueAnimator.setInterpolator(new AccelerateInterpolator());
        valueAnimator.setDuration(DURATION);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override public void onAnimationUpdate(ValueAnimator animation) {
                float y = (float) animation.getAnimatedValue();
                view.setY(y);
            }
        });
        valueAnimator.addListener(new AnimatorListenerAdapter() {
            @Override public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                view.setVisibility(VISIBLE);
                view.setLayerType(LAYER_TYPE_SOFTWARE, null);

            }

            @Override public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                view.setLayerType(LAYER_TYPE_NONE, null);
                int tag = (int) view.getTag();
                if (tag == 0) {
                    if (mOnFinishListener != null) {
                        mOnFinishListener.onFinish();
                    }
                }
            }
        });
        valueAnimator.start();
    }

    private OnFinishListener mOnFinishListener;

    public void setOnFinishListener(OnFinishListener mOnFinishListener) {
        this.mOnFinishListener = mOnFinishListener;
    }

    private OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(OnItemClickListener mOnItemClickListener) {
        this.mOnItemClickListener = mOnItemClickListener;
    }

    public interface OnFinishListener {
        void onFinish();

        void onShown();
    }

    public interface OnItemClickListener {
        void onItemClick(int tag, View view);
    }

    private OnClickListener mOnClickListener = new OnClickListener() {
        @Override public void onClick(View v) {
            int tag = (int) v.getTag();
            if (mOnItemClickListener != null) {
                mOnItemClickListener.onItemClick(tag, v);
            }
        }
    };

}
