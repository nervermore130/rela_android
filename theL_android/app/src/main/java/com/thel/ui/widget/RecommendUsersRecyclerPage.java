package com.thel.ui.widget;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.RecommendListBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.imp.follow.FollowStatusChangedImpl;
import com.thel.modules.main.home.RecommendUsersActivity;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.ui.widget.popupwindow.UnFollowPopupWindow;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;
import com.thel.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lingwei on 2019/3/19.
 */

public class RecommendUsersRecyclerPage extends LinearLayout {

    private Context mContext;
    private RecyclerView mRecyclerview;
    private ArrayList<RecommendListBean.GuideDataBean> list = new ArrayList<>();

    private LinearLayoutManager manager;
    private RecommendUsersAdapter adapter;
    private TextView follow_statue_view;
    private TextView tv_all_recommend;
    private View ll_root;

    public RecommendUsersRecyclerPage(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RecommendUsersRecyclerPage(Context context) {
        this(context, null);
    }

    public RecommendUsersRecyclerPage(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }

    private void init() {
        inflate(mContext, R.layout.fragment_recycler_recommend_users, this);
      /*  ll_root = findViewById(R.id.ll_root);
        ll_root.setVisibility(GONE);*/
        tv_all_recommend = findViewById(R.id.tv_all_recommend);
        mRecyclerview = findViewById(R.id.recommend_recyclerView);
        follow_statue_view = findViewById(R.id.follow_statue_view);
        manager = new LinearLayoutManager(mContext);
        manager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mRecyclerview.setHasFixedSize(true);
        mRecyclerview.setLayoutManager(manager);
        adapter = new RecommendUsersAdapter(list);
        mRecyclerview.setAdapter(adapter);

        setContentVisiable(GONE);
        setLinstener();
    }

    private void setLinstener() {
        tv_all_recommend.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, RecommendUsersActivity.class);
                mContext.startActivity(intent);
            }
        });
       /* adapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (list.get(position).type.equals("live")) {
                    gotoLiveShowFragmentActivity(position);
                } else if (list.get(position).type.equals("activity")) {
                    MsgBean msgBean = new MsgBean();
                    msgBean.messageTo = list.get(position).messageTo;
                    msgBean.advertUrl = list.get(position).dumpURL;
                    msgBean.advertTitle = list.get(position).advertTitle;
                    msgBean.userId = list.get(position).userId;
                    msgBean.userName = list.get(position).nickName;
                    msgBean.avatar = list.get(position).avatar;
                    Intent intent = new Intent();
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    PushUtils.intentActivity(msgBean, intent);
                    if (intent.resolveActivity(mContext.getPackageManager()) != null) {
                        mContext.startActivity(intent);
                    }
                }
            }
        });*/
    }

    /**
     * 跳转到直播LivefragmentActivity页面
     */

    public RecommendUsersRecyclerPage initView(List<RecommendListBean.GuideDataBean> recommendList) {

        if (recommendList != null) {

            this.list.clear();
            for (int i = 0; i < recommendList.size(); i++) {

                if (!Utils.getMyUserId().equals(recommendList.get(i).userId + "")) {
                    this.list.add(recommendList.get(i));
                }
            }
            adapter.setNewData(list);
            if (list.size() <= 0) {
                setContentVisiable(GONE);
            } else {
                setContentVisiable(VISIBLE);
            }
        }
        return this;
    }

    public void setContentVisiable(int visiable) {
        mRecyclerview.setVisibility(visiable);
    }


    public class RecommendUsersAdapter extends BaseRecyclerViewAdapter<RecommendListBean.GuideDataBean> {

        public Context context;


        public RecommendUsersAdapter(ArrayList<RecommendListBean.GuideDataBean> data) {
            super(R.layout.recommend_user_item, data);
        }

        @Override
        protected void convert(BaseViewHolder helper, RecommendListBean.GuideDataBean item) {
            helper.setImageUrl(R.id.img_thumb_live, item.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE);
            helper.setText(R.id.tv_name, item.nickName);
            helper.setVisibility(R.id.recommend_verify_iv, GONE);
            TextView content = helper.getView(R.id.tv_verify_intro);

            if (!TextUtils.isEmpty(item.recommendReason) || !TextUtils.isEmpty(item.verifyIntro) || !TextUtils.isEmpty(item.intro)) {

                if (!TextUtils.isEmpty(item.recommendReason)) {
                    content.setText(item.recommendReason);
                } else {
                    if (!TextUtils.isEmpty(item.verifyIntro)) {
                        if (item.verifyType == 1) {
                            helper.setVisibility(R.id.recommend_verify_iv, VISIBLE);

                        }
                        content.setText(item.verifyIntro);

                    } else {

                        if (!TextUtils.isEmpty(item.intro)) {
                            content.setText(item.intro);

                        }

                    }
                }

            } else {
                content.setText("");

            }


            TextView followView = helper.getView(R.id.follow_statue_view);
            String text = "";

            switch (item.followStatus) {
                case 0:
                    text = TheLApp.getContext().getString(R.string.userinfo_activity_follow);
                    followView.setTextColor(TheLApp.getContext().getResources().getColor(R.color.text_selector));
                    followView.setBackgroundResource(R.drawable.follow_selector);

                    break;
                case 1:
                    text = TheLApp.getContext().getString(R.string.userinfo_activity_followed);
                    followView.setBackgroundResource(R.drawable.follow_unselector);
                    followView.setTextColor(TheLApp.getContext().getResources().getColor(R.color.text_unselector));

                    break;
                case 2:
                    text = TheLApp.getContext().getString(R.string.repowder);
                    followView.setBackgroundResource(R.drawable.follow_selector);
                    followView.setTextColor(TheLApp.getContext().getResources().getColor(R.color.text_selector));

                    break;
                case 3:
                    text = TheLApp.getContext().getString(R.string.interrelated);
                    followView.setBackgroundResource(R.drawable.follow_unselector);
                    followView.setTextColor(TheLApp.getContext().getResources().getColor(R.color.text_unselector));
                    break;
            }
            followView.setText(text);


            followView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    switch (item.followStatus) {
                        case 0:
                            FollowStatusChangedImpl.followUser(item.userId + "", FollowStatusChangedImpl.ACTION_TYPE_FOLLOW, item.nickName, item.avatar);
                            item.followStatus = 1;
                            notifyDataSetChanged();
                            break;
                        case 2:
                            FollowStatusChangedImpl.followUser(item.userId + "", FollowStatusChangedImpl.ACTION_TYPE_FOLLOW, item.nickName, item.avatar);
                            item.followStatus = 3;
                            notifyDataSetChanged();
                            break;

                        case 1:
                        case 3:
                            UnFollowPopupWindow mUnFollowPopupWindow = new UnFollowPopupWindow(mContext, item.nickName);
                            mUnFollowPopupWindow.setOnUnFollowListener(new UnFollowPopupWindow.OnUnFollowListener() {
                                @Override
                                public void onUnFollow() {
                                    mData.remove(helper.getAdapterPosition() - getHeaderLayoutCount());
                                    notifyItemRemoved(helper.getAdapterPosition());
                                    FollowStatusChangedImpl.followUser(item.userId + "", FollowStatusChangedImpl.ACTION_TYPE_CANCEL_FOLLOW, item.nickName, item.avatar);
                                }
                            });
                            mUnFollowPopupWindow.showAtLocation(v, Gravity.BOTTOM, 0, 0);
                            break;
                    }
                }
            });
            helper.setOnClickListener(R.id.img_thumb_live, new OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Intent intent = new Intent(mContext, UserInfoActivity.class);
//                    intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, item.userId + "");
//                    mContext.startActivity(intent);
                    FlutterRouterConfig.Companion.gotoUserInfo(item.userId+"");
                }
            });


        }


    }

   /* public RecommendUsersAdapter.OnFollowStatusListener mOnFollowStatusListener;

    public void setOnFollowStatusListener(RecommendUsersAdapter.OnFollowStatusListener mOnFollowStatusListener) {
        this.mOnFollowStatusListener = mOnFollowStatusListener;
    }

    public interface OnFollowStatusListener {
        void OnFollowStatus();
    }*/
}
