package com.thel.ui.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.AccelerateDecelerateInterpolator;

import com.thel.R;
import com.thel.utils.L;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by liuyun on 2017/12/12.
 */

public class CountDownView extends androidx.appcompat.widget.AppCompatImageView {

    private int count = 3;

    private boolean isLinkMic = true;

    private boolean isStart = false;

    private List<ValueAnimator> animators = new ArrayList<>();

    public CountDownView(Context context) {
        super(context);
    }

    public CountDownView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CountDownView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void start() {
        if (!isStart) {
            countDown();
            isStart = true;
        }

    }

    public void stop() {
        if (animators != null && animators.size() > 0) {
            for (int i = 0; i < animators.size(); i++) {
                ValueAnimator valueAnimator = animators.get(i);
                valueAnimator.cancel();
            }
            animators.clear();
            animators = null;
        }
    }

    public void isLinkMic(boolean isLinkMic) {

        this.isLinkMic = isLinkMic;

        int width = dip2px(getContext(), 60);

        if (isLinkMic) {
            setImageResource(R.mipmap.icon_count_down_blue_three);
        } else {
            setImageResource(R.mipmap.icon_count_down_red_three);
        }

        setPivotX(width / 2);

        setPivotY(width / 2);

    }

    private void countDown() {

        ValueAnimator mValueAnimator = ValueAnimator.ofFloat(0, 1, 0);

        animators.add(mValueAnimator);

        mValueAnimator.setInterpolator(new AccelerateDecelerateInterpolator());

        mValueAnimator.setDuration(1000);

        mValueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float scale = (float) animation.getAnimatedValue();
                setScaleX(scale);
                setScaleY(scale);

            }

        });

        mValueAnimator.addListener(new AnimatorListenerAdapter() {

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);

                count--;

                if (count == 3) {
                    if (isLinkMic) {
                        setImageResource(R.mipmap.icon_count_down_blue_three);
                    } else {
                        setImageResource(R.mipmap.icon_count_down_red_three);
                    }
                } else if (count == 2) {
                    if (isLinkMic) {
                        setImageResource(R.mipmap.icon_count_down_blue_two);
                    } else {
                        setImageResource(R.mipmap.icon_count_down_red_two);
                    }
                } else if (count == 1) {
                    if (isLinkMic) {
                        setImageResource(R.mipmap.icon_count_down_blue_one);
                    } else {
                        setImageResource(R.mipmap.icon_count_down_red_one);
                    }
                }

                L.d("CountDownView", " count : " + count);

                if (count > 0) {
                    countDown();
                } else {
                    if (mCountDownListener != null) {
                        mCountDownListener.onCountDown();
                    }
                }
            }
        });
        mValueAnimator.start();
    }

    public static int dip2px(Context context, float dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }

    private CountDownListener mCountDownListener;

    public void setCountDownListener(CountDownListener mCountDownListener) {
        this.mCountDownListener = mCountDownListener;
    }

    public interface CountDownListener {
        void onCountDown();
    }

}
