package com.thel.ui.popupwindow;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseDialogFragment;
import com.thel.ui.widget.GuideView;

import video.com.relavideolibrary.Utils.DensityUtils;


/**
 * @author lingwei
 */
public class BeautyAndTextDialogView extends BaseDialogFragment {

    private static final String TAG = "BeautyAndTextPopupWindow";
    public static final int TYPE_VIDEO = 0x03;

    public static final int TYPE_MULTI_VOICE = 0x04;

    public static final int MEET_DEFAULT = 0;//

    public static final int MEETING = 1;//1相遇中

    public static final int MEET_INTERVAL = 2;//2相遇冷却
    private Context mContext;

    private View.OnClickListener beautyListener;
    private View.OnClickListener textSizeListener;
    private LinearLayout beauty_ll;
    private LinearLayout text_ll;
    private ImageView img_beauty;
    private RelativeLayout rl_live_more;
    private int tag;
    private TextView txt_set_beauty;
    private View.OnClickListener meetionListener;
    private int meetionStatus = MEET_DEFAULT;
    private GuideView mGuideView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.live_beauty_text_layout, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setListener();

    }

    @Override
    public void onStart() {
        super.onStart();

        //得到dialog对应的window
        Window window = getDialog().getWindow();
        if (window != null) {
            //得到LayoutParams
            WindowManager.LayoutParams params = window.getAttributes();

            //修改gravity
            params.gravity = Gravity.BOTTOM;
            params.height = DensityUtils.dp2px(260);
            params.width = WindowManager.LayoutParams.MATCH_PARENT;
            window.setAttributes(params);
        }
    }

    private void init(View view) {
        text_ll = view.findViewById(R.id.ll_set_text);
        beauty_ll = view.findViewById(R.id.ll_set_beauty);
        img_beauty = view.findViewById(R.id.img_beauty);
        rl_live_more = view.findViewById(R.id.rl_live_more);
        txt_set_beauty = view.findViewById(R.id.txt_set_beauty);
        mGuideView = view.findViewById(R.id.guide_view);
        if (TYPE_VIDEO == tag) {
            img_beauty.setImageResource(R.mipmap.icon_box_beauty);
            txt_set_beauty.setText(TheLApp.context.getString(R.string.beautiful));
        } else {
            setMeetionStatus(meetionStatus, "");
        }

    }

    public void setTag(int tag) {
        this.tag = tag;
    }

    public void setBeautyStyle(boolean switchBeauty) {
        if (switchBeauty) {
            img_beauty.setImageResource(R.mipmap.btn_beauty_press);

        } else {
            img_beauty.setImageResource(R.mipmap.btn_beauty_normal);

        }
    }

    public void setMeetionTips(String tips) {
        mGuideView.show(img_beauty, tips);
    }

    /**
     * 0正常 1相遇中 2相遇冷却
     *
     * @param status
     */
    public void setMeetionStatus(int status, String text) {
        this.meetionStatus = status;
        if (img_beauty == null || txt_set_beauty == null) return;
        if (status == MEETING) {
            img_beauty.setImageResource(R.mipmap.icon_box_xiangyu);
            txt_set_beauty.setText(TheLApp.context.getString(R.string.meetioning));
        } else if (status == MEET_INTERVAL) {
            img_beauty.setImageResource(R.mipmap.icon_box_xiangyu_cd);
            if (TextUtils.isEmpty(text)) {
                txt_set_beauty.setText(TheLApp.context.getString(R.string.meetioning));
            } else {
                txt_set_beauty.setText(text);
            }
        } else {
            img_beauty.setImageResource(R.mipmap.icon_box_xiangyu);
            txt_set_beauty.setText(TheLApp.context.getString(R.string.meet_start));
        }
    }

    private void setListener() {

        if (beautyListener != null) {
            beauty_ll.setOnClickListener(beautyListener);
        }

        if (meetionListener != null) {
            beauty_ll.setOnClickListener(meetionListener);

        }

        if (textSizeListener != null) {
            text_ll.setOnClickListener(textSizeListener);
        }
        rl_live_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

    }


    /**
     * 点击美颜的开关
     */
    public void setBeautyListener(View.OnClickListener beautyListener) {
        this.beautyListener = beautyListener;
    }

    /**
     * 点击文字大小的开关
     */
    public void setTextSizeListener(View.OnClickListener textSizeListener) {
        this.textSizeListener = textSizeListener;
    }

    /**
     * 点击相遇的开关
     */
    public void setMeetionListener(View.OnClickListener meetionListener) {
        this.meetionListener = meetionListener;
    }


}
