package com.thel.ui.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;

import com.thel.R;
import com.thel.utils.L;

/**
 * Created by chad
 * Time 18/11/21
 * Email: wuxianchuang@foxmail.com
 * Description: TODO 年龄设置
 */
public class RangeSeekBar extends View {
    private float lineWidth;

    private int inRangeColor;
    private int outRangeColor;

    private int bmpWidth;
    private int bmpHeight;

    private Bitmap lowerBmp;
    private Bitmap upperBmp;

    private boolean isLowerMoving = false;
    private boolean isUpperMoving = false;

    private OnRangeChangedListener onRangeChangedListener;

    private int lineHeight;
    private int lineEnd;
    private int lowerCenterX;
    private int upperCenterX = 500;

    private float smallValue = 18.0f;//最小值
    private float bigValue = 41.0f;//最大值

    private float smallRange = smallValue;
    private float bigRange = bigValue;
    private Paint linePaint;
    private Paint bmpPaint;

    private int tempLeftX = 8;
    private int tempRightX = 13;

    public RangeSeekBar(Context context) {
        super(context);
        init();
    }

    public RangeSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RangeSeekBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        lowerBmp = BitmapFactory.decodeResource(getResources(),
                R.mipmap.slider_near_age);
        upperBmp = BitmapFactory.decodeResource(getResources(),
                R.mipmap.slider_near_age);

        bmpWidth = upperBmp.getWidth();
        bmpHeight = upperBmp.getHeight();
        lowerCenterX = upperBmp.getWidth() / 2;

        lineWidth = dp2px(getContext(), 2);

        inRangeColor = getContext().getResources().getColor(R.color.rela_color);
        outRangeColor = getContext().getResources().getColor(R.color.black_alpha_60);

        // 画线
        linePaint = new Paint();
        linePaint.setAntiAlias(true);
        linePaint.setStrokeWidth(lineWidth);

        // 画图片滑块
        bmpPaint = new Paint();
    }

    public void setRange(float small, float big) {

        post(() -> {
            L.d("RangeSeekBar", "small-->" + small + "\nbig-->" + big);

            lowerCenterX = (int) ((small - smallValue + 1) * getMeasuredWidth() / (bigValue - smallValue));

            upperCenterX = (int) ((big - smallValue + 1) * getMeasuredWidth() / (bigValue - smallValue));

            L.d("RangeSeekBar", "lowerCenterX-->" + lowerCenterX + "\nupperCenterX-->" + upperCenterX);

            postInvalidate();
        });
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);

        int widthSize = MeasureSpec.getSize(widthMeasureSpec);

        int heightMode = MeasureSpec.getMode(heightMeasureSpec);

        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        int measuredHeight, measuredWidth;

        if (widthMode == MeasureSpec.EXACTLY) {

            measuredWidth = widthSize;

        } else {

            measuredWidth = 200;

        }

        if (heightMode == MeasureSpec.EXACTLY) {

            measuredHeight = heightSize;

        } else {

            measuredHeight = 20;

        }

        upperCenterX = measuredWidth - measuredHeight / 2;

        setMeasuredDimension(measuredWidth, measuredHeight);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        lineEnd = getWidth();

        lineHeight = getHeight() - lowerBmp.getHeight() / 2;

        // 绘制处于图片滑块之间线段
        linePaint.setColor(inRangeColor);
        canvas.drawLine(lowerCenterX, lineHeight, upperCenterX, lineHeight,
                linePaint);

        // 绘制处于图片滑块两端的线段
        linePaint.setColor(outRangeColor);
        canvas.drawLine(0, lineHeight, lowerCenterX, lineHeight,
                linePaint);
        canvas.drawLine(upperCenterX, lineHeight, lineEnd, lineHeight,
                linePaint);

        canvas.drawBitmap(lowerBmp, lowerCenterX - bmpWidth / 2 - tempLeftX, lineHeight
                - bmpHeight / 2, bmpPaint);
        canvas.drawBitmap(lowerBmp, upperCenterX - bmpWidth / 2 + tempRightX, lineHeight
                - bmpHeight / 2, bmpPaint);

        updateRange();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        super.onTouchEvent(event);

        float xPos = event.getX();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                // 如果按下的位置在垂直方向没有与图片接触，则不会滑动滑块
                float yPos = event.getY();
                if (Math.abs(yPos - lineHeight) > bmpHeight / 2) {
                    getParent().requestDisallowInterceptTouchEvent(false);
                    return false;
                }

                // 表示当前按下的滑块是左边的滑块
                if (Math.abs(xPos - lowerCenterX) < bmpWidth / 2) {
                    isLowerMoving = true;
                }

                // //表示当前按下的滑块是右边的滑块
                if (Math.abs(xPos - upperCenterX) < bmpWidth / 2) {
                    isUpperMoving = true;
                }
                break;
            case MotionEvent.ACTION_MOVE:
                // 滑动左边滑块时
                if (isLowerMoving) {
                    if (xPos > bmpWidth / 2 - tempLeftX + 3 && xPos < upperCenterX - bmpWidth) {
                        lowerCenterX = (int) xPos;
                        postInvalidate();
                    }
                }

                // 滑动右边滑块时
                if (isUpperMoving) {
                    if (xPos > lowerCenterX + bmpWidth && xPos < lineEnd - bmpWidth / 2 + tempRightX - 3) {
                        upperCenterX = (int) xPos;
                        postInvalidate();
                    }
                }

                break;
            case MotionEvent.ACTION_UP:
                // 修改滑块的滑动状态为不再滑动
                isLowerMoving = false;
                isUpperMoving = false;
                break;
            default:
                break;
        }
        getParent().requestDisallowInterceptTouchEvent(true);
        return true;
    }

    // 计算指定滑块对应的范围值
    private float computRange(float position) {
        return (position) * (bigValue - smallValue) / getMeasuredWidth()
                + smallValue;
    }

    // 滑动滑块的过程中，更新滑块上方的范围标识
    private void updateRange() {
        smallRange = computRange(lowerCenterX);
        bigRange = computRange(upperCenterX);

        if (null != onRangeChangedListener) {
            onRangeChangedListener.onRangeChanged((int) smallRange, (int) bigRange);
        }
    }

    private int dp2px(Context context, float dpVal) {
        return (int) TypedValue.applyDimension(1, dpVal, context.getResources().getDisplayMetrics());
    }

    // 注册滑块范围值改变事件的监听
    public void setOnRangeChangedListener(
            OnRangeChangedListener onRangeChangedListener) {
        this.onRangeChangedListener = onRangeChangedListener;
    }

    // 公共接口，用户回调接口范围值的改变
    public interface OnRangeChangedListener {

        void onRangeChanged(int lowerRange, int upperRange);

    }
}
