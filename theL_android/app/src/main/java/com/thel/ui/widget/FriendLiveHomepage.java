package com.thel.ui.widget;

import android.content.Context;
import android.content.Intent;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import com.facebook.drawee.view.SimpleDraweeView;
import com.thel.R;
import com.thel.bean.live.LiveFollowingUsersBean;
import com.thel.constants.TheLConstants;
import com.thel.modules.live.bean.LiveFollowerUsersBean;
import com.thel.modules.live.surface.watch.LiveWatchActivity;
import com.thel.modules.live.surface.watch.horizontal.LiveWatchHorizontalActivity;
import com.thel.modules.main.messages.bean.MsgBean;
import com.thel.modules.main.messages.utils.PushUtils;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;
import com.thel.utils.L;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by test1 on 2017/4/14.
 */

public class FriendLiveHomepage extends LinearLayout {
    private static final String TAG = "FriendLiveHomepage";

    private Context mContext;
    private RecyclerView mRecyclerview;
    private ArrayList<LiveFollowerUsersBean> list = new ArrayList<>();

    private LinearLayoutManager manager;
    private AttentionLiveAdapter adapter;

    public FriendLiveHomepage(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FriendLiveHomepage(Context context) {
        this(context, null);
    }

    public FriendLiveHomepage(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }

    private void init() {
        inflate(mContext, R.layout.attention_friend_live, this);
        mRecyclerview = findViewById(R.id.recyclerview_head);
        manager = new LinearLayoutManager(mContext);
        manager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mRecyclerview.setHasFixedSize(true);
        mRecyclerview.setLayoutManager(manager);
        adapter = new AttentionLiveAdapter(list);
        mRecyclerview.setAdapter(adapter);

        setContentVisiable(GONE);
        setLinstener();
    }

    private void setLinstener() {
        adapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                LiveFollowerUsersBean liveFollowerUsersBean = list.get(position);

                L.d(TAG," liveFollowerUsersBean type : " + liveFollowerUsersBean.type);

                switch (liveFollowerUsersBean.type) {
                    case "live":
                        gotoLiveShowFragmentActivity(position);
                        break;
                    case "activity":
                        MsgBean msgBean = new MsgBean();
                        msgBean.messageTo = liveFollowerUsersBean.messageTo;
                        msgBean.advertUrl = liveFollowerUsersBean.dumpURL;
                        msgBean.advertTitle = liveFollowerUsersBean.advertTitle;
                        msgBean.userId = liveFollowerUsersBean.userId;
                        msgBean.userName = liveFollowerUsersBean.nickName;
                        msgBean.avatar = liveFollowerUsersBean.avatar;
                        Intent intent = new Intent();
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        PushUtils.intentActivity(msgBean, intent);
                        if (intent.resolveActivity(mContext.getPackageManager()) != null) {
                            mContext.startActivity(intent);
                        }
                        break;
                    case "guest":
                        gotoLiveGuest(liveFollowerUsersBean);
                        break;
                    default:
                        break;
                }
            }
        });
    }

    /**
     * 跳转到热聊中的直播间
     */
    private void gotoLiveGuest(LiveFollowerUsersBean followerUsersBean) {
        Intent intent = new Intent(mContext, LiveWatchActivity.class);
        intent.putExtra(TheLConstants.BUNDLE_KEY_ID, followerUsersBean.roomId);
        intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, followerUsersBean.userId);
        intent.putExtra(LiveWatchActivity.FROM_PAGE, LiveWatchActivity.FROM_PAGE_WEB);
        mContext.startActivity(intent);
    }

    /**
     * 跳转到直播LivefragmentActivity页面
     */
    private void gotoLiveShowFragmentActivity(int position) {

        LiveFollowerUsersBean liveFollowerUsersBean = list.get(position);

        if (liveFollowerUsersBean.isLandscape == 1) {
            Intent intent = new Intent(mContext, LiveWatchHorizontalActivity.class);
            intent.putExtra(LiveWatchActivity.FROM_PAGE, LiveWatchActivity.FROM_PAGE_FRIEND_LIVE_HOME_PAGE);
            intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, liveFollowerUsersBean.userId + "");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(intent);
        } else {

            ArrayList<LiveFollowerUsersBean> cacheList = new ArrayList<>(list);

            ListIterator<LiveFollowerUsersBean> iterator = cacheList.listIterator();

            while (iterator.hasNext()) {
                LiveFollowerUsersBean lfub = iterator.next();
                if (lfub.isLandscape == 1) {
                    if (position < iterator.nextIndex()) {
                        position--;
                    }
                    iterator.remove();
                }
            }

            Intent intent = new Intent(mContext, LiveWatchActivity.class);
            intent.putExtra(LiveWatchActivity.FROM_PAGE, LiveWatchActivity.FROM_PAGE_FRIEND_LIVE_HOME_PAGE);
            intent.putExtra(TheLConstants.BUNDLE_KEY_LIVE_FOLLOWER_USER_BEAN_LIST, cacheList);
            intent.putExtra(TheLConstants.BUNDLE_KEY_POSITION, position);
            intent.putExtra(TheLConstants.BUNDLE_KEY_VIDEO_PLAYING, 1);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(intent);
        }


    }

    public FriendLiveHomepage initView(LiveFollowingUsersBean bean) {

        if (bean != null) {

            this.list.clear();
            this.list.addAll(bean.list);
            adapter.setNewData(list);
            if (list.size() <= 0) {
                setContentVisiable(GONE);
            } else {
                setContentVisiable(VISIBLE);
            }
        }
        return this;
    }

    public void setContentVisiable(int visiable) {
        mRecyclerview.setVisibility(visiable);
    }

    class AttentionLiveAdapter extends BaseRecyclerViewAdapter<LiveFollowerUsersBean> {

        public AttentionLiveAdapter(List<LiveFollowerUsersBean> data) {
            super(R.layout.attention_firend_item, data);
        }

        @Override
        protected void convert(BaseViewHolder helper, LiveFollowerUsersBean item) {
            helper.setImageUrl(R.id.img_thumb_live, item.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE);
            helper.setText(R.id.moment_name, item.nickName);
            SimpleDraweeView thumb_live = helper.convertView.findViewById(R.id.img_thumb_live);
            final int viewNum = item.count;
//            if (item.type.equals("live")) {
//                helper.setVisibility(R.id.ll_show_moment_watch, VISIBLE);
//            } else if (item.type.equals("activity")) {
//                helper.setVisibility(R.id.ll_show_moment_watch, INVISIBLE);
//            }
            helper.setVisibility(R.id.ll_show_moment_watch, GONE);
            if (viewNum > 0) {
                helper.setText(R.id.moment_watch, viewNum + "");
            } else {
                helper.setText(R.id.moment_watch, "0");

            }

            helper.getView(R.id.iv_live_type).setVisibility(VISIBLE);

            switch (item.audioType) {
                case 0:
                    helper.setImageResource(R.id.iv_live_type, R.mipmap.followpage_live_icn_video);
                    break;
                case 1:
                    helper.setImageResource(R.id.iv_live_type, R.mipmap.followpage_live_icn_chat);
                    break;
            }

            if (item.type.equals("guest")) {
                switch (item.audioType) {

                    case 1://连麦中
                        helper.setImageResource(R.id.iv_live_type, R.mipmap.followpage_live_icn_lianmai);
                        helper.setText(R.id.moment_watch, R.string.connecting_the_microphone);
                        break;
                    case 2: //上麦中
                        helper.setImageResource(R.id.iv_live_type, R.mipmap.followpage_live_icn_sit);
                        helper.setText(R.id.moment_watch, R.string.live_chat);

                        break;
                }
            } else if (item.type.equals("live")) {
                switch (item.audioType) {

                    case 1:
                        helper.setImageResource(R.id.iv_live_type, R.mipmap.followpage_live_icn_fm);

                        break;
                    case 2:
                        helper.setImageResource(R.id.iv_live_type, R.mipmap.followpage_live_icn_fm);

                        break;

                }
            }else{
                helper.getView(R.id.iv_live_type).setVisibility(GONE);
            }


            // thumb_live.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadius(16).setRoundingMethod(RoundingParams.RoundingMethod.OVERLAY_COLOR).setOverlayColor(ContextCompat.getColor(TheLApp.getContext(), R.color.white)));

        }
    }
}
