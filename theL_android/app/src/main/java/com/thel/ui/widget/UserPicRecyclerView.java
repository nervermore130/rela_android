package com.thel.ui.widget;

import android.content.Context;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.util.AttributeSet;

import com.thel.app.TheLApp;
import com.thel.utils.SizeUtils;

public class UserPicRecyclerView extends RecyclerView {

    private int picCount = 1;

    private int space = SizeUtils.dip2px(TheLApp.context, 2);

    public UserPicRecyclerView(Context context) {
        super(context);
    }

    public UserPicRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public UserPicRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override protected void onMeasure(int widthSpec, int heightSpec) {
        super.onMeasure(widthSpec, heightSpec);

        int height = getMeasuredWidth() / 3;

        if (picCount > 3 && picCount < 7) {
            height = height * 2 + space;
        }

        if (picCount >= 7) {
            height = height * 3 + space * 2;
        }

        setMeasuredDimension(getMeasuredWidth(), getMeasuredHeight());
    }

    public void setPicCount(int picCount, int space) {
        this.picCount = picCount;
        this.space = space;
        requestLayout();
    }
}
