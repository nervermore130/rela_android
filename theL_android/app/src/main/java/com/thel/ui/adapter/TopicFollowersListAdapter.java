package com.thel.ui.adapter;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.topic.TopicFollowerBean;
import com.thel.constants.TheLConstants;
import com.thel.utils.ImageUtils;

import java.util.ArrayList;
import java.util.Arrays;

public class TopicFollowersListAdapter extends BaseAdapter {

    private ArrayList<TopicFollowerBean> friendslist = new ArrayList<>();

    private LayoutInflater mInflater;

    public TopicFollowersListAdapter(ArrayList<TopicFollowerBean> friendslist) {
        mInflater = (LayoutInflater) TheLApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        freshAdapter(friendslist);
    }

    public void freshAdapter(ArrayList<TopicFollowerBean> friendslist) {

        this.friendslist.clear();
        this.friendslist.addAll(friendslist);

    }

    @Override
    public int getCount() {
        return friendslist.size();
    }

    @Override
    public Object getItem(int position) {
        return friendslist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        HoldView holdView = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.common_friend_list_item, parent, false);
            holdView = new HoldView();

            holdView.txt_distance = convertView.findViewById(R.id.txt_distance);
            holdView.img_thumb = convertView.findViewById(R.id.img_thumb);
            holdView.img_role = convertView.findViewById(R.id.img_role);
            holdView.txt_name = convertView.findViewById(R.id.txt_name);
            holdView.txt_desc = convertView.findViewById(R.id.txt_desc);
            holdView.img_indentify = convertView.findViewById(R.id.img_indentify);
            holdView.line2 = convertView.findViewById(R.id.line2);
            convertView.setTag(holdView); // 把holdview缓存下来
        } else {
            holdView = (HoldView) convertView.getTag();
        }

        TopicFollowerBean bean = friendslist.get(position);

        if (bean.verifyType <= 0) {// 非加V用户，显示两行，第一行为昵称和简介，第二行为在线状态、距离、感情状态
            holdView.line2.setVisibility(View.VISIBLE);
            holdView.img_indentify.setVisibility(View.GONE);
            holdView.txt_desc.setText(bean.intro + "");
            holdView.txt_distance.setText(getLoginTimeShow(bean));
        } else {// 加V用户，显示一行，显示昵称、认证图标、认证信息
            holdView.line2.setVisibility(View.GONE);
            holdView.txt_desc.setText(bean.verifyIntro);
            holdView.img_indentify.setVisibility(View.VISIBLE);
            if (bean.verifyType == 1) {// 个人加V
                holdView.img_indentify.setImageResource(R.mipmap.icn_verify_person);
            } else {// 企业加V
                holdView.img_indentify.setImageResource(R.mipmap.icn_verify_enterprise);

            }
        }


        // 角色设定 0=unknow,1=t,2=p,3=h,5=bi
        if (bean.roleName.equals("0")) {
            holdView.img_role.setImageResource(R.mipmap.icn_role_unknow);
        } else if (bean.roleName.equals("1")) {
            holdView.img_role.setImageResource(R.mipmap.icn_role_t);
        } else if (bean.roleName.equals("2")) {
            holdView.img_role.setImageResource(R.mipmap.icn_role_p);
        } else if (bean.roleName.equals("3")) {
            holdView.img_role.setImageResource(R.mipmap.icn_role_h);
        } else if (bean.roleName.equals("5")) {
            holdView.img_role.setImageResource(R.mipmap.icn_role_bi);
        } else if (bean.roleName.equals("6")) {
            holdView.img_role.setImageResource(R.mipmap.icn_role_s);
        } else {
            holdView.img_role.setImageResource(R.mipmap.icn_role_unknow);
        }

        // 头像
        holdView.img_thumb.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(bean.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE))).build()).setAutoPlayAnimations(true).build());

        // 用户名
        holdView.txt_name.setText(bean.nickName);

        return convertView;
    }

    class HoldView {
        TextView txt_distance;
        SimpleDraweeView img_thumb;
        ImageView img_role;
        TextView txt_name;
        TextView txt_desc;
        ImageView img_indentify;
        LinearLayout line2;
    }

    public String getLoginTimeShow(TopicFollowerBean topicFollowerBean) {
        StringBuilder str = new StringBuilder();
        if (topicFollowerBean.level == 0 || topicFollowerBean.hiding == 0) {// 如果是会员并且开了隐身功能，则不显示距离
            str.append(topicFollowerBean.distance);
            str.append(", ");
        }
        str.append(topicFollowerBean.age);
        str.append(TheLApp.getContext().getString(R.string.updatauserinfo_activity_age_unit));
        ArrayList<String> relationship_list = new ArrayList<>(Arrays.asList(TheLApp.getContext().getResources().getStringArray(R.array.userinfo_relationship))); // 感情状态
        str.append(", ");
        try {
            str.append(relationship_list.get(topicFollowerBean.affection));
        } catch (Exception e) {
            str.append(relationship_list.get(0));
        }

        return str.toString();
    }

}
