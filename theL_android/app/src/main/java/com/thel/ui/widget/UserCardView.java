package com.thel.ui.widget;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.user.UserCardBean;
import com.thel.constants.TheLConstants;
import com.thel.modules.main.me.bean.RoleBean;
import com.thel.utils.DateUtils;
import com.thel.utils.ImageUtils;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by test1 on 2017/2/18.
 */

public class UserCardView extends RelativeLayout {
    private final Context mContext;
    private SimpleDraweeView img_background;
    private SimpleDraweeView img_avatar;
    private TextView txt_name;
    private TextView txt_intro;
    private TextView birthday;
    private TextView txt_constellation;
    private TextView tv_affection;

    public UserCardView(Context context) {
        this(context, null);
    }

    public UserCardView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public UserCardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }

    private void init() {
        inflate(mContext, R.layout.user_card_view, this);
        img_background = findViewById(R.id.img_background);
        img_avatar = findViewById(R.id.img_avatar);
        txt_name = findViewById(R.id.txt_name);
        txt_intro = findViewById(R.id.txt_intro);
        birthday = findViewById(R.id.tv_birthday);
        txt_constellation = findViewById(R.id.txt_constellation);
        tv_affection = findViewById(R.id.tv_affection);
    }

    public UserCardView initView(UserCardBean bean, boolean isMyself) {

        L.d("UserCardBean", " bean toJson : " + bean.toJson());

        img_avatar.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(bean.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE))).build()).setAutoPlayAnimations(true).build());
        txt_name.setText(bean.nickName);
        if (TextUtils.isEmpty(bean.recommendDesc)) {
            txt_intro.setVisibility(GONE);
        } else {
            txt_intro.setVisibility(VISIBLE);

            txt_intro.setText(getResources().getString(R.string.recommend_a, bean.recommendDesc));
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        try {

            Calendar calendar = Calendar.getInstance();

            calendar.setTime(sdf.parse(bean.birthday));

            getBirthdayStr(calendar);

            birthday.setText(bean.birthday);

        } catch (Exception e) {
            e.printStackTrace();

            birthday.setText("");
        }

        txt_constellation.setText(bean.horoscope);
        if (bean.affection != null && bean.affection.length() > 0) {
            try {
                int affection = Integer.valueOf(bean.affection);
                RoleBean roleBean = ShareFileUtils.getRoleBean(affection);
                tv_affection.setText(roleBean.contentRes);
            } catch (Exception e) {
                tv_affection.setText("");
                e.printStackTrace();
            }
        } else {
            tv_affection.setText("");
        }
        LinearLayout.LayoutParams txt_intro_params = (LinearLayout.LayoutParams) txt_intro.getLayoutParams();
        if (isMyself) {
            txt_intro_params.gravity = Gravity.LEFT;
        } else {
            txt_intro_params.gravity = Gravity.RIGHT;
        }

        img_background.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(bean.bgImage, TheLConstants.MOMENT_PIC_BIG_SIZE, TheLConstants.MOMENT_PIC_BIG_SIZE))).build()).setAutoPlayAnimations(true).build());

        img_background.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadius(15));

        return this;
    }

    private String getBirthdayStr(Calendar birth) {
        if (null == birth) {
            return "";
        }
        Calendar today = Calendar.getInstance();
        StringBuilder sb = new StringBuilder();
        try {
            int age = today.get(Calendar.YEAR) - birth.get(Calendar.YEAR);
            if (age < 18) {
                sb.append(18).append(TheLApp.context.getString(R.string.updatauserinfo_activity_age_unit)).append(", ").append(DateUtils.date2Constellation(birth));

            } else {
                sb.append(today.get(Calendar.YEAR) - birth.get(Calendar.YEAR)).append(TheLApp.context.getString(R.string.updatauserinfo_activity_age_unit)).append(", ").append(DateUtils.date2Constellation(birth));

            }
        } catch (Exception e) {
            return "";
        }

        return sb.toString();
    }

}
