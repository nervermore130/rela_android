package com.thel.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import com.thel.R;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SizeUtils;

import static com.thel.constants.QueueConstants.levelArray;

/**
 * Created by liuyun on 2018/1/25.
 */

public class LevelImageView extends androidx.appcompat.widget.AppCompatImageView {

    private int defaultHeight = 13;

    private static final int MAX_LEVEL = 35;

    public LevelImageView(Context context) {
        super(context);
        init();
    }

    public LevelImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.LevelImageView);

        defaultHeight = typedArray.getDimensionPixelSize(R.styleable.LevelImageView_image_height, defaultHeight);

        typedArray.recycle();

        init();
    }

    public LevelImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setLevel(0);
    }

    public void setLevel(int level) {

        L.d("LevelImageView", " level : " + level);

        int levelPrivileges = ShareFileUtils.getInt(com.thel.utils.ShareFileUtils.LEVEL_PRIVILEGES, 1);

        if (levelPrivileges == 1 && level > MAX_LEVEL) {
            level = 35;
        }


        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), levelArray.get(level));

        float width = bitmap.getWidth();

        float height = bitmap.getHeight();

        float bitmapHeight = SizeUtils.dip2px(getContext(), defaultHeight);

        final float density = getContext().getResources().getDisplayMetrics().density;

        float scale = bitmapHeight / height / density;

        Matrix matrix = new Matrix();

        matrix.postScale(scale, scale);

        Bitmap scaleBitmap = Bitmap.createBitmap(bitmap, 0, 0, (int) width, (int) height, matrix, true);

        setImageBitmap(scaleBitmap);

        if (getLayoutParams() instanceof RelativeLayout.LayoutParams) {

            int leftMargin = SizeUtils.dip2px(getContext(), 18);

            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) getLayoutParams();
            params.addRule(RelativeLayout.CENTER_VERTICAL);
            params.leftMargin = leftMargin;
            setLayoutParams(params);
        }

    }

}
