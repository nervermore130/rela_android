package com.thel.ui.widget;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.RelativeLayout;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SizeUtils;

public class FastGiftGuideLayout extends RelativeLayout {

    private static final String TAG = "FastGiftGuideLayout";
    private AutoSplitTextView content_tv;

    public FastGiftGuideLayout(Context context) {
        super(context);

        init(context);

    }

    public FastGiftGuideLayout(Context context, AttributeSet attrs) {
        super(context, attrs);

        init(context);

    }

    public FastGiftGuideLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init(context);

    }

    private void init(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_live_fast_gift_guide, this, true);
        content_tv = view.findViewById(R.id.content_tv);
    }

    private ValueAnimator valueAnimator;

    private void startAnimator(final View view) {

        int startY = 0;

        int endY = SizeUtils.dip2px(getContext(), 10);

        valueAnimator = ValueAnimator.ofInt(startY, endY, startY);

        valueAnimator.setDuration(1000);

        valueAnimator.setInterpolator(new LinearInterpolator());

        valueAnimator.setRepeatCount(ValueAnimator.INFINITE);

        valueAnimator.setRepeatMode(ValueAnimator.RESTART);

        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int offset = (int) animation.getAnimatedValue();
                view.setY(offset);
            }
        });

        valueAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                if (view != null) {
                    view.setLayerType(LAYER_TYPE_HARDWARE, null);
                }
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (view != null) {
                    view.setLayerType(LAYER_TYPE_NONE, null);
                }

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        valueAnimator.start();

    }

    private void stopAnimator() {
        if (valueAnimator != null) {
            valueAnimator.cancel();
            valueAnimator = null;
        }
    }

    public void show() {

        FastGiftGuideLayout.this.setVisibility(View.VISIBLE);

        startAnimator(this);

        postDelayed(new Runnable() {
            @Override
            public void run() {
                stopAnimator();
                FastGiftGuideLayout.this.setVisibility(View.INVISIBLE);
                /***第一次显示连击礼物的离手引导*/
                ShareFileUtils.setBoolean(ShareFileUtils.FAST_GIFT_TIP, true);

            }
        }, 5000);

    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        L.d(TAG, " onDetachedFromWindow ");

        stopAnimator();
    }

    private String getEmojiStringByUnicode(int unicode) {
        return new String(Character.toChars(unicode));
    }
}
