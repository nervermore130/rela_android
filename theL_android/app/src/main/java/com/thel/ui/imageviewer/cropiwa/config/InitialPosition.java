package com.thel.ui.imageviewer.cropiwa.config;

/**
 * @author Yaroslav Polyakov 25.02.2017.
 */
public enum InitialPosition {
    CENTER_INSIDE,
    CENTER_CROP
}
