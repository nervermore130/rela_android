package com.thel.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.RelativeLayout;

/**
 * Created by chad
 * Time 18/4/12
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */
public class BlockRelativeLayout extends RelativeLayout {

    private boolean block = false;

    public BlockRelativeLayout(Context context) {
        super(context);
    }

    public BlockRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BlockRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setBlock(boolean block) {
        this.block = block;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (block) return true;
        return super.dispatchTouchEvent(ev);
    }
}
