package com.thel.ui.widget;

import android.content.Context;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.user.NearUserBean;
import com.thel.bean.user.NearUserListNetBean;
import com.thel.manager.ImageLoaderManager;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.service.DefaultRequestService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MessageEmptyLayout extends LinearLayout {

    @BindView(R.id.user_avatar_1) ImageView user_avatar_1;

    @BindView(R.id.user_avatar_2) ImageView user_avatar_2;

    @BindView(R.id.user_avatar_3) ImageView user_avatar_3;

    @BindView(R.id.user_avatar_4) ImageView user_avatar_4;

    @BindView(R.id.user_avatar_5) ImageView user_avatar_5;

    @BindView(R.id.image_rl) RelativeLayout image_rl;

    @BindView(R.id.nearby_tips_view) LinearLayout nearby_tips_view;

    @BindView(R.id.distance_tv) TextView distance_tv;

    @BindView(R.id.goto_nearby_tv) TextView goto_nearby_tv;

    private NearUserListNetBean nearUserListNetBean;

    public MessageEmptyLayout(Context context) {
        super(context);
        init();
    }

    public MessageEmptyLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MessageEmptyLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {

        View view = LayoutInflater.from(getContext()).inflate(R.layout.layout_message_empty, this, true);

        ButterKnife.bind(this, view);

    }

    public void getData() {

        if (nearUserListNetBean != null) {
            return;
        }

        Map<String, String> map = new HashMap<>();

        map.put("position", "chat");

        Flowable<NearUserListNetBean> flowable = DefaultRequestService.createNearbyRequestService().getNearbySimpleList(map);

        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<NearUserListNetBean>() {
            @Override public void onNext(NearUserListNetBean data) {
                super.onNext(data);

                initUI(data);

                nearUserListNetBean = data;

            }
        });

    }

    private void initUI(NearUserListNetBean data) {


        if (data != null && data.data != null && data.data.map_list != null && data.data.map_list.size() > 0) {

            nearby_tips_view.setVisibility(VISIBLE);

            image_rl.setVisibility(VISIBLE);

            user_avatar_5.setVisibility(VISIBLE);

            List<NearUserBean> map_list = data.data.map_list;

            if (map_list.size() > 0) {
                ImageLoaderManager.imageLoaderCircle(user_avatar_5, R.mipmap.bg_head_frame, map_list.get(0).picUrl);
            }

            if (map_list.size() >= 2) {
                user_avatar_4.setVisibility(VISIBLE);
                ImageLoaderManager.imageLoaderCircle(user_avatar_4, R.mipmap.bg_head_frame, map_list.get(1).picUrl);
            }

            if (map_list.size() >= 3) {
                user_avatar_3.setVisibility(VISIBLE);
                ImageLoaderManager.imageLoaderCircle(user_avatar_3, R.mipmap.bg_head_frame, map_list.get(2).picUrl);
            }

            if (map_list.size() >= 4) {
                user_avatar_2.setVisibility(VISIBLE);
                ImageLoaderManager.imageLoaderCircle(user_avatar_2, R.mipmap.bg_head_frame, map_list.get(3).picUrl);
            }

            if (map_list.size() >= 5) {
                user_avatar_1.setVisibility(VISIBLE);
                ImageLoaderManager.imageLoaderCircle(user_avatar_1, R.mipmap.bg_head_frame, map_list.get(4).picUrl);
            }

            int position;
            if (map_list.size() >= 5) {
                position = 4;
            } else {
                position = map_list.size() - 1;
            }

            int distance = 0;

            try {

                for (int i = 0; i <= position; i++) {
                    int dis = Integer.valueOf(map_list.get(position).distance);
                    if (dis > distance) {
                        distance = dis;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                distance = 100;
            }

            if (distance > 5000) {
                distance_tv.setVisibility(GONE);
            } else {
                String content = TheLApp.context.getString(R.string.distance_from_you, getMiles(distance));
                distance_tv.setText(content);
            }

        } else {

            distance_tv.setVisibility(GONE);

        }


    }

    private String getMiles(int miles) {
        try {
            if (miles < 500) {

                if (miles < 100) {
                    miles = 100;
                } else {
                    miles = 500;
                }
                return miles + "m";
            } else {
                int kilometer = miles / 1000;
                if (kilometer == 0) {
                    kilometer = 1;
                }
                return kilometer + "km";
            }

        } catch (Exception e) {
            e.printStackTrace();
            return "null";
        }
    }

    @OnClick(R.id.goto_nearby_tv) void gotoNearby() {

        if (mOnGotoNearbyListener != null) {
            mOnGotoNearbyListener.gotoNearby();
        }

    }

    public OnGotoNearbyListener mOnGotoNearbyListener;

    public void setOnGotoNearbyListener(OnGotoNearbyListener mOnGotoNearbyListener) {
        this.mOnGotoNearbyListener = mOnGotoNearbyListener;
    }

    public interface OnGotoNearbyListener {
        void gotoNearby();
    }

}
