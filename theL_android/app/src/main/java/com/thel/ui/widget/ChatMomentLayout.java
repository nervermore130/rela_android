package com.thel.ui.widget;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.moments.MomentsBean;
import com.thel.constants.TheLConstants;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.main.home.moments.comment.MomentCommentActivity;
import com.thel.modules.main.home.moments.theme.ThemeDetailActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChatMomentLayout extends RelativeLayout {

    @BindView(R.id.avatar_iv) ImageView avatar_iv;

    @BindView(R.id.content_tv) TextView content_tv;

    @BindView(R.id.play_iv) ImageView play_iv;

    private MomentsBean bean;

    public ChatMomentLayout(Context context) {
        super(context);
        init(context);
    }

    public ChatMomentLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ChatMomentLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_chat_moment, this, true);
        ButterKnife.bind(this, view);
    }

    public void initData(MomentsBean bean) {
        if (bean != null) {

            this.bean = bean;

            this.setVisibility(View.VISIBLE);

            if (bean.momentsText != null) {
                content_tv.setText(bean.momentsText);
            } else {
                content_tv.setVisibility(View.GONE);
            }

            if (bean.imageUrl != null && bean.imageUrl.length() > 0) {
                String[] images = bean.imageUrl.split(",");
                if (images.length > 0) {
                    ImageLoaderManager.imageLoader(avatar_iv, R.color.default_avatar_color, images[0]);
                }
            } else {
                if (bean.thumbnailUrl != null && bean.thumbnailUrl.length() > 0) {
                    ImageLoaderManager.imageLoader(avatar_iv, R.color.default_avatar_color, bean.thumbnailUrl);
                } else {
                    avatar_iv.setVisibility(View.GONE);
                }
            }

            if (bean.momentsType.equals(MomentsBean.MOMENT_TYPE_VIDEO) || (bean.themeReplyClass != null && bean.themeReplyClass.equals("video"))) {
                play_iv.setVisibility(View.VISIBLE);
                ImageLoaderManager.imageLoader(play_iv, R.mipmap.btn_play_video);
            } else if (bean.momentsType.equals(MomentsBean.MOMENT_TYPE_LIVE)) {
                play_iv.setVisibility(View.VISIBLE);
                ImageLoaderManager.imageLoader(play_iv, R.mipmap.btn_live_play);
            } else {
                play_iv.setVisibility(View.GONE);
            }

        }

    }

    @OnClick(R.id.close_iv) void close() {
        this.setVisibility(View.GONE);
    }

}
