package com.thel.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.FrameLayout;

import com.thel.utils.L;

public class GestureFrameLayout extends FrameLayout {

    private static final String TAG = "GestureRelativeLayout";

    private static final float FLIP_DISTANCE = 50;

    private GestureDetector mDetector;

    private OnFinishListener mOnFinishListener;

    public GestureFrameLayout(Context context) {
        super(context);
        initGesture();
    }

    public GestureFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initGesture();
    }

    public GestureFrameLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initGesture();
    }

    @Override protected void onFinishInflate() {
        super.onFinishInflate();
    }

    private void initGesture() {

        L.i(TAG, "initGesture...");

        mDetector = new GestureDetector(getContext(), new GestureDetector.OnGestureListener() {

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                // TODO Auto-generated method stub
                L.i(TAG, "onSingleTapUp...");
                return false;
            }

            @Override
            public void onShowPress(MotionEvent e) {
                // TODO Auto-generated method stub
                L.i(TAG, "onShowPress...");

            }

            @Override
            public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                // TODO Auto-generated method stub
                L.i(TAG, "onScroll...");
                return false;
            }

            @Override
            public void onLongPress(MotionEvent e) {
                // TODO Auto-generated method stub
                L.i(TAG, "onLongPress...");

            }

            /**
             *
             * e1 The first down motion event that started the fling. e2 The
             * move motion event that triggered the current onFling.
             */
            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {

                L.i(TAG, "onFling...");

                if (e1.getX() - e2.getX() > FLIP_DISTANCE) {
                    L.i(TAG, "向左滑...");

                    return true;
                }
                if (e2.getX() - e1.getX() > FLIP_DISTANCE) {
                    L.i(TAG, "向右滑...");

                    return true;
                }
                if (e1.getY() - e2.getY() > FLIP_DISTANCE) {
                    L.i(TAG, "向上滑...");
                    return true;
                }
                if (e2.getY() - e1.getY() > FLIP_DISTANCE) {
                    L.i(TAG, "向下滑...");

                    if (mOnFinishListener != null) {
                        mOnFinishListener.onFinish();
                    }
                    return true;
                }

                Log.d("TAG", e2.getX() + " " + e2.getY());

                return false;
            }

            @Override
            public boolean onDown(MotionEvent e) {
                // TODO Auto-generated method stub
                return true;
            }
        });

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        return mDetector.onTouchEvent(event);
    }

    public void setOnFinishListener(OnFinishListener mOnFinishListener) {
        this.mOnFinishListener = mOnFinishListener;
    }

    public interface OnFinishListener {
        void onFinish();
    }

}
