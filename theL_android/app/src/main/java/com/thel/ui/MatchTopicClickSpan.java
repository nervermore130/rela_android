package com.thel.ui;

import android.content.Intent;
import android.graphics.Color;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

import com.thel.app.TheLApp;
import com.thel.constants.TheLConstants;
import com.thel.modules.main.home.tag.TagDetailActivity;

public class MatchTopicClickSpan extends ClickableSpan {

    private CharSequence text;

    public MatchTopicClickSpan(CharSequence text) {
        super();
        this.text = text;
    }

    @Override
    public void updateDrawState(TextPaint ds) {
        ds.setColor(Color.parseColor("#4BBABC"));
        ds.setUnderlineText(false); // 去掉下划线
    }

    @Override
    public void onClick(View widget) {
        if (!TheLConstants.defaultFlag) {
            return;
        }
       /* Intent intent = new Intent(TheLApp.getContext().getApplicationContext(), TagDetailActivity.class);
        intent.putExtra(TheLConstants.BUNDLE_KEY_TOPIC_NAME, text.toString());
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        TheLApp.getContext().getApplicationContext().startActivity(intent);*/
    }
}
