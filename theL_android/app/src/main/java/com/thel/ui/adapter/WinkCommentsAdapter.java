package com.thel.ui.adapter;

import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.CommentWinkUserBean;
import com.thel.constants.TheLConstants;
import com.thel.imp.follow.FollowStatusChangedImpl;
import com.thel.imp.follow.FollowStatusChangedListener;
import com.thel.ui.widget.FollowView;
import com.thel.ui.widget.popupwindow.UnFollowPopupWindow;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;
import com.thel.utils.ShareFileUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.thel.imp.follow.FollowStatusChangedListener.FOLLOW_STATUS_FAN;
import static com.thel.imp.follow.FollowStatusChangedListener.FOLLOW_STATUS_FRIEND;
import static com.thel.imp.follow.FollowStatusChangedListener.FOLLOW_STATUS_NO;


public class WinkCommentsAdapter extends BaseRecyclerViewAdapter<CommentWinkUserBean> {

    final BaseRecyclerViewAdapter.OnItemChildClickListener mChildListener = new OnItemChildClickListener();
    private FollowView txt_follow;

    public WinkCommentsAdapter(List<CommentWinkUserBean> list) {
        super(R.layout.search_friend_listitem, list);
    }

    @Override
    protected void convert(BaseViewHolder helper, CommentWinkUserBean bean) {

        if (bean.verifyType <= 0) {// 非加V用户，显示两行，第一行为昵称和简介，第二行为在线状态、距离、感情状态
            helper.setVisibility(R.id.line2, View.VISIBLE);
            helper.setVisibility(R.id.img_indentify, View.GONE);
            helper.setText(R.id.txt_desc, bean.intro + "");


        } else {// 加V用户，显示一行，显示昵称、认证图标、认证信息
            helper.setVisibility(R.id.line2, View.VISIBLE);
            helper.setVisibility(R.id.img_indentify, View.VISIBLE);
            helper.setText(R.id.txt_desc, bean.verifyIntro + "");

            if (bean.verifyType == 1) {// 个人加V
                helper.setImageResource(R.id.img_indentify, R.mipmap.icn_verify_person);
            } else {// 企业加V
                helper.setImageResource(R.id.img_indentify, R.mipmap.icn_verify_enterprise);

            }
        }
        if (bean.hiding != 0) {
            helper.setText(R.id.txt_distance, bean.distance);

        } else {
            helper.setText(R.id.txt_distance, "");
        }
        if (bean.level > 0) {
            helper.setVisibility(R.id.img_vip, VISIBLE);
            switch (bean.level) {
                case 1:
                    helper.setImageResource(R.id.img_vip, R.mipmap.icn_vip_1);
                    break;
                case 2:
                    helper.setImageResource(R.id.img_vip, R.mipmap.icn_vip_2);

                    break;
                case 3:
                    helper.setImageResource(R.id.img_vip, R.mipmap.icn_vip_3);

                    break;
                case 4:
                    helper.setImageResource(R.id.img_vip, R.mipmap.icn_vip_4);

                    break;
            }
        } else {
            helper.setVisibility(R.id.img_vip, GONE);

        }

       /* if (bean.verifyType <= 0) {// 非加V用户，显示两行，第一行为昵称和简介，第二行为在线状态、距离、感情状态
            helper.setVisibility(R.id.line2, View.VISIBLE);
            helper.setVisibility(R.id.img_indentify, View.GONE);
            helper.setText(R.id.txt_desc, bean.intro);
            helper.setText(R.id.txt_distance, getLoginTimeShow(bean));
        } else {// 加V用户，显示一行，显示昵称、认证图标、认证信息
            helper.setVisibility(R.id.line2, View.GONE);
            helper.setText(R.id.txt_desc, bean.verifyIntro);
            helper.setVisibility(R.id.img_indentify, View.VISIBLE);
            if (bean.verifyType == 1) {// 个人加V
                helper.setImageResource(R.id.img_indentify, R.mipmap.icn_verify_person);
            } else {// 企业加V
                helper.setImageResource(R.id.img_indentify, R.mipmap.icn_verify_enterprise);
            }
        }*/

        // 角色设定 0=unknow,1=t,2=p,3=h,5=bi
        if (bean.roleName.equals("0")) {
            helper.setImageResource(R.id.img_role, R.mipmap.icn_role_unknow);
        } else if (bean.roleName.equals("1")) {
            helper.setImageResource(R.id.img_role, R.mipmap.icn_role_t);
        } else if (bean.roleName.equals("2")) {
            helper.setImageResource(R.id.img_role, R.mipmap.icn_role_p);
        } else if (bean.roleName.equals("3")) {
            helper.setImageResource(R.id.img_role, R.mipmap.icn_role_h);
        } else if (bean.roleName.equals("5")) {
            helper.setImageResource(R.id.img_role, R.mipmap.icn_role_bi);
        } else if (bean.roleName.equals("6")) {
            helper.setImageResource(R.id.img_role, R.mipmap.icn_role_s);
        } else {
            helper.setImageResource(R.id.img_role, R.mipmap.icn_role_unknow);
        }

        // 头像
        helper.setImageUrl(R.id.img_thumb, bean.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE);
        // 用户名
        helper.setText(R.id.txt_name, bean.nickName);

        txt_follow = helper.getView(R.id.txt_follow);
        if (!ShareFileUtils.getString(ShareFileUtils.ID, "").equals(bean.userId + "")) {
            setFollowStatus(bean.followStatus);
        } else {
            txt_follow.setVisibility(View.GONE);
        }
        txt_follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bean.isFollow == 1) {
                    UnFollowPopupWindow mUnFollowPopupWindow = new UnFollowPopupWindow(mContext, bean.nickName);
                    mUnFollowPopupWindow.setOnUnFollowListener(new UnFollowPopupWindow.OnUnFollowListener() {
                        @Override
                        public void onUnFollow() {
                            //  mData.remove(helper.getAdapterPosition() - getHeaderLayoutCount());
                            // notifyItemRemoved(helper.getAdapterPosition());

                            //  notifyItemChanged(helper.getAdapterPosition());
                            FollowStatusChangedImpl.followUser(bean.userId + "", FollowStatusChangedImpl.ACTION_TYPE_CANCEL_FOLLOW, bean.nickName, bean.avatar);
                            bean.isFollow = 0;

                        }
                    });
                    mUnFollowPopupWindow.showAtLocation(v, Gravity.BOTTOM, 0, 0);

                } else {
                    FollowStatusChangedImpl.followUser(bean.userId + "", FollowStatusChangedListener.ACTION_TYPE_FOLLOW, bean.nickName, bean.avatar);
                    bean.isFollow = 1;
                    // bean.followStatus = 1;
                    // refreshFollowStatus(bean);
                }


            }
        });

    }


    public String getLoginTimeShow(CommentWinkUserBean bean) {
        StringBuilder str = new StringBuilder();
        if (!TextUtils.isEmpty(bean.distance) && bean.hiding == 0) {// 如果是会员并且开了隐身功能，则不显示距离
            str.append(bean.distance);
            str.append(", ");
        }
        str.append(bean.age);
        str.append(TheLApp.getContext().getString(R.string.updatauserinfo_activity_age_unit));
        ArrayList<String> relationship_list = new ArrayList<>(Arrays.asList(TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship))); // 感情状态
        str.append(", ");
        try {
            str.append(relationship_list.get(bean.affection));
        } catch (Exception e) {
            str.append(relationship_list.get(0));
        }

        return str.toString();
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return super.onCreateViewHolder(parent, viewType);
    }

    public class WinkViewHolder extends BaseViewHolder {

        protected WinkViewHolder(View view) {
            super(view);
        }
    }

    private void setFollowStatus(final int followStatus) {
        if (followStatus == FOLLOW_STATUS_FAN) {
            txt_follow.setText(TheLApp.getContext().getString(R.string.repowder));
            txt_follow.setBackgroundResource(R.drawable.selector_follow);

            txt_follow.setTextColor(ContextCompat.getColor(TheLApp.context, R.color.white));
        } else if (followStatus == FOLLOW_STATUS_NO) {
            txt_follow.setText(TheLApp.getContext().getString(R.string.userinfo_activity_follow));
            txt_follow.setBackgroundResource(R.drawable.selector_follow);
            txt_follow.setTextColor(ContextCompat.getColor(TheLApp.context, R.color.white));

        } else if (followStatus == FOLLOW_STATUS_FRIEND) {
            txt_follow.setText(TheLApp.getContext().getString(R.string.interrelated));
            txt_follow.setBackgroundResource(R.drawable.follow_unselector);
            txt_follow.setTextColor(ContextCompat.getColor(TheLApp.context, R.color.tab_normal));
        } else {
            txt_follow.setText(TheLApp.getContext().getString(R.string.userinfo_activity_followed));
            txt_follow.setBackgroundResource(R.drawable.follow_unselector);
            txt_follow.setTextColor(ContextCompat.getColor(TheLApp.context, R.color.tab_normal));
        }


    }

}
