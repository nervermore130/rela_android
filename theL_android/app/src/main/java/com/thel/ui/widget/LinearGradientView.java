package com.thel.ui.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.View;

import com.thel.utils.SizeUtils;

import androidx.annotation.Nullable;

public class LinearGradientView extends View {

    private Paint paint;

    private LinearGradient backGradient = null;

    private int colorStart = -1;

    private int colorEnd = -1;

    private RectF mRectF;

    public LinearGradientView(Context context) {
        this(context, null);
    }

    public LinearGradientView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LinearGradientView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        initView();
    }

    private void initView() {
        paint = new Paint();
        mRectF = new RectF();
    }

    public void setBGColor(int colorStart, int colorEnd) {
        this.colorStart = colorStart;
        this.colorEnd = colorEnd;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        // 获取 View 的宽高
        int width = getWidth();
        int height = getHeight();

        if (backGradient == null && colorEnd != -1 && colorStart != -1) {
            backGradient = new LinearGradient(0, 0, width, height, colorStart, colorEnd, Shader.TileMode.CLAMP);
        }

        mRectF.set(0, 0, width, height);

        paint.setShader(backGradient);

        canvas.drawRoundRect(mRectF, SizeUtils.dip2px(getContext(), 12.5f), SizeUtils.dip2px(getContext(), 12.5f), paint);

//        canvas.drawRect(0, 0, width, height, paint);

    }

}
