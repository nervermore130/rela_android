package com.thel.ui.slidecard;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Point;
import android.graphics.Rect;

import androidx.core.view.GestureDetectorCompat;
import androidx.core.view.ViewCompat;

import android.util.AttributeSet;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.thel.R;
import com.thel.utils.ToastUtils;

import java.util.ArrayList;
import java.util.List;

import androidx.customview.widget.ViewDragHelper;
import video.com.relavideolibrary.Utils.DensityUtils;

/**
 * 卡片滑动面板，主要逻辑实现类
 *
 * @author xmuSistone
 */
@SuppressLint({"HandlerLeak", "NewApi", "ClickableViewAccessibility"})
public class CardSlidePanel extends ViewGroup {
    private List<CardItemView> viewList = new ArrayList<>(); // 存放的是每一层的view，从顶到底
    private List<View> releasedViewList = new ArrayList<>(); // 手指松开后存放的view列表

    /* 拖拽工具类 */
    private final ViewDragHelper mDragHelper; // 这个跟原生的ViewDragHelper差不多，我仅仅只是修改了Interpolator
    private int initCenterViewX = 0, initCenterViewY = 0; // 最初时，中间View的x位置,y位置
    private int allWidth = 0; // 面板的宽度
    private int allHeight = 0; // 面板的高度
    private int childWith = 0; // 每一个子View对应的宽度

    private static final float SCALE_STEP = 0.08f; // view叠加缩放的步长
    private static final int MAX_SLIDE_DISTANCE_LINKAGE = 500; // 水平距离+垂直距离

    private int itemMarginTop = DensityUtils.dp2px(15); // 卡片距离顶部的偏移量
    private int yOffsetStep = DensityUtils.dp2px(5); // view叠加垂直偏移量的步长
    private int mTouchSlop = 5; // 判定为滑动的阈值，单位是像素

    private static final int X_VEL_THRESHOLD = 800;
    private static final int X_DISTANCE_THRESHOLD = 300;

    public static final int VANISH_TYPE_LEFT = 0;
    public static final int VANISH_TYPE_RIGHT = 1;

    private CardSwitchListener cardSwitchListener; // 回调接口
    private int isShowing = 0; // 当前正在显示的小项
    private boolean btnLock = false;
    private GestureDetectorCompat moveDetector;
    private Point downPoint = new Point();
    private CardAdapter adapter;
    private static final int VIEW_COUNT = 4;
    private Rect draggableArea;
    private boolean isReverting = false;

    public CardSlidePanel(Context context) {
        this(context, null);
    }

    public CardSlidePanel(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CardSlidePanel(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        // 滑动相关类
        mDragHelper = ViewDragHelper
                .create(this, 10f, new DragHelperCallback());
        mDragHelper.setEdgeTrackingEnabled(ViewDragHelper.EDGE_BOTTOM);

        ViewConfiguration configuration = ViewConfiguration.get(getContext());
        mTouchSlop = configuration.getScaledTouchSlop();
        moveDetector = new GestureDetectorCompat(context,
                new MoveDetector());
        moveDetector.setIsLongpressEnabled(false);

        getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (getChildCount() != VIEW_COUNT) {
                    doBindAdapter();
                }
            }
        });
    }

    public void setReverting(boolean isReverting) {
        this.isReverting = isReverting;
    }

    public boolean getReverting() {
        return isReverting;
    }

    public int getMarginTop() {
        return itemMarginTop;
    }

    public int getInitCenterViewX() {
        return initCenterViewX;
    }

    public int getInitCenterViewY() {
        return initCenterViewY;
    }

    private void doBindAdapter() {
        if (adapter == null || allWidth <= 0 || allHeight <= 0) {
            return;
        }

        // 1. addView添加到ViewGroup中
        for (int i = 0; i < VIEW_COUNT; i++) {
            CardItemView itemView = new CardItemView(getContext());
            itemView.bindLayoutResId(adapter.getLayoutId());
            itemView.setParentView(this);
            addView(itemView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

            if (i == 0) {
                itemView.setAlpha(0);
            }
        }

        // 2. viewList初始化
        viewList.clear();
        for (int i = 0; i < VIEW_COUNT; i++) {
            viewList.add((CardItemView) getChildAt(VIEW_COUNT - 1 - i));
        }


        // 3. 填充数据
        int count = adapter.getCount();
        for (int i = 0; i < VIEW_COUNT; i++) {
            if (i < count) {
                adapter.bindView(viewList.get(i), i);
            } else {
                viewList.get(i).setVisibility(View.INVISIBLE);
            }
        }

        //4.初始化白色遮罩状态
        for (int i = 0; i < viewList.size(); i++) {
            CardItemView itemView = viewList.get(i);
            itemView.findViewById(R.id.shadeImage).setVisibility(i == 0 ? INVISIBLE : VISIBLE);
        }
    }

    class MoveDetector extends SimpleOnGestureListener {

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float dx,
                                float dy) {
            // 拖动了，touch不往下传递
            return Math.abs(dy) + Math.abs(dx) > mTouchSlop;
        }
    }

    private float getCenterX(View child) {
        swipeLeft = child.getWidth() / 2 + child.getX() - (allWidth / 2) < 0;
        float width = Math.abs(child.getWidth() / 2 + child.getX() - allWidth / 2);
        if (width > allWidth / 2) {
            width = allWidth / 2;
        }
        return width / allWidth / 2;
    }

    //左滑右滑判断
    private boolean swipeLeft = false;
    //手指离开屏幕的判断
    private boolean isRelise;

    /**
     * 这是viewdraghelper拖拽效果的主要逻辑
     */
    private class DragHelperCallback extends ViewDragHelper.Callback {

        @Override
        public void onViewPositionChanged(View changedView, int left, int top,
                                          int dx, int dy) {
            onViewPosChanged((CardItemView) changedView);

            //设置第二层白色遮罩不可见
            viewList.get(1).findViewById(R.id.shadeImage).setVisibility(INVISIBLE);

            if (isRelise) {
                if (swipeLeft) {
                    changedView.setRotation(-getCenterX(changedView) * 20);
                } else {
                    changedView.setRotation(getCenterX(changedView) * 20);
                }
            }
            if (cardSwitchListener != null)
                cardSwitchListener.onScroll(changedView, getScrollProgressPercent(left));
        }

        @Override
        public boolean tryCaptureView(View child, int pointerId) {
            // 如果数据List为空，或者子View不可见，则不予处理

            if (adapter == null || adapter.getCount() == 0
                    || child.getVisibility() != View.VISIBLE || child.getScaleX() <= 1.0f - SCALE_STEP) {
                // 一般来讲，如果拖动的是第三层、或者第四层的View，则直接禁止
                // 此处用getScale的用法来巧妙回避
                return false;
            }

            if (btnLock) {
                return false;
            }

            // 1. 只有顶部的View才允许滑动
            int childIndex = viewList.indexOf(child);
            if (childIndex > 0) {
                return false;
            }

            // 2. 获取可滑动区域
            ((CardItemView) child).onStartDragging();
            if (draggableArea == null) {
                draggableArea = adapter.obtainDraggableArea(child);
            }


            // 3. 判断是否可滑动
            boolean shouldCapture = true;
            if (null != draggableArea) {
                shouldCapture = draggableArea.contains(downPoint.x, downPoint.y);
            }

            // 4. 如果确定要滑动，就让touch事件交给自己消费
            if (shouldCapture) {
                getParent().requestDisallowInterceptTouchEvent(shouldCapture);
            }

            return shouldCapture;
        }

        @Override
        public int getViewHorizontalDragRange(View child) {
            // 这个用来控制拖拽过程中松手后，自动滑行的速度
            return 256;
        }

        @Override
        public void onViewReleased(View releasedChild, float xvel, float yvel) {

            //设置第二层白色遮罩可见
            viewList.get(1).findViewById(R.id.shadeImage).setVisibility(VISIBLE);

            animToSide((CardItemView) releasedChild, (int) xvel, (int) yvel);

            if (releasedChild.getLeft() / 2 > 300) {
                isRelise = true;
            }
            releasedChild.setRotation(0);
        }

        @Override
        public int clampViewPositionHorizontal(View child, int left, int dx) {
            try {
                if (isReverting) return 0;

                //1新手引导喜欢 2新手引导不喜欢
                int dataType = ((CardItemView) child).getDataType();
                boolean isSuperlike = ((CardItemView) child).getSuperlike();
                if (dataType == 1 && left < 0) {
                    ToastUtils.showCenterToastShort(getContext(), getContext().getString(R.string.match_indicator_tip3));
                    return 0;
                } else if (dataType == 2 && left > 0) {
                    ToastUtils.showCenterToastShort(getContext(), getContext().getString(R.string.match_indicator_tip5));
                    return 0;
                } else if (isSuperlike && left < 0) {
                    if (cardSwitchListener != null) cardSwitchListener.superlike();
                    return 0;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (isRelise) {
                isRelise = false;
            }
            if (swipeLeft) {
                child.setRotation(-getCenterX(child) * 20);
            } else {
                child.setRotation(getCenterX(child) * 20);
            }
            return left;
        }

        @Override
        public int clampViewPositionVertical(View child, int top, int dy) {
            return top;
        }
    }


    public void onViewPosChanged(CardItemView changedView) {
        // 调用offsetLeftAndRight导致viewPosition改变，会调到此处，所以此处对index做保护处理
        int index = viewList.indexOf(changedView);
        if (index + 2 > viewList.size()) {
            return;
        }

        processLinkageView(changedView);
    }

    /**
     * 对View重新排序
     */
    private void orderViewStack() {
        if (releasedViewList.size() == 0) {
            return;
        }

        CardItemView changedView = (CardItemView) releasedViewList.get(0);
        if (changedView.getLeft() == initCenterViewX) {
            releasedViewList.remove(0);
            return;
        }

        // 1. 消失的卡片View位置重置，由于大多手机会重新调用onLayout函数，所以此处大可以不做处理，不信你注释掉看看
        changedView.offsetLeftAndRight(initCenterViewX
                - changedView.getLeft());
        changedView.offsetTopAndBottom(initCenterViewY
                - changedView.getTop() + yOffsetStep * 2);
        float scale = 1.0f - SCALE_STEP * 2;
        changedView.setScaleX(scale);
        changedView.setScaleY(scale);
        changedView.setAlpha(0);

        // 2. 卡片View在ViewGroup中的顺次调整
        LayoutParams lp = changedView.getLayoutParams();
        removeViewInLayout(changedView);
        addViewInLayout(changedView, 0, lp, true);

        // 3. changedView填充新数据
        int newIndex = isShowing + 4;
        if (newIndex < adapter.getCount()) {
            adapter.bindView(changedView, newIndex);
        } else {
            changedView.setVisibility(View.INVISIBLE);
        }

        // 4. viewList中的卡片view的位次调整
        viewList.remove(changedView);
        viewList.add(changedView);
        releasedViewList.remove(0);

        // 5. 更新showIndex、接口回调
        if (isShowing + 1 < adapter.getCount()) {
            isShowing++;
        }

        // 6.更新白色遮罩
        for (int i = 0; i < viewList.size(); i++) {
            CardItemView itemView = viewList.get(i);
            itemView.findViewById(R.id.shadeImage).setVisibility(i == 0 ? INVISIBLE : VISIBLE);
        }

        if (null != cardSwitchListener) {
            cardSwitchListener.onShow(changedView, isShowing);
            cardSwitchListener.onScroll(changedView, 0);
        }
    }

    /**
     * 顶层卡片View位置改变，底层的位置需要调整
     *
     * @param changedView 顶层的卡片view
     */
    private void processLinkageView(View changedView) {
        int changeViewLeft = changedView.getLeft();
        int changeViewTop = changedView.getTop();
        int distance = Math.abs(changeViewTop - initCenterViewY)
                + Math.abs(changeViewLeft - initCenterViewX);
        float rate = distance / (float) MAX_SLIDE_DISTANCE_LINKAGE;

        float rate1 = rate;
        float rate2 = rate - 0.1f;

        if (rate > 1) {
            rate1 = 1;
        }

        if (rate2 < 0) {
            rate2 = 0;
        } else if (rate2 > 1) {
            rate2 = 1;
        }

        ajustLinkageViewItem(changedView, rate1, 1);
        ajustLinkageViewItem(changedView, rate2, 2);

        CardItemView bottomCardView = viewList.get(viewList.size() - 1);
        bottomCardView.setAlpha(rate2);

        //去掉第二层view的遮罩
//        CardItemView secondCardView = viewList.get(1);
//        if (rate2 == 0) {
//            secondCardView.findViewById(R.id.shadeImage).setVisibility(INVISIBLE);
//        } else if (rate2 == 1) {
//            secondCardView.findViewById(R.id.shadeImage).setVisibility(VISIBLE);
//        }
    }

    // 由index对应view变成index-1对应的view
    private void ajustLinkageViewItem(View changedView, float rate, int index) {
        int changeIndex = viewList.indexOf(changedView);
        int initPosY = yOffsetStep * index;
        float initScale = 1 - SCALE_STEP * index;

        int nextPosY = yOffsetStep * (index - 1);
        float nextScale = 1 - SCALE_STEP * (index - 1);

        int offset = (int) (initPosY + (nextPosY - initPosY) * rate);
        float scale = initScale + (nextScale - initScale) * rate;

        View ajustView = viewList.get(changeIndex + index);
        ajustView.offsetTopAndBottom(offset - ajustView.getTop()
                + initCenterViewY);
        ajustView.setScaleX(scale);
        ajustView.setScaleY(scale);
    }

    /**
     * 松手时处理滑动到边缘的动画
     */
    private void animToSide(CardItemView changedView, int xvel, int yvel) {
        int dataType = changedView.getDataType();
        boolean isSuperlike = changedView.getSuperlike();
        boolean animEnable = true;
        int finalX = initCenterViewX;
        int finalY = initCenterViewY;
        int flyType = -1;

        // 1. 下面这一坨计算finalX和finalY，要读懂代码需要建立一个比较清晰的数学模型才能理解，不信拉倒
        int dx = changedView.getLeft() - initCenterViewX;
        int dy = changedView.getTop() - initCenterViewY;

        // yvel < xvel * xyRate则允许以速度计算偏移
        final float xyRate = 3f;
        if (xvel > X_VEL_THRESHOLD && Math.abs(yvel) < xvel * xyRate) {
            // x正方向的速度足够大，向右滑动消失
            finalX = allWidth;
            finalY = yvel * (childWith + changedView.getLeft()) / xvel + changedView.getTop();
            flyType = VANISH_TYPE_RIGHT;
            if (dataType == 2) animEnable = false;
        } else if (xvel < -X_VEL_THRESHOLD && Math.abs(yvel) < -xvel * xyRate) {
            // x负方向的速度足够大，向左滑动消失
            finalX = -childWith;
            finalY = yvel * (childWith + changedView.getLeft()) / (-xvel) + changedView.getTop();
            flyType = VANISH_TYPE_LEFT;
            if (dataType == 1 || isSuperlike) animEnable = false;
        } else if (dx > X_DISTANCE_THRESHOLD && Math.abs(dy) < dx * xyRate) {
            // x正方向的位移足够大，向右滑动消失
            finalX = allWidth;
            finalY = dy * (childWith + initCenterViewX) / dx + initCenterViewY;
            flyType = VANISH_TYPE_RIGHT;
            if (dataType == 2) animEnable = false;
        } else if (dx < -X_DISTANCE_THRESHOLD && Math.abs(dy) < -dx * xyRate) {
            // x负方向的位移足够大，向左滑动消失
            finalX = -childWith;
            finalY = dy * (childWith + initCenterViewX) / (-dx) + initCenterViewY;
            flyType = VANISH_TYPE_LEFT;
            if (dataType == 1 || isSuperlike) animEnable = false;
        }

        // 如果斜率太高，就折中处理
        if (finalY > allHeight) {
            finalY = allHeight;
        } else if (finalY < -allHeight / 2) {
            finalY = -allHeight / 2;
        }

        // 如果没有飞向两侧，而是回到了中间，需要谨慎处理
        if (finalX == initCenterViewX || !animEnable || isReverting) {
            changedView.animTo(initCenterViewX, initCenterViewY);
            if (cardSwitchListener != null)
                cardSwitchListener.onScroll(changedView, 0);
        } else {
            // 2. 向两边消失的动画
            releasedViewList.add(changedView);
            if (mDragHelper.smoothSlideViewTo(changedView, finalX, finalY)) {
                ViewCompat.postInvalidateOnAnimation(this);
            }

            // 3. 消失动画即将进行，listener回调
            if (flyType >= 0 && cardSwitchListener != null) {
                cardSwitchListener.onCardVanish(isShowing, flyType);
            }
        }
    }

    private void onBtnClick(int type) {
        View animateView = viewList.get(0);
        if (animateView.getVisibility() != View.VISIBLE || releasedViewList.contains(animateView)) {
            return;
        }

        int finalX = 0;
        int extraVanishDistance = 100; // 为加快vanish的速度，额外添加消失的距离
        switch (type) {
            case VANISH_TYPE_LEFT:
                finalX = -childWith - extraVanishDistance;
                break;
            case VANISH_TYPE_RIGHT:
                finalX = allWidth + extraVanishDistance;
                break;
        }

        if (finalX != 0) {
            releasedViewList.add(animateView);
            if (mDragHelper.smoothSlideViewTo(animateView, finalX, initCenterViewY + allHeight / 2)) {
                ViewCompat.postInvalidateOnAnimation(this);
            }
        }

        if (type >= 0 && cardSwitchListener != null) {
            cardSwitchListener.onCardVanish(isShowing, type);
        }
    }

    @Override
    public void computeScroll() {
        if (mDragHelper.continueSettling(true)) {
            ViewCompat.postInvalidateOnAnimation(this);
        } else {
            // 动画结束
            if (mDragHelper.getViewDragState() == ViewDragHelper.STATE_IDLE) {
                orderViewStack();
                btnLock = false;
            }
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        int action = ev.getActionMasked();
        // 按下时保存坐标信息
        if (action == MotionEvent.ACTION_DOWN) {
            this.downPoint.x = (int) ev.getX();
            this.downPoint.y = (int) ev.getY();
        }
        return super.dispatchTouchEvent(ev);
    }

    /* touch事件的拦截与处理都交给mDraghelper来处理 */
    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        boolean shouldIntercept = mDragHelper.shouldInterceptTouchEvent(ev);
        boolean moveFlag = moveDetector.onTouchEvent(ev);
        int action = ev.getActionMasked();
        if (action == MotionEvent.ACTION_DOWN) {
            // ACTION_DOWN的时候就对view重新排序
            if (mDragHelper.getViewDragState() == ViewDragHelper.STATE_SETTLING) {
                mDragHelper.abort();
            }
            orderViewStack();

            // 保存初次按下时arrowFlagView的Y坐标
            // action_down时就让mDragHelper开始工作，否则有时候导致异常
            mDragHelper.processTouchEvent(ev);
        }

        return shouldIntercept && moveFlag;
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        try {
            // 统一交给mDragHelper处理，由DragHelperCallback实现拖动效果
            // 该行代码可能会抛异常，正式发布时请将这行代码加上try catch
            mDragHelper.processTouchEvent(e);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return true;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        measureChildren(widthMeasureSpec, heightMeasureSpec);
        int maxWidth = MeasureSpec.getSize(widthMeasureSpec);
        int maxHeight = MeasureSpec.getSize(heightMeasureSpec);
        setMeasuredDimension(
                resolveSizeAndState(maxWidth, widthMeasureSpec, 0),
                resolveSizeAndState(maxHeight, heightMeasureSpec, 0));

        allWidth = getMeasuredWidth();
        allHeight = getMeasuredHeight();
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right,
                            int bottom) {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View viewItem = viewList.get(i);
            int childHeight = viewItem.getMeasuredHeight();
            int viewLeft = (getWidth() - viewItem.getMeasuredWidth()) / 2;

            // 1. 先layout出来
            viewItem.layout(viewLeft, itemMarginTop, viewLeft + viewItem.getMeasuredWidth(), itemMarginTop + childHeight);

            // 2. 调整位置
            int offset = yOffsetStep * i;
            float scale = 1 - SCALE_STEP * i;
            if (i > 2) {
                // 备用的view
                offset = yOffsetStep * 2;
                scale = 1 - SCALE_STEP * 2;
            }
            viewItem.offsetTopAndBottom(offset);

            // 3. 调整缩放、重心等
            viewItem.setPivotY(viewItem.getMeasuredHeight());
            viewItem.setPivotX(viewItem.getMeasuredWidth() / 2);
            viewItem.setScaleX(scale);
            viewItem.setScaleY(scale);
        }

        if (childCount > 0) {
            // 初始化一些中间参数
            initCenterViewX = viewList.get(0).getLeft();
            initCenterViewY = viewList.get(0).getTop();
            childWith = viewList.get(0).getMeasuredWidth();
        }
    }

    private float getScrollProgressPercent(int aPosX) {
        float zeroToOneValue = (aPosX + childWith / 2.f - leftBorder()) / (rightBorder() - leftBorder());
        return zeroToOneValue * 2f - 1.5f;
    }

    private float leftBorder() {
        return allWidth / 5.f;
    }

    private float rightBorder() {
        return 3 * allWidth / 5.f;
    }

    public void setAdapter(final CardAdapter adapter) {
        this.adapter = adapter;
        doBindAdapter();
        adapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                orderViewStack();

                boolean reset = false;
                if (adapter.getCount() > 0) {
                    isShowing = 0;
                    reset = true;
                }

                int delay = 0;
                for (int i = 0; i < VIEW_COUNT; i++) {
                    if (viewList.size() > 0) {
                        CardItemView itemView = viewList.get(i);
                        if (isShowing + i < adapter.getCount()) {
                            if (itemView.getVisibility() == View.VISIBLE) {
                                if (!reset) {
                                    continue;
                                }
                            } else if (i == 0) {
                                if (isShowing > 0) {
                                    isShowing++;
                                }
                                cardSwitchListener.onShow(itemView, isShowing);
                                cardSwitchListener.onScroll(itemView, 0);
                                itemView.setOnClickListener(v -> {
                                    if (!isReverting && cardSwitchListener != null) {
                                        cardSwitchListener.itemClick(itemView, isShowing);
                                    }
                                });
                            }
                            if (i == VIEW_COUNT - 1) {
                                itemView.setAlpha(0);
                                itemView.setVisibility(View.VISIBLE);
                            } else {
                                itemView.setVisibilityWithAnimation(View.VISIBLE, delay++);
                            }
                            adapter.bindView(itemView, isShowing + i);
                        } else {
                            itemView.setVisibility(View.INVISIBLE);
                        }
                    }
                }
            }
        });
    }

    public CardAdapter getAdapter() {
        return adapter;
    }

    /**
     * 设置卡片操作回调
     */
    public void setCardSwitchListener(CardSwitchListener cardSwitchListener) {
        this.cardSwitchListener = cardSwitchListener;
    }

    public void leftSlide() {
        onBtnClick(VANISH_TYPE_LEFT);
    }

    public void rightSlide() {
        onBtnClick(VANISH_TYPE_RIGHT);
    }

    /**
     * 卡片回调接口
     */
    public interface CardSwitchListener {
        /**
         * 新卡片显示回调
         *
         * @param index 最顶层显示的卡片的index
         */
        void onShow(View changedView, int index);

        /**
         * 卡片飞向两侧回调
         *
         * @param index 飞向两侧的卡片数据index
         * @param type  飞向哪一侧{@link #VANISH_TYPE_LEFT}或{@link #VANISH_TYPE_RIGHT}
         */
        void onCardVanish(int index, int type);

        /**
         * 顶层卡片滑动
         *
         * @param scrollProgressPercent
         */
        void onScroll(View changedView, float scrollProgressPercent);

        void itemClick(View changedView, int index);

        void superlike();
    }

}