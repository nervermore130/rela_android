package com.thel.ui.popupwindow

import android.content.Context
import android.content.Intent
import android.graphics.drawable.BitmapDrawable
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.PopupWindow
import com.thel.R
import com.thel.app.TheLApp
import com.thel.base.BaseDataBean
import com.thel.bean.video.VideoBean
import com.thel.constants.TheLConstants
import com.thel.modules.main.home.moments.ReportActivity
import com.thel.network.InterceptorSubscribe
import com.thel.network.RequestConstants
import com.thel.network.service.DefaultRequestService
import com.thel.utils.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.pw_video_more.view.*
import java.util.HashMap

class VideoMorePW(context: Context?, var videoBean: VideoBean?) : PopupWindow() {

    private val tagName: String = "VideoMorePW"

    init {
        initView(context)
    }

    fun initView(context: Context?) {
        contentView = LayoutInflater.from(context).inflate(R.layout.pw_video_more, null)
        width = ViewGroup.LayoutParams.MATCH_PARENT
        height = ViewGroup.LayoutParams.WRAP_CONTENT
        isFocusable = true
        animationStyle = R.style.dialogAnim
        setBackgroundDrawable(BitmapDrawable())
        softInputMode = WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE
        isTouchable = true
        isOutsideTouchable = true


        contentView.report_ll.setOnClickListener {

            videoBean?.userId?.let { uid ->
                run {
                    val intent = Intent(context, ReportActivity::class.java)
                    intent.putExtra(TheLConstants.BUNDLE_KEY_REPORT_TYPE, ReportActivity.REPORT_TYPE_ABUSE)
                    intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, uid)
                    context?.startActivity(intent)

                    dismiss()

                }
            }


        }

        val isCollect = !BusinessUtils.isCollected(videoBean?.id)

        L.d(tagName, " isCollect : $isCollect")

        L.d(tagName, " videoBean.id : ${videoBean?.id}")


        if (!isCollect) {
            contentView.collect_tv.text = TheLApp.context.getString(R.string.collection_cancel)
        } else {
            contentView.collect_tv.text = TheLApp.context.getString(R.string.collection)

        }

        contentView.collect_ll.setOnClickListener {
            videoBean?.id?.let { vid ->

                collect(!BusinessUtils.isCollected(vid), vid)

                dismiss()

            }
        }

        contentView.cancel_ll.setOnClickListener {
            dismiss()
        }

    }

    /**
     * 收藏/取消收藏
     *
     * @param collect true为收藏，false为取消收藏
     */
    private fun collect(collect: Boolean, momentsId: String) {

        val map = HashMap<String, String>()

        //收藏
        if (collect) {

            map[RequestConstants.FAVORITE_COUNT] = momentsId
            map[RequestConstants.FAVORITE_TYPE] = RequestConstants.FAVORITE_TYPE_MOM

            val flowable = DefaultRequestService.createMomentRequestService().getFavoriteCreate(MD5Utils.generateSignatureForMap(map))
            flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : InterceptorSubscribe<BaseDataBean>() {
                override fun onNext(data: BaseDataBean) {
                    super.onNext(data)
                    var collectList = SharedPrefUtils.getString(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.FAVORITE_MOMENT_LIST, "")
                    val id = "[" + ShareFileUtils.getString(ShareFileUtils.ID, "") + "_" + momentsId + "]"
                    collectList += id
                    SharedPrefUtils.setString(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.FAVORITE_MOMENT_LIST, collectList)
                }

                override fun onComplete() {
                    super.onComplete()

                    DialogUtil.showToastShort(TheLApp.context, TheLApp.context.getString(R.string.collection_success))

                }

            })
        } else {                  //取消收藏

            map[RequestConstants.FAVORITE_ID] = ""
            map[RequestConstants.FAVORITE_COUNT] = momentsId
            map[RequestConstants.FAVORITE_TYPE] = RequestConstants.FAVORITE_TYPE_MOM

            val flowable = DefaultRequestService.createMomentRequestService().deleteFavoriteMoment(MD5Utils.generateSignatureForMap(map))
            flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : InterceptorSubscribe<BaseDataBean>() {
                override fun onNext(data: BaseDataBean) {
                    super.onNext(data)
                    var collectList = SharedPrefUtils.getString(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.FAVORITE_MOMENT_LIST, "")
                    val id = "[" + ShareFileUtils.getString(ShareFileUtils.ID, "") + "_" + momentsId + "]"
                    collectList = collectList.replace(id, "")
                    SharedPrefUtils.setString(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.FAVORITE_MOMENT_LIST, collectList)
                    DialogUtil.showToastShort(TheLApp.context, TheLApp.context.getString(R.string.collection_canceled))
                }
            })
        }
    }
}