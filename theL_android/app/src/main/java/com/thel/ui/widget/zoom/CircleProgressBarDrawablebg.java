package com.thel.ui.widget.zoom;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;

import com.facebook.drawee.drawable.ProgressBarDrawable;
import com.thel.R;
import com.thel.app.TheLApp;

/**
 * Created by test1 on 2017/8/21.
 */

public class CircleProgressBarDrawablebg extends ProgressBarDrawable {
    private final Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private int mLevel = 0;
    private int maxLevel = 10000;
    private int radius = 10;//背景圆角

    @Override
    protected boolean onLevelChange(int level) {
        mLevel = level;
        invalidateSelf();
        return true;
    }

    @Override
    public void draw(Canvas canvas) {
        if (getHideWhenZero() && mLevel == 0) {
            return;
        }
        drawBackground(canvas);
        drawBar(canvas, mLevel, getColor());
    }


    /**
     * 用来画背景图
     *
     * @param canvas
     */
    private void drawBackground(Canvas canvas) {
        final Rect bounds = getBounds(); //屏幕的宽高
        int minLength = Math.min(bounds.width(), bounds.height());
        float bgSize = minLength * 0.12f;
        RectF rectF = new RectF(bounds.centerX() - bgSize / 2, bounds.centerY() - bgSize / 2, bounds.centerX() + bgSize / 2, bounds.centerY() + bgSize / 2);
        mPaint.setColor(TheLApp.getContext().getResources().getColor(R.color.black_transprent_30));
        mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        mPaint.setStrokeWidth(1);
        canvas.drawRoundRect(rectF, radius, radius, mPaint);
    }

    private void drawBar(Canvas canvas, int level, int color) {
        Rect bounds = getBounds();
        int minLength = Math.min(bounds.width(), bounds.height());
        //float progressSize = minLength * 0.13f;
        float progressSize = minLength * 0.08f;
        RectF rectF = new RectF(bounds.centerX() - progressSize / 2, bounds.centerY() - progressSize / 2, bounds.centerX() + progressSize / 2, bounds.centerY() + progressSize / 2);
        mPaint.setColor(color);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(8);  //progress的宽
        if (level != 0) {
            canvas.drawArc(rectF, 0, (float) (level * 360 / maxLevel), false, mPaint);
        } else {
            canvas.drawArc(rectF, 0, (float) (10 * 360 / maxLevel), false, mPaint);
        }
    }
}
