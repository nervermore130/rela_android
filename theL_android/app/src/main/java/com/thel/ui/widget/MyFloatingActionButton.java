package com.thel.ui.widget;

import android.content.Context;
import android.os.Build;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import android.util.AttributeSet;

/**
 * Created by the L on 2016/12/16.
 */

public class MyFloatingActionButton extends FloatingActionButton {
    public boolean mIsAnimatingOut = false;
    public boolean isNeverShow = false;
    public boolean isFirstPageOut = false;

    public MyFloatingActionButton(Context context) {
        this(context,null);
    }

    public MyFloatingActionButton(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    public MyFloatingActionButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    private void init(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setElevation(0);
        }
    }

    @Override
    public void setPadding(int left, int top, int right, int bottom) {
    }
}
