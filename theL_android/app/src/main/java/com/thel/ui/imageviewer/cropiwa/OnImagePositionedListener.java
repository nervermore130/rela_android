package com.thel.ui.imageviewer.cropiwa;

import android.graphics.RectF;

/**
 * @author Yaroslav Polyakov
 * 25.02.2017.
 */

interface OnImagePositionedListener {
    void onImagePositioned(RectF imageRect);
}
