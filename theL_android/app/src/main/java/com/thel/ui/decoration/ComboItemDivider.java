package com.thel.ui.decoration;

import android.graphics.Rect;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

/**
 * Created by liuyun on 2017/12/29.
 */

public class ComboItemDivider extends RecyclerView.ItemDecoration {

    private int space;

    public ComboItemDivider(int space) {
        this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {

        int position = parent.getChildLayoutPosition(view);

//        if (position == 1 || position == 4) {
//            outRect.left = space;
//            outRect.right = space;
//        }

        if (position > 2) {
            outRect.top = space;
        }

    }
}
