package com.thel.ui.dialog;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;

import androidx.fragment.app.DialogFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FreeGoldDialogFragment extends DialogFragment {

    @BindView(R.id.content_tv) TextView content_tv;

    private OnAcceptListener mOnAcceptListener;

    private int gold = 0;

    @Nullable @Override public View onCreateView(@Nullable LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_free_gold, container, false);

        ButterKnife.bind(this, view);

        return view;
    }

    @Override public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        content_tv.setText(TheLApp.getContext().getString(R.string.free_gold2, gold));
    }

    public void setGold(int gold) {
        this.gold = gold;
    }

    public void setOnAcceptListener(OnAcceptListener mOnAcceptListener) {
        this.mOnAcceptListener = mOnAcceptListener;
    }

    @OnClick(R.id.close_iv) void onClose() {
        dismiss();
        if (mOnAcceptListener != null) {
            mOnAcceptListener.onClose();
        }
    }

    @OnClick(R.id.get_it_right_now_tv) void getItRightNow() {
        dismiss();
        if (mOnAcceptListener != null) {
            mOnAcceptListener.onAccept();
        }
    }

    public interface OnAcceptListener {
        void onClose();

        void onAccept();
    }
}
