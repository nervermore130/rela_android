package com.thel.ui;

import android.content.Context;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.util.AttributeSet;

import com.thel.app.TheLApp;
import com.thel.utils.SizeUtils;

public class RechargeRecyclerView extends RecyclerView {

    private int itemHeight = SizeUtils.dip2px(TheLApp.context, 62);

    private int spaceHeight = SizeUtils.dip2px(TheLApp.context, 10);

    public RechargeRecyclerView(Context context) {
        super(context);
    }

    public RechargeRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public RechargeRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override protected void onMeasure(int widthSpec, int heightSpec) {

        int height = itemHeight * 2 + spaceHeight * 6;

        heightSpec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);

        super.onMeasure(widthSpec, heightSpec);
    }
}
