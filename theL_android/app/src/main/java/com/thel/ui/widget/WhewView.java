package com.thel.ui.widget;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.DecelerateInterpolator;

import com.thel.utils.L;
import com.thel.utils.SizeUtils;

import java.util.ArrayList;
import java.util.List;

import static com.thel.ui.widget.WhewView.Status.GRAY;
import static com.thel.ui.widget.WhewView.Status.GREEN;
import static com.thel.ui.widget.WhewView.Status.NONE;
import static com.thel.ui.widget.WhewView.Status.RED;

/**
 * Created by liuyun on 2017/12/4.
 */

public class WhewView extends View {

    private Paint paint;
    private int maxWidth = 255;
    private boolean isStarting = false;
    private List<String> alphaList = new ArrayList<>();
    private List<String> startWidthList = new ArrayList<>();
    private float concentricCircleWidth,concentricCircleWidth1;//同心圆半径
    private int avatarRadius = 0;
    private Status mStatus = NONE;

    private boolean isLinkMic = true;

    public enum Status {
        NONE, GREEN, RED, GRAY
    }

    public WhewView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public WhewView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public WhewView(Context context) {
        super(context);
        init();
    }

    public void isLinkMic(boolean isLinkMic) {
        this.isLinkMic = isLinkMic;
    }

    private void init() {

        maxWidth = SizeUtils.dip2px(getContext(), 70 + 38);

        paint = new Paint();
        paint.setFakeBoldText(true);
        paint.setColor(0x4000bfc1);
        alphaList.add("150");
        startWidthList.add("0");

        concentricCircleWidth1 = SizeUtils.dip2px(getContext(), 6.3f);

        concentricCircleWidth = SizeUtils.dip2px(getContext(), 38);

        avatarRadius = SizeUtils.dip2px(getContext(), 70);

    }

    public void setStatus(Status mStatus) {
        this.mStatus = mStatus;
        invalidate();
    }


    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (mStatus == NONE) {
            // 依次绘制 同心圆

            setBackgroundColor(Color.TRANSPARENT);// 颜色：完全透明

            for (int i = 0; i < alphaList.size(); i++) {
                int alpha = Integer.parseInt(alphaList.get(i));
                // 圆半径
                int startWidth = Integer.parseInt(startWidthList.get(i));

                paint.setAlpha(alpha);
                // 这个半径决定你想要多大的扩散面积
                canvas.drawCircle(getWidth() / 2, getHeight() / 2, avatarRadius / 5 + startWidth,
                        paint);
                // 同心圆扩散
                if (isStarting && alpha > 0 && startWidth < maxWidth) {
                    alphaList.set(i, (alpha - 1) + "");
                    startWidthList.set(i, (startWidth + 1) + "");
                }
            }
            if (isStarting
                    && Integer
                    .parseInt(startWidthList.get(startWidthList.size() - 1)) == maxWidth / 8) {
                alphaList.add("150");
                startWidthList.add("0");
            }
            // 同心圆数量达到10个，删除最外层圆
            if (isStarting && startWidthList.size() == 5) {
                startWidthList.remove(0);
                alphaList.remove(0);
            }
            // 刷新界面
            invalidate();

//            if (isLinkMic) {
//                paint.setColor(0x4000bfc1);
//            } else {
//                paint.setColor(0x40ff6666);
//            }
//
//            canvas.drawCircle(getWidth() / 2, getHeight() / 2, avatarRadius / 2 + mOffSet,
//                    paint);
//            if (isLinkMic) {
//                paint.setColor(0x4000bfc1);
//            } else {
//                paint.setColor(0x26ff6666);
//            }
//            canvas.drawCircle(getWidth() / 2, getHeight() / 2, avatarRadius / 2 + mOffSet * 2,
//                    paint);
//            if (isLinkMic) {
//                paint.setColor(0x4000bfc1);
//            } else {
//                paint.setColor(0x0Dff6666);
//            }
//            canvas.drawCircle(getWidth() / 2, getHeight() / 2, avatarRadius / 2 + mOffSet * 3,
//                    paint);

        } else {

            setBackgroundColor(Color.TRANSPARENT);// 颜色：完全透明

            int color = 0x5963eeb1;

            if (mStatus == GREEN) {
                color = 0x5963eeb1;
            }

            if (mStatus == RED) {
                color = 0x59ff6666;
            }

            if (mStatus == GRAY) {
                color = 0x59d3d3d3;
            }

            paint.setColor(color);
            canvas.drawCircle(getWidth() / 2, getHeight() / 2, avatarRadius / 2 + concentricCircleWidth1,
                    paint);
        }
    }

    // 执行动画
    public void start() {
        isStarting = true;
//        startAnimator();
//        startCircleOne();
//        postDelayed(new Runnable() {
//            @Override public void run() {
//                startCircleTwo();
//            }
//        }, 200);
    }

    // 停止动画
    public void stop() {
        isStarting = false;
//        if (valueAnimator != null) {
//            valueAnimator.cancel();
//            valueAnimator = null;
//        }
    }

    // 判断是都在不在执行
    public boolean isStarting() {
        return isStarting;
    }

    private ValueAnimator valueAnimator = null;

    private float mOffSet = 0;

    private float mOffSetOne = 0;

    private float mOffSetTwo = 0;

    private float mAlphaOne = 0;

    private float mAlphaTwo = 0;

    private void startAnimator() {
        valueAnimator = ValueAnimator.ofFloat(0, concentricCircleWidth);
        valueAnimator.setRepeatMode(ValueAnimator.RESTART);
        valueAnimator.setRepeatCount(ValueAnimator.INFINITE);
        valueAnimator.setDuration(1000);
        valueAnimator.setInterpolator(new DecelerateInterpolator());
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override public void onAnimationUpdate(ValueAnimator animation) {
                mOffSet = (float) animation.getAnimatedValue();
                invalidate();
            }
        });
        valueAnimator.start();
    }

    @Override protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        start();
    }

    @Override protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        stop();
    }
}
