package com.thel.ui.guide;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import androidx.annotation.Nullable;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;

import com.thel.app.TheLApp;
import com.thel.utils.Utils;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by waiarl on 2018/2/2.
 */

public class NewerGuideView extends View {

    private final Context mContext;
    private Paint mPaint;
    private int bgColor;
    private RectF mTargetRectF;
    private String[] mGuideText;
    private int mWidth;
    private int mHeight;
    private int mGuideWidth;
    private int mGuideHeight;
    private TextPaint mTextPaint;
    private int textDivider;
    private int mBitmapTopMargin;
    private int mBitmapBottomMargin;
    private int mBitmapHorizontalMargin;
    private int mGuideRes = -1;
    private int mTextSize;
    private int mTextColor;
    private Typeface mTextStyle;
    private float mCenterX;
    private float mCenterY;
    private PointF offsetPointF = new PointF();
    private GuideClickListener listener;
    private float mRectRadius;
    private int mGuideGravity = Gravity.TOP;//默认为top


    public NewerGuideView(Context context) {
        this(context, null);
    }

    public NewerGuideView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NewerGuideView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }

    private void init() {
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        bgColor = Color.argb((int) (255 * 0.65), 0, 0, 0);
        textDivider = dip2Px(5);
        mBitmapTopMargin = dip2Px(5);
        mBitmapBottomMargin = dip2Px(5);
        mBitmapHorizontalMargin = dip2Px(10);
        mTextSize = dip2Px(14);
        mTextColor = Color.WHITE;
        mTextStyle = Typeface.create(Typeface.SANS_SERIF, Typeface.BOLD);
        mRectRadius = 20f;
        mTargetRectF = null;
    }

    public NewerGuideView initView(int guideRes, int guideWidth, int guideHeight, RectF targetRectF, String[] guideText) {
        if (targetRectF == null) {
            return this;
        }
        mGuideRes = guideRes;//引导支援图片
        mGuideWidth = guideWidth;//引导图片的宽度
        mGuideHeight = guideHeight;//引导图片吧的高度
        mTargetRectF = targetRectF;//画空白的矩形左边
        mGuideText = guideText;//引导内容
        mCenterX = targetRectF.centerX() + offsetPointF.x;//左右绘制的时候的中心点
        mCenterY = targetRectF.centerY() + offsetPointF.y;
        invalidate();
        return this;
    }

    public NewerGuideView initView(int guideRes, int guideWidth, int guideHeight, RectF targetRectF, String[] guideText, PointF offsetPointF) {
        if (offsetPointF != null) {
            this.offsetPointF = offsetPointF;//中心点的偏移值
        }
        return initView(guideRes, guideWidth, guideHeight, targetRectF, guideText);
    }

    public NewerGuideView initView(int guideRes, int guideWidth, int guideHeight, RectF targetRectF, String[] guideText, PointF offsetPointF, int guideGravity) {
        if (guideGravity == Gravity.TOP || guideGravity == Gravity.BOTTOM) {
            this.mGuideGravity = guideGravity;
        }
        return initView(guideRes, guideWidth, guideHeight, targetRectF, guideText, offsetPointF);
    }

    public NewerGuideView setGravite(int gravity) {
        if (gravity == Gravity.TOP || gravity == Gravity.BOTTOM) {
            this.mGuideGravity = gravity;
            invalidate();
        }
        return this;

    }


    @Override
    protected void onDraw(Canvas canvas) {
        mWidth = getMeasuredWidth();
        mHeight = getMeasuredHeight();
        saveLayer(canvas);
        drawbackground(canvas);
        drawRect(canvas);
        drawBitmap(canvas);
        drawText(canvas);
    }

    private void drawRect(Canvas canvas) {
        if (mTargetRectF == null) {
            return;
        }
        mPaint.setColor(Color.TRANSPARENT);
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setXfermode(new PorterDuffXfermode((PorterDuff.Mode.SRC)));
        canvas.drawRoundRect(mTargetRectF, mRectRadius, mRectRadius, mPaint);
    }

    private void drawbackground(Canvas canvas) {
        canvas.drawColor(bgColor);
    }

    private void saveLayer(Canvas canvas) {
        canvas.saveLayer(0, 0, mWidth, mHeight, null, Canvas.ALL_SAVE_FLAG);
    }

    private void drawBitmap(Canvas canvas) {
        if (mGuideRes == -1 || mGuideHeight == 0) {
            return;
        }
        try {

            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            final InputStream in = getResources().openRawResource(mGuideRes);//比decodeResource平均耗时不到其一半
            if (in == null) {
                return;
            }
            //预读取图片资源的宽高，
            final Bitmap bitmap = BitmapFactory.decodeStream(in, null, options);
            in.close();
            final int width = options.outWidth;
            final int height = options.outHeight;
            if (width == 0 || height == 0) {
                return;
            }
            //根据原始图的比例，以高不变为基准，从新设置本地显示的bitmap的宽度
            final float scT = width / (float) height;
            mGuideWidth = (int) (mGuideHeight * scT);
            final InputStream is = getResources().openRawResource(mGuideRes);//比decodeResource平均耗时不到其一半
            final Bitmap bmp = BitmapFactory.decodeStream(is);
            final int startX = (int) (mCenterX - mGuideWidth / 2);
            int startY;
            if (mGuideGravity == Gravity.BOTTOM) {
                startY = (int) (mTargetRectF.bottom + mBitmapTopMargin);
            } else {
                startY = (int) (mTargetRectF.top - mBitmapBottomMargin - mGuideHeight);
            }
            mPaint.setXfermode(null);
            canvas.save();
            final Rect dst = new Rect(startX, startY, startX + mGuideWidth, startY + mGuideHeight);
            canvas.drawBitmap(bmp, null, dst, null);
            bmp.recycle();
            canvas.restore();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void drawText(Canvas canvas) {
        if (mGuideText == null || mGuideText.length == 0) {
            return;
        }
        mTextPaint = new TextPaint();
        mTextPaint.setColor(mTextColor);
        mTextPaint.setAntiAlias(true);
        mTextPaint.setXfermode(null);
        mTextPaint.setTextSize(mTextSize);
        mTextPaint.setTypeface(mTextStyle);
        final int length = mGuideText.length;
        final Rect[] boundRect = new Rect[length];
        for (int i = 0; i < length; i++) {
            boundRect[i] = new Rect();
        }
        final int[] rectWidths = new int[length];
        for (int i = 0; i < length; i++) {
            final String message = mGuideText[i];
            mTextPaint.getTextBounds(message, 0, message.length(), boundRect[i]);
        }
        for (int i = 0; i < length; i++) {
            rectWidths[i] = boundRect[i].width();
        }
        final int maxWidth = getMaxWidth(rectWidths);
        final int leftLeaveWidth = (int) (mCenterX - mGuideWidth / 2);//箭头距离屏幕左边距离
        final int rightLeaveWidth = (int) (mWidth - (mCenterX + mGuideWidth / 2));//箭头距离屏幕右边距离
        final int minLeaveWidth = Math.min(leftLeaveWidth, rightLeaveWidth);//距离两边的最小距离
        final int maxLeaveWidth = Math.max(leftLeaveWidth, rightLeaveWidth);//距离两边最大距离
        final int textHeight = boundRect[0].height();

        final int bottomGravityTY = (int) (mTargetRectF.bottom + (mBitmapTopMargin + mGuideHeight + mBitmapBottomMargin));//gravity为bottom下的起点
        final Paint.FontMetricsInt fm = mTextPaint.getFontMetricsInt();
        final int baseLineH = 0 - fm.top;//基线基于起点的高度
        final int bottomGravityY = bottomGravityTY + baseLineH;

        int startX, startY;

        if (2 * minLeaveWidth > maxWidth) {//如果距离两边最小距离大于文字最大长度，则可以居中绘制文字
            startX = (int) (mCenterX - maxWidth / 2);
            if (mGuideGravity == Gravity.BOTTOM) {
                startY = bottomGravityY;
            } else {
                startY = (int) (mTargetRectF.top - (mBitmapBottomMargin + mGuideHeight + mBitmapTopMargin + length * textHeight + (length - 1) * textDivider));
            }
            for (int i = 0; i < length; i++) {
                final int y = startY + i * textHeight + i * textDivider;
                final int x = startX;
                canvas.drawText(mGuideText[i], x, y, mTextPaint);
            }
        } else if (maxLeaveWidth - mBitmapHorizontalMargin < maxWidth) {
            //如果距离两边的最大距离减去margin值小于文字最大长度，不能绘制左边也不能绘制右边的话，则建议尽量居中
            //此时有两种情况，一：view总宽度大于文字最大宽度，则尽量居中，2：view总宽度小于文字最大宽度，此时，只能把文字换行
            if (mWidth >= maxWidth) {
                startX = (mWidth - maxWidth) / 2;
                if (mGuideGravity == Gravity.BOTTOM) {
                    startY = bottomGravityY;
                } else {
                    startY = (int) (mTargetRectF.top - (mBitmapBottomMargin + mGuideHeight + mBitmapTopMargin + length * textHeight + (length - 1) * textDivider));
                }
                for (int i = 0; i < length; i++) {
                    final int y = startY + i * textHeight + i * textDivider;
                    final int x = startX;
                    canvas.drawText(mGuideText[i], x, y, mTextPaint);
                }
            } else {//view总宽度小于文字最大宽度，此时，只能把文字换行
                startX = 0;
                final int[] heights = new int[length];
                for (int i = 0; i < length; i++) {
                    final StaticLayout myStaticLayout = new StaticLayout(mGuideText[i], mTextPaint, canvas.getWidth(), Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
                    final int height = myStaticLayout.getHeight();
                    heights[i] = height;//获取没个字符串的高度，由于有换行，所以每个字符串高度不一样
                }
                final int totalHeight = getTotalHeight(heights);//所有字符串换行后 的总高度
                if (mGuideGravity == Gravity.BOTTOM) {
                    startY = bottomGravityY;
                } else {
                    startY = (int) (mTargetRectF.top - (mBitmapBottomMargin + mGuideHeight + mBitmapTopMargin + totalHeight + (length - 1) * textDivider));
                }
                for (int i = 0; i < length; i++) {//绘制
                    canvas.save();
                    final int x = startX;
                    final int y = getYByIndex(i, startY, heights, textDivider);
                    final StaticLayout myStaticLayout = new StaticLayout(mGuideText[i], mTextPaint, canvas.getWidth(), Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
                    canvas.translate(x, y);
                    myStaticLayout.draw(canvas);
                    canvas.restore();
                }
            }
        } else if (maxLeaveWidth - mBitmapHorizontalMargin >= maxWidth) {//如果最大边距减去margin值大于 最大文字宽度
            if (mGuideGravity == Gravity.BOTTOM) {
                startY = bottomGravityY;
            } else {
                startY = (int) (mTargetRectF.top - (mBitmapBottomMargin + length * textHeight + (length - 1) * textDivider));
            }
            if (leftLeaveWidth == maxLeaveWidth) {//如果是左边距离大,则文字右对齐
                startX = leftLeaveWidth - mBitmapHorizontalMargin - maxWidth;
                for (int i = 0; i < length; i++) {
                    final int x = leftLeaveWidth - mBitmapHorizontalMargin - rectWidths[i];
                    final int y = startY + i * textHeight + i * textDivider;
                    canvas.drawText(mGuideText[i], x, y, mTextPaint);
                }

            } else {//如果是右边距离大,文字左对齐
                startX = (int) (mCenterX + mGuideWidth / 2 + mBitmapHorizontalMargin);
                for (int i = 0; i < length; i++) {
                    final int x = startX;
                    final int y = startY + i * textHeight + i * textDivider;
                    canvas.drawText(mGuideText[i], x, y, mTextPaint);
                }
            }
        }
    }

    private int getTotalHeight(int[] heights) {
        int height = 0;
        if (heights == null) {
            return height;
        }
        final int length = heights.length;
        for (int i = 0; i < length; i++) {
            height += heights[i];
        }
        return height;
    }

    private int getYByIndex(int index, int startY, int[] heights, int textDivider) {
        int y = startY;
        if (heights == null) {
            return y;
        }
        int i = 1;
        final int length = heights.length;

        while (i <= index && i < length) {
            y += heights[i - 1] + textDivider;
            i += 1;
        }
        return y;
    }

    private int getMaxWidth(int[] arr) {
        if (arr == null) {
            return 0;
        }
        int max = arr[0];
        final int length = arr.length;
        for (int i = 1; i < length; i++) {
            if (max < arr[i]) {
                max = arr[i];
            }
        }
        return max;
    }

    private int dip2Px(float dip) {
        return Utils.dip2px(TheLApp.getContext(), dip);
    }

    /************************************************一下为点击事件处理********************************************************/
    long startTime = 0;
    int startX = 0, startY = 0;
    static final long CLICK_TIME = 300;
    long lastClickTime;
    static final long PREVENT_TIME = 1000;//1秒的点击保护时间


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_UP:
                final long currentTime = System.currentTimeMillis();
                if (currentTime - startTime < CLICK_TIME && currentTime - lastClickTime > PREVENT_TIME) {
                    judgeClick(startX, startY, event.getX(), event.getY());
                }
                break;
            case MotionEvent.ACTION_DOWN:
                startTime = System.currentTimeMillis();
                startX = (int) event.getX();
                startY = (int) event.getY();

                break;
        }
        // return super.onTouchEvent(event);//不拦截
        return true;//拦截，走listener
    }

    private void judgeClick(int startX, int startY, float endX, float endY) {
        final RectF rectF = new RectF(Math.min(startX, endX), Math.min(startY, endY), Math.max(startX, endX), Math.max(startY, endY));
        if (mTargetRectF != null) {
            if (rectF.left > mTargetRectF.left && rectF.right < mTargetRectF.right && rectF.bottom < mTargetRectF.bottom && rectF.top > mTargetRectF.top) {//区域是在目标区域内部
                if (listener != null) {
                    listener.clickTarget();
                }
            } else if (rectF.right < mTargetRectF.left || rectF.left > mTargetRectF.right || rectF.top < mTargetRectF.bottom || rectF.bottom > mTargetRectF.top) {//区域是在目标区域外部
                if (listener != null) {
                    listener.clickOutSide();
                }
            }
        }

    }

    public void setGuideClickListener(GuideClickListener listener) {
        this.listener = listener;
    }

    public interface GuideClickListener {
        void clickTarget();

        void clickOutSide();
    }

}
