package com.thel.ui.widget

import android.content.Context
import android.content.Intent
import android.text.TextUtils
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.SeekBar
import com.thel.R
import com.thel.app.TheLApp
import com.thel.bean.RecommendedModel
import com.thel.bean.moments.MomentsBean
import com.thel.bean.video.VideoBean
import com.thel.constants.MomentTypeConstants
import com.thel.constants.TheLConstants
import com.thel.flutter.bridge.FlutterRouterConfig
import com.thel.growingio.GrowingIoConstant
import com.thel.imp.follow.FollowStatusChangedImpl
import com.thel.imp.like.MomentLikeUtils
import com.thel.manager.ImageLoaderManager
import com.thel.modules.live.bean.LiveRoomBean
import com.thel.modules.main.home.moments.SendCardActivity
import com.thel.utils.*
import kotlinx.android.synthetic.main.layout_video_bottom.view.*
import java.lang.Exception

class VideoBottomLayout : LinearLayout {

    private var time: Int = 0

    lateinit var onFullScreenListener: OnFullScreenListener

    lateinit var onPlayStatusListener: OnPlayStatusListener

    lateinit var onSeekListener: OnSeekListener

    constructor(context: Context?) : this(context, null)
    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        context?.let { initView(it) }
    }

    fun initView(context: Context) {

        LayoutInflater.from(context).inflate(R.layout.layout_video_bottom, this, true)

    }

    fun setData(videoBean: VideoBean?) {

        videoBean?.let {

            time = it.playTime

            ImageLoaderManager.imageLoaderDefaultCircle(avatar_iv, R.mipmap.icon_user, videoBean.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE)

            nickname_tv.text = it.nickname

            follow_tv.text = getFollowText(it.isFollow)

            follow_tv.setTextColor(getFollowTextColor(it.isFollow))

            if (!TextUtils.isEmpty(it.text)) {
                MomentUtils.setMomentContent(moment_text_tv, it.text)
            }

            like_count_tv.text = "${videoBean.winkNum}"

            comment_count_tv.text = "${videoBean.commentNum}"

            if (videoBean.winkFlag != 0) {
                moment_opts_like.setChecked(true, 1)
            } else {
                moment_opts_like.setChecked(false, 1)
            }

            try {

                if (videoBean.pixelWidth > 0 && videoBean.pixelHeight > 0 && (videoBean.pixelWidth / videoBean.pixelHeight > 1)) {
                    full_screen_iv.visibility = View.VISIBLE
                } else {
                    full_screen_iv.visibility = View.GONE
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }


            moment_opts_like.setOnClickListener {

                ViewUtils.preventViewMultipleClick(it, 1000)
                if (!MomentUtils.isBlackOrBlock(videoBean.id + "")) {

                    if (videoBean.winkFlag == MomentsBean.HAS_NOT_WINKED) {
                        videoBean.winkNum++
                        MomentLikeUtils.likeMoment(videoBean.id)
                        videoBean.winkFlag = MomentsBean.HAS_WINKED
                        moment_opts_like.check()
                        moment_opts_like.setChecked(true, 1)
                        BusinessUtils.playSound(R.raw.sound_emoji)
                    } else {
                        videoBean.winkNum--
                        MomentLikeUtils.unLikeMoment(videoBean.id)
                        videoBean.winkFlag = MomentsBean.HAS_NOT_WINKED
                        moment_opts_like.setChecked(false, 1)
                    }
                    like_count_tv.text = "${videoBean.winkNum}"

                    val broadIntent = Intent(TheLConstants.BROADCAST_VIDEO_COUNT)
                    broadIntent.putExtra(TheLConstants.BUNDLE_KEY_VIDEO_BEAN, videoBean)
                    context.sendBroadcast(broadIntent)
                }
            }
        }

        play_or_pause_iv.isSelected = true

        full_screen_iv.isSelected = true

        play_or_pause_iv.setOnClickListener {
            play_or_pause_iv.isSelected = !play_or_pause_iv.isSelected

            onPlayStatusListener.onPlayStatus(if (play_or_pause_iv.isSelected) {
                1
            } else {
                0
            })
        }

        full_screen_iv.setOnClickListener {
            full_screen_iv.isSelected = !full_screen_iv.isSelected
            onFullScreenListener.onFullScreenStatus(if (full_screen_iv.isSelected) {
                0
            } else {
                1
            })
        }

        follow_tv.setOnClickListener {
            if (videoBean != null) {
                if (videoBean.isFollow == 0 || videoBean.isFollow == 2) {
                    videoBean.isFollow++
                    FollowStatusChangedImpl.followUser(videoBean.userId, FollowStatusChangedImpl.ACTION_TYPE_FOLLOW, videoBean.nickname, videoBean.avatar)
                } else {
                    videoBean.isFollow--
                    FollowStatusChangedImpl.followUser(videoBean.userId, FollowStatusChangedImpl.ACTION_TYPE_CANCEL_FOLLOW, videoBean.nickname, videoBean.avatar)
                }
                follow_tv.text = getFollowText(videoBean.isFollow)
                follow_tv.setTextColor(getFollowTextColor(videoBean.isFollow))
            }
        }

        video_sb.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                if (seekBar != null) {
                    val startTime = Utils.formatTime2((progress / 1000).toLong())

                    val endTime = Utils.formatTime2((seekBar.max / 1000).toLong())

                    val time = "$startTime/$endTime"

                    current_time_tv.text = time

                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                if (seekBar != null) {
                    onSeekListener.onSeek(seekBar.progress * 1000)
                }
            }

        })

        like_ll.setOnClickListener {

        }

        comment_ll.setOnClickListener {

            videoBean?.id?.let { vid -> FlutterRouterConfig.gotoMomentDetails(vid) }

        }

        recommend_ll.setOnClickListener {
            ViewUtils.preventViewMultipleClick(it, 2000)
            if (!MomentUtils.isBlackOrBlock(videoBean?.id)) {
                SharedPrefUtils.setBoolean(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.NEW_SHARE_MOMENT, true)
                if (videoBean != null) {
                    val recommendedModel = RecommendedModel()
                    recommendedModel.momentsType = videoBean.type
                    recommendedModel.momentsId = videoBean.id.toLong()
                    recommendedModel.momentsText = videoBean.text
                    recommendedModel.avatar = videoBean.avatar
                    recommendedModel.nickname = videoBean.nickname
                    recommendedModel.imageUrl = videoBean.image
                    recommendedModel.pixelHeight = videoBean.pixelHeight
                    recommendedModel.pixelWidth = videoBean.pixelWidth
                    recommendedModel.userId = Integer.valueOf(videoBean.userId)
                    recommendedModel.momentsType = MomentTypeConstants.MOMENT_TYPE_VIDEO
                    FlutterRouterConfig.nativeShowRecommendDialog(GsonUtils.createJsonString(recommendedModel))
                }
            }
        }

    }

    fun setPlayTime(currentPosition: Long, duration: Long) {

        video_sb.max = (duration / 1000).toInt()

        video_sb.progress = (currentPosition / 1000).toInt()

        val startTime = Utils.formatTime2(currentPosition / 1000)

        val endTime = Utils.formatTime2(duration / 1000)

        val time = "$startTime/$endTime"

        current_time_tv.text = time

        short_video_sb.max = duration.toInt()

        short_video_sb.progress = currentPosition.toInt()

    }

    fun play() {
        play_or_pause_iv.isSelected = true
    }

    fun pause() {
        play_or_pause_iv.isSelected = false
    }

    private fun getFollowText(followStatus: Int): String {

        when (followStatus) {
            0 -> {
                return TheLApp.getContext().getString(R.string.userinfo_activity_follow)
            }
            1 -> {
                return TheLApp.getContext().getString(R.string.userinfo_activity_followed)
            }
            2 -> {
                return TheLApp.getContext().getString(R.string.repowder)
            }
            3 -> {
                return TheLApp.getContext().getString(R.string.interrelated)
            }
            else -> {
                return TheLApp.getContext().getString(R.string.userinfo_activity_follow)
            }
        }

    }

    private fun getFollowTextColor(followStatus: Int): Int {

        when (followStatus) {
            0 -> {
                return R.color.rela_color_new
            }
            1 -> {
                return R.color.follow_text_color_grey
            }
            2 -> {
                return R.color.rela_color_new
            }
            3 -> {
                return R.color.follow_text_color_grey
            }
            else -> {
                return R.color.rela_color_new
            }
        }

    }

}

interface OnFullScreenListener {

    fun onFullScreenStatus(fullScreenStatus: Int)

}

interface OnPlayStatusListener {
    fun onPlayStatus(playStatus: Int)
}

interface OnSeekListener {

    fun onSeek(duration: Int)
}