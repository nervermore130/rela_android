package com.thel.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.imp.follow.FollowStatusChangedImpl;
import com.thel.ui.widget.popupwindow.UnFollowPopupWindow;

public class FollowView extends androidx.appcompat.widget.AppCompatTextView implements View.OnClickListener {

    private int followStatus;

    private String userId;

    private String nickName;

    private String avatar;

    public FollowView(Context context) {
        super(context);
        initView();
    }

    public FollowView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public FollowView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        setOnClickListener(this);
//        setBackgroundResource(R.drawable.selector_follow);
//        setTextColor(getResources().getColor(R.color.selector_follow_text));
//        setGravity(Gravity.CENTER);
//        setMinWidth(DensityUtils.dp2px(50));
//        setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    }

    public void setFollowData(String userId, String nickName, String avatar, int followStatus) {

        this.userId = userId;

        this.nickName = nickName;

        this.avatar = avatar;

        this.followStatus = followStatus;

        updateUI();
    }

    private void updateUI() {

        String text = "";

        switch (followStatus) {
            case 0:
                setSelected(true);
                text = TheLApp.getContext().getString(R.string.userinfo_activity_follow);
                break;
            case 1:
                setSelected(false);
                text = TheLApp.getContext().getString(R.string.userinfo_activity_followed);
                break;
            case 2:
                setSelected(true);
                text = TheLApp.getContext().getString(R.string.repowder);
                break;
            case 3:
                setSelected(false);
                text = TheLApp.getContext().getString(R.string.interrelated);
                break;
            default:
                setVisibility(View.GONE);
                break;
        }

        setText(text);
    }


    @Override
    public void onClick(View v) {
        if (followStatus == 0 || followStatus == 2) {
            followStatus++;
            updateUI();
            FollowStatusChangedImpl.followUser(userId, FollowStatusChangedImpl.ACTION_TYPE_FOLLOW, nickName, avatar);
        } else {
            UnFollowPopupWindow mUnFollowPopupWindow = new UnFollowPopupWindow(getContext(), nickName);
            mUnFollowPopupWindow.setOnUnFollowListener(new UnFollowPopupWindow.OnUnFollowListener() {
                @Override
                public void onUnFollow() {
                    followStatus--;
                    updateUI();
                    FollowStatusChangedImpl.followUser(userId, FollowStatusChangedImpl.ACTION_TYPE_CANCEL_FOLLOW, nickName, avatar);
                }
            });
            mUnFollowPopupWindow.showAtLocation(this, Gravity.BOTTOM, 0, 0);
        }
    }
}
