package com.thel.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.thel.R;
import com.thel.bean.RecommendListBean;
import com.thel.constants.QueueConstants;
import com.thel.constants.TheLConstants;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.preview_image.UserInfoPhotoActivity;
import com.thel.ui.widget.GuideAvatarLayout;
import com.thel.utils.SizeUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by liuyun on 2017/12/26.
 */

public class RecommendUserAdapter extends RecyclerView.Adapter<RecommendUserAdapter.RecommendUserViewHolder> {

    public Context context;

    private List<RecommendListBean.GuideDataBean> data = new ArrayList<>();

    private int picWidth = 0;

    private float cornerWidth = 0;


    public RecommendUserAdapter(Context context) {
        this.context = context;
        picWidth = SizeUtils.dip2px(context, 50);
        cornerWidth = SizeUtils.dip2px(context, 5);
    }

    @Override
    public RecommendUserViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_recommend, viewGroup, false);
        return new RecommendUserViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public void onBindViewHolder(final RecommendUserViewHolder recommendUserViewHolder, final int position) {

        RecommendListBean.GuideDataBean item = data.get(position);

        ImageLoaderManager.imageLoaderCircle(recommendUserViewHolder.avatar_iv, R.mipmap.icon_user, item.avatar);

        recommendUserViewHolder.nickname_tv.setText(item.nickName);

        if (item.verifyType == 0) {
            recommendUserViewHolder.verify_person_iv.setVisibility(View.GONE);
        } else {
            recommendUserViewHolder.verify_person_iv.setVisibility(View.VISIBLE);
        }

        if (!TextUtils.isEmpty(item.verifyIntro)) {
            recommendUserViewHolder.intro_tv.setVisibility(View.VISIBLE);
            recommendUserViewHolder.intro_tv.setText(item.verifyIntro);
        } else {
            if (!TextUtils.isEmpty(item.recommendReason)) {
                recommendUserViewHolder.intro_tv.setVisibility(View.VISIBLE);
                recommendUserViewHolder.intro_tv.setText(item.recommendReason);
            } else {
                recommendUserViewHolder.intro_tv.setVisibility(View.INVISIBLE);
            }
        }

        if (item.isFollow) {
            recommendUserViewHolder.follow_iv.setImageResource(R.mipmap.login_follow_selected);

        } else {
            recommendUserViewHolder.follow_iv.setImageResource(R.mipmap.login_follow_normal);
        }

        if (item.picList == null || item.picList.size() == 0) {
            recommendUserViewHolder.pic_gal.setVisibility(View.GONE);
        } else {
            recommendUserViewHolder.pic_gal.setVisibility(View.VISIBLE);
            if (item.picList.size() > 0) {
                recommendUserViewHolder.pic1_iv.setVisibility(View.VISIBLE);
                ImageLoaderManager.imageLoaderDefaultCorner(recommendUserViewHolder.pic1_iv, R.color.gray, item.picList.get(0).longThumbnailUrl.replace("-w204", ""), picWidth, picWidth, cornerWidth);
            } else {
                recommendUserViewHolder.pic1_iv.setVisibility(View.INVISIBLE);
            }

            if (item.picList.size() > 1) {
                recommendUserViewHolder.pic2_iv.setVisibility(View.VISIBLE);
                ImageLoaderManager.imageLoaderDefaultCorner(recommendUserViewHolder.pic2_iv, R.color.gray, item.picList.get(1).longThumbnailUrl.replace("-w204", ""), picWidth, picWidth, cornerWidth);
            } else {
                recommendUserViewHolder.pic2_iv.setVisibility(View.INVISIBLE);
            }

            if (item.picList.size() > 2) {
                ImageLoaderManager.imageLoaderDefaultCorner(recommendUserViewHolder.pic3_iv, R.color.gray, item.picList.get(2).longThumbnailUrl.replace("-w204", ""), picWidth, picWidth, cornerWidth);
            } else {
                recommendUserViewHolder.pic3_iv.setVisibility(View.INVISIBLE);
            }

        }

        String str = item.fansNum + "粉丝," + item.age + "岁," + QueueConstants.roleMap.get(item.roleName) + "," + QueueConstants.affectionArr.get(item.affection);

        recommendUserViewHolder.fans_tv.setText(str);

        recommendUserViewHolder.follow_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RecommendListBean.GuideDataBean item = data.get(position);

                item.isFollow = !item.isFollow;

                if (item.isFollow) {
                    recommendUserViewHolder.follow_iv.setImageResource(R.mipmap.login_follow_selected);

                } else {
                    recommendUserViewHolder.follow_iv.setImageResource(R.mipmap.login_follow_normal);
                }

                if (mOnFollowStatusListener != null) {
                    mOnFollowStatusListener.OnFollowStatus();
                }
            }
        });

        recommendUserViewHolder.pic1_iv.setOnClickListener(new JumpToPic(item.picList, 0));

        recommendUserViewHolder.pic2_iv.setOnClickListener(new JumpToPic(item.picList, 1));

        recommendUserViewHolder.pic3_iv.setOnClickListener(new JumpToPic(item.picList, 2));

    }

    class JumpToPic implements View.OnClickListener {

        List<RecommendListBean.PicBean> picList;

        int position;

        JumpToPic(List<RecommendListBean.PicBean> picList, int position) {
            this.picList = picList;
            this.position = position;
        }

        @Override public void onClick(View v) {

            if (picList == null || picList.size() == 0) {
                return;
            }

            Intent intent = new Intent(v.getContext(), UserInfoPhotoActivity.class);
            ArrayList<String> photos = new ArrayList<>();

            int index;

            if (picList.size() > 3) {
                index = 3;
            } else {
                index = picList.size();
            }

            for (int i = 0; i < index; i++) {
                RecommendListBean.PicBean picBean = picList.get(i);
                photos.add(picBean.picUrl);
            }
            intent.putStringArrayListExtra(TheLConstants.BUNDLE_KEY_PHOTOS, photos);
            Bundle bundle = new Bundle();
            intent.putExtra("position", position);
            intent.putExtras(bundle);

            v.getContext().startActivity(intent);

        }
    }

    public void setData(List<RecommendListBean.GuideDataBean> data) {
        this.data.clear();
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    public List<RecommendListBean.GuideDataBean> getData() {
        return data;
    }

    public class RecommendUserViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.avatar_iv)
        ImageView avatar_iv;

        @BindView(R.id.nickname_tv)
        TextView nickname_tv;

        @BindView(R.id.verify_person_iv)
        ImageView verify_person_iv;

        @BindView(R.id.intro_tv)
        TextView intro_tv;

        @BindView(R.id.fans_tv)
        TextView fans_tv;

        @BindView(R.id.follow_iv)
        ImageView follow_iv;

        @BindView(R.id.pic_gal)
        GuideAvatarLayout pic_gal;

        @BindView(R.id.pic1_iv)
        ImageView pic1_iv;

        @BindView(R.id.pic2_iv)
        ImageView pic2_iv;

        @BindView(R.id.pic3_iv)
        ImageView pic3_iv;

        public RecommendUserViewHolder(View convertView) {
            super(convertView);
            ButterKnife.bind(this, convertView);
        }
    }

    public OnFollowStatusListener mOnFollowStatusListener;

    public void setOnFollowStatusListener(OnFollowStatusListener mOnFollowStatusListener) {
        this.mOnFollowStatusListener = mOnFollowStatusListener;
    }

    public interface OnFollowStatusListener {
        void OnFollowStatus();
    }

}
