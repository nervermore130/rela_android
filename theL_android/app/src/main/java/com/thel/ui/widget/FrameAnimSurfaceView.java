package com.thel.ui.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup;

import java.io.InputStream;

/**
 * Created by the L on 2017/1/9.
 */

public class FrameAnimSurfaceView extends SurfaceView implements SurfaceHolder.Callback, Runnable {

    private static final int FACT_FINISH = 1;
    private static final int TIME_FINISH = 2;
    private static final int PLAY_INDEX = 3;
    private static final int CLEAR_BACKGOUND = 4;
    private final Context mContext;//context
    private ViewGroup parent;
    private int[] resId;//res资源
    private int rate;//帧动画频率
    private int currentIndex;//当前播放资源下标
    private int mWidth;//宽度
    private int mHeight;//高度
    private SurfaceHolder mHolder;
    private Paint mPaint;
    private Canvas mCanvas;
    private Bitmap mBitmap;
    private boolean isPlaying;//是否正在播放帧动画
    private boolean isOneShot;//是否只播放一次，true为止播放一次
    private boolean isOncreate;//是否被创建，创建以后才可以播放动画
    private boolean ready;//是否准备好播放动画，被initview后有资源才可以播放
    private boolean shouldPlaying;//是否应该播放动画
    private long startTime;
    private boolean isFirstPlaying = true;
    private AnimListener animListener;
    private Handler mHandler;
    private long drawStart;
    private boolean everDestroyed;//是否在动画播放过程中曾经被destroy
    private boolean shouldFinished;//动画是否该结束（如果曾经摧毁过并且要求帧时间已经走完并且现在还没有Oncreate）

    public FrameAnimSurfaceView(Context context) {
        this(context, null);
    }

    public FrameAnimSurfaceView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FrameAnimSurfaceView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }

    private void init() {
        currentIndex = 0;
        mHolder = getHolder();
        mHolder.addCallback(this);
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        isOncreate = false;
        setZOrderOnTop(true);
        mHolder.setFormat(PixelFormat.TRANSPARENT);
        mHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case FACT_FINISH:
                        if (animListener != null) {
                            animListener.animFinished(parent, FrameAnimSurfaceView.this);
                        }
                        break;
                    case PLAY_INDEX:
                        final int index = (int) msg.obj;
                        if (animListener != null) {
                            animListener.playIndex(index);
                        }
                        break;
                    case TIME_FINISH:
                        shouldFinished = !isOncreate && everDestroyed && shouldPlaying;
                        if (shouldFinished) {
                            shouldPlaying = false;
                        }
                        if (animListener != null) {
                            animListener.timeFinished(parent, FrameAnimSurfaceView.this, shouldFinished);
                        }
                        break;
                    case CLEAR_BACKGOUND:
                        clearBackground();
                        break;

                }
            }
        };
    }

    /**
     * @param parent    父控件
     * @param res       帧动画资源
     * @param rate      帧动画频率
     * @param isOneShot 是否只播放一次，false 为播放重复播放
     * @return
     */
    public FrameAnimSurfaceView initView(ViewGroup parent, int[] res, int rate, boolean isOneShot) {
        resId = res;
        this.parent = parent;
        this.rate = rate;
        this.isOneShot = isOneShot;
        ready = true;//初始化数据，可以播放动画，ready
        return this;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        isOncreate = true;
        mWidth = getWidth();
        mHeight = getHeight();
        if (ready && shouldPlaying) {//如果不是第一次并且准备好（切下去再切上来）并且要求播放动画
            final int rates = (int) (Math.ceil((System.currentTimeMillis() - startTime) / (float) rate));
            if (isOneShot) {//如果只播放一次
                currentIndex = rates;//使用要求播放的每帧时间
            } else {
                currentIndex = rates % resId.length;//使用要求的每帧时间
            }
            start();
            if (rates > 0) {
                clearBackground();
            }
        }

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        mWidth = width;
        mHeight = height;

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        isOncreate = false;
        isPlaying = false;
        everDestroyed = true;
    }

    @Override
    public void run() {
        synchronized (mHolder) {
            if (shouldPlaying) {//如果应该播放
                isPlaying = true;
                while (shouldPlaying && isOncreate) {
                    if (currentIndex == 1) {//帧动画第一帧播放后要播放第二张的时候，把原背景给清楚掉
                        mHandler.sendEmptyMessage(CLEAR_BACKGOUND);
                    }
                    final long takeTime = drawView();//绘制，并返回绘制花费时间
                    final long wait = rate - takeTime;
                    if (wait > 0) {//如果绘制花费时间>=每帧要求的时间，就不等待，否者，等待时间差
                        try {
                            Thread.sleep(rate - takeTime);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (currentIndex >= resId.length - 1) {//如果当前播放为最后一张
                        if (isOneShot) {//如果只播放一次
                            shouldPlaying = false;//不在播放
                            isPlaying = false;//正在播放为false
                            if (mHolder != null) {
                                mCanvas = mHolder.lockCanvas();
                                if (mCanvas != null) {
                                    mCanvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
                                    mHolder.unlockCanvasAndPost(mCanvas);//清屏
                                }
                            }
                            mHandler.sendEmptyMessage(FACT_FINISH);//发送动画结束信息
                        } else {
                            currentIndex = 0;//循环播放，当前下标为0
                        }
                    } else {
                        currentIndex++;
                    }
                }
            }
        }
    }

    private void clearBackground() {
        if (getBackground() != null) {
            setBackgroundDrawable(null);
        }
    }


    private long drawView() {
        drawStart = System.currentTimeMillis();
        if (mHolder != null) {
            mCanvas = mHolder.lockCanvas();
        }
        try {
            mCanvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
            if (currentIndex >= resId.length) {
                return rate;
            }
            final InputStream in = getResources().openRawResource(resId[currentIndex]);//比decodeResource平均耗时不到其一半
            mBitmap = BitmapFactory.decodeStream(in);
            in.close();
            if (mBitmap == null) {
                return System.currentTimeMillis() - drawStart;
            }
            final Matrix matrix = new Matrix();
            matrix.postScale((float) mWidth / mBitmap.getWidth(), (float) mHeight / mBitmap.getHeight());
            mCanvas.drawBitmap(mBitmap, matrix, mPaint);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mCanvas != null && mHolder != null) {
                mHolder.unlockCanvasAndPost(mCanvas);
                final Message msg = Message.obtain();
                msg.what = PLAY_INDEX;
                msg.obj = currentIndex;
                mHandler.sendMessage(msg);
            }
            recyle(mBitmap);
            return System.currentTimeMillis() - drawStart;
        }

    }

    private void recyle(Bitmap bitmap) {
        if (bitmap != null) {
            bitmap.recycle();
        }
    }

    public void start() {
        mWidth = getMeasuredWidth();
        mHeight = getMeasuredHeight();
        shouldPlaying = true;
        if (isOncreate && !isPlaying && ready) {//如果被创建并且不是正在播放并且准备好
            Thread t = new Thread(this);
            t.start();
        }
        if (isFirstPlaying) {
            startTime = System.currentTimeMillis();
            isFirstPlaying = false;
            if (isOneShot) {
                mHandler.sendEmptyMessageDelayed(TIME_FINISH, resId.length * rate + 20);//20毫秒的延迟
            }
        }
    }

    public void resume() {
        currentIndex = 0;
        start();
    }

    public interface AnimListener {
        /**
         * 实际结束监听
         *
         * @param parant
         * @param surfaceView
         */
        void animFinished(ViewGroup parant, SurfaceView surfaceView);

        /**
         * 时间结束监听
         *
         * @param parent
         * @param surfaceView
         */
        void timeFinished(ViewGroup parent, SurfaceView surfaceView, boolean shouldFinished);

        /**
         * 当前播放index
         *
         * @param index
         */
        void playIndex(int index);
    }

    public void setOnAnimListener(AnimListener listener) {
        this.animListener = listener;
    }
}
