package com.thel.ui.dialog;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseFragment;
import com.thel.bean.BasicInfoBean;
import com.thel.bean.BasicInfoNetBean;
import com.thel.bean.RecommendListBean;
import com.thel.bean.user.FollowUsersBean;
import com.thel.constants.TheLConstants;
import com.thel.modules.main.userinfo.MyLinearLayoutManager;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.service.DefaultRequestService;
import com.thel.ui.adapter.RecommendUserAdapter;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.L;
import com.thel.utils.MD5Utils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SharedPrefUtils;
import com.thel.utils.UserUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by liuyun on 2017/12/26.
 */

public class GuideDialogFragment extends BaseFragment {

    private static final String TAG = "GuideDialogFragment";

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    @BindView(R.id.start_tv)
    TextView start_tv;

    @BindView(R.id.skip_tv)
    TextView skip_tv;

    private RecommendUserAdapter mAdapter;

    private boolean isFollowing = false;

    private int minFollowCount = 1;

    private int defaultFollowCount = 10;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_guide, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);

        initView();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getInitData();
    }

    private void getInitData() {

        RequestBusiness.getInstance().getBasicInfo()
                .onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(new InterceptorSubscribe<BasicInfoNetBean>() {
                    @Override
                    public void onNext(BasicInfoNetBean data) {
                        super.onNext(data);
                        if (data != null && data.data != null) {

                            final BasicInfoBean basicInfoBean = data.data;
                            ShareFileUtils.setInt(TheLConstants.live_permit, basicInfoBean.perm);
                            TheLConstants.applyLivePermitPage = basicInfoBean.permApplyUrl;
                            UserUtils.setUserVipLevel(basicInfoBean.level);
                            SharedPrefUtils.setInt(SharedPrefUtils.FILE_LIVE, SharedPrefUtils.LIVE_PERMITION, basicInfoBean.perm);
                            SharedPrefUtils.setString(SharedPrefUtils.FILE_LIVE, SharedPrefUtils.LIVE_WIDTH, basicInfoBean.liveWidth + "");
                            SharedPrefUtils.setString(SharedPrefUtils.FILE_LIVE, SharedPrefUtils.LIVE_HEIGHT, basicInfoBean.liveHeight + "");
                            SharedPrefUtils.setString(SharedPrefUtils.FILE_LIVE, SharedPrefUtils.LIVE_FPS, basicInfoBean.liveFps + "");
                            SharedPrefUtils.setString(SharedPrefUtils.FILE_LIVE, SharedPrefUtils.LOWEST_BPS, basicInfoBean.lowestBps + "");
                            SharedPrefUtils.setString(SharedPrefUtils.FILE_LIVE, SharedPrefUtils.LIVE_BPS, basicInfoBean.liveBps + "");
                            SharedPrefUtils.setString(SharedPrefUtils.FILE_LIVE, SharedPrefUtils.FILE_TOP_LINK, basicInfoBean.topLink + "");
                            SharedPrefUtils.setInt(SharedPrefUtils.FILE_LIVE, SharedPrefUtils.KEY_ONLY_TEXT, basicInfoBean.onlyText);
                            ShareFileUtils.setInt(ShareFileUtils.SUPER_LIKE, data.data.superLikeRetain);
                            TheLApp.enablePubLive = basicInfoBean.enablePubLive;
                            ShareFileUtils.setInt(ShareFileUtils.PERM, basicInfoBean.perm);

                            minFollowCount = basicInfoBean.minFollowCount;

                            defaultFollowCount = basicInfoBean.defaultFollowCount;

                            getData();

                        }
                    }
                });

    }

    private void initView() {
        MyLinearLayoutManager linearLayoutManager = new MyLinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        mRecyclerView.setLayoutManager(linearLayoutManager);

        mAdapter = new RecommendUserAdapter(getContext());
        mRecyclerView.setAdapter(mAdapter);

    }

    private void getData() {
        DefaultRequestService
                .createAllRequestService()
                .getRecommendList("0", "40")
                .onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new InterceptorSubscribe<RecommendListBean>() {
                    @Override
                    public void onNext(RecommendListBean data) {
                        super.onNext(data);
                        if (data.data != null && data.data.list != null) {
                            transformData(data.data.list);
                            mAdapter.setData(data.data.list);
                        }
                    }
                });
    }

    private void followUsers() {

        L.d(TAG, "------followUsers------");

        if (isFollowing) {
            return;
        }

        isFollowing = true;

        String userIds = getUserString();

        Map<String, String> map = new HashMap<>();

        map.put("userIds", userIds);

        DefaultRequestService
                .createUserRequestService()
                .followUsers(MD5Utils.generateSignatureForMap(map))
                .onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new InterceptorSubscribe<FollowUsersBean>() {
                    @Override
                    public void onNext(FollowUsersBean data) {
                        super.onNext(data);
                        sendBroadcast();
                    }

                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                        sendBroadcast();
                    }
                });
    }

    @OnClick(R.id.start_tv)
    void start() {

        if (isFollowFiveUsers()) {
            followUsers();
        } else {
            sendBroadcast();
        }


    }

    @OnClick(R.id.skip_tv)
    void onSkip() {
        sendBroadcast();
    }

    private void sendBroadcast() {
        try {
            if (getActivity() != null) {
                getActivity().getSupportFragmentManager().beginTransaction().remove(GuideDialogFragment.this).commit();
                Intent intent = new Intent();
                intent.setAction(TheLConstants.BROADCAST_RELEASE_MOMENT_SUCCEED);
                getActivity().sendBroadcast(intent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void transformData(List<RecommendListBean.GuideDataBean> list) {

        for (int i = 0; i < list.size(); i++) {

            RecommendListBean.GuideDataBean guideDataBean = list.get(i);

            if (i < defaultFollowCount) {
                guideDataBean.isFollow = true;
            }

        }

    }

    private String getUserString() {

        StringBuilder stringBuffer = new StringBuilder();

        List<RecommendListBean.GuideDataBean> list = mAdapter.getData();

        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                RecommendListBean.GuideDataBean guideDataBean = list.get(i);
                if (guideDataBean.isFollow) {
                    String userId = String.valueOf(guideDataBean.userId);
                    GrowingIOUtil.postFollowUsers(userId);  //上报关注的用户id
                    stringBuffer.append(userId).append(",");
                }
            }
        }

        String userIds = stringBuffer.toString();

        if (userIds.endsWith(",")) {
            userIds = userIds.substring(0, userIds.length() - 1);
        }

        return userIds;
    }

    private boolean isFollowFiveUsers() {

        int count = 0;

        List<RecommendListBean.GuideDataBean> list = mAdapter.getData();

        for (RecommendListBean.GuideDataBean guideDataBean : list) {
            if (guideDataBean.isFollow) {
                count++;
                if (count >= minFollowCount) {
                    return true;
                }
            }
        }

        return false;

    }

}
