package com.thel.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class FullRelativeLayout extends RelativeLayout {

    private int widthMeasureSpec = -1;

    private int heightMeasureSpec = -1;

    private boolean isFirst = true;

    public FullRelativeLayout(Context context) {
        super(context);
    }

    public FullRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FullRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        if (isFirst) {

            this.widthMeasureSpec = widthMeasureSpec;

            this.heightMeasureSpec = heightMeasureSpec;

            isFirst = false;
        }

        super.onMeasure(this.widthMeasureSpec, this.heightMeasureSpec);
    }
}
