package com.thel.ui.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.ViewGroup;

import com.thel.R;
import com.thel.base.BaseAdapter;
import com.thel.bean.moments.MomentsBean;
import com.thel.constants.MomentTypeConstants;
import com.thel.ui.viewholder.MultiImageViewHolder;

/**
 * Created by liuyun on 2017/11/12.
 */

public class MomentsAdapter2 extends BaseAdapter<RecyclerView.ViewHolder, MomentsBean> {


    public MomentsAdapter2(Context context) {
        super(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        MomentsBean momentsBean = data.get(viewType);

        RecyclerView.ViewHolder viewHolder = null;

        switch (momentsBean.momentsType) {

            case MomentTypeConstants.MOMENT_TYPE_TEXT:
                viewHolder = new MultiImageViewHolder(mLayoutInflater.inflate(R.layout.layout_moment_music, parent, false));
                break;
            case MomentTypeConstants.MOMENT_TYPE_IMAGE:

                break;
            case MomentTypeConstants.MOMENT_TYPE_TEXT_IMAGE:

                break;
            case MomentTypeConstants.MOMENT_TYPE_VOICE:

                break;
            case MomentTypeConstants.MOMENT_TYPE_TEXT_VOICE:

                break;

            case MomentTypeConstants.MOMENT_TYPE_THEME:

                break;

            case MomentTypeConstants.MOMENT_TYPE_THEME_REPLY:

                break;

            case MomentTypeConstants.MOMENT_TYPE_VIDEO:

                break;

            case MomentTypeConstants.MOMENT_TYPE_LIVE:

                break;

            case MomentTypeConstants.MOMENT_TYPE_USER_CARD:

                break;

            case MomentTypeConstants.MOMENT_TYPE_RECOMMEND:

                break;

            case MomentTypeConstants.MOMENT_TYPE_WEB:

                break;

            case MomentTypeConstants.MOMENT_TYPE_LIVE_USER:

                break;
            case MomentTypeConstants.MOMENT_TYPE_AD:

                break;
            default:
                break;
        }


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
