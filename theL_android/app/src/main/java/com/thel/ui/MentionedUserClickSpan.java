package com.thel.ui;

import android.content.Intent;
import androidx.core.content.ContextCompat;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.modules.main.home.moments.mention.MomentMentionedListActivity;
import com.thel.modules.main.userinfo.UserInfoActivity;

public class MentionedUserClickSpan extends ClickableSpan {

    private int userId;
    private String momensId;

    public MentionedUserClickSpan(int userId, String momensId) {
        super();
        this.userId = userId;
        this.momensId = momensId;
    }

    @Override
    public void updateDrawState(TextPaint ds) {
        ds.setColor(ContextCompat.getColor(TheLApp.getContext(), R.color.text_color_green));
        ds.setUnderlineText(false); // 去掉下划线
    }

    @Override
    public void onClick(View widget) {
        if (userId == -1) {// 跳到所有at用户列表页面
            Intent intent = new Intent(TheLApp.getContext().getApplicationContext(), MomentMentionedListActivity.class);
            intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, momensId);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            TheLApp.getContext().getApplicationContext().startActivity(intent);
        } else {
//            Intent intent = new Intent(TheLApp.getContext().getApplicationContext(), UserInfoActivity.class);
//            intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, userId + "");
//            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            TheLApp.getContext().getApplicationContext().startActivity(intent);
            FlutterRouterConfig.Companion.gotoUserInfo(userId+"");
        }
    }
}
