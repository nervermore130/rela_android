package com.thel.ui.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.thel.app.TheLApp;

import java.io.IOException;
import java.io.InputStream;
import java.util.Timer;
import java.util.TimerTask;

/**
 * 无限滚动广告栏组件
 */
@SuppressWarnings("deprecation")
public class MyPagerGalleryView extends Gallery implements AdapterView.OnItemClickListener, AdapterView.OnItemSelectedListener, OnTouchListener {
    /**
     * 显示的Activity
     */
    private Context mContext;
    /**
     * 条目单击事件接口
     */
    private MyOnItemClickListener mMyOnItemClickListener;
    /**
     * 图片切换时间
     */
    private int mSwitchTime;
    /**
     * 自动滚动的定时器
     */
    private Timer mTimer;
    /**
     * 圆点容器
     */
    private LinearLayout mOvalLayout;
    /**
     * 当前选中的数组索引
     */
    private int curIndex = 0;
    /**
     * 上次选中的数组索引
     */
    private int oldIndex = 0;
    /**
     * 圆点选中时的背景ID
     */
    private int mFocusedId;
    /**
     * 圆点正常时的背景ID
     */
    private int mNormalId;
    /**
     * 图片资源ID组
     */
    private int[] mAdsId;
    /**
     * 图片网络路径数组
     */
    private String[] mUris;
    /**
     * 图片高度
     */
    private int imageHeight;
    /**
     * 图片数量
     */
    private int count;
    /**
     * ImageView组
     */
    //    private List<ImageView> listImgs;
    private SparseArray<Bitmap> dateCache = new SparseArray<Bitmap>();
    /**
     * 广告条上面textView控件
     */
    private TextView adgallerytxt;

    /**
     * 广告条上的每一条文字的数组
     */
    private String[] txtViewpager;

    public MyPagerGalleryView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public MyPagerGalleryView(Context context) {
        super(context);
    }

    public MyPagerGalleryView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * @param context      显示的Activity ,不能为null
     * @param mris         图片的网络路径数组 ,为空时 加载 adsId
     * @param adsId        图片组资源ID ,测试用
     * @param switchTime   图片切换时间 写0 为不自动切换
     * @param ovalLayout   圆点容器 ,可为空
     * @param focusedId    圆点选中时的背景ID,圆点容器可为空写0
     * @param normalId     圆点正常时的背景ID,圆点容器为空写0
     * @param adgallerytxt 广告条上面textView控件
     * @param txtViewpager 广告条上的每一条文字的数组
     */
    public void start(Context context, String[] mris, int[] adsId, int switchTime, LinearLayout ovalLayout, int focusedId, int normalId, TextView adgallerytxt, String[] txtViewpager, int imgHeight) {
        this.mContext = context;
        this.mUris = mris;
        this.mAdsId = adsId;
        this.mSwitchTime = switchTime;
        this.mOvalLayout = ovalLayout;
        this.mFocusedId = focusedId;
        this.mNormalId = normalId;
        this.adgallerytxt = adgallerytxt;
        this.txtViewpager = txtViewpager;
        this.imageHeight = imgHeight;
        this.count = mris == null ? adsId.length : mris.length;

        //        initImages(imgHeight);// 初始化图片组
        setAdapter(new AdAdapter());
        this.setOnItemClickListener(this);
        this.setOnTouchListener(this);
        this.setOnItemSelectedListener(this);
        this.setSoundEffectsEnabled(false);
        this.setAnimationDuration(2000); // 动画时间
        this.setUnselectedAlpha(1); // 未选中项目的透明度
        // 不包含spacing会导致onKeyDown()失效!!! 失效onKeyDown()前先调用onScroll(null,1,0)可处理
        setSpacing(0);
        // 取靠近中间 图片数组的整倍数
        //        setSelection((getCount() / 2 / listImgs.size()) * listImgs.size()); // 默认选中中间位置为起始位置
        setFocusableInTouchMode(true);
        initOvalLayout();// 初始化圆点
        startTimer();// 开始自动滚动任务
    }

    /**
     * 初始化圆点
     */
    private void initOvalLayout() {

        if (mOvalLayout != null && count < 2) {// 如果只有一第图时不显示圆点容器
            mOvalLayout.removeAllViews();
            mOvalLayout.getLayoutParams().height = 0;
        } else if (mOvalLayout != null) {
            mOvalLayout.removeAllViews();
            // 圆点的大小是 圆点窗口的 70%;
            int Ovalheight = (int) (mOvalLayout.getLayoutParams().height * 0.7);
            // 圆点的左右外边距是 圆点窗口的 20%;
            int Ovalmargin = (int) (mOvalLayout.getLayoutParams().height * 0.3);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(Ovalheight, Ovalheight);
            layoutParams.setMargins(Ovalmargin, 0, Ovalmargin, 0);
            for (int i = 0; i < count; i++) {
                View v = new View(mContext); // 员点
                v.setLayoutParams(layoutParams);
                v.setBackgroundResource(mNormalId);
                mOvalLayout.addView(v);
            }
            // 选中第一个
            mOvalLayout.getChildAt(0).setBackgroundResource(mFocusedId);
        }
    }

    /**
     * 无限循环适配器
     */
    class AdAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return count;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView holdView = null;
            if (convertView == null) {


                convertView = new ImageView(mContext);// 实例化ImageView的对象

                ((ImageView) convertView).setScaleType(ImageView.ScaleType.FIT_XY); // 设置缩放方式
                convertView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, imageHeight));

                holdView = (ImageView) convertView;

                convertView.setTag(holdView);

            } else {
                holdView = (ImageView) convertView.getTag();
            }

            Bitmap current = dateCache.get(position);

            if (current != null) {//如果缓存中已解码该图片，则直接返回缓存中的图片

                holdView.setImageBitmap(current);

            } else {

                if (mUris == null) {
                    current = getPhotoItem(mAdsId[position], 1);
                } else {
                    current = getPhotoItem(mUris[position], 1);
                }

                holdView.setImageBitmap(current);

                dateCache.put(position, current);

            }


            return convertView;


            //            return listImgs.get(position % listImgs.size()); // 返回ImageView
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }
    }

    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        int kEvent;
        if (isScrollingLeft(e1, e2)) { // 检查是否往左滑动
            kEvent = KeyEvent.KEYCODE_DPAD_LEFT;
        } else { // 检查是否往右滑动
            kEvent = KeyEvent.KEYCODE_DPAD_RIGHT;
        }
        onKeyDown(kEvent, null);
        return true;

    }

    /**
     * 检查是否往左滑动
     */
    private boolean isScrollingLeft(MotionEvent e1, MotionEvent e2) {
        return e2.getX() > (e1.getX() + 50);
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return super.onScroll(e1, e2, distanceX, distanceY);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (MotionEvent.ACTION_UP == event.getAction() || MotionEvent.ACTION_CANCEL == event.getAction()) {
            startTimer();// 开始自动滚动任务
        } else {
            stopTimer();// 停止自动滚动任务
        }
        return false;
    }

    /**
     * 图片切换事件
     */
    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {
        try {
            curIndex = position;
            if (mOvalLayout != null && count > 1) { // 切换圆点
                mOvalLayout.getChildAt(oldIndex).setBackgroundResource(mNormalId); // 圆点取消
                mOvalLayout.getChildAt(curIndex).setBackgroundResource(mFocusedId);// 圆点选中
                if (position != 0 && adgallerytxt.getVisibility() == VISIBLE)
                    adgallerytxt.setVisibility(INVISIBLE);
                oldIndex = curIndex;
            }
            releaseBitmap();
            // adgallerytxt.setText("" + curIndex);
        } catch (Exception e) {
            return;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
    }

    /**
     * 项目点击事件
     */
    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
        if (mMyOnItemClickListener != null) {
            mMyOnItemClickListener.onItemClick(curIndex);
        }
    }

    /**
     * 设置项目点击事件监听器
     */

    public void setMyOnItemClickListener(MyOnItemClickListener listener) {
        mMyOnItemClickListener = listener;
    }

    /**
     * 项目点击事件监听器接口
     */
    public interface MyOnItemClickListener {
        /**
         * @param curIndex //当前条目在数组中的下标
         */
        void onItemClick(int curIndex);
    }

    /**
     * 停止自动滚动任务
     */
    public void stopTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
    }

    /**
     * 开始自动滚动任务 图片大于1张才滚动
     */
    public void startTimer() {
        if (mTimer == null && count > 1 && mSwitchTime > 0) {
            mTimer = new Timer();
            mTimer.schedule(new TimerTask() {
                public void run() {
                    handler.sendMessage(handler.obtainMessage(1));
                }
            }, mSwitchTime, mSwitchTime);
        }
    }

    /**
     * 处理定时滚动任务
     */
    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            // 不包含spacing会导致onKeyDown()失效!!!
            // 失效onKeyDown()前先调用onScroll(null,1,0)可处理
            onScroll(null, null, 1, 0);
            onKeyDown(KeyEvent.KEYCODE_DPAD_RIGHT, null);
        }
    };

    private void releaseBitmap() {

        //在这，我们分别预存储了第一个和最后一个可见位置之外的3个位置的bitmap

        //即dataCache中始终只缓存了（M＝6＋Gallery当前可见view的个数）M个bitmap

        int start = this.getFirstVisiblePosition() - 1;

        int end = this.getLastVisiblePosition() + 1;

        //释放position<start之外的bitmap资源

        Bitmap delBitmap;

        for (int del = 0; del <= start; del++) {

            delBitmap = dateCache.get(del);

            if (delBitmap != null) {

                //如果非空则表示有缓存的bitmap，需要清理
                //从缓存中移除该del->bitmap的映射

                dateCache.remove(del);

                if (!delBitmap.isRecycled())
                    delBitmap.recycle();

            }

        }
        freeBitmapFromIndex(end);
    }


    /**
     * 从某一位置开始释放bitmap资源
     *
     * @param end
     */

    public void freeBitmapFromIndex(int end) {

        //释放之外的bitmap资源
        Bitmap delBitmap;

        for (int del = end + 1; del < dateCache.size(); del++) {

            delBitmap = dateCache.get(del);

            if (delBitmap != null) {

                dateCache.remove(del);

                if (!delBitmap.isRecycled())
                    delBitmap.recycle();

            }

        }

    }

    public Bitmap getPhotoItem(int id, int size) {

        BitmapFactory.Options options = new BitmapFactory.Options();

        options.inSampleSize = size;

        options.inPreferredConfig = Bitmap.Config.RGB_565;

        options.inPurgeable = true;

        options.inInputShareable = true;

        //获取资源图片
        InputStream is = TheLApp.getContext().getResources().openRawResource(id);

        Bitmap bitmap =  BitmapFactory.decodeStream(is, null, options);

        try {
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return bitmap;
    }

    public Bitmap getPhotoItem(String filePath, int size) {

        BitmapFactory.Options options = new BitmapFactory.Options();

        options.inSampleSize = size;

        options.inPreferredConfig = Bitmap.Config.RGB_565;

        options.inPurgeable = true;

        options.inInputShareable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(filePath, options);

        return bitmap;
    }

}
