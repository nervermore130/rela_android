package com.thel.ui.widget;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Handler;
import android.os.Message;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.LinearInterpolator;

import com.thel.utils.SizeUtils;

/**
 * Created by liuyun on 2018/1/28.
 */

public class VoiceWaveView extends View {

    private Paint mPaint;

    private float sinWidth = 0;          //正玄宽度

    private float sinHeight = 180;       //正玄高度

    private float sinY = 500;            //正玄的Y位置

    private float sinMaxHeight = 180;    //正玄最大高度

    private final static float PERIODIC = 720; //周期

    private float startHeight = 0;


    public VoiceWaveView(Context context) {
        super(context);
        init();
    }

    public VoiceWaveView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public VoiceWaveView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {

        sinMaxHeight = SizeUtils.dip2px(getContext(), 80);

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setColor(0xFF4BBABC);
        mPaint.setStrokeWidth(2);
        mPaint.setStyle(Paint.Style.STROKE);

    }

    @Override protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        sinWidth = getMeasuredWidth();

        sinY = getMeasuredHeight() / 2;

        float delta = sinHeight / 5;

        for (int i = 0; i < PERIODIC; i++) {

            double x = sin(i);//获取sin值

            double y = sin(i + 1);

            float startX = (float) i * sinWidth / PERIODIC;

            float stopX = (float) (i + 1) * sinWidth / PERIODIC;

            float startY = (float) (sinY - x * sinHeight);
            float stopY = (float) (sinY - y * sinHeight);
            mPaint.setColor(0XFF4BBABC);
            canvas.drawLine(startX, startY, stopX, stopY, mPaint);

            startY = (float) (sinY - x * sinHeight);
            stopY = (float) (sinY - y * sinHeight);
            mPaint.setColor(0XFFF23288);
            canvas.drawLine(startX, startY, stopX, stopY, mPaint);

            startY = (float) (sinY - x * delta * 4);
            stopY = (float) (sinY - y * delta * 4);
            mPaint.setColor(0XFFF7D446);
            canvas.drawLine(startX, startY, stopX, stopY, mPaint);

            startY = (float) (sinY - x * delta * 3);
            stopY = (float) (sinY - y * delta * 3);
            mPaint.setColor(0XFF00A4FF);
            canvas.drawLine(startX, startY, stopX, stopY, mPaint);

            startY = (float) (sinY - x * delta * 2);
            stopY = (float) (sinY - y * delta * 2);
            mPaint.setColor(0XFF0043FF);
            canvas.drawLine(startX, startY, stopX, stopY, mPaint);
        }

        mPaint.setColor(0XFF66DADB);
        canvas.drawLine(0, sinY, sinWidth, sinY, mPaint);

    }


    public double sin(int i) {

        return Math.sin(i * Math.PI / 180);
    }

    public void invalidateAsync(float data) {

        sinHeight = sinMaxHeight * data;

//        postInvalidate();

        mHandler.sendEmptyMessage(0);

    }

    private Handler mHandler = new Handler() {
        @Override public void handleMessage(Message msg) {
            super.handleMessage(msg);
            startAnimator();
        }
    };

    public void invalidateSync(float data) {

        sinHeight = sinMaxHeight * data;

//        invalidate();
        startAnimator();
    }

    private ValueAnimator mValueAnimator;

    private void startAnimator() {

        if (mValueAnimator != null) {

            mValueAnimator.cancel();

            mValueAnimator = null;

        }

        mValueAnimator = ValueAnimator.ofFloat(startHeight, sinHeight);

        mValueAnimator.setDuration(50);

        mValueAnimator.setInterpolator(new LinearInterpolator());

        mValueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override public void onAnimationUpdate(ValueAnimator animation) {

                sinHeight = (float) animation.getAnimatedValue();

                invalidate();

                startHeight = sinHeight;
            }
        });
        mValueAnimator.start();
    }

}
