package com.thel.ui.adapter;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.constants.TheLConstants;

public class SelectionDialogWithIconAdapter extends BaseAdapter {

    private LayoutInflater mInflater;

    private String[] listData;

    private int[] iconIds;

    private boolean setLastItemColor = false;

    public SelectionDialogWithIconAdapter(String[] listData, int[] icons, boolean setLastItemColor) {
        this.listData = listData;
        this.setLastItemColor = setLastItemColor;
        this.iconIds = icons;
        mInflater = (LayoutInflater) TheLApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return listData.length;
    }

    @Override
    public Object getItem(int position) {
        return listData[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {
        convertView = mInflater.inflate(R.layout.selection_dialog_with_icon_listitem, parent, false);
        TextView txt = convertView.findViewById(R.id.txt);
        SimpleDraweeView img = convertView.findViewById(R.id.img);

        txt.setText(listData[position]);
        img.setImageURI(Uri.parse(TheLConstants.RES_PIC_URL + iconIds[position]));
        if (position == listData.length - 1 && setLastItemColor) {
            txt.setTextColor(TheLApp.getContext().getResources().getColor(R.color.orange));
        }

        return convertView;
    }

}
