package com.thel.ui.widget;

import android.content.Context;
import android.graphics.Canvas;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import com.thel.utils.L;

public class BaseFrameLayout extends FrameLayout{
    public BaseFrameLayout(@NonNull Context context) {
        super(context);
    }

    public BaseFrameLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public BaseFrameLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        L.d("BaseFrameLayout","-------onMeasure------");

    }

    @Override protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        L.d("BaseFrameLayout","-------onLayout------");


    }

    @Override protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        L.d("BaseFrameLayout","-------onDraw------");


    }

    @Override public void draw(Canvas canvas) {
        super.draw(canvas);

        L.d("BaseFrameLayout","-------draw------");


    }
}
