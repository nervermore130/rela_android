package com.thel.ui;

import android.app.Activity;

import com.thel.utils.LoadingDialogUtils;


/**
 * Created by the L on 2017/2/3.
 * 数据加载loading接口
 */

public interface LoadingDialogImpl {
    /**
     * loading对话框，可以取消
     */
    void showLoading();

    /**
     * loading对话框，不可以取消
     */
    void showLoadingNoBack();

    /**
     * 关闭loading对话框
     */
    void closeDialog();

    /**
     * 摧毁loading对话框
     */
    void destroyDialog();

    class LoadingDialogInstance {
        public static LoadingDialogImpl getInstance(Activity activity) {
            return new LoadingDialogUtils(activity);
        }
    }
}
