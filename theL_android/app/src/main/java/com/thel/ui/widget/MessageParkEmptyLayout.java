package com.thel.ui.widget;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.thel.R;
import com.thel.base.BaseDataBean;
import com.thel.constants.BundleConstants;
import com.thel.modules.main.home.moments.web.WebViewActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.utils.DateUtils;
import com.thel.utils.L;
import com.thel.utils.PermissionUtil;
import com.thel.utils.ShareFileUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by chad
 * Time 2019-09-05
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */
public class MessageParkEmptyLayout extends LinearLayout {

    @BindView(R.id.image_left)
    ImageView image_left;

    @BindView(R.id.image_right)
    ImageView image_right;

    @BindView(R.id.match_tips)
    TextView match_tips;

    @BindView(R.id.btn_go)
    TextView btn_go;

    private static final int OPEN_PUSH_STATUS = 0;
    private static final int CLOSE_PUSH_STATUS = 1;
    private static final int GOTO_MATCH_STATUS = 2;
    private int btn_status = OPEN_PUSH_STATUS;

    private boolean isMsgEmpty = true;

    public MessageParkEmptyLayout(Context context) {
        super(context);
        init();
    }

    public MessageParkEmptyLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MessageParkEmptyLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {

        View view = LayoutInflater.from(getContext()).inflate(R.layout.layout_message_park_empty, this, true);

        ButterKnife.bind(this, view);

        btn_go.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), WebViewActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(BundleConstants.URL, "https://www.rela.me/game/#/chat?entry=appchat");
                intent.putExtras(bundle);
                getContext().startActivity(intent);

//                switch (btn_status) {
//                    case OPEN_PUSH_STATUS:
//                        changeMatchStatue();
//                        break;
//                    case CLOSE_PUSH_STATUS:
//                        PermissionUtil.requestPushPermission((Activity) getContext(), new PermissionUtil.PermissionCallback() {
//                            @Override
//                            public void granted() {
//                                changeMatchStatue();
//
//                                if (messageParkEmptyListener != null) messageParkEmptyListener.openPush();
//                            }
//
//                            @Override
//                            public void denied() {
//
//                            }
//
//                            @Override
//                            public void gotoSetting() {
//                                changeMatchStatue();
//                            }
//                        });
//                        break;
//                    case GOTO_MATCH_STATUS:
//                        Intent intent = new Intent(getContext(), WebViewActivity.class);
//                        Bundle bundle = new Bundle();
//                        bundle.putString(BundleConstants.URL, "https://www.rela.me/game/#/chat?entry=appchat");
//                        intent.putExtras(bundle);
//                        getContext().startActivity(intent);
//                        break;
//                }
            }
        });
    }

    public void refreshUI(boolean isMsgEmpty) {
        this.isMsgEmpty = isMsgEmpty;

        if (isMsgEmpty) {//用户没有聊天消息时
            btn_status = OPEN_PUSH_STATUS;
            match_tips.setText(R.string.goto_match_tips);
//            btn_go.setText(R.string.match_push_has_open);
            btn_go.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
            btn_go.setBackgroundResource(R.drawable.shape_chat_match_tips_park_select_red);
            image_left.setVisibility(VISIBLE);
            image_right.setVisibility(VISIBLE);
            setVisibility(VISIBLE);
        } else {//否则
            setVisibility(GONE);
        }
    }

    @Deprecated
    public void refreshUI_Old(boolean isMsgEmpty) {
        this.isMsgEmpty = isMsgEmpty;
        if (DateUtils.sevenColckBefore()) {//【0点-18点59分时】
            if (ShareFileUtils.getInt(ShareFileUtils.perfectMatch, 1) != 0) {//已打开匹配通知
                if (isMsgEmpty) {
                    btn_status = OPEN_PUSH_STATUS;
                    match_tips.setText(R.string.open_match_push_tips);
                    btn_go.setText(R.string.match_push_has_open);
                    btn_go.setTextColor(Color.parseColor("#FF14CCCC"));
                    btn_go.setBackgroundResource(R.drawable.shape_chat_match_tips_park_unselect);
                    image_left.setVisibility(GONE);
                    image_right.setVisibility(GONE);
                    setVisibility(VISIBLE);
                } else {
                    setVisibility(GONE);
                }
            } else {//没有打开匹配通知
                if (isMsgEmpty) {
                    btn_status = CLOSE_PUSH_STATUS;
                    match_tips.setText(R.string.open_match_push_tips);
                    btn_go.setText(R.string.open_match_push);
                    btn_go.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
                    btn_go.setBackgroundResource(R.drawable.shape_chat_match_tips_park_select);
                    image_left.setVisibility(GONE);
                    image_right.setVisibility(GONE);
                    setVisibility(VISIBLE);
                } else {
                    setVisibility(GONE);
                }
            }
        } else {//【19点-23点59分时】
            if (isMsgEmpty) {
                btn_status = GOTO_MATCH_STATUS;
                match_tips.setText(R.string.goto_match_tips);
                btn_go.setText(R.string.goto_match);
                btn_go.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
                btn_go.setBackgroundResource(R.drawable.shape_chat_match_tips_park_select_red);
                image_left.setVisibility(VISIBLE);
                image_right.setVisibility(VISIBLE);
                setVisibility(VISIBLE);
            } else {
                setVisibility(GONE);
            }
        }
    }

    private void changeMatchStatue() {
        Flowable<BaseDataBean> flowable = RequestBusiness.getInstance().pushConfig(ShareFileUtils.perfectMatch, ShareFileUtils.getInt(ShareFileUtils.perfectMatch, 1), ShareFileUtils.getInt(ShareFileUtils.commentTextPush, 1), ShareFileUtils.getInt(ShareFileUtils.commentReplyPush, 1), ShareFileUtils.getInt(ShareFileUtils.recordPush, 1), ShareFileUtils.getInt(ShareFileUtils.commentWinkPush, 1), ShareFileUtils.getInt(ShareFileUtils.followerPush, 1), ShareFileUtils.getInt(ShareFileUtils.winkPush, 1), ShareFileUtils.getInt(ShareFileUtils.messagePush, 1), ShareFileUtils.getInt(ShareFileUtils.videoPush, 1), ShareFileUtils.getInt(ShareFileUtils.themePush, 1), ShareFileUtils.getInt(ShareFileUtils.likeMe, 1), ShareFileUtils.getInt(ShareFileUtils.superLike, 1), 1 - ShareFileUtils.getInt(ShareFileUtils.perfectMatch, 0), ShareFileUtils.getInt(ShareFileUtils.await, 1));
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
            @Override
            public void onNext(BaseDataBean baseDataBean) {
                super.onNext(baseDataBean);
                ShareFileUtils.setInt(ShareFileUtils.perfectMatch, 1 - ShareFileUtils.getInt(ShareFileUtils.perfectMatch, 0));
                refreshUI(isMsgEmpty);
                L.d("settingsRecord", "完美匹配我更改状态啦");
            }
        });
    }

    private MessageParkEmptyListener messageParkEmptyListener;

    public void setMessageParkEmptyListener(MessageParkEmptyListener messageParkEmptyListener) {
        this.messageParkEmptyListener = messageParkEmptyListener;
    }

    public interface MessageParkEmptyListener {
        void openPush();
    }
}
