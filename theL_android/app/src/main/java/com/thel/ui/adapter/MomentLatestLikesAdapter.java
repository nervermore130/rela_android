package com.thel.ui.adapter;

import com.thel.R;
import com.thel.bean.gift.WinkCommentBean;
import com.thel.constants.TheLConstants;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

public class MomentLatestLikesAdapter extends BaseRecyclerViewAdapter<WinkCommentBean> {

    public MomentLatestLikesAdapter(List<WinkCommentBean> list) {
        super(R.layout.item_moment_latest_wink, list);
    }

    public void updateDataSource(List<WinkCommentBean> list) {
        this.mData = list == null ? new ArrayList<WinkCommentBean>() : list;
    }

    @Override
    protected void convert(BaseViewHolder helper, WinkCommentBean bean) {
        helper.setImageUrl(R.id.img_thumb, bean.avatar, TheLConstants.AVATAR_SMALL_SIZE, TheLConstants.AVATAR_SMALL_SIZE);
    }
}