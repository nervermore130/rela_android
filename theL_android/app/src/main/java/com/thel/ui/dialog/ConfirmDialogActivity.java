package com.thel.ui.dialog;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.constants.TheLConstants;
import com.thel.utils.DeviceUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by liuyun on 2017/12/9.
 */

public class ConfirmDialogActivity extends BaseActivity {

    @BindView(R.id.message_tv)
    TextView message_tv;

    @BindView(R.id.ok_tv)
    TextView ok_tv;

    @BindView(R.id.cancel_tv)
    TextView cancel_tv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.O && isTranslucentOrFloating()) {
            fixOrientation();
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog_confirm);
        ButterKnife.bind(this);

        Bundle bundle = getIntent().getExtras();

        String message = bundle.getString(TheLConstants.BUNDLE_DIALOG_MESSAGE, "");

        message_tv.setText(message);

    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    @OnClick(R.id.ok_tv)
    void ok() {
        try {

            Intent data = new Intent(Intent.ACTION_SENDTO);
            data.setData(Uri.parse("mailto:hi@rela.me"));
            data.putExtra(Intent.EXTRA_SUBJECT, "");
            data.putExtra(Intent.EXTRA_TEXT, DeviceUtils.getReportData(TheLApp.context));
            startActivity(data);

            cancel();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.cancel_tv)
    void cancel() {
        this.finish();
    }

    private boolean isTranslucentOrFloating() {
        boolean isTranslucentOrFloating = false;
        try {
            int[] styleableRes = (int[]) Class.forName("com.android.internal.R$styleable").getField("Window").get(null);
            final TypedArray ta = obtainStyledAttributes(styleableRes);
            Method m = ActivityInfo.class.getMethod("isTranslucentOrFloating", TypedArray.class);
            m.setAccessible(true);
            isTranslucentOrFloating = (boolean) m.invoke(null, ta);
            m.setAccessible(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isTranslucentOrFloating;
    }

    private boolean fixOrientation() {
        try {
            Field field = Activity.class.getDeclaredField("mActivityInfo");
            field.setAccessible(true);
            ActivityInfo o = (ActivityInfo) field.get(this);
            o.screenOrientation = -1;
            field.setAccessible(false);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

}
