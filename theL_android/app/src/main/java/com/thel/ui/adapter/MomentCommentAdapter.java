package com.thel.ui.adapter;

import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.comment.CommentBean;
import com.thel.bean.comment.MomentCommentBean;
import com.thel.manager.ImageLoaderManager;
import com.thel.ui.widget.LatestCommentView;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;
import com.thel.utils.EmojiUtils;
import com.thel.utils.MomentUtils;
import com.thel.utils.SizeUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by liuyun on 2017/10/23.
 */

public class MomentCommentAdapter extends BaseRecyclerViewAdapter<MomentCommentBean.MomentCommentLiseBean> {


    public MomentCommentAdapter(int layoutResId, List<MomentCommentBean.MomentCommentLiseBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, final MomentCommentBean.MomentCommentLiseBean commentBean) {
        CommentViewHolder holder = (CommentViewHolder) helper;

        if (holder.getAdapterPosition() > 0)
            holder.setBackgroundColor(R.id.main, ContextCompat.getColor(mContext, R.color.light_gray1));
        else {
            holder.setBackgroundColor(R.id.main, ContextCompat.getColor(mContext, R.color.white));
        }
        if (holder.getAdapterPosition() == 0) {//评论子评论界面，可以查看原日志
            holder.txt_look_original_moment.setVisibility(View.VISIBLE);
            holder.txt_look_original_moment.setTag(commentBean.commentId);
            holder.txt_look_original_moment.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {

                }
            });
        } else {
            holder.txt_look_original_moment.setVisibility(View.GONE);
        }

        // 头像和昵称
        ImageLoaderManager.imageLoader(holder.img_thumb, commentBean.avatar);
        holder.img_thumb.setTag(commentBean.userId);
        holder.img_thumb.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {

            }
        });
        holder.comment_user_name.setText(commentBean.nickname);
        if (CommentBean.SAY_TYPE_TO.equals(commentBean.sayType)) {
            holder.setVisibility(R.id.comment_say_to, View.VISIBLE);
            holder.comment_say_to.setText("@" + commentBean.toNickname);
        } else {
            holder.comment_say_to.setVisibility(View.GONE);
        }
        // 发布时间
        holder.comment_release_time.setText(MomentUtils.getReleaseTimeShow(commentBean.commentTime));
        //内容
        if (CommentBean.COMMENT_TYPE_TEXT.equals(commentBean.commentType)) {//如果是文本回复
            holder.img_sticker.setVisibility(View.GONE);
            holder.comment_content_text.setVisibility(View.VISIBLE);
            holder.comment_content_text.setText(EmojiUtils.getInstace(EmojiUtils.DEFAULT_SIZE).getExpressionString(TheLApp.getContext(), commentBean.commentText));
        } else {//表情
            holder.img_sticker.setVisibility(View.VISIBLE);
            holder.comment_content_text.setVisibility(View.GONE);
            ImageLoaderManager.imageLoaderCircle(holder.img_sticker, R.mipmap.icon_user, MomentUtils.getStickerUrl(commentBean.commentText));
        }

        // 最近三条评论
        ((LinearLayout) holder.getView(R.id.lin_latest_comments)).removeAllViews();
        ArrayList<CommentBean> commentBeans = getLatestComments();
        if (commentBeans != null) {
            if (commentBeans.size() > 0) {
                holder.setVisibility(R.id.lin_latest_comments, View.VISIBLE);
                holder.setOnClickListener(R.id.lin_latest_comments, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                    Intent intent = new Intent(TheLApp.getContext(), MomentCommentReplyActivity.class);
//                    intent.putExtra(TheLConstants.BUNDLE_KEY_COMMENT_ID, commentBean.commentId);
//                    intent.putExtra(TheLConstants.BUNDLE_KEY_INTENT_FROM, MomentCommentReplyActivity.FROM_MOMENT);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    TheLApp.getContext().startActivity(intent);
                    }
                });
            } else
                holder.setVisibility(R.id.lin_latest_comments, View.GONE);
            for (int i = 0; i < commentBeans.size(); i++) {
                final LatestCommentView latestCommentView = new LatestCommentView(mContext);
                latestCommentView.setGravity(Gravity.LEFT);
                if (i > 0) {
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    params.topMargin = SizeUtils.dip2px(TheLApp.getContext(), 5);
                    latestCommentView.setLayoutParams(params);
                }
                final String text;
                if (CommentBean.COMMENT_TYPE_STICKER.equals(commentBeans.get(i).commentType))
                    text = TheLApp.getContext().getString(R.string.message_info_sticker);
                else
                    text = commentBeans.get(i).commentText;
                latestCommentView.setText(commentBeans.get(i).nickname, text, commentBeans.get(i).userId + "", String.valueOf(commentBean.commentId), "commentReply");
                ((LinearLayout) holder.getView(R.id.lin_latest_comments)).addView(latestCommentView);
            }
        }
        // 如果超过三条评论，则需要加一个『共xx条回复』的item
        if (commentBean.commentNum > 3) {
            final TextView textView = new TextView(mContext);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.topMargin = SizeUtils.dip2px(TheLApp.getContext(), 5);
            textView.setLayoutParams(params);
            textView.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_green));
            textView.setText(TheLApp.getContext().getString(R.string.total_comment_replies, commentBean.commentNum));
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
            textView.setMaxLines(1);
            textView.setEllipsize(TextUtils.TruncateAt.END);
            ((LinearLayout) holder.getView(R.id.lin_latest_comments)).addView(textView);
        }

    }

    @Override protected BaseViewHolder onCreateDefViewHolder(ViewGroup parent, int viewType) {
        BaseViewHolder baseViewHolder = super.onCreateDefViewHolder(parent, viewType);
        return new CommentViewHolder(baseViewHolder.convertView);
    }

    public class CommentViewHolder extends BaseViewHolder {

        @BindView(R.id.img_thumb) ImageView img_thumb;

        @BindView(R.id.comment_user_name) TextView comment_user_name;

        @BindView(R.id.comment_say_to) TextView comment_say_to;

        @BindView(R.id.comment_release_time) TextView comment_release_time;

        @BindView(R.id.comment_content_text) TextView comment_content_text;

        @BindView(R.id.img_sticker) ImageView img_sticker;

        @BindView(R.id.lin_latest_comments) LinearLayout lin_latest_comments;

        @BindView(R.id.txt_look_original_moment) TextView txt_look_original_moment;


        public CommentViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

        }
    }


    private ArrayList<CommentBean> getLatestComments() {
        return null;
    }

}
