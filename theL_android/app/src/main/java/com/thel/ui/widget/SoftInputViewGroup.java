package com.thel.ui.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.InputMethodManager;

import com.thel.R;
import com.thel.utils.L;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author liuyun
 * @date 2017/11/15
 */

public class SoftInputViewGroup extends ViewGroup {

    private static final String TAG = "SoftInputViewGroup";

    private int softKeyBroadHeight = 0;

    private int oldHeightDifference = -1;

    private int[] ids;

    private boolean isShowDanmu = false;

    private List<Rect> mCurrentViewRects = new ArrayList<>();

    private OnSoftInputShowListener onSoftInputShowListener;

    public SoftInputViewGroup(Context context) {
        super(context);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            init();
        }

    }

    public SoftInputViewGroup(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            init();
        }
    }

    public SoftInputViewGroup(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            init();
        }
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {

        int childCount = getChildCount();

        for (int i = 0; i < childCount; i++) {

            View childView = getChildAt(i);

            childView.layout(l, t + y, r, b + y);

        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        measureChildren(widthMeasureSpec, heightMeasureSpec);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    public interface OnSoftKeyboardStateChangedListener {
        void OnSoftKeyboardStateChanged(boolean isKeyBoardShow, int keyboardHeight);
    }

    public void showKeyBoardAndHideDanmuView() {

        if (isShowDanmu) {
            hideDanmu(true);
        }
    }

    public void showDanmu() {

        isShowDanmu = true;

        toggleInput();
    }

    public void hideDanmu(boolean isShowKeyBoard) {

        isShowDanmu = false;

        showDanmuLayout(view.getMeasuredHeight() - danmu_selected_layout.getMeasuredHeight(), view.getMeasuredHeight(), isShowKeyBoard);

    }
    //软键盘状态监听列表
    private ArrayList<OnSoftKeyboardStateChangedListener> mKeyboardStateListeners;

    private ViewTreeObserver.OnGlobalLayoutListener mLayoutChangeListener;

    private boolean mIsSoftKeyboardShowing;

    private boolean isSoftExtend = false;

    private View view;

    private int y = 0;

    private View danmu_selected_layout;

    private void init() {
        mIsSoftKeyboardShowing = false;
        mKeyboardStateListeners = new ArrayList<>();
        mLayoutChangeListener = new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                //判断窗口可见区域大小
                Rect r = new Rect();
                Activity activity = (Activity) getContext();
                activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(r);
                //如果屏幕高度和Window可见区域高度差值大于整个屏幕高度的1/3，则表示软键盘显示中，否则软键盘为隐藏状态。

                int screenHeight = getMeasuredHeight();

                int heightDifference = screenHeight - (r.bottom - r.top);

                if (heightDifference == oldHeightDifference) {
                    return;
                }

                oldHeightDifference = heightDifference;

                boolean isKeyboardShowing = heightDifference > screenHeight / 3;
                //如果之前软键盘状态为显示，现在为关闭，或者之前为关闭，现在为显示，则表示软键盘的状态发生了改变
                if ((mIsSoftKeyboardShowing && !isKeyboardShowing) || (!mIsSoftKeyboardShowing && isKeyboardShowing)) {
                    mIsSoftKeyboardShowing = isKeyboardShowing;
                    if (view == null) {
                        view = SoftInputViewGroup.this.getChildAt(0);
                        danmu_selected_layout = SoftInputViewGroup.this.getChildAt(1);
                        view.getViewTreeObserver().addOnGlobalLayoutListener(mViewTreeObserver);
                    }

                    int viewHeight = view.getMeasuredHeight();

                    L.d(TAG, " viewHeight : " + viewHeight);

                    int y = screenHeight - heightDifference - viewHeight + getStatusBarHeight();

                    isSoftExtend = y != 0;

                    if (onSoftInputShowListener != null) {
                        onSoftInputShowListener.onSoftInputShow(isSoftExtend, isShowDanmu);
                    }

                    if (y != 0) {
                        softKeyBroadHeight = Math.abs(y);
                    }

                    L.d(TAG, " softKeyBroadHeight : " + softKeyBroadHeight);

                    if (y == 0) {
                        startAnimator(viewHeight - softKeyBroadHeight, viewHeight);
                    } else {
                        startAnimator(viewHeight, viewHeight - softKeyBroadHeight);
                    }

                    for (int i = 0; i < mKeyboardStateListeners.size(); i++) {
                        OnSoftKeyboardStateChangedListener listener = mKeyboardStateListeners.get(i);
                        listener.OnSoftKeyboardStateChanged(mIsSoftKeyboardShowing, heightDifference);
                    }
                }
            }
        };
        ((Activity) getContext()).getWindow().getDecorView().getViewTreeObserver().addOnGlobalLayoutListener(mLayoutChangeListener);
    }

    public boolean isDanmuShowing() {
        return isShowDanmu;
    }

    public boolean isSoftExtend() {
        return isSoftExtend;
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (getContext() != null) {
            ((Activity) getContext()).getWindow().getDecorView().getViewTreeObserver().removeOnGlobalLayoutListener(mLayoutChangeListener);
        }

        if (view != null && mViewTreeObserver != null) {
            view.getViewTreeObserver().removeOnGlobalLayoutListener(mViewTreeObserver);
        }
    }


    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    private void startAnimator(int startY, int endY) {

        L.d(TAG, " startY : " + startY + " endY : " + endY);

        if (view == null) {
            return;
        }

        final ValueAnimator valueAnimator = ValueAnimator.ofInt(startY, endY);
        valueAnimator.setInterpolator(new LinearInterpolator());
        valueAnimator.setDuration(250);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override public void onAnimationUpdate(ValueAnimator animation) {

                int offset = (int) animation.getAnimatedValue();

                view.setY(offset - view.getBottom());


            }
        });
        valueAnimator.addListener(new AnimatorListenerAdapter() {

            @Override public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                view.setLayerType(LAYER_TYPE_HARDWARE, null);

            }

            @Override public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);

                view.setLayerType(LAYER_TYPE_NONE, null);

                valueAnimator.cancel();

                if (isShowDanmu) {
                    showDanmuLayout(view.getMeasuredHeight(), view.getMeasuredHeight() - danmu_selected_layout.getMeasuredHeight(), false);
                }

            }
        });
        valueAnimator.start();

    }

    private void showDanmuLayout(int startY, int endY, final boolean isShowKeyBoard) {
        if (danmu_selected_layout == null) {
            return;
        }

        final ValueAnimator valueAnimator = ValueAnimator.ofInt(startY, endY);
        valueAnimator.setInterpolator(new LinearInterpolator());
        valueAnimator.setDuration(250);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override public void onAnimationUpdate(ValueAnimator animation) {

                int offset = (int) animation.getAnimatedValue();

                view.setY(offset - view.getBottom());

                danmu_selected_layout.setY(offset);


            }
        });
        valueAnimator.addListener(new AnimatorListenerAdapter() {

            @Override public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                view.setLayerType(LAYER_TYPE_HARDWARE, null);
                danmu_selected_layout.setLayerType(LAYER_TYPE_HARDWARE, null);
                danmu_selected_layout.setVisibility(VISIBLE);
            }

            @Override public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);

                view.setLayerType(LAYER_TYPE_NONE, null);

                danmu_selected_layout.setLayerType(LAYER_TYPE_NONE, null);

                valueAnimator.cancel();

                if (!isShowDanmu) {
                    danmu_selected_layout.setVisibility(INVISIBLE);
                }

                if (isShowKeyBoard) {
                    toggleInput();
                }

                if (onSoftInputShowListener != null) {
                    onSoftInputShowListener.onSoftInputShow(isSoftExtend, isShowDanmu);
                }

            }
        });
        valueAnimator.start();

    }

    public void setNotInterceptViewsId(int[] ids) {

        this.ids = ids;

    }

    private ViewTreeObserver.OnGlobalLayoutListener mViewTreeObserver = new ViewTreeObserver.OnGlobalLayoutListener() {
        @Override public void onGlobalLayout() {
            Rect mCurrentViewRect = new Rect();
            if (mCurrentViewRects.size() < ids.length) {
                for (int id : ids) {
                    if (view != null) {

                        View childView;

                        if (id == R.id.danmu_layout) {

                            childView = danmu_selected_layout;

                        } else {

                            childView = view.findViewById(id);

                        }

                        int[] locations = new int[2];

                        childView.getLocationInWindow(locations);

                        int[] locations1 = new int[2];

                        childView.getLocationOnScreen(locations1);

                        mCurrentViewRect.set(childView.getLeft(), locations[1], childView.getRight(), childView.getBottom() + locations[1]);

                        mCurrentViewRects.add(mCurrentViewRect);
                    }
                }
            }

        }
    };
//
//    private float startX = 0;
//
//    private float startY = 0;
//
//    @Override public boolean onInterceptTouchEvent(MotionEvent event) {
//
////        L.d(TAG, " isSoftExtend : " + isSoftExtend);
////
////        L.d(TAG, " isShowDanmu : " + isShowDanmu);
//
//        if (!isSoftExtend && !isShowDanmu) {
//
//            return false;
//        }
//
//        int action = event.getAction();
//
//        switch (action) {
//            case MotionEvent.ACTION_DOWN:
//
//                L.d(TAG, " onInterceptTouchEvent MotionEvent.ACTION_DOWN : ");
//
//                startX = event.getX();
//
//                startY = event.getY();
//
//                break;
//            case MotionEvent.ACTION_UP:
//
//                L.d(TAG, " onInterceptTouchEvent MotionEvent.ACTION_UP : ");
//
//                final float endX = event.getX();
//
//                final float endY = event.getY();
//
//                int controlViewHeight = SizeUtils.dip2px(TheLApp.context, 50);
//
//                if (Math.abs(endY - startY) < 8 && Math.abs(endX - startX) < 8) {
//
//                    int dispatchTouchHeight = 0;
//
//                    int disPatchTouchWidth = 0;
//
//                    if (isShowDanmu) {
//
//                        dispatchTouchHeight = view.getMeasuredHeight() - controlViewHeight - danmu_selected_layout.getMeasuredHeight();//拿到可以点击区域的高度
//
//                        disPatchTouchWidth = view.getMeasuredWidth();//拿到可以点击区域的高度
//
//                    }
//
//                    if (isSoftExtend) {
//
//                        dispatchTouchHeight = view.getMeasuredHeight() - controlViewHeight - softKeyBroadHeight;//拿到可以点击区域的高度
//
//                        disPatchTouchWidth = view.getMeasuredWidth();//拿到可以点击区域的高度
//                    }
//
//                    if (endY < dispatchTouchHeight && endX < disPatchTouchWidth) {
//
//                        if (isShowDanmu) {
//                            hideDanmu(false);
//                        }
//
//                        if (isSoftExtend) {
//                            toggleInput();
//                        }
//
//                        return true;
//                    } else {
//                        return false;
//                    }
//
//                }
//
//                break;
//        }
//
//        boolean isInterceptTouch = super.onInterceptTouchEvent(event);
//
//        L.d(TAG, " onInterceptTouchEvent isInterceptTouch : " + isInterceptTouch);
//
//        return isInterceptTouch;
//    }

    public void toggleInput() {
        InputMethodManager inputMethodManager =
                (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        assert inputMethodManager != null;
        inputMethodManager.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
    }


    public void setOnSoftInputShowListener(OnSoftInputShowListener onSoftInputShowListener) {
        this.onSoftInputShowListener = onSoftInputShowListener;
    }

    public interface OnSoftInputShowListener {
        void onSoftInputShow(boolean isShow, boolean isShowDa);
    }

}
