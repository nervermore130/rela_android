package com.thel.ui.popupwindow;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.thel.R;
import com.thel.bean.live.LiveMultiSeatBean;
import com.thel.modules.main.me.match.MatchPosterActivity;
import com.thel.modules.post.MomentPosterActivity;
import com.thel.utils.ScreenUtils;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class SharePopupWindow extends PopupWindow {

    private Context context;

    private LiveMultiSeatBean liveMultiSeatBean;

    private int role;

    private boolean isFailure = false;

    private String otherUserAvatar;

    private String otherUserNickName;
    private String shareTitle;
    private View view;

    public SharePopupWindow(Context context) {
        this.context = context;
        init();
    }

    private void init() {
        view = LayoutInflater.from(context).inflate(R.layout.pw_share, null);
        ButterKnife.bind(this, view);
        setContentView(view);
        setWidth(ScreenUtils.getScreenWidth(context));
        setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        setFocusable(true);
        setAnimationStyle(R.style.dialogAnim);
        setBackgroundDrawable(new BitmapDrawable());
        setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        setTouchable(true);
        setOutsideTouchable(true);
    }

    public void setData(LiveMultiSeatBean liveMultiSeatBean, int role, boolean isFailure) {
        this.liveMultiSeatBean = liveMultiSeatBean;
        this.role = role;
        this.isFailure = isFailure;
    }

    public void setMatchData(String otherUserAvatar, String otherUserNickName, String title) {
        this.otherUserAvatar = otherUserAvatar;
        this.otherUserNickName = otherUserNickName;
        this.shareTitle = title;
        if (view != null) {
            TextView title_tv = view.findViewById(R.id.title_tv);
            title_tv.setText(title);
        }
    }

    @OnClick(R.id.friends_circle_iv)
    void onFriendsCircleClick() {

        if (liveMultiSeatBean != null) {
            MomentPosterActivity.gotoShare("friends_circle_iv", liveMultiSeatBean, role, isFailure);

        } else {

            MatchPosterActivity.goToShare(context, "friends_circle_iv", otherUserAvatar, otherUserNickName);
        }


    }

    @OnClick(R.id.friends_we_chat_iv)
    void onWeChatClick() {
        if (liveMultiSeatBean != null) {
            MomentPosterActivity.gotoShare("friends_we_chat_iv", liveMultiSeatBean, role, isFailure);
        } else {
            MatchPosterActivity.goToShare(context, "friends_we_chat_iv", otherUserAvatar, otherUserNickName);

        }
    }

    @OnClick(R.id.weibo_iv)
    void onWeiboClick() {
        if (liveMultiSeatBean != null) {
            MomentPosterActivity.gotoShare("weibo_iv", liveMultiSeatBean, role, isFailure);
        } else {
            MatchPosterActivity.goToShare(context, "weibo_iv", otherUserAvatar, otherUserNickName);

        }
    }

    @OnClick(R.id.qq_zone_iv)
    void onQQZoneClick() {
        if (liveMultiSeatBean != null) {
            MomentPosterActivity.gotoShare("qq_zone_iv", liveMultiSeatBean, role, isFailure);
        } else {
            MatchPosterActivity.goToShare(context, "qq_zone_iv", otherUserAvatar, otherUserNickName);

        }
    }

    @OnClick(R.id.qq_iv)
    void onQQClick() {
        if (liveMultiSeatBean != null) {
            MomentPosterActivity.gotoShare("qq_iv", liveMultiSeatBean, role, isFailure);
        } else {
            MatchPosterActivity.goToShare(context, "qq_iv", otherUserAvatar, otherUserNickName);

        }
    }

    @OnClick(R.id.facebook_iv)
    void onFacebookClick() {
        if (liveMultiSeatBean != null) {
            MomentPosterActivity.gotoShare("facebook_iv", liveMultiSeatBean, role, isFailure);
        } else {
            MatchPosterActivity.goToShare(context, "facebook_iv", otherUserAvatar, otherUserNickName);

        }
    }

    @OnClick(R.id.cancel_tv)
    void ondismissClick() {
        this.dismiss();
    }

}
