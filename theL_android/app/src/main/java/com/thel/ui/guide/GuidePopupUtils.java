package com.thel.ui.guide;

import android.app.Activity;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Build;
import androidx.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.PopupWindow;

import com.thel.base.BaseActivity;
import com.thel.utils.Utils;

import java.util.concurrent.TimeUnit;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;

/**
 * Created by waiarl on 2018/2/3.
 */

public class GuidePopupUtils {
    public static void fullScreenImmersive(View contentView) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            int uiOptions = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_FULLSCREEN;
            contentView.setSystemUiVisibility(uiOptions);
        }
    }

    /**
     * 试着显示新手引导
     *
     * @param activity     actvity
     * @param targetView   //这个view是指点击新手引导后，而触发的点击事件的view
     * @param rectView     这个view 是指新手引导显示在哪个view的区域上
     * @param res          新手引导icon
     * @param guideWidth   新手引导 icon 的宽度，可以为0
     * @param guideHeight  新手引导的icon 的高度，为0 的话，icon将不显示
     * @param guideText    新手引导内容
     * @param offsetPointF 引导图的便宜值
     * @param isOnCreate   是否是在oncreate的时候显示，如果再Oncreate的时候显示，需要在view layout之后，才能显示新手引导
     * @param listener
     */
    public static void showNewGuide(@NonNull final Activity activity, final View targetView, @NonNull final View rectView, final int res, final int guideWidth, final int guideHeight, final String[] guideText, final PointF offsetPointF, boolean isOnCreate, final PopupWindow.OnDismissListener listener) {
        if (activity == null || rectView == null) {
            return;
        }
        final ViewGroup parent = Utils.getContentView(activity);
        if (parent == null) {
            return;
        }
        if (isOnCreate) {//如果是在oncreate 或者开始布局的时候调用，需要监听view是否layout之后再展示
            final ViewTreeObserver observer = rectView.getViewTreeObserver();
            observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    rectView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    tryShowTuidePopup(activity, targetView, rectView, res, guideWidth, guideHeight, guideText, offsetPointF, parent, listener);
                }
            });
        } else {
            tryShowTuidePopup(activity, targetView, rectView, res, guideWidth, guideHeight, guideText, offsetPointF, parent, listener);
        }
    }

    /**
     * 经过上一步判断，算出显示的矩形位置，显示popupwindow
     *
     * @return
     */
    protected static NewerGuidePopupWindow tryShowTuidePopup(Activity activity, View targetView, View rectView, int res, int guideWidth, int guideHeight, String[] guideText, PointF offsetPointF, ViewGroup parent, PopupWindow.OnDismissListener listener) {
        final Rect rect = new Rect();
        rectView.getGlobalVisibleRect(rect);
        final RectF rectF = new RectF(rect.left, rect.top, rect.right, rect.bottom);
        return showTuidePopup(activity, targetView, rectF, res, guideWidth, guideHeight, guideText, offsetPointF, parent, listener);
    }

    /**
     * 显示Popwindow
     *
     * @return
     */
    protected static NewerGuidePopupWindow showTuidePopup(@NonNull final Activity activity, View targetView, RectF rectF, int res, int guideWidth, int guideHeight, String[] guideText, PointF offsetPointF, ViewGroup parent, final PopupWindow.OnDismissListener listener) {
        if (activity == null) {
            return null;
        }
        if (activity instanceof BaseActivity) {
            ((BaseActivity) activity).setShowPopup(true);
        }
        final NewerGuidePopupWindow guidePopupWindow = new NewerGuidePopupWindow(activity)
                .initPopup(targetView, res, guideWidth, guideHeight, rectF, guideText, offsetPointF);
        guidePopupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                if (listener != null) {
                    listener.onDismiss();
                }
                if (activity instanceof BaseActivity) {
                    ((BaseActivity) activity).setShowPopup(false);
                }
            }
        });
        guidePopupWindow.setFocusable(false);
        guidePopupWindow.update();
        guidePopupWindow.show(parent);
        GuidePopupUtils.fullScreenImmersive(guidePopupWindow.getContentView());
        guidePopupWindow.update();
        return guidePopupWindow;
    }

    /**
     * 试着显示新手引导
     *
     * @param activity     actvity
     * @param targetView   //这个view是指点击新手引导后，而触发的点击事件的view
     * @param rectView     这个view 是指新手引导显示在哪个view的区域上
     * @param res          新手引导icon
     * @param guideWidth   新手引导 icon 的宽度，可以为0
     * @param guideHeight  新手引导的icon 的高度，为0 的话，icon将不显示
     * @param guideText    新手引导内容
     * @param offsetPointF 引导图的便宜值
     * @param isOnCreate   是否是在oncreate的时候显示，如果再Oncreate的时候显示，需要在view layout之后，才能显示新手引导
     * @param listener
     * @param guideGravity 重力，引导是显示在目标矩形的下方还是上方，目前仅支持这两种位置
     */
    public static void showNewGuide(@NonNull final Activity activity, final View targetView, @NonNull final View rectView, final int res, final int guideWidth, final int guideHeight, final String[] guideText, final PointF offsetPointF, boolean isOnCreate, final PopupWindow.OnDismissListener listener, final int guideGravity) {
        if (activity == null || rectView == null) {
            return;
        }
        final ViewGroup parent = Utils.getContentView(activity);
        if (parent == null) {
            return;
        }
        if (isOnCreate) {//如果是在oncreate 或者开始布局的时候调用，需要监听view是否layout之后再展示
            final ViewTreeObserver observer = rectView.getViewTreeObserver();
            observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    rectView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    tryShowTuidePopup(activity, targetView, rectView, res, guideWidth, guideHeight, guideText, offsetPointF, parent, listener, guideGravity);
                }
            });
        } else {
            tryShowTuidePopup(activity, targetView, rectView, res, guideWidth, guideHeight, guideText, offsetPointF, parent, listener, guideGravity);
        }
    }

    /**
     * 带重力的Popwindow显示
     *
     * @param guideGravity
     */
    private static void tryShowTuidePopup(Activity activity, View targetView, View rectView, int res, int guideWidth, int guideHeight, String[] guideText, PointF offsetPointF, ViewGroup parent, PopupWindow.OnDismissListener listener, int guideGravity) {
        final NewerGuidePopupWindow popupWindow = tryShowTuidePopup(activity, targetView, rectView, res, guideWidth, guideHeight, guideText, offsetPointF, parent, listener);
        if (popupWindow != null) {
            popupWindow.setNewGuideGravity(guideGravity);
        }
    }

    private static NewerSimpleGuidePopupWindow newerSimpleGuidePopupWindow;

    public static void showSimpleGuideView(final GuidePopupBuilder builder) {

        try {
            if (builder == null || !builder.isBuildable()) {
                return;
            }
            final ViewGroup parent = Utils.getContentView(builder.activity);
            if (parent == null) {
                return;
            }
            if (builder.isOncreate) {
                final ViewTreeObserver observer = builder.rectView.getViewTreeObserver();
                observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        builder.rectView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                        newerSimpleGuidePopupWindow = tryShowSimpleTuidePopup(builder, parent);
                    }
                });
            } else {
                newerSimpleGuidePopupWindow = tryShowSimpleTuidePopup(builder, parent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static NewerSimpleGuidePopupWindow tryShowSimpleTuidePopup(final GuidePopupBuilder builder, ViewGroup parent) {
        if (builder.activity == null) {
            return null;
        }
        if (builder.activity instanceof BaseActivity) {
            ((BaseActivity) builder.activity).setShowPopup(true);
        }
        final Rect rect = new Rect();
        builder.rectView.getGlobalVisibleRect(rect);
        final RectF rectF = new RectF(rect.left, rect.top, rect.right, rect.bottom);
        builder.builder.setTargetRect(rectF);

        final NewerSimpleGuidePopupWindow guidePopupWindow = new NewerSimpleGuidePopupWindow(builder.activity).initPopup(builder.builder);
        guidePopupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                if (builder.listener != null) {
                    builder.listener.onDismiss();
                }
                if (builder.activity instanceof BaseActivity) {
                    ((BaseActivity) builder.activity).setShowPopup(false);
                }
            }
        });
        guidePopupWindow.setFocusable(false);
        guidePopupWindow.update();
        guidePopupWindow.show(parent);
        GuidePopupUtils.fullScreenImmersive(guidePopupWindow.getContentView());
        guidePopupWindow.update();
        Flowable.timer(5, TimeUnit.SECONDS).onBackpressureDrop().observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Long>() {
            @Override
            public void accept(Long aLong) {
                if (guidePopupWindow != null && guidePopupWindow.isShowing()) {
                    guidePopupWindow.dismiss();
                }
            }
        });
        return guidePopupWindow;

    }

    public static void dismiss() {
        if (newerSimpleGuidePopupWindow != null) {
            newerSimpleGuidePopupWindow.dismiss();
        }
    }

}
