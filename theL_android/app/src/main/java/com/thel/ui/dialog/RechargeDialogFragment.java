package com.thel.ui.dialog;

import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;

import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.meituan.android.walle.WalleChannelReader;
import com.rela.pay.OnPayStatusListener;
import com.rela.pay.PayConstants;
import com.rela.pay.PayProxyImpl;
import com.thel.BuildConfig;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseAdapter;
import com.thel.base.BaseDialogFragment;
import com.thel.bean.PayOrderBean;
import com.thel.constants.TheLConstants;
import com.thel.growingio.GIoGiftTrackBean;
import com.thel.growingio.GrowingIoConstant;
import com.thel.imp.gold.GoldUtils;
import com.thel.modules.live.bean.SoftGold;
import com.thel.modules.live.bean.SoftMoneyListBean;
import com.thel.modules.live.ctrl.FirstChargeCtrl;
import com.thel.modules.main.me.aboutMe.MySoftMoneyActivity;
import com.thel.modules.main.me.match.eventcollect.collect.PayLogUtils;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.RequestConstants;
import com.thel.payment.GooglePayment;
import com.thel.payment.OnPaymentListener;
import com.thel.ui.RechargeRecyclerView;
import com.thel.ui.adapter.RechargeAdapter;
import com.thel.ui.decoration.RechargeItemDivider;
import com.thel.utils.DialogUtil;
import com.thel.utils.GooglePayUtils;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;
import com.thel.utils.ScreenUtils;
import com.thel.utils.SizeUtils;
import com.thel.utils.Utils;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class RechargeDialogFragment extends BaseDialogFragment {

    private static final String TAG = "RechargeDialogFragment";

    @BindView(R.id.recharge_rv)
    RechargeRecyclerView recharge_rv;

    @BindView(R.id.txt_soft_money)
    TextView txt_soft_money;

    @BindView(R.id.tips_tv)
    TextView tips_tv;

    @BindView(R.id.tv_recharge_bg)
    TextView tv_recharge_bg;

    private RechargeAdapter mRechargeAdapter;

    private List<String> skuIds;

    private List<SoftGold> softGolds;

    private GooglePayment mGooglePayment;

    private String tips = null;
    private int direction = 0;
    private String fromAction = "";
    private String pageId;
    private String product_type;
    private String payType;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_fragment_recharge, container);

        ButterKnife.bind(this, view);

        if (tips == null || tips.length() == 0) {
            tips_tv.setVisibility(View.GONE);
        } else {
            tips_tv.setVisibility(View.VISIBLE);
            tips_tv.setText(tips);
        }

        if (direction == 1) {
            tv_recharge_bg.setBackgroundResource(R.drawable.shape_gift_top);
        } else {
            tv_recharge_bg.setBackgroundColor(ContextCompat.getColor(TheLApp.context, R.color.white));

        }
        initAdapter();

        getGoldListNetData();

        return view;
    }

    public void setTips(String tips) {
        this.tips = tips;
    }

    public void setHorizontal(int direction) {
        this.direction = direction;

    }

    private void initAdapter() {

        mRechargeAdapter = new RechargeAdapter(getContext());

        recharge_rv.setHasFixedSize(true);

        recharge_rv.addItemDecoration(new RechargeItemDivider(SizeUtils.dip2px(TheLApp.context, 10)));

        recharge_rv.setLayoutManager(new GridLayoutManager(getContext(), 2));

        recharge_rv.setAdapter(mRechargeAdapter);

        mRechargeAdapter.setOnItemClickListener(new BaseAdapter.OnItemClickListener<SoftGold>() {
            @Override
            public void onItemClick(View view, SoftGold item, int position) {
                List<SoftGold> golds = mRechargeAdapter.getData();
                if (golds != null && golds.size() > position) {
                    buyGold(golds.get(position));
                }
            }
        });

    }

    private void getGoldListNetData() {

        L.d(TAG, " getGoldListNetData : ");

        String channel = WalleChannelReader.getChannel(TheLApp.context);

        if (TextUtils.isEmpty(channel) || (!TextUtils.isEmpty(channel) && !channel.equals("vivo"))) {
            channel = "";
        }

        L.d(TAG, " getGoldListNetData channel : " + channel);

        RequestBusiness.getInstance().getGoldListData(channel).onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<SoftMoneyListBean>() {
            @Override
            public void onNext(SoftMoneyListBean softMoney) {
                super.onNext(softMoney);

                L.d(TAG, " onNext : ");

                if (softMoney == null || softMoney.data == null || softMoney.data.list == null)
                    return;

                softGolds = softMoney.data.list;

                L.d(TAG, " onNext : " + softGolds.size());

                L.d(TAG, "");

                GoldUtils.sendGoldChangedBroad(softMoney.data.gold);

                mRechargeAdapter.setData(softGolds);

                txt_soft_money.setText(TheLApp.context.getResources().getString(R.string.balance) + TheLApp.context.getResources().getString(R.string.soft_money, softMoney.data.gold));

                if (RequestConstants.APPLICATION_ID_GLOBAL.equals(BuildConfig.APPLICATION_ID)) {
                    skuIds = new ArrayList<>();
                    for (SoftGold softGold : softMoney.data.list) {
                        softGold.isPendingPrice = true;
                        skuIds.add(softGold.iapId);
                    }
                    if (skuIds != null && softGolds != null) {

                        mGooglePayment = new GooglePayment(skuIds, softGolds);

                    }

                }


            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);

                L.d(TAG, " onError : " + t.getMessage());

            }
        });

    }

    @OnClick(R.id.customer_service_tv)
    void goToCustomerService() {

        Utils.gotoUDesk();
        PayLogUtils.getInstance().reportSoftOrVipPayLog(GrowingIoConstant.PAGE_LIVE_ROOM, pageId, "click_service", TheLConstants.IsFirstCharge);

    }

    /**
     * 购买点击的商品
     *
     * @param gold 购买的商品
     */
    private void buyGold(final SoftGold gold) {
        /**
         * 打点 软妹豆充值种类点击
         * */
        product_type = gold.gemPrice + "元" + gold.gold;
        PayLogUtils.getInstance().reportShortLiveSoftPayLog(GrowingIoConstant.PAGE_LIVE_ROOM, pageId, "click_product", TheLConstants.IsFirstCharge, fromAction, gold.gemPrice + "元" + gold.gold, -1);

        if (RequestConstants.APPLICATION_ID_LOCAL.equals(BuildConfig.APPLICATION_ID))
            DialogUtil.getInstance().showSelectionDialogWithIcon(getActivity(), getString(R.string.pay_options), new String[]{getString(R.string.pay_alipay), getString(R.string.pay_wx)}, new int[]{R.mipmap.icon_zhifubao, R.mipmap.icon_wechat}, (parent, v, position, id) -> {
                DialogUtil.getInstance().closeDialog();
                switch (position) {
                    case 0:// 支付宝

                        payType = PayConstants.PAY_TYPE_ALIPAY;

                        RequestBusiness.getInstance().createStickerPackOrder(TheLConstants.PRODUCT_TYPE_SOFT_GOLD, gold.id + "", PayConstants.PAY_TYPE_ALIPAY, PayConstants.ALIPAY_SOURCE).observeOn(AndroidSchedulers.mainThread()).onBackpressureDrop().subscribeOn(Schedulers.io()).subscribe(new InterceptorSubscribe<PayOrderBean>() {
                            @Override public void onNext(PayOrderBean data) {
                                super.onNext(data);

                                PayProxyImpl.getInstance().pay(PayConstants.PAY_TYPE_ALIPAY, getActivity(), GsonUtils.createJsonString(data.data), new OnPayStatusListener() {
                                    @Override public void onPayStatus(int payStatus, String errorInfo) {


                                        if (payStatus == 0) {
                                            if (getActivity() != null && tips_tv != null && isAdded()) {
                                                tips_tv.setVisibility(View.VISIBLE);
                                                tips_tv.setText(TheLApp.context.getResources().getString(R.string.recharge_failed));

                                            }

                                            switch (errorInfo) {
                                                case "8000":
                                                    PayLogUtils.getInstance().reportLongLiveSoftPayLog(GrowingIoConstant.PAGE_LIVE_ROOM, pageId, "click_paytype", TheLConstants.IsFirstCharge, fromAction, product_type, payType, 0, "因为支付渠道原因或者系统原因还在等待支付结果确认，最终交易是否成功以服务端异步通知为准（小概率状态）");

                                                    break;
                                                case "6001":
                                                    PayLogUtils.getInstance().reportLongLiveSoftPayLog(GrowingIoConstant.PAGE_LIVE_ROOM, pageId, "click_paytype", TheLConstants.IsFirstCharge, fromAction, product_type, payType, 0, TheLApp.context.getString(R.string.pay_canceled));
                                                    break;
                                                case "0":
                                                    PayLogUtils.getInstance().reportLongLiveSoftPayLog(GrowingIoConstant.PAGE_LIVE_ROOM, pageId, "click_paytype", TheLConstants.IsFirstCharge, fromAction, product_type, payType, 0, TheLApp.getContext().getString(R.string.pay_failed));

                                                    break;


                                            }

                                        } else {
                                            getGoldListNetData();
                                            PayLogUtils.getInstance().reportLongLiveSoftPayLog(GrowingIoConstant.PAGE_LIVE_ROOM, pageId, "click_paytype", TheLConstants.IsFirstCharge, fromAction, product_type, payType, 1, "");
                                            FirstChargeCtrl.get().updateWhenPaymentSuccessful();
                                        }

                                        PayLogUtils.getInstance().reportLongLiveSoftPayLog(GrowingIoConstant.PAGE_LIVE_ROOM, pageId, "click_paytype", TheLConstants.IsFirstCharge, fromAction, product_type, PayConstants.PAY_TYPE_ALIPAY, -1, "");
                                    }
                                });

                            }
                        });

                        break;
                    case 1: // 微信支付

                        payType = PayConstants.PAY_TYPE_WXPAY;

                        RequestBusiness.getInstance().createStickerPackOrder(TheLConstants.PRODUCT_TYPE_SOFT_GOLD, gold.id + "", PayConstants.PAY_TYPE_WXPAY, PayConstants.WXPAY_SOURCE).observeOn(AndroidSchedulers.mainThread()).onBackpressureDrop().subscribeOn(Schedulers.io()).subscribe(new InterceptorSubscribe<PayOrderBean>() {
                            @Override public void onNext(PayOrderBean data) {
                                super.onNext(data);

                                PayProxyImpl.getInstance().pay(PayConstants.PAY_TYPE_WXPAY, getActivity(), GsonUtils.createJsonString(data.data), new OnPayStatusListener() {
                                    @Override public void onPayStatus(int payStatus, String errorInfo) {

                                        MobclickAgent.onEvent(getActivity(), "create_order_succeed");

                                        if (payStatus == 1) {
                                            getGoldListNetData();
                                            PayLogUtils.getInstance().reportLongLiveSoftPayLog(GrowingIoConstant.PAGE_LIVE_ROOM, pageId, "click_paytype", TheLConstants.IsFirstCharge, fromAction, product_type, payType, 1, "");
                                            FirstChargeCtrl.get().updateWhenPaymentSuccessful();
                                        } else {

                                            if (getActivity() != null && tips_tv != null && isAdded()) {
                                                tips_tv.setVisibility(View.VISIBLE);
                                                tips_tv.setText(TheLApp.context.getResources().getString(R.string.recharge_failed));

                                            }

                                            if (errorInfo.equals("-1")) {
                                                PayLogUtils.getInstance().reportLongLiveSoftPayLog(GrowingIoConstant.PAGE_LIVE_ROOM, pageId, "click_paytype", TheLConstants.IsFirstCharge, fromAction, product_type, payType, 0, TheLApp.getContext().getString(R.string.pay_failed));

                                            }

                                            if (errorInfo.equals("-2")) {
                                                PayLogUtils.getInstance().reportLongLiveSoftPayLog(GrowingIoConstant.PAGE_LIVE_ROOM, pageId, "click_paytype", TheLConstants.IsFirstCharge, fromAction, product_type, payType, 0, TheLApp.getContext().getString(R.string.pay_canceled));
                                            }
                                        }


                                    }
                                });

                            }
                        });


                        PayLogUtils.getInstance().reportLongLiveSoftPayLog(GrowingIoConstant.PAGE_LIVE_ROOM, pageId, "click_paytype", TheLConstants.IsFirstCharge, fromAction, product_type, PayConstants.PAY_TYPE_WXPAY, -1, "");

                        break;
                    default:
                        break;
                }
            }, false, null);
        else {// 国际版，用google wallet
            if (!gold.isPendingPrice) {
                if (mGooglePayment != null) {
                    payType = GooglePayUtils.GOOGLEPAY;

                    mGooglePayment.pay(getActivity(), String.valueOf(gold.id), new MyOnPaymentListener());
                    PayLogUtils.getInstance().reportLongLiveSoftPayLog(GrowingIoConstant.PAGE_LIVE_ROOM, pageId, "click_paytype", TheLConstants.IsFirstCharge, fromAction, product_type, GooglePayUtils.GOOGLEPAY, -1, "");

                }
            }
        }
    }

    public void setFromAction(String fromAction, String pageId) {
        this.fromAction = fromAction;
        this.pageId = pageId;
    }

    private class MyOnPaymentListener implements OnPaymentListener {

        @Override
        public void onBayGoldSucceed() {
            getGoldListNetData();
            PayLogUtils.getInstance().reportLongLiveSoftPayLog(GrowingIoConstant.PAGE_LIVE_ROOM, pageId, "click_paytype", TheLConstants.IsFirstCharge, fromAction, product_type, payType, 1, "");
            FirstChargeCtrl.get().updateWhenPaymentSuccessful();
        }

        @Override
        public void onBayGoldFail() {
            if (getActivity() != null && tips_tv != null && isAdded()) {
                tips_tv.setVisibility(View.VISIBLE);
                tips_tv.setText(TheLApp.context.getResources().getString(R.string.recharge_failed));

            }
        }

        @Override
        public void onPayResult(String result) {
            switch (result) {
                case "8000":
                    PayLogUtils.getInstance().reportLongLiveSoftPayLog(GrowingIoConstant.PAGE_LIVE_ROOM, pageId, "click_paytype", TheLConstants.IsFirstCharge, fromAction, product_type, payType, 0, "因为支付渠道原因或者系统原因还在等待支付结果确认，最终交易是否成功以服务端异步通知为准（小概率状态）");

                    break;
                case "6001":
                    PayLogUtils.getInstance().reportLongLiveSoftPayLog(GrowingIoConstant.PAGE_LIVE_ROOM, pageId, "click_paytype", TheLConstants.IsFirstCharge, fromAction, product_type, payType, 0, TheLApp.context.getString(R.string.pay_canceled));

                    break;
                case "0":
                    PayLogUtils.getInstance().reportLongLiveSoftPayLog(GrowingIoConstant.PAGE_LIVE_ROOM, pageId, "click_paytype", TheLConstants.IsFirstCharge, fromAction, product_type, payType, 0, TheLApp.getContext().getString(R.string.pay_failed));

                    break;

                case "-1":
                    PayLogUtils.getInstance().reportLongLiveSoftPayLog(GrowingIoConstant.PAGE_LIVE_ROOM, pageId, "click_paytype", TheLConstants.IsFirstCharge, fromAction, product_type, payType, 0, TheLApp.getContext().getString(R.string.pay_failed));

                    break;
                case "-2":
                    PayLogUtils.getInstance().reportLongLiveSoftPayLog(GrowingIoConstant.PAGE_LIVE_ROOM, pageId, "click_paytype", TheLConstants.IsFirstCharge, fromAction, product_type, payType, 0, TheLApp.getContext().getString(R.string.pay_canceled));

                    break;

            }

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mGooglePayment != null) {
            mGooglePayment.destroy();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {

            int screenHeight = ScreenUtils.getScreenHeight(TheLApp.context);

            int screenWidth = ScreenUtils.getScreenWidth(TheLApp.context);

            if (screenHeight < screenWidth) {

                screenWidth = screenHeight;
            }

            DisplayMetrics dm = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
            WindowManager.LayoutParams attributes = dialog.getWindow().getAttributes();
            attributes.gravity = Gravity.BOTTOM;//对齐方式
            dialog.getWindow().setAttributes(attributes);
            dialog.getWindow().setLayout(screenWidth, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        try {
            PayLogUtils.getInstance().reportShortLiveSoftPayLog(GrowingIoConstant.PAGE_LIVE_ROOM, pageId, "click_back", TheLConstants.IsFirstCharge, fromAction, product_type, -1);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
