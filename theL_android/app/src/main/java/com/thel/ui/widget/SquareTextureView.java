package com.thel.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.TextureView;

/**
 * Created by liuyun on 2017/10/11.
 */

public class SquareTextureView extends TextureView {
    public SquareTextureView(Context context) {
        super(context);
    }

    public SquareTextureView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareTextureView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
//        setMeasuredDimension(getMeasuredWidth(), getMeasuredWidth());
    }
}
