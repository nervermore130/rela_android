package com.thel.ui;

import android.text.Spannable;
import android.text.method.LinkMovementMethod;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.TextView;

/**
 * Created by the L on 2016/10/21.
 */

public class ParentClickLinkMovementMethod extends LinkMovementMethod {
    @Override
    public boolean onTouchEvent(TextView widget, Spannable buffer, MotionEvent event) {
        boolean b = super.onTouchEvent(widget, buffer, event);
        //解决点击事件冲突问题
        if (!b && event.getAction() == MotionEvent.ACTION_UP) {
            ViewParent parent = widget.getParent();//处理widget的父控件点击事件
            if (parent != null && parent instanceof ViewGroup) {
                final boolean result = ((ViewGroup) parent).performClick();
                return result;
            }
        }
        return b;
    }


    public static ParentClickLinkMovementMethod getInstance() {
        if (sInstance == null)
            sInstance = new ParentClickLinkMovementMethod();

        return sInstance;
    }

    private static ParentClickLinkMovementMethod sInstance;
}
