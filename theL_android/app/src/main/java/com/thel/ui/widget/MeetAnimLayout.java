package com.thel.ui.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import androidx.annotation.NonNull;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.live.LiveMultiSeatBean;
import com.thel.manager.ImageLoaderManager;
import com.thel.utils.ScreenUtils;
import com.thel.utils.SizeUtils;
import com.thel.utils.UserUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author liuyun
 */
public class MeetAnimLayout extends RelativeLayout {

    @BindView(R.id.left_iv) ImageView left_iv;

    @BindView(R.id.right_iv) ImageView right_iv;

    @BindView(R.id.meet_ae_anim_view) LottieAnimationView meet_ae_anim_view;

    @BindView(R.id.heart_anim_view) LottieAnimationView heart_anim_view;

    @BindView(R.id.left_view) LinearLayout left_view;

    @BindView(R.id.right_view) LinearLayout right_view;

    @BindView(R.id.user_name_left_tv) TextView user_name_left_tv;

    @BindView(R.id.user_name_right_tv) TextView user_name_right_tv;


    public MeetAnimLayout(Context context) {
        super(context);

        init(context);
    }

    public MeetAnimLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);

    }

    public MeetAnimLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);

    }

    private void init(Context context) {

        View view = LayoutInflater.from(context).inflate(R.layout.layout_meet_anim, this, true);

        ButterKnife.bind(view);

        left_iv = view.findViewById(R.id.left_iv);
        right_iv = view.findViewById(R.id.right_iv);
        meet_ae_anim_view = view.findViewById(R.id.meet_ae_anim_view);
        heart_anim_view = view.findViewById(R.id.heart_anim_view);
        left_view = view.findViewById(R.id.left_view);
        right_view = view.findViewById(R.id.right_view);

    }

    public void setData(@NonNull LiveMultiSeatBean.CoupleDetail leftData, @NonNull LiveMultiSeatBean.CoupleDetail rightData) {

        ImageLoaderManager.imageLoaderCircle(left_iv, R.mipmap.icon_user, leftData.avatar);

        user_name_left_tv.setText(rightData.nickname);

        ImageLoaderManager.imageLoaderCircle(right_iv, R.mipmap.icon_user, rightData.avatar);

        user_name_right_tv.setText(leftData.nickname);

    }

    public void setData(@NonNull String avatarUrl, @NonNull String nickname) {

        ImageLoaderManager.imageLoaderCircle(left_iv, R.mipmap.icon_user, avatarUrl);

        user_name_left_tv.setText(UserUtils.getNickName());

        user_name_left_tv.setTextColor(TheLApp.context.getResources().getColor(R.color.match_success_nickname));

        ImageLoaderManager.imageLoaderCircle(right_iv, R.mipmap.icon_user, UserUtils.getUserAvatar());

        user_name_right_tv.setText(nickname);

        user_name_right_tv.setTextColor(TheLApp.context.getResources().getColor(R.color.match_success_nickname));

    }

    @Override protected void onAttachedToWindow() {
        super.onAttachedToWindow();

        startAnimation();

    }

    @Override protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        if (meet_ae_anim_view != null) {
            meet_ae_anim_view.cancelAnimation();
            meet_ae_anim_view = null;
        }
    }

    private void startAnimation() {

        leftIn();

        rightIn();

    }

    private void leftIn() {

        int startX = -SizeUtils.dip2px(getContext(), 100);

        int endX = ScreenUtils.getScreenWidth(getContext()) / 2 - SizeUtils.dip2px(getContext(), 90);

        startAnimator(startX, endX, left_view);


    }

    private void rightIn() {

        int startX = ScreenUtils.getScreenWidth(getContext()) + SizeUtils.dip2px(getContext(), 100);

        int endX = ScreenUtils.getScreenWidth(getContext()) / 2 - SizeUtils.dip2px(getContext(), 10);

        startAnimator(startX, endX, right_view);

    }

    private void startAnimator(int start, int end, final View view) {


        ValueAnimator valueAnimator = ValueAnimator.ofInt(start, end);

        valueAnimator.setDuration(500);

        valueAnimator.setInterpolator(new LinearInterpolator());

        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override public void onAnimationUpdate(ValueAnimator animation) {

                int offset = (int) animation.getAnimatedValue();

                view.setX(offset);

            }
        });

        valueAnimator.addListener(new AnimatorListenerAdapter() {
            @Override public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);

                if (view != null) {
                    view.setLayerType(LAYER_TYPE_NONE, null);

                    if (view == left_view) {
                        meet_ae_anim_view.setAnimation("meet_success_bg.json");
                        meet_ae_anim_view.playAnimation();

                        heart_anim_view.setAnimation("meet_success_heart.json");
                        heart_anim_view.playAnimation();

                    }
                }

            }

            @Override public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);

                if (view != null) {
                    view.setVisibility(VISIBLE);
                    view.setLayerType(LAYER_TYPE_HARDWARE, null);
                }

            }
        });

        valueAnimator.start();

    }
}
