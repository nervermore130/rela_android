package com.thel.ui.widget;

import android.content.Context;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.util.AttributeSet;

import com.thel.utils.ScreenUtils;
import com.thel.utils.SizeUtils;

public class ScaleRecyclerView extends RecyclerView {

    @Override protected void onMeasure(int widthSpec, int heightSpec) {

        int screenWidth = ScreenUtils.getScreenWidth(getContext());

        int viewWidth = (int) ((screenWidth * 0.7) - 5);

        widthSpec = MeasureSpec.makeMeasureSpec(viewWidth, MeasureSpec.EXACTLY);

        int viewHeight = SizeUtils.dip2px(getContext(), 200);

        heightSpec = MeasureSpec.makeMeasureSpec(viewHeight, MeasureSpec.EXACTLY);

        super.onMeasure(widthSpec, heightSpec);
    }

    public ScaleRecyclerView(Context context) {
        super(context);
    }

    public ScaleRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ScaleRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
}
