package com.thel.ui.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by the L on 2016/10/11.
 */

public class ExpandTextView extends androidx.appcompat.widget.AppCompatTextView {
    private TextChangedListener listener;

    public ExpandTextView(Context context) {
        this(context, null);
    }

    public ExpandTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ExpandTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initTextView();
    }

    private void initTextView() {

    }

    public void setTextChangedListener(TextChangedListener listener) {
        this.listener = listener;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (listener != null) {
            listener.textChanged(this);
        }
    }

    public interface TextChangedListener {
        void textChanged(TextView textView);
    }
}