package com.thel.ui.guide;

import android.app.Activity;
import android.view.View;
import android.widget.PopupWindow;

/**
 * Created by waiarl on 2018/4/18.
 */

public class GuidePopupBuilder {
    public Activity activity;
    public View targetView;
    public boolean isOncreate;
    public PopupWindow.OnDismissListener listener;
    public GuideViewBuilder builder;
    public View rectView;

    public GuidePopupBuilder setActivity(Activity activity) {
        this.activity = activity;
        return this;
    }

    public GuidePopupBuilder setTargetView(View targetView) {
        this.targetView = targetView;
        return this;
    }

    public GuidePopupBuilder setOncreate(boolean oncreate) {
        isOncreate = oncreate;
        return this;
    }

    public GuidePopupBuilder setListener(PopupWindow.OnDismissListener listener) {
        this.listener = listener;
        return this;
    }

    public GuidePopupBuilder setBuilder(GuideViewBuilder builder) {
        this.builder = builder;
        return this;
    }

    public GuidePopupBuilder setRectView(View rectView) {
        this.rectView = rectView;
        return this;
    }

    public boolean isBuildable() {
        return activity != null && targetView != null && builder != null && rectView != null;
    }
}
