package com.thel.ui.widget.moments;

import android.content.Context;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.ui.widget.ExpandTextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by liuyun on 2017/9/26.
 */

public class MomentTextLayout extends LinearLayout {

    @BindView(R.id.moment_content_text) ExpandTextView moment_content_text;

    @BindView(R.id.txt_whole) TextView txt_whole;

    public MomentTextLayout(Context context) {
        super(context);
        initView();
    }

    public MomentTextLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public MomentTextLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.layout_moment_text, this, true);

        ButterKnife.bind(this, view);

        moment_content_text.setText("flaksdjfalsdfjlaskdjflaskdjflaksdjflaskdjflaksdjflaskdjflaskdjflaksdjfkasldfjlaskdjfalskdfjalskdfjalskdfjlkasdjflaskdfjlasdkjflaskdjflaskdfjlasdkfjlaskdfjalsdkfjlaskdjfalskdfjalsdkjf");
    }

}
