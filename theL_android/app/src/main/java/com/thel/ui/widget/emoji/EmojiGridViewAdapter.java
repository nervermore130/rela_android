package com.thel.ui.widget.emoji;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.StickerBean;
import com.thel.utils.ImageUtils;

import java.util.ArrayList;


/**
 * Created by setsail on 15/8/25.
 */
public class EmojiGridViewAdapter extends BaseAdapter {
    private ArrayList<StickerBean> stickerList = new ArrayList<StickerBean>();

    private LayoutInflater mInflater;

    public EmojiGridViewAdapter(ArrayList<StickerBean> beans) {
        mInflater = (LayoutInflater) TheLApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        refreshAdapter(beans);
    }

    public void refreshAdapter(ArrayList<StickerBean> beans) {
        this.stickerList.clear();
        this.stickerList.addAll(beans);
    }

    @Override
    public int getCount() {
        return stickerList.size();
    }

    @Override
    public Object getItem(int position) {
        return stickerList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        HoldView holdView = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.emoji_grid_item, parent, false);
            holdView = new HoldView();
            holdView.img = convertView.findViewById(R.id.img);
            convertView.setTag(holdView); // 把holdview缓存下来
        } else {
            holdView = (HoldView) convertView.getTag();
        }

        StickerBean stickerBean = stickerList.get(position);
        holdView.img.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(stickerBean.thumbnail))).build()).setAutoPlayAnimations(true).build());

        holdView.img.setTag(stickerBean);

        return convertView;
    }

    class HoldView {
        SimpleDraweeView img;
    }
}
