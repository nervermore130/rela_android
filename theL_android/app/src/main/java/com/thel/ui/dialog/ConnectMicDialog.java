package com.thel.ui.dialog;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseDialogFragment;
import com.thel.constants.TheLConstants;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.live.LiveConstant;
import com.thel.ui.widget.CountDownView;
import com.thel.ui.widget.WhewView;
import com.thel.utils.L;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.thel.constants.TheLConstants.BUNDLE_KEY_NICKNAME;
import static com.thel.constants.TheLConstants.BUNDLE_KEY_USER_AVATAR;

/**
 * Created by liuyun on 2017/12/4.
 */

public class ConnectMicDialog extends BaseDialogFragment {

    public static final int TYPE_ACCEPT = 0x01;//接受

    public static final int TYPE_CANCEL = 0x02;//取消

    public static final int TYPE_REFUSE = 0x03;//拒绝

    public static final int TYPE_REQUEST_NO_RESPONSE = 0x04;//长时间对方没回应（请求超时）

    public static final int TYPE_I_NO_RESPONSE = 0x05;//对方请求 自己长时间没回应

    public static final int TYPE_REQUEST_REFUSE = 0x06;//请求被拒绝

    public static final int TYPE_HANGUP = 0x07;//挂断

    public static final int TYPE_AUDIENCE_LINK_MIC_REQUEST = 0x08;//观众连麦请求

    public static final int TYPE_AUDIENCE_LINK_MIC_RESPONSE = 0x09;//观众连麦响应


    /***直播Pk或者连麦显示的类型***/
    public static final int DIALOG_TYPE_PK_I = 1;//pk请求方
    public static final int DIALOG_TYPE_PK_REQUEST = 2;//pk接受请求方
    public static final int DIALOG_TYPE_MIC_I = 3;//连麦请求方
    public static final int DIALOG_TYPE_MIC_REQUEST = 4;//连麦接受请求方
    public static final int DIALOG_TYPE_HANGUP = 5;

    @BindView(R.id.avatar)
    ImageView avatarIv;

    @BindView(R.id.nickname)
    TextView nickname;

    @BindView(R.id.refuse_tv)
    TextView refuse_tv;

    @BindView(R.id.accept_tv)
    TextView accept_tv;

    @BindView(R.id.single_click_tv)
    TextView single_click_tv;

    @BindView(R.id.title_tv)
    TextView title_tv;

    @BindView(R.id.content_tv)
    TextView content_tv;

    @BindView(R.id.multi_button_ll)
    LinearLayout multi_button_ll;

    @BindView(R.id.count_down_tv)
    CountDownView count_down_tv;

    @BindView(R.id.whewView)
    WhewView whewView;

    private OnButtonClickListener mOnButtonClickListener;
    private CountDownOutListener countDownListener;
    private int type;
    private boolean isShowSingleButton = false;

    private boolean dailyGuard;//是否为日榜连麦

    private boolean mAudience;//是否为观众端

    private boolean mFriendLinkMic;//是否为好友连麦

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_connect_mic, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getDialog().setCanceledOnTouchOutside(false);

        Bundle bundle = getArguments();
        type = bundle.getInt(TheLConstants.BUNDLE_KEY_ACTION_TYPE, DIALOG_TYPE_PK_I);
        isShowSingleButton = (type == DIALOG_TYPE_PK_I) || (type == DIALOG_TYPE_MIC_I) || (type == DIALOG_TYPE_HANGUP);
        String nickName = bundle.getString(BUNDLE_KEY_NICKNAME);
        String avatar = bundle.getString(BUNDLE_KEY_USER_AVATAR);
        dailyGuard = bundle.getBoolean("dailyGuard");
        mAudience = bundle.getBoolean("mAudience");
        mFriendLinkMic = bundle.getBoolean("mFriendLinkMic");

        if (nickName != null) {
            nickname.setText(nickName);
        }

        ImageLoaderManager.imageLoaderCircle(avatarIv, R.mipmap.icon_user, avatar);

        L.d("ConnectMicDialog", " type : " + type);

        if (type == TYPE_AUDIENCE_LINK_MIC_RESPONSE) {

            title_tv.setText(TheLApp.context.getString(R.string.andience_beam));

            count_down_tv.setCountDownListener(new CountDownView.CountDownListener() {
                @Override
                public void onCountDown() {
                    dismiss();
                }
            });

            showIAcceptView();

        } else if (type == TYPE_AUDIENCE_LINK_MIC_REQUEST) {

            if (dailyGuard) {
                title_tv.setText(TheLApp.context.getString(R.string.boardcast_mic));
            } else {
                title_tv.setText(TheLApp.context.getString(R.string.andience_beam));
            }

            count_down_tv.setCountDownListener(new CountDownView.CountDownListener() {
                @Override
                public void onCountDown() {
                    dismiss();
                }
            });

            showRequestAcceptView();

        } else {

            if (type == DIALOG_TYPE_MIC_I) {
                if (dailyGuard) {
                    title_tv.setText(TheLApp.getContext().getResources().getString(R.string.andience_beam));
                } else
                    title_tv.setText(TheLApp.getContext().getResources().getString(R.string.connect_mic_friend));
            } else if (type == DIALOG_TYPE_MIC_REQUEST) {
                if (dailyGuard) {
                    title_tv.setText(TheLApp.getContext().getResources().getString(R.string.boardcast_mic));
                } else
                    title_tv.setText(TheLApp.getContext().getResources().getString(R.string.connect_mic_friend));
            } else {
                if (dailyGuard) {
                    title_tv.setText(TheLApp.getContext().getResources().getString(R.string.andience_beam));
                } else if (mAudience) {
                    title_tv.setText(TheLApp.context.getString(R.string.boardcast_mic));
                } else if (mFriendLinkMic) {
                    title_tv.setText(TheLApp.getContext().getResources().getString(R.string.connect_mic_friend));
                }
                else {
                    title_tv.setText(TheLApp.getContext().getResources().getString(R.string.pk_with_friend));
                }
            }

            String content;

            if (type == DIALOG_TYPE_MIC_I) {
                if (dailyGuard) {
                    content = TheLApp.context.getString(R.string.link_mic_wait);
                } else
                    content = TheLApp.context.getResources().getString(R.string.connect_mic_friend);
            } else if (type == DIALOG_TYPE_PK_I) {
                content = TheLApp.context.getResources().getString(R.string.pk_with_friend);
            } else if (type == DIALOG_TYPE_MIC_REQUEST) {
                content = TheLApp.context.getResources().getString(R.string.invited_you_to_mic, nickName);
            } else if (type == DIALOG_TYPE_PK_REQUEST) {
                content = TheLApp.context.getResources().getString(R.string.invited_you_to_pk, nickName);
            } else {
                content = TheLApp.context.getResources().getString(R.string.she_hang_up);
                content_tv.setTextColor(TheLApp.context.getResources().getColor(R.color.Red));
                whewView.setStatus(WhewView.Status.GRAY);
                single_click_tv.setText(TheLApp.context.getResources().getString(R.string.info_ok));
            }

            content_tv.setText(content);

            if (isShowSingleButton) {
                multi_button_ll.setVisibility(View.GONE);
                single_click_tv.setVisibility(View.VISIBLE);
            } else {
                multi_button_ll.setVisibility(View.VISIBLE);
                single_click_tv.setVisibility(View.GONE);
            }

            whewView.isLinkMic(type == DIALOG_TYPE_MIC_I || type == DIALOG_TYPE_MIC_REQUEST);

            count_down_tv.isLinkMic(type == DIALOG_TYPE_MIC_I || type == DIALOG_TYPE_MIC_REQUEST);

            count_down_tv.setCountDownListener(new CountDownView.CountDownListener() {

                @Override
                public void onCountDown() {
                    if (countDownListener != null) {
                        countDownListener.countDown(type);
                    }

                    dismiss();
                }
            });
            single_click_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnButtonClickListener != null) {
                        if (type == DIALOG_TYPE_HANGUP) {
                            mOnButtonClickListener.onClick(TYPE_HANGUP);
                        } else {
                            mOnButtonClickListener.onClick(TYPE_CANCEL);
                        }
                    }
                }
            });

        }


    }

    @OnClick(R.id.accept_tv)
    void accept() {
        if (mOnButtonClickListener != null) {
            mOnButtonClickListener.onClick(TYPE_ACCEPT);
        }
    }

    @OnClick(R.id.refuse_tv)
    void refuse() {
        if (mOnButtonClickListener != null) {
            mOnButtonClickListener.onClick(TYPE_REFUSE);
        }
    }

    public void setOnButtonClickListener(OnButtonClickListener mOnButtonClickListener) {
        this.mOnButtonClickListener = mOnButtonClickListener;
    }


    public interface OnButtonClickListener {
        void onClick(int type);
    }

    public void cancelThisLinkMicByOwn() {

        L.d("ConnectMicDialog", "----------cancelThisLinkMicByOwn---------");

        if (getActivity() != null && isAdded()) {
            multi_button_ll.setVisibility(View.GONE);
            single_click_tv.setVisibility(View.VISIBLE);
            single_click_tv.setText(TheLApp.context.getResources().getString(R.string.info_ok));
            content_tv.setTextColor(TheLApp.context.getResources().getColor(R.color.Red));
            if (type == LiveConstant.TYPE_LINK_MIC || type == DIALOG_TYPE_MIC_REQUEST) {
                content_tv.setText(TheLApp.getContext().getResources().getString(R.string.she_canceled_mic));
            } else {
                content_tv.setText(TheLApp.getContext().getResources().getString(R.string.she_canceled_pk));
            }
        }
    }

    /**
     * 请求被拒绝
     */
    public void showRequestRefuseView() {

        L.d("ConnectMicDialog", "----------showRequestRefuseView---------");

        if (getActivity() != null && isAdded()) {
            if (type == DIALOG_TYPE_MIC_I || type == DIALOG_TYPE_MIC_REQUEST) {
                content_tv.setText(TheLApp.getContext().getResources().getString(R.string.refused_connecting_mic));
            } else {
                content_tv.setText(TheLApp.getContext().getResources().getString(R.string.she_refused_pk));
            }

            single_click_tv.setText(TheLApp.getContext().getResources().getString(R.string.info_ok));

            whewView.setStatus(WhewView.Status.GRAY);
        }
    }

    /**
     * 我接受了请求
     */
    public void showIAcceptView() {

        if (getActivity() != null && isAdded()) {
            multi_button_ll.setVisibility(View.GONE);
            single_click_tv.setVisibility(View.GONE);
            count_down_tv.setVisibility(View.VISIBLE);
            count_down_tv.start();
            if (type == DIALOG_TYPE_MIC_I || type == DIALOG_TYPE_MIC_REQUEST || type == TYPE_AUDIENCE_LINK_MIC_RESPONSE) {
                content_tv.setText(TheLApp.getContext().getResources().getString(R.string.you_accepted_mic));
            } else {
                content_tv.setText(TheLApp.getContext().getResources().getString(R.string.you_accepted_ok));
            }
        }
    }

    /**
     * 请求被接受
     */
    public void showRequestAcceptView() {

        if (getActivity() != null && isAdded()) {
            if (type == DIALOG_TYPE_MIC_I || type == DIALOG_TYPE_MIC_REQUEST || type == TYPE_AUDIENCE_LINK_MIC_REQUEST) {
                if (dailyGuard) {
                    content_tv.setText(TheLApp.getContext().getResources().getString(R.string.link_mic_wait_timer));
                } else {
                    content_tv.setText(TheLApp.getContext().getResources().getString(R.string.accepted_mic));
                }
            } else {
                content_tv.setText(TheLApp.getContext().getResources().getString(R.string.she_accepter_mic));
            }
            multi_button_ll.setVisibility(View.GONE);
            single_click_tv.setVisibility(View.GONE);
            count_down_tv.setVisibility(View.VISIBLE);
            count_down_tv.start();
            whewView.setStatus(WhewView.Status.GREEN);
        }
    }

    /**
     * 请求被取消
     */
    public void showRequestCancelView() {

        if (getActivity() != null && isAdded()) {
            multi_button_ll.setVisibility(View.GONE);
            single_click_tv.setVisibility(View.VISIBLE);
            single_click_tv.setText(TheLApp.context.getResources().getString(R.string.info_ok));
            content_tv.setTextColor(TheLApp.context.getResources().getColor(R.color.Red));
            if (type == DIALOG_TYPE_MIC_I || type == DIALOG_TYPE_MIC_REQUEST) {
                content_tv.setText(TheLApp.getContext().getResources().getString(R.string.she_canceled_mic));
            } else {
                content_tv.setText(TheLApp.getContext().getResources().getString(R.string.she_canceled_pk));
            }
            whewView.setStatus(WhewView.Status.GRAY);
            count_down_tv.setVisibility(View.GONE);
        }
    }

    /**
     * 我长时间没回应
     */
    public void showITimeOutView() {

        if (getActivity() != null && isAdded()) {

            multi_button_ll.setVisibility(View.GONE);
            single_click_tv.setVisibility(View.VISIBLE);

            String content;

            if (type == DIALOG_TYPE_MIC_I || type == DIALOG_TYPE_MIC_REQUEST) {
                content = TheLApp.getContext().getResources().getString(R.string.havenot_response);
            } else {
                content = TheLApp.getContext().getResources().getString(R.string.you_not_response);
            }
            content_tv.setText(content);
            content_tv.setTextColor(TheLApp.context.getResources().getColor(R.color.red));
            single_click_tv.setText(TheLApp.getContext().getResources().getString(R.string.info_ok));
            single_click_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnButtonClickListener != null) {
                        mOnButtonClickListener.onClick(TYPE_I_NO_RESPONSE);
                    }
                }
            });

            whewView.setStatus(WhewView.Status.GRAY);
        }
    }

    /**
     * 请求超时（对方长时间没回应）
     */
    public void showRequestTimeOutView() {

        if (getActivity() != null && isAdded()) {

            multi_button_ll.setVisibility(View.GONE);
            single_click_tv.setVisibility(View.VISIBLE);

            String content;

            if (type == DIALOG_TYPE_MIC_I || type == DIALOG_TYPE_MIC_REQUEST) {
                content = TheLApp.getContext().getResources().getString(R.string.she_hasnot_response);
            } else {
                content = TheLApp.getContext().getResources().getString(R.string.she_not_response);
            }
            content_tv.setText(content);
            content_tv.setTextColor(TheLApp.context.getResources().getColor(R.color.red));
            single_click_tv.setText(TheLApp.getContext().getResources().getString(R.string.info_ok));
            single_click_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnButtonClickListener != null) {
                        mOnButtonClickListener.onClick(TYPE_REQUEST_NO_RESPONSE);
                    }
                }
            });

            whewView.setStatus(WhewView.Status.GRAY);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (count_down_tv != null) {
            count_down_tv.stop();
        }

        if (whewView != null) {
            whewView.stop();
        }

    }

    /**
     * 设置倒计时监听
     *
     * @param listener
     */
    public void setCountDownOutListener(CountDownOutListener listener) {
        this.countDownListener = listener;
    }

    /**
     * 倒计时结束监听
     */
    public interface CountDownOutListener {
        void countDown(int type);
    }

}
