package com.thel.ui.popupwindow;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.thel.BuildConfig;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.live.LiveMultiSeatBean;
import com.thel.constants.TheLConstants;
import com.thel.growingio.GrowingIoConstant;
import com.thel.imp.follow.bean.SingleUserRelationBean;
import com.thel.imp.follow.bean.SingleUserRelationNetBean;
import com.thel.modules.live.GiftFirstChargeBean;
import com.thel.modules.live.bean.SoftEnjoyBean;
import com.thel.modules.live.bean.SoftEnjoyNetBean;
import com.thel.modules.live.bean.SoftGiftBean;
import com.thel.modules.live.view.SoftGiftListView;
import com.thel.modules.main.me.aboutMe.MySoftMoneyActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.service.DefaultRequestService;
import com.thel.ui.widget.TimeTextView;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;
import com.thel.utils.NetworkUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SimpleDraweeViewUtils;
import com.thel.utils.Utils;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * @author lingwei
 */
public class GiftHorizontalPopupWindow extends PopupWindow {

    private static final String TAG = "GiftHorizontalPopupWindow";

    private Context mContext;

    @BindView(R.id.gift_list_viewpager)
    ViewPager viewPager;

    @BindView(R.id.txt_soft_money)
    TextView txt_soft_money;

    @BindView(R.id.txt_recharge)
    TextView txt_recharge;

    @BindView(R.id.lin_recharge)
    LinearLayout lin_recharge;

    @BindView(R.id.lin_point)
    LinearLayout lin_points;

    @BindView(R.id.rl_guide_gift_lian)
    RelativeLayout rl_guide_gift_lian;

    private SoftEnjoyBean softEnjoyBean;//打赏页面请求返回数据封装对象
    private List<View> pagerViews;//viewpager的页面集合；
    private List<List<SoftGiftBean>> pagerGifts;//以页为单位的礼物集合
    private int pagerGiftLines = 2;//每页显示几行
    private int lineGiftSize = 6;//每行显示的礼物数
    private int pagerGiftSize = pagerGiftLines * lineGiftSize;//每页显示的礼物数
    private int pagerSize;//viewpager的总共页数
    private ViewPagerAdapter viewPagerAdatper;// viewpager adaapter
    private ArrayList<SimpleDraweeView> pointViews = new ArrayList<SimpleDraweeView>();//下方点集合
    private SoftGiftListView.GiftOnClickListener giftClickListener;
    public static final int SCREEN_PORTRAIT = 0;
    public static final int SCREEN_LANDSCAPE = 1;
    private int mOrientation = SCREEN_PORTRAIT;
    private SoftGiftListView.GiftLianClickListener giftLianClickListener;
    private boolean isAnimating = false;
    private LiveMultiSeatBean seatBean;
    private boolean sendAnchor = true;
    private SoftGiftListView.GuestsShowInfoListener guestsShowInfoListener;
    //上一个被点击的view中的timeTextView
    private TimeTextView preTimeTextView;
    private long balance;
    private SoftGiftListView.GotoRechargeSoftMoneyListener gotoSoftMoneyLinsener;


    public GiftHorizontalPopupWindow(Context context, long balance) {
        this.mContext = context;
        this.balance = balance;
        init();
        initData();
    }

    private void init() {
        View view = LayoutInflater.from(mContext).inflate(R.layout.live_gift_horizontal_list_layout, null);
        ButterKnife.bind(this, view);
        setContentView(view);
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        int width = Utils.dip2px(74 * 6);
        int popupHeight = view.getMeasuredHeight();
        setWidth(width);
        setHeight(popupHeight);

        setFocusable(true);
        setAnimationStyle(R.style.dialogAnim);
        setBackgroundDrawable(new BitmapDrawable());
        setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        setTouchable(true);
        setOutsideTouchable(true);

        if (seatBean == null) {
            show();
        }
        updateBalance(balance);
        /****是否是首充**/
        if (TheLConstants.IsFirstCharge != 0) {
            getGiftIsFirstCharge();
        }

        rl_guide_gift_lian.setVisibility(View.VISIBLE);

    }

    public void getGiftIsFirstCharge() {
        Flowable<GiftFirstChargeBean> beanFlowable = DefaultRequestService.createAllRequestService().getIsFirstCharge();
        beanFlowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<GiftFirstChargeBean>() {
            @Override
            public void onNext(GiftFirstChargeBean data) {
                super.onNext(data);

                showIsFirstCharge(data);

            }
        });

    }

    public void showIsFirstCharge(GiftFirstChargeBean firstChargeBean) {
        if (firstChargeBean != null && firstChargeBean.data != null) {
            //缓存到本地
            TheLConstants.IsFirstCharge = firstChargeBean.data.isFirstCharge;
            if (firstChargeBean.data.isFirstCharge == 1) {
                lin_recharge.setBackgroundResource(R.drawable.bg_first_charge_shape_pink);
                txt_recharge.setText(mContext.getText(R.string.first_charge_discount));
            }
        }
    }

    private void initData() {

        if (NetworkUtils.isNetworkConnected(TheLApp.context)) {
            getLiveGiftList();
        } else {
            String json = ShareFileUtils.getString(ShareFileUtils.GIFT_LIST, "{}");
            SoftEnjoyNetBean softEnjoyNetBean = GsonUtils.getObject(json, SoftEnjoyNetBean.class);
            if (softEnjoyNetBean != null && softEnjoyNetBean.data != null) {
                initView(softEnjoyNetBean.data);
            }
        }

    }

    public void getLiveGiftList() {

        String etag = ShareFileUtils.getString(ShareFileUtils.GIFT_ETAG, "");

        final Flowable<SoftEnjoyNetBean> flowable = DefaultRequestService.createLiveRequestService().getLiveGiftList(etag, BuildConfig.APPLICATION_ID);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<SoftEnjoyNetBean>() {
            @Override
            public void onNext(SoftEnjoyNetBean data) {

                if (data != null && data.data != null) {

                    ShareFileUtils.setString(ShareFileUtils.GIFT_ETAG, data.data.md5);

                    if (data.data.list != null) {
                        if (data.data.list.size() <= 0) {
                            String json = ShareFileUtils.getString(ShareFileUtils.GIFT_LIST, "{}");

                            SoftEnjoyNetBean softEnjoyNetBean = GsonUtils.getObject(json, SoftEnjoyNetBean.class);

                            if (softEnjoyNetBean != null && softEnjoyNetBean.data != null) {

                                initView(softEnjoyNetBean.data);
                            }
                        } else {
                            String json = GsonUtils.createJsonString(data);
                            ShareFileUtils.setString(ShareFileUtils.GIFT_LIST, json);
                            initView(data.data);
                        }
                    }

                }


            }
        });
    }

    /**
     * 初始化数据
     */
    public void initView(SoftEnjoyBean softEnjoyBean) {
        this.softEnjoyBean = softEnjoyBean;
        //总共多少页，pagergiftsize目前为8
        pagerSize = (int) Math.ceil(softEnjoyBean.list.size() / (double) pagerGiftSize);
        //viewpager 子vIEW
        pagerViews = new ArrayList<>();
        //以页为单位的礼物集合
        pagerGifts = new ArrayList<>();
        //设置viewpager要显示的view集合
        setListPagerViews();
        //设置下面显示的点
        setPoints();
        //对viewpager进行设置
        setViewpager();
    }

    public void setOrientation(int orientation) {
        if (mOrientation == orientation) {
            return;
        }
        mOrientation = orientation;
        if (SCREEN_PORTRAIT == orientation) {
            //每页显示几行
            pagerGiftLines = 2;
            //每行显示的礼物数
            lineGiftSize = 6;
        } else {
            //每页显示几行
            pagerGiftLines = 1;
            //每行显示的礼物数
            lineGiftSize = 9;
        }
        //每页显示的礼物数
        pagerGiftSize = pagerGiftLines * lineGiftSize;
        if (softEnjoyBean != null) {
            initView(softEnjoyBean);
        }
    }

    /**
     * 设置viewpager要显示的view集合
     */
    private void setListPagerViews() {
        //求取pagerGifts集合
        for (int i = 0; i < softEnjoyBean.list.size(); i++) {
            if (i % pagerGiftSize == 0) {
                List<SoftGiftBean> gifts = new ArrayList<>();
                pagerGifts.add(gifts);
            }
            pagerGifts.get(i / pagerGiftSize).add(softEnjoyBean.list.get(i));
        }
        for (int i = 0; i < pagerSize; i++) {
            //设置每一页
            setPagers(i);
        }
    }

    /**
     * 设置viewpager的每一页显示
     *
     * @param pageIndex 第几页
     */
    private void setPagers(final int pageIndex) {
        final GridView gridView = new GridView(mContext);
        gridView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        gridView.setNumColumns(lineGiftSize);
        gridView.setBackgroundColor(Color.TRANSPARENT);
        gridView.setStretchMode(GridView.STRETCH_COLUMN_WIDTH);
        gridView.setCacheColorHint(0);
        gridView.setSelector(new ColorDrawable(Color.TRANSPARENT));
        gridView.setGravity(Gravity.CENTER_HORIZONTAL);
        //行间距7dp
        gridView.setVerticalSpacing(Utils.dip2px(TheLApp.getContext(), 5));
        SoftGiftAdapter adapter = new SoftGiftAdapter(pagerGifts.get(pageIndex), pageIndex);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                L.d(TAG, " onItemClick giftClickListener : " + giftClickListener);

                if (giftClickListener != null) {
                    TimeTextView timeTextView = view.findViewById(R.id.txt_gift_name);
                    //如果上一个计时器不为空并且不是同一个计时器，并且还处于连击状态
                    if (preTimeTextView != null && preTimeTextView.isCombo() && preTimeTextView != timeTextView) {
                        //显示正常内容
                        preTimeTextView.showContent();
                    }
                    giftClickListener.OnGiftClicked(parent, view, timeTextView, pageIndex, position / lineGiftSize, position % lineGiftSize, position, pageIndex * pagerGiftSize + position, (SoftGiftBean) parent.getAdapter().getItem(position), sendAnchor, seatBean);
                    timeTextView.count();
                    preTimeTextView = timeTextView;
                }
            }
        });

        //添加到viewpager子页面列表
        pagerViews.add(gridView);

    }

    /**
     * 对外接口方法
     *
     * @param listener
     */
    public void setOnGiftClickListener(SoftGiftListView.GiftOnClickListener listener) {
        giftClickListener = listener;
    }

    public void setOnGiftLianClickListener(SoftGiftListView.GiftLianClickListener listener) {
        giftLianClickListener = listener;
    }

    /**
     * 设置下面显示的点
     */
    private void setPoints() {
        lin_points.removeAllViews();
        pointViews.clear();
        SimpleDraweeView imageView;
        if (pagerSize <= 1)
            return;
        for (int i = 0; i < pagerSize; i++) {
            imageView = new SimpleDraweeView(mContext);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(new ViewGroup.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
            layoutParams.leftMargin = 10;
            layoutParams.rightMargin = 10;
            layoutParams.width = Utils.dip2px(TheLApp.getContext(), 6);
            layoutParams.height = layoutParams.width;
            imageView.setBackgroundResource(R.drawable.circle_gray);
            lin_points.addView(imageView, layoutParams);
            if (i == 0) {
                imageView.setBackgroundResource(R.drawable.circle_main_color);
            }
            pointViews.add(imageView);
        }
    }

    /**
     * 对viewpager进行设置
     */
    private void setViewpager() {
        viewPagerAdatper = new ViewPagerAdapter(pagerViews);
        viewPager.setAdapter(viewPagerAdatper);
        viewPager.setCurrentItem(0);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                drawPoint(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    /**
     * 绘制游标背景
     */
    private void drawPoint(int index) {
        for (int i = 0; i < pointViews.size(); i++) {
            if (index == i) {
                pointViews.get(i).setBackgroundResource(R.drawable.circle_main_color);
            } else {
                pointViews.get(i).setBackgroundResource(R.drawable.circle_gray);
            }
        }
    }

    public void refreshAll() {
        for (int i = 0; i < pagerViews.size(); i++) {
            ((SoftGiftAdapter) ((GridView) pagerViews.get(i)).getAdapter()).notifyDataSetChanged();
        }
    }

    /**
     * 更新余额
     *
     * @param balance 余额
     */
    public void updateBalance(long balance) {

        L.d(TAG, " balance : " + balance);

        if (balance > 1) {
            txt_soft_money.setText(TheLApp.context.getString(R.string.balance_credits, balance));
        } else {
            txt_soft_money.setText(TheLApp.context.getString(R.string.balance_credits, balance));
        }
    }

    /**
     * 送给主播
     *
     * @return
     */

    public void show() {
        sendAnchor = true;
    }

    /**
     * 获取是否是好友
     * 如果是好友就不显示关注
     *
     * @param userId
     */
    public void getSingeUserRelation(final String userId) {
        final Flowable<SingleUserRelationNetBean> flowable = DefaultRequestService.createCommonRequestService().getSingleUsersRelation(userId);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<SingleUserRelationNetBean>() {
            @Override
            public void onSubscribe(Subscription s) {
                s.request(1);
            }

            @Override
            public void onNext(SingleUserRelationNetBean singleUserRelationNetBean) {
                final SingleUserRelationBean bean = singleUserRelationNetBean.data;
                refreshFollowStatus(bean);
            }

            @Override
            public void onError(Throwable t) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void refreshFollowStatus(SingleUserRelationBean bean) {
        if (seatBean != null && bean != null) {
            seatBean.followState = bean.status;

        }

    }

    @Override
    public void dismiss() {
        super.dismiss();
        seatBean = null;
        sendAnchor = true;
    }

    /**
     * viewPager Adapter
     */
    class ViewPagerAdapter extends PagerAdapter {

        private final List<View> pagerViews;

        public ViewPagerAdapter(List<View> pagerViews) {
            super();
            this.pagerViews = pagerViews;

        }

        @Override
        public int getCount() {
            return pagerViews.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public int getItemPosition(Object object) {
            return super.getItemPosition(object);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView(pagerViews.get(position));
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            container.addView(pagerViews.get(position));
            return pagerViews.get(position);
        }
    }

    /**
     * GridView adapter
     */
    class SoftGiftAdapter extends BaseAdapter {

        private final LayoutInflater mInflater;
        private final int pageIndex;
        private List<SoftGiftBean> softGifts;
        private float size;

        public SoftGiftAdapter(List<SoftGiftBean> softGiftBeen, int pageIndex) {
            mInflater = (LayoutInflater) TheLApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            setGifts(softGiftBeen);
            size = TheLApp.getContext().getResources().getDimension(R.dimen.gift_horizontal_icon_size);
            this.pageIndex = pageIndex;
        }

        private void setGifts(List<SoftGiftBean> softGiftBeen) {
            if (softGiftBeen != null) {
                this.softGifts = softGiftBeen;
            } else {
                softGifts = new ArrayList<>();
            }
        }

        @Override
        public int getCount() {
            return softGifts.size();
        }

        @Override
        public Object getItem(int position) {
            return softGifts.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, final ViewGroup parent) {
            SoftGiftAdapter.ViewHolder holder = null;

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.live_soft_gift_horizontal_item, parent, false);
                holder = new SoftGiftAdapter.ViewHolder();
                holder.icon_gift_soft = convertView.findViewById(R.id.icon_soft_gift);
                holder.txt_gift_name = convertView.findViewById(R.id.txt_gift_name);
                holder.txt_gift_price = convertView.findViewById(R.id.txt_gift_price);
                holder.img_is_hot = convertView.findViewById(R.id.img_is_hot);
                holder.img_lian = convertView.findViewById(R.id.img_lian);
                convertView.setTag(holder);
            } else {
                holder = (SoftGiftAdapter.ViewHolder) convertView.getTag();
            }

            SoftGiftBean softGiftBean = softGifts.get(position);

            SimpleDraweeViewUtils.setImageUrl(holder.icon_gift_soft, softGiftBean.icon, size, size);
            holder.txt_gift_name.getInstance(20, 100, 100).setContent(softGiftBean.title + "");
            if (softGiftBean.gold > 1) {
                holder.txt_gift_price.setText(mContext.getString(R.string.soft_moneys, softGiftBean.gold));
            } else {
                holder.txt_gift_price.setText(mContext.getString(R.string.soft_money, softGiftBean.gold));
            }
            holder.img_is_hot.setVisibility(View.GONE);

            if (softGiftBean.canCombo > 0) {
                holder.img_lian.setVisibility(View.VISIBLE);
            } else {
                holder.img_lian.setVisibility(View.GONE);
            }
            final View currentView = convertView;
            holder.img_lian.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    L.d(TAG, " giftLianClickListener : " + giftLianClickListener);

                    if (giftLianClickListener != null) {

                        int seatNum = -1;

                        if (seatBean != null) {

                            seatNum = seatBean.seatNum;

                        }

                        giftLianClickListener.onGiftClicked(parent, currentView, v, pageIndex, position / lineGiftSize, position % lineGiftSize, position, pagerSize * pageIndex + position, softGifts.get(position), sendAnchor, seatNum);
                    }
                }
            });
            return convertView;
        }

        class ViewHolder {
            SimpleDraweeView icon_gift_soft;//图标
            TimeTextView txt_gift_name;//名字，title
            TextView txt_gift_price;//价格，gold
            ImageView img_is_hot;//是否热门（新加）
            ImageView img_lian;//连击按钮
        }
    }

    @OnClick(R.id.lin_recharge)
    void goRecharge() {
        if (gotoSoftMoneyLinsener != null) {
            gotoSoftMoneyLinsener.onGotoRechageClicked();
        }
        //gotoSoftMoneyActivity();
    }

    /**
     * 跳转到充值软妹豆界面
     */
    private void gotoSoftMoneyActivity() {
        Activity activity = (Activity) mContext;
        final Intent intent = new Intent(mContext, MySoftMoneyActivity.class);
        GrowingIOUtil.payTrack(GrowingIoConstant.LIVEPAGE_CLICK);
        activity.startActivityForResult(intent, TheLConstants.REQUEST_CODE_RECHARGE);
    }

    public void setGotoSoftMoneyLinsener(SoftGiftListView.GotoRechargeSoftMoneyListener gotoSoftMoneyLinsener) {
        this.gotoSoftMoneyLinsener = gotoSoftMoneyLinsener;
    }

}
