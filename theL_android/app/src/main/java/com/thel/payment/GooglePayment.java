package com.thel.payment;

import android.app.Activity;
import android.content.DialogInterface;
import android.text.TextUtils;

import com.facebook.appevents.AppEventsLogger;
import com.thel.R;
import com.thel.android.pay.IabHelper;
import com.thel.android.pay.IabResult;
import com.thel.android.pay.Inventory;
import com.thel.android.pay.Purchase;
import com.thel.android.pay.SkuDetails;
import com.thel.android.pay.SyncOrdersService;
import com.thel.app.TheLApp;
import com.thel.bean.GoogleIapNotifyResultBean;
import com.thel.bean.PayOrderBean;
import com.thel.constants.TheLConstants;
import com.thel.db.MomentsDataBaseAdapter;
import com.thel.growingio.GIoPayTrackBean;
import com.thel.growingio.GrowingIoConstant;
import com.thel.modules.live.bean.SoftGold;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.utils.DialogUtil;
import com.thel.utils.GooglePayUtils;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.JsonUtils;
import com.thel.utils.ShareFileUtils;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class GooglePayment implements PaymentStrategy {

    private IabHelper mHelper;

    private boolean iabSetup = false;

    private String price_currency_code = "";//货币参数

    private List<SoftGold> goldList;

    public GooglePayment(List<String> skuIds, List<SoftGold> goldList) {
        this.goldList = goldList;
        setupIap(skuIds);
    }

    private Activity activity;

    @Override public void pay(Activity activity, String goldId, OnPaymentListener mOnPaymentListener) {

        this.activity = activity;

        RequestBusiness.getInstance().createStickerPackOrder(TheLConstants.PRODUCT_TYPE_SOFT_GOLD, goldId, GooglePayUtils.GOOGLEPAY, GooglePayUtils.GOOGLEPAY_SOURCE).observeOn(AndroidSchedulers.mainThread()).onBackpressureDrop().subscribeOn(Schedulers.io()).subscribe(new PayConsumer());

    }

    @Override public void destroy() {
        if (mHelper != null) {
            mHelper.dispose();
        }
        mHelper = null;
    }

    public IabHelper getIabHelper() {
        return mHelper;
    }

    private class PayConsumer extends InterceptorSubscribe<PayOrderBean> {

        @Override
        public void onNext(PayOrderBean result) {
            super.onNext(result);

            MobclickAgent.onEvent(activity, "create_order_succeed");

            //购买软妹豆

            if (iabSetup && mHelper != null) {
                final String sku = result.data.iapId;
                buyItem(sku, result.data.outTradeNo, result.data.totalFee);
                final GIoPayTrackBean payTrackBean = new GIoPayTrackBean(GrowingIoConstant.RMDPURCHASE, result.data.outTradeNo, result.data.totalFee + "", GooglePayUtils.GOOGLEPAY);
                GrowingIOUtil.payTrack(payTrackBean, 0);

                //购买软妹豆金额

                final GIoPayTrackBean payBean = new GIoPayTrackBean(GrowingIoConstant.RMD_PURCHASE_AMOUNT, result.data.outTradeNo, GooglePayUtils.GOOGLEPAY);
                GrowingIOUtil.payTrack(payBean, 10);
            }
        }
    }

    private void buyItem(final String sku, final String outTradeNo, final double totalFee) {

        mHelper.handleActivityResult(TheLConstants.REQUEST_CODE_ANDROID_PAY, -1, null);

        mHelper.launchPurchaseFlow(activity, sku, TheLConstants.REQUEST_CODE_ANDROID_PAY, new IabHelper.OnIabPurchaseFinishedListener() {
            @Override
            public void onIabPurchaseFinished(IabResult result, Purchase info) {
                if (result.isSuccess()) {
                    onPaid(info);
                    AppEventsLogger logger = AppEventsLogger.newLogger(activity);
                    if (logger != null && !TextUtils.isEmpty(price_currency_code)) {//facebook记录购买成功
                        logger.logPurchase(BigDecimal.valueOf(totalFee), Currency.getInstance(price_currency_code));
                    }
                } else {
                    if (IabHelper.IABHELPER_USER_CANCELLED == result.getResponse()) {
                        DialogUtil.showToastShort(activity, TheLApp.context.getResources().getString(R.string.pay_canceled));
                    } else if (IabHelper.BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED == result.getResponse()) { // 已购买过该商品，但是没有消耗掉，此时需要查询本地是否有未同步的订单
                        List<String> skus = new ArrayList<>();
                        skus.add(sku);
                        mHelper.queryInventoryAsync(true, skus, new IabHelper.QueryInventoryFinishedListener() {
                            @Override
                            public void onQueryInventoryFinished(IabResult result, Inventory inv) {
                                if (result.isSuccess())
                                    mHelper.consumeAsync(inv.getPurchase(sku), new IabHelper.OnConsumeFinishedListener() {
                                        @Override
                                        public void onConsumeFinished(Purchase purchase, IabResult result) {
                                            if (result.isSuccess()) {
                                                buyItem(sku, outTradeNo, totalFee);
                                            } else {
                                                DialogUtil.showToastShort(activity, TheLApp.context.getResources().getString(R.string.google_play_connect_error));
                                            }
                                        }
                                    });
                                else {
                                    DialogUtil.showToastShort(activity, TheLApp.context.getResources().getString(R.string.google_play_connect_error) + "(" + result.getResponse() + ")");
                                }
                            }
                        });
                    } else {
                        DialogUtil.showToastShort(activity, TheLApp.context.getResources().getString(R.string.pay_failed) + "(" + result.getResponse() + ")");
                    }
                }
            }
        }, outTradeNo);
    }

    private void setupIap(final List<String> skuIds) {
        if (mHelper == null)
            mHelper = new IabHelper(TheLApp.getContext(), GooglePayUtils.KEY);
        if (!iabSetup) {
            mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
                @Override
                public void onIabSetupFinished(IabResult result) {
                    iabSetup = result.isSuccess();
                    if (iabSetup && mHelper != null) {
                        queryItems(skuIds);
                    } else {
                        loadFailed(result.getResponse() + "");
                        DialogUtil.showToastShort(activity, TheLApp.context.getResources().getString(R.string.google_play_connect_error));
                    }
                }
            });
        } else {
            queryItems(skuIds);
        }
    }

    private void queryItems(final List<String> skuIds) {
        mHelper.queryInventoryAsync(true, skuIds, new IabHelper.QueryInventoryFinishedListener() {
            @Override
            public void onQueryInventoryFinished(IabResult result, Inventory inv) {

                if (result.isSuccess()) {
                    for (SoftGold softGold : goldList) {
                        SkuDetails skuDetails = inv.getSkuDetails(softGold.iapId);
                        if (skuDetails != null) {
                            softGold.isPendingPrice = false;
                            softGold.googlePrice = skuDetails.getPrice();
                            try {
                                if (TextUtils.isEmpty(price_currency_code)) {//获取货币参数
                                    price_currency_code = JsonUtils.getString(new JSONObject(skuDetails.getJson()), "price_currency_code", "");
                                }
                                // google返回的price_amount_micros是*1000000，我们在这里只/10000，因为后面会再/100（为了跟国内版逻辑统一）
                                softGold.price = JsonUtils.getLong(new JSONObject(skuDetails.getJson()), "price_amount_micros", 0) / 10000d;
                            } catch (JSONException e) {
                                loadFailed("");
                                return;
                            }
                        }
                    }
                } else {
                    loadFailed(result.getResponse() + "");
                }
            }
        });
    }

    private void onPaid(Purchase purchase) {
        MomentsDataBaseAdapter.getInstance(TheLApp.getContext(), ShareFileUtils.getString(ShareFileUtils.ID, "")).saveInAppOrder(purchase);
        RequestBusiness.getInstance().iapNotify(purchase.getDeveloperPayload(), purchase.getOriginalJson(), purchase.getSignature()).observeOn(AndroidSchedulers.mainThread()).onBackpressureDrop().subscribeOn(Schedulers.io()).subscribe(new IapNotifyConsumer(purchase.getDeveloperPayload()));
    }

    private class IapNotifyConsumer extends InterceptorSubscribe<GoogleIapNotifyResultBean> {

        private String outTradeNo;

        public IapNotifyConsumer(String outTradeNo) {
            this.outTradeNo = outTradeNo;
        }

        @Override
        public void onNext(GoogleIapNotifyResultBean googleIapNotifyResultBean) {
            super.onNext(googleIapNotifyResultBean);
            if (GooglePayUtils.getInstance().isNotified(googleIapNotifyResultBean, true)) {
                final Purchase purchase = MomentsDataBaseAdapter.getInstance(TheLApp.getContext(), ShareFileUtils.getString(ShareFileUtils.ID, "")).getInAppOrder(outTradeNo);
                mHelper.consumeAsync(purchase, new IabHelper.OnConsumeFinishedListener() {
                    @Override
                    public void onConsumeFinished(Purchase purchase, IabResult result) {
                        bugGoldSucceed();
                    }
                });
                MomentsDataBaseAdapter.getInstance(TheLApp.getContext(), ShareFileUtils.getString(ShareFileUtils.ID, "")).deleteInAppOrder(outTradeNo);
            } else {
                notifyFailed(outTradeNo);
            }

        }
    }

    private void notifyFailed(final String outTradeNo) {
        DialogUtil.showConfirmDialog(activity, "", TheLApp.getContext().getResources().getString(R.string.synchronize_order_failed), TheLApp.getContext().getResources().getString(R.string.retry), TheLApp.getContext().getResources().getString(R.string.info_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Purchase pur = MomentsDataBaseAdapter.getInstance(TheLApp.getContext(), ShareFileUtils.getString(ShareFileUtils.ID, "")).getInAppOrder(outTradeNo);
                if (pur != null) {
                    RequestBusiness.getInstance().iapNotify(pur.getDeveloperPayload(), pur.getOriginalJson(), pur.getSignature()).onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new IapNotifyConsumer(pur.getDeveloperPayload()));
                } else {
                    SyncOrdersService.startSyncService(activity);
                }
            }
        }, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                SyncOrdersService.startSyncService(activity);
            }
        });
    }

    public void bugGoldSucceed() {

    }

    public void loadFailed(String result) {

    }

}
