package com.thel.payment;

public interface OnPaymentListener {

    /**
     * 购买热拉豆成功
     */
    void onBayGoldSucceed();

    /**
     * 购买热拉豆失败
     */
    void onBayGoldFail();

    /**
     * 失败原因
     * */
    void onPayResult(String result);

}
