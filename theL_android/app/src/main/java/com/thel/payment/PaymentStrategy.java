package com.thel.payment;

import android.app.Activity;

public interface PaymentStrategy {

    void pay(Activity activity, String goldId, OnPaymentListener mOnPaymentListener);

    void destroy();

}
