package com.thel.network.api.loginapi.api;

import com.thel.base.BaseDataBean;
import com.thel.bean.BasicInfoBean;
import com.thel.bean.user.BlackListBean;
import com.thel.network.RequestConstants;
import com.thel.network.api.loginapi.bean.CheckNumberBean;
import com.thel.network.api.loginapi.bean.CodeBean;
import com.thel.network.api.loginapi.bean.SignInBean;

import java.util.Map;

import io.reactivex.Flowable;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by waiarl on 2017/9/23.
 */

public interface LoginApi {

    /**
     * 登录/注册
     *
     * @param map
     * @return
     */
    @POST("v1/auth/signin")
    @FormUrlEncoded
    Flowable<SignInBean> signIn(@FieldMap Map<String, String> map);

    /**
     * 登录/注册
     *
     * @param body
     * @return
     */
    @POST("v1/auth/signin")
    Flowable<SignInBean> signIn(@Body RequestBody body);

    /**
     * 获取一些基本的必要信息接口，如'我'是否是会员、是否有续播的直播
     *
     * @return
     */
    @GET("v1/live/initparams")
    Flowable<BasicInfoBean> getBasicInfo();

    /**
     * 获取自己是否有主播权限
     *
     * @return
     */
    @GET("v1/live/perm")
    @FormUrlEncoded
    Flowable<BaseDataBean> getLivePermit(@Field("userId") String userId);

    /**
     * 上传设备id,主要是个推的id
     *
     * @param type    参数 为 android
     * @param getuiId 个推id
     * @return
     */
    @POST("v2/push/device/upload")
    @FormUrlEncoded
    Flowable<BasicInfoBean> uploadDeviceToken(@Field("type") String type, @Field("token") String getuiId);

    /**
     * 显示欢迎消息
     *
     * @return
     */
    @GET("v1/msg/request-welcome")
    Flowable<BaseDataBean> showWelcomeMsg();

    /**
     * 通过服务器获取短信验证码，目前仅支持国内区域86
     *
     * @param cell
     * @param zone
     * @return
     */
    @POST("v1/sms/sendcode")
    @FormUrlEncoded
    Flowable<CodeBean> sendCode(@Field("cell") String cell, @Field("zone") String zone);

    /**
     * 通过服务器获取短信验证码，目前仅支持国内区域86
     *
     * @param map
     * @return
     */
    @POST("v1/sms/sendcode")
    @FormUrlEncoded
    Flowable<CodeBean> sendCode(@FieldMap Map<String,String> map);

    /**
     * 检查电话号码是否被注册过了
     *
     * @return
     */
    @GET("v1/auth/check-cell")
    Flowable<CheckNumberBean> checkPhoneNumber(@QueryMap Map<String, String> map);

    /**
     * 获取黑名单
     * 包括 我屏蔽的，屏蔽我的，我屏蔽的日志的的人的名单
     *
     * @return
     */
    @GET("v2/users/blacklist")
    Flowable<BlackListBean> getBlackList();

    /**
     * 手机号，微信，fb绑定
     *
     * @param map
     * @return
     */
    @POST("v1/auth/rebind")
    @FormUrlEncoded
    Flowable<BaseDataBean> rebind(@FieldMap Map<String, String> map);

    /**
     * 检查用户名是否重复
     *
     * @param userName
     * @return RequestConstants.CHECK_USERNAME
     */
    @GET("v1/auth/check-username")
    Flowable<BaseDataBean> checkUserName(@Field(RequestConstants.I_USER_NAME) String userName);

    /**
     * 找回密码
     *
     * @param email
     * @return RequestConstants.FIND_BACK_PASSWORD
     */
    @GET("interface/stay/appSystem!getBackPassword.do")
    Flowable<BaseDataBean> findBackPassword(@Query(RequestConstants.I_EMAIL) String email);
}
