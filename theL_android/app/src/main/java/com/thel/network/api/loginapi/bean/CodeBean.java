package com.thel.network.api.loginapi.bean;

import com.thel.base.BaseDataBean;

/**
 * Created by chad
 * Time 17/9/25
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */

public class CodeBean extends BaseDataBean {

    @Override
    public String toString() {
        return "CodeBean{" +
                "data=" + data +
                '}';
    }

    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {
        @Override
        public String toString() {
            return "Data{" +
                    "smsRequestId='" + smsRequestId + '\'' +
                    '}';
        }

        private String smsRequestId;

        public String getSmsRequestId() {
            return smsRequestId;
        }

        public void setSmsRequestId(String smsRequestId) {
            this.smsRequestId = smsRequestId;
        }
    }
}
