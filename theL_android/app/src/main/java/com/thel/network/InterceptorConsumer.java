package com.thel.network;

import com.thel.base.BaseDataBean;

import io.reactivex.functions.Consumer;

public class InterceptorConsumer<T> implements Consumer<T> {

    protected boolean hasErrorCode = false;

    @Override public void accept(T data) throws Exception {
        try {

            if (data instanceof BaseDataBean) {
                BaseDataBean baseDataBean = (BaseDataBean) data;
                hasErrorCode = ErrorCode.getInstance().handlerErrorDataCallBack(baseDataBean);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
