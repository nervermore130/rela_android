package com.thel.network.api

import com.thel.bean.BasicInfoBean
import com.thel.bean.live.LiveChatCreateResBean
import com.thel.network.RequestConstants
import io.reactivex.Flowable
import okhttp3.RequestBody
import retrofit2.http.*

interface ChatApi {


    /**
     * 创建直播聊天室
     *
     * @return
     */
    @FormUrlEncoded
    @POST(RequestConstants.ChatApiUrlConstants.CREATE_CHAT)
    fun createChat(@FieldMap paramsMap: Map<String, String>): Flowable<LiveChatCreateResBean>

    /**
     * 创建直播聊天室
     *
     * @return
     */
    @FormUrlEncoded
    @POST(RequestConstants.ChatApiUrlConstants.JOIN_CHAT)
    fun joinChat(@FieldMap paramsMap: Map<String, String>): Flowable<LiveChatCreateResBean>

    /**
     * 解散直播聊天室
     *
     * @return
     */
    @POST(RequestConstants.ChatApiUrlConstants.DESTROY_CHAT)
    @FormUrlEncoded
    fun destroyChat(@FieldMap paramsMap: Map<String, String>): Flowable<BasicInfoBean>

    /**
     * 离开直播聊天室
     *
     * @return
     */
    @POST(RequestConstants.ChatApiUrlConstants.LEAVE_CHAT)
    @FormUrlEncoded
    fun leaveChat(@FieldMap paramsMap: Map<String, String>): Flowable<BasicInfoBean>

    /**
     * 赠送礼物
     *
     * @return
     */
    @POST(RequestConstants.ChatApiUrlConstants.SEND_GIFT)
    @FormUrlEncoded
    fun sendGift(@FieldMap paramsMap: Map<String, String>): Flowable<BasicInfoBean>


}