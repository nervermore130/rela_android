package com.thel.network.api.loginapi.bean;

import com.thel.base.BaseDataBean;

/**
 * Created by chad
 * Time 17/9/26
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */

public class CheckNumberBean extends BaseDataBean {

    private Data data;

    @Override
    public String toString() {
        return "CheckNumberBean{" +
                "data=" + data +
                '}';
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {
        private boolean exists;

        public boolean isExists() {
            return exists;
        }

        public void setExists(boolean exists) {
            this.exists = exists;
        }

        @Override
        public String toString() {
            return "Data{" +
                    "exists=" + exists +
                    '}';
        }
    }
}
