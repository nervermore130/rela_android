package com.thel.network.api;

import com.thel.network.RequestConstants;

import java.util.Map;

import io.reactivex.Flowable;
import okhttp3.FormBody;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface TestReportApi {

    /**
     * 4.15.0上传匹配埋点分析用户行为
     */
    @POST(RequestConstants.MATCH_REPORTS_LOGS)
    Flowable<String> postMatchLogs(@Body FormBody formBody);

}
