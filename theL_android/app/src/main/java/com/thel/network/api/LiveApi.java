package com.thel.network.api;

import com.thel.base.BaseDataBean;
import com.thel.bean.LiveAdInfoBean;
import com.thel.bean.LivePermitBean;
import com.thel.bean.live.LiveRecommendNetBean;
import com.thel.bean.live.LiveTypeNetBean;
import com.thel.bean.user.UserCardV2Bean;
import com.thel.modules.live.bean.ARGiftNetBean;
import com.thel.modules.live.bean.LiveTopFansTodayBean;
import com.thel.modules.live.bean.LiveGiftsRankingNetBean;
import com.thel.modules.live.bean.LivePkFriendListNetBean;
import com.thel.modules.live.bean.LivePopularityNetBean;
import com.thel.modules.live.bean.LiveRoomNetBean;
import com.thel.modules.live.bean.LiveRoomsNetBean;
import com.thel.modules.live.bean.LiveUserCardNetBean;
import com.thel.modules.live.bean.SoftEnjoyNetBean;
import com.thel.modules.live.bean.SoftGiftNetBean;
import com.thel.network.RequestConstants;

import java.util.Map;

import io.reactivex.Flowable;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * @author liuyun
 * @date 2017/10/10
 */

public interface LiveApi {

    /**
     * 获取直播列表
     */
    @GET(RequestConstants.GET_LIVE_ROOM_USER_CARD)
    Flowable<UserCardV2Bean> getLiveRoomUserCard(@Query(RequestConstants.I_USERID) String id);

    @GET(RequestConstants.GET_LIVE_PERMIT)
    Flowable<LivePermitBean> getLivePermit(@Query(RequestConstants.I_USERID) String userId);

    @POST(RequestConstants.CREATE_LIVE)
    @FormUrlEncoded
    Flowable<LiveRoomNetBean> createLiveRoom(@FieldMap Map<String, String> data);


    /**
     * 获取直播礼物列表
     *
     * @return
     */
    @GET("v1/live/gift/list")
    Flowable<SoftEnjoyNetBean> getLiveGiftList(@Header("etag") String header, @Query("appId") String appId);

    /**
     * 获取直播礼物列表
     *
     * @return
     */
    @GET("v1/live/gift/arPreReleaselist")
    Flowable<ARGiftNetBean> getARGiftList(@Query("appId") String appId);

    /**
     * 获取直播间用户名片
     *
     * @param userId
     * @return
     */
    @GET("v2/users/card")
    Flowable<LiveUserCardNetBean> getLiveUserCardBean(@Query(RequestConstants.I_USERID) String userId);

    /**
     * 获取直播列表
     *
     * @param cursor 请求cursor，第一页是1，没有时候为0
     * @param sort   类型 hot
     *               GET_LIVE_ROOMS
     * @return
     */
    @GET("v1/live/list")
    Flowable<LiveRoomsNetBean> getLiveRoomsListBean(@Query(RequestConstants.I_CURSOR) String cursor, @Query(RequestConstants.I_SORT) String sort);

    /**
     * 获取直播详情
     *
     * @param liveId
     * @param userId GET_LIVE_ROOM_DETAIL
     * @return
     */
    @GET("v1/live/detail-info")
    Flowable<LiveRoomNetBean> getLiveRoomDetail(@Query(RequestConstants.I_ID) String liveId, @Query(RequestConstants.I_USERID) String userId);

    /**
     * 获取直播礼物排行榜
     *
     * @param userId
     * @param tag    今天，本周，全部
     * @return GET_LIVE_GIFT_RANKING_LIST
     */
    @GET("v1/live/topfansv2")
    Flowable<LiveGiftsRankingNetBean> getLiveGiftRankingList(@Query(RequestConstants.I_USERID) String userId, @Query(RequestConstants.I_TYPE) String tag);

    /**
     * 获取直播排行榜
     **/
    @GET("v1/live/rank")
    Flowable<LivePopularityNetBean> getLiveRank();

    /**
     * 获取直播类型
     */
    @GET(RequestConstants.GET_LIVE_TYPE)
    Flowable<LiveTypeNetBean> getLiveTypeList();

    /**
     * 获取附近直播
     */
    @GET(RequestConstants.GET_NEARBY_LIVE)
    Flowable<LiveRoomsNetBean> getNeighbours(@Query(RequestConstants.I_CURSOR) String cursor, @Query(RequestConstants.I_LIMIT) String limit);

    /**
     * 推荐关注主播类型
     */
    @GET(RequestConstants.GET_FOLLOW_RECOMMEND_TYPE)
    Flowable<LiveRecommendNetBean> getRecommendFollowLive(@Query(RequestConstants.I_CURSOR) String cursor, @Query(RequestConstants.I_LIMIT) String limit, @Query(RequestConstants.I_LIVE_TYPE_ID) String typeId);

    /**
     * 推荐关注新人列表
     */
    @GET(RequestConstants.GET_RECOMMEND_ANCHORS_LIST)
    Flowable<LiveRecommendNetBean> getRecommendNewAnchors(@Query(RequestConstants.I_CURSOR) String cursor, @Query(RequestConstants.I_LIMIT) String limit);

    /**
     * 按类型获取直播
     */
    @GET(RequestConstants.GET_TYPE_LIVE_LIST)
    Flowable<LiveRoomsNetBean> getTypeLive(@Query(RequestConstants.I_LIVE_TYPE_ID) String typeId, @Query(RequestConstants.I_CURSOR) String cursor, @Query(RequestConstants.I_LIMIT) String limit);

    /**
     * 获取正在直播的新人列表
     */
    @GET(RequestConstants.GET_NEW_LIVE_LIST)
    Flowable<LiveRoomsNetBean> getNewsLiveList(@Query(RequestConstants.I_CURSOR) String cursor, @Query(RequestConstants.I_LIMIT) String limit);

    /**
     * 获取直播间PK好友列表
     *
     * @return
     */
    @GET(RequestConstants.GET_LIVE_PK_FRIEND_LIST)
    Flowable<LivePkFriendListNetBean> getLivePkFriendListNetBean();

    @POST
    Flowable<LiveAdInfoBean> getLiveAdInfo(@Url String url);

    /**
     * 直播Pk errorlog 上报
     *
     * @param map
     * @return
     */
    @POST(RequestConstants.POST_LIVE_PK_LOG_UPLOAD)
    @FormUrlEncoded
    Flowable<BaseDataBean> uploadPkErrorLog(@FieldMap Map<String, String> map);

    /**
     * 单个礼物详情
     *
     * @param id
     * @return
     */
    @GET(RequestConstants.GET_LIVE_GIFT_DETAIL)
    Flowable<SoftGiftNetBean> getSingleLiveGiftList(@Query(RequestConstants.I_ID) int id);

    /**
     * 获取前三名粉丝信息
     */
    @GET(RequestConstants.GET_TOP_FANS_TODAY)
    Flowable<LiveTopFansTodayBean> getTopFans(@Query(RequestConstants.I_USERID) String userId);
}
