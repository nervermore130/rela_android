package com.thel.network;

import com.thel.app.TheLApp;
import com.thel.utils.NetworkUtils;

import org.reactivestreams.Subscription;

/**
 * Created by chad
 * Time 18/3/13
 * Email: wuxianchuang@foxmail.com
 * Description: TODO  登录注册流程不判断是否登录，不拦截未登录时的网络请求
 */

public class LoginInterceptorSubscribe<T> extends InterceptorSubscribe<T> {
    @Override
    public void onSubscribe(Subscription s) {

        if (NetworkUtils.isNetworkConnected(TheLApp.context)) {
            s.request(Integer.MAX_VALUE);
        }
    }
}
