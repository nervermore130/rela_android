package com.thel.network.api;

import com.thel.R;
import com.thel.base.BaseDataBean;
import com.thel.bean.AdBean;
import com.thel.bean.AwaitLiveBean;
import com.thel.bean.BasicInfoBean;
import com.thel.bean.BasicInfoNetBean;
import com.thel.bean.BlackListBean;
import com.thel.bean.CheckUserNameBean;
import com.thel.bean.ContactsListBean;
import com.thel.bean.FavoritesBean;
import com.thel.bean.FirstHotTopicBean;
import com.thel.bean.FriendListBean;
import com.thel.bean.FriendRecommendDataBean;
import com.thel.bean.GoogleIapNotifyResultBean;
import com.thel.bean.HiddenLoveBean;
import com.thel.bean.InitRecommendUserListBean;
import com.thel.bean.LikeListBean;
import com.thel.bean.LikeResultBean;
import com.thel.bean.LiveClassificationBean;
import com.thel.bean.LiveNoticeCheckBean;
import com.thel.bean.MomentsDataBean;
import com.thel.bean.MusicListBean;
import com.thel.bean.PayOrderBean;
import com.thel.bean.RecentAndHotTopicsBean;
import com.thel.bean.RechargeRecordListBeanNew;
import com.thel.bean.RecommendListBean;
import com.thel.bean.ReleaseMomentSucceedBean;
import com.thel.bean.ReleaseMomentSucceedBean2;
import com.thel.bean.ReleasedCommentBeanNew;
import com.thel.bean.ReleasedCommentReplyBeanNew;
import com.thel.bean.ResultBean;
import com.thel.bean.SearchUsersListBean;
import com.thel.bean.StickerPackListNetBean;
import com.thel.bean.StickerPackNetBean;
import com.thel.bean.SuperlikeBean;
import com.thel.bean.SuperlikeListBean;
import com.thel.bean.ThemeCommentListBeanNew;
import com.thel.bean.TopicListBean;
import com.thel.bean.TrendingTagsBean;
import com.thel.bean.UploadVideoAlbumBean;
import com.thel.bean.VersionBean;
import com.thel.bean.WhoSeenMeListBean;
import com.thel.bean.comment.CommentListBeanNew;
import com.thel.bean.comment.CommentReplyListBeanNew;
import com.thel.bean.gift.WinkCommentListBean;
import com.thel.bean.gift.WinkListBean;
import com.thel.bean.live.LiveFollowingUsersBeanNew;
import com.thel.bean.me.MyBlockBean;
import com.thel.bean.me.MyCircleFriendListBean;
import com.thel.bean.me.MyCircleRequestListBean;
import com.thel.bean.me.MyWinkListBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.moments.MomentsBeanNew;
import com.thel.bean.moments.MomentsListBean;
import com.thel.bean.moments.MomentsMsgsListBean;
import com.thel.bean.recommend.RecommendedStickerListNetBean;
import com.thel.bean.theme.ThemeListBean;
import com.thel.bean.topic.TopicFollowerListBean;
import com.thel.bean.topic.TopicMainPageBean;
import com.thel.bean.user.BlackMomentListBean;
import com.thel.bean.user.BlockListBean;
import com.thel.bean.user.BlockListBeanNew;
import com.thel.bean.user.LocationNameBean;
import com.thel.bean.user.MatchQuestionBean;
import com.thel.bean.user.MyImagesListBean;
import com.thel.bean.user.NearUserBean;
import com.thel.bean.user.NearUserListNetBean;
import com.thel.bean.user.UploadTokenBean;
import com.thel.bean.user.UserInfoBean;
import com.thel.bean.video.VideoListBeanNew;
import com.thel.modules.live.GiftFirstChargeBean;
import com.thel.modules.live.bean.SoftMoneyListBean;
import com.thel.modules.live.bean.ZmxyAuthBean;
import com.thel.modules.live.bean.ZmxyCheckCallBackBean;
import com.thel.modules.main.home.search.SearchRecommendBean;
import com.thel.modules.main.me.bean.FriendsListBean;
import com.thel.modules.main.me.bean.FriendsListBeanNew;
import com.thel.modules.main.me.bean.IncomeRecordListBean;
import com.thel.modules.main.me.bean.MatchListBean;
import com.thel.modules.main.me.bean.MatchSettingsBean;
import com.thel.modules.main.me.bean.MyInfoNetBean;
import com.thel.modules.main.me.bean.UpdataInfoBean;
import com.thel.modules.main.me.bean.UserLevelBean;
import com.thel.modules.main.me.bean.UserLevelSwitchBean;
import com.thel.modules.main.me.bean.VipListBean;
import com.thel.modules.main.me.bean.WalletBean;
import com.thel.modules.main.settings.bean.PushSwitchTypeBean;
import com.thel.modules.main.userinfo.bean.FollowersListBean;
import com.thel.modules.main.userinfo.bean.FollowersListNetBean;
import com.thel.network.RequestConstants;

import java.util.Map;

import io.reactivex.Flowable;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by liuyun on 2017/9/25.
 */

public interface AllApi {

    /**
     * 获取一些基本的必要信息接口，如'我'是否是会员、是否有续播的直播
     *
     * @return
     */
    @GET(RequestConstants.GET_BASIC_INFO)
    Flowable<BasicInfoNetBean> getBasicInfo();

    /**
     * 获取自己是否有主播权限
     *
     * @param userId
     * @return
     */
    @GET(RequestConstants.GET_LIVE_PERMIT)
    Flowable<BaseDataBean> getLivePermit(@Query("userId") String userId);

    /**
     * 上传设备id，主要是个推的id
     *
     * @param type
     * @param token
     * @return
     */
    @FormUrlEncoded
    @POST(RequestConstants.UPLOAD_DEVICE_TOKEN)
    Flowable<BasicInfoBean> uploadDeviceToken(@Field("type") String type, @Field("token") String token);

    /**
     * 显示欢迎消息
     *
     * @return
     */
    @GET(RequestConstants.SHOW_WELCOME_MSG)
    Flowable<BaseDataBean> showWelcomeMsg();

    /**
     * 通过服务器获取短信验证码，目前仅支持国内区域86
     *
     * @param cell
     * @param zone
     * @return
     */
    @GET(RequestConstants.SEND_CODE)
    Flowable<BaseDataBean> sendCode(@Query("cell") String cell, @Query("zone") String zone);


    /**
     * 获取上传视频文件到七牛的token
     *
     * @return
     */
    @FormUrlEncoded
    @POST(RequestConstants.GET_UPLOAD_TOKEN)
    Flowable<UploadTokenBean> getVideoUploadToken(@FieldMap Map<String, String> map);

    /**
     * 检查电话号码是否被注册过了
     *
     * @param cell
     * @param zone
     * @return
     */
    @GET(RequestConstants.CHECK_PHONE_NUMBER)
    Flowable<BaseDataBean> checkPhoneNumber(@Query("cell") String cell, @Query("zone") String zone);

    /**
     * 获取黑名单
     *
     * @return
     */
    @GET(RequestConstants.BLACK_LIST)
    Flowable<BlackListBean> blackList();

    /**
     * 手机号、微信、fb绑定
     *
     * @param map
     * @return
     */
    @GET(RequestConstants.REBIND)
    Flowable<BaseDataBean> rebind(@QueryMap Map<String, String> map);

    /**
     * 检查用户名是否重复
     *
     * @param userName
     * @return
     */
    @GET(RequestConstants.CHECK_USERNAME)
    Flowable<CheckUserNameBean> checkUserName(@Query("userName") String userName);

    /**
     * 找回密码
     *
     * @param email
     * @return
     */
    @GET(RequestConstants.FIND_BACK_PASSWORD)
    Flowable<BaseDataBean> findBackPassword(@Query("email") String email);

    /**
     * 用户推出
     *
     * @return
     */
    @POST(RequestConstants.USER_LOGOUT)
    @FormUrlEncoded
    Flowable<BaseDataBean> loadUserLogout(@FieldMap Map<String, String> map);

    /**
     * 获取附近用户列表(已登录)参数
     *
     * @param map
     * @return
     */
    @GET(RequestConstants.NEAR_BY_SIMPLE_LIST)
    Flowable<NearUserBean> getNearbySimpleList(@QueryMap Map<String, String> map);

    /**
     * 获取附近用户列表(已登录)参数
     *
     * @param map
     * @return
     */
    @GET(RequestConstants.GET_ACTIVE_USERS)
    Flowable<NearUserListNetBean> getActiveUsers(@QueryMap Map<String, String> map);

    /**
     * 世界功能
     *
     * @param map
     * @return
     */
    @GET(RequestConstants.WORLD_LIST)
    Flowable<NearUserListNetBean> getWorldUsers(@QueryMap Map<String, String> map);

    /**
     * 读取其他用户个人信息
     *
     * @param userId
     * @return
     */
    @GET(RequestConstants.USER_INFO)
    Flowable<UserInfoBean> getUserInfo(@Query("userId") String userId);

    /**
     * 读取我的信息
     *
     * @return
     */
    @GET(RequestConstants.MY_INFO)
    Flowable<MyInfoNetBean> getMyInfo();

    /**
     * 更新个人信息
     *
     * @param body
     * @return
     */
    @POST(RequestConstants.UPDATE_USER_INFO)
    Flowable<BaseDataBean> updateUserInfo(@Body RequestBody body);

    /**
     * 更新个人信息
     *
     * @param map
     * @return
     */
    @POST(RequestConstants.UPDATE_USER_INFO)
    @FormUrlEncoded
    Flowable<UpdataInfoBean> updateUserInfo(@FieldMap Map<String, String> map);

    /**
     * 获取朋友列表
     *
     * @param userId
     * @param pageSize
     * @param curPage
     * @return
     */
    @GET(RequestConstants.GET__FRIENDS)
    Flowable<FriendsListBean> getFriendsList(@Query("userid") String userId, @Query("pageSize") String pageSize, @Query("curPage") String curPage);

    /**
     * 获取朋友列表(简易，只包含头像、名字、id)
     *
     * @return
     */
    @GET(RequestConstants.GET__CONTACTS)
    Flowable<ContactsListBean> getContactsList();

    /**
     * 获取关注列表
     *
     * @param userId
     * @param pageSize
     * @param curPage
     * @return
     */
    @GET(RequestConstants.GET__FOLLOW)
    Flowable<FriendsListBean> getFollowList(@Query("userid") String userId, @Query("pageSize") String pageSize, @Query("curPage") String curPage);

    /**
     * 获取关注列表
     *
     * @param userId
     * @param pageSize
     * @param curPage
     * @return
     */
    @GET(RequestConstants.GET__FOLLOW)
    Flowable<FollowersListNetBean> getUserFollowList(@Query("userid") String userId, @Query("pageSize") String pageSize, @Query("curPage") String curPage);

    /**
     * 获取关注的主播列表
     */
    @GET(RequestConstants.GET_FOLLOW_ANCHORS)
    Flowable<FriendsListBean> getFollowAnchors(@Query("cursor") String cursor);

    /**
     * 获取粉丝列表
     *
     * @param userId
     * @param pageSize
     * @param curPage
     * @return
     */
    @GET(RequestConstants.GET__FANS)
    Flowable<FriendsListBean> getFansList(@Query("userid") String userId, @Query("pageSize") String pageSize, @Query("curPage") String curPage);

    /**
     * 获取悄悄查看列表
     *
     * @param userId
     * @param pageSize
     * @param curPage
     * @return
     */
    @GET(RequestConstants.GET_SECRET)
    Flowable<FriendsListBeanNew> getSecretFollowList(@Query("userid") String userId, @Query("pageSize") String pageSize, @Query("curPage") String curPage);

    /**
     * 获取期待开播列表
     *
     * @param userId
     * @param pageSize
     * @param curPage
     * @return
     */
    @GET(RequestConstants.GET_AWAIT)
    Flowable<AwaitLiveBean> getAwaitLiveList(@Query("userId") String userId, @Query("limit") String pageSize, @Query("cursor") String curPage);

    /**
     * 获取点赞列表
     *
     * @param pageSize
     * @param curPage
     * @return
     */
    @GET(RequestConstants.GET_LIKED)
    Flowable<LikeListBean> getLikedList(@Query("pageSize") String pageSize, @Query("curPage") String curPage);

    /**
     * 搜索我的朋友
     *
     * @return
     */
    @GET(RequestConstants.SEARCH_MY_FRIENDS)
    Flowable<FriendsListBean> searchMyFriends(@QueryMap Map<String, String> map);

    /**
     * 上传相册图片(最终url)
     *
     * @return
     */
    @POST(RequestConstants.UPLOAD_IMAGE)
    Flowable<BaseDataBean> uploadImageUrl(@Body RequestBody body);

    /**
     * 上传相册图片(最终url)
     *
     * @return
     */
    @POST(RequestConstants.UPLOAD_IMAGE)
    @FormUrlEncoded
    Flowable<UpdataInfoBean> uploadImageUrl(@FieldMap Map<String, String> map);

    /**
     * 上传视频
     *
     * @return
     */
    @FormUrlEncoded
    @POST(RequestConstants.UPLOAD_ALBUM_VIDEO)
    Flowable<UploadVideoAlbumBean> uploadAlbumVideoUrl(@FieldMap Map<String, String> map);

    /**
     * 获取我的照片列表
     *
     * @param curPage
     * @param pageSize
     * @return
     */
    @GET(RequestConstants.MY_IMAGES_LIST)
    Flowable<MyImagesListBean> getMyImagesList(@Query("curPage") String curPage, @Query("pageSize") String pageSize);

    /**
     * 获取匹配我的照片列表
     *
     * @param curPage
     * @param pageSize
     * @return
     */
    @GET(RequestConstants.MY_IMAGES_LIST)
    Flowable<MyImagesListBean> getMatchMyImagesList(@Query("entry") String entry, @Query("curPage") String curPage, @Query("pageSize") String pageSize);

    /**
     * 删除照片
     *
     * @return
     */
    @POST(RequestConstants.DELETE_IMAGE)
    Flowable<BaseDataBean> deleteImage(@Body RequestBody body);

    /**
     * 删除照片
     *
     * @return
     */
    @POST(RequestConstants.DELETE_IMAGE)
    @FormUrlEncoded
    Flowable<UpdataInfoBean> deleteImage(@FieldMap Map<String, String> map);

    /**
     * 设为隐私照片
     *
     * @return
     */
    @POST(RequestConstants.SET_PRIVATE_STATUS)
    Flowable<BaseDataBean> setPrivateStatus(@Body RequestBody body);

    /**
     * 设为隐私照片
     *
     * @return
     */
    @POST(RequestConstants.SET_PRIVATE_STATUS)
    @FormUrlEncoded
    Flowable<BaseDataBean> setPrivateStatus(@FieldMap Map<String, String> map);

    /**
     * 设为封面照片
     *
     * @return
     */
    @POST(RequestConstants.SET_COVER)
    Flowable<BaseDataBean> setCover(@Body RequestBody body);

    /**
     * 设为封面照片
     *
     * @return
     */
    @POST(RequestConstants.SET_COVER)
    @FormUrlEncoded
    Flowable<BaseDataBean> setCover(@FieldMap Map<String, String> map);

    /**
     * 搜索好友
     *
     * @param picId
     * @param curPage
     * @return
     */
    @GET(RequestConstants.SEARCH_NICKNAME)
    Flowable<SearchUsersListBean> searchNickname(@Query(RequestConstants.I_KEYWORD) String picId, @Query(RequestConstants.I_CURPAGE) String curPage);

    /**
     * 谁来看过我
     *
     * @param curPage
     * @param pageSize
     * @return
     */
    @GET(RequestConstants.CHEACK_VIEW)
    Flowable<WhoSeenMeListBean> checkView(@Query("curPage") String curPage, @Query("pageSize") String pageSize);

    /**
     * 我看过谁
     *
     * @param curPage
     * @param pageSize
     * @return
     */
    @GET(RequestConstants.I_VIEW)
    Flowable<WhoSeenMeListBean> iView(@Query("curPage") String curPage, @Query("pageSize") String pageSize);

    /**
     * 我的黑名单列表
     *
     * @return
     */
    @GET(RequestConstants.GET_USER_BLACK_LIST)
    Flowable<BlockListBean> getUserBlackList(@QueryMap Map<String, String> map);

    @GET(RequestConstants.GET_USER_BLACK_LIST)
    Flowable<BlockListBeanNew> getUserBlackList(@Query("curPage") String curPage, @Query("pageSize") String pageSize);

    /**
     * 解除黑名单
     *
     * @return
     */
    @POST(RequestConstants.REMOVE_FROM_BLACKLIST)
    Flowable<BaseDataBean> removeFromBlackList(@Body RequestBody body);

    /**
     * 解除黑名单
     *
     * @return
     */
    @POST(RequestConstants.REMOVE_FROM_BLACKLIST)
    @FormUrlEncoded
    Flowable<MyBlockBean> removeFromBlackList(@FieldMap Map<String, String> map);

    /**
     * @param userId
     * @return
     */
    @GET(RequestConstants.SEND_HIDDEN_LOVE)
    Flowable<HiddenLoveBean> sendHiddenLove(@Query("userId") String userId);


    /**
     * 传情
     *
     * @param userId
     * @param type
     * @return
     */
    @GET(RequestConstants.SEND_WINK_CREATE)
    Flowable<BaseDataBean> sendWink(@Query("userId") String userId, @Query("type") String type);

    /**
     * 关注
     *
     * @return
     */
    @POST(RequestConstants.FOLLOW_USER)
    Flowable<ResultBean> followUser(@Body RequestBody body);

    @POST(RequestConstants.FOLLOW_USER)
    @FormUrlEncoded
    Flowable<ResultBean> followUser(@FieldMap Map<String, String> map);

    @POST(RequestConstants.FOLLOW_USER_BATCH)
    Flowable<ResultBean> followUserBatch(@Body RequestBody body);

    @POST(RequestConstants.FOLLOW_USER_BATCH)
    @FormUrlEncoded
    Flowable<ResultBean> followUserBatch(@FieldMap Map<String, String> map);

    /**
     * 修改密码
     *
     * @return
     */
    @POST(RequestConstants.MODIFY_PASSWORD)
    Flowable<BaseDataBean> modifyPassword(@Body RequestBody body);

    /**
     * 修改密码
     *
     * @return
     */
    @POST(RequestConstants.MODIFY_PASSWORD)
    @FormUrlEncoded
    Flowable<BaseDataBean> modifyPassword(@FieldMap Map<String, String> map);

    /**
     * 获取用户的粉丝列表
     *
     * @param userId
     * @param pageSize
     * @param curPage
     * @return
     */
    @GET(RequestConstants.GET_USER_FOLLOWERS_LIST)
    Flowable<FollowersListBean> getUserFollowersList(@Query("userId") String userId, @Query("pageSize") String pageSize, @Query("curPage") String curPage);

    /**
     * 我的黑名单列表
     *
     * @param userId
     * @param pageSize
     * @param curPage
     * @return
     */
    @GET(RequestConstants.GET_USER_WINK_LIST)
    Flowable<WinkListBean> getUserWinkList(@Query("userId") String userId, @Query("cursor") String curPage, @Query("pageSize") String pageSize);

    /**
     * @param pageSize
     * @param curPage
     * @return
     */
    @GET(RequestConstants.GET_MY_WINK_LIST)
    Flowable<MyWinkListBean> getMyWinkList(@Query("pageSize") String pageSize, @Query("curPage") String curPage);

    /**
     * 消息的push 由客户端发出
     *
     * @param message
     * @param nickName
     * @param userId
     * @param token
     * @return
     */
    @GET(RequestConstants.APP_PUSH)
    Flowable<BaseDataBean> messagePushByClient(@Query("message") String message, @Query("nickName") String nickName, @Query("userId") String userId, @Query("token") String token);

    /**
     * 获取我的密友请求
     *
     * @return
     */
    @GET(RequestConstants.GET_MY_CIRCLE_DATA)
    Flowable<MyCircleFriendListBean> getMyCircleData();

    /**
     * 获取我的密友数据
     *
     * @param pageSize
     * @param curPage
     * @return
     */
    @GET(RequestConstants.GET_MY_CIRCLE_REQUESTS)
    Flowable<MyCircleRequestListBean> getMyCircleRequestData(@Query("pageSize") String pageSize, @Query("curPage") String curPage);

    /**
     * 发送一条密友请求
     *
     * @return
     */
    @POST(RequestConstants.SEND_CIRCLE_REQUEST)
    @FormUrlEncoded
    Flowable<MyCircleRequestListBean> sendCircleRequest(@FieldMap Map<String, String> map);

    /**
     * 取消我发出的密友圈请求
     *
     * @return
     */
    @POST(RequestConstants.CANCEL_CIRCLE_REQUEST)
    @FormUrlEncoded
    Flowable<BaseDataBean> cancelRequest(@FieldMap Map<String, String> map);

    /**
     * 接受密友圈请求
     *
     * @return
     */
    @POST(RequestConstants.ACCEPT_REQUEST)
    @FormUrlEncoded
    Flowable<BaseDataBean> acceptRequest(@FieldMap Map<String, String> map);

    /**
     * 拒绝密友圈请求
     *
     * @return
     */
    @POST(RequestConstants.REFUSE_REQUEST)
    @FormUrlEncoded
    Flowable<BaseDataBean> refuseRequest(@FieldMap Map<String, String> map);

    /**
     * 移除密友圈朋友
     *
     * @return
     */
    @POST(RequestConstants.REMOVE_CIRCLE_FRIEND)
    Flowable<BaseDataBean> removeCircleFriend(@Body RequestBody body);

    /**
     * 移除密友圈朋友
     *
     * @return
     */
    @POST(RequestConstants.REMOVE_CIRCLE_FRIEND)
    @FormUrlEncoded
    Flowable<BaseDataBean> removeCircleFriend(@FieldMap Map<String, String> map);

    /**
     * 获取推荐的匹配用户列表
     *
     * @return
     */
    @GET(RequestConstants.MATCH_LIST)
    Flowable<MatchListBean> getMatchList();

    /**
     * 获取喜欢我的人的列表
     *
     * @return
     */
    @GET(RequestConstants.GET_LIKE_ME_LIST)
    Flowable<MatchListBean> getLikeMeMatchList(@QueryMap Map<String, String> map);

    /**
     * 获取推荐的匹配用户列表
     *
     * @return
     */
    @POST(RequestConstants.MATCH_ROLLBACK)
    @FormUrlEncoded
    Flowable<BaseDataBean> matchRollback(@FieldMap Map<String, String> map);

    /**
     * 获取我的匹配问题
     *
     * @return
     */
    @GET(RequestConstants.MY_QUESTIONS)
    Flowable<MatchQuestionBean> getMyQuestions();

    /**
     * setMyQuestions
     *
     * @return
     */
    @POST(RequestConstants.SET_QUESTIONS)
    @FormUrlEncoded
    Flowable<BaseDataBean> setMyQuestions(@FieldMap Map<String, String> map);

    /**
     * 超级喜欢
     *
     * @param map
     * @return
     */
    @POST(RequestConstants.SUPERLIKE)
    @FormUrlEncoded
    Flowable<SuperlikeBean> superlike(@FieldMap Map<String, String> map);

    /**
     * 获取超级喜欢购买列表
     *
     * @return
     */
    @GET(RequestConstants.SUPERLIKE_LIST)
    Flowable<SuperlikeListBean> getSuperlikeList();

    /**
     * 喜欢或者不喜欢对方，或取消喜欢对方
     *
     * @return
     */
    @POST(RequestConstants.LIKE_OR_NOT)
    @FormUrlEncoded
    Flowable<LikeResultBean> likeOrNot(@FieldMap Map<String, String> map);

    /**
     * 获取表情商店首页-更多表情包
     *
     * @param vipfree
     * @return
     */
    @GET(RequestConstants.STICKER_STORE_RECOMMENDED)
    Flowable<RecommendedStickerListNetBean> getStickerStoreRecommended(@Query("vipfree") String vipfree);

    /**
     * 表情包列表
     *
     * @return
     */
    @GET(RequestConstants.STICKER_STORE_MORE)
    Flowable<StickerPackListNetBean> getStickerStoreMore(@QueryMap Map<String, String> map);

    /**
     * 获取我已购买过的表情包
     *
     * @return
     */
    @GET(RequestConstants.PURCHAESED_STICKER_PACKS)
    Flowable<StickerPackListNetBean> getPurchasedStickerPacks();

    /**
     * 获取表情包详情
     *
     * @return
     */
    @GET(RequestConstants.STICKER_PACK_DETAIL)
    Flowable<StickerPackNetBean> getStickerPackDetail(@QueryMap Map<String, String> map);

    /**
     * 从服务端获取生成支付订单的必要数据
     *
     * @return
     */
    @POST(RequestConstants.CREATE_STICKER_PACK_ORDER)
    Flowable<PayOrderBean> createStickerPackOrder(@Body RequestBody body);

    /**
     * 从服务端获取生成支付订单的必要数据
     *
     * @return
     */
    @POST(RequestConstants.CREATE_STICKER_PACK_ORDER)
    @FormUrlEncoded
    Flowable<PayOrderBean> createStickerPackOrder(@FieldMap Map<String, String> map);


    /**
     * @param userId   用户id。这个是可选参数，如果有，则是查看某个用户（也可以看自己）的朋友圈信息列表。如果没有这个参数（ 或者这个参数传递一个空值），则查看“我能看到的所有的”朋友圈信息列表
     * @param mainType 接受 3 种值：空，moments，popular:“空”和“moments”代表：“关注的 日志列表”, 或者是“某个人发的 日志列表”;"popular"代表：热门
     * @param pageSize 每页显示多少条数据
     * @param curPage  当前页码
     * @return
     */
    @GET(RequestConstants.MOMENTS_LIST)
    Flowable<MomentsListBean> getMomentsList(@Query("userId") String userId, @Query("mainType") String mainType, @Query("pageSize") String pageSize, @Query("curPage") String curPage);

    /**
     * 获取最近5个参与的话题，加上 15个热门的话题
     */
    @GET(RequestConstants.MOMENTS_GET_RECENT_AND_HOT_TOPICS)
    Flowable<RecentAndHotTopicsBean> getRecentAndHotTopics();

    /**
     * 搜索话题列表
     */
    @GET(RequestConstants.MOMENTS_SEARCH_TOPICS)
    Flowable<TopicListBean> searchTopics(@Query(RequestConstants.I_TOPIC_NAME) String topicName, @Query(RequestConstants.I_CURPAGE) int curPage);

    /**
     * 用户昵称修改时间验证（30天内只能修改一次昵称）
     **/
    @GET(RequestConstants.GET_NICKNAME_TIMEVERIFY)
    Flowable<BaseDataBean> getNickNameTimeVerify(@Query("nickname") String nickname);

    /**
     * 获取最热门的标签
     */
    @GET(RequestConstants.GET_FIRST_HOT_TOPIC)
    Flowable<FirstHotTopicBean> getFirstHotTopic();

    @POST(RequestConstants.MOMENTS_RELEASE_MOMENT)
    @FormUrlEncoded
    Flowable<ReleaseMomentSucceedBean2> releaseMoment(@FieldMap Map<String, String> map);

    @POST(RequestConstants.GET_UPLOAD_TOKEN)
    @FormUrlEncoded
    Flowable<UploadTokenBean> getUploadToken(@FieldMap Map<String, String> map);

    /**
     * 我的钱包数据
     */
    @GET(RequestConstants.GET_WALLET_DATA)
    Flowable<WalletBean> getWalletData();

    /**
     * 获取热门话题列表
     *
     * @param page
     * @param limit
     * @return
     */
    @GET(RequestConstants.GET_HOTTHEME_LIST)
    Flowable<ThemeListBean> getHotThemeList(@Query(RequestConstants.I_CURSOR) String page, @Query(RequestConstants.I_LIMIT) String limit);

    /**
     * 获取最新话题列表
     *
     * @param page
     * @param limit
     * @return
     */
    @GET(RequestConstants.GET_NEWTHEMES_LIST)
    Flowable<ThemeListBean> getNewThemeList(@Query(RequestConstants.I_CURSOR) String page, @Query(RequestConstants.I_LIMIT) String limit);

    /**
     * 获取话题列表
     *
     * @param page
     * @param limit
     * @param themeClass
     * @return
     */
    @GET(RequestConstants.GET_THEMES_LIST)
    Flowable<ThemeListBean> getThemesList(@Query(RequestConstants.I_CURSOR) String page, @Query(RequestConstants.I_LIMIT) String limit, @Query(RequestConstants.I_THEMECLASS) String themeClass);

    @GET(RequestConstants.GET_MAIN_ADVERT)
    Flowable<AdBean> getAd(@Query(RequestConstants.I_CLIENT) String client);

    /**
     * 收益或提现记录
     */
    @GET(RequestConstants.GET_INCOME_OR_WITHDRAW_RECORD)
    Flowable<IncomeRecordListBean> getIncomeOrWithdrawRecord(@QueryMap Map<String, String> map);

    /**
     * 获取朋友圈消息
     *
     * @param pageSize
     * @param curPage
     */
    @GET(RequestConstants.MOMENTS_GET_MSGS)
    Flowable<MomentsMsgsListBean> getMomentsMsgsList(@Query("pageSize") int pageSize, @Query("curPage") int curPage);

    //    /**
    //     * 获取moment的评论列表
    //     *
    //     * @param id     朋友圈消息id
    //     * @param limit  每页显示多少条数据
    //     * @param cursor 数据库游标
    //     * @return
    //     */
    //    @GET(RequestConstants.GET_COMMENT_LIST)
    //    Flowable<MomentCommentBean> getCommentList(@Query("id") String id, @Query("limit") String limit, @Query("cursor") String cursor);

    @GET(RequestConstants.GET_TRENDING_TAGS)
    Flowable<TrendingTagsBean> getTrendingTags();

    @POST(RequestConstants.INIT_USER_INFO)
    @FormUrlEncoded
    Flowable<UpdataInfoBean> initUserInfo(@FieldMap Map<String, String> map);

    /**
     * 获取购买vip页面的列表
     *
     * @param channel 默认为空
     * @return
     */
    @GET(RequestConstants.BUY_VIP_LIST)
    Flowable<VipListBean> getBuyVipList(@Query("channel") String channel);

    /**
     * 获取地理名
     */
    @GET(RequestConstants.GET_LOC_NAME)
    Flowable<LocationNameBean> getLocationName(@QueryMap Map<String, String> map);

    /**
     * google wallet 购买成功后提交服务端验证
     */
    @POST(RequestConstants.IAP_NOTIFY)
    Flowable<GoogleIapNotifyResultBean> iapNotify(@Body RequestBody body);

    /**
     * google wallet 购买成功后提交服务端验证
     */
    @POST(RequestConstants.IAP_NOTIFY)
    @FormUrlEncoded
    Flowable<GoogleIapNotifyResultBean> iapNotify(@FieldMap Map<String, String> map);

    /**
     * 获取点赞列表
     *
     * @param id
     * @param cursor
     * @param limit
     */
    @GET(RequestConstants.MOMENTS_GET_LIKES)
    Flowable<WinkCommentListBean> getWinkComments(@Query("id") String id, @Query("limit") String limit, @Query("cursor") String cursor);

    /**
     * Gold购买／赠送商品（会员）
     */
    @POST(RequestConstants.BUY_WITH_GOLD)
    Flowable<BaseDataBean> buyWithGold(@Body RequestBody body);

    /**
     * Gold购买／赠送商品（会员）
     */
    @POST(RequestConstants.BUY_WITH_GOLD)
    @FormUrlEncoded
    Flowable<BaseDataBean> buyWithGold(@FieldMap Map<String, String> map);

    /**
     * 话题主页面(热门日志)
     *
     * @param topicName
     * @param curPage
     * @param pageSize
     */
    @GET(RequestConstants.MOMENTS_TOPIC_MAINPAGE_HOT)
    Flowable<TopicMainPageBean> getTopicMainPageHot(@Query("topicName") String topicName, @Query("curPage") String curPage, @Query("pageSize") String pageSize);

    /**
     * 获取moment详情，带评论、wink等等所有信息
     *
     * @param momentsId 朋友圈消息id
     * @param pageSize  当前页码
     * @param curPage   每页显示多少条数据
     */
    @GET(RequestConstants.MOMENTS_GET_MOMENT_INFO)
    Flowable<MomentsBean> getMomentInfo(@Query("momentsId") String momentsId, @Query("pageSize") String pageSize, @Query("curPage") String curPage);

    /***
     * 获取话题详细 4.6.0
     *
     * **/
    @GET(RequestConstants.MOMENTS_GET_THEME_INFO)
    Flowable<MomentsBeanNew> getThemeMomentInfo(@Query("id") String momentsId);

    /**
     * 4.1.0 话题回复
     *
     * @param map 要回复的话题bean
     */
    @POST(RequestConstants.THEMES_COMMENT_ADD)
    @FormUrlEncoded
    Flowable<BaseDataBean> releaseThemesComment(@FieldMap Map<String, String> map);

    /**
     * 发布一条评论
     *
     * @param map
     * @return
     */
    @POST(RequestConstants.MOMENTS_RELEASE_COMMENT)
    @FormUrlEncoded
    Flowable<ReleasedCommentBeanNew> releaseComment(@FieldMap Map<String, String> map);

    /**
     * 获取被推荐的日志详情
     *
     * @param momentId
     */
    @GET(RequestConstants.GET_FRIEND_RECOMMEND_DETAIL)
    Flowable<MomentsDataBean> getFriendRecommendDetail(@Query("id") String momentId);

    /**
     * 推荐的日志列表的日志详情
     *
     * @param momentId
     * @param cursor
     * @param limit
     * @return
     */
    @GET(RequestConstants.GET_MOMENTS_FRIEND_RECOMMEND_LIST)
    Flowable<FriendRecommendDataBean> getMomentsFriendRecommendList(@Query("id") String momentId, @Query("cursor") String cursor, @Query("limit") String limit);

    /**
     * 发布一条推荐用户日志
     *
     * @param map
     */
    @POST(RequestConstants.MOMENTS_RELEASE_MOMENT)
    @FormUrlEncoded
    Flowable<ReleaseMomentSucceedBean> releaseUserCardMoment(@FieldMap Map<String, String> map);

    /**
     * 查看指定用户的视频列表
     *
     * @param userId
     * @param cursor
     * @param limit
     * @return
     */
    @GET(RequestConstants.GET_MOMENTS_VIDEO_USER_LIST)
    Flowable<VideoListBeanNew> getVideoListData(@Query("userId") String userId, @Query("cursor") String cursor, @Query("limit") String limit);

    /**
     * 视频播放次数上报
     *
     * @param map
     * @return
     */
    @POST(RequestConstants.POST_VIDEO_PLAY_COUNT_UPLOAD)
    @FormUrlEncoded
    Flowable<BaseDataBean> postVideoPlayCountUpload(@FieldMap Map<String, String> map);

    /**
     * 获取开关列表
     */
    @GET(RequestConstants.PUSH_SWITCH)
    Flowable<PushSwitchTypeBean> getPushSwitchList();

    /**
     * 获取小红点开关列表
     */
    @GET(RequestConstants.GET_REDPOINT_SWITCH)
    Flowable<PushSwitchTypeBean> getredPointSwitchList();

    /**
     * 获取屏蔽日志的用户列表
     */
    @GET(RequestConstants.GET_BLOCK_USER_LIST_FOR_MOMENTS)
    Flowable<BlackMomentListBean> getBlockUserListForMoments();

    /**
     * 屏蔽某人的日志(从屏蔽黑名单中移除)
     */
    @POST(RequestConstants.CANCEL_BLOCK_USER_MOMENTS)
    Flowable<BaseDataBean> cancelBlockUserMoments(@Body RequestBody body);

    /**
     * 屏蔽某人的日志(从屏蔽黑名单中移除)
     */
    @POST(RequestConstants.CANCEL_BLOCK_USER_MOMENTS)
    @FormUrlEncoded
    Flowable<BaseDataBean> cancelBlockUserMoments(@FieldMap Map<String, String> map);

    /**
     * 红点开关
     */
    @POST(RequestConstants.SWITCH_REDPOINT_UPDATE)
    @FormUrlEncoded
    Flowable<PushSwitchTypeBean> pushRedPointConfig(@FieldMap Map<String, String> map);

    /**
     * 推送开关
     */
    @POST(RequestConstants.PUSH_CONFIG)
    @FormUrlEncoded
    Flowable<BaseDataBean> pushConfig(@FieldMap Map<String, String> map);

    @GET(RequestConstants.GET_COLLECTION_LIST)
    Flowable<FavoritesBean> getFavoriteList(@Query(RequestConstants.I_LIMIT) String limit, @Query(RequestConstants.I_CURSOR) String cursor);

    @POST(RequestConstants.POST_COLLECTION_DELETE)
    @FormUrlEncoded
    Flowable<BaseDataBean> deleteFavoriteMoment(@Field(RequestConstants.FAVORITE_ID) String favoriteId, @Field(RequestConstants.FAVORITE_COUNT) String momentsId, @Field(RequestConstants.FAVORITE_TYPE) String favoriteType);

    @POST(RequestConstants.POST_COLLECTION_DELETE)
    @FormUrlEncoded
    Flowable<BaseDataBean> deleteFavoriteMoment(@FieldMap Map<String, String> map);

    /**
     * @param channel 传空获取默认渠道，获取vivo渠道传入vivo
     * @return
     */
    @GET(RequestConstants.GET_GOLD_LIST)
    Flowable<SoftMoneyListBean> getGoldListData(@Query("channel") String channel);

    /**
     * 获取正在直播的用户列表
     *
     * @return
     */
    @GET(RequestConstants.GET_FOLLOWING_USERS_V2)
    Flowable<LiveFollowingUsersBeanNew> getFollowingUsers();

    /**
     * 获取评论的回复列表
     *
     * @param commentId
     * @param cursor
     * @param limit
     * @return
     */
    @GET(RequestConstants.GET_COMMENT_REPLY_LIST)
    Flowable<CommentReplyListBeanNew> getCommentReplyList(@Query("id") String commentId, @Query("cursor") String cursor, @Query("limit") String limit);

    /**
     * 回复一条评论
     *
     * @param map
     * @return
     */
    @POST(RequestConstants.REPLY_COMMENT)
    @FormUrlEncoded
    Flowable<ReleasedCommentReplyBeanNew> replyComment(@FieldMap Map<String, String> map);

    /**
     * 删除一条评论
     *
     * @param map
     * @return
     */
    @POST(RequestConstants.MOMENTS_DELETE_COMMENT)
    @FormUrlEncoded
    Flowable<BaseDataBean> deleteComment(@FieldMap Map<String, String> map);

    /**
     * 删除一条评论
     *
     * @param map
     * @return
     */
    @POST(RequestConstants.DELETE_COMMENT_REPLY)
    @FormUrlEncoded
    Flowable<BaseDataBean> deleteCommentReply(@FieldMap Map<String, String> map);

    /**
     * 举报一条评论
     *
     * @param map
     * @return
     */
    @POST(RequestConstants.MOMENTS_REPORT_COMMENT)
    @FormUrlEncoded
    Flowable<BaseDataBean> reportComment(@FieldMap Map<String, String> map);

    /**
     * 获取moment详情，2.20.0版本后用在MomentCommentActivity页面
     *
     * @param id 日志id
     * @return
     */
    @GET(RequestConstants.GET_MOMENT_DETAIL)
    Flowable<MomentsBeanNew> getMomentInfoV2(@Query("id") String id);

    /**
     * 获取moment的评论列表
     *
     * @param commentId
     * @param limit
     * @param cursor
     * @return
     */
    @GET(RequestConstants.GET_COMMENT_LIST)
    Flowable<CommentListBeanNew> getCommentList(@Query("id") String commentId, @Query("limit") String limit, @Query("cursor") String cursor);

    /**
     * @param momentsId
     * @param reasonType
     * @return
     */
    @GET(RequestConstants.BLOCK_THIS_MOMENT)
    Flowable<BaseDataBean> blockThisMoment(@Query("momentsId") String momentsId, @Query("reasonType") String reasonType);

    /**
     * 举报一条日志
     *
     * @param momentsId
     * @param reasonType
     * @param reasonContent
     * @return
     */
    @GET(RequestConstants.MOMENTS_REPORT_MOMENT)
    Flowable<BaseDataBean> reportMoment(@Query("momentsId") String momentsId, @Query("reasonType") String reasonType, @Query("reasonContent") String reasonContent);

    /**
     * 举报用户
     *
     * @return
     */
    @GET(RequestConstants.REPORT_USER)
    // Flowable<BaseDataBean> reportUser(@Query("userId") String userId, @Query("reasonType") String reasonType, @Query("imageUrlList") String imageUrlList, @Query("reasonContent") String reasonContent);

    Flowable<BaseDataBean> reportUser(@FieldMap Map<String, String> map);

    /**
     * 举报用户
     *
     * @param feedBackType
     * @param imageUrlList
     * @param feedBackContent
     * @return
     */
    @GET(RequestConstants.BUG_FEEDBACK)
    Flowable<BaseDataBean> bugFeedback(@Query("feedBackType") String feedBackType, @Query("imageUrlList") String imageUrlList, @Query("feedBackContent") String feedBackContent);

    /**
     * 2.22.0
     * 举报用户（观众或者主播）
     *
     * @param map
     * @return
     */
    @POST(RequestConstants.POST_REPORT_LIVESHOW_USER)
    @FormUrlEncoded
    Flowable<BaseDataBean> postReportLiveShowUser(@FieldMap Map<String, String> map);


    @GET(RequestConstants.GET_RECHARGE_RECORD)
    Flowable<RechargeRecordListBeanNew> getRechargeRecord(@Query(RequestConstants.I_CURSOR) String cursor, @Query(RequestConstants.I_LIMIT) String limit, @Query(RequestConstants.I_FILTER) String filter);

    @POST(RequestConstants.SEND_WINK_CREATE)
    @FormUrlEncoded
    Flowable<BaseDataBean> sendWink(@FieldMap Map<String, String> map);

    /**
     * 新注册用户获取推荐关注用户列表
     *
     * @param curPage
     */
    @GET(RequestConstants.GET_INIT_RECOMMEND_USERS)
    Flowable<InitRecommendUserListBean> getInitRecommendUsers(@Query("curPage") String curPage);

    /**
     * 4.1.0 获取热门话题回复
     * 获取moment的评论列表
     *
     * @param momentsId 朋友圈消息id
     * @param pageSize  当前页码
     * @param curPage   每页显示多少条数据
     */
    @GET(RequestConstants.THEMES_COMMENT_LIST)
    Flowable<ThemeCommentListBeanNew> getThemeCommentList(@Query("cursor") String curPage, @Query("limit") String pageSize, @Query("id") String momentsId);

    @POST(RequestConstants.POST_REPORT_USER_ORIMAGE)
    @FormUrlEncoded
    Flowable<BaseDataBean> postReportImageOrUser(@FieldMap Map<String, String> map);

    @GET(RequestConstants.GET_USERS_RELATION)
    Flowable<FriendListBean> getUsersRelation(@Query(RequestConstants.I_TYPE) String type);

    @POST(RequestConstants.UPLOAD_DEVICE_TOKEN)
    @FormUrlEncoded
    Flowable<BaseDataBean> uploadDeviceToken(@FieldMap Map<String, String> map);

    @GET(RequestConstants.MOMENTS_MENTIONED_LIST)
    Flowable<TopicFollowerListBean> getMomentMentionedList(@Query("momentsId") String momentsId);

    @GET(RequestConstants.GET_LIVE_CLASSIFICATION)
    Flowable<LiveClassificationBean> getLiveType();

    @GET(RequestConstants.CHECK_UPDATE)
    Flowable<VersionBean> checkUpdate(@QueryMap Map<String, String> map);

    @POST(RequestConstants.SUBMIT_VIDEO_REPORT)
    @FormUrlEncoded
    Flowable<BaseDataBean> submitVideoReport(@FieldMap Map<String, String> map);

    @GET(RequestConstants.GET_MUSIC_BY_CATEGORY)
    Flowable<MusicListBean> getByCategory(@QueryMap Map<String, String> map);

    @GET(RequestConstants.GET_HOT_MUSIC)
    Flowable<MusicListBean> getHot(@QueryMap Map<String, String> map);

    @GET(RequestConstants.GET_NEWEST_MUSIC)
    Flowable<MusicListBean> getNewest(@QueryMap Map<String, String> map);

    /**
     * 新用户推荐 4.5版本新增
     *
     * @param cursor 游标 默认0
     * @param limit  步长 默认20
     */
    @GET(RequestConstants.GET_RECOMMEND_LIST)
    Flowable<RecommendListBean> getRecommendList(@Query(RequestConstants.I_CURSOR) String cursor, @Query(RequestConstants.I_LIMIT) String limit);

    @GET(RequestConstants.GET_USER_LEVEL)
    Flowable<UserLevelBean> getUserLevel();

    @POST(RequestConstants.POST_USER_LEVEL_SETTING)
    @FormUrlEncoded
    Flowable<UserLevelSwitchBean> postLevelSetting(@FieldMap Map<String, String> map);

    @POST(RequestConstants.POST_PLAY_VIDEO_INFO)
    @FormUrlEncoded
    Flowable<BaseDataBean> postPlayVideoInfo(@FieldMap Map<String, String> map);

    /**
     * 移除粉丝
     */
    @POST(RequestConstants.POST_REMOVEFANS)
    @FormUrlEncoded
    Flowable<BaseDataBean> postRemoveFans(@FieldMap Map<String, String> map);

    /**
     * 获取芝麻认证的url认证
     */
    @POST(RequestConstants.POST_ZMXY_AUTH)
    @FormUrlEncoded
    Flowable<ZmxyAuthBean> postAuth(@FieldMap Map<String, String> map);

    /**
     * 验证芝麻信用回调数据
     */
    @POST(RequestConstants.POST_CHECK_CALLBACK)
    @FormUrlEncoded
    Flowable<ZmxyCheckCallBackBean> postCheckCallBack(@FieldMap Map<String, String> map);

    /**
     * push上报
     */
    @POST(RequestConstants.PUSH_ACTION_UPLOAD)
    @FormUrlEncoded
    Flowable<BaseDataBean> pushActionUpload(@FieldMap Map<String, String> map);

    /**
     * 判断是否是首充
     */
    @GET(RequestConstants.GET_IS_FIRST_CHARGE)
    Flowable<GiftFirstChargeBean> getIsFirstCharge();


    /**
     * 图片排序
     */
    @POST(RequestConstants.IMAGE_SORT)
    @FormUrlEncoded
    Flowable<BaseDataBean> sortImages(@FieldMap Map<String, String> map);

    /**
     * 检查公告栏是否合法
     */
    @POST(RequestConstants.CHECK_ANNOUNEMENT)
    @FormUrlEncoded
    Flowable<LiveNoticeCheckBean> checkAnnounement(@FieldMap Map<String, String> map);

    /**
     * 退出匹配
     **/
    @POST(RequestConstants.EXIT_MATCH)
    @FormUrlEncoded
    Flowable<BaseDataBean> exitMatch(@FieldMap Map<String, String> map);

    /**
     * 匹配设置
     */
    @POST(RequestConstants.MATCH_PROFILE)
    @FormUrlEncoded
    Flowable<BaseDataBean> uploadMatchProfile(@FieldMap Map<String, String> map);

    @GET(RequestConstants.GET_MATCH_PROFILE)
    Flowable<MatchSettingsBean> getMatchProfile();

    /**
     * 首页获取推荐用户列表
     */
    @GET(RequestConstants.FOLLOW_PAGE_RECOMMEND_LIST)
    Flowable<RecommendListBean> getRecommendUsers(@Query("type") String type,@Query(RequestConstants.I_CURSOR) String cursor, @Query(RequestConstants.I_LIMIT) String limit);


}
