package com.thel.network.api.loginapi;

import android.text.TextUtils;

import com.thel.network.RequestConstants;
import com.thel.network.api.loginapi.bean.SignInUserBean;

import java.util.HashMap;

/**
 * Created by waiarl on 2017/9/23.
 * 登录的一些针对对象的判定方法
 */

public class LoginUtils {
    /**
     * 是否是新注册用户
     * 4.1.0:根据昵称，头像，relaId是否有一个为空来判断，若有一个为空，则需完善个人资料
     *
     * @param signInUserBean
     * @return
     */
    public static boolean isNewUser(SignInUserBean signInUserBean) {
        if (null == signInUserBean) {
            return true;
        }
        return TextUtils.isEmpty(signInUserBean.nickName) || TextUtils.isEmpty(signInUserBean.avatar) || TextUtils.isEmpty(signInUserBean.userName);
    }


    /**
     * 获取登录接口参数
     *
     * @param type     登录类型:cell,password,sns,wx,fb
     * @param email    邮箱
     * @param password 密码
     * @param cell     手机号
     * @param zone     区号
     * @param code     验证码
     * @param snsId    社交软件用户id
     * @param snsToken token
     * @return
     */
    public static HashMap<String, String> getSignInParameterMap(String type, String email, String password, String cell, String zone, String code, String snsId, String snsToken, String avtar, String nickName) {
        HashMap<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_TYPE, type);
        data.put(RequestConstants.I_EMAIL, email);
        data.put(RequestConstants.I_PASSWORD, password);
        data.put(RequestConstants.I_CELL, cell);
        data.put(RequestConstants.I_ZONE, zone);
        data.put(RequestConstants.I_CODE, code);
        data.put(RequestConstants.I_SNS_ID, snsId);
        data.put(RequestConstants.I_SNS_TOKEN, snsToken);
        data.put(RequestConstants.I_MOBID, "20");
        if (!TextUtils.isEmpty(avtar)) {
            data.put(RequestConstants.I_AVATAR, avtar);
        }
        if (!TextUtils.isEmpty(nickName)) {
            data.put(RequestConstants.I_NICKNAME, nickName);
        }
        return data;
    }


    /**
     * 手机号、微信、fb绑定 参数
     *
     * @param type         登录类型:cell,wx,fb
     * @param cell         手机号
     * @param zone         区号
     * @param code         验证码
     * @param snsId        社交软件用户id
     * @param snsToken     token
     * @param smsRequestId 调用服务器发送验证码的话，就会获取这个参数
     * @return 错误码：already_bind_by_others 已被其他帐号绑定 invalid_sms_code 验证码错误
     */
    public static HashMap<String, String> rebindMap(String type, String cell, String zone, String code, String snsId, String snsToken, String smsRequestId) {
        HashMap<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_TYPE, type);
        data.put(RequestConstants.I_CELL, cell);
        data.put(RequestConstants.I_ZONE, zone);
        data.put(RequestConstants.I_CODE, code);
        data.put(RequestConstants.I_SNS_ID, snsId);
        data.put(RequestConstants.I_MOBID, "20");
        data.put(RequestConstants.I_SNS_TOKEN, snsToken);
        if (!TextUtils.isEmpty(smsRequestId)) {
            data.put(RequestConstants.I_SMS_REQUEST_ID, smsRequestId);
        }
        return data;
    }
}
