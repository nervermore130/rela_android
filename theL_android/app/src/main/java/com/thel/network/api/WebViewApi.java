package com.thel.network.api;

import java.util.Map;

import io.reactivex.Flowable;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

/**
 * Created by liuyun on 2017/12/9.
 */

public interface WebViewApi {

    @GET
    Flowable<String> getWebView(@Url String url, @QueryMap Map<String, String> map);

    @POST
    @FormUrlEncoded
    Flowable<String> postWebView(@Url String url, @FieldMap Map<String, String> map);
}
