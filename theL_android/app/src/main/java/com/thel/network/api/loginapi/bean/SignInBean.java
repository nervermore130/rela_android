package com.thel.network.api.loginapi.bean;

import com.thel.base.BaseDataBean;
import com.thel.bean.Auth;

import java.io.Serializable;

/**
 * Created by waiarl on 2017/9/23.
 * 登录/注册 对象
 */

public class SignInBean extends BaseDataBean implements Serializable {

    public Data data;

    @Override
    public String toString() {
        return "SignInBean{" +
                ", data=" + data +
                '}';
    }

    public class Data implements Serializable {
        /**
         * 登录,注册获取用户标识
         */

        public String code;

        public String key;

        public SignInUserBean user;

        public Auth auth = new Auth();

        @Override
        public String toString() {
            return "Data{" +
                    "key='" + key + '\'' +
                    "auth='" + auth + '\'' +
                    ", user=" + user +
                    '}';
        }
    }
}
