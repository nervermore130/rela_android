package com.thel.network.api;

import com.thel.base.BaseDataBean;
import com.thel.bean.ReleasedCommentBeanV2New;
import com.thel.bean.moments.MomentListDataBean;
import com.thel.bean.moments.MomentsListBean;
import com.thel.bean.user.BlockBean;
import com.thel.network.RequestConstants;

import java.util.Map;

import io.reactivex.Flowable;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by liuyun on 2017/10/26.
 */

public interface MomentApi {

    /**
     * 屏蔽某人的日志(加入到屏蔽黑名单)
     *
     * @param map
     */
    @POST(RequestConstants.BLOCK_USER_MOMENTS)
    @FormUrlEncoded
    Flowable<BlockBean> blockUserMoments(@FieldMap Map<String, String> map);

    /**
     * 2.17 收藏
     *
     * @param map
     * @return
     */
    @POST(RequestConstants.POST_COLLECTION_CREATE)
    @FormUrlEncoded
    Flowable<BaseDataBean> getFavoriteCreate(@FieldMap Map<String, String> map);

    /**
     * 删除日志
     *
     * @param map
     * @return
     */
    @POST(RequestConstants.POST_COLLECTION_DELETE)
    @FormUrlEncoded
    Flowable<BaseDataBean> deleteFavoriteMoment(@FieldMap Map<String, String> map);

    /**
     * 删除一条日志
     *
     * @param map
     */
    @POST(RequestConstants.MOMENTS_DELETE_MOMENT)
    @FormUrlEncoded
    Flowable<BaseDataBean> deleteMoment(@FieldMap Map<String, String> map);

    /**
     * 置顶我的日志
     *
     * @param map
     */
    @POST(RequestConstants.STICK_MY_MOMENT)
    @FormUrlEncoded
    Flowable<BaseDataBean> stickMyMoment(@FieldMap Map<String, String> map);

    /**
     * 取消置顶我的日志
     *
     * @param map
     */
    @POST(RequestConstants.UNSTICK_MY_MOMENT)
    @FormUrlEncoded
    Flowable<BaseDataBean> unstickMyMoment(@FieldMap Map<String, String> map);

    /**
     * 将我的公开日志设为隐私
     *
     * @param map
     */
    @POST(RequestConstants.SET_MY_MOMENT_AS_PRIVATE)
    @FormUrlEncoded
    Flowable<BaseDataBean> setMyMomentAsPrivate(@FieldMap Map<String, String> map);

    /**
     * 将我的隐私日志设为公开
     *
     * @param map
     * @return
     */
    @POST(RequestConstants.SET_MY_MOMENT_AS_PUBLIC)
    @FormUrlEncoded
    Flowable<BaseDataBean> setMyMomentAsPublic(@FieldMap Map<String, String> map);


    /**
     * 回复一条评论
     *
     * @param map
     * @return
     */
    @POST(RequestConstants.REPLY_MOMENT)
    @FormUrlEncoded
    Flowable<ReleasedCommentBeanV2New> replyMoment(@FieldMap Map<String, String> map);

    /**
     * 点赞一条日志
     * 2.22.0版本开始，只有一种点赞类型，就是喜欢，类型即为8
     *
     * @param map
     */
    @POST(RequestConstants.MOMENTS_WINK_MOMENT)
    @FormUrlEncoded
    Flowable<BaseDataBean> likeMoment(@FieldMap Map<String, String> map);

    /**
     * 撤销点赞
     *
     * @param map
     */
    @POST(RequestConstants.MOMENTS_UNWINK_MOMENT)
    @FormUrlEncoded
    Flowable<BaseDataBean> unwinkMoment(@FieldMap Map<String, String> map);

    /**
     * 恢复被删除的日志
     *
     * @param map
     * @return
     */
    @POST(RequestConstants.RECOVER_MOMENT)
    @FormUrlEncoded
    Flowable<BaseDataBean> indifferent(@FieldMap Map<String, String> map);

    /**
     * 获取7天内被删除的日志列表
     *
     * @param cursor
     * @param limit
     * @return
     */
    @GET(RequestConstants.DELETE_MOMENT_LIST)
    Flowable<MomentListDataBean> getDeleteMoments(@Query(RequestConstants.I_CURSOR) int cursor, @Query(RequestConstants.I_LIMIT) int limit);

    /***
     * 匹配用户详情里 展示的用户日志列表
     * */
    @GET(RequestConstants.MATCH_USER_MOMENT)
    Flowable<MomentListDataBean> getMatchUserMomentsList(@Query(RequestConstants.I_USERID) String userId);
}
