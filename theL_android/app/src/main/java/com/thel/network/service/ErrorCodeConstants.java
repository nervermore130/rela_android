package com.thel.network.service;

/**
 * Created by waiarl on 2018/1/8.
 * 错误码常量
 */

public class ErrorCodeConstants {
    /***个人主页，此用户已注销***/
    public static final String ERROR_CODE_USER_UNREGISTER = "user_unregister";
  /*
            "param_error":                "出了点小错",
            "require_login":              "你还没有登录哦",
            "server_error":               "服务器端错误，休息一会再来吧",
            "favorite_repeate_error":     "你已经收藏过了",
            "app_latest_version_error":   "当前已是最新版本",
            "user_no_permission_error":   "你不能这样做哦",
            "refresh_too_fast":           "我们正在努力为您生成关注列表，喝口茶休息一下吧",
            "comm_aleady_publish":        "文本内容相同，请隔5分钟再进行发布",
            "sensitive_words":            "请遵守Rela社区规定，检查并删除敏感词汇，谢谢配合！",
            "nickname_modify_frequently": "每30天可以修改一次昵称",
            "mom_aleady_publish":         "文本内容相同，请隔5分钟再进行发布",
            "user_aleady_report":         "已经举报过她啦",
            "report_yourself":            "不能举报自己",
            "office_account_tips":        "抱歉，此为官方账号，无法进行该操作！",
            "user_unregister":            "此用户已注销！",
            "unbind_cellphone":           "根据2017年6月1日执行的《中华人民共和国网络安全法》，互联网用户需进行手机验证。请放心，我们不会以任何方式泄漏你的个人隐私。请在Rela内 “ 我的 > 系统设置 > 手机 ”页面绑定手机。感谢合作！",
            "no_match_user":              "很抱歉，暂时没有匹配到符合要求的用户，换个要求试试吧",
            "api_limit":                  "当前Rela用户访问量较大，暂时挤不进去了，待会再来吧~",
            "2":                          "你还没有登录哦",
            "5":                          "禁止自恋！",
            "7":                          "不能将自己加入黑名单！",
            "8":                          "她已经在您的黑名单中！",
            "10":                         "不能对自己传情！",
            "19":                         "不能关注自己",
            "20":                         "你已经关注她啦",
            "22":                         "只能发9张图哦",
            "26":                         "图片不存在！",
            "27":                         "至少有一张图片必须公开！",
            "33":                         "购买会员能查看更多哦！",
            "37":                         "写点评论吧",
            "80":                         "她已经被禁用了！",
            "81":                         "此用户不存在！",
            "82":                         "您已经举报过该评论！",
            "83":                         "抱歉，您无权删除此评论",
            "84":                         "该评论不存在！",
            "86":                         "别人的日志不能乱动！",
            "87":                         "该日志不存在！",
            "121":                        "你已经点过赞了！",
            "301":                        "该图片不能设为封面！",
            "427":                        "您已经举报过这条日志了！",
            "429":                        "您已经隐藏过这条日志了！",
            "770":                        "你的蜜友太多啦，请先解除一个蜜友关系或清除过期请求！",
            "771":                        "处理请求失败！",
            "772":                        "你们没有互相关注！",
            "773":                        "请求不存在！",
            "776":                        "密友请求已发送！",
            "777":                        "没有个人相册！",
            "779":                        "不能脚踏两条船哦，请先解除当前的伴侣关系",
            "780":                        "她已经名花有主了！",
            "782":                        "请求已经过期！",
            "784":                        "取消绑定失败！",
            "789":                        "对方已经对你发送了请求！",
            "790":                        "你们已经是情侣了！",
            "791":                        "你们已经是密友了！",
            "870":                        "成为会员才能操作此日志哦",
            "888":                        "请完善个人资料",
            "999":                        "已经在黑名单里躺着了",
            "1000":                       "您已屏蔽了她",
            "1001":                       "没有广告",
            "8700":                       "话题评论出错了！",
            "10003":                      "Rela ID重复啦！",
            "10102":                      "今天已经对她挤眼过咯，明天再来吧！",
            "10103":                      "已到达今天的挤眼上限啦，明日再战吧！",
            "10201":                      "已到达今天的关注上限啦，去看看大家的日志吧！",
            "200301":                     "恭喜你达成今日发言小能手称号！明天再继续吧",
            "200302":                     "错误的评论类型！",
            "200401":                     "生活日记需要慢慢积累，停下来听听她人的声音吧",
            "200402":                     "你已经被她屏蔽了",
            "873332":                     "她设置了日志权限",*/
}
