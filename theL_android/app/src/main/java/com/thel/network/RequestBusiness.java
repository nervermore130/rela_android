package com.thel.network;

import android.text.TextUtils;

import com.thel.BuildConfig;
import com.thel.app.TheLApp;
import com.thel.base.BaseDataBean;
import com.thel.bean.AdBean;
import com.thel.bean.AwaitLiveBean;
import com.thel.bean.BasicInfoNetBean;
import com.thel.bean.CheckUserNameBean;
import com.thel.bean.ContactsListBean;
import com.thel.bean.FavoritesBean;
import com.thel.bean.FirstHotTopicBean;
import com.thel.bean.FriendListBean;
import com.thel.bean.GoogleIapNotifyResultBean;
import com.thel.bean.LikeResultBean;
import com.thel.bean.LiveClassificationBean;
import com.thel.bean.LivePermitBean;
import com.thel.bean.MomentSongBean;
import com.thel.bean.MusicListBean;
import com.thel.bean.PayOrderBean;
import com.thel.bean.RecentAndHotTopicsBean;
import com.thel.bean.RechargeRecordListBeanNew;
import com.thel.bean.ReleaseMomentSucceedBean;
import com.thel.bean.ReleaseMomentSucceedBean2;
import com.thel.bean.ResultBean;
import com.thel.bean.SearchUsersListBean;
import com.thel.bean.StickerPackListNetBean;
import com.thel.bean.StickerPackNetBean;
import com.thel.bean.SuperlikeBean;
import com.thel.bean.SuperlikeListBean;
import com.thel.bean.TopicListBean;
import com.thel.bean.TrendingTagsBean;
import com.thel.bean.UploadVideoAlbumBean;
import com.thel.bean.VersionBean;
import com.thel.bean.gift.WinkListBean;
import com.thel.bean.live.LiveRecommendNetBean;
import com.thel.bean.live.LiveTypeNetBean;
import com.thel.bean.me.MyBlockBean;
import com.thel.bean.me.MyCircleFriendListBean;
import com.thel.bean.me.MyCircleRequestListBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.moments.MomentsMsgsListBean;
import com.thel.bean.recommend.RecommendedStickerListNetBean;
import com.thel.bean.theme.ThemeListBean;
import com.thel.bean.user.BlackMomentListBean;
import com.thel.bean.user.BlockListBean;
import com.thel.bean.user.LocationNameBean;
import com.thel.bean.user.MatchQuestionBean;
import com.thel.bean.user.MyImagesListBean;
import com.thel.bean.user.NearUserListNetBean;
import com.thel.bean.user.UploadTokenBean;
import com.thel.bean.user.VipConfigBean;
import com.thel.db.MomentsDataBaseAdapter;
import com.thel.flutter.bridge.RfSBridgeHandlerFactory;
import com.thel.modules.live.bean.LivePopularityNetBean;
import com.thel.modules.live.bean.LiveRoomNetBean;
import com.thel.modules.live.bean.LiveRoomsNetBean;
import com.thel.modules.live.bean.SoftMoneyListBean;
import com.thel.modules.live.bean.ZmxyAuthBean;
import com.thel.modules.live.bean.ZmxyCheckCallBackBean;
import com.thel.modules.main.me.bean.FriendsListBean;
import com.thel.modules.main.me.bean.FriendsListBeanNew;
import com.thel.modules.main.me.bean.IncomeRecordListBean;
import com.thel.modules.main.me.bean.LikeCountNetBean;
import com.thel.modules.main.me.bean.MatchListBean;
import com.thel.modules.main.me.bean.MyInfoNetBean;
import com.thel.modules.main.me.bean.UpdataInfoBean;
import com.thel.modules.main.me.bean.UserLevelSwitchBean;
import com.thel.modules.main.me.bean.VipListBean;
import com.thel.modules.main.me.bean.WalletBean;
import com.thel.modules.main.settings.bean.PushSwitchTypeBean;
import com.thel.modules.main.userinfo.bean.FollowersListNetBean;
import com.thel.network.api.loginapi.bean.CheckNumberBean;
import com.thel.network.api.loginapi.bean.SignInBean;
import com.thel.network.service.DefaultRequestService;
import com.thel.utils.DeviceUtils;
import com.thel.utils.MD5Utils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.Utils;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Flowable;
import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * Created by liuyun on 2017/9/25.
 */

public class RequestBusiness {

    private static RequestBusiness INSTANCE;

    private RequestBusiness() {

    }


    public static RequestBusiness getInstance() {


        if (INSTANCE == null) {
            INSTANCE = new RequestBusiness();
        }

        return INSTANCE;
    }

    /**
     * 登陆／注册接口
     *
     * @param type
     * @param email
     * @param password
     * @param cell
     * @param zone
     * @param code
     * @param snsId
     * @param snsToken
     * @param avatar
     * @param nickName
     * @param smsRequestId
     * @return
     */
    public Flowable<SignInBean> signIn(String type, String email, String password, String cell, String zone, String code, String snsId, String snsToken, String avatar, String nickName, String smsRequestId) {
        Map<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_TYPE, type);
        data.put(RequestConstants.I_EMAIL, email);
        data.put(RequestConstants.I_PASSWORD, password);
        data.put(RequestConstants.I_CELL, cell);
        data.put(RequestConstants.I_ZONE, zone);
        data.put(RequestConstants.I_CODE, code);
        data.put(RequestConstants.I_SNS_ID, snsId);
        data.put(RequestConstants.I_SNS_TOKEN, snsToken);
        data.put(RequestConstants.I_MOBID, "20");
        if (!TextUtils.isEmpty(avatar)) {
            data.put(RequestConstants.I_AVATAR, avatar);
        }
        if (!TextUtils.isEmpty(nickName)) {
            data.put(RequestConstants.I_NICKNAME, nickName);
        }
        if (!TextUtils.isEmpty(smsRequestId))
            data.put(RequestConstants.I_SMS_REQUEST_ID, smsRequestId);

        return DefaultRequestService.createLoginRequestService().signIn(MD5Utils.generateSignatureForMap(data));
    }

    /**
     * 账号绑定接口参数
     *
     * @param type     登录类型:cell,sns,wx,fb
     * @param cell     手机号
     * @param zone     区号
     * @param code     验证码
     * @param snsId    社交软件用户id
     * @param snsToken token
     * @return
     */
    public Flowable<BaseDataBean> rebind(String type, String cell, String zone, String code, String snsId, String snsToken, String smsRequestId) {
        HashMap<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_TYPE, type);
        data.put(RequestConstants.I_CELL, cell);
        data.put(RequestConstants.I_ZONE, zone);
        data.put(RequestConstants.I_CODE, code);
        data.put(RequestConstants.I_SNS_ID, snsId);
        data.put(RequestConstants.I_MOBID, "20");
        data.put(RequestConstants.I_SNS_TOKEN, snsToken);
        if (!TextUtils.isEmpty(smsRequestId))
            data.put(RequestConstants.I_SMS_REQUEST_ID, smsRequestId);

        return DefaultRequestService.createLoginRequestService().rebind(MD5Utils.generateSignatureForMap(data));
    }

    /**
     * 检查电话号码是否被注册过了
     *
     * @param zone
     * @param cell
     */
    public Flowable<CheckNumberBean> checkPhoneNumber(String zone, String cell) {
        HashMap<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_ZONE, zone);
        data.put(RequestConstants.I_CELL, cell);
        return DefaultRequestService.createLoginRequestService().checkPhoneNumber(data);
    }

    /**
     * 找回密码
     *
     * @param userEmail
     */
    public Flowable<BaseDataBean> findBackPassword(String userEmail) {
        return DefaultRequestService.createLoginRequestService().findBackPassword(userEmail);
    }

    public Flowable<BasicInfoNetBean> getBasicInfo() {
        return DefaultRequestService.createAllRequestService().getBasicInfo();
    }

    /**
     * 获取上传文件到七牛的token
     *
     * @param clientTime
     * @param bucket               如果是上传视频，则要传入特定的视频bucket：RequestConstants.QINIU_BUCKET_VIDEO
     * @param path
     * @param isReleaseVideoMoment 是否是发布视频日志
     * @return
     */

    public Flowable<UploadTokenBean> getUploadToken(String clientTime, String bucket, String path, boolean... isReleaseVideoMoment) {

        Map<String, String> data = new HashMap<>();

        if (!TextUtils.isEmpty(bucket))
            data.put(RequestConstants.I_BUCKET, bucket);
        data.put(RequestConstants.I_CLIENT_TIME, clientTime);
        data.put(RequestConstants.I_PATH, path);
        if (isReleaseVideoMoment.length > 0 && isReleaseVideoMoment[0]) {
            data.put("source", "video_moment");
            //final int index=path.lastIndexOf(".");
            //final String webpPath=path.substring(0,index)+".webp";
            data.put("webpPath", Utils.getWebpPathByVideoPath(path));
        }

        return DefaultRequestService.createAllRequestService().getUploadToken(MD5Utils.generateSignatureForMap(data));
    }

    public Flowable<ResultBean> followUser(String receivedId, String actionType) {
        Map<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_RECEIVEDID, receivedId);
        data.put(RequestConstants.I_ACTIONTYPE, actionType);

        return DefaultRequestService.createAllRequestService().followUser(MD5Utils.generateSignatureForMap(data));
    }

    /**
     * 获取用户的like数量
     *
     * @param userId
     * @return
     */
    public Flowable<LikeCountNetBean> getUserLikesCount(String userId) {
        return DefaultRequestService.createUserRequestService().getUserLikesCount(userId);
    }

    /**
     * 获取vip配置信息
     *
     * @return
     */
    public Flowable<VipConfigBean> getVipConfig() {
        return DefaultRequestService.createUserRequestService().getVipConfig();
    }

    /**
     * 获取vip配置信息
     *
     * @param vipHiding
     * @param hiding
     * @param incognito
     * @param followRemind
     * @param flag
     * @return
     */
    public Flowable<BaseDataBean> vipConfig(int liveHiding, int vipHiding, int hiding, int incognito, int followRemind, int msgHiding, String flag) {
        Map<String, String> data = new HashMap<>();
        if (vipHiding != -1)
            data.put("vipHiding", vipHiding + "");
        if (hiding != -1)
            data.put("hiding", hiding + "");
        if (incognito != -1)
            data.put("incognito", incognito + "");
        if (followRemind != -1)
            data.put("followRemind", followRemind + "");
        if (liveHiding != -1)
            data.put("liveHiding", liveHiding + "");
        if (msgHiding != -1)
            data.put("msgHiding", msgHiding + "");
        //传数据到flutter那边

        RfSBridgeHandlerFactory.getInstance().getFlutterPushImpl().pushVipConfig(data);

        return DefaultRequestService.createUserRequestService().vipConfig(MD5Utils.generateSignatureForMap(data));

    }

    /**
     * 上传头像
     *
     * @param avatar 图片地址
     * @return
     */
    public Flowable<BaseDataBean> uploadAvatar(String avatar) {
        Map<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_URL, avatar);

        String body = MD5Utils.generateSignature(data);

        return DefaultRequestService.createUserRequestService().uploadAvatar(MD5Utils.generateSignatureForMap(data));
    }


    /**
     * 上传用户资料的背景图片
     *
     * @param url
     * @return
     */
    public Flowable<BaseDataBean> uploadBgImage(String url) {
        Map<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_BGIMAGE, url);

        String body = MD5Utils.generateSignature(data);

        return DefaultRequestService.createUserRequestService().uploadBgImage(MD5Utils.generateSignatureForMap(data));
    }

    /***
     *
     * 获取关注列表
     * */
    public Flowable<FriendsListBean> getFollowList(String userid, String pageSize, String curPage) {

        return DefaultRequestService.createAllRequestService().getFollowList(userid, pageSize, curPage);

    }

    /***
     *4.19.0
     * 获取关注列表
     * */
    public Flowable<FollowersListNetBean> getUserFollowList(String userid, String pageSize, String curPage) {

        return DefaultRequestService.createAllRequestService().getUserFollowList(userid, pageSize, curPage);

    }

    /***
     *
     * 获取关注主播列表
     * */
    public Flowable<FriendsListBean> getFollowAnchorsList(String cursor) {

        return DefaultRequestService.createAllRequestService().getFollowAnchors(cursor);

    }


    /**
     * 获取粉丝列表
     */
    public Flowable<FriendsListBean> getFansList(String userid, String pageSize, String curPage) {
        return DefaultRequestService.createAllRequestService().getFansList(userid, pageSize, curPage);

    }

    /**
     * 获取悄悄查看列表
     */

    public Flowable<FriendsListBeanNew> getSecretFollowList(String userid, String pageSize, String curPage) {
        return DefaultRequestService.createAllRequestService().getSecretFollowList(userid, pageSize, curPage);

    }

    /**
     * 获取期待开播列表
     */

    public Flowable<AwaitLiveBean> getAwaitLiveList(String userid, String pageSize, String curPage) {
        return DefaultRequestService.createAllRequestService().getAwaitLiveList(userid, pageSize, curPage);

    }

    /**
     * 收到传情列表
     */
    public Flowable<WinkListBean> getUserWinkList(String userId, String cursor, String limit) {
        return DefaultRequestService.createAllRequestService().getUserWinkList(userId, cursor, limit);

    }

    /**
     * 获取朋友列表
     */
    public Flowable<FriendsListBean> getFriendsList(String userId, String pageSize, String curPage) {
        return DefaultRequestService.createAllRequestService().getFriendsList(userId, pageSize, curPage);

    }

    public Flowable<ContactsListBean> getContactsList() {
        return DefaultRequestService.createAllRequestService().getContactsList();
    }

    /**
     * 获取最近5个参与的话题，加上 15个热门的话题
     */
    public Flowable<RecentAndHotTopicsBean> getRecentAndHotTopics() {
        return DefaultRequestService.createAllRequestService().getRecentAndHotTopics();
    }

    /**
     * 搜索话题列表
     */
    public Flowable<TopicListBean> searchTopics(String topicName, int curPage) {
        return DefaultRequestService.createAllRequestService().searchTopics(topicName, curPage);
    }

    /**
     * 接口工程师要提供一下,请参照4.1.0上的设计
     * 获取最热门的标签
     */
    public Flowable<FirstHotTopicBean> getFirstHotTopic() {
        return DefaultRequestService.createAllRequestService().getFirstHotTopic();
    }

    /**
     * 接口工程师要提供一下,请参照4.1.0上的设计
     */
    public Flowable<String> getSingleUsersRelation(final String userId, final String nickname, final String avatar) {
        return null;
    }

    /**
     * 检查用户名是否重复
     */
    public Flowable<CheckUserNameBean> checkUserName(String userName) {
        return DefaultRequestService.createAllRequestService().checkUserName(userName);
    }

    /**
     * @param userName
     * @param birthday
     * @param wantRole
     * @param professionalTypes
     * @param avatarURL
     * @param nickName
     * @param height
     * @param weight
     * @param roleName
     * @param affection
     * @param purpose
     * @param ethnicity
     * @param occupation
     * @param liveCity
     * @param intro
     * @return
     */
    public Flowable<UpdataInfoBean> updateUserInfo(String userName, String birthday, String wantRole, String professionalTypes, String avatarURL, String nickName, String height, String weight, String roleName, String affection, String purpose, String ethnicity, String occupation, String liveCity, String intro) {
        Map<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_USER_NAME, userName);
        data.put(RequestConstants.I_BIRTHDAY, birthday);
        data.put(RequestConstants.I_WANT_ROLE, wantRole);
        data.put(RequestConstants.I_CAREER_TYPE, professionalTypes);
        // data.put(RequestConstants.I_OUT_LEVEL, outLevel);
        data.put(RequestConstants.I_AVATAR_URL, avatarURL);
        data.put(RequestConstants.I_NICKNAME, nickName);
        data.put(RequestConstants.I_HEIGHT, height);
        data.put(RequestConstants.I_WEIGHT, weight);
        data.put(RequestConstants.I_ROLENAME, roleName);
        data.put(RequestConstants.I_AFFECTION, affection);
        data.put(RequestConstants.I_PURPOSE, purpose);
        data.put(RequestConstants.I_ETHNICITY, ethnicity);
        data.put(RequestConstants.I_OCCUPATION, occupation);
        data.put(RequestConstants.I_LIVECITY, liveCity);
        // data.put(RequestConstants.I_OTHERS, others);
        data.put(RequestConstants.I_INTRO, intro);

        String body = MD5Utils.generateSignature(data);

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/x-www-form-urlencoded; charset=UTF8"), body);

        return DefaultRequestService.createAllRequestService().updateUserInfo(MD5Utils.generateSignatureForMap(data));
    }

    /**
     * 读取我的信息
     */
    public Flowable<MyInfoNetBean> getMyInfo() {
        return DefaultRequestService.createAllRequestService().getMyInfo();
    }

    /**
     * 用户昵称修改时间验证（30天内只能修改一次昵称）
     **/
    public Flowable<BaseDataBean> getNickNameTimeVerify(String nickname) {
        return DefaultRequestService.createAllRequestService().getNickNameTimeVerify(nickname);
    }

    public Flowable<ReleaseMomentSucceedBean2> releaseMoment(String momentsType, String momentsText, MomentSongBean song, int shareTo, int secret, String atUserList, String imageUrlList, String imageLengthList, String themeTagName, String videoUrl, long mediaSize, int playTime, int pixelWidth, int pixelHeight, String videoColor, String themeClass, String parentId, String userId, String videoWebp, String tagList) {

        Map<String, String> data = new HashMap<>();

        data.put(RequestConstants.I_MOMENTS_TYPE, momentsType);
        data.put(RequestConstants.I_MOMENT_TEXT, momentsText);
        data.put(RequestConstants.I_SHARE_TO, shareTo + "");
        data.put(RequestConstants.I_SECRET, secret + "");
        data.put(RequestConstants.I_IMAGE_URLS, imageUrlList);
        data.put(RequestConstants.I_IMAGE_LENGTHS, imageLengthList);
        data.put(RequestConstants.I_THEMECLASS, themeClass);
        if (!TextUtils.isEmpty(atUserList)) {
            data.put(RequestConstants.I_AT_USER_LIST, atUserList);
        }
        if (!TextUtils.isEmpty(tagList))
            data.put("tagList", tagList);
        if (song != null) {
            data.put(RequestConstants.I_SONG_ID, song.songId + "");
            data.put(RequestConstants.I_SONG_NAME, song.songName);
            data.put(RequestConstants.I_ARTIST_NAME, song.artistName);
            data.put(RequestConstants.I_ALBUM_NAME, song.albumName);
            data.put(RequestConstants.I_ALBUM_LOGO_100, song.albumLogo100);
            data.put(RequestConstants.I_ALBUM_LOGO_444, song.albumLogo444);
            data.put(RequestConstants.I_SONG_LOCATION, song.songLocation);
            data.put(RequestConstants.I_SONG_TO_URL, song.toURL);
        }

        if (!TextUtils.isEmpty(themeTagName))
            data.put("themeTagName", themeTagName);
        if (!TextUtils.isEmpty(videoUrl)) {
            data.put("videoUrl", videoUrl);
            data.put("mediaSize", mediaSize + "");
            data.put("playTime", (playTime / 1000) + "");
            data.put("pixelWidth", pixelWidth + "");
            data.put("pixelHeight", pixelHeight + "");
            data.put("videoColor", videoColor);
            data.put("videoWebp", videoWebp);
        }
        if (MomentsBean.MOMENT_TYPE_RECOMMEND.equals(momentsType)) {//如果是推荐日志的话
            data.put("parentId", parentId);
        }
        if (MomentsBean.MOMENT_TYPE_LIVE_USER.equals(momentsType)) {
            data.put("userId", userId + "");
        }

        data.put("isFive","1");

        return DefaultRequestService.createAllRequestService().releaseMoment(MD5Utils.generateSignatureForMap(data));
    }

    public Flowable<ReleaseMomentSucceedBean> uploadCover(String momentsType, String imageUrlList, String videoUrl, long mediaSize, int playTime, int pixelWidth, int pixelHeight, String videoColor, String videoWebp) {
        Map<String, String> data = new HashMap<>();

        data.put(RequestConstants.I_MOMENTS_TYPE, momentsType);
        data.put(RequestConstants.I_SHARE_TO, "1");
        data.put(RequestConstants.I_SECRET, "0");
        data.put("mediaSize", mediaSize + "");
        data.put("pixelHeight", pixelHeight + "");
        data.put("pixelWidth", pixelWidth + "");
        if (!TextUtils.isEmpty(videoUrl)) {
            data.put("videoUrl", videoUrl);
            data.put("playTime", (playTime / 1000) + "");
            data.put("videoColor", videoColor);
            data.put("videoWebp", videoWebp);
        } else {
            data.put(RequestConstants.I_IMAGE_URLS, imageUrlList);
        }
        return DefaultRequestService.createNearbyRequestService().uploadCover(MD5Utils.generateSignatureForMap(data));
    }

    /**
     * 创建直播间
     *
     * @param text
     * @param imageUrl
     * @param atUserList
     * @param audioType
     */
    public Flowable<LiveRoomNetBean> createLiveRoom(String text, String imageUrl, String atUserList, int audioType, String announcement) {
        Map<String, String> data = new HashMap<>();

        data.put(RequestConstants.I_LIVE_TYPE, String.valueOf(audioType));
        data.put(RequestConstants.I_TEXT, text);
        data.put(RequestConstants.I_IMAGE_URL, imageUrl);
        data.put(RequestConstants.I_AT_USER_LIST, atUserList);
        data.put(RequestConstants.I_ANNOUNCEMENT, announcement);
        data.put("appId", BuildConfig.APPLICATION_ID);
        //4.5.0新增参数
        data.put(RequestConstants.I_CITY, ShareFileUtils.getString(ShareFileUtils.CITY, ""));
        return DefaultRequestService.createLiveRequestService().createLiveRoom(MD5Utils.generateSignatureForMap(data));
    }

    /**
     * 创建直播间 //4.8.0多人直播 0不是多人直播，1为多人直播
     *
     * @param text
     * @param imageUrl
     */
    public Flowable<LiveRoomNetBean> createVoiveLiveRoom(String text, String imageUrl, String atUserList, int audioType, int isMulti, String announcement) {
        Map<String, String> data = new HashMap<>();

        data.put(RequestConstants.I_LIVE_TYPE, String.valueOf(audioType));
        data.put(RequestConstants.I_TEXT, text);
        data.put(RequestConstants.I_IMAGE_URL, imageUrl);
        data.put(RequestConstants.I_AT_USER_LIST, atUserList);
        data.put(RequestConstants.I_CITY, ShareFileUtils.getString(ShareFileUtils.CITY, ""));//4.5.0新增参数
        data.put("isMulti", String.valueOf(isMulti));//4.8.0多人直播 0不是多人直播，1为多人直播
        data.put(RequestConstants.I_ANNOUNCEMENT, announcement);
        data.put("appId", BuildConfig.APPLICATION_ID);

        return DefaultRequestService.createLiveRequestService().createLiveRoom(MD5Utils.generateSignatureForMap(data));
    }

    /**
     * 获取自己是否有主播权限
     */
    public Flowable<LivePermitBean> getLivePermit() {
        return DefaultRequestService.createLiveRequestService().getLivePermit(ShareFileUtils.getString(ShareFileUtils.ID, ""));
    }

    /**
     * 获取我的密友请求
     */
    public Flowable<MyCircleFriendListBean> getMyCircleData() {
        return DefaultRequestService.createAllRequestService().getMyCircleData();
    }

    /**
     * 获取我的密友数据
     */
    public Flowable<MyCircleRequestListBean> getMyCircleRequestData(int curPage, int pageSize) {
        return DefaultRequestService.createAllRequestService().getMyCircleRequestData(pageSize + "", curPage + "");
    }

    /**
     * 拒绝密友圈请求
     *
     * @param requestId 请求的id
     * @param uuid      请求所在的列表的位置
     */
    public Flowable<BaseDataBean> refuseRequest(String requestId, String uuid) {
        Map<String, String> map = new HashMap<>();
        map.put(RequestConstants.I_REQUEST_ID, requestId);
        return DefaultRequestService.createAllRequestService().refuseRequest(MD5Utils.generateSignatureForMap(map));
    }

    /**
     * 接受密友圈请求
     *
     * @param requestId 请求的id
     * @param uuid      请求所在的列表的位置
     */
    public Flowable<BaseDataBean> acceptRequest(String requestId, String uuid) {
        Map<String, String> map = new HashMap<>();
        map.put(RequestConstants.I_REQUEST_ID, requestId);
        return DefaultRequestService.createAllRequestService().acceptRequest(MD5Utils.generateSignatureForMap(map));
    }

    /**
     * 我的照片
     *
     * @param pageSize
     * @param curPage
     */
    public Flowable<MyImagesListBean> getMyImagesList(String pageSize, String curPage) {
        return DefaultRequestService.createAllRequestService().getMyImagesList(curPage, pageSize);
    }

    /**
     * 我的匹配照片
     *
     * @param pageSize
     * @param curPage
     */
    public Flowable<MyImagesListBean> getMyMatchImagesList(String entry, String pageSize, String curPage) {
        return DefaultRequestService.createAllRequestService().getMatchMyImagesList(entry, curPage, pageSize);
    }

    /**
     * 移除密友圈朋友
     *
     * @param requestId 请求id
     * @param userId    用户id
     */
    public Flowable<BaseDataBean> removeCircleFriend(String requestId, String userId) {
        HashMap<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_REQUEST_ID, requestId);
        data.put(RequestConstants.I_USERID, userId);

        String body = MD5Utils.generateSignature(data);

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/x-www-form-urlencoded; charset=UTF8"), body);

        return DefaultRequestService.createAllRequestService().removeCircleFriend(MD5Utils.generateSignatureForMap(data));
    }

    /**
     * 取消我发出的密友圈请求
     */
    public Flowable<BaseDataBean> cancelRequest(String requestId) {
        HashMap<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_REQUEST_ID, requestId);
        String body = MD5Utils.generateSignature(data);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/x-www-form-urlencoded; charset=UTF8"), body);
        return DefaultRequestService.createAllRequestService().cancelRequest(MD5Utils.generateSignatureForMap(data));

    }

    /**
     * 我的钱包数据
     *
     * @param
     */
    public Flowable<WalletBean> getWalletData() {
        return DefaultRequestService.createAllRequestService().getWalletData();
    }

    /**
     * 上传相册图片(最终url)
     */
    public Flowable<UpdataInfoBean> uploadImageUrl(String imageUrl, String imageWidth, String imageHeight) {
        HashMap<String, String> data = new HashMap<>();
        data.put("imageUrl", imageUrl);
        data.put("imageWidth", imageWidth);
        data.put("imageHeight", imageHeight);
        String body = MD5Utils.generateSignature(data);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/x-www-form-urlencoded; charset=UTF8"), body);
        return DefaultRequestService.createAllRequestService().uploadImageUrl(MD5Utils.generateSignatureForMap(data));

    }

    /**
     * 上传视频
     *
     * @param imageWidth
     * @param imageHeight
     * @param uploadVideoPath 视频地址
     */
    public Flowable<UploadVideoAlbumBean> uploadAlbumVideoUrl(String imageWidth, String imageHeight, String uploadVideoPath) {
        HashMap<String, String> data = new HashMap<>();
        data.put("imageWidth", imageWidth);
        data.put("imageHeight", imageHeight);
        data.put("videoUrl", uploadVideoPath);
        return DefaultRequestService.createAllRequestService().uploadAlbumVideoUrl(MD5Utils.generateSignatureForMap(data));
    }

    /**
     * 获取上传视频文件到七牛的token
     *
     * @param clientTime 上传时间
     * @param bucket
     * @param path       视频路径
     * @param gifPath    gif路径(可选)
     * @param gifType    相册，album（可选）
     * @param gifKey     相册id((可选)
     * @return
     */
    public Flowable<UploadTokenBean> getVideoUploadToken(String clientTime, String bucket, String path, String gifPath, String gifType, String gifKey) {
        HashMap<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_BUCKET, bucket);
        data.put(RequestConstants.I_CLIENT_TIME, clientTime);
        data.put(RequestConstants.I_PATH, path);
        data.put(RequestConstants.I_GIFPATH, gifPath);
        data.put(RequestConstants.I_GIFTYPE, gifType);
        data.put(RequestConstants.I_GIFKEY, gifKey);
        return DefaultRequestService.createAllRequestService().getVideoUploadToken(MD5Utils.generateSignatureForMap(data));

    }

    /**
     * 删除照片
     *
     * @param picId
     */
    public Flowable<UpdataInfoBean> deleteImage(String picId) {
        HashMap<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_PICID, picId);

        String body = MD5Utils.generateSignature(data);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/x-www-form-urlencoded; charset=UTF8"), body);
        return DefaultRequestService.createAllRequestService().deleteImage(MD5Utils.generateSignatureForMap(data));
    }

    /**
     * 设为封面照片
     *
     * @param picId
     */
    public Flowable<BaseDataBean> setCover(String picId) {
        HashMap<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_PICID, picId);

        String body = MD5Utils.generateSignature(data);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/x-www-form-urlencoded; charset=UTF8"), body);
        return DefaultRequestService.createAllRequestService().setCover(MD5Utils.generateSignatureForMap(data));
    }

    /**
     * 设为隐私照片
     *
     * @param picId
     */
    public Flowable<BaseDataBean> setPrivateStatus(String picId) {
        HashMap<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_PICID, picId);
        return DefaultRequestService.createAllRequestService().setPrivateStatus(MD5Utils.generateSignatureForMap(data));
    }

    /**
     * 3.2.0热门话题列表
     */
    public Flowable<ThemeListBean> getHotThemeList(int curPage) {
        return DefaultRequestService.createAllRequestService().getHotThemeList(String.valueOf(curPage), "20");
    }

    /**
     * 3.2.0获取话题列表
     */
    public Flowable<ThemeListBean> getThemesList(String themeclass, int curpage) {
        return DefaultRequestService.createAllRequestService().getThemesList(String.valueOf(curpage), "20", themeclass);
    }

    /**
     * 4.8.0获取最新话题列表
     */
    public Flowable<ThemeListBean> getNewThemesList(int curpage) {
        return DefaultRequestService.createAllRequestService().getNewThemeList(String.valueOf(curpage), "20");
    }

    /**
     * theL广告
     */
    public Flowable<AdBean> getAd() {
        return DefaultRequestService.createAllRequestService().getAd("android");
    }

    /**
     * 收益或提现记录
     *
     * @param cursor
     * @param limit
     * @param filter
     */
    public Flowable<IncomeRecordListBean> getIncomeOrWithdrawRecord(int cursor, int limit, String filter) {
        HashMap<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_CURSOR, cursor + "");
        data.put(RequestConstants.I_LIMIT, limit + "");
        data.put(RequestConstants.I_FILTER, filter);
        return DefaultRequestService.createAllRequestService().getIncomeOrWithdrawRecord(data);
    }

    /**
     * 获取我的通知消息
     *
     * @param pageSize
     * @param curPage
     */
    public Flowable<MomentsMsgsListBean> getMomentsMsgsList(int pageSize, int curPage) {
        return DefaultRequestService.createAllRequestService().getMomentsMsgsList(pageSize, curPage);
    }

    /**
     * 获取购买vip页面的列表
     */
    public Flowable<VipListBean> getBuyVipList(String channle) {
        return DefaultRequestService.createAllRequestService().getBuyVipList(channle);
    }

    /**
     * 从服务端获取生成支付订单的必要数据
     *
     * @param productType
     * @param productId
     */
    public Flowable<PayOrderBean> createStickerPackOrder(String productType, String productId, String payType, String source) {
        HashMap<String, String> data = new HashMap<>();

        data.put(RequestConstants.I_PRODUCT_TYPE, productType);
        data.put(RequestConstants.I_PRODUCT_ID, productId);
        data.put(RequestConstants.I_PAY_TYPE, payType);
        data.put(RequestConstants.I_PAY_SOURCE, source);
        String body = MD5Utils.generateSignature(data);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/x-www-form-urlencoded; charset=UTF8"), body);
        return DefaultRequestService.createAllRequestService().createStickerPackOrder(MD5Utils.generateSignatureForMap(data));
    }

    /**
     * 世界功能
     *
     * @param lat
     * @param lng
     * @param pageSize
     * @param curPage
     */
    public Flowable<NearUserListNetBean> getWorldUsers(String lat, String lng, String pageSize, String curPage) {
        HashMap<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_PAGESIZE, pageSize);
        data.put(RequestConstants.I_CURPAGE, curPage);
        data.put(RequestConstants.I_WORLD_LAT, lat);
        data.put(RequestConstants.I_WORLD_LNG, lng);

        return DefaultRequestService.createAllRequestService().getWorldUsers(data);
    }

    /**
     * 获取地理名
     */
    public Flowable<LocationNameBean> getLocationName(String lat, String lng, String language) {
        HashMap<String, String> data = new HashMap<>();
        if (!TextUtils.isEmpty(lat) && !TextUtils.isEmpty(lng)) {
            data.put(RequestConstants.I_LOC, lng + "," + lat);
            data.put(RequestConstants.I_LANGUAGE, language);
        }
        return DefaultRequestService.createLocationRequestService(RequestConstants.IMAGE_SERVER_IP).getLocationName(data);
    }

    /**
     * 获取推荐的匹配用户列表
     */
    public Flowable<MatchListBean> getMatchList() {

        return DefaultRequestService.createAllRequestService().getMatchList();
    }

    /**
     * 获取喜欢我的人的用户列表
     */
    public Flowable<MatchListBean> getLikeMeMatchList(String cursor, int limit, String newcursor) {
        HashMap<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_CURSOR, cursor);
        data.put(RequestConstants.I_LIMIT, limit + "");
        data.put(RequestConstants.I_NEW_CURSOR, newcursor);
        return DefaultRequestService.createAllRequestService().getLikeMeMatchList(data);
    }

    /**
     * 喜欢或者不喜欢对方，或取消喜欢对方
     *
     * @param userId
     * @param likeType like unlike cancel
     */
    public Flowable<LikeResultBean> likeOrNot(long userId, String likeType, String isRollback) {
        HashMap<String, String> data = new HashMap<>();
        data.put("likeType", likeType);
        data.put("userId", userId + "");
        if (!TextUtils.isEmpty(isRollback))
            data.put("rollback", isRollback);
        return DefaultRequestService.createAllRequestService().likeOrNot(MD5Utils.generateSignatureForMap(data));
    }

    public Flowable<SuperlikeBean> superlike(String userId, String note) {
        HashMap<String, String> data = new HashMap<>();
        data.put("note", note);
        data.put("userId", userId);
        return DefaultRequestService.createAllRequestService().superlike(MD5Utils.generateSignatureForMap(data));
    }

    public Flowable<SuperlikeListBean> superlikeList() {
        return DefaultRequestService.createAllRequestService().getSuperlikeList();
    }

    /**
     * 获取推荐的匹配用户列表
     */
    public Flowable<BaseDataBean> matchRollback(long userId) {
        HashMap<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_USERID, userId + "");
        String body = MD5Utils.generateSignature(data);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/x-www-form-urlencoded; charset=UTF8"), body);
        return DefaultRequestService.createAllRequestService().matchRollback(MD5Utils.generateSignatureForMap(data));

    }

    /**
     * 获取我的匹配问题
     */
    public Flowable<MatchQuestionBean> getMyQuestions() {
        return DefaultRequestService.createAllRequestService().getMyQuestions();
    }

    /**
     * 设置我的匹配问题
     */
    public Flowable<BaseDataBean> setMyQuestions(String q1, String q2, String q3, int matchAgeBegin, int matchAgeEnd, String matchRole, int sameCity) {
        HashMap<String, String> data = new HashMap<>();
        data.put("question1", q1);
        data.put("question2", q2);
        data.put("question3", q3);
        data.put("matchAgeBegin", matchAgeBegin + "");
        data.put("matchAgeEnd", matchAgeEnd + "");
        data.put("matchRole", matchRole);
        data.put("sameCity", sameCity + "");
        String body = MD5Utils.generateSignature(data);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/x-www-form-urlencoded; charset=UTF8"), body);
        return DefaultRequestService.createAllRequestService().setMyQuestions(MD5Utils.generateSignatureForMap(data));
    }

    /**
     * 获取表情商店首页-广告和推荐表情包
     */
    public Flowable<RecommendedStickerListNetBean> getStickerStoreRecommended() {
        return DefaultRequestService.createAllRequestService().getStickerStoreRecommended("1");
    }

    /**
     * 获取表情商店首页-更多表情包
     *
     * @param cursor
     * @param limit
     */
    public Flowable<StickerPackListNetBean> getStickerStoreMore(int cursor, int limit) {
        HashMap<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_CURSOR, cursor + "");
        data.put(RequestConstants.I_LIMIT, limit + "");
        data.put("vipfree", "1");// 2.17.1版本用来显示会员免费表情
        return DefaultRequestService.createAllRequestService().getStickerStoreMore(data);
    }

    /**
     * google wallet 购买成功后提交服务端验证
     *
     * @param receipt
     * @param iapSignature
     */
    public Flowable<GoogleIapNotifyResultBean> iapNotify(String outTradeNo, String receipt, String iapSignature) {
        HashMap<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_DATA, receipt);
        data.put(RequestConstants.I_IAP_SIGNATURE, iapSignature);
        String body = MD5Utils.generateSignature(data);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/x-www-form-urlencoded; charset=UTF8"), body);
        return DefaultRequestService.createAllRequestService().iapNotify(MD5Utils.generateSignatureForMap(data));
    }

    /**
     * 搜索好友
     *
     * @param keyWord
     * @param curPage
     */
    public Flowable<SearchUsersListBean> searchNickname(String keyWord, String curPage) {
        return DefaultRequestService.createAllRequestService().searchNickname(keyWord, curPage);
    }

    /**
     * 批量关注用户
     *
     * @param receivedIdList 用户id用逗号分隔
     * @param actionType
     */
    public Flowable<ResultBean> followUserBatch(String receivedIdList, String actionType) {
        HashMap<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_RECEIVEDID_LIST, receivedIdList);
        data.put(RequestConstants.I_ACTIONTYPE, actionType);

        String body = MD5Utils.generateSignature(data);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/x-www-form-urlencoded; charset=UTF8"), body);
        return DefaultRequestService.createAllRequestService().followUserBatch(MD5Utils.generateSignatureForMap(data));
    }

    /**
     * 获取人气标签
     */
    public Flowable<TrendingTagsBean> getTrendingTags() {
        return DefaultRequestService.createAllRequestService().getTrendingTags();
    }

    /**
     * 初始化个人信息，只需要填写昵称、thelID和头像
     *
     * @param role
     * @param userName
     * @param avatarURL
     * @param nickName
     */
    public Flowable<UpdataInfoBean> initUserInfo(String role, String userName, String avatarURL, String nickName) {
        HashMap<String, String> data = new HashMap<>();

        data.put(RequestConstants.I_USER_NAME, userName);
        data.put(RequestConstants.I_AVATAR_URL, avatarURL);
        data.put(RequestConstants.I_NICKNAME, nickName);
        data.put(RequestConstants.I_ROLENAME, role);

        return DefaultRequestService.createAllRequestService().initUserInfo(MD5Utils.generateSignatureForMap(data));
    }

    /**
     * 获取表情包详情
     *
     * @param id       表情包id
     * @param download 是否是下载表情，0：不是 1：是
     */
    public Flowable<StickerPackNetBean> getStickerPackDetail(long id, int download) {
        HashMap<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_ID, id + "");
        data.put(RequestConstants.I_DOWNLOAD, download + "");
        return DefaultRequestService.createAllRequestService().getStickerPackDetail(data);
    }

    /**
     * Gold购买／赠送商品（会员）
     *
     * @param productType   商品类型，暂时只支持vip。
     * @param productId     商品id
     * @param friend_userid 赠送给用户（可选）
     * @param say_something 礼物卡片（可选
     */
    public Flowable<BaseDataBean> buyWithGold(String productType, long productId, String friend_userid, String say_something) {
        HashMap<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_PRODUCT_TYPE, productType);
        data.put(RequestConstants.I_PRODUCT_ID, productId + "");
        data.put(RequestConstants.I_SEND_TOLD, friend_userid);//朋友id
        data.put(RequestConstants.I_GIFT_CARD, say_something);//留言
        String body = MD5Utils.generateSignature(data);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/x-www-form-urlencoded; charset=UTF8"), body);
        return DefaultRequestService.createAllRequestService().buyWithGold(MD5Utils.generateSignatureForMap(data));
    }

    /**
     * 退出登录
     */
    public Flowable<BaseDataBean> loadUserLogout() {
        HashMap<String, String> data = new HashMap<>();
        return DefaultRequestService.createAllRequestService().loadUserLogout(MD5Utils.generateSignatureForMap(data));
    }

    /**
     * 修改密码
     *
     * @param oldPassword
     * @param newPassword
     * @return
     */
    public Flowable<BaseDataBean> modifyPassword(String oldPassword, String newPassword) {
        HashMap<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_OLD_PASSWORD, oldPassword);
        data.put(RequestConstants.I_NEW_PASSWORD, newPassword);
        String body = MD5Utils.generateSignature(data);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/x-www-form-urlencoded; charset=UTF8"), body);
        return DefaultRequestService.createAllRequestService().modifyPassword(MD5Utils.generateSignatureForMap(data));
    }

    /**
     * 获取开关列表
     */
    public Flowable<PushSwitchTypeBean> getPushSwitchList() {
        return DefaultRequestService.createAllRequestService().getPushSwitchList();
    }

    /**
     * 获取小红点开关列表
     */
    public Flowable<PushSwitchTypeBean> getRedPointList() {
        return DefaultRequestService.createAllRequestService().getredPointSwitchList();
    }

    /**
     * 我的黑名單列表
     */
    public Flowable<BlockListBean> getUserBlackList(String pageSize, String curPage) {
        HashMap<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_PAGESIZE, pageSize);
        data.put(RequestConstants.I_CURPAGE, curPage);
        return DefaultRequestService.createAllRequestService().getUserBlackList(data);
    }

    /**
     * 获取我已购买过的表情包
     */
    public Flowable<StickerPackListNetBean> getPurchasedStickerPacks() {
        return DefaultRequestService.createAllRequestService().getPurchasedStickerPacks();
    }

    /**
     * 获取屏蔽日志的用户列表
     */
    public Flowable<BlackMomentListBean> getBlockUserListForMoments() {
        return DefaultRequestService.createAllRequestService().getBlockUserListForMoments();
    }

    /**
     * 解除黑名单
     */
    public Flowable<MyBlockBean> removeFromBlackList(String userId) {
        HashMap<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_USERID, userId);
        return DefaultRequestService.createAllRequestService().removeFromBlackList(MD5Utils.generateSignatureForMap(data));
    }

    /**
     * 屏蔽某人的日志(从屏蔽黑名单中移除)
     *
     * @param userId
     */
    public Flowable<BaseDataBean> cancelBlockUserMoments(String userId) {
        HashMap<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_USERID, userId);
        String body = MD5Utils.generateSignature(data);
        return DefaultRequestService.createAllRequestService().cancelBlockUserMoments(MD5Utils.generateSignatureForMap(data));
    }

    /**
     * 推送开关
     *
     * @param switchType:开关类型
     * @param live:是否打开直播推送            （0表示不推送，1表示推送）
     * @param commentText:是否打开新日志评论推送  （0表示不推送，1表示推送）
     * @param commentReply:是否打开新评论回复推送 （0表示不推送，1表示推送）
     * @param commentWinkPush:日志点赞     （0表示不推送，1表示推送）
     **/
    public Flowable<BaseDataBean> pushConfig(String switchType, int live, int commentText, int commentReply, int view, int commentWinkPush, int follower, int wink, int message, int video, int theme, int likeMe, int superLike, int perfectMatch, int await) {
        HashMap<String, String> data = new HashMap<>();
        data.put("live", live + "");
        data.put("commentText", commentText + "");
        data.put("commentReply", commentReply + "");
        data.put("view", view + "");
        data.put("commentWink", commentWinkPush + "");
        data.put("follower", follower + "");
        data.put("wink", wink + "");
        data.put("message", message + "");
        data.put("video", video + "");
        data.put("theme", theme + "");
        data.put("likeMe", likeMe + "");
        data.put("superLike", superLike + "");
        data.put("perfectMatch", perfectMatch + "");
        data.put("await", await + "");
        String body = MD5Utils.generateSignature(data);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/x-www-form-urlencoded; charset=UTF8"), body);
        return DefaultRequestService.createAllRequestService().pushConfig(MD5Utils.generateSignatureForMap(data));
    }

    /**
     * 小红点开关
     */
    public Flowable<PushSwitchTypeBean> pushRedPointConfig(int viewMeRedpoint) {
        HashMap<String, String> data = new HashMap<>();
        data.put("viewMeRedpoint", viewMeRedpoint + "");
        return DefaultRequestService.createAllRequestService().pushRedPointConfig(MD5Utils.generateSignatureForMap(data));
    }


    /**
     * 收藏列表
     *
     * @param limit  每页个数
     * @param cursor 第几页，0开始
     */
    public Flowable<FavoritesBean> getFavoriteList(int limit, int cursor) {
        return DefaultRequestService.createAllRequestService().getFavoriteList(String.valueOf(limit), String.valueOf(cursor));
    }

    /**
     * 删除日志
     *
     * @param favoriteId   收藏的Id
     * @param momentsId    日志id
     * @param favoriteType 收藏类型
     */
    public Flowable<BaseDataBean> deleteFavoriteMoment(String favoriteId, String momentsId, String favoriteType) {

        Map<String, String> map = new HashMap();
        map.put(RequestConstants.FAVORITE_ID, favoriteId);
        map.put(RequestConstants.FAVORITE_COUNT, momentsId);
        map.put(RequestConstants.FAVORITE_TYPE, favoriteType);

        return DefaultRequestService.createAllRequestService().deleteFavoriteMoment(MD5Utils.generateSignatureForMap(map));
    }

    /**
     * 充值列表，
     * 获取商品列表
     * 以及当前余额
     */
    public Flowable<SoftMoneyListBean> getGoldListData(String channel) {
        return DefaultRequestService.createAllRequestService().getGoldListData(channel);
    }

    /**
     * 软妹豆充值和消费记录
     *
     * @param cursor
     * @param limit
     * @param filter
     */
    public Flowable<RechargeRecordListBeanNew> getRechargeRecord(int cursor, int limit, String filter) {
        return DefaultRequestService.createAllRequestService().getRechargeRecord(String.valueOf(cursor), String.valueOf(limit), filter);
    }

    /**
     * 传情
     *
     * @param userId 4.1.0修改为新的接口
     */
    public Flowable<BaseDataBean> sendWink(String userId, String type) {
        HashMap<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_USERID, userId);
        data.put(RequestConstants.I_TYPE, type);
        return DefaultRequestService.createAllRequestService().sendWink(MD5Utils.generateSignatureForMap(data));
    }

    /**
     * 发送一条密友请求
     *
     * @param requestType     用户请求类型（0表示情侣绑定，1表示闺蜜绑定）
     * @param receivedId      接收请求的用户id
     * @param anniversaryDate 纪念日日期（yyyy-MM-dd），情侣绑定时需要，闺蜜绑定时可以不传或者传空值
     * @param requestComent   发送请求时的捎带的一句话
     */
    public Flowable<MyCircleRequestListBean> sendCircleRequest(String requestType, String receivedId, String anniversaryDate, String requestComent) {
        HashMap<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_REQUEST_TYPE, requestType);
        data.put(RequestConstants.I_RECEIVEDID, receivedId);
        data.put(RequestConstants.I_ANNIVERSARY_DATE, anniversaryDate);
        data.put(RequestConstants.I_REQUEST_COMMENT, requestComent);
        return DefaultRequestService.createAllRequestService().sendCircleRequest(MD5Utils.generateSignatureForMap(data));
    }

    /**
     * 搜索我的朋友
     */
    public Flowable<FriendsListBean> searchMyFriends(int type, String keyword, String curPage, String pageSize) {
        HashMap<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_KEY_WORD, keyword);
        data.put(RequestConstants.I_CURPAGE, curPage);
        data.put(RequestConstants.I_PAGESIZE, pageSize);
        data.put(RequestConstants.I_TYPE, type + "");
        return DefaultRequestService.createAllRequestService().searchMyFriends(data);
    }

    /***
     * 获取直播排行榜
     * */
    public Flowable<LivePopularityNetBean> getLiveRank() {
        return DefaultRequestService.createLiveRequestService().getLiveRank();
    }

    /***
     * 获取直播类型列表
     * */
    public Flowable<LiveTypeNetBean> getLiveTypeList() {
        return DefaultRequestService.createLiveRequestService().getLiveTypeList();
    }

    /**
     * 获取附近直播
     */
    public Flowable<LiveRoomsNetBean> getNeighbours(String cursor, String limit) {
        return DefaultRequestService.createLiveRequestService().getNeighbours(cursor, limit);
    }

    /**
     * 推荐关注主播类型
     **/
    public Flowable<LiveRecommendNetBean> getRecommendLive(String cursor, String limit, String typeId) {
        return DefaultRequestService.createLiveRequestService().getRecommendFollowLive(cursor, limit, typeId);
    }

    /**
     * 推荐关注新人列表
     */
    public Flowable<LiveRecommendNetBean> getRecommendNewAnchors(String cursor, String limit) {
        return DefaultRequestService.createLiveRequestService().getRecommendNewAnchors(cursor, limit);

    }

    /**
     * 按类型获取直播
     */
    public Flowable<LiveRoomsNetBean> getTypeLive(String typeId, String cursor, String limit) {
        return DefaultRequestService.createLiveRequestService().getTypeLive(typeId, cursor, limit);
    }

    /**
     * 获取正在直播的新人列表
     */
    public Flowable<LiveRoomsNetBean> getNewsLiveList(String cursor, String limit) {
        return DefaultRequestService.createLiveRequestService().getNewsLiveList(cursor, limit);
    }

    /**
     * 3.2.0
     * 举报用户头像或者图片
     */
    public Flowable<BaseDataBean> postReportImageOrUser(String userId, String report_type, int imgId) {
        HashMap<String, String> data = new HashMap<>();
        data.put(RequestConstants.REPORT_LIVEUSER_RECEIVED_ID, userId);
        data.put(RequestConstants.REPORT_LIVEUSER_REPORT_TYPE, report_type + "");
        data.put(RequestConstants.REPORT_LIVEUSER_REPORT_IMGID, imgId + "");
        return DefaultRequestService.createAllRequestService().postReportImageOrUser(MD5Utils.generateSignatureForMap(data));
    }

    /**
     * 推荐用户名片到日志
     *
     * @param cardUserId
     * @param recommendReason
     * @return
     */
    public Flowable<ReleaseMomentSucceedBean> releaseUserCardMoment(String cardUserId, String recommendReason) {
        HashMap<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_MOMENTS_TYPE, MomentsBean.MOMENT_TYPE_USER_CARD);
        data.put(MomentsDataBaseAdapter.F_CARD_USER_ID, cardUserId);
        data.put(RequestConstants.I_MOMENT_TEXT, recommendReason);
        data.put(RequestConstants.I_SHARE_TO, MomentsBean.SHARE_TO_ALL + "");
        return DefaultRequestService.createAllRequestService().releaseUserCardMoment(MD5Utils.generateSignatureForMap(data));
    }

    /**
     * 2.22.0
     * 举报用户（观众或者主播）
     */
    public Flowable<BaseDataBean> postReportLiveShowUser(String userId, String reason_type, String reason_content, String imageUrls, String report_type, String txt_word, String roomId) {
        HashMap<String, String> data = new HashMap<>();
        data.put(RequestConstants.REPORT_LIVESHOW_RECEIVED_ID, userId);
        data.put(RequestConstants.REPORT_LIVESHOW_REASON_TYPE, reason_type + "");
        data.put(RequestConstants.REPORT_LIVESHOW_REASON_CONTENT, reason_content);
        data.put(RequestConstants.REPORT_LIVESHOW_IMAGE_URL, imageUrls);
        data.put(RequestConstants.REPORT_LIVESHOW_REPORT_TYPE, report_type + "");
        data.put(RequestConstants.REPORT_LIVESHOW_REPORT_CONTENT_WORD, txt_word);
        data.put("roomId", roomId);
        return DefaultRequestService.createAllRequestService().postReportLiveShowUser(MD5Utils.generateSignatureForMap(data));

    }

    /**
     * 4.7.2
     * 举报用户
     */
    public Flowable<BaseDataBean> postReportUser(String userId, String reason_type, String reason_content, String imageUrls, String report_type, String txt_word) {
        HashMap<String, String> data = new HashMap<>();
        data.put(RequestConstants.REPORT_LIVESHOW_RECEIVED_ID, userId);
        data.put(RequestConstants.REPORT_LIVESHOW_REASON_TYPE, reason_type + "");
        data.put(RequestConstants.REPORT_LIVESHOW_REASON_CONTENT, reason_content);
        data.put(RequestConstants.REPORT_LIVESHOW_IMAGE_URL, imageUrls);
        data.put(RequestConstants.REPORT_LIVESHOW_REPORT_TYPE, report_type + "");
        data.put(RequestConstants.REPORT_LIVESHOW_REPORT_CONTENT_WORD, txt_word);
        return DefaultRequestService.createAllRequestService().postReportLiveShowUser(MD5Utils.generateSignatureForMap(data));

    }

    /**
     * 获取好友，朋友列表
     *
     * @param type
     */
    public Flowable<FriendListBean> getUsersRelation(String type) {
        return DefaultRequestService.createAllRequestService().getUsersRelation(type);
    }

    /**
     * 上传设备id，主要是个推的id
     */
    public Flowable<BaseDataBean> uploadDeviceToken(String getuiId, String registrationID) {
        HashMap<String, String> data = new HashMap<>();
        data.put("type", "android");
        data.put("token", getuiId);
        data.put("registrationID", registrationID);
        return DefaultRequestService.createAllRequestService().uploadDeviceToken(MD5Utils.generateSignatureForMap(data));
    }

    public Flowable<LiveClassificationBean> getLiveType() {
        return DefaultRequestService.createAllRequestService().getLiveType();
    }

    /**
     * 检查是否有更新
     */
    public Flowable<VersionBean> checkUpdate() {
        HashMap<String, String> data = new HashMap<>();
        data.put("myVersion", DeviceUtils.getVersionName(TheLApp.getContext()));
        return DefaultRequestService.createAllRequestService().checkUpdate(data);
    }

    /**
     * 上传视频统计数据
     */
    public Flowable<BaseDataBean> submitVideoReport(String reportJson) {
        HashMap<String, String> data = new HashMap<>();
        data.put("reportJson", reportJson);
        return DefaultRequestService.createAllRequestService().submitVideoReport(MD5Utils.generateSignatureForMap(data));
    }

    /**
     * 视频播放次数上报 以废弃
     */
    @Deprecated
    public Flowable<BaseDataBean> postVideoPlayCountUpload(String momentId, int... count) {
        HashMap<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_ID, momentId);
        if (count.length > 0) {
            data.put(RequestConstants.VIDEO_PLAY_COUNT, count[0] + "");
        } else {
            data.put(RequestConstants.VIDEO_PLAY_COUNT, 1 + "");
        }
        return DefaultRequestService.createAllRequestService().postVideoPlayCountUpload(MD5Utils.generateSignatureForMap(data));
    }

    public Flowable<MusicListBean> getByCategory(int categoryId, int pageIndex) {
        HashMap<String, String> data = new HashMap<>();
        data.put("categoryId", String.valueOf(categoryId));
        data.put("pageIndex", String.valueOf(pageIndex));
        return DefaultRequestService.createAllRequestService().getByCategory(data);
    }

    public Flowable<MusicListBean> getHot(int pageIndex) {
        HashMap<String, String> data = new HashMap<>();
        data.put("pageIndex", String.valueOf(pageIndex));
        return DefaultRequestService.createAllRequestService().getHot(data);
    }

    public Flowable<MusicListBean> getNewest(int pageIndex) {
        HashMap<String, String> data = new HashMap<>();
        data.put("pageIndex", String.valueOf(pageIndex));
        return DefaultRequestService.createAllRequestService().getNewest(data);
    }

    public Flowable<BaseDataBean> postADIndifferent(String momentId) {
        HashMap<String, String> data = new HashMap<>();
        data.put("id", String.valueOf(momentId));
        return DefaultRequestService.createMomentRequestService().indifferent(data);
    }

    /**
     * 视频播放详情上报
     *
     * @param entry
     * @param momentId
     * @param count
     * @param playTime
     * @return
     */
    public Flowable<BaseDataBean> postPlayVideoInfo(String entry, String momentId, String count, String playTime) {
        HashMap<String, String> data = new HashMap<>();
        data.put("entry", entry);
        data.put("id", momentId);
        data.put("count", count);
        data.put("playTime", playTime);
        return DefaultRequestService.createAllRequestService().postPlayVideoInfo(MD5Utils.generateSignatureForMap(data));
    }

    /**
     * 我的等级页面设置
     *
     * @return
     */
    public Flowable<UserLevelSwitchBean> postLiveSetting(String entrySwitch, String levelIconSwitch, String isCloaking) {
        HashMap<String, String> data = new HashMap<>();
        data.put("entrySwitch", entrySwitch);
        data.put("levelIconSwitch", levelIconSwitch);
        data.put("isCloaking", isCloaking);
        return DefaultRequestService.createAllRequestService().postLevelSetting(MD5Utils.generateSignatureForMap(data));
    }

    /**
     * 移除粉丝
     *
     * @return
     */
    public Flowable<BaseDataBean> postRemoveFans(String userId) {
        HashMap<String, String> data = new HashMap<>();
        data.put("userId", userId);
        return DefaultRequestService.createAllRequestService().postRemoveFans(MD5Utils.generateSignatureForMap(data));
    }

    /**
     * 获取芝麻认证的url地址
     **/
    public Flowable<ZmxyAuthBean> postZmxyAuth(String name, String certNo, String callback) {
        HashMap<String, String> data = new HashMap<>();
        data.put("name", name);
        data.put("certNo", certNo);
        data.put("callback", callback);
        return DefaultRequestService.createAllRequestService().postAuth(MD5Utils.generateSignatureForMap(data));
    }

    /**
     * 验证芝麻信用回调数据
     **/
    public Flowable<ZmxyCheckCallBackBean> postCheckCallback(String sign, String params) {
        HashMap<String, String> data = new HashMap<>();
        data.put("sign", sign);
        data.put("params", params);
        return DefaultRequestService.createAllRequestService().postCheckCallBack(MD5Utils.generateSignatureForMap(data));
    }
}
