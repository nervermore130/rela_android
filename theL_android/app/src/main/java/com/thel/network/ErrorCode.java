package com.thel.network;

import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseDataBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.modules.main.MainActivity;
import com.thel.modules.main.settings.SettingActivity;
import com.thel.ui.dialog.ConfirmDialogActivity;
import com.thel.utils.BusinessUtils;
import com.thel.utils.DeviceUtils;
import com.thel.utils.DialogUtil;
import com.thel.utils.L;
import com.thel.utils.AppUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.ToastUtils;

import me.rela.rf_s_bridge.RfSBridgePlugin;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by liuyun on 2017/9/25.
 */

public class ErrorCode {

    private static ErrorCode INSTANCE;

    public static ErrorCode getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ErrorCode();
        }
        return INSTANCE;
    }

    public boolean handlerErrorDataCallBack(BaseDataBean baseDataBean) {
        try {
            if (baseDataBean == null) {
                return true;
            }

            L.d("ErrorCode", " baseDataBean.errcode : " + baseDataBean.errcode);

            String key = ShareFileUtils.getString(ShareFileUtils.KEY, "");

            if (TextUtils.isEmpty(key)) {
                return false;
            }

            L.d("ErrorCode", " key : " + key);

            if (baseDataBean.errcode.equals("2") || baseDataBean.errcode.equals("require_login")) {

                ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.info_login_other));
                L.d("ErrorCode", TheLApp.getContext().getString(R.string.info_login_other));
                BusinessUtils.logoutAndClearUserData();
                removeNotification();

                RfSBridgePlugin.getInstance().activeRpc("EventLogout", "", null);

                Intent intent = new Intent(TheLApp.context, MainActivity.class);
                TheLApp.context.startActivity(intent);

//            } else if (baseDataBean.errcode.equals("0")) {
                ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.info_system_error));
            } else if (baseDataBean.errcode.equals("1")) {
                ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.info_email_registered));
            } else if (baseDataBean.errcode.equals("3")) {
                ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.info_email_no_register));
            } else if (baseDataBean.errcode.equals("no_such_user")) {
                ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.info_email_no_register));
            } else if (baseDataBean.errcode.equals("4")) {
                ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.info_password_error));
            } else if (baseDataBean.errcode.equals("bad_password")) {
                ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.info_password_error));
            } else if (baseDataBean.errcode.equals("invalid_sms_code")) {
                ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.info_code_error));
            } else if (baseDataBean.errcode.equals("6")) {
                // ToastUtils.showToastShort(TheLApp.getContext(),
                // TheLApp.getContext().getString(R.string.info_blacklist_empty),
                // );
           /* } else if (baseDataBean.errcode.equals("8")) {
                ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.info_had_add_to_black));*/
            } else if (baseDataBean.errcode.equals("11")) {
                ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.info_wink_once));
            } else if (baseDataBean.errcode.equals("13")) {
                ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.info_same_password));
            } else if (baseDataBean.errcode.equals("14")) {
                ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.info_old_password_wrong));
            } else if (baseDataBean.errcode.equals("15")) {
                ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.info_enter_reason));
            } else if (baseDataBean.errcode.equals("18")) {
                ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.info_not_find));
           /* } else if (baseDataBean.errcode.equals("20")) {
                ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.webview_activity_already_followed));*/
           /* } else if (baseDataBean.errcode.equals("22")) {
                ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.info_only_nine_allowed));*/
            } else if (baseDataBean.errcode.equals("23")) {
                ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.info_upload_cover_failed));
            } else if (baseDataBean.errcode.equals("24")) {
                //            ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.info_upload_photo_first));
            /*} else if (baseDataBean.errcode.equals("27")) {
                ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.info_public_photo));*/
            } else if (baseDataBean.errcode.equals("29")) {
                ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.info_has_send_key));
            } else if (baseDataBean.errcode.equals("30")) {
                // ToastUtils.showToastShort(
                // TheLApp.getContext(),
                // TheLApp.getContext()
                // .getString(R.string.info_key_list_empty),
                // );
            } else if (baseDataBean.errcode.equals("36")) {
                ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.info_account_pause));
            } else if (baseDataBean.errcode.equals("63")) {// 发重复的日志
                ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.info_repeat_moment));
            } else if (baseDataBean.errcode.equals("86")) {// 重复举报朋友圈
                ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.moments_repeat_report_moment));
           /* } else if (baseDataBean.errcode.equals("80")) {// 重复屏蔽用户日志
                //            ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.my_block_user_moments_activity_block_repeat));*/
           /* } else if (baseDataBean.errcode.equals("82")) {// 重复举报朋友圈评论
                ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.moments_repeat_report_comment));*/
          /*  } else if (baseDataBean.errcode.equals("87")) {// 日志已经被删除
                ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.moment_comments_error));*/
            } else if (baseDataBean.errcode.equals("165")) {// 不能把封面图片设为隐私
                ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.uploadimage_activity_cannot_set_cover_private));
          /*  } else if (baseDataBean.errcode.equals("301")) {// 不能把隐私图片设为封面
                ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.uploadimage_activity_cannot_set_private_cover));*/
            } else if (baseDataBean.errcode.equals("302")) {// 服务器维护
                if (!TheLConstants.isServerMaintaining && AppUtils.isAppOnForeground(TheLApp.context)) {
                    TheLConstants.isServerMaintaining = true;
                    try {
                        //                    JSONObject jsonObject = new JSONObject(rcb.responseDataStr);
                        //                    String url = JsonUtils.getString(jsonObject, "location", "");
                        //                    Intent intent = new Intent(TheLApp.context, WebViewActivity.class);
                        //                    intent.putExtra(WebViewActivity.URL, url);
                        //                    intent.putExtra(WebViewActivity.NEED_SECURITY_CHECK, false);
                        //                    TheLApp.context.startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    ToastUtils.showToastShort(TheLApp.getContext(), baseDataBean.errdesc);
                }
            } else if (baseDataBean.errcode.equals("325")) {// 上传文件时文件为空
                ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.info_wrong));
            } else if (baseDataBean.errcode.equals("408")) {// 重复举报用户
                ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.info_repeat_report_user));
           /* } else if (baseDataBean.errcode.equals("427")) {// 重复举报日志
                ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.moments_repeat_report_moment));*/
            } else if (baseDataBean.errcode.equals("997")) {// 账号禁用

                Intent intent = new Intent(TheLApp.context, ConfirmDialogActivity.class);

                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                Bundle bundle = new Bundle();

                bundle.putString(TheLConstants.BUNDLE_DIALOG_MESSAGE, TheLApp.context.getString(R.string.forgetpassword_error_account_disabled));

                intent.putExtras(bundle);

                TheLApp.context.startActivity(intent);

            } else if (baseDataBean.errcode.equals("10002")) {// 设备禁用

                Intent intent = new Intent(TheLApp.context, ConfirmDialogActivity.class);

                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                Bundle bundle = new Bundle();

                bundle.putString(TheLConstants.BUNDLE_DIALOG_MESSAGE, TheLApp.context.getString(R.string.forgetpassword_error_device_disabled));

                intent.putExtras(bundle);

                TheLApp.context.startActivity(intent);

            } else if (baseDataBean.errcode.equals("998")) {
                ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.forgetpassword_error_account_not_registered));
            } else if (baseDataBean.errcode.equals("999")) {
                if ("30分钟后再来!".equals(baseDataBean.errdesc)) {
                    ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.forgetpassword_activity_tip));
                }
           /* } else if (baseDataBean.errcode.equals("1000")) {
                ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.register_activity_wrong_email));*/
           /* } else if (baseDataBean.errcode.equals("1001")) {
                //            AgeLimitedDiscCache.putAgeCache(this, RequestConstants.GET_MAIN_ADVERT, "");
                if (!TextUtils.isEmpty(baseDataBean.errdesc) && "邮箱无效,收不到邮件!".equals(baseDataBean.errdesc)) {
                    ToastUtils.showToastShort(TheLApp.getContext(), baseDataBean.errdesc);
                }*/
           /* } else if (baseDataBean.errcode.equals("10003")) {
                ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.updatauserinfo_activity_username_exists));*/
          /*  } else if (baseDataBean.errcode.equals("772")) {// 你们不是好友，不能发送请求
                ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.send_circle_request_act_err772));*/
           /* } else if (baseDataBean.errcode.equals("789")) {// 对方已经给你发过密友请求了
                ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.send_circle_request_act_err789));*/
            /*} else if (baseDataBean.errcode.equals("770")) {// 最多只能绑定6位闺蜜
                ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.send_circle_request_act_err770));*/
           /* } else if (baseDataBean.errcode.equals("780")) {// 对方已经绑定情侣了
                ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.send_circle_request_act_err780));*/
            } else if (baseDataBean.errcode.equals("already_bind_by_others")) {
                ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.setting_activity_cell_already_binded));
            } else if (baseDataBean.errcode.equals("unbind_not_allowed")) {
                ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.setting_activity_err_bind));
            } else if (baseDataBean.errcode.equals("live_text_dumplicated")) {
                ToastUtils.showToastShort(TheLApp.getContext(), baseDataBean.errdesc);
            } else if (baseDataBean.errcode.equals("app_latest_version_error")) {
                //            ToastUtils.showToastShort(TheLApp.getContext(), baseDataBean.errdesc);
            } else if (baseDataBean.errcode.equals("97")) {

            } else if (baseDataBean.errcode.equals("87")) {

            } else if (baseDataBean.errcode.equals("10103")) {
                ToastUtils.showToastShort(TheLApp.getContext(), baseDataBean.errdesc);
            } else if (baseDataBean.errcode.equals("has_binded_by_others")) {
                ToastUtils.showToastShort(TheLApp.getContext(), baseDataBean.errdesc);
            } else if (baseDataBean.errcode.equals("login_spam_user")) {
                ToastUtils.showToastShort(TheLApp.getContext(), baseDataBean.errdesc);
            } else {
                return handleErrorMessage(baseDataBean);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    /**
     * 自定义错误码返回
     *
     * @param baseDataBean
     * @return
     */
    private boolean handleErrorMessage(BaseDataBean baseDataBean) {
        try {
            if (baseDataBean == null) {
                return true;
            }
            if (TextUtils.isEmpty(baseDataBean.errcode) || "0".equals(baseDataBean.errcode)) {
                return false;
            }

            boolean isChinese = DeviceUtils.getLanguage().contains("zh_CN");

            if (isChinese && !TextUtils.isEmpty(baseDataBean.errdesc)) {
                DialogUtil.showToastShort(TheLApp.getContext(), baseDataBean.errdesc);
            } else if (!TextUtils.isEmpty(baseDataBean.errdesc_en)) {
                DialogUtil.showToastShort(TheLApp.getContext(), baseDataBean.errdesc_en);
            } else if (!TextUtils.isEmpty(baseDataBean.errdesc)) {
                DialogUtil.showToastShort(TheLApp.getContext(), baseDataBean.errdesc);
            } else {
                DialogUtil.showToastShort(TheLApp.getContext(), baseDataBean.errdesc);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        return true;
    }

    /**
     * 通知栏上的消息消失
     */
    private void removeNotification() {
        NotificationManager mgr = (NotificationManager) TheLApp.context.getSystemService(NOTIFICATION_SERVICE);
        mgr.cancelAll();
    }

}
