package com.thel.network;

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.thel.base.BaseDataBean;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;

import java.io.IOException;

import io.reactivex.functions.Consumer;

public class InterceptorThrowableConsumer implements Consumer<Throwable> {

    protected boolean hasErrorCode = false;

    protected BaseDataBean errDataBean = null;

    @Override public void accept(Throwable throwable) throws Exception {
        try {
            if (throwable instanceof HttpException) {
                HttpException httpException = (HttpException) throwable;

                String body = httpException.response().errorBody().string();
                errDataBean = GsonUtils.getObject(body, BaseDataBean.class);
                if (errDataBean == null) {
                    L.d("InterceptorSubscribe", "baseDataBean gson null");
                } else {
                    ErrorCode.getInstance().handlerErrorDataCallBack(errDataBean);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
