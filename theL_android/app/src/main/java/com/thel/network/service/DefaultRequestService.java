package com.thel.network.service;

import android.net.Uri;
import android.text.TextUtils;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.thel.BuildConfig;
import com.thel.app.TheLApp;
import com.thel.network.RequestConstants;
import com.thel.network.api.AllApi;
import com.thel.network.api.ChatApi;
import com.thel.network.api.DiscoveryApi;
import com.thel.network.api.LiveApi;
import com.thel.network.api.MomentApi;
import com.thel.network.api.MsgApi;
import com.thel.network.api.ReportApi;
import com.thel.network.api.TestReportApi;
import com.thel.network.api.UserApi;
import com.thel.network.api.WebViewApi;
import com.thel.network.api.commonapi.CommonApi;
import com.thel.network.api.loginapi.api.LoginApi;
import com.thel.network.api.nearbyapi.NearbyApi;
import com.thel.utils.DeviceUtils;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;
import com.thel.utils.MD5Utils;
import com.thel.utils.PhoneUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.UrlConstants;
import com.thel.utils.UserUtils;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSink;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * @author liuyun
 * @date 2017/9/13
 */

public class DefaultRequestService {

    public static final String TAG = "DefaultRequestService";

    private final static String POST = "POST";

    private final static String GET = "GET";

    private final static int CONNECT_TIME_OUT = 15 * 1000;

    private static Retrofit.Builder mBuilder;

    private static Retrofit.Builder mStringBuilder;

    private static Retrofit.Builder mTestReportBuilder;

    private static Retrofit.Builder mReportBuilder;

    private static OkHttpClient mClient;

    private static OkHttpClient mReportClient;

    private final static String HEADER_CONTENT_TYPE = "Content-Type";

    private static final String DEFAULT_PARAMS_ENCODING = "UTF-8";

    static {

        String url = UrlConstants.BASE_URL;

        mBuilder = new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(GsonUtils.getGson()))
                .baseUrl(url);

        mStringBuilder = new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create()).
                        addConverterFactory(ScalarsConverterFactory.create()).
                        baseUrl(url);

        mReportBuilder = new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .baseUrl(UrlConstants.UPLOAD_LOG_URL);

        mTestReportBuilder = new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .baseUrl(UrlConstants.MATCH_REPORTS_LOGS);

        mReportClient = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request request = chain.request()
                                .newBuilder()
                                .header(HEADER_CONTENT_TYPE, getBodyContentType())
                                .header("User-Agent", MD5Utils.getUserAgent())
                                .header("Connection", "close")
                                .build();

                        return chain.proceed(request);
                    }
                })
                .retryOnConnectionFailure(true)
                .connectTimeout(CONNECT_TIME_OUT, TimeUnit.MILLISECONDS)
                .readTimeout(CONNECT_TIME_OUT, TimeUnit.MILLISECONDS)
                .writeTimeout(CONNECT_TIME_OUT, TimeUnit.MILLISECONDS)
                .build();

        //设置代理
        Proxy proxy = Proxy.NO_PROXY;
        String proxyStr = ShareFileUtils.getString(ShareFileUtils.FLUTTER_PROXY, "");
        String[] strings = proxyStr.split(":");
        if (BuildConfig.DEBUG && ShareFileUtils.getBoolean(ShareFileUtils.FLUTTER_PROXY_OPEN, false) && !proxyStr.isEmpty() && strings.length > 1) {
            proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(strings[0], Integer.parseInt(strings[1])));
        }
        mClient = new OkHttpClient.
                Builder().
                retryOnConnectionFailure(true).
                addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {

                        Request request = chain.request();

                        Response response;

                        if (!request.method().equals(POST)) {

                            HttpUrl.Builder urlBuilder = request.url().newBuilder();

                            String latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");

                            if (TextUtils.isEmpty(latitude) || latitude.length() >= 32) {
                                latitude = "0.0";
                            }

                            String longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");

                            if (TextUtils.isEmpty(longitude) || longitude.length() >= 32) {
                                longitude = "0.0";
                            }

                            urlBuilder.addQueryParameter(RequestConstants.I_KEY, UserUtils.getUserKey());
                            urlBuilder.addQueryParameter(RequestConstants.I_DEBUG, "0");
                            urlBuilder.addQueryParameter(RequestConstants.I_LAT, latitude);
                            urlBuilder.addQueryParameter(RequestConstants.I_LNG, longitude);
                            urlBuilder.addQueryParameter(RequestConstants.I_MOBILE_OS, "Android " + PhoneUtils.getOSVersion());
                            urlBuilder.addQueryParameter(RequestConstants.I_CLIENT_VERSION, DeviceUtils.getVersionCode(TheLApp.getContext()) + "");
                            urlBuilder.addQueryParameter(RequestConstants.I_LANGUAGE, DeviceUtils.getLanguageStr());
                            urlBuilder.addQueryParameter(RequestConstants.I_DEVICE_ID, ShareFileUtils.getString(ShareFileUtils.DEVICE_ID, UUID.randomUUID().toString()));

                            if (ShareFileUtils.isGlobalVersion()) {
                                urlBuilder.addQueryParameter(RequestConstants.I_APP_TYPE, "global");
                            }

                            HttpUrl httpUrl = urlBuilder.build();

                            String params = urlBuilder.build().url().getQuery();

                            String decodeParams = Uri.decode(params);

                            L.subLog("DefaultRequestService", " decodeParams :" + decodeParams);

                            String signature = MD5Utils.generateSignature(decodeParams);

                            L.subLog("DefaultRequestService", " params signature :" + signature);

                            Request newRequest = request.newBuilder().url(httpUrl).build();

                            L.d(TAG, " httpUrl.url :" + newRequest.url());

                            response = chain.proceed(getRequest(newRequest, newRequest.url(), signature));

                        } else {

                            BufferedSink bufferedSink = new Buffer();

                            String signature = "";

                            if (request.body() != null) {

                                L.d(TAG, " httpUrl.url :" + request.url());

                                request.body().writeTo(bufferedSink);

                                String body = bufferedSink.buffer().readUtf8();

                                String decodeParams = Uri.decode(body);

                                L.subLog(TAG, " RequestPost decodeParams :" + decodeParams);

                                signature = MD5Utils.generateSignature(decodeParams);

                            }

                            Request newRequest = getRequest(request, request.url(), signature);

                            response = chain.proceed(newRequest);

                        }

                        String body = response.body().string();

                        L.subLog(TAG, " body :" + body);

                        return response.newBuilder().body(ResponseBody.create(MediaType.parse("application/json; charset=utf-8"), body)).build();
                    }
                })//设置超时
                .connectTimeout(CONNECT_TIME_OUT, TimeUnit.MILLISECONDS)
                .readTimeout(CONNECT_TIME_OUT, TimeUnit.MILLISECONDS)
                .writeTimeout(CONNECT_TIME_OUT, TimeUnit.MILLISECONDS)
                .proxy(proxy)
                .build();

    }

    private static Request getRequest(Request request, HttpUrl httpUrl, String signature) {
        return request.newBuilder()
                .header(HEADER_CONTENT_TYPE, getBodyContentType())
                .header("User-Agent", MD5Utils.getUserAgent())
                .header("Host", ShareFileUtils.getString(ShareFileUtils.HTTP_HOST, UrlConstants.HTTP_HOST))
                .header("Connection", "close")
                .header(RequestConstants.I_SIGNATURE, signature == null ? "" : signature)
                .url(httpUrl).build();
    }

    private DefaultRequestService() {

    }

    public static UserApi createUserRequestService() {
        return mBuilder.client(mClient).build().create(UserApi.class);
    }

    public static LoginApi createLoginRequestService() {
        return mBuilder.client(mClient).build().create(LoginApi.class);
    }

    public static AllApi createAllRequestService() {
        return mBuilder.client(mClient).build().create(AllApi.class);
    }

    public static MsgApi createMsgRequestService() {
        return mBuilder.client(mClient).build().create(MsgApi.class);
    }

    public static NearbyApi createNearbyRequestService() {
        return mBuilder.client(mClient).build().create(NearbyApi.class);
    }

    public static LiveApi createLiveRequestService() {
        return mBuilder.client(mClient).build().create(LiveApi.class);
    }

    public static MomentApi createMomentRequestService() {
        return mBuilder.client(mClient).build().create(MomentApi.class);
    }

    public static AllApi createLocationRequestService(String baseUrl) {
        return getBuilder(baseUrl).client(mClient).build().create(AllApi.class);
    }

    public static CommonApi createCommonRequestService() {
        return mBuilder.client(mClient).build().create(CommonApi.class);
    }

    public static ChatApi createChatRequestService() {
        return mBuilder.client(mClient).build().create(ChatApi.class);
    }

    public static Retrofit.Builder getBuilder(String baseUrl) {
        return new Retrofit.Builder().
                addCallAdapterFactory(RxJava2CallAdapterFactory.create()).
                addConverterFactory(GsonConverterFactory.create(GsonUtils.getGson())).
                baseUrl(baseUrl);
    }

    public static Retrofit createUrlRetrofit(String baseUrl) {
        return getBuilder(baseUrl).client(mClient).build();
    }

    public static DiscoveryApi createDiscoveryRequestService() {
        return mBuilder.client(mClient).build().create(DiscoveryApi.class);
    }

    public static WebViewApi createWebViewRequestService() {
        return mStringBuilder.client(mClient).build().create(WebViewApi.class);
    }

    public static TestReportApi createTestReportRequestService() {

        return mReportBuilder.client(mReportClient).build().create(TestReportApi.class);
    }

    public static String getBodyContentType() {
        return "application/x-www-form-urlencoded; charset=" + getParamsEncoding();
    }

    protected static String getParamsEncoding() {
        return DEFAULT_PARAMS_ENCODING;
    }


    public static void reBuild() {

        String url = ShareFileUtils.getString(ShareFileUtils.HTTP_URL, UrlConstants.BASE_URL);

        mBuilder = new Retrofit.Builder().
                addCallAdapterFactory(RxJava2CallAdapterFactory.create()).
                addConverterFactory(GsonConverterFactory.create(GsonUtils.getGson())).
                baseUrl(url);

        mStringBuilder = new Retrofit.Builder().
                addCallAdapterFactory(RxJava2CallAdapterFactory.create()).
                addConverterFactory(ScalarsConverterFactory.create()).
                baseUrl(url);
    }

}
