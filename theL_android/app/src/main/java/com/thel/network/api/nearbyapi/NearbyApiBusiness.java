package com.thel.network.api.nearbyapi;

import com.thel.base.BaseDataBean;
import com.thel.modules.main.userinfo.BlockNetBean;
import com.thel.modules.main.userinfo.bean.HiddenLoveNetBean;
import com.thel.network.RequestConstants;
import com.thel.network.service.DefaultRequestService;
import com.thel.utils.MD5Utils;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Flowable;

/**
 * Created by waiarl on 2017/10/30.
 * 附近页面的api伴生 business
 * 包括个人详情
 */

public class NearbyApiBusiness {

    public static Flowable<BaseDataBean> secretlyFollow(String userId, String actionType) {
        final Map<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_SECRET_FOLLOW_RECEIVEID, userId);
        data.put(RequestConstants.I_ACTIONTYPE, actionType + "");
        return DefaultRequestService.createNearbyRequestService().secretlyFollow(MD5Utils.generateSignatureForMap(data));
    }

    public static Flowable<BlockNetBean> pullUserIntoBlackList(String userId) {
        final Map<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_USERID, userId);
        return DefaultRequestService.createNearbyRequestService().pullUserIntoBlackList(MD5Utils.generateSignatureForMap(data));
    }

    public static Flowable<HiddenLoveNetBean> sendHiddenLove(String userId) {
        final Map<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_USERID, userId);
        return DefaultRequestService.createNearbyRequestService().sendHiddenLove(MD5Utils.generateSignatureForMap(data));

    }

    public static Flowable<BaseDataBean> reportImageOrUser(String userId, String type, String imgId) {
        final Map<String, String> data = new HashMap<>();
        data.put(RequestConstants.REPORT_LIVEUSER_RECEIVED_ID, userId);
        data.put(RequestConstants.REPORT_LIVEUSER_REPORT_TYPE, type + "");
        data.put(RequestConstants.REPORT_LIVEUSER_REPORT_IMGID, imgId + "");
        return DefaultRequestService.createNearbyRequestService().reportImageOrUser(MD5Utils.generateSignatureForMap(data));
    }
}
