package com.thel.network.api.commonapi;

import com.thel.base.BaseDataBean;
import com.thel.bean.BlackListNetBean;
import com.thel.bean.FriendListBean;
import com.thel.bean.ResultNetBean;
import com.thel.bean.user.VipConfigBean;
import com.thel.imp.follow.bean.SingleUserRelationNetBean;
import com.thel.modules.main.userinfo.BlockNetBean;
import com.thel.network.RequestConstants;

import java.util.Map;

import io.reactivex.Flowable;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by waiarl on 2017/9/23.
 * 通用api
 */

public interface CommonApi {
    /**
     * 关注
     * FOLLOW_USER
     *
     * @return
     */
    @POST("interface/stay/appUserFollower!followUser.do")
    @FormUrlEncoded
    Flowable<ResultNetBean> followUser(@FieldMap Map<String, String> map);


    /**
     * 获取单个用户关系
     * RequestConstants.GET_SINGLE_USER_RELATION
     */
    @GET("v2/users/relation/single")
    Flowable<SingleUserRelationNetBean> getSingleUsersRelation(@Query("userId") String userId);

    /**
     * 挤眼
     * SEND_WINK_CREATE
     *
     * @param map
     * @return
     */
    @POST("v1/wink/create")
    @FormUrlEncoded
    Flowable<BaseDataBean> sendWink(@FieldMap Map<String, String> map);

    /**
     * 屏蔽
     *
     * @param map
     * @return RequestConstants.PULL_USER_INTO_BLACKLIST
     */
    @POST("interface/stay/appUser!pullUserIntoBlackList.do")
    @FormUrlEncoded
    Flowable<BlockNetBean> pullUserIntoBlackList(@FieldMap Map<String, String> map);

    /**
     * 移除黑名单
     *
     * @param map
     * @return REMOVE_FROM_BLACKLIST
     */
    @POST("interface/stay/appUser!removeFromBlackList.do")
    @FormUrlEncoded
    Flowable<BaseDataBean> removeFromBlackList(@FieldMap Map<String, String> map);

    /**
     * 屏蔽这条日志
     *
     * @param map
     * @return BLOCK_THIS_MOMENT
     */
    @POST("friend/stay/moments/hideMomentsPlus")
    @FormUrlEncoded
    Flowable<BaseDataBean> blackThisMoment(@FieldMap Map<String, String> map);

    /**
     * 屏幕某人的日志（全部日志）
     *
     * @param map
     * @return BLOCK_USER_MOMENTS
     */
    @POST("friend/stay/moments/addToBlackList")
    @FormUrlEncoded
    Flowable<BlockNetBean> blackHerMoment(@FieldMap Map<String, String> map);

    /**
     * 给日志点赞
     *
     * @param map
     * @return MOMENTS_WINK_MOMENT
     */
    @POST("friend/stay/moments/wink")
    @FormUrlEncoded
    Flowable<BaseDataBean> winkMoment(@FieldMap Map<String, String> map);

    /**
     * 取消给日志点赞
     *
     * @param map
     * @return MOMENTS_WINK_MOMENT
     */
    @POST("friend/stay/moments/unwink")
    @FormUrlEncoded
    Flowable<BaseDataBean> unWinkMoment(@FieldMap Map<String, String> map);

    /**
     * 删除日志
     *
     * @param map
     * @return MOMENTS_DELETE_MOMENT
     */
    @POST("friend/stay/moments/delete")
    @FormUrlEncoded
    Flowable<BaseDataBean> deleteMoment(@FieldMap Map<String, String> map);

    /**
     * 举报日志
     *
     * @param map
     * @return MOMENTS_REPORT_MOMENT
     */
    @POST("friend/stay/moments/reportMomentsPlus")
    @FormUrlEncoded
    Flowable<BaseDataBean> reportMoment(@FieldMap Map<String, String> map);

    /**
     * 获取vip等级
     * return GET_VIP_CONFIG
     */
    @GET(RequestConstants.GET_VIP_CONFIG)
    Flowable<VipConfigBean> getVipConfig();

    /**
     * 从服务端获取自己的好友列表
     *
     * @param type
     * @return
     */
    @GET("v2/users/relation")
    Flowable<FriendListBean> getNetFriendList(@Query(RequestConstants.I_TYPE) String type);

    /**
     * 获取网络黑名单列表，再刚登陆的时候获取
     *
     * @return
     */
    @GET(RequestConstants.BLACK_LIST)
    Flowable<BlackListNetBean> getBlackNetList();

}
