package com.thel.network.api.loginapi.bean;

import android.text.TextUtils;

import com.thel.base.BaseDataBean;

import java.io.Serializable;

/**
 * Created by waiarl on 2017/9/23.
 */

public class SignInUserBean extends BaseDataBean implements Serializable {
    /**
     * id
     */
    public int id;

    /**
     * 是否已填过用户资料，0：未填过 1：已填过
     */
    public int isNew;

    /**
     * 是否已初始化过昵称、thelId、头像 0:未初始化 1：已初始化
     */
    public int isInit;

    /********************  必填项 start ******************/

    /**
     * 昵称
     */
    public String nickName;

    /**
     * 生日 yyyy-MM-dd
     */
    public String birthday;

    /**
     * 头像地址
     */
    public String avatar;

    /**
     * 身高
     */
    public int height;

    /**
     * 体重
     */
    public int weight;

    public String roleName;

    public String purpose;
    /**
     * thelID
     */
    public String userName;
    public String wantRole;
    public String professionalTypes;
    public String outLevel;

    /**
     * 感情状态
     */
    public int affection; // (0=不想透露，1=单身，2=约会中，3=稳定关系，4=已婚，5=开放关系)

    /********************  必填项 start ******************/

    /**
     * 现居地
     */
    public String livecity;

    /**
     * 上线时间
     */
    public long serviceDate;

    /**
     * 爱好
     */
    public String interests;

    /**
     * 旅行地
     */
    public String travelcity;

    /**
     * 简介
     */
    public String intro;

    /**
     * 种族
     */
    public String ethnicity;

    /**
     * 位置
     */
    public String location;

    // add
    public String discriptions;

    public String music;

    public String books;

    public String occupation;

    public String others;

    public String movie;

    public String food;

    public String messageUser;

    public String messagePassword;

    public String blackmelist = "";
    public String blacklist = "";
    public String reportMomentsList = "";
    public String hideMomentsList = "";
    public String hideMomentsUserList = "";
    public String reportUserList = "";

    public int ratio = 0;

    public int perm;

    /**
     * 判断是否是新注册用户
     * 4.1.0:根据昵称，头像，relaId是否有一个为空来判断，若有一个为空，则需完善个人资料
     *
     * @return
     */
    public boolean isNewUser() {
        return TextUtils.isEmpty(nickName) || TextUtils.isEmpty(avatar) || TextUtils.isEmpty(userName) || TextUtils.isEmpty(roleName);
    }

    @Override
    public String toString() {
        return "SignInUserBean{" +
                "id=" + id +
                ", isNew=" + isNew +
                ", isInit=" + isInit +
                ", nickName='" + nickName + '\'' +
                ", birthday='" + birthday + '\'' +
                ", avatar='" + avatar + '\'' +
                ", height=" + height +
                ", weight=" + weight +
                ", roleName='" + roleName + '\'' +
                ", purpose='" + purpose + '\'' +
                ", userName='" + userName + '\'' +
                ", wantRole='" + wantRole + '\'' +
                ", professionalTypes='" + professionalTypes + '\'' +
                ", outLevel='" + outLevel + '\'' +
                ", affection=" + affection +
                ", livecity='" + livecity + '\'' +
                ", serviceDate=" + serviceDate +
                ", interests='" + interests + '\'' +
                ", travelcity='" + travelcity + '\'' +
                ", intro='" + intro + '\'' +
                ", ethnicity='" + ethnicity + '\'' +
                ", location='" + location + '\'' +
                ", discriptions='" + discriptions + '\'' +
                ", music='" + music + '\'' +
                ", books='" + books + '\'' +
                ", occupation='" + occupation + '\'' +
                ", others='" + others + '\'' +
                ", movie='" + movie + '\'' +
                ", food='" + food + '\'' +
                ", messageUser='" + messageUser + '\'' +
                ", messagePassword='" + messagePassword + '\'' +
                ", blackmelist='" + blackmelist + '\'' +
                ", blacklist='" + blacklist + '\'' +
                ", reportMomentsList='" + reportMomentsList + '\'' +
                ", hideMomentsList='" + hideMomentsList + '\'' +
                ", hideMomentsUserList='" + hideMomentsUserList + '\'' +
                ", reportUserList='" + reportUserList + '\'' +
                '}';
    }
}
