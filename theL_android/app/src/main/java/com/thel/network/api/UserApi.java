package com.thel.network.api;

import com.thel.base.BaseDataBean;
import com.thel.bean.InitRecommendUserListBean;
import com.thel.bean.SearchUsersListBean;
import com.thel.bean.user.FollowUsersBean;
import com.thel.bean.user.VipConfigBean;
import com.thel.modules.main.home.search.SearchRecommendBean;
import com.thel.modules.main.me.bean.LikeCountNetBean;
import com.thel.modules.main.me.bean.MyInfoNetBean;
import com.thel.network.RequestConstants;

import java.util.Map;

import io.reactivex.Flowable;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by lingwei on 2017/9/21.
 */

public interface UserApi {

    @GET("interface/stay/appUser!myInfoForFriend.do")
    Flowable<MyInfoNetBean> getMyInfo(@Query("cursor") String curPage);

    //    /**
    //     * @param userId
    //     * @return
    //     */
    //    @GET("interface/stay/match/likeMeCount")
    //    Flowable<LikeCountBean> getUserLikesCount(@Field("key") String userId);

    //    //上传用户头像
    //    @POST("v2/users/avatar/upload")
    //
    //    Flowable<>uploadAvatar(@Field("key") String avatar);

    /**
     * 获取用户的like数量
     *
     * @param userId
     * @return
     */
    @GET(RequestConstants.GET_LIKE_ME_COUNT)
    Flowable<LikeCountNetBean> getUserLikesCount(@Query("userId") String userId);

    /**
     * 获取vip配置信息
     *
     * @return
     */
    @GET(RequestConstants.GET_VIP_CONFIG)
    Flowable<VipConfigBean> getVipConfig();

    /**
     * 配置vip功能
     *
     * @param map
     * @return
     */
    @POST(RequestConstants.VIP_CONFIG)
    @FormUrlEncoded
    Flowable<BaseDataBean> vipConfig(@FieldMap Map<String, String> map);


    /**
     * 上传头像
     *
     * @return
     */
    @POST(RequestConstants.UPLOAD_AVATAR)
    @FormUrlEncoded
    Flowable<BaseDataBean> uploadAvatar(@FieldMap Map<String, String> map);


    /**
     * 上传用户资料的背景图片
     *
     * @return
     */
    @POST(RequestConstants.UPLOAD_BG_IMAGE)
    @FormUrlEncoded
    Flowable<BaseDataBean> uploadBgImage(@FieldMap Map<String, String> map);

    /**
     * 关注多个用户
     *
     * @param map
     * @return
     */
    @POST(RequestConstants.FOLLOW_USERS)
    @FormUrlEncoded
    Flowable<FollowUsersBean> followUsers(@FieldMap Map<String, String> map);

    /**
     * 获取随机推荐用户
     *
     * @return
     */
    @GET(RequestConstants.GET_RECOMMEND_USERS_RANDOM)
    Flowable<SearchRecommendBean> getRecommendUserRandom(@Query("curPage") String curPage, @Query("limit") String limit);


    /**
     * 期待直播
     *
     * @param map
     * @return
     */
    @POST(RequestConstants.LOOKING_FORWARD_TO_LIVE)
    @FormUrlEncoded
    Flowable<BaseDataBean> lookingForwardToLive(@FieldMap Map<String, String> map);
}
