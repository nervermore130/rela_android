package com.thel.network.api.commonapi;

import com.thel.base.BaseDataBean;
import com.thel.bean.ResultNetBean;
import com.thel.modules.main.userinfo.BlockNetBean;
import com.thel.network.RequestConstants;
import com.thel.network.service.DefaultRequestService;
import com.thel.utils.MD5Utils;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Flowable;

/**
 * 一些通用api
 * Created by waiarl on 2017/9/23.
 */

public class CommonApiBusiness {
    /**
     * 关注
     *
     * @param userId     关注用户
     * @param actionType 关注类型（1：关注，0：取消关注）
     * @return
     */
    public static Flowable<ResultNetBean> followUser(String userId, String actionType) {
        final Map<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_RECEIVEDID, userId);
        data.put(RequestConstants.I_ACTIONTYPE, actionType);
        return DefaultRequestService.createCommonRequestService().followUser(MD5Utils.generateSignatureForMap(data));
    }

    /**
     * 挤眼
     *
     * @param userId 被挤眼的用户
     * @param type   挤眼类型，详情参考winkUtils
     * @return
     */
    public static Flowable<BaseDataBean> sendWink(String userId, String type) {
        final Map<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_USERID, userId);
        data.put(RequestConstants.I_TYPE, type);
        return DefaultRequestService.createCommonRequestService().sendWink(MD5Utils.generateSignatureForMap(data));
    }

    /**
     * 屏蔽
     *
     * @param userId
     * @return
     */
    public static Flowable<BlockNetBean> pullUserIntoBlackList(String userId) {
        final Map<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_USERID, userId);
        return DefaultRequestService.createCommonRequestService().pullUserIntoBlackList(MD5Utils.generateSignatureForMap(data));
    }

    /**
     * 移除黑名单
     *
     * @param userId
     * @return
     */
    public static Flowable<BaseDataBean> removeFromBlackList(String userId) {
        final Map<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_USERID, userId);
        return DefaultRequestService.createCommonRequestService().removeFromBlackList(MD5Utils.generateSignatureForMap(data));

    }

    /**
     * 屏蔽这条日志
     *
     * @param momentId
     * @return
     */
    public static Flowable<BaseDataBean> blackThisMoment(String momentId) {
        final Map<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_MOMENT_ID, momentId);
        data.put(RequestConstants.I_REASON_TYPE, "0");
        return DefaultRequestService.createCommonRequestService().blackThisMoment(MD5Utils.generateSignatureForMap(data));

    }

    /**
     * 屏蔽这条日志
     *
     * @param momentId
     * @return
     */
    public static Flowable<BaseDataBean> blackThisMoment(String momentId, String reasonType) {
        final Map<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_MOMENT_ID, momentId);
        data.put(RequestConstants.I_REASON_TYPE, reasonType);
        return DefaultRequestService.createCommonRequestService().blackThisMoment(MD5Utils.generateSignatureForMap(data));

    }

    /**
     * 屏蔽某人的日志
     *
     * @param userId
     * @return
     */
    public static Flowable<BlockNetBean> blackherMoment(String userId) {
        final Map<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_USERID, userId);
        return DefaultRequestService.createCommonRequestService().blackHerMoment(MD5Utils.generateSignatureForMap(data));

    }

    /**
     * 给日志点赞
     *
     * @param momentId
     * @return
     */
    public static Flowable<BaseDataBean> winkMoment(String momentId) {
        final Map<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_MOMENTS_ID, momentId);
        data.put(RequestConstants.I_WINK_COMMENT_TYPE, "8");
        return DefaultRequestService.createCommonRequestService().winkMoment(MD5Utils.generateSignatureForMap(data));

    }

    /**
     * 取消给日志点赞
     *
     * @param momentId
     * @return
     */
    public static Flowable<BaseDataBean> unWinkMoment(String momentId) {
        final Map<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_MOMENTS_ID, momentId);
        return DefaultRequestService.createCommonRequestService().unWinkMoment(MD5Utils.generateSignatureForMap(data));

    }

    /**
     * 删除日志
     *
     * @param momentId
     * @return
     */
    public static Flowable<BaseDataBean> deleteMoment(String momentId) {
        final Map<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_MOMENTS_ID, momentId);
        return DefaultRequestService.createCommonRequestService().deleteMoment(MD5Utils.generateSignatureForMap(data));

    }

    /**
     * 举报日志
     *
     * @param momentId
     * @return
     */
    public static Flowable<BaseDataBean> reportMoment(String momentId) {
        final Map<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_MOMENTS_ID, momentId);
        return DefaultRequestService.createCommonRequestService().reportMoment(MD5Utils.generateSignatureForMap(data));

    }
}
