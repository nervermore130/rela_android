package com.thel.network.api.nearbyapi;


import com.thel.base.BaseDataBean;
import com.thel.bean.ReleaseMomentSucceedBean;
import com.thel.bean.moments.MomentsListBean;
import com.thel.bean.user.NearUserListNetBean;
import com.thel.modules.main.nearby.visit.WhoSeenMeListNetBean;
import com.thel.modules.main.userinfo.BlockNetBean;
import com.thel.modules.main.userinfo.bean.FollowersListNetBean;
import com.thel.modules.main.userinfo.bean.HiddenLoveNetBean;
import com.thel.modules.main.userinfo.bean.UserInfoNetBean;
import com.thel.modules.main.video_discover.videofalls.VideoMomentNetListBean;
import com.thel.network.RequestConstants;

import java.util.Map;

import io.reactivex.Flowable;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by waiarl on 2017/10/11.
 */

public interface NearbyApi {
    /**
     * 获取附近用户列表（附近,明星）
     *
     * @param curPage   当前页
     * @param supStar   是否收到"传情"最多(0不是, 1 是) *
     * @param newRegist 是否最新注册会员(0不是, 1 是) *
     * @param pageSize  页数大小（通常为20）
     * @return
     */
    @GET("interface/stay/appUser!nearbySimpleList.do")
    Flowable<NearUserListNetBean> getNearbySimpleList(@Query("pageSize") String pageSize, @Query("curPage") String curPage, @Query("supStar") String supStar, @Query("newRegist") String newRegist);

    /**
     * 4.16.0 获取附近用户列表 筛选
     *
     * @param params
     * @return
     */
    @GET("interface/stay/appUser!nearbySimpleList.do")
    Flowable<NearUserListNetBean> getNearbySimpleList(@QueryMap Map<String, String> params);

    /**
     * 获取附近用户列表（人气）
     *
     * @param curPage   当前页
     * @param supStar   是否收到"传情"最多(0不是, 1 是) *
     * @param newRegist 是否最新注册会员(0不是, 1 是) *
     * @param pageSize  页数大小（通常为20）
     * @return GET_ACTIVE_USERS
     */
    @GET("v2/users/active/list")
    Flowable<NearUserListNetBean> getActiveUserList(@Query("pageSize") String pageSize, @Query("curPage") String curPage, @Query("supStar") String supStar, @Query("newRegist") String newRegist);

    /**
     * 谁来看过我
     *
     * @param pageSize 页数大小
     * @param curPage  当前页，第一页为1
     * @return
     */
    @GET("interface/stay/appUserView!cheackView.do")
    Flowable<WhoSeenMeListNetBean> getWhoSeenMeList(@Query("pageSize") String pageSize, @Query("curPage") String curPage);

    /**
     * 获取附近日志列表
     *
     * @param curPage 当前页，初始为1
     * @return
     */
    @GET("friend/stay/explore/nearByMoment_v1")
    Flowable<MomentsListBean> getNearbyMomentsList(@Query("curPage") String curPage);

    /**
     * 获取附近小鲜肉
     */
    @GET("/friend/stay/freshman")
    Flowable<NearUserListNetBean> getNearbyPigList(@Query(RequestConstants.I_CURSOR) int cursor, @Query(RequestConstants.I_LIMIT) int limit);

    /**
     * 我看过谁
     *
     * @param pageSize 页数大小
     * @param curPage  当前页，第一页为1
     * @return
     */
    @GET("interface/stay/appUserView!iView.do")
    Flowable<WhoSeenMeListNetBean> getVisitList(@Query("pageSize") String pageSize, @Query("curPage") String curPage);

    /**
     * 获取个人信息
     *
     * @param userId
     * @return
     */
    @GET("/interface/stay/appUser!newUserInfoForFriend.do")
    Flowable<UserInfoNetBean> getUserInfoBean(@Query("userId") String userId);

    /**
     * 获取个人日志列表
     *
     * @param userId   用户id。这个是可选参数，如果有，则是查看某个用户（也可以看自己）的朋友圈信息列表。如果没有这个参数（
     *                 或者这个参数传递一个空值），则查看“我能看到的所有的”朋友圈信息列表
     * @param mainType 接受 3 种值：空，moments，popular:“空”和“moments”代表：“关注的 日志列表”, 或者是
     *                 “某个人发的 日志列表”;"popular"代表：热门
     * @param pageSize 每页显示多少条数据
     * @param curPage  当前页码
     *                 MOMENTS_LIST
     * @return
     */
    @GET("friend/stay/moments/listWithMusicMultiImg")
    Flowable<MomentsListBean> getUserInfoMomentsList(@Query(RequestConstants.I_USERID) String userId, @Query(RequestConstants.I_PAGESIZE) String pageSize, @Query(RequestConstants.I_CURPAGE) String curPage, @Query(RequestConstants.I_MAIN_TYPE) String mainType);

    /**
     * 悄悄关注
     *
     * @param map
     * @return
     */
    @POST("interface/stay/appUserFollower!secretlyFollow.do")
    @FormUrlEncoded
    Flowable<BaseDataBean> secretlyFollow(@FieldMap Map<String, String> map);


    /**
     * 屏蔽
     *
     * @param map
     * @return RequestConstants.PULL_USER_INTO_BLACKLIST
     */
    @POST("interface/stay/appUser!pullUserIntoBlackList.do")
    @FormUrlEncoded
    Flowable<BlockNetBean> pullUserIntoBlackList(@FieldMap Map<String, String> map);

    /**
     * 暗恋
     *
     * @param data
     * @return RequestConstants.SEND_HIDDEN_LOVE
     */
    @POST("interface/stay/appUser!sendHiddenLove.do")
    @FormUrlEncoded
    Flowable<HiddenLoveNetBean> sendHiddenLove(@FieldMap Map<String, String> data);

    /**
     * 举报用户或者头像
     *
     * @param data
     * @return POST_REPORT_USER_ORIMAGE
     */
    @POST("v2/users/picture/report")
    @FormUrlEncoded
    Flowable<BaseDataBean> reportImageOrUser(@FieldMap Map<String, String> data);

    /**
     * 获取个人用户详情中的粉丝列表
     *
     * @param userId
     * @param pageSize
     * @param curPage
     * @return GET_USER_FOLLOWERS_LIST
     */
    @GET("interface/stay/appUserFollower!getNewFansUser.do")
    Flowable<FollowersListNetBean> getUserFollowersList(@Query("userid") String userId, @Query(RequestConstants.I_PAGESIZE) String pageSize, @Query(RequestConstants.I_CURPAGE) String curPage);

    @GET(RequestConstants.GET_MOMENTS_VIDEO_LIST)
    Flowable<VideoMomentNetListBean> getVideoFallsMomentsList(@Query(RequestConstants.I_CURSOR) int cursor, @Query(RequestConstants.I_LIMIT) int limit);

    @GET(RequestConstants.GET_PGC_MOMENTS_VIDEO_LIST)
    Flowable<VideoMomentNetListBean> getPgcVideoFallsMomentsList(@Query(RequestConstants.I_CURSOR) int cursor, @Query(RequestConstants.I_LIMIT) int limit);

    /**
     * 上传封面
     *
     * @param map
     * @return
     */
    @POST("v2/users/coverimage/set")
    @FormUrlEncoded
    Flowable<ReleaseMomentSucceedBean> uploadCover(@FieldMap Map<String, String> map);
}
