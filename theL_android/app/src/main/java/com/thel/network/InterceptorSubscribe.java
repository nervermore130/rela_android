package com.thel.network;

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.thel.app.TheLApp;
import com.thel.base.BaseDataBean;
import com.thel.bean.analytics.AnalyticsBean;
import com.thel.modules.live.utils.LiveUtils;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;
import com.thel.utils.NetworkUtils;
import com.thel.utils.ShareFileUtils;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.io.IOException;
import java.net.SocketTimeoutException;

/**
 * @author liuyun
 * @date 2017/10/23
 * 未登录状态，拦截网络请求
 */

public class InterceptorSubscribe<T> implements Subscriber<T> {

    protected boolean hasErrorCode = false;

    protected BaseDataBean errDataBean = null;

    private int requestCount = 0;

    private static final int MAX_REQUEST_COUNT = 80;

    @Override
    public void onSubscribe(Subscription s) {

        requestCount++;

        //未登录状态，拦截网络请求
        if (!ShareFileUtils.getBoolean(ShareFileUtils.HAS_LOGGED, false))
            return;

        if (NetworkUtils.isNetworkConnected(TheLApp.context)) {
            if (requestCount <= MAX_REQUEST_COUNT) {
                s.request(Integer.MAX_VALUE);
            } else {
                try {
                    s.cancel();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }

    @Override
    public void onNext(T data) {
        L.i("InterceptorSubscribe", "data=" + data.toString() + ",class=" + data.getClass().getName());
        try {

            if (data instanceof BaseDataBean) {
                BaseDataBean baseDataBean = (BaseDataBean) data;
                hasErrorCode = ErrorCode.getInstance().handlerErrorDataCallBack(baseDataBean);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        requestCount--;

    }

    @Override
    public void onError(Throwable t) {
        L.d("InterceptorSubscribe", " onError : " + t.getMessage());

        requestCount--;

        try {
            if (t instanceof HttpException) {
                HttpException httpException = (HttpException) t;

                String body = httpException.response().errorBody().string();

                L.d("InterceptorSubscribe", " onError body " + body);

                errDataBean = GsonUtils.getObject(body, BaseDataBean.class);
                if (errDataBean == null) {
                    L.d("InterceptorSubscribe", " onError baseDataBean gson null");
                } else {
                    ErrorCode.getInstance().handlerErrorDataCallBack(errDataBean);
                }
            } else if (t instanceof SocketTimeoutException) {

                try {

                    SocketTimeoutException socketTimeoutException = (SocketTimeoutException) t;

                    String text = socketTimeoutException.getMessage();

                    L.d("InterceptorSubscribe", " text " + text);

                    HttpException httpException = (HttpException) t;

                    AnalyticsBean analyticsBean = new AnalyticsBean();

                    analyticsBean.time = System.currentTimeMillis() + "";

                    analyticsBean.body = httpException.response().raw().body().string();

                    analyticsBean.api = httpException.response().raw().request().url().toString();

                    analyticsBean.logType = "api_timeout";

                    LiveUtils.pushLivePointLog(analyticsBean);

                    L.d("InterceptorSubscribe", " httpException.response().body() " + httpException.response().body());

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        onComplete();
    }

    @Override
    public void onComplete() {

        L.d("InterceptorSubscribe", " onComplete : ");

    }
}
