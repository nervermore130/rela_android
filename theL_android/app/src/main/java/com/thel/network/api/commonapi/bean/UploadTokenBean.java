package com.thel.network.api.commonapi.bean;

import com.thel.base.BaseDataBean;

import java.io.Serializable;

/**
 * Created by waiarl on 2017/9/23.
 * 上传图片到七牛的token
 */

public class UploadTokenBean extends BaseDataBean implements Serializable {
    public String uploadToken;
}
