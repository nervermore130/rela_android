package com.thel.network.api;

import com.thel.bean.moments.MomentsCheckBean;
import com.thel.network.RequestConstants;

import io.reactivex.Flowable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by liuyun on 2017/10/10.
 */

public interface MsgApi {

    /**
     * 检查朋友圈，是否有新的朋友圈或是是否有新的消息
     */
    @GET(RequestConstants.MOMENTS_CHECK)
    Flowable<MomentsCheckBean> checkMoments( @Query("cursor") String cursor);

}
