package com.thel.network.api;

import com.thel.modules.live.bean.LiveRoomsNetBean;
import com.thel.modules.main.discover.bean.LiveMetaNetBean;
import com.thel.modules.main.discover.bean.LiveRoomsEntranceNetBean;
import com.thel.network.RequestConstants;

import io.reactivex.Flowable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by waiarl on 2017/11/7.
 */

public interface DiscoveryApi {
    /**
     * 关注日志列表
     *
     * @return BROADCASTERS_ON_LIVE
     */
    @GET("v1/live/banner")
    Flowable<LiveRoomsEntranceNetBean> getLiveRoomEntryce();

    /**
     * 获取当前有多少位我关注的主播正在直播
     *
     * @return GET_LIVE_META
     */
    @GET("v1/live/meta")
    Flowable<LiveMetaNetBean> getLiveMeta();

    /**
     * 获取新人直播列表
     *
     * @param cursor
     * @param pageSize
     * @return GET_NEW_ANCHORS
     */
    @GET("v1/live/newAnchors")
    Flowable<LiveRoomsNetBean> getNewAnchors(@Query(RequestConstants.I_CURSOR) int cursor, @Query(RequestConstants.I_LIMIT) int pageSize);

    /**
     * 获取正在直播的新人直播列表
     *
     * @param cursor
     * @param pageSize
     * @return GET_NEW_LIVE_LIST
     */
    @GET("v1/live/new-anchor-live-list")
    Flowable<LiveRoomsNetBean> getNewAnchorLiveList(@Query(RequestConstants.I_CURSOR) int cursor, @Query(RequestConstants.I_LIMIT) int pageSize);

    /**
     * 获取推荐关注新人列表
     *
     * @param cursor
     * @param pageSize
     * @return GET_RECOMMEND_ANCHORS_LIST
     */
    @GET("v1/live/recommend-follow-anchors")
    Flowable<LiveRoomsNetBean> getRecommendNewFollowAnchorsList(@Query(RequestConstants.I_CURSOR) int cursor, @Query(RequestConstants.I_LIMIT) int pageSize);

    /**
     * 获取附近正在直播用户
     *
     * @param cursor
     * @param pageSize
     * @return GET_NEARBY_LIVE
     */
    @GET("/v1/live/neibors")
    Flowable<LiveRoomsNetBean> getNeighboursListListData(@Query(RequestConstants.I_CURSOR) int cursor, @Query(RequestConstants.I_LIMIT) int pageSize);

    /***
     * 获取音频直播列表
     * @param isMulti 0不是多人直播，1为多人直播
     * */
    @GET("/v1/live/audio-live-list")
    Flowable<LiveRoomsNetBean> getLiveAudioListBean(@Query(RequestConstants.I_CURSOR) int cursor, @Query(RequestConstants.I_LIMIT) int limit, @Query("isMulti") int isMulti);

    /**
     * 按类型获取直播
     *
     * @param cursor
     * @param pageSize
     * @return GET_TYPE_LIVE_LIST
     */
    @GET("v1/live/list-with-logType")
    Flowable<LiveRoomsNetBean> getLiveListWithTypeData(@Query(RequestConstants.I_CURSOR) int cursor, @Query(RequestConstants.I_LIMIT) int pageSize, @Query(RequestConstants.I_LIVE_TYPE_ID) int typeId);

    /***
     * 获取视频直播列表
     *
     * */
    @GET("v1/live/video-live-list")
    Flowable<LiveRoomsNetBean> getLiveVideoListBean(@Query(RequestConstants.I_CURSOR) int cursor, @Query(RequestConstants.I_LIMIT) int limit);

    /**
     * 推荐关注主播类型
     *
     * @param cursor
     * @param pageSize
     * @return GET_FOLLOW_RECOMMEND_TYPE
     */
    @GET("v1/live/recommend-follow-with-logType")
    Flowable<LiveRoomsNetBean> getRecommendFollowListTypeData(@Query(RequestConstants.I_CURSOR) int cursor, @Query(RequestConstants.I_LIMIT) int pageSize, @Query(RequestConstants.I_LIVE_TYPE_ID) int typeId);


}
