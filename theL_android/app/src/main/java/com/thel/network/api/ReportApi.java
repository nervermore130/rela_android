package com.thel.network.api;

import com.thel.network.RequestConstants;

import java.util.Map;

import io.reactivex.Flowable;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ReportApi {

    /**
     * 屏蔽某人的日志(加入到屏蔽黑名单)
     *
     * @param map
     */
    @POST(RequestConstants.REPORTS_LOGS)
    @FormUrlEncoded
    Flowable<String> pushLiveLog(@FieldMap Map<String, String> map);

}
