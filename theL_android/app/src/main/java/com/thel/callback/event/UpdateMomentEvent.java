package com.thel.callback.event;

import com.thel.base.BaseDataBean;
import com.thel.bean.gift.WinkCommentBean;
import com.thel.bean.moments.MomentsBean;

import java.util.ArrayList;
import java.util.List;

/**
 * 刷新日志数据event，主要刷新点赞、评论、权限
 *
 * @author Setsail
 */
public class UpdateMomentEvent extends BaseDataBean {

    public UpdateMomentEvent(MomentsBean momentsBean) {
        momentsId = momentsBean.momentsId;
        commentNum = momentsBean.commentNum;
        winkNum = momentsBean.winkNum;
        winkFlag = momentsBean.winkFlag;
        followerStatus = momentsBean.followerStatus;
        shareTo = momentsBean.shareTo;
        winkComments.addAll(momentsBean.winkUserList);
    }

    /**
     * 日志在列表中所处的位置
     */
    public int position;

    /**
     * moments的唯一标识
     */
    public String momentsId = "";

    /**
     * 评论数量
     */
    public int commentNum;

    /**
     * 点赞数量
     */
    public int winkNum;

    /**
     * 是否点过赞 0:没点过，1~9
     */
    public int winkFlag;

    /**
     * 是否关注了该用户 0:没关注过，1:关注过
     */
    public int followerStatus;

    /**
     * 发布的类型，1 代表 all, 2 代表 friends,3 代表 only me
     */
    public int shareTo;

    /**
     * 点赞对象
     */
    public List<WinkCommentBean> winkComments = new ArrayList<>();

}
