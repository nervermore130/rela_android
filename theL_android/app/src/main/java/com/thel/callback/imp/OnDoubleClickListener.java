package com.thel.callback.imp;

import android.view.View;

/**
 * Created by liuyun on 2018/1/13.
 */

public abstract class OnDoubleClickListener implements View.OnClickListener {

    private long firstClickTime = 0;

    @Override public final void onClick(View v) {
        long secondClickTime = firstClickTime;

        firstClickTime = System.currentTimeMillis();

        if (firstClickTime - secondClickTime < 500) {
            onDoubleClick(v);
        }
    }

    protected abstract void onDoubleClick(View v);
}
