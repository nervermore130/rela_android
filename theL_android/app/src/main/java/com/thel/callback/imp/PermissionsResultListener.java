package com.thel.callback.imp;

public interface PermissionsResultListener {
    void onPermissionGranted();

    void onPermissionDenied();
}
