package com.thel.callback.imp;

import android.view.MotionEvent;
import android.view.View;

import com.thel.utils.L;

public abstract class OnEditTouchListener implements View.OnTouchListener {

    private long startTime = -1;

    private float startX = -1;

    private float startY = -1;

    @Override public boolean onTouch(View v, MotionEvent event) {

        int action = event.getAction();

        switch (action) {
            case MotionEvent.ACTION_DOWN:

                startTime = System.currentTimeMillis();

                startX = event.getX();

                startY = event.getY();

                break;

            case MotionEvent.ACTION_UP:

                long endTime = System.currentTimeMillis();

                float endX = event.getX();

                float endY = event.getY();

                boolean isSingleClick = (endTime - startTime < 300) && (Math.abs(endX - startX) < 8) && (Math.abs(endY - startY) < 8);

                onTouch(isSingleClick);

                startX = -1;

                startY = -1;

                break;
        }

        return false;
    }

    protected abstract void onTouch(boolean isSingleClick);
}
