package com.thel.callback;


import com.thel.IMsgStatusCallback;
import com.thel.db.table.message.MsgTable;
import com.thel.modules.main.messages.bean.MsgBean;

/**
 * Created by liuyun on 2018/3/30.
 */

public class IMsgCallback extends IMsgStatusCallback.Stub {

    @Override public void onConnectStatus(int status) {

    }

    @Override public void onMsgSendStatus(MsgBean msgBean) {

    }

    @Override public void onNewMsgComing(MsgBean msgList) {

    }

    @Override public void onUpdateMsg(MsgTable msgTable) {

    }

    @Override public void onDeleteMsgTable(String userId) {

    }

    @Override public void onChatInfo() {

    }

    @Override public void onTransferPercents(int percents) {

    }

}
