package com.thel.callback;

import com.thel.chat.netty.MsgPacket;
import com.thel.modules.main.messages.bean.MsgBean;
import com.thel.service.chat.ChatService;

/**
 * @author liuyun
 * @date 2018/3/19
 */

public interface IChatClient {

    void setBinder(ChatService.ChatServiceBinder chatServiceBinder);

    /**
     * 验证消息
     */
    void authMsg();

    /**
     * 拉取消息
     *
     * @param type
     */
    void syncMsg(int type);

    /**
     * ping服务器
     */
    void pingMsg();

    /**
     * 发送消息
     *
     * @param msgBean 消息的内容
     */
    void sendMsg(MsgBean msgBean);

    /**
     * 发送回执消息
     *
     * @param msgBean
     */
    void sendReceiptMsg(MsgBean msgBean);

    /**
     * 发送 正在打字 和 正在语音 的消息
     *
     * @param toUserId 对方的用户id
     * @param content  内容
     */
    void sendPendingMsg(String toUserId, String content);

    void receivePendingMsg(MsgPacket msgPacket);

    /**
     * 每收到一条消息发送消息回执
     *
     * @param msgId
     */
    void receiptMsg(String msgId);

    /**
     * 消息连接成功时间 - APP打开时间（毫秒时间戳）
     */
    void authReport();

    /**
     * 异步拉取消息
     */
    void syncReport(long startTime);
}
