package com.thel.callback.imp;

import android.app.Activity;
import android.os.Build;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;


/**
 * simple and powerful Keyboard show/hidden listener,view {@android.R.id.content} and {@ViewTreeObserver.OnGlobalLayoutListener}
 * Created by yes.cpu@gmail.com 2016/7/13.
 */
public class KeyboardChangeListener implements ViewTreeObserver.OnGlobalLayoutListener {
    private static final String TAG = "ListenerHandler";
    private View mContentView;
    private int mOriginHeight;
    private int mPreHeight;
    private KeyBoardListener mKeyBoardListen;

    public interface KeyBoardListener {
        /**
         * call back
         *
         * @param isShow         true is show else hidden
         * @param keyboardHeight keyboard height
         */
        void onKeyboardChange(boolean isShow, int keyboardHeight);
    }

    public void setKeyBoardListener(KeyBoardListener keyBoardListen) {
//        Utils.Log("refredsh","keyboardListener:setKeyBoardListener");

        this.mKeyBoardListen = keyBoardListen;
    }

    public KeyboardChangeListener(Activity contextObj) {
        if (contextObj == null) {
            Log.i(TAG, "contextObj is null");
            return;
        }
        mContentView = findContentView(contextObj);
        if (mContentView != null) {
            addContentTreeObserver();
        }
    }

    public KeyboardChangeListener(Fragment fragment) {
        if (fragment == null) {
            Log.i(TAG, "contextObj is null");
            return;
        }
        mContentView = fragment.getView();
//        Utils.Log("refresh", "mContentView=null?:" + (mContentView == null));
        if (mContentView != null) {
            addContentTreeObserver();
        }
    }

    public KeyboardChangeListener(View rootView) {
        if (rootView == null) {
            Log.i(TAG, "contextObj is null");
            return;
        }
        mContentView = rootView;
//        Utils.Log("refresh", "mContentView=null?:" + (mContentView == null));
        if (mContentView != null) {
            addContentTreeObserver();
        }
    }

    private View findContentView(Activity contextObj) {
        return contextObj.findViewById(android.R.id.content);
    }

    private void addContentTreeObserver() {
        mContentView.getViewTreeObserver().addOnGlobalLayoutListener(this);
    }

    @Override
    public void onGlobalLayout() {
        int currHeight = mContentView.getHeight();
        if (currHeight == 0) {
            Log.i(TAG, "currHeight is 0");
            return;
        }
        boolean hasChange = false;
        if (mPreHeight == 0) {
            mPreHeight = currHeight;
            mOriginHeight = currHeight;
        } else {
            if (mPreHeight != currHeight) {
                hasChange = true;
                mPreHeight = currHeight;
            } else {
                hasChange = false;
            }
        }

//        Utils.Log("refresh", "mPreHeight=" + mPreHeight + ",mOriginHeight=" + mOriginHeight +" currHeight="+currHeight+ ",hasChange=" + hasChange);
        if (hasChange) {
            boolean isShow;
            int keyboardHeight = 0;
            if (mOriginHeight == currHeight) {
                //hidden
                isShow = false;
            } else {
                //show
                keyboardHeight = mOriginHeight - currHeight;
                isShow = true;
            }

            if (mKeyBoardListen != null) {
                mKeyBoardListen.onKeyboardChange(isShow, keyboardHeight);
            }
        }
    }

    public void destroy() {
//        Utils.Log("refredsh","keyboardListener:destroy");

        if (mContentView != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                mContentView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        }
    }

    public void close() {
//        Utils.Log("refredsh","keyboardListener:close");

        mPreHeight = 0;
        mOriginHeight = 0;
        setKeyBoardListener(null);
        destroy();
    }

    public void reset() {
//        Utils.Log("refredsh","keyboardListener:reset");
        mPreHeight = 0;
        mOriginHeight = 0;
        setKeyBoardListener(null);
        if (mContentView != null) {
            addContentTreeObserver();
        }
    }
}
