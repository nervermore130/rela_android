package com.thel.callback;

import android.os.RemoteException;

import com.thel.app.TheLApp;
import com.thel.bean.FriendListBean;
import com.thel.bean.LiveInfoLogBean;
import com.thel.bean.moments.MomentsCheckBean;
import com.thel.constants.TheLConstants;
import com.thel.manager.ChatServiceManager;
import com.thel.modules.live.utils.LiveUtils;
import com.thel.modules.main.messages.bean.MsgBean;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestConstants;
import com.thel.network.service.DefaultRequestService;
import com.thel.utils.NetworkUtils;
import com.thel.utils.Utils;
import com.umeng.analytics.MobclickAgent;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class IMainProcessCallback extends com.thel.IMainProcessCallback.Stub {

    @Override
    public void checkFriendsRelationship(final String userId, final MsgBean msgBean) {

        if (userId != null || msgBean != null) {
            DefaultRequestService
                    .createCommonRequestService()
                    .getNetFriendList(RequestConstants.RELATION_TYPE_FRIEND)
                    .onBackpressureDrop()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new InterceptorSubscribe<FriendListBean>() {

                        @Override
                        public void onNext(FriendListBean friendListBean) {

                            super.onNext(friendListBean);

                            if (friendListBean != null && friendListBean.data != null && friendListBean.data.list != null) {

                                final List<String> userIds = friendListBean.data.list;

                                ChatServiceManager.getInstance().setUserList(userIds);

                                for (int i = 0; i < userIds.size(); i++) {

                                    if (userId != null && userId.equals(userIds.get(i))) {
                                        ChatServiceManager.getInstance().changeRelationship(userId, msgBean, 0);
                                        return;
                                    }

                                }

                                ChatServiceManager.getInstance().changeRelationship(userId, msgBean, 1);

                            }
                        }
                    });
        }
    }

    @Override
    public void umengPushStatus(int status, String msg, String ip) {

        if (NetworkUtils.isNetworkConnected(TheLApp.context)) {

            switch (status) {

                case 0:
//                MobclickAgent.onEvent(TheLApp.context, "im_msg_fail", "msg_get_ip_fail_android"); //  Android 获取ip失败
                    break;
                case 1:
                    LiveInfoLogBean.getInstance().getImAnalytics().errorReason = "ping_ip_fail";
                    MobclickAgent.onEvent(TheLApp.context, "im_msg_fail", "msg_ping_ip_fail_android"); //  Android ping IP失败
                    pushErrorLog(ip);
                    break;
                case 2:
                    LiveInfoLogBean.getInstance().getImAnalytics().errorReason = "connect_fail";
                    MobclickAgent.onEvent(TheLApp.context, "im_msg_fail", "msg_connect_fail_android"); //  Android 连接失败
                    pushErrorLog(ip);
                    break;
                case 3:
                    LiveInfoLogBean.getInstance().getImAnalytics().errorReason = "auth_fail_" + msg;
                    MobclickAgent.onEvent(TheLApp.context, "im_msg_fail", msg); //  Android 认证失败
                    pushErrorLog(ip);
                    break;
                case 4:
                    LiveInfoLogBean.getInstance().getImAnalytics().errorReason = "auth_fail_" + msg;
                    MobclickAgent.onEvent(TheLApp.context, "im_msg_fail", msg); //  Android 认证超时
                    pushErrorLog(ip);
                    break;
                case 5:
                    MobclickAgent.onEvent(TheLApp.context, "msg_start_connect_android"); //  android消息开始连接
                    break;
                case 6:
                    MobclickAgent.onEvent(TheLApp.context, "msg_connect_success_android"); //  android消息连接成功
                    break;
                case 7:
                    MobclickAgent.onEvent(TheLApp.context, "msg_start_auth_android"); //  Android android消息开始认证
                    break;
                case 8:
                    MobclickAgent.onEvent(TheLApp.context, "msg_auth_success_android"); //  android消息认证成功
                    break;
                case 9:
                    MobclickAgent.onEvent(TheLApp.context, "im_msg_ping_timeout_android"); //  im ping超时
                    LiveInfoLogBean.getInstance().getImAnalytics().errorReason = "ping_timeout";
                    pushErrorLog(ip);
                    break;
                case 10:
                    MobclickAgent.onEvent(TheLApp.context, "im_msg_ping_fail_android"); //  im ping失败
                    LiveInfoLogBean.getInstance().getImAnalytics().errorReason = "ping_fail";
                    pushErrorLog(ip);
                    break;

            }
        }

    }

    @Override
    public void serviceKilledBySystem() {
        ChatServiceManager.getInstance().serviceKilledBySystem();
    }

    private void pushErrorLog(String ip) {
        LiveInfoLogBean.getInstance().getImAnalytics().time = LiveUtils.getLiveTime();
        LiveInfoLogBean.getInstance().getImAnalytics().userId = Utils.getMyUserId();
        LiveInfoLogBean.getInstance().getImAnalytics().cdnDomain = ip;
        LiveInfoLogBean.getInstance().getImAnalytics().logType = TheLConstants.LiveInfoLogConstants.IM_MSG_ERROR_LOG;
        LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getImAnalytics());
    }

}
