package com.thel.callback;

/**
 * @author liuyun
 * @date 2017/12/4
 */

public interface IConnectMic {


    /**
     * 连麦
     *
     * @param method
     * @param toUserId
     * @param x
     * @param y
     * @param height
     * @param width
     * @param dailyGuard 是否为日榜好友
     */
    void connectMic(String method, String toUserId, float x, float y, float height, float width, String nickName, String avatar,boolean dailyGuard);


    /**
     * 接收方响应连麦请求
     *
     * @param method
     * @param toUserId
     * @param result
     * @param x
     * @param y
     * @param height
     * @param width
     */
    void rejectConnectMic(String method, String toUserId, String result, float x, float y, float height, float width);

    /**
     * 连麦发起方取消连麦
     *
     * @param method
     * @param toUserId
     */
    void cancelConnectMic(String method, String toUserId);

    /**
     * @param method
     * @param toUserId
     */
    void hangupConnectMic(String method, String toUserId);

    /**
     * 响应观众连麦请求
     *
     * @param method
     * @param body
     */
    void responseAudienceLinkMic(String method, String body);

}
