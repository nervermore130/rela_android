package com.thel.callback;

import android.view.View;

import com.thel.bean.ImgShareBean;

/**
 * Created by waiarl on 2017/8/18.
 * 用于带日志列表的一些需要同一数据的方法
 */

public interface MomentOperateIn {
    int SHARE_TYPE_URL = 0;//分享类型，url分享
    int SHARE_TYPE_POSTER = 1;//分享类型，海报分享

    /**
     * 对一个日志是否点赞
     *
     * @param momentId   日志ID
     * @param like       是否点赞（true:点赞;false:取消点赞）
     * @param likeButton 点赞按钮
     * @param animButton 动画view
     * @param position   所在位置
     * @param moment     所点赞的日志
     */
    void LikeOrNot(String momentId, boolean like, View likeButton, View animButton, int position, Object moment);

    /**
     * 打开图片
     *
     * @param imageUrls
     * @param position
     * @param photoView
     * @param relaId
     * @param imgShareBean
     */
    void openPhoto(String imageUrls, int position, View photoView, String relaId, ImgShareBean imgShareBean);

    /**
     * 点击评论
     *
     * @param momentId
     * @param position
     * @param clickView
     * @param momentBean 被评论的日志
     */
    void commentMoment(String momentId, int position, View clickView, Object momentBean);

    /**
     * 更多操作
     *
     * @param momentId
     * @param position
     * @param clickView
     */
    void operateMore(String momentId, int position, View clickView);

    /**
     * 弹出加载dialog
     *
     * @param canClose 是否可以关闭
     */
    void showLoadingDialog(boolean canClose);

    /**
     * 关闭加载dialog
     */
    void closeLoadingDialog();

    /**
     * 删除某篇日志
     *
     * @param momentId
     * @param position
     * @param momentBean
     */
    void deleteMoment(String momentId, int position, Object momentBean);

    /**
     * 分享日志
     *
     * @param type       分享类型，1
     * @param momentId   分享的日志id
     * @param position   position
     * @param momentBean 分享的日志bean
     */
    void shareMoment(int type, String momentId, int position, Object momentBean);

    /**
     * 举报日志
     *
     * @param momentId
     * @param position
     * @param momentBean
     */
    void reportMoment(String momentId, int position, Object momentBean);
}
