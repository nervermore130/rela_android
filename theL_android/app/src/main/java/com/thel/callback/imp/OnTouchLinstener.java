package com.thel.callback.imp;

import android.view.MotionEvent;
import android.view.View;

public abstract class OnTouchLinstener implements View.OnTouchListener {

    float startX = 0;

    float startY = 0;

    long startTime = 0;

    @Override public final boolean onTouch(View v, MotionEvent event) {

        int action = event.getAction();

        switch (action) {
            case MotionEvent.ACTION_DOWN:
                startX = event.getX();
                startY = event.getY();
                break;
            case MotionEvent.ACTION_UP:

                float deltaX = event.getX() - startX;

                float deltaY = event.getY() - startY;

                long deltaTime = System.currentTimeMillis() - startTime;

                if (Math.abs(deltaX) < 8 && Math.abs(deltaY) < 8 && deltaTime < 200) {
                    onClick();
                    return true;
                } else {
                    return false;
                }

            default:
                break;
        }
        return false;
    }

    public abstract void onClick();

}
