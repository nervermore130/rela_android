package com.thel.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.thel.app.TheLApp;

/**
 * SharedPreferences的工具类
 *
 * @author Setsail
 */
public class SharedPrefUtils {

    private static SharedPreferences pref; // 文件操作对象
    private static final String SHARED_FILE_NAME = "the_l"; // sharefile 文件名

    /**
     * 复合筛选 *
     */
    public static final String FILE_NAME_FILTER_USERS = "filter_users"; // 复合筛选文件名
    public static final String ROLE = "role";
    public static final String RELATIONSHIP = "relationship";
    public static final String LOOKING_FOR = "lookingFor";
    public static final String ONLINE_STATUS = "onlineStatus";
    public static final String AGE = "age";
    public static final String HEIGHT = "height";
    public static final String WEIGHT = "weight";
    public static final String CONSTELLATION = "constellation";
    public static final String ETHNICITY = "ethnicity";
    // 世界定位的经纬度
    public static final String WORLD_LAT = "worldLat";
    public static final String WORLD_LNG = "worldLng";

    /**
     * 快捷筛选 *
     */
    public static final String QUICK_FILTER_AGE_RANGE = "quick_age_range";
    public static final String QUICK_FILTER_ROLE = "quick_role";

    /**
     * 我关注的话题相关 *
     */
    public static final String FILE_NAME_TAGS = "thel_tags"; // 热门话题
    public static final String TRENDING_TAGS = "trending_tags";// 热门标签缓存
    public static final String HOT_THEMES = "hot_themes";// 热门话题缓存

    /**
     * 聊天背景 *
     */
    public static final String FILE_NAME_CHAT_BG = "chat_bg";
    public static final String CHAT_BG_ALL = "all";

    /**
     * 消息相关的文件
     */
    public static final String FILE_NAME_MSG = "msg";
    public static final String MSG_CURSOR = "cursor_";

    /**
     * 聊天草稿 *
     */
    public static final String FILE_NAME_CHAT_DRAFT = "chat_draft";

    /**
     * 存储一些屏蔽相关的数据
     */
    public static final String FILE_NAME_BLOCK_FILE = "block_file";
    public static final String REPORT_USER_LIST = "reportUserList";
    public static final String REPORT_MOMENTS_LIST = "reportMomentsList";
    public static final String HIDE_MOMENTS_LIST = "hideMomentsList";
    public static final String BLACK_LIST = "blacklist";
    public static final String HIDE_MOMENTS_USER_LIST = "hideMomentsUserList";
    /**
     * 存储搜索历史的相关数据
     */
    public static final String FILE_NAME_HISTORY_USERS = "history_file";
    public static final String HISTORY_LIST = "historylist";

    /**
     * httpdns
     */
    public static final String FILE_HTTP_DNS = "thel_http_dns";
    public static final String IP = "ip";
    public static final String TIME_OUT = "time_out";//什么时候过期

    /**
     * 开发测试用，可以手动修改DNS地址
     */
    public static final String FILE_TEST_DNS = "thel_test_dns";
    public static final String AUTO_COMPLETE = "auto_complete";
    public static final String TEST_DNS = "test_dns";

    /**
     * 广告
     */
    public static final String FILE_AD = "thel_ad";
    public static final String WELCOME_PAGE_AD_IMG = "welcome_page_ad_img";// 欢迎页显示的广告图
    public static final String WELCOME_PAGE_AD_TIME = "welcome_page_ad_time";// 欢迎页显示的广告图倒计时
    public static final String WELCOME_PAGE_AD_REDIRECT_URL = "welcome_page_redirect_url";// 欢迎页显示的广告跳转地址
    public static final String WELCOME_PAGE_AD_TITLE = "welcome_page_ad_title";// 欢迎页显示的广告的标题
    public static final String WELCOME_PAGE_AD_REDIRECT_TYPE = "welcome_page_redirect_type";// 欢迎页显示的广告跳转类型

    /**
     * 密友请求
     */
    public static final String FILE_REQUEST = "thel_request";
    public static final String MY_CIRCLE_CACHE_DATA = "my_circle_cache_data";// 用来做我的密友页面的缓存，由于数据较少且数据结构较差，选择用文件来缓存

    /**
     * guide信息
     */
    public static final String FILE_GUIDES = "thel_guides";
    public static final String GUIDES_CHANGE_ANNIVERSARY = "change_anniversary";// 是否已经显示过修改纪念日的guide了
    public static final String GUIDES_REQUEST_CHANGE_BG = "request_change_bg";// 是否已经显示过修改纪念日的guide了
    public static final String GUIDES_CHANGE_TOPIC_MOMENT_BG = "change_topic_moment_bg";// 是否已经显示过修改话题日志背景图的guide了

    /**
     * 历史表情
     */
    public static final String FILE_STICKERS = "thel_stickers";
    public static final String NEW_STICKER_RECOMMEND = "new_sticker_recommend";// 推荐表情包
    public static final String VIEWED_RECOMMENDED_STICKER_MAIN = "viewed_recommended_sticker_main";// 主页已经浏览过的推荐表情包
    public static final String VIEWED_RECOMMENDED_STICKER_STORE = "viewed_recommended_sticker_store";// 表情商店已经浏览过的推荐表情包

    /**
     * 充值软妹豆
     */
    public static final String NEW_SOFT_RECOMMEND = "new_soft_recommend";// 充值软妹豆提示
    public static final String NEW_STICKER_STORE = "new_sticker_store";// 表情商店提示


    /**
     * 数据同步
     */
    public static final String FILE_SYNC = "thel_sync";
    public static final String LAST_SYNC_TIME = "last_sync_time";// 上次同步时间

    /**
     * 直播相关的参数
     */
    public static final String FILE_LIVE = "thel_live";
    public static final String LIVE_WIDTH = "liveWidth";
    public static final String LIVE_HEIGHT = "liveHeight";
    public static final String LOWEST_BPS = "lowestBps";
    public static final String LIVE_BPS = "liveBps";
    public static final String LIVE_FPS = "liveFps";
    public static final String LIVE_PERMITION = "perm";// 是否有直播权限

    /**
     * 存储聊天界面关掉『关注』区域的用户id，key是我的userId，value是关掉的用户的userId数组
     */
    public static final String FILE_CHAT_CLOSE_FOLLOW = "thel_chat_close_follow";

    /**
     * 存储一些缓存数据
     */
    public static final String FILE_CACHE = "thel_cache";
    public static final String MY_LEVEL_POINT = "my_level_point";

    public static final String VIDEO_GUIDE = "video_guide";
    public static final String CURRENT_VERSION = "current_version";// 当前版本号，用来实现引导用户点击会员功能
    public static final String CURRENT_VERSION1 = "current_version1";// 当前版本号，用来实现引导用户详情点击更多
    public static final String REMEMBER_CARD_FIRST_CLICK = "remember_card_first_click";//3.0.0 用来记住已经点击过的进入推荐名片界面。
    public static final String INIT_CIRCLE_PAGE = "init_circle_page_";// 判断新用户是否关注够了10个人，这是前缀，后面跟用户id
    public static final String HAS_SHOWN_WELCOME_MSG = "has_shown_welcome_msg_";// 该用户有没有显示过欢迎消息，欢迎消息只在用户新安装软件第一次登陆（注册）后才显示，这是前缀，后面跟用户id
    public static final String FAVORITE_MOMENT_LIST = "favorite_moment_list";//收藏日志
    public static final String LANGUAGE_SETTING = "language_setting";//语言设置
    public static final String LIVE_GUIDE = "live_guide";//直播新手向导
    public static final String WINK_GUIDE = "live_guide";//直播新手向导
    public static final String NEW_SHARE_MOMENT = "new_share_moment1";// 2.22.0版本分享日志新功能提醒
    public static final String NEW_COLLECT_MOMENT = "new_collect_moment";//4.4.0收藏优化
    public static final String NEW_SHARE_ME = "new_share_me1";// 2.22.0版本分享自己新功能提醒
    public static final String LIVE_SHOW_CAPTURE_TEXTSIZE = "live_show_capture_textsize";// 2.22.0版本 直播端当前文本大小
    public static final String MARK_ENTRANCE = "mark_entrance";// 2.24.0版本 收藏入口修改提醒
    public static final String VIDEO_LIST_SCROLL_NEW = "video_list_scroll_new1";//3.0.0 滑动查看她的全部视频
    public static final String VIDEO_PLAY_LIST_SCROLL_NEW = "video_play_list_scroll_new1";//3.0.0 上下滑动查看她的全部视频
    public static final String LIVE_HOME_GUIDE = "live_home_page_guide";//关注页直播列表新手引导
    public static final String LIVE_ROOM_SCROLL_NEW = "live_room_scroll_new1";//直播上下滑动引导
    //4.0.0
    public static final String RELATION_FRIEND = "relation_friend";//获取朋友列表
    public static final String WINK_USER_IDS = "wink_user_ids";//获取朋友列表
    public static final String WINK_FILE_CACHE = "thel_wink_cache";
    //4.1.0
    public static final String FILE_TOP_LINK = "file_top_link";//获取榜单
    //我的好友列表
    public static final String FILE_FRIEND_LIST = "file_friend_list_";//获取榜单
    //直播Pk列表被拒绝的列表
    public static final String FILE_LIVE_PK_REFUSER = "file_live_pk_refreser_list_";
    //直播连麦列表被拒绝的列表
    public static final String FILE_LIVE_LINk_MIC_REFUSER = "file_live_link_mic_refreser_list_";
    //主播向观众发起连麦
    public static final String FILE_LIVE_AUDIENCE_LINk_MIC_REFUSER = "file_live_audience_link_mic_refreser_list_";

    //视频已经被观看列表
    public static final String FILE_VIDEO = "file_video";
    public static final String KEY_VIDEO_SEEN_LIST = "key_video_seen_list";
    public static final String KEY_VIDEO_SEEK_LIST = "key_video_seek_list";

    public static final String KEY_ONLY_TEXT = "onlyText";

    /**
     * 数据升级相关
     */
    public static final String UPDATE_DATA = "thel_update_data";

    public static final String FILE_PHOTO_PATH_CACHE = "photos";

    public static final String PHOTO_PATH_CACHE_ALL = "all";


    /**
     * 从共享文件中获取字符串
     *
     * @param key      表签名
     * @param defValue 值
     */
    public static String getString(String fileName, String key, String defValue) {
        Context context = TheLApp.getContext();
        if (FILE_STICKERS.equals(fileName)) {// 表情文件按用户id分文件
            pref = context.getSharedPreferences(fileName + ShareFileUtils.getString(ShareFileUtils.ID, ""), Context.MODE_PRIVATE);
        } else {
            pref = context.getSharedPreferences(fileName, Context.MODE_PRIVATE);
        }
        String result = "";
        try {
            if (FILE_STICKERS.equals(fileName)) {// 表情文件是经过签名加密的
                result = AESEncryptor.decryptSigned(AESEncryptor.SEED, pref.getString(key, defValue));
            } else
                result = AESEncryptor.decrypt(AESEncryptor.SEED, pref.getString(key, defValue));
        } catch (Exception e) {
            result = pref.getString(key, defValue);
        }
        return result;
    }

    /**
     * 从共享文件中获取字符串(不加密)
     *
     * @param key      表签名
     * @param defValue 值
     */
    public static String getStringWithoutEncrypt(String fileName, String key, String defValue) {
        Context context = TheLApp.getContext();
        pref = context.getSharedPreferences(fileName, Context.MODE_PRIVATE);
        return pref.getString(key, defValue);
    }

    /**
     * 从共享文件中获取整型数据
     *
     * @param key      表签名
     * @param defValue 值
     */
    public static int getInt(String fileName, String key, int defValue) {
        Context context = TheLApp.getContext();
        pref = context.getSharedPreferences(fileName, Context.MODE_PRIVATE);
        return pref.getInt(key, defValue);
    }

    /**
     * 从共享文件中获取长整型数据
     *
     * @param key
     * @param defValue
     * @return
     */
    public static long getLong(String fileName, String key, long defValue) {
        Context context = TheLApp.getContext();
        pref = context.getSharedPreferences(fileName, Context.MODE_PRIVATE);
        return pref.getLong(key, defValue);
    }

    /**
     * 从共享文件中获取boolean数据
     *
     * @param key      表签名
     * @param defValue 值
     */
    public static boolean getBoolean(String fileName, String key, boolean defValue) {
        pref = TheLApp.getContext().getSharedPreferences(fileName, Context.MODE_PRIVATE);
        return pref.getBoolean(key, defValue);
    }

    /**
     * 从共享文件中获取boolean数据
     *
     * @param key      表签名
     * @param defValue 值
     */
    public static boolean getBoolean(String key, boolean defValue) {
        if (null == pref) {
            pref = TheLApp.getContext().getSharedPreferences(SHARED_FILE_NAME, Context.MODE_PRIVATE);
        }
        return pref.getBoolean(key, defValue);
    }

    /**
     * 保存字符串数据
     *
     * @param key   表签名
     * @param value 值
     */
    public static void setString(String fileName, String key, String value) {
        if (FILE_STICKERS.equals(fileName)) {// 表情文件按用户id分文件
            pref = TheLApp.getContext().getSharedPreferences(fileName + ShareFileUtils.getString(ShareFileUtils.ID, ""), Context.MODE_PRIVATE);
        } else {
            pref = TheLApp.getContext().getSharedPreferences(fileName, Context.MODE_PRIVATE);
        }
        try {
            if (FILE_STICKERS.equals(fileName)) {// 表情文件要签名加密
                pref.edit().putString(key, AESEncryptor.encryptSigned(AESEncryptor.SEED, value)).apply();
            } else {
                pref.edit().putString(key, AESEncryptor.encrypt(AESEncryptor.SEED, value)).apply();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 保存字符串数据(不加密)
     *
     * @param key   表签名
     * @param value 值
     */
    public static void setStringWithoutEncrypt(String fileName, String key, String value) {
        pref = TheLApp.getContext().getSharedPreferences(fileName, Context.MODE_PRIVATE);
        pref.edit().putString(key, value).apply();
    }

    /**
     * 保存整型数据
     *
     * @param key   表签名
     * @param value 值
     */
    public static void setInt(String fileName, String key, int value) {
        pref = TheLApp.getContext().getSharedPreferences(fileName, Context.MODE_PRIVATE);
        pref.edit().putInt(key, value).apply();
    }

    /**
     * 保存长整型数据
     *
     * @param key
     * @param value
     */
    public static void setLong(String fileName, String key, long value) {
        pref = TheLApp.getContext().getSharedPreferences(fileName, Context.MODE_PRIVATE);
        pref.edit().putLong(key, value).apply();
    }

    /**
     * 保存boolean数据
     *
     * @param key   表签名
     * @param value 值
     */
    public static void setBoolean(String fileName, String key, boolean value) {
        pref = TheLApp.getContext().getSharedPreferences(fileName, Context.MODE_PRIVATE);
        pref.edit().putBoolean(key, value).apply();
    }

    /**
     * 删除一条数据
     *
     * @param fileName
     * @param key
     */
    public static void remove(String fileName, String key) {
        pref = TheLApp.getContext().getSharedPreferences(fileName, Context.MODE_PRIVATE);
        pref.edit().remove(key).apply();
    }

    /**
     * 清空数据
     *
     * @param fileName
     */
    public static void clearData(String fileName) {
        pref = TheLApp.getContext().getSharedPreferences(fileName, Context.MODE_PRIVATE);
        pref.edit().clear().apply();
    }

}
