package com.thel.utils;

import android.app.Activity;
import android.content.Intent;

import com.thel.ui.dialog.LoadingActivity;

/**
 * Created by liuyun on 2017/9/18.
 */

public class ActivityDialogUtils {

    /**
     * 显示loading
     */
    public static void showLoading(Activity context) {

        if (context == null || PhoneUtils.getNetWorkType() == PhoneUtils.TYPE_NO) {
            return;
        }
        try {
            if (LoadingActivity.isShowing != 1) {
                LoadingActivity.isShowing = 1;
                context.startActivity(new Intent(context, LoadingActivity.class));
            }
        } catch (Exception e) {
        }
    }

    /**
     * 显示loading(屏蔽返回键)
     */
    public static void showLoadingNoBack(Activity context) {

        if (context == null || PhoneUtils.getNetWorkType() == PhoneUtils.TYPE_NO) {
            return;
        }

        try {
            if (LoadingActivity.isShowing != 1) {
                LoadingActivity.isShowing = 1;
                Intent intent = new Intent(context, LoadingActivity.class);
                intent.putExtra("noBack", true);
                context.startActivity(intent);
            }
        } catch (Exception e) {
        }
    }

    /**
     * 关闭loading框
     */
    public static void closeLoading() {
        try {
            if (LoadingActivity.isShowing == 1) {
                LoadingActivity.isShowing = -1;
                LoadingActivity.close();
            }
        } catch (Exception e) {
        }
    }

}
