package com.thel.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.List;

/**
 * 使用json相关的工具类
 *
 * @author zhangwenjia
 */
public class JsonUtils {

    /**
     * 获取JSON里的根节点
     *
     * @param object
     * @param key
     * @return
     */
    public static JSONObject getJSONObject(JSONObject object, String key) {
        JSONObject jsonObject = null;
        if (object != null && !object.isNull(key)) {
            try {
                jsonObject = object.getJSONObject(key);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jsonObject;
    }

    /**
     * 获取节里点的数组对象
     *
     * @param object
     * @param key
     * @return
     */
    public static JSONArray getJSONArray(JSONObject object, String key) {
        JSONArray jsonArray = new JSONArray();
        if (object != null && !object.isNull(key)) {
            try {
                jsonArray = object.getJSONArray(key);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jsonArray;
    }

    /**
     * 获取JSON里的String类型数据
     *
     * @param object
     * @param key节点名称
     * @param defaultValue String类型默认值
     * @return String类型结果
     */
    public static String getString(JSONObject object, String key, String defaultValue) {
        String result = defaultValue;
        if (object != null && !object.isNull(key)) {
            try {
                result = object.getString(key);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    /**
     * 获取JSON里的boolean类型数据
     *
     * @param object
     * @param key节点名称
     * @param defaultValue boolean类型默认值
     * @return boolean类型结果
     */
    public static boolean getBoolean(JSONObject object, String key, boolean defaultValue) {
        boolean result = defaultValue;
        if (object != null && !object.isNull(key)) {
            try {
                result = object.getBoolean(key);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    /**
     * 获取JSON里的int类型数据
     *
     * @param object
     * @param key节点名称
     * @param defaultValue int类型默认值
     * @return int类型结果
     */
    public static int getInt(JSONObject object, String key, int defaultValue) {
        int result = defaultValue;
        if (object != null && !object.isNull(key)) {
            try {
                result = object.getInt(key);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    /**
     * 获取JSON里的double类型数据
     *
     * @param object
     * @param key节点名称
     * @param defaultValue double类型默认值
     * @return double类型结果
     */
    public static double getDouble(JSONObject object, String key, double defaultValue) {
        double result = defaultValue;
        if (object != null && !object.isNull(key)) {
            try {
                result = object.getDouble(key);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    /**
     * 获取JSON里的long类型数据
     *
     * @param object
     * @param key节点名称
     * @param defaultValue long类型默认值
     * @return long类型结果
     */
    public static long getLong(JSONObject object, String key, long defaultValue) {
        long result = defaultValue;
        if (object != null && !object.isNull(key)) {
            try {
                result = object.getLong(key);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    /**
     * 删除JSONArray中的某一项
     *
     * @param jsonArray
     * @param index
     * @throws Exception
     */
    public static void removeIndexOfJSONArray(JSONArray jsonArray, int index) throws Exception {
        if (index < 0)
            return;
        Field valuesField = JSONArray.class.getDeclaredField("values");
        valuesField.setAccessible(true);
        List<Object> values = (List<Object>) valuesField.get(jsonArray);
        if (index >= values.size())
            return;
        values.remove(index);
    }
}
