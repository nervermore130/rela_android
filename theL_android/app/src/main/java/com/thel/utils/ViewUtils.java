package com.thel.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ListView;
import android.widget.Toast;

import com.thel.BuildConfig;
import com.thel.R;
import com.thel.app.TheLApp;

/**
 * View操作的工具类
 *
 * @author Setsail
 */
public class ViewUtils {

    private static final String TAG = ViewUtils.class.getSimpleName();

    // 防止控件被连续误点击的实用方法，传入要保护的时间，在此时间内将不可被再次点击
    public static void preventViewMultipleClick(final View v, int protectionMilliseconds) {
        if (v == null) {
            return;
        }
        v.setClickable(false);
        v.postDelayed(new Runnable() {
            @Override
            public void run() {
                v.setClickable(true);
            }
        }, protectionMilliseconds);
    }

    // 防止控件被连续误点击的实用方法，传入要保护的时间，在此时间内将不可被再次点击
    public static void preventViewMultipleTouch(final View v, int protectionMilliseconds) {
        if (v == null) {
            return;
        }
        v.setEnabled(false);
        v.postDelayed(new Runnable() {
            @Override
            public void run() {
                v.setEnabled(true);
            }
        }, protectionMilliseconds);
    }

    /**
     * 隐藏软键盘(并非对所有机型有效)
     *
     * @param context
     * @param view
     */
    public static void hideSoftInput(Context context, View view) {
        try {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm.isActive()) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void hideSoftInput() {
        InputMethodManager imm = (InputMethodManager) TheLApp.context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
    }

    /**
     * 隐藏软键盘(并非对所有机型有效)
     *
     * @param context
     */
    public static void hideSoftInput(Activity context) {
        try {
            ((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(context.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 显示软键盘(并非对所有机型有效)
     *
     * @param context
     * @param view
     */
    public static void showSoftInput(Context context, View view) {
        try {
            view.requestFocus();
            view.requestFocusFromTouch();
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, 0);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    /**
     * 初始化下拉刷新控件的属性
     *
     * @param swipeRefreshLayout
     */
    public static void initSwipeRefreshLayout(SwipeRefreshLayout swipeRefreshLayout) {
        swipeRefreshLayout.setColorSchemeResources(R.color.tab_normal);
        swipeRefreshLayout.setDistanceToTriggerSync(150);// 设置手指在屏幕下拉多少距离会触发下拉刷新
        swipeRefreshLayout.setProgressBackgroundColorSchemeColor(Color.WHITE); // 设定下拉圆圈的背景
        swipeRefreshLayout.setSize(SwipeRefreshLayout.DEFAULT); // 设置圆圈的大小
    }

    public static View getListViewItemByPosition(int pos, ListView listView) {
        final int firstListItemPosition = listView.getFirstVisiblePosition();
        final int lastListItemPosition = firstListItemPosition + listView.getChildCount() - listView.getHeaderViewsCount();

        if (pos < firstListItemPosition || pos > lastListItemPosition) {
            return null;
        } else {
            final int childIndex = pos - firstListItemPosition;
            return listView.getChildAt(childIndex);
        }
    }

    /**
     * 跳转到应用商店
     *
     * @param context
     */
    public static void directToAppStore(Context context) {
        try {
            Uri uri = Uri.parse("market://details?id=" + BuildConfig.APPLICATION_ID);
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        } catch (Exception e) {
            // DialogUtil.showToastShort(context, context.getString(R.string.me_activity_no_market));

            Toast toast = Toast.makeText(context, context.getString(R.string.me_activity_no_market), Toast.LENGTH_SHORT);
            toast.show();
        }
    }
}
