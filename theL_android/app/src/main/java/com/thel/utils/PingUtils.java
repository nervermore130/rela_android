package com.thel.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class PingUtils {

    public static synchronized float ping(int count, String ip) {
        Process process = null;
        float time = -1;
        try {
            process = Runtime.getRuntime().exec(createSimplePingCommand(count, ip));
            InputStream is = process.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();
            String line;
            while (null != (line = reader.readLine())) {
                sb.append(line);
                sb.append("\n");
            }
            reader.close();
            is.close();
            return getTime(sb.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != process) {
                process.destroy();
            }
        }


        return time;
    }

    private static String createSimplePingCommand(int count, String ip) {
        return "/system/bin/ping -c " + count + " " + ip;
    }

    private static float getTime(String line) {
        try {
            String[] lines = line.split("\n");

            for (String l : lines) {

                if (!l.contains("time="))
                    continue;

                int index = l.indexOf("time=");

                String time = l.substring(index + "time=".length());

                String currentTime = time.replace(" ms", "");

                if (currentTime != null) {

                    return Float.valueOf(currentTime);

                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        return -1;
    }

}
