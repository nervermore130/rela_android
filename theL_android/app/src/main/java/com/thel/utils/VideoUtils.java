package com.thel.utils;

import android.text.TextUtils;

import com.thel.app.TheLApp;
import com.thel.base.BaseDataBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.video.VideoReportBean;
import com.thel.db.MomentsDataBaseAdapter;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


/**
 * 视频工具类
 *
 * @author Setsail
 */
public class VideoUtils {

    private static final String TAG = VideoUtils.class.getSimpleName();

    public static boolean willAutoPlayVideo() {
        int netType = PhoneUtils.getNetWorkType();
        switch (netType) {
            case PhoneUtils.TYPE_WIFI:
                return ShareFileUtils.getBoolean(ShareFileUtils.AUTO_PLAY_WIFI, true);
            case PhoneUtils.TYPE_MOBILE_3G:
            case PhoneUtils.TYPE_MOBILE_4G:
            case PhoneUtils.TYPE_UNDEFINE:
                return ShareFileUtils.getBoolean(ShareFileUtils.AUTO_PLAY_MOBILE, false);
            default:
                return false;
        }
    }

    public static void saveVideoReport(String momentsId, int duration, int isFullScreen) {
        String userId = ShareFileUtils.getString(ShareFileUtils.ID, "");
        if (TextUtils.isEmpty(userId))
            return;
        VideoReportBean videoReportBean = new VideoReportBean();
        videoReportBean.userId = Integer.parseInt(userId);
        videoReportBean.momentId = momentsId;
        videoReportBean.playTime = System.currentTimeMillis();
        videoReportBean.playSeconds = duration / 1000;
        videoReportBean.showFull = isFullScreen;
        MomentsDataBaseAdapter.getInstance(TheLApp.getContext(), userId).saveVideoReport(videoReportBean);
    }

    public static void submitVideoReport() {
        String report = MomentsDataBaseAdapter.getInstance(TheLApp.getContext(), ShareFileUtils.getString(ShareFileUtils.ID, "")).getVideoReport();
        if (!TextUtils.isEmpty(report)) {
            RequestBusiness.getInstance()
                    .submitVideoReport(report)
                    .onBackpressureDrop()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new InterceptorSubscribe<BaseDataBean>() {
                        @Override
                        public void onNext(BaseDataBean data) {
                            super.onNext(data);
                            L.d(TAG, "submitVideoReport granted");
                            MomentsDataBaseAdapter.getInstance(TheLApp.getContext(), ShareFileUtils.getString(ShareFileUtils.ID, "")).clearVideoReport();
                        }
                    });
        }
    }

    /**
     * 上传视频播放次数
     * 以废弃,使用postPlayVideoInfo替代
     */
    @Deprecated
    public static void uploadVideoCount(String momentId) {
        RequestBusiness.getInstance()
                .postVideoPlayCountUpload(momentId)
                .onBackpressureDrop()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new InterceptorSubscribe<BaseDataBean>() {
                    @Override
                    public void onNext(BaseDataBean data) {
                        super.onNext(data);
                        L.d(TAG, "uploadVideoCount granted");
                    }
                });

    }

    /**
     * 上传视频播放信息
     *
     * @param entry         视频播放页面
     * @param momentId
     * @param playCount
     * @param playTimeMills
     */
    public static void postPlayVideoInfo(String entry, String momentId, int playCount, long playTimeMills) {

        //本地预发日志不上报播放次数
        if (momentId.contains(MomentsBean.SEND_MOMENT_FLAG)) return;

        L.d(TAG, "postPlayVideoInfo entry : " + entry);

        L.d(TAG, "postPlayVideoInfo momentId : " + momentId);

        L.d(TAG, "postPlayVideoInfo playCount : " + playCount);

        L.d(TAG, "postPlayVideoInfo playTimeMills : " + playTimeMills);

        if (playTimeMills > 500) {
            playTimeMills = 1;
        }

        RequestBusiness.getInstance()
                .postPlayVideoInfo(entry, momentId, String.valueOf(playCount), String.valueOf(playTimeMills))
                .onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new InterceptorSubscribe<>());
    }
}
