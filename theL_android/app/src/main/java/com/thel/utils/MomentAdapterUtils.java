package com.thel.utils;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.text.Layout;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseDataBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.theme.ThemeCommentBean;
import com.thel.callback.MomentOperateIn;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.imp.follow.FollowStatusChangedImpl;
import com.thel.imp.momentdelete.MomentDeleteUtils;
import com.thel.modules.main.home.moments.comment.MomentCommentActivity;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestConstants;
import com.thel.network.service.DefaultRequestService;
import com.thel.ui.widget.ExpandTextView;
import com.thel.ui.widget.LikeButtonView;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static android.view.View.GONE;
import static com.thel.utils.StringUtils.getString;

/**
 * Created by waiarl on 2017/8/14.
 * 用来处理日志列表中一些重复的代码，一些公用的方法
 * 也许不是最终处理，部分需要回调给接口
 */

public class MomentAdapterUtils {

    /**
     * 点击头像，进入个人主页
     *
     * @param userId   userId
     * @param imgThumb 头像
     * @param position 所在的数据位置（不是adapter位置，因为可能有header）
     * @param isSecret 是否是匿名（匿名不可点击）
     * @param listener 是否有监听（若是没有监听，这说明不需要这个点击事件）
     */
    public static void clickImgThumb(String userId, View imgThumb, int position, boolean isSecret, View.OnClickListener listener) {
        if (isSecret || listener == null || "0".equals(userId) || TextUtils.isEmpty(userId)) {
            return;
        }
        ViewUtils.preventViewMultipleClick(imgThumb, 1000);
//        final Intent intent = new Intent();
//        intent.setClass(TheLApp.getContext(), UserInfoActivity.class);
//        intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, userId + "");
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        TheLApp.getContext().startActivity(intent);
        FlutterRouterConfig.Companion.gotoUserInfo(userId);
    }

    /**
     * /**
     * 点击右侧关注按钮
     *
     * @param userId     userId
     * @param followView 关注按钮
     * @param position   所在的数据位置（不是adapter位置，因为可能有header）
     * @param listener   是否有监听（若是没有监听，这说明不需要这个点击事件）
     * @param nickname   昵称
     * @param avatar     头像
     */
    public static void clickFollow(String userId, View followView, int position, View.OnClickListener listener, String nickname, String avatar) {
        if (TextUtils.isEmpty(userId) || "0".equals(userId) || listener == null) {
            return;
        }
        ViewUtils.preventViewMultipleClick(followView, 1000);
        FollowStatusChangedImpl.followUser(userId, FollowStatusChangedImpl.ACTION_TYPE_FOLLOW, nickname, avatar);
    }

    /**
     * 给日志点赞
     *
     * @param momentId
     * @param likeButtonView
     * @param animView
     * @param position
     * @param moment
     * @param onClickListener
     * @param momentOperateIn
     */
    public static void clickLike(String momentId, LikeButtonView likeButtonView, View animView, int position, Object moment, View.OnClickListener onClickListener, MomentOperateIn momentOperateIn) {
        if (TextUtils.isEmpty(momentId) || "0".equals(momentId) || likeButtonView == null || animView == null || onClickListener == null) {
            return;
        }
        ViewUtils.preventViewMultipleClick(likeButtonView, 1000);
        // 判断是否联网
        if (PhoneUtils.getNetWorkType() == PhoneUtils.TYPE_NO) {//如果没有网络
            DialogUtil.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.info_no_network));
            return;
        }
        int likeOrNot = 0;
        if (moment instanceof ThemeCommentBean) {
            likeOrNot = ((ThemeCommentBean) moment).winkFlag;
        }
        if (likeOrNot == MomentsBean.HAS_NOT_WINKED) {//如果未点赞
            likeButtonView.check();//播放动画,然后再请求接口
            //            likeMoment(true, momentId, likeButtonView, animView, position, moment, momentOperateIn);
        } else {
            //            likeMoment(false, momentId, likeButtonView, animView, position, moment, momentOperateIn);
        }
    }

    /**
     * 点击评论
     *
     * @param momentId         日志的id
     * @param commentButton
     * @param position
     * @param mOnClickListener
     * @param momentBean
     * @param momentOperateIn
     */
    public static void clickComment(String momentId, View commentButton, int position, View.OnClickListener mOnClickListener, Object momentBean, MomentOperateIn momentOperateIn) {
        if (TextUtils.isEmpty(momentId) || "0".equals(momentId) || mOnClickListener == null) {
            return;
        }
        ViewUtils.preventViewMultipleClick(commentButton, 1000);
        if (momentOperateIn != null) {
            momentOperateIn.commentMoment(momentId, position, commentButton, momentBean);
        }

    }

    /**
     * 话题回复中 内容显示与全文显示,4.1.0 的新话题回复
     *
     * @param txt_content
     * @param txt_whole
     * @param commentBean
     * @param max_lines
     * @param max_expand_lines
     */
    public static void bindWholeTextContent(final ExpandTextView txt_content, final TextView txt_whole, final ThemeCommentBean commentBean, final int max_lines, final int max_expand_lines) {
        if (TextUtils.isEmpty(commentBean.commentText)) {
            return;
        }
        txt_content.setVisibility(View.VISIBLE);
        txt_content.setText(EmojiUtils.getInstace(EmojiUtils.DEFAULT_SIZE).getExpressionString(TheLApp.getContext(), commentBean.commentText));
        if (commentBean.contentStatus == MomentsBean.CONTENT_STATUS_HANG) {//状态为收起状态
            txt_whole.setText(TheLApp.getContext().getString(R.string.whole_content));//显示为全文
            txt_content.setMaxLines(max_lines);
        } else {
            txt_whole.setText(TheLApp.getContext().getString(R.string.pack_up));//显示为收起
            txt_content.setMaxLines(Integer.MAX_VALUE);
        }
        txt_content.setTextChangedListener(new ExpandTextView.TextChangedListener() {
            @Override
            public void textChanged(TextView textView) {
                if (View.VISIBLE == txt_content.getVisibility()) {
                    final Layout layout = txt_content.getLayout();
                    final int lineCount = layout.getLineCount();
                    commentBean.textLines = lineCount;
                    int eCount = 0;
                    if (lineCount >= max_lines) {
                        eCount = layout.getEllipsisCount(max_lines - 1);
                    }
                    if (eCount > 0 || lineCount > max_lines) {
                        txt_whole.setVisibility(View.VISIBLE);
                    } else {
                        txt_whole.setVisibility(GONE);
                    }
                } else {
                    txt_whole.setVisibility(GONE);
                }
            }
        });
        txt_whole.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (commentBean.textLines > max_expand_lines) {
//                    Intent intent = new Intent();
//                    intent.setClass(TheLApp.getContext(), MomentCommentActivity.class);
//                    intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, commentBean.commentId);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    TheLApp.getContext().startActivity(intent);
                    FlutterRouterConfig.Companion.gotoMomentDetails(commentBean.commentId);
                } else {
                    if (commentBean.contentStatus == MomentsBean.CONTENT_STATUS_HANG) {//当前状态是收起，显示全文
                        commentBean.contentStatus = MomentsBean.CONTENT_STATUS_WHOLE;
                        txt_content.setMaxLines(Integer.MAX_VALUE);
                        txt_whole.setText(TheLApp.getContext().getString(R.string.pack_up));//显示为收起
                    } else {//否者，收起
                        commentBean.contentStatus = MomentsBean.CONTENT_STATUS_HANG;
                        txt_content.setMaxLines(max_lines);
                        txt_whole.setText(TheLApp.getContext().getString(R.string.whole_content));//显示为全文
                    }
                }
            }
        });
    }

    /**
     * 设置图片显示
     *
     * @param imageUrls
     * @param pic
     * @param pic1
     * @param pic2
     * @param pic3
     * @param pic4
     * @param pic5
     * @param pic6
     * @param maxWidth
     */
    public static void setPicViews(String imageUrls, ViewGroup pic_parent, SimpleDraweeView pic,//
            SimpleDraweeView pic1, SimpleDraweeView pic2, SimpleDraweeView pic3, SimpleDraweeView pic4, SimpleDraweeView pic5, SimpleDraweeView pic6, //
            int maxWidth, float photoMargin) {
        if (TextUtils.isEmpty(imageUrls)) {
            return;
        }
        String[] picUrls = imageUrls.split(",");
        final int count = picUrls.length;
        if (count > 0 && !TextUtils.isEmpty(picUrls[0])) {
            pic_parent.setVisibility(View.VISIBLE);
            if (count == 1) {
                pic.setVisibility(View.VISIBLE);
                setImageUrl(pic, picUrls[0], TheLConstants.MOMENT_PIC_BIG_SIZE, TheLConstants.MOMENT_PIC_BIG_SIZE);
            } else {
                pic1.setVisibility(View.VISIBLE);
                setImageUrl(pic1, picUrls[0], TheLConstants.MOMENT_PIC_BIG_SIZE, TheLConstants.MOMENT_PIC_BIG_SIZE);
                pic2.setVisibility(View.VISIBLE);
                setImageUrl(pic2, picUrls[1], TheLConstants.MOMENT_PIC_SMALL_SIZE, TheLConstants.MOMENT_PIC_SMALL_SIZE);
                if (picUrls.length > 2) {
                    pic3.setVisibility(View.VISIBLE);
                    setImageUrl(pic3, picUrls[2], TheLConstants.MOMENT_PIC_SMALL_SIZE, TheLConstants.MOMENT_PIC_SMALL_SIZE);
                    if (picUrls.length > 3) {
                        pic4.setVisibility(View.VISIBLE);
                        setImageUrl(pic4, picUrls[3], TheLConstants.MOMENT_PIC_SMALL_SIZE, TheLConstants.MOMENT_PIC_SMALL_SIZE);
                        if (picUrls.length > 4) {
                            pic5.setVisibility(View.VISIBLE);
                            setImageUrl(pic5, picUrls[4], TheLConstants.MOMENT_PIC_SMALL_SIZE, TheLConstants.MOMENT_PIC_SMALL_SIZE);
                            if (picUrls.length > 5) {
                                pic6.setVisibility(View.VISIBLE);
                                setImageUrl(pic6, picUrls[5], TheLConstants.MOMENT_PIC_SMALL_SIZE, TheLConstants.MOMENT_PIC_SMALL_SIZE);
                            }
                        }
                    }
                }
                setPicsLayoutParams(pic1, pic2, pic3, pic4, pic5, pic6, count, maxWidth, photoMargin);
            }
        }

    }

    /**
     * 设置图片显示参数
     *
     * @param pic1
     * @param pic2
     * @param pic3
     * @param pic4
     * @param pic5
     * @param pic6
     * @param count
     * @param maxWidth
     * @param photoMargin
     */
    public static void setPicsLayoutParams(SimpleDraweeView pic1, SimpleDraweeView pic2, SimpleDraweeView pic3, SimpleDraweeView pic4, SimpleDraweeView pic5, SimpleDraweeView pic6, int count, int maxWidth, float photoMargin) {
        final int bigSize = (int) (maxWidth - photoMargin * 2);
        final int smallSize = (int) (bigSize - photoMargin) / 2;
        final int tinySize = (int) ((bigSize - photoMargin * 2) / 3);
        if (count == 2 || count == 5) {
            final RelativeLayout.LayoutParams smallSizeParamsLeft = new RelativeLayout.LayoutParams(smallSize, smallSize);
            final RelativeLayout.LayoutParams smallSizeParamsRight = new RelativeLayout.LayoutParams(smallSize, smallSize);
            smallSizeParamsRight.leftMargin = (int) photoMargin;
            smallSizeParamsRight.addRule(RelativeLayout.RIGHT_OF, pic1.getId());
            pic1.setLayoutParams(smallSizeParamsLeft);
            pic2.setLayoutParams(smallSizeParamsRight);
            if (count == 5) {
                final RelativeLayout.LayoutParams params3 = new RelativeLayout.LayoutParams(tinySize, tinySize);
                params3.addRule(RelativeLayout.BELOW, pic1.getId());
                params3.topMargin = (int) photoMargin;
                pic3.setLayoutParams(params3);
                final RelativeLayout.LayoutParams params4 = new RelativeLayout.LayoutParams(tinySize, tinySize);
                params4.addRule(RelativeLayout.BELOW, pic1.getId());
                params4.addRule(RelativeLayout.RIGHT_OF, pic3.getId());
                params4.topMargin = (int) photoMargin;
                params4.leftMargin = (int) photoMargin;
                pic4.setLayoutParams(params4);
                final RelativeLayout.LayoutParams params5 = new RelativeLayout.LayoutParams(tinySize, tinySize);
                params5.addRule(RelativeLayout.BELOW, pic1.getId());
                params5.addRule(RelativeLayout.RIGHT_OF, pic4.getId());
                params5.topMargin = (int) photoMargin;
                params5.leftMargin = (int) photoMargin;
                pic5.setLayoutParams(params5);
            }
        } else if (count == 3) {
            final RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, smallSize);
            pic1.setLayoutParams(params1);
            final RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(smallSize, smallSize);
            params2.topMargin = (int) photoMargin;
            params2.addRule(RelativeLayout.BELOW, pic1.getId());
            final RelativeLayout.LayoutParams params3 = new RelativeLayout.LayoutParams(smallSize, smallSize);
            params3.leftMargin = (int) photoMargin;
            params3.topMargin = (int) photoMargin;
            params3.addRule(RelativeLayout.BELOW, pic1.getId());
            params3.addRule(RelativeLayout.RIGHT_OF, pic2.getId());
            pic2.setLayoutParams(params2);
            pic3.setLayoutParams(params3);
        } else if (count == 4) {
            final RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(smallSize, smallSize);
            pic1.setLayoutParams(params1);
            final RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(smallSize, smallSize);
            params2.leftMargin = (int) photoMargin;
            params2.addRule(RelativeLayout.RIGHT_OF, pic1.getId());
            pic2.setLayoutParams(params2);
            final RelativeLayout.LayoutParams params3 = new RelativeLayout.LayoutParams(smallSize, smallSize);
            params3.topMargin = (int) photoMargin;
            params3.addRule(RelativeLayout.BELOW, pic1.getId());
            pic3.setLayoutParams(params3);
            final RelativeLayout.LayoutParams params4 = new RelativeLayout.LayoutParams(smallSize, smallSize);
            params4.topMargin = (int) photoMargin;
            params4.leftMargin = (int) photoMargin;
            params4.addRule(RelativeLayout.BELOW, pic2.getId());
            params4.addRule(RelativeLayout.RIGHT_OF, pic3.getId());
            pic4.setLayoutParams(params4);
        } else if (count == 6) {
            int pic1Size = (int) (tinySize * 2 + photoMargin);
            final RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(pic1Size, pic1Size);
            pic1.setLayoutParams(params1);
            final RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(tinySize, tinySize);
            params2.addRule(RelativeLayout.RIGHT_OF, pic1.getId());
            params2.leftMargin = (int) photoMargin;
            pic2.setLayoutParams(params2);
            final RelativeLayout.LayoutParams params3 = new RelativeLayout.LayoutParams(tinySize, tinySize);
            params3.addRule(RelativeLayout.RIGHT_OF, pic1.getId());
            params3.addRule(RelativeLayout.BELOW, pic2.getId());
            params3.leftMargin = (int) photoMargin;
            params3.topMargin = (int) photoMargin;
            pic3.setLayoutParams(params3);
            final RelativeLayout.LayoutParams params4 = new RelativeLayout.LayoutParams(tinySize, tinySize);
            params4.addRule(RelativeLayout.BELOW, pic1.getId());
            params4.topMargin = (int) photoMargin;
            pic4.setLayoutParams(params4);
            final RelativeLayout.LayoutParams params5 = new RelativeLayout.LayoutParams(tinySize, tinySize);
            params5.addRule(RelativeLayout.RIGHT_OF, pic4.getId());
            params5.addRule(RelativeLayout.BELOW, pic1.getId());
            params5.leftMargin = (int) photoMargin;
            params5.topMargin = (int) photoMargin;
            pic5.setLayoutParams(params5);
            final RelativeLayout.LayoutParams params6 = new RelativeLayout.LayoutParams(tinySize, tinySize);
            params6.addRule(RelativeLayout.RIGHT_OF, pic5.getId());
            params6.addRule(RelativeLayout.BELOW, pic1.getId());
            params6.leftMargin = (int) photoMargin;
            params6.topMargin = (int) photoMargin;
            pic6.setLayoutParams(params6);
        }

    }

    /**
     * 设置网络图片
     *
     * @param view
     * @param url
     * @param width
     * @param height
     */
    public static void setImageUrl(SimpleDraweeView view, String url, float width, float height) {
        view.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(url, width, height))).build()).setAutoPlayAnimations(true).build());
    }

    /**
     * 点赞接口调用
     *
     * @param like
     * @param momentId
     * @param likeButtonView
     * @param animView
     * @param position
     * @param moment
     * @param momentOperateIn
     */
    private static void likeMoment(final boolean like, final String momentId, final LikeButtonView likeButtonView, final View animView, final int position, final Object moment, final MomentOperateIn momentOperateIn) {
        if (like) {//点赞
            BusinessUtils.playSound(R.raw.sound_emoji);

            MomentUtils.likeMoment(momentId);

            //            new RequestBussiness().likeMoment(new OneRequestNetworkHelper(new UIDataListener() {
            //                @Override
            //                public void onDataChanged(RequestCoreBean rcb) {
            //                    Utils.Log("refresh", "likeMoment:data=" + rcb.responseDataStr + ",logType=" + rcb.requestType);
            //                    // 成功数据
            //                    if (RequestConstants.RESPONSE_SUCCESS == rcb.responseValue && null != rcb.responseDataObj) {
            //                        if (RequestConstants.MOMENTS_WINK_MOMENT.equals(rcb.requestType)) {// 点赞
            //                            likeMomentOrNot(like, momentId, likeButtonView, animView, position, moment, momentOperateIn);
            //                        }
            //                    } else {
            //                        NetworkUtils.handleErrorResponse(rcb, true);
            //                    }
            //                }
            //
            //                @Override
            //                public void onErrorHappened(VolleyError error, RequestCoreBean rcb) {
            //                    NetworkUtils.handleErrorMessage(error);
            //                }
            //            }), momentId);
        } else {//取消点赞
            //            new RequestBussiness().unwinkMoment(new OneRequestNetworkHelper(new UIDataListener() {
            //                @Override
            //                public void onDataChanged(RequestCoreBean rcb) {
            //                    // 成功数据
            //                    if (RequestConstants.RESPONSE_SUCCESS == rcb.responseValue && null != rcb.responseDataObj) {
            //                        if (RequestConstants.MOMENTS_UNWINK_MOMENT.equals(rcb.requestType)) {// 撤销点赞
            //                            likeMomentOrNot(like, momentId, likeButtonView, animView, position, moment, momentOperateIn);
            //                        }
            //                    } else {
            //                        NetworkUtils.handleErrorResponse(rcb, true);
            //                    }
            //                }
            //
            //                @Override
            //                public void onErrorHappened(VolleyError error, RequestCoreBean rcb) {
            //                    NetworkUtils.handleErrorMessage(error);
            //                }
            //            }), momentId);
            MomentUtils.unwinkMoment(momentId);
        }
    }

    /**
     * 更多操作 包括 删除，收藏，分享，分享海报，巨擘等功能（自己的日志可删除不能举报，不是自己的日志可举报不可以删除）
     *
     * @param momentId
     * @param optsMoreButton
     * @param position
     * @param mOnClickListener
     * @param momentBean
     * @param momentOperateIn
     */
    public static void clickOptsMore(final Activity activity, final String momentId, View optsMoreButton, final int position, View.OnClickListener mOnClickListener, final Object momentBean, String userId, final MomentOperateIn momentOperateIn) {
        if (activity == null || TextUtils.isEmpty(momentId) || "0".equals(momentId) || mOnClickListener == null) {
            return;
        }
        ViewUtils.preventViewMultipleClick(optsMoreButton, 1000);
        final DialogUtils dialogUtils = new DialogUtils();
        final boolean isCollect = !BusinessUtils.isCollected(momentId);//是否被收藏
        //如果被收藏，则显示取消收藏
        String collectMsg = isCollect ? TheLApp.getContext().getString(R.string.collection) : TheLApp.getContext().getString(R.string.collection_cancel);//收藏/取消收藏 文案
        final String myUserId = ShareFileUtils.getString(ShareFileUtils.ID, "");
        if (myUserId.equals(userId)) {//如果是我的日志
            DialogUtil.getInstance().showSelectionDialog(activity, new String[]{TheLApp.getContext().getString(R.string.info_delete),
                    collectMsg,
                    TheLApp.getContext().getString(R.string.post_moment_share_to),
                    TheLApp.getContext().getString(R.string.share_poster)}, new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
                    DialogUtil.getInstance().closeDialog();
                    switch (pos) {
                        case 0://删除
                            deleteMoment(activity, momentId, position, momentBean, momentOperateIn);
                            //                            MomentDeleteUtils.deleteMoment(momentId);
                            break;
                        case 1://收藏
                            collect(isCollect, momentId, position, momentBean, momentOperateIn);
                            break;
                        case 2://分享
                            share(MomentOperateIn.SHARE_TYPE_URL, momentId, position, momentBean, momentOperateIn);
                            break;
                        case 3://分享海报
                            share(MomentOperateIn.SHARE_TYPE_POSTER, momentId, position, momentBean, momentOperateIn);
                            break;
                        default:
                            break;
                    }
                }
            }, true, 1, null);
        } else {
            DialogUtil.getInstance().showSelectionDialog(activity, new String[]{collectMsg,
                    TheLApp.getContext().getString(R.string.post_moment_share_to),
                    TheLApp.getContext().getString(R.string.share_poster),
                    TheLApp.getContext().getString(R.string.info_report)}, new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
                    DialogUtil.getInstance().closeDialog();
                    switch (pos) {
                        case 0://2.17 收藏
                            collect(isCollect, momentId, position, momentBean, momentOperateIn);
                            break;
                        case 1://分享
                            share(MomentOperateIn.SHARE_TYPE_URL, momentId, position, momentBean, momentOperateIn);
                            break;
                        case 2://分享海报
                            share(MomentOperateIn.SHARE_TYPE_POSTER, momentId, position, momentBean, momentOperateIn);
                            break;
                        case 3://举报
                            reportMoment(momentId, position, momentBean, momentOperateIn);
                            //                            MomentReportUtils.sendReportMomentBroad(momentId);
                            break;

                        default:
                            break;
                    }
                }
            }, false, 2, null);
        }
    }

    /**
     * 日志分享
     *
     * @param type       0:SHARE_TYPE_URL 分享链接  1:SHARE_TYPE_POSTER 分享海报
     * @param momentBean
     */
    private static void share(int type, String momentId, int position, Object momentBean, MomentOperateIn momentOperateIn) {
        if (momentBean == null || momentOperateIn == null) {
            return;
        }
        momentOperateIn.shareMoment(type, momentId, position, momentBean);
    }

    /**
     * 举报日志
     *
     * @param momentId   momentId
     * @param position
     * @param momentBean
     */
    private static void reportMoment(String momentId, int position, Object momentBean, MomentOperateIn momentOperateIn) {
        if (momentOperateIn != null) {
            momentOperateIn.reportMoment(momentId, position, momentBean);
        }
    }

    /**
     * 收藏某篇日志（或者取消收藏）
     *
     * @param isCollect true:收藏，false:收藏
     */
    private static void collect(boolean isCollect, final String momentId, int position, Object momentBean, MomentOperateIn momentOperateIn) {

        Map<String, String> map = new HashMap<>();

        if (isCollect) {//如果是收藏

            map.put(RequestConstants.FAVORITE_COUNT, momentId);
            map.put(RequestConstants.FAVORITE_TYPE, RequestConstants.FAVORITE_TYPE_MOM);

            Flowable<BaseDataBean> flowable = DefaultRequestService.createMomentRequestService().getFavoriteCreate(MD5Utils.generateSignatureForMap(map));
            flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
                @Override
                public void onNext(BaseDataBean data) {
                    super.onNext(data);
                    BusinessUtils.addCollectMoment(momentId);
                }

                @Override
                public void onComplete() {
                    super.onComplete();
                    DialogUtil.showToastShort(TheLApp.context, getString(R.string.collection_success));

                }
            });

        } else {//取消收藏

            map.put(RequestConstants.FAVORITE_ID, "");
            map.put(RequestConstants.FAVORITE_COUNT, momentId);
            map.put(RequestConstants.FAVORITE_TYPE, RequestConstants.FAVORITE_TYPE_MOM);

            Flowable<BaseDataBean> flowable = DefaultRequestService.createMomentRequestService().deleteFavoriteMoment(MD5Utils.generateSignatureForMap(map));
            flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
                @Override
                public void onNext(BaseDataBean data) {
                    super.onNext(data);
                    BusinessUtils.deleteCollectMoment(momentId);
                }
            });
        }
    }

    /**
     * 删除某篇日志
     *
     * @param momentId        日志位置
     * @param position        日志所在id
     * @param momentBean      日志
     * @param momentOperateIn 接口
     */
    private static void deleteMoment(Activity activity, final String momentId, final int position, final Object momentBean, final MomentOperateIn momentOperateIn) {
        if (activity == null || momentOperateIn == null) {
            return;
        }
        DialogUtil.showConfirmDialog(activity, null, TheLApp.getContext().getString(R.string.moments_delete_moment_confirm), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                MomentDeleteUtils.deleteMoment(momentId);
               /* if (!TextUtils.isEmpty(momentId)) {
                    momentOperateIn.showLoadingDialog(false);

                    Map<String, String> map = new HashMap<>();
                    map.put(RequestConstants.I_MOMENTS_ID, momentId);
                    Flowable<BaseDataBean> flowable = DefaultRequestService.createMomentRequestService().deleteMoment(MD5Utils.generateSignatureForMap(map));
                    flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
                        @Override public void onNext(BaseDataBean data) {
                            super.onNext(data);
                            momentOperateIn.closeLoadingDialog();
                            momentOperateIn.deleteMoment(momentId, position, momentBean);
                        }

                        @Override
                        public void onError(Throwable t) {
                            super.onError(t);
                            momentOperateIn.closeLoadingDialog();
                        }
                    });

                }*/
            }
        });
    }

}
