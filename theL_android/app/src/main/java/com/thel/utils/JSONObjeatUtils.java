package com.thel.utils;

import com.thel.bean.moments.MomentsBean;

import org.json.JSONObject;

public class JSONObjeatUtils {
    public static MomentsBean fromJson(String text) {
        MomentsBean momentsBean = new MomentsBean();
        try {

            JSONObject tempobj = new JSONObject(text);

            momentsBean.topicFlag = JsonUtils.getInt(tempobj, "topicFlag", 0);
            momentsBean.topicId = JsonUtils.getString(tempobj, "topicId", "");
            momentsBean.topicName = JsonUtils.getString(tempobj, "topicName", "");
            momentsBean.topicColor = JsonUtils.getString(tempobj, "topicColor", "");
            momentsBean.notReadNum = JsonUtils.getInt(tempobj, "notReadNum", 0);
            momentsBean.userId = JsonUtils.getInt(tempobj, "userId", 0);
            momentsBean.nickname = JsonUtils.getString(tempobj, "nickname", "");
            momentsBean.avatar = JsonUtils.getString(tempobj, "avatar", "");
            momentsBean.commentNum = JsonUtils.getInt(tempobj, "commentNum", 0);
            momentsBean.winkNum = JsonUtils.getInt(tempobj, "winkNum", 0);
            momentsBean.winkFlag = JsonUtils.getInt(tempobj, "winkFlag", 0);
            momentsBean.followerStatus = JsonUtils.getInt(tempobj, "followerStatus", 0);
            momentsBean.momentsId = JsonUtils.getString(tempobj, "momentsId", "");
            momentsBean.userBoardTop = JsonUtils.getInt(tempobj, "userBoardTop", 0);
            momentsBean.myself = JsonUtils.getInt(tempobj, "myself", 0);
            momentsBean.momentsTime = JsonUtils.getString(tempobj, "momentsTime", "");
            momentsBean.momentsType = JsonUtils.getString(tempobj, "momentsType", "");
            momentsBean.themeClass = JsonUtils.getString(tempobj, "themeClass", "");

            try {
                momentsBean.shareTo = JsonUtils.getInt(tempobj, "shareTo", 0);
            } catch (Exception var12) {
                var12.printStackTrace();
            }

            momentsBean.secret = JsonUtils.getInt(tempobj, "secret", 0);
            momentsBean.momentsText = JsonUtils.getString(tempobj, "momentsText", "");
            momentsBean.thumbnailUrl = JsonUtils.getString(tempobj, "thumbnailUrl", "");
            momentsBean.imageUrl = JsonUtils.getString(tempobj, "imageUrl", "");
            momentsBean.tag = JsonUtils.getString(tempobj, "tag", "");
            momentsBean.songId = JsonUtils.getLong(tempobj, "songId", 0L);
            momentsBean.songName = JsonUtils.getString(tempobj, "songName", "");
            momentsBean.artistName = JsonUtils.getString(tempobj, "artistName", "");
            momentsBean.albumName = JsonUtils.getString(tempobj, "albumName", "");
            momentsBean.albumLogo100 = JsonUtils.getString(tempobj, "albumLogo100", "");
            momentsBean.albumLogo444 = JsonUtils.getString(tempobj, "albumLogo444", "");
            momentsBean.songLocation = JsonUtils.getString(tempobj, "songLocation", "");
            momentsBean.toURL = JsonUtils.getString(tempobj, "toURL", "");
            momentsBean.distance = JsonUtils.getString(tempobj, "distance", "");
            momentsBean.commentNumThisPage = JsonUtils.getInt(tempobj, "commentNumThisPage", 0);
            momentsBean.hideFlag = JsonUtils.getInt(tempobj, "hideFlag", 0);
            momentsBean.hideNum = JsonUtils.getInt(tempobj, "hideNum", 0);
            momentsBean.hideType = JsonUtils.getInt(tempobj, "hideType", 1);
            momentsBean.themeTagName = JsonUtils.getString(tempobj, "themeTagName", "");
            momentsBean.themeParticipates = JsonUtils.getLong(tempobj, "participates", 0L);
            momentsBean.videoUrl = JsonUtils.getString(tempobj, "videoUrl", "");
            momentsBean.playTime = JsonUtils.getInt(tempobj, "playTime", 0);
            momentsBean.mediaSize = JsonUtils.getLong(tempobj, "mediaSize", 0L);
            momentsBean.pixelWidth = JsonUtils.getInt(tempobj, "pixelWidth", 0);
            momentsBean.pixelHeight = JsonUtils.getInt(tempobj, "pixelHeight", 0);
            momentsBean.playCount = JsonUtils.getInt(tempobj, "playCount", 0);
            momentsBean.liveId = JsonUtils.getString(tempobj, "liveId", "");
            momentsBean.cardUserId = JsonUtils.getLong(tempobj, "cardUserId", 0L);
            momentsBean.cardNickName = JsonUtils.getString(tempobj, "cardNickName", "");
            momentsBean.cardAvatar = JsonUtils.getString(tempobj, "cardAvatar", "");
            momentsBean.cardAge = JsonUtils.getInt(tempobj, "cardAge", 0);
            momentsBean.cardHeight = JsonUtils.getInt(tempobj, "cardHeight", 0);
            momentsBean.cardWeight = JsonUtils.getInt(tempobj, "cardWeight", 0);
            momentsBean.cardIntro = JsonUtils.getString(tempobj, "cardIntro", "");
            momentsBean.cardAffection = JsonUtils.getString(tempobj, "cardAffection", "");
            momentsBean.momentsNum = JsonUtils.getInt(tempobj, "momentsNum", 0);
            momentsBean.joinCount = JsonUtils.getInt(tempobj, "joinCount", 0);


            momentsBean.liveStatus = JsonUtils.getInt(tempobj, "liveStatus", -1);
            momentsBean.userName = JsonUtils.getString(tempobj, "userName", "");
            momentsBean.parentId = JsonUtils.getString(tempobj, "parentId", "");
            momentsBean.isLandscape = JsonUtils.getInt(tempobj, "isLandscape", 0);

        } catch (Exception var15) {
            if (var15.getMessage() != null) {
                L.e(MomentsBean.class.getName(), var15.getMessage());
            }
            var15.printStackTrace();
            return null;
        }
        return momentsBean;
    }
}
