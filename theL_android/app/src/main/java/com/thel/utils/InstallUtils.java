package com.thel.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import androidx.core.content.FileProvider;

import com.thel.BuildConfig;
import com.thel.constants.TheLConstants;

import java.io.File;

public class InstallUtils {

    public static void installApk(Context context, String path) {

        if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            boolean installAllowed = context.getPackageManager().canRequestPackageInstalls();

            if (installAllowed) {
                openFile(context, new File(path));
            } else {
                Intent intent = new Intent();
                intent.putExtra("apkUrl", path);
                intent.setAction(TheLConstants.BROADCAST_ACTION_APK_DOWNLOADED);
                context.sendBroadcast(intent);
            }

        } else {
            openFile(context, new File(path));
        }

    }

    public static Intent openFile(Context context, File file) {

        Intent intent;

        if (Build.VERSION.SDK_INT > 23) {
            Uri uri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".install", file);
            intent = new Intent(Intent.ACTION_VIEW).setDataAndType(uri, "application/vnd.android.package-archive");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            context.startActivity(intent);
        } else {
            intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
            context.startActivity(intent);
        }

        return intent;

    }
}
