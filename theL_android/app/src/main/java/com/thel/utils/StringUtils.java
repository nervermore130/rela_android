package com.thel.utils;

import android.content.Context;
import androidx.annotation.NonNull;
import android.text.Spannable;
import android.text.SpannableString;

import com.thel.app.TheLApp;
import com.thel.modules.main.messages.bean.MsgBean;
import com.thel.modules.main.messages.span.StrangerMsgClickSpan;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by kevin on 2017/9/18.
 */

public class StringUtils {
    /**
     * 把map转为String
     *
     * @param msgsMap
     * @return
     */
    public static String mapToString(Map<String, ArrayList<MsgBean>> msgsMap) {
        java.util.Map.Entry entry;
        StringBuffer sb = new StringBuffer();
        for (Iterator iterator = msgsMap.entrySet().iterator(); iterator.hasNext(); ) {
            entry = (java.util.Map.Entry) iterator.next();
            sb.append(entry.getKey().toString()).append("'").append(null == entry.getValue() ? "" : entry.getValue().toString()).append(iterator.hasNext() ? "^" : "");
        }
        return sb.toString();
    }

    /**
     * 屏蔽或者举报
     *
     * @param string
     * @param black
     * @param report
     * @return
     */
    public static SpannableString createStrangerMsgSpannableString(Context activity, String userId, String string, String black, String report) {
        final SpannableString spannableString = new SpannableString(string);
        final int start1 = string.lastIndexOf(black);
        final int end1 = start1 + black.length();
        final int start2 = string.lastIndexOf(report);
        final int end2 = start2 + report.length();
        if (start1 >= 0 && end1 >= 0 && start2 >= 0 && end2 >= 0) {
            spannableString.setSpan(new StrangerMsgClickSpan(activity, userId, StrangerMsgClickSpan.TYPE_BLOCK), start1, end1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannableString.setSpan(new StrangerMsgClickSpan(activity, userId, StrangerMsgClickSpan.TYPE_REPORT), start2, end2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        return spannableString;
    }

    @NonNull
    public static String getString(int resId) {
        return TheLApp.getContext().getString(resId);
    }

    public static final String getString(int resId, Object... formatArgs) {
        return TheLApp.getContext().getString(resId, formatArgs);
    }

    /*  */

    /***
     * 谁看过我提示语，关闭通知
     * *//*
    public static SpannableString createCloseNotification(Context context, String content, String string1, String string2) {
        final SpannableString spannableString = new SpannableString(content);
        final int start1 = content.lastIndexOf(string1);
        final int end1 = start1 + string1.length();
        final int start2 = content.lastIndexOf(string2);
        final int end2 = start2 + string2.length();
        spannableString.setSpan(new );
    }*/
    public static CharSequence getText(int resId) {
        return TheLApp.getContext().getText(resId);
    }
}
