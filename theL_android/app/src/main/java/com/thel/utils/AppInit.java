package com.thel.utils;


import android.content.Context;
import android.util.DisplayMetrics;


public class AppInit {


    private static AppInit instance = new AppInit();

    private Context applicationContext;

    public static long openMainActivityTime = -1;

    /**
     * 私有化构造函数，这样就不能在别的地方new对象了。保证单例
     */
    private AppInit() {

    }

    /**
     * 屏幕工具
     */
    public static DisplayMetrics displayMetrics;

    public static AppInit getInstance() {
        return instance;
    }

    /**
     * 初始化一些参数，和线程启动
     *
     * @param context
     */
    public void init(Context context) {
        if (applicationContext == null) {
            displayMetrics = context.getResources().getDisplayMetrics();
            applicationContext = context;
        }
    }

    public Context getApplicationContext() {
        return applicationContext;
    }

    public void initMainActivity() {

        openMainActivityTime = System.currentTimeMillis();

    }

}
