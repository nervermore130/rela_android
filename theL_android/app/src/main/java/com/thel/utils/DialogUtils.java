package com.thel.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.constants.TheLConstants;
import com.thel.growingio.GIOShareTrackBean;
import com.thel.growingio.GrowingIoConstant;
import com.thel.modules.preview_image.SelectionDialogAdapter;
import com.thel.ui.adapter.ShareGridAdapter;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by liuyun on 2017/10/26.
 */

public class DialogUtils {

    protected static Dialog mDialog;

    /**
     * 分享框
     */
    public void showShareDialog(final Activity context, final String title, final String shareTitle, final String shareContent, final String singleTitle, final String url, final String logo, boolean canOpenByBrowser, boolean canCopyUrl, final CallbackManager facebookCallbackMgr, final FacebookCallback facebookCallback, final boolean recordEvent, final GIOShareTrackBean trackBean, final DialogInterface.OnDismissListener... onDismissListeners) {
        if (context == null) {
            return;
        }
        mDialog = new Dialog(context, R.style.CustomDialogBottom);
        mDialog.show();

        LayoutInflater flater = LayoutInflater.from(TheLApp.getContext());
        View view = flater.inflate(R.layout.layout_share, null);
        mDialog.setContentView(view);
        GridView gridView = view.findViewById(R.id.gridView);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (onDismissListeners.length > 0)// 只在直播页观众分享的时候记这个事件，为了计算观众主动取消的次数
                    MobclickAgent.onEvent(context, "audience_go_to_share");
                if (mDialog != null && mDialog.isShowing()) {
                    mDialog.dismiss();
                }
                try {
                    final UMShareListener umShareListener = new UMShareListener() {

                        @Override
                        public void onStart(SHARE_MEDIA share_media) {

                        }

                        @Override
                        public void onResult(SHARE_MEDIA platform) {
                            if (recordEvent)
                                MobclickAgent.onEvent(context, "share_succeeded");
                        }

                        @Override
                        public void onError(SHARE_MEDIA platform, Throwable t) {
                            if (t != null && t.getMessage() != null) {
                                DialogUtil.showToastShort(context, t.getMessage());
                            }
                        }

                        @Override
                        public void onCancel(SHARE_MEDIA platform) {
                            DialogUtil.showToastShort(context, TheLApp.getContext().getString(R.string.my_circle_requests_act_canceled));
                        }
                    };
                    switch (position) {
                        case 0:// 微信朋友圈
                            MobclickAgent.onEvent(context, "start_share");
                            // 微信的分享图片大小要做特别的裁剪，否则有些大图在微信中显示不出
                            // new ShareAction(context).setPlatform(SHARE_MEDIA.WEIXIN_CIRCLE).withMedia(new UMImage(TheLApp.getContext(), ImageUtils.buildNetPictureUrl(logo, TheLConstants.WX_SHARE_IMAGE_SIZE, TheLConstants.WX_SHARE_IMAGE_SIZE))).withTargetUrl(url).withTitle(singleTitle).withText(singleTitle).setCallback(umShareListener).share();
                            UMShareUtils.share(context, SHARE_MEDIA.WEIXIN_CIRCLE, new UMImage(TheLApp.getContext(), ImageUtils.buildNetPictureUrl(logo, TheLConstants.WX_SHARE_IMAGE_SIZE, TheLConstants.WX_SHARE_IMAGE_SIZE)), url, singleTitle, singleTitle, umShareListener, trackBean);
                            break;
                        case 1:// 微信消息
                            MobclickAgent.onEvent(context, "start_share");
                            // 微信的分享图片大小要做特别的裁剪，否则有些大图在微信中显示不出
                            //  new ShareAction(context).setPlatform(SHARE_MEDIA.WEIXIN).withMedia(new UMImage(TheLApp.getContext(), ImageUtils.buildNetPictureUrl(logo, TheLConstants.WX_SHARE_IMAGE_SIZE, TheLConstants.WX_SHARE_IMAGE_SIZE))).withTargetUrl(url).withTitle(shareTitle).withText(shareContent).setCallback(umShareListener).share();
                            UMShareUtils.share(context, SHARE_MEDIA.WEIXIN, new UMImage(TheLApp.getContext(), ImageUtils.buildNetPictureUrl(logo, TheLConstants.WX_SHARE_IMAGE_SIZE, TheLConstants.WX_SHARE_IMAGE_SIZE)), url, shareTitle, shareContent, umShareListener, trackBean);
                            break;
                        case 2:// 微博分享
                            MobclickAgent.onEvent(context, "start_share");
                            //  new ShareAction(context).setPlatform(SHARE_MEDIA.SINA).withMedia(new UMImage(TheLApp.getContext(), logo)).withTargetUrl(url).withTitle(singleTitle).withText(singleTitle).setCallback(umShareListener).share();
                            UMShareUtils.share(context, SHARE_MEDIA.SINA, new UMImage(TheLApp.getContext(), logo), url, singleTitle, singleTitle, umShareListener, trackBean);

                            break;
                        case 3:// QQ空间
                            // new ShareAction(context).setPlatform(SHARE_MEDIA.QZONE).withMedia(new UMImage(TheLApp.getContext(), logo)).withTargetUrl(url).withTitle(shareTitle).withText(shareContent).setCallback(umShareListener).share();
                            UMShareUtils.share(context, SHARE_MEDIA.QZONE, new UMImage(TheLApp.getContext(), logo), url, shareTitle, shareContent, umShareListener, trackBean);
                            break;
                        case 4:// Facebook
                            MobclickAgent.onEvent(context, "start_share");
                            if (ShareDialog.canShow(ShareLinkContent.class)) {
                                ShareLinkContent contentS = new ShareLinkContent.Builder().setContentUrl(Uri.parse(url)).setContentTitle(shareTitle).setContentDescription(shareContent).setImageUrl(Uri.parse(logo)).build();
                                ShareDialog shareDialog = new ShareDialog(context);
                                if (facebookCallbackMgr != null && facebookCallback != null)
                                    shareDialog.registerCallback(facebookCallbackMgr, facebookCallback);
                                shareDialog.show(contentS);
                                if (trackBean != null) {
                                    trackBean.shareBean.shareType = GrowingIoConstant.TYPE_FACEBOOK;
                                    GrowingIOUtil.shareTrack(trackBean);
                                }
                            }
                            break;
                        case 5:// QQ消息
                            MobclickAgent.onEvent(context, "start_share");
                            //  new ShareAction(context).setPlatform(SHARE_MEDIA.QQ).withMedia(new UMImage(TheLApp.getContext(), logo)).withTargetUrl(url).withTitle(shareTitle).withText(shareContent).setCallback(umShareListener).share();
                            UMShareUtils.share(context, SHARE_MEDIA.QQ, new UMImage(TheLApp.getContext(), logo), url, shareTitle, shareContent, umShareListener, trackBean);
                            break;
                        case 6:// 复制链接
                            DeviceUtils.copyToClipboard(TheLApp.getContext(), url);
                            DialogUtil.showToastShort(context, context.getString(R.string.info_copied));
                            break;
                        case 7:
                            Intent intent = new Intent();
                            intent.setAction("android.intent.action.VIEW");
                            Uri uri = Uri.parse(url);
                            intent.setData(uri);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            TheLApp.getContext().startActivity(intent);
                            break;
                        default:
                            break;
                    }
                } catch (Exception e) {
                }
            }
        });
        ((TextView) view.findViewById(R.id.title)).setText(title);
        List<Integer> images = new ArrayList<Integer>();
        images.add(R.drawable.btn_share_moments_s);
        images.add(R.drawable.btn_share_wx_s);
        images.add(R.drawable.btn_share_sina_s);
        images.add(R.drawable.btn_share_qzone_s);
        images.add(R.drawable.btn_share_fb_s);
        images.add(R.drawable.btn_share_qq_s);
        List<String> texts = new ArrayList<String>();
        texts.add(TheLApp.getContext().getString(R.string.share_moments));
        texts.add(TheLApp.getContext().getString(R.string.share_wx));
        texts.add(TheLApp.getContext().getString(R.string.share_sina));
        texts.add(TheLApp.getContext().getString(R.string.share_qzone));
        texts.add(TheLApp.getContext().getString(R.string.share_fb));
        texts.add(TheLApp.getContext().getString(R.string.share_qq));
        if (canCopyUrl) {
            images.add(R.drawable.btn_share_link_s);
            texts.add(TheLApp.getContext().getString(R.string.copy_the_link));
        }
        if (canOpenByBrowser) {
            images.add(R.drawable.btn_share_browser_s);
            texts.add(TheLApp.getContext().getString(R.string.webview_activity_open_in_browser));
        }
        gridView.setAdapter(new ShareGridAdapter(images, texts, gridView));
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.gravity = Gravity.BOTTOM;
        lp.width = context.getResources().getDisplayMetrics().widthPixels; // 设置宽度
        mDialog.getWindow().setAttributes(lp);
        mDialog.setCanceledOnTouchOutside(true);
        if (onDismissListeners.length > 0)
            mDialog.setOnDismissListener(onDismissListeners[0]);
    }

    /**
     * 自定义选择控件
     *
     * @param context
     * @param width    dialog的宽度（与屏幕宽度的占比）
     * @param array    选项字符串数组
     * @param listener
     * @return
     */
    public static void showSelectionDialogWide(Activity context, float width, String[] array, AdapterView.OnItemClickListener listener, boolean setLastItemColor, int setLastItem, DialogInterface.OnDismissListener dismissListener) {
        if (context == null) {
            return;
        }
        mDialog = new Dialog(context, R.style.CustomDialog);
        mDialog.setCancelable(true);
        mDialog.setCanceledOnTouchOutside(true);
        if (dismissListener != null) {
            mDialog.setOnDismissListener(dismissListener);
        }
        mDialog.show();
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.gravity = Gravity.CENTER;
        lp.width = (int) (context.getResources().getDisplayMetrics().widthPixels * width); // 设置宽度
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        mDialog.getWindow().setAttributes(lp);

        LayoutInflater flater = LayoutInflater.from(context);
        View view = flater.inflate(R.layout.selection_dialog, null);
        mDialog.setContentView(view);

        ListView listview = view.findViewById(R.id.listview);

        final SelectionDialogAdapter adapter = new SelectionDialogAdapter(array, setLastItemColor, setLastItem);
        listview.setAdapter(adapter);
        listview.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listview.setOnItemClickListener(listener);
    }

    public static void dismiss() {
        if (mDialog != null) {
            mDialog.dismiss();
            mDialog = null;
        }
    }

}
