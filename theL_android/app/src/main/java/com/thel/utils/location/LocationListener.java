package com.thel.utils.location;

public interface LocationListener {

    void onLocation(double lat, double lng);

    void onFailed();

}
