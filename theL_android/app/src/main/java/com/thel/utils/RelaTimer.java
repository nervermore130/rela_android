package com.thel.utils;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.thel.app.TheLApp;
import com.thel.bean.LiveInfoLogBean;
import com.thel.bean.analytics.AnalyticsBean;
import com.thel.bean.moments.MomentsCheckBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterPushImpl;
import com.thel.flutter.bridge.RfSBridgeHandlerFactory;
import com.thel.manager.ChatServiceManager;
import com.thel.modules.live.utils.LiveUtils;
import com.thel.modules.main.messages.bean.MsgBean;
import com.thel.modules.main.messages.db.MessageDataBaseAdapter;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.service.DefaultRequestService;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class RelaTimer {

    private static final String TAG = "RelaTimer";

    private static final int LOOP_DELAY_TIME_25 = 25;

    private static final int LOOP_DELAY_TIME_30 = 30;

    private static final RelaTimer mRelaTimer = new RelaTimer();

    private CompositeDisposable mCompositeDisposable;

    private Disposable loopRequestUnReadDisposable;

    private Disposable loopRequestNotPushLogDisposable;

    private RelaTimer() {

    }

    public static RelaTimer getInstance() {
        return mRelaTimer;
    }

    public void init() {

        L.d(TAG, " RelaTimer init ");

        if (mCompositeDisposable == null) {

            mCompositeDisposable = new CompositeDisposable();

        }

        stop();

        startCheckNotReadTimer();

        startCheckNotPushLiveLogTimer();

    }

    public void stop() {

        if (mCompositeDisposable != null) {
            mCompositeDisposable.clear();
        }

    }

    /**
     * 开启获取未读消息数量的接口的定时器
     */
    private void startCheckNotReadTimer() {

        if (loopRequestUnReadDisposable != null) {
            mCompositeDisposable.remove(loopRequestUnReadDisposable);
        }

        loopRequestUnReadDisposable = Flowable.interval(0, LOOP_DELAY_TIME_25, TimeUnit.SECONDS).onBackpressureDrop().subscribe(new Consumer<Long>() {
            @Override
            public void accept(Long aLong) {

                getUnReadInfo();

            }
        });

        mCompositeDisposable.add(loopRequestUnReadDisposable);

    }

    /**
     * 开启重新上传没有上传成功的直播日志的定时器
     */
    private void startCheckNotPushLiveLogTimer() {

        if (loopRequestNotPushLogDisposable != null) {
            mCompositeDisposable.remove(loopRequestNotPushLogDisposable);
        }

        loopRequestNotPushLogDisposable = Flowable.interval(0, LOOP_DELAY_TIME_30, TimeUnit.SECONDS).onBackpressureDrop().subscribe(new Consumer<Long>() {
            @Override
            public void accept(Long aLong) {

                pushLiveLog();
            }
        });

        mCompositeDisposable.add(loopRequestNotPushLogDisposable);

    }

    private void pushLiveLog() {

        if (TextUtils.isEmpty(UserUtils.getMyUserId())) {
            return;
        }

        L.d(TAG, "-------startCheckNotPushLiveLogTimer-------");

        try {

            List<AnalyticsBean> analyticsBeans = LiveInfoLogBean.getInstance().getLiveLogCache();

            L.d(TAG, " analyticsBeans ：" + analyticsBeans);

            if (analyticsBeans != null && analyticsBeans.size() > 0) {

                LiveUtils.pushLivePointLog(analyticsBeans.get(analyticsBeans.size() - 1));

                LiveInfoLogBean.getInstance().removeCache();

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void getUnReadInfo() {

        if (TextUtils.isEmpty(UserUtils.getMyUserId())) {
            return;
        }

        String cursor = ShareFileUtils.getString(ShareFileUtils.MATCH_NEW_LIKE_ME_COUNT, "");

        DefaultRequestService
                .createMsgRequestService()
                .checkMoments(cursor)
                .onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new InterceptorSubscribe<MomentsCheckBean>() {
                    @Override
                    public void onNext(MomentsCheckBean result) {
                        super.onNext(result);

                        L.d("IMainProcessCallback", " momentsCheckBean result 0 : " + result);

                        showShortBadger(result);

                        ShareFileUtils.setInt(ShareFileUtils.NEW_LIKE_NUM, result.likeMeNum);

                        ShareFileUtils.setInt(ShareFileUtils.SUM_LIKE_COUNT, result.likeCount);

                        ShareFileUtils.setInt(ShareFileUtils.NOT_REPLY_MECOUNT, result.notReplyMeCount);

                        FlutterPushImpl.Companion.setMomentsCheckBean(result);

                        FlutterPushImpl.Companion.getMomentsCheckBean().isVip = UserUtils.getUserVipLevel() > 0;

                        RfSBridgeHandlerFactory.getInstance().getFlutterPushImpl().pushNotRead();

                        if (result.requestNotReadNum > 0) {
                            MsgBean msgBean = new MsgBean();
                            msgBean.msgType = MsgBean.MSG_TYPE_REQUEST;
                            msgBean.userId = MessageDataBaseAdapter.CIRCLE_REQUEST_CHAT_USER_ID;
                            msgBean.msgTime = System.currentTimeMillis();
                            // 显示入口
                            if (result.requestNotReadNum > 0) {
                                msgBean.msgText = MsgBean.SHOW_CIRCLE_REQUEST_CHAT;
                            }
                            ChatServiceManager.getInstance().insertRequestDataToMsgTable(msgBean, result.requestNotReadNum);
                        }

                        // 发广播
                        Intent intent = new Intent();
                        intent.setAction(TheLConstants.BROADCAST_MOMENTS_CHECK_ACTION);
                        Bundle bundle = new Bundle();
                        bundle.putParcelable(TheLConstants.BUNDLE_KEY_MOMENTS_CHECK_BEAN, result);
                        bundle.putInt(TheLConstants.BUNDLE_UNREAD_MOMENT_COUNT, result.notReadMomentCount);
                        intent.putExtras(bundle);
                        TheLApp.context.sendBroadcast(intent);

                    }
                });
    }

    private void showShortBadger(MomentsCheckBean momentsCheckBean) {

        int unReadCount = 0;

        if (momentsCheckBean.notReadCommentNum > 0) {
            unReadCount += momentsCheckBean.notReadCommentNum;
        }

        if (momentsCheckBean.likeMeNum > 0 && UserUtils.getUserVipLevel() > 0) {
            unReadCount += momentsCheckBean.likeMeNum;
        }

        if (momentsCheckBean.notReadViewMeNum > 0) {
            unReadCount += momentsCheckBean.notReadViewMeNum;
        }

        int unreadMsgCount = ChatServiceManager.getInstance().getUnreadMsgCount();

        BadgeUtil.setBadgeCount(TheLApp.context, unReadCount + unreadMsgCount);

    }

}
