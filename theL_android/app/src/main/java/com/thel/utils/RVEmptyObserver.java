package com.thel.utils;

import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;

/**
 * Created by chad
 * Time 17/6/22
 * Email: wuxianchuang@foxmail.com
 * Description: TODO 实现RecyclerView EmptyView
 * adapter.registerAdapterDataObserver(observer)要在setAdapter()之后调用
 */

public class RVEmptyObserver extends RecyclerView.AdapterDataObserver {
    public static final String TAG = "RVEmptyObserver";
    private View emptyView;
    private RecyclerView recyclerView;
    /**
     * 数据为空时，如果有head或者footer，是否显示emptyview
     */
    private boolean mHeadAndEmptyEnable = true;

    public RVEmptyObserver(RecyclerView rv, View ev) {
        this.recyclerView = rv;
        this.emptyView = ev;
        checkIfEmpty();
    }

    public RVEmptyObserver(RecyclerView rv, View ev, boolean headAndEmptyEnable) {
        this.recyclerView = rv;
        this.emptyView = ev;
        this.mHeadAndEmptyEnable = headAndEmptyEnable;
        checkIfEmpty();
    }

    private void checkIfEmpty() {
        if (emptyView != null && recyclerView.getAdapter() != null) {

            int headAndFootCount = 0;
            if (recyclerView.getAdapter() instanceof HeaderAndFooterRecyclerAdapter) {
                Log.d(TAG, "Adapter Type --> HeaderAndFooterRecyclerAdapter");
                HeaderAndFooterRecyclerAdapter adapter = (HeaderAndFooterRecyclerAdapter) recyclerView.getAdapter();
                headAndFootCount = adapter.getFooterCount() + adapter.getHeaderCount();
            }

            boolean emptyViewVisible;
            if (mHeadAndEmptyEnable)
                emptyViewVisible = (headAndFootCount == 0 && recyclerView.getAdapter().getItemCount() == 0) ||
                        (headAndFootCount != 0 && recyclerView.getAdapter().getItemCount() == headAndFootCount);

            else {
                if (headAndFootCount > 0)
                    emptyViewVisible = false;
                else
                    emptyViewVisible = recyclerView.getAdapter().getItemCount() == 0;
            }

            emptyView.setVisibility(emptyViewVisible ? View.VISIBLE : View.GONE);
            recyclerView.setVisibility(emptyViewVisible ? View.GONE : View.VISIBLE);
        }
    }

    public void onChanged() {
        checkIfEmpty();
    }

    public void onItemRangeInserted(int positionStart, int itemCount) {
        checkIfEmpty();
    }

    public void onItemRangeRemoved(int positionStart, int itemCount) {
        checkIfEmpty();
    }
}
