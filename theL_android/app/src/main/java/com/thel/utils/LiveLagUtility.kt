package com.thel.utils

import kotlin.math.pow
import kotlin.math.sqrt

class LiveLagUtility constructor(private val sampleCountLimit: Int = 20, private val coefficientOfVariationThreshold: Double = 0.4) {
    private var samples: LimitedQueue = LimitedQueue(sampleCountLimit)

    fun addSample(sample: Int) {
        samples.enqueue(sample)
    }

    fun isLag(): Boolean {
        // 如果是刚开始记录，窗口没有填满的话，就不切源
        if (samples.count() < sampleCountLimit) {
            return false
        }
        return samples.coefficientOfVariation() > coefficientOfVariationThreshold
    }

    // 重制计数器，重新推流时应该重制样本
    fun reset() {
        samples = LimitedQueue(sampleCountLimit)
    }
}

class LimitedQueue constructor(private val limitation: Int) {
    private val array = mutableListOf<Int>()

    fun enqueue(e: Int) {
        if (array.count() == limitation)
            array.removeAt(0)
        array.add(e)
    }

    fun count(): Int {
        return array.count()
    }

    fun coefficientOfVariation(): Double {
        return standardDeviation() / array.average()
    }

    private fun variance(): Double {
        val left = (array.map { (it * it).toDouble() }).average()
        val right = array.average().pow(2.0)
        val count = array.count().toDouble()
        return (left - right) * (count / (count - 1))
    }

    private fun standardDeviation(): Double {
        return sqrt(variance())
    }
}