package com.thel.utils;

/**
 * Created by liuyun on 2017/9/13.
 */

public class UrlConstants {
    // public static final String BASE_URL = L.IS_USE_BAIDU_TEST ? "http://pre-api.rela.me" : "http://api.rela.me/";
//    public static final String BASE_URL = L.IS_USE_BAIDU_TEST ? "http://test-api.rela.me/" : "http://api.rela.me/";
    public static final String HTTP = ShareFileUtils.getInt(ShareFileUtils.HTTPS_SWITCH, 0) == 0 ? "http://" : "https://";
    public static final String HTTP_HOST = "api.rela.me";
    public static final String HTTP_PRE_HOST = "ali-pre-api.rela.me";
    public static final String HTTP_TEST_HOST = "test-api.rela.me";
    public static final String IM_HOST = "im.rela.me";
    public static final String IM_PRE_HOST = "im-pre.rela.me";
    public static final String IM_TEST_HOST = "im-test.rela.me";
    public static final String BASE_URL = HTTP + HTTP_HOST;
    public static final String BASE_PRE_URL = HTTP + HTTP_PRE_HOST;
    public static final String BASE_TEST_URL = HTTP + HTTP_TEST_HOST;
    public static final String UPLOAD_LOG_URL = HTTP + "report-api.rela.me/";
    public static final String MATCH_REPORTS_LOGS = HTTP + "test-report-api.rela.me/";

    public static final String NX_URL = "http://nx-tmp-api.rela.me/";

    public static class WebURLConstants {
        public static final String URL_GETINFO = "thel://com.user/getInfo";
        public static final String URL_FOLLOW = "thel://com.user/follow";
        public static final String URL_UNFOLLOW = "thel://com.user/unfollow";
        public static final String URL_USER = "thel://com.user/path";
        public static final String URL_MOMENT = "thel://com.moment/path";
        public static final String URL_THEME = "thel://com.topic/path";
        public static final String URL_TAG = "thel://com.tag/path";
        public static final String URL_STICKER_PACK = "thel://com.emoji_config/path";
        public static final String URL_STICKER_STORE = "thel://com.emojiShop/path";
        public static final String URL_VIDEO = "thel://com.video/path";
        public static final String URL_RELEASE_THEME = "thel://com.writetopic/path";
        public static final String URL_REQUEST = "thel-gap://request";
        public static final String URL_ALERT = "thel-gap://alert";
        public static final String URL_FEEDBACK = "thel://com.support/path";
        public static final String URL_LIVE_ROOM = "thel://com.live/path";
        public static final String URL_SHARE = "thel://com.share/shareByClient";
        public static final String URL_WRITE_VIDEO = "thel://com.video/publish";
        public static final String URL_BUY_VIP = "thelweb://com.buy.vip/path";

    }

}
