package com.thel.utils;

import android.text.SpannableString;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.modules.login.login_register.LoginRegisterActivity;
import com.thel.modules.main.me.bean.MatchBean;
import com.thel.modules.main.me.bean.RoleBean;
import com.thel.modules.main.userinfo.UserInfoUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import java.util.UUID;

/**
 * @author liuyun
 * @date 2018/3/13
 */

public class UserUtils {

    /**
     * 没有绑定手机号码不能发布日志，直接跳到绑定手机页面
     */
    public static boolean isVerifyCell() {
        String cell = ShareFileUtils.getString(ShareFileUtils.BIND_CELL, "");
        // 简体中文
        if (Locale.SIMPLIFIED_CHINESE.getLanguage().equals(Locale.getDefault().getLanguage())) {
            if (TextUtils.isEmpty(cell) || "null".equals(cell)) {
                //打开注册页面
                LoginRegisterActivity.startActivity(TheLApp.getContext(), LoginRegisterActivity.PHONE_VERIFY,false);
                return false;
            }
        }
        return true;
    }

    /**
     * 获取自己的id
     * U
     *
     * @return
     */
    public static String getMyUserId() {
        return ShareFileUtils.getString(ShareFileUtils.ID, "");
    }

    /**
     * 获取当前用户的key
     *
     * @return
     */
    public static String getUserKey() {
        return ShareFileUtils.getString(ShareFileUtils.KEY, "");
    }

    /**
     * 获取热拉ID
     *
     * @return
     */
    public static String getUserName() {
        return ShareFileUtils.getString(ShareFileUtils.USER_NAME, "");
    }

    /**
     * 获取用户头像
     *
     * @return
     */
    public static String getUserAvatar() {
        return ShareFileUtils.getString(ShareFileUtils.AVATAR, "");
    }

    /**
     * 获取用户等级
     *
     * @return
     */
    public static String getUserLevel() {
        return ShareFileUtils.getString(ShareFileUtils.USER_LEVEL, "");
    }

    /**
     * 获取VIP等级
     */
    public static int getUserVipLevel() {
        return ShareFileUtils.getInt(ShareFileUtils.USER_VIP_LEVEL, 0);
    }

    /**
     * 获取VIP等级
     *
     * @param level
     */
    public static void setUserVipLevel(int level) {
        ShareFileUtils.setInt(ShareFileUtils.USER_VIP_LEVEL, level);
    }

    /**
     * 获取热拉ID
     *
     * @return
     */
    public static String getNickName() {
        return ShareFileUtils.getString(ShareFileUtils.USER_NAME, "");
    }

    /**
     * 直播权限 1有直播权限 0没有直播权限
     *
     * @return
     */
    public static int getPerm() {
        return ShareFileUtils.getInt(ShareFileUtils.PERM, 0);
    }

    /**
     * 获取用户角色
     *
     * @return
     */
    public static String getRoleName() {
        return ShareFileUtils.getString(ShareFileUtils.ROLE_NAME, "");

    }

    /**
     * 判断是否是新用户
     *
     * @return
     */
    public static boolean isNewUser() {
        return TextUtils.isEmpty(getNickName()) || TextUtils.isEmpty(getUserAvatar()) || TextUtils.isEmpty(getRoleName());
    }

    //体重
    public static String getHandW(String weight) {
        int weightUnits = ShareFileUtils.getInt(ShareFileUtils.WEIGHT_UNITS, 0); // 体重单位(0=kg, 1=Lbs)
        try {
            String w;

            if (weightUnits == 0) {
                w = Float.valueOf(weight).intValue() + " kg";
            } else {
                w = Utils.kgToLbs(weight) + " Lbs";
            }
            return w;
        } catch (Exception e) {
        }
        return "";
    }

    //身高
    public static String getHandH(String height) {
        int heightUnits = ShareFileUtils.getInt(ShareFileUtils.HEIGHT_UNITS, 0); // 身高单位(0=cm, 1=Inches)
        try {
            String h;
            String w;
            if (heightUnits == 0) {
                h = Float.valueOf(height).intValue() + " cm";
            } else {
                h = Utils.cmToInches(height) + " Inches";
            }

            return h;
        } catch (Exception e) {
        }
        return "";
    }

    private static ArrayList<String> role_list = new ArrayList<String>(); // 角色

    //寻找角色
    public static  String setLookfor(String wantRole) {
        String[] role_Array = TheLApp.context.getResources().getStringArray(R.array.userinfo_role);
        role_list = new ArrayList<String>(Arrays.asList(role_Array));

        if (TextUtils.isEmpty(wantRole))
            return "";
        int otherRoleCount = 0;
        try {
            String[] lookingForRole = wantRole.split(",");
            StringBuilder sb = new StringBuilder();// 交友目的内容字符串拼接
            int size = lookingForRole.length;
            ArrayList newRoleList = new ArrayList();
            for (int i = 0; i < size; i++) {
                int index = Integer.parseInt(lookingForRole[i]); //1.2.3.4.5.6.7

                if ((index == 6 || index == 7 || index == 5)) {
                    if (otherRoleCount < 1) {
                        index = 5;
                        otherRoleCount++;
                    } else {
                        continue;
                    }
                }
                RoleBean roleBean = ShareFileUtils.getRoleBean(index);
                if (roleBean != null) {
                    int listPoi = roleBean.listPoi;
                    if (listPoi == 5) {
                        newRoleList.add(listPoi);
                    } else {

                    }
                    sb.append(role_list.get(listPoi));
                } else {
                }
                if (i != size - 1) {

                    sb.append(", ");
                }
            }

            int j = 0;
            for (int i = 0; i < sb.length(); i++) {
                if (newRoleList.contains(sb.charAt(i))) {
                    sb.deleteCharAt(i - j);
                }
            }

            return  sb.toString();
           /* SpannableString spannaString = UserInfoUtils.getUserInfoSpannaString("", sb.toString());
            String content = spannaString.toString();
            if (content != null && content.length() > 0) {
                if (content.endsWith(",")) {
                    content = content.substring(0, content.length() - 1);
                }
                txt_lookfor.setText(content);
            }*/

        } catch (Exception e) {
            e.printStackTrace();
        }
        return  "";
    }

    public static String getDiviceId(){
        return ShareFileUtils.getString(ShareFileUtils.DEVICE_ID, UUID.randomUUID().toString());
    }

}
