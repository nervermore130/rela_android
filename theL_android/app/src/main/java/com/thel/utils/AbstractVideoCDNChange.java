package com.thel.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liuyun
 */
public abstract class AbstractVideoCDNChange {

    private static final int FIVE_SECOND = 5 * 1000;

    private static final int THREE_SECOND = 3 * 1000;

    private long videoBufferStartTime = 0;

    private long videoBufferStartTimeOld = 0;

    private long videoBufferEndTime = 0;

    public void videoBufferStart() {

        L.d("AbstractVideoCDNChange", " videoBufferStart : ");

        videoBufferStartTime = System.currentTimeMillis();

        if (videoBufferStartTimeOld != 0) {

            long deltaTime = videoBufferStartTime - videoBufferStartTimeOld;

            L.d("AbstractVideoCDNChange", " videoBufferStart deltaTime : " + deltaTime);

            if (deltaTime > FIVE_SECOND) {

                videoCDNChange();

                reCount();

            }

        }

        videoBufferStartTimeOld = videoBufferStartTime;

    }

    public void videoBufferEnd() {

        L.d("AbstractVideoCDNChange", " videoBufferEnd : ");

        videoBufferEndTime = System.currentTimeMillis();

        long deltaTime = videoBufferEndTime - videoBufferStartTime;

        L.d("AbstractVideoCDNChange", " videoBufferEnd deltaTime : " + deltaTime);

        if (deltaTime > THREE_SECOND) {

            videoCDNChange();

            reCount();
        }
    }

    public void reCount() {

        videoBufferStartTimeOld = 0;

        videoBufferStartTime = 0;

        videoBufferEndTime = 0;

    }

    public void countLowSpeed() {
        times.add(System.currentTimeMillis());

        if (times.size() == 4) {
            long startTime = times.get(0);

            long endTime = times.get(times.size() - 1);

            if (endTime - startTime <= 5000) {

                videoCDNChange();

                times.clear();
            }
        }

    }


    public abstract void videoCDNChange();

    private List<Long> times = new ArrayList<>();
}
