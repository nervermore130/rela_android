package com.thel.utils;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.SearchManager;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.location.Location;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.PowerManager;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;
import android.telephony.TelephonyManager;
import android.text.ClipboardManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.meituan.android.walle.WalleChannelReader;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.constants.TheLConstants;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

/**
 * 设备操作与信息获取
 *
 * @author wufucheng
 */
public class DeviceUtils {

    private static final String TAG = DeviceUtils.class.getSimpleName();

    private static final String ACTION_ADD_SHORTCUT = "com.android.launcher.action.INSTALL_SHORTCUT";
    private static final String ACTION_DEL_SHORTCUT = "com.android.launcher.action.UNINSTALL_SHORTCUT";
    private static final String EXTRA_SHORTCUT_DUPLICATE = "duplicate";
    private static final String APK_MIME_TYPE = "application/vnd.android.package-archive";
    private static final int SCREEN_DENSITY_MEDIUM = 160;
    private static final String MIME_TYPE_TEXT = "text/*";
    private static final String MIME_TYPE_EMAIL = "message/rfc822";
    private static final int NETWORK_TYPE_HSPA = 10;
    private static final int NETWORK_TYPE_HSDPA = 8;
    private static final int NETWORK_TYPE_HSUPA = 9;

    public static final int TO_MEDIASTORE = 999;
    public static final int TO_MEDIASTORE_KITKAT = 888;
    public static final int TAKE_PHOTO = 777;
    public static final int CUT_PHOTO = 666;

    private static final int CUT_PHOTO_X = 1;
    private static final int CUT_PHOTO_Y = 1;

	/*--------------------------------------------------------------------------
    | 设备信息获取
	--------------------------------------------------------------------------*/

    /**
     * 获得设备分辨率，eg: dm.widthPixels dm.heightPixels dm.density dm.xdpi dm.ydpi
     */
    public static DisplayMetrics getDisplayInfo(Context context) {
        return context.getApplicationContext().getResources().getDisplayMetrics();
    }

    /**
     * 获得设备的DensityDpi
     *
     * @param context
     * @return
     */
    public static int getDensityDpi(Context context) {
        int screenDensity = -1;
        try {
            DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
            screenDensity = DisplayMetrics.class.getField("densityDpi").getInt(displayMetrics);
        } catch (Exception e) {
            screenDensity = SCREEN_DENSITY_MEDIUM;
        }
        return screenDensity;
    }

    /**
     * 获得设备的型号
     */
    public static String getModel() {
        return Build.MODEL;
    }

    /**
     * 获得设备的基带信息
     *
     * @return
     */
    public static String getBrand() {
        return Build.BRAND;
    }

    /**
     * 获取设备的产品信息
     *
     * @return
     */
    public static String getProduct() {
        return Build.PRODUCT;
    }

    /**
     * 获得设备的SDK版本
     */
    public static int getSdkVersion() {
        return Integer.parseInt(Build.VERSION.SDK);
    }

    /**
     * 获得设备的系统版本
     */
    public static String getReleaseVersion() {
        return Build.VERSION.RELEASE;
    }

    /**
     * 获取厂商信息
     */
    public static String getManufacturer() {
        return Build.MANUFACTURER;
    }

    /**
     * 从数美获取DeviceId
     */
    public static String getDeviceId() {
        return ShareFileUtils.getString(ShareFileUtils.DEVICE_ID, "");
    }

    /**
     * 获得设备的IMSI
     */
    public static String getIMSI(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getSubscriberId();
    }

    /**
     * 判断国家是否是国内用户
     *
     * @return
     */
    public static boolean isCN() {
        TelephonyManager tm = (TelephonyManager) TheLApp.context.getSystemService(Context.TELEPHONY_SERVICE);
        String countryIso = tm.getSimCountryIso();
        boolean isCN = false;//判断是不是大陆
        if (!TextUtils.isEmpty(countryIso)) {
            countryIso = countryIso.toUpperCase(Locale.US);
            if (countryIso.contains("CN")) {
                isCN = true;
            }
        }
        return isCN;
    }

    /**
     * 查询手机的 MCC+MNC
     */
    private static String getSimOperator() {
        TelephonyManager tm = (TelephonyManager) TheLApp.context.getSystemService(Context.TELEPHONY_SERVICE);
        try {
            return tm.getSimOperator();
        } catch (Exception e) {

        }
        return null;
    }

    /**
     * 因为发现像华为Y300，联想双卡的手机，会返回 "null" "null,null" 的字符串
     */
    private static boolean isOperatorEmpty(String operator) {
        if (operator == null) {
            return true;
        }


        return operator.equals("") || operator.toLowerCase(Locale.US).contains("null");


    }

    /**
     * 判断是否是国内的 SIM 卡，优先判断注册时的mcc
     */
    public static boolean isChinaSimCard() {
        String mcc = getSimOperator();
        if (isOperatorEmpty(mcc)) {
            return false;
        } else {
            return mcc.startsWith("460");
        }
    }

    /**
     * 获得设备的Sim卡序列号
     */
    public static String getSimSerialNumber(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getSimSerialNumber();
    }

    /**
     * 获取本机号码
     *
     * @param context
     * @return
     */
    public static String getPhoneNumber(Context context) {
        TelephonyManager mTelephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return mTelephonyManager.getLine1Number();
    }

    /**
     * 获取语言信息
     *
     * @return
     */
    public static String getLanguage() {
        return getLanguageStr(getLanguageLocale());
    }

    public static String getLanguage2() {
        Locale locale = getLanguageLocale();
        if (locale != null) {
            return locale.getLanguage();
        } else {
            return "";
        }
    }

    public static String getLanguageStr() {

        String languageStr = Objects.requireNonNull(getLanguageLocale()).toString();

        if (languageStr == null) {
            languageStr = "zh_CN";
        }

        return languageStr;
    }

    public static Locale getLanguageLocale() {
        Locale locale;
        try {
            final String mLanguageKey = SharedPrefUtils.getString(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.LANGUAGE_SETTING, "");//获取缓存语言，userid+_+key
            if (mLanguageKey.equals(TheLConstants.LANGUAGE_ZH_CN)) {//简体中文
                locale = Locale.SIMPLIFIED_CHINESE;
            } else if (mLanguageKey.equals(TheLConstants.LANGUAGE_ZH_TW)) {//繁体中文 (台湾)
                locale = Locale.TAIWAN;
            } else if (mLanguageKey.equals(TheLConstants.LANGUAGE_ZH_HK)) {
                locale = Locale.TRADITIONAL_CHINESE;

            } else if (mLanguageKey.equals(TheLConstants.LANGUAGE_EN_US)) {//英文（美）
                locale = Locale.US;
            } else if (mLanguageKey.equals(TheLConstants.LANGUAGE_FR_RFR)) {//法语（法国）
                locale = Locale.FRANCE;
            } else if (mLanguageKey.equals(TheLConstants.LANGUAGE_TH_RTH)) {//泰语
                locale = new Locale("th", "TH");
            } else if (mLanguageKey.equals(TheLConstants.LANGUAGE_ES)) {//西班牙语
                locale = new Locale("es");
            } else if (mLanguageKey.equals(TheLConstants.LANGUAGE_JA_JP)) {
                locale = Locale.JAPAN;

            } else {
                locale = Locale.getDefault();
            }
            return locale;
        } catch (Exception e) {
            log(e);
        }
        return null;
    }

    /**
     * 获取语言字符串
     *
     * @param locale
     * @return
     */
    public static String getLanguageStr(Locale locale) {
        if (locale != null) {
            return locale.toString();
        } else {
            return "";
        }
    }

    /**
     * 获得设备IP
     */
    public static String getLocalIP() {
        try {
            Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
            for (; en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses();
                for (; enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        return inetAddress.getHostAddress();
                    }
                }
            }
            return "";
        } catch (SocketException e) {
            return "";
        }
    }

    /**
     * 获得Wifi的MAC地址
     *
     * @param context
     * @return
     */
    public static String getWifiMAC(Context context) {
        try {
            WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            if (wifiManager == null) {
                return "";
            }
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            if (wifiInfo == null || wifiInfo.getMacAddress() == null) {
                return null;
            }
            return wifiInfo.getMacAddress().replaceAll(":", "");
        } catch (Exception e) {
            log(e);
            return "";
        }
    }

    /**
     * 获得AndroidId
     *
     * @param context
     * @return
     */
    public static String getAndroidId(Context context) {
        return android.provider.Settings.System.getString(context.getContentResolver(), android.provider.Settings.System.ANDROID_ID);
    }

	/*--------------------------------------------------------------------------
    | 软件信息获取
	--------------------------------------------------------------------------*/

    /**
     * 获得版本号
     */
    public static int getVersionCode(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            L.i("DeviceUtils", "getVersionCode:versionCode=" + info.versionCode);
            return info.versionCode;
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * 获得版本名称
     */
    public static String getVersionName(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return info.versionName;
        } catch (Exception e) {
            return "";
        }
    }

    /**
     * 获得应用名称
     */
    public static String getAppName(Context context) {
        try {
            PackageManager manager = context.getPackageManager();
            PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return info.applicationInfo.loadLabel(manager).toString();
        } catch (Exception e) {
            return "";
        }
    }

    /**
     * 获取Application中<meta-data>元素的数据
     *
     * @param context
     * @return
     */
    public static Bundle getMetaDataInApplication(Context context) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
            return applicationInfo.metaData;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 获取Activity中<meta-data>元素的数据
     *
     * @param activity
     * @return
     */
    public static Bundle getMetaDataInActivity(Activity activity) {
        try {
            ActivityInfo activityInfo = activity.getPackageManager().getActivityInfo(activity.getComponentName(), PackageManager.GET_META_DATA);
            return activityInfo.metaData;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 获取Service中<meta-data>元素的数据
     *
     * @param context
     * @param cls
     * @return
     */
    public static Bundle getMetaDataInService(Context context, Class<?> cls) {
        try {
            ComponentName componentName = new ComponentName(context, cls);
            ServiceInfo serviceInfo = context.getPackageManager().getServiceInfo(componentName, PackageManager.GET_META_DATA);
            return serviceInfo.metaData;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 获取Receiver中<meta-data>元素的数据
     *
     * @param context
     * @param cls
     * @return
     */
    public static Bundle getMetaDataInReceiver(Context context, Class<?> cls) {
        try {
            ComponentName componentName = new ComponentName(context, cls);
            ActivityInfo activityInfo = context.getPackageManager().getReceiverInfo(componentName, PackageManager.GET_META_DATA);
            return activityInfo.metaData;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 返回软件是否已安装
     *
     * @param context
     * @param uri     软件包名
     * @return
     */
    public static boolean isAppInstalled(Context context, String uri) {
        try {
            PackageManager manager = context.getPackageManager();
            manager.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 返回软件的原始签名
     *
     * @param context
     * @return
     */
    public static String[] getSignature(Context context) {
        try {
            String[] result = null;
            Signature[] signatures = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_SIGNATURES).signatures;
            if (signatures != null && signatures.length != 0) {
                result = new String[signatures.length];
                for (int i = 0; i < signatures.length; i++) {
                    result[i] = signatures[i].toCharsString();
                }
            }
            return result;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 返回软件的签名，经过Hash处理
     *
     * @param context
     * @return
     */
    public static String[] getSignatureHash(Context context) {
        try {
            String[] result = null;
            Signature[] signatures = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_SIGNATURES).signatures;
            if (signatures != null && signatures.length != 0) {
                result = new String[signatures.length];
                for (int i = 0; i < signatures.length; i++) {
                    result[i] = Integer.toHexString(signatures[i].toCharsString().hashCode());
                }
            }
            return result;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 返回Intent是否可用
     *
     * @param context
     * @param intent
     * @return
     */
    public static boolean isIntentEnabled(Context context, Intent intent) {
        try {
            PackageManager packageManager = context.getPackageManager();
            List<ResolveInfo> list = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
            return list.size() > 0;
        } catch (Exception e) {
            return false;
        }
    }

	/*--------------------------------------------------------------------------
    | 设备状态检查
	--------------------------------------------------------------------------*/

    /**
     * 检查网络是否可用
     */
    public static boolean isNetworkEnabled(Context context) {
        try {
            ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null) {
                NetworkInfo[] infoArray = connectivity.getAllNetworkInfo();
                if (infoArray != null) {
                    for (NetworkInfo info : infoArray) {
                        if (info.getState() == NetworkInfo.State.CONNECTED) {
                            return true;
                        }
                    }
                }
            }
            return false;
        } catch (Exception e) {
            log(e);
            return false;
        }
    }

    /**
     * 检查Sim卡状态
     */
    public static boolean isSimEnabled(Context context) {
        try {
            TelephonyManager mTelephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            return mTelephonyManager.getSimState() == TelephonyManager.SIM_STATE_READY;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 检查移动网络是否可用
     */
    public static boolean isMobileEnabled(Context context) {
        try {
            ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            return connectivity.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 检查WIFI网络是否可用
     */
    public static boolean isWifiEnabled(Context context) {
        try {
            ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            return connectivity.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 检查网络是否联通WCDMA
     */
    public static boolean isWCDMA(Context context) {
        try {
            // 获得手机SIMType
            TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            int pType = tm.getPhoneType();
            int nType = tm.getNetworkType();
            String operator = tm.getNetworkOperator();
            if (operator.equals("46001")) {
                // 运营商为联通
                if (pType == TelephonyManager.PHONE_TYPE_GSM) {
                    return nType == NETWORK_TYPE_HSPA || nType == NETWORK_TYPE_HSDPA || nType == NETWORK_TYPE_HSUPA;
                }
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 检查是否开通GPS定位或网络定位
     */
    public static boolean isLocationEnabled(Context context) {
        try {
            LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            return lm.isProviderEnabled(LocationManager.GPS_PROVIDER) || lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 检查指定的定位服务是否开通
     *
     * @param context
     * @param provider
     */
    public static boolean isProviderEnabled(Context context, String provider) {
        try {
            LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            return lm.isProviderEnabled(provider);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 根据指定的Provider获取位置
     *
     * @param context
     * @param provider
     */
    public static Location getLocation(Context context, String provider) {
        try {
            LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            List<String> providers = lm.getProviders(true);
            if (providers != null && providers.contains(provider)) {
                //                Location locTmp = lm.getLastKnownLocation(provider);
                //                if (locTmp != null) {
                //                    return locTmp;
                //                }
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 根据指定的Provider获取位置
     *
     * @param context
     * @param provider
     * @param time     有效时间界限，获取的位置在此时间之后的为有效位置
     */
    public static Location getLocation(Context context, String provider, long time) {
        try {
            LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            List<String> providers = lm.getProviders(true);
            if (providers != null && providers.contains(provider)) {
                //                Location locTmp = lm.getLastKnownLocation(provider);
                //                if (locTmp != null && locTmp.getTime() >= time) {
                //                    return locTmp;
                //                }
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 检查设备存储卡是否装载
     */
    public static boolean isSDCardMounted() {
        return Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
    }

//    /**
//     * 检查设备存储卡的读权限
//     */
//    public static boolean isSDCardReadable() {
//        try {
//            if (!isSDCardMounted()) {
//                return false;
//            }
//            File sdcardDir = Environment.getExternalStorageDirectory();
//            return sdcardDir.canRead();
//        } catch (Exception e) {
//            return false;
//        }
//    }
//
//    /**
//     * 检查设备存储卡的写权限
//     */
//    public static boolean isSDCardWritable() {
//        try {
//            if (!isSDCardMounted()) {
//                return false;
//            }
//            File sdcardDir = Environment.getExternalStorageDirectory();
//            return sdcardDir.canWrite();
//        } catch (Exception e) {
//            return false;
//        }
//    }

    /**
     * 检查相机是否能使用
     */
    public static boolean isCameraEnabled(Context context) {
        Camera camera = null;
        try {
            camera = android.hardware.Camera.open();
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            if (camera != null) {
                camera.release();
            }
        }
    }

	/*--------------------------------------------------------------------------
    | 设备硬件操作
	--------------------------------------------------------------------------*/

    /**
     * 使设备震动
     */
    public static void vibrate(Context context, long milliseconds) {
        try {
            Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(milliseconds);
        } catch (Exception e) {
            log(e);
        }
    }

    /**
     * 使设备震动
     */
    public static void vibrate(Context context, long[] pattern, int repeat) {
        try {
            Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
            // long[] pattern = {100,400,100,400}; // OFF/ON/OFF/ON...
            vibrator.vibrate(pattern, repeat);
        } catch (Exception e) {
            log(e);
        }
    }

    /**
     * 使设备停止震动
     */
    public static void cancelVibrate(Context context) {
        try {
            Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.cancel();
        } catch (Exception e) {
            log(e);
        }
    }

    /**
     * 点亮屏幕
     *
     * @param context
     */
    public static PowerManager.WakeLock wakeupScreen(Context context) {
        try {
            PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            PowerManager.WakeLock wakeLock = powerManager.newWakeLock(PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.FULL_WAKE_LOCK, "My Tag");
            if (!isScreenOn(powerManager)) {
                wakeLock.acquire();
            }
            return wakeLock;
        } catch (Exception e) {
            log(e);
            return null;
        }
    }

    /**
     * 使屏幕变亮
     */
    public static void turnOnScreen(Activity act) {

        WindowManager.LayoutParams params = act.getWindow().getAttributes();
        params.flags |= WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
        params.screenBrightness = getScreenBrightness(act);
        act.getWindow().setAttributes(params);
    }

    /**
     * 使屏幕变暗
     */
    public static void turnOffScreen(Activity act) {

        WindowManager.LayoutParams params = act.getWindow().getAttributes();
        params.flags |= WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
        params.screenBrightness = 0;
        act.getWindow().setAttributes(params);
    }

    public static float getScreenBrightness(Activity activity) {
        float value = 1;
        ContentResolver cr = activity.getContentResolver();
        try {
            value = Settings.System.getFloat(cr, Settings.System.SCREEN_BRIGHTNESS);
        } catch (SettingNotFoundException e) {
        }
        return value;
    }

    /**
     * 返回屏幕是否点亮
     *
     * @param powerManager
     * @return
     */
    public static boolean isScreenOn(PowerManager powerManager) {
        try {
            Method method = PowerManager.class.getMethod("isScreenOn");
            return (Boolean) method.invoke(powerManager);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 禁用键盘锁
     *
     * @param context
     */
    public static void disableKeyguard(Context context) {
        //        try {
        //            KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
        //            KeyguardLock keyguardLock = keyguardManager.newKeyguardLock("");
        //            keyguardLock.disableKeyguard();
        //        } catch (Exception e) {
        //            log(e);
        //        }
    }

    /**
     * 启用键盘锁
     *
     * @param context
     */
    public static void reenableKeyguard(Context context) {
        //        try {
        //            KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
        //            KeyguardLock keyguardLock = keyguardManager.newKeyguardLock("");
        //            keyguardLock.reenableKeyguard();
        //        } catch (Exception e) {
        //            log(e);
        //        }
    }

	/*--------------------------------------------------------------------------
    | 系统程序操作
	--------------------------------------------------------------------------*/

    /**
     * 调出拨号程序
     *
     * @param context
     * @param phone   电话号码
     */
    public static void dial(Context context, String phone) {
        phone = "tel:" + phone;
        Uri uri = Uri.parse(phone);
        Intent intent = new Intent(Intent.ACTION_DIAL, uri);
        context.startActivity(intent);
    }

    /**
     * 直接拨打电话
     *
     * @param context
     * @param phone   电话号码
     */
    public static void call(Context context, String phone) {
        phone = "tel:" + phone;
        Uri uri = Uri.parse(phone);
        Intent intent = new Intent(Intent.ACTION_CALL, uri);
        //        context.startActivity(intent);
    }

    /**
     * 发送短信
     *
     * @param context
     * @param phone   电话号码
     * @param content 短信内容
     */
    public static void sendSMS(Context context, String phone, String content) {
        phone = "smsto:" + phone;
        Uri uri = Uri.parse(phone);
        Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
        intent.putExtra("sms_body", content);
        context.startActivity(intent);
    }

    /**
     * 从本地选取图片，应处理onActivityResult，示例： protected void onActivityResult(int
     * requestCode, int resultCode, Intent data) { //获得图片的真实地址 String path =
     * getPathByUri(this, data.getData()); }
     * <p/>
     * 兼容4.4
     *
     * @param activity
     */
    public static void pickImage(Activity activity) {
        if (Build.VERSION.SDK_INT < 19) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            activity.startActivityForResult(intent, TO_MEDIASTORE);
        } else {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("image/*");
            activity.startActivityForResult(intent, TO_MEDIASTORE_KITKAT);
        }
    }

    /**
     * 从本地选取图片，应处理onActivityResult，示例： protected void onActivityResult(int
     * requestCode, int resultCode, Intent data) { //获得图片的真实地址 String path =
     * getPathByUri(this, data.getData()); }
     * <p/>
     * 兼容4.4
     *
     * @param activity
     */
    public static void pickImageAndCut(Activity activity, String fileName, int xSize, int ySize) {
        File fileDir = new File(TheLConstants.F_TAKE_PHOTO_ROOTPATH);
        if (!fileDir.exists()) {
            fileDir.mkdirs();
        }
        if (Build.VERSION.SDK_INT < 19) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(TheLConstants.F_TAKE_PHOTO_ROOTPATH + fileName)));
            intent.putExtra("crop", "true");
            intent.putExtra("aspectX", CUT_PHOTO_X);// 裁剪框比例
            intent.putExtra("aspectY", CUT_PHOTO_Y);
            intent.putExtra("outputX", xSize);// 输出图片大小
            intent.putExtra("outputY", ySize);
            activity.startActivityForResult(intent, TO_MEDIASTORE);
        } else {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("image/*");
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(TheLConstants.F_TAKE_PHOTO_ROOTPATH + fileName)));
            activity.startActivityForResult(intent, TO_MEDIASTORE_KITKAT);
        }
    }

    /**
     * 调用拍照程序拍摄图片，返回图片对应的Uri，应处理onActivityResult
     * ContentResolver的insert方法会默认创建一张空图片，如取消了拍摄，应根据方法返回的Uri删除图片
     *
     * @param activity
     * @param localTempImgFileName
     * @return
     */
    public static Uri captureImage(Activity activity, String localTempImgFileName) {
        File dir = new File(TheLConstants.F_TAKE_PHOTO_ROOTPATH);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        File file = new File(dir, localTempImgFileName);
        Uri uri = Uri.fromFile(file);
        intent.putExtra(MediaStore.Images.Media.ORIENTATION, 0);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        activity.startActivityForResult(intent, TAKE_PHOTO);
        // 设置文件参数
        // ContentValues values = new ContentValues();
        // values.put(MediaColumns.TITLE, fileName);
        // values.put(ImageColumns.DESCRIPTION, desc);
        // // 获得uri
        // Uri imageUri = activity.getContentResolver().insert(
        // MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        // Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE, null);
        // intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        // intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
        // activity.startActivityForResult(intent, requestCode);
        return uri;
    }

    /**
     * 处理部分机型系统相机拍照方向不对问题
     *
     * @param originpath 原图路径
     * @return 返回修复完毕后的图片路径
     */
    public static String amendRotatePhoto(String originpath) {

        // 取得图片旋转角度
        int degree = 0;
        try {
            ExifInterface exifInterface = new ExifInterface(originpath);
            int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    degree = 90;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    degree = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    degree = 270;
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (degree == 0) return originpath;

        // 把原图压缩后得到Bitmap对象
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = false;
        options.inSampleSize = 3;  // 图片的大小设置为原来的十分之一
        Bitmap bmp = BitmapFactory.decodeFile(originpath, options);
        options = null;

        // 修复图片被旋转的角度
        Bitmap returnBm = null;
        // 根据旋转角度，生成旋转矩阵
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        try {
            // 将原始图片按照旋转矩阵进行旋转，并得到新的图片
            returnBm = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);
        } catch (OutOfMemoryError e) {
        }
        if (returnBm == null) {
            returnBm = bmp;
        }
        if (bmp != returnBm) {
            bmp.recycle();
        }

        // 保存修复后的图片并返回保存后的图片路径
        FileOutputStream outStream = null;
        String fileName = TheLConstants.F_TAKE_PHOTO_ROOTPATH + ImageUtils.getPicName();
        try {
            outStream = new FileOutputStream(fileName);
            // 把数据写入文件，100表示不压缩
            returnBm.compress(Bitmap.CompressFormat.PNG, 100, outStream);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (outStream != null) {
                    // 记得要关闭流！
                    outStream.close();
                }
                if (returnBm != null) {
                    returnBm.recycle();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return fileName;
    }

    /**
     * 剪裁图片
     *
     * @param activity
     * @param path
     */
    public static void cutPhoto(Activity activity, String path, String outputPath, int xSize, int ySize) {
        // Uri uri = Uri.fromFile(new File(path));
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(outputPath)));
        intent.setDataAndType(Uri.fromFile(new File(path)), "image/*");
        // crop为true是设置在开启的intent中设置显示的view可以剪裁
        intent.putExtra("crop", "true");

        // aspectX aspectY 是宽高的比例
        intent.putExtra("aspectX", CUT_PHOTO_X);// 裁剪框比例
        intent.putExtra("aspectY", CUT_PHOTO_Y);
        // outputX,outputY 是剪裁图片的宽高
        intent.putExtra("outputX", xSize);// 输出图片大小
        intent.putExtra("outputY", ySize);
        intent.putExtra("return-data", true);
        activity.startActivityForResult(intent, CUT_PHOTO);
    }

    /**
     * 调用地图程序
     *
     * @param activity
     * @param address
     * @param placeTitle
     */
    public static void callMap(Activity activity, String address, String placeTitle) {
        StringBuilder sb = new StringBuilder();
        sb.append("geo:0,0?q=");
        sb.append(Uri.encode(address));
        // pass text for the info window
        String titleEncoded = Uri.encode("(" + placeTitle + ")");
        sb.append(titleEncoded);
        // set locale
        sb.append("&hl=" + Locale.getDefault().getLanguage());
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(sb.toString()));
        activity.startActivity(intent);
    }

    /**
     * 调用分享程序
     *
     * @param activity
     * @param subject
     * @param message
     * @param chooserDialogTitle
     */
    public static void callShare(Activity activity, String subject, String message, String chooserDialogTitle) {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        shareIntent.putExtra(Intent.EXTRA_TEXT, message);
        shareIntent.setType(MIME_TYPE_TEXT);
        Intent intent = Intent.createChooser(shareIntent, chooserDialogTitle);
        activity.startActivity(intent);
    }

    /**
     * 调用发送电子邮件程序
     *
     * @param activity
     * @param address
     * @param subject
     * @param body
     */
    public static void callEmail(Activity activity, String address, String subject, String body) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{address});
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, body);
        intent.setType(MIME_TYPE_EMAIL);
        activity.startActivity(intent);
    }

    /**
     * 调用网络搜索
     *
     * @param activity
     * @param keyword
     * @throws Exception
     */
    public static void callWebSearch(Activity activity, String keyword) {
        Intent search = new Intent(Intent.ACTION_WEB_SEARCH);
        search.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        search.putExtra(SearchManager.QUERY, keyword);
        Bundle appData = activity.getIntent().getBundleExtra(SearchManager.APP_DATA);
        if (appData != null) {
            search.putExtra(SearchManager.APP_DATA, appData);
        }
        activity.startActivity(search);
    }

    /**
     * 在桌面创建快捷方式
     */
    // public static void addShortcut(Context context) {
    // try {
    // // 点击快捷方式后的Intent
    // Intent intentShortcut = new Intent(Intent.ACTION_MAIN);
    // intentShortcut.setClass(context, context.getClass());
    // intentShortcut.addCategory(Intent.CATEGORY_LAUNCHER);
    // // 不需要总是新建任务
    // // intentShortcut.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    // // intentShortcut.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    // Intent intentAddShortcut = new Intent(ACTION_ADD_SHORTCUT);
    // intentAddShortcut.putExtra(Intent.EXTRA_SHORTCUT_INTENT,
    // intentShortcut);
    // // 快捷方式显示名
    // intentAddShortcut.putExtra(Intent.EXTRA_SHORTCUT_NAME,
    // getAppName(context));
    // // 快捷方式icon
    // intentAddShortcut.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
    // Intent.ShortcutIconResource.fromContext(context,
    // R.drawable.icon));
    // // 不允许重复创建
    // intentAddShortcut.putExtra(EXTRA_SHORTCUT_DUPLICATE, false);
    // context.sendBroadcast(intentAddShortcut);
    // } catch (Exception e) {
    // log(e);
    // }
    // }

    /**
     * 删除本应用的桌面快捷方式
     */
    public static void delShortcut(Context context) {
        try {
            // 点击快捷方式后的Intent
            Intent intentShortcut = new Intent(Intent.ACTION_MAIN);
            intentShortcut.setClass(context, context.getClass());
            intentShortcut.addCategory(Intent.CATEGORY_LAUNCHER);
            // intentShortcut.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            // intentShortcut.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            Intent intentDelshortcut = new Intent(ACTION_DEL_SHORTCUT);
            intentDelshortcut.putExtra(Intent.EXTRA_SHORTCUT_INTENT, intentShortcut);
            intentDelshortcut.putExtra(Intent.EXTRA_SHORTCUT_NAME, getAppName(context));
            context.sendBroadcast(intentDelshortcut);
        } catch (Exception e) {
            log(e);
        }
    }

    /**
     * 去系统定位设置界面
     */
    public static void gotoLocationSettings(Context context) {
        try {
            Intent settingsIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            context.startActivity(settingsIntent);
        } catch (Exception e) {
            log(e);
        }
    }

    /**
     * 去系统无线设置界面
     */
    public static void gotoWirelessSettings(Context context) {
        try {
            Intent settingsIntent = new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS);
            context.startActivity(settingsIntent);
        } catch (Exception e) {
            log(e);
        }
    }

    /**
     * 安装apk文件
     */
    public static void installApk(Context context, File file) {
        try {
            Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
            Uri data = Uri.fromFile(file);
            intent.setDataAndType(data, APK_MIME_TYPE);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        } catch (Exception e) {
            log(e);
        }
    }

    /**
     * 卸载apk文件
     */
    public static void uninstallApk(Context context, String packageName) {
        try {
            Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
            Uri data = Uri.parse("package:" + packageName);
            intent.setData(data);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        } catch (Exception e) {
            log(e);
        }
    }

    /**
     * 清空缓存目录下的文件(不清除子文件夹内的文件)
     */
//    public static boolean clearDiskCache(Context context, String path) {
//        try {
//            File diskCacheDir;
//            if (DeviceUtils.isSDCardMounted()) {
//                diskCacheDir = new File(android.os.Environment.getExternalStorageDirectory(), path);
//            } else {
//                diskCacheDir = context.getCacheDir();
//            }
//            if (diskCacheDir != null) {
//                boolean success = true;
//                if (!diskCacheDir.exists() || !diskCacheDir.isDirectory()) {
//                    return true;
//                }
//                for (File child : diskCacheDir.listFiles()) {
//                    if (child.isFile()) {
//                        success = child.delete();
//                        if (!success) {
//                            return false;
//                        }
//                    }
//                }
//                return success;
//            }
//            return false;
//        } catch (Exception e) {
//            log(e);
//            return false;
//        }
//    }

    /**
     * 根据Uri获取媒体文件的路径
     *
     * @param activity
     * @param uri
     * @return
     */
    public static String getMediaPathByUri(Activity activity, Uri uri) {
        String result = "";
        try {
            if (uri == null) {
                return result;
            }
            String[] proj = {MediaColumns.DATA};
            Cursor cursor = activity.managedQuery(uri, proj, null, null, null);
            if (cursor == null || cursor.isClosed()) {
                return result;
            }
            int column_index = cursor.getColumnIndexOrThrow(MediaColumns.DATA);
            cursor.moveToFirst();
            result = cursor.getString(column_index);
            cursor.close();
            return result;
        } catch (Exception e) {
            log(e);
            return result;
        }
    }

    /**
     * 复制文本信息到剪贴板
     *
     * @param context
     * @param text
     */
    public static void copyToClipboard(Context context, String text) {
        try {
            ClipboardManager clipboardManager = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            clipboardManager.setText(text);
        } catch (Exception e) {
            log(e);
        }
    }

    /**
     * 从剪贴板获取文本信息
     *
     * @param context
     * @return
     */
    public static CharSequence getFromClipboard(Context context) {
        try {
            ClipboardManager clipboardManager = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            if (clipboardManager.hasText()) {
                return clipboardManager.getText();
            }
            return "";
        } catch (Exception e) {
            log(e);
            return "";
        }
    }

    /**
     * 从资源中获取View
     */
    public static View inflateView(Context context, int resource) {
        return inflateView(context, resource, null);
    }

    /**
     * 从资源中获取View
     */
    public static View inflateView(Context context, int resource, ViewGroup root) {
        return inflateView(context, resource, root, false);
    }

    /**
     * 从资源中获取View
     */
    public static View inflateView(Context context, int resource, ViewGroup root, boolean attachToRoot) {
        return ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(resource, root, attachToRoot);
    }

    /**
     * 记录错误
     *
     * @param tr
     */
    private static void log(Throwable tr) {
        L.d(TAG, tr.getMessage());
    }

    public static int getScreenWidth(Context context) {
        WindowManager manager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = manager.getDefaultDisplay();
        return display.getWidth();
    }

    public static String getChannelId() {
        return WalleChannelReader.getChannel(TheLApp.context);
    }

    /**
     * 得到当前的手机网络类型
     *
     * @param context
     * @return
     */
    public static String getCurrentNetType(Context context) {
        String type = "";
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        if (info == null) {
            type = "";
        } else if (info.getType() == ConnectivityManager.TYPE_WIFI) {
            type = "wifi";
        } else if (info.getType() == ConnectivityManager.TYPE_MOBILE) {
            int subType = info.getSubtype();
            if (subType == TelephonyManager.NETWORK_TYPE_CDMA || subType == TelephonyManager.NETWORK_TYPE_GPRS || subType == TelephonyManager.NETWORK_TYPE_EDGE) {
                type = "2g";
            } else if (subType == TelephonyManager.NETWORK_TYPE_UMTS || subType == TelephonyManager.NETWORK_TYPE_HSDPA || subType == TelephonyManager.NETWORK_TYPE_EVDO_A || subType == TelephonyManager.NETWORK_TYPE_EVDO_0 || subType == TelephonyManager.NETWORK_TYPE_EVDO_B) {
                type = "3g";
            } else if (subType == TelephonyManager.NETWORK_TYPE_LTE) {// LTE是3g到4g的过渡，是3.9G的全球标准
                type = "4g";
            }
        }
        return type;
    }

    public static int getStatusBarHeight(Context context) {
        Class<?> c = null;
        Object obj = null;
        java.lang.reflect.Field field = null;
        int x = 0;
        int statusBarHeight = 0;
        try {
            c = Class.forName("com.android.internal.R$dimen");
            obj = c.newInstance();
            field = c.getField("status_bar_height");
            x = Integer.parseInt(field.get(obj).toString());
            statusBarHeight = context.getResources().getDimensionPixelSize(x);
            return statusBarHeight;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return statusBarHeight;
    }

    public static String getReportData(Context context) {
        String data = "";
        String nickName = context.getString(R.string.updatauserinfo_activity_nick) + ShareFileUtils.getString(ShareFileUtils.USER_NAME, "") + "<br>";
        String userId = context.getString(R.string.userid) + ShareFileUtils.getString(ShareFileUtils.ID, "") + "<br>";
        String relaVersion = context.getString(R.string.rela_version) + DeviceUtils.getVersionName(context) + "<br>";
        String mobileSystem = context.getString(R.string.mobile_system) + DeviceUtils.getReleaseVersion() + "<br>";
        String mobileModel = context.getString(R.string.mobile_model) + DeviceUtils.getModel() + "<br>";
        String deviceId = context.getString(R.string.mobile_deviceid) + DeviceUtils.getDeviceId() + "<br>";
        data = nickName + userId + relaVersion + mobileSystem + mobileModel + deviceId;
        return data;
    }

    /**
     * 获取application中指定的meta-data
     *
     * @return 如果没有获取成功(没有对应值 ， 或者异常)，则返回值为空
     */
    public static String getAppMetaData(Context ctx, String key) {
        if (ctx == null || TextUtils.isEmpty(key)) {
            return null;
        }
        String resultData = null;
        try {
            PackageManager packageManager = ctx.getPackageManager();
            if (packageManager != null) {
                ApplicationInfo applicationInfo = packageManager.getApplicationInfo(ctx.getPackageName(), PackageManager.GET_META_DATA);
                if (applicationInfo != null) {
                    if (applicationInfo.metaData != null) {
                        resultData = applicationInfo.metaData.getString(key);
                    }
                }

            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return resultData;
    }

    /**
     * 获取虚拟按键的高度
     *
     * @param context
     * @return
     */
    public static int getNavigationBarHeight(Context context) {
        int result = 0;
        if (hasNavBar(context)) {
            Resources res = context.getResources();
            int resourceId = res.getIdentifier("navigation_bar_height", "dimen", "android");
            if (resourceId > 0) {
                result = res.getDimensionPixelSize(resourceId);
            }
        }
        return result;
    }

    /**
     * 检查是否存在虚拟按键栏
     *
     * @param context
     * @return
     */
    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public static boolean hasNavBar(Context context) {
        Resources res = context.getResources();
        int resourceId = res.getIdentifier("config_showNavigationBar", "bool", "android");
        if (resourceId != 0) {
            boolean hasNav = res.getBoolean(resourceId);
            // check override flag
            String sNavBarOverride = getNavBarOverride();
            if ("1".equals(sNavBarOverride)) {
                hasNav = false;
            } else if ("0".equals(sNavBarOverride)) {
                hasNav = true;
            }
            return hasNav;
        } else { // fallback
            return !ViewConfiguration.get(context).hasPermanentMenuKey();
        }
    }

    /**
     * 判断虚拟按键栏是否重写
     *
     * @return
     */
    private static String getNavBarOverride() {
        String sNavBarOverride = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                Class c = Class.forName("android.os.SystemProperties");
                Method m = c.getDeclaredMethod("get", String.class);
                m.setAccessible(true);
                sNavBarOverride = (String) m.invoke(null, "qemu.hw.mainkeys");
            } catch (Throwable e) {
            }
        }
        return sNavBarOverride;
    }

    /**
     * @param context
     * @param uri
     * @return
     */
    public static String getFilePathFromUri(Context context, Uri uri) {
        if (null == uri) return null;
        final String scheme = uri.getScheme();
        String data = null;
        if (scheme == null)
            data = uri.getPath();
        else if (ContentResolver.SCHEME_FILE.equals(scheme)) {
            data = uri.getPath();
        } else if (ContentResolver.SCHEME_CONTENT.equals(scheme)) {
            Cursor cursor = context.getContentResolver().query(uri, new String[]{MediaStore.Images.ImageColumns
                    .DATA}, null, null, null);
            if (null != cursor) {
                if (cursor.moveToFirst()) {
                    int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                    if (index > -1) {
                        data = cursor.getString(index);
                    }
                }
                cursor.close();
            }
        }
        return data;
    }
}
