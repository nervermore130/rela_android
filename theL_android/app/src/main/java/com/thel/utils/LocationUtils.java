package com.thel.utils;


import android.Manifest;
import androidx.core.content.ContextCompat;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.thel.R;
import com.thel.app.TheLApp;

import java.util.ArrayList;
import java.util.List;

public class LocationUtils {

    public static void saveLocation(Double geoLat, Double geoLng, String city) {
        // 把位置信息缓存到share里
        ShareFileUtils.setString(ShareFileUtils.LATITUDE, geoLat + "");
        ShareFileUtils.setString(ShareFileUtils.LONGITUDE, geoLng + "");
        ShareFileUtils.setString(ShareFileUtils.CITY, city + "");
    }

    public static List<Double> GCJToBD(double lat, double lon) {
        double xPi = Math.PI * 3000.0 / 180.0;
        double x = lon, y = lat;
        double z = Math.sqrt(x * x + y * y) + 0.00002 * Math.sin(y * xPi);
        double theta = Math.atan2(y, x) + 0.000003 * Math.cos(x * xPi);
        double bd_lon = z * Math.cos(theta) + 0.0065;
        double bd_lat = z * Math.sin(theta) + 0.006;
        List<Double> result = new ArrayList<Double>();
        result.add(bd_lat);
        result.add(bd_lon);
        return result;
    }

    public static List<Double> BDToGCJ(double lat, double lon) {
        double xPi = Math.PI * 3000.0 / 180.0;
        double x = lon - 0.0065, y = lat - 0.006;
        double z = Math.sqrt(x * x + y * y) - 0.00002 * Math.sin(y * xPi);
        double theta = Math.atan2(y, x) - 0.000003 * Math.cos(x * xPi);
        double gg_lon = z * Math.cos(theta);
        double gg_lat = z * Math.sin(theta);
        List<Double> result = new ArrayList<Double>();
        result.add(gg_lat);
        result.add(gg_lon);
        return result;
    }

    static boolean isFirst = true;
    static int count = 3;

    public static void setUpLocation() {
        isFirst = true;
        count = 3;

        final AMapLocationClient mLocationClient = new AMapLocationClient(TheLApp.getContext());
        mLocationClient.setLocationListener(new AMapLocationListener() {
            @Override
            public void onLocationChanged(AMapLocation aMapLocation) {
                if (aMapLocation != null && aMapLocation.getErrorCode() == 0) {
                    //获取位置信息
                    LocationUtils.saveLocation(aMapLocation.getLatitude(), aMapLocation.getLongitude(), aMapLocation.getCity());
                    mLocationClient.stopLocation();
                    mLocationClient.onDestroy();
                } else {
                    LocationUtils.saveLocation(0.0, 0.0, "");
                    if (isFirst) {
                        DialogUtil.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.info_location_error));
                    }
                    isFirst = false;
                    count -= 1;
                    if (count == 0) {//定位三次失败后关闭
                        mLocationClient.stopLocation();
                        mLocationClient.onDestroy();
                    }
                }
            }
        });
        final AMapLocationClientOption mLocationOption = new AMapLocationClientOption();
        mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Battery_Saving);
        mLocationClient.setLocationOption(mLocationOption);
        mLocationClient.startLocation();
    }

    public static boolean isHasLocationPermission() {

        int is_access_fine_location = ContextCompat.checkSelfPermission(TheLApp.context, Manifest.permission.ACCESS_FINE_LOCATION);

        int is_access_coarse_location = ContextCompat.checkSelfPermission(TheLApp.context, Manifest.permission.ACCESS_COARSE_LOCATION);

        L.d("LocationUtils"," is_access_fine_location : " + is_access_fine_location);

        L.d("LocationUtils"," is_access_coarse_location : " + is_access_coarse_location);

        return is_access_fine_location >= 0 && is_access_coarse_location >= 0;
    }

}
