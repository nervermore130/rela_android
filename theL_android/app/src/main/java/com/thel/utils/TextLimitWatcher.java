package com.thel.utils;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;

import com.thel.app.TheLApp;


/**
 * Created by the L on 2016/9/18.
 * 输入端截取限定字符数字符串
 */
public class TextLimitWatcher implements TextWatcher {
    private int limit;
    private boolean judge;
    private String toastStr;


    public TextLimitWatcher(int limit, boolean judge, String toastStr) {
        this.limit = limit;
        this.judge = judge;
        this.toastStr=toastStr;
    }

    public void setJudge(boolean judge) {
        this.judge = judge;
    }

    public boolean getJudge() {
        return judge;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getLimit(int limit) {
        return limit;
    }

    public String getToastStr() {
        return toastStr;
    }

    public void setToastStr(String toastStr) {
        this.toastStr = toastStr;
    }

    public void setParams(int limit, boolean judge, String toastStr) {
        this.limit = limit;
        this.judge = judge;
        this.toastStr = toastStr;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (judge) {
            boolean showToast = false;
            for (int i = 0, j = 1; i < s.length(); i++, j++) {
                final char[] arr = s.toString().toCharArray();
                char ch = arr[i];
                if (Utils.isChineseChar(ch))
                    j++;
                if (j > limit) {
                    if (Utils.isChineseChar(ch))
                        j--;
                    s = s.delete(i, i + 1);
                    j--;
                    i--;
                    showToast = true;
                }
            }
            if (showToast && !TextUtils.isEmpty(toastStr)) {
                DialogUtil.showToastShort(TheLApp.getContext(), toastStr);
            }
        }
    }
}
