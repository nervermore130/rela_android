package com.thel.utils;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.thel.app.TheLApp;
import com.thel.modules.others.BannerAdParser;
import com.thel.modules.others.beans.BannerAdBean;
import com.thel.modules.others.beans.BannerAdListBean;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;

public class AgeLimitedDiscCache {

    private static final String TAG = AgeLimitedDiscCache.class.getSimpleName();

    protected File cacheDir;

    /**
     * cacheDir 缓存路径 maxAge 默认缓存时间
     */
    public AgeLimitedDiscCache(File cacheDir) {
        this.cacheDir = cacheDir;
    }

    public static void putAgeCache(Context context, final String url, final String content) {
        try {
            SharedPrefUtils.setString(SharedPrefUtils.FILE_AD, url, content);
            //			File file = context.getCacheDir();
            //			String fileDir = file.getAbsolutePath() + File.separator
            //					+ "ageCache";
            //			final File cacheDir = new File(fileDir);
            //			AgeLimitedDiscCache cache = new AgeLimitedDiscCache(cacheDir);
            //			cache.put(url, content);
            processWelcomPageAd(content);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void processWelcomPageAd(String content) throws Exception {
        if (!TextUtils.isEmpty(content)) {
            final BannerAdParser parser = new BannerAdParser();
            final BannerAdListBean bean = (BannerAdListBean) parser.parse(content);
            BannerAdBean adBean = null;
            for (BannerAdBean tempAdBean : bean.adlist) {
                if (tempAdBean.advertLocation.equals(BannerAdBean.ADV_LOCATION_COVER)) {
                    adBean = tempAdBean;
                    break;
                }
            }

            for (BannerAdBean tempAdBean : bean.adlist) {// 有新的推荐表情
                if (tempAdBean.advertLocation.equals(BannerAdBean.ADV_LOCATION_NEW_STICKER)) {
                    // 保存推荐表情的icon
                    SharedPrefUtils.setString(SharedPrefUtils.FILE_STICKERS, SharedPrefUtils.NEW_STICKER_RECOMMEND, tempAdBean.advertURL);
                    // 刷新主页图标
                    Intent intent = new Intent();
                    intent.setAction(com.thel.constants.TheLConstants.BROADCAST_UPDATE_RECOMMEND_STICKER);
                    TheLApp.getContext().sendBroadcast(intent);
                    break;
                }
            }

            if (adBean != null) {
                final String advertURL = adBean.advertURL;
                final String time = adBean.time;
                final String redirectUrl = adBean.dumpURL;
                final String title = adBean.advertTitle;
                final String type = adBean.dumpType;
                if (adBean != null && !TextUtils.isEmpty(advertURL)) {// 存储封面广告信息
                    if (!SharedPrefUtils.getString(SharedPrefUtils.FILE_AD, SharedPrefUtils.WELCOME_PAGE_AD_IMG, "").equals(advertURL)) {
                        SharedPrefUtils.setString(SharedPrefUtils.FILE_AD, SharedPrefUtils.WELCOME_PAGE_AD_IMG, advertURL);
                        SharedPrefUtils.setString(SharedPrefUtils.FILE_AD, SharedPrefUtils.WELCOME_PAGE_AD_TIME, time);
                        SharedPrefUtils.setString(SharedPrefUtils.FILE_AD, SharedPrefUtils.WELCOME_PAGE_AD_REDIRECT_URL, redirectUrl);
                        SharedPrefUtils.setString(SharedPrefUtils.FILE_AD, SharedPrefUtils.WELCOME_PAGE_AD_TITLE, title);
                        SharedPrefUtils.setString(SharedPrefUtils.FILE_AD, SharedPrefUtils.WELCOME_PAGE_AD_REDIRECT_TYPE, type);
                        //                        final ImageLoader imageLoader = ImageLoader.newInstance();
                        //                        DisplayImageOptions options = new DisplayImageOptions.Builder().bitmapConfig(Bitmap.Config.RGB_565).imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2).cacheInMemory(false).cacheOnDisk(true).considerExifParams(true).build();
                        //                        imageLoader.loadImage(ImageUtils.buildNetPictureUrl(adBean.advertURL, AppInit.displayMetrics.widthPixels, AppInit.displayMetrics.heightPixels), options, new ImageLoadingListener() {
                        //                            @Override
                        //                            public void onLoadingStarted(String s, View view) {
                        //                            }
                        //
                        //                            @Override
                        //                            public void onLoadingFailed(String s, View view, FailReason failReason) {
                        //                            }
                        //
                        //                            @Override
                        //                            public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                        //                                try {
                        //                                    if (bitmap != null && !bitmap.isRecycled()) {
                        //                                        bitmap.recycle();
                        //                                    }
                        //                                    SharedPrefUtils.setString(SharedPrefUtils.FILE_AD, SharedPrefUtils.WELCOME_PAGE_AD_IMG, advertURL);
                        //                                    SharedPrefUtils.setString(SharedPrefUtils.FILE_AD, SharedPrefUtils.WELCOME_PAGE_AD_TIME, time);
                        //                                } catch (Exception e) {
                        //                                }
                        //                            }
                        //
                        //                            @Override
                        //                            public void onLoadingCancelled(String s, View view) {
                        //
                        //                            }
                        //                        });
                    }
                }
            } else {
                SharedPrefUtils.setString(SharedPrefUtils.FILE_AD, SharedPrefUtils.WELCOME_PAGE_AD_IMG, "");
                SharedPrefUtils.setString(SharedPrefUtils.FILE_AD, SharedPrefUtils.WELCOME_PAGE_AD_TIME, "");
                SharedPrefUtils.setString(SharedPrefUtils.FILE_AD, SharedPrefUtils.WELCOME_PAGE_AD_REDIRECT_URL, "");
                SharedPrefUtils.setString(SharedPrefUtils.FILE_AD, SharedPrefUtils.WELCOME_PAGE_AD_TITLE, "");
                SharedPrefUtils.setString(SharedPrefUtils.FILE_AD, SharedPrefUtils.WELCOME_PAGE_AD_REDIRECT_TYPE, "");
            }
        } else {
            SharedPrefUtils.setString(SharedPrefUtils.FILE_AD, SharedPrefUtils.WELCOME_PAGE_AD_IMG, "");
            SharedPrefUtils.setString(SharedPrefUtils.FILE_AD, SharedPrefUtils.WELCOME_PAGE_AD_TIME, "");
            SharedPrefUtils.setString(SharedPrefUtils.FILE_AD, SharedPrefUtils.WELCOME_PAGE_AD_REDIRECT_URL, "");
            SharedPrefUtils.setString(SharedPrefUtils.FILE_AD, SharedPrefUtils.WELCOME_PAGE_AD_TITLE, "");
            SharedPrefUtils.setString(SharedPrefUtils.FILE_AD, SharedPrefUtils.WELCOME_PAGE_AD_REDIRECT_TYPE, "");
        }
    }

    public static String getAgeCache(Context context, String url, long maxAge) {
        File file = context.getCacheDir();
        String fileDir = file.getAbsolutePath() + File.separator + "ageCache";
        File cacheDir = new File(fileDir);
        try {
            AgeLimitedDiscCache cache = new AgeLimitedDiscCache(cacheDir);
            String result = cache.get(url, maxAge);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getAgeCache(Context context, String url) {
        if (context == null) {
            return "";
        }
        //		File file = context.getCacheDir();
        //		String fileDir = file.getAbsolutePath() + File.separator + "ageCache";
        //		File cacheDir = new File(fileDir);
        try {
            //			AgeLimitedDiscCache cache = new AgeLimitedDiscCache(cacheDir);
            //			String result = cache.get(url);
            //			Vlog.i(TAG, "get chache url==" + url + " reuslt==" + result);
            return SharedPrefUtils.getString(SharedPrefUtils.FILE_AD, url, "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static File getAgeCacheFile(Context context, String url) {
        File file = context.getCacheDir();
        String fileDir = file.getAbsolutePath() + File.separator + "ageCache";
        File cacheDir = new File(fileDir);
        try {
            AgeLimitedDiscCache cache = new AgeLimitedDiscCache(cacheDir);
            File cacheFile = cache.getFile(url);
            return cacheFile;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void put(final String url, final String content) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    if (!cacheDir.exists()) {
                        cacheDir.mkdirs();
                    }
                    File saveFile = new File(cacheDir, MD5Utils.md5(url));
                    if (!saveFile.exists()) {
                        saveFile.createNewFile();
                    } else {
                        if (saveFile != null) {
                            saveFile.delete();
                        }
                    }
                    BufferedWriter bw = new BufferedWriter(new FileWriter(saveFile));
                    bw.write(content);
                    bw.close();
                    Long currentTime = System.currentTimeMillis();
                    saveFile.setLastModified(currentTime);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }

    public File getFile(String key) {
        File file = new File(cacheDir, MD5Utils.md5(key));
        return file;
    }

    public String get(String key) {
        File file = new File(cacheDir, MD5Utils.md5(key));
        if (file.exists()) {
            try {
                ByteArrayOutputStream outStream = new ByteArrayOutputStream();
                FileInputStream inStream = new FileInputStream(file);
                byte[] buffer = new byte[1024];
                int len = 0;
                while ((len = inStream.read(buffer)) != -1) {
                    outStream.write(buffer, 0, len);
                }
                outStream.close();
                inStream.close();
                byte[] data = outStream.toByteArray();
                String content = new String(data);
                return content;

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    public String get(String key, long maxFileAge) {
        File file = new File(cacheDir, MD5Utils.md5(key));
        Long oldestUsage = file.lastModified();
        if (System.currentTimeMillis() - oldestUsage > maxFileAge) {
            file.delete();
            return null;
        }
        if (file.exists()) {
            try {
                ByteArrayOutputStream outStream = new ByteArrayOutputStream();
                FileInputStream inStream = new FileInputStream(file);
                byte[] buffer = new byte[1024];
                int len = 0;
                while ((len = inStream.read(buffer)) != -1) {
                    outStream.write(buffer, 0, len);
                }
                outStream.close();
                inStream.close();
                byte[] data = outStream.toByteArray();
                String content = new String(data);
                return content;

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

}