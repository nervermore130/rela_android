package com.thel.utils;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;
import android.view.View;

import com.growingio.android.sdk.collection.GrowingIO;
import com.thel.bean.gift.GIOGiftBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.video.VideoBean;
import com.thel.constants.MomentTypeConstants;
import com.thel.growingio.GIOGiftPurchaseBean;
import com.thel.growingio.GIOPayBean;
import com.thel.growingio.GIOShareBean;
import com.thel.growingio.GIOShareTrackBean;
import com.thel.growingio.GIoGiftTrackBean;
import com.thel.growingio.GIoPayTrackBean;
import com.thel.growingio.GrowingIoConstant;
import com.thel.modules.live.utils.LiveStatus;
import com.umeng.socialize.bean.SHARE_MEDIA;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by chad
 * Time 18/3/9
 * Email: wuxianchuang@foxmail.com
 * Description: TODO GIO打点管理
 */

public class GrowingIOUtil implements GrowingIoConstant {

    public static final String TAG = "GrowingIOUtil";

    public static void setViewInfo(View view, String type) {
        GrowingIO growingIO = GrowingIO.getInstance();
        if (growingIO != null) {
            GrowingIO.setViewInfo(view, type);
        }
    }

    //升级到2.3.0需要将CS1改为setUserId
    public static void uploadGrowingCS1(String userId) {
        GrowingIO growingIO = GrowingIO.getInstance();
        if (growingIO != null) {
            growingIO.setUserId(userId); //上传用户userid

            if (userId.length() > 1) {
                String subUserId = userId.substring(userId.length() - 1);
                String userGroup = getUserGroup(subUserId);
                growingIO.setPeopleVariable(GrowingIoConstant.USER_GROUP, userGroup);

            }

        }
    }

    public static String getUserGroup(String subUserId) {
        if (subUserId.equals("0")) {

            return "A";

        } else if (subUserId.equals("1")) {

            return "B";

        } else if (subUserId.equals("2") || subUserId.equals("3") || subUserId.equals("4") || subUserId.equals("5")) {

            return "C";

        } else {

            return "D";
        }

    }

    //升级到2.3.0 清除userid需要将CS1改为clearUserId
    public static void clearUserId() {
        GrowingIO growingIO = GrowingIO.getInstance();
        if (growingIO != null) {
            growingIO.clearUserId();
        }
    }

    /**
     * 视频瀑布流上报pgc/ugc
     *
     * @param videoType
     */
    public static void uploadVideoType(final int videoType) {
        GrowingIO instance = GrowingIO.getInstance();
        if (instance != null) {
            instance.track("vrVideoPageView", new JSONObject() {
                {
                    try {
                        if (videoType == VideoBean.VIDEO_TYPE_PGC) {
                            put("videoType", "pgc视频");
                        } else if (videoType == VideoBean.VIDEO_TYPE_UGC) {
                            put("videoType", "ugc视频");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    /**
     * GIO视频观看时长上报
     *
     * @param duration
     */
    public static void uploadCommonVideoDuration(long duration) {
        GrowingIO instance = GrowingIO.getInstance();
        if (instance != null) {
            instance.track("videoWatchTime", duration);
        }
    }

    /**
     * GIO瀑布流页用户视频观看时长上报
     *
     * @param duration
     */
    public static void uploadRecommendVideoDuration(long duration) {
        GrowingIO instance = GrowingIO.getInstance();
        if (instance != null) {
            instance.track("recommendvideoWatchTime", duration);
        }
    }

    /**
     * GIO视频直播页用户直播观看时长上报 区分视频直播和声音直播
     *
     * @param time
     */
    public static void uploadLiveTimeAndCount(long time) {
        GrowingIO instance = GrowingIO.getInstance();
        if (instance != null) {
            instance.track("liveTime", time);

            instance.track("liveCount");

        }
    }

    /**
     * GIO视频直播页用户直播观看时长上报 区分视频直播和声音直播
     *
     * @param type
     */
    public static void postNativePage(String type) {
        GrowingIO instance = GrowingIO.getInstance();
        if (instance != null) {
            instance.track(type);
        }
    }

    /**
     * 主播端直播时长，增加直播类型／主播昵称/主播ID 维度
     *
     * @param time
     */
    public static void postLiveTimeEvent(@NonNull long time, @NonNull int liveType, @NonNull String hostName, @NonNull String hostID) {
        String type = "";
        switch (liveType) {
            case 0:
                type = GrowingIoConstant.MATERIAL_LIVE_VIDEO;

                break;
            case 1:
                type = GrowingIoConstant.MATERIAL_LIVE_VOICE;

                break;
            case 2:
                type = GrowingIoConstant.MATERIAL_LIVE_MULTI;

                break;


        }

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("liveType", type);
            jsonObject.put("hostName", hostName);
            jsonObject.put("hostID", hostID);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final GrowingIO instance = GrowingIO.getInstance();
        final String key_live_type = "liveTime";

        if (instance != null) {
            instance.track(key_live_type, time, jsonObject);
        }
    }

    /***
     * 上传直播页面级浏览量
     * */
    public static void uploadLivePageView(int liveType) {
        String type = "";
        switch (liveType) {
            case 0:
                type = GrowingIoConstant.MATERIAL_LIVE_VIDEO;

                break;
            case 1:
                type = GrowingIoConstant.MATERIAL_LIVE_VOICE;

                break;
            case 2:
                type = GrowingIoConstant.MATERIAL_LIVE_MULTI;

                break;
        }
        final String livePageView = GrowingIoConstant.TRACK_LIVE_PAGE_VIEW;
        final String finalType = type;
        JSONObject obj = new JSONObject() {
            {
                try {
                    put("liveType", finalType);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        GrowingIOUtil.track(livePageView, obj);
    }

    /**
     * 根据直播类型上传直播观看时间
     */
    public static void liveWatchTimeTrack(@NonNull int liveType, @NonNull long number) {
        String type = "";
        switch (liveType) {
            case 0:
                type = GrowingIoConstant.MATERIAL_LIVE_VIDEO;

                break;
            case 1:
                type = GrowingIoConstant.MATERIAL_LIVE_VOICE;

                break;
            case 2:
                type = GrowingIoConstant.MATERIAL_LIVE_MULTI;

                break;
        }

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("liveType", type);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final GrowingIO instance = GrowingIO.getInstance();
        final String key_live_type = GrowingIoConstant.LIVE_WATCH_TIME;

        if (instance != null) {
            instance.track(key_live_type, number, jsonObject);
        }

    }

    public static void shareTrack(@NonNull String track, @NonNull GIOShareBean bean) {
        final GrowingIO instance = GrowingIO.getInstance();

        try {
            final JSONObject object = new JSONObject(GsonUtils.createJsonString(bean));
            if (instance != null) {
                instance.track(track, object);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public static void shareTrack(@NonNull GIOShareTrackBean bean) {
        final String track = bean.track;
        final GIOShareBean shareBean = bean.shareBean;
        if (!TextUtils.isEmpty(track)) {
            shareTrack(track, shareBean);
        }
    }

    public static void goToWeb(String eventName) {
        L.d(TAG, "eventName :" + eventName);
        final GrowingIO instance = GrowingIO.getInstance();
        instance.track(eventName);
    }

    public static void uploadWatchTimeLonger2M(String eventName) {
        L.d(TAG, "eventName :" + eventName);
        final GrowingIO instance = GrowingIO.getInstance();
        instance.track(eventName);
    }

    public static void goToChat(boolean isFollow) {
        final GrowingIO instance = GrowingIO.getInstance();
        if (isFollow) {
            instance.track("Follow");
        } else {
            instance.track("Chat");
        }
    }

    /************************************trackBean的共用方法*********************************************/

    public static void setMomentShareTrack(@NonNull MomentsBean momentsBean, @NonNull GIOShareTrackBean bean) {
        bean.track = getTrack(momentsBean);
        bean.shareBean.shareMaterialType = getMaterial(momentsBean);
        if (MomentTypeConstants.MOMENT_TYPE_LIVE.equals(momentsBean.momentsType)) {
            bean.shareBean.shareEntry = ENTRY_LIVE_MOMENT;
        }
    }

    public static String getMaterial(@NonNull MomentsBean momentBean) {
        final String momentType = momentBean.momentsType;
        switch (momentType) {
            case MomentTypeConstants.MOMENT_TYPE_THEME:
                return GrowingIoConstant.MATERIAL_THEME;
            case MomentTypeConstants.MOMENT_TYPE_THEME_REPLY:
                return GrowingIoConstant.MATERIAL_THEME_REPLY;
            case MomentTypeConstants.MOMENT_TYPE_VOICE_LIVE:
                return GrowingIoConstant.MATERIAL_LIVE_VOICE;
            case MomentTypeConstants.MOMENT_TYPE_LIVE:
                return GrowingIoConstant.MATERIAL_LIVE_VIDEO;
            case MomentTypeConstants.MOMENT_TYPE_VIDEO:
                return MATERIAL_VIDEO;
            default:
                return momentBean.momentsType;
        }
    }


    public static String getTrack(@NonNull MomentsBean momentBean) {
        final String momentType = momentBean.momentsType;
        if (MomentsBean.MOMENT_TYPE_THEME.equals(momentType) || MomentsBean.MOMENT_TYPE_THEME_REPLY.equals(momentType)) {
            return GrowingIoConstant.TOPIC_SHARE;
        } else if (MomentsBean.MOMENT_TYPE_LIVE.equals(momentType) || MomentsBean.MOMENT_TYPE_VOICE_LIVE.equals(momentType)) {
            return GrowingIoConstant.LIVE_SHARE;
        } else if (MomentsBean.MOMENT_TYPE_VIDEO.equals(momentType)) {
            return GrowingIoConstant.VIDEO_SHARE;
        } else {
            return GrowingIoConstant.LOG_SHARE;
        }
    }


    public static void payTrack(@NonNull String type, GIOPayBean payBean) {
        final GrowingIO instance = GrowingIO.getInstance();
        try {
            final JSONObject object = new JSONObject(GsonUtils.createJsonString(payBean));
            if (instance != null) {
                instance.track(type, object);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void payTrack(@NonNull String Clicktype) {
        final GrowingIO instance = GrowingIO.getInstance();
        if (instance != null) {
            instance.track(Clicktype);
        }

    }

    public static void payEvar(@NonNull String entry_evar) {
        final GrowingIO instance = GrowingIO.getInstance();
        if (instance != null) {
            instance.setEvar(GrowingIoConstant.RMDPURCHASEENTRY_EVAR, entry_evar);
        }

    }

    public static void payTrack(@NonNull GIoPayTrackBean bean, @NonNull int number) {
        final String type = bean.track;
        final GIOPayBean payBean = bean.payBean;
        if (!TextUtils.isEmpty(type)) {
            if (number != 0) {
                payTrack(type, number, payBean);

            } else {
                payTrack(type, payBean);
            }
        }
    }

    public static void payTrack(@NonNull String type, @NonNull int number, @NonNull GIOPayBean payOrderBean) {
        final GrowingIO instance = GrowingIO.getInstance();
        try {
            final JSONObject object = new JSONObject(GsonUtils.createJsonString(payOrderBean));
            if (instance != null) {
                instance.track(type, number, object);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void payGiftTrack(@NonNull GIoGiftTrackBean bean) {
        final String type = bean.track;
        GIOGiftPurchaseBean payBean = bean.payBean;
        if (!TextUtils.isEmpty(type)) {
            payGiftTrack(type, payBean);
        }
    }

    public static void payGiftTrack(@NonNull String type, GIOGiftPurchaseBean payBean) {
        final GrowingIO instance = GrowingIO.getInstance();
        try {
            final JSONObject object = new JSONObject(GsonUtils.createJsonString(payBean));
            if (instance != null) {
                instance.track(type, object);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void track(String track, JSONObject obj) {
        if (TextUtils.isEmpty(track) || obj == null) {
            return;
        }
        final GrowingIO instance = GrowingIO.getInstance();
        instance.track(track, obj);
    }

    public static void track(Fragment fragment, String key, String value) {
        if (TextUtils.isEmpty(key) || TextUtils.isEmpty(value) || fragment == null) {
            return;
        }
        final GrowingIO instance = GrowingIO.getInstance();
        instance.setPageVariable(fragment, key, value);

    }

    public static void track(Fragment fragment, String key, int value) {
        if (TextUtils.isEmpty(key) || fragment == null) {
            return;
        }
        final GrowingIO instance = GrowingIO.getInstance();
        instance.setPageVariable(fragment, key, value);

    }

    public static void track(Activity activity, String key, String value) {
        if (TextUtils.isEmpty(key) || TextUtils.isEmpty(value) || activity == null) {
            return;
        }
        final GrowingIO instance = GrowingIO.getInstance();
        instance.setPageVariable(activity, key, value);
    }

    public static String getShareType(SHARE_MEDIA platform) {
        switch (platform) {
            case WEIXIN_CIRCLE:
                return TYPE_WEIXIN_CIRCLE;
            case WEIXIN:
                return TYPE_WEIXIN;
            case SINA:
                return TYPE_SINA;
            case QZONE:
                return TYPE_QZONE;
            case QQ:
                return TYPE_QQ;
            default:
                return "";

        }
    }


    public static void pushIncome(LiveStatus liveStatus, GIOGiftBean gioGiftBean) {

        String json = GsonUtils.createJsonString(gioGiftBean);

        String type = null;

        final GrowingIO instance = GrowingIO.getInstance();

        if (liveStatus != null) {
            switch (liveStatus) {
                case LIVE_AUDIENCE_LINK_MIC:
                    type = "audienceLinkMicMoney";
                    break;
                case LIVE_PK:
                    type = "pkMoney";
                    break;
                case LIVE_LINK_MIC:
                    type = "hostLinkMicMoney";
                    break;
                case LIVE_MULTI_LINK_MIC:
                    type = "voiceChatRoomMoney";
                    break;
                default:
                    break;
            }

            if (type != null && json != null) {

                try {
                    final JSONObject object = new JSONObject(json);
                    instance.track(type, object);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }
    }

    /***
     * 从各个路径跳转到直播间
     *
     * **/
    public static void postWatchLiveEntry(String value) {
        final GrowingIO instance = GrowingIO.getInstance();
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(GrowingIoConstant.WATCH_LIVE_ENTRY_KEY, value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        instance.track(GrowingIoConstant.WATCH_LIVE_ENTRY, jsonObject);
    }


    /**
     * 针对引导用户主动发起聊天的实验进行以下埋点：
     * 1、记录触发实验规则的聊天发起者UserId，和该发起者发送对象用户的UserId
     * 2、触发该实验规则的用户为AC组（即尾号是0、2、3、4、5）
     */
    public static void postVisitToChat(String toUserId) {
        final GrowingIO instance = GrowingIO.getInstance();
        JSONObject jsonObject = new JSONObject();
        try {
            String userId = UserUtils.getMyUserId();

            jsonObject.put(GrowingIoConstant.TO_USER_ID, toUserId);
            jsonObject.put(GrowingIoConstant.MY_USER_ID, userId);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        instance.track(GrowingIoConstant.GUIDE_VISIT_CHAT, jsonObject);


    }

    /**
     * 上传观众连麦 多人连麦上麦次数／人数／时长 4.18.0
     */
    public static void postGuestLinkMic(long guestLinkMicDuration) {
        final GrowingIO instance = GrowingIO.getInstance();
        instance.track(GrowingIoConstant.GUEST_LINK_MIC);
        instance.track(GrowingIoConstant.GuestLinkMicDuration, guestLinkMicDuration);
    }

    /**
     * 直播间各类型关注情况分类（资料卡关注／退出直播间引导关注／主播直接关注）（增加事件直播间关注、维度被关注对象[主播、观众]和关注方式）
     */
    public static void followLiveUser(String FollowWay, String FollowTarget) {

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("FollowWay", FollowWay);
            jsonObject.put("FollowTarget", FollowTarget);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final GrowingIO instance = GrowingIO.getInstance();
        final String key_live_type = GrowingIoConstant.LIVE_ROOM_FOLLOW;

        if (instance != null) {
            instance.track(key_live_type, jsonObject);
        }
    }

    /***
     * 埋点上报关闭push通知和关闭地理位置通知
     *
     * **/
    public static void tracePush(String key) {
        try {
            String myUserId = Utils.getMyUserId();
            final GrowingIO instance = GrowingIO.getInstance();

            if (TextUtils.isEmpty(myUserId)) {
                instance.track(key, 0);

            } else {
                int userId = Integer.parseInt(myUserId);
                instance.track(key, userId);

            }
        } catch (Exception c) {

        }

    }

    /**
     * 上报新用户进来关注推荐的用户
     */
    public static void postFollowUsers(String userid) {
        GrowingIO instance = GrowingIO.getInstance();
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("userId", userid);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        instance.track(GrowingIoConstant.FOLLOW_USERS, jsonObject);
    }

}
