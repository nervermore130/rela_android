package com.thel.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.thel.app.TheLApp;

import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.List;
import java.util.Locale;

import static android.content.Context.LOCATION_SERVICE;

/**
 * 手机信息类 获取手机硬件相关信息，和运行商信息
 *
 * @author zhangwenjia
 */
public class PhoneUtils {
    private static PhoneUtils uniqueInstance = new PhoneUtils(TheLApp.context);
    String TAG = "ZLW.LocationUtils";
    private Context mContext;
    private Location location;
    private String locationProvider;

    private PhoneUtils(Context context) {
        mContext = context;
        //getLocationInfo();
        // getLastKnownLocation();
    }

    private void getLocationInfo() {
        //1.获取位置管理器
        locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);
        //2.获取位置提供器，GPS或是NetWork


        List<String> providers = locationManager.getProviders(true);
        if (providers.contains(LocationManager.NETWORK_PROVIDER)) {
            //如果是网络定位
            Log.d(TAG, "如果是网络定位");
            locationProvider = LocationManager.NETWORK_PROVIDER;
        } else if (providers.contains(LocationManager.GPS_PROVIDER)) {
            //如果是GPS定位
            Log.d(TAG, "如果是GPS定位");
            locationProvider = LocationManager.GPS_PROVIDER;
        } else {
            Log.d(TAG, "没有可用的位置提供器");
            return;
        }
        // 需要检查权限,否则编译报错,想抽取成方法都不行,还是会报错。只能这样重复 code 了。
        if (Build.VERSION.SDK_INT >= 23 && ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }


        //从可用的位置提供器中，匹配以上标准的最佳提供器
        if (ActivityCompat.checkSelfPermission(TheLApp.context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(TheLApp.context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "onCreate: 没有权限 ");
            return;
        }
        Location location = locationManager.getLastKnownLocation(locationProvider);
        Log.d(TAG, "onCreate: " + (location == null) + "..");
        if (location != null) {
            Log.d(TAG, "onCreate: location");
            //不为空,显示地理位置经纬度
            setLocation(location);
        }


        // 监视地理位置变化，第二个和第三个参数分别为更新的最短时间minTime和最短距离minDistace
        locationManager.requestLocationUpdates(locationProvider, 5000, 2, locationListener);

    }

    private void getLastKnownLocation() {
        int MIN_TIME_BW_UPDATES = 10000;
        int MIN_DISTANCE_CHANGE_FOR_UPDATES = 10000;
        try {
            //1.获取位置管理器
            locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);

            boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

            boolean isPassiveEnabled = locationManager.isProviderEnabled(LocationManager.PASSIVE_PROVIDER);

            boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            // 需要检查权限,否则编译报错,想抽取成方法都不行,还是会报错。只能这样重复 code 了。
            if (Build.VERSION.SDK_INT >= 23 && ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }


            if (isGPSEnabled || isNetworkEnabled || isPassiveEnabled) {

                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled && location == null) {
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, locationListener);
                    Log.d("GPS", "GPS Enabled");
                    if (locationManager != null) {
                        location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    }
                }
                if (isPassiveEnabled && location == null) {
                    locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, locationListener);
                    Log.d("Network", "Network Enabled");
                    if (locationManager != null) {
                        location = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
                    }
                }

                if (isNetworkEnabled && location == null) {
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, locationListener);
                    Log.d("Network", "Network Enabled");
                    if (locationManager != null) {
                        location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    }
                }
                setLocation(location);

            } else {
                return;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //采用Double CheckLock(DCL)实现单例
    public static PhoneUtils getInstance() {
        return uniqueInstance;
    }

    private void setLocation(Location location) {
        this.location = location;
        String address = "纬度：" + location.getLatitude() + "经度：" + location.getLongitude();
        Log.d(TAG, address);
    }

    //获取经纬度
    public Location showLocation() {

        return location;

    }


    // 移除定位监听
    public void removeLocationUpdatesListener() {
        // 需要检查权限,否则编译不过
        if (Build.VERSION.SDK_INT >= 23 && ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        if (locationManager != null) {
            uniqueInstance = null;
            locationManager.removeUpdates(locationListener);
        }


    }

    /**
     * LocationListern监听器
     * 参数：地理位置提供器、监听位置变化的时间间隔、位置变化的距离间隔、LocationListener监听器
     */

    LocationListener locationListener = new LocationListener() {

        /**
         * 当某个位置提供者的状态发生改变时
         */
        @Override
        public void onStatusChanged(String provider, int status, Bundle arg2) {

        }

        /**
         * 某个设备打开时
         */
        @Override
        public void onProviderEnabled(String provider) {

        }

        /**
         * 某个设备关闭时
         */
        @Override
        public void onProviderDisabled(String provider) {

        }

        /**
         * 手机位置发生变动
         */
        @Override
        public void onLocationChanged(Location location) {
            location.getAccuracy();//精确度
            String address = "纬度：" + location.getLatitude() + "经度2222：" + location.getLongitude();
            Log.d(TAG, address);
            setLocation(location);
        }
    };

    private LocationManager locationManager;

    /**
     * 获取当前操作系统的语言
     *
     * @return String 系统语言
     */
    public static String getSysLanguage() {
        return Locale.getDefault().getLanguage();
    }

    /**
     * 获取系统平台版本
     *
     * @return String 系统平台版本
     */
    public static String getOSVersion() {
        return Build.VERSION.RELEASE;
    }

    /**
     * 获取操作系统的版本号
     *
     * @return String 系统版本号
     */
    public static String getSysRelease() {
        return Build.VERSION.RELEASE;
    }

    /**
     * 获取手机型号
     *
     * @return String 手机型号
     */
    public static String getModel() {
        return Build.MODEL;
    }

    /**
     * 获取sim卡序列号
     *
     * @param context 上下文 return sim卡的序列号
     */
    public static String getSimSerialNum(Context context) {
        if (null != context) {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

            return telephonyManager.getSimSerialNumber();
        }
        return "sim card number unknown";
    }

    /**
     * 读取手机串号
     *
     * @param context 上下文
     * @return String 手机串号
     */
    public static String getTelephoneSerialNum(Context context) {
        String deviceId = "000000000000000";

        if (null != context) {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            deviceId = telephonyManager.getDeviceId();
            if (null == deviceId || deviceId.length() == 0) {
                deviceId = getLocalMacAddress(context);
                if (deviceId == null || deviceId.length() == 0) {
                    deviceId = "000000000000000";
                }
            }
            return deviceId;
        }
        return deviceId;

    }

    /**
     * 获取运营商信息
     *
     * @param context 上下文
     * @return String 运营商信息
     */
    public static String getCarrier(Context context) {
        if (null != context) {
            TelephonyManager telManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            String imsi = telManager.getSubscriberId();
            if (imsi != null && imsi.length() > 0) {
                // 因为移动网络编号46000下的IMSI已经用完，所以虚拟了一个46002编号，134/159号段使用了此编号
                if (imsi.startsWith("46000") || imsi.startsWith("46002")) {
                    return "China Mobile";
                } else if (imsi.startsWith("46001")) {
                    return "China Unicom";
                } else if (imsi.startsWith("46003")) {
                    return "China Telecom";
                }
            }
        }
        return "Operators Unknown";
    }

    /**
     * 获取Wifi网卡MAC地址
     *
     * @return String Wifi网卡MAC地址
     */
    public static String getLocalMacAddress(Context context) {
        if (null != context) {
            WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            WifiInfo info = wifi.getConnectionInfo();
            return info.getMacAddress();
        }

        return "mac address unknown";
    }

    /**
     * 获取是否设置了代理信息
     *
     * @return proxy 对象
     */
    public static Proxy getProxy() {
        Context context = TheLApp.context;

        Proxy proxy = null;
        if (null != context) {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            // 获得当前网络信息
            NetworkInfo networkInfo = cm.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isAvailable() && networkInfo.getType() == ConnectivityManager.TYPE_MOBILE) {
                String proxyHost = android.net.Proxy.getDefaultHost(); // 代理主机
                int port = android.net.Proxy.getDefaultPort(); // 代理端口
                if (proxyHost != null) {
                    InetSocketAddress sa = new InetSocketAddress(proxyHost, port);
                    if (sa != null) {
                        proxy = new Proxy(Proxy.Type.HTTP, sa);
                    }
                }
            }
        }
        return proxy;
    }

    /**
     * 获取SD卡剩余空间的大小
     *
     * @return long SD卡剩余空间的大小（单位：byte）
     */
//    public static long getSDSize() {
//        String str = Environment.getExternalStorageDirectory().getPath();
//        StatFs localStatFs = new StatFs(str);
//        long blockSize = localStatFs.getBlockSize();
//        return localStatFs.getAvailableBlocks() * blockSize;
//    }

    /**
     * 判断手机是否有sd卡
     *
     * @return
     */
    public static boolean hasSdcard() {
        String status = Environment.getExternalStorageState();
        return status.equals(Environment.MEDIA_MOUNTED);
    }

    /**
     * 判断是否连接网络
     *
     * @param context 上下文
     * @return boolean true：表示连接|false：无网
     */
    public static boolean isNetworkAvailable(Context context) {
        if (null != context) {
            try {
                ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo info = cm.getActiveNetworkInfo();
                return (info != null && info.isConnected());
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }
        return false;
    }

    /**
     * 获取网络类型
     *
     * @param context 上下文
     * @return String 返回网络类型
     */
    public static String getAccessNetworkType(Context context) {
        if (null != context) {
            int type = 0;
            try {
                ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                type = cm.getActiveNetworkInfo().getType();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (type == ConnectivityManager.TYPE_WIFI) {
                return "wifi";
            } else if (type == ConnectivityManager.TYPE_MOBILE) {
                return "3G/GPRS";
            }
        }
        return "network unknown";
    }

    public static final int TYPE_MOBILE_1G = 1;
    public static final int TYPE_MOBILE_2G = 2;
    public static final int TYPE_MOBILE_3G = 3;
    public static final int TYPE_MOBILE_4G = 4;
    public static final int TYPE_UNDEFINE = 100;
    public static final int TYPE_WIFI = 0;
    public static final int TYPE_NO = -1;

    /**
     * 获得当前网络类型
     */
    public static int getNetWorkType() {

        ConnectivityManager cm = (ConnectivityManager) TheLApp.context.getSystemService(Context.CONNECTIVITY_SERVICE);
        // 获得当前网络信息
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni != null && ni.isAvailable()) {
            int type = ni.getType();
            int subType = ni.getSubtype();
            if (type == ConnectivityManager.TYPE_WIFI) {
                return TYPE_WIFI;
            } else if (type == ConnectivityManager.TYPE_MOBILE) {
                switch (subType) {
                    case TelephonyManager.NETWORK_TYPE_1xRTT:
                    case TelephonyManager.NETWORK_TYPE_CDMA:
                        return TYPE_MOBILE_1G; // ~ 50-100 kbps
                    case TelephonyManager.NETWORK_TYPE_EDGE:
                    case TelephonyManager.NETWORK_TYPE_GPRS:
                        return TYPE_MOBILE_2G; // ~ 50-100 kbps
                    case TelephonyManager.NETWORK_TYPE_HSDPA:
                    case TelephonyManager.NETWORK_TYPE_HSPA:
                    case TelephonyManager.NETWORK_TYPE_HSUPA:
                    case TelephonyManager.NETWORK_TYPE_UMTS:
                        return TYPE_MOBILE_3G; // ~ 2-14 Mbps
                    case TelephonyManager.NETWORK_TYPE_LTE: // API level 11
                        return TYPE_MOBILE_4G; // ~ 10+ Mbps
                    // Unknown
                    case TelephonyManager.NETWORK_TYPE_UNKNOWN:
                    default:
                        return TYPE_UNDEFINE;
                }
            } else {
                return TYPE_UNDEFINE;
            }
        }

        return TYPE_NO;
    }

    /**
     * 获取手机的IMSI号
     *
     * @return String 获取手机的IMSI号
     */
    public static String getImsiNum(Context context) {
        String imsi = "";
        if (null != context) {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            imsi = telephonyManager.getSubscriberId();
        }
        if (null == imsi || imsi.length() == 0) {
            imsi = "000000000000000";
        }
        return imsi;
    }

    /**
     * GPS是否打开
     *
     * @return
     */
    public static boolean isGpsOpen() {
        LocationManager lm = (LocationManager) TheLApp.context.getSystemService(LOCATION_SERVICE);
        boolean GPS_status = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);// 获得手机是不是设置了GPS开启状态true：gps开启，false：GPS未开启
        // boolean NETWORK_status =
        // lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);//另一种Gpsprovider（Google网路地图）

        return GPS_status;

    }

    /**
     * 判断定位权限是否打开
     */
    public static boolean isHasLocationPermission() {
        int is_access_fine_location = ContextCompat.checkSelfPermission(TheLApp.context, Manifest.permission.ACCESS_FINE_LOCATION);
        return is_access_fine_location >= 0;
    }
}
