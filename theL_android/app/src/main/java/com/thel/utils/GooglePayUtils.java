package com.thel.utils;

import android.text.TextUtils;

import com.thel.app.TheLApp;
import com.thel.bean.GoogleIapNotifyResultBean;


/**
 * google钱包相关工具类
 * Created by setsail on 16/12/6.
 */
public class GooglePayUtils {

    public static final String GOOGLEPAY = "googlepay";
    public static final String GOOGLEPAY_SOURCE = "android-googlepay";

    private static GooglePayUtils alipayUtils;

    public static final String KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtFlfziR2bbTd1Js45d9CXn1gUq/rmJQxt0SyuZcyyJO5FBzrC62G7JAOXDnihlG06iJCI6Xf6jh82va+eigQ5YaqhwBconNL/nC42jNo95S/5mmr9nTWCX8zE7+FzD7qVChnsiDQkUzxyWD+wtDOIz2fpPKE7sthl27gqda48WGVtXWBDYODO5ijzX1cUM0OVRF90w+3z2kAr9d7YSmJqH5L6/C37nKVvdD6rYvjVAcMlPLn7pU/izPUj+VokEBWE2ZTpqO+xIXqBXRxYaqnySlcBP95igAaO7Z6tHMfTzPxXoGh1XO0KK/dRsTMWvnmAS9R4hXpyoxDYlLQirjWuQIDAQAB";

    private GooglePayUtils() {
    }

    public static GooglePayUtils getInstance() {
        if (alipayUtils == null) {
            alipayUtils = new GooglePayUtils();
        }
        return alipayUtils;
    }

    public boolean isNotified(GoogleIapNotifyResultBean result, boolean toastError) {
        if (result != null && !TextUtils.isEmpty(result.errcode)) {
            if (toastError)
                if ("zh_CN".equals(DeviceUtils.getLanguage()) && !TextUtils.isEmpty(result.errdesc)) {
                    DialogUtil.showToastShort(TheLApp.getContext(), result.errdesc);
                } else if (!TextUtils.isEmpty(result.errdesc_en)) {
                    DialogUtil.showToastShort(TheLApp.getContext(), result.errdesc_en);
                } else if (!TextUtils.isEmpty(result.errdesc)) {
                    DialogUtil.showToastShort(TheLApp.getContext(), result.errdesc);
                } else {
                    DialogUtil.showToastShort(TheLApp.getContext(), "error_" + result.errcode);
                }
            return false;
        } else {
            return result.received == 1;
        }
    }

}
