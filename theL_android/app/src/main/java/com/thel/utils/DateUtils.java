package com.thel.utils;


import com.thel.R;
import com.thel.app.TheLApp;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 时间相关的工具类
 *
 * @author zhangwenjia
 */
public class DateUtils {

    /**
     * 组成时间字符串
     *
     * @param time 毫秒
     * @return String 时间字符串
     */
    public static String createTimeString(long time) {
        int day = getDay(time);
        int hour = getHours(time);
        int min = getMinute(time);
        int sec = getSecond(time);
        if (0 != day) {
            return day + "天" + hour + "时" + min + "分" + sec + "秒";
        } else if (0 == day && 0 != hour) {
            return hour + "时" + min + "分" + sec + "秒";
        } else if (0 == day && 0 == hour && 0 != min) {
            return min + "分" + sec + "秒";
        } else if (0 == day && 0 == hour && 0 == min && 0 != sec) {
            return sec + "秒";
        } else {
            return sec + "秒";
        }
    }

    /**
     * 获取所剩小时
     *
     * @param timeLeft 剩余时间（毫秒数）
     * @return int 剩余小时数
     */
    public static int getDay(long timeLeft) {
        return (int) (timeLeft / 3600000) / 24;
    }

    /**
     * 获取所剩分钟
     *
     * @param timeLeft 剩余时间（毫秒数）
     * @return int 剩余分钟数
     */
    public static int getMinute(long timeLeft) {
        return (int) (timeLeft % 3600000 / 60000);
    }

    /**
     * 获取所剩秒
     *
     * @param timeLeft 剩余时间（毫秒数）
     * @return int 剩余秒数
     */
    public static int getSecond(long timeLeft) {
        return (int) (timeLeft % 3600000 % 60000 / 1000);
    }

    /**
     * 获取所剩小时
     *
     * @param timeLeft 剩余时间（毫秒数）
     * @return int 剩余小时数
     */
    public static int getHours(long timeLeft) {
        return (int) (timeLeft / 3600000) % 24;
    }

    /**
     * 获取系统当前时间（年月日时分秒），精确到秒
     *
     * @return 返回当前时间字符串
     */
    public static String getNowTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        return sdf.format(Calendar.getInstance().getTime());
    }

    /**
     * 获取系统当前时间（年-月-日），yyyy-MM-dd
     *
     * @return 返回当前时间字符串
     */
    public static String getNowToDay() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(Calendar.getInstance().getTime());
    }

    /**
     * 获取系统当前时间（时：分：秒），HH:mm:ss
     *
     * @return 返回当前时间字符串
     */
    public static String getNowFromHour() {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        return sdf.format(Calendar.getInstance().getTime());
    }

    /**
     * 获取系统当前时间（年-月-日 时：分：秒），yyyy-MM-dd HH:mm:ss
     *
     * @return 返回当前时间字符串
     */
    public static String getNowFormat() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(Calendar.getInstance().getTime());
    }

    /**
     * 获取格式化的系统当前时间（年-月-日 时：分：秒），yyyy-MM-dd HH:mm:ss
     *
     * @return 返回当前时间字符串
     */
    public static String getFormatTimeNow(String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(Calendar.getInstance().getTime());
    }

    /**
     * 获取格式化的系统当前时间（年-月-日 时：分：秒），yyyy-MM-dd HH:mm:ss
     *
     * @return 返回当前时间字符串
     */
    public static String getFormatTimeFromMillis(long time, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(new Date(time));
    }

    /**
     * 判断当前时间是否在指定时间之间
     *
     * @param star 开始时间
     * @param end  结束时间
     * @return boolean true:代表在所传来的时间段内
     */
    public static boolean isTimeBetween(String star, String end) {
        SimpleDateFormat localTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date sdate = localTime.parse(star);
            Date edate = localTime.parse(end);
            Date date = new Date();
            if (date.after(sdate) && date.before(edate)) {
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    /**
     * 计算指定时间是否在开始和结束时间之间 字符串类型的比较 传入的时间格式为2012-09-06 12:52:24
     *
     * @param time      指定时间
     * @param endtime   结束时间
     * @param starttime 开始时间
     * @return boolean 返回布尔值
     */
    public static boolean isBetweenTimeStr(String time, String starttime, String endtime) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 日期格式化类
        try {
            Date sysTime = simpleDateFormat.parse(time); // 指定时间
            Date endTime = simpleDateFormat.parse(endtime); // 结束时间
            Date startTime = simpleDateFormat.parse(starttime);
            return isBetweenTimeStr(sysTime, endTime, startTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 计算指定时间是否在开始和结束时间之间 字符串类型的比较 传入的时间格式为20120906125224
     *
     * @param time      指定时间
     * @param endtime   结束时间
     * @param starttime 开始时间
     * @return boolean 返回布尔值
     */
    public static boolean isBetweenTimeForNum(String time, String starttime, String endtime) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");// 日期格式化类
        try {
            Date sysTime = simpleDateFormat.parse(time); // 指定时间
            Date endTime = simpleDateFormat.parse(endtime); // 结束时间
            Date startTime = simpleDateFormat.parse(starttime);
            return isBetweenTimeStr(sysTime, endTime, startTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 计算指定时间是否在开始和结束时间之间 字符串类型的比较
     *
     * @param sysTime   指定时间
     * @param endTime   结束时间
     * @param startTime 开始时间
     * @return boolean 返回布尔值
     */
    private static boolean isBetweenTimeStr(Date sysTime, Date endTime, Date startTime) {
        int compareResult = sysTime.compareTo(startTime);
        if (compareResult == -1) {
            return false;
        } else if (compareResult == 0) {
            return true;
        } else if (compareResult == 1) {
            compareResult = sysTime.compareTo(endTime);
            return compareResult < 0;
        }
        return false;
    }

    /**
     * 计算指定时间是否在结束时间之后
     *
     * @param time    指定时间
     * @param endtime 结束时间
     * @return boolean
     */
    public static boolean isAfterTime(String time, String endtime) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 日期格式化类
        try {
            Date sysTime = simpleDateFormat.parse(time); // 指定时间
            Date endTime = simpleDateFormat.parse(endtime); // 结束时间
            // 如果处于开始和结束时间之间，则返回true
            if (sysTime.after(endTime)) {
                return true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 获取毫秒时间是星期几
     *
     * @param millis
     * @return
     */
    public static int getDayOfWeek(long millis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(millis);
        return calendar.get(Calendar.DAY_OF_WEEK);
    }

    // 星座临界点
    private static final int[] constellationEdgeDay = {20, 19, 21, 20, 21, 21, 23, 23, 23, 23, 22, 22};

    /**
     * 根据日期获取星座
     *
     * @param time
     * @return
     */
    public static String date2Constellation(Calendar time) {
        String[] constellationArr = TheLApp.getContext().getResources().getStringArray(R.array.userinfo_constellation);
        int month = time.get(Calendar.MONTH);
        int day = time.get(Calendar.DAY_OF_MONTH);
        if (day < constellationEdgeDay[month]) {
            month = month - 1;
        }
        if (month >= 0 && month < constellationArr.length) {
            return constellationArr[month];
        }
        //default to return 魔羯
        return constellationArr[11];
    }

    /**
     * 根据日期获取星座
     *
     * @param timeStr
     * @return
     */
    public static String date2Constellation(String timeStr) {
        Calendar time = Calendar.getInstance();
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            time.setTime(sdf.parse(timeStr));
        } catch (Exception e) {
            e.printStackTrace();
        }
        String[] constellationArr = TheLApp.getContext().getResources().getStringArray(R.array.userinfo_constellation);
        int month = time.get(Calendar.MONTH);
        int day = time.get(Calendar.DAY_OF_MONTH);
        if (day < constellationEdgeDay[month]) {
            month = month - 1;
        }
        if (month >= 0 && month < constellationArr.length) {
            return constellationArr[month];
        }
        //default to return 魔羯
        return constellationArr[11];
    }

    public static String monthFormat(String month) {
        String engMonth = null;
        if (month.equals("01")) {
            engMonth = "Jan";
        } else if (month.equals("02")) {
            engMonth = "Feb";
        } else if (month.equals("03")) {
            engMonth = "Mar";
        } else if (month.equals("04")) {
            engMonth = "Apr";
        } else if (month.equals("05")) {
            engMonth = "May";
        } else if (month.equals("06")) {
            engMonth = "June";
        } else if (month.equals("07")) {
            engMonth = "July";
        } else if (month.equals("08")) {
            engMonth = "Aug";
        } else if (month.equals("09")) {
            engMonth = "Sept";
        } else if (month.equals("10")) {
            engMonth = "Oct";
        } else if (month.equals("11")) {
            engMonth = "Nov";
        } else if (month.equals("12")) {
            engMonth = "Dec";
        }
        return engMonth;
    }

    public static boolean sevenColckBefore() {
        try {
            SimpleDateFormat df = new SimpleDateFormat("HH:mm");

            Date now = df.parse(df.format(new Date()));

            Date begin = df.parse("00:00");

            Date end = df.parse("18:59");
            //test
//            Date end = df.parse("12:00");

            Calendar nowTime = Calendar.getInstance();

            nowTime.setTime(now);

            Calendar beginTime = Calendar.getInstance();

            beginTime.setTime(begin);

            Calendar endTime = Calendar.getInstance();

            endTime.setTime(end);

            return nowTime.before(endTime) && nowTime.after(beginTime);
        } catch (ParseException e) {
            e.printStackTrace();
            return true;
        }
    }

    //18:55-19:05期间
    public static boolean parkMsgActiveTime() {
        try {
            SimpleDateFormat df = new SimpleDateFormat("HH:mm");

            Date now = df.parse(df.format(new Date()));

            Date begin = df.parse("18:55");

            Date end = df.parse("19:05");

            Calendar nowTime = Calendar.getInstance();

            nowTime.setTime(now);

            Calendar beginTime = Calendar.getInstance();

            beginTime.setTime(begin);

            Calendar endTime = Calendar.getInstance();

            endTime.setTime(end);

            return nowTime.before(endTime) && nowTime.after(beginTime);
        } catch (ParseException e) {//ignore
            e.printStackTrace();
            return true;
        }
    }
}
