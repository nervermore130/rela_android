package com.thel.utils;

import android.text.TextUtils;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;

public class PinyinUtils {

	private static final String TAG = PinyinUtils.class.getName();

	/**
	 * 获取汉字串拼音，英文字符变为小写
	 * 
	 * @param chinese
	 *            汉字串
	 * @return 汉语拼音
	 */
	public static String cn2Spell(String chinese) {
		StringBuffer pybf = new StringBuffer();
		try {
			char[] arr = chinese.toCharArray();
			HanyuPinyinOutputFormat defaultFormat = new HanyuPinyinOutputFormat();
			defaultFormat.setCaseType(HanyuPinyinCaseType.LOWERCASE);
			defaultFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
			for (int i = 0; i < arr.length; i++) {
				if (arr[i] > 128) {
					// try {
					String[] strs = PinyinHelper.toHanyuPinyinStringArray(
							arr[i], defaultFormat);
					if (strs != null && strs.length > 0) {
						pybf.append(strs[0]);
					}
					// } catch (BadHanyuPinyinOutputFormatCombination e) {
					// e.printStackTrace();
					// }
				} else {
					// pybf.append(arr[i]);
					pybf.append(Character.toLowerCase(arr[i]));
				}
			}
		} catch (Exception e) {
		}
		return pybf.toString();
	}

	public static String getPinYinHeadChar(String str) {
		String pinyin = cn2Spell(str);
		if (TextUtils.isEmpty(pinyin)) {
			return "";
		}
		return String.valueOf(pinyin.charAt(0));
	}
}
