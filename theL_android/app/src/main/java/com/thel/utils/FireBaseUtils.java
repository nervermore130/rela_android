package com.thel.utils;

import android.content.Context;

import com.thel.analytics.FireBaseImpl;

public class FireBaseUtils {
    /**
     * 上传埋点
     */
    public static void uploadGoogle(String event, Context context) {
        FireBaseImpl.getInstance().uploadGoogle(event, context);
    }
}
