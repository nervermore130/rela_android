package com.thel.utils;

import android.content.Context;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.thel.R;
import com.thel.app.TheLApp;

/**
 * Created by liuyun on 2017/9/25.
 */

public class ToastUtils {

    private static long lastTime = 0; // 上次显示Toast的时间

    private static final int TOAST_INTERVAL_SHORT = 1000; // 短Toast显示间隔
    private static final int TOAST_INTERVAL_LONG = 3000; // 长Toast显示间隔

    /**
     * 显示 短提示
     *
     * @param context
     * @param text
     */
    public static void showToastShort(Context context, String text) {
        // LayoutInflater inflate = LayoutInflater.from(context);
        // View toastRoot = inflate.inflate(R.layout.toast_layout, null);
        // TextView message = (TextView) toastRoot.findViewById(R.id.message);
        // message.setText(text);

        // Toast toast = new Toast(context);
        // toast.setDuration(Toast.LENGTH_SHORT);
        // toast.setGravity(Gravity.CENTER, 0, 0);
        // toast.setView(toastRoot);
        // 如果页面不在前台，则不show toast
        if (context == null || TextUtils.isEmpty(text)) {
            return;
        }
        if (!AppUtils.isAppOnForeground(TheLApp.context)) {
            return;
        }
        Toast toast = Toast.makeText(context, text, Toast.LENGTH_SHORT);

        if (lastTime == 0) {
            toast.show();
            lastTime = System.currentTimeMillis();
        }
        long thisTime = System.currentTimeMillis();
        if (thisTime - lastTime >= TOAST_INTERVAL_SHORT) {
            toast.show();
            lastTime = thisTime;
        }
    }

    public static void showCenterToastShort(Context context, String text) {
        // LayoutInflater inflate = LayoutInflater.from(context);
        // View toastRoot = inflate.inflate(R.layout.toast_layout, null);
        // TextView message = (TextView) toastRoot.findViewById(R.id.message);
        // message.setText(text);

        // Toast toast = new Toast(context);
        // toast.setDuration(Toast.LENGTH_SHORT);
        // toast.setGravity(Gravity.CENTER, 0, 0);
        // toast.setView(toastRoot);
        // 如果页面不在前台，则不show toast
        if (context == null) {
            return;
        }
        if (!AppUtils.isAppOnForeground(TheLApp.context)) {
            return;
        }
        Toast toast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);

        if (lastTime == 0) {
            toast.show();
            lastTime = System.currentTimeMillis();
        }
        long thisTime = System.currentTimeMillis();
        if (thisTime - lastTime >= TOAST_INTERVAL_SHORT) {
            toast.show();
            lastTime = thisTime;
        }
    }

    /***
     * 显示直播错误码提示的吐司
     */
    public static void showCautionToastShort(Context context, String text) {
        if (context == null) {
            return;
        }
        if (!BusinessUtils.isAppOnForeground()) {
            return;
        }
        LayoutInflater inflate = LayoutInflater.from(context);
        View toastRoot = inflate.inflate(R.layout.toast_image_text_layout, null);
        TextView message = toastRoot.findViewById(R.id.tv_live_caution);
        message.setText(text);

        Toast toast = new Toast(context);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(toastRoot);

        if (lastTime == 0) {
            toast.show();
            lastTime = System.currentTimeMillis();
        }
        long thisTime = System.currentTimeMillis();
        if (thisTime - lastTime >= TOAST_INTERVAL_SHORT) {
            toast.show();
            lastTime = thisTime;
        }
    }
}
