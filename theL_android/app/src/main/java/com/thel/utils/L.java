package com.thel.utils;

import android.util.Log;

/**
 * Created by liuyun on 2017/9/14.
 */

public class L {

    public static boolean DEBUG = true;

    public static boolean IS_USE_BAIDU_TEST = false;

    private static final String TAG = "theL";

    public static void d(Object o) {
        if (DEBUG) {
            Log.d(TAG, String.valueOf(o));
        }
    }

    public static void e(Object o) {
        if (DEBUG) {
            Log.e(TAG, String.valueOf(o));
        }

    }

    public static void d(String tag, Object o) {
        if (DEBUG) {
            Log.d(tag, String.valueOf(o));
        }

    }

    public static void e(String tag, Object o) {
        if (DEBUG || Log.isLoggable(tag, Log.ERROR)) {
            Log.e(tag, String.valueOf(o));
        }

    }

    public static void i(String tag, Object o) {
        if (DEBUG) {
            String xml = String.valueOf(o);
            Log.i(tag, xml);
        }
    }

    public static void subLog(String tag, String msg) {
        // tag = null;//打包
        if (tag == null || tag.length() == 0 || msg == null || msg.length() == 0)
            return;

        int segmentSize = 3 * 1024;
        long length = msg.length();
        if (length <= segmentSize) {// 长度小于限制直接打印
            L.i(tag, msg);
        } else {
            while (msg.length() > segmentSize) {// 循环分段打印日志
                String logContent = msg.substring(0, segmentSize);
                msg = msg.replace(logContent, "");
                L.i(tag, logContent);
            }
            L.i(tag, msg);// 打印剩余日志
        }
    }

}
