package com.thel.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by chad
 * Time 18/7/16
 * Email: wuxianchuang@foxmail.com
 * Description: TODO 一网通支付
 */
public class BankPay {

    public static final String TAG = "BankPay";

    // 商户密钥
    private static final String SECRET_KEY = "1234567890abcABC";
    private static final String CHARSET = "GBK";

    private static final String HOST = "http://121.15.180.66:801/";

    // 支付+签约
    private static final String URL_PREPAYEUSERP_D = "http://121.15.180.66:801/NetPayment/BaseHttp.dll?MB_EUserPay";

    // 按商户日期查询订单
    private static final String URL_QUERYORDERBYMERCHANTDATE = HOST + "NetPayment_dl/BaseHttp.dll?QuerySettledOrderByMerchantDate";
    // 查询入账明细
    private static final String URL_QUERYACCOUNTEDORDER = HOST + "NetPayment_dl/BaseHttp.dll?QueryAccountList";
    // 查询单笔订单
    private static final String URL_QUERYSINGLEORDER = HOST + "NetPayment_dl/BaseHttp.dll?QuerySingleOrder";
    // 退款
    private static final String URL_DOREFUND = HOST + "NetPayment_dl/BaseHttp.dll?DoRefund";

    private static BankPay instance;

    private BankPay() {
    }

    public static synchronized BankPay getInstance() {
        if (instance == null) {
            instance = new BankPay();
        }
        return instance;
    }

    public void pay(Context context) {
        if (isCMBAppInstalled(context)) {
            callCMBApp(context);
        } else {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    //        String result = "";
//        Scanner in = new Scanner(System.in);
//        while (true) {
//            System.out.println("1,支付签约     2,查询入账明细    3,查询单笔订单明细    4,退款    5,查询已处理订单（按商户日期查询）   0,退出");
//            switch (in.nextInt()) {
//                case 1:// 支付+签约
//                    result = BankPay.uploadParam(BankPay.prePay(), BankPay.URL_PREPAYEUSERP_D, BankPay.CHARSET);
//                    break;
//                case 2:// 查询入账明细
//                    result = BankPay.uploadParam(BankPay.queryAccountedOrder(), BankPay.URL_QUERYACCOUNTEDORDER, BankPay.CHARSET);
//                    break;
//                case 3:// 查询单笔订单明细
//                    result = BankPay.uploadParam(BankPay.querySingleOrder(), BankPay.URL_QUERYSINGLEORDER, BankPay.CHARSET);
//                    break;
//                case 4:// 退款
//                    result = BankPay.uploadParam(BankPay.doRefund(), BankPay.URL_DOREFUND, BankPay.CHARSET);
//                    break;
//                case 5:// 查询已处理订单（按处理日期查询）
//                    result = BankPay.uploadParam(BankPay.queryByMerchantDate(), BankPay.URL_QUERYORDERBYMERCHANTDATE, BankPay.CHARSET);
//                    break;
//                case 0:
//                    System.exit(0);
//                    break;
//                default:
//                    break;
//            }
//            System.out.println("result:  " + result);
//        }
                    String result = uploadParam(prePay(), URL_PREPAYEUSERP_D, CHARSET);
                    L.d(TAG, result);
                }
            }).start();
        }
    }

    private boolean isCMBAppInstalled(Context context) {
        PackageInfo packageInfo;

        try {
            packageInfo = context.getPackageManager().getPackageInfo("cmb.pb", 0);
        } catch (PackageManager.NameNotFoundException e) {
            packageInfo = null;
        }

        if (packageInfo == null) {
            L.d(TAG, "没有安装");
            return false;
        } else {
            L.d(TAG, "已经安装");
            return true;
        }
    }

    private void callCMBApp(Context context) {
//        final String url = "cmbmobilebank:CMBLS/FunctionJump?action=gofuncid&funcid=200007&serverid= CMBEUserPay&cmb_app_trans_parms_start=here&……";
        final String url = "cmbmobilebank://CMBLS/FunctionJump?action=gofuncid&funcid=200007&serverid=CMBEUserPay&requesttype=post&cmb_app_trans_parms_start=here&charset=utf-8&jsonRequestData={\"version\":\"1.0\",\"sign\":\"7396C1A77634741BACAB678EBA856EFDA1FC62C3A2B6F2625031D261343FD50A\",\"signType\":\"SHA-256\",\"reqData\":{\"dateTime\":\"20170523154402\",\"date\":\"20170523\",\"orderNo\":\"9999616650\",\"amount\":\"0.01\",\"expireTimeSpan\":\"30\",\"payNoticeUrl\":\"http://99.12.231.19:8700/callback\",\"payNoticePara\":\"aaa\",\"appType\":\"C\",\"branchNo\":\"0755\",\"merchantNo\":\"000054\",\"returnUrl\":\"yunjiazheng://callback\",\"goodsUrl\":\"\",\"shippingAdd\":\"\",\"clientIP\":\"99.12.43.61\",\"cardType\":\"\",\"extendInfo\":\"FC779C8E6953AC0C97BE115D61FE1669AECBE5836DF120A84157D9515DFD375E00B0538C85B930E6A33BB9E0213150E07953DEB1D4E30EF4356FF0B2700357881B3A8670B37C25C76DF2378DA81E3FD478FCC0DC3B2BFE925746AB92\",\"encrypType\":\"RC4\",\"subMerchantNo\":\"\",\"subMerchantName\":\"\",\"subMerchantTPCode\":\"\",\"subMerchantTPName\":\"\"}}";
        try {
            Intent intent = new Intent();
            Uri data = Uri.parse(url);
            intent.setData(data);
            intent.setAction("android.intent.action.VIEW");
            context.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "Exception", e);
        }
    }

    /**
     * 发送POST请求
     */
    private String uploadParam(String jsonParam, String url, String charset) {
        System.out.println("URL:　" + url);
        System.out.println(jsonParam);
        OutputStreamWriter out = null;
        BufferedReader in = null;
        StringBuffer result = new StringBuffer();
        try {
            URL httpUrl = new URL(url);
            HttpURLConnection urlCon = (HttpURLConnection) httpUrl.openConnection();
            urlCon.setRequestMethod("POST");
            urlCon.setRequestProperty("Content-logType", "application/x-www-form-urlencoded");
            urlCon.setDoOutput(true);
            urlCon.setDoInput(true);
            urlCon.setReadTimeout(5 * 1000);
            out = new OutputStreamWriter(urlCon.getOutputStream(), charset);// 指定编码格式
            out.write("jsonRequestData=" + jsonParam);
            out.flush();

            in = new BufferedReader(new InputStreamReader(urlCon.getInputStream(), charset));
            String str = null;
            while ((str = in.readLine()) != null) {
                result.append(str);
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (in != null) {
                    in.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result.toString();
    }

    /**
     * 获取当前时间戳
     */
    private String getNowTime() {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
        return format.format(System.currentTimeMillis());
    }

    /**
     * DES加密
     */
    private String DesEncrypt(byte[] plain, byte[] key) {
        try {
            SecureRandom random = new SecureRandom();
            DESKeySpec desKeySpec = new DESKeySpec(key);
            // 创建一个密匙工厂，然后用它把DESKeySpec转换成
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            SecretKey securekey = keyFactory.generateSecret(desKeySpec);
            // Cipher对象实际完成加密操作
            Cipher cipher = Cipher.getInstance("DES");// DES/ECB/PKCS5Padding
            // 用密匙初始化Cipher对象
            cipher.init(Cipher.ENCRYPT_MODE, securekey, random);
            // 现在，获取数据并加密
            // 正式执行加密操作
            byte[] byteBuffer = cipher.doFinal(plain);
            // 將 byte转换为16进制string
            StringBuffer strHexString = new StringBuffer();
            for (int i = 0; i < byteBuffer.length; i++) {
                String hex = Integer.toHexString(0xff & byteBuffer[i]);
                if (hex.length() == 1) {
                    strHexString.append('0');
                }
                strHexString.append(hex);
            }
            return strHexString.toString();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * RC4加密
     */
    private String RC4Encrypt(byte[] plain, byte[] key) {
        try {
            SecretKey secretKey = new SecretKeySpec(key, "RC4");
            // Cipher对象实际完成加密操作
            Cipher cipher = Cipher.getInstance("RC4");
            // 用密匙初始化Cipher对象
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            // 现在，获取数据并加密
            // 正式执行加密操作
            byte[] byteBuffer = cipher.doFinal(plain);
            // 將 byte转换为16进制string
            StringBuffer strHexString = new StringBuffer();
            for (int i = 0; i < byteBuffer.length; i++) {
                String hex = Integer.toHexString(0xff & byteBuffer[i]);
                if (hex.length() == 1) {
                    strHexString.append('0');
                }
                strHexString.append(hex);
            }
            return strHexString.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }


    /**
     * 签约+支付
     */
    private String prePay() {
        Map<String, String> reqData = new HashMap<String, String>();
        reqData.put("dateTime", getNowTime());
        reqData.put("branchNo", "0755");
        reqData.put("merchantNo", "000054");
        reqData.put("date", "20160624");
        reqData.put("orderNo", "9999000001");
        reqData.put("amount", "0.01");
        reqData.put("expireTimeSpan", "30");
        reqData.put("payNoticeUrl", "http://www.merchant.com/path/WAPProcResult.dll");
        reqData.put("payNoticePara", "支付");
        reqData.put("returnUrl", "");
        reqData.put("cardType", "");
        reqData.put("agrNo", "9934567890987654332");// 已签约客户协议号
        // reqData.put("agrNo", "12345678901234567890");//新签约协议号
        // reqData.put("merchantSerialNo", "20160804100538");
        reqData.put("userID", "2016062388888");
        reqData.put("mobile", "18202532653");
        reqData.put("lon", "30.949505");
        reqData.put("lat", "50.949506");
        reqData.put("riskLevel", "3");
        reqData.put("signNoticeUrl", "");
        reqData.put("signNoticePara", "");
        reqData.put("merchantCssUrl", "");
        reqData.put("merchantBridgeName", "");

        return buildParam(reqData);
    }

    /**
     * 查询已处理订单（按商户日期查询）
     */
    private String queryByMerchantDate() {
        Map<String, String> reqData = new HashMap<String, String>();
        reqData.put("dateTime", getNowTime());
        reqData.put("branchNo", "0755");
        reqData.put("merchantNo", "000054");
        reqData.put("beginDate", "20160502");
        reqData.put("endDate", "20160503");
        reqData.put("operatorNo", "9999");
        reqData.put("nextKeyValue", "");

        return buildParam(reqData);
    }

    /**
     * 给请求数据增加签名字段
     */
    private String addSignParam(String reqJSON) {
        try {
            JSONObject param = new JSONObject(reqJSON);
            String reqData = param.getString("reqData");
            String sign = sign(reqData, SECRET_KEY, param.getString("charset"));
            param.put("sign", sign);
            return param.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * 查询入账明细
     */
    private String queryAccountedOrder() {
        Map<String, String> reqData = new HashMap<String, String>();
        reqData.put("dateTime", getNowTime());
        reqData.put("branchNo", "0755");
        reqData.put("merchantNo", "000054");
        reqData.put("date", "20160625");
        reqData.put("operatorNo", "9999");
        reqData.put("nextKeyValue", "");

        return buildParam(reqData);
    }

    /**
     * 查询单笔订单明细
     */
    private String querySingleOrder() {
        Map<String, String> reqData = new HashMap<String, String>();
        reqData.put("dateTime", getNowTime());
        reqData.put("branchNo", "0755");
        reqData.put("merchantNo", "000054");
        reqData.put("type", "A");
        reqData.put("bankSerialNo", "16250323300000000010");
        reqData.put("date", "20160625");
        reqData.put("orderNo", "9999000001");
        reqData.put("orderRefNo", "");
        reqData.put("operatorNo", "9999");

        return buildParam(reqData);
    }

    /**
     * 退款
     */
    private String doRefund() {
        Map<String, String> reqData = new HashMap<String, String>();
        reqData.put("dateTime", getNowTime());
        reqData.put("branchNo", "0755");
        reqData.put("merchantNo", "000054");
        reqData.put("date", "20160503");
        reqData.put("orderNo", "9999000015");
        reqData.put("amount", "0.01");
        reqData.put("desc", "取消订单");
        reqData.put("refundSerialNo", "16250323300000000010");
        reqData.put("operatorNo", "9998");
        reqData.put("encrypType", "");
        reqData.put("pwd", "123456");

        return buildParam(reqData);
    }

    private String buildParam(Map<String, String> reqDataMap) {
        JSONObject jsonParam = new JSONObject();
        try {
            jsonParam.put("version", "1.0");
            jsonParam.put("charset", CHARSET);// 支持GBK和UTF-8两种编码
            jsonParam.put("sign", sign(reqDataMap, SECRET_KEY));
            jsonParam.put("signType", "SHA-256");
            jsonParam.put("reqData", reqDataMap);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonParam.toString();
    }

    /**
     * 对参数签名：
     * 对reqData所有请求参数按从a到z的字典顺序排列，如果首字母相同，按第二个字母排列，以此类推。排序完成后按将所有键值对以“&”符号拼接。
     * 拼接完成后再加上商户密钥。示例：param1=value1&param2=value2&...&paramN=valueN&secretKey
     *
     * @param reqDataMap 请求参数
     * @param secretKey  商户密钥
     */
    private String sign(Map<String, String> reqDataMap, String secretKey) {
        StringBuffer buffer = new StringBuffer();
        List<String> keyList = sortParams(reqDataMap);
        for (String key : keyList) {
            buffer.append(key).append("=").append(reqDataMap.get(key)).append("&");
        }
        buffer.append(secretKey);// 商户密钥
        System.out.println(buffer.toString());

        try {
            // 创建加密对象
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            // 传入要加密的字符串,按指定的字符集将字符串转换为字节流
            messageDigest.update(buffer.toString().getBytes(CHARSET));
            byte[] byteBuffer = messageDigest.digest();

            // 將 byte转换为16进制string
            StringBuffer strHexString = new StringBuffer();
            for (int i = 0; i < byteBuffer.length; i++) {
                String hex = Integer.toHexString(0xff & byteBuffer[i]);
                if (hex.length() == 1) {
                    strHexString.append('0');
                }
                strHexString.append(hex);
            }
            return strHexString.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * 对参数签名
     */
    private String sign(String reqDataJSON, String secretKey, String charset) {
        StringBuffer buffer = new StringBuffer();

        try {
            JSONObject json = new JSONObject(reqDataJSON);
            List<String> keyList = sortParams(json);
            for (String key : keyList) {
                buffer.append(key).append("=").append(json.get(key)).append("&");
            }
            buffer.append(secretKey);// 商户密钥
            System.out.println(buffer.toString());
            // 创建加密对象
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            // 传入要加密的字符串,按指定的字符集将字符串转换为字节流
            messageDigest.update(buffer.toString().getBytes(charset));
            byte[] byteBuffer = messageDigest.digest();

            // 將 byte转换为16进制string
            StringBuffer strHexString = new StringBuffer();
            for (int i = 0; i < byteBuffer.length; i++) {
                String hex = Integer.toHexString(0xff & byteBuffer[i]);
                if (hex.length() == 1) {
                    strHexString.append('0');
                }
                strHexString.append(hex);
            }
            return strHexString.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * 对参数按字典顺序排序，不区分大小写
     */
    private List<String> sortParams(Map<String, String> reqDataMap) {
        List<String> list = new ArrayList<String>(reqDataMap.keySet());
        Collections.sort(list, new Comparator<String>() {
            public int compare(String o1, String o2) {
                String[] temp = {o1.toLowerCase(), o2.toLowerCase()};
                Arrays.sort(temp);
                if (o1.equalsIgnoreCase(temp[0])) {
                    return -1;
                } else if (temp[0].equalsIgnoreCase(temp[1])) {
                    return 0;
                } else {
                    return 1;
                }
            }
        });
        return list;
    }

    /**
     * 对参数排序
     */
    private List<String> sortParams(JSONObject json) {
        List<String> list = new ArrayList<String>();
        Iterator it = json.keys();
        while (it.hasNext()) {
            list.add((String) it.next());
        }
        Collections.sort(list, new Comparator<String>() {
            public int compare(String o1, String o2) {
                String[] temp = {o1.toLowerCase(), o2.toLowerCase()};
                Arrays.sort(temp);
                if (o1.equalsIgnoreCase(temp[0])) {
                    return -1;
                } else if (temp[0].equalsIgnoreCase(temp[1])) {
                    return 0;
                } else {
                    return 1;
                }
            }
        });
        return list;
    }
}
