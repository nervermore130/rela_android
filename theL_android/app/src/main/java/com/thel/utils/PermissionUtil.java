package com.thel.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.database.Cursor;
import android.hardware.Camera;
import android.location.Location;
import android.location.LocationManager;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.provider.Telephony;

import androidx.annotation.NonNull;

import com.tbruyelle.rxpermissions2.RxPermissions;
import com.thel.R;
import com.thel.app.TheLApp;

import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import io.reactivex.functions.Consumer;

import static android.content.DialogInterface.BUTTON_POSITIVE;

/**
 * Created by chad
 * Time 18/3/26
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */

public class PermissionUtil {

    public static final String TAG = "PermissionUtil";

    public interface PermissionCallback {
        void granted();//获取权限

        void denied();//拒绝权限

        void gotoSetting();//前往设置页设置权限
    }

    public static boolean isOpenGPS(@NonNull final Activity activity) {
        LocationManager locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    @SuppressLint("CheckResult")
    public static void requestLocationPermission(@NonNull final Activity activity, final PermissionCallback permissionCallback) {
        final String message = activity.getString(R.string.location_permission);
        new RxPermissions(activity).
                request(Manifest.permission.ACCESS_FINE_LOCATION
                        , Manifest.permission.ACCESS_COARSE_LOCATION).
                subscribe(aBoolean -> {
                    if (aBoolean && testLocationPermission()) {
                        // All requested permissions are granted
                        if (permissionCallback != null) {
                            permissionCallback.granted();
                        }
                    } else {
                        // At least one permission is denied
                        // Need to go to the settings
                        showGotoSettingDialog(activity, message, permissionCallback);
                    }
                }, throwable -> {

                });
    }

    @SuppressLint("CheckResult")
    public static void requestStoragePermission(@NonNull final Activity activity, final PermissionCallback permissionCallback) {
        final String message = activity.getString(R.string.storage_permission);
        new RxPermissions(activity).
                request(Manifest.permission.READ_EXTERNAL_STORAGE
                        , Manifest.permission.WRITE_EXTERNAL_STORAGE).
                subscribe(new Consumer<Boolean>() {
                    @Override
                    public void accept(Boolean aBoolean) {
                        if (aBoolean && testStoragePermission()) {
                            // All requested permissions are granted
                            if (permissionCallback != null) permissionCallback.granted();
                        } else {
                            // At least one permission is denied
                            // Need to go to the settings
                            showGotoSettingDialog(activity, message, permissionCallback);
                        }
                    }
                });
    }

    @SuppressLint("CheckResult")
    public static void requestCameraPermission(@NonNull final Activity activity, final PermissionCallback permissionCallback) {
        //oppo6.0以下，使用Camera时，系统自己会弹出权限请求
        Intent oppoIntent = activity.getPackageManager().getLaunchIntentForPackage("com.coloros.safecenter");
        if (oppoIntent != null && Build.VERSION.SDK_INT < Build.VERSION_CODES.M && permissionCallback != null) {
            permissionCallback.granted();
        } else {
            final String message = activity.getString(R.string.camera_permission);
            new RxPermissions(activity).
                    request(Manifest.permission.CAMERA).
                    subscribe(new Consumer<Boolean>() {
                        @Override
                        public void accept(Boolean aBoolean) {
                            if (aBoolean) {
                                if (!isHasCameraPermission()) {
                                    ToastUtils.showToastShort(activity, "错误: 相机被占用！");
                                } else {
                                    // All requested permissions are granted
                                    if (permissionCallback != null) permissionCallback.granted();
                                }
                            } else {
                                // At least one permission is denied
                                // Need to go to the settings
                                showGotoSettingDialog(activity, message, permissionCallback);
                            }
                        }
                    });
        }
    }

    @SuppressLint("CheckResult")
    public static void requestRecordPermission(@NonNull final Activity activity, final PermissionCallback permissionCallback) {
        //oppo6.0以下，使用Camera时，系统自己会弹出权限请求
        Intent oppoIntent = activity.getPackageManager().getLaunchIntentForPackage("com.coloros.safecenter");
        if (oppoIntent != null && Build.VERSION.SDK_INT < Build.VERSION_CODES.M && permissionCallback != null) {
            permissionCallback.granted();
        } else {
            final String message = activity.getString(R.string.camera_permission);
            new RxPermissions(activity).
                    request(Manifest.permission.RECORD_AUDIO).
                    subscribe(new Consumer<Boolean>() {
                        @Override
                        public void accept(Boolean aBoolean) {

                            if (aBoolean) {
                                if (!isHasAudioRecordPermission()) {
                                    ToastUtils.showToastShort(activity, "错误: 麦克风被占用！");
                                } else {
                                    // All requested permissions are granted
                                    if (permissionCallback != null) permissionCallback.granted();
                                }
                            } else {
                                // At least one permission is denied
                                // Need to go to the settings
                                showGotoSettingDialog(activity, message, permissionCallback);
                            }

                        }
                    });
        }
    }

    public static void requestPushPermission(@NonNull final Activity activity, final PermissionCallback permissionCallback) {
        if (!isNotificationEnabled(activity)) {
            String message = activity.getString(R.string.push_permission);
            //vivo和oppo的设置页无法设置权限，需要到'i 管家'app里设置
            Intent vivoIntent = activity.getPackageManager().getLaunchIntentForPackage("com.iqoo.secure");
            Intent oppoIntent = activity.getPackageManager().getLaunchIntentForPackage("com.coloros.safecenter");
            if (vivoIntent != null || oppoIntent != null)
                message = activity.getString(R.string.push_permission_oppo_vivo);
            showGotoSettingDialog(activity, message, permissionCallback);
        } else {
            if (permissionCallback != null) permissionCallback.granted();
        }
    }

    /**
     * 请求读取短信权限
     */
    @SuppressLint("CheckResult")
    public static void requestSMSPermission(@NonNull final Activity activity, final PermissionCallback permissionCallback) {
        final String message = activity.getString(R.string.sms_permission);
        new RxPermissions(activity).
                request(Manifest.permission.RECEIVE_SMS
                        , Manifest.permission.READ_SMS).
                subscribe(new Consumer<Boolean>() {
                    @Override
                    public void accept(Boolean aBoolean) {
                        if (aBoolean) {
                            // All requested permissions are granted
                            if (permissionCallback != null) permissionCallback.granted();
                        } else {
                            // At least one permission is denied
                            // Need to go to the settings
                            showGotoSettingDialog(activity, message, permissionCallback);
                        }
                    }
                });
    }


    private static void showGotoSettingDialog(@NonNull final Activity activity, final String message, final PermissionCallback permissionCallback) {
        DialogUtil.showConfirmDialog(activity, activity.getString(R.string.request_permission), message, activity.getString(R.string.setting_permission), activity.getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (which == BUTTON_POSITIVE) {
                    if (permissionCallback != null) permissionCallback.gotoSetting();
                    //如果是oppo/vivo请求通知权限
                    if (message.equals(activity.getString(R.string.push_permission_oppo_vivo))) {
                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
                        intent.setData(uri);
                        activity.startActivity(intent);
                    } else {
                        //vivo和oppo的设置页无法设置权限，需要到'i 管家'app里设置
                        Intent vivoIntent = activity.getPackageManager().getLaunchIntentForPackage("com.iqoo.secure");
                        Intent oppoIntent = activity.getPackageManager().getLaunchIntentForPackage("com.coloros.safecenter");

                        if (vivoIntent != null) {
                            activity.startActivity(vivoIntent);
                        } else if (oppoIntent != null) {
                            activity.startActivity(oppoIntent);
                        } else {
                            Intent intent = new Intent();
                            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
                            intent.setData(uri);
                            activity.startActivity(intent);
                        }
                    }
                } else {
                    dialog.dismiss();
                    if (permissionCallback != null) permissionCallback.denied();
                }
            }
        });
    }

    private static boolean testLocationPermission() {
        //针对定制系统做特殊权限判断
        try {
            LocationManager locationManager = (LocationManager) TheLApp.context.getSystemService(Context.LOCATION_SERVICE);//获得位置服务
            String provider = LocationManager.GPS_PROVIDER;// 指定LocationManager的定位方法
            @SuppressLint("MissingPermission") Location location = locationManager.getLastKnownLocation(provider);// 调用
            if (location != null) location.getLatitude();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            L.d(TAG, "testLocationPermission Exception");
            return false;
        }
    }

    private static boolean testStoragePermission() {
        //针对定制系统做特殊权限判断
        try {
            String sdCardDir;
            if (android.os.Build.VERSION_CODES.Q == Build.VERSION.SDK_INT) {
                sdCardDir = TheLApp.getContext().getExternalFilesDir(null).getAbsolutePath();
            } else {
                sdCardDir = Environment.getExternalStorageDirectory().getAbsolutePath();
            }
            File saveFile = new File(sdCardDir, "test.txt");
            FileOutputStream outStream = new FileOutputStream(saveFile);
            outStream.write("permission test".getBytes());
            outStream.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            L.d(TAG, "testStoragePermission Exception");
            return false;
        }
    }

    /**
     * 针对定制系统做特殊权限判断
     * 捕获异常，判断是否有相机权限
     */
    private static boolean isHasCameraPermission() {
        try {
            Camera camera = Camera.open();
            camera.stopPreview();
            Camera.Parameters mParameters = camera.getParameters();
            camera.setParameters(mParameters);
            camera.release();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            L.d(TAG, "testCameraPermission Exception : " + e.getMessage());
            return false;
        }
    }

    /**
     * 针对定制系统做特殊权限判断
     * 获取app的录音权限是否打开
     */
    private static boolean isHasAudioRecordPermission() {
        // 音频获取源
        int audioSource = MediaRecorder.AudioSource.MIC;
        // 设置音频采样率，44100是目前的标准，但是某些设备仍然支持22050，16000，11025
        int sampleRateInHz = 44100;
        // 设置音频的录制的声道CHANNEL_IN_STEREO为双声道，CHANNEL_CONFIGURATION_MONO为单声道
        int channelConfig = AudioFormat.CHANNEL_IN_STEREO;
        // 音频数据格式:PCM 16位每个样本。保证设备支持。PCM 8位每个样本。不一定能得到设备支持。
        int audioFormat = AudioFormat.ENCODING_PCM_16BIT;
        // 缓冲区字节大小
        int bufferSizeInBytes = 0;
        bufferSizeInBytes = AudioRecord.getMinBufferSize(sampleRateInHz,
                channelConfig, audioFormat);
        AudioRecord audioRecord = new AudioRecord(audioSource, sampleRateInHz,
                channelConfig, audioFormat, bufferSizeInBytes);
        //开始录制音频
        try {
            // 防止某些手机崩溃，例如联想
            audioRecord.startRecording();
        } catch (IllegalStateException e) {
            e.printStackTrace();
            L.d(TAG, "testRecordingPermission IllegalStateException");
        }
        /**
         * 根据开始录音判断是否有录音权限
         */

        L.d(TAG, " audioRecord.getRecordingState() : " + audioRecord.getRecordingState());

        if (audioRecord.getRecordingState() != AudioRecord.RECORDSTATE_RECORDING) {
            return false;
        }
        audioRecord.stop();
        audioRecord.release();
        audioRecord = null;
        return true;
    }

    /**
     * 判断推送通知是否开启
     *
     * @param context
     * @return
     */
    @SuppressLint("NewApi")
    public static boolean isNotificationEnabled(Context context) {

        AppOpsManager mAppOps = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
        ApplicationInfo appInfo = context.getApplicationInfo();
        String pkg = context.getApplicationContext().getPackageName();
        int uid = appInfo.uid;

        Class appOpsClass = null;
        /* Context.APP_OPS_MANAGER */
        try {
            appOpsClass = Class.forName(AppOpsManager.class.getName());
            Method checkOpNoThrowMethod = appOpsClass.getMethod("checkOpNoThrow", Integer.TYPE, Integer.TYPE,
                    String.class);
            Field opPostNotificationValue = appOpsClass.getDeclaredField("OP_POST_NOTIFICATION");

            int value = (Integer) opPostNotificationValue.get(Integer.class);
            return ((Integer) checkOpNoThrowMethod.invoke(mAppOps, value, uid, pkg) == AppOpsManager.MODE_ALLOWED);

        } catch (ClassNotFoundException | NoSuchMethodException | NoSuchFieldException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 判断是否有读取短信权限
     *
     * @param activity
     * @return
     */
    private static boolean testReadSMS(Activity activity) {
        try {
            String[] project = {Telephony.Sms._ID, Telephony.Sms.BODY};
            Cursor query = activity.getContentResolver().query(Uri.parse("content://mms-sms/conversations/"), project, null, null, null);

            if (query != null && query.moveToFirst()) {
                new StringBuilder().append(query.getString(0)).append('\n').append(query.getString(1));
                query.close();
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }


}
