package com.thel.utils;

import android.graphics.Bitmap;
import android.view.Surface;
import android.view.SurfaceView;

import com.ksyun.media.player.IMediaPlayer;
import com.ksyun.media.player.KSYMediaPlayer;
import com.thel.app.TheLApp;

import java.io.IOException;

/**
 * Created by chad
 * Time 17/10/31
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */

public class VideoPlayManager implements IMediaPlayer.OnPreparedListener, IMediaPlayer.OnSeekCompleteListener, IMediaPlayer.OnInfoListener {

    private static VideoPlayManager instance;

    private KSYMediaPlayer ksyMediaPlayer;

    public static VideoPlayManager getInstance() {
        if (instance == null) {
            instance = new VideoPlayManager();
        }
        return instance;
    }

    public void playVideo(String url, SurfaceView surfaceView) {

        if (ksyMediaPlayer == null) {
            ksyMediaPlayer = new KSYMediaPlayer.Builder(TheLApp.context).build();
          /*  ksyMediaPlayer.setOnPreparedListener(this);
            ksyMediaPlayer.setOnSeekCompleteListener(this);
            ksyMediaPlayer.setOnInfoListener(this);*/
            ksyMediaPlayer.setLooping(true);
            ksyMediaPlayer.setVideoScalingMode(KSYMediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
            try {
                ksyMediaPlayer.setDataSource(url);
            } catch (IOException e) {
                e.printStackTrace();
            }
            ksyMediaPlayer.setDisplay(surfaceView.getHolder());
            ksyMediaPlayer.prepareAsync();
        } else {
            ksyMediaPlayer.reload(url, false);
        }
    }

    public void playVideo(String url, Surface surface) {

        if (ksyMediaPlayer == null) {
            ksyMediaPlayer = new KSYMediaPlayer.Builder(TheLApp.context).build();
          /*  ksyMediaPlayer.setOnPreparedListener(this);
            ksyMediaPlayer.setOnSeekCompleteListener(this);
            ksyMediaPlayer.setOnInfoListener(this);*/
            ksyMediaPlayer.setLooping(true);
            ksyMediaPlayer.setVideoScalingMode(KSYMediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
            try {
                ksyMediaPlayer.setDataSource(url);
            } catch (IOException e) {
                e.printStackTrace();
            }
            ksyMediaPlayer.setSurface(surface);
            ksyMediaPlayer.prepareAsync();
        } else {
            ksyMediaPlayer.reload(url, true);
        }
    }

    public KSYMediaPlayer getKsyMediaPlayer() {
        if (ksyMediaPlayer == null) {
            return null;
        }
        return ksyMediaPlayer;
    }

    /**
     * 继续当前播放
     */
    public void restart() {
        if (ksyMediaPlayer != null) {
            ksyMediaPlayer.start();
        }
    }

    /**
     * app切到后台，暂停播放
     */
    public void pause() {
        if (ksyMediaPlayer != null) {
            ksyMediaPlayer.pause();
        }
    }

    /**
     * 不再使用播放器时，要释放掉，避免占用内存
     */
    public void releaseMediaPlayer() {
        if (ksyMediaPlayer != null) {
            ksyMediaPlayer.release();
            ksyMediaPlayer = null;
        }
    }

    public void setOnPreparedListener(IMediaPlayer.OnPreparedListener listener) {
        if (getKsyMediaPlayer() != null) {
            getKsyMediaPlayer().setOnPreparedListener(listener);
        }
    }

    public void setOnSeekCompleteListener(IMediaPlayer.OnSeekCompleteListener listener) {
        if (getKsyMediaPlayer() != null) {
            getKsyMediaPlayer().setOnSeekCompleteListener(listener);
        }
    }

    public void setOnInfoListener(IMediaPlayer.OnInfoListener listener) {
        if (getKsyMediaPlayer() != null) {
            getKsyMediaPlayer().setOnInfoListener(listener);
        }
    }

    @Override
    public void onPrepared(IMediaPlayer iMediaPlayer) {

    }

    @Override
    public void onSeekComplete(IMediaPlayer iMediaPlayer) {

    }

    @Override
    public boolean onInfo(IMediaPlayer iMediaPlayer, int i, int i1) {
        return false;
    }

    public void setOnErrorListener(IMediaPlayer.OnErrorListener listener) {
        if (getKsyMediaPlayer() != null) {
            getKsyMediaPlayer().setOnErrorListener(listener);
        }
    }

    public void reset() {
        if (ksyMediaPlayer != null) {
            ksyMediaPlayer.reset();
        }
    }

    public Bitmap getScreenShot() {
        if (ksyMediaPlayer != null) {
            return ksyMediaPlayer.getScreenShot();
        }
        return null;
    }

    public void setVolume(float v, float v1) {
        if(ksyMediaPlayer!=null){
            ksyMediaPlayer.setVolume(v,v1);
        }
    }
}
