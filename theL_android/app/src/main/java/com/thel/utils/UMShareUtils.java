package com.thel.utils;

import android.app.Activity;

import com.thel.growingio.GIOShareTrackBean;
import com.thel.growingio.GrowingIoConstant;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.media.UMWeb;

/**
 * Created by chad
 * Time 17/10/17
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */

public class UMShareUtils implements GrowingIoConstant {

    /* public static void share(Activity context, SHARE_MEDIA platform, UMImage umImage, final String url, final String shareTitle, final String shareContent, UMShareListener umShareListener) {
        final UMWeb web = new UMWeb(url);
        web.setTitle(shareTitle);
        web.setThumb(umImage);
        web.setTitle(shareTitle);
        web.setDescription(shareContent);
        new ShareAction(context).setPlatform(platform).withMedia(web).setCallback(umShareListener).share();
    }*/
    public static void share(Activity context, SHARE_MEDIA platform, UMImage umImage, final String url, final String shareTitle, final String shareContent, UMShareListener umShareListener) {

        //1.2.4.2  去掉了ShareAction的WithTargetURl
        final UMWeb web = new UMWeb(url);
        web.setTitle(shareTitle);//标题
        web.setThumb(umImage);  //缩略图
        web.setDescription(shareContent);//描述

        new ShareAction(context).setPlatform(platform).withMedia(web).setCallback(umShareListener).share();

        //        new ShareAction(context).setPlatform(platform).withMedia(umImage).withTargetUrl(url).withTitle(shareTitle).withText(shareContent).setCallback(umShareListener).share();

    }

    public static void share(Activity context, SHARE_MEDIA platform, UMImage umImage, final String url, final String shareTitle, final String shareContent, UMShareListener umShareListener, GIOShareTrackBean bean) {
        share(context, platform, umImage, url, shareTitle, shareContent, umShareListener);
        if (bean != null) {
            bean.shareBean.shareType = GrowingIOUtil.getShareType(platform);
            GrowingIOUtil.shareTrack(bean);
        }
    }

    public static void share(Activity context, SHARE_MEDIA platform, UMImage umImage, UMShareListener umShareListener) {
        new ShareAction(context).setPlatform(platform).withMedia(umImage).setCallback(umShareListener).share();

    }
}
