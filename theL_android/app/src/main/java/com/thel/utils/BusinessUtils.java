package com.thel.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.text.TextUtils;

import com.thel.R;
import com.thel.android.pay.SyncOrdersService;
import com.thel.app.TheLApp;
import com.thel.bean.AdBean;
import com.thel.bean.BasicInfoBean;
import com.thel.bean.BasicInfoNetBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.constants.BundleConstants;
import com.thel.constants.TheLConstants;
import com.thel.constants.TheLConstantsExt;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.flutter.bridge.RfSBridgeHandlerFactory;
import com.thel.growingio.GIOShareTrackBean;
import com.thel.imp.black.BlackUtils;
import com.thel.manager.CDNBalanceManager;
import com.thel.manager.ChatServiceManager;
import com.thel.modules.live.surface.watch.LiveWatchActivity;
import com.thel.modules.live.surface.watch.horizontal.LiveWatchHorizontalActivity;
import com.thel.modules.main.home.moments.ReleaseMomentActivity;
import com.thel.modules.main.home.moments.web.WebViewActivity;
import com.thel.modules.main.me.aboutMe.StickerPackDetailActivity;
import com.thel.modules.main.me.aboutMe.StickerStoreActivity;
import com.thel.modules.main.messages.db.DataBaseAdapter;
import com.thel.modules.others.VipConfigActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@SuppressLint("NewApi")
public class BusinessUtils {

    private static final String TAG = BusinessUtils.class.getSimpleName();

    private static final long MIN = 60 * 1000;

    private static final int MAX_SEQUENCE = 4096;

    public static void logoutAndClearUserData() {
        ShareFileUtils.setBoolean(ShareFileUtils.HAS_LOGGED, false);
        ShareFileUtils.setString(ShareFileUtils.KEY, "");
        ShareFileUtils.setString(ShareFileUtils.ID, "");
        ShareFileUtils.setString(ShareFileUtils.USER_NAME, "");
        ShareFileUtils.setString(ShareFileUtils.EMAIL, "");
        ShareFileUtils.setString(ShareFileUtils.AVATAR, "");
        ShareFileUtils.setString(ShareFileUtils.USER_THEL_ID, "");
        ShareFileUtils.setString(ShareFileUtils.MESSAGE_USER, "");
        ShareFileUtils.setString(ShareFileUtils.MESSAGE_PASSWORD, "");
        ShareFileUtils.setInt(ShareFileUtils.NEAR_BY_USER, 0);
        ShareFileUtils.setInt(ShareFileUtils.NEAR_BY_USER_TOTAL, 0);
        ShareFileUtils.setLong(ShareFileUtils.NEAR_BY_USER_TIMESTAMP, 0);
        ShareFileUtils.setInt(ShareFileUtils.FRIENDS_FRIENDS_TOTAL, 0);
        ShareFileUtils.setInt(ShareFileUtils.FRIENDS_FOLLOW_TOTAL, 0);
        ShareFileUtils.setInt(ShareFileUtils.FRIENDS_FANS_TOTAL, 0);
        ShareFileUtils.setInt(ShareFileUtils.FRIENDS_LIKED_TOTAL, 0);
        ShareFileUtils.setInt(ShareFileUtils.HEIGHT_UNITS, 0);
        ShareFileUtils.setInt(ShareFileUtils.WEIGHT_UNITS, 0);
        ShareFileUtils.setInt(ShareFileUtils.UNREAD_MOMENTS_MSG_NUM, 0);
        ShareFileUtils.setInt(ShareFileUtils.LIVE_PUSH, 1);
        ShareFileUtils.setBoolean(ShareFileUtils.NEW_CHECK, false);
        ShareFileUtils.setString(ShareFileUtils.NOTIFIED_ACTIVITY_NICKNAME, "");
        ShareFileUtils.setString(ShareFileUtils.NOTIFIED_ACTIVITY_TYPE, "");
        ShareFileUtils.setString(ShareFileUtils.NOTIFIED_ACTIVITY_COMMENT_TEXT, "");
        ShareFileUtils.setString(ShareFileUtils.BLOCK_ME_LIST, "");
        ShareFileUtils.setLong(ShareFileUtils.MATCH_NEXT_TIME, 0);
        ShareFileUtils.setLong(ShareFileUtils.LAST_COUNT_DOWN, 0);
        ShareFileUtils.setBoolean(ShareFileUtils.NEED_COMPLETE_USER_INFO, false);
        ShareFileUtils.setString(ShareFileUtils.BIND_CELL, "");
        ShareFileUtils.setString(ShareFileUtils.BIND_WX, "");
        ShareFileUtils.setString(ShareFileUtils.BIND_FB, "");
        ShareFileUtils.setBoolean(ShareFileUtils.NEARBY_COVER_GUIDE, true);
        ShareFileUtils.setInt(ShareFileUtils.PERM, 0);
        ShareFileUtils.setBoolean(ShareFileUtils.CLOSE_NONYMOUS_BROWSING,false);
        ShareFileUtils.setString(ShareFileUtils.BEFORE_TIME,"");
//        SharedPrefUtils.clearData(SharedPrefUtils.FILE_NAME_BLOCK_FILE);// 删除屏蔽
//        SharedPrefUtils.clearData(SharedPrefUtils.FILE_NAME_CHAT_DRAFT);// 删除草稿
//        SharedPrefUtils.clearData(SharedPrefUtils.FILE_REQUEST);// 删除密友请求数据
//        SharedPrefUtils.clearData(SharedPrefUtils.FILE_SYNC);// 删除数据同步数据
//        SharedPrefUtils.clearData(SharedPrefUtils.FILE_NAME_TAGS);// 发现页缓存
        //        SharedPrefUtils.clearData(SharedPrefUtils.FILE_STICKERS);// 删除历史表情
        ShareFileUtils.setInt(ShareFileUtils.IS_SHOW_USER_INFO_CHAT_GUIDE, -1); //退出重新聊天引导

        DataBaseAdapter dbAdatper = DataBaseAdapter.getInstance(TheLApp.getContext());
        dbAdatper.clearUserData();

        // 将growingIO的cs1置空
        GrowingIOUtil.clearUserId();

        // 重置会员等级
        UserUtils.setUserVipLevel(0);
        // 重置直播权限
        ShareFileUtils.setInt(TheLConstants.live_permit, 0);
        // 重置用户引导页面是否跳过
        // TODO: 2017/10/31   MomentsFragmentActivity.hasStarted = false;

        // 将growingIO的cs1置空
        // TODO: 2017/10/31   BusinessUtils.uploadGrowingCS1("");
        // 将growingIO的cs2-cs7置空
        // TODO: 2017/10/31   BusinessUtils.uploadMyInfoToGrowing(new MyInfoBean());

        SyncOrdersService.stopSyncService(TheLApp.getContext());
        clearWXLogInfo();

        ShareFileUtils.setString(ShareFileUtils.MSG_AUTH_TOKEN, "{}");
        //清除好友列表缓存
        ChatServiceManager.getInstance().stopService(TheLApp.context);

        BadgeUtil.clearBadgeCount(TheLApp.context);

        RelaTimer.getInstance().stop();

//        Intent intent = new Intent(TheLApp.context, FlutterLoginActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        TheLApp.context.startActivity(intent);


//        Intent intent = new Intent(TheLApp.context, MainActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        TheLApp.context.startActivity(intent);
    }

    private static void clearWXLogInfo() {
        ShareFileUtils.setWxLoginUserInfo(null);
        ShareFileUtils.setWxLoginToken(null);
        ShareFileUtils.clearLoginTime();
    }

    public static void playSound(int resid) {
        final MediaPlayer mMediaPlayer = MediaPlayer.create(TheLApp.getContext(), resid);
        if (mMediaPlayer != null) {
            if (!mMediaPlayer.isPlaying()) {
                mMediaPlayer.start();
            } else {
                mMediaPlayer.stop();
            }
            mMediaPlayer.setOnCompletionListener(new OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mMediaPlayer.release();
                }
            });
        }
    }

    public static boolean isAppOnForeground() {
        try {
            ActivityManager activityManager = (ActivityManager) TheLApp.getContext().getSystemService(Context.ACTIVITY_SERVICE);
            List<RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
            String packageName = TheLApp.getContext().getPackageName();
            if (appProcesses == null) {
                return false;
            }
            for (RunningAppProcessInfo appProcess : appProcesses) {
                if (appProcess.processName.equals(packageName) && appProcess.importance == RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    return true;
                }
            }
            return false;
        } catch (Exception e) {
            return false;
        }

    }

    /**
     * 获取最上层的activity的名字 eg:MeActivity.class
     *
     * @param context
     * @return
     */
    public static String getTopActivityName(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
        return cn.getClassName();
    }

    /**
     * 生成消息ID
     *
     * @return
     */
    private static long lastTimestamp = 0;

    private static long sequence = 0;

    public static String generateMsgId() {
        long userId = Long.valueOf(ShareFileUtils.getString(ShareFileUtils.ID, "123456789"));
        long currentTimestamp = ((System.currentTimeMillis() - 1500000000000L) / MIN);   // 保证为整数型，不要是浮点数
        if (currentTimestamp != lastTimestamp) {
            sequence = 0;
            lastTimestamp = currentTimestamp;
        } else if (++sequence >= MAX_SEQUENCE) {
            //失败
            return "-1";
        }

        String packetId = "" + (((userId & 0xfffff) << 32) | ((currentTimestamp & 0xfffff) << 12) | sequence);

        L.d("BusinessUtils", " packetId : " + packetId);

        return packetId; // 等价于  id = currentTimestamp * 4096 + sequence;
    }

    public static String generateJsKey() {
        return ShareFileUtils.getString(ShareFileUtils.ID, "") + "-" + MD5Utils.md5(ShareFileUtils.getString(ShareFileUtils.KEY, "") + "ba3bce8d");
    }

    public static String generateUA() {
        return "theL/" +
                DeviceUtils.getVersionName(TheLApp.getContext()) +
                " (" +
                DeviceUtils.getModel() +
                "; android " +
                DeviceUtils.getReleaseVersion() +
                "; " +
                DeviceUtils.getLanguageStr() +
                "; " +
                DeviceUtils.getCurrentNetType(TheLApp.getContext()) +
                "; " +
                DeviceUtils.getChannelId() + ")";
    }

    public static String generateUA(String str) {
        return "theL/" +
                DeviceUtils.getVersionName(TheLApp.getContext()) +
                " (" +
                DeviceUtils.getModel() +
                "; android " +
                DeviceUtils.getReleaseVersion() +
                "; " +
                DeviceUtils.getLanguageStr() +
                "; " +
                DeviceUtils.getCurrentNetType(TheLApp.getContext()) +
                "; " +
                DeviceUtils.getChannelId() +
//                ";" +
//                NetworkUtils.getAPNType() +
                ";" +
                str + ")";
    }

    public static void AdRedirect(final AdBean.Map_list adBean) {

        L.d(TAG, " adBean toString : " + GsonUtils.createJsonString(adBean));

        try {
            if (adBean.dumpType.equals(AdBean.ADV_DUMP_TYPE_WEB)) {

                Intent intent = new Intent(TheLApp.getContext().getApplicationContext(), WebViewActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(BundleConstants.URL, adBean.dumpURL);
                bundle.putString(BundleConstants.SITE, "ad");
                bundle.putString("title", adBean.advertTitle);
                intent.putExtras(bundle);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                TheLApp.getContext().startActivity(intent);
            } else if (adBean.dumpType.equals(AdBean.ADV_DUMP_TYPE_LOCAL)) {
                Intent intent = new Intent(TheLApp.getContext(), ReleaseMomentActivity.class);
                intent.putExtra(TheLConstants.BUNDLE_KEY_TOPIC_NAME, adBean.advertTitle);
                intent.putExtra(TheLConstants.BUNDLE_KEY_AD_FLAG, true);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                TheLApp.getContext().startActivity(intent);
            } else if (adBean.dumpType.equals(AdBean.ADV_DUMP_TYPE_TAG)) {
//                Intent intent = new Intent(TheLApp.getContext(), TagDetailActivity.class);
//                intent.putExtra(TheLConstants.BUNDLE_KEY_TOPIC_NAME, adBean.advertTitle);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                TheLApp.getContext().startActivity(intent);
                FlutterRouterConfig.Companion.gotoTagDetails(adBean.id, adBean.advertTitle);
            } else if (adBean.dumpType.equals(AdBean.ADV_DUMP_TYPE_MOMENT)) {
//                Intent intent = new Intent(TheLApp.getContext().getApplicationContext(), MomentCommentActivity.class);
//                intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, adBean.advertTitle);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                TheLApp.getContext().startActivity(intent);
                FlutterRouterConfig.Companion.gotoMomentDetails(adBean.advertTitle);
            } else if (adBean.dumpType.equals(AdBean.ADV_DUMP_TYPE_TOPIC)) {
//                Intent intent = new Intent(TheLApp.getContext().getApplicationContext(), ThemeDetailActivity.class);
//                intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, adBean.advertTitle);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                TheLApp.getContext().startActivity(intent);
                FlutterRouterConfig.Companion.gotoThemeDetails(adBean.advertTitle);
            } else if (adBean.dumpType.equals(AdBean.ADV_DUMP_TYPE_STICKER)) { // 跳转表情详情页面
                Intent intent = new Intent(TheLApp.getContext().getApplicationContext(), StickerPackDetailActivity.class);
                intent.putExtra("id", Long.valueOf(adBean.dumpURL));// 表情表id
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                TheLApp.getContext().startActivity(intent);
            } else if (adBean.dumpType.equals(AdBean.ADV_DUMP_TYPE_STICKER_STORE)) { // 跳转表情商店页面
                Intent intent = new Intent(TheLApp.getContext(), StickerStoreActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                TheLApp.getContext().startActivity(intent);
            } else if (AdBean.ADV_DUMP_TYPE_LIVE_ROOMS.equals(adBean.dumpType)) {
                Intent intent = new Intent();
                intent.setAction(TheLConstants.BROADCAST_GOTO_LIVE_ROOMS_PAGE);
                TheLApp.getContext().startActivity(intent);
            } else if (AdBean.ADV_DUMP_TYPE_LIVE_ROOM.equals(adBean.dumpType)) {
                Intent intent;
                if (adBean.isLandscape == 1) {
                    intent = new Intent(TheLApp.getContext(), LiveWatchHorizontalActivity.class);
                } else {
                    intent = new Intent(TheLApp.getContext(), LiveWatchActivity.class);
                }
                intent.putExtra(LiveWatchActivity.FROM_PAGE, LiveWatchActivity.FROM_PAGE_WEB);
                intent.putExtra(TheLConstants.BUNDLE_KEY_ID, adBean.advertTitle);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                TheLApp.getContext().startActivity(intent);

            } else if (AdBean.ADV_DUMP_TYPE_BUY_VIP.equals(adBean.dumpType)) {
                Intent vipConfigIntent = new Intent(TheLApp.getContext(), VipConfigActivity.class);
                vipConfigIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                TheLApp.getContext().startActivity(vipConfigIntent);
            } else if (AdBean.ADV_DUMP_TYPE_USER_INFO.equals(adBean.dumpType)) {
                FlutterRouterConfig.Companion.gotoUserInfo(adBean.advertTitle);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 判断日志是否被收藏
     *
     * @param momentsId
     * @return
     */
    public static boolean isCollected(String momentsId) {
        String favoriteList = SharedPrefUtils.getString(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.FAVORITE_MOMENT_LIST, "");
        String id = "[" + ShareFileUtils.getString(ShareFileUtils.ID, "") + "_" + momentsId + "]";
        return favoriteList.contains(id);
    }

    /**
     * 添加收藏日志
     *
     * @param momentId
     */
    public static void addCollectMoment(String momentId) {
        if (!TextUtils.isEmpty(momentId) && !"0".equals(momentId) && !BusinessUtils.isCollected(momentId)) {
            String collectList = SharedPrefUtils.getString(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.FAVORITE_MOMENT_LIST, "");
            String id = "[" + ShareFileUtils.getString(ShareFileUtils.ID, "") + "_" + momentId + "]";
            collectList += id;
            SharedPrefUtils.setString(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.FAVORITE_MOMENT_LIST, collectList);
            DialogUtil.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.collection_success));
        }
    }

    /**
     * 删除收藏日志
     *
     * @param momentId
     */
    public static void deleteCollectMoment(String momentId) {
        if (!TextUtils.isEmpty(momentId) && !"0".equals(momentId) && BusinessUtils.isCollected(momentId)) {
            String collectList = SharedPrefUtils.getString(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.FAVORITE_MOMENT_LIST, "");
            String id = "[" + ShareFileUtils.getString(ShareFileUtils.ID, "") + "_" + momentId + "]";
            collectList = collectList.replace(id, "");
            SharedPrefUtils.setString(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.FAVORITE_MOMENT_LIST, collectList);
            DialogUtil.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.collection_canceled));
        }
    }

    public static void shareMoment(Activity context, DialogUtils dialogUtils, MomentsBean momentBean, String entry) {
        String logo = MomentsBean.MOMENT_TYPE_THEME.equals(momentBean.momentsType) ? TheLConstantsExt.SHARE_THEME_LOGO_URL : TheLConstantsExt.DEFAULT_SHARE_LOGO_URL;
        if (momentBean != null) {
            GIOShareTrackBean trackBean = new GIOShareTrackBean();
            final String track = GrowingIOUtil.getTrack(momentBean);
            final String material = GrowingIOUtil.getMaterial(momentBean);
            trackBean.shareBean.shareEntry = entry;
            trackBean.track = track;
            trackBean.shareBean.shareMaterialType = material;

            if (MomentsBean.MOMENT_TYPE_IMAGE.equals(momentBean.momentsType) || MomentsBean.MOMENT_TYPE_TEXT_IMAGE.equals(momentBean.momentsType) || MomentsBean.MOMENT_TYPE_THEME_REPLY.equals(momentBean.momentsType) || MomentsBean.MOMENT_TYPE_THEME.equals(momentBean.momentsType) || MomentsBean.MOMENT_TYPE_USER_CARD.equals(momentBean.momentsType)) {
                if (!TextUtils.isEmpty(momentBean.imageUrl)) {
                    String[] photoUrls = momentBean.imageUrl.split(",");
                    if (photoUrls.length > 0) {
                        logo = photoUrls[0];
                    }
                }
            } else if (MomentsBean.MOMENT_TYPE_VOICE.equals(momentBean.momentsType) || MomentsBean.MOMENT_TYPE_TEXT_VOICE.equals(momentBean.momentsType)) {
                if (!TextUtils.isEmpty(momentBean.albumLogo444)) {
                    logo = momentBean.albumLogo444;
                }
            } else if (MomentsBean.MOMENT_TYPE_VIDEO.equals(momentBean.momentsType) || MomentsBean.MOMENT_TYPE_LIVE.equals(momentBean.momentsType)) {
                if (!TextUtils.isEmpty(momentBean.thumbnailUrl)) {
                    logo = momentBean.thumbnailUrl;
                }
            }
            try {
                if (MomentsBean.MOMENT_TYPE_THEME_REPLY.equals(momentBean.momentsType)) {//话题回复
                    String defaults = context.getResources().getString(R.string.default_share_talk, momentBean.nickname);
                    dialogUtils.showShareDialog(context, context.getString(R.string.share_title_moment), context.getString(R.string.share_theme_reply), TextUtils.isEmpty(momentBean.momentsText) ? defaults : momentBean.momentsText, momentBean.momentsText, TheLConstants.ALL_MOMENT_SHARE_URL(momentBean.momentsId), logo, false, true, null, null, false, trackBean);
                } else if (MomentsBean.MOMENT_TYPE_THEME.equals(momentBean.momentsType)) {//话题
                    String defaults = context.getResources().getString(R.string.default_share_talk, momentBean.nickname);
                    dialogUtils.showShareDialog(context, context.getString(R.string.share_title_moment), context.getString(R.string.share_released_topic), TextUtils.isEmpty(momentBean.momentsText) ? defaults : momentBean.momentsText, momentBean.momentsText, TheLConstants.ALL_MOMENT_SHARE_URL(momentBean.momentsId), logo, false, true, null, null, false, trackBean);

                } else if (MomentsBean.MOMENT_TYPE_VIDEO.equals(momentBean.momentsType)) {//视频
                    String defaults = context.getResources().getString(R.string.share_video_content, momentBean.nickname);
                    dialogUtils.showShareDialog(context, context.getString(R.string.share_title_moment), context.getString(R.string.share_video_title), TextUtils.isEmpty(momentBean.momentsText) ? defaults : momentBean.momentsText, defaults, TheLConstants.ALL_MOMENT_SHARE_URL(momentBean.momentsId), logo, false, true, null, null, false, trackBean);
                } else if (MomentsBean.MOMENT_TYPE_TEXT_IMAGE.equals(momentBean.momentsType) || MomentsBean.MOMENT_TYPE_IMAGE.equals(momentBean.momentsType) || MomentsBean.MOMENT_TYPE_TEXT.equals(momentBean.momentsType)) {
                    String defaults = context.getResources().getString(R.string.share_theme_content, momentBean.nickname);
                    dialogUtils.showShareDialog(context, context.getString(R.string.share_title_moment), context.getString(R.string.share_theme_journal), TextUtils.isEmpty(momentBean.momentsText) ? defaults : momentBean.momentsText, defaults, TheLConstants.ALL_MOMENT_SHARE_URL(momentBean.momentsId), logo, false, true, null, null, false, trackBean);

                } else if (MomentsBean.MOMENT_TYPE_LIVE.equals(momentBean.momentsType)) {//直播
                    String title = context.getResources().getString(R.string.share_live_titles, momentBean.nickname);
                    dialogUtils.showShareDialog(context, context.getString(R.string.share_title_moment), title, momentBean.momentsText, momentBean.momentsText, TheLConstants.ALL_MOMENT_SHARE_URL(momentBean.momentsId), logo, false, true, null, null, false, trackBean);

                } else if (MomentsBean.MOMENT_TYPE_USER_CARD.equals(momentBean.momentsType)) {//名片
                    String defaults = context.getResources().getString(R.string.share_card_title, momentBean.nickname);
                    String intro = context.getResources().getString(R.string.share_card_content);
                    dialogUtils.showShareDialog(context, context.getString(R.string.share_title_moment), defaults, TextUtils.isEmpty(momentBean.cardIntro) ? intro : momentBean.momentsText, momentBean.momentsText, TheLConstants.ALL_MOMENT_SHARE_URL(momentBean.momentsId), logo, false, true, null, null, false, trackBean);
                } else if (MomentsBean.MOMENT_TYPE_VOICE.equals(momentBean.momentsType) || MomentsBean.MOMENT_TYPE_TEXT_VOICE.equals(momentBean.momentsType)) {//音乐日志（音乐与文本音乐）
                    String defaults = context.getResources().getString(R.string.share_music_content, momentBean.nickname);
                    dialogUtils.showShareDialog(context, context.getString(R.string.share_title_moment), context.getString(R.string.share_music_title), TextUtils.isEmpty(momentBean.momentsText) ? defaults : momentBean.momentsText, defaults, TheLConstants.ALL_MOMENT_SHARE_URL(momentBean.momentsId), logo, false, true, null, null, false, trackBean);
                } else {//默认为文本
                    String defaults = context.getResources().getString(R.string.share_theme_content, momentBean.nickname);
                    dialogUtils.showShareDialog(context, context.getString(R.string.share_title_moment), context.getString(R.string.share_theme_journal), TextUtils.isEmpty(momentBean.momentsText) ? defaults : momentBean.momentsText, defaults, TheLConstants.ALL_MOMENT_SHARE_URL(momentBean.momentsId), logo, false, true, null, null, false, trackBean);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void getInitData() {

        RequestBusiness.getInstance().getBasicInfo()
                .onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new InterceptorSubscribe<BasicInfoNetBean>() {
                    @Override
                    public void onNext(BasicInfoNetBean data) {
                        super.onNext(data);
                        if (data != null && data.data != null) {

                            L.d("BusinessUtils", " data.date toString : " + data.data.toString());

                            RfSBridgeHandlerFactory.getInstance().getFlutterPushImpl().pushInitParams(data.data);

                            CDNBalanceManager.getInstance().initData(data.data.rtmpUrls, data.data.flvInfos, data.data.multiFlvInfos);

                            final BasicInfoBean basicInfoBean = data.data;
                            UserUtils.setUserVipLevel(basicInfoBean.level);
                            ShareFileUtils.setInt(TheLConstants.live_permit, basicInfoBean.perm);
                            TheLConstants.applyLivePermitPage = basicInfoBean.permApplyUrl;

                            L.d("BusinessUtils", " basicInfoBean.IMServer : " + basicInfoBean.IMServer);

                            if (basicInfoBean.IMServer != null && basicInfoBean.IMServer.size() > 0) {

                                ShareFileUtils.setString(ShareFileUtils.CHAT_IP, GsonUtils.createJsonString(basicInfoBean.IMServer));

                            }

                            if (data.data.vipSetting != null) {
                                ShareFileUtils.setInt(ShareFileUtils.MSG_HIDING, data.data.vipSetting.msgHiding);
                                ShareFileUtils.setInt(ShareFileUtils.VIP_INCOGNITO, data.data.vipSetting.incognito);
                            }

                            ShareFileUtils.setInt(ShareFileUtils.LIVE_URL_SORT, data.data.liveUrlSort);
                            ShareFileUtils.setInt(ShareFileUtils.RTMP_URL_SORT, data.data.rtmpUrlSort);
                            ShareFileUtils.setInt(ShareFileUtils.SUPER_LIKE, data.data.superLikeRetain);

                            SharedPrefUtils.setInt(SharedPrefUtils.FILE_LIVE, SharedPrefUtils.LIVE_PERMITION, basicInfoBean.perm);
                            SharedPrefUtils.setString(SharedPrefUtils.FILE_LIVE, SharedPrefUtils.LIVE_WIDTH, basicInfoBean.liveWidth + "");
                            SharedPrefUtils.setString(SharedPrefUtils.FILE_LIVE, SharedPrefUtils.LIVE_HEIGHT, basicInfoBean.liveHeight + "");
                            SharedPrefUtils.setString(SharedPrefUtils.FILE_LIVE, SharedPrefUtils.LIVE_FPS, basicInfoBean.liveFps + "");
                            SharedPrefUtils.setString(SharedPrefUtils.FILE_LIVE, SharedPrefUtils.LOWEST_BPS, basicInfoBean.lowestBps + "");
                            SharedPrefUtils.setString(SharedPrefUtils.FILE_LIVE, SharedPrefUtils.LIVE_BPS, basicInfoBean.liveBps + "");
                            SharedPrefUtils.setString(SharedPrefUtils.FILE_LIVE, SharedPrefUtils.FILE_TOP_LINK, basicInfoBean.topLink);
                            SharedPrefUtils.setInt(SharedPrefUtils.FILE_LIVE, SharedPrefUtils.KEY_ONLY_TEXT, basicInfoBean.onlyText);

                            ShareFileUtils.setInt(ShareFileUtils.PERM, basicInfoBean.perm);
                            ShareFileUtils.setInt(ShareFileUtils.HTTPS_SWITCH, basicInfoBean.httpsSwitch);
                            ShareFileUtils.setInt(ShareFileUtils.LEVEL_PRIVILEGES, basicInfoBean.levelPrivileges);

                            TheLApp.enablePubLive = basicInfoBean.enablePubLive;
                        }
                    }
                });
    }

    /**
     * 我是否可以进入某人直播间
     *
     * @param context
     * @param userId
     * @return
     */
    public static boolean canIntoLiveRoom(Context context, String userId) {
        return !BlackUtils.isBlockMe(userId);
    }
}
