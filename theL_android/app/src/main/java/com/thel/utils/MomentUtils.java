package com.thel.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.reflect.TypeToken;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.base.BaseDataBean;
import com.thel.bean.ImgShareBean;
import com.thel.bean.MentionedUserBean;
import com.thel.bean.SharePosterBean;
import com.thel.bean.comment.CommentBean;
import com.thel.bean.message.MomentToChatBean;
import com.thel.bean.moments.LiveUserMomentBean;
import com.thel.bean.moments.MomentParentBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.moments.RecommendMomentBean;
import com.thel.bean.recommend.RecommendWebBean;
import com.thel.bean.theme.ThemeClassBean;
import com.thel.bean.user.BlockBean;
import com.thel.bean.video.VideoBean;
import com.thel.constants.BundleConstants;
import com.thel.constants.MomentTypeConstants;
import com.thel.constants.QueueConstants;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.growingio.GrowingIoConstant;
import com.thel.imp.momentblack.MomentBlackUtils;
import com.thel.imp.momentdelete.MomentDeleteUtils;
import com.thel.modules.main.home.moments.ReportActivity;
import com.thel.modules.main.home.moments.web.WebViewActivity;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.modules.others.VipConfigActivity;
import com.thel.modules.post.MomentPosterActivity;
import com.thel.modules.preview_image.UserInfoPhotoActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestConstants;
import com.thel.network.service.DefaultRequestService;
import com.thel.ui.MatchTopicClickSpan;
import com.thel.ui.MentionedUserClickSpan;
import com.thel.ui.MentionedUserSpan;
import com.thel.ui.ParentClickLinkMovementMethod;
import com.thel.ui.RecommendUserLookSpan;
import com.thel.ui.TextSpan;
import com.thel.ui.TopicClickSpan;
import com.thel.ui.adapter.MomentsAdapter;
import com.thel.ui.widget.LatestCommentView;
import com.thel.ui.widget.LikeButtonView;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.thel.utils.StringUtils.getString;

/**
 * Created by liuyun on 2017/10/23.
 */

public class MomentUtils {

    private static String topicName = "";

    private static Matcher topicMatcher = TheLConstants.TOPIC_PATTERN.matcher("");

    private static Matcher urlMatcher = TheLConstants.URL_PATTERN.matcher("");

    public static void addComments(final Context mContext, LinearLayout lin_latest_comments, List<CommentBean> commentBeans, String momentsId, String momentsType) {

        for (int i = 0; i < commentBeans.size(); i++) {
            LatestCommentView latestCommentView = new LatestCommentView(lin_latest_comments.getContext());
            latestCommentView.setGravity(Gravity.LEFT);
            if (i > 0) {
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.topMargin = SizeUtils.dip2px(TheLApp.getContext(), 5);
                latestCommentView.setLayoutParams(params);
            }
            final String text;
            if (CommentBean.COMMENT_TYPE_STICKER.equals(commentBeans.get(i).commentType))
                text = TheLApp.getContext().getString(R.string.message_info_sticker);
            else
                text = commentBeans.get(i).commentText;
            latestCommentView.setText(commentBeans.get(i).nickname, text, commentBeans.get(i).userId + "", momentsId, MomentTypeConstants.MOMENT_TYPE_THEME.equals(momentsType) ? "theme" : "moment");
            latestCommentView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    ((LatestCommentView) v).disableSpanClick();
                    DialogUtil.getInstance().showSelectionDialog((Activity) mContext, new String[]{
                            TheLApp.getContext().getString(R.string.info_copy)}, new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            DialogUtil.getInstance().closeDialog();
                            switch (position) {
                                case 0:// 复制
                                    DeviceUtils.copyToClipboard(TheLApp.getContext(), text);
                                    break;

                                default:
                                    break;
                            }
                        }
                    }, false, 2, null);
                    return true;
                }
            });
            lin_latest_comments.addView(latestCommentView);
        }
    }

    public static SpannableString getMetionedShowString(List<MentionedUserBean> atUserList, String momentsId) {
        if (atUserList.size() == 0) {
            return null;
        }
        StringBuilder sb = new StringBuilder(TheLApp.getContext().getString(R.string.moments_mentioned));
        int size = 1;
        if (atUserList.size() >= 2) {
            size = 2;
        }
        for (int i = 0; i < size; i++) {
            sb.append(atUserList.get(i).nickname);
            sb.append(", ");
        }
        sb.delete(sb.length() - 2, sb.length());
        if (atUserList.size() > 2) {
            sb.append(TheLApp.getContext().getString(R.string.moments_and));
            sb.append(atUserList.size() - 2);
            sb.append(TheLApp.getContext().getString(R.string.moments_more));
        }

        // 构造高亮点击样式
        SpannableString str = new SpannableString(sb.toString());
        if (atUserList.size() > 0) {// 第一个用户
            str.setSpan(new MentionedUserClickSpan(atUserList.get(0).userId, momentsId), sb.indexOf(atUserList.get(0).nickname), sb.indexOf(atUserList.get(0).nickname) + atUserList.get(0).nickname.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            if (atUserList.size() > 1) {// 第二个用户
                str.setSpan(new MentionedUserClickSpan(atUserList.get(1).userId, momentsId), sb.indexOf(atUserList.get(1).nickname), sb.indexOf(atUserList.get(1).nickname) + atUserList.get(1).nickname.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                if (atUserList.size() > 2) {// more
                    str.setSpan(new MentionedUserClickSpan(-1, momentsId), sb.indexOf(atUserList.get(1).nickname) + atUserList.get(1).nickname.length(), sb.indexOf(atUserList.size() - 2 + TheLApp.getContext().getString(R.string.moments_more)) + (atUserList.size() - 2 + TheLApp.getContext().getString(R.string.moments_more)).length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
            }
        }

        return str;
    }

    public static SpannableString getMetionedShowStringForSendMoment(List<MentionedUserBean> atUserList, String momentsId) {
        if (atUserList.size() == 0) {
            return null;
        }
        StringBuilder sb = new StringBuilder(TheLApp.getContext().getString(R.string.moments_mentioned));
        int size = 1;
        if (atUserList.size() >= 2) {
            size = 2;
        }
        for (int i = 0; i < size; i++) {
            sb.append(atUserList.get(i).nickname);
            sb.append(", ");
        }
        sb.delete(sb.length() - 2, sb.length());
        if (atUserList.size() > 2) {
            sb.append(TheLApp.getContext().getString(R.string.moments_and));
            sb.append(atUserList.size() - 2);
            sb.append(TheLApp.getContext().getString(R.string.moments_more));
        }

        // 构造高亮点击样式
        SpannableString str = new SpannableString(sb.toString());
        if (atUserList.size() > 0) {// 第一个用户
            str.setSpan(new MentionedUserSpan(), sb.indexOf(atUserList.get(0).nickname), sb.indexOf(atUserList.get(0).nickname) + atUserList.get(0).nickname.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            if (atUserList.size() > 1) {// 第二个用户
                str.setSpan(new MentionedUserSpan(), sb.indexOf(atUserList.get(1).nickname), sb.indexOf(atUserList.get(1).nickname) + atUserList.get(1).nickname.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                if (atUserList.size() > 2) {// more
                    str.setSpan(new MentionedUserSpan(), sb.indexOf(atUserList.size() - 2 + TheLApp.getContext().getString(R.string.moments_more)), sb.indexOf(atUserList.size() - 2 + TheLApp.getContext().getString(R.string.moments_more)) + (atUserList.size() - 2 + TheLApp.getContext().getString(R.string.moments_more)).length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
            }
        }

        return str;
    }

    public static String getReleaseTimeShow(int secret, String momentsTime) {
        if (TextUtils.isEmpty(momentsTime)) {
            return "";
        }
        String[] arr = momentsTime.split("_");
        if (arr.length < 2) {
            return "";
        }
        if (secret == 0) {// 非匿名
            if ("0".equals(arr[0])) {
                return TheLApp.getContext().getString(R.string.info_just_now);
            } else if ("1".equals(arr[0])) {
                return arr[1] + TheLApp.getContext().getString(R.string.info_seconds_ago);
            } else if ("2".equals(arr[0])) {
                return arr[1] + TheLApp.getContext().getString(R.string.info_minutes_ago);
            } else if ("3".equals(arr[0])) {
                return arr[1] + TheLApp.getContext().getString(R.string.info_hours_ago);
            } else if ("4".equals(arr[0])) {
                return arr[1] + TheLApp.getContext().getString(R.string.info_days_ago);
            } else if ("5".equals(arr[0])) {
                return arr[1];
            }
        } else {// 匿名
            if ("11".equals(arr[0])) {
                return TheLApp.getContext().getString(R.string.chat_activity_today);
            } else if ("12".equals(arr[0])) {
                return arr[1] + TheLApp.getContext().getString(R.string.info_days_ago);
            } else if ("13".equals(arr[0])) {
                return arr[1];
            }
        }
        return "";

    }

    public static void jumpToUserPage(String userId) {
//        Intent intent = new Intent(TheLApp.getContext(), UserInfoActivity.class);
//        intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, userId);
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        TheLApp.getContext().startActivity(intent);
        FlutterRouterConfig.Companion.gotoUserInfo(userId);
    }

    public static String buildDesc(int joinTotal) {
        String joins;
        if (joinTotal > 1) {
            joins = TheLApp.getContext().getString(R.string.hot_themes_join_even, joinTotal);
        } else {
            joins = TheLApp.getContext().getString(R.string.hot_themes_join_odd, joinTotal);
        }
        return joins;
    }

    public static String buildUserCardInfoStr(int cardAge, String cardAffection, int cardHeight, int cardWeight) {
        ArrayList<String> affections = new ArrayList<>(Arrays.asList(TheLApp.getContext().getResources().getStringArray(R.array.userinfo_relationship)));
        final StringBuilder sb = new StringBuilder();
        sb.append(cardAge).append(TheLApp.getContext().getString(R.string.updatauserinfo_activity_age_unit)).append("，").append(getHandW(cardHeight, cardWeight));
        if (!TextUtils.isEmpty(cardAffection)) {
            try {
                sb.append("，" + affections.get(Integer.valueOf(cardAffection)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    //身高体重
    public static String getHandW(int height, int weight) {
        int heightUnits = ShareFileUtils.getInt(ShareFileUtils.HEIGHT_UNITS, 0); // 身高单位(0=cm, 1=Inches)
        int weightUnits = ShareFileUtils.getInt(ShareFileUtils.WEIGHT_UNITS, 0); // 体重单位(0=kg, 1=Lbs)
        try {
            String h;
            String w;
            if (heightUnits == 0) {
                h = height + " cm";
            } else {
                h = height + " Inches";
            }
            if (weightUnits == 0) {
                w = weight + " kg";
            } else {
                w = weight + " Lbs";
            }
            return h + "，" + w;
        } catch (Exception e) {
        }
        return "";
    }

    public static ThemeClassBean getThemeByType(String type) {
        final int size = QueueConstants.themeClassBeanMap.size();
        for (int i = 0; i < size; i++) {
            ThemeClassBean bean = QueueConstants.themeClassBeanMap.get(i);
            if (bean.type.equals(type)) {
                return bean;
            }
        }
        return null;
    }

    /**
     * 获取日志里面 重复推荐的人 SP，张三，李四，也推荐了这个日志，查看（A,B,C,等5人也推荐了这个日志，查看）
     * 本方法里面的数据从后台传送过来的
     *
     * @param momentBean
     * @return
     */
    public static SpannableString getRecommendUserListBySystem(MomentsBean momentBean) {
        if (momentBean == null && momentBean.recommendMoment == null && momentBean.parentMoment != null) {
            return null;
        }
        final RecommendMomentBean recommendMomentBean = momentBean.recommendMoment;
        if (recommendMomentBean.count == 0) {
            return null;
        }
        final int count = recommendMomentBean.count;

        int size;

        if (recommendMomentBean.userList == null) {
            size = 0;
        } else {
            size = recommendMomentBean.userList.size();
        }

        final StringBuilder sb = new StringBuilder();
        if (size > 3) {
            for (int i = 0; i < 3; i++) {
                sb.append(recommendMomentBean.userList.get(i).nickname).append(",");
            }
            sb.append(TheLApp.getContext().getString(R.string.sonme_other_people, count))//
                    .append(TheLApp.getContext().getString(R.string.recommend_this_moment))//
                    .append(TheLApp.getContext().getString(R.string.click_for_look));
        } else {
            for (int i = 0; i < size; i++) {
                sb.append(recommendMomentBean.userList.get(i).nickname).append(",");
            }
            sb.append(TheLApp.getContext().getString(R.string.recommend_this_moment))//
                    .append(TheLApp.getContext().getString(R.string.click_for_look));
        }
        SpannableString str = new SpannableString(sb.toString());
        if (size > 0) {
            str.setSpan(new MentionedUserClickSpan(Integer.parseInt(recommendMomentBean.userList.get(0).userId), momentBean.momentsId), sb.indexOf(recommendMomentBean.userList.get(0).nickname), sb.indexOf(recommendMomentBean.userList.get(0).nickname) + recommendMomentBean.userList.get(0).nickname.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            if (size > 1) {
                str.setSpan(new MentionedUserClickSpan(Integer.parseInt(recommendMomentBean.userList.get(0).userId), momentBean.momentsId), sb.indexOf(recommendMomentBean.userList.get(1).nickname), sb.indexOf(recommendMomentBean.userList.get(1).nickname) + recommendMomentBean.userList.get(1).nickname.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                if (size > 2) {
                    str.setSpan(new MentionedUserClickSpan(Integer.parseInt(recommendMomentBean.userList.get(0).userId), momentBean.momentsId), sb.indexOf(recommendMomentBean.userList.get(2).nickname), sb.indexOf(recommendMomentBean.userList.get(2).nickname) + recommendMomentBean.userList.get(2).nickname.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
            }
        }
        str.setSpan(new RecommendUserLookSpan(momentBean.parentMoment.momentsId, count), sb.lastIndexOf(TheLApp.getContext().getString(R.string.click_for_look)), sb.lastIndexOf(TheLApp.getContext().getString(R.string.click_for_look)) + TheLApp.getContext().getString(R.string.click_for_look).length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return str;
    }

    public static void setMomentContent(TextView text_moment_content_parent, String momentsText) {
        if (!TextUtils.isEmpty(momentsText)) {
            text_moment_content_parent.setVisibility(VISIBLE);
        }
        // moment文字内容
        // 检查是否有输入话题，有的话把话题标红
        topicMatcher.reset(momentsText);
        List<Integer> startIndexs = new ArrayList<Integer>();
        List<Integer> endIndexs = new ArrayList<Integer>();
        while (topicMatcher.find()) {
            startIndexs.add(topicMatcher.start());
            endIndexs.add(topicMatcher.start() + topicMatcher.group().length());
        }
        // 检查是否有输入网址，有的话把网址替换样式
        urlMatcher.reset(momentsText);
        List<Integer> startIndexsUrl = new ArrayList<Integer>();
        List<Integer> endIndexsUrl = new ArrayList<Integer>();
        while (urlMatcher.find()) {
            startIndexsUrl.add(urlMatcher.start());
            endIndexsUrl.add(urlMatcher.start() + urlMatcher.group().length());
        }
        if (!startIndexs.isEmpty() || !startIndexsUrl.isEmpty()) {
            // 创建一个 SpannableString对象
            SpannableString sp = new SpannableString(momentsText);
            for (int i = 0; i < startIndexs.size(); i++) {
                // 设置高亮样式，话题主页不能点击话题只给高亮，所以加个标识区分一下
                if (TextUtils.isEmpty(topicName)) {
                    sp.setSpan(new TopicClickSpan(sp.subSequence(startIndexs.get(i), endIndexs.get(i))), startIndexs.get(i), endIndexs.get(i), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                } else {
                    if (sp.subSequence(startIndexs.get(i), endIndexs.get(i)).toString().equals(topicName)) {
                        sp.setSpan(new ForegroundColorSpan(ContextCompat.getColor(TheLApp.getContext(), R.color.tag_color)), startIndexs.get(i), endIndexs.get(i), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                    } else {
                        sp.setSpan(new TopicClickSpan(sp.subSequence(startIndexs.get(i), endIndexs.get(i))), startIndexs.get(i), endIndexs.get(i), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    }
                }

            }
            for (int i = 0; i < startIndexsUrl.size(); i++) {
                final String url = momentsText.substring(startIndexsUrl.get(i), endIndexsUrl.get(i));
                sp.setSpan(new ImageSpan(TheLApp.getContext(), createUrlDrawable()), startIndexsUrl.get(i), endIndexsUrl.get(i), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                sp.setSpan(new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(TheLApp.getContext(), WebViewActivity.class);
                        intent.putExtra(BundleConstants.URL, url);
                        intent.putExtra("title", "");
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        TheLApp.getContext().startActivity(intent);
                    }
                }, startIndexsUrl.get(i), endIndexsUrl.get(i), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
            // SpannableString对象设置给TextView
            text_moment_content_parent.setText(sp);
            text_moment_content_parent.setMovementMethod(ParentClickLinkMovementMethod.getInstance());
        } else {
            text_moment_content_parent.setText(momentsText);
        }
    }

    /**
     * 匹配 不跳转
     **/
    public static void setMatchMomentContent(TextView text_moment_content_parent, String momentsText) {
        if (!TextUtils.isEmpty(momentsText)) {
            text_moment_content_parent.setVisibility(VISIBLE);
        }
        // moment文字内容
        // 检查是否有输入话题，有的话把话题标红
        topicMatcher.reset(momentsText);
        List<Integer> startIndexs = new ArrayList<Integer>();
        List<Integer> endIndexs = new ArrayList<Integer>();
        while (topicMatcher.find()) {
            startIndexs.add(topicMatcher.start());
            endIndexs.add(topicMatcher.start() + topicMatcher.group().length());
        }
        // 检查是否有输入网址，有的话把网址替换样式
        urlMatcher.reset(momentsText);
        List<Integer> startIndexsUrl = new ArrayList<Integer>();
        List<Integer> endIndexsUrl = new ArrayList<Integer>();
        while (urlMatcher.find()) {
            startIndexsUrl.add(urlMatcher.start());
            endIndexsUrl.add(urlMatcher.start() + urlMatcher.group().length());
        }
        if (!startIndexs.isEmpty() || !startIndexsUrl.isEmpty()) {
            // 创建一个 SpannableString对象
            SpannableString sp = new SpannableString(momentsText);
            for (int i = 0; i < startIndexs.size(); i++) {
                // 设置高亮样式，话题主页不能点击话题只给高亮，所以加个标识区分一下
                if (TextUtils.isEmpty(topicName)) {
                    sp.setSpan(new MatchTopicClickSpan(sp.subSequence(startIndexs.get(i), endIndexs.get(i))), startIndexs.get(i), endIndexs.get(i), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                } else {
                    if (sp.subSequence(startIndexs.get(i), endIndexs.get(i)).toString().equals(topicName)) {
                        sp.setSpan(new ForegroundColorSpan(ContextCompat.getColor(TheLApp.getContext(), R.color.tag_color)), startIndexs.get(i), endIndexs.get(i), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                    } else {
                        sp.setSpan(new MatchTopicClickSpan(sp.subSequence(startIndexs.get(i), endIndexs.get(i))), startIndexs.get(i), endIndexs.get(i), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    }
                }

            }
            for (int i = 0; i < startIndexsUrl.size(); i++) {
                final String url = momentsText.substring(startIndexsUrl.get(i), endIndexsUrl.get(i));
                sp.setSpan(new ImageSpan(TheLApp.getContext(), createUrlDrawable()), startIndexsUrl.get(i), endIndexsUrl.get(i), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                sp.setSpan(new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                      /*  Intent intent = new Intent(TheLApp.getContext(), WebViewActivity.class);
                        intent.putExtra(BundleConstants.URL, url);
                        intent.putExtra("title", "");
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        TheLApp.getContext().startActivity(intent);*/
                    }
                }, startIndexsUrl.get(i), endIndexsUrl.get(i), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
            // SpannableString对象设置给TextView
            text_moment_content_parent.setText(sp);
            text_moment_content_parent.setMovementMethod(ParentClickLinkMovementMethod.getInstance());
        } else {
            text_moment_content_parent.setText(momentsText);
        }
    }

    public static Bitmap createUrlDrawable() {
        View view = LayoutInflater.from(TheLApp.getContext()).inflate(R.layout.layout_url, null);
        view.setDrawingCacheEnabled(true);
        view.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        return view.getDrawingCache();
    }

    public static void updateLikeStatus(MomentsBean momentBean, LikeButtonView moment_opts_like, TextView moment_opts_emoji_txt, RecyclerView rv_likes, int likePosition) {
        // 是否已点过赞
        if (momentBean.winkFlag != MomentsBean.HAS_NOT_WINKED) {
            moment_opts_like.setChecked(true);
        } else {
            moment_opts_like.setChecked(false);
        }
        moment_opts_like.setTag(R.id.moment_like_tag, momentBean.winkFlag);
        // 点赞数
        if (momentBean.winkNum <= 0) {
            moment_opts_emoji_txt.setVisibility(GONE);
        } else {
            moment_opts_emoji_txt.setVisibility(VISIBLE);
            moment_opts_emoji_txt.setTag(R.id.moment_id_tag, momentBean.momentsId);

            //            moment_opts_emoji_txt.setOnClickListener(mOnClickListener == null ? (View.OnClickListener) context : mOnClickListener);
            moment_opts_emoji_txt.setText(TheLApp.getContext().getString(momentBean.winkNum == 1 ? R.string.like_count_odd : R.string.like_count_even, momentBean.winkNum));
        }

        final RecyclerView.Adapter adapter = rv_likes.getAdapter();
        if (adapter != null) {
            if (adapter.getItemCount() == 0)
                rv_likes.setVisibility(GONE);
            else
                rv_likes.setVisibility(View.VISIBLE);
            if (likePosition >= 0) {
                if (momentBean.winkFlag != MomentsBean.HAS_NOT_WINKED) {
                    adapter.notifyItemInserted(likePosition);
                } else {
                    adapter.notifyItemRemoved(likePosition);
                }
            } else {
                adapter.notifyDataSetChanged();
            }
        } else {
            rv_likes.setVisibility(GONE);
        }
    }

    public static void openPhoto(View v, String relaId, String urls, int position, MomentsBean momentBean) {
        ViewUtils.preventViewMultipleClick(v, 1000);
        String[] photoUrls = urls.split(",");
        Intent intent = new Intent(TheLApp.getContext(), UserInfoPhotoActivity.class);
        ImgShareBean bean = getImageShareBean(momentBean);
        Bundle bundle = new Bundle();
        bundle.putSerializable(TheLConstants.BUNDLER_KEY_IMAGESHAREBEAN, bean);
        intent.putExtras(bundle);
        ArrayList<String> photos = new ArrayList<>();
        for (int i = 0; i < photoUrls.length; i++) {
            photos.add(photoUrls[i]);
        }
        intent.putStringArrayListExtra(TheLConstants.BUNDLE_KEY_PHOTOS, photos);
        intent.putExtra(TheLConstants.BUNDLE_KEY_RELA_ID, relaId);
        intent.putExtra(TheLConstants.IS_WATERMARK, true);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("position", position);
        TheLApp.getContext().startActivity(intent);
    }

    public static ImgShareBean getImageShareBean(MomentsBean momentBean) {
        if (momentBean != null) {
            final ImgShareBean bean = new ImgShareBean();
            bean.userID = momentBean.userName;
            bean.picUserUrl = momentBean.avatar;
            bean.nickName = momentBean.nickname;
            bean.commentText = momentBean.momentsText;
            bean.imageUrl = momentBean.imageUrl;
            return bean;
        }
        return null;
    }

    public static void reportMoment(Context mContext, MomentsBean momentsBean) {
        Intent intent = new Intent(mContext, ReportActivity.class);
        intent.putExtra(TheLConstants.BUNDLE_KEY_REPORT_TYPE, ReportActivity.REPORT_TYPE_ABUSE);
        intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, momentsBean.momentsId);
        ((Activity) mContext).startActivityForResult(intent, TheLConstants.BUNDLE_CODE_MOMENTS_ACTIVITY);
    }

    public static void reportMoment(Context mContext, String momentsId) {
        Intent intent = new Intent(mContext, ReportActivity.class);
        intent.putExtra(TheLConstants.BUNDLE_KEY_REPORT_TYPE, ReportActivity.REPORT_TYPE_ABUSE);
        intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, momentsId);
        ((Activity) mContext).startActivityForResult(intent, TheLConstants.BUNDLE_CODE_MOMENTS_ACTIVITY);
    }

    /**
     * 屏蔽这条日志
     */
    public static void blockThisMoment(Context mContext, MomentsBean momentsBean) {
        Intent intent = new Intent(mContext, ReportActivity.class);
        intent.putExtra(TheLConstants.BUNDLE_KEY_REPORT_TYPE, ReportActivity.REPORT_TYPE_HIDE_MOMENT);
        intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, momentsBean.momentsId);
        ((Activity) mContext).startActivityForResult(intent, TheLConstants.BUNDLE_CODE_MOMENTS_ACTIVITY);
    }

    /**
     * 屏蔽这个用户的所有日志
     */
    public static void blockHerMoments(final Context mContext, final MomentsBean momentsBean) {
        DialogUtil.showConfirmDialog((Activity) mContext, null, TheLApp.context.getString(R.string.my_block_user_moments_activity_block_confirm), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                ((BaseActivity) mContext).showLoadingNoBack();
                Map<String, String> data = new HashMap<>();
                data.put(RequestConstants.I_USERID, String.valueOf(momentsBean.userId));
                L.d("getMyuserid", "id2： " + momentsBean.userId);

                Flowable<BlockBean> flowable = DefaultRequestService.createMomentRequestService().blockUserMoments(MD5Utils.generateSignatureForMap(data));
                flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BlockBean>() {
                    @Override
                    public void onComplete() {
                        super.onComplete();
                        ((BaseActivity) mContext).closeLoading();


                    }
                });
            }
        });
    }

    /**
     * 收藏/取消收藏
     *
     * @param collect true为收藏，false为取消收藏
     */
    public static void collect(boolean collect, final MomentsBean momentsBean) {

        Map<String, String> map = new HashMap<>();

        if (collect) {           //收藏

            map.put(RequestConstants.FAVORITE_COUNT, momentsBean.momentsId);
            map.put(RequestConstants.FAVORITE_TYPE, RequestConstants.FAVORITE_TYPE_MOM);

            Flowable<BaseDataBean> flowable = DefaultRequestService.createMomentRequestService().getFavoriteCreate(MD5Utils.generateSignatureForMap(map));
            flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
                @Override
                public void onNext(BaseDataBean data) {
                    super.onNext(data);
                    if (momentsBean != null && !BusinessUtils.isCollected(momentsBean.momentsId)) {
                        String collectList = SharedPrefUtils.getString(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.FAVORITE_MOMENT_LIST, "");
                        String id = "[" + ShareFileUtils.getString(ShareFileUtils.ID, "") + "_" + momentsBean.momentsId + "]";
                        collectList += id;
                        SharedPrefUtils.setString(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.FAVORITE_MOMENT_LIST, collectList);
                    }
                }

                @Override
                public void onComplete() {
                    super.onComplete();
                    DialogUtil.showToastShort(TheLApp.context, getString(R.string.collection_success));

                }
            });

        } else {                  //取消收藏
            map.put(RequestConstants.FAVORITE_ID, "");
            map.put(RequestConstants.FAVORITE_COUNT, momentsBean.momentsId);
            map.put(RequestConstants.FAVORITE_TYPE, RequestConstants.FAVORITE_TYPE_MOM);

            Flowable<BaseDataBean> flowable = DefaultRequestService.createMomentRequestService().deleteFavoriteMoment(MD5Utils.generateSignatureForMap(map));
            flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
                @Override
                public void onNext(BaseDataBean data) {
                    super.onNext(data);
                    if (momentsBean != null && BusinessUtils.isCollected(momentsBean.momentsId)) {
                        String collecList = SharedPrefUtils.getString(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.FAVORITE_MOMENT_LIST, "");
                        String id = "[" + ShareFileUtils.getString(ShareFileUtils.ID, "") + "_" + momentsBean.momentsId + "]";
                        collecList = collecList.replace(id, "");
                        SharedPrefUtils.setString(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.FAVORITE_MOMENT_LIST, collecList);
                    }
                }

                @Override
                public void onComplete() {
                    super.onComplete();
                    DialogUtil.showToastShort(TheLApp.context, getString(R.string.collection_canceled));

                }
            });
        }
    }

    /**
     * @param type 0: 分享链接  1: 分享海报
     */
    public static void share(final Context mContext, int type, MomentsBean momentsBean) {
        if (type == 0) {
            BusinessUtils.shareMoment((Activity) mContext, new DialogUtils(), momentsBean, GrowingIoConstant.ENTRY_MOMENT_DETAIL_PAGE);
        } else {
            if (momentsBean != null) {
                SharePosterBean sharePosterBean = new SharePosterBean();
                sharePosterBean.avatar = momentsBean.avatar;
                sharePosterBean.momentsText = momentsBean.momentsText;
                sharePosterBean.imageUrl = momentsBean.imageUrl;
                sharePosterBean.nickname = momentsBean.nickname;
                sharePosterBean.userName = momentsBean.userName;
                sharePosterBean.from = MomentPosterActivity.FROM_MOMENT;
                MomentPosterActivity.gotoShare(sharePosterBean);
            }
        }
    }

    /**
     * 删除moment数据
     */
    public static void deleteMomentData(final Context mContext, final MomentsBean momentBean) {
        DialogUtil.showConfirmDialog((Activity) mContext, null, TheLApp.context.getString(R.string.moments_delete_moment_confirm), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (!TextUtils.isEmpty(momentBean.momentsId)) {
                    ((BaseActivity) mContext).showLoadingNoBack();
                    Map<String, String> map = new HashMap<>();
                    map.put(RequestConstants.I_MOMENTS_ID, momentBean.momentsId);
                    Flowable<BaseDataBean> flowable = DefaultRequestService.createMomentRequestService().deleteMoment(MD5Utils.generateSignatureForMap(map));
                    flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
                        @Override
                        public void onComplete() {
                            ((BaseActivity) mContext).closeLoading();
                        }

                        @Override
                        public void onNext(BaseDataBean data) {
                            super.onNext(data);
                            ((BaseActivity) mContext).closeLoading();
                            if (!hasErrorCode && data != null) {
                                MomentDeleteUtils.sendDeleteMomentBroad(momentBean.momentsId);
                            }
                        }
                    });

                }
            }
        });
    }

    /**
     * 置顶moment数据
     */
    public static void stickMomentDataWhitDialog(final Context mContext, final MomentsBean momentBean) {
        DialogUtil.showConfirmDialog((Activity) mContext, null, TheLApp.context.getString(R.string.moments_stick_moment_confirm), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                stickMomentData(mContext, momentBean);
            }
        });
    }

    public static void stickMomentData(final Context mContext, final MomentsBean momentBean) {
        if (!TextUtils.isEmpty(momentBean.momentsId)) {
            Map<String, String> map = new HashMap<>();
            map.put(RequestConstants.I_MOMENTS_ID, momentBean.momentsId);
            Flowable<BaseDataBean> flowable = DefaultRequestService.createMomentRequestService().stickMyMoment(MD5Utils.generateSignatureForMap(map));
            flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
                @Override
                public void onComplete() {
                    ToastUtils.showToastShort(TheLApp.context, TheLApp.context.getString(R.string.moments_sticked));
                    sendStickBroadcast(mContext, momentBean.momentsId, 1);

                }
            });
        }
    }

    /**
     * 取消置顶moment数据
     */
    public static void unstickMomentDataWhitDialog(final Context mContext, final MomentsBean momentBean) {
        DialogUtil.showConfirmDialog((Activity) mContext, null, TheLApp.context.getString(R.string.moments_unstick_moment_confirm), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                unstickMomentData(mContext, momentBean);
            }
        });
    }

    public static void unstickMomentData(final Context mContext, final MomentsBean momentBean) {
        if (!TextUtils.isEmpty(momentBean.momentsId)) {
            Map<String, String> map = new HashMap<>();
            map.put(RequestConstants.I_MOMENTS_ID, momentBean.momentsId);
            Flowable<BaseDataBean> flowable = DefaultRequestService.createMomentRequestService().unstickMyMoment(MD5Utils.generateSignatureForMap(map));
            flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
                @Override
                public void onComplete() {
                    ToastUtils.showToastShort(TheLApp.context, TheLApp.context.getString(R.string.moments_unsticked));
                    sendStickBroadcast(mContext, momentBean.momentsId, 0);
                }
            });
        }
    }


    private static void sendStickBroadcast(Context mContext, String momentsId, int stick_status) {

        Intent intent = new Intent();
        intent.setAction(TheLConstants.BROADCAST_STICK_MOMENT);
        intent.putExtra("momentsId", momentsId);
        intent.putExtra("stick_status", stick_status);
        mContext.sendBroadcast(intent);

    }

    /**
     * 将日志设为仅自己可见
     */
    public static void setMomentAsPrivate(final Context mContext, final MomentsBean momentBean, MomentsAdapter.PrivateOrPublicMomentSubscribe interceptorSubscribe) {
        DialogUtil.showConfirmDialog((Activity) mContext, null, TheLApp.context.getString(R.string.moments_set_private_moment_confirm), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (!TextUtils.isEmpty(momentBean.momentsId)) {
                    Map<String, String> map = new HashMap<>();
                    map.put(RequestConstants.I_MOMENTS_ID, momentBean.momentsId);
                    Flowable<BaseDataBean> flowable = DefaultRequestService.createMomentRequestService().setMyMomentAsPrivate(MD5Utils.generateSignatureForMap(map));
                    flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(interceptorSubscribe);
                }
            }
        });
    }

    /**
     * 将日志设为公开
     */
    public static void setMomentAsPublic(final Context mContext, final MomentsBean momentBean, MomentsAdapter.PrivateOrPublicMomentSubscribe interceptorSubscribe) {
        DialogUtil.showConfirmDialog((Activity) mContext, null, TheLApp.context.getString(R.string.moments_set_public_moment_confirm), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (!TextUtils.isEmpty(momentBean.momentsId)) {
                    Map<String, String> data = new HashMap<>();
                    data.put(RequestConstants.I_MOMENTS_ID, momentBean.momentsId);
                    Flowable<BaseDataBean> flowable = DefaultRequestService.createMomentRequestService().setMyMomentAsPublic(MD5Utils.generateSignatureForMap(data));
                    flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(interceptorSubscribe);
                }
            }
        });
    }

    public static void clickMore(final Context mContext, final MomentsBean momentsBean, MomentsAdapter.PrivateOrPublicMomentSubscribe interceptorSubscribe) {
        if (!momentsBean.momentsId.contains(MomentsBean.SEND_MOMENT_FLAG)) {

            if (!TextUtils.isEmpty(momentsBean.momentsId)) {
                if (momentsBean.myself == MomentsBean.IS_MINE) {
                    String stick;
                    if (momentsBean.userBoardTop == 0) {
                        if (UserUtils.getUserVipLevel() > 0) {
                            stick = TheLApp.context.getString(R.string.moments_stick_moment_vip);
                        } else {
                            stick = TheLApp.context.getString(R.string.moments_stick_moment);
                        }
                    } else {
                        if (UserUtils.getUserVipLevel() > 0) {
                            stick = TheLApp.context.getString(R.string.moments_unstick_moment_vip);
                        } else {
                            stick = TheLApp.context.getString(R.string.moments_unstick_moment);
                        }
                    }
                    String setPrivate;
                    if (momentsBean.shareTo < MomentsBean.SHARE_TO_ONLY_ME) {
                        setPrivate = TheLApp.context.getString(R.string.moments_set_as_private);
                    } else {
                        setPrivate = TheLApp.context.getString(R.string.moments_set_as_public);
                    }
                    DialogUtils.showSelectionDialogWide((Activity) mContext, 3 / 4f, new String[]{
                            stick,
                            setPrivate,
                            TheLApp.context.getString(R.string.info_delete)}, new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            DialogUtils.dismiss();
                            switch (position) {
                                case 0:
                                    if (UserUtils.getUserVipLevel() > 0) {
                                        if (momentsBean.userBoardTop == 0)
                                            stickMomentDataWhitDialog(mContext, momentsBean);
                                        else
                                            unstickMomentDataWhitDialog(mContext, momentsBean);
                                    } else {
                                        MobclickAgent.onEvent(TheLApp.getContext(), "check_top_moment");
                                        mContext.startActivity(new Intent(mContext, VipConfigActivity.class));
                                    }
                                    break;
                                case 1:
                                    if (momentsBean.shareTo < MomentsBean.SHARE_TO_ONLY_ME) {
                                        interceptorSubscribe.isPrivate(true);
                                        setMomentAsPrivate(mContext, momentsBean, interceptorSubscribe);
                                    } else {
                                        interceptorSubscribe.isPrivate(false);
                                        setMomentAsPublic(mContext, momentsBean, interceptorSubscribe);
                                    }
                                    break;
                                case 2:
                                    MomentDeleteUtils.deleteMoment(mContext, momentsBean.momentsId);
                                    break;

                                default:
                                    break;
                            }
                        }
                    }, true, 1, null);
                } else {
                    DialogUtils.showSelectionDialogWide((Activity) mContext, 3 / 4f, new String[]{
                            TheLApp.context.getString(R.string.info_report),
                            TheLApp.context.getString(R.string.my_block_user_moments_activity_block_moment),
                            TheLApp.context.getString(R.string.dont_see_her_all_moment)}, new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            DialogUtils.dismiss();
                            switch (position) {
                                case 0:
                                    MomentUtils.reportMoment(mContext, momentsBean);
                                    break;
                                case 1:
                                    blockThisMoment(mContext, momentsBean);
                                    break;
                                case 2:
                                    MomentBlackUtils.blackHerMoment(momentsBean.userId + "");
                                    break;

                                default:
                                    break;
                            }
                        }
                    }, true, 0, null);
                }
            }
        }
    }

    public static void showAdControlDialog(Context context, final MomentsBean momentsBean) {

        DialogUtils.showSelectionDialogWide((Activity) context, 3 / 4f, new String[]{"不感兴趣",
                "取消"}, new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                switch (position) {
                    case 0:
                        hideAdMoment(momentsBean);
                        break;
                    case 1:
                        break;
                }

                DialogUtils.dismiss();

            }
        }, false, 0, null);
    }


    public static String getReleaseTimeShow(String commentTime) {
        if (TextUtils.isEmpty(commentTime)) {
            return "";
        }
        String[] arr = commentTime.split("_");
        if (arr.length < 2) {
            return "";
        }
        if ("0".equals(arr[0])) {
            return TheLApp.getContext().getString(R.string.info_just_now);
        } else if ("1".equals(arr[0])) {
            return arr[1] + TheLApp.getContext().getString(R.string.info_seconds_ago);
        } else if ("2".equals(arr[0])) {
            return arr[1] + TheLApp.getContext().getString(R.string.info_minutes_ago);
        } else if ("3".equals(arr[0])) {
            return arr[1] + TheLApp.getContext().getString(R.string.info_hours_ago);
        } else if ("4".equals(arr[0])) {
            return arr[1] + TheLApp.getContext().getString(R.string.info_days_ago);
        } else if ("5".equals(arr[0])) {
            return arr[1];
        }
        return "";

    }

    public static String getStickerUrl(String msgText) {
        try {
            String img = new JSONObject(msgText).getString("img");
            if (!TextUtils.isEmpty(img)) {
                return img;
            }
        } catch (Exception e) {
            return "";
        }

        return "";
    }

    /**
     * 根据日志类型设置昵称
     *
     * @param textView
     * @param momentsBean
     */
    public static void setNicknameWithMomentType(TextView textView, MomentsBean momentsBean) {
        if (textView == null || momentsBean == null) {
            return;
        }
        SpannableString sp;
        final String nickName = momentsBean.nickname;
        if (TextUtils.isEmpty(momentsBean.momentsType)) {
            sp = buildThemeHeaderStr(nickName, "");
            textView.setText(sp);
            return;
        }
        if (MomentsBean.MOMENT_TYPE_THEME.equals(momentsBean.momentsType)) {//发布话题
            sp = buildThemeHeaderStr(nickName, TheLApp.getContext().getString(R.string.posted_a_theme));
        } else if (MomentsBean.MOMENT_TYPE_THEME_REPLY.equals(momentsBean.momentsType)) {//参与话题
            sp = buildThemeHeaderStr(nickName, TheLApp.getContext().getString(R.string.participated_a_theme));
        } else if (MomentsBean.MOMENT_TYPE_LIVE_USER.equals(momentsBean.momentsType)) {//推荐主播
            sp = buildThemeHeaderStr(nickName, TheLApp.getContext().getString(R.string.shared_a_anchor));
        } else if (MomentsBean.MOMENT_TYPE_USER_CARD.equals(momentsBean.momentsType)) {//推荐名片
            sp = buildThemeHeaderStr(nickName, TheLApp.getContext().getString(R.string.recommend_a_user) + " " + momentsBean.momentsText);
        } else if (MomentsBean.MOMENT_TYPE_WEB.equals(momentsBean.momentsType)) {//推荐网页
            sp = buildThemeHeaderStr(nickName, TheLApp.getContext().getString(R.string.shared_a_website));
        } else if (MomentsBean.MOMENT_TYPE_RECOMMEND.equals(momentsBean.momentsType)) {//推荐日志
            if (momentsBean.parentMoment == null || TextUtils.isEmpty(momentsBean.parentMoment.momentsType)) {
                sp = buildThemeHeaderStr(nickName, TheLApp.getContext().getString(R.string.recommend_a_moment));
                textView.setText(sp);
                return;
            }
            final MomentParentBean bean = momentsBean.parentMoment;
            if (MomentsBean.MOMENT_TYPE_THEME.equals(bean.momentsType)) {//推荐话题话题
                sp = buildThemeHeaderStr(nickName, TheLApp.getContext().getString(R.string.shared_a_topic));
            } else if (MomentsBean.MOMENT_TYPE_VOICE.equals(bean.momentsType) || MomentsBean.MOMENT_TYPE_TEXT_VOICE.equals(bean.momentsType)) {//推荐音乐
                sp = buildThemeHeaderStr(nickName, TheLApp.getContext().getString(R.string.shared_a_song));
            } else {
                sp = buildThemeHeaderStr(nickName, TheLApp.getContext().getString(R.string.recommend_a_moment));
            }
        } else {
            if (momentsBean.isCoverImage) {
                sp = buildThemeHeaderStr(nickName, TheLApp.getContext().getString(R.string.update_fengmian));
            } else
                sp = buildThemeHeaderStr(nickName, "");
        }
        textView.setText(sp);
    }

    public static SpannableString buildThemeHeaderStr(String nickName, String str) {
        SpannableString spannableString = new SpannableString(nickName + " " + str);
        spannableString.setSpan(new TextSpan(12, R.color.text_release_type), nickName.length(), nickName.length() + str.length() + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannableString;
    }

    /**
     * 4.0.0 由于网页日志的特殊性，所以要重新获取momentText
     *
     * @param moment
     */
    public static String getMomentTextFromMoment(MomentsBean moment) {
        if (null == moment || TextUtils.isEmpty(moment.momentsText)) {
            return "";
        }
        if (MomentsBean.MOMENT_TYPE_WEB.equals(moment.momentsType)) {
            try {
                final JSONObject obj = new JSONObject(moment.momentsText);
                RecommendWebBean bean = new RecommendWebBean();
                bean.fromJson(obj);
                return bean.momentsText;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return moment.momentsText;
    }

    public static Bitmap createUrlDrawble() {
        View view = LayoutInflater.from(TheLApp.getContext()).inflate(R.layout.layout_url_view, null);
        view.setDrawingCacheEnabled(true);
        view.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        return view.getDrawingCache();
    }

    /**
     * 从日志中获取到VideoBean
     *
     * @param momentBean
     * @return
     */
    public static VideoBean getVideoFromMoment(MomentsBean momentBean) {
        if (momentBean != null) {
            final VideoBean bean = new VideoBean();
            bean.id = momentBean.momentsId;
            bean.videoUrl = momentBean.videoUrl;
            bean.image = momentBean.thumbnailUrl;
            bean.pixelWidth = momentBean.pixelWidth;
            bean.pixelHeight = momentBean.pixelHeight;
            bean.playTime = momentBean.playTime;
            bean.playCount = momentBean.playCount;
            bean.text = momentBean.momentsText;
            bean.winkFlag = momentBean.winkFlag;
            bean.winkNum = momentBean.winkNum;
            bean.commentNum = momentBean.commentNum;
            return bean;
        }
        return null;
    }

    public static void likeMoment(String momentsId) {
        Map<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_MOMENTS_ID, momentsId);
        data.put(RequestConstants.I_WINK_COMMENT_TYPE, "8");
        Flowable<BaseDataBean> flowable = DefaultRequestService.createMomentRequestService().likeMoment(MD5Utils.generateSignatureForMap(data));
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>());
    }

    public static void unwinkMoment(String momentsId) {
        Map<String, String> map = new HashMap<>();
        map.put(RequestConstants.I_MOMENTS_ID, momentsId);

        Flowable<BaseDataBean> flowable = DefaultRequestService.createMomentRequestService().unwinkMoment(MD5Utils.generateSignatureForMap(map));
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>());
    }

    public static boolean isBlackOrBlock(String userId) {


        if (isBlack(userId)) {
            DialogUtil.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.info_had_add_to_black));
            return true;
        }
        if (isBlockMe(userId)) {
            DialogUtil.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.info_had_bean_added_to_black));
            return true;
        }
        return false;
    }

    /**
     * 是否屏蔽了我
     *
     * @param userId
     * @return
     */
    public static boolean isBlockMe(String userId) {
        if (TextUtils.isEmpty(userId)) {
            return false;
        }
        return ShareFileUtils.getString(ShareFileUtils.BLOCK_ME_LIST, "").contains("[" + userId + "]");
    }

    /**
     * 是否在我的黑名单（被我屏蔽）
     *
     * @param userId
     * @return
     */
    public static boolean isBlack(String userId) {
        if (TextUtils.isEmpty(userId)) {
            return false;
        }
        final List<String> blackList = Arrays.asList(SharedPrefUtils.getString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.BLACK_LIST, "").split(","));
        return blackList.contains(userId);
    }

    /**
     * 从日志中获取到VideoBean
     *
     * @param momentBean
     * @return
     */
    public static VideoBean getVideoFromMomentParent(MomentParentBean momentBean) {
        if (momentBean != null) {
            final VideoBean bean = new VideoBean();
            bean.id = momentBean.momentsId;
            bean.videoUrl = momentBean.videoUrl;
            bean.image = momentBean.thumbnailUrl;
            bean.pixelWidth = momentBean.pixelWidth;
            bean.pixelHeight = momentBean.pixelHeight;
            bean.playTime = momentBean.playTime;
            bean.playCount = momentBean.playCount;
            bean.text = momentBean.momentsText;
            bean.winkFlag = momentBean.winkFlag;
            bean.winkNum = momentBean.winkNum;
            bean.commentNum = momentBean.commentNum;
            bean.avatar = momentBean.avatar;
            bean.isFollow = momentBean.followerStatus;
            bean.nickname = momentBean.nickname;
            return bean;
        }
        return null;
    }

    /**
     * 从日志中获取RecommenWebBean
     *
     * @param bean
     * @return
     */
    public static RecommendWebBean getRecommendWebBeanFromMoment(MomentParentBean bean) {
        if (null == bean || !MomentsBean.MOMENT_TYPE_WEB.equals(bean.momentsType)) {
            return null;
        }
        JSONObject obj = new JSONObject();
        try {
            obj = new JSONObject(bean.momentsText);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RecommendWebBean recommendWebBean = new RecommendWebBean();
        recommendWebBean.fromJson(obj);
        return recommendWebBean;
    }

    /**
     * 把 推荐 的直播日志转换为普通日志
     *
     * @param liveUserMoment
     * @param imageUrl
     * @return
     */
    public static MomentsBean getMomentByLiveMoment(LiveUserMomentBean liveUserMoment, String imageUrl) {
        final MomentsBean momentsBean = new MomentsBean();

        if (liveUserMoment != null) {
            if (liveUserMoment.userId != null) {
                momentsBean.userId = Integer.parseInt(liveUserMoment.userId);
            }
            momentsBean.nickname = liveUserMoment.nickname;
            momentsBean.liveId = liveUserMoment.liveId;
            momentsBean.liveStatus = liveUserMoment.liveStatus;
            momentsBean.description = liveUserMoment.description;
            momentsBean.imageUrl = imageUrl;
            momentsBean.audioType = liveUserMoment.audioType;
            momentsBean.isLandscape = liveUserMoment.isLandscape;
        }

        return momentsBean;
    }

    public static void hideAdMoment(MomentsBean momentsBean) {

        if (momentsBean != null) {

            String oldJson = ShareFileUtils.getString(ShareFileUtils.AD_HIDE, "[]");

            Type typeOfDest = new TypeToken<List<String>>() {
            }.getType();

            final List<String> list = GsonUtils.getObjects(oldJson, typeOfDest);

            final String momentsId = momentsBean.momentsId;

            if (list.size() > 0) {
                for (String str : list) {
                    if (!momentsId.equals(str)) {
                        list.add(momentsId);
                    }
                }
            } else {
                list.add(momentsId);
            }

            String json = GsonUtils.createJsonString(list);
            L.d("MomentUtils", " json : " + json);

            ShareFileUtils.setString(ShareFileUtils.AD_HIDE, json);
        }

    }

    public static String getMomentToChat(String chatText, MomentsBean momentsBean) {

        MomentToChatBean momentToChatBean = new MomentToChatBean();

        if (momentsBean.imageUrl != null && momentsBean.imageUrl.length() > 0) {

            String[] imageUrls = momentsBean.imageUrl.split(",");

            momentToChatBean.imgUrl = imageUrls[0];

        }

        momentToChatBean.chatText = chatText;

        momentToChatBean.momentText = momentsBean.momentsText;

        momentToChatBean.momentId = momentsBean.momentsId;

        momentToChatBean.momentType = momentsBean.momentsType;

        return GsonUtils.createJsonString(momentToChatBean);

    }

}
