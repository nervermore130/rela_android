package com.thel.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.SurfaceTexture;
import android.net.Uri;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.ksyun.media.player.IMediaPlayer;
import com.ksyun.media.player.KSYMediaPlayer;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.video.VideoBean;
import com.thel.constants.TheLConstants;
import com.thel.modules.main.home.moments.SingleChoiseDialogAdapter;
import com.thel.modules.preview_image.SelectionDialogAdapter;
import com.thel.modules.select_image.SelectLocalImagesActivity;
import com.thel.ui.CustomDialog;
import com.thel.ui.adapter.SelectionDialogWithIconAdapter;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import video.com.relavideolibrary.RelaVideoSDK;
import video.com.relavideolibrary.onRelaVideoActivityResultListener;

/**
 * Created by chad
 * Time 17/9/20
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */

public class DialogUtil {
    private static final String TAG = DialogUtil.class.getSimpleName();

    private static long lastTime = 0; // 上次显示Toast的时间

    private static DialogUtil instance;
    private Dialog mDialog;

    public static DialogUtil getInstance() {
        if (instance == null) instance = new DialogUtil();
        return instance;
    }

    private static final int TOAST_INTERVAL_SHORT = 1000; // 短Toast显示间隔
    private static final int TOAST_INTERVAL_LONG = 3000; // 长Toast显示间隔

    /**
     * 创建确认对话框
     *
     * @param context
     * @param title
     * @param msg
     * @param textOK
     * @param textCancel
     * @param listerner
     */
    public static void showConfirmDialog(Context context, String title, String msg, String textOK, String textCancel, DialogInterface.OnClickListener... listerner) {

        if (context == null) {
            return;
        }

        if (context instanceof Activity) {
            if (((Activity) context).isFinishing() || ((Activity) context).isDestroyed()) {
                return;
            }
        }

        CustomDialog.Builder builder = new CustomDialog.Builder(context);
        builder.setMessage(msg);
        builder.setTitle(title);

        if (listerner == null) {
            return;
        }

        if (listerner.length > 0) {
            builder.setPositiveButton(textOK, listerner[0]);
            if (listerner.length > 1) {
                builder.setNegativeButton(textCancel, listerner[1]);
            } else {
                builder.setNegativeButton(textCancel, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
            }
        }

        builder.create().show();
    }

    /**
     * 创建确认对话框
     *
     * @param context
     * @param msg
     * @param textOK
     * @param textCancel
     * @param listerner
     */
    public static void showConfirmDialogNoTitle(Context context, String msg, String textOK, String textCancel, DialogInterface.OnClickListener... listerner) {

        if (context == null) {
            return;
        }

        CustomDialog.Builder builder = new CustomDialog.Builder(context);
        builder.setMessage(msg);

        if (listerner == null) {
            return;
        }

        if (listerner.length > 0) {
            builder.setPositiveButton(textOK, listerner[0]);
            if (listerner.length > 1) {
                builder.setNegativeButton(textCancel, listerner[1]);
            } else {
                builder.setNegativeButton(textCancel, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
            }
        }

        builder.create().show();

    }

    /**
     * 创建确认对话框
     *
     * @param context
     * @param title
     * @param msg
     * @param listerner
     */
    public static void showConfirmDialog(Activity context, String title, String msg, DialogInterface.OnClickListener... listerner) {
        if (context == null) {
            return;
        }
        showConfirmDialog(context, title, msg, "", "", listerner);
    }

    /**
     * 单选控件
     *
     * @param context
     * @param list
     * @param curPosition
     * @return
     */
    public Dialog showSingleChoiseDialog(Activity context, String title, List<String> list, Integer[] pics, List<String> tips, int curPosition, AdapterView.OnItemClickListener listener) {
        if (context == null) {
            return null;
        }

        mDialog = new Dialog(context, R.style.CustomDialogBottom);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.show();
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.gravity = Gravity.BOTTOM;
        lp.width = context.getResources().getDisplayMetrics().widthPixels; // 设置宽度
        mDialog.getWindow().setAttributes(lp);

        LayoutInflater flater = LayoutInflater.from(context);
        View view = flater.inflate(R.layout.single_choice_dialog, null);
        mDialog.setContentView(view);

        final LinearLayout lin_next = view.findViewById(R.id.lin_next);
        lin_next.setVisibility(View.GONE);
        ListView listview = view.findViewById(R.id.listview);

        final SingleChoiseDialogAdapter adapter = new SingleChoiseDialogAdapter(list, pics, tips, curPosition);
        listview.setAdapter(adapter);
        listview.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listview.setSelection(curPosition);
        listview.setOnItemClickListener(listener);

        if (!TextUtils.isEmpty(title)) {
            TextView txt_title = view.findViewById(R.id.txt_title);
            txt_title.setText(title);
        }

        return mDialog;
    }

    /**
     * 自定义选择控件
     *
     * @param context
     * @param array    选项字符串数组
     * @param listener
     * @return
     */
    public void showSelectionDialog(Activity context, String[] array, AdapterView.OnItemClickListener listener, boolean setLastItemColor, int setLastItem, DialogInterface.OnDismissListener dismissListener) {
        if (context == null) {
            return;
        }
        mDialog = new Dialog(context, R.style.CustomDialog);
        mDialog.setCancelable(true);
        mDialog.setCanceledOnTouchOutside(true);
        if (dismissListener != null) {
            mDialog.setOnDismissListener(dismissListener);
        }
        mDialog.show();
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.gravity = Gravity.CENTER;
        lp.width = context.getResources().getDisplayMetrics().widthPixels / 2; // 设置宽度
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        mDialog.getWindow().setAttributes(lp);

        LayoutInflater flater = LayoutInflater.from(context);
        View view = flater.inflate(R.layout.selection_dialog, null);
        mDialog.setContentView(view);

        ListView listview = view.findViewById(R.id.listview);

        final SelectionDialogAdapter adapter = new SelectionDialogAdapter(array, setLastItemColor, setLastItem);
        listview.setAdapter(adapter);
        listview.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listview.setOnItemClickListener(listener);
    }

    public void closeDialog() {
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
        }
    }

    /**
     * 显示 短提示
     *
     * @param context
     * @param text
     */
    public static void showToastShort(Context context, String text) {

        try {
            if (context == null || TextUtils.isEmpty(text)) {
                return;
            }

            Toast toast = Toast.makeText(context, text, Toast.LENGTH_SHORT);

            if (lastTime == 0) {
                toast.show();
                lastTime = System.currentTimeMillis();
            }
            long thisTime = System.currentTimeMillis();
            if (thisTime - lastTime >= TOAST_INTERVAL_SHORT) {
                toast.show();
                lastTime = thisTime;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 显示 短提示 居中显示
     *
     * @param context
     * @param text
     */
    public static void showToastShortGravityCenter(Context context, String text) {

        try {
            if (context == null) {
                return;
            }

            Toast toast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            if (lastTime == 0) {
                toast.show();
                lastTime = System.currentTimeMillis();
            }
            long thisTime = System.currentTimeMillis();
            if (thisTime - lastTime >= TOAST_INTERVAL_SHORT) {
                toast.show();
                lastTime = thisTime;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 日期选择控件
     *
     * @param minDate -1 不限制，>=0 限制
     * @param maxDate -1 不限制，>=0 限制
     * @param context
     * @return
     */
    public void showDatePicker(Activity context, String title, Calendar initTime, long minDate, long maxDate, boolean showTip, View.OnClickListener listener) {
        if (context == null) {
            return;
        }

        mDialog = new Dialog(context, R.style.CustomDialogBottom);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.show();
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.gravity = Gravity.BOTTOM;
        lp.width = context.getResources().getDisplayMetrics().widthPixels; // 设置宽度
        mDialog.getWindow().setAttributes(lp);

        LayoutInflater flater = LayoutInflater.from(context);
        View view = flater.inflate(R.layout.date_picker_dialog, null);
        if (!TextUtils.isEmpty(title))
            ((TextView) view.findViewById(R.id.txt_title)).setText(title);
        view.findViewById(R.id.txt_tip).setVisibility(showTip ? View.VISIBLE : View.GONE);
        mDialog.setContentView(view);

        DatePicker datePicker = view.findViewById(R.id.datepicker);
        try {
            if (null != initTime) {
                datePicker.init(initTime.get(Calendar.YEAR), initTime.get(Calendar.MONTH), initTime.get(Calendar.DAY_OF_MONTH), null);
            }
            if (minDate != -1) {
                datePicker.setMinDate(minDate);
            }
            if (maxDate != -1) {
                datePicker.setMaxDate(maxDate);
            }
        } catch (Exception e) {
        }

        TextView lin_done = view.findViewById(R.id.lin_done);
        lin_done.setTag(datePicker);

        lin_done.setOnClickListener(listener);
        view.findViewById(R.id.txt_title).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
    }

    /**
     * 创建提示 对话框
     *
     * @param context
     * @param title
     * @param msg
     * @param listerner
     */
    public static void showAlert(Context context, String title, String msg, DialogInterface.OnClickListener listerner) {
        if (context == null) {
            return;
        }
        CustomDialog.Builder builder = new CustomDialog.Builder(context);
        builder.setMessage(msg);
        builder.setTitle(title);
        builder.setPositiveButton(android.R.string.ok, listerner);
        try {
            builder.create().show();
        } catch (Exception e) {
            L.e(TAG, e.getMessage());
        }
    }

    /**
     * 自定义选择控件(带图片icon)
     *
     * @param context
     * @param array    选项字符串数组
     * @param icons    文字前面的icon id列表
     * @param listener
     * @return
     */
    public void showSelectionDialogWithIcon(Activity context, String title, String[] array, int[] icons, AdapterView.OnItemClickListener listener, boolean setLastItemColor, DialogInterface.OnDismissListener dismissListener) {
        if (context == null) {
            return;
        }
        mDialog = new Dialog(context, R.style.CustomDialog);
        mDialog.setCancelable(true);
        mDialog.setCanceledOnTouchOutside(true);
        if (dismissListener != null) {
            mDialog.setOnDismissListener(dismissListener);
        }
        mDialog.show();
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.gravity = Gravity.CENTER;
        lp.width = context.getResources().getDisplayMetrics().widthPixels * 2 / 3; // 设置宽度
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        mDialog.getWindow().setAttributes(lp);

        LayoutInflater flater = LayoutInflater.from(context);
        View view = flater.inflate(R.layout.selection_dialog, null);
        if (!TextUtils.isEmpty(title)) {
            TextView t = view.findViewById(R.id.title);
            t.setVisibility(View.VISIBLE);
            t.setText(title);
            view.findViewById(R.id.divider).setVisibility(View.VISIBLE);
        }
        mDialog.setContentView(view);

        ListView listview = view.findViewById(R.id.listview);

        final SelectionDialogWithIconAdapter adapter = new SelectionDialogWithIconAdapter(array, icons, setLastItemColor);
        listview.setAdapter(adapter);
        listview.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listview.setOnItemClickListener(listener);
    }

    /**
     * 创建提示 对话框
     *
     * @param context
     * @param title
     * @param msg
     * @param buttons
     */
    public static void showMultiButtonAlert(Context context, String title, String msg, HashMap<String, DialogInterface.OnClickListener> buttons) {
        if (context == null) {
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setTitle(title);
        builder.setMessage(msg);
        Iterator<String> iterator = buttons.keySet().iterator();
        int i = 0;
        while (iterator.hasNext()) {
            String key = iterator.next();
            if (i == 0)
                builder.setNeutralButton(key, buttons.get(key));
            else if (i == 1)
                builder.setPositiveButton(key, buttons.get(key));
            else if (i == 2)
                builder.setNegativeButton(key, buttons.get(key));
            i++;
        }
        try {
            builder.show();
        } catch (Exception e) {
            L.d(TAG, e.getMessage());
        }
    }

    /**
     * 显示 短提示
     *
     * @param context
     * @param text
     * @param x
     * @param y
     */
    public static void showMyToastShort(Context context, SpannableString text, int gravity, int x, int y) {
        if (context == null) {
            return;
        }
        if (!BusinessUtils.isAppOnForeground()) {
            return;
        }
        LayoutInflater inflate = LayoutInflater.from(context);
        View toastRoot = inflate.inflate(R.layout.toast_layout, null);
        TextView message = toastRoot.findViewById(R.id.message);
        message.setText(text);

        Toast toast = new Toast(context);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setGravity(gravity, x, y);
        toast.setView(toastRoot);

        if (lastTime == 0) {
            toast.show();
            lastTime = System.currentTimeMillis();
        }
        long thisTime = System.currentTimeMillis();
        if (thisTime - lastTime >= TOAST_INTERVAL_SHORT) {
            toast.show();
            lastTime = thisTime;
        }
    }

    /**
     * 发布日志选择框 activity.startActivityForResult
     */
    public static void showThemeReplyCommentTypeDialog(final Activity context, final int picLimit) {
        if (context == null) {
            return;
        }
        final Dialog dialog = new Dialog(context, R.style.CustomDialog);
        dialog.show();

        LayoutInflater flater = LayoutInflater.from(context);
        View view = flater.inflate(R.layout.theme_reply_comment_type_dialog, null);
        dialog.setContentView(view);
        dialog.setCanceledOnTouchOutside(true);

        final LinearLayout lin_album = view.findViewById(R.id.lin_album);
        final LinearLayout lin_video = view.findViewById(R.id.lin_video);
        // final LinearLayout lin_music = (LinearLayout) view.findViewById(R.id.lin_music);

        // 发布图片日志
        lin_album.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (picLimit == 0) {
                    DialogUtil.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.Select_image_));
                    dialog.dismiss();
                }
                ViewUtils.preventViewMultipleClick(v, 2000);
                Intent intent = new Intent(context, SelectLocalImagesActivity.class);
                intent.putExtra(TheLConstants.BUNDLE_KEY_SELECT_AMOUNT, picLimit);
                context.startActivityForResult(intent, TheLConstants.BUNDLE_CODE_WRITE_MOMENT_ACTIVITY);
                dialog.dismiss();
            }
        });

        // 发布视频日志
        lin_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                PermissionUtil.requestCameraPermission(context, new PermissionUtil.PermissionCallback() {
                    @Override
                    public void granted() {
                        PermissionUtil.requestStoragePermission(context, new PermissionUtil.PermissionCallback() {
                            @Override
                            public void granted() {
                                if (context instanceof onRelaVideoActivityResultListener) {
                                    onRelaVideoActivityResultListener resultListener = (onRelaVideoActivityResultListener) context;
                                    RelaVideoSDK.startVideoGalleryActivity(context, resultListener);
                                }
                            }

                            @Override
                            public void denied() {
                                dialog.dismiss();
                            }

                            @Override
                            public void gotoSetting() {
                                dialog.dismiss();
                            }
                        });
                    }

                    @Override
                    public void denied() {
                        dialog.dismiss();
                    }

                    @Override
                    public void gotoSetting() {
                        dialog.dismiss();
                    }
                });
                dialog.dismiss();
            }
        });
    }

    /**
     * 全屏播放视频
     *
     * @param context
     * @param videoUrl
     * @param thumbnail
     * @param momentsId 日志视频的话传日志id，日志id分两种：网络日志或预发布日志；聊天视频的话，是发出的消息，则传空字符串，是收到的消息，则传"1"
     */
    public void showPlayVideoDialog(Activity context, final String videoUrl, String thumbnail, final String momentsId, VideoBean... bean) {
        if (context == null) {
            return;
        }
        mDialog = new Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        mDialog.setCancelable(true);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.show();
        L.d(TAG, " showPlayVideoDialog : " + thumbnail);
        final KSYMediaPlayer player = new KSYMediaPlayer.Builder(context).build();

        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.gravity = Gravity.CENTER;
        lp.width = AppInit.displayMetrics.widthPixels;
        lp.height = AppInit.displayMetrics.heightPixels;
        lp.dimAmount = 0;
        mDialog.getWindow().setAttributes(lp);

        LayoutInflater flater = LayoutInflater.from(context);
        View view = flater.inflate(R.layout.video_dialog, null);
        mDialog.setContentView(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDialog();

                if (player != null) {
                    player.release();
                }
            }
        });


        final TextureView videoView = view.findViewById(R.id.video);
        final SimpleDraweeView img_cover = view.findViewById(R.id.img_cover);
        final RelativeLayout rel_parent = view.findViewById(R.id.rel_parent);
        final TextView txt_info = view.findViewById(R.id.txt_info);
        // 加载213dp尺寸的图，因为外面的列表已经加载出来了
        img_cover.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(thumbnail, TheLApp.getContext().getResources().getDimension(R.dimen.moment_pic_thumbnail_big), TheLApp.getContext().getResources().getDimension(R.dimen.moment_pic_thumbnail_big))));
        final ProgressBar progressbar_video = view.findViewById(R.id.progressbar_video);
        final ProgressBar playerProgress = view.findViewById(R.id.playerProgress);
        progressbar_video.setVisibility(View.VISIBLE);

        if (bean.length > 0) {
            final VideoBean mVideobean = bean[0];
            L.d(TAG, " bean : " + mVideobean);
            final RelativeLayout.LayoutParams txt_info_param = (RelativeLayout.LayoutParams) txt_info.getLayoutParams();
            final LinearLayout.LayoutParams player_progress_param = (LinearLayout.LayoutParams) playerProgress.getLayoutParams();
            //如果宽*1.5f以后依然比高小，就认为是竖屏视频
            if (mVideobean.pixelWidth * 1.5f < mVideobean.pixelHeight) {
                txt_info_param.topMargin = Utils.dip2px(TheLApp.getContext(), -25);
                player_progress_param.topMargin = Utils.dip2px(TheLApp.getContext(), -4);
            } else {
                txt_info_param.topMargin = Utils.dip2px(TheLApp.getContext(), 25);
                player_progress_param.topMargin = 0;
            }
            final String txt = mVideobean.playTime + "s / " + TheLApp.getContext().getString(R.string.play_count, mVideobean.playCount);
            txt_info.setText(txt);
        }
        final int size = AppInit.displayMetrics.widthPixels;
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(size, size);
        params.addRule(RelativeLayout.CENTER_IN_PARENT);
        img_cover.setLayoutParams(params);

        videoView.setSurfaceTextureListener(new TextureView.SurfaceTextureListener() {

            @Override
            public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {

                L.d(TAG, " onSurfaceTextureAvailable : ");

                L.d(TAG, " momentsId : " + momentsId);

                L.d(TAG, " surface : " + surface);

                // 预发布日志的话是本地视频，直接播放；聊天视频如果是发出的话momentId为空，也播放本地视频
                if (surface != null && videoUrl != null && videoUrl.length() > 0) {

                    L.d(TAG, " ------1------ : ");

                    try {
                        player.setSurface(new Surface(surface));
                        player.setDataSource(videoUrl);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    player.prepareAsync();
                }

            }

            @Override
            public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

                L.d(TAG, " onSurfaceTextureSizeChanged : ");

            }

            @Override
            public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {

                L.d(TAG, " onSurfaceTextureDestroyed : ");

                return false;
            }

            @Override
            public void onSurfaceTextureUpdated(SurfaceTexture surface) {

                L.d(TAG, " onSurfaceTextureUpdated : ");

            }
        });

        player.setOnPreparedListener(new IMediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(IMediaPlayer iMediaPlayer) {

                L.d(TAG, " ------onPrepared------ : ");
                player.setVideoScalingMode(KSYMediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT);
                progressbar_video.setVisibility(View.GONE);
                img_cover.setVisibility(View.GONE);
                player.start();
            }
        });
        player.setOnCompletionListener(new IMediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(IMediaPlayer iMediaPlayer) {
                // 预发布日志播放不统计
                if (!TextUtils.isEmpty(momentsId) && !momentsId.contains(MomentsBean.SEND_MOMENT_FLAG)) {
                    VideoUtils.saveVideoReport(momentsId, (int) player.getDuration(), 1);
                }
                player.seekTo(0);
                player.start();
            }
        });

        mDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mDialog = null;
            }
        });

        final android.os.Handler handler = new android.os.Handler();
        final Runnable run = new Runnable() {
            @Override
            public void run() {
                if (mDialog != null) {
                    playerProgress.setMax((int) player.getDuration());
                    playerProgress.setProgress((int) player.getCurrentPosition());
                    handler.postDelayed(this, 10);
                }
            }
        };
        handler.post(run);
    }

    /**
     * 显示 长提示
     *
     * @param context
     * @param text
     */
    public static void showToastLong(Context context, String text) {
        // LayoutInflater inflate = LayoutInflater.from(context);
        // View toastRoot = inflate.inflate(R.layout.toast_layout, null);
        // TextView message = (TextView) toastRoot.findViewById(R.id.message);
        // message.setText(text);

        // Toast toast = new Toast(context);
        // toast.setDuration(Toast.LENGTH_SHORT);
        // toast.setGravity(Gravity.CENTER, 0, 0);
        // toast.setView(toastRoot);

        if (context == null) {
            return;
        }
        // 如果页面不在前台，则不show toast
        if (!BusinessUtils.isAppOnForeground()) {
            return;
        }
        Toast toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
        if (lastTime == 0) {
            toast.show();
            lastTime = System.currentTimeMillis();
        }
        long thisTime = System.currentTimeMillis();
        if (thisTime - lastTime >= TOAST_INTERVAL_LONG) {
            toast.show();
            lastTime = thisTime;
        }
    }

}
