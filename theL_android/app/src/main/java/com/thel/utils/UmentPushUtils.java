package com.thel.utils;

import android.content.Context;

import com.thel.app.TheLApp;
import com.umeng.analytics.MobclickAgent;

public class UmentPushUtils {

    public static void onMsgEvent(Context context, String s1, String s2) {

        L.d("UmengPushUtils", " NetworkUtils.isNetworkConnected(TheLApp.context) : " + NetworkUtils.isNetworkConnected(TheLApp.context));

        if (NetworkUtils.isNetworkConnected(TheLApp.context)) {

            MobclickAgent.onEvent(context, s1, s2);

        }

    }

    public static void onMsgEvent(Context context, String s1) {

        if (NetworkUtils.isNetworkConnected(TheLApp.context)) {

            MobclickAgent.onEvent(context, s1);
        }
    }

}
