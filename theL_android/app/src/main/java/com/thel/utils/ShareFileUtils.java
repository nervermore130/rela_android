package com.thel.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.SparseArray;

import com.tencent.bugly.crashreport.CrashReport;
import com.thel.BuildConfig;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.db.table.message.UserInfoTable;
import com.thel.modules.main.me.bean.MyInfoBean;
import com.thel.modules.main.me.bean.RoleBean;
import com.thel.network.RequestConstants;
import com.thel.network.api.loginapi.bean.SignInBean;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;
import java.util.Locale;

/**
 * SharedPreferences的工具类
 *
 * @author zhangwenjia
 */
public class
ShareFileUtils {

    private static SharedPreferences pref; // 文件操作对象

    private static final String SHARED_FILE_NAME = "the_l"; // sharefile 文件名

    // 假装情侣/热拉游乐园
    public static class PerfectMatch {
        public static final String IS_FIRST_OPEN = "pm_is_first_open";// 用户首次登陆app

    }

    // share中的字段名
    public static final String HAS_LOGGED = "has_logged";// 是否登录
    public static final String KEY = "key";
    public static final String ID = "id";
    public static final String Brocaster_ID = "Brocaster_ID";   //直播间主播的id
    public static final String USER_NAME = "name";
    public static final String USER_THEL_ID = "thelID";
    public static final String USER_BACKGROUND = "user_background";
    public static final String LONGITUDE = "longitude";
    public static final String LATITUDE = "latitude";
    public static final String AVATAR = "avatar";
    public static final String EMAIL = "email";
    public static final String MESSAGE_USER = "message_user";
    public static final String MESSAGE_PASSWORD = "message_password";
    public static final String BIND_CELL = "cell";
    public static final String BIND_WX = "wx";
    public static final String BIND_FB = "fb";
    public static final String BIND_SINA = "sina";
    public static final String rolePosition = "rolePosition";
    public static final String CITY = "city";
    public static final String FIRSTREGISTER = "firstRegister";
    public static final String USER_LEVEL = "user_level";
    public static final String SUM_LIKE_COUNT = "sumLikeCount";
    public static final String NOT_REPLY_MECOUNT = "notReplyMeCount";

    public static final String USER_AGE = "user_age";
    public static final String PERM = "perm";
    // 附近用户列表页字段
    public static final String NEAR_BY_USER = "near_by_user";
    public static final String NEAR_BY_USER_TOTAL = "near_by_user_total";
    public static final String NEAR_BY_USER_TIMESTAMP = "near_by_user_timestamp";
    // 0:瀑布流 1:列表
    //    public static final String NEAR_BY_UI_STYLE = "near_by_ui_style";
    // 朋友表字段
    public static final String FRIENDS_FRIENDS_TOTAL = "friends_friends_total";
    public static final String FRIENDS_FOLLOW_TOTAL = "friends_follow_total";
    public static final String FRIENDS_FANS_TOTAL = "friends_fans_total";
    public static final String FRIENDS_LIKED_TOTAL = "friends_liked_total";

    // 更新用户信息
    public static final String BIRTHDAY = "birthday";//年龄
    public static final String HEIGHT = "height";// 身高
    public static final String WEIGHT = "weight";// 体重
    public static final String ROLE_NAME = "role_name";// 角色
    public static final String AFFECTION = "affection";// 感情状态
    public static final String PURPOSE = "purpose";// 交友目的
    public static final String WANT_ROLE = "want_role";// 寻找角色
    public static final String PROFESSIONAL_TYPES = "professional_types"; // 职业类型
    public static final String OUT_LEVEL = "out_level";// 出柜程度
    public static final String ETHNICITY = "ethnicity";// 种族
    public static final String OCCUPATION = "occupation";// 职业
    public static final String LIVECITY = "livecity";// 居住地
    public static final String TRAVELCITY = "travelcity";// 旅行地
    public static final String LOCATION = "location";// 活动范围
    public static final String MOVIE = "movie";// 影视剧
    public static final String MUSIC = "music";// 音乐
    public static final String BOOKS = "books";// 书籍
    public static final String FOOD = "food";// 食物
    public static final String OTHERS = "others";// 其他爱好
    public static final String INTRO = "intro";// 自我介绍
    public static final String INTERESTS = "interests";//其它爱好4.4.0
    // 更新用户信息
    public static final String HEIGHT_UNITS = "height_units";
    public static final String WEIGHT_UNITS = "weight_units";

    // 消息提醒声音，默认是打开的
    public static final String MESSAGE_SOUND = "message_sound";
    public static final String MESSAGE_REDPOINT = "message_redpoint";
    // 动态提醒声音，默认是关闭的
    public static final String ACTIVITY_SOUND = "activity_sound";

    // wifi下自动播放视频
    public static final String AUTO_PLAY_WIFI = "auto_play_wifi";
    // 3G/4G下自动播放视频
    public static final String AUTO_PLAY_MOBILE = "auto_play_mobile";

    //表情包id
    public static final String STICKER_STORE_ID = "STICKER_STORE_ID";
    //表情包filename
    public static final String STICKER = "stickers";
    //4.5.0资料完成度
    public static final String RATIO = "ratio";
    public static final String Level = "level"; //缓存等级
    public static final String NO_RATIO = "no_ratio";
    //购买会员订单信息
    public static final String BUY_VIP_ODER_ID = "oder_id";
    /**
     * 4.1.0新增push
     */
    public static final String LIVE_PUSH = "live";//直播推送
    public static final String commentTextPush = "commentText";
    public static final String commentReplyPush = "commentReply";
    public static final String commentWinkPush = "commentWink";//日志点赞
    public static final String followerPush = "follower";
    public static final String winkPush = "wink";
    public static final String messagePush = "message";
    public static final String videoPush = "video";
    public static final String themePush = "theme";
    public static final String recordPush = "view";//来访记录
    public static final String superLike = "superLike";//匹配超级喜欢
    public static final String perfectMatch = "perfectMatch";//完美匹配
    public static final String await = "await";//期待开播

    public static final String exitMatch = "EXITMATCH";//退出匹配 客户端加一层保护


    public static final String likeMe = "likeMe";//
    /**
     * 4.7.5 修改等级设置
     */
    public static final String LEVEL_SETTING_ENTRYLIVE = "level_setting_entrylive";
    public static final String USER_LEVEL_SETTING = "user_level_setting";

    public static final String MATCH_LIKE_ME_NEW_CURSOR = "match_like_me_new_cursor";
    public static final String MATCH_LIKE_ME_OLD_CURSOR = "match_like_me_old_cursor";

    public static final String MATCH_LIKE_ME_LIST_CURSOR = "match_like_me_list_cursor";

    public static final String MATCH_SAW_LIKE_ME = "match_saw_like_me";

    /**
     * 是否打开开关
     */
    public static final String FIRST_RECORD_CLOSE = "first_record_close";

    /**
     * 无痕浏览新功能引导
     */
    public static final String CLOSE_NONYMOUS_BROWSING = "close_nonymous_browsing";

    // 谁屏蔽了我
    public static final String BLOCK_ME_LIST = "block_me_list";

    // 登陆账号联想
    public static final String ACCOUNT_AUTO_COMPLETE = "account_auto_complete";

    // 附近页面头部提示双击回到顶部的提示是否显示过了
    public static final String IS_GO_TO_TOP_TIP_SHOWED = "is_go_to_top_showed";

    // 检查moments的任务是否开始
    public static final String HAS_CHECK_MOMENTS_SERVICE_STARTED = "hasCheckMomentsServiceStarted";

    // 未读的朋友圈消息
    public static final String UNREAD_MOMENTS_MSG_NUM = "unreadMomentsMsgNum";
    //朋友圈是否有消息
    public static final String MOMENTS_MSG_NUM = "MomentsMsgNum";

    public static final String NEW_CHECK = "newCheckshow";
    //新来访数量
    public static final String NEW_CHECK_NUM = "checkNewNum";

    //新喜欢数量
    public static final String NEW_LIKE_NUM = "checkNewNum";

    //匹配设置红点提示
    public static final String NEW_MATCH_SETTINGS = "new_match_settings";

    // 发过提醒的新动态的标识(发评论人的用户名)
    public static final String NOTIFIED_ACTIVITY_NICKNAME = "notified_act_nickname";
    // 发过提醒的新动态的标识(类型)
    public static final String NOTIFIED_ACTIVITY_TYPE = "notified_act_type";
    // 发过提醒的新动态的标识(评论内容)
    public static final String NOTIFIED_ACTIVITY_COMMENT_TEXT = "notified_act_commentText";

    // 软键盘的高度，聊天界面用到
    public static final String SOFT_KEYBOARD_HEIGHT = "soft_keyboard_height";
    // 是否需要完善个人资料
    public static final String NEED_COMPLETE_USER_INFO = "need_complete_user_info";
    // 必须完成个人资料才能进行一些操作
    public static final String MUST_COMPLETE_USER_INFO = "must_complete_user_info";

    public static final String DEVICE_ID = "device_id";

    // 是否第一次进入匹配功能
    public static final String IS_FIRST_MATCH = "is_first_match";
    public static final String MATCH_NEXT_TIME = "match_next_time";
    public static final String LAST_COUNT_DOWN = "last_count_down";
    public static final String MATCH_FIRST_UNLIKE = "first_unlike";
    public static final String MATCH_FIRST_SUPER_UNLIKE = "first_super_unlike";
    public static final String MATCH_ME_GUIDE_FIRST = "match_me_guide_first";

    public static final String MATCH_FIRST_LIKE = "first_like";
    public static final String MATCH_NEW_LIKE_ME_COUNT = "new_like_me_count";
    //点击我的系统设置提示手机绑定遮罩
    public static final String IS_FIRST_SHOW_MASK = "is_first_show_mask";

    public static final String IS_FIRST_LOGIN = "is_first_login";

    public static final String WX_LOGIN_TOKEN = "wx_login_token";

    public static final String WX_LOGIN_TIME = "wx_login_time";

    public static final String WX_LOGIN_USER_INFO = "wx.login_user_info";

    public static final String AD_HIDE = "ad_hide";

    public static final String GIFT_ETAG = "etag";

    public static final String AR_GIFT_ETAG = "ar_etag";

    public static final String GIFT_LIST = "gift_list";

    public static final String AR_GIFT_LIST = "ar_gift_list";

    public static final String IS_FIRST_START = "isFirstStart";

    public static final String IS_JUMP_UPDATE_USERINFO_ACTIVITY = "is_jump_update_userinfo_activity";

    public static final String IS_RELEASE_MOMENT = "is_release_moment";

    public static final String HTTP_URL = "http_url";

    public static final String HTTP_HOST = "http_host";

    public static final String IM_HOST = "im_host";

    public static final String FLUTTER_PROXY = "flutter_proxy";

    public static final String FLUTTER_PROXY_OPEN = "flutter_proxy_open";

    public static final String MY_ID_CARD = "my_id_card";

    public static final String MY_REAL_NAME = "my_real_name";

    public static final String CERTIFICATION_TYPE = "cerrification_type";
    /***是否需要新手引导***/

    public static final String LIVE_GIFT_GIFT_LIAN = "live_gift_gift_lian";//直播间观看连击礼物提示

    public static final String MATCH_UPDATE_USERINFO_GUIDE = "match_update_userinfo_guide";// 匹配页面修改资料提示弹窗

    public static final String MY_MATCH_GUIDE = "match_me_guide_new_settings";//我的热拉速配新手引导

    public static final String IS_SHOW_GUIDE_LAYOUT = "is_show_guide_layout";

    public static final String CHAT_USER_ID = "chat_user_id";

    public static final String PUSH_CACHE_DATA = "push_cache_data";

    public static final String IS_FIRST_TRANSFER_DATA = "is_first_transfer_data";

    public static final String IS_SECOND_TRANSFER_DATA = "is_second_transfer_data";

    public static final String needShowUpdateView = "needShowUpdateView";

    public static final String IS_FIRST_SHOW_PUSH_DIALOG = "is_first_show_push_dialog";

    public static final String IS_FIRST_SHOW_MOMENT_TO_CHAT = "is_first_show_moment_to_chat";

    public static final String SHOW_REQUEST_PERMISSTION_RATIONALE = "showRequestPermisstionRationale";

    public static final String CHAT_IP = "chat_ip";

    public static final String SHOW_DRAG_PHOTO_GUIDE = "show_drag_photo_guide";

    public static final String MSG_AUTH_TOKEN = "msg_auth_token";

    public static final String USER_VIP_LEVEL = "user_vip_level";

    public static final String FAST_GIFT_TIP = "has_fast_gift_tip";

    public static final String FAST_GIFT_ID_NEW = "fast_gift_id_new"; //后台返回了新的快捷礼物id 保存本地


    /**
     * 悄悄查看消息
     */
    public static final String MSG_HIDING = "msg_hiding";

    public static final String MSG_HIDING_CLOSE_TIME = "msg_hiding_close_time";

    public static final String VIP_INCOGNITO = "incognito";

    /**
     * 是否显示悄悄查看
     */
    public static final String IS_CLOSE_MSG_HIDING = "is_close_msg_hiding";

    public static final String TIME_MACHANE_NOT_READ = "时光机有未读信息";

    public static final String LIVE_NOTICE_EDIT = "发布直播提示";

    public static final String LIVE_NOTICE_SHOW = "观看直播提示";

    /**
     * 观看直播默认排序
     */
    public static final String LIVE_URL_SORT = "liveUrlSort";

    /**
     * 主播推流切换默认排序
     */
    public static final String RTMP_URL_SORT = "rtmpUrlSort";

    /**
     * 消息界面的推送提示
     */
    public static final String PUSH_TIPS_CLOSE_TIME = "push_tips_close_time";

    /**
     * 是否关注过用户
     */
    public static final String IS_FOLLOW_USER = "is_follow_user";

    public static final String IS_HOME_PAGE_GUIDE_SHOWED = "is_home_page_guide_showed";

    /**
     * 超级喜欢卡个数
     */
    public static final String SUPER_LIKE = "super_like";

    /**
     * 是否显示个人主页的聊天引导
     */
    public static final String IS_SHOW_USER_INFO_CHAT_GUIDE = "is_show_user_info_chat_guide";

    /**
     * 个人主页的聊天引导
     */
    public static final String USER_INFO_CHAT_GUIDE = "user_info_chat_guide";

    /**
     * 存储上报失败的直播间日志
     */
    public static final String LIVE_LOG = "live_log";

    /**
     * 存储匹配上报失败的日志
     */
    public static final String MATCH_LOG = "match_log";

    /**
     * 直播间取消关注提示
     */
    public static final String CHAT_UN_FOLLOW_TIPS = "chat_un_follow_tips";

    /**
     * 直播间取消关注提示
     */
    public static final String CHAT_MATCH_FOLLOW_TIPS = "chat_match_follow_tips";

    /**
     * 悄悄关注引导
     */
    public static final String SECRETLY_FOLLOW = "secretly_follow";
    /**
     * 来源页面
     */
    public static final String MATCH_FROM_PAGE = "match_from_page";
    /**
     * 来源页面Id
     */
    public static final String MATCH_FROM_PAGE_ID = "match_from_page_id";

    /**
     * 当前页面Id
     */
    public static final String MATCH_CURRENT_PAGE_ID = "match_current_page_id";

    /**
     * 广告
     */
    public static final String ADS = "ads";

    /**
     * 我的页面广告
     */
    public static final String ME_ADS = "me_ads";
    /**
     * 我的页面广告Id
     */
    public static final String ME_ADS_ID = "me_ads_id";

    /**
     * 发布直播的标题
     */
    public static final String RELEASED_LIVE_TITLE = "released_live_title";

    /**
     * 发布直播的图片
     */
    public static final String RELEASED_LIVE_IMAGE = "released_live_image";

    /**
     * 附近的人封面引导
     */
    public static final String NEARBY_COVER_GUIDE = "nearby_cover_guide";

    /**
     * 附近筛选引导
     */
    public static final String NEARBY_FILTER_GUIDE = "nearby_filter_guide";

    /**
     * 友盟推送的token
     */
    public static final String UMENG_TOKEN = "umeng_token";

    /**
     * 直播间观看端，快捷礼物弹窗提示
     */
    public static final String FAST_GIFT_NO_REMINDER = "fast_gift_no_reminder";
    /**
     * 贴纸礼物提示
     */
    public static final String STICKER_TIPS = "sticker_visible";

    /**
     * 附近引导
     */
    public static final String NEARBY_PEOPLE_GUIDE = "nearby_people_guide";
    /**
     * 新版支持和“守护”们发起连麦啦！
     */
    public static final String LiNK_MIC_TIPS = "link_mic_tips";
    public static final String TU_SDK_MASTER_KEY = "tuSDKMasterKey";

    public static final String IS_NEW_USER = "isNewUser";

    public static final String homePageId = "homePageId";
    public static final String homePage = "friendmoments";
    public static final String rootSwitchPage = "rootSwitchPage";
    public static final String rootSwitchPageId = "rootSwitchPageId";
    public static final String nearPeoplePage = "nearPeoplePage";
    public static final String nearPeoplePageId = "nearPeoplePageId";

    public static final String nearMoments = "nearMoments";

    public static final String HTTPS_SWITCH = "httpsSwitch";

    public static final String LEVEL_PRIVILEGES = "levelPrivileges";

    public static final String LOCATION_STATUS = "location_status";

    public static final String PUSH_MSG = "push_msg";
    public static final String AMUSEMENT_PARK_UNREAD_COUNT_DATE = "amusement_park_unread_count_date";//未读数增加日期

    public static final String IS_TEST = "is_test";
    public static final String CRASH_LOG_UPLOAD = "crash_catch_log_upload";//崩溃日志上报
    public static final String bindedPhoneUrl = "bindedPhoneUrl"; //绑定手机号跳转的url
    public static final String localLanguage = "localLanguage"; //本地语言
    public static final String BEFORE_TIME = "before_time"; //之前被关闭的时间



    /**
     * 从共享文件中获取字符串
     *
     * @param key      表签名
     * @param defValue 值
     */
    public static String getString(String key, String defValue) {
        try {
            if (null == pref) {
                pref = TheLApp.context.getSharedPreferences(SHARED_FILE_NAME, Context.MODE_PRIVATE);
            }
            String result;
            try {
                result = AESEncryptor.decrypt(AESEncryptor.SEED, pref.getString(key, defValue));
            } catch (Exception e) {
                result = pref.getString(key, defValue);
            }
            return result;

        } catch (Exception e) {
            return defValue;
        }

    }

    /**
     * 从共享文件中获取整型数据
     *
     * @param key      表签名
     * @param defValue 值
     */
    public static int getInt(String key, int defValue) {
        if (null == pref) {
            Context context = TheLApp.context;
            pref = TheLApp.context.getSharedPreferences(SHARED_FILE_NAME, Context.MODE_PRIVATE);
        }
        return pref.getInt(key, defValue);
    }

    /**
     * 从共享文件中获取长整型数据
     *
     * @param key
     * @param defValue
     * @return
     */
    public static long getLong(String key, long defValue) {
        if (null == pref) {
            Context context = TheLApp.context;
            pref = TheLApp.context.getSharedPreferences(SHARED_FILE_NAME, Context.MODE_PRIVATE);
        }
        return pref.getLong(key, defValue);
    }

    /**
     * 从共享文件中获取boolean数据
     *
     * @param key      表签名
     * @param defValue 值
     */
    public static boolean getBoolean(String key, boolean defValue) {
        if (null == pref) {
            Context context = TheLApp.context;
            pref = TheLApp.context.getSharedPreferences(SHARED_FILE_NAME, Context.MODE_PRIVATE);
        }
        return pref.getBoolean(key, defValue);
    }

    /**
     * 保存字符串数据
     *
     * @param key   表签名
     * @param value 值
     */
    public static void setString(String key, String value) {
        if (null == pref) {
            pref = TheLApp.context.getSharedPreferences(SHARED_FILE_NAME, Context.MODE_PRIVATE);
        }
        try {
            pref.edit().putString(key, AESEncryptor.encrypt(AESEncryptor.SEED, value)).apply();
        } catch (Exception e) {
            pref.edit().putString(key, value).apply();
        }
    }

    /**
     * 保存整型数据
     *
     * @param key   表签名
     * @param value 值
     */
    public static void setInt(String key, int value) {
        if (null == pref) {
            pref = TheLApp.context.getSharedPreferences(SHARED_FILE_NAME, Context.MODE_PRIVATE);
        }
        pref.edit().putInt(key, value).apply();
    }

    /**
     * 保存长整型数据
     *
     * @param key
     * @param value
     */
    public static void setLong(String key, long value) {
        if (null == pref) {
            pref = TheLApp.context.getSharedPreferences(SHARED_FILE_NAME, Context.MODE_PRIVATE);
        }
        pref.edit().putLong(key, value).apply();
    }

    /**
     * 保存boolean数据
     *
     * @param key   表签名
     * @param value 值
     */
    public static void setBoolean(String key, boolean value) {
        if (null == pref) {
            pref = TheLApp.context.getSharedPreferences(SHARED_FILE_NAME, Context.MODE_PRIVATE);
        }
        pref.edit().putBoolean(key, value).apply();
    }

    /**
     * 清空数据
     */
    public static void clearData() {
        pref = TheLApp.context.getSharedPreferences(SHARED_FILE_NAME, Context.MODE_PRIVATE);
        pref.edit().clear().apply();
    }

    public static void saveUserData(SignInBean userBean) {

        L.d(" saveUserData : 缓存数据 ");

        if (userBean == null || userBean.data == null)
            return;

        L.d(" saveUserData : userBean.data.key " + userBean.data.key);

        ShareFileUtils.setString(ShareFileUtils.KEY, userBean.data.key);
        ShareFileUtils.setString(ShareFileUtils.ID, String.valueOf(userBean.data.user.id));
        ShareFileUtils.setString(ShareFileUtils.USER_NAME, userBean.data.user.nickName);
        ShareFileUtils.setString(ShareFileUtils.USER_THEL_ID, userBean.data.user.userName);
        ShareFileUtils.setString(ShareFileUtils.AVATAR, userBean.data.user.avatar);
        ShareFileUtils.setString(ShareFileUtils.MESSAGE_USER, userBean.data.user.messageUser);
        ShareFileUtils.setString(ShareFileUtils.MESSAGE_PASSWORD, userBean.data.user.messagePassword);
        ShareFileUtils.setString(ShareFileUtils.ROLE_NAME, userBean.data.user.roleName);

        if (!TextUtils.isEmpty(userBean.data.auth.cell)) {
            ShareFileUtils.setString(ShareFileUtils.BIND_CELL, userBean.data.auth.cell);
        } else {
            ShareFileUtils.setString(ShareFileUtils.BIND_CELL, "");
        }
        if (!TextUtils.isEmpty(userBean.data.auth.wx)) {
            ShareFileUtils.setString(ShareFileUtils.BIND_WX, userBean.data.auth.wx);
        } else {
            ShareFileUtils.setString(ShareFileUtils.BIND_WX, "");
        }
        if (!TextUtils.isEmpty(userBean.data.auth.fb)) {
            ShareFileUtils.setString(ShareFileUtils.BIND_FB, userBean.data.auth.fb);
        } else {
            ShareFileUtils.setString(ShareFileUtils.BIND_FB, "");
        }

        // 检查必填项是不是都填了
        if (TextUtils.isEmpty(userBean.data.user.userName) || TextUtils.isEmpty(userBean.data.user.avatar) || TextUtils.isEmpty(userBean.data.user.nickName) || TextUtils.isEmpty(userBean.data.user.roleName)) {
            ShareFileUtils.setBoolean(ShareFileUtils.NEED_COMPLETE_USER_INFO, true);
        } else {
            ShareFileUtils.setBoolean(ShareFileUtils.NEED_COMPLETE_USER_INFO, false);
        }

        int ratio = userBean.data.user.ratio;
        ShareFileUtils.savaUserRatio(ratio);
        ShareFileUtils.setString(NO_RATIO, String.valueOf(userBean.data.user.ratio));

        CrashReport.setUserId(String.valueOf(userBean.data.user.id));
        CrashReport.putUserData(TheLApp.getContext(), "nickName", userBean.data.user.nickName);
        CrashReport.putUserData(TheLApp.getContext(), "userName", userBean.data.user.userName);
    }

    public static void saveUserData(MyInfoBean userBean) {
        if (userBean != null) {
            ShareFileUtils.setString(ShareFileUtils.AVATAR, userBean.avatar == null ? "" : userBean.avatar);
            ShareFileUtils.setString(ShareFileUtils.USER_NAME, userBean.nickName);
            ShareFileUtils.setString(ShareFileUtils.USER_THEL_ID, userBean.userName);
            ShareFileUtils.setString(ShareFileUtils.BIRTHDAY, userBean.birthday);
            ShareFileUtils.setString(ShareFileUtils.HEIGHT, userBean.height);
            ShareFileUtils.setString(ShareFileUtils.WEIGHT, userBean.weight);
            ShareFileUtils.setString(ShareFileUtils.ROLE_NAME, userBean.roleName);
            ShareFileUtils.setString(ShareFileUtils.AFFECTION, userBean.affection);
            ShareFileUtils.setString(ShareFileUtils.PURPOSE, userBean.purpose);
            ShareFileUtils.setString(ShareFileUtils.WANT_ROLE, userBean.wantRole);
            ShareFileUtils.setString(ShareFileUtils.PROFESSIONAL_TYPES, userBean.professionalTypes);
            ShareFileUtils.setString(ShareFileUtils.OUT_LEVEL, userBean.outLevel);
            ShareFileUtils.setString(ShareFileUtils.ETHNICITY, userBean.ethnicity);
            ShareFileUtils.setString(ShareFileUtils.OCCUPATION, userBean.occupation);
            ShareFileUtils.setString(ShareFileUtils.LIVECITY, userBean.livecity);
            ShareFileUtils.setString(ShareFileUtils.TRAVELCITY, userBean.travelcity);
            ShareFileUtils.setString(ShareFileUtils.LOCATION, userBean.location);
            ShareFileUtils.setString(ShareFileUtils.MOVIE, userBean.movie);
            ShareFileUtils.setString(ShareFileUtils.MUSIC, userBean.music);
            ShareFileUtils.setString(ShareFileUtils.BOOKS, userBean.books);
            ShareFileUtils.setString(ShareFileUtils.FOOD, userBean.food);
            ShareFileUtils.setString(ShareFileUtils.INTERESTS, userBean.interests);
            ShareFileUtils.setString(ShareFileUtils.INTRO, userBean.intro);
        }
    }

    public static UserInfoTable getUserInfoTable() {
        String key = ShareFileUtils.getString(ShareFileUtils.KEY, "");
        String userId = ShareFileUtils.getString(ShareFileUtils.ID, ""); // 保存用户自己的userid
        String nickName = ShareFileUtils.getString(ShareFileUtils.USER_NAME, "");
        String userName = ShareFileUtils.getString(ShareFileUtils.USER_THEL_ID, "");
        String avatar = ShareFileUtils.getString(ShareFileUtils.AVATAR, "");
        String roleName = ShareFileUtils.getString(ShareFileUtils.ROLE_NAME, "");
        String messageUser = ShareFileUtils.getString(ShareFileUtils.MESSAGE_USER, "");
        String ips = ShareFileUtils.getString(ShareFileUtils.CHAT_IP, "");

        UserInfoTable userInfoTable = new UserInfoTable();
        userInfoTable.msgCursor = "0";
        userInfoTable.key = key;
        userInfoTable.id = userId;
        userInfoTable.nickName = nickName;
        userInfoTable.userName = userName;
        userInfoTable.avatar = avatar;
        userInfoTable.roleName = roleName;
        userInfoTable.messageUser = messageUser;
        userInfoTable.imIps = ips;

        return userInfoTable;

    }

    public static MyInfoBean getUserData() {
        MyInfoBean userBean = new MyInfoBean();
        userBean.avatar = ShareFileUtils.getString(ShareFileUtils.AVATAR, "");
        userBean.nickName = ShareFileUtils.getString(ShareFileUtils.USER_NAME, "");
        userBean.userName = ShareFileUtils.getString(ShareFileUtils.USER_THEL_ID, "");
        userBean.birthday = ShareFileUtils.getString(ShareFileUtils.BIRTHDAY, "");
        userBean.height = ShareFileUtils.getString(ShareFileUtils.HEIGHT, "");
        userBean.weight = ShareFileUtils.getString(ShareFileUtils.WEIGHT, "");
        userBean.roleName = ShareFileUtils.getString(ShareFileUtils.ROLE_NAME, "");
        userBean.affection = ShareFileUtils.getString(ShareFileUtils.AFFECTION, "");
        userBean.purpose = ShareFileUtils.getString(ShareFileUtils.PURPOSE, "");
        userBean.wantRole = ShareFileUtils.getString(ShareFileUtils.WANT_ROLE, "");
        userBean.professionalTypes = ShareFileUtils.getString(ShareFileUtils.PROFESSIONAL_TYPES, "");
        userBean.outLevel = ShareFileUtils.getString(ShareFileUtils.OUT_LEVEL, "");
        userBean.ethnicity = ShareFileUtils.getString(ShareFileUtils.ETHNICITY, "");
        userBean.occupation = ShareFileUtils.getString(ShareFileUtils.OCCUPATION, "");
        userBean.livecity = ShareFileUtils.getString(ShareFileUtils.LIVECITY, "");
        userBean.travelcity = ShareFileUtils.getString(ShareFileUtils.TRAVELCITY, "");
        userBean.location = ShareFileUtils.getString(ShareFileUtils.LOCATION, "");
        userBean.movie = ShareFileUtils.getString(ShareFileUtils.MOVIE, "");
        userBean.music = ShareFileUtils.getString(ShareFileUtils.MUSIC, "");
        userBean.books = ShareFileUtils.getString(ShareFileUtils.BOOKS, "");
        userBean.food = ShareFileUtils.getString(ShareFileUtils.FOOD, "");
        userBean.interests = ShareFileUtils.getString(ShareFileUtils.INTERESTS, "");
        userBean.intro = ShareFileUtils.getString(ShareFileUtils.INTRO, "");
        return userBean;
    }

    /**
     * 保存资料完成度
     */
    public static void savaUserRatio(int ration) {
        ShareFileUtils.setInt(ShareFileUtils.RATIO, ration);
    }

    public static int getUserRatio() {
        int ratio = ShareFileUtils.getInt(ShareFileUtils.RATIO, 0);

        if (ratio > 0) {
            return ratio;

        } else {
            return 0;
        }
    }

    private static void setLogin(String str) {
        ShareFileUtils.setString(ShareFileUtils.IS_FIRST_LOGIN, str);
    }

    public static String isFirstLogin() {
        return ShareFileUtils.getString(ShareFileUtils.IS_FIRST_LOGIN, null);
    }

    public static String getWxLoginToken() {
        return ShareFileUtils.getString(ShareFileUtils.WX_LOGIN_TOKEN, null);
    }

    public static void setWxLoginToken(String wxToken) {
        ShareFileUtils.setString(ShareFileUtils.WX_LOGIN_TOKEN, wxToken);
    }

    public static long getWxLoginTime() {
        return ShareFileUtils.getLong(ShareFileUtils.WX_LOGIN_TIME, 0);
    }

    public static void setWxLoginTime() {
        ShareFileUtils.setLong(ShareFileUtils.WX_LOGIN_TIME, System.currentTimeMillis());
    }

    public static void clearLoginTime() {
        ShareFileUtils.setLong(ShareFileUtils.WX_LOGIN_TIME, 0);
    }

    public static String getWxLoginUserInfo() {
        return ShareFileUtils.getString(ShareFileUtils.WX_LOGIN_USER_INFO, null);
    }

    public static void setWxLoginUserInfo(String userInfo) {
        ShareFileUtils.setString(ShareFileUtils.WX_LOGIN_USER_INFO, userInfo);
    }

    /**
     * 判断是否为国际版
     *
     * @return
     */
    public static boolean isGlobalVersion() {
        return RequestConstants.APPLICATION_ID_GLOBAL.equals(BuildConfig.APPLICATION_ID) || !Locale.SIMPLIFIED_CHINESE.getLanguage().equals(DeviceUtils.getLanguage2());
    }

    /**
     * 返回角色map
     */
    public static RoleBean getRoleBean(int rolePosition) {
        SparseArray<RoleBean> roleMap = new SparseArray<RoleBean>();

        roleMap.put(0, new RoleBean(5, TheLApp.context.getResources().getStringArray(R.array.userinfo_role)[5]));
        roleMap.put(1, new RoleBean(0, TheLApp.context.getResources().getStringArray(R.array.userinfo_role)[0]));
        roleMap.put(2, new RoleBean(1, TheLApp.context.getResources().getStringArray(R.array.userinfo_role)[1]));
        roleMap.put(3, new RoleBean(2, TheLApp.context.getResources().getStringArray(R.array.userinfo_role)[2]));
        roleMap.put(4, new RoleBean(3, TheLApp.context.getResources().getStringArray(R.array.userinfo_role)[3]));
        roleMap.put(5, new RoleBean(4, TheLApp.context.getResources().getStringArray(R.array.userinfo_role)[4]));

        return roleMap.get(rolePosition);
    }

    public static <T> void setMatchData(List<T> list, int type, String tag) {
        File file = TheLApp.context.getCacheDir();
        File Cache = null;
        String name;
        if (type == 0) {
            name = "match" + tag;
            Cache = new File(file, name);
        }
        if (Cache.exists()) {
            Cache.delete();
        }
        try {
            ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(Cache));
            outputStream.writeObject(list);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static <T> List<T> getMatchData(String tag, int type) throws IllegalAccessException, InstantiationException {
        File file = TheLApp.context.getCacheDir();
        String name;
        File cache;
        List<T> list = null;
        if (type == 0) {
            name = "match" + tag;
            cache = new File(file, name);
            if (!cache.exists()) {
                return null;
            }
            try {
                ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(cache));
                list = (List<T>) inputStream.readObject();
                return list;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

}
