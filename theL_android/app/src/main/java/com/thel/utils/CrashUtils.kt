package com.thel.utils

import java.util.concurrent.TimeoutException

class CrashUtils {

    companion object {
        fun uncaughtDaemonException() {
            try {
                val defaultUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler()
                Thread.setDefaultUncaughtExceptionHandler { t, e ->

                    L.d("CrashUtils", " t : " + t.name + " \n e : " + e.message)

                    ShareFileUtils.setBoolean(ShareFileUtils.CRASH_LOG_UPLOAD, true)
                    if ((t?.name.equals("FinalizerWatchdogDaemon") && e is TimeoutException) || e is IllegalArgumentException) {
                        //ignore it
                    } else {
                        defaultUncaughtExceptionHandler.uncaughtException(t, e)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

}