package com.thel.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.thel.BuildConfig;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.ImgShareBean;
import com.thel.bean.VersionBean;
import com.thel.bean.comment.CommentBean;
import com.thel.bean.moments.MomentParentBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.recommend.RecommendWebBean;
import com.thel.bean.theme.ThemeClassBean;
import com.thel.bean.theme.ThemeCommentBean;
import com.thel.bean.user.UserInfoBean;
import com.thel.bean.video.VideoBean;
import com.thel.constants.BundleConstants;
import com.thel.constants.TheLConstants;
import com.thel.modules.live.bean.LiveRoomBean;
import com.thel.modules.main.MainActivity;
import com.thel.modules.main.MainFragment;
import com.thel.modules.main.home.moments.web.WebViewActivity;
import com.thel.modules.update.VersionUpdateActivity;
import com.thel.network.LoginInterceptorSubscribe;
import com.thel.network.RequestBusiness;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;

import cn.udesk.UdeskSDKManager;
import cn.udesk.callback.ITxtMessageWebonCliclk;
import cn.udesk.config.UdeskConfig;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import udesk.core.UdeskConst;

/**
 * 工具类
 *
 * @author zhangwenjia
 */
public class Utils {

    public static int px2dip(Context context, float pxValue) {
        DisplayMetrics dm = new DisplayMetrics();
        dm = context.getResources().getDisplayMetrics();
        return (int) (pxValue / dm.density + 0.5f);
    }

    public static int dip2px(Context context, float dipValue) {
        if (context == null) {
            context = TheLApp.getContext();
        }
        DisplayMetrics dm = new DisplayMetrics();
        dm = context.getResources().getDisplayMetrics();
        return (int) (dipValue * dm.density + 0.5f);
    }

    public static int dip2px(float dipValue) {
        return dip2px(TheLApp.getContext(), dipValue);
    }

    public static int px2sp(Context context, float pxValue) {
        final float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
        return (int) (pxValue / fontScale + 0.5f);
    }

    public static int sp2px(Context context, float spValue) {
        final float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
        return (int) (spValue * fontScale + 0.5f);
    }

    public static void sendVideoPlayingBroadcast(boolean isPlaying) {
        final Intent intent = new Intent();
        intent.setAction(TheLConstants.BROADCAST_VIDEO_PLAYING);
        intent.putExtra(TheLConstants.BUNDLE_KEY_VIDEO_PLAYING, isPlaying);
        TheLApp.getContext().sendBroadcast(intent);
    }

    public static String cmToInches(String cmStr) {
        Float LbsFlo = Float.parseFloat(cmStr);
        String inchesStr = (int) (LbsFlo * 0.3937 + 0.5f) + "";
        int inchesInt = Integer.parseInt(inchesStr);
        int inches = inchesInt / 12;
        int feet = inchesInt % 12;
        return inches + "'" + feet + "\"";
    }

    public static String kgToLbs(String kgStr) {
        Float kgFlo = Float.parseFloat(kgStr);
        String Lbs = (int) (kgFlo * 2.2 + 0.5f) + "";
        return Lbs;
    }

    public static String inchesToCm(String inchesStr) {
        String oneTemp = inchesStr;
        int oneLeng = oneTemp.indexOf('\'');
        String oneResult = oneTemp.substring(0, oneLeng);
        Float one = Float.parseFloat(oneResult);
        Float two = 0.0f;

        String twoTemp = inchesStr;
        int twoLeng = twoTemp.indexOf('\"');
        if (twoLeng > 1) {
            String twoResult = twoTemp.substring(oneLeng + 1, twoLeng);
            two = Float.parseFloat(twoResult);
        }

        Float result = one * 12 + two;
        String cmStr = (int) (result * 2.54 + 0.5f) + "";
        return cmStr;
    }

    public static String LbsTokg(String LbsStr) {
        Float LbsFlo = Float.parseFloat(LbsStr);
        String kgStr = (int) (LbsFlo * 0.4536 + 0.5f) + "";
        return kgStr;
    }

    /**
     * 是否是简体中文或者繁体英文
     *
     * @return 是 true,否 false
     */
    public static boolean isChaneseLanguage() {
        final Locale locale = DeviceUtils.getLanguageLocale();
        if (null != locale) {
            return Locale.CHINESE.getLanguage().equals(locale.getLanguage());
        }
        return false;
    }

    /**
     * 计算字符长度，中文算两个字符，其它算一个字符
     *
     * @param str
     * @return
     */
    public static int charCount(String str) {
        int count = 0;
        if (!TextUtils.isEmpty(str)) {
            char[] arr = str.toCharArray();
            for (int i = 0; i < arr.length; i++) {
                if (isChineseChar(arr[i])) {
                    count += 2;
                } else {
                    count++;
                }
            }
        }
        return count;
    }

    /**
     * 判断是否为中文
     *
     * @param c
     * @return
     */
    public static boolean isChineseChar(char c) {
        Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
        return ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS || ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A || ub == Character.UnicodeBlock.GENERAL_PUNCTUATION || ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION || ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS;
    }

    /**
     * 获取指定字符长度的字符串（中文2个字符，英文一个字符）
     *
     * @param s     要截取的字符串
     * @param limit 限定数
     * @return
     */
    public static String getLimitedStr(String s, int limit) {
        if (TextUtils.isEmpty(s)) {
            return "";
        }
        s = s.replace("\n", "");
        final int length = s.length();
        char[] arr = s.toCharArray();
        for (int i = 0, j = 1; i < length; i++, j++) {
            char ch = arr[i];
            if (isChineseChar(ch)) {
                j++;
            }
            if (j > limit) {
                return s.substring(0, i) + "...";
            }
        }
        return s;
    }

    /**
     * 从日志中获取到ImageShareBean
     *
     * @param momentBean
     * @return
     */
    public static ImgShareBean getImageShareBean(MomentsBean momentBean) {
        if (momentBean != null) {
            final ImgShareBean bean = new ImgShareBean();
            bean.userID = momentBean.userName;
            bean.picUserUrl = momentBean.avatar;
            bean.nickName = momentBean.nickname;
            bean.commentText = momentBean.momentsText;
            bean.imageUrl = momentBean.imageUrl;
            return bean;
        }
        return null;
    }

    /**
     * 从日志中获取到ImageShareBean
     *
     * @param momentBean
     * @return
     */
    public static ImgShareBean getImageShareBean(MomentParentBean momentBean) {
        if (momentBean != null) {
            final ImgShareBean bean = new ImgShareBean();
            bean.userID = momentBean.userName;
            bean.picUserUrl = momentBean.avatar;
            bean.nickName = momentBean.nickname;
            bean.commentText = momentBean.momentsText;
            bean.imageUrl = momentBean.imageUrl;
            return bean;
        }
        return null;
    }

    /**
     * 从个人主页获取到ImageShareBean
     *
     * @param userInfoBean
     * @return
     */
    public static ImgShareBean getUserImageShareBean(UserInfoBean userInfoBean) {
        if (userInfoBean != null) {
            final ImgShareBean bean = new ImgShareBean();
            bean.userID = userInfoBean.userName;
            bean.picUserUrl = userInfoBean.avatar;
            bean.nickName = userInfoBean.nickName;
            //  bean.commentText = userInfoBean.momentsText;
            if (userInfoBean.liveInfo != null) {
                bean.imageUrl = userInfoBean.liveInfo.imageUrl;
            } else {
                bean.imageUrl = "";
            }
            return bean;
        }
        return null;
    }

    /**
     * 从话题回复获取到imagesharebean
     *
     * @param userInfoBean
     * @return
     */
    public static ImgShareBean getThemeImageShareBean(CommentBean userInfoBean) {
        if (userInfoBean != null) {
            final ImgShareBean bean = new ImgShareBean();
            bean.userID = userInfoBean.userName;
            bean.picUserUrl = userInfoBean.avatar;
            bean.nickName = userInfoBean.nickname;
            bean.commentText = userInfoBean.commentText;
            bean.imageUrl = userInfoBean.imageUrl;
            return bean;
        }
        return null;
    }

    /**
     * 从话题回复获取到imagesharebean
     *
     * @param userInfoBean
     * @return
     */
    public static ImgShareBean getThemeImageShareBean(ThemeCommentBean userInfoBean) {
        if (userInfoBean != null) {
            final ImgShareBean bean = new ImgShareBean();
            bean.userID = userInfoBean.userName;
            bean.picUserUrl = userInfoBean.avatar;
            bean.nickName = userInfoBean.nickname;
            bean.commentText = userInfoBean.commentText;
            bean.imageUrl = userInfoBean.imageUrl;
            return bean;
        }
        return null;
    }

    /**
     * 根据byte[]来生成bitmap对象
     *
     * @param byteArray
     * @return Bitmap 返回位图
     */
    public static Bitmap decodeBitmap(byte[] byteArray) {
        return BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
    }

    public static String savePicFromBase64(String img64) {
        File dir = new File(TheLConstants.F_TAKE_PHOTO_ROOTPATH);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        try {
            final String imgName = System.currentTimeMillis() + TheLConstants.WEB_SHARE_IMG_END + ".jpg";
            final byte[] data = Base64.decode(img64);
            saveBytes(data, TheLConstants.F_TAKE_PHOTO_ROOTPATH, imgName);
            return TheLConstants.F_TAKE_PHOTO_ROOTPATH + imgName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * 保存字节流到文件
     *
     * @param data     字节数据
     * @param path     保存路径
     * @param fileName 保存文件名
     */
    public static void saveBytes(byte[] data, String path, String fileName) {
        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
            File file = new File(path);
            if (!file.exists()) {
                file.mkdirs();
            }
            File picPath = new File(path + fileName);
            BufferedOutputStream bufferedOutputStream;
            try {
                bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(picPath));
                bufferedOutputStream.write(data);
                bufferedOutputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 判断是否是自己
     *
     * @param userId
     * @return
     */
    public static boolean isMyself(String userId) {
        return ShareFileUtils.getString(ShareFileUtils.ID, "").equals(userId);
    }

    /**
     * 获取自己的id
     *
     * @return
     */
    public static String getMyUserId() {
        return ShareFileUtils.getString(ShareFileUtils.ID, "");
    }

    /**
     * 去UDesk
     */
    public static void gotoUDesk() {
        final String userId = ShareFileUtils.getString(ShareFileUtils.ID, "");
        final String nickName = ShareFileUtils.getString(ShareFileUtils.USER_NAME, "");
        final String rela_id = ShareFileUtils.getString(ShareFileUtils.USER_THEL_ID, "");
        final String avatar = ShareFileUtils.getString(ShareFileUtils.AVATAR, "");
        Map<String, String> info = new HashMap<>();
        info.put(UdeskConst.UdeskUserInfo.USER_SDK_TOKEN, userId);
        info.put(UdeskConst.UdeskUserInfo.NICK_NAME, nickName);

        Map<String, String> newInfo = new HashMap<>();

        newInfo.put("TextField_17119", rela_id);
        newInfo.put("TextField_17120", userId);
        newInfo.put("TextField_17839", DeviceUtils.getVersionName(TheLApp.context));//VERSION_CODE

        // 只设置用户基本信息的配置
        UdeskConfig.Builder builder = new UdeskConfig.Builder();
        builder.setDefaultUserInfo(info);
        //传入需要更新的自定义文本字段

        builder.setDefinedUserTextField(newInfo);
        builder.setCustomerUrl(avatar);
        builder.setUdeskTitlebarBgResId(R.color.tab_normal);
        builder.setTxtMessageClick(new ITxtMessageWebonCliclk() {
            @Override
            public void txtMsgOnclick(String url) {
                Intent intent = new Intent(TheLApp.getContext(), WebViewActivity.class);
                intent.putExtra(BundleConstants.URL, url);
                intent.putExtra("title", "");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                TheLApp.getContext().startActivity(intent);
            }
        });
        UdeskSDKManager.getInstance().entryChat(TheLApp.getContext(), builder.build(), userId);


        //  UdeskSDKManager.getInstance().getFormCallBak();

    }

    private static void uiStyle() {
        //  UdeskConfig.udeskTitlebarBgResId = R.color.tab_normal;
        //        UdeskConfig.udeskTitlebarTextLeftRightResId = R.color.udesk_color_navi_text1;
        //        UdeskConfig.udeskIMRightTextColorResId = R.color.udesk_color_im_text_right1;
        //        UdeskConfig.udeskIMLeftTextColorResId = R.color.udesk_color_im_text_left1;
        //        UdeskConfig.udeskIMAgentNickNameColorResId = R.color.udesk_color_im_left_nickname1;
        //        UdeskConfig.udeskIMTimeTextColorResId = R.color.udesk_color_im_time_text1;
        //        UdeskConfig.udeskIMTipTextColorResId = R.color.udesk_color_im_tip_text1;
        //        UdeskConfig.udeskbackArrowIconResId = R.drawable.udesk_titlebar_back;
        //
        //        UdeskConfig.udeskCommityBgResId = R.color.udesk_color_im_commondity_bg1;
        //        UdeskConfig.udeskCommityTitleColorResId = R.color.udesk_color_im_commondity_title1;
        //        UdeskConfig.udeskCommitysubtitleColorResId = R.color.udesk_color_im_commondity_subtitle1;
        //        UdeskConfig.udeskCommityLinkColorResId = R.color.udesk_color_im_commondity_title1;
    }

    /**
     * 从日志中获取到VideoBean
     *
     * @param momentBean
     * @return
     */
    public static VideoBean getVideoFromMoment(MomentsBean momentBean) {
        if (momentBean != null) {
            final VideoBean bean = new VideoBean();
            bean.id = momentBean.momentsId;
            bean.videoUrl = momentBean.videoUrl;
            bean.image = momentBean.thumbnailUrl;
            bean.pixelWidth = momentBean.pixelWidth;
            bean.pixelHeight = momentBean.pixelHeight;
            bean.playTime = momentBean.playTime;
            bean.playCount = momentBean.playCount;
            bean.text = momentBean.momentsText;
            bean.winkFlag = momentBean.winkFlag;
            bean.winkNum = momentBean.winkNum;
            bean.commentNum = momentBean.commentNum;
            bean.avatar = momentBean.avatar;
            bean.isFollow = momentBean.followerStatus;
            bean.nickname = momentBean.nickname;
            bean.liveStatus = momentBean.liveStatus;
            return bean;
        }
        return null;
    }

    public static VideoBean getVideoFromParentMoment(MomentParentBean momentParentBean) {
        if (momentParentBean != null) {
            final VideoBean bean = new VideoBean();
            bean.id = momentParentBean.momentsId;
            bean.videoUrl = momentParentBean.videoUrl;
            bean.image = momentParentBean.thumbnailUrl;
            bean.pixelWidth = momentParentBean.pixelWidth;
            bean.pixelHeight = momentParentBean.pixelHeight;
            bean.playTime = momentParentBean.playTime;
            bean.playCount = momentParentBean.playCount;
            bean.text = momentParentBean.momentsText;
            bean.winkFlag = momentParentBean.winkFlag;
            bean.winkNum = momentParentBean.winkNum;
            bean.commentNum = momentParentBean.commentNum;
            bean.avatar = momentParentBean.avatar;
            bean.isFollow = momentParentBean.followerStatus;
            bean.nickname = momentParentBean.nickname;
            bean.liveStatus = momentParentBean.liveStatus;

            return bean;
        }
        return null;
    }

    /**
     * 跳转到指定的话题类型
     *
     * @param themeClassBean
     */
    public static void gotoThemeTab(ThemeClassBean themeClassBean, Context context) {
        if (themeClassBean != null) {
            if (isChaneseLanguage()) {
                final Intent intent = new Intent();
                intent.putExtra(TheLConstants.BUNDLE_KEY_THEME_CLASS_BEAN, themeClassBean);
                intent.setAction(TheLConstants.BROADCAST_GOTO_THEME_TAB);
                TheLApp.getContext().sendBroadcast(intent);

                Intent intent1 = new Intent(context, MainActivity.class);
                intent1.putExtra(TheLConstants.BUNDLE_KEY_THEME_CLASS_BEAN, themeClassBean);
                intent1.putExtra(TheLConstants.BUNDLE_KEY_WHERE_TO_GO, MainFragment.NEARBY_TAG);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent1);

            } else {
                DialogUtil.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.only_for_chinese));
            }
        }
    }

    /**
     * 把直播间信息转换为日志信息
     *
     * @param liveRoomBean
     * @return
     */
    public static String getMomentBeanFromLiveRoomBean(LiveRoomBean liveRoomBean) {

        L.d("ChatUtils", " liveRoomBean toString : " + GsonUtils.createJsonString(liveRoomBean));

        MomentsBean momentsBean = new MomentsBean();
        if (liveRoomBean.audioType == 0) {//视频直播
            momentsBean.momentsType = MomentsBean.MOMENT_TYPE_LIVE;
        } else {
            momentsBean.momentsType = MomentsBean.MOMENT_TYPE_VOICE_LIVE;

        }
        momentsBean.userId = (int) liveRoomBean.user.id;
        momentsBean.nickname = liveRoomBean.user.nickName;
        momentsBean.imageUrl = liveRoomBean.user.avatar;
        momentsBean.liveId = liveRoomBean.id;
        momentsBean.avatar = liveRoomBean.user.avatar;
        momentsBean.momentsText = liveRoomBean.text;
        momentsBean.isLandscape = liveRoomBean.isLandscape;
        return momentsBean.toJson(momentsBean.momentsType).toString();
    }

    /***
     * 陌生人消息提示 屏蔽 跟举报
     */
    //public static SpannableString
    public static MomentsBean getWebRecommendBean(MomentsBean recommendbean) {
        if (recommendbean == null || recommendbean.momentsType.equals(MomentsBean.MOMENT_TYPE_WEB)) {
            return null;
        }
        final String text = recommendbean.momentsText;
        final RecommendWebBean bean = new RecommendWebBean();
        bean.fromJson(text);
        recommendbean.momentsText = bean.momentsText;
        recommendbean.imageUrl = bean.imageUrl;
        return recommendbean;
    }


    public static <K, V> String mapToString(Map<K, V> map) {
        java.util.Map.Entry entry;
        StringBuffer sb = new StringBuffer();
        for (Iterator iterator = map.entrySet().iterator(); iterator.hasNext(); ) {
            entry = (java.util.Map.Entry) iterator.next();
            sb.append(entry.getKey().toString()).append("'").append(null == entry.getValue() ? "" : entry.getValue().toString()).append(iterator.hasNext() ? "^" : "");
        }
        return sb.toString();
    }

    public static void setTextWithUrl(TextView textView, String content) {
        textView.setText(createUrlSpannableString(content));
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    /**
     * 把 list转为String 后再转为List
     *
     * @param listString
     * @return
     */
    public static List<String> listStringToList(String listString) {
        if (TextUtils.isEmpty(listString)) {
            return new ArrayList<>();
        }
        final StringBuilder sb = new StringBuilder(listString);
        if (listString.startsWith("[")) {
            sb.deleteCharAt(0);
        }
        if (listString.endsWith("]")) {
            sb.deleteCharAt(sb.length() - 1);
        }
        return new ArrayList<>(Arrays.asList(sb.toString().split(",")));
    }

    /**
     * list 转换为 a,b,c的String
     *
     * @param list
     * @return
     */
    public static String listToString(List<String> list) {
        final StringBuilder sb = new StringBuilder();
        for (String st : list) {
            sb.append(st).append(",");
        }
        if (sb.length() > 0) {
            sb.deleteCharAt(sb.length() - 1);
        }
        return sb.toString();
    }

    /**
     * 文本带网页的相应转换
     *
     * @param string
     * @return
     */
    public static SpannableString createUrlSpannableString(String string) {
        Matcher urlMatcher = TheLConstants.URL_PATTERN.matcher("");
        SpannableString spannableString = new SpannableString(string);
        urlMatcher.reset(spannableString);
        List<Integer> startIndexsUrl = new ArrayList<>();
        List<Integer> endIndexsUrl = new ArrayList<>();
        while (urlMatcher.find()) {
            startIndexsUrl.add(urlMatcher.start());
            endIndexsUrl.add(urlMatcher.start() + urlMatcher.group().length());
        }
        if (!startIndexsUrl.isEmpty()) {
            for (int i = 0; i < startIndexsUrl.size(); i++) {
                final String url = spannableString.toString().substring(startIndexsUrl.get(i), endIndexsUrl.get(i));
                spannableString.setSpan(new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(TheLApp.getContext(), WebViewActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(BundleConstants.URL, url);
                        bundle.putString("title", "");
                        intent.putExtras(bundle);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        TheLApp.getContext().startActivity(intent);
                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        ds.setColor(Color.parseColor("#4BBABC"));
                        ds.setUnderlineText(false); // 去掉下划线
                    }
                }, startIndexsUrl.get(i), endIndexsUrl.get(i), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }
        return spannableString;
    }

    public static void checkUpdate(final Context context) {

        if (ShareFileUtils.isGlobalVersion()) {
            return;
        }

        RequestBusiness.getInstance().checkUpdate().onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new LoginInterceptorSubscribe<VersionBean>() {
            @Override
            public void onNext(VersionBean bean) {
                super.onNext(bean);

                if (bean == null || bean.data == null) {
                    return;
                }
                // 成功数据
                if (TextUtils.isEmpty(bean.data.version) && TextUtils.isEmpty(bean.data.google_version)) {
                    return;
                }
                //如果是googleplay版本
                if (ShareFileUtils.isGlobalVersion() && TextUtils.isEmpty(bean.data.google_version)) {
                    //如果googleplay不是更高版本，则返回
                    if (!isHigherVersion(bean.data.google_version)) {
                        return;
                    }
                    //非googleplay的不是更高版本也返回
                } else if (!isHigherVersion(bean.data.version)) {
                    return;
                }
                Intent intent = new Intent();
                intent.setClass(context, VersionUpdateActivity.class);
                intent.putExtra(TheLConstants.BUNDLE_KEY_VERSIONBEAN, bean);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);

            }
        });

    }

    /**
     * 后台提供版本是否是更高版本
     *
     * @param onlineVersion
     * @return
     */
    public static boolean isHigherVersion(String onlineVersion) {
        if (TextUtils.isEmpty(onlineVersion)) {
            return false;
        }
        final String currentVersion = DeviceUtils.getVersionName(TheLApp.getContext());
        final String[] onlineArr = onlineVersion.split("\\.");
        final String[] currentArr = currentVersion.split("\\.");
        if (onlineArr.length > currentArr.length) {//如果当前版本的名称分隔比以前的多，则肯定是更高版本
            return true;
        }
        if (onlineArr.length == currentArr.length) {
            final int length = onlineArr.length;
            for (int i = 0; i < length; i++) {
                try {
                    int onlineCo = Integer.parseInt(onlineArr[i]);
                    int currentCo = Integer.parseInt(currentArr[i]);
                    if (onlineCo > currentCo) {//更高版本
                        return true;
                    } else if (onlineCo == currentCo) {
                    } else if (onlineCo < currentCo) {
                        return false;
                    }
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    /**
     * 跳转到应用商店
     *
     * @param context
     */
    public static void directToAppStore(Context context) {
        try {
            Uri uri = Uri.parse("market://details?id=" + BuildConfig.APPLICATION_ID);
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        } catch (Exception e) {
            DialogUtil.showToastShort(context, context.getString(R.string.me_activity_no_market));
        }
    }

    public static int getColor(@NonNull int res) {
        return ContextCompat.getColor(TheLApp.getContext(), res);
    }

    public static short[] toShortArray(byte[] src) {
        final int count = src.length;
        final short[] dest = new short[count / 2];
        ByteBuffer.wrap(src).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(dest);
        return dest;
    }

    /**
     * 获取根布局view
     *
     * @param activity
     * @return
     */
    public static ViewGroup getContentView(Activity activity) {
        if (activity == null) {
            return null;
        }
        return activity.findViewById(android.R.id.content);
    }

    private static StringBuffer mStringBuffer = new StringBuffer();

    public static String formatTime(long count) {
        mStringBuffer.setLength(0);

        //得到分钟数
        long min = count / 60;
        //得到秒
        long seconds = count % 60;

        if (min >= 1) {

            if (min > 9) {
                mStringBuffer.append(min + ":");
            } else {
                mStringBuffer.append("0" + min + ":");
            }

            if (seconds < 10) {
                mStringBuffer.append("0" + seconds);
            } else {
                mStringBuffer.append(seconds);
            }

        } else {
            String time = TheLApp.context.getString(R.string.n_second, String.valueOf(seconds));
            mStringBuffer.append(time);
        }

        return mStringBuffer.toString();
    }

    public static String formatTime2(long count) {
        mStringBuffer.setLength(0);

        //得到分钟数
        long min = count / 60;
        //得到秒
        long seconds = count % 60;

        if (min >= 10) {
            mStringBuffer.append(min + ":");
            if (seconds >= 10) {
                mStringBuffer.append(seconds);
            } else {
                mStringBuffer.append("0" + seconds);
            }
        } else {
            mStringBuffer.append("0" + min + ":");
            if (seconds >= 10) {
                mStringBuffer.append(seconds);
            } else {
                mStringBuffer.append("0" + seconds);
            }
        }

        return mStringBuffer.toString();
    }

    public static boolean isNewUser() {
        //这个-10000是
        String roleName = ShareFileUtils.getString(ShareFileUtils.ROLE_NAME, "-10000");
        boolean isFirstStart = ShareFileUtils.getBoolean(ShareFileUtils.IS_FIRST_START, false);
        return isFirstStart && (roleName.equals("") || roleName.equals("-10000"));
    }

    public static String getWebpPathByVideoPath(String videoPath) {
        if (TextUtils.isEmpty(videoPath) || videoPath.indexOf(".") == -1) {
            return "";
        }
        final int index = videoPath.lastIndexOf(".");
        final String webpPath = videoPath.substring(0, index) + ".webp";
        return webpPath;
    }

    /**
     * 获取ram大小
     *
     * @return
     */
    public static int getTotalRam() {
        String path = "/proc/meminfo";
        String firstLine = null;
        int totalRam = 0;
        try {
            FileReader fileReader = new FileReader(path);
            BufferedReader br = new BufferedReader(fileReader, 8192);
            firstLine = br.readLine().split("\\s+")[1];
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (firstLine != null) {
            totalRam = (int) Math.ceil((Float.valueOf(Float.valueOf(firstLine) / (1024 * 1024)).doubleValue()));
        }

        return totalRam;//返回1GB/2GB/3GB/4GB
    }

    public static boolean isLogged() {
        return ShareFileUtils.getBoolean(ShareFileUtils.HAS_LOGGED, false);
    }

    public static void playSound(int rawId) {

        final MediaPlayer mMediaPlayer = MediaPlayer.create(TheLApp.context, rawId);
        mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                if (mMediaPlayer != null) {
                    mMediaPlayer.stop();
                    mMediaPlayer.release();
                }
            }
        });
        mMediaPlayer.start();

    }

    /**
     * 根据时间戳和自己的id结合作为页面的的pageid
     **/
    public static String getPageId() {
        long time = System.currentTimeMillis();
        return time + "_" + UserUtils.getMyUserId();

    }

    /**
     * 根据时间戳和自己的id结合作为页面的的pageid
     **/
    public static String getCurrentPageId() {
        long time = System.currentTimeMillis();
        String userid = ShareFileUtils.getString(ShareFileUtils.ID, "");
        return time + "_" + userid;

    }

}
