package com.thel.utils;

import com.thel.bean.moments.MomentsBean;
import com.thel.bean.recommend.RecommendWebBean;
import com.thel.constants.MomentTypeConstants;

/**
 * Created by kevin on 2017/10/13.
 */

public class BeanUtils {
    /**
     * 从日志中获取RecommenWebBean
     *
     * @param bean
     * @return
     */
    public static RecommendWebBean getRecommendWebBeanFromMoment(MomentsBean bean) {
        if (null == bean || !MomentTypeConstants.MOMENT_TYPE_WEB.equals(bean.momentsType)) {
            return null;
        }
        RecommendWebBean recommendWebBean = GsonUtils.getObject(bean.momentsText, RecommendWebBean.class);
        return recommendWebBean;
    }
}
