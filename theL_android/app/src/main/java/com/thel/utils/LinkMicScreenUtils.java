package com.thel.utils;

import android.graphics.RectF;

import com.thel.app.TheLApp;

/**
 * Created by liuyun on 2017/12/15.
 */

public class LinkMicScreenUtils {


    private static float height = ScreenUtils.getScreenHeight(TheLApp.context);      //屏幕高度

    private static float width = ScreenUtils.getScreenWidth(TheLApp.context);        //屏幕宽度

    public static final float LINK_MIC_VIDEO_WIDTH = 0.3f;                          //子视频宽度占比

    private static float LINK_MIC_VIDEO_SCALE = width / height;                      //子视频比例

    final static float bottomBarHeight = SizeUtils.dip2px(TheLApp.context, 67);//bottom高度

    public final static float CommonScale = 9f / 16f;


    public static RectF getParentVideoRect() {
        return new RectF(0, 0, 1, 1);
    }

    public static final float bottomHeight = 0.1f;//设定连麦底部的基本高度

    /*public static RectF getChildVideoRect() {


        final float mSubWidth = LINK_MIC_VIDEO_WIDTH;                        //子视频的宽度占比
        final float mSubHeight = LINK_MIC_VIDEO_WIDTH / LINK_MIC_VIDEO_SCALE * width / height;//子视频高度占比

        final float mSubLeft = 1 - mSubWidth;
        final float mSubTop = (height - bottomBarHeight) / height - mSubHeight;

        return new RectF(mSubLeft, mSubTop, mSubWidth, mSubHeight);
    }
*/

    /***以16:9的手机为标准设置的连麦位置***/
    /*public static RectF getChildVideoRect() {
        final float mWidth = AppInit.displayMetrics.widthPixels;
        final float mHeight = AppInit.displayMetrics.heightPixels;
        final float mScale = mWidth / mHeight;
        float rd = ((mScale - CommonScale) / 2f) / mScale;  //当前手机宽高比与标准设置宽高比的差值（因为居中截取，所有要除以2）
        if (rd <= 0) {//当前手机宽高比小于标准设置的宽高比（9：18）
            rd = 0;
        }

        final float mSubWidth = LINK_MIC_VIDEO_WIDTH * (CommonScale / mScale);
        final float mSubHeight = LINK_MIC_VIDEO_WIDTH;
        final float mSubLeft = 1 - rd - mSubWidth;
        final float mSubTop = (height - bottomBarHeight) / height - mSubHeight;
        return new RectF(mSubLeft, mSubTop, mSubWidth, mSubHeight);
    }
*/

    /***以16:9的手机为标准设置的连麦位置***/
    public static RectF getChildVideoRect() {
        final float mWidth = AppInit.displayMetrics.widthPixels;
        final float mHeight = AppInit.displayMetrics.heightPixels;
        final float mScale = mWidth / mHeight;
        if (mScale >= CommonScale) {//若果当前宽高比大于要求宽高比，则宽度截取
            final float tHeight = LINK_MIC_VIDEO_WIDTH;
            final float tY = 1 - bottomHeight - tHeight;

            final float sc = CommonScale / mScale;
            final float tWidth = tHeight * sc;
            final float rm = ((mScale - CommonScale) / 2) / mScale;
            final float tX = 1 - rm - tWidth;
            return new RectF(tX, tY, tWidth, tHeight);
        } else {//如果当前宽高比小于标准宽高比，则高度截取
            final float tWidth = LINK_MIC_VIDEO_WIDTH;
            final float tX = 1 - tWidth;
            final float sc = mScale / CommonScale;
            final float tHeight = tWidth * sc;
            final float rb1 = ((CommonScale - mScale) / 2) / CommonScale;
            final float rb2 = bottomHeight * sc;
            final float tY = 1 - rb1 - rb2 - tHeight;
            return new RectF(tX, tY, tWidth, tHeight);

        }
    }

    /**
     * 根据连麦的坐标，设置连麦覆盖View的右边距
     *
     * @param rectF
     * @return
     */
    public static float getRightMargin(RectF rectF) {
        float rightMargin = 0f;
        final float mWidth = AppInit.displayMetrics.widthPixels;
        rightMargin = mWidth * (1 - (rectF.left + rectF.right));
        return rightMargin;
    }

    public static RectF getVideoInfoRect() {

        float heightScale = (height - bottomBarHeight - LINK_MIC_VIDEO_WIDTH * height) / height;

        RectF rectF = new RectF();
        rectF.left = 1f - LINK_MIC_VIDEO_WIDTH;                          //视频距离左边比例 x
        rectF.top = heightScale;                                         //视频距离顶部比例 y
        rectF.right = LINK_MIC_VIDEO_WIDTH;                              //视频占频宽的比例 width
        rectF.bottom = LINK_MIC_VIDEO_WIDTH;                             //视频占频高的比例 height
        return rectF;
    }

    public static int getChildVideoWidth() {
        final float width = ScreenUtils.getScreenWidth(TheLApp.context);
        return (int) (width * LINK_MIC_VIDEO_WIDTH);
    }

    public static int getChildVideoHeight() {
        final float mSubHeight = LINK_MIC_VIDEO_WIDTH / LINK_MIC_VIDEO_SCALE * width / height;//子视频高度占比
        return (int) (mSubHeight * height);
    }

    public static int getRecyclerViewWidth() {
        final float marginRight = SizeUtils.dip2px(TheLApp.context, 20);
        int childVideoWidth = (int) (LINK_MIC_VIDEO_WIDTH * width);
        return (int) (width - childVideoWidth - marginRight);
    }

    public static RectF getLinkMicSmallViewRectF() {
        RectF mRectF = new RectF();
        mRectF.left = 1 - LINK_MIC_VIDEO_WIDTH;
        mRectF.top = 1 - (mRectF.right * 16 / 9) - (bottomBarHeight / ScreenUtils.getScreenHeight(TheLApp.context));
        mRectF.right = LINK_MIC_VIDEO_WIDTH;
        mRectF.bottom = mRectF.right * 16 / 9;
        return mRectF;
    }


    /*public static RectF getLiveShowlinkRect(float x, float y, float width, float height) {
        //由于每个主播端都是按照本地屏幕居中截取9：18的比例进行相应的缩放传参
        //所以，可以得到相应屏幕长宽比例,按理说，长款比例应该是1：1，而对于宽长比例小于9：18的，按照了9：18的计算，所以这个误差可以除去
        //所以，理论上，传过来的height，一直是0.3f，而width，是经过了相应计算得到的
        //然后再根据本地屏幕的长款比例
        //可以计算进行相应的缩放
        //height=0.3;
        if (width == 0) {//被除数要防止为0
            width = 0.3f;
        }
        final float mWidth = AppInit.displayMetrics.widthPixels;
        final float mHeight = AppInit.displayMetrics.heightPixels;
        if (mWidth == 0 || mHeight == 0) {
            return new RectF(x, y, width, height);
        }
        final float mScale = mWidth / mHeight;
        final float commonScale = CommonScale;
        //已知，width=(commonScale/netScale)*o.3，所以得出直播段的流宽长比例netScale
        final float netScale = commonScale * LINK_MIC_VIDEO_WIDTH / width;
        //如果当前的屏幕的宽长比大于推流端的宽长比，则是对流扩大后截取高度，则宽度方面的x,with,与推流端一样
        if (mScale >= netScale) {
            final float tX = x;
            final float tWidth = width;
            //算出推流端的流放大的比例
            final float sc = mScale / netScale;
            //高度扩大相应比例
            final float tHeight = height * sc;
            //当前屏幕y轴坐标应该为
            final float a = 0f, b = 1f;
            // final float tY = (y - (a + b) / 2f) * (sc ) + y;
            final float tY = (netScale * y - (netScale - mScale) / 2) / mScale;

            return new RectF(tX, tY, tWidth, tHeight);
        } else {//当屏幕是的宽长比例小于推流端的宽长比例的时候，则宽要相应的拉伸，高度方面,y,height不变
            final float tY = y;
            final float tHeight = height;
            //流的扩大相应比例
            final float sc = netScale / mScale;
            final float tWidth = width * sc;
            //当前屏幕x坐标应该为
            final float a = 0f, b = 1f;
            final float tX = (netScale * x - (netScale - mScale) / 2) / mScale;
            return new RectF(tX, tY, tWidth, tHeight);
        }

    }*/

    public static RectF getLiveShowlinkRect(float x, float y, float width, float height) {
        //由于每个主播端都是按照本地屏幕居中截取9：16的比例进行相应的缩放传参
        //所以，可以得到相应屏幕长宽比例,按理说，width:height应该都是0.3，即1：1
        //而且由于必然是宽截取或者是高截取，所以，必然有一个是0.3，0一个要小于0.3
        //然后再根据本地屏幕的长款比例
        //可以计算进行相应的缩放
        //height=0.3;
        if (width == 0) {//被除数要防止为0
            width = 0.3f;
        }
        if (height == 0) {
            height = 0.3f;
        }

        final float mWidth = AppInit.displayMetrics.widthPixels;
        final float mHeight = AppInit.displayMetrics.heightPixels;
        if (mWidth == 0 || mHeight == 0) {
            return new RectF(x, y, width, height);
        }

        final float mScale = mWidth / mHeight;
        if (mWidth == 0 || mHeight == 0) {
            return new RectF(x, y, width, height);
        }
        float netScale = (height / width) * CommonScale;//为推流端的屏幕比例

        if (mScale >= netScale) {
            final float tX = x;
            final float tWidth = width;
            //算出推流端的流放大的比例
            final float sc = mScale / netScale;
            //高度扩大相应比例
            final float tHeight = height * sc;
            //当前屏幕y轴坐标应该为
            final float tY = (mScale * y - (mScale - netScale) / 2) / netScale;

            return new RectF(tX, tY, tWidth, tHeight);
        } else {//当屏幕是的宽长比例小于推流端的宽长比例的时候，则宽要相应的拉伸，高度方面,y,height不变
            final float tY = y;
            final float tHeight = height;
            //流的扩大相应比例
            final float sc = netScale / mScale;
            final float tWidth = width * sc;
            //当前屏幕x坐标应该为
            final float tX = (netScale * x - (netScale - mScale) / 2) / mScale;
            return new RectF(tX, tY, tWidth, tHeight);
        }
    }

}
