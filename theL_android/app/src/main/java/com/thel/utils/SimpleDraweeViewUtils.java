package com.thel.utils;

import android.net.Uri;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

/**
 * Created by waiarl on 2017/11/28.
 */

public class SimpleDraweeViewUtils {
    /**
     * Load image by Fresco.
     *
     * @param simpleDraweeView simpleDraweeView.
     * @param url              image url
     * @param width            view width
     * @param height           view height
     * @return The BaseViewHolder for chaining.
     */
    public static void setImageUrl(SimpleDraweeView simpleDraweeView, String url, float width, float height) {
        if (simpleDraweeView == null) {
            return;
        }
        simpleDraweeView.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(url, width, height))).build()).setAutoPlayAnimations(true).build());
    }

    public static void setImageUrl(SimpleDraweeView simpleDraweeView, String url) {
        if (simpleDraweeView == null) {
            return;
        }
        simpleDraweeView.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(url))).build()).setAutoPlayAnimations(true).build());
    }

    public static void setImageUri(SimpleDraweeView simpleDraweeView, Uri uri) {
        try {
            if (simpleDraweeView == null || uri == null) {
                return;
            }
            simpleDraweeView.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(uri).build()).setAutoPlayAnimations(true).build());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
