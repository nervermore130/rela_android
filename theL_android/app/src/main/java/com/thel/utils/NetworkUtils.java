package com.thel.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import com.thel.app.TheLApp;
import com.thel.network.RequestConstants;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

/**
 * Created by setsail on 16/3/3.
 */
public class NetworkUtils {

    private static int connectTimeout = 5000;

    /**
     * 对域名进行HTTPDNS解析
     *
     * @param host
     * @return
     */
    public static String httpDNSLookUp(String host) {
        long timeout = SharedPrefUtils.getLong(SharedPrefUtils.FILE_HTTP_DNS, SharedPrefUtils.TIME_OUT + host, 0);
        long timeCur = System.currentTimeMillis();
        // 如果超时了，则重新请求DNS
        if (timeCur > timeout) {
            // 返回的结果格式为 180.153.105.151;180.153.105.152;180.153.105.148;180.153.105.150,358
            String result = executeHttpGet(RequestConstants.HTTP_DNS_SERVICE + host);
            try {
                if (!TextUtils.isEmpty(result)) {
                    String[] arr = result.split(",");
                    if (arr.length == 2) {
                        String ip = null;
                        String[] ips = arr[0].split(";");
                        if (ips != null && ips.length > 0) {
                            ip = ips[new Random().nextInt(ips.length)];
                        }
                        if (!TextUtils.isEmpty(ip)) {
                            SharedPrefUtils.setString(SharedPrefUtils.FILE_HTTP_DNS, host, ip);
                            SharedPrefUtils.setLong(SharedPrefUtils.FILE_HTTP_DNS, SharedPrefUtils.TIME_OUT + host, Long.valueOf(arr[1]) * 1000 + timeCur);
                            return ip;
                        }
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            return SharedPrefUtils.getString(SharedPrefUtils.FILE_HTTP_DNS, host, host);
        }
        // 如果请求httpdns失败，则缓存没解析的host 1分钟
        SharedPrefUtils.setString(SharedPrefUtils.FILE_HTTP_DNS, host, host);
        SharedPrefUtils.setLong(SharedPrefUtils.FILE_HTTP_DNS, SharedPrefUtils.TIME_OUT + host, 60 * 1000 + timeCur);
        return SharedPrefUtils.getString(SharedPrefUtils.FILE_HTTP_DNS, host, host);
    }

    private static String executeHttpGet(String str) {
        String result = null;
        HttpURLConnection connection = null;
        InputStreamReader in = null;
        try {
            URL url = new URL(str);
            connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(connectTimeout);
            in = new InputStreamReader(connection.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(in);
            StringBuffer strBuffer = new StringBuffer();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                strBuffer.append(line);
            }
            result = strBuffer.toString();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
        return result;
    }

    public static boolean isNetworkConnected(Context context) {
        if (context != null) {
            ConnectivityManager mConnectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();
            if (mNetworkInfo != null) {
                return mNetworkInfo.isAvailable();
            }
        }
        return false;
    }

    /**
     * @return 判断网络是否可用，并返回网络类型，ConnectivityManager.TYPE_WIFI，ConnectivityManager.TYPE_MOBILE，不可用返回-1
     */
    public static final int getNetWorkConnectionType(Context context) {
        final ConnectivityManager connectivityManager = (ConnectivityManager) context.
                getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo wifiNetworkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        final NetworkInfo mobileNetworkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);


        if (wifiNetworkInfo != null && wifiNetworkInfo.isAvailable()) {
            return ConnectivityManager.TYPE_WIFI;
        } else if (mobileNetworkInfo != null && mobileNetworkInfo.isAvailable()) {
            return ConnectivityManager.TYPE_MOBILE;
        } else {
            return -1;
        }

    }

    public static String getAPNType() {
        //结果返回值
        String netType = null;
        //获取手机所有连接管理对象
        ConnectivityManager manager = (ConnectivityManager) TheLApp.context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (manager == null) {
            return null;
        }

        //获取NetworkInfo对象
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        //NetworkInfo对象为空 则代表没有网络
        if (networkInfo == null) {
            return null;
        }
        //否则 NetworkInfo对象不为空 则获取该networkInfo的类型
        int nType = networkInfo.getType();
        if (nType == ConnectivityManager.TYPE_WIFI) {
            //WIFI
            netType = "WIFI";
        } else if (nType == ConnectivityManager.TYPE_MOBILE) {
            int nSubType = networkInfo.getSubtype();
            TelephonyManager telephonyManager = (TelephonyManager) TheLApp.context.getSystemService(Context.TELEPHONY_SERVICE);
            //3G   联通的3G为UMTS或HSDPA 电信的3G为EVDO
            if (nSubType == TelephonyManager.NETWORK_TYPE_LTE
                    && !telephonyManager.isNetworkRoaming()) {
                netType = "4G";
            } else if (nSubType == TelephonyManager.NETWORK_TYPE_UMTS
                    || nSubType == TelephonyManager.NETWORK_TYPE_HSDPA
                    || nSubType == TelephonyManager.NETWORK_TYPE_EVDO_0
                    && !telephonyManager.isNetworkRoaming()) {
                netType = "3G";
                //2G 移动和联通的2G为GPRS或EGDE，电信的2G为CDMA
            } else if (nSubType == TelephonyManager.NETWORK_TYPE_GPRS
                    || nSubType == TelephonyManager.NETWORK_TYPE_EDGE
                    || nSubType == TelephonyManager.NETWORK_TYPE_CDMA
                    && !telephonyManager.isNetworkRoaming()) {
                netType = "2G";
            } else {
                netType = "2G";
            }
        }
        return netType;
    }


}
