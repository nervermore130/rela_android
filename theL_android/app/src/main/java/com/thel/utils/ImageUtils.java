package com.thel.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.media.ExifInterface;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

import androidx.annotation.Nullable;

import com.facebook.common.executors.CallerThreadExecutor;
import com.facebook.common.references.CloseableReference;
import com.facebook.datasource.DataSource;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.datasource.BaseBitmapDataSubscriber;
import com.facebook.imagepipeline.image.CloseableImage;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.ImgShareBean;
import com.thel.bean.SharePosterBean;
import com.thel.constants.TheLConstants;
import com.thel.modules.post.MomentPosterActivity;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * 图片处理
 *
 * @author Setsail
 */
public class ImageUtils {

    private static final String TAG = ImageUtils.class.getSimpleName();

    /**
     * 读取图片属性：旋转的角度
     *
     * @param path 图片绝对路径
     * @return degree旋转的角度
     */
    public static int readPictureDegree(String path) {
        int degree = 0;
        if (TextUtils.isEmpty(path)) {
            return degree;
        }
        try {
            ExifInterface exifInterface = new ExifInterface(path);
            int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    degree = 90;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    degree = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    degree = 270;
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return degree;
    }

    /*
     * 旋转图片
     *
     * @param angle
     *
     * @param bitmap
     *
     * @return Bitmap
     */
    public static Bitmap rotateImageView(int angle, Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        // 旋转图片 动作
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        // 创建新的图片
        Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        return resizedBitmap;
    }

    /**
     * 生成图片名称
     *
     * @return
     */
    public static String getPicName() {
        String imageName = "img_" + System.currentTimeMillis() + ".jpg";
        return imageName;
    }

    /**
     * 返回处理好的图片的文件路径
     *
     * @param imagePath
     * @param filePath
     * @param fileName
     * @param needWaterMark
     * @param outputWidth
     * @param outputHeight
     * @param quality
     * @return
     */
    public static String handlePhoto1(String imagePath, String filePath, String fileName, boolean needWaterMark, int outputWidth, int outputHeight, int quality, int ratio) {
        File fileDir = new File(filePath);
        if (!fileDir.exists()) {
            fileDir.mkdirs();
        }
        if (outputWidth == 0 || outputHeight == 0) {
            return "";
        }

        int degree = ImageUtils.readPictureDegree(imagePath);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inJustDecodeBounds = true; // 只读入图边界
        BitmapFactory.decodeFile(imagePath, options);

        int zoom = 1;

        if (options.outWidth != 0 && options.outHeight != 0) {
            // 如果长宽比例大于ratio，则不能上传
            if (options.outWidth / options.outHeight >= ratio || options.outHeight / options.outWidth >= ratio || options.outHeight < 320 || options.outWidth < 320) {
                DialogUtil.showToastShort(TheLApp.context, TheLApp.getContext().getString(R.string.uploadimage_activity_size_error));
                return "";
            }
        }

        // 先判断照片的横竖
        boolean isLandscape = options.outWidth >= options.outHeight;
        if (isLandscape) {
            zoom = (int) (options.outWidth / (float) outputWidth);
        } else {
            zoom = (int) (options.outHeight / (float) outputHeight);
        }

        if (zoom < 1) {
            return imagePath;
        } else {
            options.inSampleSize = zoom;
            options.inJustDecodeBounds = false; // 读取全部图信息
            Bitmap bitmap = BitmapFactory.decodeFile(imagePath, options);// 图片的压缩

            /**
             * 把图片旋转为正的方向
             */
            Bitmap newBitmap = ImageUtils.rotateImageView(degree, bitmap);
            if (needWaterMark) {
                // 打水印
                /** 暂时将水印注掉，等iOS改好再打水印，暂时像iOS一样只在显示的时候盖上水印 **/
                // createWatermark(newBitmap);
            }

            String handlePhotoPath = filePath + fileName;

            if (savePic(newBitmap, handlePhotoPath, quality)) {
                return handlePhotoPath;
            } else {
                return "";
            }
        }
    }

    /**
     * 处理本地图片，根据需要压缩后保存到指定路径
     *
     * @param imagePath
     * @param filePath
     * @param fileName
     * @param outputWidth
     * @param outputHeight
     * @param quality
     * @return
     */
    public static boolean handleLocalPic(String imagePath, String filePath, String fileName, int outputWidth, int outputHeight, int quality, boolean limit) {
        File fileDir = new File(filePath);
        if (!fileDir.exists()) {
            fileDir.mkdirs();
        }
        if (outputWidth == 0 || outputHeight == 0) {
            return false;
        }

        int degree = ImageUtils.readPictureDegree(imagePath);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inJustDecodeBounds = true; // 只读入图边界
        BitmapFactory.decodeFile(imagePath, options);

        int zoom = 1;

        if (options.outWidth != 0 && options.outHeight != 0) {
            if (limit && (options.outWidth / options.outHeight >= 3 || options.outHeight / options.outWidth >= 3 || options.outHeight < 320 || options.outWidth < 320)) {
                return false;
            }
        }

        // 先判断照片的横竖
        boolean isLandscape = options.outWidth >= options.outHeight;
        if (isLandscape) {
            zoom = (int) (options.outWidth / (float) outputWidth);
        } else {
            zoom = (int) (options.outHeight / (float) outputHeight);
        }
        // int zoomWidth = (int) (options.outWidth / (float) outputWidth);
        // int zoomHeight = (int) (options.outHeight / (float) outputHeight);
        //
        // zoom = Math.max(zoomWidth, zoomHeight);

        if (zoom < 1) {
            zoom = 1;
        }

        options.inSampleSize = zoom;
        options.inJustDecodeBounds = false; // 读取全部图信息
        Bitmap bitmap = BitmapFactory.decodeFile(imagePath, options);// 图片的压缩

        /**
         * 把图片旋转为正的方向
         */
        Bitmap newBitmap = ImageUtils.rotateImageView(degree, bitmap);

        String handlePhotoPath = filePath + fileName;

        return savePic(newBitmap, handlePhotoPath, quality);
    }

    /**
     * 保存图片
     *
     * @param bitmap
     * @param filePath
     * @param quality
     * @return
     */
    public static boolean savePic(Bitmap bitmap, String filePath, int quality) {

        L.d("MomentPoster", " bitmap : " + bitmap);

        L.d("MomentPoster", " filePath : " + filePath);

        L.d("MomentPoster", " quality : " + quality);

        try {
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, bos);
            bos.flush();
            bos.close();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (bitmap != null && !bitmap.isRecycled()) {
                bitmap.recycle();
            }
        }
        return true;
    }

    /**
     * 判断图片是不是符合规格
     *
     * @param imagePath
     * @param minWidth
     * @param minHeight
     * @return
     */
    public static boolean isImageLegal(String imagePath, int minWidth, int minHeight) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inJustDecodeBounds = true; // 只读入图边界
        BitmapFactory.decodeFile(imagePath, options);
        options.inJustDecodeBounds = false;

        if (options.outWidth != 0 && options.outHeight != 0) {
            return options.outWidth >= minWidth && options.outHeight >= minHeight;
//            return options.outWidth / options.outHeight < 3 && options.outHeight / options.outWidth < 3 && options.outHeight >= minHeight && options.outWidth >= minWidth;
        }

        return true;
    }

    /**
     * 获取缩放配置
     *
     * @param width
     * @param heigth
     * @param imagePath
     * @return
     */
    public static BitmapFactory.Options getZoomOptionBySize(int width, int heigth, String imagePath) {
        int zoom = 1;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true; // 只读入图边界
        BitmapFactory.decodeFile(imagePath, options);
        // 先判断照片的横竖
        boolean isLandscape = options.outWidth >= options.outHeight;
        if (isLandscape) {
            zoom = (int) (options.outWidth / (float) width);
        } else {
            zoom = (int) (options.outHeight / (float) heigth);
        }
        options.inSampleSize = zoom;
        options.inJustDecodeBounds = false; // 读取全部图信息
        return options;
    }

    /**
     * 给图片打水印
     *
     * @param src
     */
    public static void createWatermark(Bitmap src) {
        if (src == null) {
            return;
        }
        int srcWidth = src.getWidth();
        int srcHeight = src.getHeight();

        Bitmap watermark = BitmapFactory.decodeResource(TheLApp.context.getResources(), R.mipmap.icn_watermark);
        int waterWidth = watermark.getWidth();
        int waterHeight = watermark.getHeight();
        Canvas cv = new Canvas(src);
        cv.drawBitmap(src, 0, 0, null);// 在0,0坐标开始画入src
        /*
         * Paint paint = new Paint(); paint.setColor(Color.RED);
         */
        if (watermark != null) {
            // TODO 还没调整水印位置
            cv.drawBitmap(watermark, srcWidth - waterWidth, srcHeight - waterHeight, null);// 在src的右下解画入水印图片
            // cv.drawText("HELLO",srcWidth-waterWidth,srcHeight-waterHeight,
            // paint);//这是画入水印文字，在画文字时，需要指定paint
        }
        cv.save();// 保存
        cv.restore();// 存储
        watermark.recycle();
    }

    /**
     * 将图片截取为圆角图片
     *
     * @param bitmap 原图片
     * @param ratio  截取比例，如果是8，则圆角半径是宽高的1/8，如果是2，则是圆形图片
     * @return 圆角矩形图片
     */
    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, float ratio) {

        if (bitmap == null) {
            return null;
        }

        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        canvas.drawRoundRect(rectF, bitmap.getWidth() / ratio, bitmap.getHeight() / ratio, paint);

        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        bitmap.recycle();
        return output;
    }

    /**
     * 测量图片高度
     *
     * @param path
     * @return
     */
    public static int measurePicHeight(String path) {
        Bitmap bitmap = null;
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();

            /**
             * 最关键在此，把options.inJustDecodeBounds = true;
             * 这里再decodeFile()，返回的bitmap为空，但此时调用options.outHeight时，已经包含了图片的高了
             */
            options.inJustDecodeBounds = true;
            bitmap = BitmapFactory.decodeFile(path, options); // 此时返回的bitmap为null
            /**
             *options.outHeight为原始图片的高
             */
            return options.outHeight;
        } catch (Exception e) {
        } finally {
            if (bitmap != null && !bitmap.isRecycled()) {
                bitmap.recycle();
            }
        }
        return 0;
    }

    /**
     * 测量图片高度
     *
     * @param path
     * @return
     */
    public static List<Integer> measurePicWidthAndHeight(String path) {
        List<Integer> result = new ArrayList<Integer>();
        Bitmap bitmap = null;
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();

            /**
             * 最关键在此，把options.inJustDecodeBounds = true;
             * 这里再decodeFile()，返回的bitmap为空，但此时调用options.outHeight时，已经包含了图片的高了
             */
            options.inJustDecodeBounds = true;
            bitmap = BitmapFactory.decodeFile(path, options); // 此时返回的bitmap为null
            /**
             *options.outHeight为原始图片的高
             */
            result.add(options.outWidth);
            result.add(options.outHeight);
        } catch (Exception e) {
        } finally {
            if (bitmap != null && !bitmap.isRecycled()) {
                bitmap.recycle();
            }
        }
        return result;
    }

    /**
     * 计算图片文件的大小
     *
     * @param path
     * @return
     */
    public static long getPicFileSize(String path) {
        File f = new File(path);
        if (f.exists() && f.isFile()) {
            return f.length();
        } else {
            return 0;
        }
    }

    /**
     * 利用七牛的云服务剪裁图片尺寸
     *
     * @param url
     * @param width
     * @param height
     * @return
     */
    public static String buildNetPictureUrl(String url, float width, float height) {
        if (TextUtils.isEmpty(url)) {
            return "";
        }
        if (url.contains("file://") || url.contains(".gif") || url.contains(".webp")) {// 本地图片，返回原来的url
            return url;
        }
        try {
            url = url.replaceAll("-wh?\\d+($|\\?)", "$1");
            if (url.contains("?imageView2")) {
                return url.substring(0, url.indexOf("?imageView2")) + "?imageView2/1/w/" + (int) width + "/h/" + (int) height + (url.contains(".gif") ? "" : "/format/webp");// 七牛gif图后面不能加webp
            } else {
                return url + "?imageView2/1/w/" + (int) width + "/h/" + (int) height + (url.contains(".gif") ? "" : "/format/webp");// 七牛gif图后面不能加webp
            }
        } catch (Exception e) {
            return url;
        }
    }

    /**
     * 不缩放，只加载webp格式
     *
     * @param url
     * @return
     */
    public static String buildNetPictureUrl(String url) {
        if (TextUtils.isEmpty(url)) {
            return "";
        }
        if (url.contains("file://") || url.contains(".gif")) {// 本地图片，返回原来的url
            return url;
        }
        try {
            url = url.replaceAll("-wh?\\d+($|\\?)", "$1");
            if (url.contains("?imageView2")) {
                return url.substring(0, url.indexOf("?imageView2")) + "?imageView2/2" + (url.contains(".gif") ? "" : "/format/webp");
            } else {
                return url + "?imageView2/2" + (url.contains(".gif") ? "" : "/format/webp");
            }
        } catch (Exception e) {
            return url;
        }
    }

    /**
     * 添加水印
     *
     * @param context      上下文
     * @param bitmap       原图
     * @param markText     水印文字用户热拉id
     * @param markBitmapId 水印图片
     * @return bitmap      打了水印的图
     */

    public static Bitmap createWatermark(Context context, Bitmap bitmap, String markText, int markBitmapId) {
        int markBitmapHeight = 0;
        int markBitmapWidth = 0;
        int margin_right = 0;
        int margin_top = 0;
        int margin = 0;
        float scale = 1f;
        Paint mPaint;

        // 获取图片的宽高
        int bitmapWidth = bitmap.getWidth();
        int bitmapHeight = bitmap.getHeight();

        // 创建一个和图片一样大的背景图
        Canvas canvas = new Canvas(bitmap);
        // 画背景图
        canvas.drawBitmap(bitmap, 0, 0, null);
        //------------开始绘制图片-------------------------
        if (markBitmapId != 0) {
            // 载入水印图片
            Bitmap markBitmap = BitmapFactory.decodeResource(TheLApp.context.getResources(), markBitmapId);
            //   Bitmap shadowBitmap = markBitmap.extractAlpha();
            // 如果图片的大小小于水印的3倍，就不添加水印

            markBitmapWidth = markBitmap.getWidth();
            markBitmapHeight = markBitmap.getHeight();

            margin_right = SizeUtils.dip2px(TheLApp.context, 7);
            margin_top = SizeUtils.dip2px(TheLApp.context, 7);

            margin = SizeUtils.dip2px(TheLApp.context, 0);
            //缩放图片，使其宽/高 小于/等于 bitmapwidth/3或者 bitmapheight/3;
            if (markBitmapWidth > bitmapWidth / 7f || markBitmapHeight > bitmapHeight / 7f) {
                scale = Math.min(bitmapWidth / (7f * markBitmapWidth), bitmapHeight / (7f * markBitmapHeight));
                final Matrix matrix = new Matrix();
                matrix.postScale(scale, scale);
                markBitmap = Bitmap.createBitmap(markBitmap, 0, 0, markBitmapWidth, markBitmapHeight, matrix, true);
            }
            markBitmapWidth = markBitmap.getWidth();
            markBitmapHeight = markBitmap.getHeight();

            canvas.drawBitmap(markBitmap, bitmapWidth - markBitmapWidth - margin_right, margin_top, null);
        }


        //-------------开始绘制文字-------------------------------

        Log.d("ImageUtils", " markText : " + markText);

        if (!TextUtils.isEmpty(markText)) {
            // 创建画笔
            mPaint = new Paint();
            // 文字矩阵区域
            Rect textBounds = new Rect();

            final float testTextSize = 80f;

            // Get the bounds of the text, using our testTextSize.
            mPaint.setTextSize(testTextSize);
            mPaint.getTextBounds(markText, 0, markText.length(), textBounds);

            // Calculate the desired size as a proportion of our testTextSize.
            float desiredTextSize = testTextSize * scale;
            // 水印的字体大小
            mPaint.setTextSize(desiredTextSize);

            Typeface font = Typeface.create(Typeface.SANS_SERIF, Typeface.BOLD);
            mPaint.setTypeface(font);
            // 文字阴影
            mPaint.setShadowLayer(0.6f, 0f, 1f, Color.DKGRAY);
            // 抗锯齿
            mPaint.setAntiAlias(true);
            // 水印的区域
            mPaint.getTextBounds(markText, 0, markText.length(), textBounds);
            final Paint.FontMetricsInt fontMetricsInt = mPaint.getFontMetricsInt();
            // 水印的颜色
            mPaint.setColor(Color.WHITE);
            // 当图片大小小于文字水印大小的3倍的时候，不绘制水印

//            if (textBounds.width() > bitmapWidth / 3 || textBounds.height() > bitmapHeight / 3) {
//                return bitmap;
//            }

            int text_start = bitmapWidth - margin_right - textBounds.width() - markBitmapWidth / 18;//水印图片右侧好像有20/360的留白
            int text_base = margin_top + markBitmapHeight + margin - fontMetricsInt.ascent;

            // 画文字
            canvas.drawText(markText, text_start, text_base, mPaint);
        }

        //保存所有元素
        canvas.save();
        canvas.restore();

        return bitmap;
    }

}
