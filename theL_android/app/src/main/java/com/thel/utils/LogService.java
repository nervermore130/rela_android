package com.thel.utils;


import android.annotation.SuppressLint;
import android.text.TextUtils;

import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UploadManager;
import com.thel.app.TheLApp;
import com.thel.bean.user.UploadTokenBean;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static android.os.Process.THREAD_PRIORITY_BACKGROUND;
import static android.os.Process.setThreadPriority;

/**
 * 系统log上传
 * {@link L#DEBUG} 使用需要关闭L.java的log打印
 */
public class LogService implements Runnable {

    @SuppressLint("CheckResult")
    @Override
    public void run() {
        setThreadPriority(THREAD_PRIORITY_BACKGROUND);

        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-HH:mm:ss");
        String date = sdf.format(Calendar.getInstance().getTime());
        String uploadPath = ShareFileUtils.getString(ShareFileUtils.ID, "") + "/" + date + "-log.html";

        try {
            File localFile = new File(TheLApp.getContext().getCacheDir().getPath(), date + "-log.html");

            L.d("LogService", "抓取缓存日志开始");

            //抓取当前的缓存日志
            Process process = Runtime.getRuntime().exec("logcat -t 1000 time *:V com.thel");
//            Process process = Runtime.getRuntime().exec("logcat -v time com.thel");
            //获取输入流
            BufferedReader buffRead = new BufferedReader(new InputStreamReader(process.getInputStream()));
            //清除是为了下次抓取不会从头抓取
//            Runtime.getRuntime().exec(clearLog.toArray("logcat -c");
            L.d("LogService", "抓取缓存日志完成");

            String logMsg = null;

            StringBuilder builder = new StringBuilder();

            while ((logMsg = buffRead.readLine()) != null) {         //循环读取每一行

                if (logMsg.contains(" I ")) {
                    builder.append("<font color=\"black\">");
                } else if (logMsg.contains(" D ")) {
                    builder.append("<font color=\"green\">");
                } else if (logMsg.contains(" E ")) {
                    builder.append("<font color=\"#fc0d1b\">");
                } else if (logMsg.contains(" W ")) {
                    builder.append("<font color=\"#FFD306\">");
                } else {
                    builder.append("<font color=\"black\">");
                }

                builder.append(logMsg);
                builder.append("</font><br> ");

            }

            String log = builder.toString();

            L.d("LogService", " error log => " + log);

            FileOutputStream outputStream = new FileOutputStream(localFile, true);
            outputStream.write(log.getBytes());
            outputStream.close();

            L.d("LogService", "开始上传");
            RequestBusiness.getInstance().getUploadToken(System.currentTimeMillis() + "", "rela-client-log", uploadPath).onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<UploadTokenBean>() {
                @Override
                public void onNext(UploadTokenBean uploadTokenBean) {
                    super.onNext(uploadTokenBean);
                    L.d("LogService", "onNext uploadTokenBean-->" + uploadTokenBean);
                    if (!TextUtils.isEmpty(uploadTokenBean.data.uploadToken)) {
                        UploadManager uploadManager = new UploadManager();
                        uploadManager.put(localFile, uploadPath, uploadTokenBean.data.uploadToken, (UpCompletionHandler) (key, info, response) -> {
                            L.d("LogService", "info-->" + info);
                            if (info != null && info.statusCode == 200 && uploadPath.equals(key)) {
                                ShareFileUtils.setBoolean(ShareFileUtils.CRASH_LOG_UPLOAD, false);
                                L.d("LogService", "success--> " + key);
                            }
                        }, null);
                    }
                }

                @Override
                public void onError(Throwable t) {
                    super.onError(t);
                    t.printStackTrace();
                    L.d("LogService", "onError");
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
