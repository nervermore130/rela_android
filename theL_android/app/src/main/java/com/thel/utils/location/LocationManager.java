package com.thel.utils.location;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.thel.BuildConfig;
import com.thel.app.TheLApp;
import com.thel.utils.L;
import com.thel.utils.LocationUtils;

public class LocationManager {

    private static final String TAG = "LocationManager";

    private LocationListener mLocationListener;

    public LocationManager(LocationListener listener) {

        this.mLocationListener = listener;

        initLocation();

    }

    public void requestLocation() {
        initLocation();
    }

    private void initLocation() {
        if (BuildConfig.APPLICATION_ID.equals("com.thel")) {
            initAmapLocation();
        } else {
            initGoogleLocation();
        }
    }

    private void initAmapLocation() {

        L.d(TAG, "--------initAmapLocation--------");

        final AMapLocationClient mLocationClient = new AMapLocationClient(TheLApp.getContext());
        mLocationClient.setLocationListener(new AMapLocationListener() {
            @Override
            public void onLocationChanged(AMapLocation aMapLocation) {
                if (aMapLocation != null && aMapLocation.getErrorCode() == 0) {
                    //获取位置信息
                    LocationUtils.saveLocation(aMapLocation.getLatitude(), aMapLocation.getLongitude(), aMapLocation.getCity());

                    if (mLocationListener != null) {
                        mLocationListener.onLocation(aMapLocation.getLatitude(), aMapLocation.getLongitude());
                    }

                    mLocationClient.stopLocation();
                    mLocationClient.onDestroy();
                } else {
                    if (mLocationListener != null) {
                        mLocationListener.onFailed();
                    }
                }

            }
        });
        final AMapLocationClientOption mLocationOption = new AMapLocationClientOption();
        mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Battery_Saving);
        mLocationClient.setLocationOption(mLocationOption);
        mLocationClient.startLocation();
    }

    private void initGoogleLocation() {

        L.d(TAG, "--------initGoogleLocation--------");

        if (ActivityCompat.checkSelfPermission(TheLApp.context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(TheLApp.context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

    }

}
