package com.thel.utils;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.airbnb.lottie.LottieAnimationView;
import com.thel.base.BaseDataBean;
import com.thel.constants.TheLConstants;
import com.thel.db.DBUtils;
import com.thel.db.table.message.MsgTable;
import com.thel.imp.wink.WinkUtils;
import com.thel.manager.ChatServiceManager;
import com.thel.modules.main.messages.bean.MsgBean;
import com.thel.modules.main.messages.utils.MsgUtils;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by the L on 2017/3/14.
 * 针对View 的一些动画操作 类（写在activity里面太长站篇幅）
 */

public class AnimUtils {
    public static void sendGiftWinkAnim(final ViewGroup parent, final View view, int width, int height, int left, int top, final AnimListener listener) {
        final int translation_duration = 500;
        final float bigScale = 5f;
        final float centerScale = 2f;
        final int centerX = parent.getMeasuredWidth() / 2;
        final int centerY = parent.getMeasuredHeight() / 2;
        final int tX = centerX - left - width / 2;
        final int tY = centerY - top - height / 2;
        view.animate().scaleX(centerScale).scaleY(centerScale).translationX(tX).translationY(tY).setDuration(translation_duration).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                if (listener != null) {
                    listener.animStart(parent, view);
                }
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                JumpGiftWinkAnim(parent, view, bigScale, centerScale, listener);
                view.animate().setListener(null);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    private static void JumpGiftWinkAnim(final ViewGroup parent, final View view, float bigScale, float centerScale, final AnimListener listener) {
        final int jump_duration = 1500;
        final int alpha_duration = 300;
        final TimeInterpolator interpolator = new LinearInterpolator();
        final float[] scaleT = {centerScale, bigScale, centerScale, bigScale, centerScale};
        final ObjectAnimator scaleX = ObjectAnimator.ofFloat(view, View.SCALE_X, scaleT);
        final ObjectAnimator scaleY = ObjectAnimator.ofFloat(view, View.SCALE_Y, scaleT);
        scaleX.setDuration(jump_duration);
        scaleY.setDuration(jump_duration);
        scaleX.setInterpolator(interpolator);
        scaleY.setInterpolator(interpolator);
        final ObjectAnimator alpha = ObjectAnimator.ofFloat(view, View.ALPHA, 1, 0);
        alpha.setDuration(alpha_duration);
        alpha.setInterpolator(interpolator);
        final AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(scaleX).with(scaleY);
        animatorSet.play(alpha).after(scaleX);
        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (listener != null) {
                    listener.animEnd(parent, view);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        animatorSet.start();

    }

    public static void appearFollow(final ImageView lin_follow) {

        final ViewGroup.LayoutParams layoutParams = lin_follow.getLayoutParams();
        final int delta = layoutParams.width;
        ObjectAnimator transAnim = ObjectAnimator.ofFloat(lin_follow, "translationX", 0, delta);//向右移动
        transAnim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                layoutParams.width = 0;
                lin_follow.setLayoutParams(layoutParams);
                lin_follow.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                layoutParams.width = delta;
                lin_follow.setLayoutParams(layoutParams);
                lin_follow.setVisibility(View.VISIBLE);
                lin_follow.setTranslationX(lin_follow.getTranslationX() - delta);
            }

            @Override
            public void onAnimationCancel(Animator animator) {
                layoutParams.width = delta;
                lin_follow.setLayoutParams(layoutParams);
                lin_follow.setVisibility(View.VISIBLE);
                lin_follow.setTranslationX(lin_follow.getTranslationX() - delta);
            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        transAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float currentValue = (float) animation.getAnimatedValue();
                layoutParams.width = Math.abs((int) currentValue);
                lin_follow.setLayoutParams(layoutParams);
                lin_follow.setVisibility(View.INVISIBLE);
            }
        });
        transAnim.setDuration(200);
        transAnim.start();
    }

    public static void disappearFollow(final ImageView lin_follow) {
        lin_follow.animate().alpha(0).setDuration(2000).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                lin_follow.setVisibility(View.INVISIBLE);
                animateFollow(lin_follow);

            }

            @Override
            public void onAnimationCancel(Animator animation) {
                lin_follow.setVisibility(View.INVISIBLE);
                animateFollow(lin_follow);
            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    private static void animateFollow(final ImageView lin_follow) {
        final ViewGroup.LayoutParams layoutParams = lin_follow.getLayoutParams();
        final int delta = layoutParams.width;
        ObjectAnimator transAnim = ObjectAnimator.ofFloat(lin_follow, "translationX", delta, 0);//向左移动
        transAnim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                layoutParams.width = delta;
                lin_follow.setLayoutParams(layoutParams);
                lin_follow.setVisibility(View.GONE);

            }

            @Override
            public void onAnimationCancel(Animator animator) {
                layoutParams.width = delta;
                lin_follow.setLayoutParams(layoutParams);
                lin_follow.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        transAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float currentValue = (float) animation.getAnimatedValue();
                layoutParams.width = Math.abs((int) currentValue);
                lin_follow.setLayoutParams(layoutParams);
            }
        });
        transAnim.setDuration(200);
        transAnim.start();

    }

    public static void disappearSendEdit(final LinearLayout send_edit_msg) {
        final float alpha = send_edit_msg.getAlpha();
        send_edit_msg.animate().alpha(0).setDuration(300).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                send_edit_msg.setVisibility(View.GONE);
                send_edit_msg.setAlpha(alpha);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                send_edit_msg.setVisibility(View.GONE);
                send_edit_msg.setAlpha(alpha);
            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    public static void setWnkCtrl(final ImageView img_wink, final LottieAnimationView img_sendwink, final String userId, final String nickName, final String avatar, Context context) {
        L.d("AnimUtils", "userId=" + userId + ",nickName=" + nickName + ",avatar=" + avatar);

        if (userId.equals(DBUtils.getStrangerTableName())) {
            return;
        }

        img_wink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sendWink(img_wink, img_sendwink, userId, nickName, avatar);
                FireBaseUtils.uploadGoogle(TheLConstants.FireBaseConstant.WINK, context);

            }
        });
    }

    public static void setWnkCtrl(final ImageView img_wink, final LottieAnimationView img_sendwink, final MsgTable msgTable) {
        img_wink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (msgTable == null || msgTable.userId.equals(DBUtils.getStrangerTableName())) {
                    return;
                }

                sendWink(img_wink, img_sendwink, msgTable);

            }
        });
    }

    private static void requestSendWink(final String userId) {

        RequestBusiness.getInstance()
                .sendWink(userId + "", TheLConstants.WINK_TYPE.get(TheLConstants.WINK_TYPE_WINK))
                .onBackpressureDrop().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new InterceptorSubscribe<BaseDataBean>() {
                    @Override
                    public void onNext(BaseDataBean data) {
                        super.onNext(data);
                        if (data != null && data.errcode.equals("10102")) {//已经挤过眼了
                            WinkUtils.addMyTodayWinkUser(userId);
                        }

                    }
                });
    }

    public static void sendWink(final ImageView img_wink, final LottieAnimationView img_sendwink, final String userId, final String nickName, final String avatar) {

        ViewUtils.preventViewMultipleClick(img_wink, 1000);

        L.d("AnimUtils", "sendWink");

        img_sendwink.setVisibility(View.VISIBLE);
        img_sendwink.setAnimation("wink_android1.json");
        img_sendwink.addAnimatorListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                img_wink.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {

                L.d("AnimUtils", " wink onAnimationEnd ");
                img_wink.setVisibility(View.INVISIBLE);
                img_sendwink.setVisibility(View.GONE);
                WinkUtils.addMyTodayWinkUser(userId);

                requestSendWink(userId);

            }

            @Override
            public void onAnimationCancel(Animator animation) {

                L.d("AnimUtils", " wink onAnimationEnd ");
//                img_wink.setVisibility(View.INVISIBLE);
//                img_sendwink.setVisibility(View.GONE);
//                WinkUtils.addMyTodayWinkUser(userId);


            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        img_sendwink.playAnimation();
    }

    private static void sendWink(final ImageView img_wink, final LottieAnimationView img_sendwink, final MsgTable msgTable) {

        ViewUtils.preventViewMultipleClick(img_wink, 1000);

        L.d("AnimUtils", "sendWink");

        img_sendwink.setVisibility(View.VISIBLE);
        img_sendwink.setAnimation("wink_android1.json");
        img_sendwink.addAnimatorListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                img_wink.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {

                L.d("AnimUtils", " wink onAnimationEnd ");

                img_sendwink.setVisibility(View.GONE);

                WinkUtils.addMyTodayWinkUser(msgTable.userId);

                requestSendWink(msgTable.userId);

            }

            @Override
            public void onAnimationCancel(Animator animation) {

                L.d("AnimUtils", " wink onAnimationEnd ");
//
//                img_sendwink.setVisibility(View.GONE);
//
//                WinkUtils.addMyTodayWinkUser(msgTable.userId);
//
//                requestSendWink(msgTable.userId);

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        img_sendwink.playAnimation();
    }

    public interface AnimListener {
        /**
         * 动画开始监听
         */
        void animStart(ViewGroup parent, View view);

        /**
         * 动画结束监听
         */
        void animEnd(ViewGroup parent, View view);
    }


}
