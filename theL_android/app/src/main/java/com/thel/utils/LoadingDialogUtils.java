package com.thel.utils;

import android.app.Activity;
import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.thel.R;
import com.thel.ui.LoadingDialogImpl;

/**
 * Created by the L on 2017/2/3.
 */

public class LoadingDialogUtils implements LoadingDialogImpl {
    private final Activity mContext;
    Dialog loadingDialog;
    Dialog noBackLoadingDialog;

    public LoadingDialogUtils(Activity activity) {
        mContext = activity;
    }

    @Override
    public void showLoading() {

        try {

            if (mContext != null && !mContext.isDestroyed()) {

                if (loadingDialog == null) {
                    loadingDialog = createDialog(mContext, true);
                }
                if (loadingDialog != null && !loadingDialog.isShowing() && (noBackLoadingDialog == null || !noBackLoadingDialog.isShowing())) {
                    loadingDialog.show();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void showLoadingNoBack() {
        if (noBackLoadingDialog == null && mContext != null && !mContext.isFinishing()) {
            noBackLoadingDialog = createDialog(mContext, false);
        }
        if (noBackLoadingDialog != null && !noBackLoadingDialog.isShowing() && (loadingDialog == null || !loadingDialog.isShowing())) {
            noBackLoadingDialog.show();
        }
    }

    @Override
    public void closeDialog() {
        if (loadingDialog != null && loadingDialog.isShowing()) {
            try {
                loadingDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (noBackLoadingDialog != null && noBackLoadingDialog.isShowing()) {
            try {
                noBackLoadingDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void destroyDialog() {
        closeDialog();
        loadingDialog = null;
        noBackLoadingDialog = null;
    }

    protected Dialog createDialog(Activity activity, boolean cancelable) {
        View view = LayoutInflater.from(activity).inflate(R.layout.the_l_loading_dialog, null);
        Dialog loadingDialog = new Dialog(activity, R.style.CustomDialog);
        loadingDialog.setContentView(view, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        loadingDialog.setCancelable(cancelable);
        loadingDialog.setCanceledOnTouchOutside(cancelable);
        return loadingDialog;
    }
}
