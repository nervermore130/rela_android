package com.thel.flutter.bridge;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.SparseArray;

import androidx.core.content.ContextCompat;

import com.growingio.android.sdk.collection.GrowingIO;
import com.meituan.android.walle.WalleChannelReader;
import com.thel.BuildConfig;
import com.thel.ContactBeanNew;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.ContactBean;
import com.thel.bean.ImgShareBean;
import com.thel.bean.SharePosterBean;
import com.thel.bean.analytics.AnalyticsBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.moments.MomentsCheckBean;
import com.thel.bean.user.UserInfoBean;
import com.thel.constants.TheLConstants;
import com.thel.db.table.message.MsgTable;
import com.thel.flutter.FlutterRouterPathConstants;
import com.thel.flutter.FlutterViewContainerActivity;
import com.thel.flutter.IFlutterPush;
import com.thel.flutter.OnActivityResultCallback;
import com.thel.flutter.model.AtUserModel;
import com.thel.flutter.model.AtUsersModel;
import com.thel.flutter.model.MeModel;
import com.thel.flutter.model.TopicTextFieldContent;
import com.thel.imp.black.BlackUtils;
import com.thel.imp.follow.FollowStatusChangedImpl;
import com.thel.imp.momentblack.MomentBlackUtils;
import com.thel.manager.ActivityManager;
import com.thel.manager.ChatServiceManager;
import com.thel.modules.live.bean.LiveFollowerUsersBean;
import com.thel.modules.live.utils.LiveUtils;
import com.thel.modules.login.login_register.WeChatLogin;
import com.thel.modules.main.MainActivity;
import com.thel.modules.main.me.bean.MyInfoBean;
import com.thel.modules.main.me.bean.RoleBean;
import com.thel.modules.main.me.match.eventcollect.collect.MatchLogUtils;
import com.thel.modules.main.messages.bean.MsgBean;
import com.thel.modules.main.messages.utils.MessagesUtils;
import com.thel.modules.main.messages.utils.MsgUtils;
import com.thel.modules.preview_image.UserInfoPhotoActivity;
import com.thel.modules.select_image.ImageBean;
import com.thel.modules.welcome.WelcomeActivity;
import com.thel.ui.LoadingDialogImpl;
import com.thel.ui.imageviewer.cropiwa.image.CropIwaResultReceiver;
import com.thel.utils.BusinessUtils;
import com.thel.utils.DeviceUtils;
import com.thel.utils.ExecutorServiceUtils;
import com.thel.utils.FireBaseUtils;
import com.thel.utils.GsonUtils;
import com.thel.utils.ImageUtils;
import com.thel.utils.L;
import com.thel.utils.MD5Utils;
import com.thel.utils.MomentUtils;
import com.thel.utils.PermissionUtil;
import com.thel.utils.PhoneUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SharedPrefUtils;
import com.thel.utils.ToastUtils;
import com.thel.utils.UrlConstants;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import me.rela.rf_s_bridge.RfSBridgeHandler;
import me.rela.rf_s_bridge.RfSBridgePlugin;
import me.rela.rf_s_bridge.RfSResultCallback;
import me.rela.rf_s_bridge.new_router.NewFlutterBoost;
import me.rela.rf_s_bridge.proto3.RfSHttpRequestField;
import me.rela.rf_s_bridge.proto3.RfSRpc;
import me.rela.rf_s_bridge.proto3.RfSRpcCommonWidget;
import me.rela.rf_s_bridge.proto3.RfSRpcDiscover;
import me.rela.rf_s_bridge.proto3.RfSRpcIndex;
import me.rela.rf_s_bridge.proto3.RfSRpcLogin;
import me.rela.rf_s_bridge.proto3.RfSRpcMe;
import me.rela.rf_s_bridge.proto3.RfSRpcRecommend;
import me.rela.rf_s_bridge.proto3.RfSRpcRelease;
import me.rela.rf_s_bridge.new_router.NewUniversalRouter;
import me.rela.rf_s_bridge.router.FlutterBoostPlugin;
import video.com.relavideolibrary.Utils.Constant;
import video.com.relavideolibrary.onRelaVideoActivityResultListener;

import static com.thel.utils.DeviceUtils.getFilePathFromUri;

/**
 * @author liuyun
 */
public class RfSBridgeHandlerFactory implements RfSBridgeHandler, OnActivityResultCallback {

    private static String TAG = "RfSBridgeHandlerFactory";

    // 图片的本地文件路径
    private String imageLocalPath;

    private static RfSBridgeHandlerFactory instance = new RfSBridgeHandlerFactory();

    private LoadingDialogImpl loadingDialog;

    private IFlutterPush mIFlutterPush;

    private RfSBridgeHandlerFactory() {

    }

    private SparseArray<RoleBean> relationshipMap = new SparseArray<RoleBean>();


    public static RfSBridgeHandlerFactory getInstance() {

        return instance;
    }

    public IFlutterPush getFlutterPushImpl() {
        if (mIFlutterPush == null) {
            mIFlutterPush = new FlutterPushImpl();
        }
        return mIFlutterPush;
    }

    @Override
    public RfSRpc.RfS_Rpc handle(RfSRpc.RfS_Rpc rpc) {
        L.d(TAG, " handle getAction: " + rpc.getMethod());

        RfSRpc.RfS_Rpc.Builder builder = RfSRpc.RfS_Rpc.newBuilder();

        builder.setMethod(rpc.getMethod());

        if (rpc.getMethod().equals("getTagList")) {
            // FIXME: flutter传来的标签列表
            String jsonString = rpc.getJsonString();
            L.i(TAG, "getTagList jsonString: " + jsonString);
            Intent intent = new Intent();
            intent.setAction(TheLConstants.BROADCAST_ACTION_SELECT_TAG);
            intent.putExtra("tagJsonString", jsonString);
            TheLApp.context.sendBroadcast(intent);
        } else if (rpc.getMethod().equals("EventNativeEnvironment")) {
            // FIXME: 从原生读取参数（API endpoint之类的）
        } else if (rpc.getMethod().equals("exit_app")) {
            try {
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(1);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (rpc.getMethod().equals("flutter_error")) {
            String jsonString = rpc.getJsonString();
            L.i(TAG, " flutter_error jsonString : " + jsonString);
            if (!TextUtils.isEmpty(jsonString)) {

                AnalyticsBean analyticsBean = new AnalyticsBean();

                analyticsBean.logType = "flutter_error";

                analyticsBean.errorReason = jsonString;

                analyticsBean.userId = UserUtils.getMyUserId();

                LiveUtils.pushLivePointLog(analyticsBean);
            }
        } else if (rpc.getMethod().equals("TabRefreshEvent")) {
            String jsonString = rpc.getJsonString();
            L.d(TAG, " TabRefreshEvent jsonString: " + jsonString);
            Intent intent = new Intent();
            intent.setAction(TheLConstants.BROADCAST_ACTION_REFRESH_NATIVE);
            intent.putExtra("index", jsonString);
            TheLApp.context.sendBroadcast(intent);
        } else if (rpc.getMethod().equals("androidChannel")) {
            String androidChannel = WalleChannelReader.getChannel(TheLApp.context);
            if (!TextUtils.isEmpty(androidChannel)) {
                return builder.setJsonString(androidChannel).build();
            }
        } else if (rpc.getMethod().equals("requestMeasureUnit")) {
            String json = "{\"isMetricHeight\":true,\"isMetricWeight\":true}";
            return builder.setJsonString(json).build();
        }
        return builder.build();
    }

    @Override
    public RfSHttpRequestField.RfS_HttpRequestField requestField() {
        String latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");

        if (TextUtils.isEmpty(latitude) || latitude.length() >= 32) {
            latitude = "0.0";
        }

        String longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");

        if (TextUtils.isEmpty(longitude) || longitude.length() >= 32) {
            longitude = "0.0";
        }

        return RfSHttpRequestField.RfS_HttpRequestField
                .newBuilder()
                .setLat(latitude)
                .setLng(longitude)
                .setKey(UserUtils.getUserKey())
                .setMobileOS("Android " + PhoneUtils.getOSVersion())
                .setClientVersion(DeviceUtils.getVersionCode(TheLApp.getContext()) + "")
                .setLanguage(DeviceUtils.getLanguageStr())
                .setDeviceId(ShareFileUtils.getString(ShareFileUtils.DEVICE_ID, UUID.randomUUID().toString()))
                .setApptype("")
                .setUserAgent(MD5Utils.getUserAgent())
                .build();
    }

    RfSResultCallback rfSResultCallback;

    @Override
    public void handleLoginAction(RfSRpcLogin.RfS_ActionsLogin action, RfSResultCallback rfSResultCallback) {
        this.rfSResultCallback = rfSResultCallback;

        L.d(TAG, " handleLoginAction getAction: " + action.getAction().toString());

        L.d(TAG, " handleLoginAction getJsonString: " + action.getRpc().getJsonString());

        switch (action.getAction()) {
            case EMAIL:
                BridgeUtils.goToEmailActivity(NewFlutterBoost.instance().currentActivity());

                break;
            case FACEBOOK:

                Activity activity = NewFlutterBoost.instance().currentActivity();
                if (activity instanceof WelcomeActivity) {
                    WelcomeActivity welcomeActivity = (WelcomeActivity) activity;
                    welcomeActivity.facebookLogin();
                }

                if (activity instanceof MainActivity) {
                    MainActivity mainActivity = (MainActivity) activity;
                    mainActivity.facebookLogin();
                }

                if (activity instanceof FlutterViewContainerActivity) {
                    FlutterViewContainerActivity flutterViewContainerActivity = (FlutterViewContainerActivity) activity;
                    flutterViewContainerActivity.facebookLogin();
                }

                break;
            case WECHAT:

                WeChatLogin.getInstance().login(NewFlutterBoost.instance().currentActivity(), rfSResultCallback);
                break;
            case UNRECOGNIZED:

                break;
            case LOGIN_SUCCESS:

                BridgeUtils.loginSuccess(NewFlutterBoost.instance().currentActivity(), action.getRpc().getJsonString());

                break;
            case REQUEST_PHOTO:

                L.d(TAG, " REQUEST_PHOTO getSimpleName : " + NewFlutterBoost.instance().currentActivity().getClass().getSimpleName());

                BridgeUtils.gotoTakePhotoActivity(NewFlutterBoost.instance().currentActivity());
                break;
            case USER_AGREEMENT:
                BridgeUtils.gotoAgreementActivity(NewFlutterBoost.instance().currentActivity());
                break;
            case SMS_CODE_FAILED:
                BridgeUtils.gotoHelpWebViewActivity(NewFlutterBoost.instance().currentActivity());
                break;
//            case REGISTER_SUCCESS:
//                if (action.hasRpc()) {
//                    ShareFileUtils.setBoolean(ShareFileUtils.IS_NEW_USER, true);
//                } else {
//                    BridgeUtils.gotoMainActivity(NewFlutterBoost.instance().currentActivity());
//                }
//                break;
            case FILL_DATA_BY_PHONE_ROUTINE:
                BridgeUtils.gotoMainActivity(NewFlutterBoost.instance().currentActivity(), action.getRpc().getJsonString());
                break;
            case FILL_DATA_BY_OAUTH_ROUTINE:

                NewUniversalRouter.sharedInstance.flutterPopToRoot(false);

                RfSBridgePlugin.getInstance().activeRpc("EventLogin", UserUtils.getMyUserId(), null);

                RfSBridgeHandlerFactory.getInstance().getFlutterPushImpl().pushUserInfo();

                RfSBridgeHandlerFactory.getInstance().getFlutterPushImpl().pushMyUserId();

                if (NewFlutterBoost.instance().currentActivity() != null && NewFlutterBoost.instance().currentActivity() instanceof MainActivity) {
                    MainActivity mainActivity = (MainActivity) NewFlutterBoost.instance().currentActivity();
                    mainActivity.init();
                }

//                Intent intent2 = new Intent(NewFlutterBoost.instance().currentActivity(), MainActivity.class);
//                intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                intent2.putExtra("isNew", false);
//                intent2.putExtra(TheLConstants.BUNDLE_KEY_WHERE_TO_GO, "ONE");
//                NewFlutterBoost.instance().currentActivity().startActivity(intent2);

                break;
            default:
                break;
        }

    }

    @Override
    public void handleReleaseAction(RfSRpcRelease.RfS_ActionsRelease action, RfSResultCallback rfSResultCallback) {

        this.rfSResultCallback = rfSResultCallback;

        switch (action.getAction()) {
            case GET_RELEASE_TOPIC_PHOTO:
                BridgeUtils.gotoSelectSinglePhotoActivity(NewFlutterBoost.instance().currentActivity(), this);
                break;
            case GET_RELEASE_TOPIC_VIDEO:
                BridgeUtils.gotoRecordingActivity(NewFlutterBoost.instance().currentActivity(), new onRelaVideoActivityResultListener() {
                    @Override
                    public void onRelaVideoActivityResult(Intent intent) {
                        Bundle bundle = intent.getExtras();
                        String videoPath = bundle.getString(video.com.relavideolibrary.Utils.Constant.BundleConstants.RESULT_VIDEO_PATH);
                        String videoThumnail = bundle.getString(video.com.relavideolibrary.Utils.Constant.BundleConstants.RESULT_VIDEO_THUMB);
                        int playTime = Integer.parseInt(bundle.getString(Constant.BundleConstants.RESULT_VIDEO_DURATION)) / 1000;
                        double pixelWidth = Integer.parseInt(bundle.getString(Constant.BundleConstants.RESULT_VIDEO_WIDTH));
                        double pixelHeight = Integer.parseInt(bundle.getString(Constant.BundleConstants.RESULT_VIDEO_HEIGHT));
                        TopicTextFieldContent content = new TopicTextFieldContent();
                        content.imageUrl = videoThumnail;
                        content.videoUrl = videoPath;
                        content.pixelWidth = pixelWidth;
                        content.pixelHeight = pixelHeight;
                        content.playTime = playTime;
                        content.contentType = TopicTextFieldContent.ContentType.video;
                        rfSResultCallback.onResult(GsonUtils.createJsonString(content));
                    }
                });
                break;
            case GET_RELEASE_VIDEO:


                BridgeUtils.gotoRecordingActivity(NewFlutterBoost.instance().currentActivity(), new onRelaVideoActivityResultListener() {
                    @Override
                    public void onRelaVideoActivityResult(Intent intent) {
                        Bundle bundle = intent.getExtras();
                        String videoPath = bundle.getString(video.com.relavideolibrary.Utils.Constant.BundleConstants.RESULT_VIDEO_PATH);
                        String videoThumnail = bundle.getString(video.com.relavideolibrary.Utils.Constant.BundleConstants.RESULT_VIDEO_THUMB);
                        int playTime = Integer.parseInt(bundle.getString(Constant.BundleConstants.RESULT_VIDEO_DURATION)) / 1000;
                        double pixelWidth = Integer.parseInt(bundle.getString(Constant.BundleConstants.RESULT_VIDEO_WIDTH));
                        double pixelHeight = Integer.parseInt(bundle.getString(Constant.BundleConstants.RESULT_VIDEO_HEIGHT));
                        ArrayList<ImageBean> imageList = new ArrayList<>();
                        ImageBean imageBean = new ImageBean();
                        imageBean.imageName = videoThumnail;
                        imageBean.width = pixelWidth;
                        imageBean.height = pixelHeight;
                        imageList.add(imageBean);
                        String jsonString = "{" +
                                "\"isVideo\":true" + "," +
                                "\"videoPath\" : " + "\"" + videoPath + "\"" + "," +
                                "\"videoTime\" : " + playTime + "," +
                                "\"fileInfoList\" : " + GsonUtils.createJsonString(imageList) +
                                "}";
                        L.i("handleReleaseAction", "jsonString: " + jsonString);
                        rfSResultCallback.onResult(jsonString);
                    }
                });
                break;
            case GET_RELEASE_ATUSERS:
                AtUsersModel atUsersModelList = GsonUtils.getObject(action.getRpc().getJsonString(), AtUsersModel.class);
                ArrayList<ContactBean> contactBeans = new ArrayList<>();
                for (AtUserModel atUsersModel : atUsersModelList.atUsers) {
                    ContactBean contactBean = new ContactBean();
                    contactBean.userId = String.valueOf(atUsersModel.userId);
                    contactBean.avatar = atUsersModel.avatar;
                    contactBean.nickName = atUsersModel.nickname;
                    contactBeans.add(contactBean);
                }
                BridgeUtils.gotoAtUsersActivity(NewFlutterBoost.instance().currentActivity(), this, contactBeans, 10);
                break;
            case PREVIEW_PHOTO:
                BridgeUtils.gotoImagePreviewActivity(NewFlutterBoost.instance().currentActivity(), action.getRpc().getJsonString(), this);
                break;
            case GET_RELEASE_PERMISSIONS:
                BridgeUtils.gotoSharePermissionActivity(NewFlutterBoost.instance().currentActivity(), Integer.parseInt(action.getRpc().getJsonString()), this);
                break;
            case GOTO_RELEASE_ANNIVERSARY:
                BridgeUtils.gotoReleaseLoveActivity(NewFlutterBoost.instance().currentActivity(), rfSResultCallback);
                break;
            case GET_RELEASE_PHOTO:
                BridgeUtils.gotoSelectMultiPhotoActivity(NewFlutterBoost.instance().currentActivity(), action.getRpc().getJsonString(), this);
                break;
            case GOTO_RELEASE_LIVE:
                BridgeUtils.gotoReleaseLiveActivity(NewFlutterBoost.instance().currentActivity());
                break;
            case SET_HOMETAB_STATUS://notRead
                MomentsCheckBean checkBean = GsonUtils.getObject(action.getRpc().getJsonString(), MomentsCheckBean.class);

                FlutterPushImpl.Companion.setMomentsCheckBean(checkBean);

                FlutterPushImpl.Companion.getMomentsCheckBean().isVip = UserUtils.getUserVipLevel() > 0;
                // 发现页面显示匹配的数字 点击跳转后 需要消除
                if (!TextUtils.isEmpty(checkBean.likeMeCursor)) {
                    ShareFileUtils.setString(ShareFileUtils.MATCH_LIKE_ME_OLD_CURSOR, checkBean.likeMeCursor);
                }
                RfSBridgeHandlerFactory.getInstance().getFlutterPushImpl().pushNotRead();
                break;
            case SEND_RECOMMENDED_MSG:
                //先关闭前两个页面
                try {
                    int popCount = 2;
                    int activityCount = ActivityManager.getInstance().getStackActivityCount();
                    for (int i = activityCount - 1; i > activityCount - popCount - 1; i--) {
                        Activity activity = ActivityManager.getInstance().getActivitys().get(i);

                        activity.finish();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                rfSResultCallback.onResult("1");

                BridgeUtils.gotoChatActivity(NewFlutterBoost.instance().currentActivity(), action.getRpc().getJsonString());


                break;
            default:
                break;
        }
    }

    @Override
    public void handleMeAction(RfSRpcMe.RfS_ActionsMe action, RfSResultCallback rfSResultCallback) {

        this.rfSResultCallback = rfSResultCallback;

        L.d(TAG, "handleMeAction: " + action.getAction().toString());

        L.d(TAG, "handleMeAction: " + action.getRpc().getJsonString());

        switch (action.getAction()) {
            case UPDATE_MY_USER_INFO://获取我的信息
                MyInfoBean myInfoBean = GsonUtils.getObject(action.getRpc().getJsonString(), MyInfoBean.class);
                ShareFileUtils.savaUserRatio(myInfoBean.ratio);
                break;
            case GOTO_VIP_PAGE:
                BridgeUtils.gotoVipActivity(NewFlutterBoost.instance().currentActivity(), action.getRpc().getMethod());
                break;
            case GOTO_LEVEL_PAGE:
                BridgeUtils.gotoLevelActivity(NewFlutterBoost.instance().currentActivity());
                break;
            case GOTO_FANS_PAGE:
                BridgeUtils.gotoFansActivity(NewFlutterBoost.instance().currentActivity());
                break;
            case GOTO_FOLLOW_PAGE:
                BridgeUtils.gotoFollowActivity(NewFlutterBoost.instance().currentActivity());
                break;
            case GOTO_WINK_PAGE:
                MeModel simpleUser = GsonUtils.getObject(action.getRpc().getJsonString(), MeModel.class);
                BridgeUtils.gotoWinkActivity(NewFlutterBoost.instance().currentActivity(), simpleUser.userId);
                break;
            case GOTO_EDIT_INTRO_PAGE:
                MeModel meModel = GsonUtils.getObject(action.getRpc().getJsonString(), MeModel.class);
                BridgeUtils.gotoEditIntroActivity(NewFlutterBoost.instance().currentActivity(), meModel.from_page, meModel.selfintroduction, this);
                break;
            case GOTO_WHO_SEE_ME_PAGE:
                BridgeUtils.gotoWhoSeeMeActivity(NewFlutterBoost.instance().currentActivity());
                break;
            case GOTO_EDIT_PAGE:
                BridgeUtils.gotoEditActivity(NewFlutterBoost.instance().currentActivity());
                break;
            case GOTO_MY_RELATIONSHIP_PAGE:
                BridgeUtils.gotoMyRelationshipActivity(NewFlutterBoost.instance().currentActivity());
                break;
            case GOTO_RECHARGE_GOLD_PAGE:
                BridgeUtils.gotoRechargeGoldActivity(NewFlutterBoost.instance().currentActivity());
                break;
            case GOTO_MY_WALLET_PAGE:
                BridgeUtils.gotoMyWalletActivity(NewFlutterBoost.instance().currentActivity());
                break;
            case GOTO_SETTING_PAGE:
                BridgeUtils.gotoSettingActivity(NewFlutterBoost.instance().currentActivity());
                break;
            case GOTO_INVITE_FRIEND_PAGE:
                SharePosterBean posterBean = GsonUtils.getObject(action.getRpc().getJsonString(), SharePosterBean.class);
                BridgeUtils.gotoInviteFriendActivity(NewFlutterBoost.instance().currentActivity(), posterBean);

                break;
            case GOTO_SCAN_QR_PAGE:
                BridgeUtils.gotoScanQRActivity(NewFlutterBoost.instance().currentActivity());

                break;
            case GOTO_CUSTOMER_SERVICE_PAGE:
                BridgeUtils.gotoCustomerServiceActivity(NewFlutterBoost.instance().currentActivity());
                break;
            case CORE_ENCOURAGE:
                BridgeUtils.gotoDirectToAppActivity(NewFlutterBoost.instance().currentActivity());
                break;
            case CHANGE_USER_HEIGHT_WEIGHT:
                UserInfoBean userInfoModel = GsonUtils.getObject(action.getRpc().getJsonString(), UserInfoBean.class);
                setUserInfoData(userInfoModel, rfSResultCallback);

                break;
            case SHOW_USER_IMAGE:

                try {

                    JSONObject jsonObject = new JSONObject(action.getRpc().getJsonString());

                    UserInfoBean userInfoBean = GsonUtils.getObject(jsonObject.getString("userInfoModel"), UserInfoBean.class);
                    L.d("UserInfoPrint" + userInfoBean.toString());

                    Intent i = new Intent(NewFlutterBoost.instance().currentActivity(), UserInfoPhotoActivity.class);
                    i.putExtra("fromPage", "UserInfoActivity");
                    i.putExtra("position", jsonObject.getInt("index"));
                    i.putExtra("userinfo", userInfoBean.picList);
                    i.putExtra("userId", userInfoBean.id);
                    i.putExtra(TheLConstants.BUNDLE_KEY_RELA_ID, userInfoBean.userName);//username后台定义的热拉id
                    i.putExtra(TheLConstants.IS_WATERMARK, true);

                    Bundle bundle = new Bundle();
                    ImgShareBean bean = Utils.getUserImageShareBean(userInfoBean);
                    bundle.putSerializable(TheLConstants.BUNDLER_KEY_USER_IMAGE, bean);
                    bundle.putSerializable(TheLConstants.BUNDLE_KEY_PIC_BEAN, userInfoBean.picList.get(jsonObject.getInt("index")));
                    i.putExtras(bundle);
                    NewFlutterBoost.instance().currentActivity().startActivity(i);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case UPDATE_BG_IMAGE:
                String picUrl = action.getRpc().getJsonString();
                if (picUrl == null || TextUtils.isEmpty(picUrl)) {
                    //用户背景图
                    BridgeUtils.gotoUserBgPhotoActivity(NewFlutterBoost.instance().currentActivity(), this);
                } else {
                    //设置附近封面
                    BridgeUtils.gotoNearbyBgPhotoActivity(NewFlutterBoost.instance().currentActivity(), this, action.getRpc().getJsonString());
                }
                break;
            case USER_REPORT:
                String userId = action.getRpc().getJsonString();
                BridgeUtils.gotoReportActivity(NewFlutterBoost.instance().currentActivity(), userId);
                break;
            case GOTO_PUSH_SETTING:
                BridgeUtils.gotoPushSettingsActivity(NewFlutterBoost.instance().currentActivity());
                break;
            case HIDDEN_LOVE_SUCCESS:
                try {
                    JSONObject jsonObject = new JSONObject(action.getRpc().getJsonString());
                    String user_Id = jsonObject.getString("userId");
                    String nickname = jsonObject.getString("userName");
                    String avatar = jsonObject.getString("avatar");
                    MessagesUtils.saveMessageToDB(MsgUtils.createMsgBean(MsgBean.MSG_TYPE_HIDDEN_LOVE, "", 1, user_Id, nickname, avatar));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            default:
                break;

        }
    }

    private void setUserInfoData(UserInfoBean userInfoModel, RfSResultCallback rfSResultCallback) {

        initRelationshipMap();
        String height = UserUtils.getHandH(userInfoModel.height);
        String weight = UserUtils.getHandW(userInfoModel.weight);
        String affection = getRelationShip(userInfoModel.affection);

        //年龄
        String tAge = TextUtils.isEmpty(userInfoModel.age) ? "" : userInfoModel.age + TheLApp.context.getString(R.string.updatauserinfo_activity_age_unit);

        if (tAge.length() < 0 && !TextUtils.isEmpty(userInfoModel.birthday)) {
            tAge = "18岁";
        }

        String roleName = getRoleName(userInfoModel.roleName);
        String wantRoleName = UserUtils.setLookfor(userInfoModel.wantRole);
        boolean isReported = getIsReported(userInfoModel);

        UserInfoBean userInfoBean = new UserInfoBean();
        userInfoBean.isReport = isReported;
        userInfoBean.roleName = roleName;
        userInfoBean.wantRole = wantRoleName;
        userInfoBean.affection = affection;
        userInfoBean.age = tAge;
        userInfoBean.height = height;
        userInfoBean.weight = weight;
        rfSResultCallback.onResult(GsonUtils.createJsonString(userInfoBean));

    }

    private boolean getIsReported(UserInfoBean userInfoModel) {
        // 检查是否本地存储的举报数据中有该用户
        boolean hasReportedHer = false;
        final String[] reportUserList = SharedPrefUtils.getString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.REPORT_USER_LIST, "").split(",");
        if (reportUserList.length > 0) {
            for (String id : reportUserList) {
                if (id.equals(userInfoModel.id)) {
                    hasReportedHer = true;
                    return hasReportedHer;
                }
            }
        }

        return hasReportedHer;
    }

    private String getRoleName(String roleName) {
        try {

            int index = Integer.parseInt(roleName);
            RoleBean roleBean = ShareFileUtils.getRoleBean(index);
            return roleBean.contentRes;
        } catch (Exception e) {

        }
        return "";
    }

    @Override
    public void handleCommonWidgetAction(RfSRpcCommonWidget.RfS_ActionsCommonWidget action, RfSResultCallback rfSResultCallback) {

        L.d(TAG, "handleCommonWidgetAction: " + action.getAction().toString());

        L.d(TAG, "handleCommonWidgetAction: " + action.getRpc().getJsonString());

        this.rfSResultCallback = rfSResultCallback;

        switch (action.getAction()) {
            case MOMENT_BLOCK://屏蔽日志
                L.i("handleCommonWidgetAction", "momentId: " + action.getRpc().getJsonString());
                MomentsBean momentsBean = new MomentsBean();
                momentsBean.momentsId = action.getRpc().getJsonString();
                MomentUtils.blockThisMoment(NewFlutterBoost.instance().currentActivity(), momentsBean);
                break;
            case MOMENT_REPORT:
                L.i("handleCommonWidgetAction", "momentId: " + action.getRpc().getJsonString());
                MomentUtils.reportMoment(NewFlutterBoost.instance().currentActivity(), action.getRpc().getJsonString());
                break;
            case FOLLOW_SUCCESS:
//firebase 埋点 当用户“关注”她人时触发
                FireBaseUtils.uploadGoogle(TheLConstants.FireBaseConstant.ATTENTION, NewFlutterBoost.instance().currentActivity());
                try {
                    JSONObject jsonObject = new JSONObject(action.getRpc().getJsonString());
                    String userId = jsonObject.getString("userId");
                    String nickname = jsonObject.getString("nickname");
                    String avatar = jsonObject.getString("avatar");
                    FollowStatusChangedImpl.getSingeUserRelation(userId, nickname, avatar);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case MOMENT_BLOCK_ALL://屏蔽当前用户所有日志
                L.i("handleCommonWidgetAction", "userId: " + action.getRpc().getJsonString());
                MomentBlackUtils.blackHerMoment(action.getRpc().getJsonString());
                break;
            case SHOW_IMAGE_BROWSE:
                String rela_id = "";
                String nickname = "";
                String avatar = "";
                String urls = "";
                int position = 0;
                try {
                    JSONObject jsonObject = new JSONObject(action.getRpc().getJsonString());
                    rela_id = jsonObject.getString("userName");
                    nickname = jsonObject.getString("nickname");
                    avatar = jsonObject.getString("avatar");
                    urls = jsonObject.getString("imageUrl");
                    position = jsonObject.getInt("imageIndex");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                String[] photoUrls = urls.split(",");
                Intent intent = new Intent(NewFlutterBoost.instance().currentActivity(), UserInfoPhotoActivity.class);
                intent.putStringArrayListExtra(TheLConstants.BUNDLE_KEY_PHOTOS, new ArrayList<>(Arrays.asList(photoUrls)));
                intent.putExtra(TheLConstants.BUNDLE_KEY_RELA_ID, rela_id);
                intent.putExtra(TheLConstants.IS_WATERMARK, true);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("position", position);
                ImgShareBean imageShareBean = new ImgShareBean();
                imageShareBean.imageUrl = photoUrls[0];
                imageShareBean.nickName = nickname;
                imageShareBean.userID = rela_id;
                imageShareBean.picUserUrl = avatar;
                intent.putExtra(TheLConstants.BUNDLER_KEY_IMAGESHAREBEAN, imageShareBean);
                TheLApp.getContext().startActivity(intent);
                break;
            case GET_COMMENTS_AT_USERS:
                AtUsersModel atUsersModelList = GsonUtils.getObject(action.getRpc().getJsonString(), AtUsersModel.class);
                ArrayList<ContactBean> contactBeans = new ArrayList<>();
                for (AtUserModel atUsersModel : atUsersModelList.atUsers) {
                    ContactBean contactBean = new ContactBean();
                    contactBean.userId = String.valueOf(atUsersModel.userId);
                    contactBean.avatar = atUsersModel.avatar;
                    contactBean.nickName = atUsersModel.nickname;
                    contactBeans.add(contactBean);
                }
                BridgeUtils.gotoAtUsersActivity(NewFlutterBoost.instance().currentActivity(), this, contactBeans, 3);
                break;
            default:
                break;

        }
    }

    @Override
    public void handleDiscoverAction(RfSRpcDiscover.RfS_ActionsDiscover action, RfSResultCallback rfSResultCallback) {

        L.d(TAG, "handleDiscoverAction: " + action.getAction().toString());

        L.d(TAG, "handleDiscoverAction: " + action.getRpc().getJsonString());

        this.rfSResultCallback = rfSResultCallback;

        switch (action.getAction()) {
            case OPEN_LOCATION:
            case REQUEST_LOCATION_PERMISSIONS:
                PermissionUtil.requestLocationPermission(NewFlutterBoost.instance().currentActivity(), new PermissionUtil.PermissionCallback() {
                    @Override
                    public void granted() {
                        if (!PermissionUtil.isOpenGPS(NewFlutterBoost.instance().currentActivity())) {
                            BridgeUtils.openGPSSetting(NewFlutterBoost.instance().currentActivity(), RfSBridgeHandlerFactory.this);
                        } else {
                            getFlutterPushImpl().pushLocationState();
                            rfSResultCallback.onResult("1");
                        }
                    }

                    @Override
                    public void denied() {
                        getFlutterPushImpl().pushLocationState();
                        rfSResultCallback.onResult("0");
                    }

                    @Override
                    public void gotoSetting() {
                        if (!PermissionUtil.isOpenGPS(NewFlutterBoost.instance().currentActivity())) {
                            BridgeUtils.openGPSSetting(NewFlutterBoost.instance().currentActivity(), RfSBridgeHandlerFactory.this);
                        } else {
                            getFlutterPushImpl().pushLocationState();
                        }
                    }
                });

                break;
            case THEME_SHOW_IMAGE_BROWSE:
                String rela_id = "";
                ArrayList<String> urls = new ArrayList<>();
                int position = 0;
                String avatar = "";
                String nickname = "";
                try {
                    JSONObject jsonObject = new JSONObject(action.getRpc().getJsonString());
                    rela_id = jsonObject.getString("userName");
                    position = jsonObject.getInt("imageIndex");
                    avatar = jsonObject.getString("avatar");
                    nickname = jsonObject.getString("nickname");
                    JSONArray jsonArray = jsonObject.getJSONArray("imageUrls");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        urls.add((String) jsonArray.get(i));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Intent intent = new Intent(NewFlutterBoost.instance().currentActivity(), UserInfoPhotoActivity.class);
                intent.putStringArrayListExtra(TheLConstants.BUNDLE_KEY_PHOTOS, urls);
                intent.putExtra(TheLConstants.BUNDLE_KEY_RELA_ID, rela_id);
                intent.putExtra(TheLConstants.IS_WATERMARK, true);
                ImgShareBean imageShareBean = new ImgShareBean();
                imageShareBean.imageUrl = urls.get(0);
                imageShareBean.nickName = nickname;
                imageShareBean.userID = rela_id;
                imageShareBean.picUserUrl = avatar;
                intent.putExtra(TheLConstants.BUNDLER_KEY_IMAGESHAREBEAN, imageShareBean);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("position", position);
                TheLApp.getContext().startActivity(intent);

                break;
            case FIREBASE_ACTIVITY:  ///firebase埋点
                String activity = action.getRpc().getJsonString();
                FireBaseUtils.uploadGoogle(activity, NewFlutterBoost.instance().currentActivity());
                break;
            default:
                break;
        }
    }

    @Override
    public void handleIndexAction(RfSRpcIndex.RfS_ActionsIndex action, RfSResultCallback rfSResultCallback) {

        L.d(TAG, "handleIndexAction: " + action.getAction().toString());

        L.d(TAG, "handleIndexAction: " + action.getRpc().getJsonString());

        this.rfSResultCallback = rfSResultCallback;

        switch (action.getAction()) {
            case GOTO_ACTIVITY:
                L.i("handleIndexAction", "json: " + action.getRpc().getJsonString());

                LiveFollowerUsersBean liveFollowerUsersBean = GsonUtils.getObject(action.getRpc().getJsonString(), LiveFollowerUsersBean.class);

                BridgeUtils.gotoRelaActivities(NewFlutterBoost.instance().currentActivity(), liveFollowerUsersBean);
                break;
            case GOTO_BIND_PHONE:
                BridgeUtils.isBindPhone(NewFlutterBoost.instance().currentActivity(), rfSResultCallback);
                break;
            default:
                break;
        }
    }

    @Override
    public void handleRecommendAction(RfSRpcRecommend.RfS_ActionsRecommend action, RfSResultCallback rfSResultCallback) {
        L.d(TAG, "handleRecommendAction: " + action.getAction().toString());

        L.d(TAG, "handleRecommendAction: " + action.getRpc().getJsonString());

        this.rfSResultCallback = rfSResultCallback;

        switch (action.getAction()) {
            case RECENZT_CHAT_LIST:
                Flowable.create(new FlowableOnSubscribe<List<MsgTable>>() {

                    @Override
                    public void subscribe(FlowableEmitter<List<MsgTable>> e) throws Exception {

                        List<MsgTable> msgTables = ChatServiceManager.getInstance().getMsgList(0);

                        e.onNext(msgTables);

                        e.onComplete();

                    }
                }, BackpressureStrategy.DROP).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<List<MsgTable>>() {
                    @Override
                    public void onSubscribe(Subscription s) {
                        s.request(Integer.MAX_VALUE);
                    }

                    @Override
                    public void onNext(List<MsgTable> msgTables) {
                        List<ContactBeanNew> recommendedModels = new ArrayList<>();
                        if (msgTables != null && msgTables.size() > 0) {
                            for (MsgTable msgTable : msgTables) {
                                Pattern pattern = Pattern.compile("[0-9]*");
                                Matcher isNum = pattern.matcher(msgTable.userId);
                                if (isNum.matches()) {
                                    ContactBeanNew recommendedModel = new ContactBeanNew();
                                    recommendedModel.avatar = msgTable.avatar;
                                    recommendedModel.nickName = msgTable.userName;
                                    recommendedModel.userId = Integer.parseInt(msgTable.userId);
                                    recommendedModels.add(recommendedModel);
                                }
                            }
                        }
                        rfSResultCallback.onResult(GsonUtils.createJsonString(recommendedModels));
                    }

                    @Override
                    public void onError(Throwable t) {
                        rfSResultCallback.onResult(null);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
                break;
            case GENERATE_POSTER:
                BridgeUtils.gotoPosterActivity(NewFlutterBoost.instance().currentActivity(), action.getRpc().getJsonString(), this);
                break;
            case WECHAT:
            case WX_MOMENT:
            case QQ:
            case QZONE:
            case SINA:
            case FACEBOOK:
                BridgeUtils.shareToChannel(NewFlutterBoost.instance().currentActivity(), action.getAction(), action.getRpc().getJsonString(), this);
                break;
            case RECOMMENT_TO_CHAT:
                BridgeUtils.gotoChatActivity(NewFlutterBoost.instance().currentActivity(), action.getRpc().getJsonString());
                break;
        }

    }

    @Override
    public void handleTracing(RfSRpc.RfS_Rpc rpc, String destination, RfSResultCallback rfSResultCallback) {

        L.d(TAG, "handleTracing: " + rpc.getJsonString());

        L.d(TAG, "destination: " + destination);

        ExecutorServiceUtils.getInstatnce().exec(() -> {
            Map data = GsonUtils.fromJsonX(rpc.getJsonString(), Map.class);
            final String method = rpc.getMethod();

            if ("relaTrace".equals(destination)) {
                // AI 打点
                MatchLogUtils.getInstance().send(data);
            } else if ("growingIOTrace".equals(destination)) {
                GrowingIO.getInstance().track(method);
            } else {
                L.e(TAG, destination);
            }
        });

    }

    @Override
    public void flutterReady() {

        L.d(TAG, "------flutterReady-------");

        getFlutterPushImpl().pushUserInfo();

        getFlutterPushImpl().pushMyUserId();

        getFlutterPushImpl().pushLocationState();

        Map<String, Object> map = new HashMap<>();
        if (BuildConfig.DEBUG && ShareFileUtils.getBoolean(ShareFileUtils.FLUTTER_PROXY_OPEN, false)) {
            map.put("proxy", ShareFileUtils.getString(ShareFileUtils.FLUTTER_PROXY, ""));
        }
        if (BuildConfig.DEBUG) {
            map.put("api_endpoint", ShareFileUtils.getString(ShareFileUtils.HTTP_URL, UrlConstants.BASE_URL));
        } else {
            map.put("api_endpoint", UrlConstants.BASE_URL);
        }
        getFlutterPushImpl().pushUpdateEnvironment(map);

        registerRouter();

        if (!TextUtils.isEmpty(UserUtils.getMyUserId())) {

            BusinessUtils.getInitData();

            BlackUtils.getNetBlackList();
        }

        //test
//        if(BuildConfig.DEBUG){
//
//            ShareFileUtils.setString(ShareFileUtils.HTTP_URL, UrlConstants.BASE_TEST_URL);
//            ShareFileUtils.setString(ShareFileUtils.HTTP_HOST, UrlConstants.HTTP_TEST_HOST);
//            ShareFileUtils.setString(ShareFileUtils.IM_HOST, UrlConstants.IM_TEST_HOST);
//            DefaultRequestService.reBuild();
//        }else {
//            ShareFileUtils.setString(ShareFileUtils.HTTP_URL, UrlConstants.BASE_URL);
//            ShareFileUtils.setString(ShareFileUtils.HTTP_HOST, UrlConstants.HTTP_HOST);
//            ShareFileUtils.setString(ShareFileUtils.IM_HOST, UrlConstants.IM_HOST);
//            DefaultRequestService.reBuild();
//        }
//
//        Map<String, Object> map1 = new HashMap<>();
//        map1.put("api_endpoint", ShareFileUtils.getString(ShareFileUtils.HTTP_URL, UrlConstants.BASE_URL));
//        getFlutterPushImpl().pushUpdateEnvironment(map);
    }

    @Override
    public Context getContext() {
        return null;
    }

    public CropIwaResultReceiver.Listener getListener() {

        return new CropIwaResultReceiver.Listener() {
            @Override
            public void onCropSuccess(Uri croppedUri) {

                if (NewFlutterBoost.instance().currentActivity() != null) {

                    Activity activity = NewFlutterBoost.instance().currentActivity();

                    L.d(TAG, " onCropSuccess : " + activity.getClass().getSimpleName());

                    String imageUrl = getFilePathFromUri(activity, croppedUri);

                    L.d(TAG, " imageUrl : " + imageUrl);

                    uploadImage(imageUrl);
                }


            }

            @Override
            public void onCropFailed(Throwable e) {

                L.d(TAG, " onCropFailed : " + e.getMessage());

                Activity activity = NewFlutterBoost.instance().currentActivity();

                if (activity != null) {
                    ToastUtils.showToastShort(activity, "crop failed");
                }

            }
        };
    }

    private void uploadImage(String filePath) {
        Activity activity = NewFlutterBoost.instance().currentActivity();

        L.d(TAG, " filePath : " + filePath);

        L.d(TAG, " activity : " + activity);

        L.d(TAG, " rfSResultCallback : " + rfSResultCallback);

        if (rfSResultCallback != null && filePath != null && activity != null) {

            new UploadFileManager(filePath, new UploadFileManager.OnImageUpdateListener() {
                @Override
                public void onStart() {
                    try {

                        if (loadingDialog != null) {
                            loadingDialog.showLoading();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailed() {
                    try {

                        if (loadingDialog != null) {
                            loadingDialog.closeDialog();
                        }

                        if (NewFlutterBoost.instance().currentActivity() != null) {
                            NewFlutterBoost.instance().currentActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    ToastUtils.showToastShort(TheLApp.context, TheLApp.context.getString(R.string.udesk_upload_img_error));
                                }
                            });
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onSuccess(String path) {
                    if (rfSResultCallback != null) {
                        rfSResultCallback.onResult(path);
                    }

                    if (NewFlutterBoost.instance().currentActivity() != null) {
                        NewFlutterBoost.instance().currentActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (loadingDialog != null) {
                                    loadingDialog.closeDialog();
                                }
                            }
                        });
                    }


                }
            });

        }
    }

    private void uploadBgImage(String filePath) {
        Activity activity = NewFlutterBoost.instance().currentActivity();

        L.d(TAG, " filePath : " + filePath);

        L.d(TAG, " activity : " + activity);

        if (rfSResultCallback != null && filePath != null && activity != null) {

            new UploadFileManager(filePath, new UploadFileManager.OnImageUpdateListener() {
                @Override
                public void onStart() {
                    try {

                        if (loadingDialog != null) {
                            loadingDialog.showLoading();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailed() {
                    try {

                        if (loadingDialog != null) {
                            loadingDialog.closeDialog();
                        }

                        if (NewFlutterBoost.instance().currentActivity() != null) {
                            NewFlutterBoost.instance().currentActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    ToastUtils.showToastShort(TheLApp.context, TheLApp.context.getString(R.string.udesk_upload_img_error));
                                }
                            });
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onSuccess(String path) {
                    if (rfSResultCallback != null) {
                        String json = "{\"imageUrl\":\"" + path + "\"}";

                        L.d("flutter", " json : " + json);
                        if (NewFlutterBoost.instance().currentActivity() != null) {
                            NewFlutterBoost.instance().currentActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    rfSResultCallback.onResult(path);
                                }
                            });
                        }
                    }

                    if (NewFlutterBoost.instance().currentActivity() != null) {
                        NewFlutterBoost.instance().currentActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (loadingDialog != null) {
                                    loadingDialog.closeDialog();
                                }
                            }
                        });
                    }


                }
            }, 2);

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        L.d(TAG, " onActivityResult requestCode : " + requestCode);

        L.d(TAG, " onActivityResult resultCode : " + resultCode);

        if (requestCode == TheLConstants.BUNDLE_CODE_SELECT_IMAGE && resultCode == TheLConstants.RESULT_CODE_SELECT_LOCAL_IMAGE) {
            String path = data.getStringExtra(TheLConstants.BUNDLE_KEY_IMAGE_OUTPUT_PATH);
            if (!TextUtils.isEmpty(path)) {
                imageLocalPath = ImageUtils.handlePhoto1(path, TheLConstants.F_TAKE_PHOTO_ROOTPATH, ImageUtils.getPicName(), false, 0, 0, TheLConstants.PIC_QUALITY, 3);
            }

            TopicTextFieldContent content = new TopicTextFieldContent();
            content.imageUrl = path;
            content.contentType = TopicTextFieldContent.ContentType.image;
            content.text = "";

            if (rfSResultCallback != null) {
                rfSResultCallback.onResult(GsonUtils.createJsonString(content));
            }


        } else if (requestCode == TheLConstants.BUNDLE_CODE_SELECT_BG && resultCode == TheLConstants.RESULT_CODE_SELECT_LOCAL_IMAGE) {
            String path = data.getStringExtra(TheLConstants.BUNDLE_KEY_IMAGE_OUTPUT_PATH);
            if (!TextUtils.isEmpty(path)) {
                imageLocalPath = ImageUtils.handlePhoto1(path, TheLConstants.F_TAKE_PHOTO_ROOTPATH, ImageUtils.getPicName(), false, 0, 0, TheLConstants.PIC_QUALITY, 3);
            }
            uploadBgImage(path);
        } else if (resultCode == TheLConstants.RESULT_CODE_WRITE_MOMENT_SELECT_CONTACT) {// 选择@联系人
            ArrayList<ContactBean> contactBeans = (ArrayList<ContactBean>) data.getSerializableExtra(TheLConstants.BUNDLE_KEY_FRIENDS_LIST);
            AtUsersModel atUsersModel = new AtUsersModel();
            for (ContactBean contactBean : contactBeans) {
                AtUserModel atUserModel = new AtUserModel();
                atUserModel.avatar = contactBean.avatar;
                atUserModel.nickname = contactBean.nickName;
                atUserModel.userId = Integer.parseInt(contactBean.userId);
                atUsersModel.atUsers.add(atUserModel);
                L.d("onActivityResult", "nickname: " + contactBean.nickName);
            }
            rfSResultCallback.onResult(GsonUtils.createJsonString(atUsersModel));
        } else if (requestCode == TheLConstants.PERMISSIONS_REQUEST_READ_LOCATION) {


            getFlutterPushImpl().pushLocationState();


        } else if (requestCode == TheLConstants.BUNDLE_CODE_WRITE_MOMENT_ACTIVITY) {

            if (data != null && data.getExtras() != null) {

                ArrayList<ImageBean> imageList = data.getExtras().getParcelableArrayList(TheLConstants.BUNDLE_KEY_IMAGE_OUTPUT_INFO);
                if (imageList != null && imageList.size() > 0) {

                    String jsonString = "{" +
                            "\"isVideo\":false" + "," +
                            "\"videoPath\":\"\"" + "," +
                            "\"fileInfoList\" : " + GsonUtils.createJsonString(imageList)
                            + "}";

                    L.d("jsonString", " jsonString : " + jsonString);

                    if (rfSResultCallback != null) {
                        rfSResultCallback.onResult(jsonString);
                    }

                }

            }

        } else if (requestCode == TheLConstants.BUNDLE_CODE_SHARE_PERMISSION && resultCode == Activity.RESULT_OK) {
            int position = data.getIntExtra("position", 0);
            rfSResultCallback.onResult(position + "");
        } else if (requestCode == TheLConstants.BUNDLE_CODE_WRITE_MOMENT_PERVIEW_IMAGE && resultCode == TheLConstants.RESULT_CODE_WRITE_MOMENT_DELETE_PICTURE) {
            if (data != null && data.getExtras() != null) {

                ArrayList<ImageBean> imageList = data.getExtras().getParcelableArrayList(TheLConstants.BUNDLE_KEY_IMAGE_OUTPUT_INFO);
                if (imageList == null || imageList.size() == 0) {
                    if (rfSResultCallback != null) {
                        rfSResultCallback.onResult("");
                    }
                } else {
                    String jsonString;
                    if (imageList.size() == 1 && imageList.get(0).imageName.contains(".mp4")) {

                        ImageBean imageBean = data.getExtras().getParcelable(TheLConstants.BUNDLE_KEY_OUT_FLUTTER_VIDEO);
                        jsonString = "{" +
                                "\"isVideo\":true" + "," +
                                "\"videoPath\" : " + "\"" + imageBean.videoPath + "\"" + "," +
                                "\"videoTime\" : " + imageBean.videoTime + "," +
                                "\"fileInfoList\" : " + GsonUtils.createJsonString(new ArrayList<ImageBean>().add(imageBean)) +
                                "}";
                    } else {
                        jsonString = "{" +
                                "\"isVideo\":false" + "," +
                                "\"videoPath\":\"\"" + "," +
                                "\"fileInfoList\" : " + GsonUtils.createJsonString(imageList)
                                + "}";
                    }

                    L.d("jsonString", " jsonString : " + jsonString);

                    if (rfSResultCallback != null) {
                        rfSResultCallback.onResult(jsonString);
                    }
                }

            }
        } else if (requestCode == TheLConstants.BUNDLE_CODE_UPDATE_USER_INFO_ACTIVITY) {

        } else if (resultCode == DeviceUtils.CUT_PHOTO) {

            if (data != null) {
                String photoPath = data.getStringExtra(TheLConstants.BUNDLE_KEY_LOCAL_IMAGE_PATH);
                uploadImage(photoPath);
            }

        } else if (requestCode == TheLConstants.BUNDLE_CODE_EDITE_INTRO && resultCode == Activity.RESULT_OK) {
            try {
                String intro = ShareFileUtils.getString(ShareFileUtils.INTRO, "");
                MyInfoBean myInfoBean = new MyInfoBean();
                myInfoBean.intro = intro;

                String jsonString = "{" +
                        "\"intro\":" + "\"" + intro + "\"" +
                        "}";

                if (rfSResultCallback != null) {
                    rfSResultCallback.onResult(jsonString);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == TheLConstants.PERMISSIONS_REQUEST_GPS) {
            getFlutterPushImpl().pushLocationState();
            if (!PhoneUtils.isGpsOpen() || ContextCompat.checkSelfPermission(TheLApp.context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(TheLApp.context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                if (rfSResultCallback != null) {
                    rfSResultCallback.onResult("0");
                }
            } else {
                if (rfSResultCallback != null) {
                    rfSResultCallback.onResult("1");
                }
            }
        } else if (requestCode == TheLConstants.REQUEST_SAVE_POSTER && resultCode == Activity.RESULT_OK) {
            L.d(TAG, "REQUEST_SAVE_POSTER");
            if (rfSResultCallback != null) {
                rfSResultCallback.onResult("");
            }
        } else if (resultCode == TheLConstants.RESULT_CODE_REPORT_MOMENT_SUCCEED) {
            String jsonString = "1";
            if (rfSResultCallback != null) {
                rfSResultCallback.onResult(jsonString);
            }
        } else if (requestCode == TheLConstants.REQUEST_CODE_EMAIL_LOGIN) {

            if (MainActivity.sRef != null) {
                MainActivity.sRef.get().init();
            }
        } else if (requestCode == TheLConstants.REQUEST_CODE_COVER && resultCode == Activity.RESULT_OK) {
            String photoPath = data.getStringExtra(TheLConstants.BUNDLE_KEY_COVER);
            L.d(TAG, "photoPath: " + photoPath);
            if (rfSResultCallback != null) {
                rfSResultCallback.onResult(photoPath);
            }
        }

    }

    private void registerRouter() {

        NewUniversalRouter.sharedInstance.registerRoute(FlutterRouterPathConstants.ROUTER_NATIVE_CHATREQUSET, (context, object) -> FlutterRouterConfig.Companion.getChatRequsetIntent());

        NewUniversalRouter.sharedInstance.registerRoute(FlutterRouterPathConstants.ROUTER_NATIVE_VIPSERVICE, (context, object) -> FlutterRouterConfig.Companion.getVipServiceIntent());

        NewUniversalRouter.sharedInstance.registerRoute(FlutterRouterPathConstants.ROUTER_NATIVE_VISITOR, (context, object) -> FlutterRouterConfig.Companion.getVisitorIntent());

        NewUniversalRouter.sharedInstance.registerRoute(FlutterRouterPathConstants.ROUTER_NATIVE_RECHARGE, (context, object) -> FlutterRouterConfig.Companion.getRechargeIntent());

        NewUniversalRouter.sharedInstance.registerRoute(FlutterRouterPathConstants.ROUTER_NATIVE_MATCH, (context, object) -> FlutterRouterConfig.Companion.getMatchIntent());

        NewUniversalRouter.sharedInstance.registerRoute(FlutterRouterPathConstants.ROUTER_NATIVE_WHOLIKESME, (context, object) -> FlutterRouterConfig.Companion.getWhoLikeMeIntent());

        NewUniversalRouter.sharedInstance.registerRoute(FlutterRouterPathConstants.ROUTER_NATIVE_PERFECTMATCH, (context, object) -> FlutterRouterConfig.Companion.getPerfectMatchIntent());

        NewUniversalRouter.sharedInstance.registerRoute(FlutterRouterPathConstants.ROUTER_NATIVE_NEARBY, (context, object) -> FlutterRouterConfig.Companion.getNearbyIntent());

        NewUniversalRouter.sharedInstance.registerRoute(FlutterRouterPathConstants.ROUTER_NATIVE_WORLDMAP, (context, object) -> FlutterRouterConfig.Companion.getWorldMapIntent());

        NewUniversalRouter.sharedInstance.registerRoute(FlutterRouterPathConstants.ROUTER_NATIVE_WALKMAN, (context, object) -> FlutterRouterConfig.Companion.getWalkmanIntent());

        NewUniversalRouter.sharedInstance.registerRoute(FlutterRouterPathConstants.ROUTER_NATIVE_SEARCH, (context, object) -> FlutterRouterConfig.Companion.getSearchIntent());

        NewUniversalRouter.sharedInstance.registerRoute(FlutterRouterPathConstants.ROUTER_NATIVE_CHAT, (context, object) -> FlutterRouterConfig.Companion.getChatIntent(object.toString()));

        NewUniversalRouter.sharedInstance.registerRoute(FlutterRouterPathConstants.ROUTER_NATIVE_RECOMMENDED, (context, object) -> FlutterRouterConfig.Companion.getRecommendedIntent(object.toString()));

        NewUniversalRouter.sharedInstance.registerRoute(FlutterRouterPathConstants.ROUTER_NATIVE_USERFANSLIST, (context, object) -> FlutterRouterConfig.Companion.getUserFansListIntent(object.toString()));

        NewUniversalRouter.sharedInstance.registerRoute(FlutterRouterPathConstants.ROUTER_NATIVE_OPENMEMBER, (context, object) -> FlutterRouterConfig.Companion.getOpenMemberIntent());

        NewUniversalRouter.sharedInstance.registerRoute(FlutterRouterPathConstants.ROUTER_NATIVE_RECUSERLIST, (context, object) -> FlutterRouterConfig.Companion.getRecUserListIntent());

        NewUniversalRouter.sharedInstance.registerRoute(FlutterRouterPathConstants.ROUTER_NATIVE_EXPECTLIST, (context, object) -> FlutterRouterConfig.Companion.getExpectListIntent(object.toString()));

        NewUniversalRouter.sharedInstance.registerRoute(FlutterRouterPathConstants.ROUTER_NATIVE_MOMENTATUSER, (context, object) -> FlutterRouterConfig.Companion.getMomentAtUserIntent(object));

        NewUniversalRouter.sharedInstance.registerRoute(FlutterRouterPathConstants.ROUTER_NATIVE_VIDEO, (context, object) -> FlutterRouterConfig.Companion.getVideoIntent(object.toString()));

        NewUniversalRouter.sharedInstance.registerRoute(FlutterRouterPathConstants.ROUTER_NATIVE_LIVE, (context, object) -> FlutterRouterConfig.Companion.getLiveIntent(object));

        NewUniversalRouter.sharedInstance.registerRoute(FlutterRouterPathConstants.ROUTER_NATIVE_MAIN, (context, object) -> FlutterRouterConfig.Companion.getMainIntent());

        NewUniversalRouter.sharedInstance.registerRoute(FlutterRouterPathConstants.ROUTER_NATIVE_ADVERT, (context, object) -> FlutterRouterConfig.Companion.getActivitiesIntent(object.toString()));

        NewUniversalRouter.sharedInstance.registerRoute(FlutterRouterPathConstants.ROUTER_NATIVE_WINK_LIST, (context, object) -> FlutterRouterConfig.Companion.getUserWinkListIntent(object.toString()));

        NewUniversalRouter.sharedInstance.registerRoute(FlutterRouterPathConstants.ROUTER_FLUTTER_CONTAINER, (context, object) -> FlutterRouterConfig.Companion.getFlutterCommonIntent());
    }

    //感情状态
    private String getRelationShip(String affectio) {
        try {

            int index = Integer.parseInt(affectio);
            RoleBean relationshipBean = relationshipMap.get(index);

            return relationshipBean.contentRes;

        } catch (Exception e) {
            return "";

        }
    }

    private void initRelationshipMap() {
        relationshipMap.put(1, new RoleBean(0, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[0]));
        relationshipMap.put(6, new RoleBean(1, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[1]));
        relationshipMap.put(7, new RoleBean(2, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[2]));
        relationshipMap.put(2, new RoleBean(3, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[3]));
        relationshipMap.put(3, new RoleBean(4, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[4]));
        relationshipMap.put(4, new RoleBean(5, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[5]));
        relationshipMap.put(5, new RoleBean(6, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[6]));
        relationshipMap.put(0, new RoleBean(7, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[7]));
    }
}

