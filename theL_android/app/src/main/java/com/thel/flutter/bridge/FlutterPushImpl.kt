package com.thel.flutter.bridge

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.text.TextUtils
import androidx.core.content.ContextCompat
import com.meituan.android.walle.WalleChannelReader
import com.thel.app.TheLApp
import com.thel.bean.BasicInfoBean
import com.thel.bean.BlackListBean
import com.thel.bean.moments.MomentsCheckBean
import com.thel.bean.user.FlutterUserInfo
import com.thel.flutter.IFlutterPush
import com.thel.utils.*
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import me.rela.rf_s_bridge.RfSBridgePlugin

class FlutterPushImpl : IFlutterPush {
    override fun pushVipConfig(map: Map<String, Any>) {
        Flowable.just("").onBackpressureDrop().observeOn(AndroidSchedulers.mainThread()).subscribe {
            map.let {
                RfSBridgePlugin.getInstance().activeRpc("vip_set", GsonUtils.mapToJson(it), null)
            }
        }
    }

    override fun pushUpdateEnvironment(map: Map<String, Any>) {
        Flowable.just("").onBackpressureDrop().observeOn(AndroidSchedulers.mainThread()).subscribe {
            map.let {
                RfSBridgePlugin.getInstance().activeRpc("EventNativeEnvironment", GsonUtils.mapToJson(it), null)
            }
        }
    }

    private val tagName: String = "FlutterPushImpl"

    companion object {

        var momentsCheckBean = MomentsCheckBean()

    }

    override fun pushLanguage(language: String) {
        Flowable.just("").onBackpressureDrop().observeOn(AndroidSchedulers.mainThread()).subscribe {
            language.let {
                RfSBridgePlugin.getInstance().activeRpc("EventChangeLanguage", language, null)
            }
        }
    }

    @SuppressLint("CheckResult")
    override fun pushUserInfo() {
        Flowable.just("").onBackpressureDrop().observeOn(AndroidSchedulers.mainThread()).subscribe {
            try {

                val userInfoBean = FlutterUserInfo()
                userInfoBean.let {
                    val id = ShareFileUtils.getString(ShareFileUtils.ID, "0")
                    if (!TextUtils.isEmpty(id)) {
                        userInfoBean.id = Integer.valueOf(id)
                    }
                    userInfoBean.nickName = ShareFileUtils.getString(ShareFileUtils.USER_NAME, "")
                    userInfoBean.userName = ShareFileUtils.getString(ShareFileUtils.USER_THEL_ID, "")
                    userInfoBean.avatar = ShareFileUtils.getString(ShareFileUtils.AVATAR, "")
                    userInfoBean.roleName = ShareFileUtils.getString(ShareFileUtils.ROLE_NAME, "");
                    userInfoBean.cell = ShareFileUtils.getString(ShareFileUtils.BIND_CELL, "")
                    userInfoBean.wx = ShareFileUtils.getString(ShareFileUtils.BIND_WX, "")
                    userInfoBean.fb = ShareFileUtils.getString(ShareFileUtils.BIND_FB, "")

                    val ratio = ShareFileUtils.getString(ShareFileUtils.NO_RATIO, "0")

                    if (!TextUtils.isEmpty(ratio)) {
                        userInfoBean.ratio = Integer.valueOf(ratio)
                    }

                    RfSBridgePlugin.getInstance().activeRpc("userInfo", GsonUtils.createJsonString(userInfoBean), null)
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

    }

    @SuppressLint("CheckResult")
    override fun pushNotRead() {

        Flowable.just("").onBackpressureDrop().observeOn(AndroidSchedulers.mainThread()).subscribe {
            momentsCheckBean.let {
                RfSBridgePlugin.getInstance().activeRpc("notRead", GsonUtils.createJsonString(it), null)
            }
        }

    }

    @SuppressLint("CheckResult")
    override fun pushMyUserId() {

        Flowable.just("").onBackpressureDrop().observeOn(AndroidSchedulers.mainThread()).subscribe {
            UserUtils.getMyUserId().let {
                RfSBridgePlugin.getInstance().activeRpc("myUserId", it, null)

            }
        }
    }

    @SuppressLint("CheckResult")
    override fun pushBlackListInfo(blackListBean: BlackListBean) {
        Flowable.just("").onBackpressureDrop().observeOn(AndroidSchedulers.mainThread()).subscribe {
            blackListBean.let {
                RfSBridgePlugin.getInstance().activeRpc("blackListInfo", GsonUtils.createJsonString(it), null)
            }
        }

    }

    @SuppressLint("CheckResult")
    override fun pushLocationState() {
        Flowable.just("").onBackpressureDrop().observeOn(AndroidSchedulers.mainThread()).subscribe {

            val json: String = if (!PhoneUtils.isGpsOpen() || ContextCompat.checkSelfPermission(TheLApp.context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(TheLApp.context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                "0"
            } else {
                "1"
            }

            L.d(tagName, " pushLocationState json : $json")

            if(json == "0") {
                ShareFileUtils.setString(ShareFileUtils.LATITUDE, "0.0")
                ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0")
            }
            RfSBridgePlugin.getInstance().activeRpc("isOpenLocation", json, null)
        }
    }

    @SuppressLint("CheckResult")
    override fun pushInitParams(basicInfoNetBean: BasicInfoBean) {
        basicInfoNetBean.let {
            RfSBridgePlugin.getInstance().activeRpc("initParams", GsonUtils.createJsonString(it), null)
        }

    }

}