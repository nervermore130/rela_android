package com.thel.flutter.bean

data class RouteBean(var route: String, var isNative: Boolean, var uuid: String)