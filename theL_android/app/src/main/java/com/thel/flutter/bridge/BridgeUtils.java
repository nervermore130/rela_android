package com.thel.flutter.bridge;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.gson.reflect.TypeToken;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.bean.ContactBean;
import com.thel.bean.LivePermitBean;
import com.thel.bean.RecommendedModel;
import com.thel.bean.SharePosterBean;
import com.thel.bean.me.MyCircleActivity;
import com.thel.bean.me.MyCircleFriendListBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.recommend.RecommendWebBean;
import com.thel.bean.user.UserCardBean;
import com.thel.constants.BundleConstants;
import com.thel.constants.TheLConstants;
import com.thel.constants.TheLConstantsExt;
import com.thel.flutter.OnActivityResultCallback;
import com.thel.growingio.GIOShareTrackBean;
import com.thel.growingio.GrowingIoConstant;
import com.thel.modules.live.Certification.ZhimaCertificationActivity;
import com.thel.modules.live.bean.LiveFollowerUsersBean;
import com.thel.modules.live.bean.LiveRoomBean;
import com.thel.modules.live.surface.LiveShowPresenter;
import com.thel.modules.live.surface.watch.LiveWatchActivity;
import com.thel.modules.live.surface.watch.horizontal.LiveWatchHorizontalActivity;
import com.thel.modules.login.email.EmailLoginActivity;
import com.thel.modules.login.login_register.LoginRegisterActivity;
import com.thel.modules.main.MainActivity;
import com.thel.modules.main.home.moments.MomentPermissionActivity;
import com.thel.modules.main.home.moments.ReleaseLiveMomentActivity;
import com.thel.modules.main.home.moments.ReleaseMomentActivity;
import com.thel.modules.main.home.moments.ReportActivity;
import com.thel.modules.main.home.moments.SendCardActivity;
import com.thel.modules.main.home.moments.web.WebViewActivity;
import com.thel.modules.main.me.aboutMe.MyLevelActivity;
import com.thel.modules.main.me.aboutMe.MySoftMoneyActivity;
import com.thel.modules.main.me.aboutMe.MyWalletActivity;
import com.thel.modules.main.me.aboutMe.RelationshipActivity;
import com.thel.modules.main.me.aboutMe.UpdateUserInfoActivity;
import com.thel.modules.main.me.bean.LogInfoBean;
import com.thel.modules.main.me.match.eventcollect.collect.MatchLogUtils;
import com.thel.modules.main.messages.ChatActivity;
import com.thel.modules.main.messages.bean.MsgBean;
import com.thel.modules.main.messages.utils.PushUtils;
import com.thel.modules.main.nearby.CoverPreviewActivity;
import com.thel.modules.main.nearby.visit.VisitActivity;
import com.thel.modules.main.settings.SettingActivity;
import com.thel.modules.main.settings.SettingsActivity;
import com.thel.modules.others.UserInfoWinksActivity;
import com.thel.modules.others.VipConfigActivity;
import com.thel.modules.post.MomentPosterActivity;
import com.thel.modules.post.PosterActivity;
import com.thel.modules.preview_image.UserInfoPhotoActivity;
import com.thel.modules.qrcode.QRActivity;
import com.thel.modules.select_contacts.SelectContactsActivity;
import com.thel.modules.select_image.ImageBean;
import com.thel.modules.select_image.SelectLocalImagesActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.api.loginapi.bean.SignInBean;
import com.thel.network.api.loginapi.bean.SignInUserBean;
import com.thel.ui.dialog.ActionSheetDialog;
import com.thel.utils.DialogUtil;
import com.thel.utils.FireBaseUtils;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.GsonUtils;
import com.thel.utils.ImageUtils;
import com.thel.utils.L;
import com.thel.utils.PermissionUtil;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.ToastUtils;
import com.thel.utils.UMShareUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import me.rela.rf_s_bridge.RfSBridgePlugin;
import me.rela.rf_s_bridge.RfSResultCallback;
import me.rela.rf_s_bridge.new_router.NewUniversalRouter;
import me.rela.rf_s_bridge.proto3.RfSRpcRecommend;
import me.rela.rf_s_bridge.router.FlutterBoostPlugin;
import me.rela.rf_s_bridge.router.UniversalRouter;
import video.com.relavideolibrary.RelaVideoSDK;
import video.com.relavideolibrary.onRelaVideoActivityResultListener;

import static com.thel.utils.ShareFileUtils.IS_RELEASE_MOMENT;
import static com.thel.utils.StringUtils.getString;

public class BridgeUtils {

    public static void goToEmailActivity(Activity activity) {
        if (activity != null) {
            Intent intent = new Intent(activity, EmailLoginActivity.class);
            activity.startActivityForResult(intent, TheLConstants.REQUEST_CODE_EMAIL_LOGIN);
        }
    }

    public static void gotoTakePhotoActivity(Activity activity) {
        if (activity != null) {
            PermissionUtil.requestStoragePermission(activity, new PermissionUtil.PermissionCallback() {

                @Override
                public void granted() {
                    Intent intent = new Intent(activity, SelectLocalImagesActivity.class);
                    intent.putExtra("isAvatar", true);
                    activity.startActivityForResult(intent, TheLConstants.BUNDLE_CODE_UPDATE_USER_INFO_ACTIVITY);
                }

                @Override
                public void denied() {

                }

                @Override
                public void gotoSetting() {

                }
            });

        }

    }

    public static void gotoSelectSinglePhotoActivity(Activity activity, OnActivityResultCallback onActivityResultCallback) {
        if (activity != null) {
            if (activity instanceof BaseActivity) {
                ((BaseActivity) activity).setOnActivityResultCallback(onActivityResultCallback);
            }
            activity.startActivityForResult(new Intent(activity, SelectLocalImagesActivity.class), TheLConstants.BUNDLE_CODE_SELECT_IMAGE);
        }

    }

    /**
     * 用户背景图
     *
     * @param activity
     * @param onActivityResultCallback
     */
    public static void gotoUserBgPhotoActivity(Activity activity, OnActivityResultCallback onActivityResultCallback) {
        if (activity != null) {
            if (activity instanceof MainActivity) {
                ((MainActivity) activity).setOnActivityResultCallback(onActivityResultCallback);
            }
            activity.startActivityForResult(new Intent(activity, SelectLocalImagesActivity.class), TheLConstants.BUNDLE_CODE_SELECT_BG);
        }

    }

    /**
     * 设置附近封面
     *
     * @param activity
     * @param onActivityResultCallback
     * @param picUrl
     */
    public static void gotoNearbyBgPhotoActivity(Activity activity, OnActivityResultCallback onActivityResultCallback, String picUrl) {
        if (activity != null) {
            if (activity instanceof MainActivity) {
                ((MainActivity) activity).setOnActivityResultCallback(onActivityResultCallback);
            }
            Intent intent = new Intent(activity, CoverPreviewActivity.class);
            if (picUrl != null && !TextUtils.isEmpty(picUrl))
                intent.putExtra(TheLConstants.BUNDLE_KEY_COVER, picUrl);
            activity.startActivityForResult(intent, TheLConstants.REQUEST_CODE_COVER);
        }

    }


    public static void gotoSelectMultiPhotoActivity(Activity activity, String jsonString, OnActivityResultCallback onActivityResultCallback) {

        L.d("BridgeUtils", " jsonString : " + jsonString);

        if (activity != null) {
            if (activity instanceof BaseActivity) {
                ((BaseActivity) activity).setOnActivityResultCallback(onActivityResultCallback);
            }
            try {
                Intent intent = new Intent(activity, SelectLocalImagesActivity.class);
                intent.putExtra(TheLConstants.BUNDLE_KEY_SELECT_AMOUNT, 9);
                if (!TextUtils.isEmpty(jsonString)) {
                    JSONObject jsonObject = new JSONObject(jsonString);

                    ArrayList<ImageBean> photoUrls = new ArrayList<>();

                    String fileInfoListString = jsonObject.getString("fileInfoList");

                    List<ImageBean> list = GsonUtils.getGson().fromJson(fileInfoListString, new TypeToken<List<ImageBean>>() {
                    }.getType());

                    if (list != null) {
                        photoUrls.addAll(list);
                    }

                    if (photoUrls.size() > 0) {
                        intent.putParcelableArrayListExtra(TheLConstants.BUNDLE_KEY_PHOTO_INFO_LIST, photoUrls);
                    }
                }
                activity.startActivityForResult(intent, TheLConstants.BUNDLE_CODE_WRITE_MOMENT_ACTIVITY);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public static void gotoImagePreviewActivity(Activity activity, String jsonString, OnActivityResultCallback onActivityResultCallback) {
        if (activity != null) {
            if (activity instanceof BaseActivity) {
                ((BaseActivity) activity).setOnActivityResultCallback(onActivityResultCallback);
            }

            try {
                Intent i = new Intent(activity, UserInfoPhotoActivity.class);
                i.putExtra("fromPage", ReleaseMomentActivity.class.getName());
                JSONObject jsonObject = new JSONObject(jsonString);
                boolean isVideo = jsonObject.getBoolean("isVideo");
                ArrayList<String> photoUrls = new ArrayList<>();
                if (isVideo) {
                    photoUrls.add(jsonObject.getString("videoPath"));
                    ImageBean imageBean = new ImageBean();
                    imageBean.videoPath = jsonObject.getString("videoPath");
                    imageBean.videoTime = jsonObject.getInt("videoTime");
                    JSONObject jsonObject1 = jsonObject.getJSONArray("fileInfoList").getJSONObject(0);
                    imageBean.width = jsonObject1.getDouble("width");
                    imageBean.height = jsonObject1.getDouble("height");
                    imageBean.imageName = jsonObject1.getString("imageName");
                    i.putExtra(TheLConstants.BUNDLE_KEY_FLUTTER_VIDEO, imageBean);
                } else {
                    JSONArray array = jsonObject.getJSONArray("imageUrlList");
                    for (int j = 0; j < array.length(); j++) {
                        photoUrls.add((String) array.get(j));
                    }
                }
                i.putExtra("position", jsonObject.getInt("index"));
                i.putStringArrayListExtra(TheLConstants.BUNDLE_KEY_PHOTOS, photoUrls);
                activity.startActivityForResult(i, TheLConstants.BUNDLE_CODE_WRITE_MOMENT_PERVIEW_IMAGE);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public static void gotoRecordingActivity(Activity activity, onRelaVideoActivityResultListener listener) {
        if (activity != null) {
            PermissionUtil.requestStoragePermission(activity, new PermissionUtil.PermissionCallback() {
                @Override
                public void granted() {
                    RelaVideoSDK.startVideoGalleryActivity(activity, listener);
                }

                @Override
                public void denied() {

                }

                @Override
                public void gotoSetting() {

                }
            });
        }
    }

    public static void gotoAtUsersActivity(Activity activity, OnActivityResultCallback onActivityResultCallback, ArrayList<ContactBean> mentionList, int selectLimit) {
        if (activity != null) {
            if (activity instanceof BaseActivity) {
                ((BaseActivity) activity).setOnActivityResultCallback(onActivityResultCallback);
            }
            Intent intent = new Intent(activity, SelectContactsActivity.class);
            intent.putExtra(TheLConstants.BUNDLE_KEY_FRIENDS_LIST, mentionList);
            intent.putExtra("selectLimit", selectLimit);
            activity.startActivityForResult(intent, TheLConstants.RESULT_CODE_WRITE_MOMENT_SELECT_CONTACT);
        }
    }

    public static void loginSuccess(Activity activity, String json) {

        if (activity != null) {

            SignInBean.Data data = GsonUtils.getObject(json, SignInBean.Data.class);

            SignInBean signInBean = new SignInBean();

            signInBean.data = data;

            ShareFileUtils.saveUserData(signInBean);

            ShareFileUtils.setBoolean(ShareFileUtils.HAS_LOGGED, true);

            if (!signInBean.data.user.isNewUser()) {

                MobclickAgent.onEvent(activity, "register_succeed");

                ShareFileUtils.setBoolean(ShareFileUtils.IS_NEW_USER, false);

                MobclickAgent.onEvent(activity, "enter_main_page");

                NewUniversalRouter.sharedInstance.flutterPopToRoot(false);

                RfSBridgeHandlerFactory.getInstance().getFlutterPushImpl().pushUserInfo();

                RfSBridgeHandlerFactory.getInstance().getFlutterPushImpl().pushMyUserId();

                RfSBridgePlugin.getInstance().activeRpc("EventLogin", UserUtils.getMyUserId(), null);

                if (activity instanceof MainActivity) {
                    MainActivity mainActivity = (MainActivity) activity;
                    mainActivity.init();
                }
//                Intent intent = new Intent(activity, MainActivity.class);
//                intent.putExtra("isNew", false);
//                intent.putExtra(TheLConstants.BUNDLE_KEY_WHERE_TO_GO, "ONE");
//                activity.startActivity(intent);
//                activity.finish();
            } else {
                MobclickAgent.onEvent(activity, "register_or_login_succeed");
                ShareFileUtils.setBoolean(ShareFileUtils.IS_NEW_USER, true);
            }


        }
    }

    public static void gotoAgreementActivity(Activity activity) {
        if (activity != null) {
            Intent intent = new Intent(activity, WebViewActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(BundleConstants.URL, TheLConstants.USER_AGREEMENT_PAGE_URL);
            bundle.putBoolean(BundleConstants.NEED_SECURITY_CHECK, false);
            intent.putExtras(bundle);
            activity.startActivity(intent);
        }
    }

    public static void gotoHelpWebViewActivity(Activity activity) {
        if (activity != null) {
            Intent intent = new Intent(activity, WebViewActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(BundleConstants.URL, TheLConstants.SERVICE_HELP_URL);
            intent.putExtras(bundle);
            activity.startActivity(intent);
        }
    }

    public static void gotoMainActivity(Activity activity, String json) {
        if (activity != null) {

            SignInUserBean signInUserBean = GsonUtils.getObject(json, SignInUserBean.class);

            if (signInUserBean != null) {

                ShareFileUtils.setString(ShareFileUtils.USER_NAME, signInUserBean.nickName);
                ShareFileUtils.setString(ShareFileUtils.USER_THEL_ID, signInUserBean.userName);
                ShareFileUtils.setString(ShareFileUtils.AVATAR, signInUserBean.avatar);
                ShareFileUtils.setString(ShareFileUtils.ROLE_NAME, signInUserBean.roleName);
            }

            NewUniversalRouter.sharedInstance.flutterPopToRoot(false);

            RfSBridgeHandlerFactory.getInstance().getFlutterPushImpl().pushUserInfo();

            RfSBridgeHandlerFactory.getInstance().getFlutterPushImpl().pushMyUserId();

            RfSBridgePlugin.getInstance().activeRpc("EventLogin", UserUtils.getMyUserId(), null);

            Intent intent = new Intent(activity, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("isNew", true);
            intent.putExtra(TheLConstants.BUNDLE_KEY_WHERE_TO_GO, "ONE");
            activity.startActivity(intent);
            activity.finish();
        }
    }

    /**
     * 跳转到会员页面
     */
    public static void gotoVipActivity(Activity activity, String method) {
        if (activity != null) {
            Intent intent = new Intent(activity, VipConfigActivity.class);
            intent.putExtra("fromPage", "my");
            String pageId = Utils.getPageId();
            intent.putExtra("fromPageId", pageId);
            if (method.equals("hiding")) {
                intent.putExtra("showType", VipConfigActivity.SHOW_MSG_HIDING);
                intent.putExtra("fromPage", "nearby");
            } else if (method.equals("filter")) {
                intent.putExtra("showType", VipConfigActivity.SHOW_NEARBY_FILTER);
                intent.putExtra("fromPage", "nearby");
            }
            activity.startActivity(intent);
        }
    }

    /**
     * 跳转到等级页面
     */
    public static void gotoLevelActivity(Activity activity) {
        if (activity != null) {
            Intent intent = new Intent(activity, MyLevelActivity.class);
            activity.startActivity(intent);
        }
    }

    /**
     * 跳转到粉丝页面
     */
    public static void gotoFansActivity(Activity activity) {
        if (activity != null) {
            Intent intent = new Intent(activity, RelationshipActivity.class);
            intent.putExtra("type", RelationshipActivity.TYPE_FANS);
            intent.putExtra(TheLConstants.FROM_PAGE, "my");
            String pageId = Utils.getPageId();
            intent.putExtra(TheLConstants.FROM_PAGE_ID, pageId);

            activity.startActivity(intent);
        }
    }

    /**
     * 跳转到关注页面
     */
    public static void gotoFollowActivity(Activity activity) {
        if (activity != null) {
            Intent intent = new Intent(activity, RelationshipActivity.class);
            intent.putExtra("type", RelationshipActivity.TYPE_FOLLOW);
            activity.startActivity(intent);
        }
    }

    /**
     * 跳转到挤眼页面
     */
    public static void gotoWinkActivity(Activity activity, String userId) {
        if (activity != null) {
            Intent intent = new Intent(activity, UserInfoWinksActivity.class);
            intent.putExtra(TheLConstants.FROM_PAGE, "my");
            String pageId = Utils.getPageId();
            intent.putExtra(TheLConstants.FROM_PAGE_ID, pageId);
            intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, userId);
            activity.startActivity(intent);
        }
    }

    /**
     * 跳转到修改个人签名页面
     */
    public static void gotoEditIntroActivity(Activity activity, String fromPage, String intro, OnActivityResultCallback onActivityResultCallback) {
        if (activity != null) {
            if (activity instanceof BaseActivity) {
                ((BaseActivity) activity).setOnActivityResultCallback(onActivityResultCallback);
            }
            Intent intent = new Intent(activity, UpdateUserInfoActivity.class);
            intent.putExtra(UpdateUserInfoActivity.FROM_PAGE, fromPage);
            intent.putExtra("selfintroduction", intro);
            activity.startActivityForResult(intent, TheLConstants.BUNDLE_CODE_EDITE_INTRO);
        }
    }

    /**
     * 跳转到谁来看过我页面
     */
    public static void gotoWhoSeeMeActivity(Activity activity) {
        if (activity != null) {
            Intent intent = new Intent(activity, VisitActivity.class);
            intent.putExtra(TheLConstants.FROM_PAGE, "my");
            String pageId = Utils.getPageId();
            intent.putExtra(TheLConstants.FROM_PAGE_ID, pageId);
            activity.startActivity(intent);
        }
    }


    /**
     * 跳转到修改个人资料页面
     */
    public static void gotoEditActivity(Activity activity) {
        if (activity != null) {
            Intent intent = new Intent(activity, UpdateUserInfoActivity.class);
            activity.startActivity(intent);
        }
    }


    /**
     * 跳转到我的关系页面
     */
    public static void gotoMyRelationshipActivity(Activity activity) {
        if (activity != null) {
            Intent intent = new Intent(activity, MyCircleActivity.class);
            activity.startActivity(intent);
        }
    }


    /**
     * 跳转到充值软妹豆页面
     */
    public static void gotoRechargeGoldActivity(Activity activity) {
        if (activity != null) {
            Intent intent = new Intent(activity, MySoftMoneyActivity.class);
            activity.startActivity(intent);
        }
    }


    /**
     * 跳转到我的钱包页面
     */
    public static void gotoMyWalletActivity(Activity activity) {
        if (activity != null) {
            Intent intent = new Intent(activity, MyWalletActivity.class);
            activity.startActivity(intent);
        }
    }

    /**
     * 跳转到设置页面
     */
    public static void gotoSettingActivity(Activity activity) {
        if (activity != null) {
            Intent intent = new Intent(activity, SettingActivity.class);
            activity.startActivity(intent);
        }
    }

    /**
     * 跳转到邀请好友页面
     */
    public static void gotoInviteFriendActivity(Activity activity, SharePosterBean posterBean) {
        if (activity != null) {
            Intent intent = new Intent(activity, SettingActivity.class);
            MomentPosterActivity.gotoShare(posterBean);

            // activity.startActivity(intent);
        }
    }

    /**
     * 跳转到扫描二维码页面
     */
    public static void gotoScanQRActivity(Activity activity) {
        if (activity != null) {
            PermissionUtil.requestCameraPermission(activity, new PermissionUtil.PermissionCallback() {
                @Override
                public void granted() {
                    Intent intent = new Intent(activity, QRActivity.class);

                    activity.startActivity(intent);

                }

                @Override
                public void denied() {

                }

                @Override
                public void gotoSetting() {

                }
            });

        }
    }

    /**
     * 跳转到客服中心页面
     */
    public static void gotoCustomerServiceActivity(Activity activity) {
        if (activity != null) {
            Intent intent = new Intent(activity, WebViewActivity.class);
            intent.putExtra(WebViewActivity.SITE, "help");
            activity.startActivity(intent);
        }
    }

    /**
     * 跳转到评分页面
     */
    public static void gotoDirectToAppActivity(Activity activity) {
        if (activity != null) {
            ViewUtils.directToAppStore(TheLApp.getContext());
            // activity.startActivity(intent);
        }
    }

    /**
     * 跳转系统权限设置页面
     */
    public static void gotoSystemLocationSetting(Activity activity, OnActivityResultCallback onActivityResultCallback) {
        if (activity != null) {
            if (activity instanceof BaseActivity && onActivityResultCallback != null) {
                ((BaseActivity) activity).setOnActivityResultCallback(onActivityResultCallback);
            }
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
            intent.setData(uri);
            activity.startActivityForResult(intent, TheLConstants.PERMISSIONS_REQUEST_READ_LOCATION);
        }
    }

    /**
     * 跳转rela活动页
     */
    public static void gotoRelaActivities(Activity activity, LiveFollowerUsersBean liveFollowerUsersBean) {
        if (activity == null) {
            return;
        }

        Intent intent = null;

        switch (liveFollowerUsersBean.type) {
            case "live":
                if (liveFollowerUsersBean.isLandscape == 1) {
                    intent = new Intent(activity, LiveWatchHorizontalActivity.class);
                    intent.putExtra(LiveWatchActivity.FROM_PAGE, LiveWatchActivity.FROM_PAGE_FRIEND_LIVE_HOME_PAGE);
                    intent.putExtra(TheLConstants.BUNDLE_KEY_ID, liveFollowerUsersBean.roomId + "");
                    intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, liveFollowerUsersBean.userId + "");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    activity.startActivity(intent);
                } else {

                    intent = new Intent(activity, LiveWatchActivity.class);
                    intent.putExtra(LiveWatchActivity.FROM_PAGE, LiveWatchActivity.FROM_PAGE_FRIEND_LIVE_HOME_PAGE);
                    intent.putExtra(TheLConstants.BUNDLE_KEY_ID, liveFollowerUsersBean.roomId + "");
                    intent.putExtra(TheLConstants.BUNDLE_KEY_ID, liveFollowerUsersBean.userId + "");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    activity.startActivity(intent);
                }
                break;
            case "activity":
                MsgBean msgBean = new MsgBean();
                msgBean.messageTo = liveFollowerUsersBean.messageTo;
                msgBean.advertUrl = liveFollowerUsersBean.dumpURL;
                msgBean.advertTitle = liveFollowerUsersBean.advertTitle;
                msgBean.userId = liveFollowerUsersBean.userId;
                msgBean.userName = liveFollowerUsersBean.nickName;
                msgBean.avatar = liveFollowerUsersBean.avatar;
                intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                PushUtils.intentActivity(msgBean, intent);
                break;
            case "guest":
                intent = new Intent(activity, LiveWatchActivity.class);
                intent.putExtra(TheLConstants.BUNDLE_KEY_ID, liveFollowerUsersBean.roomId);
                intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, liveFollowerUsersBean.userId);
                intent.putExtra(LiveWatchActivity.FROM_PAGE, LiveWatchActivity.FROM_PAGE_WEB);
                activity.startActivity(intent);
                break;
            default:
                break;
        }
    }

    /**
     * 判断用户有没有绑定手机号
     */
    public static void isBindPhone(Activity activity, RfSResultCallback rfSResultCallback) {
        if (activity == null) return;

        String binded = "1";

        String cell = ShareFileUtils.getString(ShareFileUtils.BIND_CELL, "");
        // 简体中文
        if (ShareFileUtils.getString(ShareFileUtils.localLanguage, "zh-CN").equals("zh-CN")) {
            if (TextUtils.isEmpty(cell) || "null".equals(cell)) {
                //打开注册页面
                LoginRegisterActivity.startActivity(activity, LoginRegisterActivity.PHONE_VERIFY, false);
                binded = "0";

            } else {
                binded = "1";

            }
        }
        String jsonString = "{" +
                "\"binded\":" + "\"" + binded + "\"" +
                "}";
        rfSResultCallback.onResult(jsonString);
    }

    /**
     * 发布直播页
     */
    public static void gotoReleaseLiveActivity(Activity activity) {
        if (activity == null) return;
        if (UserUtils.isVerifyCell())
            PermissionUtil.requestCameraPermission(activity, new PermissionUtil.PermissionCallback() {
                @Override
                public void granted() {
                    PermissionUtil.requestRecordPermission(activity, new PermissionUtil.PermissionCallback() {
                        @Override
                        public void granted() {
                            if (ShareFileUtils.getInt(TheLConstants.live_permit, 0) == 1) {
                                final Intent intent = new Intent(activity, ReleaseLiveMomentActivity.class);
                                activity.startActivity(intent);
                            } else {
                                RequestBusiness.getInstance().getLivePermit().onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<LivePermitBean>() {
                                    @Override
                                    public void onNext(LivePermitBean livePermitBean) {
                                        super.onNext(livePermitBean);
                                        if (livePermitBean != null && livePermitBean.data != null) {
                                            ShareFileUtils.setInt(TheLConstants.live_permit, livePermitBean.data.perm);
                                            if (ShareFileUtils.getInt(TheLConstants.live_permit, 0) == 1) {
                                                Intent intent = new Intent(TheLApp.getContext(), ReleaseLiveMomentActivity.class);

                                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                TheLApp.getContext().startActivity(intent);
                                            } else {
                                                DialogUtil.showConfirmDialog(activity, "", TheLApp.getContext().getString(R.string.no_live_permition_tip), TheLApp.getContext().getString(R.string.info_continue), TheLApp.getContext().getString(R.string.info_no), new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        dialog.dismiss();
                                                        activity.startActivity(new Intent(activity, ZhimaCertificationActivity.class));
                                                    }
                                                });

                                            }
                                        }
                                    }
                                });
                            }
                        }

                        @Override
                        public void denied() {

                        }

                        @Override
                        public void gotoSetting() {

                        }
                    });

                }

                @Override
                public void denied() {

                }

                @Override
                public void gotoSetting() {

                }
            });
    }

    /**
     * 发布日志分享权限页
     */
    public static void gotoSharePermissionActivity(Activity activity, int position, OnActivityResultCallback onActivityResultCallback) {
        if (activity != null) {
            if (activity instanceof BaseActivity) {
                ((BaseActivity) activity).setOnActivityResultCallback(onActivityResultCallback);
            }
            Intent intent = new Intent(activity, MomentPermissionActivity.class);
            intent.putExtra("position", position);
            activity.startActivityForResult(intent, TheLConstants.BUNDLE_CODE_SHARE_PERMISSION);
        }
    }

    /**
     * 发布伴侣日志页
     */
    public static void gotoReleaseLoveActivity(Activity activity, RfSResultCallback rfSResultCallback) {
        if (activity == null) return;
        PermissionUtil.requestStoragePermission(activity, new PermissionUtil.PermissionCallback() {
            @Override
            public void granted() {
                if (UserUtils.isVerifyCell()) {
                    ShareFileUtils.setBoolean(Utils.getMyUserId() + "_" + IS_RELEASE_MOMENT, true);

                    RequestBusiness.getInstance().getMyCircleData().onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<MyCircleFriendListBean>() {
                        @SuppressLint("CheckResult")
                        @Override
                        public void onNext(MyCircleFriendListBean myCircelBean) {
                            super.onNext(myCircelBean);
                            if (myCircelBean != null && myCircelBean.data != null && myCircelBean.data.paternerd != null && 1 == myCircelBean.data.paternerd.requestStatus) {//有情侣
                                Intent intent = new Intent(TheLApp.getContext(), ReleaseMomentActivity.class);
                                intent.putExtra(TheLConstants.BUNDLE_KEY_RELEASE_TYPE, ReleaseMomentActivity.RELEASE_TYPE_LOVE);
                                Bundle bundle = new Bundle();
                                bundle.putSerializable("partner", myCircelBean.data.paternerd);
                                intent.putExtras(bundle);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                TheLApp.getContext().startActivity(intent);
                                Observable.timer(500, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(aLong -> rfSResultCallback.onResult("1"));
                            } else {
                                DialogUtil.showConfirmDialog(FlutterBoostPlugin.currentActivity(), "", TheLApp.getContext().getString(R.string.have_no_lover), TheLApp.getContext().getString(R.string.bind_lover), TheLApp.getContext().getString(R.string.info_no), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent intent = new Intent(TheLApp.getContext(), MyCircleActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        TheLApp.getContext().startActivity(intent);
                                        dialog.cancel();
                                    }
                                });
                            }
                        }
                    });
                }
            }

            @Override
            public void denied() {

            }

            @Override
            public void gotoSetting() {

            }
        });
    }

    /**
     * 举报个人主页用户
     */
    public static void gotoReportActivity(Activity activity, String userId) {
        if (activity != null) {
            Intent intent = new Intent(activity, ReportActivity.class);
            intent.putExtra(TheLConstants.BUNDLE_KEY_REPORT_TYPE, ReportActivity.REPORT_TYPE_REPORT_USER);
            intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, userId);
            activity.startActivity(intent);
        }
    }

    /**
     * push设置的页面
     */
    public static void gotoPushSettingsActivity(Activity activity) {
        if (activity != null) {
            Intent intent = new Intent(activity, SettingsActivity.class);
            intent.putExtra(SettingsActivity.PAGE_TYPE_TAG, SettingsActivity.PAGE_TYPE_PUSH);
            activity.startActivity(intent);
        }
    }

    /**
     * 推荐日志/直播用户/网页到聊天
     */
    public static void gotoChatActivity(Activity activity, String jsonString) {
        L.d("gotoChatActivity", "jsonString: " + jsonString);
        L.d("gotoChatActivity", "activity: " + activity);
        if (activity != null) {

            LocalBroadcastManager.getInstance(activity).sendBroadcast(new Intent(LiveShowPresenter.BR_RECOMMEND));

            RecommendedModel recommendedModel = GsonUtils.getObject(jsonString, RecommendedModel.class);
            final Intent intent = new Intent(activity, ChatActivity.class);
            Bundle bundle = new Bundle();
            intent.putExtra("toUserId", String.valueOf(recommendedModel.receiveUserId));
            intent.putExtra("toAvatar", recommendedModel.receiveAvatar);
            intent.putExtra("toName", recommendedModel.receiveNickName);
            intent.putExtra("recommendText", recommendedModel.momentsText);

            if (recommendedModel.recommendedType.equals("liveUser")) {
                LiveRoomBean liveuserbean = new LiveRoomBean();
                if (recommendedModel.msgType.equals("moment_voicelive")) {
                    liveuserbean.audioType = 1;
                } else {
                    liveuserbean.audioType = 0;
                }
                liveuserbean.user.id = recommendedModel.userId;
                liveuserbean.user.nickName = recommendedModel.nickname;
                liveuserbean.user.avatar = recommendedModel.avatar;
                liveuserbean.id = recommendedModel.liveId;
                liveuserbean.text = recommendedModel.momentsText;
                liveuserbean.isLandscape = recommendedModel.isLandscape ? 1 : 0;
                intent.putExtra(TheLConstants.BUNDLE_KEY_INTENT_FROM, ChatActivity.FROM_PAGE_LIVE_USER);
                bundle.putSerializable(TheLConstants.BUNDLE_KEY_LIVE_ROOM, liveuserbean);
            } else if (recommendedModel.recommendedType.equals("web")) {
                RecommendWebBean recommendWebBean = new RecommendWebBean();
                recommendWebBean.imageUrl = recommendedModel.imageUrl;
                recommendWebBean.momentsText = recommendedModel.momentsText;
                recommendWebBean.title = recommendedModel.title;
                recommendWebBean.url = recommendedModel.url;
                intent.putExtra(TheLConstants.BUNDLE_KEY_INTENT_FROM, ChatActivity.FROM_PAGE_WEBVIEW);
                bundle.putSerializable(TheLConstants.BUNDLE_KEY_RECOMMEND_WEB_BEAN, recommendWebBean);
            } else if (recommendedModel.recommendedType.equals("user")) {
                UserCardBean usercardbean = new UserCardBean();
                if (!recommendedModel.cardUserId.isEmpty())
                    usercardbean.userId = recommendedModel.cardUserId;
                usercardbean.nickName = recommendedModel.cardNickName;
                usercardbean.avatar = recommendedModel.cardAvatar;
                usercardbean.bgImage = recommendedModel.cardBgImage;
                if (!recommendedModel.cardAge.isEmpty())
                    usercardbean.birthday = recommendedModel.cardAge;
                usercardbean.affection = recommendedModel.cardAffection;
                usercardbean.recommendDesc = recommendedModel.cardIntro;
                bundle.putSerializable("usercardbean", usercardbean);
                intent.putExtra("send", 1);
            } else {
                MomentsBean momentsBean = new MomentsBean();
                momentsBean.momentsId = String.valueOf(recommendedModel.momentsId);
                momentsBean.userId = recommendedModel.userId;
                momentsBean.nickname = recommendedModel.nickname;
                momentsBean.avatar = recommendedModel.avatar;
                momentsBean.momentsText = recommendedModel.momentsText;
                momentsBean.momentsType = recommendedModel.momentsType;
                momentsBean.commentNum = recommendedModel.commentNum;
                momentsBean.imageUrl = recommendedModel.imageUrl;
                momentsBean.pixelWidth = recommendedModel.pixelWidth;
                momentsBean.pixelHeight = recommendedModel.pixelHeight;
                momentsBean.playTime = recommendedModel.playTime;
                momentsBean.playCount = recommendedModel.playCount;
                momentsBean.shareTo = recommendedModel.shareTo;
                momentsBean.adUrl = recommendedModel.adUrl;
                momentsBean.msgText = recommendedModel.msgText;
                bundle.putSerializable(TheLConstants.BUNDLE_KEY_MOMENT_BEAN, momentsBean);
                intent.putExtra("recommend", 2);
            }
            intent.putExtras(bundle);
            activity.startActivity(intent);
        }
    }

    /**
     * 打开系统GPS设置页
     *
     * @param activity
     * @param onActivityResultCallback
     */
    public static void openGPSSetting(@NonNull final Activity activity, OnActivityResultCallback onActivityResultCallback) {
        if (activity instanceof BaseActivity && onActivityResultCallback != null) {
            ((BaseActivity) activity).setOnActivityResultCallback(onActivityResultCallback);
            new ActionSheetDialog(activity).builder()
                    .setTitle(getString(R.string.open_gps))
                    .setCancelable(true)
                    .setCanceledOnTouchOutside(true)
                    .addSheetItem(getString(R.string.info_set), ActionSheetDialog.SheetItemColor.BLACK, which -> {
                        //跳转到手机原生设置页面
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        activity.startActivityForResult(intent, TheLConstants.PERMISSIONS_REQUEST_GPS);
                    }).show();
        }
    }

    /**
     * 分享到第三方
     */
    public static void shareToChannel(@NonNull final Activity activity, RfSRpcRecommend.RfS_ActionsRecommend.Actions action, String json, OnActivityResultCallback onActivityResultCallback) {

        if (activity instanceof BaseActivity) {
            ((BaseActivity) activity).setOnActivityResultCallback(onActivityResultCallback);
        }
        RecommendedModel recommendedModel = GsonUtils.getObject(json, RecommendedModel.class);
        if (recommendedModel.recommendedType.equals("user")) {
            Intent intent = new Intent(activity, PosterActivity.class);
            intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, String.valueOf(recommendedModel.cardUserId));
            int type = PosterActivity.DOWNLOAD;
            switch (action) {
                case WECHAT:
                    type = PosterActivity.SHARE_TYPE_WX;
                    break;
                case WX_MOMENT:
                    type = PosterActivity.SHARE_TYPE_WX_CIRCLE;
                    break;
                case QQ:
                    type = PosterActivity.SHARE_TYPE_QQ;
                    break;
                case QZONE:
                    type = PosterActivity.SHARE_TYPE_QZONE;
                    break;
                case SINA:
                    type = PosterActivity.SHARE_TYPE_WEIBO;
                    break;
                case FACEBOOK:
                    type = PosterActivity.SHARE_TYPE_FACEBOOK;
                    break;
            }
            intent.putExtra("shareType", type);
            activity.startActivity(intent);
        }else {
            Intent intent = new Intent();
            intent.setAction(LiveShowPresenter.BR_SHARE);
            LocalBroadcastManager.getInstance(activity).sendBroadcast(intent);

            PermissionUtil.requestStoragePermission(activity, new PermissionUtil.PermissionCallback() {
                @Override
                public void granted() {
                    try {
                        String activitys = "";
                        String url = "";
                        String newUrl = "";
                        String logo = "";
                        String shareTitle = "";
                        String shareContent = "";
                        String singleTitle = "";

                        UMShareListener umShareListener = new UMShareListener() {

                            @Override
                            public void onStart(SHARE_MEDIA share_media) {

                            }

                            @Override
                            public void onResult(SHARE_MEDIA platform) {
                                MobclickAgent.onEvent(activity, "share_succeeded");
                            }

                            @Override
                            public void onError(SHARE_MEDIA platform, Throwable t) {
                                if (t != null && t.getMessage() != null) {
                                    DialogUtil.showToastShort(activity, t.getMessage());
                                }
                            }

                            @Override
                            public void onCancel(SHARE_MEDIA platform) {
                                //这里注销掉cancel提示，分享成功点击返回也会走到onCancel
//            DialogUtil.showToastShort(getContext(), TheLApp.getContext().getString(R.string.my_circle_requests_act_canceled));
                            }
                        };

                        GIOShareTrackBean gioTrackBean = new GIOShareTrackBean();
                        RecommendedModel momentBean = GsonUtils.getObject(json, RecommendedModel.class);
                        if (momentBean != null) {

                            MomentsBean momentsBean2 = new MomentsBean();
                            momentsBean2.momentsType = momentBean.momentsType;
                            GrowingIOUtil.setMomentShareTrack(momentsBean2, gioTrackBean);

                            logo = MomentsBean.MOMENT_TYPE_THEME.equals(momentBean.momentsType) ? TheLConstantsExt.SHARE_THEME_LOGO_URL : TheLConstantsExt.DEFAULT_SHARE_LOGO_URL;

                            if (MomentsBean.MOMENT_TYPE_IMAGE.equals(momentBean.momentsType) || MomentsBean.MOMENT_TYPE_TEXT_IMAGE.equals(momentBean.momentsType) || MomentsBean.MOMENT_TYPE_THEME_REPLY.equals(momentBean.momentsType) || MomentsBean.MOMENT_TYPE_THEME.equals(momentBean.momentsType) || MomentsBean.MOMENT_TYPE_USER_CARD.equals(momentBean.momentsType)) {
                                if (!TextUtils.isEmpty(momentBean.imageUrl)) {
                                    String[] photoUrls = momentBean.imageUrl.split(",");
                                    if (photoUrls.length > 0) {
                                        logo = photoUrls[0];
                                    }
                                }
                            } else if (MomentsBean.MOMENT_TYPE_VIDEO.equals(momentBean.momentsType) || MomentsBean.MOMENT_TYPE_LIVE.equals(momentBean.momentsType)) {
                                if (!TextUtils.isEmpty(momentBean.imageUrl)) {
                                    logo = momentBean.imageUrl;
                                }
                            }
                            try {
                                if (MomentsBean.MOMENT_TYPE_THEME_REPLY.equals(momentBean.momentsType)) {//话题回复
                                    String defaults = activity.getResources().getString(R.string.default_share_talk, momentBean.nickname);

                                    shareTitle = activity.getString(R.string.share_theme_reply);
                                    shareContent = TextUtils.isEmpty(momentBean.momentsText) ? defaults : momentBean.momentsText;
                                    singleTitle = momentBean.momentsText;
                                    url = TheLConstants.ALL_MOMENT_SHARE_URL(String.valueOf(momentBean.momentsId));
                                    newUrl = getNewUrl(url);

                                } else if (MomentsBean.MOMENT_TYPE_THEME.equals(momentBean.momentsType)) {//话题
                                    String defaults = activity.getResources().getString(R.string.default_share_talk, momentBean.nickname);

                                    shareTitle = activity.getString(R.string.share_released_topic);
                                    shareContent = TextUtils.isEmpty(momentBean.momentsText) ? defaults : momentBean.momentsText;
                                    singleTitle = momentBean.momentsText;
                                    url = TheLConstants.ALL_MOMENT_SHARE_URL(String.valueOf(momentBean.momentsId));
                                    newUrl = getNewUrl(url);

                                } else if (MomentsBean.MOMENT_TYPE_VIDEO.equals(momentBean.momentsType)) {//视频
                                    String defaults = activity.getResources().getString(R.string.share_video_content, momentBean.nickname);

                                    shareTitle = getString(R.string.share_video_title);
                                    shareContent = TextUtils.isEmpty(momentBean.momentsText) ? defaults : momentBean.momentsText;
                                    singleTitle = defaults;
                                    url = TheLConstants.VIDEO_MOMENT_SHARE_URL(String.valueOf(momentBean.momentsId));
                                    newUrl = getNewUrl(url);

                                } else if (MomentsBean.MOMENT_TYPE_TEXT_IMAGE.equals(momentBean.momentsType) || MomentsBean.MOMENT_TYPE_IMAGE.equals(momentBean.momentsType) || MomentsBean.MOMENT_TYPE_TEXT.equals(momentBean.momentsType)) {
                                    String defaults = activity.getResources().getString(R.string.share_theme_content, momentBean.nickname);

                                    shareTitle = activity.getString(R.string.share_theme_journal);
                                    shareContent = TextUtils.isEmpty(momentBean.momentsText) ? defaults : momentBean.momentsText;
                                    singleTitle = defaults;
                                    url = TheLConstants.ALL_MOMENT_SHARE_URL(String.valueOf(momentBean.momentsId));
                                    newUrl = getNewUrl(url);


                                } else if (MomentsBean.MOMENT_TYPE_LIVE.equals(momentBean.momentsType) || MomentsBean.MOMENT_TYPE_VOICE_LIVE.equals(momentBean.momentsType)) {//直播

                                    shareTitle = momentBean.momentsText;
                                    shareContent = momentBean.title;
                                    singleTitle = momentBean.momentsText;
                                    url = TheLConstants.LIVE_MOMENT_SHARE_URL(momentBean.userId + "");
                                    logo = momentBean.imageUrl;
                                    newUrl = getNewUrl(url);


                                } else if (MomentsBean.MOMENT_TYPE_VOICE.equals(momentBean.momentsType) || MomentsBean.MOMENT_TYPE_TEXT_VOICE.equals(momentBean.momentsType)) {//音乐日志（音乐与文本音乐）
                                    String defaults = activity.getResources().getString(R.string.share_music_content, momentBean.nickname);

                                    shareTitle = activity.getString(R.string.share_music_title);
                                    shareContent = TextUtils.isEmpty(momentBean.momentsText) ? defaults : momentBean.momentsText;
                                    singleTitle = defaults;
                                    url = TheLConstants.ALL_MOMENT_SHARE_URL(String.valueOf(momentBean.momentsId));
                                    newUrl = getNewUrl(url);

                                } else if (MomentsBean.MOMENT_TYPE_WEB.equals(momentBean.momentsType)) {//网页
                                    shareTitle = TextUtils.isEmpty(momentBean.title) ? "Rela热拉" : momentBean.title;
                                    shareContent = TextUtils.isEmpty(momentBean.momentsText) ? getString(R.string.share_content_event) : momentBean.momentsText;
                                    singleTitle = TextUtils.isEmpty(momentBean.title) ? getString(R.string.share_content_event) : momentBean.title;
                                    logo = TextUtils.isEmpty(momentBean.imageUrl) ? TheLConstantsExt.DEFAULT_SHARE_LOGO_URL : momentBean.imageUrl;
                                    url = momentBean.url;
                                    String addurl = "";
                                    switch (action) {
                                        case WX_MOMENT:
                                            addurl = "wx_timeline";
                                            break;
                                        case WECHAT:
                                            addurl = "wx_appmsg";
                                            break;
                                        case SINA:
                                            addurl = "weibo";
                                            break;
                                        case QQ:
                                            addurl = "qq_appmsg";
                                            break;
                                        case QZONE:
                                            addurl = "qq_zone qq";
                                            break;
                                        case FACEBOOK:
                                            addurl = "facebook";
                                            return;
                                    }
                                    newUrl = getWebnewurl(url, addurl);
                                } else {//默认为文本
                                    String defaults = activity.getResources().getString(R.string.share_theme_content, momentBean.nickname);
                                    shareTitle = activity.getString(R.string.share_theme_journal);
                                    shareContent = TextUtils.isEmpty(momentBean.momentsText) ? defaults : momentBean.momentsText;
                                    singleTitle = defaults;
                                    url = TheLConstants.ALL_MOMENT_SHARE_URL(String.valueOf(momentBean.momentsId));
                                    newUrl = getNewUrl(url);

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        switch (action) {
                            case WX_MOMENT:
                                MobclickAgent.onEvent(activity, "start_share");
                                activitys = "share_wechatmoments";
                                UMShareUtils.share(activity, SHARE_MEDIA.WEIXIN_CIRCLE, new UMImage(TheLApp.getContext(), ImageUtils.buildNetPictureUrl(logo, TheLConstants.WX_SHARE_IMAGE_SIZE, TheLConstants.WX_SHARE_IMAGE_SIZE)), newUrl, singleTitle, singleTitle, umShareListener, gioTrackBean);
                                break;
                            case WECHAT:
                                MobclickAgent.onEvent(activity, "start_share");
                                activitys = "share_wechat";
                                UMShareUtils.share(activity, SHARE_MEDIA.WEIXIN, new UMImage(TheLApp.getContext(), ImageUtils.buildNetPictureUrl(logo, TheLConstants.WX_SHARE_IMAGE_SIZE, TheLConstants.WX_SHARE_IMAGE_SIZE)), newUrl, shareTitle, shareContent, umShareListener, gioTrackBean);
                                break;
                            case SINA:
                                MobclickAgent.onEvent(activity, "start_share");
                                activitys = "share_weibo";
                                UMShareUtils.share(activity, SHARE_MEDIA.SINA, new UMImage(TheLApp.getContext(), logo), newUrl, singleTitle, singleTitle, umShareListener, gioTrackBean);
                                break;
                            case QQ:
                                MobclickAgent.onEvent(activity, "start_share");
                                activitys = "share_qq";
                                UMShareUtils.share(activity, SHARE_MEDIA.QQ, new UMImage(TheLApp.getContext(), logo), newUrl, shareTitle, shareContent, umShareListener, gioTrackBean);
                                break;
                            case QZONE:
                                activitys = "share_qzone";
                                UMShareUtils.share(activity, SHARE_MEDIA.QZONE, new UMImage(TheLApp.getContext(), logo), url, shareTitle, shareContent, umShareListener, gioTrackBean);
                                break;
                            case FACEBOOK:
                                MobclickAgent.onEvent(activity, "start_share");
                                if (ShareDialog.canShow(ShareLinkContent.class)) {
                                    ShareLinkContent contentS = new ShareLinkContent.Builder().setContentUrl(Uri.parse(newUrl)).setContentTitle(shareTitle).setContentDescription(shareContent).setImageUrl(Uri.parse(logo)).build();
                                    ShareDialog shareDialog = new ShareDialog(activity);
                                    shareDialog.show(contentS);
                                    if (gioTrackBean != null) {
                                        gioTrackBean.shareBean.shareType = GrowingIoConstant.TYPE_FACEBOOK;
                                        GrowingIOUtil.shareTrack(gioTrackBean);
                                    }
                                }
                                return;
                            default:
                                break;
                        }

                        LogInfoBean logInfoBean = new LogInfoBean();
                        logInfoBean.activity = activitys;
                        logInfoBean.page = "share_page";
                        logInfoBean.page_id = Utils.getPageId();
                        logInfoBean.ua = android.os.Build.BRAND.toUpperCase() + "," + Build.MODEL.toUpperCase() + ",ANDROID" + Build.VERSION.RELEASE.toUpperCase();

                        LogInfoBean.LogsDataBean dataBean = new LogInfoBean.LogsDataBean();
                        if (momentBean != null) {
                            dataBean.data_id = String.valueOf(momentBean.momentsId);
                            dataBean.share_id = Utils.getPageId();
                            dataBean.data_type = momentBean.momentsType;
                        }

                        logInfoBean.data = dataBean;
                        String json = "[" + GsonUtils.createJsonString(logInfoBean) + "]";
                        MatchLogUtils.getInstance().send(json);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void denied() {
                    ToastUtils.showToastShort(activity, "缺少文件读写权限，无法分享");
                }

                @Override
                public void gotoSetting() {

                }
            });
        }
    }

    private static String getWebnewurl(String url, String addurl) {
        String newurl = url;
        if (url.contentEquals("?")) {
            newurl = url + "&rela_webview_share=" + addurl;
        } else {
            newurl = url + "?rela_webview_share=" + addurl;
        }
        return newurl;
    }

    private static String getNewUrl(String url) {

        String addParm = ShareFileUtils.getString(ShareFileUtils.ID, "");
        addParm = addParm + "&page_id=" + Utils.getPageId() + "&page=share_page";

        String newUrl;
        if (url.contentEquals("?")) {
            newUrl = url + "&shareUserId=" + addParm;
        } else {
            newUrl = url + "?shareUserId=" + addParm;
        }
        return newUrl;
    }

    public static void gotoPosterActivity(Activity activity, String json, OnActivityResultCallback onActivityResultCallback) {
        if (activity != null) {
            if (activity instanceof BaseActivity) {
                ((BaseActivity) activity).setOnActivityResultCallback(onActivityResultCallback);
            }
            RecommendedModel recommendedModel = GsonUtils.getObject(json, RecommendedModel.class);
            if (recommendedModel.recommendedType.equals("user")) {
                Intent intent = new Intent(activity, PosterActivity.class);
                intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, String.valueOf(recommendedModel.cardUserId));
                intent.putExtra("shareType", PosterActivity.DOWNLOAD);
                activity.startActivityForResult(intent, TheLConstants.REQUEST_SAVE_POSTER);
            } else {
                SharePosterBean sharePosterBean = new SharePosterBean();
                sharePosterBean.nickname = recommendedModel.nickname;
                sharePosterBean.avatar = recommendedModel.avatar;
                sharePosterBean.momentsText = recommendedModel.momentsText;
                sharePosterBean.imageUrl = recommendedModel.imageUrl;
                sharePosterBean.userId = String.valueOf(recommendedModel.userId);
                sharePosterBean.from = MomentPosterActivity.FROM_MOMENT;
                Intent intent = new Intent(TheLApp.getContext(), MomentPosterActivity.class);
                intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_BEAN, sharePosterBean);
                intent.putExtra("shareType", MomentPosterActivity.SHARE_TYPE_DOWNLOAD);
                activity.startActivityForResult(intent, TheLConstants.REQUEST_SAVE_POSTER);
            }
        }
    }
}
