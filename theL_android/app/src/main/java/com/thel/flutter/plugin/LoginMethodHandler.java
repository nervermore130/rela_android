package com.thel.flutter.plugin;

import android.app.Activity;

import androidx.annotation.NonNull;

import java.lang.ref.WeakReference;

import io.flutter.app.FlutterFragmentActivity;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.view.FlutterView;

/**
 * @author liuyun
 */
public class LoginMethodHandler implements MethodChannel.MethodCallHandler {

    private static final String TAG = "LoginMethodHandler";

    private static final String METHOND_CHANNEL = "com.thel.login.plugin/method";

    private Activity mActivity;

    private WeakReference<Activity> activityWeakReference;

    private static LoginMethodHandler instance;

    private LoginResultDelegate mLoginResultDelegate;

    public static LoginMethodHandler getInstance() {
        if (instance == null) {
            instance = new LoginMethodHandler();
        }
        return instance;
    }

    @Override public void onMethodCall(MethodCall methodCall, MethodChannel.Result result) {


        if (mLoginResultDelegate != null) {

            mLoginResultDelegate.handleResult(methodCall, result);

        }

    }

    public void registerWith(@NonNull FlutterView flutterView, Activity activity) {

        activityWeakReference = new WeakReference<>(activity);

        mActivity = activityWeakReference.get();

        mLoginResultDelegate = new LoginResultDelegate(mActivity);

        MethodChannel methodChannel = new MethodChannel(flutterView, METHOND_CHANNEL);

        methodChannel.setMethodCallHandler(instance);


    }

    public LoginResultDelegate getLoginResultDelegate(){
        return  mLoginResultDelegate;
    }


    public void onDestroy() {
        try {
            if (activityWeakReference != null) {
                activityWeakReference.clear();
            }

            if (mLoginResultDelegate != null) {
                mLoginResultDelegate.destroy();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
