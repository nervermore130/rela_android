package com.thel.flutter.bridge;

import android.text.TextUtils;

import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UploadManager;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseDataBean;
import com.thel.bean.user.UploadTokenBean;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.LoginInterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.RequestConstants;
import com.thel.utils.ImageUtils;
import com.thel.utils.L;
import com.thel.utils.MD5Utils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.ToastUtils;

import org.json.JSONObject;

import java.io.File;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class UploadFileManager {

    private static final String TAG = "UploadFileManager";
    private static final int TYPE_AVATAR = 1;//上传头像
    private static final int TYPE_BG = 2;//上传背景

    private OnImageUpdateListener mOnImageUpdateListener;

    public UploadFileManager(String avatarPhotoPath, OnImageUpdateListener onImageUpdateListener) {
        mOnImageUpdateListener = onImageUpdateListener;

        uploadPhoto(avatarPhotoPath, TYPE_AVATAR);
    }

    public UploadFileManager(String avatarPhotoPath, OnImageUpdateListener onImageUpdateListener, int type) {
        mOnImageUpdateListener = onImageUpdateListener;

        uploadPhoto(avatarPhotoPath, TYPE_BG);
    }


    private void uploadPhoto(String photoPath, int type) {


        if (mOnImageUpdateListener != null) {
            mOnImageUpdateListener.onStart();
        }
        String path = "";
        if (TYPE_AVATAR == type) {
            path = RequestConstants.UPLOAD_FILE_ROOT_PATH_AVATAR + ShareFileUtils.getString(ShareFileUtils.ID, "") + "/" + MD5Utils.calculateMD5(new File(photoPath)) + ".jpg";

        } else if (TYPE_BG == type) {
            path = RequestConstants.UPLOAD_FILE_ROOT_PATH_BG_IMG + ShareFileUtils.getString(ShareFileUtils.ID, "") + "/" + MD5Utils.calculateMD5(new File(photoPath)) + ".jpg";

        }
        RequestBusiness.getInstance().getUploadToken(System.currentTimeMillis() + "", "", path).onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new MyConsumer(path, photoPath, type));
    }

    public class MyConsumer extends LoginInterceptorSubscribe<UploadTokenBean> {

        String picUploadPath;

        String picLocalPath;

        int picType;

        public MyConsumer(String picUploadPath, String picLocalPath, int type) {
            this.picUploadPath = picUploadPath;
            this.picLocalPath = picLocalPath;
            this.picType = type;
        }

        @Override
        public void onError(Throwable t) {
            super.onError(t);
        }

        @Override
        public void onNext(UploadTokenBean uploadTokenBean) {
            super.onNext(uploadTokenBean);

            L.d(TAG, " picUploadPath : " + picUploadPath);

            L.d(TAG, " picLocalPath : " + picLocalPath);

            if (!TextUtils.isEmpty(uploadTokenBean.data.uploadToken)) {
                UploadManager uploadManager = new UploadManager();
                // 上传头像图片
                if (!TextUtils.isEmpty(picUploadPath) && !TextUtils.isEmpty(picLocalPath)) {
                    uploadManager.put(new File(picLocalPath), picUploadPath, uploadTokenBean.data.uploadToken, new UpCompletionHandler() {
                        @Override
                        public void complete(String key, ResponseInfo info, JSONObject response) {

                            L.d(TAG, " info : " + info);


                            L.d(TAG, " key : " + key);

                            try {

                                if (info != null && info.statusCode == 200 && picUploadPath.equals(key)) {
                                    List<Integer> wh = ImageUtils.measurePicWidthAndHeight(picLocalPath);
                                    if (wh.size() == 2) {
                                        String avatarUrl = RequestConstants.FILE_BUCKET + key;

                                        if (mOnImageUpdateListener != null) {
                                            mOnImageUpdateListener.onSuccess(avatarUrl);
                                        }

                                        if (picType == TYPE_BG) {
                                            uploadBgImage(avatarUrl);
                                        }

                                    }
                                } else {
                                    if (mOnImageUpdateListener != null) {
                                        mOnImageUpdateListener.onFailed();
                                    }
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    }, null);
                } else {
                    ToastUtils.showToastShort(TheLApp.context, TheLApp.context.getString(R.string.udesk_upload_img_error));

                }
            } else {
                ToastUtils.showToastShort(TheLApp.context, TheLApp.context.getString(R.string.udesk_upload_img_error));

            }
        }
    }

    private void uploadBgImage(final String bgimage) {
        Flowable<BaseDataBean> flowable = RequestBusiness.getInstance().uploadBgImage(bgimage);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
            @Override
            public void onNext(BaseDataBean baseDataBean) {
                super.onNext(baseDataBean);
                L.d("更新背景图片了");
            }
        });
    }

    public interface OnImageUpdateListener {

        void onStart();

        void onFailed();

        void onSuccess(String path);

    }

}
