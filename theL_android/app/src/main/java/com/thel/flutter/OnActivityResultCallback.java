package com.thel.flutter;

import android.content.Intent;

/**
 * Created by chad
 * Time 2019-05-29
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */
public interface OnActivityResultCallback {
    void onActivityResult(int requestCode, int resultCode, Intent data);
}
