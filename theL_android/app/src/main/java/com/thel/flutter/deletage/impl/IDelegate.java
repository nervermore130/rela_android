package com.thel.flutter.deletage.impl;

import me.rela.rf_s_bridge.RfSResultCallback;

public interface IDelegate {

    void setResultCallback(RfSResultCallback rfSResultCallback);

}
