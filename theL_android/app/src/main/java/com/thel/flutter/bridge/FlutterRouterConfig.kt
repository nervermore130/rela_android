package com.thel.flutter.bridge

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.google.gson.Gson
import com.thel.TagBean
import com.thel.app.TheLApp
import com.thel.bean.moments.MomentsBean
import com.thel.constants.BundleConstants
import com.thel.constants.TheLConstants
import com.thel.flutter.FlutterCommonActivity
import com.thel.flutter.FlutterRouterPathConstants
import com.thel.flutter.NewRelaBoostFlutterActivity
import com.thel.modules.live.bean.LiveFollowerUsersBean
import com.thel.modules.live.surface.watch.LiveWatchActivity
import com.thel.modules.live.surface.watch.horizontal.LiveWatchHorizontalActivity
import com.thel.modules.main.MainActivity
import com.thel.modules.main.home.RecommendUsersActivity
import com.thel.modules.main.home.moments.SendCardActivity
import com.thel.modules.main.home.moments.mention.MomentMentionedListActivity
import com.thel.modules.main.home.moments.web.WebViewActivity
import com.thel.modules.main.home.search.SearchActivity
import com.thel.modules.main.me.AwaitLiveActivity
import com.thel.modules.main.me.aboutMe.*
import com.thel.modules.main.me.match.MatchSuccessActivity
import com.thel.modules.main.me.match.WhoLikesMeActivity
import com.thel.modules.main.messages.ChatActivity
import com.thel.modules.main.messages.MyCircleRequestsActivity
import com.thel.modules.main.nearby.nearbypeople.NearbyPeopleActivity
import com.thel.modules.main.nearby.visit.VisitActivity
import com.thel.modules.main.userinfo.fan.UserInfoFollowersActivity
import com.thel.modules.others.UserInfoWinksActivity
import com.thel.modules.others.VipConfigActivity
import com.thel.modules.video.UserVideoListActivity
import com.thel.utils.FireBaseUtils
import com.thel.utils.GsonUtils
import com.thel.utils.L
import com.thel.utils.Utils
import me.rela.rf_s_bridge.new_router.NewUniversalRouter
import me.rela.rf_s_bridge.new_router.containers.NewBoostFlutterActivity
import org.json.JSONException
import org.json.JSONObject
import java.lang.Class as Class

class FlutterRouterConfig {

    companion object {

        private val tagName: String = "FlutterRouterConfig"


        internal fun getIntent(context: Context, zlass: Class<Any>): Intent {
            val intent = Intent(context, zlass)
            intent.putExtra("flutter_router_tag", "flutter_router_tag")
            return intent
        }

        fun getMainIntent(): Intent {

            val intent = Intent()

            intent.setClass(TheLApp.context, MainActivity::class.java)

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

            intent.putExtra("flutter_router_tag", "flutter_router_tag")

            return intent
        }

        fun getLiveIntent(any: Any): Intent {

            L.d(tagName, " any : $any")
            val intent: Intent

            val liveFollowerUsersBean = GsonUtils.getObject(any.toString(), LiveFollowerUsersBean::class.java)
//
            if (liveFollowerUsersBean.isLandscape == 0) {
                intent = Intent(TheLApp.context, LiveWatchActivity::class.java)
            } else {
                intent = Intent(TheLApp.context, LiveWatchHorizontalActivity::class.java)
            }
            intent.putExtra(TheLConstants.BUNDLE_KEY_ID, liveFollowerUsersBean.liveId)
            intent.putExtra(LiveWatchActivity.FROM_PAGE, LiveWatchActivity.FROM_PAGE_MOMENT)
            intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, liveFollowerUsersBean.userId)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra("flutter_router_tag", "flutter_router_tag")

            return intent
        }

        fun getAdvertIntent(any: Any): Intent {

            val json: String = any as String

            var jsonObject = JSONObject(json)

            val dumpURL = jsonObject.getString("dumpURL")

//            val advertTitle = jsonObject.getString("advertTitle")

            var intent = Intent()

            val bundle = Bundle()

            bundle.putString(BundleConstants.URL, dumpURL)

//            bundle.putString(BundleConstants.TITLE, advertTitle)

            intent.setClass(TheLApp.context, WebViewActivity::class.java)

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

            intent.putExtra("flutter_router_tag", "flutter_router_tag")


            return intent
        }

        fun getActivitiesIntent(jsonString: String): Intent {
            L.i("Activities", "jsonString: $jsonString")
            var url = ""
            try {
                val jsonObject = JSONObject(jsonString)
                if (jsonObject.has("dumpURL")) {
                    url = jsonObject.getString("dumpURL")
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            val intent = Intent(TheLApp.context, WebViewActivity::class.java)
            intent.putExtra(BundleConstants.URL, url)
            intent.putExtra("title", "")
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra("flutter_router_tag", "flutter_router_tag")


            return intent

        }

        fun getChatRequsetIntent(): Intent {

            val intent = Intent(TheLApp.context, MyCircleRequestsActivity::class.java)

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

            intent.putExtra("flutter_router_tag", "flutter_router_tag")


            return intent
        }

        fun getVipServiceIntent(): Intent {

            val intent = Intent(TheLApp.context, VipConfigActivity::class.java)

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

            intent.putExtra("flutter_router_tag", "flutter_router_tag")

            intent.putExtra("fromPage","userinfo")

            val pageId = Utils.getPageId()

            intent.putExtra("fromPageId", pageId)

            return intent
        }

        fun getVisitorIntent(): Intent {

            val intent = Intent(TheLApp.context, VisitActivity::class.java)

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

            intent.putExtra("flutter_router_tag", "flutter_router_tag")


            return intent
        }

        fun getRechargeIntent(): Intent {

            val intent = Intent(TheLApp.context, MySoftMoneyActivity::class.java)

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

            intent.putExtra("flutter_router_tag", "flutter_router_tag")


            return intent
        }

        //todo
        fun getMatchIntent(): Intent {

            val intent = Intent(TheLApp.context, MatchActivity::class.java)

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

            intent.putExtra("flutter_router_tag", "flutter_router_tag")

            return intent
        }

        //todo
        fun getWhoLikeMeIntent(): Intent {
            val intent = Intent(TheLApp.context, WhoLikesMeActivity::class.java)

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

            intent.putExtra("flutter_router_tag", "flutter_router_tag")


//            intent.putExtra("sumLikeCount", sumLikeCount)
//            intent.putExtra("notReplyMeCount", notReplyMeCount)
//            intent.putExtra("hasSeenCursor", hasSeenCursor)
//            val bundle3 = Bundle()
//            bundle3.putString(ShareFileUtils.MATCH_FROM_PAGE_ID, pageId)
//            bundle3.putString(ShareFileUtils.MATCH_FROM_PAGE, "push")
//            intent.putExtras(bundle3)
            return intent
        }

        //todo
        fun getPerfectMatchIntent(): Intent {

            val intent = Intent(TheLApp.context, MatchSuccessActivity::class.java)

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

            return intent
        }

        fun getNearbyIntent(): Intent {

            val intent = Intent(TheLApp.context, NearbyPeopleActivity::class.java)

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

            intent.putExtra("flutter_router_tag", "flutter_router_tag")


            return intent
        }

        fun getWorldMapIntent(): Intent {

            val intent = Intent(TheLApp.context, WorldActivity::class.java)

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

            intent.putExtra("flutter_router_tag", "flutter_router_tag")


            return intent
        }

        fun getWalkmanIntent(): Intent {

            val intent = Intent(TheLApp.context, WalkmanActivity::class.java)

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

            intent.putExtra("flutter_router_tag", "flutter_router_tag")

            return intent
        }

        fun getSearchIntent(): Intent {

            val intent = Intent(TheLApp.context, SearchActivity::class.java)

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

            intent.putExtra("flutter_router_tag", "flutter_router_tag")

            return intent
        }

        fun getChatIntent(jsonString: String): Intent {
            L.d("FlutterRouterConfig", "jsonString: $jsonString")
            val jsonObj = JSONObject(jsonString)
            val intent = Intent(TheLApp.context, ChatActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            if (jsonObj.has("userId")) {
                intent.putExtra("toUserId", jsonObj.getString("userId"))
            }
            if (jsonObj.has("userName")) {
                intent.putExtra("toName", jsonObj.getString("userName"))
            }
            if (jsonObj.has("userAvatar")) {
                intent.putExtra("toAvatar", jsonObj.getString("userAvatar"))
            }
            intent.putExtra("flutter_router_tag", "flutter_router_tag")

            FireBaseUtils.uploadGoogle(TheLConstants.FireBaseConstant.VIEW_ITEM_CHAT, TheLApp.context)

            return intent
        }

        fun getRecommendedIntent(jsonString: String): Intent {

            L.i("FlutterRouterConfig", "jsonString: $jsonString")

            val intent = Intent(TheLApp.context, SendCardActivity::class.java)
            intent.putExtra("flutterJsonString", jsonString)
            intent.putExtra(TheLConstants.BUNDLE_KEY_INTENT_FROM, SendCardActivity.FROM_FLUTTER)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra("flutter_router_tag", "flutter_router_tag")
            return intent
        }

        fun getUserFansListIntent(jsonString: String): Intent {
            val intent = Intent(TheLApp.context, UserInfoFollowersActivity::class.java)
            intent.putExtra("pageType", "fans")
            intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, JSONObject(jsonString).getString("userId"))
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra("flutter_router_tag", "flutter_router_tag")
            return intent
        }

        fun getOpenMemberIntent(): Intent {
            val intent = Intent(TheLApp.context, VipConfigActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra("flutter_router_tag", "flutter_router_tag")
            return intent
        }

        fun getRecUserListIntent(): Intent {
            val intent = Intent(TheLApp.context, RecommendUsersActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra("flutter_router_tag", "flutter_router_tag")
            return intent
        }

        fun getExpectListIntent(jsonString: String): Intent {
            L.d("FlutterRouterConfig", "jsonString: $jsonString")

            val intent = Intent(TheLApp.context, AwaitLiveActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, JSONObject(jsonString).getString("userId"))
            intent.putExtra("flutter_router_tag", "flutter_router_tag")
            return intent
        }

        fun getMomentAtUserIntent(any: Any): Intent {

            val jsonObject = JSONObject(any.toString())

            val momentsId = jsonObject.getString("momentsId")

            val intent = Intent(TheLApp.context, MomentMentionedListActivity::class.java)

            intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, momentsId)

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

            intent.putExtra("flutter_router_tag", "flutter_router_tag")

            return intent
        }

        fun getVideoIntent(jsonString: String): Intent {
            L.d("FlutterRouterConfig", " getVideoIntent jsonString : $jsonString")

            val momentsBean = Gson().fromJson(jsonString, MomentsBean::class.java)

            val videoBean = Utils.getVideoFromMoment(momentsBean)

            if (momentsBean != null) {
                videoBean.winkFlag = momentsBean.winkFlag
            }
            val userId = momentsBean.userId
            val bundle = Bundle()
            bundle.putSerializable(TheLConstants.BUNDLE_KEY_VIDEO_BEAN, videoBean)
            bundle.putString(TheLConstants.BUNDLE_KEY_USER_ID, userId.toString() + "")
            val intent = Intent(TheLApp.context, UserVideoListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_BEAN, momentsBean)
            intent.putExtra("flutter_router_tag", "flutter_router_tag")
            intent.putExtras(bundle)
            return intent
        }

        fun getUserWinkListIntent(jsonString: String): Intent {
            val intent = Intent(TheLApp.context, UserInfoWinksActivity::class.java)
            intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, JSONObject(jsonString).getString("userId"))
            intent.putExtra(TheLConstants.FROM_PAGE, "不确定")
            val pageId = Utils.getPageId()
            intent.putExtra(TheLConstants.FROM_PAGE_ID, pageId)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra("flutter_router_tag", "flutter_router_tag")
            return intent
        }

        fun getFlutterCommonIntent(): Intent {
            val intent = Intent(TheLApp.context, NewRelaBoostFlutterActivity::class.java)
            val pageId = Utils.getPageId()
            intent.putExtra(TheLConstants.FROM_PAGE_ID, pageId)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra("flutter_router_tag", "flutter_router_tag")
            return intent
        }

        /************************************************Flutter Page**********************************************/
        fun gotoThemeDetails(themeId: String) {
            val map = HashMap<String, String>()
            map["themeId"] = themeId
            NewUniversalRouter.sharedInstance.navigateTo(FlutterRouterPathConstants.ROUTER_FLUTTER_THEME_DETAILS, GsonUtils.createJsonString(map), false)
        }

//        fun gotoThemePartakeDetails(momentsId: String) {
//            var map = HashMap<String, String>()
//            map["momentsId"] = momentsId
//            NewUniversalRouter.sharedInstance.navigateTo(FlutterRouterPathConstants.ROUTER_FLUTTER_THEME_PARTAKE_DETAILS, GsonUtils.createJsonString(map), false)
//        }

        fun gotoReleaseRecommend(jsonModel: String) {
            NewUniversalRouter.sharedInstance.navigateTo(FlutterRouterPathConstants.ROUTER_FLUTTER_RELEASE_RECOMMEND, jsonModel, false)
        }

        fun gotoReleaseMoment() {
            NewUniversalRouter.sharedInstance.navigateTo(FlutterRouterPathConstants.ROUTER_FLUTTER_RELEASE_MOMENT, null, false)
        }

//        fun gotoReleaseTheme() {
//            NewUniversalRouter.sharedInstance.navigateTo(FlutterRouterPathConstants.ROUTER_FLUTTER_RELEASE_THEME, null, false)
//        }

//        fun gotoReleasePartakeTheme(partakeTopicId: String, partakeTopicTitle: String, partakeTopicMinText: String, imageUrl: String, videoUrl: String) {
//            var map = HashMap<String, String>()
//            map["partakeTopicId"] = partakeTopicId
//            map["partakeTopicTitle"] = partakeTopicTitle
//            map["partakeTopicMinText"] = partakeTopicMinText
//            map["imageUrl"] = imageUrl
//            map["videoUrl"] = videoUrl
//            NewUniversalRouter.sharedInstance.navigateTo(FlutterRouterPathConstants.ROUTER_FLUTTER_RELEASE_THEME, GsonUtils.createJsonString(map), false)
//        }

        fun gotoTagDetails(topicId: Int, topicName: String) {
            var map = HashMap<String, Any>()
            map["tagId"] = topicId
            map["tagName"] = topicName
            NewUniversalRouter.sharedInstance.navigateTo(FlutterRouterPathConstants.ROUTER_FLUTTER_TAG_DETAILS, GsonUtils.createJsonString(map), false)
        }

        fun gotoTagList(tagList: ArrayList<TagBean>) {
            var map = HashMap<String, ArrayList<TagBean>>()
            map["tagList"] = tagList
            NewUniversalRouter.sharedInstance.navigateTo(FlutterRouterPathConstants.ROUTER_FLUTTER_TAG_LIST, GsonUtils.createJsonString(map), false)
        }

        fun gotoMomentDetails(momentId: String) {
            var map = HashMap<String, String>()
            map["momentsId"] = momentId
            NewUniversalRouter.sharedInstance.navigateTo(FlutterRouterPathConstants.ROUTER_FLUTTER_MOMENT_DETAILS, GsonUtils.createJsonString(map), false)
        }

        fun gotoCommentReply(commentId: String) {
            var map = HashMap<String, String>()
            map["commentId"] = commentId
            NewUniversalRouter.sharedInstance.navigateTo(FlutterRouterPathConstants.ROUTER_FLUTTER_COMMENT_REPLY, GsonUtils.createJsonString(map), false)
        }

        fun gotoTimeMachine() {
            NewUniversalRouter.sharedInstance.navigateTo(FlutterRouterPathConstants.ROUTER_FLUTTER_TIME_MACHINE, null, false)
        }

        fun gotoUserInfo(userId: String) {
            val map = HashMap<String, String>()
            map["userId"] = userId
            val json = GsonUtils.createJsonString(map)
            NewUniversalRouter.sharedInstance.navigateTo(FlutterRouterPathConstants.ROUTER_FLUTTER_USERINFO, json, false)
        }

        fun gotoLoginStart() {
            NewUniversalRouter.sharedInstance.navigateTo(FlutterRouterPathConstants.ROUTER_FLUTTER_LOGIN_START, null, false)
        }

        fun gotoWhoLikeMe() {
            NewUniversalRouter.sharedInstance.navigateTo(FlutterRouterPathConstants.ROUTER_FLUTTER_LOGIN_START, null, false)
        }

        fun gotoMomentMsg() {
            NewUniversalRouter.sharedInstance.navigateTo(FlutterRouterPathConstants.ROUTER_FLUTTER_MOMENT_MSG, null, false)

        }

        fun nativeShowRecommendDialog(jsonString: String) {
            NewUniversalRouter.sharedInstance.displayFlutterViewByRoute(FlutterRouterPathConstants.ROUTER_FLUTTER_TRANSPARENT_PAGE, jsonString)

        }

    }

}