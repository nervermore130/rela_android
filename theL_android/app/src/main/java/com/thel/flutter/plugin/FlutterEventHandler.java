package com.thel.flutter.plugin;

import android.util.Log;

import com.thel.app.TheLApp;
import com.thel.utils.DeviceUtils;
import com.thel.utils.PhoneUtils;
import com.thel.utils.ShareFileUtils;

import java.util.UUID;

import io.flutter.plugin.common.EventChannel;
import io.flutter.view.FlutterView;

/**
 * @author liuyun
 */
public class FlutterEventHandler implements EventChannel.StreamHandler {

    public static final String CHANNEL_NAME = "com.thel.login.plugin/event";

    String data;

    EventChannel.EventSink eventSink;

    public FlutterEventHandler(String data) {

        this.data = data;

    }

    @Override public void onListen(Object o, EventChannel.EventSink eventSink) {

        this.eventSink = eventSink;

        if (data != null) {
            eventSink.success(data);
        }
    }

    @Override public void onCancel(Object o) {

        Log.d("FlutterEventHandler", "---------onCancel--------");

    }

    public static void registerWith(FlutterView flutterView, String channelName, String data) {

        EventChannel eventChannel = new EventChannel(flutterView, channelName);

        FlutterEventHandler flutterEventHandler = new FlutterEventHandler(data);

        eventChannel.setStreamHandler(flutterEventHandler);

    }


}
