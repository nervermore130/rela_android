package com.thel.flutter;

public class FlutterRouterPathConstants {

    /**
     * 首页
     */
    public static final String ROUTER_NATIVE_MAIN = "/native/main";

    /**
     * 直播
     */
    public static final String ROUTER_NATIVE_LIVE = "/native/live";

    /**
     * 点击banner进入广告页（WebView）
     */
    public static final String ROUTER_NATIVE_ADVERT = "/native/advert";

    /**
     * 密友和伴侣请求
     */
    public static final String ROUTER_NATIVE_CHATREQUSET = "/native/chatRequset";

    /**
     * 会员服务
     */
    public static final String ROUTER_NATIVE_VIPSERVICE = "/native/vipService";

    /**
     * 谁看过我
     */
    public static final String ROUTER_NATIVE_VISITOR = "/native/visitor";

    /**
     * 充值
     */
    public static final String ROUTER_NATIVE_RECHARGE = "/native/recharge";

    /**
     * 匹配
     */
    public static final String ROUTER_NATIVE_MATCH = "/native/match";

    /**
     * 谁喜欢我
     */
    public static final String ROUTER_NATIVE_WHOLIKESME = "/native/whoLikesMe";

    /**
     * 完美匹配
     */
    public static final String ROUTER_NATIVE_PERFECTMATCH = "/native/perfectMatch";

    /**
     * 附近
     */
    public static final String ROUTER_NATIVE_NEARBY = "/native/nearBy";

    /**
     * 漫游世界
     */
    public static final String ROUTER_NATIVE_WORLDMAP = "/native/worldMap";

    /**
     * Rela随身听
     */
    public static final String ROUTER_NATIVE_WALKMAN = "/native/walkman";

    /**
     * 搜索
     */
    public static final String ROUTER_NATIVE_SEARCH = "/native/search";

    /**
     * 聊天
     */
    public static final String ROUTER_NATIVE_CHAT = "/native/chat";

    /**
     * 推荐用户
     */
    public static final String ROUTER_NATIVE_RECOMMENDED = "/native/recommended";

    /**
     * 用户粉丝列表
     */
    public static final String ROUTER_NATIVE_USERFANSLIST = "/native/userFansList";

    /**
     * 会员购买
     */
    public static final String ROUTER_NATIVE_OPENMEMBER = "/native/openMember";

    /**
     * 推荐用户列表页
     */
    public static final String ROUTER_NATIVE_RECUSERLIST = "/native/recUserList";

    /**
     * 期待直播列表
     */
    public static final String ROUTER_NATIVE_EXPECTLIST = "/native/expectList";

    /**
     * 日志at的人列表
     */
    public static final String ROUTER_NATIVE_MOMENTATUSER = "/native/momentAtUser";

    /**
     * 视频
     */
    public static final String ROUTER_NATIVE_VIDEO = "/native/video";

    /**
     * 人气列表
     */
    public static final String ROUTER_NATIVE_WINK_LIST = "/native/userWinkList";

    /********************************************Flutter Router Url*******************************************/

    /**
     * 话题详情
     */
    public static final String ROUTER_FLUTTER_THEME_DETAILS = "/topic/detials";

    /**
     * 话题描述详情
     */
    public static final String ROUTER_FLUTTER_THEME_PARTAKE_DETAILS = "/topic/describe/detials";

    /**
     * 发布推荐日志
     */
    public static final String ROUTER_FLUTTER_RELEASE_RECOMMEND = "/moment/releaseRecommended";

    /**
     * 发布日志
     */
    public static final String ROUTER_FLUTTER_RELEASE_MOMENT = "/moment/releaseMoment";

    /**
     * 发布话题/参与话题
     */
    public static final String ROUTER_FLUTTER_RELEASE_THEME = "/moment/releaseTopic";

    /**
     * 标签详情
     */
    public static final String ROUTER_FLUTTER_TAG_DETAILS = "/tag/tagDetail";

    /**
     * 标签搜索页
     */
    public static final String ROUTER_FLUTTER_TAG_LIST = "/tag/tagList";

    /**
     * 日志详情
     */
    public static final String ROUTER_FLUTTER_MOMENT_DETAILS = "/index/momentDetail";

    /**
     * 评论详情
     */
    public static final String ROUTER_FLUTTER_COMMENT_REPLY = "/index/commentReply";

    /**
     * 时光机
     */
    public static final String ROUTER_FLUTTER_TIME_MACHINE = "/index/timeMachine";

    /**
     * 个人主页
     */
    public static final String ROUTER_FLUTTER_USERINFO = "/me/userInfo";

    /**
     * 登录起始页
     */
    public static final String ROUTER_FLUTTER_LOGIN_START = "/login/start";

    public static final  String ROUTER_FLUTTER_MOMENT_MSG= "/me/momentsMsg";

    /**
     * 原生弹flutter dialog
     */
    public static final  String ROUTER_FLUTTER_TRANSPARENT_PAGE= "/tools/transParentPage";


    public static final  String ROUTER_FLUTTER_CONTAINER= "/native/flutterContainer";
}
