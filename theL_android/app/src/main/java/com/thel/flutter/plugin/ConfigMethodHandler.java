package com.thel.flutter.plugin;

import androidx.annotation.NonNull;

import com.networkbench.agent.impl.NBSAppAgent;
import com.thel.BuildConfig;
import com.thel.app.TheLApp;
import com.thel.constants.TheLConstants;
import com.thel.network.RequestConstants;
import com.thel.utils.L;
import com.thel.utils.MD5Utils;
import com.umeng.socialize.PlatformConfig;

import java.util.Map;

import cn.udesk.UdeskSDKManager;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.view.FlutterView;

public class ConfigMethodHandler implements MethodChannel.MethodCallHandler {

    private static final String TAG = "ConfigFlutter";

    private static final String MC_CONFIG = "dart.rela.me/configuration";

    private static ConfigMethodHandler configMethodHandler = new ConfigMethodHandler();

    private ConfigMethodHandler() {

    }

    public static ConfigMethodHandler getInstance() {
        return configMethodHandler;
    }

    @Override public void onMethodCall(MethodCall methodCall, MethodChannel.Result result) {

        L.d(TAG, " methodCall.method : " + methodCall.method);

        L.d(TAG, " methodCall.arguments : " + methodCall.arguments);

        if (methodCall.method.equals("configurationInformation")) {

            try {

                Map<String, String> ketMap = (Map<String, String>) methodCall.arguments;

                L.d(TAG, " ketMap : " + ketMap);

                MD5Utils.SALT = ketMap.get("REQUEST_SALT");

                TheLConstants.WX_APP_ID  = ketMap.get("WX_APP_ID");

                TheLConstants.WX_SECRET  = ketMap.get("WX_SECRET");

                TheLConstants.WX_APP_ID_GLOBAL  = ketMap.get("WX_APP_ID_GLOBAL");

                TheLConstants.WX_SECRET_GLOBAL  = ketMap.get("WX_SECRET_GLOBAL");

                TheLConstants.WX_APP_ID_PAY  = ketMap.get("WX_APP_ID_PAY");

                TheLConstants.WX_API_KEY  = ketMap.get("WX_API_KEY");

                TheLConstants.UDESK_APP_ID  = ketMap.get("UDESK_APP_ID");

                TheLConstants.UDESK_APP_KEY  = ketMap.get("UDESK_APP_KEY");

                TheLConstants.QQ_APP_ID  = ketMap.get("QQ_APP_ID");

                TheLConstants.QQ_APP_KEY  = ketMap.get("QQ_APP_KEY");

                TheLConstants.TINGYUN_KEY  = ketMap.get("TINGYUN_KEY");

                TheLConstants.TINGYUN_KEY_GLOBAL  = ketMap.get("TINGYUN_KEY_GLOBAL");

                TheLConstants.SINA_APP_KEY  = ketMap.get("SINA_APP_KEY");

                TheLConstants.SINA_APP_SECRET  = ketMap.get("SINA_APP_SECRET");

                TheLConstants.AGORA_APP_ID  = ketMap.get("AGORA_APP_ID");

                TheLConstants.KS_KMC_TOKEN  = ketMap.get("KS_KMC_TOKEN");

                TheLConstants.WX_MINIPROGRAM_RELA_WALKMAN_ID = ketMap.get("WX_MINIPROGRAM_RELA_WALKMAN_ID");

                initShareConfig();

                initUDesk();

                initNBS();
            }catch (Exception e){
                e.printStackTrace();
            }


        }

    }

    public void registerWith(@NonNull FlutterView flutterView) {

        L.d(TAG, " registerWith : ");

        MethodChannel methodChannel = new MethodChannel(flutterView, MC_CONFIG);

        methodChannel.setMethodCallHandler(this);

    }

    private void initShareConfig(){
        if (RequestConstants.APPLICATION_ID_LOCAL.equals(BuildConfig.APPLICATION_ID)) {
            PlatformConfig.setWeixin(TheLConstants.WX_APP_ID, TheLConstants.WX_SECRET);
        } else {
            PlatformConfig.setWeixin(TheLConstants.WX_APP_ID_GLOBAL, TheLConstants.WX_SECRET_GLOBAL);
        }
        PlatformConfig.setQQZone(TheLConstants.QQ_APP_ID, TheLConstants.QQ_APP_KEY);
        PlatformConfig.setSinaWeibo(TheLConstants.SINA_APP_KEY, TheLConstants.SINA_APP_SECRET, "http://sns.whalecloud.com");
    }

    private void initNBS(){
        if (RequestConstants.APPLICATION_ID_LOCAL.equals(BuildConfig.APPLICATION_ID))
            NBSAppAgent.setLicenseKey(TheLConstants.TINGYUN_KEY).withLocationServiceEnabled(true).start(TheLApp.context);
        else
            NBSAppAgent.setLicenseKey(TheLConstants.TINGYUN_KEY_GLOBAL).withLocationServiceEnabled(true).start(TheLApp.context);
    }

    private void initUDesk(){

        //udesk sdk
        UdeskSDKManager.getInstance().initApiKey(TheLApp.context, TheLConstants.UDESK_DOMAIN, TheLConstants.UDESK_APP_KEY, TheLConstants.UDESK_APP_ID);
    }

}
