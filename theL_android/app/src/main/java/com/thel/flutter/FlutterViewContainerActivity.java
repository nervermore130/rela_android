package com.thel.flutter;

import android.content.Intent;
import android.os.Bundle;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.thel.flutter.bridge.RfSBridgeHandlerFactory;
import com.thel.modules.login.login_register.FacebookLogin;
import com.thel.ui.imageviewer.cropiwa.image.CropIwaResultReceiver;
import com.thel.utils.L;

import java.util.HashMap;
import java.util.Map;

import androidx.fragment.app.Fragment;
import io.flutter.plugin.common.PluginRegistry;
import io.flutter.plugins.GeneratedPluginRegistrant;

public class FlutterViewContainerActivity extends RelaBoostFlutterActivity {

    private CropIwaResultReceiver cropResultReceiver;

    private CallbackManager callbackManager = CallbackManager.Factory.create();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showStatusBar();

//        RfSBridgeHandlerFactory.getInstance().setActivity(this);

        cropResultReceiver = new CropIwaResultReceiver();
        cropResultReceiver.register(this);
        cropResultReceiver.setListener(RfSBridgeHandlerFactory.getInstance().getListener());

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        L.d("FacebookLogin", " ------onActivityResult------ ");

        if (callbackManager != null) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        if (cropResultReceiver != null) {
            cropResultReceiver.unregister(this);
        }
        super.onDestroy();
    }

    @Override
    public String getContainerName() {
        return FlutterViewContainerActivity.class.getName();
    }

    @Override
    public Map getContainerParams() {
        return new HashMap();
    }

    @Override public Fragment setNativeFragment() {
        return null;
    }

    @Override
    public void onRegisterPlugins(PluginRegistry registry) {
        GeneratedPluginRegistrant.registerWith(registry);
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }


    public void facebookLogin(){

        AccessToken accessToken = AccessToken.getCurrentAccessToken();

        if (accessToken != null) {

            boolean isLoggedIn = !accessToken.isExpired();

            if (isLoggedIn) {
                FacebookLogin.getInstance().signIn(FlutterViewContainerActivity.this, accessToken, null);
            }

        } else {

            FacebookLogin.getInstance().login(FlutterViewContainerActivity.this, callbackManager, null);

        }
    }
}
