package com.thel.flutter;

import java.util.HashMap;
import java.util.Map;

import io.flutter.plugin.common.PluginRegistry;
import io.flutter.plugins.GeneratedPluginRegistrant;
import me.rela.rf_s_bridge.router.containers.BoostFlutterFragment;

public class RelaFlutterFragment extends BoostFlutterFragment {

    private static RelaFlutterFragment instance = new RelaFlutterFragment();

    public static RelaFlutterFragment getInstance(){

        return instance;
    }

    @Override public void destroyContainer() {

    }

    @Override public String getContainerName() {
        return RelaFlutterFragment.class.getName();
    }

    @Override public Map getContainerParams() {
        return new HashMap<>();
    }

    @Override public void onRegisterPlugins(PluginRegistry registry) {
        GeneratedPluginRegistrant.registerWith(registry);
    }
}
