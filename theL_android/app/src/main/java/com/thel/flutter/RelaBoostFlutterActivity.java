package com.thel.flutter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.thel.R;
import com.thel.base.BaseFlutterActivity;
import com.thel.flutter.bridge.RfSBridgeHandlerFactory;
import com.thel.utils.L;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import androidx.fragment.app.Fragment;

import io.flutter.facade.Flutter;
import io.flutter.view.FlutterNativeView;
import io.flutter.view.FlutterView;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import me.rela.rf_s_bridge.router.BoostFlutterView;
import me.rela.rf_s_bridge.router.FlutterBoostPlugin;
import me.rela.rf_s_bridge.router.RelaFlutterInterface;
import me.rela.rf_s_bridge.router.UniversalRouter;
import me.rela.rf_s_bridge.router.containers.FlutterViewStub;
import me.rela.rf_s_bridge.router.interfaces.IFlutterViewContainer;

abstract public class RelaBoostFlutterActivity extends BaseFlutterActivity implements IFlutterViewContainer, RelaFlutterInterface {

    private static final String TAG = "RelaBoostFlutterActivity";

    private FlutterContent mFlutterContent;

    private FrameLayout flutterContainer;

    private FrameLayout nativeContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_rela_boost_flutter);

        Fragment fragment = setNativeFragment();

        if (fragment != null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.native_container_fl, fragment, fragment.getClass().getSimpleName() + "_flutter_" + System.currentTimeMillis()).commit();
        }

        nativeContainer = findViewById(R.id.native_container_fl);

        mFlutterContent = new FlutterContent(this);

        flutterContainer = findViewById(R.id.flutter_container_fl);

        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);

        flutterContainer.addView(mFlutterContent, layoutParams);

        //TODO 后面两行代码需要删除
//        flutterContainer.setVisibility(View.GONE);
//
//        nativeContainer.setVisibility(View.VISIBLE);

        FlutterBoostPlugin.containerManager().onContainerCreate(this);

        onRegisterPlugins(this);

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        FlutterBoostPlugin.containerManager().onContainerAppear(RelaBoostFlutterActivity.this);
        mFlutterContent.attachFlutterView(getBoostFlutterView());
    }

    @Override
    protected void onPause() {
        mFlutterContent.detachFlutterView();
        FlutterBoostPlugin.containerManager().onContainerDisappear(this);
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        FlutterBoostPlugin.containerManager().onContainerDestroy(this);
        mFlutterContent.destroy();
        super.onDestroy();
    }

    @Override
    public FlutterView createFlutterView(Context context) {
        return FlutterBoostPlugin.viewProvider().createFlutterView(this);
    }

    @Override
    public FlutterNativeView createFlutterNativeView() {

        return FlutterBoostPlugin.viewProvider().createFlutterNativeView(this);
    }

    @Override
    public boolean retainFlutterNativeView() {
        return true;
    }

    protected View createSplashScreenView() {
        FrameLayout frameLayout = new FrameLayout(this);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.CENTER;
        frameLayout.addView(new ProgressBar(this), params);
        return frameLayout;
    }

    protected View createFlutterInitCoverView() {
        View initCover = new View(this);
        initCover.setBackgroundColor(Color.WHITE);
        return initCover;
    }

    @Override
    public void onContainerShown() {
        mFlutterContent.onContainerShown();
    }

    @Override
    public void onContainerHidden() {
        mFlutterContent.onContainerHidden();
    }

    @Override
    public void onBackPressed() {
        FlutterBoostPlugin.containerManager().onBackPressed(this);
    }

    @Override
    public Activity getActivity() {
        return this;
    }

    @Override
    public BoostFlutterView getBoostFlutterView() {
        return (BoostFlutterView) getFlutterView();
    }

    @Override
    public void destroyContainer() {
        finish();
    }

    @Override
    public abstract String getContainerName();

    @Override
    public abstract Map getContainerParams();


    @Override
    public void setBoostResult(HashMap result) {
        FlutterBoostPlugin.setBoostResult(this, result);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        FlutterBoostPlugin.onBoostResult(this, requestCode, resultCode, data);
    }

    class FlutterContent extends FlutterViewStub {

        public FlutterContent(Context context) {
            super(context);
        }

        @Override
        public View createFlutterInitCoverView() {
            return RelaBoostFlutterActivity.this.createFlutterInitCoverView();
        }

        @Override
        public BoostFlutterView getBoostFlutterView() {
            return RelaBoostFlutterActivity.this.getBoostFlutterView();
        }

        @Override
        public View createSplashScreenView() {
            return RelaBoostFlutterActivity.this.createSplashScreenView();
        }
    }

    public void showFlutterView() {

        L.d("FlutterPlatform", " showFlutterView : ");

        if (nativeContainer != null) {
            nativeContainer.setVisibility(View.GONE);
        }

    }

    @SuppressLint("CheckResult")
    public void hideFlutterView() {

        L.d("FlutterPlatform", " hideFlutterView : ");
        Observable.timer(200, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(
                aLong -> UniversalRouter.sharedInstance.flutterPopToRoot(false)
        );
        if (nativeContainer != null) {
            nativeContainer.setVisibility(View.VISIBLE);
        }
    }

    public abstract Fragment setNativeFragment();

    public View getFlutterViewContainer() {
        return flutterContainer;
    }


}
