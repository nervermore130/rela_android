package com.thel.flutter

import com.thel.bean.BasicInfoBean
import com.thel.bean.BlackListBean

interface IFlutterPush {

    fun pushUserInfo()

    fun pushNotRead()

    fun pushMyUserId()

    fun pushBlackListInfo(blackListBean: BlackListBean)

    fun pushLocationState()

    fun pushInitParams(basicInfoNetBean: BasicInfoBean)

    fun pushLanguage(language: String)

    fun pushUpdateEnvironment(map: Map<String, Any>)

    fun pushVipConfig(map: Map<String, Any>)
}

