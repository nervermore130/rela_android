package com.thel.flutter.deletage.impl;

public interface IDelegateFactory {

    IDelegate getDelegate();

}
