package com.thel.flutter

import android.os.Bundle
import android.text.TextUtils
import com.google.gson.reflect.TypeToken
import com.thel.R
import com.thel.app.TheLApp
import com.thel.base.BaseActivity
import com.thel.flutter.bean.RouteBean
import com.thel.utils.GsonUtils
import com.thel.utils.ScreenUtils
import io.flutter.embedding.android.FlutterView
import kotlinx.android.synthetic.main.activity_flutter_common.*
import me.rela.rf_s_bridge.new_router.NewUniversalRouter
import me.rela.rf_s_bridge.new_router.containers.NewFlutterFragment

class FlutterCommonActivity : BaseActivity() {

    private val tagName: String = "FlutterCommonActivity"

    override fun isStatusBarTranslucent(): Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_flutter_common)

//        val layoutParams = fragment_container_fl.layoutParams
//
//        layoutParams.width = ScreenUtils.getScreenWidth(TheLApp.context)
//
//        layoutParams.height = ScreenUtils.getScreenHeight(TheLApp.context)
//
//        fragment_container_fl.layoutParams = layoutParams

        val newFlutterFragment = NewFlutterFragment.NewEngineFragmentBuilder().renderMode(FlutterView.RenderMode.texture).build<NewFlutterFragment>()

        supportFragmentManager.beginTransaction().add(R.id.fragment_container_fl, newFlutterFragment, FlutterCommonActivity::javaClass.name).commit()
    }

    override fun onBackPressed() {
        NewUniversalRouter.sharedInstance.getFlutterRouteStack { route ->
            if (!TextUtils.isEmpty(route)) {
                val routeBeans = GsonUtils.getGson().fromJson<List<RouteBean>>(route, object : TypeToken<List<RouteBean>>() {

                }.type)
                if (routeBeans.size > 1) {
                    val routerBean: RouteBean = routeBeans[routeBeans.size - 2]

                    if (routerBean.isNative) {
                        NewUniversalRouter.sharedInstance.nativePop()
                    } else {
                        NewUniversalRouter.sharedInstance.nativeRequestFlutterPop()
                    }
                } else {
                    if (NewUniversalRouter.sharedInstance.mOnRouteBackListener != null) {
                        NewUniversalRouter.sharedInstance.mOnRouteBackListener.onRouteBack()
                    }
                }
            }
        }
    }

}