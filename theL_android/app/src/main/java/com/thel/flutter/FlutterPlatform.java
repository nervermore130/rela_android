package com.thel.flutter;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;

import com.thel.app.TheLApp;
import com.thel.constants.TheLConstants;
import com.thel.modules.main.MainActivity;
import com.thel.utils.L;

import androidx.annotation.NonNull;
import me.rela.rf_s_bridge.router.UniversalRouter;
import me.rela.rf_s_bridge.router.interfaces.IPlatform;

public class FlutterPlatform implements IPlatform {

    private static final String TAG = "FlutterPlatform";

    private Application application;

    private OnTabCheckListener mOnTabCheckListener;

    public FlutterPlatform(Application application) {
        this.application = application;

        UniversalRouter.sharedInstance.setActivityClass(FlutterViewContainerActivity.class);

    }

    public void setOnTabCheckListener(OnTabCheckListener onTabCheckListener){
        this.mOnTabCheckListener = onTabCheckListener;
    }

    @Override
    public Application getApplication() {
        return application;
    }

    @Override
    public Activity getMainActivity() {
        if (MainActivity.sRef != null) {
            return MainActivity.sRef.get();
        }

        L.d(TAG, " --------getMainActivity---------");

        return null;
    }

    @Override
    public boolean isDebug() {

        L.d(TAG, " --------isDebug---------");

        return false;
    }

    /**
     * 如果flutter想打开一个本地页面，将会回调这个方法，页面参数将会拼接在url中
     * <p>
     * 例如：sample://nativePage?aaa=bbb
     * <p>
     * 参数就是类似 aaa=bbb 这样的键值对
     *
     * @param context
     * @return
     */
    @Override
    public boolean startActivity(Context context, Intent intent) {

        L.d("Router", " startActivity intent : " + intent.toString());

        context.startActivity(intent);

        return true;
    }

    @Override
    public void finishActivity(Activity activity) {

        L.d("Router", " finishActivity : " + activity.getClass().getSimpleName());

//        UniversalRouter.sharedInstance.nativePop();

    }

    @Override
    public void containerSwitchToFlutter() {

        L.d(TAG, " --------containerSwitchToFlutter---------");

        TheLApp.context.sendBroadcast(new Intent(TheLConstants.BROADCAST_GOTO_FLUTTER));

    }

    @Override
    public void containerSwitchToNative(@NonNull String uri) {
        L.d(TAG, " containerSwitchToNative : " + uri);

        if(mOnTabCheckListener!=null){
            mOnTabCheckListener.onTabCheck(uri);
        }
    }

    public interface OnTabCheckListener{

        void onTabCheck(String tab);

    }
}
