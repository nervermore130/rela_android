package com.thel.flutter.model;

/**
 * Created by chad
 * Time 2019-05-29
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */
public class TopicTextFieldContent {

    public String text;
    public String imageUrl;
    public String videoUrl;
    public double pixelWidth;
    public double pixelHeight;
    public int playTime;
    public ContentType contentType;

    public enum ContentType {
        text,
        image,
        video,
    }
}
