package com.thel.flutter.plugin;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import com.facebook.AccessToken;
import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UploadManager;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.user.UploadTokenBean;
import com.thel.constants.BundleConstants;
import com.thel.constants.TheLConstants;
import com.thel.modules.login.email.EmailLoginActivity;
import com.thel.modules.login.login_register.FacebookLogin;
import com.thel.modules.main.MainActivity;
import com.thel.modules.main.home.moments.web.WebViewActivity;
import com.thel.modules.select_image.SelectLocalImagesActivity;
import com.thel.modules.welcome.FlutterLoginActivity;
import com.thel.network.LoginInterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.RequestConstants;
import com.thel.network.api.loginapi.bean.SignInBean;
import com.thel.ui.LoadingDialogImpl;
import com.thel.ui.imageviewer.cropiwa.image.CropIwaResultReceiver;
import com.thel.utils.GsonUtils;
import com.thel.utils.ImageUtils;
import com.thel.utils.L;
import com.thel.utils.MD5Utils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.ToastUtils;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONObject;

import java.io.File;
import java.util.List;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.thel.utils.DeviceUtils.getFilePathFromUri;

public class LoginResultDelegate {

    private static final String TAG = "LoginResultDelegate";

    private Activity mActivity;

    private MethodChannel.Result result;

    private LoadingDialogImpl loadingDialog;

    public LoginResultDelegate(Activity mActivity) {

        this.mActivity = mActivity;

        loadingDialog = LoadingDialogImpl.LoadingDialogInstance.getInstance(mActivity);

    }

    public void handleResult(MethodCall methodCall, MethodChannel.Result result) {


        if (mActivity == null || mActivity.isDestroyed()) {
            return;
        }

        this.result = result;

        Intent intent;

        Bundle bundle;

        Log.d(TAG, " onMethodCall : " + methodCall.method);

        switch (methodCall.method) {
            case "com.thel.login.plugin.event.facebook":

                AccessToken accessToken = AccessToken.getCurrentAccessToken();

                if (accessToken != null) {

                    L.d(TAG, " result : " + result);

                    L.d(TAG, " accessToken getToken : " + accessToken.getToken());

                    boolean isLoggedIn = !accessToken.isExpired();

                    if (isLoggedIn) {
                        FacebookLogin.getInstance().signIn(mActivity, accessToken, null);
                    }

                } else {

                    if (mActivity != null && mActivity instanceof FlutterLoginActivity) {

                        FlutterLoginActivity flutterLoginActivity = (FlutterLoginActivity) mActivity;

                        FacebookLogin.getInstance().login(mActivity, flutterLoginActivity.getCallbackManager(), null);
                    }

                }

                break;
            case "com.thel.login.plugin.event.wechat":

                L.d(TAG, " 微信登录 : ");

//                WeChatLogin.getInstance().login(mActivity, result);

                break;

            case "com.thel.login.plugin/event.email":

                goToEmailActivity();

                break;

            case "com.thel.login.plugin/event.photo":

                gotoTakePhotoActivity();

                break;

            case "com.thel.login.plugin/event.success":

                L.d(TAG, " mActivity : " + mActivity);

                if (mActivity != null) {

                    L.d(TAG, " methodCall.arguments : " + methodCall.arguments.toString());

                    String signInJson = (String) methodCall.arguments;

                    SignInBean signInBean = GsonUtils.getObject(signInJson, SignInBean.class);

                    ShareFileUtils.saveUserData(signInBean);

                    ShareFileUtils.setBoolean(ShareFileUtils.HAS_LOGGED, true);

                    if (!signInBean.data.user.isNewUser()) {

                        MobclickAgent.onEvent(mActivity, "register_succeed");

                        ShareFileUtils.setBoolean(ShareFileUtils.IS_NEW_USER, false);

                        MobclickAgent.onEvent(mActivity, "enter_main_page");

                        intent = new Intent(mActivity, MainActivity.class);
                        intent.putExtra("isNew", false);
                        intent.putExtra(TheLConstants.BUNDLE_KEY_WHERE_TO_GO, "ONE");
                        mActivity.startActivity(intent);
                        mActivity.finish();
                    } else {
                        MobclickAgent.onEvent(mActivity, "register_or_login_succeed");
                        ShareFileUtils.setBoolean(ShareFileUtils.IS_NEW_USER, true);
                    }

                }
                break;
            case "com.thel.init.user.plugin/event.success":

                try {

                    if (methodCall.arguments != null) {

                        String phoneData = methodCall.arguments.toString();

                        JSONObject jsonObject = new JSONObject(phoneData);

                        String cell = jsonObject.getString("cell");

                        ShareFileUtils.setString(ShareFileUtils.BIND_CELL, cell);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                intent = new Intent(mActivity, MainActivity.class);
                intent.putExtra("isNew", true);
                intent.putExtra(TheLConstants.BUNDLE_KEY_WHERE_TO_GO, "ONE");
                mActivity.startActivity(intent);
                mActivity.finish();

                break;
            case "com.thel.login.plugin/method.user.agreement":

                Log.d(TAG, " onMethodCall 用户协议 ");

                intent = new Intent(mActivity, WebViewActivity.class);
                bundle = new Bundle();
                bundle.putString(BundleConstants.URL, TheLConstants.USER_AGREEMENT_PAGE_URL);
                bundle.putBoolean(BundleConstants.NEED_SECURITY_CHECK, false);
                intent.putExtras(bundle);
                mActivity.startActivity(intent);

                break;
            case "com.thel.login.plugin/method.failure.receive.code":

                Log.d(TAG, " onMethodCall 收不到验证码 ");

                intent = new Intent(mActivity, WebViewActivity.class);
                bundle = new Bundle();
                bundle.putString(BundleConstants.URL, TheLConstants.LOGIN_HELP_RUL);
                intent.putExtras(bundle);
                mActivity.startActivity(intent);
                break;
            case "com.thel.login.plugin/method.init.user.info":

                ShareFileUtils.setBoolean(ShareFileUtils.IS_NEW_USER, true);

                break;

            default:
                break;
        }


    }


    private void goToEmailActivity() {
        if (mActivity != null) {
            Intent intent = new Intent(mActivity, EmailLoginActivity.class);
            mActivity.startActivityForResult(intent, TheLConstants.REQUEST_CODE_EMAIL_LOGIN);
        }
    }

    private void gotoTakePhotoActivity() {
        if (mActivity != null) {
            Intent intent = new Intent(mActivity, SelectLocalImagesActivity.class);
            intent.putExtra("isAvatar", true);
            mActivity.startActivityForResult(intent, TheLConstants.BUNDLE_CODE_UPDATE_USER_INFO_ACTIVITY);
        }
    }


    public CropIwaResultReceiver.Listener getListener() {
        return listener;
    }

    CropIwaResultReceiver.Listener listener = new CropIwaResultReceiver.Listener() {
        @Override
        public void onCropSuccess(Uri croppedUri) {

            if (result != null && croppedUri != null) {
                String filePath = getFilePathFromUri(mActivity, croppedUri);
                if (filePath != null) {
                    uploadPhoto(filePath, result);
                }
            }

        }

        @Override
        public void onCropFailed(Throwable e) {
            ToastUtils.showToastShort(mActivity, "crop failed");
        }
    };


    private void uploadPhoto(String avatarPhotoPath, MethodChannel.Result result) {

        try {

            if (loadingDialog != null) {
                loadingDialog.showLoading();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        String uploadAvatarPath = RequestConstants.UPLOAD_FILE_ROOT_PATH_AVATAR + ShareFileUtils.getString(ShareFileUtils.ID, "") + "/" + MD5Utils.calculateMD5(new File(avatarPhotoPath)) + ".jpg";
        RequestBusiness.getInstance().getUploadToken(System.currentTimeMillis() + "", "", uploadAvatarPath).onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new MyConsumer(uploadAvatarPath, avatarPhotoPath, result));
    }

    public class MyConsumer extends LoginInterceptorSubscribe<UploadTokenBean> {

        String picUploadPath;

        String picLocalPath;

        MethodChannel.Result result;

        public MyConsumer(String picUploadPath, String picLocalPath, MethodChannel.Result result) {
            this.picUploadPath = picUploadPath;
            this.picLocalPath = picLocalPath;
            this.result = result;
        }

        @Override
        public void onError(Throwable t) {
            super.onError(t);
        }

        @Override
        public void onNext(UploadTokenBean uploadTokenBean) {
            super.onNext(uploadTokenBean);
            if (!TextUtils.isEmpty(uploadTokenBean.data.uploadToken)) {
                UploadManager uploadManager = new UploadManager();
                // 上传头像图片
                if (!TextUtils.isEmpty(picUploadPath) && !TextUtils.isEmpty(picLocalPath)) {
                    uploadManager.put(new File(picLocalPath), picUploadPath, uploadTokenBean.data.uploadToken, new UpCompletionHandler() {
                        @Override
                        public void complete(String key, ResponseInfo info, JSONObject response) {

                            try {

                                if (info != null && info.statusCode == 200 && picUploadPath.equals(key)) {
                                    List<Integer> wh = ImageUtils.measurePicWidthAndHeight(picLocalPath);
                                    if (wh.size() == 2) {
                                        String avatarUrl = RequestConstants.FILE_BUCKET + key;
                                        result.success(avatarUrl);
                                    }
                                } else {
                                    if (mActivity != null) {
                                        mActivity.runOnUiThread(new Runnable() {
                                            @Override public void run() {
                                                ToastUtils.showToastShort(TheLApp.context, TheLApp.context.getString(R.string.udesk_upload_img_error));
                                            }
                                        });
                                    }
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            if (loadingDialog != null) {
                                loadingDialog.closeDialog();
                            }
                        }
                    }, null);
                } else {
                    ToastUtils.showToastShort(TheLApp.context, TheLApp.context.getString(R.string.udesk_upload_img_error));

                }
            } else {
                ToastUtils.showToastShort(TheLApp.context, TheLApp.context.getString(R.string.udesk_upload_img_error));

            }
        }
    }

    public void destroy() {
        if (loadingDialog != null) {
            loadingDialog.closeDialog();
        }
    }

}
