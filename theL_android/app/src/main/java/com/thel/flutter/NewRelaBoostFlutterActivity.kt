package com.thel.flutter

import android.text.TextUtils
import com.google.gson.reflect.TypeToken
import com.thel.flutter.bean.RouteBean
import com.thel.utils.GsonUtils
import me.rela.rf_s_bridge.new_router.NewUniversalRouter
import me.rela.rf_s_bridge.new_router.containers.NewBoostFlutterActivity

class NewRelaBoostFlutterActivity : NewBoostFlutterActivity() {

    override fun onBackPressed() {
        NewUniversalRouter.sharedInstance.getFlutterRouteStack { route ->
            if (!TextUtils.isEmpty(route)) {
                val routeBeans = GsonUtils.getGson().fromJson<List<RouteBean>>(route, object : TypeToken<List<RouteBean>>() {

                }.type)
                if (routeBeans.size > 1) {
                    val routerBean: RouteBean = routeBeans[routeBeans.size - 2]

                    if (routerBean.isNative) {
                        NewUniversalRouter.sharedInstance.nativePop()
                    } else {
                        NewUniversalRouter.sharedInstance.nativeRequestFlutterPop()
                    }
                } else {
                    if (NewUniversalRouter.sharedInstance.mOnRouteBackListener != null) {
                        NewUniversalRouter.sharedInstance.mOnRouteBackListener.onRouteBack()
                    }
                }
            }
        }
    }

}