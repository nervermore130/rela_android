package com.thel.constants;

import android.os.Environment;

import com.thel.BuildConfig;
import com.thel.R;
import com.thel.app.TheLApp;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class TheLConstants {

    /**
     * 数据库历史记录职能保存30个人
     */
    public static final int HISTORYSEARCHNUM = 30;
    /**
     * message对象中what的值.1代表循环请求队列里的数据
     */
    public static final int WHATLOOPERQUEUE = 1;
    /**
     * 通过HANDLER通知UI线程
     */
    public static final int HANLDERNOTIUI = 1;
    /**
     * 请求接口成功通知UI线程
     */
    public static final int REPORTNOTIUI = 2;
    /**
     * 官方通知的账号 *
     */
    public static final String SYSTEM_USER = "100241057";
    /**
     * 热拉官方账号
     */
    public static final String RELA_ACCOUNT = "3568";

    public static long SECOND = 1000;

    public static long MINUTES = 60 * SECOND;

    public static long HOUR = 60 * MINUTES;

    public static long DAY = 24 * HOUR;

    ////     微信的appId(只用于做登录和分享)
    //    public static final String WX_APP_ID = TheLConstantsExt.WX_APP_ID;
    //    public static final String WX_SECRET = TheLConstantsExt.WX_SECRET;
    //    public static final String WX_APP_ID_GLOBAL = TheLConstantsExt.WX_APP_ID_GLOBAL;
    //    public static final String WX_SECRET_GLOBAL = TheLConstantsExt.WX_SECRET_GLOBAL;
    //    // 微信登陆的appId(用于支付)
    //    public static final String WX_APP_ID_PAY = TheLConstantsExt.WX_APP_ID_PAY;
    //    public static final String WX_API_KEY = TheLConstantsExt.WX_API_KEY;
    //    // QQ
    //    public static final String QQ_APP_ID = TheLConstantsExt.QQ_APP_ID;// 在manifest文件中也用到了这个值，如果有变，那边也要记得改
    //    public static final String QQ_APP_KEY = TheLConstantsExt.QQ_APP_KEY;
    //    // 听云APP key
    //    public static final String TINGYUN_KEY = TheLConstantsExt.TINGYUN_KEY;
    //    public static final String TINGYUN_KEY_GLOBAL = TheLConstantsExt.TINGYUN_KEY_GLOBAL;
    // 微信的appId(只用于做登录和分享)
    public static String WX_APP_ID = "";
    public static String WX_SECRET = "";
    public static String WX_APP_ID_GLOBAL = "";
    public static String WX_SECRET_GLOBAL = "";
    public static String WX_MINIPROGRAM_RELA_WALKMAN_ID = "";//rela随身听小程序原始ID
    public static String WX_MINIPROGRAM_RELA_WALKMAN_PATH = "/pages/index/index?source=relaapp";//rela随身听小程序path
    // 微信登陆的appId(用于支付)
    public static String WX_APP_ID_PAY = "";
    public static String WX_API_KEY = "";
    //usdk
    public static String UDESK_DOMAIN = "rela.udesk.cn";//UDesk域名
    public static String UDESK_APP_ID = "";//UDesk APP ID
    public static String UDESK_APP_KEY = "";//UDesk APP KEY
    // QQ
    public static String QQ_APP_ID = "";// 在manifest文件中也用到了这个值，如果有变，那边也要记得改
    public static String QQ_APP_KEY = "";
    // 听云APP key
    public static String TINGYUN_KEY = "";
    public static String TINGYUN_KEY_GLOBAL = "";

    public static String SINA_APP_KEY = "";
    public static String SINA_APP_SECRET = "";
    //facebook
    public static String FACE_BOOK_ID = "752962688178144";
    //芝麻信用验证
    public static String ZMXY_MERCHANT_ID = "268821000000977351513";
    //声网
    public static String AGORA_APP_ID = "a245b1e158a8468c86027e4b1568c25d";//走服务端拿
    /***金山模仿直播推流TOKEN***/
    public static String KS_KMC_TOKEN = "e08edbe7b47e2aadb02ecec3b31861f5";
    public static String KS_KMC_GLOBAL_TOKEN = "e08edbe7b47e2aadb02ecec3b31861f5";

    public static final String SINA_SCOPE = "email,direct_messages_read,direct_messages_write,friendships_groups_read,friendships_groups_write,statuses_to_me_read,follow_app_official_microblog,invitation_write";
    // 收到消息时的广播
    public static final String BROADCAST_VIDEO_COUNT = "com.thel.action.video.count";//视频播放次数＋1
    public static final String BROADCAST_REFRESH_MESSAGE = "com.thel.action.refresh.message";//刷新message fragment 消息列表
    public static final String BROADCAST_CLICK_MESSAGE = "com.thel.action.click.message";//点击tab按钮切换MessageFragment
    public static final String BROADCAST_NEW_MESSAGE = "com.thel.action.new.message";//新消息
    public static final String BROADCAST_PENDING_MSG_TYPING = "com.thel.action.broadcast.pending.msg.typing";
    public static final String BROADCAST_PENDING_MSG_SPEAKING = "com.thel.action.broadcast.pending.msg.speaking";
    public static final String BROADCAST_NEW_CHECK_ACTION = "com.thel.new.check.action.broadcast";
    public static final String BROADCAST_NEW_CHECK_LIKE_ME_ACTION = "com.thel.new.check.like_me_action.broadcast";

    public static final String BROADCAST_CLEAR_NEW_CHECK_ACTION = "com.thel.new.clear_check.action.broadcast";
    public static final String BROADCAST_CLEAR_LIKE_ME_NEW_CHECK_ACTION = "com.thel.new.clear_like_me_check.action.broadcast";
    public static final String BROADCAST_CLEAR_RELA_LISTENER_NEW_CHECK_ACTION = "com.thel.new.clear_rela_listener_check.action.broadcast";

    public static final String BROADCAST_RELEASE_NEW_VIDEO_ACTION = "com.thel.release.new.video.action.broadcast";
    public static final String BROADCAST_AUTO_RELEASE_NEW_RECOMMEND_COMMENT_ACTION = "com.thel.auto.release.new.recommend.comment.action.broadcast";
    public static final String BROADCAST_AUTO_RELEASE_NEW_COMMENT_ACTION = "com.thel.auto.release.new.comment.action.broadcast";
    public static final String BROADCAST_MOMENTS_CHECK_ACTION = "com.thel.moments.check.action.broadcast";
    public static final String BROADCAST_UPDATE_RECOMMEND_STICKER = "com.thel.update.recommend.sticker";
    public static final String BROADCAST_UPDATE_UNREAD_MSG_COUNT = "com.thel.update.unread.msg.count";
    public static final String BROADCAST_UPDATE_UNREAD_MSG_COUNT_COMPLETELY = "com.thel.update.unread.msg.count.completely";
    public static final String BROADCAST_CLEAR_NEW_MOMENT_MSG = "com.thel.clear.new.moment.msg";
    public static final String BROADCAST_CLEAR_MOMENT_MSG_COUNT = "com.thel.clear.moment.msg.count";
    public static final String BROADCAST_CLEAR_MOMENT_MSG_COUNT_TOP = "com.thel.clear.moment.msg.count.top";// 日志页面右上角小铃铛
    public static final String BROADCAST_REFRESH_NEED_COMPLETE_USER_INFO = "com.thel.refresh.need.complete_user_info";
    public static final String BROADCAST_FAILED_MOMENTS_CHECK_ACTION = "com.thel.failed.moments.check.action.broadcast";
    public static final String BROADCAST_SEND_MSG_SUCCEED_RECEIPT = "com.thel.send.msg.succeed.receipt.broadcast";
    public static final String BROADCAST_SEND_MSG_FAIL_RECEIPT = "com.thel.send.msg.fail.receipt.broadcast";
    public static final String BROADCAST_RECEIVE_RECEIPT = "com.thel.receive.receipt.broadcast";
    public static final String BROADCAST_MSG_PING = "com.thel.msg.ping.broadcast";
    public static final String BROADCAST_MUSIC = "com.thel.music";
    public static final String BROADCAST_RELEASE_MOMENT_SUBMIT = "com.thel.release.moment.submit";
    public static final String BROADCAST_RELEASE_MOMENT_SUCCEED = "com.thel.release.moment.succeed";
    public static final String BROADCAST_RELEASE_MOMENT_FAIL = "com.thel.release.moment.fail";
    public static final String BROADCAST_RELEASE_MOMENT_DELETE_FAILED = "com.thel.release.moment.delete_failed";
    public static final String BROADCAST_LINK_WX_SUCCEED = "com.thel.link.wx.succeed";
    public static final String BROADCAST_WX_LOGIN_SUCCEED = "com.thel.wx.login.succeed";
    public static final String BROADCAST_WX_LOGIN_CANCEL = "com.thel.wx.login.cancel";
    public static final String BROADCAST_PAY = "com.thel.pay";
    public static final String BROADCAST_WX_SHARE = "com.thel.wx.share";
    public static final String BROADCAST_ACTION_CLOSE_BEFORE_ACTIVITY = "com.thel.ui.activity.BuyVipActivity.closeBeforeActivity().";//关闭之前activity广播action
    public static final String BROADCAST_GOTO_LIVE_ROOMS_PAGE = "com.thel.goto.live.rooms.page";
    public static final String BROADCAST_LIVE_GIFT_FOLLOW_USER = "com.thel.live.gift.follow.user";
    public static final String BROADCAST_GOTO_THEME_PAGE = "com.thel.goto.theme.page";
    public static final String BROADCAST_GOTO_MOMENTFRAGMENTLIST_ACTIVITY_PAGE = "com.thel.goto.momentfragment.list.page";
    public static final String BROADCAST_MOMENT_DATA_CHANGED = "com.thel.moment.data.changed";
    public static final String BROADCAST_VIDEO_PLAYING = "com.thel.video.playing";
    public static final String BROADCAST_NEW_MOMENTS_CLEAR = "com.thel.moments_clear";
    public static final String BROADCAST_STICK_MOMENT = "com.thel.StickMoment";
    public static final String BROADCAST_MATCH_LIKE_ME_LIST = "com.thel.like_me_list";


    public static final String BROADCAST_LIVE_CLOSE = "com.thel.live.close";//直播结束
    public static final String BROADCAST_REFRESH_VIDEOBEAN = "com.thel.refresh.videobean";
    public static final String BROADCAST_GOTO_THEME_TAB = "com.thel.goto_theme_tab";
    public static final String BROADCAST_GOTO_LIVE = "com.thel.goto_live";
    public static final String BROADCAST_GOTO_MESSAGE = "com.thel.goto_message";
    public static final String BROADCAST_GOTO_FLUTTER = "com.thel.goto_flutter";

    public static final String BROADCAST_RELATION_CHANGED = "com.thel.relation_changed";//好友关系变更
    public static final String BROADCAST_REFRESH_USER_FOLLOW_STATUS = "com.thel.refresh_user_follow_status";//好友关系变更
    public static final String BROADCAST_FOLLOW_STATUS_CHANGED = "com.thel.follow_status_changed";//关注状态变更
    public static final String BROADCAST_WINK_USER_CHANGED = "com.thel.wink.user.changed";//是否挤过眼
    public static final String BROADCAST_CHAT_ACTIVITY_REFRESH_UI = "com.thel.chat.activity.refresh.ui";//是否挤过眼
    public static final String BROADCAST_GIFT_WINK_SUCCESS = "com.thel.gift.wink.granted";//是否挤过眼
    public static final String BROADCAST_BLACK_USERS_CHANGED = "com.thel.black.users.changed";//是否挤过眼
    public static final String BROADCAST_BLACK_ONE_MOMENT = "com.thel.black.moment.changed";//屏蔽单个日志
    public static final String BROADCAST_BLACK_ONE_USER_MOMENT = "com.thel.black.one.user.moment";//屏蔽她的全部日志
    public static final String BROADCAST_MOMENT_LIKE_STATUS_CHANGED = "com.thel.black.one.user.moment";//给一个日志点赞状态变化
    public static final String BROADCAST_MOMENT_DELETE_SUCCESS = "com.thel.delete.one.moment";//删除一个日志（只能删除自己的）
    public static final String BROADCAST_MOMENT_REPORT_SUCCESS = "com.thel.report.one.moment";//举报一个日志（只能举报别人的）
    public static final String BROADCAST_VIP_STATUS_CHANGED = "com.thel.vip.status.changed";//会员状态改变（购买了会员）
    public static final String BROADCAST_STICKER_CHANGED = "com.thel.sticker.changed";//表情包变化
    public static final String BROADCAST_GOLD_CHANGED = "com.thel.gold.changed";//软妹豆变化
    public static final String BROADCAST_UPDATA_RATIO = "com.thel.updata.ratio";//更新资料完成度
    public static final String BROADCAST_SEE_ME = "com.thel.see.me";//谁来看过我
    public static final String BROADCAST_SHOW_HOME_GUIDE = "come.thel.show.home.guide";//谁来看过我
    public static final String BROADCAST_ACTION_VIDEO_WEBP_UPDATE = "com.thel.video.webp.update";//谁来看过我
    public static final String BROADCAST_ACTION_MULTI_LIVE_SHARE_RESULT = "com.thel.ui.widget.meetshare";//多人连麦匹配分享结果页
    public static final String BROADCAST_ACTION_HIDING = "com.thel.hiding";//开启，关闭隐身广播
    public static final String BROADCAST_ACTION_SUPERLIKE = "com.thel.superlike";//超级喜欢某人
    public static final String BROADCAST_ACTION_WALKMAN_NEW = "com.thel.walkman_new";//热拉随身听小红点提示
    public static final String BROADCAST_ACTION_SELECT_TAG = "com.thel.select_tag";//发布纪念日选择标签
    public static final String BROADCAST_ACTION_REFRESH_NATIVE = "com.thel.refresh.native";//刷新页面

    public static final String COUNT = "count";
    // 发消息用户ID的前缀，发消息用户id就是前缀+userId
    public static final String MSG_ACCOUNT_USER_ID_STR = "acountuser";
    public static final Pattern TOPIC_PATTERN = Pattern.compile("#([^\\#]+)#");
    public static final String URL_PATTERN_STR = "((http[s]{0,1}|ftp)://[a-zA-Z0-9\\.\\-]+\\.([a-zA-Z]{2,4})(:\\d+)?(/[a-zA-Z0-9\\.\\-~!@#$%^&*+?:_/=<>]*)?)|(www.[a-zA-Z0-9\\.\\-]+\\.([a-zA-Z]{2,4})(:\\d+)?(/[a-zA-Z0-9\\.\\-~!@#$%^&*+?:_/=<>]*)?)";
    public static final Pattern URL_PATTERN = Pattern.compile(URL_PATTERN_STR);

    // 消息和广告的notification id
    public static final int NOTIFICATION_ID_MSG = 1000001;
    public static final int NOTIFICATION_ID_ADVERT = 1000002;
    public static final int NOTIFICATION_ID_SOUND = 1000003;
    public static final int NOTIFICATION_ID_MATCH_ALARM = 1000004;
    public static final int NOTIFICATION_ID_ACTIVITY = 1000005;
    /**
     * activity的标识 start *
     */
    public static final int BUNDLE_CODE_FRIENDS_ACTIVITY = 1001;
    public static final int BUNDLE_CODE_USER_INFO_ACTIVITY = 1002;
    public static final int BUNDLE_CODE_UPDATE_USER_INFO_ACTIVITY = 1003;
    public static final int BUNDLE_CODE_REGISTER_ACTIVITY = 1004;
    public static final int BUNDLE_CODE_LOGIN_ACTIVITY = 1005;
    public static final int BUNDLE_CODE_ME_ACTIVITY = 1006;
    public static final int BUNDLE_CODE_MOMENTS_ACTIVITY = 1007;
    public static final int BUNDLE_CODE_WRITE_MOMENT_ACTIVITY = 1011;
    public static final int BUNDLE_CODE_UPLOAD_IMAGE_ACTIVITY = 1012;
    public static final int BUNDLE_CODE_CHAT_ACTIVITY = 1013;
    public static final int BUNDLE_CODE_TOPIC_MAIN_ACTIVITY = 1014;
    public static final int BUNDLE_CODE_CHAT_OPERATIONS_ACTIVITY = 1015;
    public static final int BUNDLE_CODE_SEARCH_ACTIVITY = 1016;
    public static final int BUNDLE_CODE_REGISTER_FOLLOW_TAGS_ACTIVITY = 1017;
    public static final int BUNDLE_CODE_SMS_REGISTER = 1018;
    public static final int BUNDLE_CODE_SMS_IDENTIFY = 1019;
    public static final int BUNDLE_CODE_SMS_SELECT_COUNTRY = 1020;
    public static final int BUNDLE_CODE_SELECT_PHOTO = 1021;
    public static final int BUNDLE_CODE_FACEBOOK_LOGIN = 64206;
    public static final int BUNDLE_CODE_RECORD_VIDEO = 1022;
    public static final int BUNDLE_CODE_THEME_DETAIL_FRAGMENT = 1023;
    public static final int BUNDLE_CODE_SELECT_BG = 1024;
    public static final int BUNDLE_CODE_SELECT_IMAGE = 1025;
    public static final int BUNDLE_CODE_SELECT_THEME = 1026;
    public static final int BUNDLE_CODE_MY_CIRCLE_REQUEST = 1027;
    public static final int BUNDLE_CODE_FOLLOW_ACTIVITY = 1028;
    public static final int BUNDLE_CODE_SELECT_CLASSFY = 1029;
    public static final int BUNDLE_CODE_ROLE_ACTIVITY = 1030;
    public static final int BUNDLE_CODE_MATCH_ACTIVITY = 1031;
    public static final int BUNDLE_CODE_SHARE_PERMISSION = 1032;
    public static final int BUNDLE_CODE_WRITE_MOMENT_PERVIEW_IMAGE = 1033;
    public static final int BUNDLE_CODE_EDITE_INTRO = 1034;

    /**
     * activity的标识 end *
     */

    public static final int RESULT_CODE_WRITE_MOMENT_DELETE_PICTURE = 10001;
    public static final int RESULT_CODE_WRITE_MOMENT_DELETE_MUSIC = 10002;
    public static final int RESULT_CODE_WRITE_MOMENT_SELECT_TOPIC = 10003;
    public static final int RESULT_CODE_WRITE_MOMENT_SELECT_CONTACT = 10004;
    public static final int RESULT_CODE_SELECT_LOCAL_IMAGE = 10005;
    public static final int RESULT_CODE_CLEAR_CHAT = 10006;
    public static final int RESULT_CODE_BLOCK_THIS_MOMENT = 10007;
    public static final int RESULT_CODE_BLOCK_HER_MOMENTS = 10008;
    public static final int RESULT_CODE_TAKE_PHOTO = 10009;
    public static final int RESULT_CODE_REPORT_MOMENT_SUCCEED = 10011;
    public static final int RESULT_CODE_CANCEL_RELEASE_MOMENT = 10012;
    public static final int RESULT_CODE_WX_LOGIN = 10013;
    public static final int RESULT_CODE_SELECT_CAREER_TYPE = 10014;
    public static final int RESULT_CODE_WRITE_MOMENT_DELETE_VIDEO = 10016;
    public static final int REQUEST_CODE_INIT_USER_INFO = 10055;//新注册用户初始化用户信息
    public static final int REQUEST_CODE_RECHARGE = 10017;//去充值
    public static final int REQUEST_CODE_UPDATA = 10035;//去更新资料
    public static final int RESULT_CODE_RECHARGE = 10018;//去充值,充值成功，表示充值过
    public static final int REQUEST_CODE_BUY_VIP = 10019;//购买会员请求，code
    public static final int REQUEST_CODE_ANDROID_PAY = 10020;// 安卓支付requestcode
    public static final int RESULT_CODE_SMS_NEW = 10021;// 手机登录（注册新用户）
    public static final int RESULT_CODE_SMS_LOGIN = 10022;// 手机登录（注册新用户）
    public static final int REQUEST_CODE_EMAIL_LOGIN = 10023;// 邮箱登录
    public static final int RESULT_CODE_SHOW_WRITE_MOMENT_DIALOG = 10024;// 通知空白，显示写日志对话框
    public static final int RESULT_CODE_VIDEO_PLAY_COUNT = 10025;//视频播放次数
    public static final int RESULT_CODE_EDIT_NICKNAME = 10026;//修改昵称返回
    public static final int RESULT_CODE_EDIT_CAREER = 10046;//修改职业描述
    public static final int RESULT_CODE_EDIT_LIVEIN = 10047;//修改居之地
    public static final int RESULT_CODE_EDIT_TRAVEL = 10048;//修改旅行
    public static final int RESULT_CODE_EDIT_LOCATION = 10049;//修改活动范围
    public static final int RESULT_CODE_EDIT_MOVIE = 10050;//修改影视剧
    public static final int RESULT_CODE_EDIT_MUSIC = 10051;//修改音乐
    public static final int RESULT_CODE_EDIT_BOOK = 10052;//修改书籍
    public static final int RESULT_CODE_EDIT_FOOD = 10053;//修改食物
    public static final int RESULT_CODE_EDIT_OTHER = 10054;//修改其它兴趣
    public static final int RESULT_CODE_EDIT_RELAID = 10036;//relaid修改
    public static final int RESULT_CODE_SELECT_TYPE = 10027;//话题选择分类
    public static final int RESULT_CODE_CANCEL = 10028;//返回，不做任何处理
    public static final int RESULT_CODE_FINISH = 10029;//关闭本页面
    public static final int RESULT_CODE_IGNORE_BIND_PHONE = 10030;//忽略绑定手机
    public static final int RESULT_CODE_SELECT_TYPE_CLASSFY = 10031;//直播选择分类
    public static final int RESULT_CODE_EDIT_INTRO = 10032;//修改详细自述返回
    public static final int RESULT_CODE_RECOMMEND_SUCCESS = 10032;//推荐成功
    public static final int RESULT_CODE_RECOMMEND_TO_CHAT_SUCCESS = 10033;//分享到聊天成功
    public static final int RESULT_CODE_RECOMMEND_TO_MOMENT_SUCCESS = 10034;//推荐到日志成功
    public static final int BUNDLE_CODE_SETTINGS_ACTIVITY = 10035;//去设置页面
    public static final int BUNDLE_CODE_SEND_CARD_ACTIVITY = 10036;
    public static final int REQUEST_CODE_COVER = 10135;//去设封面
    public static final int PERMISSIONS_REQUEST_READ_LOCATION = 10136;//安卓系统设置页面
    public static final int PERMISSIONS_REQUEST_GPS = 10137;
    public static final int REQUEST_SAVE_POSTER = 10138;

    /**
     * bundle用的key start *
     */
    public static final String BUNDLE_KEY_ID = "id";
    public static final String BUNDLE_KEY_LIVE_ROOM = "live_room";
    public static final String BUNDLE_KEY_LIVE_ROOM_CAMERA_CONFIG = "live_room_camera_config";
    public static final String BUNDLE_KEY_LIVE_SEAT_ROOM = "live_seat_room";
    public static final String BUNDLE_KEY_NEED_REFRESH = "need_refresh";
    public static final String BUNDLE_KEY_WHERE_TO_GO = "where_to_go";
    public static final String BUNDLE_KEY_FRAGMENT_TAG = "tag";
    public static final String BUNDLE_KEY_USER_BEAN = "user_bean"; // 传用户对象
    public static final String BUNDLE_KEY_USER_ID = "userId"; // 传用户对象
    public static final String BUNDLE_KEY_USER_AVATAR = "avatar"; // 传用户对象
    public static final String BUNDLE_KEY_MOMENT_BEAN = "moment_bean"; // 传moment对象
    public static final String BUNDLE_KEY_MOMENT_ID = "moment_id"; // 传momentid
    public static final String BUNDLE_KEY_COMMENT_ID = "comment_id"; // 传momentid
    public static final String BUNDLE_KEY_WINK_COMMENT_NUM = "wink_comment_num"; // 点赞数量
    public static final String BUNDLE_KEY_MSG_BEAN = "msg_bean"; // 消息
    public static final String BUNDLE_KEY_MOMENTS_CHECK_BEAN = "moments_check_bean"; // 检查朋友圈
    public static final String BUNDLE_KEY_PHOTOS = "photos"; // 图片列表
    public static final String BUNDLE_KEY_FLUTTER_VIDEO = "flutter_video";
    public static final String BUNDLE_KEY_OUT_FLUTTER_VIDEO = "out_flutter_video";
    public static final String BUNDLE_KEY_PHOTO_INFO_LIST = "photos"; // 图片列表
    public static final String BUNDLE_KEY_SONG = "song"; // 歌曲
    public static final String BUNDLE_KEY_MUSIC_ACTION = "action"; // 歌曲播放
    public static final String BUNDLE_KEY_TOPIC_NAME = "topic_name"; // 话题名
    public static final String BUNDLE_KEY_TOPIC_ID = "topic_id"; // 话题id
    public static final String BUNDLE_KEY_TOPIC_COLOR = "topic_color"; // 话题颜色
    public static final String BUNDLE_KEY_FRIENDS_LIST = "friends_list"; // 朋友列表
    public static final String BUNDLE_KEY_LOCAL_IMAGE_PATH = "local_image_path"; // 本地图片的文件地址
    public static final String BUNDLE_KEY_IMAGE_OUTPUT_PATH = "image_output_path"; // 图片输出地址
    public static final String BUNDLE_KEY_IMAGE_OUTPUT_INFO = "image_output_info"; // 图片输出地址
    public static final String BUNDLE_KEY_LIVE_ROOM_ID = "live_room_id"; // 房间号
    public static final String BUNDLE_KEY_QUALITY = "quality"; // 图片压缩质量
    public static final String BUNDLE_KEY_IMAGE_DATA = "image_data"; // 图片数据
    public static final String BUNDLE_KEY_WIDTH = "width";
    public static final String BUNDLE_KEY_HEIGHT = "height";
    public static final String BUNDLE_KEY_AD_FLAG = "fromAD";
    public static final String BUNDLE_KEY_SELECT_AMOUNT = "selectAmount";// 多选图片的最大数量
    public static final String BUNDLE_KEY_PHOTO_NAME = "photoName";
    public static final String BUNDLE_KEY_INDEX = "index";
    public static final String BUNDLE_KEY_RELEASE_TIME = "releaseTime";
    public static final String BUNDLE_KEY_RELEASE_TYPE = "releaseType";
    public static final String BUNDLE_KEY_TAG_CATEGORY_ID = "tagCategoryId";
    public static final String BUNDLE_KEY_TAG_CATEGORY_NAME = "tagCategoryName";
    public static final String BUNDLE_KEY_REPORT_TYPE = "type";
    public static final String BUNDLE_KEY_DIRECTORY_PATH = "directory_path";
    public static final String BUNDLE_KEY_SELECTED_IMGS = "selected_imgs";
    public static final String BUNDLE_KEY_GOTO_MOMENTSFRAGMENT_TAB = "goto_tab";
    public static final String BUNDLE_KEY_GOTO_NEARBYFRAGMENT_TAB = "goto_nearby_tab";
    public static final String BUNDLE_KEY_MENTIONED_USER_LIST = "mentioned_user_list";//直播页面中提及的朋友列表数据
    public static final String BUNDLE_KEY_THEME_LIST = "bundle_key_theme_list";//话题列表选中的条目
    public static final String BUNDLE_KEY_THEME_CLASS_BEAN = "bundle_key_theme_class_bean";//话题列表选中的条目
    public static final String BUNDLE_KEY_FOLLOW_STATUS = "bundle_key_follow_status";//关注 状态
    public static final String BUNDLE_KEY_BY_PUSH = "bundle_key_by_push"; //4.1。0push推送参数
    public static final String BUNDLE_KEY_BY_CLASSFY = "bundle_key_by_classfy";//4.4.0发布直播日志添加直播分类型
    public static final String BUNDLE_KEY_MEET_RESULT = "bundle_key_meet_result";//4.8.0 相遇结果分享页
    public static final String BUNDLE_KEY_CETTIFICATION_TYPE = "bundle_key_certification";
    //赠送会员新增
    public static final String BUNDLE_KEY_GIVE_VIP = "give_vip";//赠送会员，已用到的value为boolean类型
    public static final String BUNDLE_KEY_FRIEND = "friend";//赠送会员的朋友
    public static final String BUNDLE_KEY_SAY_SOMETHING = "say_something";//赠送会员的留言
    public static final String BUNDLE_KEY_VIP = "vipbean";//赠送的会员(对象)
    public static final String BUNDLE_KEY_CLOSE_BEFORE_ACTIVITY = "colse_before_activity";//关闭之前界面，广播的key,传true关闭activity
    //2.17
    public static final String BUNDLE_KEY_IMAGE_URL = "imageUrl";//直播界面背景图
    //2.18 跳转到版本更新界面传值VersionBean对象
    public static final String BUNDLE_KEY_VERSIONBEAN = "versionbean";
    public static final String BUNDLE_KEY_APK_URL = "apk_url";
    //2.20 跳转来自哪里
    public static final String BUNDLE_KEY_INTENT_FROM = "intent_from";

    //2.22 举报的评论的内容
    public static final String BUNDLE_KEY_REPORT_CONTENT = "report_content";
    //2.24
    public static final String BUNDLE_KEY_RELA_ID = "rela_id";
    public static final String IS_WATERMARK = "is_watermark";
    //3.0.0
    public static final String BUNDLE_KEY_VIDEO_URL = "video_url";//视频地址
    public static final String BUNDLE_KEY_VIDEO_THUMBNAIL_URL = "video_thumbnail_url";//视频缩略图
    public static final String BUNDLE_KEY_VIDEO_BEAN = "video_bean";//视频缩略图
    public static final String BUNDLE_KEY_BEAN_LIST = "bean_list";//bean list
    public static final String BUNDLE_KEY_PLAY_COUNT = "video_play_count";//视频播放次数
    //3.1.0
    public static final String BUNDLE_KEY_NICKNAME = "nickname";//视频播放次数
    public static final String BUNDLE_KEY_VIDEO_PLAYING = "video_playing";
    public static final String BUNDLE_KEY_LIVEROOMBEAN_LIST = "liveroombean_list";
    public static final String BUNDLE_KEY_LIVEROOMBEAN = "live_room_bean";
    public static final String BUNDLE_KEY_ROLE = "logType role";
    public static final String BUNDLE_KEY_POSITION = "position";
    public static final String BUNDLE_KEY_CURSOR = "cursor";
    public static final String BUNDLE_KEY_LIVE_FOLLOWER_USER_BEAN_LIST = "live_follower_user_bean";
    //3.1.1
    public static final String BUNDLE_KEY_HEAD_IMAGEURL = "headurl";//分享海报的人户头像
    public static final String BUNDLE_KEY_IMAGE = "headurl";//分享海报的用户名称
    public static final String BUNDLER_KEY_IMAGESHAREBEAN = "headbean";//分享海报的bean
    public static final String BUNDLER_KEY_USER_IMAGE = "user_image";//个人页面图片分享
    public static final String BUNDLER_KEY_MY_IMAGE = "my_image";//从我的页面相册
    public static final String BUNDLER_KEY_THEME_IMAGE = "theme_image";//从话题分享
    //3.2.0
    public static final String BUNDLE_KEY_PIC_BEAN = "report_pic_id";//举报人户图片id
    /**
     * 4.0.0
     **/
    public static final String BUNDLE_KEY_RECOMMEND_WEB_BEAN = "recommend_web_bean";//推荐的网页日志
    public static final String BUNDLE_KEY_LIVEUSER_BEAN = "live_user";//推荐主播
    public static final String BUNDLE_KEY_TOAST_POSTED = "toast_posted";//发布完推荐内容，已推荐
    public static final String BUNDLE_KEY_RECOMMEND_COUNT = "recommend_count";//推荐日志的人数

    public static final String BUNDLE_KEY_MOMENT_TAG = "moment_tag";

    public static final String BUNDLE_KEY_ACTION_TYPE = "action_type";//用例传递一些type参数
    public static final String BUNDLE_KEY_CONTACT = "contact";//用来传递赠送会员的好友bean
    public static final String BUNDLE_KEY_BLACKUSER_ADD = "blackuser_add";//用来判断黑名单是否添加，传boolean值
    public static final String BUNDLE_KEY_MOMENT_WINK = "is_moment_wink";//判断是否对日志点赞，传boolean值
    public static final String BUNDLE_KEY_MOMENT_COMMENT_WINK_BEAN = "moment_comment_wink_bean";//判断是否对日志点赞，传boolean值
    public static final String BUNDLE_KEY_IS_VIP = "is_vip";//判断是否是vip，传boolean值
    public static final String BUNDLE_KEY_VIP_LEVEL = "vip_level";//会员等级

    public static final String BUNDLE_KEY_CONTENT = "content";
    public static final String BUNDLE_KEY_TITLE = "title";

    /***4.4.0***/
    public static final String BUNDLE_KEY_CURRENT_GOLD = "current_gold";//当前软妹豆数量
    public static final String BUNDLE_DIALOG_MESSAGE = "dialog_message";//弹窗的信息展示
    public static final String BUNDLE_UNREAD_MOMENT_COUNT = "unread_moment_count";
    /**
     * 4.5.0
     */
    public static final String BUNDLE_KEY_INTROCONTENT = "bundle_key_introcontent"; //改动后的详细自述
    public static final String BUNDLE_KEY_INFO_RATIO = "bundle_key_info_ratio";//资料完成度
    public static final String MATCH_LIST = "match_list";
    public static final String BUNDLE_KEY_RELAID = "bundle_key_relaid";//修改的relaId
    public static final String BUNDLE_KEY_CAREER = "bundle_key_career";//修改职业描述
    public static final String BUNDLE_KEY_LIVEIN = "bundle_key_livein";//修改居住地
    public static final String BUNDLE_KEY_TRAVEL = "bundle_key_travel";//修改旅行
    public static final String BUNDLE_KEY_LOCATION = "bundle_key_location";//修改活动范围
    public static final String BUNDLE_KEY_MOVIE = "bundle_key_movie";//修改影视剧
    public static final String BUNDLE_KEY_MUSIC = "bundle_key_music";//修改音乐
    public static final String BUNDLE_KEY_BOOK = "bundle_key_book";//修改书籍
    public static final String BUNDLE_KEY_FOOD = "bundle_key_food";//修改食物
    public static final String BUNDLE_KEY_OTHER = "bundle_key_other";//修改其它兴趣

    /**
     * 4.7.0
     */
    public static final String BUNDLE_KEY_VIDEO_LIST = "bundle_key_video_list";
    public static final String BUNDLE_KEY_VIDEO_LIST_POSITION = "bundle_key_video_list_position";
    public static final String BUNDLE_KEY_MOMENT_SEND_ID = "bundle_key_moment_send_id";
    /**
     * 查看自己推荐的日志时，评论置顶
     */
    public static final String BUNDLE_KEY_COMMENT_ON_TOP = "bundle_key_comment_on_top";
    /**
     * 4.7.3
     */
    public static final String BUNDLE_KEY_VIDEO_WEBP = "bundle_key_video_webp";
    public static final String BUNDLE_KEY_ENTRY = "bundle_key_entry";
    public static final String BUNDLE_KEY_PAGE_FROM = "bundle_key_page_from";
    public static final String BUNDLE_KEY_HAVE_NEXT_PAGE = "bundle_key_have_next_page";

    public static final String BUNDLE_KEY_COUPLE_DETAIL = "BUNDLE_KEY_COUPLE_DETAIL";
    public static final String BUNDLE_KEY_IS_ME_RESULT = "BUNDLE_KEY_IS_ME_RESULT";
    public static final String BUNDLE_KEY_COVER = "BUNDLE_KEY_COVER";
    public static final String BUNDLE_KEY_CHILD_PAGE = "bundle_key_child_page";

    public static final String BUNDLE_KEY_TIME = "bundle_key_time";

    public static final String CHILD_PAGE = "child_page";

    public static final String BROADCAST_ACTION_APK_DOWNLOADED = "com.thel.apk_downloaded";
    /**
     * 埋点用的key
     */
    public static final String FROM_PAGE = "from_page";
    public static final String FROM_PAGE_ID = "from_page_id";

    /**
     * bundle用value end *
     */

    public static final Integer[] emojiImagsSmall = new Integer[]{R.mipmap.icn_activity_emoji_01, R.mipmap.icn_activity_emoji_02, R.mipmap.icn_activity_emoji_03, R.mipmap.icn_activity_emoji_04, R.mipmap.icn_activity_emoji_05, R.mipmap.icn_activity_emoji_06, R.mipmap.icn_activity_emoji_07, R.mipmap.icn_activity_emoji_08, R.mipmap.icn_activity_emoji_09};
    public static final int AVATAR_BIG_SIZE = 250;
    public static final int AVATAR_MIDDLE_SIZE = 175;
    public static final int AVATAR_SMALL_SIZE = 100;
    public static final int MOMENT_PIC_BIG_SIZE = 1500;
    public static final int MOMENT_PIC_SMALL_SIZE = 750;
    public static final int ALBUM_PHOTO_SIZE = 700;
    public static final int ICON_MIDDLE_SIZE = 300;
    public static final int ICON_SMALL_SIZE = 150;
    public static final int ICON_TINY_SIZE = 60;
    public static final int MAX_PIC_WIDTH = 1000;
    public static final int MAX_PIC_HEIGHT = 1000;
    public static final int WX_SHARE_IMAGE_SIZE = 500;
    public static final int PIC_QUALITY = 70;
    public static final String RES_PIC_URL = "res:///";
    public static final String FILE_PIC_URL = "file://";
    public static final String ASSET_PIC_URL = "asset://";
    public static final String BUNDLE_FILTER = "filter";
    /**
     * 购买产品类型：表情包
     */
    public static final String PRODUCT_TYPE_STICKER_PACK = "stickerpack";
    public static final String PRODUCT_TYPE_VIP = "vip";
    public static final String PRODUCT_TYPE_SOFT_GOLD = "gold";
    public static final String PRODUCT_TYPE_SOFT_SUPER_LIKE = "superlike";
    public static final float ad_aspect_ratio = 2.19f;
    public static final float live_ad_aspect_ratio = 3f;
    public static final float LIVE_LIST_BANNER = 4f;
    /**
     * 打赏礼物大动画标识
     */
    public static final String BIG_ANIM_BALLOON = "animBalloon";//气球
    public static final String BIG_ANIM_CROWN = "animCrown";//皇冠
    public static final String BIG_ANIM_STAR_SHOWER = "animStarShower";//流星雨
    public static final String BIG_ANIM_COIN_DROP = "animCoinDrop";//掉金币
    public static final String BIG_ANIM_HUG_HUG = "animBearHug";//抱抱
    public static final String BIG_ANIM_BUBBLE = "animBubbleLove";//泡泡
    public static final String BIG_ANIM_SNOWMAN = "animSnowman";//雪人
    public static final String BIG_ANIM_RING = "animDiamondRing";//戒指
    public static final String BIG_ANIM_KISS = "animLipPrint";//么么哒
    public static final String BIG_ANIM_FERRIS_WHEEL = "animFerrisWheel";//摩天轮
    public static final String BIG_ANIM_PUMPKIN = "animPumpkin";//南瓜
    public static final String BIG_ANIM_CHRISTMAS = "animChristmas";//圣诞老人
    public static final String BIG_ANIM_BOMB = "animBomb";//圣诞老人
    public static final String BIG_ANIM_RICEBALL = "animRiceball";//元宵汤圆
    public static final String BIG_ANIM_FIREWORK = "animFirework";//烟花
    public static final String BIG_ANIM_FIRECRACKER = "animFirecracker";//炮竹
    /**
     * 语言切换
     */
    public static final String LANGUAGE_ZH_CN = "language_zh_CN";//简体中文
    public static final String LANGUAGE_ZH_TW = "language_zh_TW";//繁体中文 台湾
    public static final String LANGUAGE_ZH_HK = "language_zh_HK";//繁体中文 香港//4.1.4新加
    public static final String LANGUAGE_EN_US = "language_en_US";//英语（美）
    public static final String LANGUAGE_FR_RFR = "language_fr_rFR";//法语（法国）
    public static final String LANGUAGE_TH_RTH = "language_th_rTH";//泰语
    public static final String LANGUAGE_ES = "language_ES";//西班牙语
    public static final String LANGUAGE_JA_JP = "language_JA_JP";//日语，4.1.4新加
    /**
     * 2.20.0 日志海报分享 图片名结尾
     */
    public static final String MOMENT_POSTER_SHARE_IMG_END = "_moment_poster_img";
    /**
     * 4.0.0 网页下载图片 图片名结尾
     */
    public static final String WEB_SHARE_IMG_END = "_web_share_img";
    /**
     * 2.22.0 举报主播截图 图片名几位
     */
    public static final String REPORT_LIVESHOW_IMG_END = "_report_liveshow_img";
    /**
     * SD卡位置
     */
    private static final String F_SDCARD = TheLApp.getContext().getExternalFilesDir(null).getPath();
    /**
     * 网络缓存数据目录
     */
    public static final String F_ETAG_ROOTPATH = F_SDCARD + "/thel/netcache/";
    public static final String F_THEL_ROOTPATH = F_SDCARD + "/thel/";
    /**
     * 录音文件存储位置
     */
    public static final String F_VOICE_ROOTPATH = F_SDCARD + "/thel/voice/";
    /**
     * 语音消息存储根目录
     */
    public static final String F_MSG_VOICE_ROOTPATH = F_SDCARD + "/thel/msgVoice/";
    /**
     * 拍照照片存储位置
     */
    public static final String F_TAKE_PHOTO_ROOTPATH = F_SDCARD + "/thel/photo/";
    /**
     * 聊天窗口背景图片存储位置
     */
    public static final String F_CHAT_BG_ROOTPATH = F_SDCARD + "/thel/bg/";
    /**
     * 缓存的音乐
     */
    public static final String F_MUSIC_ROOTPATH = F_SDCARD + "/thel/music/";
    /**
     * 缓存的视频
     */
    public static final String F_VIDEO_ROOTPATH = F_SDCARD + "/thel/video/";
    /**
     * 下载的文件
     */
    public static final String F_DOWNLOAD_ROOTPATH = F_SDCARD + "/thel/download/";
    /**
     * 崩溃日志存储位置
     */
    public static final String F_CRASHLOG_ROOTPATH = F_SDCARD + "/thel/crashlog/";
    /**
     * AR贴纸礼物
     */
    public static final String F_AR_GIFT_ROOTPATH = F_SDCARD + "/thel/stickers/";
    // 如果服务器在维护，每次打开软件只弹一次维护页面
    public static boolean isServerMaintaining = false;
    /**
     * 消息系统版本号
     */
    public static String MSG_CLIENT_VERSION = "1";
    // 可以用来做一些控制的标识
    public volatile static boolean defaultFlag = true;

    public static String live_permit = "live_permit";
    //4.9.0是否是首充 1是首充 0不是
    public static int IsFirstCharge = -1;
    //    public static String applyLivePermitPage = "http://" + URL_TITLE + "/act/newbierecruit/";
    public static final String URL_TITLE = BuildConfig.BUILD_TYPE.equals("debug") ? "dev.rela.me" : "www.rela.me";//4.0.0域名

    /**
     * 该方法是防止带有链接或标签的日志文本点击后，既打开了链接或标签指向的页面，又打开了日志详情。在webviewActivity和TagDetailActivity两个页面oncreate的时候调用该方法，入参为true。在用户点击日志内容中的链接或标签时，调用该方法，入参为false。
     *
     * @param clickable
     */
    public static void setMomentContentClickable(boolean clickable) {
        //                TagDetailActivity.momentContentClickable = clickable;
        //                MomentsFragmentActivity.momentContentClickable = clickable;
        //                UserInfoActivity.momentContentClickable = clickable;
    }


    //趣拍的appkey 与 secret
    public static final String QUPAI_APP_KEY = "20893ec48c39c65";
    public static final String QUPAI_APP_SECRET = "97615926383a4e0196ac78e8767f68ff";

    //数美organization 数美分配给企业的唯一识别码
    public static final String SM_ORGANIZATION = "z8T9p6PjPS40Gq0F8w3q";

    public static final String XIAMI_SONG_PAGE = "http://www.xiami.com/song/";

    public static final String XIAMI_KEY = "ea46428b3aff8b6fe30e93ff765a60af";

    public static final String XIAMI_SECRET = "d081b30e187b21fbb72bac292c4b4e48";

    public static final String RELEASE_CONTENT_KEY = "release content";

    public static final String USER_AGREEMENT_PAGE_URL = "https://" + URL_TITLE + "/termandconditions";

    public static String applyLivePermitPage = "http://" + URL_TITLE + "/act/newbierecruit/";

    public static final String LIVE_AGREEMENT_PAGE_URL = "https://" + URL_TITLE + "/live/convention.html";

    //会员协议
    public static final String BUY_SOFT_MONEY_HELP_PAGE_URL = "https://" + URL_TITLE + "/support/detail.html#Q24";
    //认证信息
    public static final String HOW_TO_VERIFY_PAGE_URL = "https://" + URL_TITLE + "/verification/tutorial.html";
    //直播榜
    public static final String LIVE_RANKING_LIST_URL = "http://" + URL_TITLE + "/act/anchorlist/";
    public static final String VIP_AGREEMENT_PAGE_URL = "https://" + URL_TITLE + "/mobile/share/vip_terms.html";
    //注册帮助
    public static final String LOGIN_HELP_RUL = "https://" + URL_TITLE + "/support/phone";

    public static final String SERVICE_HELP_URL = "https://" + URL_TITLE + "/support";


    /**
     * 挤眼的五种类型
     */
    public static final String WINK_TYPE_WINK = "wink";
    public static final String WINK_TYPE_KISS = "kiss";
    public static final String WINK_TYPE_DIAMOND = "diamond";
    public static final String WINK_TYPE_FLOWER = "flower";
    public static final String WINK_TYPE_LOVE = "love";

    /**
     * 发送挤眼所对应的请求int 类型值
     */
    public static Map<String, String> WINK_TYPE = new HashMap<String, String>() {
        {
            put(WINK_TYPE_WINK, 0 + "");
            put(WINK_TYPE_FLOWER, 5 + "");
            put(WINK_TYPE_KISS, 6 + "");
            put(WINK_TYPE_LOVE, 7 + "");
            put(WINK_TYPE_DIAMOND, 8 + "");
        }
    };

    // about the l site
    public static final String THEL_HELP_SITE = "https://" + URL_TITLE + "/mobile/support_new.html";
    public static final String THEL_SINA_SITE = "http://weibo.com/theLapp";
    public static final String THEL_INS_SITE = "http://instagram.com/thelforlesbian ";
    public static final String THEL_FB_SITE = "https://www.facebook.com/theLapp";
    public static final String THEL_DOUBAN_COOPERATOR = "http://site.douban.com/191933/room/2362135/";

    public static final String SEARCH_HISTORY_JSON = "search history save local";

    //所有日志分享的url
    public static String ALL_MOMENT_SHARE_URL(String momentId) {
        return "https://" + TheLConstants.URL_TITLE + "/share/moment/" + momentId + ".html";
    }

    public static String VIDEO_MOMENT_SHARE_URL(String momentId) {
        return "https://" + TheLConstants.URL_TITLE + "/share/video/" + momentId + ".html";
    }

    public static String LIVE_MOMENT_SHARE_URL(String userId) {
        return "https://" + TheLConstants.URL_TITLE + "/share/live/88888" + userId + ".html";
    }


    public static final String LIVE_PK_LOG_PATH_NAME = "live_pk_log_path.txt";

    public static class NotificationChannelConstants {
        public static final String MSG_SOUND_CHANNEL_ID = "msg_sound_channel_id";//声音通知
        public static final String MSG_ALARM_CHANNEL_ID = "msg_alarm_channel_id";//定时通知
        public static final String MSG_CHANNEL_ID = "msg_channel_id";
    }


    /***4.6.0新增用户等级相对应资源***/
    public static final int[] USER_LEVEL_RES = {R.mipmap.user_level_0, R.mipmap.user_level_1, R.mipmap.user_level_2, R.mipmap.user_level_3, R.mipmap.user_level_4, R.mipmap.user_level_5, R.mipmap.user_level_6, R.mipmap.user_level_7, R.mipmap.user_level_8, R.mipmap.user_level_9, R.mipmap.user_level_10, R.mipmap.user_level_11, R.mipmap.user_level_12, R.mipmap.user_level_13, R.mipmap.user_level_14, R.mipmap.user_level_15, R.mipmap.user_level_16, R.mipmap.user_level_17, R.mipmap.user_level_18, R.mipmap.user_level_19, R.mipmap.user_level_20, R.mipmap.user_level_21, R.mipmap.user_level_22, R.mipmap.user_level_23, R.mipmap.user_level_24, R.mipmap.user_level_25, R.mipmap.user_level_26, R.mipmap.user_level_27, R.mipmap.user_level_28, R.mipmap.user_level_29, R.mipmap.user_level_30, R.mipmap.user_level_31, R.mipmap.user_level_32, R.mipmap.user_level_33, R.mipmap.user_level_34, R.mipmap.user_level_35, R.mipmap.user_level_36, R.mipmap.user_level_37, R.mipmap.user_level_38, R.mipmap.user_level_39, R.mipmap.user_level_40};
    /***4.6.0会员等级相对应资源***/
    public static final int[] VIP_LEVEL_RES = {0, R.mipmap.icn_vip_1, R.mipmap.icn_vip_2, R.mipmap.icn_vip_3, R.mipmap.icn_vip_4};

    public static class EntryConstants {
        /**
         * 关注列表
         */
        public static final String ENTRY_FOLLOWING = "Following";
        /**
         * 日志详情
         */
        public static final String ENTRY_MOMENTDETAIL = "MomentDetail";
        /**
         * 话题评论页
         */
        public static final String ENTRY_THEMECOMMENT = "ThemeComment";
        /**
         * 个人主页
         */
        public static final String ENTRY_USER = "User";
        /**
         * 个人所有视频
         */
        public static final String ENTRY_USERVIDEOS = "UserVideos";
        /**
         * 视频列表
         */
        public static final String ENTRY_RECOMMENDVIDEOS = "RecommendVideos";
        /**
         * 聊天视频分享
         */
        public static final String ENTRY_CHAT = "Chat";
        /**
         * 标签页
         */
        public static final String ENTRY_HASHTAG = "HashTag";
        /**
         * 附近的日志
         */
        public static final String ENTRY_NEARBYMOMENTS = "NearByMoments";
        /**
         * 视频列表上下滑
         */
        public static final String ENTRY_RECOMMENDVIDEOSSLIDE = "RecommendVideosSlide";
        /**
         * 个人所有视频上下滑动
         */
        public static final String ENTRY_USERVIDEOSSLIDE = "UserVideosSlide";

    }

    /**
     * 主界面切换的tab
     */
    public static class MainFragmentPageConstants {
        public static final String FRAGMENT_HOME = "ONE";
        public static final String FRAGMENT_DISCOVER = "TWO";
        public static final String FRAGMENT_NEARBY = "THREE";
        public static final String FRAGMENT_MESSAGE = "FOUR";
        public static final String FRAGMENT_ME = "FIVE";

        public static final String FRAGMENT_THEME = "theme";
    }

    public static class PushConstants {
        public static final String GETUI_RECEIVER_MSG_TYPE = "video";
    }

    /**
     * 直播上报
     */
    public static class LiveInfoLogConstants {
        public static final String TYPE_LIVE_LOG = "liveLog";

        /**
         * 观众切源的status
         */
        public static final String TYPE_PLAY_CHANGE_LOG = "playChangeLog";

        /**
         * 主播切源上报
         */
        public static final String TYPE_LIVE_CHANGE_LONG = "pushChangeLog";

        public static final String TYPE_PK = "pk";

//        public static final String TYPE_FRIEND_LINK_MIC = "friend_link_mic";//好友连麦
//
//        public static final String TYPE_AUDIENCE_LINK_MIC = "audience_link_mic";//观众连麦

        public static final String TYPE_LINK_MIC = "link_mic";

        public static final String TYPE_RTMPPushStart = "RTMPPushStart";

        public static final String TYPE_RTMPPushFail = "RTMPPushFail";

        public static final String TYPE_RTMPPushError = "RTMPPushError";

        public static final String TYPE_FIRST_PUSH = "firstPush";

        public static final String TYPE_FIRST_PLAY = "firstPlay";

        /**
         * 消息失败API上报，（不管认证失败，认证超时，连接失败，ping失败，服务端下发失败）
         */
        public static final String IM_MSG_ERROR_LOG = "imMsgErrorLog";

        public static final String LIVE_MSG_ERROR_LOG = "LiveMsgErrorLog";

    }

    /**
     * GrowingIO打点
     */
    public static class GrowingIOConstants {
        /**
         * 光看视频时长
         */
        public static final String VIDEO_WATCH_TIME = "videoWatchTime";

        /**
         * 观看推荐视频时长
         */
        public static final String RECOMMEND_VIDEO_WATCH_TIME = "recommendvideoWatchTime";

        /**
         * 观看直播时长
         */
        public static final String LIVE_WATCH_TIME = "liveWatchTime";
    }

    /**
     * 聊天服务器常量类
     */
    public static class MsgTypeConstant {

        /**
         * 连接聊天服务器成功
         */
        public static final String MSG_TYPE_OK = "OK";

        /**
         * 初始化聊天时返回500
         */
        public static final String MSG_TYPE_FIVE_HUNDRED = "500";

        /**
         * 有新消息了，需要同步数据了
         */
        public static final String MSG_TYPE_INCOMING = "incoming";

        /**
         * 正在打字和正在说话消息
         */
        public static final String MSG_TYPE_DIRECT_FROM = "direct:from";

        /**
         * 正在打字和正在说话消息
         */
        public static final String MSG_TYPE_DIRECT_TO = "direct:to:";

        /**
         * 连接聊天服务器验证
         */
        public static final String MSG_TYPE_AUTH = "auth";

        /**
         * 连接聊天服务器验证V1
         */
        public static final String MSG_TYPE_AUTH_V1 = "auth_v1";

        /**
         * 拉去数据
         */
        public static final String MSG_TYPE_SYNC = "sync";

        /**
         * 最后拉取数据的下标
         */
        public static final String MSG_TYPE_LAST_SYNC = "lastSync";

        /**
         * ping服务器
         */
        public static final String MSG_TYPE_PING = "ping";

        /**
         * ping服务器成功返回
         */
        public static final String MSG_TYPE_PONG = "pong";

        /**
         * 还有更多消息
         */
        public static final String MSG_TYPE_MORE = "more";

        /**
         * 文字消息类型
         */
        public static final String MSG_TYPE_TEXT = "text";

        /**
         * 声音消息类型
         */
        public static final String MSG_TYPE_VOICE = "voice";

        /**
         * 已读消息的回执
         */
        public static final String MSG_TYPE_RECEIVED = "received";

        /**
         * 错误的token
         */
        public static final String ERR_TOKEN = "err_token";

        public static final String NO_SUCH_SESSION = "no_such_session";

    }

    /**
     * 聊天消息的状态
     */
    public static class MsgSendingStatusConstants {

        /**
         * 消息发送中
         */
        public static final String MSG_SENDING = "sending";

        /**
         * 消息发送成功
         */
        public static final String MSG_SUCCESS = "success";

        /**
         * 消息发送失败
         */
        public static final String MSG_FAILED = "failed";

        /**
         * 不合法的消息
         */
        public static final String MSG_ILLEGAL = "illegal";

        /**
         * 已读消息
         */
        public static final String MSG_READ = "read";

        /**
         * 用户已被禁用
         */
        public static final String MSG_CONFINE = "confine";

        /**
         * 你已经屏蔽该用户
         */
        public static final String MSG_BLOCK = "block";

        /**
         * 对方屏蔽了你
         */
        public static final String MSG_BLACK = "black";
    }

    public static class MsgCodeConstants {

        /**
         * 消息发送成功
         */
        public static final String MSG_CODE_OK = "OK";

        /**
         * 消息不合法
         */
        public static final String MSG_CODE_ERROR = "400";

        /**
         * 服务器错误
         */
        public static final String MSG_CODE_ZERO = "500";

        /**
         * 连接出错
         */
        public static final String MSG_CODE_NO_SUCH_SESSION = "no_such_session";

    }

    public static class SyncMsgType {

        public static final int SYNC_MSG_DELAY_TIME = 20;

        /**
         * 循环调用sync
         */
        public static final int SYNC_MSG_LOOP = 0x01;

        /**
         * 当消息来时 主动调用sync
         */
        public static final int SYNC_MSG_COMING = 0x02;

    }

    /********************4.8.0多人连麦**********************/
    public static final String BUNDLE_KEY_SEAT_BEAN = "seatBean";
    public static final String[] defaultSeatNames = {"1号", "2号", "3号", "4号", "5号", "6号", "7号", "8号"};

    public static class MeetResultConstant {

        /**
         * 主播全场结果页
         */
        public static final int MEET_RESULT_BROADCASTER = 0x01;

        /**
         * 我的嘉宾结果页
         */
        public static final int MEET_RESULT_GUEST_ME = 0x02;

        /**
         * 嘉宾全场结果页
         */
        public static final int MEET_RESULT_GUEST_ALL = 0x03;

        /**
         * 观众全场结果页
         */
        public static final int MEET_RESULT_AUDIENCE = 0x04;

    }

    public static class MeetStatusConstant {

        /**
         * 相遇成功
         */
        public static final int MEET_STATUS_SUCCESS = 0X01;

        /**
         * 相遇失败
         */
        public static final int MEET_STATUS_FAILED = 0X02;
    }

    public static class SchemeConstant {
        public static final String SCHEME_PATH_MATCH = "thel://path.match";
        public static final String SCHEME_PATH_THEME = "thel://path.theme";
        public static final String SCHEME_PATH_THEMEDETAIL = "thel://path.themeDetail";
        public static final String SCHEME_PATH_MOMENTDETAIL = "thel://path.momentDetail";
        public static final String SCHEME_PATH_VIP = "thel://path.vipServiceView";
    }

    public static final String[] LIVE_FAST_EMOJI = {"\uD83D\uDC4F\uD83D\uDC4F\uD83D\uDC4F", "\uD83D\uDE02\uD83D\uDE02\uD83D\uDE02", "❤️❤️❤️", "\uD83D\uDE44\uD83D\uDE44\uD83D\uDE44", "\uD83D\uDE09\uD83D\uDE09\uD83D\uDE09"};

    public static final String[] IM_FAST_EMOJI = {"Hi～", "\uD83D\uDE00\uD83D\uDE00\uD83D\uDE00", "\uD83C\uDF39\uD83C\uDF39\uD83C\uDF39", "\uD83D\uDE09\uD83D\uDE09\uD83D\uDE09", "❤️❤️❤️", "\uD83D\uDE33\uD83D\uDE33\uD83D\uDE33", "\uD83D\uDE2C\uD83D\uDE2C\uD83D\uDE2C️"};

    public static class FireBaseConstant {
        public static final String Login = "login";//登陆---当用户成功登录账户后触发
        public static final String SIGN_UP = "register";//注册---当用户成功注册账户后触发
        public static final String ATTENTION = "attention";//关注---当用户“关注”她人时触发
        public static final String VIEW_ITEM_CHAT = "view_item_chat";//聊天列表进去聊天页面
        public static final String WINK = "wink";//挤眼---当用户向她人“挤眼”时触发
        public static final String VIEW_ITEM_LIVE = "view_item_live";// 直播观看---当用户进入“直播间”观看直播时触发，（是直播放间，不是直播列表页哦
        public static final String VIEW_ITEM_TOPIC = "view_item_topic";//访问话题详情---当用户进入“话题详情页”时触发
        public static final String VIEW_ITEM_PEOPLE_NEARBY = "view_item_people_nearby";//访问附近的人---当用户访问“附近的人页面”时触发
        public static final String VIEW_ITEM_NEARBY_LOG = "view_item_nearby_log";//访问附近日志---当用户访问”附近的日志页面“时触发
        public static final String VIEW_ITEM_PURCHASE_DETAILS = "view_item_purchase_details";//商品详情页访问---当用户访问“软妹豆购买列表页面或者会员购买列表页”时触发


    }
}
