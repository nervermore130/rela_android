package com.thel.constants;

/**
 * Created by liuyun on 2017/9/28.
 */

public class ActionConstants {
    public static final String MUSIC_SERVICE_PLAY = "com.thel.service.MusicService.PLAY";

    public static final String MUSIC_SERVICE_PAUSE = "com.thel.service.MusicService.PAUSE";

    public static final String MUSIC_SERVICE_DATA = "com.thel.service.MusicService.DATA";

    public static final String VIDEO_START = "com.thel.service.VideoPLayer.START";

    public static final String VIDEO_STOP = "com.thel.service.VideoPLayer.STOP";

    public static final String VIDEO_POSITION = "com.thel.service.VideoPLayer.POSITION";

}
