package com.thel.constants;

import android.util.SparseArray;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.theme.ThemeClassBean;
import com.thel.modules.main.me.bean.RoleBean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by liuyun on 2017/9/19.
 */

public class QueueConstants {

    public static SparseArray<String> affectionArr = new SparseArray<>();

    public static Map<String, String> roleMap = new HashMap<>();

    public static List<RoleBean> roleList = new ArrayList<>();

    public static SparseArray<Integer> levelArray = new SparseArray<>();

    static {

        affectionArr.put(0, "不想透露");
        affectionArr.put(1, "单身");
        affectionArr.put(2, "约会中");
        affectionArr.put(3, "稳定关系");
        affectionArr.put(4, "已婚");
        affectionArr.put(5, "开放关系");

        roleMap.put("0", "不想透露");
        roleMap.put("1", "T");
        roleMap.put("2", "P");
        roleMap.put("3", "H");
        roleMap.put("4", "Bi");
        roleMap.put("5", "其他");

        roleList.add(new RoleBean(1, "T"));
        roleList.add(new RoleBean(2, "P"));
        roleList.add(new RoleBean(3, "H"));
        roleList.add(new RoleBean(4, "Bi"));
        roleList.add(new RoleBean(5, "其他"));
        roleList.add(new RoleBean(0, "不想透露"));

        levelArray.put(0, R.mipmap.user_level_0);
        levelArray.put(1, R.mipmap.user_level_1);
        levelArray.put(2, R.mipmap.user_level_2);
        levelArray.put(3, R.mipmap.user_level_3);
        levelArray.put(4, R.mipmap.user_level_4);
        levelArray.put(5, R.mipmap.user_level_5);
        levelArray.put(6, R.mipmap.user_level_6);
        levelArray.put(7, R.mipmap.user_level_7);
        levelArray.put(8, R.mipmap.user_level_8);
        levelArray.put(9, R.mipmap.user_level_9);
        levelArray.put(10, R.mipmap.user_level_10);
        levelArray.put(11, R.mipmap.user_level_11);
        levelArray.put(12, R.mipmap.user_level_12);
        levelArray.put(13, R.mipmap.user_level_13);
        levelArray.put(14, R.mipmap.user_level_14);
        levelArray.put(15, R.mipmap.user_level_15);
        levelArray.put(16, R.mipmap.user_level_16);
        levelArray.put(17, R.mipmap.user_level_17);
        levelArray.put(18, R.mipmap.user_level_18);
        levelArray.put(19, R.mipmap.user_level_19);
        levelArray.put(20, R.mipmap.user_level_20);
        levelArray.put(21, R.mipmap.user_level_21);
        levelArray.put(22, R.mipmap.user_level_22);
        levelArray.put(23, R.mipmap.user_level_23);
        levelArray.put(24, R.mipmap.user_level_24);
        levelArray.put(25, R.mipmap.user_level_25);
        levelArray.put(26, R.mipmap.user_level_26);
        levelArray.put(27, R.mipmap.user_level_27);
        levelArray.put(28, R.mipmap.user_level_28);
        levelArray.put(29, R.mipmap.user_level_29);
        levelArray.put(30, R.mipmap.user_level_30);
        levelArray.put(31, R.mipmap.user_level_31);
        levelArray.put(32, R.mipmap.user_level_32);
        levelArray.put(33, R.mipmap.user_level_33);
        levelArray.put(34, R.mipmap.user_level_34);
        levelArray.put(35, R.mipmap.user_level_35);
        levelArray.put(36, R.mipmap.user_level_36);
        levelArray.put(37, R.mipmap.user_level_37);
        levelArray.put(38, R.mipmap.user_level_38);
        levelArray.put(39, R.mipmap.user_level_39);
        levelArray.put(40, R.mipmap.user_level_40);

    }

    public static Map<Integer, String> GIFT_NUM_MEANING = new HashMap<Integer, String>() {
        {
            put(11, TheLApp.context.getString(R.string.lian_1));
            put(52, TheLApp.context.getString(R.string.lian_52));
            put(99, TheLApp.context.getString(R.string.lian_99));
            put(100, TheLApp.context.getString(R.string.lian_100));
            put(222, TheLApp.context.getString(R.string.lian_222));
            put(299, TheLApp.context.getString(R.string.lian_299));
            put(520, TheLApp.context.getString(R.string.lian_520));
            put(666, TheLApp.context.getString(R.string.lian_666));
            put(999, TheLApp.context.getString(R.string.lian_999));
            put(1314, TheLApp.context.getString(R.string.lian_1314));
            put(3344, TheLApp.context.getString(R.string.lian_3344));
            put(9999, TheLApp.context.getString(R.string.lian_9999));
        }
    };
    public static Map<Integer, Integer> themerResMap = new HashMap<Integer, Integer>() {
        {
            put(0, R.mipmap.icn_topic_heart);
            put(1, R.mipmap.icn_topic_hand);
            put(2, R.mipmap.icn_topic_smile);
            put(3, R.mipmap.icn_topic_leaves);
            put(4, R.mipmap.icn_topic_literary);
            put(5, R.mipmap.icn_topic_comeout);
            put(6, R.mipmap.icn_topic_work);
            put(7, R.mipmap.icn_topic_bi);
            put(8, R.mipmap.icn_topic_recourse);
            put(9, R.mipmap.icn_topic_other);

        }
    };
    public static Map<Integer, ThemeClassBean> themeClassBeanMap = new HashMap<Integer, ThemeClassBean>() {
        {
            put(0, new ThemeClassBean("hot", TheLApp.context.getResources().getString(R.string.theme_select_title), -1, TheLApp.context.getResources().getStringArray(R.array.theme_select_classify_info)[0]));
            put(1, new ThemeClassBean("love", TheLApp.context.getResources().getStringArray(R.array.theme_select_classify)[0], 0, TheLApp.context.getResources().getStringArray(R.array.theme_select_classify_info)[0]));
            //put(2, new ThemeClassBean("sex", TheLApp.context.getResources().getStringArray(R.array.theme_select_classify)[1], 1, TheLApp.context.getResources().getStringArray(R.array.theme_select_classify_info)[1]));
            put(2, new ThemeClassBean("joke", TheLApp.context.getResources().getStringArray(R.array.theme_select_classify)[2], 2, TheLApp.context.getResources().getStringArray(R.array.theme_select_classify_info)[2]));
            put(3, new ThemeClassBean("life", TheLApp.context.getResources().getStringArray(R.array.theme_select_classify)[3], 3, TheLApp.context.getResources().getStringArray(R.array.theme_select_classify_info)[3]));
            put(4, new ThemeClassBean("art", TheLApp.context.getResources().getStringArray(R.array.theme_select_classify)[4], 4, TheLApp.context.getResources().getStringArray(R.array.theme_select_classify_info)[4]));
            put(5, new ThemeClassBean("out", TheLApp.context.getResources().getStringArray(R.array.theme_select_classify)[5], 5, TheLApp.context.getResources().getStringArray(R.array.theme_select_classify_info)[5]));
            put(6, new ThemeClassBean("job", TheLApp.context.getResources().getStringArray(R.array.theme_select_classify)[6], 6, TheLApp.context.getResources().getStringArray(R.array.theme_select_classify_info)[6]));
            //  put(8, new ThemeClassBean("bisexual", TheLApp.context.getResources().getStringArray(R.array.theme_select_classify)[7], 7, TheLApp.context.getResources().getStringArray(R.array.theme_select_classify_info)[7]));
            put(7, new ThemeClassBean("help", TheLApp.context.getResources().getStringArray(R.array.theme_select_classify)[8], 8, TheLApp.context.getResources().getStringArray(R.array.theme_select_classify_info)[8]));
            put(8, new ThemeClassBean("other", TheLApp.context.getResources().getStringArray(R.array.theme_select_classify)[9], 9, TheLApp.context.getResources().getStringArray(R.array.theme_select_classify_info)[9]));

        }
    };

    /**
     * 挤眼的五种类型
     */
    public static final String WINK_TYPE_WINK = "wink";
    public static final String WINK_TYPE_KISS = "kiss";
    public static final String WINK_TYPE_DIAMOND = "diamond";
    public static final String WINK_TYPE_FLOWER = "flower";
    public static final String WINK_TYPE_LOVE = "love";

    /**
     * 发送挤眼所对应的请求int 类型值
     */
    public static Map<String, String> WINK_TYPE = new HashMap<String, String>() {
        {
            put(WINK_TYPE_WINK, 0 + "");
            put(WINK_TYPE_FLOWER, 5 + "");
            put(WINK_TYPE_KISS, 6 + "");
            put(WINK_TYPE_LOVE, 7 + "");
            put(WINK_TYPE_DIAMOND, 8 + "");
        }
    };

    public static final Integer[] emojiImags = new Integer[]{R.drawable.btn_emoji_01_s, R.drawable.btn_emoji_02_s, R.drawable.btn_emoji_03_s, R.drawable.btn_emoji_04_s, R.drawable.btn_emoji_05_s, R.drawable.btn_emoji_06_s, R.drawable.btn_emoji_07_s, R.drawable.btn_emoji_08_s, R.drawable.btn_emoji_09_s};
    public static final Integer[] emojiImagsMiddle = new Integer[]{R.mipmap.emoji_like_01, R.mipmap.emoji_like_02, R.mipmap.emoji_like_03, R.mipmap.emoji_like_04, R.mipmap.emoji_like_05, R.mipmap.emoji_like_06, R.mipmap.emoji_like_07, R.mipmap.emoji_like_08, R.mipmap.emoji_like_09};
    public static final Integer[] emojiImagsSmall = new Integer[]{R.mipmap.icn_activity_emoji_01, R.mipmap.icn_activity_emoji_02, R.mipmap.icn_activity_emoji_03, R.mipmap.icn_activity_emoji_04, R.mipmap.icn_activity_emoji_05, R.mipmap.icn_activity_emoji_06, R.mipmap.icn_activity_emoji_07, R.mipmap.icn_activity_emoji_08, R.mipmap.icn_activity_emoji_09};
    public static final String[] filterAgeRangeItems = new String[]{"18 - 22", "23 - 28", "29 - 34", "35 - 40", "41 - 100", TheLApp.context.getString(R.string.info_any)};
    public static final String[] filterWeightItems = new String[]{"41 - 50", "51 - 60", "61 - 70", "71 - 80", TheLApp.context.getString(R.string.info_any)};
    public static final String[] filterHeightItems = new String[]{"140 - 150", "151 - 160", "161 - 170", "171 - 180", "181 - 190", "190 - 201", TheLApp.context.getString(R.string.info_any)};
    public static final String[] filterOnlineItems = new String[]{TheLApp.context.getString(R.string.online_status_online), TheLApp.context.getString(R.string.online_status_active), TheLApp.context.getString(R.string.info_any)};

}
