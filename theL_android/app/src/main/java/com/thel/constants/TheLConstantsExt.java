package com.thel.constants;

public final class TheLConstantsExt {


    private native static String getUrlTitle();

    private native static String getWxAppId();

    private native static String getWxSecret();

    private native static String getWxAppIdGlobal();

    private native static String getWxSecretGlobal();

    private native static String getWxAppIdPay();

    private native static String getWxApiKey();

    private native static String getQqAppId();

    private native static String getQqAppKey();

    private native static String getTingyunKey();

    private native static String getTingyunKeyGlobal();

    private native static String getUdeskDomain();

    private native static String getUdeskAppId();

    private native static String getUdeskAppKey();

    private native static String getThelHelpSite();

    private native static String getThelSinaSite();

    private native static String getThelInsSite();

    private native static String getThelFbSite();

    private native static String getThelDoubanCooperator();

    private native static String getXiamiSongPage();

    private native static String getSinaAppKey();

    private native static String getSinaAppSecret();

    private native static String getSinaRedirectUrl();

    public static final String URL_TITLE = getUrlTitle();//4.0.0域名
    // 微信的appId(只用于做登录和分享)
    public static final String WX_APP_ID = getWxAppId();
    public static final String WX_SECRET = getWxSecret();
    public static final String WX_APP_ID_GLOBAL = getWxAppIdGlobal();
    public static final String WX_SECRET_GLOBAL = getWxSecretGlobal();
    // 微信登陆的appId(用于支付)
    public static final String WX_APP_ID_PAY = getWxAppIdPay();
    public static final String WX_API_KEY = getWxApiKey();
    // QQ
    public static final String QQ_APP_ID = getQqAppId();// 在manifest文件中也用到了这个值，如果有变，那边也要记得改
    public static final String QQ_APP_KEY = getQqAppKey();
    // 听云APP key
    // public static final String TINGYUN_KEY = "4c08614a4d9a4397913fa618cbcc0af8";
    public static final String TINGYUN_KEY = getTingyunKey();
    public static final String TINGYUN_KEY_GLOBAL = getTingyunKeyGlobal();
    //usdk
    public static final String UDESK_DOMAIN = getUdeskDomain();//UDesk域名
    public static final String UDESK_APP_ID = getUdeskAppId();//UDesk APP ID
    public static final String UDESK_APP_KEY = getUdeskAppKey();//UDesk APP KEY

    // about the l site
    public static final String THEL_HELP_SITE = getThelHelpSite();
    public static final String THEL_SINA_SITE = getThelSinaSite();
    public static final String THEL_INS_SITE = getThelInsSite();
    public static final String THEL_FB_SITE = getThelFbSite();
    public static final String THEL_DOUBAN_COOPERATOR = getThelDoubanCooperator();
    public static final String XIAMI_SONG_PAGE = getXiamiSongPage();
    public static final String SINA_APP_KEY = getSinaAppKey();
    public static final String SINA_APP_SECRET = getSinaAppSecret();
    public static final String SINA_REDIRECT_URL = getSinaRedirectUrl();
    public static final String DEFAULT_SHARE_LOGO_URL = "http://pro.rela.me/app/share/icon/share_logo.png";
    public static final String SHARE_THEME_LOGO_URL = "http://pro.rela.me/app/share/icon/topicpost_default.png";
    public static final String USER_AGREEMENT_PAGE_URL = "https://" + URL_TITLE + "/termandconditions";
    public static final String HOW_TO_VERIFY_PAGE_URL = "https://" + URL_TITLE + "/verification/tutorial.html";
    public static final String VIP_AGREEMENT_PAGE_URL = "https://" + URL_TITLE + "/mobile/share/vip_terms.html";
    public static final String LIVE_AGREEMENT_PAGE_URL = "https://" + URL_TITLE + "/live/terms.html";
    public static final String LIVE_RANKING_LIST_URL = "http://" + URL_TITLE + "/act/anchorlist/";
    //会员协议
    public static final String BUY_SOFT_MONEY_HELP_PAGE_URL = "https://" + URL_TITLE + "/support/detail.html#Q24";
    //所有日志分享的url
//    public static final String ALL_MOMENT_SHARE_URL = "https://" + TheLConstants.URL_TITLE + "/share/moment/";
//    public static final String VIDEO_MOMENT_SHARE_URL = "https://" + TheLConstants.URL_TITLE + "/share/video/";
//    public static final String LIVE_MOMENT_SHARE_URL = "https://" + TheLConstants.URL_TITLE + "/share/live/88888";
    public static String applyLivePermitPage = "http://" + URL_TITLE + "/act/newbierecruit/";

    public static final int MOMENT_TEXT_CONTENT_MAX_LINE = 5;

}
