package com.thel.constants;

import com.networkbench.agent.impl.NBSAppAgent;
import com.thel.BuildConfig;
import com.thel.app.TheLApp;
import com.thel.network.RequestConstants;
import com.thel.utils.L;
import com.thel.utils.MD5Utils;
import com.umeng.socialize.PlatformConfig;

import java.util.Map;

import cn.udesk.UdeskSDKManager;

public class Configurations {

    private static final String TAG = "Configurations";

    static {
        System.loadLibrary("native-configuration-lib");
    }

    public static native Map<String, String> configurationMap();

    public static void initConfiguration() {

        Map<String, String> ketMap = Configurations.configurationMap();

        try {


            L.d(TAG, " ketMap : " + ketMap);

            MD5Utils.SALT = ketMap.get("REQUEST_SALT");

            TheLConstants.WX_APP_ID = BuildConfig.APPLICATION_ID.equals("com.thel") ? ketMap.get("WX_APP_ID") : "wx1b28a72274545b88";

            TheLConstants.WX_SECRET = BuildConfig.APPLICATION_ID.equals("com.thel") ? ketMap.get("WX_SECRET") : "afb717805b3a81420767078b858ff2fc";

            TheLConstants.WX_APP_ID_GLOBAL = ketMap.get("WX_APP_ID_GLOBAL");

            TheLConstants.WX_SECRET_GLOBAL = ketMap.get("WX_SECRET_GLOBAL");

            TheLConstants.WX_APP_ID_PAY = ketMap.get("WX_APP_ID_PAY");

            TheLConstants.WX_API_KEY = ketMap.get("WX_API_KEY");

            TheLConstants.UDESK_APP_ID = ketMap.get("UDESK_APP_ID");

            TheLConstants.UDESK_APP_KEY = ketMap.get("UDESK_APP_KEY");

            TheLConstants.QQ_APP_ID = ketMap.get("QQ_APP_ID");

            TheLConstants.QQ_APP_KEY = ketMap.get("QQ_APP_KEY");

            TheLConstants.TINGYUN_KEY = ketMap.get("TINGYUN_KEY");

            TheLConstants.TINGYUN_KEY_GLOBAL = ketMap.get("TINGYUN_KEY_GLOBAL");

            TheLConstants.SINA_APP_KEY = ketMap.get("SINA_APP_KEY");

            TheLConstants.SINA_APP_SECRET = ketMap.get("SINA_APP_SECRET");

            TheLConstants.AGORA_APP_ID = ketMap.get("AGORA_APP_ID");

            TheLConstants.KS_KMC_TOKEN = ketMap.get("KS_KMC_TOKEN");

            TheLConstants.WX_MINIPROGRAM_RELA_WALKMAN_ID = "gh_5acda266e504";

            initShareConfig();

            initUDesk();

            initNBS();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static void initShareConfig() {
        if (RequestConstants.APPLICATION_ID_LOCAL.equals(BuildConfig.APPLICATION_ID)) {
            PlatformConfig.setWeixin(TheLConstants.WX_APP_ID, TheLConstants.WX_SECRET);
        } else {
            PlatformConfig.setWeixin(TheLConstants.WX_APP_ID_GLOBAL, TheLConstants.WX_SECRET_GLOBAL);
        }
        PlatformConfig.setQQZone(TheLConstants.QQ_APP_ID, TheLConstants.QQ_APP_KEY);
        PlatformConfig.setSinaWeibo(TheLConstants.SINA_APP_KEY, TheLConstants.SINA_APP_SECRET, "http://sns.whalecloud.com");
    }

    private static void initNBS() {
        if (RequestConstants.APPLICATION_ID_LOCAL.equals(BuildConfig.APPLICATION_ID))
            NBSAppAgent.setLicenseKey(TheLConstants.TINGYUN_KEY).withLocationServiceEnabled(true).start(TheLApp.context);
        else
            NBSAppAgent.setLicenseKey(TheLConstants.TINGYUN_KEY_GLOBAL).withLocationServiceEnabled(true).start(TheLApp.context);
    }

    private static void initUDesk() {

        //udesk sdk
        UdeskSDKManager.getInstance().initApiKey(TheLApp.context, TheLConstants.UDESK_DOMAIN, TheLConstants.UDESK_APP_KEY, TheLConstants.UDESK_APP_ID);
    }

}
