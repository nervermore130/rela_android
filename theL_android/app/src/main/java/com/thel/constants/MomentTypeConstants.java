package com.thel.constants;

/**
 *
 * @author liuyun
 * @date 2017/10/11
 */

public class MomentTypeConstants {
    /**
     * 纯文本
     */
    public static final String MOMENT_TYPE_TEXT = "text";
    /**
     * 图片日志
     */
    public static final String MOMENT_TYPE_IMAGE = "image";
    /**
     * 带文本的图片日志
     */
    public static final String MOMENT_TYPE_TEXT_IMAGE = "text_image";
    /**
     * 纯音乐日志
     */
    public static final String MOMENT_TYPE_VOICE = "voice";
    /**
     * 带文字的音乐日志
     */
    public static final String MOMENT_TYPE_TEXT_VOICE = "text_voice";
    /**
     * 话题
     */
    public static final String MOMENT_TYPE_THEME = "theme";
    /**
     * 参与话题
     */
    public static final String MOMENT_TYPE_THEME_REPLY = "themereply";
    /**
     * 视频
     */
    public static final String MOMENT_TYPE_VIDEO = "video";
    /**
     * 直播
     */
    public static final String MOMENT_TYPE_LIVE = "live";
    /**
     * 推荐用户卡片
     */
    public static final String MOMENT_TYPE_USER_CARD = "user_card";
    /**
     * 推荐日志（转发）
     */
    public static final String MOMENT_TYPE_RECOMMEND = "recommend";
    /**
     * 网页日志
     */
    public static final String MOMENT_TYPE_WEB = "web";
    /**
     * 直播用户
     */
    public static final String MOMENT_TYPE_LIVE_USER = "live_user";
    /**
     * 广告日志
     */
    public static final String MOMENT_TYPE_AD = "ad";
    /**
     * 声音直播日志
     */
    public static final String MOMENT_TYPE_VOICE_LIVE = "voice_live";
    /**
     * 推荐声音直播用户
     */
    public static final String MOMENT_TYPE_VOICE_LIVE_USER = "voice_live_user";

}
