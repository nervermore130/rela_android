package com.thel.constants;

/**
 *
 * @author liuyun
 * @date 2017/10/12
 */

public class BundleConstants {
    public static final String MOMENTS_ID = "moments_id";
    public static final String SONG_ID = "songId";
    public static final String POSITION = "position";
    public static final String NEED_SECURITY_CHECK = "needSecurityCheck";
    public static final String URL = "url";
    public static final String SITE = "site";
    public static final String TITLE = "title";
    public static final String THEMEPARTICIPATES = "themeParticipates";
    public static final String FROME_WRITE_COMMENT="momentCommentOpr";
}
