package com.thel.imp.like;

import com.thel.bean.gift.WinkCommentBean;

/**
 * Created by waiarl on 2017/11/17.
 * 点赞需要实现的接口
 */

public interface MomentLikeStatusChangedListener {
    /**
     * @param momentId       日志id
     * @param like           是否是喜欢，true:点赞，false:取消点赞
     * @param myUserId       我的id
     * @param myWinkUserBean 我的winkbean
     */
    void onLikeStatusChanged(String momentId, boolean like, String myUserId, WinkCommentBean myWinkUserBean);
}
