package com.thel.imp.like;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;
import android.util.Log;

import com.thel.app.TheLApp;
import com.thel.base.BaseDataBean;
import com.thel.bean.gift.WinkCommentBean;
import com.thel.constants.TheLConstants;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.api.commonapi.CommonApiBusiness;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.Utils;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by waiarl on 2017/11/17.
 * 对日志点赞工具类
 */

public class MomentLikeUtils {
    private final String myUserId;
    private final WinkCommentBean mWinkCommentBean;
    private Fragment fragment;
    private Activity activity;
    private LikeStatusChangedReceiver receiver;


    public MomentLikeUtils() {
        myUserId = Utils.getMyUserId();

        mWinkCommentBean = new WinkCommentBean();
        mWinkCommentBean.avatar = ShareFileUtils.getString(ShareFileUtils.AVATAR, "");
        mWinkCommentBean.userId = myUserId;

    }

    /**
     * 注册
     *
     * @param fragment
     */
    public void registerReceiver(Fragment fragment) {
        this.fragment = fragment;
        receiver = new LikeStatusChangedReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(TheLConstants.BROADCAST_MOMENT_LIKE_STATUS_CHANGED);
        if (fragment != null && fragment.getActivity() != null) {
            fragment.getActivity().registerReceiver(receiver, filter);
        }
    }

    /**
     * 取消注册
     *
     * @param fragment
     */
    public void unRegisterReceiver(Fragment fragment) {
        if (fragment != null && fragment.getActivity() != null) {
            try {
                fragment.getActivity().unregisterReceiver(receiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 注册
     *
     * @param activity
     */
    public void registerReceiver(Activity activity) {
        this.activity = activity;
        IntentFilter filter = new IntentFilter();
        receiver = new LikeStatusChangedReceiver();
        filter.addAction(TheLConstants.BROADCAST_MOMENT_LIKE_STATUS_CHANGED);
        if (activity != null) {
            try {
                activity.registerReceiver(receiver, filter);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 取消注册
     *
     * @param activity
     */
    public void unRegisterReceiver(Activity activity) {
        if (activity != null) {
            try {
                activity.unregisterReceiver(receiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 日志点赞广播接收器
     */
    class LikeStatusChangedReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                final Bundle bundle = intent.getExtras();
                final String momentId = bundle.getString(TheLConstants.BUNDLE_KEY_MOMENT_ID);
                final boolean isWink = bundle.getBoolean(TheLConstants.BUNDLE_KEY_MOMENT_WINK);
                if (fragment != null && fragment instanceof MomentLikeStatusChangedListener) {
                    ((MomentLikeStatusChangedListener) fragment).onLikeStatusChanged(momentId, isWink, myUserId + "", mWinkCommentBean);
                }
                if (activity != null && activity instanceof MomentLikeStatusChangedListener) {
                    ((MomentLikeStatusChangedListener) activity).onLikeStatusChanged(momentId, isWink, myUserId + "", mWinkCommentBean);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 给一个日志点赞
     *
     * @param momentId
     */
    public static void likeMoment(final String momentId) {
        winkSuccess(momentId);
        if (TextUtils.isEmpty(momentId)) {
            return;
        }
        final Flowable<BaseDataBean> flowable = CommonApiBusiness.winkMoment(momentId);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
            @Override
            public void onNext(BaseDataBean data) {
                super.onNext(data);
                if (!hasErrorCode && data != null) {//代表请求成功
                    //                              winkSuccess(momentId);
                }

                Log.d("MomentLikeUtils", " onNext : ");
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);

                Log.d("MomentLikeUtils", " onError : " + t.getMessage());
            }

            @Override
            public void onComplete() {
                super.onComplete();
            }
        });
    }

    /**
     * 给一个日志取消点赞
     */
    public static void unLikeMoment(final String momentId) {
        unWinkSuccess(momentId);
        if (TextUtils.isEmpty(momentId)) {
            return;
        }
        final Flowable<BaseDataBean> flowable = CommonApiBusiness.unWinkMoment(momentId);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
            @Override
            public void onNext(BaseDataBean data) {
                super.onNext(data);
                if (!hasErrorCode && data != null) {//代表请求成功
//                    unWinkSuccess(momentId);
                }
            }

            @Override
            public void onComplete() {
                super.onComplete();
            }
        });
    }

    private static void winkSuccess(String momentId) {
        sendWinkMomentBroadcast(momentId, true);

    }

    private static void unWinkSuccess(String momentId) {
        sendWinkMomentBroadcast(momentId, false);
    }

    /**
     * 对日志点赞或者取消点赞
     *
     * @param momentId
     * @param like
     */
    public static void likeMoment(String momentId, boolean like) {
        if (like) {
            likeMoment(momentId);
        } else {
            unLikeMoment(momentId);
        }
    }

    /**
     * 发送对日志的点赞广播
     *
     * @param momentId
     * @param wink
     */
    public static void sendWinkMomentBroadcast(String momentId, boolean wink) {
        final Intent intent = new Intent();
        intent.setAction(TheLConstants.BROADCAST_MOMENT_LIKE_STATUS_CHANGED);
        final Bundle bundle = new Bundle();
        bundle.putString(TheLConstants.BUNDLE_KEY_MOMENT_ID, momentId);
        bundle.putBoolean(TheLConstants.BUNDLE_KEY_MOMENT_WINK, wink);
        //  bundle.putString(TheLConstants.BUNDLE_KEY_USER_ID, myUserId + "");
        // bundle.putSerializable(TheLConstants.BUNDLE_KEY_MOMENT_COMMENT_WINK_BEAN, mWinkCommentBean);
        intent.putExtras(bundle);
        TheLApp.getContext().sendBroadcast(intent);
    }

}
