package com.thel.imp.momentblack;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;

import com.google.gson.reflect.TypeToken;
import com.thel.app.TheLApp;
import com.thel.base.BaseDataBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.user.BlockBean;
import com.thel.constants.TheLConstants;
import com.thel.imp.black.BlackUtils;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestConstants;
import com.thel.network.api.commonapi.CommonApiBusiness;
import com.thel.network.service.DefaultRequestService;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;
import com.thel.utils.MD5Utils;
import com.thel.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by waiarl on 2017/11/14.
 * <p>
 * 屏蔽某篇日志或者屏蔽某人的全部日志工具类
 * 使用结果需要实现 MomentBlackListener
 */

public class MomentBlackUtils {
    private Fragment fragment;
    private Activity activity;
    private MomentBlackReceiver receiver;

    /**
     * 注册
     *
     * @param fragment
     */
    public void registerReceiver(Fragment fragment) {
        this.fragment = fragment;
        receiver = new MomentBlackReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(TheLConstants.BROADCAST_BLACK_ONE_MOMENT);
        filter.addAction(TheLConstants.BROADCAST_BLACK_ONE_USER_MOMENT);
        if (fragment != null && fragment.getActivity() != null) {
            fragment.getActivity().registerReceiver(receiver, filter);
        }
    }

    /**
     * 取消注册
     *
     * @param fragment
     */
    public void unRegisterReceiver(Fragment fragment) {
        if (fragment != null && fragment.getActivity() != null) {
            try {
                fragment.getActivity().unregisterReceiver(receiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 注册
     *
     * @param activity
     */
    public void registerReceiver(Activity activity) {
        this.activity = activity;
        IntentFilter filter = new IntentFilter();
        receiver = new MomentBlackReceiver();

        filter.addAction(TheLConstants.BROADCAST_BLACK_ONE_MOMENT);
        filter.addAction(TheLConstants.BROADCAST_BLACK_ONE_USER_MOMENT);
        if (activity != null) {
            try {
                activity.registerReceiver(receiver, filter);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 取消注册
     *
     * @param activity
     */
    public void unRegisterReceiver(Activity activity) {
        if (activity != null) {
            try {
                activity.unregisterReceiver(receiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * 屏蔽日志（单个或者某个人的全部）广播接收器
     */
    class MomentBlackReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {
                /**屏蔽单个日志**/
                case TheLConstants.BROADCAST_BLACK_ONE_MOMENT:
                    final Bundle bundle = intent.getExtras();
                    final String momentId = bundle.getString(TheLConstants.BUNDLE_KEY_MOMENT_ID);
                    if (!TextUtils.isEmpty(momentId)) {
                        if (fragment != null && fragment instanceof MomentBlackListener) {
                            ((MomentBlackListener) fragment).onBlackOneMoment(momentId);
                        }
                        if (activity != null && activity instanceof MomentBlackListener) {
                            ((MomentBlackListener) activity).onBlackOneMoment(momentId);
                        }
                    }
                    break;
                /**屏蔽莫个人的全部日志**/
                case TheLConstants.BROADCAST_BLACK_ONE_USER_MOMENT:
                    final Bundle bundle1 = intent.getExtras();
                    final String userId = bundle1.getString(TheLConstants.BUNDLE_KEY_USER_ID);
                    if (!TextUtils.isEmpty(userId)) {
                        if (fragment != null && fragment instanceof MomentBlackListener) {
                            ((MomentBlackListener) fragment).onBlackherMoment(userId);
                        }
                        if (activity != null && activity instanceof MomentBlackListener) {
                            ((MomentBlackListener) activity).onBlackherMoment(userId);
                        }
                    }
                    break;

            }

        }
    }
    /**************************************************一下为屏蔽单挑日志*****************************************************************************/
    /**
     * 网络请求屏蔽这条日志
     *
     * @param momentId
     */
    public static void blackThisMoment(final String momentId) {
        final Flowable<BaseDataBean> flowable = CommonApiBusiness.blackThisMoment(momentId);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
            @Override
            public void onNext(BaseDataBean data) {
                super.onNext(data);

                if (!hasErrorCode && data != null) {
                    blackThisMomentSuccess(momentId);
                }
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
            }
        });
    }

    public static void blackThisMoment(final String momentId, String reasonType) {
        final Flowable<BaseDataBean> flowable = CommonApiBusiness.blackThisMoment(momentId);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
            @Override
            public void onNext(BaseDataBean data) {
                super.onNext(data);


                if (!hasErrorCode && data != null) {
                    blackThisMomentSuccess(momentId);
                }
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
            }
        });
    }

    /**
     * 屏蔽单挑日志请求成功
     *
     * @param momentId
     */
    public static void blackThisMomentSuccess(String momentId) {

        saveOneBlackMoment(momentId);
        sendBlackMomentSuccessBroakcast(momentId);
    }

    /**
     * 发送屏蔽一条日志的广播
     *
     * @param momentId
     */
    public static void sendBlackMomentSuccessBroakcast(String momentId) {

        L.d("MomentBlackUtils", "-----------sendBlackMomentSuccessBroakcast-----------");

        final Intent intent = new Intent();
        intent.setAction(TheLConstants.BROADCAST_BLACK_ONE_MOMENT);
        final Bundle bundle = new Bundle();
        bundle.putString(TheLConstants.BUNDLE_KEY_MOMENT_ID, momentId);
        intent.putExtras(bundle);
        TheLApp.getContext().sendBroadcast(intent);
    }

    /**
     * sp中保存一条屏蔽的日志id
     *
     * @param momentsId
     */
    public static void saveOneBlackMoment(String momentsId) {

        final String hideMomentsList = SharedPrefUtils.getString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.HIDE_MOMENTS_LIST, "");

        if (TextUtils.isEmpty(hideMomentsList)) {
            SharedPrefUtils.setString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.HIDE_MOMENTS_LIST, momentsId);
        } else {
            SharedPrefUtils.setString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.HIDE_MOMENTS_LIST, hideMomentsList + "," + momentsId);
        }
    }

    /**************************************************一下为屏蔽单这个人的日志*****************************************************************************/
    public static void blackHerMoment(final String userId) {

        blackHerMomentSuccess(userId);

        Map<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_USERID, String.valueOf(userId));
        Flowable<BlockBean> floable = DefaultRequestService.createMomentRequestService().blockUserMoments(MD5Utils.generateSignatureForMap(data));
        floable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BlockBean>() {
            @Override
            public void onNext(BlockBean data) {
                super.onNext(data);
                if (!hasErrorCode && data != null) {
                    L.d("followfragment", "next: " + data.toString());
                }
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                L.d("followfragment", "error: " + t.getMessage());
            }
        });

    }

    /**
     * 忘了请求屏蔽一个用户的日志成功
     *
     * @param userId
     */
    private static void blackHerMomentSuccess(String userId) {
        savaOneBlackUserMoment(userId);
        sendBlackOneUserMoment(userId);
    }

    /**
     * 发送屏蔽某个人人全部日志成功的广播
     *
     * @param userId
     */
    public static void sendBlackOneUserMoment(String userId) {
        final Intent intent = new Intent();
        intent.setAction(TheLConstants.BROADCAST_BLACK_ONE_USER_MOMENT);
        final Bundle bundle = new Bundle();
        bundle.putString(TheLConstants.BUNDLE_KEY_USER_ID, userId + "");
        intent.putExtras(bundle);
        TheLApp.getContext().sendBroadcast(intent);
    }

    /**
     * sp 中保存被屏蔽所有日志的用户
     *
     * @param userId
     * @return
     */
    public static boolean savaOneBlackUserMoment(String userId) {
        if (TextUtils.isEmpty(userId)) {
            return false;
        }

        try {
            String hideMomentsUserList = SharedPrefUtils.getString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.HIDE_MOMENTS_USER_LIST, "[]");

            List<Long> userList = GsonUtils.getGson().fromJson(hideMomentsUserList, new TypeToken<List<Long>>() {
            }.getType());

            userList.add(Long.valueOf(userId));

            hideMomentsUserList = GsonUtils.createJsonString(userList);

            SharedPrefUtils.setString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.HIDE_MOMENTS_USER_LIST, hideMomentsUserList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    /**
     * 从日志屏蔽名单中移除
     *
     * @param userId
     */
    public static void removeOneBlackUserMoment(String userId) {
        if (TextUtils.isEmpty(userId)) {
            return;
        }

        String blockList = SharedPrefUtils.getString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.HIDE_MOMENTS_USER_LIST, "[]");

        L.d("isUserBlock", " blockList begin : " + blockList);

        if (blockList != null) {

            List<Long> userList = GsonUtils.getGson().fromJson(blockList, new TypeToken<List<Long>>() {
            }.getType());

            ListIterator<Long> listIterator = userList.listIterator();

            while (listIterator.hasNext()) {

                Long uid = listIterator.next();

                if (String.valueOf(uid).equals(userId)) {

                    listIterator.remove();

                }

            }

            blockList = GsonUtils.createJsonString(userList);

            L.d("isUserBlock", " blockList end : " + blockList);

            SharedPrefUtils.setString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.HIDE_MOMENTS_USER_LIST, blockList);

        }
    }

    /**
     * 查询是否这个人的全部日志被屏蔽
     *
     * @param userId
     * @return
     */
    public static boolean isMomentBlackUser(String userId) {
        if (TextUtils.isEmpty(userId)) {
            return false;
        }
        String hideMomentsUserList = SharedPrefUtils.getString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.HIDE_MOMENTS_USER_LIST, "");
        final List<String> blackList = Arrays.asList(hideMomentsUserList.split(","));
        return blackList.contains(userId);
    }

    /**
     * 保存被屏蔽全部日志的用户列表
     */
    public static void saveBlackMomentUsersList(List<String> userList) {
        if (userList == null || userList.isEmpty()) {
            SharedPrefUtils.setString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.HIDE_MOMENTS_USER_LIST, "");
            return;
        }
        final StringBuilder sb = new StringBuilder();
        for (String userId : userList) {
            sb.append(",").append(userId);
        }
        SharedPrefUtils.setString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.HIDE_MOMENTS_USER_LIST, sb.toString());
    }

    /**
     * 添加被屏蔽全部日志的用户列表
     */
    public static void addBlackMomentUsersList(List<String> userList) {
        if (userList == null || userList.isEmpty()) {
            return;
        }
        String hideMomentsUserList = SharedPrefUtils.getString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.HIDE_MOMENTS_USER_LIST, "");
        final List<String> blackList = Arrays.asList(hideMomentsUserList.split(","));

        for (String userId : userList) {
            if (!blackList.contains(userId)) {
                blackList.add(userId);
            }
        }
        final StringBuilder sb = new StringBuilder();
        for (String userId : blackList) {
            sb.append(",").append(userId);
        }
        SharedPrefUtils.setString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.HIDE_MOMENTS_USER_LIST, sb.toString());
    }

    public static void filtNearbyMoment(List<MomentsBean> momentsList) {
        //过滤黑名单和被屏蔽
        if (momentsList == null || momentsList.isEmpty()) {
            return;
        }
        // List<String> blackList = Arrays.asList(SharedPrefUtils.getString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.BLACK_LIST, "").split(","));
        List<String> blackList = BlackUtils.getBlackList();

        List<String> trimBlacklist = new ArrayList<>(blackList);
        trimBlacklist = removeEmptyElem(trimBlacklist);
        List<String> reportUserList = Arrays.asList(SharedPrefUtils.getString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.REPORT_USER_LIST, "").split(","));
        List<String> trimReportUserList = new ArrayList<>(blackList);
        trimReportUserList = removeEmptyElem(trimReportUserList);
        List<String> blockMeList = BlackUtils.getblockMeList();
        List<String> trimBlockMeUserList = new ArrayList<>(blackList);
        trimBlockMeUserList = removeEmptyElem(blockMeList);
        List<String> blockUsers = new ArrayList<String>();
        if (trimBlacklist.size() > 0 && !TextUtils.isEmpty(trimBlacklist.get(0))) {
            blockUsers.addAll(trimBlacklist);
        }
        if (trimReportUserList.size() > 0 && !TextUtils.isEmpty(trimReportUserList.get(0))) {
            blockUsers.addAll(trimReportUserList);
        }
        if (trimBlockMeUserList.size() > 0 && !TextUtils.isEmpty(trimBlockMeUserList.get(0))) {
            blockUsers.addAll(trimBlockMeUserList);
        }
        if (blockUsers.size() > 0) {
            List<MomentsBean> blockUserList = new ArrayList<MomentsBean>();
            for (MomentsBean momentsBean : momentsList) {
                for (String userId : blockUsers) {
                    if (userId.equals(momentsBean.userId + "")) {
                        blockUserList.add(momentsBean);
                        break;
                    }
                }
            }
            momentsList.removeAll(blockUserList);
        }

    }

    private static List<String> removeEmptyElem(List<String> processList) {
        List<String> list = new ArrayList<String>();
        list.add("");
        processList.removeAll(list);
        return processList;
    }

}
