package com.thel.imp.sticker;

import java.util.List;

/**
 * Created by waiarl on 2017/11/18.
 * 表情接口类
 */

public class StickerContract {

    public interface GetStickerListCallback<T> {
        /**
         * 获取表情列表回调
         *
         * @param stickerList
         */
        void getStickerList(List<T> stickerList);
    }

    public interface SaveStickerCallBack {
        /**
         * 保存表情到文件成功
         */
        void saveStickerSuccess();
    }

    /**
     * 表情包更改
     */
    public interface StickerChangedListener {
        /**
         * 表情包更改
         */
        void onStickerChanged();
    }
}
