package com.thel.imp.friend;

import java.util.List;

/**
 * Created by waiarl on 2017/11/25.
 * 好友列表改变监听
 */

public class FriendListContract {
    /**
     * 好友列改变接口
     */
    public interface FriendListChangedListener {
        /**
         * 好友列表改变接口
         *
         * @param isAdd  是否是添加
         * @param userId 改变的userId
         */
        void onFriendListChanged(boolean isAdd, String userId);
    }

    /**
     * 好友列表从网络获取
     * (好友列表数据刷新)
     */
    public interface FriendListRefreshCallBack {

        void onFriendListRefresh(List<String> userList);
    }

    /**
     * 好友列表数据库数据改变接口
     */
    public interface FriendListTableChangedListener {
        /**
         * 好友列表数据库刷新接口
         */
        void onFriendListTableChanged();
    }
}
