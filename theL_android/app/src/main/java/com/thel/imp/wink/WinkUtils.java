package com.thel.imp.wink;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseDataBean;
import com.thel.constants.TheLConstants;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.api.commonapi.CommonApiBusiness;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SharedPrefUtils;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by waiarl on 2017/7/19.
 */

public class WinkUtils {


    /**
     * 挤眼的五种类型
     */
    public static final String WINK_TYPE_WINK = "wink";
    public static final String WINK_TYPE_KISS = "kiss";
    public static final String WINK_TYPE_DIAMOND = "diamond";
    public static final String WINK_TYPE_FLOWER = "flower";
    public static final String WINK_TYPE_LOVE = "love";

    /**
     * 发送挤眼所对应的请求int 类型值
     */
    public static Map<String, String> WINK_TYPE = new HashMap<String, String>() {
        {
            put(WINK_TYPE_WINK, 0 + "");
            put(WINK_TYPE_FLOWER, 5 + "");
            put(WINK_TYPE_KISS, 6 + "");
            put(WINK_TYPE_LOVE, 7 + "");
            put(WINK_TYPE_DIAMOND, 8 + "");
        }
    };

    private static void sendWinkUserChangedBroadCast(String userId) {
        final Intent intent = new Intent();
        intent.setAction(TheLConstants.BROADCAST_WINK_USER_CHANGED);
        intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, userId);
        TheLApp.context.sendBroadcast(intent);
    }


    /**
     /**************************自己挤过眼的人的一些公共方法************************************/
    /**
     * 添加被我挤过眼的人
     * 由于挤眼一天只能机一次，所有只有添加，没有去除方法
     *
     * @param userId
     */
    public static void addMyTodayWinkUser(String userId) {
        if (TextUtils.isEmpty(userId)) {
            return;
        }
        final String id = "[" + userId + "]";
        String userIds = getTodayMyWinkUsers();
        if (!userIds.contains(id)) {
            userIds = userIds + id;
            SharedPrefUtils.setString(SharedPrefUtils.WINK_FILE_CACHE, getTodayWinkSpName(), userIds);
            sendWinkUserChangedBroadCast(userId);
        }
    }

    /**
     * 是否今天被我挤过眼
     *
     * @param userId
     * @return
     */
    public static boolean isTodayWinkedUser(String userId) {
        if (TextUtils.isEmpty(userId)) {
            return false;
        }
        return getTodayMyWinkUsers().contains(userId);
    }

    /**
     * 今天挤眼的缓存文件名
     *
     * @return
     */
    public static String getTodayWinkSpName() {
        final String myUserId = ShareFileUtils.getString(ShareFileUtils.ID, "");
        final String date = new SimpleDateFormat("yyyy-MM-DD").format(System.currentTimeMillis());
        return myUserId + "_" + date;
    }

    /**
     * 获取今天被我挤过眼的人
     *
     * @return
     */
    public static String getTodayMyWinkUsers() {
        final String winkUsers = SharedPrefUtils.getString(SharedPrefUtils.WINK_FILE_CACHE, getTodayWinkSpName(), "");
        L.d("refresh", "todayWinkUsers=" + winkUsers);
        return winkUsers;
    }

    /*******************************注册广播以及实现*************************************************/

    private Fragment fragment;
    private Activity activity;
    private WinkStatusChangedReceiver receiver;

    public void registerReceiver(Fragment fragment) {
        this.fragment = fragment;
        receiver = new WinkStatusChangedReceiver();
        IntentFilter filter = new IntentFilter(TheLConstants.BROADCAST_WINK_USER_CHANGED);
        if (fragment != null && fragment.getActivity() != null) {
            fragment.getActivity().registerReceiver(receiver, filter);
        }
    }

    public void unRegisterReceiver(Fragment fragment) {
        if (fragment != null && fragment.getActivity() != null) {
            try {
                fragment.getActivity().unregisterReceiver(receiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void registerReceiver(Activity activity) {
        this.activity = activity;
        receiver = new WinkStatusChangedReceiver();
        IntentFilter filter = new IntentFilter(TheLConstants.BROADCAST_WINK_USER_CHANGED);
        if (activity != null) {
            try {
                activity.registerReceiver(receiver, filter);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void unRegisterReceiver(Activity activity) {
        if (activity != null) {
            try {
                activity.unregisterReceiver(receiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 发送挤眼
     *
     * @param userId
     * @param type   五种类型，非int值
     */
    public static void sendWink(final String userId, final String type, final String nickname, final String avatar) {

        final String winkType = WINK_TYPE.get(type);
        final Flowable<BaseDataBean> flowable = CommonApiBusiness.sendWink(userId, winkType);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
            @Override
            public void onNext(BaseDataBean data) {
                super.onNext(data);
                winkSuccess(userId, type);
                // 发送一条消息
//
//                MsgBean msgBean = MsgUtils.createMsgBean(MsgBean.MSG_TYPE_WINK, TheLConstants.WINK_TYPE_WINK, MsgBean.IS_WINK_MSG, userId, nickname, avatar);
//
//                if (msgBean != null && msgBean.toUserId != null) {
//
//                    msgBean.msgStatus = TheLConstants.MsgSendingStatusConstants.MSG_SUCCESS;
//
//                    ChatServiceManager.getInstance().winkMsgSaveToDB(msgBean);
//                    ChatServiceManager.getInstance().insertChatTable(msgBean, DBUtils.getChatTableName(msgBean));
//                }
            }
        });
    }


    /**
     * 发送挤眼
     *
     * @param userId
     */
    public static void sendUserCardWink(final String userId, final String type) {

        final String winkType = WINK_TYPE.get(type);
        final Flowable<BaseDataBean> flowable = CommonApiBusiness.sendWink(userId, winkType);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
            @Override
            public void onNext(BaseDataBean data) {
                super.onNext(data);
                winkUserCardSuccess(userId, type, "UserCard");

            }
        });
    }

    private static void winkUserCardSuccess(String userId, String type, String userCard) {
        giftWinkSuccess(userId, type);

    }

    /**
     * 挤眼成功
     *
     * @param userId
     * @param type   五种类型
     */
    private static void winkSuccess(String userId, String type) {
        if (WINK_TYPE_WINK.equals(type)) {
            addMyTodayWinkUser(userId);
        } else {
            giftWinkSuccess(userId, type);
        }
    }

    /**
     * 礼物挤眼广播
     *
     * @param userId
     * @param type
     */
    private static void giftWinkSuccess(String userId, String type) {
        final Intent intent = new Intent();
        intent.setAction(TheLConstants.BROADCAST_GIFT_WINK_SUCCESS);
        final Bundle bundle = new Bundle();
        bundle.putString(TheLConstants.BUNDLE_KEY_USER_ID, userId);
        bundle.putString(TheLConstants.BUNDLE_KEY_ACTION_TYPE, type);
        intent.putExtras(bundle);
        TheLApp.getContext().sendBroadcast(intent);
    }

    /**
     * 挤眼接收器（普通挤眼）
     */
    class WinkStatusChangedReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            final String userId = intent.getStringExtra(TheLConstants.BUNDLE_KEY_USER_ID);
            if (!TextUtils.isEmpty(userId)) {
                if (fragment != null && fragment instanceof WinkStatusChangedListener) {
                    ((WinkStatusChangedListener) fragment).onWinkSucceess(userId, WINK_TYPE_WINK);
                }
                if (activity != null && activity instanceof WinkStatusChangedListener) {
                    ((WinkStatusChangedListener) activity).onWinkSucceess(userId, WINK_TYPE_WINK);
                }
            }
        }
    }

    public static void playWinkSound() {

        final MediaPlayer mMediaPlayer = MediaPlayer.create(TheLApp.context, R.raw.sound_wink);

        if (mMediaPlayer != null) {

            mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    if (mMediaPlayer != null) {
                        mMediaPlayer.stop();
                        mMediaPlayer.release();
                    }
                }
            });
            mMediaPlayer.start();
        }

    }

}
