package com.thel.imp.picupload;

import android.text.TextUtils;
import android.util.SparseArray;

import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UploadManager;
import com.thel.bean.user.UploadTokenBean;
import com.thel.modules.main.home.moments.PicBean;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.RequestConstants;
import com.thel.utils.L;
import com.thel.utils.MD5Utils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.Utils;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by waiarl on 2017/11/21.
 */

public class PicUploadUtils {
    private String TAG = "PicUploadUtils";

    /**
     * 上传图片或者视频
     *
     * @param list
     * @param callback
     */
    public static void uploadPic(List<PicBean> list, PicUploadContract.PicUploadCallback callback, boolean isReleaseVideoMoment) {
        if (list == null || list.isEmpty()) {
            callback.uploadComplete("");
            return;
        }
        final SparseArray<String> uploadImgesUrlMap = new SparseArray<>();
        StringBuilder picsUrlSB = new StringBuilder();
        for (PicBean pic : list) {
            if (!pic.isSelectBtn && !TextUtils.isEmpty(pic.localUrl)) {
                picsUrlSB.append(pic.localUrl);
                picsUrlSB.append(",");
            }
        }
        final String[] pics = picsUrlSB.deleteCharAt(picsUrlSB.length() - 1).toString().split(",");
        final int size = pics.length;
        final PicUploadContract.PicUploadStatusCallback statusCallback = new MyPicUploadStatusCallback(size);
        for (int i = 0; i < size; i++) {
            final String fileType = pics[i].substring(pics[i].lastIndexOf("."));
            final String uploadPath = RequestConstants.UPLOAD_FILE_ROOT_PATH_TIMELINE + ShareFileUtils.getString(ShareFileUtils.ID, "") + "/" + MD5Utils.calculateMD5(new File(pics[i])) + fileType;
            final int index = i;
            // 如果是上传视频的话要穿『桶』名
            RequestBusiness.getInstance()
                    .getUploadToken(System.currentTimeMillis() + "", ".mp4".equals(fileType) ? RequestConstants.QINIU_BUCKET_VIDEO : "", uploadPath, isReleaseVideoMoment)
                    .onBackpressureDrop().subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new MyConsumer(uploadPath, pics[i], uploadImgesUrlMap, callback, statusCallback, index, size));
        }

    }

    /**
     * 上传要固定图片地址的本地图片
     *
     * @param localPath  图片地址
     * @param uploadPath 图片Url
     * @param callback
     */
    public static void uploadPic(String localPath, String uploadPath, PicUploadContract.PicUploadCallback callback, boolean isReleaseVideoMoment) {
        try {
            if (TextUtils.isEmpty(localPath) && TextUtils.isEmpty(uploadPath)) {//同时为空则上传成功
                callback.uploadComplete(uploadPath);
            } else if (TextUtils.isEmpty(localPath) || TextUtils.isEmpty(uploadPath)) {//不同时为空上传失败
                callback.uploadFailed(0);
            }
            final SparseArray<String> uploadImgesUrlMap = new SparseArray<>();
            final PicUploadContract.PicUploadStatusCallback statusCallback = new MyPicUploadStatusCallback(1);
            final String fileType = localPath.substring(localPath.lastIndexOf("."));

            String type = "";
            if (localPath.endsWith(".mp4")) {
                type = RequestConstants.QINIU_BUCKET_VIDEO;
            }

            RequestBusiness.getInstance()
                    .getUploadToken(System.currentTimeMillis() + "", type, uploadPath, isReleaseVideoMoment)
                    .observeOn(AndroidSchedulers.mainThread())
                    .onBackpressureDrop().subscribeOn(Schedulers.io())
                    .subscribe(new MyConsumer(uploadPath, localPath, uploadImgesUrlMap, callback, statusCallback, 0, 1));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 上传图片或者视频
     *
     * @param unUploadImageUrls
     * @param callback
     */
    public static void uploadPic(String unUploadImageUrls, PicUploadContract.PicUploadCallback callback, boolean isReleaseVideoMoment) {
        if (TextUtils.isEmpty(unUploadImageUrls)) {
            callback.uploadComplete("");
            return;
        }
        final String[] arr = unUploadImageUrls.split(",");
        final int size = arr.length;
        if (size == 1 && TextUtils.isEmpty(arr[0])) {
            callback.uploadComplete("");
            return;
        }
        final List<PicBean> picList = new ArrayList<>();
        PicBean bean;
        for (String pic : arr) {
            bean = new PicBean();
            bean.localUrl = pic;
            picList.add(bean);
        }
        uploadPic(picList, callback, isReleaseVideoMoment);
    }


    static class MyConsumer extends InterceptorSubscribe<UploadTokenBean> {

        private final PicUploadContract.PicUploadCallback callback;
        private final SparseArray<String> uploadImgesUrlMap;
        private final PicUploadContract.PicUploadStatusCallback statucCallback;
        private final int index;
        private final int size;
        String picUploadPath;

        String picLocalPath;

        public MyConsumer(String picUploadPath, String picLocalPath, SparseArray<String> uploadImgesUrlMap, PicUploadContract.PicUploadCallback callback, PicUploadContract.PicUploadStatusCallback statusCallback, int index, int size) {
            this.picUploadPath = picUploadPath;
            this.picLocalPath = picLocalPath;
            this.callback = callback;
            this.uploadImgesUrlMap = uploadImgesUrlMap;
            this.statucCallback = statusCallback;
            this.index = index;
            this.size = size;
        }

        @Override
        public void onNext(UploadTokenBean uploadTokenBean) {
            super.onNext(uploadTokenBean);
            if (!TextUtils.isEmpty(uploadTokenBean.data.uploadToken)) {
                if (!TextUtils.isEmpty(picLocalPath) && !TextUtils.isEmpty(picUploadPath)) {
                    UploadManager uploadManager = new UploadManager();
                    uploadManager.put(new File(picLocalPath), picUploadPath, uploadTokenBean.data.uploadToken, new UpCompletionHandler() {
                        @Override
                        public void complete(String key, ResponseInfo info, JSONObject response) {

                            if (info != null && info.statusCode == 200 && picUploadPath.equals(key)) {
                                statucCallback.reduceUnUploadCount();//总未上传数减1
                                if (key.endsWith(".mp4")) {//如果是视频
                                    final String videoUrl = RequestConstants.VIDEO_BUCKET + key;
                                    final String webpUrl = RequestConstants.FILE_BUCKET + Utils.getWebpPathByVideoPath(key);
                                    callback.uploadVideoComplete(videoUrl, webpUrl);
                                } else {
                                    uploadImgesUrlMap.put(index, RequestConstants.FILE_BUCKET + key);
                                }
                                if (statucCallback.getUnUploadCount() == 0) {//说明全部上传成功
                                    final String imageUrls = getImageUrls(uploadImgesUrlMap, callback, size);
                                    if (!TextUtils.isEmpty(imageUrls)) {
                                        callback.uploadComplete(imageUrls);
                                    }
                                }
                            } else {
                                if (key.endsWith(".mp4")) {//如果视频上传失败
                                    callback.uploadVideoComplete("", "");
                                }
                                callback.uploadFailed(index);
                            }
                        }
                    }, null);
                } else {
                    callback.uploadFailed(index);
                }
            } else {
                callback.uploadFailed(index);
            }
        }
    }

    /**
     * 按顺序返回最后的Imageurls
     *
     * @param array
     * @param callback
     * @param size
     * @return
     */
    public static String getImageUrls(SparseArray<String> array, PicUploadContract.PicUploadCallback callback, int size) {
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < size; i++) {
            if (TextUtils.isEmpty(array.get(i))) {
                L.i("TAG", "uploadFaied,maybeVideo:index=" + i);
            } else {
                sb.append(array.get(i)).append(",");
            }
        }
        return sb.deleteCharAt(sb.length() - 1).toString();
    }

    static class MyPicUploadStatusCallback implements PicUploadContract.PicUploadStatusCallback {
        private int unUploadCount = 0;

        public MyPicUploadStatusCallback(int count) {
            unUploadCount = count;
        }

        @Override
        public void setUnUploadCount(int count) {
            this.unUploadCount = count;
        }

        @Override
        public int getUnUploadCount() {
            return unUploadCount;
        }

        @Override
        public int reduceUnUploadCount() {
            unUploadCount -= 1;
            return unUploadCount;
        }
    }

}
