package com.thel.imp.vip;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.fragment.app.Fragment;

import com.thel.app.TheLApp;
import com.thel.bean.user.VipConfigBean;
import com.thel.constants.TheLConstants;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.service.DefaultRequestService;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;
import com.thel.utils.UserUtils;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by waiarl on 2017/11/18.
 * 会员状态改变监听广播
 */

public class VipUtils {
    private Fragment fragment;
    private Activity activity;
    VipStatusChangedReceiver receiver;

    /**
     * 注册
     *
     * @param fragment
     */
    public void registerReceiver(Fragment fragment) {
        this.fragment = fragment;
        receiver = new VipStatusChangedReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(TheLConstants.BROADCAST_VIP_STATUS_CHANGED);
        if (fragment != null && fragment.getActivity() != null) {
            fragment.getActivity().registerReceiver(receiver, filter);
        }
    }

    /**
     * 取消注册
     *
     * @param fragment
     */
    public void unRegisterReceiver(Fragment fragment) {
        if (fragment != null && fragment.getActivity() != null) {
            try {
                fragment.getActivity().unregisterReceiver(receiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 注册
     *
     * @param activity
     */
    public void registerReceiver(Activity activity) {
        this.activity = activity;
        IntentFilter filter = new IntentFilter();
        receiver = new VipStatusChangedReceiver();
        filter.addAction(TheLConstants.BROADCAST_VIP_STATUS_CHANGED);
        if (activity != null) {
            try {
                activity.registerReceiver(receiver, filter);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 取消注册
     *
     * @param activity
     */
    public void unRegisterReceiver(Activity activity) {
        if (activity != null) {
            try {
                activity.unregisterReceiver(receiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 购买了vip以后调用的方法，此时肯定是vip，最少会员等级为1
     */
    public static void hasBoughtVip() {

        UserUtils.setUserVipLevel(1);

        L.d("showWhoSeenMeData", " hasBoughtVip  UserUtils.getUserVipLevel() : " + UserUtils.getUserVipLevel());

        sendVipStatusChangedBroad(true, 1);

        getVipConfig();
    }

    private static void getVipConfig() {
        final Flowable<VipConfigBean> flowable = DefaultRequestService.createCommonRequestService().getVipConfig();
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new InterceptorSubscribe<VipConfigBean>() {
                    @Override
                    public void onNext(VipConfigBean data) {
                        super.onNext(data);

                        L.d("showWhoSeenMeData", " getVipConfig data : " + GsonUtils.createJsonString(data));

                        if (!hasErrorCode && data != null && data.data != null && data.data.vipSetting != null) {
                            UserUtils.setUserVipLevel(data.data.vipSetting.level);
                        }
                    }
                });
    }


    class VipStatusChangedReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            final Bundle bundle = intent.getExtras();
            if (bundle != null) {
                final boolean isVip = bundle.getBoolean(TheLConstants.BUNDLE_KEY_IS_VIP, false);
                final int level = bundle.getInt(TheLConstants.BUNDLE_KEY_VIP_LEVEL, 0);
                if (fragment != null && fragment instanceof VipStatusChangerListener) {
                    ((VipStatusChangerListener) fragment).onVipStatusChanged(isVip, level);
                }
                if (activity != null && activity instanceof VipStatusChangerListener) {
                    ((VipStatusChangerListener) activity).onVipStatusChanged(isVip, level);
                }
            }
        }
    }

    /**
     * 发送会员广播
     *
     * @param
     */
    public static void sendVipStatusChangedBroad(boolean isVip, int level) {
        final Intent intent = new Intent();
        intent.setAction(TheLConstants.BROADCAST_VIP_STATUS_CHANGED);
        final Bundle bundle = new Bundle();
        bundle.putBoolean(TheLConstants.BUNDLE_KEY_IS_VIP, isVip);
        bundle.putInt(TheLConstants.BUNDLE_KEY_VIP_LEVEL, level);
        intent.putExtras(bundle);
        TheLApp.getContext().sendBroadcast(intent);
    }

}
