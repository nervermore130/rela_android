package com.thel.imp.black;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.TextUtils;

import com.google.gson.reflect.TypeToken;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseDataBean;
import com.thel.bean.BlackListBean;
import com.thel.bean.BlackListNetBean;
import com.thel.bean.user.BlockBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.RfSBridgeHandlerFactory;
import com.thel.modules.main.messages.bean.MsgBean;
import com.thel.modules.main.userinfo.BlockNetBean;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.api.commonapi.CommonApiBusiness;
import com.thel.network.service.DefaultRequestService;
import com.thel.utils.DialogUtil;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by waiarl on 2017/10/20.
 * 黑名单工具类
 */

public class BlackUtils {
    private Fragment fragment;
    private Activity activity;
    private BlackUsersChangedReceiver receiver;

    /**
     * 是否屏蔽了我
     *
     * @param userId
     * @return
     */
    public static boolean isBlockMe(String userId) {
        if (TextUtils.isEmpty(userId)) {
            return false;
        }

        String blockMeList = ShareFileUtils.getString(ShareFileUtils.BLOCK_ME_LIST, "");
        try {
            if (blockMeList != null) {
                List<Long> userList = GsonUtils.getGson().fromJson(blockMeList, new TypeToken<List<Long>>() {
                }.getType());
                if (userList != null && userList.size() > 0) {
                    for (Long userIdDouble : userList) {
                        if (String.valueOf(userIdDouble).equals(userId)) {
                            return true;
                        }
                    }
                    return false;
                } else {
                    return false;
                }
            }
        } catch (Exception e) {

            e.printStackTrace();
        }

        return false;
    }

    /**
     * 是否在我的黑名单（被我屏蔽）
     *
     * @param userId
     * @return
     */
    public static boolean isBlack(String userId) {

        try {
            String blackList = SharedPrefUtils.getString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.BLACK_LIST, "[]");

            if (blackList != null) {
                List<Long> userList = GsonUtils.getGson().fromJson(blackList, new TypeToken<List<Long>>() {
                }.getType());
                if (userList != null && userList.size() > 0) {
                    for (Long userIdDouble : userList) {
                        if (String.valueOf(userIdDouble).equals(userId)) {
                            return true;
                        }
                    }
                    return false;
                } else {
                    return false;
                }
            }
        } catch (Exception e) {

            L.d("UserInfoBlack", " Exception : " + e.getMessage());

            e.printStackTrace();
        }

        L.d("UserInfoBlack", " end : ");

        return false;
    }

    public static boolean isUserBlock(String userId) {

        try {
            String blockList = SharedPrefUtils.getString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.HIDE_MOMENTS_USER_LIST, "[]");

            L.d("isUserBlock", " blockList : " + blockList);

            if (blockList != null) {

                List<Long> userList = GsonUtils.getGson().fromJson(blockList, new TypeToken<List<Long>>() {
                }.getType());

                L.d("isUserBlock", " userList : " + userList);

                if (userList != null && userList.size() > 0) {
                    for (Long userIdDouble : userList) {

                        if (String.valueOf(userIdDouble).equals(userId)) {
                            return true;
                        }
                    }
                    return false;
                } else {
                    return false;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean isMomentBlock(String momentId) {

        try {

            String hideMomentsList = SharedPrefUtils.getString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.HIDE_MOMENTS_LIST, "");

            L.d("BlackUtils", " hideMomentsList : " + hideMomentsList);

            hideMomentsList = "[" + hideMomentsList + "]";

            List<Long> userList = GsonUtils.getGson().fromJson(hideMomentsList, new TypeToken<List<Long>>() {
            }.getType());

            if (userList != null && userList.size() > 0) {
                for (Long userIdDouble : userList) {

                    if (String.valueOf(userIdDouble).equals(momentId)) {
                        return true;
                    }
                }
                return false;
            } else {
                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    /***
     * 返回我的黑名單集合
     * */
    public static List<String> getBlackList() {
        String blackString = SharedPrefUtils.getString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.BLACK_LIST, "");
        L.d("BlackUtils", " blackString : " + blackString);
        List<String> blackList = new ArrayList<>(Arrays.asList(blackString.split(",")));
        return blackList;
    }

    /**
     * 保存一个屏蔽我的名单
     *
     * @param userId
     */
    public static void saveOneBlockMeUser(String userId) {
        if (TextUtils.isEmpty(userId)) {
            return;
        }
        String blockMeList = ShareFileUtils.getString(ShareFileUtils.BLOCK_ME_LIST, "");
        final String id = "[" + userId + "]";
        if (!blockMeList.contains(id)) {
            blockMeList += id;
            ShareFileUtils.setString(ShareFileUtils.BLOCK_ME_LIST, blockMeList);
        }
    }

    /**
     * 获取被屏蔽人的名单列表
     *
     * @return
     */
    public static List<String> getblockMeList() {
        String blockMeList = ShareFileUtils.getString(ShareFileUtils.BLOCK_ME_LIST, "");
        final String st1 = blockMeList.replace("[", ",");
        final String st2 = st1.replace("\\[", "");
        return new ArrayList<>(Arrays.asList(st2.split(",")));
    }


    /**
     * 保存一个我屏蔽的人
     *
     * @param userId
     */
    public static void saveOneBlackUser(String userId) {
        try {
            if (TextUtils.isEmpty(userId)) {
                return;
            }
            String blackList = SharedPrefUtils.getString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.BLACK_LIST, "[]");

            L.d("BlackUtils", " blackList : " + blackList);

            if (!TextUtils.isEmpty(blackList) && !isBlack(userId)) {

                List<Long> userList = GsonUtils.getGson().fromJson(blackList, new TypeToken<List<Long>>() {
                }.getType());

                userList.add(Long.valueOf(userId));

                blackList = GsonUtils.createJsonString(userList);

                SharedPrefUtils.setString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.BLACK_LIST, blackList);

                sendBlackOneUser(userId + "", true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 是否被我屏蔽或者屏蔽了我
     *
     * @param userId
     * @return
     */
    public static boolean isBlackOrBlock(String userId) {

        if (isBlack(userId)) {
            DialogUtil.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.info_had_add_to_black));
            return true;
        }
        if (isBlockMe(userId)) {
            DialogUtil.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.info_had_bean_added_to_black));
            return true;
        }
        return false;
    }

    /**
     * 注册
     *
     * @param fragment
     */
    public void registerReceiver(Fragment fragment) {
        this.fragment = fragment;
        receiver = new BlackUsersChangedReceiver();
        IntentFilter filter = new IntentFilter(TheLConstants.BROADCAST_BLACK_USERS_CHANGED);
        if (fragment != null && fragment.getActivity() != null) {
            fragment.getActivity().registerReceiver(receiver, filter);
        }
    }

    /**
     * 取消注册
     *
     * @param fragment
     */
    public void unRegisterReceiver(Fragment fragment) {
        if (fragment != null && fragment.getActivity() != null) {
            try {
                fragment.getActivity().unregisterReceiver(receiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 注册
     *
     * @param activity
     */
    public void registerReceiver(Activity activity) {
        this.activity = activity;
        receiver = new BlackUsersChangedReceiver();
        IntentFilter filter = new IntentFilter(TheLConstants.BROADCAST_BLACK_USERS_CHANGED);
        if (activity != null) {
            try {
                activity.registerReceiver(receiver, filter);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 取消注册
     *
     * @param activity
     */
    public void unRegisterReceiver(Activity activity) {
        if (activity != null) {
            try {
                activity.unregisterReceiver(receiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void saveOneNetBlacker(String userId) {

        L.d("BlackUtils", " userId : " + userId);

        final Flowable<BlockNetBean> flowable = CommonApiBusiness.pullUserIntoBlackList(userId);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BlockNetBean>() {
            @Override
            public void onNext(BlockNetBean data) {
                super.onNext(data);

                if (!hasErrorCode && data != null && data.data != null) {

                    L.d("BlackUtils", " onNext : " + data.data.toString());

                    blackOneSuccess(data.data);
                }
            }
        });
    }

    public static void removeOneNetBlacker(final String userId) {
        final Flowable<BaseDataBean> flowable = CommonApiBusiness.removeFromBlackList(userId);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
            @Override
            public void onNext(BaseDataBean data) {
                super.onNext(data);
                if (!hasErrorCode && data != null) {
                    removeOneBlackSuccess(userId);
                }
            }

            @Override
            public void onComplete() {
                super.onComplete();

            }
        });
    }

    private static void removeOneBlackSuccess(String userId) {
        removeOneBlackUser(userId);
    }


    private static void blackOneSuccess(BlockBean bean) {
        saveOneBlackUser(bean.userId + "");
    }

    /**
     * 发送屏蔽一个人的广播
     *
     * @param userId
     * @param isAdd
     */
    public static void sendBlackOneUser(String userId, boolean isAdd) {
        final Intent intent = new Intent();
        intent.setAction(TheLConstants.BROADCAST_BLACK_USERS_CHANGED);
        final Bundle bundle = new Bundle();
        bundle.putString(TheLConstants.BUNDLE_KEY_USER_ID, userId);
        bundle.putBoolean(TheLConstants.BUNDLE_KEY_BLACKUSER_ADD, isAdd);
        intent.putExtras(bundle);
        TheLApp.getContext().sendBroadcast(intent);
    }

    /**
     * 移除一个黑名单（网络请求成功以后本地保持）
     *
     * @param userId
     */
    public static void removeOneBlackUser(String userId) {
        if (TextUtils.isEmpty(userId)) {
            return;
        }

        String blackList = SharedPrefUtils.getString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.BLACK_LIST, "[]");

        L.d("BlackUtils", " blackList : " + blackList);

        if (blackList != null) {

            List<Long> userList = GsonUtils.getGson().fromJson(blackList, new TypeToken<List<Long>>() {
            }.getType());

            if (userList != null) {

                ListIterator<Long> iterator = userList.listIterator();

                while (iterator.hasNext()) {

                    Long uid = iterator.next();

                    if (String.valueOf(uid).equals(userId)) {
                        iterator.remove();
                    }
                }

            }

            SharedPrefUtils.setString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.BLACK_LIST, GsonUtils.createJsonString(userList));

            sendBlackOneUser(userId, false);

        }

    }

    /**
     * 保存黑名单列表
     *
     * @param blackList
     */
    public static void saveBlackUserList(List<String> blackList) {
        final StringBuilder sb = new StringBuilder();
        for (String userId : blackList) {
            sb.append(",").append(userId);
        }
        SharedPrefUtils.setString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.BLACK_LIST, sb.toString());

    }

    /**
     * 屏蔽广播接收器
     */
    class BlackUsersChangedReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            final String userId = intent.getStringExtra(TheLConstants.BUNDLE_KEY_USER_ID);
            final boolean isAdd = intent.getBooleanExtra(TheLConstants.BUNDLE_KEY_BLACKUSER_ADD, false);
            if (!TextUtils.isEmpty(userId)) {
                if (fragment != null && fragment instanceof BlackUsersChangedListener) {
                    ((BlackUsersChangedListener) fragment).onBlackUsersChanged(userId, isAdd);
                }
                if (activity != null && activity instanceof BlackUsersChangedListener) {
                    ((BlackUsersChangedListener) activity).onBlackUsersChanged(userId, isAdd);
                }
            }
        }
    }

    /**
     * 获取网络黑名单数据包括日志屏蔽名单，黑名单以及屏蔽我的名单
     */
    public static void getNetBlackList() {
        final Flowable<BlackListNetBean> flowable = DefaultRequestService.createCommonRequestService().getBlackNetList();
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BlackListNetBean>() {
            @Override
            public void onNext(BlackListNetBean data) {
                super.onNext(data);
                if (data != null && data.data != null) {
                    final BlackListBean blackListBean = data.data;

                    RfSBridgeHandlerFactory.getInstance().getFlutterPushImpl().pushBlackListInfo(blackListBean);

                    ShareFileUtils.setString(ShareFileUtils.BLOCK_ME_LIST, GsonUtils.createJsonString(blackListBean.blackMeList));

                    SharedPrefUtils.setString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.BLACK_LIST, GsonUtils.createJsonString(blackListBean.blackList));

                    SharedPrefUtils.setString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.HIDE_MOMENTS_USER_LIST, GsonUtils.createJsonString(blackListBean.blackMomentsUserList));

                }
            }
        });
    }

    public static void saveMsgToSP(MsgBean msgBean) {
        //移除黑名单 消息不存储
        if (MsgBean.MSG_TYPE_REMOVE_FROM_BLACK_LIST.equals(msgBean.msgType)) {

            String blockMeList = ShareFileUtils.getString(ShareFileUtils.BLOCK_ME_LIST, "");

            String userId = "[" + msgBean.fromUserId + "]";
            if (blockMeList.contains(userId)) {
                blockMeList = blockMeList.replace(userId, "");
                ShareFileUtils.setString(ShareFileUtils.BLOCK_ME_LIST, blockMeList);
            }

        }

        //添加黑名单 消息不存储
        if (MsgBean.MSG_TYPE_ADD_TO_BLACK_LIST.equals(msgBean.msgType)) {

            String blockMeList = ShareFileUtils.getString(ShareFileUtils.BLOCK_ME_LIST, "");

            String userId = "[" + msgBean.fromUserId + "]";
            if (!blockMeList.contains(userId)) {
                blockMeList += userId;
                ShareFileUtils.setString(ShareFileUtils.BLOCK_ME_LIST, blockMeList);
            }

        }
    }


}
