package com.thel.imp.momentdelete;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.base.BaseDataBean;
import com.thel.constants.TheLConstants;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.api.commonapi.CommonApiBusiness;
import com.thel.utils.DialogUtil;
import com.thel.utils.ShareFileUtils;

import androidx.fragment.app.Fragment;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by waiarl on 2017/11/18.
 * 日志删除工具类
 */

public class MomentDeleteUtils {
    private Fragment fragment;
    private Activity activity;
    MomentDeleteReceiver receiver;

    /**
     * 注册
     *
     * @param fragment
     */
    public void registerReceiver(Fragment fragment) {
        this.fragment = fragment;
        receiver = new MomentDeleteReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(TheLConstants.BROADCAST_MOMENT_DELETE_SUCCESS);
        if (fragment != null && fragment.getActivity() != null) {
            fragment.getActivity().registerReceiver(receiver, filter);
        }
    }

    /**
     * 取消注册
     *
     * @param fragment
     */
    public void unRegisterReceiver(Fragment fragment) {
        if (fragment != null && fragment.getActivity() != null) {
            try {
                fragment.getActivity().unregisterReceiver(receiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 注册
     *
     * @param activity
     */
    public void registerReceiver(Activity activity) {
        this.activity = activity;
        IntentFilter filter = new IntentFilter();
        receiver = new MomentDeleteReceiver();
        filter.addAction(TheLConstants.BROADCAST_MOMENT_DELETE_SUCCESS);
        if (activity != null) {
            try {
                activity.registerReceiver(receiver, filter);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 取消注册
     *
     * @param activity
     */
    public void unRegisterReceiver(Activity activity) {
        if (activity != null) {
            try {
                activity.unregisterReceiver(receiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    class MomentDeleteReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            final Bundle bundle = intent.getExtras();
            if (bundle != null) {
                final String momentId = bundle.getString(TheLConstants.BUNDLE_KEY_MOMENT_ID);
                if (fragment != null && fragment instanceof MomentDeleteListener) {
                    ((MomentDeleteListener) fragment).onMomentDelete(momentId);
                }
                if (activity != null && activity instanceof MomentDeleteListener) {
                    ((MomentDeleteListener) activity).onMomentDelete(momentId);
                }
            }
        }
    }

    public static void deleteMoment(final Context context, final String momentId) {

        DialogUtil.showConfirmDialog((Activity) context, null, TheLApp.context.getString(R.string.moments_delete_moment_confirm), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                ((BaseActivity) context).showLoadingNoBack();
                dialog.dismiss();
                if (TextUtils.isEmpty(momentId)) {
                    return;
                }
                final Flowable<BaseDataBean> flowable = CommonApiBusiness.deleteMoment(momentId);
                flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new InterceptorSubscribe<BaseDataBean>() {
                            @Override
                            public void onNext(BaseDataBean data) {
                                super.onNext(data);
                                ShareFileUtils.setBoolean(ShareFileUtils.TIME_MACHANE_NOT_READ, true);
                                ((BaseActivity) context).closeLoading();
                                if (!hasErrorCode && data != null) {
                                    deleteMomentSuccess(momentId);

                                }
                            }
                        });
            }
        });
    }

    /**
     * 网络请求删除日志
     *
     * @param momentId
     */
    public static void deleteMoment(final String momentId) {

        if (TextUtils.isEmpty(momentId)) {
            return;
        }
        final Flowable<BaseDataBean> flowable = CommonApiBusiness.deleteMoment(momentId);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new InterceptorSubscribe<BaseDataBean>() {
                    @Override
                    public void onNext(BaseDataBean data) {
                        super.onNext(data);
                        if (!hasErrorCode && data != null) {
                            deleteMomentSuccess(momentId);
                        }
                    }
                });
    }

    private static void deleteMomentSuccess(String momentId) {
        sendDeleteMomentBroad(momentId);
    }

    /**
     * 发送删除日志成功广播
     *
     * @param momentId
     */
    public static void sendDeleteMomentBroad(String momentId) {
        final Intent intent = new Intent();
        intent.setAction(TheLConstants.BROADCAST_MOMENT_DELETE_SUCCESS);
        final Bundle bundle = new Bundle();
        bundle.putString(TheLConstants.BUNDLE_KEY_MOMENT_ID, momentId);
        intent.putExtras(bundle);
        TheLApp.getContext().sendBroadcast(intent);
    }


}
