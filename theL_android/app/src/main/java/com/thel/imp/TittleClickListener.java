package com.thel.imp;

/**
 * Created by waiarl on 2017/11/13.
 * 顶部点击接口
 */

public interface TittleClickListener {
    /**
     * 点击了顶部
     */
    void onTitleClick();
}
