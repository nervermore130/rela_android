package com.thel.imp.follow.bean;

import com.thel.base.BaseDataBean;

/**
 * Created by waiarl on 2017/10/30.
 * 一个人的关注状态
 */

public class SingleUserRelationBean extends BaseDataBean {
    /**
     * 关注状态，详情参考 FollowStatusChangedListener
     */
    public int status;
}
