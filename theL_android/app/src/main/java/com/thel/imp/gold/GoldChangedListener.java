package com.thel.imp.gold;

/**
 * Created by waiarl on 2017/12/8.
 * 软妹豆更改监听
 */

public interface GoldChangedListener {
    /**
     * 软妹豆变化
     *
     * @param gold 当前软妹豆数量（这个数据仅做参考）
     */
    void onGoldChanged(long gold);
}
