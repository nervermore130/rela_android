package com.thel.imp.gold;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.fragment.app.Fragment;

import com.thel.app.TheLApp;
import com.thel.constants.TheLConstants;

/**
 * Created by waiarl on 2017/12/8.
 */

public class GoldUtils {
    private Fragment fragment;
    private Activity activity;
    GoldChangedReceiver receiver;

    /**
     * 注册
     *
     * @param fragment
     */
    public void registerReceiver(Fragment fragment) {
        this.fragment = fragment;
        receiver = new GoldChangedReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(TheLConstants.BROADCAST_GOLD_CHANGED);
        if (fragment != null && fragment.getActivity() != null) {
            fragment.getContext().registerReceiver(receiver, filter);
        }
    }

    /**
     * 取消注册
     *
     * @param fragment
     */
    public void unRegisterReceiver(Fragment fragment) {
        if (fragment != null && fragment.getActivity() != null) {
            try {
                fragment.getContext().unregisterReceiver(receiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 注册
     *
     * @param activity
     */
    public void registerReceiver(Activity activity) {
        this.activity = activity;
        IntentFilter filter = new IntentFilter();
        receiver = new GoldChangedReceiver();
        filter.addAction(TheLConstants.BROADCAST_GOLD_CHANGED);
        if (activity != null) {
            try {
                activity.registerReceiver(receiver, filter);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 取消注册
     *
     * @param activity
     */
    public void unRegisterReceiver(Activity activity) {
        if (activity != null) {
            try {
                activity.unregisterReceiver(receiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 发送软妹豆变化广播
     *
     * @param gold 当前软妹豆数量
     */
    public static void sendGoldChangedBroad(long gold) {
        final Intent intent = new Intent();
        intent.setAction(TheLConstants.BROADCAST_GOLD_CHANGED);
        final Bundle bundle = new Bundle();
        bundle.putLong(TheLConstants.BUNDLE_KEY_CURRENT_GOLD, gold);
        intent.putExtras(bundle);
        TheLApp.getContext().sendBroadcast(intent);
    }

    /**
     * 软妹豆变化广播接收器
     */
    class GoldChangedReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            final Bundle bundle = intent.getExtras();
            final long gold = bundle.getLong(TheLConstants.BUNDLE_KEY_CURRENT_GOLD, 0);
            if (fragment != null && fragment instanceof GoldChangedListener) {
                ((GoldChangedListener) fragment).onGoldChanged(gold);
            }
            if (activity != null && activity instanceof GoldChangedListener) {
                ((GoldChangedListener) activity).onGoldChanged(gold);
            }
        }
    }

}
