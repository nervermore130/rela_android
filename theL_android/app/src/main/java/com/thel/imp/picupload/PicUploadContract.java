package com.thel.imp.picupload;

/**
 * Created by waiarl on 2017/11/21.
 * 上传图片或者视频的一些接口
 */

public class PicUploadContract {
    /**
     * 上传视频或者图片接口回调
     */
    public interface PicUploadCallback {
        /**
         * 上传失败
         *
         * @param index 上传失败的 下标
         */
        void uploadFailed(int index);

        /**
         * 上传完成
         *
         * @param imageUrls 按顺序返回的总的urls
         */
        void uploadComplete(String imageUrls);

        /**
         * 上传视频完成
         *
         * @param videoUrl 返回的视频链接，在uploadComplete之前
         */
        void uploadVideoComplete(String videoUrl, String videoWebp);

    }

    /**
     * 上传状态接口回调
     */
    public interface PicUploadStatusCallback {
        /**
         * @param count 设置未上传成功的个数
         */
        void setUnUploadCount(int count);

        /**
         * @return 尚未上传成功的个数
         */
        int getUnUploadCount();

        /**
         * 未上传总数减一（需实现）
         *
         * @return
         */
        int reduceUnUploadCount();
    }
}
