package com.thel.imp.momentdelete;

/**
 * Created by waiarl on 2017/11/18.
 * 日志删除监听
 */

public interface MomentDeleteListener {
    /**
     * 删除日志监听
     *
     * @param momentId 删除的日志id
     */
    void onMomentDelete(String momentId);
}
