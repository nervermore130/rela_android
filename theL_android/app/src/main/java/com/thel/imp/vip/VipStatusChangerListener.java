package com.thel.imp.vip;

/**
 * Created by waiarl on 2017/10/16.
 * 会员状态改变
 * 建议使用广播
 */

public interface VipStatusChangerListener {
    /**
     * 会员状态改变
     *
     * @param isVip    现在是否是会员
     * @param vipLevel 当前会员等级
     */
    void onVipStatusChanged(boolean isVip, int vipLevel);
}
