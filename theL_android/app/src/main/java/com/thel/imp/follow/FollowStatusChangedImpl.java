package com.thel.imp.follow;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.ResultNetBean;
import com.thel.constants.TheLConstants;
import com.thel.imp.follow.bean.SingleUserRelationBean;
import com.thel.imp.follow.bean.SingleUserRelationNetBean;
import com.thel.imp.friend.FriendUtils;
import com.thel.manager.ChatServiceManager;
import com.thel.modules.main.messages.bean.MsgBean;
import com.thel.modules.main.messages.db.MessageDataBaseAdapter;
import com.thel.modules.main.messages.db.MessageDataBaseDelegate;
import com.thel.modules.main.messages.utils.MsgUtils;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.api.commonapi.CommonApiBusiness;
import com.thel.network.service.DefaultRequestService;
import com.thel.utils.DialogUtil;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * 关注的统一调用实现类
 * 以及对好友的一些判断
 * Created by waiarl on 2017/10/30.
 */

public class FollowStatusChangedImpl implements FollowStatusChangedListener {
    FollowStatusReceiver receiver;
    private Fragment fragment;
    private Activity activity;


    class FollowStatusReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                final String userId = intent.getStringExtra(TheLConstants.BUNDLE_KEY_USER_ID);
                final String avatar = intent.getStringExtra(TheLConstants.BUNDLE_KEY_USER_AVATAR);
                final String nickName = intent.getStringExtra(TheLConstants.BUNDLE_KEY_NICKNAME);
                final int followStatus = intent.getIntExtra(TheLConstants.BUNDLE_KEY_FOLLOW_STATUS, FOLLOW_STATUS_NO);

                if (fragment != null && fragment instanceof FollowStatusChangedListener) {
                    ((FollowStatusChangedListener) fragment).onFollowStatusChanged(followStatus, userId, nickName, avatar);
                }
                if (activity != null && activity instanceof FollowStatusChangedListener) {
                    ((FollowStatusChangedListener) activity).onFollowStatusChanged(followStatus, userId, nickName, avatar);
                }
            }
        }
    }

    public void registerReceiver(Fragment fragment) {
        this.fragment = fragment;
        receiver = new FollowStatusReceiver();
        IntentFilter filter = new IntentFilter(TheLConstants.BROADCAST_FOLLOW_STATUS_CHANGED);
        if (fragment != null && fragment.getActivity() != null) {
            fragment.getActivity().registerReceiver(receiver, filter);
        }
    }

    public void unRegisterReceiver(Fragment fragment) {
        if (fragment != null && fragment.getActivity() != null) {
            try {
                fragment.getActivity().unregisterReceiver(receiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void registerReceiver(Activity activity) {
        this.activity = activity;
        receiver = new FollowStatusReceiver();
        IntentFilter filter = new IntentFilter(TheLConstants.BROADCAST_FOLLOW_STATUS_CHANGED);
        if (activity != null) {
            try {
                activity.registerReceiver(receiver, filter);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void unRegisterReceiver(Activity activity) {
        if (activity != null) {
            try {
                activity.unregisterReceiver(receiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void followUser(final String userId, final String actionType, final String nickName, final String avatar) {
        final int sta = ACTION_TYPE_CANCEL_FOLLOW.equals(actionType) ? FOLLOW_STATUS_NO : FOLLOW_STATUS_FOLLOW;
        sendFollowUserBroadCast(userId, sta, nickName, avatar);

        final Flowable<ResultNetBean> flowable = CommonApiBusiness.followUser(userId, actionType);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<ResultNetBean>() {

            @Override
            public void onNext(ResultNetBean resultNetBean) {
                super.onNext(resultNetBean);
                if (hasErrorCode || resultNetBean == null) {
                    return;
                }
                final int status = ACTION_TYPE_CANCEL_FOLLOW.equals(actionType) ? FOLLOW_STATUS_NO : FOLLOW_STATUS_FOLLOW;
                //  sendFollowUserBroadCast(userId, status, nickName, avatar);
                refreshRelationFriend(userId, status, nickName, avatar);

                ShareFileUtils.setBoolean(ShareFileUtils.IS_FOLLOW_USER, true);

                if (ACTION_TYPE_FOLLOW.equals(actionType)) {//如果是关注，弹对话框，发送消息

                    try {

                        DialogUtil.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.userinfo_activity_followed));

                        MsgBean msgBean = MsgUtils.createMsgBean(MsgBean.MSG_TYPE_FOLLOW, "", MsgBean.IS_FOLLOW_MSG, userId + "", nickName, avatar);

                        if (msgBean != null) {

                            msgBean.msgStatus = TheLConstants.MsgSendingStatusConstants.MSG_SUCCESS;

                            ChatServiceManager.getInstance().winkMsgSaveToDB(msgBean);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                ChatServiceManager.getInstance().checkFriendsRelationship(userId, null);
            }

        });
    }

    public static void followUserWithNoDialog(final String userId, final String actionType, final String nickName, final String avatar) {
        final int sta = ACTION_TYPE_CANCEL_FOLLOW.equals(actionType) ? FOLLOW_STATUS_NO : FOLLOW_STATUS_FOLLOW;
        sendFollowUserBroadCast(userId, sta, nickName, avatar);
        final Flowable<ResultNetBean> flowable = CommonApiBusiness.followUser(userId, actionType);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<ResultNetBean>() {

            @Override
            public void onNext(ResultNetBean resultNetBean) {
                super.onNext(resultNetBean);
                if (hasErrorCode || resultNetBean == null) {
                    return;
                }
                final int status = ACTION_TYPE_CANCEL_FOLLOW.equals(actionType) ? FOLLOW_STATUS_NO : FOLLOW_STATUS_FOLLOW;
                //   sendFollowUserBroadCast(userId, status, nickName, avatar);
                refreshRelationFriend(userId, status, nickName, avatar);

                ShareFileUtils.setBoolean(ShareFileUtils.IS_FOLLOW_USER, true);
                //如果是关注，弹对话框，发送消息
                if (ACTION_TYPE_FOLLOW.equals(actionType)) {
                    // 发送关注消息// 发送关注消息,4.1.0后改为只存储本地消息
                    MsgBean msgBean = MsgUtils.createMsgBean(MsgBean.MSG_TYPE_FOLLOW, "", MsgBean.IS_FOLLOW_MSG, userId + "", nickName, avatar);

                    if (msgBean != null) {

                        msgBean.msgStatus = TheLConstants.MsgSendingStatusConstants.MSG_SUCCESS;

                        ChatServiceManager.getInstance().winkMsgSaveToDB(msgBean);

                    }


                }
                ChatServiceManager.getInstance().checkFriendsRelationship(userId, null);
            }

        });
    }

    public static void sendFollowUserBroadCast(String userId, int flowStatus, String nickName, String avatar) {
        L.i("refresh", "userId=" + userId + ",flowStatus=" + flowStatus + ",nickName=" + nickName + ",avatar=" + avatar);
        Intent intent = new Intent();
        intent.setAction(TheLConstants.BROADCAST_FOLLOW_STATUS_CHANGED);
        intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, userId);
        intent.putExtra(TheLConstants.BUNDLE_KEY_NICKNAME, nickName);
        intent.putExtra(TheLConstants.BUNDLE_KEY_USER_AVATAR, avatar);
        intent.putExtra(TheLConstants.BUNDLE_KEY_FOLLOW_STATUS, flowStatus);
        TheLApp.getContext().sendBroadcast(intent);

        //    refreshRelationFriend(userId, flowStatus, nickName, avatar);
    }

    public static void refreshRelationFriend(String userId, int followStatus, String nickName, String avatar) {
        if (FOLLOW_STATUS_NO == followStatus) {//如果是取消关注，肯定要从好友里面移除
            removeFriend(userId, nickName, avatar);
        } else if (FOLLOW_STATUS_FOLLOW == followStatus) {//如果是关注，需要重新请求接口看是否是朋友
            getSingeUserRelation(userId, nickName, avatar);
        }
    }

    /**
     * 获取是否是好友
     * 如果是好友而以前不是好友，则添加到好友数据库，并且刷新聊天界面
     *
     * @param userId
     */
    public static void getSingeUserRelation(final String userId, final String nickname, final String avatar) {
        final Flowable<SingleUserRelationNetBean> flowable = DefaultRequestService.createCommonRequestService().getSingleUsersRelation(userId);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<SingleUserRelationNetBean>() {

            @Override
            public void onNext(SingleUserRelationNetBean singleUserRelationNetBean) {
                final SingleUserRelationBean bean = singleUserRelationNetBean.data;
                if (FOLLOW_STATUS_FRIEND == bean.status && !isRelationFriend(userId)) {
                    saveFriend(userId, nickname, avatar);
                }
            }

        });
    }

    /**
     * 从好友列表中删除
     *
     * @param userId
     * @param nickName
     * @param userId
     */
    public static void removeFriend(final String userId, final String nickName, final String avatar) {
        final String myUserId = ShareFileUtils.getString(ShareFileUtils.ID, "");
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (isRelationFriend(userId)) {//原本是好友，现在不是好友
                    //把这个名单仿佛到好友列表里面
                    // DataBaseAdapter.getInstance(TheLApp.getContext()).deleteOneRelationFriend(myUserId, userId);
                    FriendUtils.deleteOneFriend(userId, null);
                    MessageDataBaseAdapter.getInstance(TheLApp.getContext(), myUserId).putUserMsgIntoStranger(userId + "");
                    sendRelationChangedBroadCast();
                    addFollowTipMsg(false, userId, nickName, avatar);

                }
            }
        }).start();
    }

    /**
     * 添加到好友列表
     *
     * @param userId
     */
    public static void saveFriend(final String userId, final String nickname, final String avatar) {
        final String myUserId = ShareFileUtils.getString(ShareFileUtils.ID, "");
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (!isRelationFriend(userId)) {//原本不是好友，现在是好友
                    //    DataBaseAdapter.getInstance(TheLApp.getContext()).saveOneRelationFriend(myUserId, userId);
                    FriendUtils.saveOneFriend(userId, null);

                    MessageDataBaseAdapter.getInstance(TheLApp.getContext(), myUserId).putUserMsgIntoFriend(userId + "");
                    sendRelationChangedBroadCast();
                    addFollowTipMsg(true, userId, nickname, avatar);
                }
            }
        }).start();
    }

    /**
     * @param isFriend   当前状态：true:当前为好友（以前不是好友），false:现在不是好友（本来是好友）
     * @param toUserId
     * @param toNickname
     * @param toAvatar
     */
    private static void addFollowTipMsg(boolean isFriend, String toUserId, final String toNickname, final String toAvatar) {
        if (TextUtils.isEmpty(toUserId)) {
            return;
        }
        final String myUid = ShareFileUtils.getString(ShareFileUtils.ID, "");
        final String myName = ShareFileUtils.getString(ShareFileUtils.USER_NAME, "");

        if (TextUtils.isEmpty(myUid)) {
            return;
        }
        final MsgBean msgBean = new MsgBean();
        msgBean.msgType = MsgBean.MSG_TYPE_RELATION_TIP;
        if (isFriend) {
            msgBean.msgText = TheLApp.getContext().getString(R.string.chat_following_each);
        } else {
            msgBean.msgText = TheLApp.getContext().getString(R.string.no_longer_following_each);
        }
        msgBean.msgTime = System.currentTimeMillis();

        // new message
        msgBean.fromUserId = myUid;
        msgBean.fromNickname = myName;
        msgBean.fromAvatar = ShareFileUtils.getString(ShareFileUtils.AVATAR, "");
        msgBean.fromMessageUser = ShareFileUtils.getString(ShareFileUtils.MESSAGE_USER, "");
        msgBean.toUserId = toUserId;
        msgBean.toUserNickname = toNickname;
        msgBean.toAvatar = toAvatar;
        msgBean.toMessageUser = TheLConstants.MSG_ACCOUNT_USER_ID_STR + toUserId;
        msgBean.hadRead = 1; // 发出的消息一律为读过
        msgBean.msgDirection = "0";
        msgBean.msgStatus = MsgBean.MSG_STATUS_RECEIVED;

//        MessageDataBaseAdapter.getInstance(TheLApp.getContext(), myUid).saveWarningMessage(msgBean, msgBean.toUserId);
        MessageDataBaseDelegate.sendRefreshUiBroadcast(msgBean);
    }

    /**
     * 发广播，通知一些聊天列表，要更新列表
     */
    public static void sendRelationChangedBroadCast() {
        // 发广播
        Intent intent = new Intent();
        intent.setAction(TheLConstants.BROADCAST_RELATION_CHANGED);
        TheLApp.getContext().sendBroadcast(intent);
    }

    /**
     * 是否是好友
     *
     * @param userId
     * @return
     */
    public static boolean isRelationFriend(String userId) {
        // return DataBaseAdapter.getInstance(TheLApp.getContext()).isRelationFriend(ShareFileUtils.getString(ShareFileUtils.ID, ""), userId);
        return FriendUtils.isFriend(userId);
    }

    @Override
    public void onFollowStatusChanged(int followStatus, String userId, String nickName, String avatar) {

    }

}
