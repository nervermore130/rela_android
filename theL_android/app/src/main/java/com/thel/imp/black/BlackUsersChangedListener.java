package com.thel.imp.black;

/**
 * Created by waiarl on 2017/11/13.
 * 黑名单变化接口
 */

public interface BlackUsersChangedListener {
    /**
     * 黑名单变化
     *
     * @param userId 用户id
     * @param isAdd  是否被添加到黑名单：true：添加到黑名单，false:移除黑名单
     */
    void onBlackUsersChanged(String userId, boolean isAdd);
}
