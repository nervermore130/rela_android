package com.thel.imp.sticker;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.StickerPackBean;
import com.thel.constants.TheLConstants;
import com.thel.data.local.FileHelper;
import com.thel.utils.GsonUtils;
import com.thel.utils.PhoneUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.ToastUtils;

import org.json.JSONArray;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by waiarl on 2017/11/18.
 * 表情工具类
 */

public class StickerUtils {
    private Fragment fragment;
    private Activity activity;
    StickerChangedReceiver receiver;

    /**
     * 获取文件名
     *
     * @return
     */
    public static String getMyStickerFileName() {
        final String myUserId = ShareFileUtils.getString(ShareFileUtils.ID, "");
        final String fileName = ShareFileUtils.STICKER + "_" + myUserId;
        return fileName;
    }

    /**
     * 获取我的表情列表
     * 根据sp里存储的消息
     *
     * @param callback
     */
    public static void getMySpStickFileList(final StickerContract.GetStickerListCallback<StickerPackBean> callback) {
        final String fileName = getMyStickerFileName();
        final String dir = FileHelper.getInstance().getEmojiDir();
        final List<StickerPackBean> myList = new ArrayList<>();
        if (TextUtils.isEmpty(dir) || TextUtils.isEmpty(fileName)) {
            return;
        }
        if (!PhoneUtils.hasSdcard()) {
            ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.empty_sdcard));
            return;
        }

        Flowable.just(dir + fileName).onBackpressureDrop().subscribeOn(Schedulers.io()).map(new Function<String, String>() {
            @Override
            public String apply(String s) {
                return FileHelper.getInstance().readerFileToString(s);
            }
        }).observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<String>() {
            @Override
            public void onSubscribe(Subscription s) {
                s.request(1);
            }

            @Override
            public void onNext(String s) {
                if (TextUtils.isEmpty(s)) {
                } else {
                    final List<StickerPackBean> list = new ArrayList<>();
                    try {
                        final JSONArray array = new JSONArray(s);
                        final int length = array.length();
                        for (int i = 0; i < length; i++) {
                            final StickerPackBean bean = GsonUtils.getObject(array.getString(i), StickerPackBean.class);
                            if (isStickerSaved(bean.id + "")) {
                                list.add(bean);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    myList.addAll(list);
                }
                if (callback != null) {
                    callback.getStickerList(myList);
                }

            }

            @Override
            public void onError(Throwable t) {
                if (callback != null) {
                    callback.getStickerList(myList);
                }
            }

            @Override
            public void onComplete() {

            }
        });

    }

    /**
     * 获取我的表情列表,无论sp中有没有
     *
     * @param callback
     */
    public static void getMyStickFileList(final StickerContract.GetStickerListCallback<StickerPackBean> callback) {
        final String fileName = getMyStickerFileName();
        final String dir = FileHelper.getInstance().getEmojiDir();
        final List<StickerPackBean> myList = new ArrayList<>();
        if (TextUtils.isEmpty(dir) || TextUtils.isEmpty(fileName)) {
            return;
        }
        if (!PhoneUtils.hasSdcard()) {
            ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.empty_sdcard));
            return;
        }

        Flowable.just(dir + fileName).onBackpressureDrop().subscribeOn(Schedulers.io()).map(new Function<String, String>() {
            @Override
            public String apply(String s) {
                return FileHelper.getInstance().readerFileToString(s);
            }
        }).observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<String>() {
            @Override
            public void onSubscribe(Subscription s) {
                s.request(1);
            }

            @Override
            public void onNext(String s) {
                if (TextUtils.isEmpty(s)) {
                } else {
                    final List<StickerPackBean> list = new ArrayList<>();
                    try {
                        final JSONArray array = new JSONArray(s);
                        final int length = array.length();
                        for (int i = 0; i < length; i++) {
                            final StickerPackBean bean = GsonUtils.getObject(array.getString(i), StickerPackBean.class);
                            list.add(bean);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    myList.addAll(list);
                }
                if (callback != null) {
                    callback.getStickerList(myList);
                }

            }

            @Override
            public void onError(Throwable t) {
                if (callback != null) {
                    callback.getStickerList(myList);
                }
            }

            @Override
            public void onComplete() {

            }
        });
    }

    /**
     * 保存一个表情列表
     *
     * @param bean
     * @param callBack
     */
    public static void saveOneSticker(final StickerPackBean bean, final StickerContract.SaveStickerCallBack callBack) {
        getMyStickFileList(new StickerContract.GetStickerListCallback<StickerPackBean>() {
            @Override
            public void getStickerList(List<StickerPackBean> stickerList) {
                final int size = stickerList.size();
                for (int i = 0; i < size; i++) {//如果本地文件已经保存，就保存到缓存sp中
                    if (stickerList.get(i).id == bean.id) {
                        saveOneStickerId(bean.id + "");
                        if (callBack != null) {
                            callBack.saveStickerSuccess();
                        }
                        sendStickerChangedBroad();
                        return;
                    }
                }//如果本地文件还没有保存，就保存到本地文件
                stickerList.add(bean);
                final String json = GsonUtils.createJsonString(stickerList);

                Flowable.just(json).onBackpressureDrop().subscribeOn(Schedulers.io()).map(new Function<String, Integer>() {

                    @Override
                    public Integer apply(String s) {
                        int i = FileHelper.getInstance().writeTxtToFile(s, FileHelper.getInstance().getEmojiDir(), getMyStickerFileName());
                        return i;
                    }
                }).observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<Integer>() {
                    @Override
                    public void onSubscribe(Subscription s) {
                        s.request(1);
                    }

                    @Override
                    public void onNext(Integer integer) {
                        saveOneStickerId(bean.id + "");
                        if (callBack != null) {
                            callBack.saveStickerSuccess();
                        }
                        sendStickerChangedBroad();
                    }

                    @Override
                    public void onError(Throwable t) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });


            }
        });
    }

    /**
     * 在原有表情列表的基础上新加上表情列表
     *
     * @param stickerPackBeanList
     * @param callBack
     */
    public static void addStickerList(final List<StickerPackBean> stickerPackBeanList, final StickerContract.SaveStickerCallBack callBack) {
        getMyStickFileList(new StickerContract.GetStickerListCallback<StickerPackBean>() {
            @Override
            public void getStickerList(List<StickerPackBean> stickerList) {
                final List<StickerPackBean> list = new ArrayList<>();
                if (stickerList != null) {
                    list.addAll(stickerList);
                }
                list.removeAll(stickerList);
                list.addAll(stickerList);
                saveStickerList(list, callBack);
                for (StickerPackBean bean : stickerList) {
                    saveOneStickerId(bean.id + "");
                }
            }
        });
    }


    /**
     * 保存表情列表（清空以前的，保留现在的）
     *
     * @param stickerList
     * @param callBack
     */
    public static void saveStickerList(List<StickerPackBean> stickerList, final StickerContract.SaveStickerCallBack callBack) {
        final String json = GsonUtils.createJsonString(stickerList);

        Flowable.just(json).onBackpressureDrop().subscribeOn(Schedulers.io()).map(new Function<String, Integer>() {

            @Override
            public Integer apply(String s) {
                int i = FileHelper.getInstance().writeTxtToFile(s, FileHelper.getInstance().getEmojiDir(), getMyStickerFileName());
                return i;
            }
        }).observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<Integer>() {
            @Override
            public void onSubscribe(Subscription s) {
                s.request(1);
            }

            @Override
            public void onNext(Integer integer) {
                if (callBack != null) {
                    callBack.saveStickerSuccess();
                }
            }

            @Override
            public void onError(Throwable t) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    /**
     * 保持一个表情id
     *
     * @param id
     */
    public static void saveOneStickerId(String id) {
        if (TextUtils.isEmpty(id)) {
            return;
        }
        List<String> list = getMyStickerIdList();
        if (list == null) {
            list = new ArrayList<>();
        }
        if (!list.contains(id)) {
            list.add(id);
            saveStickerIdList(list);
        }
    }

    /**
     * 保持表情id list
     *
     * @param list
     */
    private static void saveStickerIdList(List<String> list) {
        if (list == null) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        for (String st : list) {
            sb.append(",").append(st);
        }
        ShareFileUtils.setString(getMyStickerFileName(), sb.toString());
    }

    /**
     * 本地是否有这个表情
     *
     * @param id
     * @return
     */
    public static boolean isStickerSaved(String id) {
        final List<String> list = getMyStickerIdList();
        return list.contains(id);
    }

    /**
     * 删除一个表情的的id
     *
     * @param id
     */
    public static void deleteOneStickerId(String id) {
        if (TextUtils.isEmpty(id)) {
            return;
        }
        final List<String> list = getMyStickerIdList();
        list.remove(id);
        saveStickerIdList(list);
    }

    /**
     * 获取本地表情id列表
     *
     * @return
     */
    public static List<String> getMyStickerIdList() {
        return new ArrayList<>(Arrays.asList(ShareFileUtils.getString(getMyStickerFileName(), "").split(",")));
    }


    /********************************一下为广播方法***************************************************/
    /**
     * 注册
     *
     * @param fragment
     */
    public void registerReceiver(Fragment fragment) {
        this.fragment = fragment;
        receiver = new StickerChangedReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(TheLConstants.BROADCAST_STICKER_CHANGED);
        if (fragment != null && fragment.getActivity() != null) {
            fragment.getActivity().registerReceiver(receiver, filter);
        }
    }

    /**
     * 取消注册
     *
     * @param fragment
     */
    public void unRegisterReceiver(Fragment fragment) {
        if (fragment != null && fragment.getActivity() != null) {
            try {
                fragment.getActivity().unregisterReceiver(receiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 注册
     *
     * @param activity
     */
    public void registerReceiver(Activity activity) {
        this.activity = activity;
        IntentFilter filter = new IntentFilter();
        receiver = new StickerChangedReceiver();
        filter.addAction(TheLConstants.BROADCAST_STICKER_CHANGED);
        if (activity != null) {
            try {
                activity.registerReceiver(receiver, filter);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 取消注册
     *
     * @param activity
     */
    public void unRegisterReceiver(Activity activity) {
        if (activity != null) {
            try {
                activity.unregisterReceiver(receiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 表情包更改后广播接收器
     */
    class StickerChangedReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (fragment != null && fragment instanceof StickerContract.StickerChangedListener) {
                ((StickerContract.StickerChangedListener) fragment).onStickerChanged();
            }
            if (activity != null && activity instanceof StickerContract.StickerChangedListener) {
                ((StickerContract.StickerChangedListener) activity).onStickerChanged();
            }
        }
    }

    /**
     * 发送表情包改变广播
     */
    public static void sendStickerChangedBroad() {
        final Intent intent = new Intent(TheLConstants.BROADCAST_STICKER_CHANGED);
        TheLApp.getContext().sendBroadcast(intent);
    }

}
