package com.thel.imp.follow;

/**
 * Created by waiarl on 2017/10/19.
 * 关注状态改变接口
 */

public interface FollowStatusChangedListener {
    /**
     * 关注四种状态
     * 0:我不关注她，她也不关注我 1:我关注她，但是她不关注我 2:我不关注她，但是她却关注我 3:我们互相关注
     **/
    int FOLLOW_STATUS_NO = 0;
    int FOLLOW_STATUS_FOLLOW = 1;
    int FOLLOW_STATUS_FAN = 2;
    int FOLLOW_STATUS_FRIEND = 3;

    String ACTION_TYPE_FOLLOW = "1";//请求状态，请求关注
    String ACTION_TYPE_CANCEL_FOLLOW = "0";//请求状态，请求取消关注


    /**
     * 关注状态改变
     *
     * @param followStatus 关注状态
     * @param userId       关注的用户Id
     * @param nickName     关注的用户昵称
     * @param avatar       关注的用户头像
     */
    void onFollowStatusChanged(int followStatus, String userId, String nickName, String avatar);
}
