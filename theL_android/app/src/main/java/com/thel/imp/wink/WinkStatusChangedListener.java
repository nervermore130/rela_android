package com.thel.imp.wink;

/**
 * Created by waiarl on 2017/10/20.
 * 挤眼接口
 */

public interface WinkStatusChangedListener {

    /**
     * 挤眼成功
     * @param userId
     * @param type
     */
    void onWinkSucceess(String userId, String type);
}
