package com.thel.imp.momentreport;

/**
 * Created by waiarl on 2017/11/18.
 * 日志举报监听
 */

public interface MomentReportListener {
    /**
     * @param momentId 举报的日志id
     */
    void onMomentReport(String momentId);
}
