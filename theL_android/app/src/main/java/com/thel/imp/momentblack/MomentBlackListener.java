package com.thel.imp.momentblack;

/**
 * Created by waiarl on 2017/11/14.
 * 日志屏蔽监听
 */

public interface MomentBlackListener {
    /**
     * 屏蔽一篇日志
     *
     * @param momentId 被屏蔽的日志id
     */
    void onBlackOneMoment(String momentId);

    /**
     * 屏蔽她的所有日志
     *
     * @param userId 被屏蔽全部日志的用户id
     */
    void onBlackherMoment(String userId);
}
