package com.thel.imp;

import android.os.Bundle;
import androidx.annotation.Nullable;

import com.thel.base.BaseFragment;
import com.thel.bean.gift.WinkCommentBean;
import com.thel.imp.black.BlackUsersChangedListener;
import com.thel.imp.black.BlackUtils;
import com.thel.imp.follow.FollowStatusChangedImpl;
import com.thel.imp.follow.FollowStatusChangedListener;
import com.thel.imp.like.MomentLikeStatusChangedListener;
import com.thel.imp.like.MomentLikeUtils;
import com.thel.imp.momentblack.MomentBlackListener;
import com.thel.imp.momentblack.MomentBlackUtils;
import com.thel.imp.momentdelete.MomentDeleteListener;
import com.thel.imp.momentdelete.MomentDeleteUtils;
import com.thel.imp.momentreport.MomentReportListener;
import com.thel.imp.momentreport.MomentReportUtils;
import com.thel.imp.wink.WinkStatusChangedListener;
import com.thel.imp.wink.WinkUtils;

/**
 * Created by waiarl on 2017/10/20.
 * 公共工具集成Fragment
 */

public class BaseFunctionFragment extends BaseFragment implements
        FollowStatusChangedListener,//关注
        WinkStatusChangedListener,//挤眼
        MomentBlackListener,//日志屏蔽
        MomentLikeStatusChangedListener,//点赞
        BlackUsersChangedListener,//黑名单
        MomentDeleteListener,//日志删除
        MomentReportListener {//日志举报


    protected FollowStatusChangedImpl followStatusChangedImpl;//关注状态处理
    private WinkUtils winkUtils;//挤眼
    private MomentBlackUtils momentBlackUtils;//日志屏蔽（单个日志，全部日志）
    private MomentLikeUtils momentLikeUtils;//点赞
    private BlackUtils blackUtils;//屏蔽一个人
    private MomentDeleteUtils momentDeleteUtils;//删除日志
    private MomentReportUtils momentReportUtils;//举报日志

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        followStatusChangedImpl = new FollowStatusChangedImpl();
        followStatusChangedImpl.registerReceiver(this);
        winkUtils = new WinkUtils();
        winkUtils.registerReceiver(this);
        momentBlackUtils = new MomentBlackUtils();
        momentBlackUtils.registerReceiver(this);
        momentLikeUtils = new MomentLikeUtils();
        momentLikeUtils.registerReceiver(this);
        blackUtils = new BlackUtils();
        blackUtils.registerReceiver(this);
        momentDeleteUtils = new MomentDeleteUtils();
        momentDeleteUtils.registerReceiver(this);
        momentReportUtils = new MomentReportUtils();
        momentReportUtils.registerReceiver(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        followStatusChangedImpl.unRegisterReceiver(this);
        winkUtils.unRegisterReceiver(this);
        momentBlackUtils.unRegisterReceiver(this);
        momentLikeUtils.unRegisterReceiver(this);
        blackUtils.unRegisterReceiver(this);
        momentDeleteUtils.unRegisterReceiver(this);
        momentReportUtils.unRegisterReceiver(this);
    }

    @Override
    public void onWinkSucceess(String userId, String type) {
        closeLoading();
    }

    @Override
    public void onFollowStatusChanged(int followStatus, String userId, String nickName, String avatar) {
        closeLoading();
    }

    /**
     * 屏蔽单个日志结果，结果要实现的方法
     *
     * @param momentId
     */
    @Override
    public void onBlackOneMoment(String momentId) {

    }

    /**
     * 屏蔽某个人的日志成功结果，要实现的方法
     *
     * @param userId
     */
    @Override
    public void onBlackherMoment(String userId) {

    }

    /**
     * 日志点赞状态变化
     *
     * @param momentId       日志id
     * @param like           是否是喜欢，true:点赞，false:取消点赞
     * @param myUserId       我的id
     * @param myWinkUserBean 我的winkbean
     */
    @Override
    public void onLikeStatusChanged(String momentId, boolean like, String myUserId, WinkCommentBean myWinkUserBean) {
    }

    /**
     * 黑名单（添加或者移除）
     *
     * @param userId
     * @param isAdd
     */
    @Override
    public void onBlackUsersChanged(String userId, boolean isAdd) {

    }

    /**
     * 删除日志
     *
     * @param momentId
     */
    @Override
    public void onMomentDelete(String momentId) {

    }

    /**
     * 举报日志
     *
     * @param momentId
     */
    @Override
    public void onMomentReport(String momentId) {

    }


    /************************************以下代码容易歧义所以删除************************************************/
   /* *//**
     * 关注某个用户，关注成功后不谈对话框
     *
     * @param userId
     * @param actionType
     * @param nickname
     * @param avatar
     *//*
    protected void followUseWithNoDialog(String userId, String actionType, String nickname, String avatar) {
        FollowStatusChangedImpl.followUserWithNoDialog(userId, actionType, nickname, avatar);
    }

    *//**
     * 关注某个用户，关注成功后有对话框
     *
     * @param userId
     * @param actionType
     * @param nickname
     * @param avatar
     *//*
    protected void followUser(String userId, String actionType, String nickname, String avatar) {
        FollowStatusChangedImpl.followUser(userId, actionType, nickname, avatar);
    }

    *//**
     * 对某个人挤眼
     *
     * @param userId
     * @param logType   挤眼类型
     *//*
    protected void sendWink(String userId, String logType, String nickname, String avatar) {
        WinkUtils.sendWink(userId, logType, nickname, avatar);
    }

    *//**
     * 屏蔽某篇日志
     *
     * @param momentId
     *//*
    protected void blackOneMoment(String momentId) {
        MomentBlackUtils.blackThisMoment(momentId);
    }

    *//**
     * 屏蔽某个人的全部日志
     *
     * @param userId
     *//*
    protected void blackHerMoment(String userId) {
        MomentBlackUtils.blackHerMoment(userId);
    }*/


}
