package com.thel.imp.friend;

import android.text.TextUtils;

import com.thel.app.TheLApp;
import com.thel.bean.FriendListBean;
import com.thel.modules.main.messages.config.MessageConstants;
import com.thel.modules.main.messages.db.MessageDataBaseAdapter;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestConstants;
import com.thel.network.service.DefaultRequestService;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SharedPrefUtils;
import com.thel.utils.Utils;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by waiarl on 2017/11/24.
 */

public class FriendUtils {
    private static final String TAG = FriendUtils.class.getSimpleName();

    /**
     * 从网络中获取自己的好友列表
     */
    public static void getNetRelationFriendList(final FriendListContract.FriendListRefreshCallBack callBack) {
        final Flowable<FriendListBean> flowable = DefaultRequestService.createCommonRequestService().getNetFriendList(RequestConstants.RELATION_TYPE_FRIEND);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(Schedulers.io()).subscribe(new InterceptorSubscribe<FriendListBean>() {

            @Override
            public void onNext(FriendListBean friendListNetBean) {
                saveFriendListNetBean(friendListNetBean);
                if (callBack != null) {//注意，此时在子线程
                    callBack.onFriendListRefresh(friendListNetBean.data.list);
                }

            }
        });
    }

    /**
     * 获取存储好友的sp文件名，要根据userId区分
     *
     * @return
     */
    public static String getSpFriendListName() {
        return SharedPrefUtils.FILE_FRIEND_LIST + ShareFileUtils.getString(ShareFileUtils.ID, "");
    }

    /**
     * 保存网络获取的好友列表
     *
     * @param friendListNetBean
     */
    private static void saveFriendListNetBean(FriendListBean friendListNetBean) {
        if (friendListNetBean == null || friendListNetBean.data == null) {
            return;
        }
        saveFriendList(friendListNetBean.data.list);
    }

    /**
     * 保存好友列表 字符串
     *
     * @param list
     */
    public static void saveFriendStrList(String list) {
        L.i(TAG, "saveFriendList,list=" + list);

        if (TextUtils.isEmpty(list)) {
            return;
        }
        SharedPrefUtils.setString(getSpFriendListName(), SharedPrefUtils.FILE_FRIEND_LIST, list);

    }

    /**
     * 获取好友列表字符串
     *
     * @return
     */
    public static String getFriendListStr() {
        final String friendList = SharedPrefUtils.getString(getSpFriendListName(), SharedPrefUtils.FILE_FRIEND_LIST, "[]");
        L.i(TAG, "getFriendListStr:list=" + friendList);
        return friendList;

    }

    /**
     * 获取好友列表List
     *
     * @return
     */
    public static List<String> getMyFriendList() {
        final List<String> list = Utils.listStringToList(getFriendListStr());
        return list;
    }

    /**
     * 保存一个好友
     *
     * @param userId
     */
    public static void saveOneFriend(String userId, FriendListContract.FriendListChangedListener listener) {
        if (TextUtils.isEmpty(userId) || userId.equals("0")) {
            return;
        }
        L.i(TAG, "saveOneFriend,userId=" + userId);
        final List<String> list = getMyFriendList();
        final boolean isFriend = list.remove(userId);
        list.add(userId);
        saveFriendList(list);
        if (!isFriend && listener != null) {
            listener.onFriendListChanged(true, userId);
        }
    }

    /**
     * 删除一个好友
     *
     * @param userId
     */
    public static void deleteOneFriend(String userId, FriendListContract.FriendListChangedListener listener) {
        if (TextUtils.isEmpty(userId) || userId.equals("0")) {
            return;
        }
        L.i(TAG, "deleteOneFriend,userId=" + userId);

        final List<String> list = getMyFriendList();
        final boolean isFriend = list.remove(userId);
        saveFriendList(list);
        if (isFriend && listener != null) {
            listener.onFriendListChanged(false, userId);
        }
    }

    /**
     * 保存好友列表List
     *
     * @param list
     */
    public static void saveFriendList(List<String> list) {
        if (list == null) {
            return;
        }
        saveFriendStrList(Utils.listToString(list));
    }

    /**
     * 判断是否是好友
     *
     * @param userId
     * @return
     */
    public static boolean isFriend(String userId) {
        if (TextUtils.isEmpty(userId) || "-1".equals(userId)) {
            return false;
        }
        L.i(TAG, "isFriend,userId=" + userId);

        if (MessageConstants.RELA_ACCOUNT_IDS.contains(userId)) {//如果是热拉官方账号,返回true
            return true;
        }
        final boolean isFriend = getMyFriendList().contains(userId);
        L.i(TAG, "isFriend,userId=" + userId + ",isFriend=" + isFriend);

        return isFriend;
    }

    /**
     * 刷新message 列表
     */
    public static void refreshMessageTableList() {
        FriendUtils.getNetRelationFriendList(new FriendListContract.FriendListRefreshCallBack() {
            @Override
            public void onFriendListRefresh(List<String> userList) {
                MessageDataBaseAdapter.getInstance(TheLApp.getContext(), Utils.getMyUserId()).refreshMsgList(null);
                MessageDataBaseAdapter.getInstance(TheLApp.getContext(), Utils.getMyUserId()).refreshMsgList();
            }
        });
    }
}
