package com.thel.imp.momentreport;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.fragment.app.Fragment;

import com.thel.app.TheLApp;
import com.thel.constants.TheLConstants;
import com.thel.imp.momentdelete.MomentDeleteListener;

/**
 * Created by waiarl on 2017/11/18.
 * 举报日志工具类，由于举报日志是在一个activity里面，所以本地只实现了发广播与注册
 */

public class MomentReportUtils {
    private Fragment fragment;
    private Activity activity;
    MomentReportReceiver receiver;

    /**
     * 注册
     *
     * @param fragment
     */
    public void registerReceiver(Fragment fragment) {
        this.fragment = fragment;
        receiver = new MomentReportReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(TheLConstants.BROADCAST_MOMENT_REPORT_SUCCESS);
        if (fragment != null && fragment.getActivity() != null) {
            fragment.getActivity().registerReceiver(receiver, filter);
        }
    }

    /**
     * 取消注册
     *
     * @param fragment
     */
    public void unRegisterReceiver(Fragment fragment) {
        if (fragment != null && fragment.getActivity() != null) {
            try {
                fragment.getActivity().unregisterReceiver(receiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 注册
     *
     * @param activity
     */
    public void registerReceiver(Activity activity) {
        this.activity = activity;
        IntentFilter filter = new IntentFilter();
        receiver = new MomentReportReceiver();
        filter.addAction(TheLConstants.BROADCAST_MOMENT_REPORT_SUCCESS);
        if (activity != null) {
            try {
                activity.registerReceiver(receiver, filter);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 取消注册
     *
     * @param activity
     */
    public void unRegisterReceiver(Activity activity) {
        if (activity != null) {
            try {
                activity.unregisterReceiver(receiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    class MomentReportReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            final Bundle bundle = intent.getExtras();
            if (bundle != null) {
                final String momentId = bundle.getString(TheLConstants.BUNDLE_KEY_MOMENT_ID);
                if (fragment != null && fragment instanceof MomentDeleteListener) {
                    ((MomentDeleteListener) fragment).onMomentDelete(momentId);
                }
                if (activity != null && activity instanceof MomentDeleteListener) {
                    ((MomentDeleteListener) activity).onMomentDelete(momentId);
                }
            }
        }
    }

    /**
     * 发送举报日志广播
     *
     * @param momentId
     */
    public static void sendReportMomentBroad(String momentId) {
        final Intent intent = new Intent();
        intent.setAction(TheLConstants.BROADCAST_MOMENT_REPORT_SUCCESS);
        final Bundle bundle = new Bundle();
        bundle.putString(TheLConstants.BUNDLE_KEY_MOMENT_ID, momentId);
        intent.putExtras(bundle);
        TheLApp.getContext().sendBroadcast(intent);
    }

}
