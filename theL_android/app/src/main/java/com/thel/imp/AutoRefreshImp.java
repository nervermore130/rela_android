package com.thel.imp;

/**
 * Created by waiarl on 2018/3/12.
 */

public interface AutoRefreshImp {
    // long REFRESH_RATE = 30 * 1000;//测试30秒
    long REFRESH_RATE = 30 * 60 * 1000;//自动刷新频率是30分钟

    void tryRefreshData();

}
