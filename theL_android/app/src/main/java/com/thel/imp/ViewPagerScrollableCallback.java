package com.thel.imp;

/**
 * Created by waiarl on 2018/4/1.
 */

public interface ViewPagerScrollableCallback {

    void canScroll(boolean canScroll);
}
