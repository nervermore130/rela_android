package com.thel.bean.comment;

import java.util.List;

/**
 * 评论列表，评论分组
 *
 * @author Setsail
 */
public class CommentListBeanV2   {

    public boolean haveNextPage;

    public String momentId;

    public List<CommentBean> commentList;

    public int cursor;

    public long id;

}
