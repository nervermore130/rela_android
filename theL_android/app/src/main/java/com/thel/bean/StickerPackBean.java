package com.thel.bean;

import android.database.Cursor;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseDataBean;
import com.thel.db.MomentsDataBaseAdapter;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * @author Setsail
 */
public class StickerPackBean extends BaseDataBean implements Serializable {

    /**
     * 贴图的id就是服务端的id
     * 历史表情包的id：-1
     * 内置静态表情包的id：0
     */
    public long id;
    /**
     * 表情包名称
     */
    public String title;
    /**
     * 表情表小缩略图（表情键盘viewpager显示）
     */
    public String icon;
    /**
     * 表情表大缩略图（列表中显示）
     */
    public String thumbnail;

    @Override
    public String toString() {
        return "StickerPackBean{" + "id=" + id + ", title='" + title + '\'' + ", icon='" + icon + '\'' + ", thumbnail='" + thumbnail + '\'' + ", price='" + price + '\'' + ", bgImg='" + bgImg + '\'' + ", cover='" + cover + '\'' + ", summary='" + summary + '\'' + ", isNew=" + isNew + ", description='" + description + '\'' + ", authorAvatar='" + authorAvatar + '\'' + ", authorName='" + authorName + '\'' + ", authorId=" + authorId + ", pack_version=" + pack_version + ", status=" + status + ", showTitle=" + showTitle + ", isAnimation=" + isAnimation + ", purchased=" + purchased + ", copyright='" + copyright + '\'' + ", stickers=" + stickers + ", isSelected=" + isSelected + ", isEmojiPack=" + isEmojiPack + ", sign='" + sign + '\'' + ", vipFree=" + vipFree + '}';
    }

    public String price;
    /**
     * 表情包背景图
     */
    public String bgImg;
    /**
     * 表情包详情页封面
     */
    public String cover;
    /**
     * 表情包简介
     */
    public String summary;
    /**
     * 是不是最新推荐表情
     */
    public int isNew;
    /**
     * 表情包详细描述
     */
    public String description;
    /**
     * 作者头像
     */
    public String authorAvatar;
    /**
     * 作者名字
     */
    public String authorName;
    /**
     * 作者userd
     */
    public long authorId;
    /**
     * 表情包版本
     */
    public int pack_version;

    /**
     * 表情状态
     * 0:未购买 1:已购买，未下载 2:已下载 3:正在下载
     */
    public int status;

    /**
     * 0:不显示标题 1:显示推荐表情标题 2:显示更多表情标题
     */
    public int showTitle = 0;

    /**
     * 0:静态图表情包 1:动态图表情包
     * 判断详情页面是不是要显示"(GIF)"
     */
    public int isAnimation;

    /**
     * 我是否已经购买过
     */
    public int purchased;

    public String copyright;

    /**
     * 表情列表
     */
    public ArrayList<StickerBean> stickers = new ArrayList<>();

    /**
     * 表情键盘需要用到的几个标识
     */
    public boolean isSelected = false;// 是否选中
    public boolean isEmojiPack = false;// 内置静态表情选项

    /**
     * 签名，防止数据伪造
     */
    public String sign;
    /**
     * 会员免费
     * 为1的时候该表情会员免费，其他情况下默认
     */
    public int vipFree;

    public String generatePriceShow() {
        try {
            DecimalFormat df = new DecimalFormat("0.00");
            if (Double.valueOf(price) == 0) {
                return TheLApp.context.getString(R.string.sticker_store_act_download);
            }
            return "￥" + df.format(Double.valueOf(price) / 100);
        } catch (Exception e) {
            return "￥" + price;
        }
    }

    public void fromCursor(Cursor cursor) {
        id = cursor.getLong(cursor.getColumnIndex(MomentsDataBaseAdapter.F_IDENTIFICATION));
        title = cursor.getString(cursor.getColumnIndex(MomentsDataBaseAdapter.F_TITLE));
        icon = cursor.getString(cursor.getColumnIndex(MomentsDataBaseAdapter.F_ICON));
        cover = cursor.getString(cursor.getColumnIndex(MomentsDataBaseAdapter.F_COVER));
        bgImg = cursor.getString(cursor.getColumnIndex(MomentsDataBaseAdapter.F_BG_IMAGE));
        summary = cursor.getString(cursor.getColumnIndex(MomentsDataBaseAdapter.F_SUMMARY));
        thumbnail = cursor.getString(cursor.getColumnIndex(MomentsDataBaseAdapter.F_THUMBNAIL));
        price = cursor.getString(cursor.getColumnIndex(MomentsDataBaseAdapter.F_PRICE));
        sign = cursor.getString(cursor.getColumnIndex(MomentsDataBaseAdapter.F_SIGN));
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof StickerPackBean && ((StickerPackBean) obj).id == id;
    }
}
