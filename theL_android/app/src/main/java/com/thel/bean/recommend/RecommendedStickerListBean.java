package com.thel.bean.recommend;

import com.thel.base.BaseDataBean;
import com.thel.bean.SimpleStikerBean;

import java.util.ArrayList;
import java.util.List;

public class RecommendedStickerListBean extends BaseDataBean {


    /**
     * data : {"hots":[{"id":16,"title":"小白","thumbnail":"http://pro.thel.co/sticker/white/white_icon_m.png","bgImg":"http://pro.thel.co/sticker/white/white_bg.png","summary":"搞怪少女小白","isNew":0,"price":0,"vipFree":1,"purchased":0,"iapId":""},{"id":15,"title":"肥肥鼠","thumbnail":"http://pro.thel.co/sticker/mouse/mouse_icon_m.png","bgImg":"http://pro.thel.co/sticker/mouse/mouse_bg.png","summary":"身高只有两颗枣","isNew":0,"price":0,"vipFree":1,"purchased":0,"iapId":""},{"id":14,"title":"Luck熊","thumbnail":"http://pro.thel.co/sticker/luckbear/luckbear_icon_m.png","bgImg":"http://pro.thel.co/sticker/luckbear/luckbear_bg.png","summary":"送你一只Luck熊","isNew":0,"price":0,"vipFree":1,"purchased":0,"iapId":""},{"id":13,"title":"小幺鸡","thumbnail":"http://pro.thel.co/sticker/chicken/chicken_icon_m.png","bgImg":"http://pro.thel.co/sticker/chicken/chicken_bg.png","summary":"一只贱贱的小鸡","isNew":0,"price":0,"vipFree":1,"purchased":0,"iapId":""},{"id":12,"title":"彼格梨","thumbnail":"http://pro.thel.co/sticker/bigpear/bigpear_icon_m.png","bgImg":"http://pro.thel.co/sticker/bigpear/bigpear_bg.png","summary":"悲催的彼格梨","isNew":0,"price":0,"vipFree":1,"purchased":0,"iapId":""},{"id":11,"title":"神经猫","thumbnail":"http://pro.thel.co/sticker/jerkycat/jerkycat_icon_m.png","bgImg":"http://pro.thel.co/sticker/jerkycat/jerkycat_bg.png","summary":"耍贱高手必备表情","isNew":0,"price":0,"vipFree":0,"purchased":0,"iapId":""},{"id":9,"title":"TP的日常","thumbnail":"http://pro.thel.co/sticker/tp/tp_icon_m.png","bgImg":"http://pro.thel.co/sticker/tp/tp_bg.png","summary":"你和她的专属表情","isNew":0,"price":600,"vipFree":1,"purchased":0,"iapId":"com.la.la.tp"},{"id":8,"title":"大梨小苹","thumbnail":"http://pro.thel.co/sticker/pearapple/pearapple_icon_m.png","bgImg":"http://pro.thel.co/sticker/pearapple/pearapple_bg.png","summary":"两只水果的日常","isNew":0,"price":600,"vipFree":1,"purchased":1,"iapId":"com.la.la.apple"},{"id":6,"title":"热拉妹妹","thumbnail":"http://pro.thel.co/sticker/redhair/redhair_icon_m.png","bgImg":"http://pro.thel.co/sticker/redhair/redhair_bg.png","summary":"L君的可爱小妹妹","isNew":0,"price":600,"vipFree":1,"purchased":1,"iapId":"com.la.la.sisl"}]}
     */

        public List<SimpleStikerBean> hots = new ArrayList<>();
}