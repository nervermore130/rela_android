package com.thel.bean.recommend;

import com.thel.base.BaseDataBean;

/**
 * 话题推荐
 * Created by the L on 2016/12/1.
 */
public class RecommendThemeBean extends BaseDataBean {
    /**
     * id
     */
    public String id;

    /**
     * 内容
     */
    public String text;

    /**
     * 图片
     */
    public String image;

    /**
     * 颜值
     */
    public int joinTotal;

}
