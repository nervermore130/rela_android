package com.thel.bean.user;

import java.io.Serializable;

/**
 * @author Setsail
 */
public class MyCircleFriendBean implements Serializable {

    public long requestId;
    /**
     * 请求状态，-3=解除绑定, －2=取消，－1=拒绝，0=未处理，1=同意，2=过期*
     */
    public int requestStatus;
    public long userId;
    public String nickName;
    public String avatar;
    /**
     * 距离对方的生日的天数，后台接口拼写错了，我们跟他保持一致...
     */
    public String reminday;
    /**
     * 生日 yyyy-MM-dd
     */
    public String birthday;

    /**
     * 是否是添加
     */
    public boolean isAdd = false;

    public String online;
    public String distance;

    @Override
    public String toString() {
        return "MyCircleFriendBean{" + "requestId=" + requestId + ", requestStatus=" + requestStatus + ", userId=" + userId + ", nickName='" + nickName + '\'' + ", avatar='" + avatar + '\'' + ", reminday='" + reminday + '\'' + ", birthday='" + birthday + '\'' + ", isAdd=" + isAdd + ", online='" + online + '\'' + ", distance='" + distance + '\'' + '}';
    }
}
