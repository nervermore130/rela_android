package com.thel.bean.live;

import com.thel.base.BaseDataBean;

/**
 * Created by test1 on 2017/11/17.
 */

public class LiveTypeDataBean extends BaseDataBean {
    /**
     * id : 1
     * name : 才艺
     * icon : http://xxx/xxx.jpg
     * selectedIcon : http://xxx/xx.jpg
     */

    public int id;
    public String name;
    public String icon;
    public String selectedIcon;

    @Override
    public String toString() {
        return "LiveTypeDataBean{" + "id=" + id + ", name='" + name + '\'' + ", icon='" + icon + '\'' + ", selectedIcon='" + selectedIcon + '\'' + '}';
    }
}
