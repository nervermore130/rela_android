package com.thel.bean;

import com.thel.base.BaseDataBean;
import com.thel.utils.JsonUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 日志收藏集合类
 * Created by the L on 2016/8/3.
 */
public class FavoritesBean extends BaseDataBean {

    public Data data;

    public class Data {

        /**
         * 分页下标
         */
        public int cursor;
        /**
         * 收藏的日志集合
         */
        public List<FavoriteBean> list = new ArrayList<>();

//    public void fromJson(JSONObject tempobj) {
//        cursor = JsonUtils.getInt(tempobj, "cursor", 0);
//        JSONArray arr = JsonUtils.getJSONArray(tempobj, "list");
//        FavoriteBean bean = null;
//        for (int i = 0; i < arr.length(); i++) {
//            JSONObject obj = arr.optJSONObject(i);
//            bean = new FavoriteBean();
//            bean.fromJson(obj);
//            list.add(bean);
//        }
//    }

        @Override
        public String toString() {
            return "FavoritesBean{" +
                    "cursor=" + cursor +
                    ", list=" + list +
                    '}';
        }
    }
}
