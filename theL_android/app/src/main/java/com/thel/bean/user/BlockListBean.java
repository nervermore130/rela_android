package com.thel.bean.user;

import com.thel.base.BaseDataBean;

import java.util.ArrayList;

public class BlockListBean extends BaseDataBean {
    public BlockListDataBean data;

    public class BlockListDataBean {
        public int total;

        public ArrayList<BlockBean> blackListUsers = new ArrayList<BlockBean>();
    }


}

/*
    "data": {
        "total": 1,
        "blackListUsers": [
            {
                "userIntro": "模糊地迷恋你一场，就当风雨下潮涨。",
                "userId": 8208,
                "userName": "林晓糖",
                "userAvatar": "http://s-46344.gotocdn.com:8081/user-image/13550654615888208_head.jpg"
            }
        ]
	},
 */