package com.thel.bean.recommend;

import com.thel.utils.JsonUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by waiarl on 2017/7/12.
 */

public class RecommendWebBean implements Serializable {
    public String imageUrl;
    public String title;
    public String url;
    public String momentsText;

    @Override
    public String toString() {
        return "RecommendWebBean{" +
                "imageUrl='" + imageUrl + '\'' +
                ", title='" + title + '\'' +
                ", url='" + url + '\'' +
                ", momentsText='" + momentsText + '\'' +
                '}';
    }

    public JSONObject beJson() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("imageUrl", imageUrl);
            obj.put("title", title);
            obj.put("url", url);
            obj.put("momentsText", momentsText);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return obj;
    }

    public void fromJson(JSONObject object) {
        imageUrl = JsonUtils.getString(object, "imageUrl", "");
        title = JsonUtils.getString(object, "title", "");
        url = JsonUtils.getString(object, "url", "");
        momentsText = JsonUtils.getString(object, "momentsText", "");
    }

    public RecommendWebBean fromJson(String text) {
        JSONObject object = new JSONObject();
        try {
            object = new JSONObject(text);
            fromJson(object);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return this;
    }
}
