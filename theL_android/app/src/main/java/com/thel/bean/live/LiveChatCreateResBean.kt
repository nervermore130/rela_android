package com.thel.bean.live

import com.thel.modules.live.bean.LiveRoomMsgBean

data class LiveChatCreateResBean(var data: LiveChatDataBean)

data class LiveChatDataBean(var channelInfo: LiveChatChannelInfoBean, var channelStatus: LiveChatChannelStatusBean)

data class LiveChatChannelInfoBean(var userCount: Int, var rank: Int, var gem: String, var nowGem: String, var extra_msg: LiveRoomMsgBean)

data class LiveChatChannelStatusBean(var userId: String, var nickName: String, var avatar: String, var status: String)
