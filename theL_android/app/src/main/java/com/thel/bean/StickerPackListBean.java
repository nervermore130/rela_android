package com.thel.bean;

import com.thel.base.BaseDataBean;

import java.util.ArrayList;

/**
 * 表情包列表
 *
 * @author Setsail
 */
public class StickerPackListBean extends BaseDataBean {

        public ArrayList<SimpleStikerBean> list = new ArrayList<>();
        public int cursor;


}