package com.thel.bean.moments;

import android.text.TextUtils;

import com.thel.R;
import com.thel.app.TheLApp;

import java.io.Serializable;

/**
 * moments的消息
 *
 * @author Setsail
 */
public class MomentsMsgBean implements Serializable {

    /**
     * 用户id
     */
    public int userId;

    /**
     * 用户名
     */
    public String nickname;

    /**
     * 方头像地址
     */
    public String avatar;

    // 评论id
    public String commentId;

    // 评论时间
    public String commentTime;

    // 评论内容
    public String commentText;

    // 评论类型(text, at, image, voice, wink, sticker)(文字评论)
    public String commentType;

    // 点赞类型
    public int winkCommentType;

    // 评论对象
    public int toUserId;

    // 评论对象nickname
    public String toNickname;

    // 评论对象方头像地址
    public String toAvatar;

    // 评论类型，wink, say, to
    public String activityType;

    /**
     * moments的唯一标识，但动态是评论的回复时，该属性存储父评论id
     */
    public String momentsId;

    // moment的类型，文字还是图片还是都有
    public String momentsType;

    // moment 的文字内容
    public String momentsText;

    // moment的图片
    public String thumbnailUrl;

    /**
     * 是否匿名(0表示不匿名,1表示匿名)
     **/
    public int secret;

    /**
     * 音乐属性
     **/
    public long songId;
    public String songName;
    public String artistName;
    public String albumName;
    public String albumLogo100;
    public String albumLogo444;
    public String songLocation;
    public String toURL;

    public static final String MSG_TYPE_SAY = "say";
    public static final String MSG_TYPE_TO = "to";
    public static final String MSG_TYPE_WINK = "wink";
    public static final String MSG_TYPE_AT = "at";
    public static final String MSG_TYPE_RECOMMEND = "recommend";
    public static final String MSG_TYPE_AWAIT = "await";

    public static final String COMMENT_TYPE_AT = "at";
    public static final String COMMENT_TYPE_AT_COMMENT = "atComment";

    public static final String TYPE_COMMENT_REPLY = "com_reply";

    /**
     * 2.20.0版本做了评论分组功能，新增此属性，目前值仅可能为com_reply或空
     */
    public String type;

    public String getReleaseTimeShow() {
        if (TextUtils.isEmpty(commentTime)) {
            return "";
        }
        String[] arr = this.commentTime.split("_");
        if (arr.length < 2) {
            return "";
        }
        if ("0".equals(arr[0])) {
            return TheLApp.getContext().getString(R.string.info_just_now);
        } else if ("1".equals(arr[0])) {
            return arr[1] + TheLApp.getContext().getString(R.string.info_seconds_ago);
        } else if ("2".equals(arr[0])) {
            return arr[1] + TheLApp.getContext().getString(R.string.info_minutes_ago);
        } else if ("3".equals(arr[0])) {
            return arr[1] + TheLApp.getContext().getString(R.string.info_hours_ago);
        } else if ("4".equals(arr[0])) {
            return arr[1] + TheLApp.getContext().getString(R.string.info_days_ago);
        } else if ("5".equals(arr[0])) {
            return arr[1];
        }
        return "";

    }

}
