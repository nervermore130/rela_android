package com.thel.bean;

import com.thel.base.BaseDataBean;

import java.io.Serializable;

/**
 * Created by the L on 2016/8/31.
 */
public class VersionBean extends BaseDataBean implements Serializable {

    public Data data;

    public static class Data implements Serializable{
        /**
         * apk更新地址
         */
        public String url;
        /**
         * 最新版本
         */
        public String version;
        /**
         * 最新版本大小
         */
        public String apkSize;
        /**
         * 中文描述
         */
        public String desc_cn;
        /**
         * 英文描述
         */
        public String desc_en;
        /**
         * 繁体描述
         */
        public String desc_tw;

        /**
         * 4.0.2新加字段 googleplay的版本
         */
        public String google_version;
    }

}
