package com.thel.bean;

/**
 * Created by chad
 * Time 17/11/17
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */

public class WXLoginBean {

    public String access_token = null;

    public int expires_in;

    public String refresh_token;

    public String openid;

    public String scope;

    public String unionid;

    public String errcode = null;

    public String errmsg;

    @Override public String toString() {
        return "WXLoginBean{" +
                "access_token='" + access_token + '\'' +
                ", expires_in=" + expires_in +
                ", refresh_token='" + refresh_token + '\'' +
                ", openid='" + openid + '\'' +
                ", scope='" + scope + '\'' +
                ", unionid='" + unionid + '\'' +
                ", errcode='" + errcode + '\'' +
                ", errmsg='" + errmsg + '\'' +
                '}';
    }
}
