package com.thel.bean.user;


import com.thel.base.BaseDataBean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by waiarl on 2017/9/23.
 * 获取黑名单对象
 * 包括被我屏蔽的 黑名单，屏蔽我的名单，以及我屏蔽的日志的人的名单
 * 本地存储使用的是字符串形式，需要转换
 */

public class BlackListBean extends BaseDataBean implements Serializable {

    public List<String> blackList = new ArrayList<>();
    public List<String> blackMeList = new ArrayList<>();
    public List<String> blackMomentsUserList = new ArrayList<>();

    @Override
    public String toString() {
        return "BlackListBean{" + "blackList=" + blackList + ", blackMeList=" + blackMeList + ", blackMomentsUserList=" + blackMomentsUserList + '}';
    }
}
