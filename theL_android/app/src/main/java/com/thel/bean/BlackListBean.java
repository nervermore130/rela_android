package com.thel.bean;

import com.thel.base.BaseDataBean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 黑名单对象
 *
 * @author Setsail
 */
public class BlackListBean extends BaseDataBean implements Serializable {

    public List<Long> blackMeList = new ArrayList<>();
    public List<Long> blackList = new ArrayList<>();
    public List<Long> blackMomentsUserList = new ArrayList<>();

}
