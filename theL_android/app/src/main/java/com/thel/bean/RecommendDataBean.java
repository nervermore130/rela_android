package com.thel.bean;

import com.thel.base.BaseDataBean;

/**
 * Created by lingwei on 2017/11/21.
 */

public class RecommendDataBean extends BaseDataBean {
    /**
     * user : {"id":101225103,"level":4,"hiding":0,"nickName":"66","avatar":"http://static.rela.me/app/avatar/101225103/9baf680189d5862e243f3edda2b9050a.jpg","intro":"","age":21,"sex":0,"status":1,"affection":0,"roleName":"0","online":1,"loginTime":7,"unit":"days","verifyType":"","verifyIntro":"","isFollow":0,"distance":"","fansNum":3347}
     * label : null
     * imageUrl : http://static.rela.me/app/avatar/101225103/9baf680189d5862e243f3edda2b9050a.jpg
     */

    public UserBean user;
    public Object label;
    public String imageUrl;

    public static class UserBean {
        /**
         * id : 101225103
         * level : 4
         * hiding : 0
         * nickName : 66
         * avatar : http://static.rela.me/app/avatar/101225103/9baf680189d5862e243f3edda2b9050a.jpg
         * intro :
         * age : 21
         * sex : 0
         * status : 1
         * affection : 0
         * roleName : 0
         * online : 1
         * loginTime : 7
         * unit : days
         * verifyType :
         * verifyIntro :
         * isFollow : 0
         * distance :
         * fansNum : 3347
         */

        public int id;
        public int level;
        public int hiding;
        public String nickName;
        public String avatar;
        public String intro;
        public int age;
        public int sex;
        public int status;
        public int affection;
        public String roleName;
        public int online;
        public int loginTime;
        public String unit;
        public String verifyType;
        public String verifyIntro;
        public int isFollow;
        public String distance;
        public int fansNum;
    }
}
