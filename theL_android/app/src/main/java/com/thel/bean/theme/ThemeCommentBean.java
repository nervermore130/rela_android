package com.thel.bean.theme;

import com.thel.bean.comment.CommentBean;
import com.thel.bean.comment.CommentVideoBean;
import com.thel.bean.comment.CommentVoiceBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.constants.MomentTypeConstants;

import java.io.Serializable;
import java.util.List;

/**
 * Created by waiarl on 2017/8/18.
 * 4.1.0 话题回复，源于CommentBean
 */
public class ThemeCommentBean implements Serializable {

    /**
     * moments的唯一标识
     */
    public String momentsId;

    /**
     * 用户id
     */
    public int userId;

    /**
     * 用户名
     */
    public String nickname;

    /**
     * 方头像地址
     */
    public String avatar;

    /**
     * 评论id，当在是话题下的评论时，这个commentId其实就是该评论对应的日志id
     */
    public String commentId;

    // 评论时间
    public String commentTime;

    // 评论内容
    public String commentText;

    // 评论对象
    public int toUserId;

    // 评论对象nickname
    public String toNickname;

    // 评论对象方头像地址
    public String toAvatar;

    // 评论类型,say 或 to,say是直接评论，to是对某人的评论进行评论
    public String sayType;

    // 评论类型，text、sticker
    public String commentType;

    /**
     * 话题日志下的评论会带表态数和评论数以及我的表态类型
     */
    public int winkNum;
    public int commentNum;// 在评论二级页面，commentNum就是二级评论的总数
    public int winkFlag;

    /**
     * 最新的三条评论，存储为json数组字符串，取出来之后再解析
     */
    public List<CommentBean> commentList;

    /**
     * 话题日志的评论其实就是一条日志，所以有图片
     */
    public String imageUrl;
    /**
     * relaId
     */
    public String userName;
    /**
     * 等级
     */
    public int level;

    /**
     * 是否关注了该用户 0:没关注过，1:关注过
     */
    public int followerStatus;
    public static final int FOLLOW_TYPE_UNFOLLOWED = 0;// 未关注
    public static final int FOLLOW_TYPE_FOLLOWED = 1;// 已关注

    public static final String SAY_TYPE_SAY = "say";
    public static final String SAY_TYPE_TO = "to";

    public static final String COMMENT_TYPE_TEXT = "text";
    public static final String COMMENT_TYPE_STICKER = "sticker";
    public static final String COMMENT_TYPE_THEME_REPLY = "themereply";

    public int contentStatus = MomentsBean.CONTENT_STATUS_HANG;//默认收起状态

    public int textLines = 0;

    /**
     * 4.1.0 话题评论的类型 voice:音乐；video:视频
     */
    public String commentTypeClass;

    /**
     * themeReplyClass 参数解释：
     */
    public static final String TYPE_CLASS_TEXT = "text";//文本
    public static final String TYPE_CLASS_IMAGE = "image";//图文
    public static final String TYPE_CLASS_VIDEO = "video";//视频
    public static final String TYPE_CLASS_VOICE = "voice";//音乐


    /**
     * 4.1.0 音乐数据
     */
    public CommentVoiceBean voiceComment;

    /**
     * 4.1.0 视频数据
     */
    public CommentVideoBean videoComment;

    public static MomentsBean themeCommentBeanToMomentsBean(ThemeCommentBean themeCommentBean) {
        if (themeCommentBean == null)
            return null;
        MomentsBean momentsBean = new MomentsBean();
        momentsBean.momentsId = themeCommentBean.commentId;
        momentsBean.momentsText = themeCommentBean.commentText;
        momentsBean.momentsTime = themeCommentBean.commentTime;
        momentsBean.userId = themeCommentBean.userId;
        momentsBean.userName = themeCommentBean.userName;
        momentsBean.nickname = themeCommentBean.nickname;
        momentsBean.avatar = themeCommentBean.avatar;
        momentsBean.winkNum = themeCommentBean.winkNum;
        momentsBean.winkFlag = themeCommentBean.winkFlag;
        momentsBean.momentsNum = themeCommentBean.commentNum;
        momentsBean.commentList = themeCommentBean.commentList;
        momentsBean.imageUrl = themeCommentBean.imageUrl;
        momentsBean.thumbnailUrl = themeCommentBean.imageUrl;
        momentsBean.level = themeCommentBean.level;
        momentsBean.followerStatus = themeCommentBean.followerStatus;
        momentsBean.contentStatus = themeCommentBean.contentStatus;
        momentsBean.textLines = themeCommentBean.textLines;
        momentsBean.commentId = themeCommentBean.commentId;
        if (CommentBean.TYPE_CLASS_TEXT.equals(themeCommentBean.commentTypeClass)) {//文本
            momentsBean.momentsType = MomentTypeConstants.MOMENT_TYPE_TEXT;
        } else if (CommentBean.TYPE_CLASS_IMAGE.equals(themeCommentBean.commentTypeClass)) {//图文
            momentsBean.momentsType = MomentTypeConstants.MOMENT_TYPE_IMAGE;
        } else if (CommentBean.TYPE_CLASS_VIDEO.equals(themeCommentBean.commentTypeClass)) {//视频
            momentsBean.momentsType = MomentTypeConstants.MOMENT_TYPE_VIDEO;
        }
        if (themeCommentBean.voiceComment != null) {
            momentsBean.songId = themeCommentBean.voiceComment.songId;
            momentsBean.songName = themeCommentBean.voiceComment.songName;
            momentsBean.artistName = themeCommentBean.voiceComment.artistName;
            momentsBean.albumName = themeCommentBean.voiceComment.albumName;
            momentsBean.albumLogo100 = themeCommentBean.voiceComment.albumLogo100;
            momentsBean.albumLogo444 = themeCommentBean.voiceComment.albumLogo444;
            momentsBean.songLocation = themeCommentBean.voiceComment.songLocation;
            momentsBean.toURL = themeCommentBean.voiceComment.toURL;
        }
        if (themeCommentBean.videoComment != null) {
            momentsBean.videoUrl = themeCommentBean.videoComment.videoUrl;
            momentsBean.playTime = themeCommentBean.videoComment.playTime;
            momentsBean.mediaSize = themeCommentBean.videoComment.mediaSize;
            momentsBean.playCount = (int) themeCommentBean.videoComment.playCount;
            momentsBean.pixelWidth = themeCommentBean.videoComment.pixelWidth;
            momentsBean.pixelHeight = themeCommentBean.videoComment.pixelHeight;
        }
        return momentsBean;
    }


}
