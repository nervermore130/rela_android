package com.thel.bean;

import com.thel.base.BaseDataBean;

import java.util.ArrayList;

/**
 * 附近用户列表信息
 *
 * @author Setsail
 */
public class SearchUsersListBean extends BaseDataBean {

    public ArrayList<SearchBean> userList = new ArrayList<>();

}
