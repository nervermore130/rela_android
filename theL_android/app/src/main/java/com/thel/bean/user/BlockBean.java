package com.thel.bean.user;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseDataBean;

import java.util.ArrayList;
import java.util.Arrays;

public class BlockBean extends BaseDataBean {

    public int myUserId;
    public String userIntro;
    public int userId;
    public String userName;
    public String avatar;
    public String userAvatarLocal;
    public int online;

    /**
     * 一下2.19.3新增属性
     */

    public int age;//年龄
    /**
     * 认证状态，是否是加V用户 0:否 1:个人V 2:企业V
     */
    public int verifyType;
    /**
     * 认证说明
     */
    public String verifyIntro = "";
    /**
     * 是否开启了隐身
     */
    public int hiding;
    /**
     * 会员等级
     */
    public int level;
    /**
     * 距离
     */
    public String distance = "0m";

    @Override
    public String toString() {
        return "BlockBean{" + "myUserId=" + myUserId + ", userIntro='" + userIntro + '\'' + ", userId=" + userId + ", userName='" + userName + '\'' + ", avatar='" + avatar + '\'' + ", userAvatarLocal='" + userAvatarLocal + '\'' + ", online=" + online + ", age=" + age + ", verifyType=" + verifyType + ", verifyIntro='" + verifyIntro + '\'' + ", hiding=" + hiding + ", level=" + level + ", distance='" + distance + '\'' + ", affection=" + affection + ", roleName='" + roleName + '\'' + '}';
    }

    /**
     * 恋爱状态
     */
    public int affection;
    /**
     * 角色
     */
    public String roleName = "1";


    public String getLoginTimeShow() {
        StringBuilder str = new StringBuilder();
        if (level == 0 || hiding == 0) {// 如果是会员并且开了隐身功能，则不显示距离
            str.append(distance);
            str.append(", ");
        }
        str.append(age);
        str.append(TheLApp.getContext().getString(R.string.updatauserinfo_activity_age_unit));
        ArrayList<String> relationship_list = new ArrayList<>(Arrays.asList(TheLApp.getContext().getResources().getStringArray(R.array.userinfo_relationship))); // 感情状态
        str.append(", ");
        try {
            str.append(relationship_list.get(affection));
        } catch (Exception e) {
            str.append(relationship_list.get(0));
        }

        return str.toString();
    }

}

/*
 * "data": { "total": 1, "blackListUsers": [ { "userIntro": "模糊地迷恋你一场，就当风雨下潮涨。",
 * "userId": 8208, "userName": "林晓糖", "userAvatar":
 * "http://s-46344.gotocdn.com:8081/user-image/13550654615888208_head.jpg" } ]
 * },
 */