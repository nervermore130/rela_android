package com.thel.bean;

public class CDNSpeedBean {

    public String host;

    public String ip;

    public int port;

    public float time = -1;

    public String url;

    @Override public String toString() {
        return "CDNSpeedBean{" +
                "host='" + host + '\'' +
                ", ip='" + ip + '\'' +
                ", port='" + port + '\'' +
                ", time=" + time +
                ", url='" + url + '\'' +
                '}';
    }
}
