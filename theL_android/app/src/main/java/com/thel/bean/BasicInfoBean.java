package com.thel.bean;

import com.thel.base.BaseDataBean;
import com.thel.bean.user.VipConfigBean;
import com.thel.modules.live.bean.LiveRoomBean;

import java.io.Serializable;
import java.util.List;

/**
 * 基本数据对象
 *
 * @author Setsail
 */
public class BasicInfoBean extends BaseDataBean implements Serializable {

    /**
     * 我的会员等级
     */
    public int level;
    /**
     * 直播权限
     */
    public int perm;
    /**
     * 申请直播权限的网址
     */
    public String permApplyUrl;

    public int liveWidth;
    public int liveHeight;
    public int liveFps;
    public int lowestBps;
    public int liveBps;
    public int minFollowCount;
    public int defaultFollowCount;

    public LiveRoomBean currentLive;

    public int enablePubLive;
    public String topLink;
    public int onlyText;
    public int superLikeRetain;

    /**
     * 推流服务器列表
     */
    public List<String> rtmpUrls;

    /**
     * 视频直播拉流服务器列表
     */
    public List<IpsInfoBean> flvInfos;

    /**
     * 多人语音直播拉流服务器列表
     */
    public List<IpsInfoBean> multiFlvInfos;

    /**
     * 聊天服务器ip列表
     */
    public List<String> IMServer;

    /**
     * 观看直播默认切源排序
     */
    public int liveUrlSort;

    /**
     * 推流直播默认排序
     */
    public int rtmpUrlSort;

    /**
     * 是否使用https 0,1 1表示打开
     */
    public int httpsSwitch;

    /**
     * 是否显示我的等级里面专属发言背景以及40级
     * */
    public int levelPrivileges;


    @Override public String toString() {
        return "BasicInfoBean{" +
                "level=" + level +
                ", perm=" + perm +
                ", permApplyUrl='" + permApplyUrl + '\'' +
                ", liveWidth='" + liveWidth + '\'' +
                ", liveHeight='" + liveHeight + '\'' +
                ", liveFps='" + liveFps + '\'' +
                ", lowestBps='" + lowestBps + '\'' +
                ", liveBps='" + liveBps + '\'' +
                ", currentLive=" + currentLive +
                ", enablePubLive=" + enablePubLive +
                ", topLink='" + topLink + '\'' +
                ", onlyText=" + onlyText +
                '}';
    }

    public class IpsInfoBean {
        public String host = "";
        public String bucket = "";
        public String suffix = "";
        public String type = "";
    }

    public VipConfigBean.DataBean.VipSettingBean vipSetting;
}
