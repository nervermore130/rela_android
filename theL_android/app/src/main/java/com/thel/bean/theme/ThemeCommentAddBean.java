package com.thel.bean.theme;

import java.io.Serializable;

/**
 * Created by waiarl on 2017/8/24.
 * 话题回复时候，提交的数据类型
 * 为了以防万一需要本地存储，所以建造此对象
 */

public class ThemeCommentAddBean implements Serializable {
    public String momentsText;
    public String imageUrlList;
    public String momentsType;
    public String videoUrl;
    public int pixelWidth;
    public int pixelHeight;
    public int playTime;
    public long mediaSize;
    public String albumLogo100;
    public String albumLogo444;
    public String albumName;
    public String songName;
    public String songLocation;
    public String artistName;
    public String toURL;
    public long songId;
    public String themeReplyClass;
    public String parentId;
    //待上传的图片
    public String imgsToUpload;
    // 预发布日志已上传成功的图片地址
    public String uploadedImgsUrls = "";
    public String videoWebp;
    public String videoColor;

    public ThemeCommentAddBean() {
    }

    public ThemeCommentAddBean(String momentsText, String imageUrlList, String momentsType, String videoUrl, int pixelWidth, int pixelHeight, int playTime, long mediaSize, String albumLogo100, String albumLogo444, String albumName, String songName, String songLocation, String artistName, String toURL, long songId, String themeReplyClass, String parentId, String imgsToUpload) {
        this.momentsText = momentsText;
        this.imageUrlList = imageUrlList;
        this.momentsType = momentsType;
        this.videoUrl = videoUrl;
        this.pixelWidth = pixelWidth;
        this.pixelHeight = pixelHeight;
        this.playTime = playTime;
        this.mediaSize = mediaSize;
        this.albumLogo100 = albumLogo100;
        this.albumLogo444 = albumLogo444;
        this.albumName = albumName;
        this.songName = songName;
        this.songLocation = songLocation;
        this.artistName = artistName;
        this.toURL = toURL;
        this.songId = songId;
        this.themeReplyClass = themeReplyClass;
        this.parentId = parentId;
        this.imgsToUpload = imgsToUpload;
    }

    @Override
    public String toString() {
        return "ThemeCommentAddBean{" +
                "momentsText='" + momentsText + '\'' +
                ", imageUrlList='" + imageUrlList + '\'' +
                ", momentsType='" + momentsType + '\'' +
                ", videoUrl='" + videoUrl + '\'' +
                ", pixelWidth=" + pixelWidth +
                ", pixelHeight=" + pixelHeight +
                ", playTime=" + playTime +
                ", mediaSize=" + mediaSize +
                ", albumLogo100='" + albumLogo100 + '\'' +
                ", albumLogo444='" + albumLogo444 + '\'' +
                ", albumName='" + albumName + '\'' +
                ", songName='" + songName + '\'' +
                ", songLocation='" + songLocation + '\'' +
                ", artistName='" + artistName + '\'' +
                ", toURL='" + toURL + '\'' +
                ", songId=" + songId +
                ", themeReplyClass='" + themeReplyClass + '\'' +
                ", parentId='" + parentId + '\'' +
                ", imgsToUpload='" + imgsToUpload + '\'' +
                ", uploadedImgsUrls='" + uploadedImgsUrls + '\'' +
                '}';
    }
}
