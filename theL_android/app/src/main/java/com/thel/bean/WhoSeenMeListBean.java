package com.thel.bean;

import com.thel.base.BaseDataBean;

import java.util.ArrayList;
import java.util.List;

public class WhoSeenMeListBean extends BaseDataBean {

    public int total;

    /**
     * 近七天内看过我的数量
     */
    public int viewCount;

    public List<WhoSeenMeBean> map_list = new ArrayList<>();

    public List<MoreGirlsBean> moreGirls;


    public class MoreGirlsBean {
        public long userId;
        public String nickName;
        public String avatar;
    }

}
