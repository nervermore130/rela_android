package com.thel.bean;

import com.thel.base.BaseDataBean;

/**
 * Created by chad
 * Time 18/9/12
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */
public class SuperlikeBean extends BaseDataBean{

    public Data data;

    public class Data {
        public int success;// 1 表示匹配成功
        public int superLikeSuccess;// 1 表示超级喜欢成功
        public int superlikeRetain;// 剩余可用数
        public int userId;
        public String nickname;
        public String avatar;

    }
}
