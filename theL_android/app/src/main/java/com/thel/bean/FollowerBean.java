package com.thel.bean;

import com.thel.R;
import com.thel.app.TheLApp;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author Setsail
 */
public class FollowerBean {

    public int userId;
    public String nickName;
    public String Avatar;
    public int online;
    /**
     * 0 互相未关注；
     * 1 我关注对方；
     * 2 对方关注我；
     * 3 互相关注；
     */
    public int followStatus;
    // 新接口新增字段
    public String roleName;
    // days 和 hours,和loginTime组合使用，标识几天前登陆或几小时前登陆
    public String unit;
    public String loginTime;
    public String distance;
    public String intro;

    /**
     * 认证状态，是否是加V用户 0:否 1:个人V 2:企业V
     */
    public int verifyType;
    /**
     * 认证说明
     */
    public String verifyIntro;
    /**
     * 年龄
     */
    public int age;
    /**
     * 是否开启了隐身
     */
    public int hiding;
    /**
     * 会员等级
     */
    public int level;
    /**
     * 感情状态
     */
    public int affection;


 /*   public String getLoginTimeShow() {
        StringBuilder str = new StringBuilder();
        if (level == 0 || hiding == 0) {// 如果是会员并且开了隐身功能，则不显示距离
            str.append(distance);
            str.append(", ");
        }
        str.append(age);
        str.append(TheLApp.getContext().getString(R.string.updatauserinfo_activity_age_unit));
        ArrayList<String> relationship_list = new ArrayList<>(Arrays.asList(TheLApp.getContext().getResources().getStringArray(R.array.userinfo_relationship))); // 感情状态
        str.append(", ");
        try {
            str.append(relationship_list.get(affection));
        } catch (Exception e) {
            str.append(relationship_list.get(0));
        }

        return str.toString();
    }
*/
    @Override
    public String toString() {
        return "FollowerBean{" +
                "userId=" + userId +
                ", nickName='" + nickName + '\'' +
                ", Avatar='" + Avatar + '\'' +
                ", online=" + online +
                ", roleName='" + roleName + '\'' +
                ", unit='" + unit + '\'' +
                ", loginTime='" + loginTime + '\'' +
                ", distance='" + distance + '\'' +
                ", intro='" + intro + '\'' +
                ", verifyType=" + verifyType +
                ", verifyIntro='" + verifyIntro + '\'' +
                ", age=" + age +
                ", hiding=" + hiding +
                ", level=" + level +
                ", affection=" + affection +
                '}';
    }
}