package com.thel.bean;

import com.thel.base.BaseDataBean;
import com.thel.bean.user.SimpleUserBean;

import java.io.Serializable;

/**
 * Created by chad
 * Time 17/11/17
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */

public class SimpleUserDataBean extends BaseDataBean implements Serializable {

    public SimpleUserBean data;

    @Override
    public String toString() {
        return "SimpleUserDataBean{" +
                "data=" + data +
                '}';
    }
}
