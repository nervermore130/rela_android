package com.thel.bean;

import java.io.Serializable;

public class ContactBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户id
     */
    public String userId = "";

    /**
     * 用户名
     */
    public String nickName = "";

    /**
     * 用户名(拼音，用于排序)
     */
    public String nickNamePinYin = "";

    /**
     * 头像
     */
    public String avatar = "";

    /**
     * 背景图，密友功能需要
     */
    public String bgImage = "";

    // 是否被选中
    public boolean isSelected = false;

}
