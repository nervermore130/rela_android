package com.thel.bean;

import java.io.Serializable;

/**
 * Created by chad
 * Time 17/12/1
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */

public class SharePosterBean implements Serializable{

    public int from;
    public String avatar;
    public String nickname;
    public String userName;
    public String imageUrl;
    public String momentsText;
    public String userId;
}
