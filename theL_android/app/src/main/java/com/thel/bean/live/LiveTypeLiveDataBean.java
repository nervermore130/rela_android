package com.thel.bean.live;

import com.thel.base.BaseDataBean;

import java.util.List;

/**
 * Created by lingwei on 2017/11/19.
 */

public class LiveTypeLiveDataBean extends BaseDataBean {

    /**
     * data : {"cursor":0,"list":[{"id":"88888101735266","user":{"id":101735266,"level":4,"hiding":1,"nickName":"2狗子","avatar":"http://static.thel.co/app/avatar/101735266/389ee5bf3fcee853b9acde634f2cea9b.jpg-wh150","intro":"微信：d2gd2gd2g\n微博：我叫2狗子  \n淘宝：D2g不一样的中性美","age":100,"sex":0,"status":1,"affection":1,"roleName":"0","online":0,"loginTime":9,"unit":"hours","verifyType":"","verifyIntro":"","isFollow":0,"distance":""},"active":1,"atUserList":[],"text":"不要恋爱 一直酷下去","liveUrl":"http://flv.live-baidu.rela.me/live/101735266.flv","imageUrl":"http://static.rela.me/app/live/101735266/eb3468e81c368c7e2faf8dd51ecb9589.jpg","badge":"https://pro.rela.me/app/icn_live-rank1.png","liveUsersCount":"339","liveStatus":0,"label":""}]}
     */


    /**
     * cursor : 0
     * list : [{"id":"88888101735266","user":{"id":101735266,"level":4,"hiding":1,"nickName":"2狗子","avatar":"http://static.thel.co/app/avatar/101735266/389ee5bf3fcee853b9acde634f2cea9b.jpg-wh150","intro":"微信：d2gd2gd2g\n微博：我叫2狗子  \n淘宝：D2g不一样的中性美","age":100,"sex":0,"status":1,"affection":1,"roleName":"0","online":0,"loginTime":9,"unit":"hours","verifyType":"","verifyIntro":"","isFollow":0,"distance":""},"active":1,"atUserList":[],"text":"不要恋爱 一直酷下去","liveUrl":"http://flv.live-baidu.rela.me/live/101735266.flv","imageUrl":"http://static.rela.me/app/live/101735266/eb3468e81c368c7e2faf8dd51ecb9589.jpg","badge":"https://pro.rela.me/app/icn_live-rank1.png","liveUsersCount":"339","liveStatus":0,"label":""}]
     */

    public int cursor;
    public List<ListBean> list;

    public static class ListBean {
        /**
         * id : 88888101735266
         * user : {"id":101735266,"level":4,"hiding":1,"nickName":"2狗子","avatar":"http://static.thel.co/app/avatar/101735266/389ee5bf3fcee853b9acde634f2cea9b.jpg-wh150","intro":"微信：d2gd2gd2g\n微博：我叫2狗子  \n淘宝：D2g不一样的中性美","age":100,"sex":0,"status":1,"affection":1,"roleName":"0","online":0,"loginTime":9,"unit":"hours","verifyType":"","verifyIntro":"","isFollow":0,"distance":""}
         * active : 1
         * atUserList : []
         * text : 不要恋爱 一直酷下去
         * liveUrl : http://flv.live-baidu.rela.me/live/101735266.flv
         * imageUrl : http://static.rela.me/app/live/101735266/eb3468e81c368c7e2faf8dd51ecb9589.jpg
         * badge : https://pro.rela.me/app/icn_live-rank1.png
         * liveUsersCount : 339
         * liveStatus : 0
         * label :
         */

        public String id;
        public UserBean user;
        public int active;
        public String text;
        public String liveUrl;
        public String imageUrl;
        public String badge;
        public String liveUsersCount;
        public int liveStatus;
        public String label;
        public List<?> atUserList;

        public static class UserBean {
            /**
             * id : 101735266
             * level : 4
             * hiding : 1
             * nickName : 2狗子
             * avatar : http://static.thel.co/app/avatar/101735266/389ee5bf3fcee853b9acde634f2cea9b.jpg-wh150
             * intro : 微信：d2gd2gd2g
             * 微博：我叫2狗子
             * 淘宝：D2g不一样的中性美
             * age : 100
             * sex : 0
             * status : 1
             * affection : 1
             * roleName : 0
             * online : 0
             * loginTime : 9
             * unit : hours
             * verifyType :
             * verifyIntro :
             * isFollow : 0
             * distance :
             */

            public int id;
            public int level;
            public int hiding;
            public String nickName;
            public String avatar;
            public String intro;
            public int age;
            public int sex;
            public int status;
            public int affection;
            public String roleName;
            public int online;
            public int loginTime;
            public String unit;
            public String verifyType;
            public String verifyIntro;
            public int isFollow;
            public String distance;
        }
    }
}
