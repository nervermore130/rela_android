package com.thel.bean;

import com.thel.base.BaseDataBean;

public class ReleasedCommentBean extends BaseDataBean {
    public String momentsId;
    public String commentId;
    /**
     * 在话题日志下发布一个评论（实则是日志），生成的的日志id就存储在这里
     */
    public String subMomentId;

}
