package com.thel.bean.gift;

import java.io.Serializable;

/**
 * Created by waiarl on 2017/4/11.
 */

public class WinkGiftBean implements Serializable {
    public String id;
    public String title;
    public int price;
    public String icon;
    public int popularity;

}
