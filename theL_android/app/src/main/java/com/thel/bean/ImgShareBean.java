package com.thel.bean;

import com.thel.base.BaseDataBean;
import com.thel.utils.JsonUtils;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by test1 on 2017/5/16.
 */

public class ImgShareBean extends BaseDataBean implements Serializable {
    /**
     * 昵称
     */
    public String nickName;

    /**
     * 用户头像
     */
    public String picUserUrl;
    /**
     * 用户relaiD
     */
    public String userID;
    /**
     * 内容
     */
    public String commentText;
    // moment图片内容
    public String imageUrl;

    /**
     * 从json对象封装对象
     *
     * @param tempobj
     */
    public void fromJson(JSONObject tempobj) {
        nickName = JsonUtils.getString(tempobj, "nickName", "");
        picUserUrl = JsonUtils.getString(tempobj, "picUserUrl", "");
        userID = JsonUtils.getString(tempobj, "userID", "");
        commentText = JsonUtils.getString(tempobj, "commentText", "");
        imageUrl = JsonUtils.getString(tempobj, "imageUrl", "");

    }

    @Override
    public String toString() {
        return "imageShareBean{" +
                "nickName='" + nickName + '\'' +
                ", picUserUrl='" + picUserUrl + '\'' +
                ", userID='" + userID + '\'' +
                '}';
    }
}
