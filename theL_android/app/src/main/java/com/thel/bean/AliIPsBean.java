package com.thel.bean;

import java.util.List;

public class AliIPsBean {

    public int state;

    public AliIPsContentBean content;

    public static class AliIPsContentBean {
        public List<AliIPsItemBean> list;
    }

    public static class AliIPsItemBean {
        public String ip;
        public int port;
    }

}
