package com.thel.bean;

import com.google.gson.reflect.TypeToken;
import com.thel.bean.analytics.AnalyticsBean;
import com.thel.utils.GsonUtils;
import com.thel.utils.ShareFileUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author liuyun
 * @date 2018/3/11
 */

public class LiveInfoLogBean {

    public static final String CHANGE_CDN_DEFAULT = "使用IP";

    public static final String CHANGE_CDN_USE_IP = "使用IP_切源";

    public static final String CHANGE_CDN_IP_IS_NOT = "IP为空_使用域名_切源";

    public static final String CHANGE_CDN_USE_HOST = "IP轮询完_使用域名_切源";

    public static final String CHANGE_CDN_USE_OTHER = "服务端下发_使用域名_切源";

    public static final String IM_ANALYTICS = "im_analytics";

    public static final String LIVE_CHAT_ANALYTICS = "host_analytics";

    public static final String LIVE_PLAY_ANALYTICS = "audience_analytics";

    public static final String LIVE_TRACE_ROUTE_ANALYTICS = "live_trace_route_analytics";

    public static final String LIVE_AGORA_ANALYTICS = "live_trace_route_analytics";

    public static final String LIVE_MSG_LOG = "live_msg_log";

    public static final String LIVE_IM_NOT_MATCH = "liveImNotMatch";

    /**
     * 直播间用户说卡时上报数据
     */
    public static final String LIVE_USER_MSG_LOW_SPEED = "live_user_msg_low_speed";

    public static final String RELEASE_MOMENT_FAILED = "release_moment_failed";

    private Map<String, AnalyticsBean> analyticsBeanMap = new HashMap<>();

    private static LiveInfoLogBean instance;

    private LiveInfoLogBean() {

        analyticsBeanMap.put(IM_ANALYTICS, new AnalyticsBean());

        analyticsBeanMap.put(LIVE_CHAT_ANALYTICS, new AnalyticsBean());

        analyticsBeanMap.put(LIVE_PLAY_ANALYTICS, new AnalyticsBean());

        analyticsBeanMap.put(LIVE_TRACE_ROUTE_ANALYTICS, new AnalyticsBean());

        analyticsBeanMap.put(LIVE_AGORA_ANALYTICS, new AnalyticsBean());

        analyticsBeanMap.put(LIVE_USER_MSG_LOW_SPEED, new AnalyticsBean());

        analyticsBeanMap.put(RELEASE_MOMENT_FAILED, new AnalyticsBean());

        analyticsBeanMap.put(LIVE_MSG_LOG, new AnalyticsBean());

        analyticsBeanMap.put(LIVE_IM_NOT_MATCH, new AnalyticsBean());

    }

    public static LiveInfoLogBean getInstance() {

        if (instance == null) {
            instance = new LiveInfoLogBean();
        }

        return instance;

    }

    public void clear() {
        if (instance != null) {
            instance = null;
        }

        if (analyticsBeanMap != null) {
            analyticsBeanMap.clear();
        }
    }

    public AnalyticsBean getImAnalytics() {
        if (analyticsBeanMap.get(IM_ANALYTICS) == null) {
            analyticsBeanMap.put(IM_ANALYTICS, new AnalyticsBean());
        }
        return analyticsBeanMap.get(IM_ANALYTICS);
    }

    public AnalyticsBean getLivePlayAnalytics() {
        if (analyticsBeanMap.get(LIVE_PLAY_ANALYTICS) == null) {
            analyticsBeanMap.put(LIVE_PLAY_ANALYTICS, new AnalyticsBean());
        }
        return analyticsBeanMap.get(LIVE_PLAY_ANALYTICS);
    }

    public AnalyticsBean getLiveChatAnalytics() {
        if (analyticsBeanMap.get(LIVE_CHAT_ANALYTICS) == null) {
            analyticsBeanMap.put(LIVE_CHAT_ANALYTICS, new AnalyticsBean());
        }
        return analyticsBeanMap.get(LIVE_CHAT_ANALYTICS);
    }

    public AnalyticsBean getLiveTraceRouteAnalytics() {
        if (analyticsBeanMap.get(LIVE_TRACE_ROUTE_ANALYTICS) == null) {
            analyticsBeanMap.put(LIVE_TRACE_ROUTE_ANALYTICS, new AnalyticsBean());
        }
        return analyticsBeanMap.get(LIVE_TRACE_ROUTE_ANALYTICS);
    }

    public AnalyticsBean getAgoraAnalytics() {
        if (analyticsBeanMap.get(LIVE_AGORA_ANALYTICS) == null) {
            analyticsBeanMap.put(LIVE_AGORA_ANALYTICS, new AnalyticsBean());
        }
        return analyticsBeanMap.get(LIVE_AGORA_ANALYTICS);
    }

    public AnalyticsBean getLiveLowSpeedAnalytics() {
        if (analyticsBeanMap.get(LIVE_USER_MSG_LOW_SPEED) == null) {
            analyticsBeanMap.put(LIVE_USER_MSG_LOW_SPEED, new AnalyticsBean());
        }
        return analyticsBeanMap.get(LIVE_USER_MSG_LOW_SPEED);
    }

    public AnalyticsBean getReleaseMomentFailedAnalytics() {
        if (analyticsBeanMap.get(RELEASE_MOMENT_FAILED) == null) {
            analyticsBeanMap.put(RELEASE_MOMENT_FAILED, new AnalyticsBean());
        }
        return analyticsBeanMap.get(RELEASE_MOMENT_FAILED);
    }

    public AnalyticsBean getLiveMsgLogAnalytics() {
        if (analyticsBeanMap.get(LIVE_MSG_LOG) == null) {
            analyticsBeanMap.put(LIVE_MSG_LOG, new AnalyticsBean());
        }
        return analyticsBeanMap.get(LIVE_MSG_LOG);
    }

    public AnalyticsBean getLiveImNotMatchAnalytics() {
        if (analyticsBeanMap.get(LIVE_IM_NOT_MATCH) == null) {
            analyticsBeanMap.put(LIVE_IM_NOT_MATCH, new AnalyticsBean());
        }
        return analyticsBeanMap.get(LIVE_IM_NOT_MATCH);
    }

    public void clearReleaseMoment() {
        try {
            analyticsBeanMap.remove(RELEASE_MOMENT_FAILED);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearLiveMsgLog() {
        try {
            analyticsBeanMap.remove(LIVE_MSG_LOG);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cache(String log) {
        try {

            if (log == null) {
                return;
            }

            AnalyticsBean analyticsBean = GsonUtils.getObject(log, AnalyticsBean.class);

            List<AnalyticsBean> analyticsBeans = getLiveLogCache();

            if (analyticsBeans == null) {
                analyticsBeans = new ArrayList<>();
            }
            analyticsBeans.add(analyticsBean);
            save(analyticsBeans);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void removeCache() {
        ShareFileUtils.setString(ShareFileUtils.LIVE_LOG, "[]");
    }

    public List<AnalyticsBean> getLiveLogCache() {
        String liveLog = ShareFileUtils.getString(ShareFileUtils.LIVE_LOG, "[]");
        return GsonUtils.getGson().fromJson(liveLog, new TypeToken<List<AnalyticsBean>>() {
        }.getType());
    }

    private void save(List<AnalyticsBean> analyticsBeans) {
        if (analyticsBeans == null) {
            return;
        }
        String liveLog = GsonUtils.createJsonString(analyticsBeans);
        ShareFileUtils.setString(ShareFileUtils.LIVE_LOG, liveLog);
    }

}