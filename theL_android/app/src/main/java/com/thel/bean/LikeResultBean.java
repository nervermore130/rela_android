package com.thel.bean;

import com.thel.base.BaseDataBean;

public class LikeResultBean extends BaseDataBean {

    /**
     * 单向like成功：“0” 或 互相like成功：“1”
     */
    public LikeResultDataBean data;

    public class LikeResultDataBean {

        public String likeSuccess;
        public String avatar;
        public Long userId;

        @Override
        public String toString() {
            return "LikeResultDataBean{" + "likeSuccess='" + likeSuccess + '\'' + ", avatar='" + avatar + '\'' + ", userId=" + userId + '}';
        }
    }

}