package com.thel.bean.me;


import com.thel.base.BaseDataBean;

import java.io.Serializable;
import java.util.ArrayList;

public class MyCircleFriendListBean extends BaseDataBean {

    /**
     * 伴侣对象
     */
    public MyCircleFriendListDataBean data;

    public class MyCircleFriendListDataBean implements Serializable {

        public Paternerd paternerd;

        /**
         * 闺蜜列表
         */
        public ArrayList<MyCircleFriendBean> bffs = new ArrayList<>();

    }

    @Override
    public String toString() {
        return "MyCircleFriendListBean{" + "data=" + data + '}';
    }

    public class Paternerd implements Serializable {

        public long requestId;
        /**
         * 请求状态，-3=解除绑定, －2=取消，－1=拒绝，0=未处理，1=同意，2=过期*
         */
        public int requestStatus;
        /**
         * 我的id
         */
        public long userId;
        /**
         * 伴侣的id
         */
        public long paternerId;
        /**
         * 伴侣的背景图片
         */
        public String bgImage;
        public String userNickName;
        public String paternerNickName;// 后台拼错了我们保持一致...
        public String userAvatar;
        public String paternerAvatar;
        /**
         * 结为伴侣多少天了
         */
        public String days;
        /**
         * 距离对方的生日的天数，后台接口拼写错了，我们跟他保持一致...
         */
        public String reminday;
        /**
         * 生日 yyyy-MM-dd
         */
        public String paternerBirthday;

        /**
         * 是否是添加
         */
        public boolean isAdd = false;

        @Override
        public String toString() {
            return "MyCirclePartnerBean{" + "requestId=" + requestId + ", requestStatus=" + requestStatus + ", userId=" + userId + ", paternerId=" + paternerId + ", bgImage='" + bgImage + '\'' + ", userNickName='" + userNickName + '\'' + ", paternerNickName='" + paternerNickName + '\'' + ", userAvatar='" + userAvatar + '\'' + ", paternerAvatar='" + paternerAvatar + '\'' + ", days='" + days + '\'' + ", reminday='" + reminday + '\'' + ", paternerBirthday='" + paternerBirthday + '\'' + ", isAdd=" + isAdd + ", online='" + online + '\'' + ", myCircleFriendBeanArrayList=" + '}';
        }

        public String online;
        public String distance;
    }
}