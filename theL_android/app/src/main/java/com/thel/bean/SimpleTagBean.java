package com.thel.bean;


import com.thel.utils.JsonUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Simple Topic bean
 *
 * @author Setsail
 */
public class SimpleTagBean implements Serializable {

    /**
     * 标签id
     */
    public long tagId;

    /**
     * 标签名
     */
    public String tagsName;

    /**
     * 标签下最热门的一个日志的图片
     */
    public String bgImg;

    public List<SimpleMomentBean> momentList = new ArrayList<>();

    /**
     * 标签的关注人数
     */
    public int followerNum;

    /**
     * 标签下的总日志数
     */
    public int totalMomentNum;

    /**
     * 新增日志数
     */
    public int noReadNum;

    /**
     * 标签类型，热门标签：popular
     */
    public String tagType;
    public boolean isFirstPopTag = false;// 是不是第一个热门标签，如果是的话界面不一样

    /**
     * 从json对象封装对象
     *
     * @param jsonObject
     */
    public void fromJson(JSONObject jsonObject) {
        try {
            tagId = JsonUtils.getLong(jsonObject, "tagId", 0);
            tagsName = JsonUtils.getString(jsonObject, "tagsName", "");
            bgImg = JsonUtils.getString(jsonObject, "bgImg", "");
            followerNum = JsonUtils.getInt(jsonObject, "followerNum", 0);
            totalMomentNum = JsonUtils.getInt(jsonObject, "totalMomentNum", 0);
            noReadNum = JsonUtils.getInt(jsonObject, "noReadNum", 0);
            tagType = JsonUtils.getString(jsonObject, "tagType", "");

            // 标签下的热门日志
            JSONArray momentsArr = JsonUtils.getJSONArray(jsonObject, "momentList");
            SimpleMomentBean momentBean = null;
            for (int i = 0; i < momentsArr.length(); i++) {
                momentBean = new SimpleMomentBean();
                JSONObject tempobj = momentsArr.getJSONObject(i);

                momentBean.fromJson(tempobj);

                momentList.add(momentBean);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
