package com.thel.bean;

import java.util.List;

/**
 * Created by chad
 * Time 19/2/25
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */
public class AwaitLiveBean {
    public Data data;

    public class Data {
        public String result;

        public String errcode;

        public String errdesc;

        public String errdesc_en;

        public List<Users> users ;

        public int cursor;

        public boolean haveNextPage;
    }

    public class Users {
        public int userId;

        public String nickName;

        public String avatar;

        public int id;

        public String intro;

        public int online;

        public String roleName;

        public String affection;

        public String distance;

        public int verifyType;

        public String verifyIntro;

        public int age;

        public int level;

        public int autoSubscribe;

        public int hiding;

        public int isFollow;

        public int followStatus;

        public String createDate;

        public String createTime;

        public String Avatar;

        public String create_date;

        public String userIntro;

        public int secretly;

        public String userName;

        public boolean awaitStatus;
    }
}
