package com.thel.bean.user;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.AdBean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 附近用户个人信息
 *
 * @author Setsail
 */
public class NearUserBean {
    /**
     * 埋点
     * */
    public String rank_id = "";

    /**
     * 图片高度(下载图片前布局用)
     */
    public int imageHeight;

    /**
     * 图片宽度(下载图片前布局用)
     */
    public int imageWidth;

    /**
     * 距离
     */
    public String distance;

    /**
     * 图片地址
     */
    public String picUrl;

    /**
     * 用户id
     */
    public int userId;

    /**
     * 是否在线( 0 不在,1 在)
     */
    public int online;

    /**
     * 上次登录时间
     */
    public String loginTime;

    /**
     * 年龄
     */
    public String age;

    /**
     * 取向
     */
    public String roleName;

    /**
     * 签名
     */
    public String intro;

    /**
     * 用户名
     */
    public String nickname;

    /**
     * 在线时段
     */
    public String online_time;

    /**
     * 方头像地址
     */
    public String avatar;

    /**
     * 上次登录时间
     */
    public String loginTime2; // "4"、"5"
    // days 和 hours,和loginTime2组合使用，标识几天前登陆或几小时前登陆
    public String unit;
    /**
     * 恋爱状态
     */
    public String affection;

    // 附近,在线,在线,明星
    public String filterType;

    // 请求时间戳
    public String timestamp;

    /**
     * 认证状态，是否是加V用户 0:否 1:个人V 2:企业V
     */
    public int verifyType;
    /**
     * 认证说明
     */
    public String verifyIntro;
    /**
     * vip等级
     */
    public int level;
    /**
     * 是否在直播，0=未直播　１=直播 2=推荐主播
     */
    public int live;
    /*
     * 直播时观看的观众数
     * */
    public int liveLooker;
    /**
     * 直播时观众数人数的排名
     */
    public int liveRank;
    /**
     * 直播描述
     */
    public String liveText;

    /***4.5.0 是否隐身 1=隐身 0或不存在此字段=不隐身 4.5版本新增***/
    public int hiding = 0;

    /***0=普通直播 1=声音直播   4.6新增***/
    public String liveType = "";

    public String professionalTypes;

    /**
     * 0为普通item，1为人气明星入口，2为banner
     */
    public int itemType;

    public List<AdBean.Map_list> adList;

    public NearUserListNetBean.HsImagesBean hs_images;

    public static final String LIVE_TYPE_NORMAL = "0";//直播类型，普通直播

    public static final String LIVE_TYPE_VOICE = "1";//直播类型，声音直播

    public String getLoginTimeShow() {
        StringBuilder str = new StringBuilder();
        str.append(distance);
        str.append(", ");
        str.append(age);
        str.append(TheLApp.getContext().getString(R.string.updatauserinfo_activity_age_unit));
        ArrayList<String> relationship_list = new ArrayList<>(Arrays.asList(TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship))); // 感情状态
        str.append(", ");
        try {
            str.append(relationship_list.get(Integer.valueOf(affection)));
        } catch (Exception e) {
            str.append(relationship_list.get(0));
        }

        return str.toString();
    }

}
