package com.thel.bean.theme;

import com.thel.R;
import com.thel.app.TheLApp;

import java.io.Serializable;

/**
 * Created by test1 on 2017/6/7.
 */

public class ThemeBean implements Serializable {
    public String id;
    public String text;
    public String image;
    public int joinTotal;

    @Override
    public String toString() {
        return "ThemeBean{" +
                "id='" + id + '\'' +
                ", text='" + text + '\'' +
                ", image='" + image + '\'' +
                ", joinTotal=" + joinTotal +
                '}';
    }

    public String buildDesc() {
        String joins;
        if (joinTotal > 1) {
            joins = TheLApp.getContext().getString(R.string.hot_themes_join_even, joinTotal);
        } else {
            joins = TheLApp.getContext().getString(R.string.hot_themes_join_odd, joinTotal);
        }
        return joins;
    }

}