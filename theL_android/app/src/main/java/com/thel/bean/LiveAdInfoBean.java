package com.thel.bean;

import java.util.List;

/**
 *
 * @author liuyun
 * @date 2017/12/17
 */

public class LiveAdInfoBean {
    public int status;

    //该字段为5.2.0新增，用list替换之前的单个数据
    public List<LiveAdCellBean> list;

    //以下为5.1.0,直播盛典暂时
    public String icon;
    public String url;
    public String rank;
    public String offset;

    @Override
    public String toString() {
        return "LiveAdInfoBean{" +
                "status=" + status +
                ", list=" + list +
                ", icon='" + icon + '\'' +
                ", url='" + url + '\'' +
                ", rank='" + rank + '\'' +
                ", offset='" + offset + '\'' +
                '}';
    }

    public static class LiveAdCellBean {
        public String icon;
        public String url;
        public String rank;
        public String offset;

        @Override
        public String toString() {
            return "LiveAdCellBean{" +
                    "icon='" + icon + '\'' +
                    ", url='" + url + '\'' +
                    ", rank='" + rank + '\'' +
                    ", offset='" + offset + '\'' +
                    '}';
        }
    }
}
