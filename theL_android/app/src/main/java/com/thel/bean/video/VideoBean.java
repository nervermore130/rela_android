package com.thel.bean.video;

import androidx.annotation.Nullable;
import android.text.TextUtils;

import com.thel.base.BaseDataBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.constants.TheLConstants;

import java.io.Serializable;

/**
 * @author the L
 * @date 2017/2/10
 */

public class VideoBean extends BaseDataBean implements Serializable {
    /**
     * id
     */
    public String id;

    /**
     * 简介
     */
    public String text;

    /**
     * 视频缩略图
     */
    public String image;

    /**
     * 视频宽度
     */
    public int pixelWidth;

    /**
     * 视频高度
     */
    public int pixelHeight;

    /**
     * 视频地址
     */
    public String videoUrl;

    /**
     * 时长
     */
    public int playTime;

    /**
     * 播放次数
     */
    public int playCount;
    /**
     * 是否点赞
     */
    public int winkFlag;
    /**
     * 喜欢次数
     */
    public int winkNum;

    /**
     * 观看人数
     */
    public int liveStatus = -1;

    /**
     * 评论次数
     */
    public int commentNum;
    public static final int HAS_NOT_WINKED = 0;
    public static final int HAS_BEAN_WINKED = 1;

    public boolean isPlaying = false;//是否在播放
    public int currentPosition = -1;//当前播放duration

    /***一下为4.7.0新增属性***/

    /*** 新增视频日志webp***/
    public String videoWebp;

    /***新增视频日志社快***/
    public String videoColor;

    /***新增视频推荐标签类型***/
    public int videoTag;

    public String type;

    public long mediaSize;

    public String userId;

    public String nickname;

    public String avatar;

    public int isFollow;

    public int secret;

    public int shareTo;
    /**
     * 本地数据，用来判断是否可以点击
     */
    public static final int CLICK_STATUS_ENABLE = 0;
    public static final int CLICK_STATUS_DISABLE = 1;
    public int clickStatus = 0;

    public static final int VIDEO_TYPE_UGC = 0;
    public static final int VIDEO_TYPE_PGC = 1;

    public int videoType = VIDEO_TYPE_UGC;

    public long interval = -1;

    public long distance = -1;

    @Override
    public String toString() {
        return "VideoBean{" +
                "id='" + id + '\'' +
                ", text='" + text + '\'' +
                ", image='" + image + '\'' +
                ", pixelWidth=" + pixelWidth +
                ", pixelHeight=" + pixelHeight +
                ", videoUrl='" + videoUrl + '\'' +
                ", playTime=" + playTime +
                ", playCount=" + playCount +
                ", winkFlag=" + winkFlag +
                ", winkNum=" + winkNum +
                ", commentNum=" + commentNum +
                ", isPlaying=" + isPlaying +
                ", currentPosition=" + currentPosition +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (obj != null && obj instanceof VideoBean && !TextUtils.isEmpty(((VideoBean) obj).id)) {
            return ((VideoBean) obj).id.equals(id);
        }
        if (obj != null && obj instanceof MomentsBean && !TextUtils.isEmpty(((MomentsBean) obj).momentsId)) {
            return ((MomentsBean) obj).momentsId.equals(id);
        }
        if (obj != null && obj instanceof String) {
            return obj.equals(id);
        }
        return super.equals(obj);
    }


    public static VideoBean getVideoBeanFromMoment(@Nullable MomentsBean momentsBean) {
        final VideoBean videoBean = new VideoBean();
        videoBean.id = momentsBean.momentsId;
        videoBean.text = momentsBean.momentsText;
        videoBean.image = getImageUrl(momentsBean.imageUrl);
        videoBean.pixelWidth = momentsBean.pixelWidth;
        videoBean.pixelHeight = momentsBean.pixelHeight;
        videoBean.videoUrl = momentsBean.videoUrl;
        videoBean.playTime = momentsBean.playTime;
        videoBean.playCount = momentsBean.playCount;
        videoBean.winkFlag = momentsBean.winkFlag;
        videoBean.winkNum = momentsBean.winkNum;
        videoBean.liveStatus = momentsBean.liveStatus;
        videoBean.commentNum = momentsBean.commentNum;
        videoBean.videoWebp = "";
        videoBean.videoColor = momentsBean.videoColor;
        videoBean.videoTag = momentsBean.videoTag;
        videoBean.type = momentsBean.momentsType;
        videoBean.mediaSize = momentsBean.mediaSize;
        videoBean.userId = momentsBean.userId + "";
        videoBean.nickname = momentsBean.nickname;
        videoBean.avatar = momentsBean.avatar;
        videoBean.isFollow = momentsBean.followerStatus;
        videoBean.secret = momentsBean.secret;
        videoBean.shareTo = momentsBean.shareTo;
        videoBean.clickStatus = VideoBean.CLICK_STATUS_DISABLE;
        return videoBean;
    }

    public static String getImageUrl(String imageUrls) {
        if (TextUtils.isEmpty(imageUrls)) {
            return "";
        }
        String[] arr = imageUrls.split(",");
        final int length = arr.length;
        if (length > 0) {
            return TheLConstants.FILE_PIC_URL + arr[length - 1];
        }
        return "";
    }
}
