package com.thel.bean;

import com.thel.R;
import com.thel.app.TheLApp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

public class FriendsBean implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final int FRIEND_TYPE_FRIENDS = 0;
    public static final int FRIEND_TYPE_FOLLOW = 1;
    public static final int FRIEND_TYPE_FANS = 2;

    /**
     * 用户id
     */
    public String userId;

    /**
     * 是否在线( 0 不在,1 在,2忙)
     */
    public String online;

    /**
     * 用户名
     */
    public String nickName;

    /**
     * 用户名(拼音，用于排序)
     */
    public String userNamePinYin;

    /**
     * 角色
     */
    public String roleName;

    /**
     * 头像
     */
    public String avatar;

    /**
     * 距离
     */
    public String distance;

    public String intro;
    // days 和 hours,和loginTime组合使用，标识几天前登陆或几小时前登陆
    public String unit;
    public String loginTime;

    public int friendType;

    /**
     * 认证状态，是否是加V用户 0:否 1:个人V 2:企业V
     */
    public int verifyType;
    /**
     * 认证说明
     */
    public String verifyIntro;
    /**
     * 年龄
     */
    public int age;
    /**
     * 是否开启了隐身
     */
    public int hiding;
    /**
     * 会员等级
     */
    public int level;
    /**
     * 感情状态
     */
    public int affection;
    /**
     * 悄悄关注（0:未悄悄关注，1：悄悄关注）
     */
    public int secretly = 0;

    public String getLoginTimeShow() {
        StringBuilder str = new StringBuilder();
        if (level == 0 || hiding == 0) {// 如果是会员并且开了隐身功能，则不显示距离
            str.append(distance);
            str.append(", ");
        }
        str.append(age);
        str.append(TheLApp.getContext().getString(R.string.updatauserinfo_activity_age_unit));
        ArrayList<String> relationship_list = new ArrayList<>(Arrays.asList(TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship))); // 感情状态
        str.append(", ");
        try {
            str.append(relationship_list.get(affection));
        } catch (Exception e) {
            str.append(relationship_list.get(0));
        }

        return str.toString();
    }
}
