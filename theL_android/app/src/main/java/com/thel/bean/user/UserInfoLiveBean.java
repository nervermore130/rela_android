package com.thel.bean.user;

import java.io.Serializable;

/**
 * Created by waiarl on 2017/11/10.
 * 个人详情直播信息
 */

public class UserInfoLiveBean implements Serializable {
    /**
     * 是否在直播，0否 1是
     **/
    public int live;
    /**
     * 观众人数排名
     **/
    public int rank;
    public String discription;
    public int looker;
    public String imageUrl;
    public String type; //1.声音 2.视频
    /**
     * 是否是横竖屏直播 1横屏 0竖屏
     */
    public int isLandscape;

    /**
     * 直播间id
     */
    public String liveId;
}
