package com.thel.bean;

import java.util.List;

/**
 * Created by chad
 * Time 18/4/18
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */
public class PgcVideoSeekListBean {

    public List<PgcVideoSeekBean> list;

    public static class PgcVideoSeekBean {
        public String videoId;

        public long seekTo;
    }
}
