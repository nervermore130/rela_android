package com.thel.bean.moments;

import android.os.Parcel;
import android.os.Parcelable;

import com.thel.base.BaseDataBean;

/**
 * Comment bean
 *
 * @author Setsail
 */
public class MomentsCheckBean extends BaseDataBean implements Parcelable {

    /**
     * 返回对象包含下面 2 个值： 优先: 未读的评论（包括点赞）数量。如果大于0，就用这个数量，如果等于0，就用下面的“其次” 其次:
     * 是否有未读的朋友圈信息(0: 没有，1: 有)
     */

    /**
     * 未读的评论（包括点赞）数量
     */
    public int notReadCommentNum;

    /**
     * 未读的密友请求数量
     */
    public int requestNotReadNum;

    /**
     * 是否有未读的朋友圈信息(0: 没有，1: 有)
     */
    public int haveNotReadMomentsFlag;

    /**
     * 是否有新来访(0: 没有，!=0: 有)
     */
    public int notReadCheckRemind;

    /**
     * 来看过我的num (0 就显示小红点)
     */
    public int notReadViewMeNum;

    /**
     * 未读日志数量
     */
    public int notReadMomentCount;
    /**
     * 喜欢我的数量
     */
    public int likeMeNum;

    /**
     * 喜欢我的cursor
     */
    public String likeMeCursor;

    /**-------一发送到flutter的未读消息--------*/
    /**
     * 未读聊天数
     */
    public int notReadChat;

    /**
     * 清理日志发送状态
     */
    public int homeTabStatus;

    /**
     * 是否是vip
     */
    public boolean isVip;

    /**
     * 是否有新的热拉FM
     */
    public boolean fMPoint;

    /**
     * 是否是首充
     */
    public boolean firstCharge;

    // "likeCount":23   //喜欢数量 4.5版本新增 可以不再调用/interface/stay/match/likeMeCount接口
    public int likeCount;
    //未回应的喜欢的人
    public int notReplyMeCount;

    /**-------一发送到flutter的未读消息--------*/


    @Override public int describeContents() {
        return 0;
    }

    @Override public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.notReadCommentNum);
        dest.writeInt(this.requestNotReadNum);
        dest.writeInt(this.haveNotReadMomentsFlag);
        dest.writeInt(this.notReadCheckRemind);
        dest.writeInt(this.notReadViewMeNum);
        dest.writeInt(this.notReadMomentCount);
        dest.writeInt(this.likeMeNum);
        dest.writeString(this.likeMeCursor);
    }

    public MomentsCheckBean() {
    }

    protected MomentsCheckBean(Parcel in) {
        this.notReadCommentNum = in.readInt();
        this.requestNotReadNum = in.readInt();
        this.haveNotReadMomentsFlag = in.readInt();
        this.notReadCheckRemind = in.readInt();
        this.notReadViewMeNum = in.readInt();
        this.notReadMomentCount = in.readInt();
        this.likeMeNum = in.readInt();
        this.likeMeCursor = in.readString();
    }

    public static final Creator<MomentsCheckBean> CREATOR = new Creator<MomentsCheckBean>() {
        @Override public MomentsCheckBean createFromParcel(Parcel source) {
            return new MomentsCheckBean(source);
        }

        @Override public MomentsCheckBean[] newArray(int size) {
            return new MomentsCheckBean[size];
        }
    };

    @Override public String toString() {
        return "MomentsCheckBean{" +
                "notReadCommentNum=" + notReadCommentNum +
                ", requestNotReadNum=" + requestNotReadNum +
                ", haveNotReadMomentsFlag=" + haveNotReadMomentsFlag +
                ", notReadCheckRemind=" + notReadCheckRemind +
                ", notReadViewMeNum=" + notReadViewMeNum +
                ", notReadMomentCount=" + notReadMomentCount +
                ", likeMeNum=" + likeMeNum +
                ", likeMeCursor='" + likeMeCursor + '\'' +
                '}';
    }
}
