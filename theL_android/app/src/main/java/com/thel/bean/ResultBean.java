package com.thel.bean;

import com.thel.base.BaseDataBean;

/**
 * 默认的请求结果对象(可根据需要拓展)
 *
 * @author Setsail
 */
public class ResultBean extends BaseDataBean {

    // 关注或者取消关注成功的用户id
    public String followUserId;
    // 关注成功的用户id列表
    public String successUserIdList = "";
    // 关注成功的数量
    public int successNum = 0;
    // 关注失败的数量
    public int errorNum = 0;
    // 关注失败的原因
    public String errorReason;

    @Override
    public String toString() {
        return "ResultBean{" +
                "followUserId='" + followUserId + '\'' +
                ", successUserIdList='" + successUserIdList + '\'' +
                ", successNum=" + successNum +
                ", errorNum=" + errorNum +
                ", errorReason='" + errorReason + '\'' +
                '}';
    }
}

