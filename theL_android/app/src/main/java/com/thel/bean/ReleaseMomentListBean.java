package com.thel.bean;

import com.thel.bean.moments.MomentsBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chad
 * Time 17/10/13
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */

public class ReleaseMomentListBean {

    public List<MomentsBean> list = new ArrayList<>();

    @Override
    public String toString() {
        return "ReleaseMomentListBean{" +
                "list=" + list +
                '}';
    }
}
