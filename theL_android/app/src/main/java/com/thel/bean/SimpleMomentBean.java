package com.thel.bean;


import com.thel.utils.JsonUtils;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Simple Moment bean
 *
 * @author Setsail
 */
public class SimpleMomentBean implements Serializable {

    public String momentId;

    public String momentText;

    public String momentImg;

    public String momentType;

    /**
     * 从json对象封装对象
     *
     * @param jsonObject
     */
    public void fromJson(JSONObject jsonObject) {
        try {
            momentId = JsonUtils.getString(jsonObject, "momentId", "0");
            momentText = JsonUtils.getString(jsonObject, "momentText", "0");
            momentImg = JsonUtils.getString(jsonObject, "momentImg", "0");
            momentType = JsonUtils.getString(jsonObject, "momentType", "0");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
