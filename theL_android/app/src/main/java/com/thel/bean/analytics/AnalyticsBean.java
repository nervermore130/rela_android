package com.thel.bean.analytics;

public class AnalyticsBean {

    /**
     * 获取日志的时间
     */
    public String time;

    /**
     * 观众id
     */
    public String userId;

    /**
     * 日志类型是直播消息日志liveChat，还是主播推流liveLog,或pk
     */
    public String logType;

    /**
     * 推流地址
     */
    public String host;

    /**
     * host主播 audience观众
     */
    public String role;

    /**
     * 声网的房间id
     */
    public String channelId;

    /**
     * 直播间id
     */
    public String roomId;

    /**
     * 切进来的Url
     */
    public String inUrl;

    /**
     * 切出去的url
     */
    public String outUrl;

    /**
     * 主播用户Id
     */
    public String liveUserId;

    /**
     * 错误原因
     */
    public String errorReason;

    /**
     * 网络检测的日志
     */
    public String traceRoute;

    /**
     * Rela版本号
     */
    public String relaVersion;

    /**
     * 网络状态
     */
    public String network;

    /**
     * 使用域名的原因
     */
    public String reason;

    /**
     * 旁路推流地址
     */
    public String cdnDomain;

    /**
     * 推流地址排序
     */
    public int rtmpUrlSort;

    /**
     * 拉流地址排序
     */
    public int liveUrlSort;

    /**
     * 服务端指定拉流地址
     */
    public int forceServerLiveUrl;

    /**
     * 请求超时的接口
     */
    public String api;

    /**
     * 请求超时的参数
     */
    public String body;

    /**
     * 客户端系统
     */
    public String cSystem;

    /**
     * 数据
     */
    public String log;

    /**
     * 当前连接的token
     */
    public String token;

    /**
     * 网络请求数据
     */
    public String apiData;

    /**
     * 主播的id
     */
    public String hostUserId;

    /**
     * 拉流地址
     */
    public String liveUrl;
}
