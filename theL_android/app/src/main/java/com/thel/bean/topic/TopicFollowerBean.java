package com.thel.bean.topic;

/**
 * 话题关注者
 *
 * @author Setsail
 */
public class TopicFollowerBean {

    /**
     * 距离
     */
    public String distance;

    /**
     * 用户id
     */
    public int userId;

    /**
     * 是否在线( 0 不在,1 在)
     */
    public int online;

    /**
     * 用户名
     */
    public String nickName;

    /**
     * 在线时段
     */
    public String loginTime;

    /**
     * 方头像地址
     */
    public String avatar;

    /**
     * 上次登录时间
     */
    public String loginTime2; // "4"、"5"
    // days 和 hours,和loginTime2组合使用，标识几天前登陆或几小时前登陆
    public String unit;
    public String roleName;
    /**
     * 恋爱状态
     */
    public String intro;
    /**
     * 年龄
     */
    public int age;
    /**
     * 是否开启了隐身
     */
    public int hiding;
    /**
     * 会员等级
     */
    public int level;
    /**
     * 感情状态
     */
    public int affection;
    /**
     * 认证状态，是否是加V用户 0:否 1:个人V 2:企业V
     */
    public int verifyType;
    /**
     * 认证说明
     */
    public String verifyIntro;

}
