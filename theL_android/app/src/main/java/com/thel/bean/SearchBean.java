package com.thel.bean;

public class SearchBean {
    public int myUserId;
    public String userAvatarLocal;

    /**
     * 距离
     */
    public String distance;

    /**
     * 用户id
     */
    public int userId;

    /**
     * 是否在线( 0 不在,1 在)
     */
    public int online;

    /**
     * 年龄
     */
    public int age;

    /**
     * 取向
     */

    /**
     * 签名
     */
    public String userIntro;

    /**
     * 用户名
     */
    public String userName;

    /**
     * 恋爱状态
     */
    public int affection;

    /**
     * 是否关注这个用户,1:已关注，0未关注
     */
    public int isFollow = 0;

    /**
     * 认证状态，是否是加V用户 0:否 1:个人V 2:企业V
     */
    public int verifyType;
    /**
     * 认证说明
     */
    public String verifyIntro;
    /**
     * 是否开启了隐身
     */
    public int hiding;
    /**
     * 会员等级
     */
    public int level;
    /**
     * 角色
     */
    public String roleName;

    public String recommendType;// 推荐类型：共同标签用户1，特色用户2，明星用户3
    public String recommendReason;
    public String avatar;
    public static final String RECOMMEND_TYPE_TAG = "1";// 共同标签用户
    public static final String RECOMMEND_TYPE_INTERESTING = "2";// 特色用户
    public static final String RECOMMEND_TYPE_STAR = "3";// 明星用户
    public static final int FOLLOW_TYPE_UNFOLLOWED = 0;// 未关注
    public static final int FOLLOW_TYPE_FOLLOWED = 1;// 已关注
}