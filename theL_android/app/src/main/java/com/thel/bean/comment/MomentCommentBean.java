package com.thel.bean.comment;

import com.thel.base.BaseDataBean;
import com.thel.bean.NormalListBean;
import com.thel.bean.NormalUserInfoBean;

import java.util.List;

/**
 * Created by liuyun on 2017/10/26.
 */

public class MomentCommentBean extends BaseDataBean {

    public MomentCommentDataBean data;


    public class MomentCommentDataBean extends NormalListBean {

        public List<MomentCommentLiseBean> commentList;

    }

    public class MomentCommentLiseBean extends NormalUserInfoBean{

        public String commentTime;

        public String commentText;

        public String commentType;

        public long commentId;

        public String commentId_str;

        public int commentNum;

        public int followerStatus;

        public long toUserId;

        public String toNickname;

        public String toAvatar;

        public String sayType;

        public String commentList;

        public String imageUrl;

        public int winkFlag;

    }

}
