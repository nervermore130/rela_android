package com.thel.bean;

/**
 * Created by liuyun on 2017/10/25.
 */

public class CommentWinkUserBean extends NormalUserInfoBean{
    public int id;
    public String intro;
    public int online;
    public String roleName;
    public int affection;
    public String distance;
    public int verifyType;
    public String verifyIntro;
    public int workField;
    public int age;
    public int level;
    public int hiding;
    public int isFollow;
    public int followStatus;
    public String createDate;
    public String createTime;
    public String Avatar;
    public String create_date;
    public String userIntro;
    public int secretly;
    public String userName;

}
