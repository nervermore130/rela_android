package com.thel.bean;

import com.thel.base.BaseDataBean;

import java.util.List;

public class AdBean extends BaseDataBean {

    public Data data;

    public class Map_list {

        public int id;

        public String advertURL;

        public String advertLocation;

        public String dumpURL;

        public String time;

        public String type;

        public String advertTitle;

        public String dumpType;

        /**
         * 1横屏 0竖屏
         */
        public int isLandscape;
    }

    public class Data {
        public List<Map_list> map_list;
    }

    public static final String ADV_DUMP_TYPE_NO = "0";
    public static final String ADV_DUMP_TYPE_WEB = "1";
    public static final String ADV_DUMP_TYPE_LOCAL = "2";
    public static final String ADV_DUMP_TYPE_TAG = "5";
    public static final String ADV_DUMP_TYPE_STICKER = "6";
    public static final String ADV_DUMP_TYPE_STICKER_STORE = "7";
    public static final String ADV_DUMP_TYPE_MOMENT = "8";
    public static final String ADV_DUMP_TYPE_TOPIC = "9";
    public static final String ADV_DUMP_TYPE_LIVE_ROOMS = "10";
    public static final String ADV_DUMP_TYPE_LIVE_ROOM = "11";
    public static final String ADV_DUMP_TYPE_BUY_VIP = "13";
    public static final String ADV_DUMP_TYPE_USER_INFO = "14";

    public static final String ADV_LOCATION_COVER = "main";
    public static final String ADV_LOCATION_NEARBY = "list";
    public static final String ADV_LOCATION_MOMENTS = "friend";
    public static final String ADV_LOCATION_STICKER_STORE = "sticker";
    public static final String ADV_LOCATION_NEW_STICKER = "sticker_icon";
    public static final String ADV_LOCATION_WEEKLY = "weekly";
    public static final String ADV_LOCATION_VIP = "vip";
    public static final String ADV_LOCATION_FIND = "find";
    public static final String ADV_LOCATION_LIVE_ROOMS = "live";
    public static final String ADV_LOCATION_FM = "fm";

    /***4.7.0新增视频广告类型***/
    public static final String ADV_LOCATION_VIDEO = "video";

}