package com.thel.bean.user;

/**
 * Created by test1 on 2017/11/3.
 */

public class BlackBean {

    /**
     * userId : 101811205
     * userName : 睡仙
     * userAvatar : http://static.thel.co/app/avatar/101811205/03b518f2a282bafc2592fdd3c8f80317.jpg-wh150
     * userIntro :
     * online : 0
     * id : 101811205
     * nickName : 睡仙
     * avatar : http://static.thel.co/app/avatar/101811205/03b518f2a282bafc2592fdd3c8f80317.jpg-wh150
     * intro :
     * roleName : 3
     * level : 0
     * hiding : 0
     * verifyType : 0
     * verifyIntro :
     * workField : 0
     */

    public int userId;
    public String userName;
    public String userAvatar;
    public String userIntro;
    public int online;
    public int id;
    public String nickName;
    public String avatar;
    public String intro;
    public String roleName;
    public int level;
    public int hiding;
    public int verifyType;
    public String verifyIntro;
    public int workField;
}
