package com.thel.bean.theme;

import com.thel.base.BaseDataBean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by waiarl on 2017/8/18.
 * 4.1.0 话题回复，源于CommentListBean
 */
public class ThemeCommentListBean extends BaseDataBean implements Serializable {

    public int cursor;
    public boolean haveNextPage;
    public List<ThemeCommentBean> list;

}
