package com.thel.bean;

import com.thel.base.BaseDataBean;

import java.util.List;

/**
 * Created by chad
 * Time 18/9/13
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */
public class SuperlikeListBean extends BaseDataBean {


    public Data data;

    public class Lists {
        public int id;

        public int status;

        public int price;

        public int realPrice;

        public int count;

        public int rank;

        public String iapId;

        /**
         * 用google play支付的话需要从google play上获取价格
         */
        public boolean isPendingPrice = false;

        /**
         * 谷歌钱包直接显示价格
         */
        public String googlePrice;

    }

    public class Data {
        public List<Lists> list;

    }
}
