package com.thel.bean.me;

import com.thel.base.BaseDataBean;

import java.io.Serializable;

/**
 * 用户自己的信息
 *
 * @author zhangwenjia
 */
public class MyInfoBean extends BaseDataBean implements Serializable {

    /**
     * 总传情数
     */
    public String winkNum;

    /**
     * 关注数 *
     */
    public String followNum;

    /**
     * 用户上传的背景 *
     */
    public String bgImage;

    /**
     * 新传情数
     */
    public String newWink;

    /**  */
    public String isWinked;

    /**
     * 性别
     */
    public String sex;

    /**
     * 新浪微博id
     */
    public String sinaUid;

    /**
     * 新浪微博 Token
     */
    public String sinaToken;

    /**
     * 新浪微博 TokenSecret
     */
    public String sinaTokenSecret;

    /**
     * 别名
     */
    public String nickname = "";

    /**
     * 头像地址
     */
    public String avatar;

    /**
     * 生日
     */
    public String birthday;

    /**
     * 身高
     */
    public String height;

    /**
     * 体重
     */
    public String weight;

    /**
     * 爱好
     */
    public String interests;

    /**
     * 现居地
     */
    public String livecity = "";

    /**
     * 旅行地
     */
    public String travelcity = "";

    /**
     * 签名
     */
    public String intro = "";

    /**  */
    public String locked;

    /**
     * 种族
     */
    public String ethnicity;

    /**
     * 活动范围
     */
    public String location = "";

    /**
     * 经度
     */
    public String lng;

    /**
     * 纬度
     */
    public String lat;

    /**
     * 感情状态
     */
    public String affection; // (0=不想透露，1=单身，2=约会中，3=稳定关系，4=已婚，5=开放关系)

    /**
     * 角色
     */
    public String roleName; // T,P,H,S

    /**
     * 交友目的
     */
    public String purpose;

    /**
     * 音乐
     */
    public String music = "";

    /**
     * 书
     */
    public String books = "";

    /**
     * 影视剧
     */
    public String movie = "";

    /**
     * 职业
     */
    public String occupation = "";

    /**
     * 食物
     */
    public String food = "";

    /**
     * 其他爱好
     */
    public String others = "";

    /**
     * 个人描述
     */
    public String discriptions;

    /**
     * email
     */
    public String email;

    /**
     * 语音的url,每一个用户的url是固定的
     */
    public String voiceMessage;

    /**
     * 语音的序列号,用户每更新一次语音,这个数值就会变化,用来做客户端判断缓存用的
     */
    public String recorderDate;

    /**
     * 语音的播放时长,小于60的正整数
     */
    public String recorderTimes;

    /**
     * theLID
     */
    public String userName;
    public String wantRole;
    public String professionalTypes;
    public String outLevel;

    // 是否有伴侣
    public MyCirclePartnerBean partner = null;

    /**
     * 认证状态，是否是加V用户 0:否 1:个人V 2:企业V
     */
    public int verifyType;

    /**
     * 认证说明
     */
    public String verifyIntro;

    /**
     * vip等级
     */
    public int level;

    /**
     * 希望匹配年龄段的最小岁数
     */
    public int wantAgeBegin;

    /**
     * 希望匹配年龄段的最大岁数
     */
    public int wantAgeEnd;

    /** 一些推送开关 **/
    /**
     * 直播推送开关
     */
    public int livePush;
    // 一下这些push开关安卓版暂时用不到，仅做存储
    public int commentTextPush;
    public int commentUserPush;
    public int commentWinkPush;
    public int keyPush;
    public int followerPush;
    public int winkPush;
    public int messagePush;

    /**
     * 3.0.1 粉丝数
     */
    public int fansNum;

}
