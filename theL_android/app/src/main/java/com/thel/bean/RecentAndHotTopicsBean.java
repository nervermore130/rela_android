package com.thel.bean;

import com.thel.base.BaseDataBean;

import java.util.List;

/**
 * Created by chad
 * Time 17/9/30
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */

public class RecentAndHotTopicsBean extends BaseDataBean {

    public List<RecentAndHotTagBean> recentTopicList;
    public List<RecentAndHotTagBean> hotTopicList;
}
