package com.thel.bean.moments;

import java.io.Serializable;

/**
 * Created by waiarl on 2017/7/4.
 * 推荐日志 列表里面 重复推荐的 人的信息
 */

public class MomentRepeatUserBean implements Serializable {
    private static final long serialVersionUID = 1L;

    public String userId;
    public String nickname;
    public String avatar;
    public String momentsId;//当前日志id
    public String parentId;//推荐的日志的id

}
