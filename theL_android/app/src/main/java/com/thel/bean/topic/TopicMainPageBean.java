package com.thel.bean.topic;

import android.text.TextUtils;

import com.thel.app.TheLApp;
import com.thel.base.BaseDataBean;
import com.thel.bean.ContactBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TopicMainPageBean extends BaseDataBean {

    /**
     * 话题id
     */
    public String topicId;

    /**
     * 话题名称
     */
    public String topicName;
    /**
     * 我是否关注此话题 *
     */
    public int followFlag;
    /**
     * 关注此话题的用户数量 *
     */
    public int followNum;
    /**
     * 话题下的日志数量 *
     */
    public int momentsNum;
    /**
     * 话题的颜色 *
     */
    public String topicColor;

    public static final int FOLLOW_FLAG_FOLLOWED = 1;
    public static final int FOLLOW_FLAG_UNFOLLOWED = 0;

    public List<ContactBean> followUserList;
    public List<MomentsBean> momentsList;

    public List<MomentsBean> hotMomentList;
    public List<MomentsBean> nearbyMomentList;

    /**
     * 摇一摇随机标签的时候用的关注人数
     */
    public int joinCount;

//    /**
//     * 将屏蔽的用户过滤掉，不出现在moments里
//     */
//    public void filtBlockUsers() {
//
//        String myUserId = ShareFileUtils.getString(ShareFileUtils.ID, "");
//
//        if (TextUtils.isEmpty(myUserId)) {
//            return;
//        }
//
//        List<Integer> blockUserIds = DataBaseAdapter.getInstance(TheLApp.getContext()).getBlackListIds(Integer.valueOf(myUserId));
//
//        if (blockUserIds.isEmpty()) {
//            return;
//        }
//
//        List<MomentsBean> blockUserMomentsBeans = new ArrayList<MomentsBean>();
//
//        for (MomentsBean momentsBean : momentsList) {
//            for (Integer userId : blockUserIds) {
//                if (momentsBean.userId == userId) {
//                    blockUserMomentsBeans.add(momentsBean);
//                    break;
//                }
//            }
//
//        }
//        momentsList.removeAll(blockUserMomentsBeans);
//    }

    /**
     * 过滤屏蔽的日志
     */
    public void filterBlockMoments() {
        try {
            String[] hideMomentsUserList = SharedPrefUtils.getString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.HIDE_MOMENTS_USER_LIST, "").split(",");
            List<MomentsBean> blockMoments1 = new ArrayList<MomentsBean>();
            //过滤不看她的日志
            if (hideMomentsUserList.length > 0 && !TextUtils.isEmpty(hideMomentsUserList[0])) {
                for (MomentsBean momentsBean : momentsList) {
                    for (String userId : hideMomentsUserList) {
                        if (userId.equals(momentsBean.userId + "")) {
                            blockMoments1.add(momentsBean);
                            break;
                        }
                    }
                }
                momentsList.removeAll(blockMoments1);
            }
            // 过滤不看这条日志和举报日志
            List<String> hideMomentsList = Arrays.asList(SharedPrefUtils.getString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.HIDE_MOMENTS_LIST, "").split(","));
            List<String> reportMomentsList = Arrays.asList(SharedPrefUtils.getString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.REPORT_MOMENTS_LIST, "").split(","));
            List<String> blockMoments = new ArrayList<String>();
            if (hideMomentsList.size() > 0 && !TextUtils.isEmpty(hideMomentsList.get(0))) {
                blockMoments.addAll(hideMomentsList);
            }
            if (reportMomentsList.size() > 0 && !TextUtils.isEmpty(reportMomentsList.get(0))) {
                blockMoments.addAll(reportMomentsList);
            }
            if (blockMoments.size() > 0) {
                List<MomentsBean> blockMoments2 = new ArrayList<MomentsBean>();
                for (MomentsBean momentsBean : momentsList) {
                    for (String momentsId : blockMoments) {
                        if (momentsId.equals(momentsBean.momentsId)) {
                            blockMoments2.add(momentsBean);
                            break;
                        }
                    }
                }
                momentsList.removeAll(blockMoments2);
            }
        } catch (Exception e) {
        }
    }
}

/**
 * {"result":"1","errcode":"","errdesc":"","topicId":-1,"topicName":"#寻找女神#"
 * ,"followFlag":0,"followNum":0,"followUserList":[],"momentsNum":0,
 * "momentsList":[]}
 */
