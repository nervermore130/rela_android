package com.thel.bean.live;

import java.util.List;

/**
 * Created by chad
 * Time 18/5/9
 * Email: wuxianchuang@foxmail.com
 * Description: TODO 多人连麦进直播间初始化信息
 */
public class LiveMultiOnCreateBean {

    public String status;

    public int leftTime;

    public boolean isWaitMic;//是否为排麦模式

    public boolean IsInWaitMicList;//是否在排麦列表中

    public List<LiveMultiSeatBean> seats;

    @Override
    public String toString() {
        return "LiveMultiOnCreateBean{" +
                "status='" + status + '\'' +
                ", isWaitMic=" + isWaitMic + '\'' +
                ", IsInWaitMicList=" + IsInWaitMicList + '\'' +
                ", leftTime=" + leftTime + '\'' +
                ", seats=" + seats +
                '}';
    }
}
