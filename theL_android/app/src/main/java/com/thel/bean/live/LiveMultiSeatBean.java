package com.thel.bean.live;

import com.thel.modules.live.bean.AgoraBean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by chad
 * Time 18/5/10
 * Email: wuxianchuang@foxmail.com
 * Description: TODO 多人连麦座位信息
 */
public class LiveMultiSeatBean implements Serializable {

    public boolean isSelected = false;

    public String method = "";

    public boolean isSelf;//method 为offseat时返回，用于判断是自己下麦，还是被主播下麦

    public int seatNum;

    public String micStatus = "";

    public String seatStatus = "";

    public int heartNum;

    public int choice;

    public String userId = "";

    public String nickname = "";

    public String avatar = "";

    public String toUserId = "";

    public String toNickname = "";

    public String toAvatar = "";

    public int id;//礼物id

    public int combo;

    public int userLevel;

    public int leftTime;//相遇剩余时间

    public String ownerGem = "";

    public int couple;
    /**
     * 我是否关注她
     */
    public int followState;

    public List<Integer> seatChoice;

    public List<CoupleDetail> coupleDetail = new ArrayList<>();

    public List<CoupleDetail> list = new ArrayList<>();

    public AgoraBean agora;

    public static class CoupleDetail implements Serializable {
        public String userId = "";

        public String nickname = "";

        public String avatar = "";

        public int iconSwitch = 1;//1显示用户等级 0不显示用户等级

        public int userLevel = 0;//用户等级

        public int vip;

        @Override
        public String toString() {
            return "CoupleDetail{" +
                    "userId='" + userId + '\'' +
                    ", nickname='" + nickname + '\'' +
                    ", avatar='" + avatar + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "LiveMultiSeatBean{" +
                "method='" + method + '\'' +
                ", seatNum=" + seatNum +
                ", micStatus='" + micStatus + '\'' +
                ", seatStatus='" + seatStatus + '\'' +
                ", heartNum=" + heartNum +
                ", choice=" + choice +
                ", userId='" + userId + '\'' +
                ", nickname='" + nickname + '\'' +
                ", avatar='" + avatar + '\'' +
                ", toUserId='" + toUserId + '\'' +
                ", toNickname='" + toNickname + '\'' +
                ", toAvatar='" + toAvatar + '\'' +
                ", id=" + id +
                ", combo=" + combo +
                ", userLevel=" + userLevel +
                ", leftTime=" + leftTime +
                ", ownerGem='" + ownerGem + '\'' +
                ", couple=" + couple +
                ", coupleDetail=" + coupleDetail +
                '}';
    }
}
