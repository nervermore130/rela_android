package com.thel.bean.message;

public class MomentToChatBean {

    public String imgUrl;

    public String momentText;

    public String chatText;

    public String momentType;

    public String momentId;

    @Override public String toString() {
        return "MomentToChatBean{" +
                "imgUrl='" + imgUrl + '\'' +
                ", momentText='" + momentText + '\'' +
                ", chatText='" + chatText + '\'' +
                ", momentType='" + momentType + '\'' +
                ", momentId='" + momentId + '\'' +
                '}';
    }
}
