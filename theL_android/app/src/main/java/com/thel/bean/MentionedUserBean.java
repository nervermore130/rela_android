package com.thel.bean;

import java.io.Serializable;

/**
 * 日志提到的用户
 *
 * @author Setsail
 */
public class MentionedUserBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    public int userId;
    public String nickname;
    public String avatar;

    /**
     * moments的唯一标识
     */
    public String momentsId;
    public long timestamp;
    public String filterType;

    @Override
    public String toString() {
        return "MentionedUserBean{" +
                "userId=" + userId +
                ", nickname='" + nickname + '\'' +
                ", avatar='" + avatar + '\'' +
                ", momentsId='" + momentsId + '\'' +
                ", timestamp=" + timestamp +
                ", filterType='" + filterType + '\'' +
                '}';
    }
}

/*
 * "atUserList":[{"userId":100163791,"nickname":"安卓测试账号二号"},{"userId":71599,
 * "nickname":"Sammy 武"}
 */