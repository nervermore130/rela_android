package com.thel.bean;

import com.thel.base.BaseDataBean;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by chad
 * Time 17/11/22
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */

public class FriendListBean extends BaseDataBean {

    public Data data;

    public class Data {

        public List<String> list;

        @Override public String toString() {
            return "Data{" +
                    "list=" + list +
                    '}';
        }
    }

    @Override public String toString() {
        return "FriendListBean{" +
                "data=" + data +
                '}';
    }
}
