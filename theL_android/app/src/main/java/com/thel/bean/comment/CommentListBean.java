package com.thel.bean.comment;

import com.thel.base.BaseDataBean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 附近用户列表信息
 *
 * @author Setsail
 */
public class CommentListBean extends BaseDataBean implements Serializable {

    public int commentNumThisPage;

    public String momentsId;

    public List<CommentBean> comments;

}
