package com.thel.bean.topic;

import java.io.Serializable;

/**
 * Topic bean
 *
 * @author Setsail
 */
public class TopicImageBean implements Serializable {

    /**
     * 日志id
     */
    public String momentId;

    /**
     * 日志图片
     */
    public String imageUrl;

    // 是否是音乐日志图片
    public int musicFlag = 0;

	/*-----------------------------------topic	接口报文--------------------------------------*/
    /*
     * {"result":"1","errcode":"","errdesc":"","list":[{"topicId":84,"topicName":
	 * "#Tattoo#"
	 * ,"topicColor":"EA8888","joinCount":118,"topicImageList":[{"momentId"
	 * :2131612566553509,"imageUrl":
	 * "http://image.thel-service.com/moments-image/20150115/1421316279043_173009_small.jpg"
	 * },{"momentId":2131776432271460,"imageUrl":
	 * "http://image.thel-service.com/moments-image/20150115/1421317811593_245160_small.jpg"
	 * },{"momentId":2131985683734775,"imageUrl":
	 * "http://image.thel-service.com/moments-image/20150115/1421320010422_100050575_small.jpg"
	 * },{"momentId":2132311868923864,"imageUrl":
	 * "http://image.thel-service.com/moments-image/20150115/1421323272476_100094664_small.jpg"
	 * },{"momentId":2132325065973425,"imageUrl":
	 * "http://image.thel-service.com/moments-image/20150115/1421323382052_186825_small.jpg"
	 * }
	 * ]},{"topicId":1,"topicName":"#寻找女神#","topicColor":"C3E874","joinCount":138
	 * ,"topicImageList":[{"momentId":2072397223954569,"imageUrl":
	 * "http://image.thel-service.com/moments-image/20150108/1420723988976_100132269_small.jpg"
	 * },{"momentId":2072763654456081,"imageUrl":
	 * "http://image.thel-service.com/moments-image/20150108/1420727653507_100246381_small.jpg"
	 * },{"momentId":2073440524768004,"imageUrl":
	 * "http://image.thel-service.com/moments-image/20150108/1420734422671_100230704_small.jpg"
	 * },{"momentId":2073510009265165,"imageUrl":
	 * "http://image.thel-service.com/moments-image/20150108/1420735117530_100077465_small.jpg"
	 * },{"momentId":2074037315752340,"imageUrl":
	 * "http://image.thel-service.com/moments-image/20150108/1420740390894_100030940_small.jpg"
	 * }
	 * ]},{"topicId":40,"topicName":"#L君，你粗来#","topicColor":"C19EF7","joinCount"
	 * :50,"topicImageList":[{"momentId":2107145446827718,"imageUrl":
	 * "http://image.thel-service.com/moments-image/20150112/1421071594680_100043218_small.jpg"
	 * },{"momentId":2107995782311062,"imageUrl":
	 * "http://image.thel-service.com/moments-image/20150112/1421080099857_159262_small.jpg"
	 * },{"momentId":2108011953835394,"imageUrl":
	 * "http://image.thel-service.com/moments-image/20150112/1421080154625_100247694_small.jpg"
	 * },{"momentId":2108557509678988,"imageUrl":
	 * "http://image.thel-service.com/moments-image/20150112/1421085717404_125288_small.jpg"
	 * },{"momentId":2110539899887835,"imageUrl":
	 * "http://image.thel-service.com/moments-image/20150112/1421105542294_100163435_small.jpg"
	 * }
	 * ]},{"topicId":83,"topicName":"#tattoo#","topicColor":"A3E886","joinCount"
	 * :38,"topicImageList":[{"momentId":2131566496272552,"imageUrl":
	 * "http://image.thel-service.com/moments-image/20150115/1421315723512_100106352_small.jpg"
	 * },{"momentId":2132776320372926,"imageUrl":
	 * "http://image.thel-service.com/moments-image/20150115/1421327894720_100211426_small.jpg"
	 * },{"momentId":2132873512261137,"imageUrl":
	 * "http://image.thel-service.com/moments-image/20150115/1421328889185_159437_small.jpg"
	 * },{"momentId":2133178817469006,"imageUrl":
	 * "http://image.thel-service.com/moments-image/20150115/1421331919798_61206_small.jpg"
	 * },{"momentId":2133240965274042,"imageUrl":
	 * "http://image.thel-service.com/moments-image/20150115/1421332540907_100186642_small.jpg"
	 * }
	 * ]},{"topicId":80,"topicName":"#Tatoo#","topicColor":"8CABF7","joinCount":
	 * 26,"topicImageList":[{"momentId":2129747939011082,"imageUrl":
	 * "http://image.thel-service.com/moments-image/20150114/1421297610185_100143882_small.jpg"
	 * },{"momentId":2129871192595942,"imageUrl":
	 * "http://image.thel-service.com/moments-image/20150115/1421298770164_248942_small.jpg"
	 * },{"momentId":2130065954466858,"imageUrl":
	 * "http://image.thel-service.com/moments-image/20150115/1421300717950_858_small.jpg"
	 * },{"momentId":2130293289463538,"imageUrl":
	 * "http://image.thel-service.com/moments-image/20150115/1421302991249_12038_small.jpg"
	 * },{"momentId":2130542826940662,"imageUrl":
	 * "http://image.thel-service.com/moments-image/20150115/1421305581252_159262_small.jpg"
	 * }
	 * ]},{"topicId":11,"topicName":"#1.11#","topicColor":"f2657f","joinCount":
	 * 25 ,"topicImageList":[{"momentId":2081856517882940,"imageUrl":
	 * "http://image.thel-service.com/moments-image/20150109/1420818694543_100176140_small.jpg"
	 * },{"momentId":2082893047233155,"imageUrl":
	 * "http://image.thel-service.com/moments-image/20150109/1420829060424_100246455_small.jpg"
	 * },{"momentId":2088725705390654,"imageUrl":
	 * "http://image.thel-service.com/moments-image/20150110/1420887378059_100213254_small.jpg"
	 * },{"momentId":2089414207243791,"imageUrl":
	 * "http://image.thel-service.com/moments-image/20150110/1420894167631_244191_small.jpg"
	 * },{"momentId":2090948268589064,"imageUrl":
	 * "http://image.thel-service.com/moments-image/20150110/1420909604233_100094664_small.jpg"
	 * }
	 * ]},{"topicId":81,"topicName":"#tatoo#","topicColor":"DEEAA1","joinCount":
	 * 14,"topicImageList":[{"momentId":2129849637921440,"imageUrl":
	 * "http://image.thel-service.com/moments-image/20150115/1421298649004_100176140_small.jpg"
	 * },{"momentId":2129999102391636,"imageUrl":
	 * "http://image.thel-service.com/moments-image/20150115/1421300037439_100065336_small.jpg"
	 * },{"momentId":2130104437828433,"imageUrl":
	 * "http://image.thel-service.com/moments-image/20150115/1421301197097_251333_small.jpg"
	 * },{"momentId":2130105656217520,"imageUrl":
	 * "http://image.thel-service.com/moments-image/20150115/1421301103030_100162820_small.jpg"
	 * },{"momentId":2131210828279994,"imageUrl":
	 * "http://image.thel-service.com/moments-image/20150115/1421312261490_100247694_small.jpg"
	 * }]},{"topicId":3,"topicName":"#微信订阅号FrankieCocky#","topicColor":"F4A275",
	 * "joinCount":8,"topicImageList":[{"momentId":2077871593041328,"imageUrl":
	 * "http://image.thel-service.com/moments-image/20150108/1420778843438_3528_small.jpg"
	 * },{"momentId":2086042251225828,"imageUrl":
	 * "http://image.thel-service.com/moments-image/20150109/1420860472724_3528_small.jpg"
	 * },{"momentId":2094751852834328,"imageUrl":
	 * "http://image.thel-service.com/moments-image/20150110/1420947570305_3528_small.jpg"
	 * },{"momentId":2102928812019328,"imageUrl":
	 * "http://image.thel-service.com/moments-image/20150111/1421029427718_3528_small.jpg"
	 * },{"momentId":2120590026557828,"imageUrl":
	 * "http://image.thel-service.com/moments-image/20150113/1421206048374_3528_small.jpg"
	 * }]}]}
	 */

}
