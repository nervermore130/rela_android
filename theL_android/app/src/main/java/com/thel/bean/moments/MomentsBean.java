package com.thel.bean.moments;

import android.text.TextUtils;
import android.view.View;

import com.thel.R;
import com.thel.TagBean;
import com.thel.app.TheLApp;
import com.thel.base.BaseDataBean;
import com.thel.bean.MentionedUserBean;
import com.thel.bean.comment.CommentBean;
import com.thel.bean.comment.CommentListBean;
import com.thel.bean.gift.WinkCommentBean;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Moments bean
 *
 * @author Setsail
 */
public class MomentsBean extends BaseDataBean implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 埋点
     * */
    public String rank_id = "";

    /**
     * moments的唯一标识
     */
    public String momentsId = "";

    /**
     * 日志是否置顶（在个人主页置顶）
     */
    public int userBoardTop = 0;

    /**
     * 用户id
     */
    public int userId;

    /**
     * 用户名
     */
    public String nickname;
    /**
     * 会员
     */
    public int level;
    /**
     * 方头像地址
     */
    public String avatar;

    /**
     * 评论数量
     */
    public int commentNum;

    /**
     * 点赞数量
     */
    public int winkNum;

    /**
     * 是否点过赞 0:没点过，1~9
     */
    public int winkFlag;

    /**
     * 是否关注了该用户 0:没关注过，1:关注过
     */
    public int followerStatus;

    /**
     * 是不是我自己的moment，1：是，0：否
     */
    public int myself;

    /**
     * moment发布的时间
     */
    public long releaseTime;

    /**
     * moment发布的时间（与服务器时间的偏移）
     */
    public String momentsTime;

    /**
     * 朋友圈的类型：text,image, text_image,voice,text_voice,theme,theme_reply,video,live
     */
    public String momentsType;

    /**
     * 发布的类型，1 代表 all, 2 代表 friends,3 代表 only me
     */
    public int shareTo;

    /**
     * 是否匿名（0 代表 不匿名，1 代表 匿名）
     */
    public int secret;

    // moment的文字内容
    public String momentsText;
    //用在分享直播链接时
    public String title;

    // moment图片内容的缩略图
    public String thumbnailUrl;

    // moment图片内容
    public String imageUrl;
    //话题分类
    public String themeClass = "";

    /**
     * 话题下附近的日志要显示距离
     */
    public String distance;

    /**
     * 评论id
     * */
    public String commentId;

    /**
     * 是否带有话题信息(已废弃)
     */
    public int topicFlag;
    public String topicId;
    public String topicName;
    public String topicColor;

    /**
     * 话题下未读的日志总数
     */
    public int notReadNum;

    // 提到的用户对象
    public List<MentionedUserBean> atUserList = new ArrayList<>();

    // 点赞对象
    public List<WinkCommentBean> winkUserList = new ArrayList<>();

    //选择标签
    public String tagList;


    /**
     * 最新的三条评论，存储为json数组字符串，取出来之后再解析
     */
    public List<CommentBean> commentList;

    /**
     * 转发的根日志，目前就是话题
     */
    //    public ParentMomentBean parentMoment;

    /**
     * 音乐属性 *
     */
    public long songId;
    public String songName;
    public String artistName;
    public String albumName;
    public String albumLogo100;
    public String albumLogo444;
    public String songLocation;
    public String toURL;

    /**
     * 推荐用户卡片相关字段
     */
    public long cardUserId;
    public String cardNickName;
    public String cardAvatar;
    public int cardAge;
    public int cardHeight;
    public int cardWeight;
    public String cardIntro;
    public String cardAffection;

    // 1：置顶 2：推荐 0：其他
    public String tag;

    public long timestamp;
    public String filterType;

    /**
     * 折叠的几个属性，分别是 是否折叠，有多少人折叠，折叠原因
     */
    public int hideFlag = 0;// 0\1
    public int hideNum = 0;
    public int hideType = 1;// 1\2\3
    public boolean isHiding = true;// 是否折叠状态

    /**
     * 发现榜带标签头的日志需要的属性
     */
    public int momentsNum = 0;
    public int joinCount = 0;

    public long participates;

    public boolean isCoverImage;//是否是更换封面发布的日志

    /**
     * 刷新评论页面所有数据（moment和评论）时用到的属性 *
     */
    public int commentNumThisPage;
    public CommentListBean commentListBean;
    /**
     * 刷新评论页面所有数据（moment和评论）时用到的属性 *
     */

    public static final int SHARE_TO_ALL = 1;
    public static final int SHARE_TO_FRIENDS = 2;
    public static final int SHARE_TO_ONLY_ME = 3;

    public static final int IS_SECRET = 1;
    public static final int IS_NOT_SECRET = 0;

    public static final int IS_MINE = 1;
    public static final int NOT_MINE = 0;

    public static final int HAS_NOT_WINKED = 0;//未点赞
    public static final int HAS_WINKED = 1;//已点赞

    public static final String TAG_TOP = "1";
    public static final String TAG_RECOMMEND = "2";
    public static final String TAG_OTHER = "0";

    // 朋友圈的类型
    public static final String MOMENT_TYPE_TEXT = "text";//纯文本
    public static final String MOMENT_TYPE_IMAGE = "image";//图片日志
    public static final String MOMENT_TYPE_TEXT_IMAGE = "text_image";//带文本的图片日志
    public static final String MOMENT_TYPE_VOICE = "voice";//纯音乐日志
    public static final String MOMENT_TYPE_TEXT_VOICE = "text_voice";//带文字的音乐日志
    public static final String MOMENT_TYPE_THEME = "theme";// 话题
    public static final String MOMENT_TYPE_THEME_REPLY = "themereply";// 参与话题
    public static final String MOMENT_TYPE_VIDEO = "video";// 视频
    public static final String MOMENT_TYPE_LIVE = "live";// 直播
    public static final String MOMENT_TYPE_USER_CARD = "user_card";// 推荐用户卡片
    public static final String MOMENT_TYPE_RECOMMEND = "recommend";// 推荐日志（转发）
    public static final String MOMENT_TYPE_WEB = "web";//网页日志
    public static final String MOMENT_TYPE_LIVE_USER = "live_user";//直播用户
    public static final String MOMENT_TYPE_AD = "ad";
    public static final String MOMENT_TYPE_VOICE_LIVE = "voice_live";
    public static final String MSG_TYPE_UNSUPPORTED = "unsupported";


    /**
     * 话题日志可能会有有一个特殊的标签
     */
    public String themeTagName;
    /**
     * 话题言值
     */
    public long themeParticipates = 0;

    // 预发布日志的标识
    public static final String SEND_MOMENT_FLAG = "send";
    // 未上传成功的日志
    //    public String imgsToUpload = "";
    // 预发布日志已上传成功的图片地址
    //    public String uploadedImgsUrls = "";
    /**
     * 视频大小
     */
    public long mediaSize;
    /**
     * 视频时长
     */
    public int playTime;
    /**
     * 视频分辨率
     */
    public int pixelWidth = 640;
    public int pixelHeight = 640;
    /**
     * 视频地址
     */
    public String videoUrl = "";
    /**
     * 视频播放次数
     */
    public int playCount;

    /**
     * 直播间id
     */
    public String liveId;
    /**
     * 新增属性，（播放状态&,观看人数），-1 代表已结束。else为 观看人数
     */
    public int liveStatus = -1;
    /**
     * 新增属性，relaId
     */
    public String userName;


    public int audioType;

    /**
     * 4.0.0 新增属性，推荐日志的父日志id
     */
    public String parentId;

    public String description;

    public MomentParentBean parentMoment;//推荐日志的父日志

    public List<MomentRepeatUserBean> recommendUserList = new ArrayList<>();

    public LiveUserMomentBean liveUserMoment;

    public RecommendMomentBean recommendMoment;

    public static final int CONTENT_STATUS_WHOLE = 0;//当前内容显示状态为全文（可以收起）
    public static final int CONTENT_STATUS_HANG = 1;//当前内容显示状态为收起（可以全文）
    public int contentStatus = CONTENT_STATUS_HANG;//默认收起状态
    public int textLines = 0;
    public static final int MAX_EXPAND_LINES = 30;

    public boolean isVideoPlaying = false;

    public String themeReplyClass;

    public int joinTotal;
    /**
     * 0未恢复，1已恢复
     */
    public int deleteFlag;

    public String expire;

    public String adUrl;

    public String appSchemeUrl;

    public String adType;

    /***一下为4.7.0新增属性***/

    /*** 新增视频日志webp***/
    public String videoWebp;

    /***新增视频日志社快***/
    public String videoColor;

    /***新增视频推荐标签类型***/
    public int videoTag;

    /**
     * 是否是横屏直播 1横屏 0竖屏
     */
    public int isLandscape;

    public String msgText;

    @Override
    public String toString() {
        return "MomentsBean{" + "momentsId='" + momentsId + '\'' + ", userBoardTop=" + userBoardTop + ", userId=" + userId + ", nickname='" + nickname + '\'' + ", avatar='" + avatar + '\'' + ", commentNum=" + commentNum + ", winkNum=" + winkNum + ", winkFlag=" + winkFlag + ", followerStatus=" + followerStatus + ", myself=" + myself + ", releaseTime=" + releaseTime + ", momentsTime='" + momentsTime + '\'' + ", momentsType='" + momentsType + '\'' + ", shareTo=" + shareTo + ", secret=" + secret + ", momentsText='" + momentsText + '\'' + ", thumbnailUrl='" + thumbnailUrl + '\'' + ", imageUrl='" + imageUrl + '\'' + ", themeClass='" + themeClass + '\'' + ", distance='" + distance + '\'' + ", topicFlag=" + topicFlag + ", topicId='" + topicId + '\'' + ", topicName='" + topicName + '\'' + ", topicColor='" + topicColor + '\'' + ", notReadNum=" + notReadNum + ", atUserList=" + atUserList + ", winkUserList=" + winkUserList + ", commentList=" + commentList + ", parentMoment='" + parentMoment + '\'' + ", songId=" + songId + ", songName='" + songName + '\'' + ", artistName='" + artistName + '\'' + ", albumName='" + albumName + '\'' + ", albumLogo100='" + albumLogo100 + '\'' + ", albumLogo444='" + albumLogo444 + '\'' + ", songLocation='" + songLocation + '\'' + ", toURL='" + toURL + '\'' + ", cardUserId=" + cardUserId + ", cardNickName='" + cardNickName + '\'' + ", cardAvatar='" + cardAvatar + '\'' + ", cardAge=" + cardAge + ", cardHeight=" + cardHeight + ", cardWeight=" + cardWeight + ", cardIntro='" + cardIntro + '\'' + ", cardAffection='" + cardAffection + '\'' + ", tag='" + tag + '\'' + ", timestamp=" + timestamp + ", filterType='" + filterType + '\'' + ", hideFlag=" + hideFlag + ", hideNum=" + hideNum + ", hideType=" + hideType + ", isHiding=" + isHiding + ", momentsNum=" + momentsNum + ", joinCount=" + joinCount + ", commentNumThisPage=" + commentNumThisPage + ", commentListBean=" + commentListBean + ", themeTagName='" + themeTagName + '\'' + ", themeParticipates=" + themeParticipates +
                //                ", imgsToUpload='" + imgsToUpload + '\'' +
                //                ", uploadedImgsUrls='" + uploadedImgsUrls + '\'' +
                ", mediaSize=" + mediaSize + ", playTime=" + playTime + ", pixelWidth=" + pixelWidth + ", pixelHeight=" + pixelHeight + ", videoUrl='" + videoUrl + '\'' + ", playCount=" + playCount + ", liveId='" + liveId + '\'' + ", liveStatus=" + liveStatus + ", userName='" + userName + '\'' + ", parentId='" + parentId + '\'' + ", recommendUserList=" + recommendUserList + ", liveUserMoment=" + liveUserMoment + ", recommendMoment=" + recommendMoment + ", contentStatus=" + contentStatus + ", textLines=" + textLines + '}';
    }

    public boolean isMusicPlaying = false;

    @Override
    public void setActive(View newActiveView, int newActiveViewPosition) {
        L.d("MomentsBean", " setActive newActiveViewPosition " + newActiveViewPosition);

    }

    @Override
    public void deactivate(View currentView, int deactivate) {
        L.d("MomentsBean", " deactivate deactivate " + deactivate);

    }

    public String buildUserCardInfoStr() {
        ArrayList<String> affections = new ArrayList<>(Arrays.asList(TheLApp.getContext().getResources().getStringArray(R.array.userinfo_relationship)));
        final StringBuilder sb = new StringBuilder();
        sb.append(cardAge).append(TheLApp.getContext().getString(R.string.updatauserinfo_activity_age_unit)).append("，").append(getHandW(cardHeight, cardWeight));
        if (!TextUtils.isEmpty(cardAffection)) {
            try {
                sb.append("，" + affections.get(Integer.valueOf(cardAffection)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }


    //身高体重
    private String getHandW(int height, int weight) {
        int heightUnits = ShareFileUtils.getInt(ShareFileUtils.HEIGHT_UNITS, 0); // 身高单位(0=cm, 1=Inches)
        int weightUnits = ShareFileUtils.getInt(ShareFileUtils.WEIGHT_UNITS, 0); // 体重单位(0=kg, 1=Lbs)
        try {
            String h;
            String w;
            if (heightUnits == 0) {
                h = height + " cm";
            } else {
                h = height + " Inches";
            }
            if (weightUnits == 0) {
                w = weight + " kg";
            } else {
                w = weight + " Lbs";
            }
            return h + "，" + w;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public JSONObject toJson(String momentType) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("momentsId", momentsId);
            obj.put("userId", userId);
            obj.put("nickname", nickname);
            obj.put("avatar", avatar);
            obj.put("momentsText", momentsText);
            obj.put("secret", secret);
            obj.put("momentsType", momentsType);
            obj.put("commentNum", commentNum);
            obj.put("userName", userName);
            //视频
            obj.put("videoUrl", videoUrl);
            obj.put("playTime", playTime);
            obj.put("playCount", playCount);
            obj.put("pixelWidth", pixelWidth);
            obj.put("pixelHeight", pixelHeight);
            //直播
            obj.put("liveId", liveId);
            obj.put("isLandscape", isLandscape);

            /***以上数据一直存在*/
            //图片，在图片日志，话题日志，视频日志里面有，为了统一（非推荐主播日志并且有图片的地方都传）
            if (!TextUtils.isEmpty(imageUrl) && !MOMENT_TYPE_LIVE_USER.equals(momentType)) {
                obj.put("imageUrl", imageUrl);
            }
            //话题类型(数据量小，传)
            obj.put("themeClass", themeClass);
            //话题父日志json
            if (MOMENT_TYPE_RECOMMEND.equals(momentType) || MOMENT_TYPE_THEME_REPLY.equals(momentType)) {
                obj.put("parentMoment", parentMoment);
            }
            //音乐
            if (MOMENT_TYPE_VOICE.equals(momentType) || MOMENT_TYPE_TEXT_VOICE.equals(momentType)) {
                obj.put("songId", songId);
                obj.put("songName", songName);
                obj.put("artistName", artistName);
                obj.put("albumName", albumName);
                obj.put("albumLogo100", albumLogo100);
                obj.put("albumLogo444", albumLogo444);
                obj.put("toURL", toURL);
            }
            //用户卡片
            if (MOMENT_TYPE_USER_CARD.equals(momentType)) {
                obj.put("cardUserId", cardUserId);
                obj.put("cardNickName", cardNickName);
                obj.put("cardAvatar", cardAvatar);
                obj.put("cardAge", cardAge);
                obj.put("cardHeight", cardHeight);
                obj.put("cardWeight", cardWeight);
                obj.put("cardIntro", cardIntro);
                obj.put("cardAffection", cardAffection);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return obj;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj != null && obj instanceof MomentsBean && !TextUtils.isEmpty(((MomentsBean) obj).momentsId)) {
            return ((MomentsBean) obj).momentsId.equals(momentsId);
        }
        if (obj instanceof String && !TextUtils.isEmpty((String) obj)) {
            return obj.equals(momentsId);
        }
        return super.equals(obj);
    }

}
