package com.thel.bean.me;

import com.thel.base.BaseDataBean;
import com.thel.bean.HotThemeBean;

import java.util.List;

/**
 * 日志-标签 页面 我的所有标签
 */
public class MyTagsBean extends BaseDataBean {

    public List<HotThemeBean> hotThemeList ;

}