package com.thel.bean;

import com.thel.base.BaseDataBean;

import java.util.List;

/**
 * Created by liuyun on 2017/11/22.
 */

public class WebViewBean extends BaseDataBean{

    public List<WebViewDataBean> data;

    public class WebViewDataBean{
        public WebViewUserBean user;
        public int gold;
    }

    public class WebViewUserBean{

        public long id;

        public int level;

        public String nickName;

        public String avatar;

        public String roleName;

        public String isFollow;

    }
}
