package com.thel.bean;

/**
 * Created by liuyun on 2017/11/7.
 */

public class MusicInfoBean {

    public MusicInfoBean() {

    }

    public MusicInfoBean(long songId, String momentId) {
        this.songId = songId;
        this.momentId = momentId;
    }

    public long songId;

    public String momentId;

}
