package com.thel.bean;

import android.content.ContentValues;
import android.database.Cursor;

import com.thel.base.BaseDataBean;
import com.thel.modules.main.messages.db.DataBaseAdapter;
import com.thel.utils.JsonUtils;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * RecentAndHotTagBean bean
 *
 * @author Setsail
 */
public class RecentAndHotTagBean extends BaseDataBean implements Serializable {

    /**
     * 话题id
     */
    public String topicId;

    /**
     * 话题名称
     */
    public String topicName;

    /**
     * 话题背景色
     */
    public String topicColor;

    /**
     * 标签下的日志数
     */
    public int momentsNum = 0;

    /**
     * 关注该标签的人数
     */
    public int joinCount = 0;

    // 在搜索话题页面，加载最近参与的话题和热门话题时用到，1加最近标签标题，2加流行标签标题
    public String recentOrHotTag = "";

    /**
     * 从json对象封装对象
     *
     * @param winkCommentJson
     */
    public void fromJson(JSONObject winkCommentJson) {
        topicId = JsonUtils.getString(winkCommentJson, "topicId", "0");
        topicName = JsonUtils.getString(winkCommentJson, "topicName", "");
        topicColor = JsonUtils.getString(winkCommentJson, "topicColor", "");
        joinCount = JsonUtils.getInt(winkCommentJson, "joinCount", 0);
        momentsNum = JsonUtils.getInt(winkCommentJson, "momentsNum", 0);
    }

    public ContentValues getContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DataBaseAdapter.F_TOPIC_NAME, topicName);
        contentValues.put(DataBaseAdapter.F_TOPIC_ID, topicId);
        contentValues.put(DataBaseAdapter.F_TOPIC_COLOR, topicColor);
        contentValues.put(DataBaseAdapter.F_JOIN_COUNT, joinCount);
        contentValues.put(DataBaseAdapter.F_MOMENTS_NUM, momentsNum);
        contentValues.put(DataBaseAdapter.F_RECENT_OR_HOT_TAG, recentOrHotTag);
        return contentValues;
    }

    public void fromCursor(Cursor cursor) {
        topicName = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.F_TOPIC_NAME));
        topicId = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.F_TOPIC_ID));
        topicColor = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.F_TOPIC_COLOR));
        joinCount = cursor.getInt(cursor.getColumnIndex(DataBaseAdapter.F_JOIN_COUNT));
        momentsNum = cursor.getInt(cursor.getColumnIndex(DataBaseAdapter.F_MOMENTS_NUM));
        recentOrHotTag = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.F_RECENT_OR_HOT_TAG));
    }

}
