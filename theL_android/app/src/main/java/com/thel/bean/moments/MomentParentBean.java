package com.thel.bean.moments;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.MentionedUserBean;
import com.thel.bean.comment.CommentBean;
import com.thel.bean.comment.CommentListBean;
import com.thel.bean.gift.WinkCommentBean;
import com.thel.ui.MentionedUserClickSpan;
import com.thel.ui.MentionedUserSpan;
import com.thel.utils.MomentUtils;
import com.thel.utils.ShareFileUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by waiarl on 2017/7/3.
 */

public class MomentParentBean implements Serializable {
    /**
     * moments的唯一标识
     */
    public String momentsId = "";

    /**
     * 日志是否置顶（在个人主页置顶）
     */
    public int userBoardTop = 0;

    /**
     * 用户id
     */
    public int userId;

    /**
     * 用户名
     */
    public String nickname;

    /**
     * 方头像地址
     */
    public String avatar;

    /**
     * 评论数量
     */
    public int commentNum;

    /**
     * 点赞数量
     */
    public int winkNum;

    /**
     * 是否点过赞 0:没点过，1~9
     */
    public int winkFlag;

    /**
     * 是否关注了该用户 0:没关注过，1:关注过
     */
    public int followerStatus;

    /**
     * 是不是我自己的moment，1：是，0：否
     */
    public int myself;

    /**
     * moment发布的时间
     */
    public long releaseTime;

    /**
     * moment发布的时间（与服务器时间的偏移）
     */
    public String momentsTime;

    /**
     * 朋友圈的类型：text,image, text_image,voice,text_voice,theme,theme_reply,video,live
     */
    public String momentsType;

    /**
     * 发布的类型，1 代表 all, 2 代表 friends,3 代表 only me
     */
    public int shareTo;

    /**
     * 是否匿名（0 代表 不匿名，1 代表 匿名）
     */
    public int secret;

    // moment的文字内容
    public String momentsText;

    // moment图片内容的缩略图
    public String thumbnailUrl;

    // moment图片内容
    public String imageUrl;
    //话题分类
    public String themeClass;
    //会员
    public int level;
    /**
     * 话题下附近的日志要显示距离
     */
    public String distance;

    /**
     * 是否带有话题信息(已废弃)
     */
    public int topicFlag;
    public String topicId;
    public String topicName;
    public String topicColor;

    /**
     * 话题下未读的日志总数
     */
    public int notReadNum;

    // 提到的用户对象
    public List<MentionedUserBean> atUserList;

    // 点赞对象
    public List<WinkCommentBean> winkComments;

    /**
     * 最新的三条评论，存储为json数组字符串，取出来之后再解析
     */
    public List<CommentBean> commentList;

    /**
     * 转发的根日志，目前就是话题
     */
    //    public String parentMoment = "";

    /**
     * 音乐属性 *
     */
    public long songId;
    public String songName;
    public String artistName;
    public String albumName;
    public String albumLogo100;
    public String albumLogo444;
    public String songLocation;
    public String toURL;

    /**
     * 推荐用户卡片相关字段
     */
    public long cardUserId;
    public String cardNickName;
    public String cardAvatar;
    public int cardAge;
    public int cardHeight;
    public int cardWeight;
    public String cardIntro;
    public String cardAffection;

    // 1：置顶 2：推荐 0：其他
    public String tag;

    public long timestamp;
    public String filterType;
    public String themeReplyClass;

    /**
     * 折叠的几个属性，分别是 是否折叠，有多少人折叠，折叠原因
     */
    public int hideFlag = 0;// 0\1
    public int hideNum = 0;
    public int hideType = 1;// 1\2\3
    public boolean isHiding = true;// 是否折叠状态

    /**
     * 发现榜带标签头的日志需要的属性
     */
    public int momentsNum = 0;
    public int joinCount = 0;

    /**
     * 刷新评论页面所有数据（moment和评论）时用到的属性 *
     */
    public int commentNumThisPage;
    public CommentListBean commentListBean;
    /**
     * 刷新评论页面所有数据（moment和评论）时用到的属性 *
     */

    public static final int SHARE_TO_ALL = 1;
    public static final int SHARE_TO_FRIENDS = 2;
    public static final int SHARE_TO_ONLY_ME = 3;

    public static final int IS_SECRET = 1;
    public static final int IS_NOT_SECRET = 0;

    public static final int IS_MINE = 1;
    public static final int NOT_MINE = 0;

    public static final int HAS_NOT_WINKED = 0;

    public static final String TAG_TOP = "1";
    public static final String TAG_RECOMMAND = "2";
    public static final String TAG_OHTER = "0";


    /**
     * 话题日志可能会有有一个特殊的标签
     */
    public String themeTagName;
    /**
     * 话题言值
     */
    public long themeParticipates = 0;

    // 预发布日志的标识
    public static final String SEND_MOMENT_FLAG = "send";
    // 未上传成功的日志
    public String imgsToUpload = "";
    // 预发布日志已上传成功的图片地址
    public String uploadedImgsUrls = "";
    /**
     * 视频大小
     */
    public long mediaSize;
    /**
     * 视频时长
     */
    public int playTime;
    /**
     * 视频分辨率
     */
    public int pixelWidth = 640;
    public int pixelHeight = 640;
    /**
     * 视频地址
     */
    public String videoUrl = "";
    /**
     * 视频播放次数
     */
    public int playCount;

    /**
     * 直播间id
     */
    public String liveId;
    /**
     * 新增属性，（播放状态&,观看人数），-1 代表已结束。else为 观看人数
     */
    public int liveStatus = -1;
    /**
     * 新增属性，relaId
     */
    public String userName;

    /**
     * 4.0.0 新增属性，推荐日志的父日志id
     */
    public String parentId;

    public int deleteFlag = 0;//是否被删除标志（1为删除，0 未删除，正常）

    public static final int CONTENT_STATUS_WHOLE = 0;//当前内容显示状态为全文（可以收起）
    public static final int CONTENT_STATUS_HANG = 1;//当前内容显示状态为收起（可以全文）
    public int contentStatus = CONTENT_STATUS_HANG;//默认收起状态
    public int textLines = 0;

    public String imgUrl;

    public String commentsNum;

    /**
     * 言值（所有回复这个话题的日志下的评论总数）
     */
    public int joinTotal;

    public String adUrl;

    public LiveUserMomentBean liveUserMoment;

    public String adType;

    /**
     * 1横屏 0竖屏
     */
    public int isLandscape;

    @Override
    public String toString() {
        return "MomentParentBean{" + "momentsId='" + momentsId + '\'' + ", userBoardTop=" + userBoardTop + ", userId=" + userId + ", nickname='" + nickname + '\'' + ", avatar='" + avatar + '\'' + ", commentNum=" + commentNum + ", winkNum=" + winkNum + ", winkFlag=" + winkFlag + ", followerStatus=" + followerStatus + ", myself=" + myself + ", releaseTime=" + releaseTime + ", momentsTime='" + momentsTime + '\'' + ", momentsType='" + momentsType + '\'' + ", shareTo=" + shareTo + ", secret=" + secret + ", momentsText='" + momentsText + '\'' + ", thumbnailUrl='" + thumbnailUrl + '\'' + ", imageUrl='" + imageUrl + '\'' + ", themeClass='" + themeClass + '\'' + ", distance='" + distance + '\'' + ", topicFlag=" + topicFlag + ", topicId='" + topicId + '\'' + ", topicName='" + topicName + '\'' + ", topicColor='" + topicColor + '\'' + ", notReadNum=" + notReadNum + ", atUserList=" + atUserList + ", winkComments=" + winkComments + ", commentList='" + commentList + '\'' + ", songId=" + songId + ", songName='" + songName + '\'' + ", artistName='" + artistName + '\'' + ", albumName='" + albumName + '\'' + ", albumLogo100='" + albumLogo100 + '\'' + ", albumLogo444='" + albumLogo444 + '\'' + ", songLocation='" + songLocation + '\'' + ", toURL='" + toURL + '\'' + ", cardUserId=" + cardUserId + ", cardNickName='" + cardNickName + '\'' + ", cardAvatar='" + cardAvatar + '\'' + ", cardAge=" + cardAge + ", cardHeight=" + cardHeight + ", cardWeight=" + cardWeight + ", cardIntro='" + cardIntro + '\'' + ", cardAffection='" + cardAffection + '\'' + ", tag='" + tag + '\'' + ", timestamp=" + timestamp + ", filterType='" + filterType + '\'' + ", hideFlag=" + hideFlag + ", hideNum=" + hideNum + ", hideType=" + hideType + ", isHiding=" + isHiding + ", momentsNum=" + momentsNum + ", joinCount=" + joinCount + ", commentNumThisPage=" + commentNumThisPage + ", commentListBean=" + commentListBean + ", themeTagName='" + themeTagName + '\'' + ", themeParticipates=" + themeParticipates + ", imgsToUpload='" + imgsToUpload + '\'' + ", uploadedImgsUrls='" + uploadedImgsUrls + '\'' + ", mediaSize=" + mediaSize + ", playTime=" + playTime + ", pixelWidth=" + pixelWidth + ", pixelHeight=" + pixelHeight + ", videoUrl='" + videoUrl + '\'' + ", playCount=" + playCount + ", liveId='" + liveId + '\'' + ", liveStatus=" + liveStatus + ", userName='" + userName + '\'' + ", parentId='" + parentId + '\'' + ", deleteFlag=" + deleteFlag + ", contentStatus=" + contentStatus + ", textLines=" + textLines + '}';
    }

    public SpannableString getMetionedShowString() {
        if (atUserList.size() == 0) {
            return null;
        }
        StringBuilder sb = new StringBuilder(TheLApp.getContext().getString(R.string.moments_mentioned));
        int size = 1;
        if (atUserList.size() >= 2) {
            size = 2;
        }
        for (int i = 0; i < size; i++) {
            sb.append(atUserList.get(i).nickname);
            sb.append(", ");
        }
        sb.delete(sb.length() - 2, sb.length());
        if (atUserList.size() > 2) {
            sb.append(TheLApp.getContext().getString(R.string.moments_and));
            sb.append(atUserList.size() - 2);
            sb.append(TheLApp.getContext().getString(R.string.moments_more));
        }

        // 构造高亮点击样式
        SpannableString str = new SpannableString(sb.toString());
        if (atUserList.size() > 0) {// 第一个用户
            str.setSpan(new MentionedUserClickSpan(atUserList.get(0).userId, momentsId), sb.indexOf(atUserList.get(0).nickname), sb.indexOf(atUserList.get(0).nickname) + atUserList.get(0).nickname.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            if (atUserList.size() > 1) {// 第二个用户
                str.setSpan(new MentionedUserClickSpan(atUserList.get(1).userId, momentsId), sb.indexOf(atUserList.get(1).nickname), sb.indexOf(atUserList.get(1).nickname) + atUserList.get(1).nickname.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                if (atUserList.size() > 2) {// more
                    str.setSpan(new MentionedUserClickSpan(-1, momentsId), sb.indexOf(atUserList.get(1).nickname) + atUserList.get(1).nickname.length(), sb.indexOf(atUserList.size() - 2 + TheLApp.getContext().getString(R.string.moments_more)) + (atUserList.size() - 2 + TheLApp.getContext().getString(R.string.moments_more)).length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
            }
        }

        return str;
    }

    public String buildUserCardInfoStr() {
        ArrayList<String> affections = new ArrayList<>(Arrays.asList(TheLApp.getContext().getResources().getStringArray(R.array.userinfo_relationship)));
        final StringBuilder sb = new StringBuilder();
        sb.append(cardAge).append(TheLApp.getContext().getString(R.string.updatauserinfo_activity_age_unit)).append("，").append(getHandW(cardHeight, cardWeight));
        if (!TextUtils.isEmpty(cardAffection)) {
            try {
                sb.append("，" + affections.get(Integer.valueOf(cardAffection)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    //身高体重
    private String getHandW(int height, int weight) {
        int heightUnits = ShareFileUtils.getInt(ShareFileUtils.HEIGHT_UNITS, 0); // 身高单位(0=cm, 1=Inches)
        int weightUnits = ShareFileUtils.getInt(ShareFileUtils.WEIGHT_UNITS, 0); // 体重单位(0=kg, 1=Lbs)
        try {
            String h;
            String w;
            if (heightUnits == 0) {
                h = height + " cm";
            } else {
                h = height + " Inches";
            }
            if (weightUnits == 0) {
                w = weight + " kg";
            } else {
                w = weight + " Lbs";
            }
            return h + "，" + w;
        } catch (Exception e) {
        }
        return "";
    }

    public String getReleaseTimeShow() {
        if (TextUtils.isEmpty(momentsTime)) {
            return "";
        }
        String[] arr = this.momentsTime.split("_");
        if (arr.length < 2) {
            return "";
        }
        if (secret == 0) {// 非匿名
            if ("0".equals(arr[0])) {
                return TheLApp.getContext().getString(R.string.info_just_now);
            } else if ("1".equals(arr[0])) {
                return arr[1] + TheLApp.getContext().getString(R.string.info_seconds_ago);
            } else if ("2".equals(arr[0])) {
                return arr[1] + TheLApp.getContext().getString(R.string.info_minutes_ago);
            } else if ("3".equals(arr[0])) {
                return arr[1] + TheLApp.getContext().getString(R.string.info_hours_ago);
            } else if ("4".equals(arr[0])) {
                return arr[1] + TheLApp.getContext().getString(R.string.info_days_ago);
            } else if ("5".equals(arr[0])) {
                return arr[1];
            }
        } else {// 匿名
            if ("11".equals(arr[0])) {
                return TheLApp.getContext().getString(R.string.chat_activity_today);
            } else if ("12".equals(arr[0])) {
                return arr[1] + TheLApp.getContext().getString(R.string.info_days_ago);
            } else if ("13".equals(arr[0])) {
                return arr[1];
            }
        }
        return "";

    }

    /**
     * 根据日志类型设置昵称
     *
     * @param textView
     * @param momentsBean
     */
    public void setNicknameWithMomentType(TextView textView, MomentParentBean momentsBean) {
        if (textView == null || momentsBean == null) {
            return;
        }
        SpannableString sp = null;
        final String nickName = momentsBean.nickname;
        if (TextUtils.isEmpty(momentsBean.momentsType)) {
            sp = MomentUtils.buildThemeHeaderStr(nickName, "");
            textView.setText(sp);
            return;
        }
        if (MomentsBean.MOMENT_TYPE_THEME.equals(momentsBean.momentsType)) {//发布话题
            sp = MomentUtils.buildThemeHeaderStr(nickName, TheLApp.getContext().getString(R.string.posted_a_theme));
        } else if (MomentsBean.MOMENT_TYPE_THEME_REPLY.equals(momentsBean.momentsType)) {//参与话题
            sp = MomentUtils.buildThemeHeaderStr(nickName, TheLApp.getContext().getString(R.string.participated_a_theme));
        } else if (MomentsBean.MOMENT_TYPE_LIVE_USER.equals(momentsBean.momentsType)) {//推荐主播
            sp = MomentUtils.buildThemeHeaderStr(nickName, TheLApp.getContext().getString(R.string.shared_a_anchor));
        } else if (MomentsBean.MOMENT_TYPE_USER_CARD.equals(momentsBean.momentsType)) {//推荐名片
            sp = MomentUtils.buildThemeHeaderStr(nickName, TheLApp.getContext().getString(R.string.recommend_a_user) + momentsBean.momentsText);
        } else if (MomentsBean.MOMENT_TYPE_WEB.equals(momentsBean.momentsType)) {//推荐网页
            sp = MomentUtils.buildThemeHeaderStr(nickName, TheLApp.getContext().getString(R.string.shared_a_website));
        } else if (MomentsBean.MOMENT_TYPE_RECOMMEND.equals(momentsBean.momentsType)) {//推荐日志
            if (TextUtils.isEmpty(momentsBean.momentsType)) {
                sp = MomentUtils.buildThemeHeaderStr(nickName, TheLApp.getContext().getString(R.string.recommend_a_moment));
                textView.setText(sp);
                return;
            }
            final MomentParentBean bean = momentsBean;
            if (MomentsBean.MOMENT_TYPE_THEME.equals(bean.momentsType)) {//推荐话题话题
                sp = MomentUtils.buildThemeHeaderStr(nickName, TheLApp.getContext().getString(R.string.shared_a_topic));
            } else if (MomentsBean.MOMENT_TYPE_VOICE.equals(bean.momentsType) || MomentsBean.MOMENT_TYPE_TEXT_VOICE.equals(bean.momentsType)) {//推荐音乐
                sp = MomentUtils.buildThemeHeaderStr(nickName, TheLApp.getContext().getString(R.string.shared_a_song));
            } else {
                sp = MomentUtils.buildThemeHeaderStr(nickName, TheLApp.getContext().getString(R.string.recommend_a_moment));
            }
        } else {
            sp = MomentUtils.buildThemeHeaderStr(nickName, "");
        }
        textView.setText(sp);
    }

    public SpannableString getMetionedShowStringForSendMoment() {
        if (atUserList.size() == 0) {
            return null;
        }
        StringBuilder sb = new StringBuilder(TheLApp.getContext().getString(R.string.moments_mentioned));
        int size = 1;
        if (atUserList.size() >= 2) {
            size = 2;
        }
        for (int i = 0; i < size; i++) {
            sb.append(atUserList.get(i).nickname);
            sb.append(", ");
        }
        sb.delete(sb.length() - 2, sb.length());
        if (atUserList.size() > 2) {
            sb.append(TheLApp.getContext().getString(R.string.moments_and));
            sb.append(atUserList.size() - 2);
            sb.append(TheLApp.getContext().getString(R.string.moments_more));
        }

        // 构造高亮点击样式
        SpannableString str = new SpannableString(sb.toString());
        if (atUserList.size() > 0) {// 第一个用户
            str.setSpan(new MentionedUserSpan(), sb.indexOf(atUserList.get(0).nickname), sb.indexOf(atUserList.get(0).nickname) + atUserList.get(0).nickname.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            if (atUserList.size() > 1) {// 第二个用户
                str.setSpan(new MentionedUserSpan(), sb.indexOf(atUserList.get(1).nickname), sb.indexOf(atUserList.get(1).nickname) + atUserList.get(1).nickname.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                if (atUserList.size() > 2) {// more
                    str.setSpan(new MentionedUserSpan(), sb.indexOf(atUserList.size() - 2 + TheLApp.getContext().getString(R.string.moments_more)), sb.indexOf(atUserList.size() - 2 + TheLApp.getContext().getString(R.string.moments_more)) + (atUserList.size() - 2 + TheLApp.getContext().getString(R.string.moments_more)).length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
            }
        }

        return str;
    }


}
