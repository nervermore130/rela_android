package com.thel.bean.comment;

import java.io.Serializable;

/**
 * Created by waiarl on 2017/8/14.
 * 话题评论中的视频信息
 */

public class CommentVideoBean implements Serializable {
    public String videoUrl = "";
    public int playTime;
    public int mediaSize;
    public long playCount;
    public int pixelWidth;
    public int pixelHeight;

}
