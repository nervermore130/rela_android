package com.thel.bean.live;

import com.thel.base.BaseDataBean;
import com.thel.modules.live.bean.LiveRoomBean;

import java.util.ArrayList;

/**
 * 直播室列表
 *
 * @author Setsail
 */
public class LiveRoomsBean extends BaseDataBean {

    public ArrayList<LiveRoomBean> list = new ArrayList<>();
    public String label;
    public String imageUrl;
    public int cursor;

}