package com.thel.bean.comment;

import android.util.Log;

import com.thel.bean.moments.MomentsBean;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

/**
 * Comment bean
 *
 * @author Setsail
 */
public class CommentBean implements Serializable {

    /**
     * moments的唯一标识
     */
    public String momentsId;

    /**
     * 用户id
     */
    public int userId;

    /**
     * 用户名
     */
    public String nickname;

    /**
     * 方头像地址
     */
    public String avatar;

    /**
     * 评论id，当在是话题下的评论时，这个commentId其实就是该评论对应的日志id
     */
    public String commentId;

    // 评论时间
    public String commentTime;

    // 评论内容
    public String commentText;

    // 评论对象
    public int toUserId;

    // 评论对象nickname
    public String toNickname;

    // 评论对象方头像地址
    public String toAvatar;

    // 评论类型,say 或 to,say是直接评论，to是对某人的评论进行评论
    public String sayType;

    // 评论类型，text、sticker
    public String commentType;

    /**
     * 话题日志下的评论会带表态数和评论数以及我的表态类型
     */
    public int winkNum;
    public int commentNum;// 在评论二级页面，commentNum就是二级评论的总数
    public int winkFlag;
    public int level;
    /**
     * 最新的三条评论，存储为json数组字符串，取出来之后再解析
     */
    public List<CommentBean> commentList;

    /**
     * 话题日志的评论其实就是一条日志，所以有图片
     */
    public String imageUrl;
    /**
     * relaId
     */
    public String userName;

    /**
     * 是否关注了该用户 0:没关注过，1:关注过
     */
    public int followerStatus;
    public static final int FOLLOW_TYPE_UNFOLLOWED = 0;// 未关注
    public static final int FOLLOW_TYPE_FOLLOWED = 1;// 已关注

    public static final String SAY_TYPE_SAY = "say";
    public static final String SAY_TYPE_TO = "to";

    public static final String COMMENT_TYPE_TEXT = "text";
    public static final String COMMENT_TYPE_STICKER = "sticker";
    public static final String COMMENT_TYPE_THEME_REPLY = "themereply";

    public int contentStatus = MomentsBean.CONTENT_STATUS_HANG;//默认收起状态

    public int textLines = 0;

    /**
     * 4.1.0 话题评论的类型 voice:音乐；video:视频
     */
    public String commentTypeClass;

    /**
     * themeReplyClass 参数解释：
     */
    public static final String TYPE_CLASS_TEXT = "text";//文本
    public static final String TYPE_CLASS_IMAGE = "image";//图文
    public static final String TYPE_CLASS_VIDEO = "video";//视频
    public static final String TYPE_CLASS_VOICE = "voice";//音乐


    /**
     * 4.1.0 音乐数据
     */
    public CommentVoiceBean voiceComment;

    /**
     * 4.1.0 视频数据
     */
    public CommentVideoBean videoComment;

    @Override public String toString() {
        return "CommentBean{" +
                "momentsId='" + momentsId + '\'' +
                ", userId=" + userId +
                ", nickname='" + nickname + '\'' +
                ", avatar='" + avatar + '\'' +
                ", commentId='" + commentId + '\'' +
                ", commentTime='" + commentTime + '\'' +
                ", commentText='" + commentText + '\'' +
                ", toUserId=" + toUserId +
                ", toNickname='" + toNickname + '\'' +
                ", toAvatar='" + toAvatar + '\'' +
                ", sayType='" + sayType + '\'' +
                ", commentType='" + commentType + '\'' +
                ", winkNum=" + winkNum +
                ", commentNum=" + commentNum +
                ", winkFlag=" + winkFlag +
                ", level=" + level +
                ", commentList=" + commentList +
                ", imageUrl='" + imageUrl + '\'' +
                ", userName='" + userName + '\'' +
                ", followerStatus=" + followerStatus +
                ", contentStatus=" + contentStatus +
                ", textLines=" + textLines +
                ", commentTypeClass='" + commentTypeClass + '\'' +
                ", voiceComment=" + voiceComment +
                ", videoComment=" + videoComment +
                '}';
    }
}
