package com.thel.bean;

import java.io.Serializable;

public class InitRecommendUserMomentBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 日志id
     */
    public String momentsId;

    /**
     * 图片地址
     */
    public String imageUrl;

    /**
     * 日志内容
     */
    public String momentsText;

    /**
     * 日志类型
     */
    public String momentsType;

}
