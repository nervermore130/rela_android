package com.thel.bean.live;

import com.thel.base.BaseDataBean;

/**
 * 获取推荐关注主播列表
 * Created by lingwei on 2017/11/19.
 */

public class LiveRecommendNetBean extends BaseDataBean {
    public LiveRecommendDataBean data;
}
