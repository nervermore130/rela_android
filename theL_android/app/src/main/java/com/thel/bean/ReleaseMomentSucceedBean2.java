package com.thel.bean;

public class ReleaseMomentSucceedBean2 {
    public int code;

    public String message;

    public Data data;

    public int ttl;

    public class Data {
        public String result;

        public String errcode;

        public String errdesc;

        public String errdesc_en;

        public int momentsId;

        public String id_str;
    }
}
