package com.thel.bean.moments;

import android.annotation.SuppressLint;
import android.text.TextUtils;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseDataBean;
import com.thel.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 附近用户列表信息
 *
 * @author Setsail
 */
public class MomentsListBean extends BaseDataBean {

    public int notReadCommentNum;

    public int momentsNum;

    public int viewMyself;

    public String mainType;

    public int momentsTotalNum;
    /**
     * 是否有下一页
     */
    public boolean haveNextPage;

    public int cursor;

    public List<MomentsBean> momentsList = new ArrayList<>();

    @Override
    public String toString() {
        return "MomentsListBean{" +
                "notReadCommentNum=" + notReadCommentNum +
                ", momentsNum=" + momentsNum +
                ", viewMyself=" + viewMyself +
                ", mainType='" + mainType + '\'' +
                ", momentsTotalNum=" + momentsTotalNum +
                ", haveNextPage=" + haveNextPage +
                ", momentsList=" + momentsList +
                '}';
    }

    /**
     * 过滤屏蔽的日志
     */
    public void filterBlockMoments() {
        try {
            String[] hideMomentsUserList = SharedPrefUtils.getString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.HIDE_MOMENTS_USER_LIST, "").split(",");
            List<MomentsBean> blockMoments1 = new ArrayList<MomentsBean>();
            //过滤不看她的日志
            if (hideMomentsUserList.length > 0 && !TextUtils.isEmpty(hideMomentsUserList[0])) {
                for (MomentsBean momentsBean : momentsList) {
                    for (String userId : hideMomentsUserList) {
                        if (userId.equals(momentsBean.userId + "")) {
                            blockMoments1.add(momentsBean);
                            break;
                        }
                    }
                }
                momentsList.removeAll(blockMoments1);
            }
            // 过滤不看这条日志和举报日志
            List<String> hideMomentsList = Arrays.asList(SharedPrefUtils.getString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.HIDE_MOMENTS_LIST, "").split(","));
            List<String> reportMomentsList = Arrays.asList(SharedPrefUtils.getString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.REPORT_MOMENTS_LIST, "").split(","));
            List<String> blockMoments = new ArrayList<String>();
            if (hideMomentsList.size() > 0 && !TextUtils.isEmpty(hideMomentsList.get(0))) {
                blockMoments.addAll(hideMomentsList);
            }
            if (reportMomentsList.size() > 0 && !TextUtils.isEmpty(reportMomentsList.get(0))) {
                blockMoments.addAll(reportMomentsList);
            }
            if (blockMoments.size() > 0) {
                List<MomentsBean> blockMoments2 = new ArrayList<MomentsBean>();
                for (MomentsBean momentsBean : momentsList) {
                    for (String momentsId : blockMoments) {
                        if (momentsId.equals(momentsBean.momentsId)) {
                            blockMoments2.add(momentsBean);
                            break;
                        }
                    }
                }
                momentsList.removeAll(blockMoments2);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 时光机列表时间转换
     */
    @SuppressLint("StringFormatMatches")
    public void timeEncoder() {
        if (momentsList == null || momentsList.size() == 0) return;
        for (int i = 0; i < momentsList.size(); i++) {
            if (!TextUtils.isEmpty(momentsList.get(i).expire)) {
                String timeStr = "";
                String[] number = momentsList.get(i).expire.split("_");
                switch (number[0]) {
                    case "1"://second
                        timeStr = TheLApp.getContext().getResources().getString(R.string.last_time_second, number[1]);
                        break;
                    case "2"://minute
                        timeStr = TheLApp.getContext().getResources().getString(R.string.last_time_minute, number[1]);
                        break;
                    case "3"://hour
                        timeStr = TheLApp.getContext().getResources().getString(R.string.last_time_hour, number[1]);
                        break;
                    case "4"://day
                        timeStr = TheLApp.getContext().getResources().getString(R.string.last_time_day, number[1]);
                        break;
                }
                momentsList.get(i).expire = timeStr;
            }
        }
    }

}
