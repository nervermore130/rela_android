package com.thel.bean;

import com.thel.base.BaseDataBean;

/**
 * 表情包列表
 *
 * @author Setsail
 */
public class StickerPackListNetBean extends BaseDataBean {
    public StickerPackListBean data;
}