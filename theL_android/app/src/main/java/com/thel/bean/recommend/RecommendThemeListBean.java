package com.thel.bean.recommend;

import com.thel.base.BaseDataBean;

import java.util.List;

/**
 * 话题推荐列表
 * Created by the L on 2016/12/1.
 */
public class RecommendThemeListBean extends BaseDataBean {
    public List<RecommendThemeBean> recommendThemelist;

}
