package com.thel.bean;

import android.database.Cursor;
import android.util.Log;

import com.thel.base.BaseDataBean;
import com.thel.utils.JsonUtils;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * @author Setsail
 */
public class StickerBean extends BaseDataBean implements Serializable {

    /**
     * 动态贴图的话就是id
     * 内置静态表情的话就是表情对应图片资源的id
     */
    public long id;
    /**
     * 表情包id
     */
    public long packId;
    /**
     * 表情名称
     * 内置静态表情的话就是表情对应的文本，如：[/大笑]
     */
    public String label;
    /**
     * 表情地址
     */
    public String img;
    /**
     * 表情缩略图
     */
    public String thumbnail;
    /**
     * 是不是删除按钮
     */
    public boolean isDeleteBtn = false;

    public JSONObject toJSON() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", id);
            jsonObject.put("packId", packId);
            jsonObject.put("label", label);
            jsonObject.put("thumbnail", thumbnail);
            jsonObject.put("img", img);
        } catch (Exception e) {
        }
        return jsonObject;
    }

    public String buildStickerMsgText() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("stickerId", id);
            jsonObject.put("packId", packId);
            jsonObject.put("img", img);
        } catch (Exception e) {
        }
        return jsonObject.toString();
    }

    /**
     * 从json对象封装对象
     *
     * @param tempobj
     */
    public void fromJson(JSONObject tempobj) {
        try {
            id = JsonUtils.getLong(tempobj, "id", 0);
            packId = JsonUtils.getLong(tempobj, "packId", 0);
            label = JsonUtils.getString(tempobj, "label", "");
            thumbnail = JsonUtils.getString(tempobj, "thumbnail", "");
            img = JsonUtils.getString(tempobj, "img", "");
        } catch (Exception e) {
            if (e.getMessage() != null) {
                Log.e(this.getClass().getName(), e.getMessage());
            }
        }
    }

    public void fromCombinedCursor(Cursor cursor) {
        id = cursor.getLong(cursor.getColumnIndex("sId"));
        packId = cursor.getLong(cursor.getColumnIndex("sPackId"));
        label = cursor.getString(cursor.getColumnIndex("sLabel"));
        thumbnail = cursor.getString(cursor.getColumnIndex("sThumbnail"));
        img = cursor.getString(cursor.getColumnIndex("sImg"));
    }
}
