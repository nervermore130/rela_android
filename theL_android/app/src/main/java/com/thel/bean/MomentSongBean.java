package com.thel.bean;

/**
 * Created by chad
 * Time 17/10/13
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */

public class MomentSongBean {
    /**
     * 音乐属性 *
     */
    public long songId;
    public String songName;
    public String artistName;
    public String albumName;
    public String albumLogo100;
    public String albumLogo444;
    public String songLocation;
    public String toURL;
}
