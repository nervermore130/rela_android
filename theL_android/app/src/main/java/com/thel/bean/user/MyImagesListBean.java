package com.thel.bean.user;

import com.google.gson.annotations.SerializedName;
import com.thel.base.BaseDataBean;

import java.util.List;

public class MyImagesListBean extends BaseDataBean {
/*

	*/
    /** 照片总数 *//*

	public int total;
	
	*/
    /** 照片列表 *//*

	public ArrayList<MyImageBean> imagesList;
*/
    /**
     * data : {"total":5,"imagesList":[{"picId":30921912,"longThumbnailUrl":"http://static.thel.co/app/album/9997/10a64e670c3f7769fdeff76a8be7e4ec.jpg-w204","isPrivate":0,"isBlcak":0,"picUrl":"http://static.thel.co/app/album/9997/10a64e670c3f7769fdeff76a8be7e4ec.jpg-wh2","picWidth":204,"picHeight":312,"coverFlag":"0"},{"picId":27618325,"longThumbnailUrl":"http://static.thel.co/app/album-gif/9997/ee081824d5b5a610e6ad34416c093b90.gif","isPrivate":0,"isBlcak":0,"picUrl":"http://video.thel.co/app/album/9997/ee081824d5b5a610e6ad34416c093b90.mp4","picWidth":204,"picHeight":362,"coverFlag":"1"},{"picId":18568766,"longThumbnailUrl":"http://static.thel.co/app/album/9997/f2a7102b94a8d2fa2e34cb540522b7d4.jpg-w204","isPrivate":0,"isBlcak":0,"picUrl":"http://static.thel.co/app/album/9997/f2a7102b94a8d2fa2e34cb540522b7d4.jpg-wh2","picWidth":204,"picHeight":271,"coverFlag":"0"},{"picId":17965236,"longThumbnailUrl":"http://static.thel.co/app/album/9997/e43c0a1ab14455db6847e856f0d885b3.jpg-w204","isPrivate":1,"isBlcak":0,"picUrl":"http://static.thel.co/app/album/9997/e43c0a1ab14455db6847e856f0d885b3.jpg-wh2","picWidth":204,"picHeight":306,"coverFlag":"0"},{"picId":17965207,"longThumbnailUrl":"http://static.thel.co/app/album/9997/9c9e831925a1204f98297b89245cf4d3.jpg-w204","isPrivate":0,"isBlcak":0,"picUrl":"http://static.thel.co/app/album/9997/9c9e831925a1204f98297b89245cf4d3.jpg-wh2","picWidth":204,"picHeight":153,"coverFlag":"0"}]}
     */

    public MyImagesListDataBean data;

    public static class MyImagesListDataBean {
        @Override
        public String toString() {
            return "MyImagesListDataBean{" + "totalX=" + total + ", imagesListX=" + imagesList + '}';
        }

        /**
         * total : 5
         * imagesList : [{"picId":30921912,"longThumbnailUrl":"http://static.thel.co/app/album/9997/10a64e670c3f7769fdeff76a8be7e4ec.jpg-w204","isPrivate":0,"isBlcak":0,"picUrl":"http://static.thel.co/app/album/9997/10a64e670c3f7769fdeff76a8be7e4ec.jpg-wh2","picWidth":204,"picHeight":312,"coverFlag":"0"},{"picId":27618325,"longThumbnailUrl":"http://static.thel.co/app/album-gif/9997/ee081824d5b5a610e6ad34416c093b90.gif","isPrivate":0,"isBlcak":0,"picUrl":"http://video.thel.co/app/album/9997/ee081824d5b5a610e6ad34416c093b90.mp4","picWidth":204,"picHeight":362,"coverFlag":"1"},{"picId":18568766,"longThumbnailUrl":"http://static.thel.co/app/album/9997/f2a7102b94a8d2fa2e34cb540522b7d4.jpg-w204","isPrivate":0,"isBlcak":0,"picUrl":"http://static.thel.co/app/album/9997/f2a7102b94a8d2fa2e34cb540522b7d4.jpg-wh2","picWidth":204,"picHeight":271,"coverFlag":"0"},{"picId":17965236,"longThumbnailUrl":"http://static.thel.co/app/album/9997/e43c0a1ab14455db6847e856f0d885b3.jpg-w204","isPrivate":1,"isBlcak":0,"picUrl":"http://static.thel.co/app/album/9997/e43c0a1ab14455db6847e856f0d885b3.jpg-wh2","picWidth":204,"picHeight":306,"coverFlag":"0"},{"picId":17965207,"longThumbnailUrl":"http://static.thel.co/app/album/9997/9c9e831925a1204f98297b89245cf4d3.jpg-w204","isPrivate":0,"isBlcak":0,"picUrl":"http://static.thel.co/app/album/9997/9c9e831925a1204f98297b89245cf4d3.jpg-wh2","picWidth":204,"picHeight":153,"coverFlag":"0"}]
         */

        public int total;
        @SerializedName("imagesList")
        public List<MyImageBean> imagesList;
    }
}

