package com.thel.bean.video;

import android.content.ContentValues;
import android.database.Cursor;

import com.thel.base.BaseDataBean;
import com.thel.db.MomentsDataBaseAdapter;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * 视频数据统计
 *
 * @author Setsail
 */
public class VideoReportBean extends BaseDataBean implements Serializable {

    /**
     * 用户id
     */
    public int userId;
    /**
     * 日志id
     */
    public String momentId;
    /**
     * 播放视频的时间
     */
    public long playTime;
    /**
     * 播放的时长（秒）
     */
    public int playSeconds;
    /**
     * 是否全屏播放: 1 是 0 否
     */
    public int showFull;

    public ContentValues getContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(MomentsDataBaseAdapter.F_USERID, userId);
        contentValues.put(MomentsDataBaseAdapter.F_MOMENT_ID, momentId);
        contentValues.put(MomentsDataBaseAdapter.F_PLAY_TIME, playTime);
        contentValues.put(MomentsDataBaseAdapter.F_PLAY_SECONDS, playSeconds);
        contentValues.put(MomentsDataBaseAdapter.F_SHOW_FULL, showFull);
        return contentValues;
    }

    public void fromCursor(Cursor cursor) {
        userId = cursor.getInt(cursor.getColumnIndex(MomentsDataBaseAdapter.F_USERID));
        momentId = cursor.getString(cursor.getColumnIndex(MomentsDataBaseAdapter.F_MOMENT_ID));
        playTime = cursor.getLong(cursor.getColumnIndex(MomentsDataBaseAdapter.F_PLAY_TIME));
        playSeconds = cursor.getInt(cursor.getColumnIndex(MomentsDataBaseAdapter.F_PLAY_SECONDS));
        showFull = cursor.getInt(cursor.getColumnIndex(MomentsDataBaseAdapter.F_SHOW_FULL));
    }

    public JSONObject toJSON() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(MomentsDataBaseAdapter.F_USERID, userId);
            jsonObject.put(MomentsDataBaseAdapter.F_MOMENT_ID, momentId);
            jsonObject.put(MomentsDataBaseAdapter.F_PLAY_TIME, playTime);
            jsonObject.put(MomentsDataBaseAdapter.F_PLAY_SECONDS, playSeconds);
            jsonObject.put(MomentsDataBaseAdapter.F_SHOW_FULL, showFull);
        } catch (Exception e) {
        }
        return jsonObject;
    }
}
