package com.thel.bean.user;

import com.thel.base.BaseDataBean;
import com.thel.imp.black.BlackUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 附近用户列表信息
 *
 * @author Setsail
 */
public class NearUserListNetBean extends BaseDataBean {

    public NearUserListBean data;

    public class NearUserListBean {
        public int total;

        public int cursor;

        public boolean guide;

        public int age;

        public String role_name;

        /**
         * 埋点
         * */
        public String rank_id = "";

        public List<NearUserBean> map_list = new ArrayList<>();

        public HsImagesBean hs_images;

        /**
         * 过滤黑名单用户
         */
        public void filterBlock() {
            for (int i = 0; i < map_list.size(); i++) {
                if (BlackUtils.isBlack(map_list.get(i).userId + "")) {
                    map_list.remove(i);
                    i -= 1;
                }
            }
        }
    }

    public class HsImagesBean {

        public String hit_image;

        public String star_image;
    }

}
