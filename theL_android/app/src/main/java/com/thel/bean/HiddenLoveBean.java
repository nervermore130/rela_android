package com.thel.bean;

import com.thel.base.BaseDataBean;

public class HiddenLoveBean extends BaseDataBean {

	public static final String HIDEN_LOVE_NOT_EACH_OTHER = "0";
	public static final String HIDEN_LOVE_EACH_OTHER = "1";
	
	public String HiddenLoveSuccess; // 0为单向暗恋，1为双向暗恋
	public String userId;
	public String receivedId;

}

/*
 *  {
    "data": {
        "HiddenLoveSuccess": "0",
        "userId": "9981",
        "debug": "0",
        "receivedId": "75933"
    },
    "errcode": "",
    "errdesc": "",
    "result": "1"
}
 */