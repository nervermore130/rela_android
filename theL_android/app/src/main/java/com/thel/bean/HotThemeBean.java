package com.thel.bean;

import java.io.Serializable;

/**
 * 热门话题
 *
 * @author Setsail
 */
public class HotThemeBean implements Serializable {

    /**
     * 日志id
     */
    public long momentId;

    /**
     * 日志内容
     */
    public String momentText;

    /**
     * 日志类型
     */
    public String momentType;

    /**
     * 话题背景图
     */
    public String momentImg;

    /**
     * 点赞数
     */
    public int winkNum;

    /**
     * 评论数
     */
    public int commentsNum;

    /**
     * 言值（所有回复这个话题的日志下的评论总数）
     */
    public int joinTotal;

}
