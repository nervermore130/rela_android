package com.thel.bean.user;

import com.thel.base.BaseDataBean;

/**
 * 附近用户列表信息
 *
 * @author Setsail
 */
public class LocationNameBean extends BaseDataBean {

    /**
     * 经纬度 *
     */
    public String lnglat;
    /**
     * 城市名 *
     */
    public String cityName;
    /**
     * 全名 *
     */
    public String fullName;

}
