package com.thel.bean.gift;

import com.thel.base.BaseDataBean;

import java.io.Serializable;
import java.util.List;

/**
 * 挤眼礼物 list集合
 * Created by waiarl on 2017/4/11.
 */

public class WinkGiftListBean extends BaseDataBean implements Serializable {
    public List<WinkGiftBean> list;
    public int gold;

}
