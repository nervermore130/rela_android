package com.thel.bean.user;

import java.io.Serializable;

/**
 * 其他用户信息中的图片信息
 * @author zhangwenjia
 *
 */
public class UserInfoPicBean implements Serializable {

	/** 原始图片地址 */
	public String picUrl;
	
	/** 图片是否能看到 */
	public int isPrivate;
	
	/** 图片是否设置了隐私 */
	public int isBlack;
	
	/** 长缩略图图片地址 */
	public String longThumbnailUrl;

	public String picId;
	
	/*
		"picList": [
            {
                "picUrl": "http://s-50395.gotocdn.com:8081/user-image/135411638235017.jpg",
                "isPrivate": 0,
                "longThumbnailUrl": "http://s-50395.gotocdn.com:8081/user-image/135411638235017_small.jpg"
            }
        ],
	 */

	@Override
	public String toString() {
		return "UserInfoPicBean{" + "picUrl='" + picUrl + '\'' + ", isPrivate=" + isPrivate + ", isBlack=" + isBlack + ", longThumbnailUrl='" + longThumbnailUrl + '\'' + ", picId='" + picId + '\'' + '}';
	}
}
