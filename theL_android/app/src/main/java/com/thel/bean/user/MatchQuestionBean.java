package com.thel.bean.user;

import com.thel.base.BaseDataBean;

public class MatchQuestionBean extends BaseDataBean {
    public MatchQuestionDataBean data;

    public static class MatchQuestionDataBean {
        /**
         * question : {"userId":9997,"questions":{"question1":"Ã¤Â½Â Ã¦Â\u0098Â¯Ã¨Â°Â\u0081Ã¯Â¼Â\u009f","question2":"ä½ è¿\u0098","question3":"ä½ å¥½"},"autoSend":1,"matchAgeBegin":23,"matchAgeEnd":30,"matchRole":"2,3","sameCity":0}
         */

        public QuestionBean question;

        public static class QuestionBean {
            /**
             * userId : 9997
             * questions : {"question1":"Ã¤Â½Â Ã¦Â\u0098Â¯Ã¨Â°Â\u0081Ã¯Â¼Â\u009f","question2":"ä½ è¿\u0098","question3":"ä½ å¥½"}
             * autoSend : 1
             * matchAgeBegin : 23
             * matchAgeEnd : 30
             * matchRole : 2,3
             * sameCity : 0
             */

            public int userId;
            public QuestionsBean questions;
            public int autoSend;
            public int matchAgeBegin;
            public int matchAgeEnd;
            public String matchRole;
            public int sameCity;

            @Override
            public String toString() {
                return "QuestionBean{" + "userId=" + userId + ", questions=" + questions + ", autoSend=" + autoSend + ", matchAgeBegin=" + matchAgeBegin + ", matchAgeEnd=" + matchAgeEnd + ", matchRole='" + matchRole + '\'' + ", sameCity=" + sameCity + '}';
            }

            public static class QuestionsBean {
                @Override
                public String toString() {
                    return "QuestionsBean{" + "question1='" + question1 + '\'' + ", question2='" + question2 + '\'' + ", question3='" + question3 + '\'' + '}';
                }

                /**
                 * question1 : Ã¤Â½Â Ã¦ÂÂ¯Ã¨Â°ÂÃ¯Â¼Â
                 * question2 : ä½ è¿
                 * question3 : ä½ å¥½
                 */

                public String question1;
                public String question2;
                public String question3;
            }
        }
    }
}