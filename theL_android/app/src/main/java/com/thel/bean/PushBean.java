package com.thel.bean;

public class PushBean {
    public String fromUserId;
    public String fromNickname;
    public String toUserId;
    public String messageTo;
    public String advertTitle;
    public String dumpURL;
    public String msgType;
    public String extra;
    public String expiry;
    public String payload;
    public String withAll;
    public String advertUrl;
}
