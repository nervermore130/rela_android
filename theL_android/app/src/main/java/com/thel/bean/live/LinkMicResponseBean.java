package com.thel.bean.live;

/**
 * Created by liuyun on 2017/12/22.
 */

public class LinkMicResponseBean {
    public String status;
    public String userId;
    public String nickName;
    public String avatar;
    public String selfGem;
    public String toGem;
    public String totalTime;
    public String leftTime;
    public String pkChannel;
    public float x;
    public float y;
    public float height;
    public float width;

    public boolean isAudienceLinkMic = true;

    @Override public String toString() {
        return "LinkMicResponseBean{" +
                "status='" + status + '\'' +
                ", userId='" + userId + '\'' +
                ", nickName='" + nickName + '\'' +
                ", avatar='" + avatar + '\'' +
                ", selfGem='" + selfGem + '\'' +
                ", toGem='" + toGem + '\'' +
                ", totalTime='" + totalTime + '\'' +
                ", leftTime='" + leftTime + '\'' +
                ", pkChannel='" + pkChannel + '\'' +
                ", x=" + x +
                ", y=" + y +
                ", height=" + height +
                ", width=" + width +
                '}';
    }
}
