package com.thel.bean.live;

/**
 * Created by liuyun on 2017/12/4.
 */

public class LinkMicRequestBean {

    public static class ConnectBean {
        public String method;

        public String toUserId;

        public float x;

        public float y;

        public float height;

        public float width;

        public ConnectBean(String method, String toUserId, float x, float y, float height, float width) {
            this.method = method;
            this.toUserId = toUserId;
            this.x = x;
            this.y = y;
            this.height = height;
            this.width = width;
        }
    }

    public static class ResponseConnectBean extends ConnectBean {

        public String result;

        public ResponseConnectBean(String method, String toUserId, String result, float x, float y, float height, float width) {
            super(method, toUserId, x, y, height, width);
            this.result = result;
        }
    }

    public static class CancelBean {
        public String method;

        public String toUserId;

        public CancelBean(String method, String toUserId) {
            this.method = method;
            this.toUserId = toUserId;
        }

    }

    public static class HangupBean extends CancelBean {

        public HangupBean(String method, String toUserId) {
            super(method, toUserId);
        }
    }

}
