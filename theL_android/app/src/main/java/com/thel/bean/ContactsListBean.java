package com.thel.bean;

import com.thel.base.BaseDataBean;

import java.util.ArrayList;
import java.util.List;

public class ContactsListBean extends BaseDataBean {

    public ContactDataBean data;

    public class ContactDataBean {
        public List<ContactBean> users;
    }

}
