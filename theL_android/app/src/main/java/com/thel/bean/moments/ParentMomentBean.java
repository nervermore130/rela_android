package com.thel.bean.moments;

import java.io.Serializable;

/**
 * 根日志 话题日志
 *
 * @author Setsail
 */
public class ParentMomentBean extends MomentParentBean implements Serializable {

    public long momentsId;

    public String momentsText;

    public String imgUrl;

    public int winkNum;

    public int commentsNum;

    /**
     * 言值（所有回复这个话题的日志下的评论总数）
     */
    public int joinTotal;

    /**
     * 话题是否已被删除：0-未删除 1-已删除
     */
    public int deleteFlag;
}
