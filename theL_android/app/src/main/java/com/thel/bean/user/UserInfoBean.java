package com.thel.bean.user;

import com.thel.base.BaseDataBean;
import com.thel.bean.GuardBean;
import com.thel.modules.main.me.bean.MyCirclePartnerBean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 其他用户的详细信息
 *
 * @author zhangwenjia
 */
public class UserInfoBean extends BaseDataBean implements Serializable {

    /**
     * 用户图片列表(最多9张)
     */
    public ArrayList<UserInfoPicBean> picList;

    /**
     * 秘钥
     */
    public String isPrivateKey;

    /**
     * 是否关注 (0否,1 是)
     */
    public String isFollow;

    /**
     * 用户关系 0:我不关注她，她也不关注我 1:我关注她，但是她不关注我 2:我不关注她，但是她却关注我 3:我们互相关注
     */
    public int followStatus;

    /**
     * 上次登录时间
     */
    public String loginTime2; // "4"、"5"
    // days 和 hours,和loginTime2组合使用，标识几天前登陆或几小时前登陆
    public String unit;

    /**
     *
     */
    public String sinaUid;

    /**
     *
     */
    public String sinaToken;

    /**
     *
     */
    public String sinaTokenSecret;

    /**
     * 性别
     */
    public String sex;

    /**
     * 体重
     */
    public String weight;

    /**
     * 兴趣
     */
    // public String interests;

    /**
     * 活动范围
     */
    public String location;

    /**
     * 是否传情(0否, 1 是)
     */
    public String isWink;

    /**
     * 是否是黑名单
     */
    public String isBlack = "0";

    /**
     * 是否在线 (0不再, 1在)
     */
    public int online;

    /**
     * 距离
     */
    public String distance;

    /**
     * 身高
     */
    public String height;

    /**
     * 传情数
     */
    public String winkNum = "0";

    /**
     * 居住地
     */
    public String livecity;

    /**
     * 年龄
     */
    public String age;

    /**
     * 星座
     */
    public String horoscope;

    /**
     * 音乐
     */
    public String music;

    /**
     * 读书
     */
    public String books;

    /**
     * 恋爱状态
     */
    public String affection;

    /**
     * 职业
     */
    public String occupation;

    /**
     * 其他爱好
     */
    public String interests;

    /**
     * 昵称
     */
    public String nickName;

    /**
     * 粉丝数
     */
    public int followerNum;

    /**
     * 关注数
     */
    public int followedNum;

    /**
     * 喜爱的电影
     */
    public String movie;

    /**
     * 头像
     */
    public String avatar;

    /**
     * 旅行地
     */
    public String travelcity;

    /**
     * 暗恋
     */
    public String isHiddenLove;

    /**
     * 自述,签名
     */
    public String intro;

    /**
     *
     */
    public String locked;

    /**
     * 种族
     */
    public String ethnicity;

    /**
     * 自述
     */
    public String discriptions;

    /**
     * 角色
     */
    public String roleName;

    /**
     * 交友目的
     */
    public String purpose;

    /**
     * 喜爱的食物
     */
    public String food;

    /**
     * openfire 用户名
     */
    public String messageUser;

    /**
     * 预留字段
     */
    public String token;

    /**
     * 语音的url,每一个用户的url是固定的
     */
    public String voiceMessage;

    /**
     * 语音的序列号,用户每更新一次语音,这个数值就会变化,用来做客户端判断缓存用的
     */
    public String recorderDate;

    /**
     * 语音的播放时长,小于60的正整数
     */
    public String recorderTimes;

    /**
     * 背景图 *
     */
    public String bgImage;

    public String userName;
    public String wantRole;
    public String professionalTypes;
    public String outLevel;
    /**
     * 生日 yyyy-MM-dd
     */
    public String birthday;

    // 账号绑定信息
    public String cell;
    public String wx;
    public String fb;

    /**
     * 伴侣
     */
    public MyCirclePartnerBean paternerd = null;
    /**
     * 密友
     */
    public List<MyCircleFriendBean> bffs = new ArrayList<>();

    public GuardBean guard;

    /**
     * 认证状态，是否是加V用户 0:否 1:个人V 2:企业V
     */
    public int verifyType;
    /**
     * 认证说明
     */
    public String verifyIntro;
    /**
     * vip等级
     */
    public int level;
    /**
     * 是否隐身
     */
    public int hiding;

    /**
     * 直播信息
     */
    /*public int live; *//*是否在直播，0否 1是*//*
    public int rank; *//*观众人数排名*//*
    public String liveDesc;
    public String liveAudience;
    public String imageUrl;*/
    public UserInfoLiveBean liveInfo;
    /**
     * 悄悄关注参数，1表示悄悄关注，0表示没有
     */
    public int secretly = 0;

    /* public int isWinked_free = 0;//是否免费挤过眼，0表示没今天没挤过眼
     public int isWinked_pay = 0;//是否付费挤过眼，0表示没今天没挤过眼*/
    public UserInfoWinkBean winked;

    /**
     * 自己的id
     */
    public String id;

    public String loveSuccess;

    public int videosTotalNum;

    /**
     * 是否举报过
     */
    public boolean isReport;

    public int ratio = 0;

    /**
     * // 如果是互相暗恋，则置为3
     */
    public void setHiddenLove() {
        if (loveSuccess.equals("1")) {
            isHiddenLove = "3";
        }
    }

    public AwaitBean await;

    public static class AwaitBean implements Serializable {
        /**
         * 期待直播人数
         */
        public long awaitCount;
        /**
         * true 已期待 false未期待
         */
        public boolean awaitStatus;
        /**
         * 最近直播时间
         */
        public String lastLiveTime;
    }

    @Override
    public String toString() {
        return "UserInfoBean{" +
                "picList=" + picList +
                ", isPrivateKey='" + isPrivateKey + '\'' +
                ", isFollow='" + isFollow + '\'' +
                ", followStatus=" + followStatus +
                ", loginTime2='" + loginTime2 + '\'' +
                ", unit='" + unit + '\'' +
                ", sinaUid='" + sinaUid + '\'' +
                ", sinaToken='" + sinaToken + '\'' +
                ", sinaTokenSecret='" + sinaTokenSecret + '\'' +
                ", sex='" + sex + '\'' +
                ", weight='" + weight + '\'' +
                ", location='" + location + '\'' +
                ", isWink='" + isWink + '\'' +
                ", isBlack='" + isBlack + '\'' +
                ", online=" + online +
                ", distance='" + distance + '\'' +
                ", height='" + height + '\'' +
                ", winkNum='" + winkNum + '\'' +
                ", livecity='" + livecity + '\'' +
                ", age='" + age + '\'' +
                ", horoscope='" + horoscope + '\'' +
                ", music='" + music + '\'' +
                ", books='" + books + '\'' +
                ", affection='" + affection + '\'' +
                ", occupation='" + occupation + '\'' +
                ", interests='" + interests + '\'' +
                ", nickName='" + nickName + '\'' +
                ", followerNum=" + followerNum +
                ", movie='" + movie + '\'' +
                ", avatar='" + avatar + '\'' +
                ", travelcity='" + travelcity + '\'' +
                ", isHiddenLove='" + isHiddenLove + '\'' +
                ", intro='" + intro + '\'' +
                ", locked='" + locked + '\'' +
                ", ethnicity='" + ethnicity + '\'' +
                ", discriptions='" + discriptions + '\'' +
                ", roleName='" + roleName + '\'' +
                ", purpose='" + purpose + '\'' +
                ", food='" + food + '\'' +
                ", messageUser='" + messageUser + '\'' +
                ", token='" + token + '\'' +
                ", voiceMessage='" + voiceMessage + '\'' +
                ", recorderDate='" + recorderDate + '\'' +
                ", recorderTimes='" + recorderTimes + '\'' +
                ", bgImage='" + bgImage + '\'' +
                ", userName='" + userName + '\'' +
                ", wantRole='" + wantRole + '\'' +
                ", professionalTypes='" + professionalTypes + '\'' +
                ", outLevel='" + outLevel + '\'' +
                ", birthday='" + birthday + '\'' +
                ", cell='" + cell + '\'' +
                ", wx='" + wx + '\'' +
                ", fb='" + fb + '\'' +
                ", paternerd=" + paternerd +
                ", bffs=" + bffs +
                ", guard=" + guard +
                ", verifyType=" + verifyType +
                ", verifyIntro='" + verifyIntro + '\'' +
                ", level=" + level +
                ", hiding=" + hiding +
                ", liveInfo=" + liveInfo +
                ", secretly=" + secretly +
                ", winked=" + winked +
                ", id='" + id + '\'' +
                ", loveSuccess='" + loveSuccess + '\'' +
                ", videosTotalNum=" + videosTotalNum +
                ", await=" + await +
                '}';
    }
}
