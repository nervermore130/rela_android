package com.thel.bean.me;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.base.BaseDataBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.imp.vip.VipStatusChangerListener;
import com.thel.imp.vip.VipUtils;
import com.thel.modules.main.home.moments.ReleaseMomentActivity;
import com.thel.modules.main.me.bean.LogInfoBean;
import com.thel.modules.main.me.match.eventcollect.collect.LiveLogUtils;
import com.thel.modules.main.me.match.eventcollect.collect.MatchLogUtils;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.modules.others.VipConfigActivity;
import com.thel.modules.select_contacts.SelectContactsActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.ui.widget.MySwipeRefreshLayout;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;
import com.thel.utils.AppInit;
import com.thel.utils.DialogUtil;
import com.thel.utils.ImageUtils;
import com.thel.utils.L;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

import org.reactivestreams.Subscription;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * 我的密友页面
 * Created by lingwei on 2017/10/16.
 */

public class MyCircleActivity extends BaseActivity implements VipStatusChangerListener {
    private MySwipeRefreshLayout swipe_container;
    private RecyclerView mRecyclerView;
    private StaggeredGridLayoutManager manager;
    /**
     * 闺蜜列表
     */
    private List<MyCircleFriendBean> list = new ArrayList<>();

    private MyCircleAdapter mAdapter;
    private View lin_more;
    private StringBuilder circleFriendIds = new StringBuilder();

    private View header;
    private SimpleDraweeView img_background;
    private View rel_header;
    private SimpleDraweeView img_avatar_right;
    private SimpleDraweeView img_avatar_left;
    private TextView txt_partner_nickname;
    private View lin_content;
    private TextView txt_distance;
    private View img_birthday;
    private TextView txt_birthday;
    private TextView txt_love_days;
    private TextView txt_waiting;
    private ImageView img_more;
    private View view_shade;

    private DialogUtil dialogUtils;

    private View rel_add;
    private TextView txt_bind_num;
    public static final int BIND_NORMAL_NUM = 6;//非会员能绑定的密友数量
    public static final int BIND_LEVEL_NUM = 12;//非会员能绑定的密友数量
    private int operatePosition;
    private View footer;
    public static boolean needRefresh = false;
    public MyCircleFriendListBean myCircleFriendListBean;
    private MyCircleFriendListBean.Paternerd partnerBean;
    public static final String TYPE_ADD_PARTNER = "0";
    public static final String TYPE_ADD_FRIEND = "1";
    private VipUtils vipUtils;
    private String pageId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_circle_activity_layout);
        findViewById();
        dialogUtils = new DialogUtil();
        processBusiness();
        setListener();
        registerReceiver();
        pageId = Utils.getPageId();
    }

    private void registerReceiver() {
        vipUtils = new VipUtils();
        vipUtils.registerReceiver(this);
    }

    private void findViewById() {
        ((TextView) findViewById(R.id.txt_title)).setText(getString(R.string.me_activity_circle));
        lin_more = findViewById(R.id.lin_more);
        swipe_container = findViewById(R.id.swipe_container);
        ViewUtils.initSwipeRefreshLayout(swipe_container);
        mRecyclerView = findViewById(R.id.recyclerview);
        manager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(manager);
        mAdapter = new MyCircleAdapter(list);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addItemDecoration(new ItemDivider());

        //header
        header = LayoutInflater.from(this).inflate(R.layout.my_circle_header_view, null);
        rel_header = header.findViewById(R.id.rel_header);
        img_background = header.findViewById(R.id.img_background);
        img_avatar_right = header.findViewById(R.id.img_avatar_right);
        img_avatar_left = header.findViewById(R.id.img_avatar_left);
        txt_partner_nickname = header.findViewById(R.id.txt_partner_nickname);
        lin_content = header.findViewById(R.id.lin_content);
        txt_distance = header.findViewById(R.id.txt_distance);
        img_birthday = header.findViewById(R.id.img_birthday);
        txt_birthday = header.findViewById(R.id.txt_birthday);
        txt_love_days = header.findViewById(R.id.txt_love_days);
        txt_waiting = header.findViewById(R.id.txt_waiting);
        img_more = header.findViewById(R.id.img_more);
        rel_add = header.findViewById(R.id.rel_add);
        txt_bind_num = header.findViewById(R.id.txt_bind_num);
        view_shade = header.findViewById(R.id.view_shade);

        footer = LayoutInflater.from(this).inflate(R.layout.my_circle_footer_view, null);
        ((TextView) footer.findViewById(R.id.txt_buy_vip)).setText(getString(R.string.buy_vip) + ">");

        mAdapter.addHeaderView(header);
        mAdapter.addFooterView(footer);
        setTipView();

    }

    private void processBusiness() {
        swipe_container.post(new Runnable() {
            @Override
            public void run() {
                swipe_container.setRefreshing(true);

            }
        });
        Flowable<MyCircleFriendListBean> flowable = RequestBusiness.getInstance().getMyCircleData();//获取页面数量
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<MyCircleFriendListBean>() {

            @Override
            public void onNext(MyCircleFriendListBean myCircleFriendListBean) {
                super.onNext(myCircleFriendListBean);
                refreshUi(myCircleFriendListBean);
                requestFinished();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(this.getClass().getName());
        MobclickAgent.onResume(this);
        if (needRefresh) {
            needRefresh = false;
            processBusiness();
        }
    }


    private void setListener() {
        findViewById(R.id.lin_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();

            }
        });
        lin_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (UserUtils.isVerifyCell()) {
                    gotoWritLoveMoment();
                }
            }
        });
        rel_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (partnerBean != null) {

                    gotoUserInfoActivty(partnerBean.paternerId + "");
                }
            }
        });
        img_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (partnerBean != null) {
                    operate(partnerBean.paternerId, partnerBean.requestStatus, partnerBean.requestId, true, 0);
                }
            }
        });
        rel_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                jumpToAdd(TYPE_ADD_PARTNER);
            }
        });
        mAdapter.setOnRecyclerViewItemChildClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseRecyclerViewAdapter adapter, View view, int position) {
                try {
                    operatePosition = (int) view.getTag() - adapter.getHeaderLayoutCount();
                } catch (Exception e) {
                    return;
                }
                final MyCircleFriendBean friendBean = mAdapter.getItem(operatePosition);
                switch (view.getId()) {
                    case R.id.img_avatar:
                        gotoUserInfoActivty(friendBean.userId + "");
                        break;
                    case R.id.img_more:
                        operate(friendBean.userId, friendBean.requestStatus, friendBean.requestId, false, operatePosition + adapter.getHeaderLayoutCount());
                        break;
                    case R.id.img_add_friend:
                        jumpToAdd(TYPE_ADD_FRIEND);
                        break;
                }
            }
        });
        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                processBusiness();
            }
        });
        footer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoBuyVip();

            }
        });
    }

    /**
     * 购买会员
     */
    private void gotoBuyVip() {
        traceMyCircleLog("addfriends");
        Intent intent = new Intent(this, VipConfigActivity.class);
        intent.putExtra("fromPage", "relation");
        intent.putExtra("fromPageId", pageId);

        startActivityForResult(intent, TheLConstants.REQUEST_CODE_BUY_VIP);
    }

    /**
     * 选择联系人
     */
    private void jumpToAdd(String requestType) {
        Intent intent = new Intent(this, SelectContactsActivity.class);
        intent.putExtra("requestType", requestType);
        intent.putExtra("circleFriendIds", circleFriendIds.toString());
        startActivity(intent);
    }

    private void gotoUserInfoActivty(String userId) {
        if (userId != null) {
//            Intent intent = new Intent();
//            intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, userId);
//            intent.setClass(this, UserInfoActivity.class);
//            startActivity(intent);
            FlutterRouterConfig.Companion.gotoUserInfo(userId);
        }
    }

    /**
     * 写情侣日志
     */
    private void gotoWritLoveMoment() {
        Intent intent1 = new Intent(this, ReleaseMomentActivity.class);
        intent1.putExtra(TheLConstants.BUNDLE_KEY_RELEASE_TYPE, ReleaseMomentActivity.RELEASE_TYPE_LOVE);
        intent1.putExtra("partner", partnerBean);
        startActivity(intent1);
    }

    /**
     * 操作情侣
     */
    private void operate(final long userId, final int requestStatus, final long requestId, final boolean isPartner, final int position) {
        if (1 == requestStatus) {//本来是同意，现在是要解除关系
            dialogUtils.showSelectionDialog(MyCircleActivity.this, new String[]{getString(R.string.my_circle_activity_remove)}, new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapter, View view, final int p, long arg3) {
                    ViewUtils.preventViewMultipleClick(view, 2000);
                    dialogUtils.closeDialog();
                    switch (p) {
                        case 0:
                            String str;
                            if (isPartner) {
                                str = getString(R.string.my_circle_activity_remove_partner_confirm);
                            } else {
                                str = getString(R.string.my_circle_activity_remove_bff_confirm);
                            }
                            DialogUtil.showConfirmDialog(MyCircleActivity.this, "", str, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                    showLoading();
                                    Flowable<BaseDataBean> flowable = RequestBusiness.getInstance().removeCircleFriend(requestId + "", userId + "");
                                    //取消请求或者解除关系
                                    flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {

                                        @Override
                                        public void onNext(BaseDataBean baseDataBean) {
                                            super.onNext(baseDataBean);
                                            relieveRelationship(isPartner, position);
                                        }

                                        @Override
                                        public void onError(Throwable t) {
                                            super.onError(t);
                                            requestFinished();

                                        }

                                        @Override
                                        public void onComplete() {
                                            requestFinished();

                                        }
                                    });
                                }
                            });
                            break;
                        default:
                            break;
                    }
                }
            }, false, 2, null);
        } else {// 取消请求
            dialogUtils.showSelectionDialog(MyCircleActivity.this, new String[]{getString(R.string.my_circle_activity_cancel)}, new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapter, View view, final int p, long arg3) {
                    ViewUtils.preventViewMultipleClick(view, 2000);
                    dialogUtils.closeDialog();
                    switch (p) {
                        case 0:
                            if (requestStatus == 2) {
                                showLoading();
                                Flowable<BaseDataBean> flowable = RequestBusiness.getInstance().cancelRequest(requestId + "");
                                flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {

                                    @Override
                                    public void onNext(BaseDataBean baseDataBean) {
                                        super.onNext(baseDataBean);
                                        relieveRelationship(isPartner, position);
                                    }

                                    @Override
                                    public void onError(Throwable t) {
                                        super.onError(t);
                                        requestFinished();

                                    }

                                    @Override
                                    public void onComplete() {
                                        requestFinished();

                                    }
                                });
                            } else {
                                DialogUtil.showConfirmDialog(MyCircleActivity.this, "", getString(R.string.my_circle_activity_cancel_confirm), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        showLoading();
                                        Flowable<BaseDataBean> flowable = RequestBusiness.getInstance().cancelRequest(requestId + "");
                                        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {


                                            @Override
                                            public void onNext(BaseDataBean baseDataBean) {
                                                super.onNext(baseDataBean);
                                                relieveRelationship(isPartner, position);
                                            }

                                            @Override
                                            public void onError(Throwable t) {
                                                super.onError(t);
                                                requestFinished();

                                            }

                                            @Override
                                            public void onComplete() {
                                                requestFinished();

                                            }
                                        });
                                    }
                                });
                            }
                            break;
                        default:
                            break;
                    }
                }
            }, false, 2, null);
        }
    }

    private void relieveRelationship(boolean isPartner, int position) {
        try {
            String userIdStr;
            if (isPartner) {// 删除partner请求
                myCircleFriendListBean.data.paternerd.isAdd = true;
                userIdStr = myCircleFriendListBean.data.paternerd + "";
                initHeaderView();
            } else {// 删除bff请求
                final int pos = position - mAdapter.getHeaderLayoutCount();
                userIdStr = myCircleFriendListBean.data.bffs.get(pos).userId + "";
                myCircleFriendListBean.data.bffs.remove(pos);
                if (myCircleFriendListBean.data.bffs.size() == BIND_NORMAL_NUM - 1 && !myCircleFriendListBean.data.bffs.get(BIND_NORMAL_NUM - 2).isAdd && UserUtils.getUserVipLevel() <= 0) {// 非会员 添加一行操作行
                    MyCircleFriendBean friendBean = new MyCircleFriendBean();
                    friendBean.isAdd = true;
                    myCircleFriendListBean.data.bffs.add(friendBean);
                } else if (myCircleFriendListBean.data.bffs.size() == BIND_LEVEL_NUM - 1 && !myCircleFriendListBean.data.bffs.get(BIND_LEVEL_NUM - 2).isAdd && UserUtils.getUserVipLevel() > 0) {// 会员 添加一行操作行
                    MyCircleFriendBean friendBean = new MyCircleFriendBean();
                    friendBean.isAdd = true;
                    myCircleFriendListBean.data.bffs.add(friendBean);
                }
            }
            circleFriendIds.delete(circleFriendIds.indexOf(userIdStr), circleFriendIds.indexOf(userIdStr) + userIdStr.length());
            refreshUi(myCircleFriendListBean);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setTipView() {
        if (UserUtils.getUserVipLevel() > 0) {//如果是会员，最多绑定12个
            txt_bind_num.setText(getString(R.string.bind_friend_num, BIND_LEVEL_NUM));
            footer.setVisibility(View.GONE);
        } else {//不是会员，绑定6个
            txt_bind_num.setText(getString(R.string.bind_friend_num, BIND_NORMAL_NUM));
        }
    }

    class ItemDivider extends RecyclerView.ItemDecoration {
        final int padding = (int) getResources().getDimension(R.dimen.live_rooms_list_padding);

        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            try {
                final BaseRecyclerViewAdapter adapter = (BaseRecyclerViewAdapter) parent.getAdapter();
                final int headerCount = adapter.getHeaderLayoutCount();
                final int footerCount = adapter.getFooterLayoutCount();
                final int count = adapter.getItemCount();
                final int pos = parent.getChildLayoutPosition(view);
                if (pos >= headerCount && pos < count - footerCount) {
                    outRect.top = padding / 2;
                    if ((pos - headerCount) % 2 == 0) {
                        outRect.right = padding / 2;
                    }
                } else if (pos >= count - footerCount) {
                    outRect.top = padding / 2;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void refreshUi(MyCircleFriendListBean myCircleFriendListBean) {
        if (myCircleFriendListBean.data != null) {
            this.myCircleFriendListBean = myCircleFriendListBean;

            if (myCircleFriendListBean.data.paternerd != null) {
                partnerBean = myCircleFriendListBean.data.paternerd;

            }
            refreshCircleFriendIds();
            initHeaderView();
            list.clear();
            if (myCircleFriendListBean.data.bffs != null && myCircleFriendListBean.data.bffs.size() > 0) {
                MyCircleFriendBean myCircleFriendBean = null;
                int size = myCircleFriendListBean.data.bffs.size();
                for (int i = 0; i < size; i++) {
                    myCircleFriendBean = myCircleFriendListBean.data.bffs.get(i);

                    list.add(myCircleFriendBean);
                }
                if (size < BIND_NORMAL_NUM) {
                    myCircleFriendBean = new MyCircleFriendBean();
                    myCircleFriendBean.isAdd = true;
                    list.add(myCircleFriendBean);
                } else if (size >= BIND_NORMAL_NUM && size < BIND_LEVEL_NUM && UserUtils.getUserVipLevel() > 0) {                //会员可绑定12个
                    myCircleFriendBean = new MyCircleFriendBean();

                    myCircleFriendBean.isAdd = true;
                    list.add(myCircleFriendBean);
                }
            } else {
                MyCircleFriendBean myCircleFriendBean = new MyCircleFriendBean();
                myCircleFriendBean.isAdd = true;

                list.add(myCircleFriendBean);

            }
            mAdapter.setNewData(list);
        }
    }

    /**
     * 封装用户id，在选择联系人页面排除掉这些用户 start
     */
    private void refreshCircleFriendIds() {
        circleFriendIds = new StringBuilder();

        if (partnerBean != null && !partnerBean.isAdd) {
            circleFriendIds.append(partnerBean.paternerId).append(",");
        }
        if (myCircleFriendListBean != null && myCircleFriendListBean.data != null && myCircleFriendListBean.data.bffs != null) {
            for (MyCircleFriendBean myCircleFriendBean : myCircleFriendListBean.data.bffs) {// bff请求的用户id
                if (!myCircleFriendBean.isAdd) {
                    circleFriendIds.append(myCircleFriendBean.userId).append(",");
                }
            }
        }
    }

    private void requestFinished() {
        if (swipe_container != null)
            swipe_container.postDelayed(new Runnable() {

                @Override
                public void run() {
                    if (swipe_container != null && swipe_container.isRefreshing())
                        swipe_container.setRefreshing(false);
                }
            }, 1000);
        closeLoading();
    }

    /**
     * headerView
     */
    private void initHeaderView() {
        rel_header.setVisibility(View.GONE);
        rel_add.setVisibility(View.GONE);
        lin_more.setVisibility(View.GONE);
        view_shade.setVisibility(View.GONE);

        if (partnerBean != null && !partnerBean.isAdd) { //已添加或者绑定
            rel_header.setVisibility(View.VISIBLE);
            lin_content.setVisibility(View.GONE);
            txt_waiting.setVisibility(View.GONE);
            img_more.setImageResource(R.mipmap.btn_moments_more);
            img_avatar_left.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(partnerBean.paternerAvatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE)));
            img_avatar_right.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(partnerBean.userAvatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE)));
            txt_partner_nickname.setText(partnerBean.paternerNickName + "");
            if (1 == partnerBean.requestStatus) {//请求状态  同意
                lin_content.setVisibility(View.VISIBLE);
                lin_more.setVisibility(View.VISIBLE);
                view_shade.setVisibility(View.VISIBLE);
                img_more.setImageResource(R.mipmap.btn_moments_more_white);
                img_background.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(partnerBean.bgImage, AppInit.displayMetrics.widthPixels, TheLApp.getContext().getResources().getDimension(R.dimen.bg_height))));
                if (!TextUtils.isEmpty(partnerBean.distance)) {
                    txt_distance.setVisibility(View.VISIBLE);
                    txt_distance.setText(partnerBean.distance);//距离
                } else {
                    txt_distance.setVisibility(View.GONE);
                }
                final StringBuilder birth = new StringBuilder(TheLApp.getContext().getString(R.string.my_circle_activity_birthday) + " " + partnerBean.paternerBirthday.substring(partnerBean.paternerBirthday.indexOf("-") + 1).replaceAll("-", "."));
                txt_birthday.setTextColor(ContextCompat.getColor(TheLApp.getContext(), R.color.white));
                setBirthday(txt_birthday, birth, partnerBean.reminday, img_birthday);//生日

                StringBuilder love_days = new StringBuilder(getString(R.string.write_love_moment_act_anniversary, partnerBean.paternerNickName, partnerBean.userNickName));
                try {
                    final int days = Integer.parseInt(partnerBean.days);
                    if (1 == days) {
                        love_days.append(getString(R.string.my_circle_activity_spacing_day_old, days + ""));
                    } else {
                        love_days.append(getString(R.string.my_circle_activity_spacing_day_even, days + ""));
                    }
                } catch (Exception e) {
                    love_days.append(getString(R.string.my_circle_activity_spacing_day_even, partnerBean.days));
                }
                txt_love_days.setText(getBoldLoveDays(love_days.toString(), partnerBean.days));//恋爱了多少天
            } else if (0 == partnerBean.requestStatus) {
                txt_waiting.setVisibility(View.VISIBLE);
                img_background.setImageResource(R.color.white);
                txt_waiting.setText(getString(R.string.my_circle_activity_unprocessed));
            } else if (2 == partnerBean.requestStatus) {
                txt_waiting.setVisibility(View.VISIBLE);
                img_background.setImageResource(R.color.white);
                txt_waiting.setText(getString(R.string.my_circle_activity_past));
            }
        } else {//还没有绑定伴侣
            rel_add.setVisibility(View.VISIBLE);
        }
    }

    private SpannableString getBoldLoveDays(String content, String boldContent) {
        SpannableString sp = new SpannableString(content);
        if (content.contains(boldContent)) {
            final int start = content.lastIndexOf(boldContent);
            final int end = start + boldContent.length();
            sp.setSpan(new StyleSpan(Typeface.BOLD), start, end, SpannableString.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        return sp;
    }

    private class MyCircleAdapter extends BaseRecyclerViewAdapter<MyCircleFriendBean> {
        private final OnItemChildClickListener listener = new OnItemChildClickListener();
        private final int item_width = AppInit.displayMetrics.widthPixels / 2;

        public MyCircleAdapter(List<MyCircleFriendBean> list) {
            super(R.layout.my_circle_item, list);
        }

        @Override
        protected void convert(BaseViewHolder helper, MyCircleFriendBean item) {
            ViewGroup.LayoutParams param = helper.getView(R.id.rel_item).getLayoutParams();
            param.height = item_width * 306 / 374;

            helper.setVisibility(R.id.lin_friend, View.GONE);
            helper.setVisibility(R.id.img_more, View.GONE);
            helper.setVisibility(R.id.img_add_friend, View.GONE);
            if (!item.isAdd) {
                helper.setVisibility(R.id.lin_friend, View.VISIBLE);
                helper.setVisibility(R.id.img_more, View.VISIBLE);
                helper.setImageUrl(R.id.img_avatar, item.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE);
                helper.setText(R.id.txt_nickname, item.nickName);
                helper.setVisibility(R.id.txt_distance, View.GONE);
                helper.setVisibility(R.id.lin_birthday, View.GONE);
                helper.setVisibility(R.id.txt_waiting, View.GONE);
                helper.setVisibility(R.id.img_birthday, View.GONE);
                if (1 == item.requestStatus) { //同意
                    helper.setVisibility(R.id.lin_birthday, View.VISIBLE);
                    if (!TextUtils.isEmpty(item.online) && !TextUtils.isEmpty(item.distance)) {
                        helper.setVisibility(R.id.txt_distance, View.VISIBLE);
                        helper.setText(R.id.txt_distance, item.distance);
                    }
                    final StringBuilder birth = new StringBuilder(TheLApp.getContext().getString(R.string.my_circle_activity_birthday) + " " + item.birthday.substring(item.birthday.indexOf("-") + 1).replaceAll("-", "."));
                    ((TextView) helper.getView(R.id.txt_birthday)).setTextColor(ContextCompat.getColor(TheLApp.getContext(), R.color.text_color_light_gray));
                    setBirthday(helper.getView(R.id.txt_birthday), birth, item.reminday, helper.getView(R.id.img_birthday));
                } else if (0 == item.requestStatus) {//等待回应
                    helper.setVisibility(R.id.txt_waiting, View.VISIBLE);
                    helper.setText(R.id.txt_waiting, TheLApp.getContext().getString(R.string.my_circle_activity_unprocessed));
                } else if (2 == item.requestStatus) {//过期
                    helper.setVisibility(R.id.txt_waiting, View.VISIBLE);
                    helper.setText(R.id.txt_waiting, TheLApp.getContext().getString(R.string.my_circle_activity_past));
                }

                helper.setTag(R.id.img_avatar, helper.getAdapterPosition());
                helper.setOnItemChildClickListener(R.id.img_avatar, listener);
                helper.setTag(R.id.img_more, helper.getAdapterPosition());
                helper.setOnItemChildClickListener(R.id.img_more, listener);
            } else {
                helper.setVisibility(R.id.img_add_friend, View.VISIBLE);
                helper.setTag(R.id.img_add_friend, helper.getAdapterPosition());
                helper.setOnItemChildClickListener(R.id.img_add_friend, listener);
            }
        }


    }

    /**
     * 设置生日显示
     */

    private void setBirthday(TextView txt_birthday, StringBuilder birth, String reminday, View img_birthday) {
        img_birthday.setVisibility(View.GONE);
        if (!TextUtils.isEmpty(reminday)) {//显示生日提醒
            final int remainDays = Integer.parseInt(reminday);
            if (remainDays >= 0 && remainDays <= 7) {
                img_birthday.setVisibility(View.VISIBLE);
                if (0 == remainDays) {
                    birth.append("(" + TheLApp.getContext().getString(R.string.chat_activity_today) + ")");
                } else if (1 == remainDays) {
                    birth.append("(" + remainDays + TheLApp.getContext().getString(R.string.my_circle_activity_day_odd) + ")");
                } else {
                    birth.append("(" + remainDays + TheLApp.getContext().getString(R.string.my_circle_activity_day_even) + ")");
                }
                txt_birthday.setTextColor(ContextCompat.getColor(TheLApp.getContext(), R.color.darkOrange));
            }
        }
        txt_birthday.setText(birth.toString());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        vipUtils.unRegisterReceiver(this);
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    @Override
    public void onVipStatusChanged(boolean isVip, int vipLevel) {
        if (isVip) {
            if (txt_bind_num != null && footer != null) {
                setTipView();
            }
            if (list != null && mAdapter != null) {
                final int size = list.size();
                if (size >= BIND_NORMAL_NUM && size < BIND_LEVEL_NUM && UserUtils.getUserVipLevel() > 0) {                //会员可绑定12个
                    MyCircleFriendBean myCircleFriendBean = new MyCircleFriendBean();

                    myCircleFriendBean.isAdd = true;
                    list.add(myCircleFriendBean);
                    mAdapter.notifyDataSetChanged();
                }
            }
        }
    }
    public void traceMyCircleLog(String activity) {
        try {

            LogInfoBean logInfoBean = new LogInfoBean();
            logInfoBean.page = "relation";
            logInfoBean.page_id = pageId;
            logInfoBean.activity = activity;

            MatchLogUtils.getInstance().addLog(logInfoBean);

        } catch (Exception e) {

        }
    }
}
