package com.thel.bean.user;

import com.thel.base.BaseDataBean;

import java.util.List;

/**
 * Created by liuyun on 2018/1/5.
 */

public class FollowUsersBean extends BaseDataBean {

    public ResponseFollowStatusListBean data;

    public class ResponseFollowStatusListBean {
        List<ResponseFollowStatusBean> list;
    }

    public class ResponseFollowStatusBean {
        public long userId;
        public String status;
    }
}
