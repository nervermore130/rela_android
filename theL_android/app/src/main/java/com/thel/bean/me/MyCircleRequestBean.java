package com.thel.bean.me;

import android.content.ContentValues;

import com.thel.modules.main.messages.db.DataBaseAdapter;

/**
 * 密友圈请求对象
 *
 * @author Setsail
 */
public class MyCircleRequestBean {

    public long requestId;
    /**
     * 请求状态，-3=解除绑定, －2=取消，－1=拒绝，0=未处理，1=同意，2=过期*
     */
    public int requestStatus;
    /**
     * 请求类型，0=paternerd,1=bff *
     */
    public int requestType;
    public long userId;
    public String nickName;
    public String avatar;
    public long receivedId;
    public long createTime;
    public long handleTime;
    public String anniversaryDate;
    /**
     * 请求可以捎带一句话，就是这个参数
     * 后台接口拼写错了，我们跟他保持一致...
     */
    public String requestComent;

    /**
     * 用于日期显示
     */
    public String date;

    public ContentValues getContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DataBaseAdapter.F_REQUEST_ID, requestId);
        contentValues.put(DataBaseAdapter.F_REQUEST_STATUS, requestStatus);
        contentValues.put(DataBaseAdapter.F_REQUEST_TYPE, requestType);
        contentValues.put(DataBaseAdapter.F_USER_ID, userId);
        contentValues.put(DataBaseAdapter.F_NICK_NAME, nickName);
        contentValues.put(DataBaseAdapter.F_AVATAR, avatar);
        contentValues.put(DataBaseAdapter.F_RECEIVED_ID, receivedId);
        contentValues.put(DataBaseAdapter.F_CREATE_TIME, createTime);
        contentValues.put(DataBaseAdapter.F_HANDLE_TIME, handleTime);
        contentValues.put(DataBaseAdapter.F_ANNIVERSARY_DATE, anniversaryDate);
        contentValues.put(DataBaseAdapter.F_REQUEST_COMENT, requestComent);
        return contentValues;
    }
}
