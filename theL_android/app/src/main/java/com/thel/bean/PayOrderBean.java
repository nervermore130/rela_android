package com.thel.bean;

import android.util.Log;

import com.thel.base.BaseDataBean;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * @author Setsail
 */
public class PayOrderBean extends BaseDataBean implements Serializable {

    public Data data;

    public class Data {
        /**
         * 订单id
         */
        public long orderId;
        /**
         * 真正提交给支付宝的订单id
         */
        public String outTradeNo;
        /**
         * 交易金额
         */
        public double totalFee;
        /**
         * 商品名称
         */
        public String subject;
        /**
         * 商品介绍
         */
        public String description;
        /**
         * 服务端地址
         */
        public String alipayNotifyUrl;
        /**
         * 超时时间
         */
        public String expire;
        /**
         * 微信订单
         */
        public WxOrder wxOrder;

        /**
         * google play的商品id
         */
        public String iapId;

        public boolean pingxxNotifyUrl;

    }

    public class WxOrder {
        public String return_code;

        public String return_msg;

        public String appid;

        public String mch_id;

        public String nonce_str;

        public String sign;

        public String result_code;

        public String prepay_id;

        public String trade_type;
    }


}
