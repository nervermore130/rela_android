package com.thel.bean.user;

import java.io.Serializable;

public class MyImageBean implements Serializable {

    public int picId;

    public int isPrivate;

    public int coverFlag = 0;

    public int picWidth;

    public int picHeight;

    public String picUrl;

    public String longThumbnailUrl;

    // 更新个人详情页面，个人照片部分第一张是拍照按钮
    public boolean isCameraBtnFlag = false;

    @Override
    public String toString() {
        return "MyImageBean{" + "picId=" + picId + ", isPrivate=" + isPrivate + ", coverFlag='" + coverFlag + '\'' + ", picWidth=" + picWidth + ", picHeight=" + picHeight + ", picUrl='" + picUrl + '\'' + ", longThumbnailUrl='" + longThumbnailUrl + '\'' + ", isCameraBtnFlag=" + isCameraBtnFlag + ", isBlack=" + isBlack + '}';
    }

    public int isBlack;

}

/*
"imagesList": [
    {
        "picWidth": 204,
        "picHeight": 153,
        "isPrivate": 0,
        "picId": 70,
        "coverFlag":"1"，
        "picUrl": "http://s-50395.gotocdn.com:8081/user-image/135705505679525.jpg",
        "longThumbnailUrl": "http://s-50395.gotocdn.com:8081/user-image/135705505679525_small.jpg"
    },
*/