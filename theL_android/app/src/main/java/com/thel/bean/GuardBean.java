package com.thel.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by chad
 * Time 17/12/12
 * Email: wuxianchuang@foxmail.com
 * Description: TODO 周榜守护
 */

public class GuardBean implements Serializable {

    public List<TopFans> topFans = new ArrayList<>();

    public String topFansLink;

    public String topFansType;

    public int livePerm;

    public class TopFans implements Serializable {
        public int userId;

        public String nickname;

        public String avatar;

        public int isCloaking;
    }
}
