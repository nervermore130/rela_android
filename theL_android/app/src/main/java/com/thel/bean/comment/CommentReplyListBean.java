package com.thel.bean.comment;

import java.util.ArrayList;
import java.util.List;

/**
 * 评论回复列表
 *
 * @author Setsail
 */
public class CommentReplyListBean {

    public boolean haveNextPage;

    /**
     * 所属日志id
     */
    public String momentId;

    /**
     * 所属日志的发布者id
     */
    public long userId;

    /**
     * 父评论的实体
     */
    public CommentBean comment;

    /**
     * 子评论列表
     */
    public List<CommentBean> commentList = new ArrayList<CommentBean>();

    /**
     * 数据库游标
     */
    public int cursor;

}
