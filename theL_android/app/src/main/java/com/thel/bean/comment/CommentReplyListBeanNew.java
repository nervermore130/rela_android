package com.thel.bean.comment;

import com.thel.base.BaseDataBean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 评论回复列表
 *
 * @author Setsail
 */
public class CommentReplyListBeanNew extends BaseDataBean implements Serializable {

    public CommentReplyListBean data;

}
