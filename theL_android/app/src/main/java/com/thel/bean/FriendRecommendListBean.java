package com.thel.bean;

import com.thel.base.BaseDataBean;
import com.thel.bean.moments.MomentsBean;

import java.util.List;

/**
 * 附近用户列表信息
 *
 * @author Setsail
 */
public class FriendRecommendListBean extends BaseDataBean {
    public int cursor;
    public int count;
    /**
     * 是否有下一页
     */
    public boolean haveNextPage;

    public List<MomentsBean> list ;


}
