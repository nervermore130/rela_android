package com.thel.bean.user;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by test1 on 2017/2/18.
 */
public class UserCardBean implements Serializable {
    /**
     * 用户id
     */
    public String userId;
    /**
     * 用户名
     */
    public String nickName;
    /**
     * 恋爱状态
     */
    public String affection;
    /**
     * 头像
     */
    public String avatar;
    /**
     * 生日
     */
    public String birthday;
    /**
     * 推荐描述
     */
    public String recommendDesc;
    /**
     * 背景图
     */
    public String bgImage;
    /**
     * 星座
     */
    public String horoscope;

    public String toJson() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("userId", userId);
            obj.put("nickName", nickName);
            obj.put("affection", affection);
            obj.put("avatar", avatar);
            obj.put("birthday", birthday);
            obj.put("recommendDesc", recommendDesc);
            obj.put("bgImage", bgImage);
            obj.put("horoscope", horoscope);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return obj.toString();
    }
}
