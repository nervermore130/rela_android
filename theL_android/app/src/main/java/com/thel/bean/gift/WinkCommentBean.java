package com.thel.bean.gift;


import java.io.Serializable;

/**
 * Moments bean
 *
 * @author Setsail
 */
public class WinkCommentBean implements Serializable {

    /**
     * moments的唯一标识
     */
    public String momentsId;

    /**
     * 用户id
     */
    public String userId;

    /**
     * 用户名
     */
    public String nickname;

    /**
     * 方头像地址
     */
    public String avatar;

    // 点赞的时间
    public String commentTime;

    /**
     * 点赞的类型，1-9中的某个整数
     * 2.22.0之后这个属性已经没什么用了
     */
    public int winkCommentType;

    public long timestamp;

    public String filterType;

    public boolean equals(Object o){
        if(o instanceof WinkCommentBean){
            return ((WinkCommentBean) o).userId .equals(userId+"");
        }
        if(o instanceof String){
            return o.equals(userId+"");
        }
       return super.equals(0);
    }

}
