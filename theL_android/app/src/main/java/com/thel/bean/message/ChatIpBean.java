package com.thel.bean.message;

import com.thel.base.BaseDataBean;

public class ChatIpBean extends BaseDataBean {

    public ChatIpDataBean data;

    public class ChatIpDataBean {
        public String host;
        public int port;
    }
}
