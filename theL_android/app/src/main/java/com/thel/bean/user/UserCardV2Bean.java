package com.thel.bean.user;

import android.text.TextUtils;

import com.thel.R;
import com.thel.app.TheLApp;

import java.util.ArrayList;
import java.util.Arrays;

public class UserCardV2Bean {

    public UserCardV2DataBean data;

    public class UserCardV2DataBean {
        public String userId;
        public String nickName;
        public String avatar;
        public String userName;
        public int verifyType;
        public String verifyIntro;
        public int level;
        public int userLevel;
        public int winkNum;
        public int fansNum;
        public boolean isFollow;
        public int age;
        public int height;
        public int weight;
        public int affection;
        public String confine;
        public String distance;

        public String getLoginTimeShow() {
            StringBuilder str = new StringBuilder();
            str.append(age);
            str.append(TheLApp.getContext().getString(R.string.updatauserinfo_activity_age_unit));
            ArrayList<String> relationship_list = new ArrayList<>(Arrays.asList(TheLApp.getContext().getResources().getStringArray(R.array.userinfo_relationship))); // 感情状态
            str.append(", ");
            try {
                str.append(relationship_list.get(affection));
            } catch (Exception e) {
                str.append(relationship_list.get(0));
            }

            return str.toString();
        }


    }

}
