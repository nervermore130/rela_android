package com.thel.bean;

import java.io.Serializable;

/**
 * Created by chad
 * Time 18/6/20
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */
public class CameraConfigBean implements Serializable {

    public boolean isBeautiful = true;//是否开启美颜

    public int cameraId = 1;//前置摄像头1 后置摄像头0

    public int eyeScale = 0;

    public int faceScale = 0;
}
