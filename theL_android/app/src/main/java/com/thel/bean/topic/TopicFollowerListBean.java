package com.thel.bean.topic;

import com.thel.base.BaseDataBean;

import java.util.ArrayList;

public class TopicFollowerListBean extends BaseDataBean {

	public int userTotalNum;
	public int userThisPageNum;
	public ArrayList<TopicFollowerBean> userList;


}

/*
 * {"result":"1","errcode":"","errdesc":"","userTotalNum":0,"userThisPageNum":1,
 * "userList":[{"userId":100277003,"nickname":"小怪兽","avatar":
 * "http://image.thel-service.com/head-image/1420985279248100277003.jpg"
 * ,"online"
 * :2,"loginTime":"3 天前 ","loginTime2":"3","unit":"days","distance":"19.79 Km"
 * ,"intro":""}]}
 */