package com.thel.bean;

import com.thel.base.BaseDataBean;

/**
 * Created by chad
 * Time 17/11/18
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */

public class UploadVideoAlbumBean extends BaseDataBean {

    public Data data;

    public class Data {
        public String longThumbnailUrl;

        public String picId;

        public String isPrivate;

        public String picUrl;

        public String picHeight;

        public String picWidth;
    }
}
