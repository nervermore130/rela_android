package com.thel.bean;

import com.thel.base.BaseDataBean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 充值和消费记录列表
 * Created by Setsail on 2016/5/31.
 */
public class RechargeRecordListBean extends BaseDataBean implements Serializable {
    public int cursor;
    /**
     * 商品列表
     */
    public List<RechargeRecord> list;

    /**
     * 从json对象封装对象
     *
     * @param jsonObject
     */
//    public void fromJson(JSONObject jsonObject) {
//        try {
//            cursor = JsonUtils.getInt(jsonObject, "cursor", 0);
//            JSONArray arr = JsonUtils.getJSONArray(jsonObject, "list");
//            int length = arr.length();
//            for (int i = 0; i < length; i++) {
//                RechargeRecord rechargeRecord = new RechargeRecord();
//                JSONObject obj = arr.optJSONObject(i);
//                rechargeRecord.fromJson(obj);
//                rechargeRecordList.add(rechargeRecord);
//            }
//        } catch (Exception e) {
//            if (e.getMessage() != null) {
//                Log.e(this.getClass().getName(), e.getMessage());
//            }
//        }
//    }
}
