package com.thel.bean.theme;

import java.io.Serializable;

/**
 * Created by test1 on 2017/6/1.
 */

public class ThemeClassBean implements Serializable {
    public String type;
    public String text;
    public int res_Index;
    public String desc;

    public ThemeClassBean() {

    }

    public ThemeClassBean(String type, String text, int res_Index,String desc) {
        this.type = type;
        this.text = text;
        this.res_Index = res_Index;
        this.desc=desc;
    }

    @Override
    public String toString() {
        return "ThemeClassBean{" +
                "logType='" + type + '\'' +
                ", text='" + text + '\'' +
                ", res_Index=" + res_Index +
                ", desc='" + desc + '\'' +
                '}';
    }
}
