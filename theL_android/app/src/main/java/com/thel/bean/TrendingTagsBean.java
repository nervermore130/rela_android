package com.thel.bean;

import com.thel.base.BaseDataBean;

import java.util.List;

/**
 * 人气标签
 */
public class TrendingTagsBean extends BaseDataBean {

    public Data data;

    public class Data {
        public int tagsNum;

        public List<SimpleTagBean> tagsList;
    }

}