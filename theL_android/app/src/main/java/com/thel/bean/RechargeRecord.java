package com.thel.bean;

import com.thel.base.BaseDataBean;

import java.io.Serializable;

/**
 * 充值和消费记录
 * Created by Setsail on 2016/5/31.
 */
public class RechargeRecord extends BaseDataBean implements Serializable {

    public String id;
    /**
     * 软妹豆充值或消费的金额
     */
    public int goldDelta = 0;
    /**
     * 当前余额
     */
    public int gold = 0;
    /**
     * 记录描述
     */
    public String description;
    /**
     * 记录时间
     */
    public String createTime;
    /**
     * 解析后的日期
     */
//    public String date;
//    /**
//     * 解析后的时间
//     */
//    public String time;
//    /**
//     * 是否显示头部
//     */
    public boolean showHeader = false;

    /**
     * 从json对象封装对象
     *
     * @param tempobj
     */
//    public void fromJson(JSONObject tempobj) {
//        try {
//            id = JsonUtils.getString(tempobj, "id", "");
//            gold = JsonUtils.getInt(tempobj, "gold", 0);
//            goldDelta = JsonUtils.getInt(tempobj, "goldDelta", 0);
//            description = JsonUtils.getString(tempobj, "description", "");
//            createTime = JsonUtils.getString(tempobj, "createTime", "");
//            // 解析日期和时间显示
//            final DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
//            df.setTimeZone(TimeZone.getTimeZone("UTC"));
//            final Date d = df.parse(createTime);
//            final DateFormat dfDate = new SimpleDateFormat("MM.dd.yyyy");
//            date = dfDate.format(d);
//            final DateFormat dfTime = new SimpleDateFormat("HH:mm");
//            time = dfTime.format(d);
//        } catch (Exception e) {
//            if (e.getMessage() != null) {
//                Log.e(this.getClass().getName(), e.getMessage());
//            }
//        }
//    }
}
