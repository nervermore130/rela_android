package com.thel.bean.moments;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by waiarl on 2017/7/15.
 */
public class RecommendMomentBean implements Serializable {
    public int count;
    public List<RecommendUserBean> userList;

}
