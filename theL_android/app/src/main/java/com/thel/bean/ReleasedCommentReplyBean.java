package com.thel.bean;

public class ReleasedCommentReplyBean {
    /**
     * 所属日志id
     */
    public long momentId;
    /**
     * 父评论id
     */
    public long commentId;
    /**
     * 本评论id
     */
    public long id;

}
