package com.thel.bean.user;

import com.thel.base.BaseDataBean;

import java.io.Serializable;

/**
 * @author lingwei
 */
public class VipConfigBean extends BaseDataBean implements Serializable {


    /**
     * data : {"vipSetting":{"userId":0,"level":4,"expireTime":"2019-07-28","hiding":0,"incognito":0,"followRemind":0,"vipHiding":0,"liveHiding":0}}
     */

    public DataBean data;

    public class DataBean {
        /**
         * vipSetting : {"userId":0,"level":4,"expireTime":"2019-07-28","hiding":0,"incognito":0,"followRemind":0,"vipHiding":0,"liveHiding":0}
         */

        public VipSettingBean vipSetting;

        public class VipSettingBean {
            /**
             * userId : 0
             * level : 4
             * expireTime : 2019-07-28
             * hiding : 0
             * incognito : 0
             * followRemind : 0
             * vipHiding : 0
             * liveHiding : 0
             */

            public int userId;
            public int level;
            public String expireTime;
            /**
             * 隐身
             */
            public int hiding;
            /**
             * 无痕浏览
             */
            public int incognito;
            /**
             * 取消关注提醒
             */
            public int followRemind;
            /**
             * 隐藏会员图标
             */
            public int vipHiding;
            /**
             * 悄悄查看
             */
            public int msgHiding;

            @Override
            public String toString() {
                return "VipSettingBean{" + "userId=" + userId + ", level=" + level + ", expireTime='" + expireTime + '\'' + ", hiding=" + hiding + ", incognito=" + incognito + ", followRemind=" + followRemind + ", vipHiding=" + vipHiding + ", msgHiding=" + msgHiding + ", liveHiding=" + liveHiding + '}';
            }

            public int liveHiding;
        }
    }

    @Override
    public String toString() {
        return "VipConfigBean{" + "data=" + data + '}';
    }
}
