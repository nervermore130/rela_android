package com.thel.bean;

import com.thel.base.BaseDataBean;

import java.io.Serializable;

/**
 * Created by waiarl on 2017/9/23.
 * 分享对象
 */

public class ShareBean extends BaseDataBean implements Serializable {
    public String title;

    public String text;

    public String link;

    @Override
    public String toString() {
        return "ShareBean{" + "title='" + title + '\'' + ", text='" + text + '\'' + ", link='" + link + '\'' + '}';
    }
}
