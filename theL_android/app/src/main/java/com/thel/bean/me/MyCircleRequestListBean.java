package com.thel.bean.me;

import com.thel.base.BaseDataBean;

import java.util.ArrayList;
import java.util.List;

public class MyCircleRequestListBean extends BaseDataBean {

    public Data data;

    public class Data {
        public ArrayList<MyCircleRequestBean> requests = new ArrayList<>();
    }
}