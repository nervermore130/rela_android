package com.thel.bean;

import com.thel.base.BaseDataBean;

import java.util.ArrayList;

public class FriendsListBean extends BaseDataBean {

    /**
     * 朋友总数
     */
    public int friendTotal;

    /**
     * 关注总数
     */
    public int followersTotal;

    /**
     * 粉丝总数
     */
    public int fansTotal;

    /**
     * liked总数
     */
    public int likeTotal;

    public ArrayList<FriendsBean> map_list ;

}
