package com.thel.bean;

/**
 * Created by chad
 * Time 2019-07-22
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */
public class RecommendedModel {

    public String recommendedType = "";
    public String avatar = "";
    public String imageUrl = "";
    public String title = "";
    public String url = "";
    public long momentsId = 0;
    public String momentsText = "";
    public String minText = "";
    public String nickname = "";
    public String momentsType = "";
    public String contentType = "";
    public String liveId = "";
    public int playTime = 0;
    public int playCount = 0;
    public int pixelHeight = 0;
    public int pixelWidth = 0;
    public int shareTo = 1;
    public int userId = 0;
    public int commentNum = 0;
    public String nickName = "";
    public String adUrl = "";

    public boolean isLandscape = false;
    public String description = "";
    public String liveStatus = "";

    //推荐名片
    public String cardUserId = "";
    public String cardAffection = "";
    public String cardAge = "";
    public String cardAvatar = "";
    public String cardWeight = "";
    public String cardHeight = "";
    public String cardIntro = "";
    public String cardNickName = "";
    public String cardBirthday = "";
    public String cardBgImage = "";

    //推荐到好友信息
    public String msgType = "";
    public int receiveUserId = 0;
    public String receiveNickName = "";
    public String receiveAvatar = "";
    public String msgText = "";
}
