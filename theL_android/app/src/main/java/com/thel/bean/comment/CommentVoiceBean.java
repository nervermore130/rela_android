package com.thel.bean.comment;

import java.io.Serializable;

/**
 * Created by waiarl on 2017/8/14.
 * 话题评论中的音乐日志信息
 */

public class CommentVoiceBean implements Serializable {
    public long songId = 0;
    public String songName = "";
    public String artistName = "";
    public String albumName = "";
    public String albumLogo100 = "";
    public String albumLogo444 = "";
    public String songLocation = "";
    public String toURL = "";

}
