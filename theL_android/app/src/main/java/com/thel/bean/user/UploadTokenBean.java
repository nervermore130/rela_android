package com.thel.bean.user;

import com.thel.base.BaseDataBean;

import java.io.Serializable;

/**
 * 上传图片到七牛的token
 *
 * @author Setsail
 */
public class UploadTokenBean extends BaseDataBean implements Serializable {

    public Data data;

    public class Data {
        @Override
        public String toString() {
            return "Data{" +
                    "uploadToken='" + uploadToken + '\'' +
                    '}';
        }

        /**
         * token
         */

        public String uploadToken;
    }

}
