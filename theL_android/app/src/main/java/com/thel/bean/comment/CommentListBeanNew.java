package com.thel.bean.comment;

import com.thel.base.BaseDataBean;

import java.io.Serializable;

/**
 * 评论列表，评论分组
 *
 * @author Setsail
 */
public class CommentListBeanNew extends BaseDataBean implements Serializable {

    public CommentListBeanV2 data;

}
