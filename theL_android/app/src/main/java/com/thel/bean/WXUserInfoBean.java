package com.thel.bean;

/**
 * Created by chad
 * Time 17/11/17
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */

public class WXUserInfoBean {

    public String openid;

    public String nickname;

    public int sex;

    public String language;

    public String city;

    public String province;

    public String country;

    public String headimgurl;

    public String errcode = null;

//    public List<Privilege> privilege ;

    public String unionid;

    @Override public String toString() {
        return "WXUserInfoBean{" +
                "openid='" + openid + '\'' +
                ", nickname='" + nickname + '\'' +
                ", sex=" + sex +
                ", language='" + language + '\'' +
                ", city='" + city + '\'' +
                ", province='" + province + '\'' +
                ", country='" + country + '\'' +
                ", headimgurl='" + headimgurl + '\'' +
                ", errcode='" + errcode + '\'' +
                ", unionid='" + unionid + '\'' +
                '}';
    }
}
