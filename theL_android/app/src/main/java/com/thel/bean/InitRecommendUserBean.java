package com.thel.bean;

import java.io.Serializable;
import java.util.ArrayList;

public class InitRecommendUserBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户id
     */
    public long userId;

    /**
     * 用户名
     */
    public String nickName;

    /**
     * 推荐原因
     */
    public String recommendReason;
    public String twReason;
    public String enReason;

    /**
     * 头像
     */
    public String avatar;

    /**
     * 我是否关注了她
     */
    public int followStatus;

    public ArrayList<InitRecommendUserMomentBean> moments;

}
