package com.thel.bean.me;


import com.thel.R;
import com.thel.app.TheLApp;

import java.util.ArrayList;
import java.util.Arrays;

public class MyWinkBean {

    public String createTime;
    public int userId;
    public String nickName;
    public String avatar;
    public int online;
    public int status;
    public String intro;
    public String roleName;

    /**
     * 年龄
     */
    public int age;
    /**
     * 是否开启了隐身
     */
    public int hiding;
    /**
     * 会员等级
     */
    public int level;
    /**
     * 感情状态
     */
    public int affection;
    /**
     * 认证状态，是否是加V用户 0:否 1:个人V 2:企业V
     */
    public int verifyType;
    /**
     * 认证说明
     */
    public String verifyIntro;
    /**
     * 距离
     */
    public String distance;

    public String getLoginTimeShow() {
        StringBuilder str = new StringBuilder();
        if (level == 0 || hiding == 0) {// 如果是会员并且开了隐身功能，则不显示距离
            str.append(distance);
            str.append(", ");
        }
        str.append(age);
        str.append(TheLApp.getContext().getString(R.string.updatauserinfo_activity_age_unit));
        ArrayList<String> relationship_list = new ArrayList<>(Arrays.asList(TheLApp.getContext().getResources().getStringArray(R.array.userinfo_relationship))); // 感情状态
        str.append(", ");
        try {
            str.append(relationship_list.get(affection));
        } catch (Exception e) {
            str.append(relationship_list.get(0));
        }

        return str.toString();
    }

}