package com.thel.bean;

import com.thel.base.BaseDataBean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by chad
 * Time 17/12/4
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */

public class LiveClassificationBean extends BaseDataBean {

    public List<Data> data;

    public class Data implements Serializable  {
        public int id;

        public String name;

        public String icon;

        public String selectedIcon;
    }
}
