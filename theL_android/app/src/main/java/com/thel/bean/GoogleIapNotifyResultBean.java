package com.thel.bean;

import com.thel.base.BaseDataBean;

import java.io.Serializable;

/**
 * google iap验证结果
 *
 * @author Setsail
 */
public class GoogleIapNotifyResultBean extends BaseDataBean implements Serializable {

    /**
     * 表示服务器已经接收并正确处理
     */
    public int received;

}
