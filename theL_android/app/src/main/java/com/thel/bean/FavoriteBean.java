package com.thel.bean;

import android.text.TextUtils;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseDataBean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by the L on 2016/7/28.
 */
public class FavoriteBean extends BaseDataBean implements Serializable {
    /**
     * 收藏id
     */
    public String id;//收藏id;
    /**
     * 发布日志的用户id
     */
    public String userId;//发布日志的用户id
    /**
     * 收藏类型(目前只有一种)
     */
    public String type;//收藏类型(目前只有一种)
    /**
     * 用户的头像
     */
    public String avatar;//用户的头像
    /**
     * 用户昵称
     */
    public String nickName;//用户昵称
    /**
     * 收藏时间
     */
    public String time;//收藏时间
    /**
     * 收藏内容是否被删除
     */
    public boolean isDelete = false;//收藏内容是否被删除
    /**
     * 收藏的日志ID
     */
    public String momentsId;//收藏的日志ID
    /**
     * 日志文字内容
     */
    public String momentsText;//日志文字内容
    /**
     * 收藏的日志的类型
     */
    public String momentsType;//收藏的日志的类型
    /**
     *
     */
    public String thumbnailUrl;//
    /**
     * 日志的图片路径
     */
    public String imageUrl;//日志的图片路径
    /**
     * 是否是匿名，1位匿名，
     */
    public int secret;

    /**
     * 4.1.0 新增，话题回复的话题回复类型
     * 类型参考 ThemeCommentBean 中的 commentTypeClass
     */
    public String themeReplyClass;


    /**
     * 图片集合
     */
//    public List<String> imageList = new ArrayList<>();

    public String getReleaseTimeShow() {
        if (TextUtils.isEmpty(time)) {
            return "";
        }
        String[] arr = this.time.split("_");
        if (arr.length < 2) {
            return "";
        }
        if ("0".equals(arr[0])) {
            return TheLApp.getContext().getString(R.string.info_just_now);
        } else if ("1".equals(arr[0])) {
            return arr[1] + TheLApp.getContext().getString(R.string.info_seconds_ago);
        } else if ("2".equals(arr[0])) {
            return arr[1] + TheLApp.getContext().getString(R.string.info_minutes_ago);
        } else if ("3".equals(arr[0])) {
            return arr[1] + TheLApp.getContext().getString(R.string.info_hours_ago);
        } else if ("4".equals(arr[0])) {
            return arr[1] + TheLApp.getContext().getString(R.string.info_days_ago);
        } else if ("5".equals(arr[0])) {
            return arr[1];
        }
        return "";

    }

    @Override
    public String toString() {
        return "FavoriteBean{" +
                "avatar='" + avatar + '\'' +
                ", id='" + id + '\'' +
                ", userId='" + userId + '\'' +
                ", logType='" + type + '\'' +
                ", nickName='" + nickName + '\'' +
                ", time='" + time + '\'' +
                ", isDelete=" + isDelete +
                ", momentsId='" + momentsId + '\'' +
                ", momentsText='" + momentsText + '\'' +
                ", momentsType='" + momentsType + '\'' +
                ", thumbnailUrl='" + thumbnailUrl + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                '}';
    }
}
