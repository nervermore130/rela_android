package com.thel.bean.gift;

import com.thel.base.BaseDataBean;
import com.thel.bean.CommentWinkUserBean;

import java.util.List;

public class WinkCommentListBean extends BaseDataBean {

    public CommentWinkDataUserBean data;

    public class CommentWinkDataUserBean {
        public List<CommentWinkUserBean> list;
        public boolean haveNextPage;
        public int cursor;
        public long num;
    }


}