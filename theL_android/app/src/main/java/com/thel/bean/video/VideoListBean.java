package com.thel.bean.video;

import java.util.List;

/**
 * Created by the L on 2017/2/10.
 */

public class VideoListBean {
    /**
     * cursor
     */
    public long cursor;

    /**
     * 列表数量
     */
    public int count;

    /**
     * 是否有下一页
     */
    public boolean haveNextPage;

    /**
     * 列表
     */
    public List<VideoBean> list;


}
