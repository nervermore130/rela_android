package com.thel.bean;

import com.thel.modules.main.me.bean.BaseTagBean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by chad
 * Time 17/9/30
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */

public class TopicBean extends BaseTagBean implements Serializable {

    /**
     * 标签下的日志数
     */
    public int momentsNum = 0;

    /**
     * 新日志数量
     */
    public int newMomentsNum = 0;

    /**
     * 关注该标签的人数(有一两个接口是保存日志数)
     */
    public String joinCount;

    public List<TopicImageBean> topicImageList = new ArrayList<TopicImageBean>();

    // 在搜索话题页面，加载最近参与的话题和热门话题时用到
    public String recentOrHotTag;

    // 话题的标志
    public String logoImageUrl;

    // 对应的标签分类的id和名称
    public String cateId;
    public String cateName;

    public class TopicImageBean implements Serializable {
        /**
         * 日志id
         */
        public String momentId;

        /**
         * 日志图片
         */
        public String imageUrl;

        // 是否是音乐日志图片
        public int musicFlag = 0;
    }
}
