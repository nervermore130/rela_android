package com.thel.bean.theme;

import com.thel.base.BaseDataBean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by test1 on 2017/6/7.
 */

public class ThemeListBean extends BaseDataBean implements Serializable {

    public Data data;

    public class Data {
        public boolean haveNextPage;
        /**
         * 数据库游标
         */
        public int cursor;
        /**
         * 子评论列表
         */
        public List<ThemeBean> list;
        /**
         * 埋点
         * */
        public String rankId;

    }
}
