package com.thel.bean;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseDataBean;

import java.text.DecimalFormat;

/**
 * 表情列表中listbean
 * Created by lingwei on 2017/11/6.
 */

public class SimpleStikerBean extends BaseDataBean {

    /**
     * id : 5
     * title : 黄猴儿
     * thumbnail : http://7xl7rh.com5.z0.glb.qiniucdn.com/sticker/poormonkey/poormonkey_icon_m.png
     * bgImg : http://7xl7rh.com5.z0.glb.qiniucdn.com/sticker/poormonkey/poormonkey_bg.png
     * summary : 人贱人爱的小猴纸
     * isNew : 0
     * price : 0
     * purchased : 1
     * iapId :
     */
    /**
     * 0:不显示标题 1:显示推荐表情标题 2:显示更多表情标题
     */
    public int showTitle;
    /**
     * 表情状态
     * 0:未购买 1:已购买，未下载 2:已下载 3:正在下载
     */
    public int status;

    public int id;

    @Override
    public String toString() {
        return "SimpleStikerBean{" + "showTitle=" + showTitle + ", status=" + status + ", id=" + id + ", vipFree=" + vipFree + ", title='" + title + '\'' + ", thumbnail='" + thumbnail + '\'' + ", bgImg='" + bgImg + '\'' + ", summary='" + summary + '\'' + ", isNew=" + isNew + ", price=" + price + ", purchased=" + purchased + ", iapId='" + iapId + '\'' + '}';
    }

    /**
     * 会员免费
     * 为1的时候该表情会员免费，其他情况下默认
     */
    public int vipFree;
    public String title;
    public String thumbnail;
    public String bgImg;
    public String summary;
    public int isNew;
    public int price;
    public int purchased;
    public String iapId;

    public String generatePriceShow() {
        try {
            DecimalFormat df = new DecimalFormat("0.00");
            if (Double.valueOf(price) == 0) {
                return TheLApp.context.getString(R.string.sticker_store_act_download);
            }
            return "￥" + df.format(Double.valueOf(price) / 100);
        } catch (Exception e) {
            return "￥" + price;
        }
    }
}
