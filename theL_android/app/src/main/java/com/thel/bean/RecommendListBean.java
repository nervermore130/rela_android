package com.thel.bean;

import com.thel.base.BaseDataBean;

import java.util.List;

/**
 *
 * @author liuyun
 * @date 2017/12/26
 */

public class RecommendListBean extends BaseDataBean {

    public RecommendDataBean data;

    public class RecommendDataBean {
        public List<GuideDataBean> list;
        public boolean haveNextPage;
        public int cursor;
    }

    public class GuideDataBean {
        public int userId;
        public String nickName;
        public String avatar;
        public String userName;
        public String roleName;
        public long fansNum;
        public boolean isFollow = false;
        public int age;
        public int affection;
        public long verifyType = 0;
        public String verifyIntro;
        public String intro;
        /**
         * 0 互相未关注；
         * 1 我关注对方；
         * 2 对方关注我；
         * 3 互相关注；
         */
        public int followStatus = 1;
        public String recommendReason;//推荐原因
        public List<PicBean> picList;
    }

    public class PicBean {
        public long picId;
        public String longThumbnailUrl;
        public int isPrivate;
        public int isBlcak;
        public String picUrl;
        public long picWidth;
        public long picHeight;
    }

}
