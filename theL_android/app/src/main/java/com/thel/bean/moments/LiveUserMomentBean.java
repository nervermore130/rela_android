package com.thel.bean.moments;

import java.io.Serializable;

/**
 * Created by waiarl on 2017/7/14.
 * 推荐的主播 bean
 */

public class LiveUserMomentBean implements Serializable {
    public String userId;
    public String nickname;
    public String liveId;
    /**
     * 新播放状态&,观看人数），-1 代表已结束。else为 观看人数
     */
    public int liveStatus;
    public String description;
    public int audioType;//0为普通直播，1为声音直播
    /**
     * 1横屏 0竖屏
     */
    public int isLandscape;

    @Override public String toString() {
        return "LiveUserMomentBean{" +
                "userId='" + userId + '\'' +
                ", nickname='" + nickname + '\'' +
                ", liveId='" + liveId + '\'' +
                ", liveStatus=" + liveStatus +
                ", description='" + description + '\'' +
                ", audioType=" + audioType +
                '}';
    }
}
