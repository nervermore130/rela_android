package com.thel.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;

import com.thel.manager.CDNBalanceManager;
import com.thel.network.RequestConstants;
import com.thel.utils.SharedPrefUtils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class NetworkReceiver {

    private static NetworkReceiver INSTANCE = new NetworkReceiver();

    public static NetworkReceiver getInstance() {
        return INSTANCE;
    }

    private NetworkReceiver() {

    }

    private Map<String, NetworkListener> listenerMap = new HashMap<>();


    public void init(Context context) {
        if (context != null && broadcastReceiver != null) {
            context.registerReceiver(broadcastReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }

    public void clear(Context context) {
        if (listenerMap != null) {
            listenerMap.clear();
        }

        if (context != null && broadcastReceiver != null) {
            context.unregisterReceiver(broadcastReceiver);
        }
    }

    public void register(String key, NetworkListener listener) {
        if (listenerMap != null) {
            listenerMap.put(key, listener);
        }
    }

    public void unregister(String key) {
        if (listenerMap != null) {
            listenerMap.remove(key);
        }
    }


    public interface NetworkListener {
        /**
         * 网络是否可用
         *
         * @param isNetworkEnable
         */
        void onNetworkEnable(boolean isNetworkEnable);
    }

    private void pushNetworkStatus(boolean isEnable) {
        Iterator<Map.Entry<String, NetworkListener>> iterator = listenerMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, NetworkListener> entry = iterator.next();
            NetworkListener listener = entry.getValue();
            listener.onNetworkEnable(isEnable);
        }
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            // 网络状态改变
            if (TextUtils.equals(action, ConnectivityManager.CONNECTIVITY_ACTION)) {
                ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();
                if (activeNetInfo != null && activeNetInfo.isConnected()) {// 连上网络
                    int netType = activeNetInfo.getType();
                    // 网络切换，从WIFI到GPRS，清空HTTPDNS缓存
                    if ((RequestConstants.NET_TYPE == ConnectivityManager.TYPE_WIFI && netType != ConnectivityManager.TYPE_WIFI)) {
                        clearHttpDNS();
                    }
                    // 网络切换，从GPRS到WIFI，清空HTTPDNS缓存
                    if ((RequestConstants.NET_TYPE != ConnectivityManager.TYPE_WIFI) && netType == ConnectivityManager.TYPE_WIFI) {
                        clearHttpDNS();
                    }

                    CDNBalanceManager.getInstance().setNetworkChanged();

                    RequestConstants.NET_TYPE = netType;

                    pushNetworkStatus(true);

                } else {// 网络断开
                    clearHttpDNS();

                    pushNetworkStatus(false);
                }
            }
        }
    };

    private static void clearHttpDNS() {
        SharedPrefUtils.clearData(SharedPrefUtils.FILE_HTTP_DNS);
    }


}
