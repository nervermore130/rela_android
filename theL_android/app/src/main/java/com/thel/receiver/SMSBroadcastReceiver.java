package com.thel.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsMessage;
import android.util.Log;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by chad
 * Time 18/4/24
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */
public class SMSBroadcastReceiver extends BroadcastReceiver {

    private static final String TAG = "SMSBroadcastReceiver";

    private static MessageListener mMessageListener;

    public SMSBroadcastReceiver() {
        super();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Object[] pdus = (Object[]) intent.getExtras().get("pdus");
        for (Object pdu : pdus) {
            SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) pdu);
            String sender = smsMessage.getDisplayOriginatingAddress();
            String content = smsMessage.getMessageBody();

            Log.i(TAG, "onReceive: 短信来自:" + sender);
            Log.i(TAG, "onReceive: 短信内容:" + content);

            //如果短信号码来自自己的短信网关号码
//            if ("10691406012360".equals(sender)) {//Rela网关号码不固定，无法通过sender判断
            if (content != null && content.contains("【Rela热拉】")) {
                if (mMessageListener != null) mMessageListener.OnReceived(getDynamicPwd(content));
                break;
            }
        }
    }

    /**
     * 从字符串中截取连续4位数字组合 ([0-9]{4})截取四位数字 进行前后断言不能出现数字 用于从短信中获取动态密码
     *
     * @param content 短信内容
     * @return 截取得到的4位Rela动态密码
     */
    private String getDynamicPwd(String content) {
        // 此正则表达式验证四位数字的短信验证码
        Pattern pattern = Pattern.compile("(?<![0-9])([0-9]{4})(?![0-9])");
        Matcher matcher = pattern.matcher(content);
        String dynamicPwd = "";
        while (matcher.find()) {
            dynamicPwd = matcher.group();
        }
        return dynamicPwd;
    }

    // 回调接口
    public interface MessageListener {

        /**
         * 接收到自己的验证码时回调
         *
         * @param code 验证码
         */
        void OnReceived(String code);
    }

    /**
     * 设置验证码接收监听
     *
     * @param messageListener 自己验证码的接受监听，接收到自己验证码时回调
     */
    public void setOnReceivedMessageListener(MessageListener messageListener) {
        mMessageListener = messageListener;
    }
}
