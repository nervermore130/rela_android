package com.thel.app;

import android.content.Context;
import android.os.Build;
import android.os.StrictMode;

import com.bumptech.glide.request.target.ViewTarget;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipelineConfig;
import com.liulishuo.filedownloader.FileDownloader;
import com.thel.R;
import com.thel.constants.TheLConstants;
import com.thel.network.OkHttpNetworkFetcher;
import com.thel.receiver.NetworkReceiver;
import com.thel.utils.AppInit;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.UMShareAPI;

import cn.udesk.UdeskSDKManager;
import okhttp3.OkHttpClient;
import video.com.relavideolibrary.RelaVideoSDK;

/**
 * Created by liuyun on 2017/9/14.
 */

public class TheLApp {

    public static Context context;

    public static Context getContext() {
        return context;
    }

    public static int enablePubLive = 1;

    public static void onCreate(Context context) {

        //Android7.0 FileUriExposedException 问题解决
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
        }

        TheLApp.context = context;

        AppInit.getInstance().init(context);

        initFresco();

        // 友盟统计
        MobclickAgent.openActivityDurationTrack(false);
        // 关闭友盟错误统计
        MobclickAgent.setCatchUncaughtExceptions(false);
        MobclickAgent.setCheckDevice(false);


        // 友盟分享初始化
        UMShareAPI.get(context);

        //facebook应用事件
        try {
            FacebookSdk.sdkInitialize(context);//旧版本会弹出一些莫名其妙的错误，先加上这句话
            AppEventsLogger.activateApp(context);
        } catch (Exception e) {
            e.printStackTrace();
        }

        setGlideTagID();

        FileDownloader.init(context);//下载更新

        //FB sdk 初始化
        FacebookSdk.setApplicationId(TheLConstants.FACE_BOOK_ID);

        FacebookSdk.sdkInitialize(context.getApplicationContext());

        RelaVideoSDK.init(context.getApplicationContext());

        NetworkReceiver.getInstance().init(context);

    }

    private static void initFresco() {
        ImagePipelineConfig imagePipelineConfig = ImagePipelineConfig.newBuilder(context).setNetworkFetcher(new OkHttpNetworkFetcher(new OkHttpClient.Builder().build())).build();
        Fresco.initialize(context, imagePipelineConfig);
    }

    private static void setGlideTagID() {
        try {
            ViewTarget.setTagId(R.id.glide_tag);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
