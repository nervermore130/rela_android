package com.thel.app;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Process;
import android.text.TextUtils;
import android.util.Log;

import com.growingio.android.sdk.collection.Configuration;
import com.growingio.android.sdk.collection.GrowingIO;
import com.growingio.android.sdk.deeplink.DeeplinkCallback;
import com.ishumei.smantifraud.SmAntiFraud;
import com.meituan.android.walle.WalleChannelReader;
import com.qiniu.pili.droid.streaming.StreamingEnv;
import com.tencent.bugly.crashreport.CrashReport;
import com.thel.BuildConfig;
import com.thel.R;
import com.thel.base.BaseDataBean;
import com.thel.chat.live.TIMManager;
import com.thel.constants.Configurations;
import com.thel.constants.TheLConstants;
import com.thel.flutter.FlutterCommonActivity;
import com.thel.flutter.FlutterPlatform;
import com.thel.flutter.NewRelaBoostFlutterActivity;
import com.thel.flutter.bridge.RfSBridgeHandlerFactory;
import com.thel.flutter.playform_view.textview.TextPlatformViewPlugin;
import com.thel.manager.ActivityManager;
import com.thel.modules.main.messages.utils.PushUtils;
import com.thel.modules.welcome.SplashActivity;
import com.thel.modules.welcome.WelcomeActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.RequestConstants;
import com.thel.push.OnPushListener;
import com.thel.push.RelaPushManager;
import com.thel.utils.AppUtils;
import com.thel.utils.CrashUtils;
import com.thel.utils.DeviceUtils;
import com.thel.utils.L;
import com.thel.utils.LoadedApkHuaWei;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.UserUtils;
import com.umeng.analytics.MobclickAgent;

import org.lasque.tusdk.core.TuSdk;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import io.flutter.app.FlutterApplication;
import io.flutter.embedding.android.FlutterView;
import io.flutter.plugin.common.MethodChannel;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;
import me.rela.rf_s_bridge.RfSBridgeHandler;
import me.rela.rf_s_bridge.RfSBridgeHandlerProvider;
import me.rela.rf_s_bridge.new_router.NewFlutterBoost;
import me.rela.rf_s_bridge.new_router.NewUniversalRouter;
import me.rela.rf_s_bridge.new_router.Platform;
import me.rela.rf_s_bridge.new_router.containers.NewBoostFlutterActivity;
import me.rela.rf_s_bridge.new_router.interfaces.INativeRouter;
import me.rela.rf_s_bridge.router.FlutterBoostPlugin;

/**
 * Created by LiuYun
 */
public class RelaApplication extends FlutterApplication implements RfSBridgeHandlerProvider {


    public static final String TAG = "RelaApplication";

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        if (!BuildConfig.DEBUG) {
            closeAndroidPDialog();
        }
        fixOppoAssetManager();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        long startTime = System.currentTimeMillis();

        initFlutterPlugin();

        TheLApp.onCreate(this);

        long endTime = System.currentTimeMillis();

        L.d("RelaApplication", " time1 : " + (endTime - startTime));

        initTinker();

        /***解决华为手机无法注册超过500个广播的问题***/
        try {
            LoadedApkHuaWei.hookHuaWeiVerifier(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        StreamingEnv.init(this);

        RxJavaPlugins.setErrorHandler(throwable -> L.d("RelaApplication", " rxjava error handler : " + throwable));
        long endTime2 = System.currentTimeMillis();

        L.d("RelaApplication", " time2 : " + (endTime2 - endTime));

        //初始化涂图sdk
        initTuSDK();

        Configurations.initConfiguration();

        CrashUtils.Companion.uncaughtDaemonException();

        TIMManager.Companion.init(this);

        CrashReport.initCrashReport(getApplicationContext(), "49ea8a7c03", false);
    }

    /*
     *  修复发生在OPPO R9和A5的手机的奔溃
     * android.content.res.AssetManager.finalize() timed out after 120 seconds android.content.res.AssetManager.destroy(Native Method)
     * */
    private void fixOppoAssetManager() {
        String device = Build.MODEL;
        L.d("RelaApplication", "fixOppoAssetManager device:" + device);
        if (!TextUtils.isEmpty(device)) {
            if (device.contains("OPPO R9") || device.contains("OPPO A5")) {
                try {
                    Class<?> c = Class.forName("java.lang.Daemons");
                    Field maxField = c.getDeclaredField("MAX_FINALIZE_NANOS");
                    maxField.setAccessible(true);
                    maxField.set(null, Long.MAX_VALUE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void initTuSDK() {
        /**
         * init must be called before any other func
         */
        StreamingEnv.init(getApplicationContext());

        /**
         ************************* TuSDK 集成三部曲 *************************
         *
         * 1. 在官网注册开发者账户
         *
         * 2. 下载SDK和示例代码
         *
         * 3. 创建应用，获取appkey，导出资源包
         *
         ************************* TuSDK 集成三部曲 *************************
         *
         * 关于TuSDK体积 (约2M大小)
         *
         * Android 编译知识：
         * APK文件包含了Java代码，JNI库和资源文件；
         * JNI库包含arm64-v8a,armeabi等不同CPU的编译结果的集合，这些都会编译进 APK 文件；
         * 在安装应用时，系统会自动选择最合适的JNI版本，其他版本不会占用空间；
         * 参考TuSDK Demo的APK 大小，除去资源和JNI库，SDK本身的大小约2M；
         *
         * 开发文档:http://tusdk.com/doc
         */


        // 设置资源类，当 Application id 与 Package Name 不相同时，必须手动调用该方法, 且在 init 之前执行。
        TuSdk.setResourcePackageClazz(R.class);

        // 初始化全局变量
        TuSdk.enableDebugLog(true);
        // 开发ID (请前往 http://tusdk.com 获取您的APP 开发秘钥)
        if (RequestConstants.APPLICATION_ID_LOCAL.equals(BuildConfig.APPLICATION_ID)) {
            TuSdk.init(this.getApplicationContext(), "d033cc5fff3746ed-00-w0u6s1");
        } else if (RequestConstants.APPLICATION_ID_GLOBAL.equals(BuildConfig.APPLICATION_ID)) {
            TuSdk.init(this.getApplicationContext(), "ef591d7ea26d2b61-00-w0u6s1");
        } else {
            TuSdk.init(this.getApplicationContext(), "d033cc5fff3746ed-00-w0u6s1");
        }
    }

    private void initTinker() {

        String channel = WalleChannelReader.getChannel(this);

        initSmSDK(channel);

        initGIO(channel);
        //fabric
//        Fabric.with(this, new Crashlytics());

        MobclickAgent.startWithConfigure(new MobclickAgent.UMAnalyticsConfig(this, "51d4325b56240b25250696b7", channel));

        RelaPushManager.getInstance().initJPush(this, mOnPushListener);

    }

    private void initGIO(String channel) {

        try {
            GrowingIO.startWithConfiguration(this, new Configuration().trackAllFragments().setDeeplinkCallback(new DeeplinkCallback() {
                @Override
                public void onReceive(Map<String, String> params, int i, long l) {
                    PushUtils.push(TheLApp.context, params.toString());

                }
            }).setChannel(channel).setDebugMode(false).setMutiprocess(true).supportMultiProcessCircle(true));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initSmSDK(String channel) {
        if (this.getPackageName().equals(getCurProcessName(this))) {
            try {
                SmAntiFraud.SmOption option = new SmAntiFraud.SmOption();
                //1.通用配置项
                option.setOrganization(TheLConstants.SM_ORGANIZATION); //必填，组织标识，邮件中organization项
                option.setAppId("default"); //必填，应用标识，登录数美后台应用管理查看
//            option.setPublicKey("xxxxxxxx"); //已跟数美确认，可以为空，加密 KEY，邮件中 android_public_key 附件内容
//            option.setAinfoKey("xxxxxxxx"); //已跟数美确认，可以为空，加密 KEY，邮件中 Android ainfo key 项
                //2.连接海外机房特殊配置项，仅供设备数据上报海外机房客户使用
                // option.setArea(SmAntiFraud.AREA_XJP); //连接新加坡机房客户使用此选项
                // option.setArea(SmAntiFraud.AREA_FJNY); //连接美国机房客户使用此选项
                option.setChannel(DeviceUtils.getAppMetaData(this, channel));
                //3.SDK 初始化
                SmAntiFraud.create(this, option);

                //4.获取设备标识，注意获取 deviceId，这个接口在需要使用 deviceId 时地方调用
                String deviceId = SmAntiFraud.getDeviceId();
                final String m_deviceId = "sm_" + deviceId;
                L.d("RelaApplication", "initSmSDK deviceId:" + deviceId);
                ShareFileUtils.setString(ShareFileUtils.DEVICE_ID, m_deviceId);
            } catch (Exception e) {
                e.printStackTrace();
                L.d("RelaApplication", "initSmSDK error:" + e);
            }
        }

    }

    private String getCurProcessName(Context context) {
        int pid = android.os.Process.myPid();
        android.app.ActivityManager mActivityManager = (android.app.ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (android.app.ActivityManager.RunningAppProcessInfo appProcess : mActivityManager
                .getRunningAppProcesses()) {
            if (appProcess.pid == pid) {
                return appProcess.processName;
            }
        }
        return null;
    }

    private OnPushListener mOnPushListener = new OnPushListener() {
        @Override
        public void onPushToken(String token) {

            L.d("RelaApplication", " onPushToken : ");
            if (!TextUtils.isEmpty(UserUtils.getUserKey()) && !TextUtils.isEmpty(UserUtils.getMyUserId())) {
                pushJGToken(token);
            }
        }

        @Override public void onPushMessage(String message) {

            L.d("RelaApplication", " ActivityManager.getInstance().getStackActivityCount() : " + ActivityManager.getInstance().getStackActivityCount());

            L.d("RelaApplication", " message : " + message);

//            PushUtils.push(TheLApp.context,message);

            int activityCount = ActivityManager.getInstance().getStackActivityCount();
            if (activityCount > 0) {
                Intent intent = new Intent(TheLApp.context, SplashActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(SplashActivity.PUSH_MSG, message);
                TheLApp.context.startActivity(intent);
            } else {
                ShareFileUtils.setString(ShareFileUtils.PUSH_MSG, message);
                Intent intent = new Intent(TheLApp.context, WelcomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                TheLApp.context.startActivity(intent);
            }
        }

    };

    private void pushJGToken(String token) {

        ShareFileUtils.setString(ShareFileUtils.UMENG_TOKEN, token);

        L.d("RelaApplication", " token : " + token);

        RequestBusiness.getInstance().uploadDeviceToken(token, token).onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
            @Override
            public void onNext(BaseDataBean data) {
                super.onNext(data);

                L.d("RelaApplication", " pushUmengToken onNext : ");

            }

            @Override public void onError(Throwable t) {
                super.onError(t);
                L.d("RelaApplication", " pushUmengToken onError : " + t.getMessage());
            }
        });
    }

    /**
     * android 9.0 调用私有api弹框的解决方案
     */
    private void closeAndroidPDialog() {
        try {
            Class aClass = Class.forName("android.content.pm.PackageParser$Package");
            Constructor declaredConstructor = aClass.getDeclaredConstructor(String.class);
            declaredConstructor.setAccessible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            Class cls = Class.forName("android.app.ActivityThread");
            Method declaredMethod = cls.getDeclaredMethod("currentActivityThread");
            declaredMethod.setAccessible(true);
            Object activityThread = declaredMethod.invoke(null);
            Field mHiddenApiWarningShown = cls.getDeclaredField("mHiddenApiWarningShown");
            mHiddenApiWarningShown.setAccessible(true);
            mHiddenApiWarningShown.setBoolean(activityThread, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initFlutterPlugin() {
        INativeRouter router = new INativeRouter() {
            @Override
            public void openContainer(Context context, String url, Map<String, Object> urlParams, int requestCode, Map<String, Object> exts) {
//                String assembleUrl = Utils.assembleUrl(url, urlParams);
//                PageRouter.openPageByUrl(context, assembleUrl, urlParams);
            }

        };

        NewFlutterBoost.BoostLifecycleListener lifecycleListener = new NewFlutterBoost.BoostLifecycleListener() {
            @Override
            public void onEngineCreated() {

            }

            @Override
            public void onPluginsRegistered() {
                MethodChannel mMethodChannel = new MethodChannel(NewFlutterBoost.instance().engineProvider().getDartExecutor(), "methodChannel");
                L.e("MyApplication", "MethodChannel create");
                TextPlatformViewPlugin.register(NewFlutterBoost.instance().getPluginRegistry().registrarFor("TextPlatformViewPlugin"));

            }

            @Override
            public void onEngineDestroy() {

            }
        };
        Platform platform = new NewFlutterBoost
                .ConfigBuilder(this, router)
                .isDebug(true)
                .whenEngineStart(NewFlutterBoost.ConfigBuilder.ANY_ACTIVITY_CREATED)
                .renderMode(FlutterView.RenderMode.texture)
                .lifecycleListener(lifecycleListener)
                .build();

        NewFlutterBoost.instance().init(platform);

        NewUniversalRouter.sharedInstance.setActivityClass(NewRelaBoostFlutterActivity.class);
    }

    @Override
    public RfSBridgeHandler bridgeHandler() {
        return RfSBridgeHandlerFactory.getInstance();
    }

}
