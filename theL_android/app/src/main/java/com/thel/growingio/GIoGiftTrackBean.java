package com.thel.growingio;

import java.io.Serializable;

/**
 * Created by lingwei on 2018/3/29.
 */

public class GIoGiftTrackBean implements Serializable {
    public String track;
    public GIOGiftPurchaseBean payBean = new GIOGiftPurchaseBean();

    public GIoGiftTrackBean(String track, String orderID, String giftID, String giftPrice, String giftName, String hostID, String hostName, String timeFromStart, String liveType) {
        this.track = track;
        this.payBean = new GIOGiftPurchaseBean(orderID, giftID, giftPrice, giftName, hostID, hostName, timeFromStart, liveType);
    }

    public GIoGiftTrackBean(String track, String orderID, String membershipType, String platform) {
        this.track = track;
        this.payBean = new GIOGiftPurchaseBean(orderID, membershipType, platform);
    }

    public GIoGiftTrackBean(String track, String orderID, String membershipType) {
        this.track = track;
        this.payBean = new GIOGiftPurchaseBean(orderID, membershipType);
    }

}
