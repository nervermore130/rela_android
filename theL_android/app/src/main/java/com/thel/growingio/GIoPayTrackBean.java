package com.thel.growingio;

import java.io.Serializable;

/**
 * Created by lingwei on 2018/3/29.
 */

public class GIoPayTrackBean implements Serializable {
    public String track;
    public GIOPayBean payBean = new GIOPayBean();

    public GIoPayTrackBean(String track, String orderID, String amount, String platform) {
        this.track = track;
        this.payBean = new GIOPayBean(orderID, amount, platform);
    }

    public GIoPayTrackBean(String track, String orderid, String platform) {
        this.track = track;
        this.payBean = new GIOPayBean(orderid, platform);
    }

}
