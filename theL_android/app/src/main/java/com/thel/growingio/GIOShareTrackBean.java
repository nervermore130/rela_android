package com.thel.growingio;

import java.io.Serializable;

/**
 * Created by waiarl on 2018/3/23.
 */

public class GIOShareTrackBean implements Serializable {
    public String track;
    public GIOShareBean shareBean = new GIOShareBean();

    public GIOShareTrackBean(String track, GIOShareBean shareBean) {
        this.track = track;
        this.shareBean = shareBean;
    }

    public GIOShareTrackBean(String track, String shareType, String shareEntry, String shareMaterialType) {
        this.track = track;
        this.shareBean.setProperty(shareType, shareEntry, shareMaterialType);
    }

    public GIOShareTrackBean() {
    }
}
