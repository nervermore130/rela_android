package com.thel.growingio;

/**
 * Created by waiarl on 2018/3/23.
 */

public interface GrowingIoConstant {
    String LIVE_SHARE = "liveShare";
    String LOG_SHARE = "logShare";
    String VIDEO_SHARE = "videoShare";
    String TOPIC_SHARE = "topicShare";
    String PERSONAL_POST_SHARE = "personalPostShare";
    String LOG_POST_SHARE = "personalPostShare";
    String ENCOUNTER_RESULT_SHARE = "encounterResultShare";//相遇结果页分享次数
    String USER_GROUP = "user_group";//用于ABtest 对用户进行分组 ABCD

    /***直播***/
    String ENTRY_LIVE_CAPTURE = "直播端分享按钮";
    String ENTRY_LIVE_MOMENT = "直播日志";
    String ENTRY_LIVE_AVATAR = "观看端头像";
    String ENTRY_LIVE_RECOMMEND = "观看端推荐按钮";
    String ENTRY_LIVE_SHARE = "观看端分享按钮";
    /***日志，视频，话题***/
    String ENTRY_FOLLOW_PAGE = "关注页";
    String ENTRY_TAG_PAGE = "标签页";
    String ENTRY_RECOMMEND_PAGE = "日志推荐页";
    String ENTRY_PERSONAL_PAGE = "个人主页";
    String ENTRY_MOMENT_DETAIL_PAGE = "日志详情页";
    String ENTRY_THEME_DETAIL_PAGE = "话题详情页";
    String ENTRY_NEARBY_PAGE = "附近页";
    String ENTRY_LONG_VIDEO = "长视频";
    String ENTRY_VIDEO = "视频观看页";
    String ENTRY_USER_CARD = "名片";
    String ENTRY_VIDEO_LIST = "视频列表";
    String ENTRY_TIME_MACHINE = "时光机";

    String MATERIAL_LIVE_VOICE = "声音直播";
    String MATERIAL_LIVE_VIDEO = "视频直播";
    String MATERIAL_LIVE_MULTI = "多人连麦";
    String MATERIAL_THEME = "话题";
    String MATERIAL_THEME_REPLY = "话题回复";
    String MATERIAL_VIDEO = "video";

    String TYPE_FACEBOOK = "Facebook";
    String TYPE_WEIXIN_CIRCLE = "朋友圈";
    String TYPE_WEIXIN = "微信";
    String TYPE_SINA = "微博";
    String TYPE_QZONE = "QQ空间";
    String TYPE_QQ = "QQ";

    /***付费****/
    String ORDERID = "orderID";
    String AMOUNT = "amount";
    String PLATFORM = "platform";

    /**
     * 付费相关的标识符
     **/
    String LIVEPAGE_CLICK = "livePage_giftList_rmdPurchaseBtnClck";  //直播页_礼物列表_去充值按钮点击
    String LIVE_PAGE_POPWIN = "livePage_popwin_rmdPurchaseBtnClck";//直播页_余额不足弹窗_充值按钮点击
    String USER_PAGE_CLICK = "userPage_rmdPurchaseBtnClck";//个人主页_充值软妹豆按钮点击
    String RMDPURCHASE = "rmdPurchase"; //购买软妹豆
    String RMD_PURCHASE_AMOUNT = "rmdPurchaseAmount";//购买软妹豆金额
    String GIFT_PURCHASE = "giftPurchase";//购买礼物
    String MEMBERSHIP_PURCHASE = "membershipPurchase";//购买会员
    String USER_PAGE_MENBERSHIP_CLICK = "userPage_membershipPurchaseBtnClck";//个人主页_开通会员按钮点击
    String MEMBERSHIP_PURCHASEASGIFT = "membershipPurchaseAsGift";//赠送会员
    String GUICK_GIFT = "guickGift"; //快捷礼物

    /***事件级变量
     软妹豆充值入口(转化变量)*/
    String RMDPURCHASEENTRY_EVAR = "rmdPurchaseEntry_eVar";
    String G_LivePagePopWin = "余额不足弹窗";
    String G_LiveGiftLis = "礼物列表";

    String TRACK_LIVE_PAGE_VIEW = "livePageView";
    String TYPE_LIVE = "liveType";
    String LIVE_WATCH_TIME = "liveWatchTime";

    String KEY_USER_PAGE = "userPage";
    String USER_PAGE_HER = "他人主页";
    String USER_PAGE_ME = "我的主页";
    /**
     * 匹配相关标识符
     */
    String MATCH_LIKE_ME_OR_RECALL = "user_vipLevel"; //跳转到喜欢我的列表或者撤回判断会员等级
    /**
     * 会员feed流广告
     */
    String AD_MOMENTS_GOTO_VIP = "adMoment_gotoVipService"; //feed流广告跳转到vip权益页

    String PHOTO_COUNT = "PhotoCount";//用户上传的所有图片数量

    String PHOTO_TYPE = "PhotoType";

    String LONG_PHOTO = "longPhoto";

    String NO_LONG_PHOTO = "noLongPhoto";

    /**
     * 直播相关 从各个入口观看直播的人数
     */
    String WATCH_LIVE_ENTRY = "watchLiveEntry"; //事件

    String WATCH_LIVE_ENTRY_KEY = "watchLiveEntryKey";//事件级变量
    String G_LiveEntry_Push = "推送通知";
    String G_LiveEntry_Feed = "朋友圈";
    String G_LiveEntry_LiveList = "直播列表";
    String G_LiveEntry_PersonalPage = "个人主页";
    String G_LiveEntry_Other = "其他";
    String G_LiveEntry_FollowedP = "关注主播";
    String G_LiveEntry_Followed_Host_List = "关注主播列表";
    /**
     * 访问用户三次弹出聊天提示弹窗，点击聊天按钮上报标识符
     */
    String GUIDE_VISIT_CHAT = "guideVisitorChat";
    String MY_USER_ID = "myUserId";
    String TO_USER_ID = "toUserId";

    /**
     * 观众连麦次数、时长
     */
    String GUEST_LINK_MIC = "guestLinkMic";
    String GuestLinkMicDuration = "GuestLinkMicDuration";
    /**
     * 直播间各类型关注情况分类（资料卡关注／退出直播间引导关注／主播直接关注）（增加事件直播间关注、维度被关注对象[主播、观众]和关注方式）
     **/
    String LIVE_ROOM_FOLLOW = "liveRoomFollow";
    String LIVE_USER_CARD = "资料卡关注";
    String LIVE_EXIT_GUIDE = "退出直播间引导关注";
    String LIVE_HOST_FOLLOW = "主播直接关注";
    String LIVE_TARGET_HOST = "主播";
    String LIVE_TARGET_GUEST = "观众";

    //用户首次登陆关闭定位权限 和push权限
    String CLOSE_PUSH = "closePush";
    String CLOSE_LOCATION = "closeGeography";

    //新用户登录进来关注的推荐用户
    String FOLLOW_USERS = "init_follow_user";

    /**
     * 负责相关埋点
     */
    String PAGE_MY = "my";
    String PAGE_PREPAY = "prepay_bean"; //软妹豆充值列表页
    String PAGE_LIVE_ROOM = "live_room";//直播间页软妹豆列表
    String PAGE_VIP_INTRO = "vip_intro"; //会员介绍页
    String PAGE_PREPAY_VIP = "prepay_vip";//会员充值列表页


}
