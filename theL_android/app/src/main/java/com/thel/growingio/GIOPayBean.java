package com.thel.growingio;

import java.io.Serializable;

/**
 * Created by lingwei on 2018/3/29.
 */

public class GIOPayBean implements Serializable {

    public String orderID = "";

    public String amount = "";

    public String platform = "";

    public GIOPayBean(String orderID, String amount, String platform) {
        this.orderID = orderID;
        this.amount = amount;
        this.platform = platform;
    }

    public GIOPayBean() {
    }

    public GIOPayBean(String orderID, String platform) {
        this.orderID = orderID;
        this.platform = platform;
    }
}
