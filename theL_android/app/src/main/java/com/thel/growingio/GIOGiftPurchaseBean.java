package com.thel.growingio;

import java.io.Serializable;

/**
 * Created by lingwei on 2018/3/29.
 */

public class GIOGiftPurchaseBean implements Serializable {
    private  String liveType;
    private String platform;
    private String membershipType;
    private String orderID;
    private String giftId;
    private String giftPrice;
    private String giftName;
    private String hostId;
    private String hostName;
    private String timeFromeStart;

    public GIOGiftPurchaseBean(String orderID, String giftID, String giftPrice, String giftName, String hostID, String hostName, String timeFromStart,String liveType) {
        this.orderID = orderID;
        this.giftId = giftID;
        this.giftPrice = giftPrice;
        this.giftName = giftName;
        this.hostId = hostID;
        this.hostName = hostName;
        this.timeFromeStart = timeFromStart;
        this.liveType = liveType;
    }

    public GIOGiftPurchaseBean(String orderID, String membershipType, String platform) {
        this.orderID = orderID;
        this.membershipType = membershipType;
        this.platform = platform;
    }

    public GIOGiftPurchaseBean(String orderID, String membershipType) {
        this.orderID = orderID;
        this.membershipType = membershipType;
    }

    public GIOGiftPurchaseBean() {
    }
}
