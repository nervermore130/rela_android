package com.thel.growingio;

import java.io.Serializable;

/**
 * Created by waiarl on 2018/3/23.
 */

public class GIOShareBean implements Serializable {
    public String shareType = "";

    public String shareEntry = "";

    public String shareMaterialType = "";

    public GIOShareBean(String shareType, String shareEntry, String shareMaterialType) {
        this.shareType = shareType;
        this.shareEntry = shareEntry;
        this.shareMaterialType = shareMaterialType;
    }

    public GIOShareBean() {
    }

    public void setProperty(String shareType, String shareEntry, String shareMaterialType) {
        this.shareType = shareType;
        this.shareEntry = shareEntry;
        this.shareMaterialType = shareMaterialType;
    }
}
