package com.thel;

import java.io.Serializable;

public class ContactBeanNew implements Serializable {

    /**
     * 用户id
     */
    public int userId = 0;

    /**
     * 用户名
     */
    public String nickName = "";

    /**
     * 头像
     */
    public String avatar = "";

    // 是否被选中
    public boolean isSelected = false;

}
