package com.thel.data.local;

import android.os.Environment;

import com.thel.app.TheLApp;
import com.thel.data.IFileHelper;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;

/**
 * @author liuyun
 * @date 2017/10/31
 */

public class FileHelper implements IFileHelper {

    private static final String EMOJI_DIR = "/emoji";

    private static final String THEL_DIR = "/thel";
    
    private static final String LIVE_PK_LOG_DIR = "/live_pk_log";

    private static FileHelper INSTANCE;

    public static FileHelper getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new FileHelper();
        }
        return INSTANCE;
    }


    @Override
    public int writeTxtToFile(String result, String path, String fileName) {
        String strContent = result;
        try {
            File file = new File(path + fileName);
            if (file.exists()) {
                file.delete();
            }

            file.createNewFile();

            RandomAccessFile raf = new RandomAccessFile(file, "rw");
            raf.seek(0);
            raf.write(strContent.getBytes());
            raf.close();
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
        return 1;

    }


    @Override
    public String readerFileToString(String filePath) {
        return getTxt(filePath);

    }


    @Override
    public void deleteFile(String filePath) {
        File file = new File(filePath);
        file.delete();
    }


    @Override
    public void createFile(String filePath) {
        File file = new File(filePath);
        if (file.isDirectory()) {
            file.mkdir();
        } else {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public String getEmojiDir() {
        String emojiDir = getThelPath() + EMOJI_DIR;
        return mkdir(emojiDir);
    }


    @Override
    public String getLiveLogPkDir() {
        final String livePkLogDir = getThelPath() + LIVE_PK_LOG_DIR;
        return mkdir(livePkLogDir);
    }


    @Override
    public String getCameraDir() {
        String cameraDir = getRootPath() + File.separator + Environment.DIRECTORY_DCIM + File.separator + "Camera/" + File.separator;
        File file = new File(cameraDir);
        if (!file.exists()) {
            file.mkdirs();
        }
        return cameraDir;
    }

    /**
     * 拿到thel的文件地址
     *
     * @return
     */
    public String getThelPath() {

        String thelDir = getRootPath() + THEL_DIR;

        return mkdir(thelDir);
    }


    /**
     * 获取跟目录
     *
     * @return
     */
    private String getRootPath() {

        String rootDir;

        boolean sdCardExist = Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);

        if (sdCardExist) {
            rootDir = TheLApp.getContext().getExternalFilesDir(null).getAbsolutePath();//获取跟目录
        } else {
            rootDir = TheLApp.getContext().getCacheDir().getAbsolutePath();//获取cache地址
        }
        return rootDir;
    }

    /**
     * 创建文件夹
     *
     * @param path
     * @return
     */

    private String mkdir(String path) {

        File file = new File(path);

        if (!file.exists()) {
            file.mkdirs();
        }
        return file.getAbsolutePath();
    }

    /**
     * 得到json数据
     *
     * @param filePath
     * @return
     */
    private String getTxt(String filePath) {
        try {

            File file = new File(filePath);

            if(file.exists()){

                InputStream inputStream = new FileInputStream(new File(filePath));

                return inputStream2String(inputStream);
            }else{
                return null;
            }

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * inputStream 转字符串
     *
     * @param inputStream
     * @return
     */
    private String inputStream2String(InputStream inputStream) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int line;
        String result = null;
        try {
            while ((line = inputStream.read()) != -1) {
                baos.write(line);
            }
            result = baos.toString();
            baos.close();
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 删除单个文件
     **/
    private boolean deletefile(String fileName) {
        File file = new File(fileName);
        //如果文件路径所对应的文件存在，并且是一个文件，则直接删除
        if (file.exists() && file.isFile()) {
            return file.delete();
        } else {
            return false;
        }
    }

}
