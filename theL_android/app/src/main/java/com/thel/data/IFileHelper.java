package com.thel.data;

/**
 * Created by liuyun on 2017/12/26.
 */

public interface IFileHelper {

    /**
     * 写入数据
     *
     * @param result
     * @param path
     * @param fileName
     * @return int
     */
    int writeTxtToFile(String result, String path, String fileName);

    /**
     * 读取数据
     *
     * @param filePath
     * @return String
     */
    String readerFileToString(String filePath);

    /**
     * 删除文件
     *
     * @param filePath
     */
    void deleteFile(String filePath);

    /**
     * 创建文件
     *
     * @param filePath
     */
    void createFile(String filePath);

    /**
     * 获取直播pK log的位置
     *
     * @return String
     */
    String getLiveLogPkDir();

    /**
     * 拿到emoji的文件地址
     *
     * @return String
     */
    String getEmojiDir();

    /**
     * 获取相册的地址
     *
     * @return String
     */
    String getCameraDir();
}
