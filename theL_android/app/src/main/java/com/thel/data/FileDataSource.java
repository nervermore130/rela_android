package com.thel.data;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by liuyun on 2017/10/31.
 */

public interface FileDataSource<D> {

    void deleteFile(String path);

    void createFile(String path);

    Flowable<D> getData(String path, Class<D> zlass);

    int saveFile(String result, String path, String fileName);

    Flowable<List<D>> getDataList(String path, Class<D> zlass);

}
