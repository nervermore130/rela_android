package com.thel.data.local;

import android.text.TextUtils;

import com.google.gson.reflect.TypeToken;
import com.thel.data.FileDataSource;
import com.thel.utils.GsonUtils;

import java.lang.reflect.Type;
import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by liuyun on 2017/10/31.
 */

public class FileLocalDataSource<D> implements FileDataSource<D> {

    private static FileLocalDataSource INSTANCE;

    public static FileLocalDataSource getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new FileLocalDataSource();
        }
        return INSTANCE;
    }

    @Override
    public void deleteFile(String path) {
        FileHelper.getInstance().deleteFile(path);
    }

    @Override
    public void createFile(String path) {

    }

    @Override
    public Flowable<D> getData(String path, Class<D> zlass) {
        String json = FileHelper.getInstance().readerFileToString(path);
        return Flowable.just(GsonUtils.getObject(json, zlass));
    }

    @Override
    public int saveFile(String result, String path, String fileName) {
        int resul = FileHelper.getInstance().writeTxtToFile(result, path, fileName);

        return resul;
    }

    @Override
    public Flowable<List<D>> getDataList(String path, Class<D> zlass) {
        String json = FileHelper.getInstance().readerFileToString(path);
        if (TextUtils.isEmpty(json)) {
            json = "[]";
        }
        return Flowable.just(GsonUtils.getObjects(json, zlass));
    }

}
