package com.thel.manager;

import android.app.Activity;

import com.thel.utils.L;

import java.util.ArrayList;
import java.util.List;

public class ActivityManager {

    private List<Activity> mActivityList = new ArrayList<>();

    private static final ActivityManager ourInstance = new ActivityManager();

    public static ActivityManager getInstance() {
        return ourInstance;
    }

    private ActivityManager() {

    }

    public void addActivity(Activity activity) {
        if (mActivityList != null && activity != null) {
            if (activity.getClass().getSimpleName().equals("MainActivity")) {
                mActivityList.clear();
            }
            mActivityList.add(activity);

            L.d("ActivityManager", " addActivity activity stack : " + activity.getClass().getSimpleName());

        }

    }

    public void removeActivity(Activity activity) {
        if (mActivityList != null && activity != null) {
            mActivityList.remove(activity);
        }
    }

    public Activity getActivity(int position) {

        L.d("ActivityManager", " getActivity position : " + position);

        if (position >= 0 && mActivityList != null && mActivityList.size() > position) {
            return mActivityList.get(position);
        }

        return null;

    }

    public Activity getPreActivity() {

        L.d("ActivityManager", " getPreActivity mActivityList size : " + mActivityList.size());

        int position = mActivityList.size() - 1;

        L.d("ActivityManager", " getPreActivity position : " + position);

        if (position >= 0) {
            return getActivity(position);
        }

        return null;

    }

    public List<Activity> getActivitys() {
        return mActivityList;
    }

    public String getActivitysStr() {

        StringBuffer stringBuffer = new StringBuffer();

        for (int i = 0; i < mActivityList.size(); i++) {
            Activity activity = mActivityList.get(i);
            if (i == 0) {
                stringBuffer.append("[");
            }
            stringBuffer.append(" name:").append(activity.getClass().getSimpleName());

            if (i == mActivityList.size() - 1) {
                stringBuffer.append("]");

            }
        }

        return stringBuffer.toString();
    }

    public int getStackActivityCount() {
        if (mActivityList != null) {
            L.d("ActivityManager", "getStackActivityCount: " + mActivityList.size());
            return mActivityList.size();
        }
        return 0;
    }

    public void exit() {
        if (mActivityList == null) {
            return;
        }

        for (Activity activity : mActivityList) {
            L.d("ActivityManager", " activity getSimpleName : " + activity.getClass().getSimpleName());

            activity.finish();
        }
        mActivityList.clear();
    }

    public void finishToRoot() {

    }
}
