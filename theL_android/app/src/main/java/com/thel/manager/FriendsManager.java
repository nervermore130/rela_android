package com.thel.manager;

/**
 * Created by liuyun on 2018/3/23.
 */

public class FriendsManager {

    private static final FriendsManager ourInstance = new FriendsManager();

    public static FriendsManager getInstance() {
        return ourInstance;
    }

    private FriendsManager() {

    }
}
