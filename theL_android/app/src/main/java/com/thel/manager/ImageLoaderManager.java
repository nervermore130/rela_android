package com.thel.manager;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.ImageViewTarget;
import com.thel.ui.transfrom.GlideCircleTransform;
import com.thel.ui.transfrom.GlideRoundCornerTransform;
import com.thel.utils.CropCircelBorderTransformation;
import com.thel.utils.ImageUtils;
import com.thel.utils.L;

/**
 * @author yun.liu@avazu.net
 * @date 2016/5/20
 */
public class ImageLoaderManager {

    public static void imageLoader(ImageView imageView, String url) {
        if (imageView == null || imageView.getContext() == null) {
            return;
        }
        if (isValidContextForGlide(imageView.getContext())) {

            if (!TextUtils.isEmpty(url)) {

                Glide.with(imageView).load(url).apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL)).into(imageView);
            }
        }
    }

    public static void imageLoader(ImageView imageView, int defaultImage, String url) {
        if (imageView == null || imageView.getContext() == null) {
            return;
        }
        if (isValidContextForGlide(imageView.getContext())) {

            if (url != null) {

                RequestOptions requestOptions = new RequestOptions().placeholder(defaultImage).error(defaultImage).diskCacheStrategy(DiskCacheStrategy.ALL);

                Glide.with(imageView).load(url).apply(requestOptions).into(imageView);
            } else {
                imageLoader(imageView, defaultImage);
            }
        }
    }

    public static void imageLoader(ImageView imageView, int defaultImage, Uri uri) {

        if (imageView == null || imageView.getContext() == null) {
            return;
        }
        if (isValidContextForGlide(imageView.getContext())) {

            RequestOptions requestOptions = new RequestOptions().placeholder(defaultImage).error(defaultImage).diskCacheStrategy(DiskCacheStrategy.ALL);

            Glide.with(imageView).load(uri).apply(requestOptions).into(imageView);
        }
    }

    public static void imageLoaderCropCircleBorder(ImageView imageView, String url) {

        if (imageView == null || imageView.getContext() == null) {
            return;
        }
        if (isValidContextForGlide(imageView.getContext())) {

            RequestOptions requestOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).transform(new CropCircelBorderTransformation());

            Glide.with(imageView).load(url).apply(requestOptions).into(imageView);
        }
    }

    public static void imageLoader(ImageView imageView, Uri uri) {

        if (imageView == null || imageView.getContext() == null) {
            return;
        }
        if (isValidContextForGlide(imageView.getContext())) {

            RequestOptions requestOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL);

            Glide.with(imageView).load(uri).apply(requestOptions).into(imageView);
        }
    }

    public static void imageLoader(ImageView imageView, int drawable) {

        if (imageView == null || imageView.getContext() == null) {
            return;
        }
        if (isValidContextForGlide(imageView.getContext())) {

            try {

                RequestOptions requestOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).error(drawable);

                Glide.with(imageView).load(drawable).apply(requestOptions).into(imageView);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
    }

    public static void imageLoaderDefault(ImageView imageView, int defaultImage, String url) {

        if (imageView != null && url != null && imageView.getContext() != null) {
            if (isValidContextForGlide(imageView.getContext())) {

                RequestOptions requestOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(defaultImage).error(defaultImage);

                Glide.with(imageView).load(url).apply(requestOptions).into(imageView);
            }
        }
    }

    public static void imageLoaderGifLoop(ImageView imageView, String fileName) {
        if (fileName != null && imageView != null && imageView.getContext() != null) {
            if (isValidContextForGlide(imageView.getContext())) {

                String url = "file:///android_asset/" + fileName;
                RequestBuilder<Drawable> requestBuilder = Glide.with(imageView)
                        .load(url);

                requestBuilder.load(url).into(imageView);
            }
        }
    }

    public static void imageLoaderGifAssents(ImageView imageView, String fileName) {
        if (fileName != null && imageView != null && imageView.getContext() != null) {
            if (isValidContextForGlide(imageView.getContext())) {

                String url = "file:///android_asset/" + fileName;

                RequestBuilder<GifDrawable> request = Glide.with(imageView).asGif();
                request.load(url).into(new ImageViewTarget<GifDrawable>(imageView) {
                    @Override protected void setResource(@Nullable GifDrawable resource) {
                        if (resource != null) {
                            resource.setLoopCount(1);
                        }
                        view.setImageDrawable(resource);
                    }
                });
            }
        }
    }

    public static void imageLoaderAssents(ImageView imageView, String foldPath, String fileName) {

        if (fileName != null && imageView != null && imageView.getContext() != null) {
            if (isValidContextForGlide(imageView.getContext())) {

                String url = "file:///android_asset/" + foldPath + "/" + fileName;
                Glide.with(imageView).load(url).diskCacheStrategy(DiskCacheStrategy.ALL).into(imageView);
            }
        }
    }

    public static void imageLoaderCircle(ImageView imageView, int defaultImage, String url) {

        if (imageView == null || imageView.getContext() == null) {
            return;
        }
        if (isValidContextForGlide(imageView.getContext())) {

            if (url != null) {
                Glide.with(imageView).load(url).placeholder(defaultImage).diskCacheStrategy(DiskCacheStrategy.ALL).error(defaultImage).dontAnimate().transform(new GlideCircleTransform()).into(imageView);
            } else {
                imageLoader(imageView, defaultImage);
            }
        }
    }

    public static void imageLoaderCircle(ImageView imageView, String url) {

        if (imageView == null || imageView.getContext() == null) {
            return;
        }
        if (isValidContextForGlide(imageView.getContext())) {

            if (url != null) {
                Glide.with(imageView).load(url).diskCacheStrategy(DiskCacheStrategy.ALL).transform(new GlideCircleTransform()).into(imageView);
            }
        }
    }

    public static void imageLoaderDefault(ImageView imageView, int defaultImage, String url, int width, int height) {

        if (imageView == null || imageView.getContext() == null) {
            return;
        }
        if (isValidContextForGlide(imageView.getContext())) {

            final Uri uri = getImageUri(url, width, height);
            Glide.with(imageView).load(uri).placeholder(defaultImage).centerCrop().dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL).skipMemoryCache(true).into(imageView);
        }

    }

    public static void imageLoaderDefaultCorner(ImageView imageView, int defaultImage, String url, int width, int height, float radius) {

        if (imageView == null || imageView.getContext() == null) {
            return;
        }
        if (isValidContextForGlide(imageView.getContext())) {

            final Uri uri = getImageUri(url, width, height);
            Glide.with(imageView).load(uri).placeholder(defaultImage).dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL).skipMemoryCache(true).transform(new GlideRoundCornerTransform(radius)).into(imageView);
        }
    }

    public static void imageLoaderDefaultCorner(ImageView imageView, int defaultImage, String url, float radius) {

        if (imageView == null || imageView.getContext() == null) {
            return;
        }

        if (isValidContextForGlide(imageView.getContext())) {
            Glide.with(imageView).load(url).placeholder(defaultImage).dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL).skipMemoryCache(true).transform(new CenterCrop(), new GlideRoundCornerTransform(radius)).into(imageView);
        }

    }

    public static void imageLoaderDefaultCircle(ImageView imageView, int defaultImage, String url, int width, int height) {
        if (imageView == null || imageView.getContext() == null) {
            return;
        }
        if (isValidContextForGlide(imageView.getContext())) {
            final Uri uri = getImageUri(url, width, height);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            Glide.with(imageView).load(uri).placeholder(defaultImage).dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL).skipMemoryCache(true).transform(new GlideCircleTransform()).into(imageView);
        }
    }

    public static void imageLoaderDefaultCircle(ImageView imageView, int defaultImage, String url) {
        if (imageView == null || imageView.getContext() == null) {
            return;
        }
        if (isValidContextForGlide(imageView.getContext())) {
            Glide.with(imageView).load(url).placeholder(defaultImage).dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL).skipMemoryCache(true).transform(new GlideCircleTransform()).into(imageView);
        }

    }

    public static void imageLoader(ImageView imageView, String url, int width, int height) {

        if (imageView == null || imageView.getContext() == null) {
            return;
        }

        if (isValidContextForGlide(imageView.getContext())) {
            Glide.with(imageView).load(getImageUri(url, width, height)).centerCrop().dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL).into(imageView);
        }

    }

    public static Uri getImageUri(String url, int width, int height) {
        final Uri uri = Uri.parse(ImageUtils.buildNetPictureUrl(url, width, height));
        return uri;
    }

    public static String getImageUrl(String url, int width, int height) {
        return ImageUtils.buildNetPictureUrl(url, width, height);
    }

    protected static Uri getImageUri(String url) {
        final Uri uri = Uri.parse(ImageUtils.buildNetPictureUrl(url));
        L.i("uri", uri.toString());
        return uri;
    }

    public static boolean isValidContextForGlide(final Context context) {
        if (context == null) {
            return false;
        }
        if (context instanceof Activity) {
            final Activity activity = (Activity) context;
            return !activity.isDestroyed() && !activity.isFinishing();
        }
        return true;
    }

}
