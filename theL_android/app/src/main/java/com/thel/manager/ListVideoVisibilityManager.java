package com.thel.manager;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;

import com.thel.R;
import com.thel.bean.moments.MomentsBean;
import com.thel.constants.MomentTypeConstants;
import com.thel.player.video.VideoInfoBuilder;
import com.thel.player.video.VideoPlayerProxy;
import com.thel.ui.adapter.MomentsAdapter;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.utils.VideoUtils;

/**
 * @author liuyun
 * @date 2018/3/5
 */

public class ListVideoVisibilityManager {

    private static ListVideoVisibilityManager mListVideoVisibilityManager;

    private static final int AUTO_PLAY_VIDEO_PERCENT = 50;

    private View currentView = null;

    private MomentsBean momentsBean = null;

    private ListVideoVisibilityManager() {

    }

    public static ListVideoVisibilityManager getInstance() {
        if (mListVideoVisibilityManager == null) {
            mListVideoVisibilityManager = new ListVideoVisibilityManager();
        }
        return mListVideoVisibilityManager;
    }

    public <LM extends RecyclerView.LayoutManager, T extends BaseRecyclerViewAdapter> void calculatorItemPercent(LM layoutManager, int scrollStatus, T adapter, String entry) {

        switch (scrollStatus) {
            case RecyclerView.SCROLL_STATE_DRAGGING:

                break;
            case RecyclerView.SCROLL_STATE_SETTLING:

                break;
            case RecyclerView.SCROLL_STATE_IDLE:

                MomentsAdapter momentsAdapter = (MomentsAdapter) adapter;

                LinearLayoutManager mLinearLayoutManager = (LinearLayoutManager) layoutManager;

                int firstVisibleItemPosition = mLinearLayoutManager.findFirstVisibleItemPosition();

                final int lastVisibleItemPosition = mLinearLayoutManager.findLastVisibleItemPosition();

                final int headCount = momentsAdapter.getHeaderLayoutCount();
                if (firstVisibleItemPosition < 0) {
                    firstVisibleItemPosition = 0;
                }
                for (int i = firstVisibleItemPosition; i <= lastVisibleItemPosition; i++) {

                    if (i >= momentsAdapter.getData().size()) {
                        return;
                    }

                    if (i - headCount < 0) {
                        continue;
                    }

                    final View itemView = mLinearLayoutManager.findViewByPosition(i);

                    MomentsBean momentsBean = momentsAdapter.getItem(i - headCount);

                    int percents = momentsBean.getVisibilityPercents(itemView);

                    if (percents > AUTO_PLAY_VIDEO_PERCENT) {

                        currentView = itemView;

                        if (VideoUtils.willAutoPlayVideo()) {

                            autoPlayVideo(i, currentView, momentsAdapter, entry);

                            return;

                        }

                    }

                }
                break;
            default:

                break;
        }

    }

    private void autoPlayVideo(int position, View view, MomentsAdapter mAdapter, String entry) {

        try {

            if (mAdapter != null) {

                int currentPosition = position - mAdapter.getHeaderLayoutCount();


                if (currentPosition >= 0) {

                    momentsBean = mAdapter.getItem(currentPosition);

                    if (momentsBean == null || momentsBean.videoUrl == null) {
                        return;
                    }

                    boolean isVideoType = momentsBean.momentsType.equals(MomentTypeConstants.MOMENT_TYPE_VIDEO) ||
                            (momentsBean.momentsType.equals(MomentTypeConstants.MOMENT_TYPE_THEME_REPLY) &&
                                    momentsBean.themeReplyClass.equals(MomentTypeConstants.MOMENT_TYPE_VIDEO));

                    if (isVideoType) {

                        boolean isOldVideo = VideoPlayerProxy.getInstance().isPlaying() && VideoPlayerProxy.getInstance().getUrl().equals(momentsBean.videoUrl);

                        if (isOldVideo) {
                            return;
                        }

                        FrameLayout videoContainer = view.findViewById(R.id.video_container);

                        VideoInfoBuilder builder = new VideoInfoBuilder.Builder()
                                .setMomentId(momentsBean.momentsId)
                                .setVideoContainer(videoContainer)
                                .setUrl(momentsBean.videoUrl)
                                .setVolume(0)
                                .setPosition(position)
                                .setEntry(entry)
                                .build();

                        VideoPlayerProxy.getInstance().play(builder);

                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
