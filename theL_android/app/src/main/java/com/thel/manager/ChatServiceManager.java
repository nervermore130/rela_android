package com.thel.manager;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;

import androidx.annotation.NonNull;

import com.thel.IChatAidlInterface;
import com.thel.IMsgStatusCallback;
import com.thel.app.TheLApp;
import com.thel.bean.FriendListBean;
import com.thel.callback.IMainProcessCallback;
import com.thel.db.DBConstant;
import com.thel.db.DBUtils;
import com.thel.db.table.message.MsgTable;
import com.thel.db.table.message.UserInfoTable;
import com.thel.db.table.message.WinkMsgTable;
import com.thel.modules.main.messages.bean.MsgBean;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestConstants;
import com.thel.network.service.DefaultRequestService;
import com.thel.service.chat.ChatService;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.UserUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;

/**
 * @author liuyun
 * @date 2018/3/19
 */

public class ChatServiceManager {

    private final static String TAG = "ChatServiceManager";

    private static ChatServiceManager instance;

    private MsgChatServiceConnection mMsgChatServiceConnection;

    private IChatAidlInterface mIChatAidlInterface;

    private boolean isConnectService = false;

    private boolean isStartConnectService = false;

    private Map<String, IMsgStatusCallback> iMsgStatusCallbackList = new HashMap<>();

    private ChatServiceManager() {

    }

    /**
     * 设置当前处于哪个界面
     *
     * @param userId
     */
    public void setUserId(String userId) {

        L.d(TAG, " setUserId : " + userId);

        L.d(TAG, " setUserId mIChatAidlInterface : " + mIChatAidlInterface);

        if (mIChatAidlInterface != null) {
            try {
                mIChatAidlInterface.setUserId(userId);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

    }

    public static ChatServiceManager getInstance() {

        if (instance == null) {

            instance = new ChatServiceManager();
        }

        return instance;

    }

    /**
     * 开启聊天进程
     */
    public void startService(@NonNull Context context) {
        L.d(TAG, " startService : ");
        try {
            if (!isStartConnectService) {
                mMsgChatServiceConnection = new MsgChatServiceConnection();
                context.bindService(new Intent(context, ChatService.class), mMsgChatServiceConnection, Context.BIND_AUTO_CREATE);
                isStartConnectService = true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 关闭聊天进程
     */
    public void stopService(@NonNull Context context) {
        try {

            isStartConnectService = false;

            if (mMsgChatServiceConnection != null && isConnectService) {

                context.unbindService(mMsgChatServiceConnection);

                isConnectService = false;

                mIChatAidlInterface = null;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 聊天进程被系统杀掉
     */
    public void serviceKilledBySystem() {
        isConnectService = false;
        isStartConnectService = false;
    }

    /**
     * 推送聊天状态
     *
     * @param connectStatus
     */
    public void pushStatus(int connectStatus) {

        if (isConnectService && mIChatAidlInterface != null) {
            try {
                mIChatAidlInterface.pushStatus(connectStatus);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public void pushDeleteMsgTable(String userId) {
        if (isConnectService && mIChatAidlInterface != null && userId != null) {
            try {
                mIChatAidlInterface.pushDeleteMsgTable(userId);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public void pushUpdateMsg(MsgTable msgTable) {

        L.d(TAG, " pushUpdateMsg mIChatAidlInterface : " + mIChatAidlInterface);

        if (isConnectService && mIChatAidlInterface != null && msgTable != null) {
            try {
                mIChatAidlInterface.pushUpdateMsg(msgTable);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * 注册聊天监听
     *
     * @param iMsgStatusCallback
     */
    public void registerCallback(String key, IMsgStatusCallback iMsgStatusCallback) {

        L.d(TAG, " isConnectService : " + isConnectService);

        L.d(TAG, " mIChatAidlInterface : " + mIChatAidlInterface);

        if (isConnectService && mIChatAidlInterface != null) {
            try {
                if (iMsgStatusCallbackList != null) {
                    iMsgStatusCallbackList.clear();
                }
                mIChatAidlInterface.registerCallBack(key, iMsgStatusCallback);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        } else {
            if (iMsgStatusCallbackList != null) {
                iMsgStatusCallbackList.put(key, iMsgStatusCallback);
            }
        }
    }

    /**
     * 移除聊天监听
     *
     * @param key
     */
    public void unRegisterCallback(String key) {


        if (isConnectService && mIChatAidlInterface != null) {
            try {
                mIChatAidlInterface.unregisterCallBack(key);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 移除所有聊天监听
     */
    public void clearCallback() {
        if (isConnectService && mIChatAidlInterface != null) {
            try {
                mIChatAidlInterface.clearCallback();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public List<String> getUserList() {
        if (isConnectService && mIChatAidlInterface != null) {
            try {
                return mIChatAidlInterface.getUserList();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    public void sendMsg(MsgBean msgBean) {

        L.d(TAG, " requestMsg  mIChatAidlInterface : " + mIChatAidlInterface);

        if (isConnectService && mIChatAidlInterface != null) {
            try {
                mIChatAidlInterface.sendMsg(msgBean);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public void sendReceiptMsg(MsgBean msgBean) {

        if (isConnectService && mIChatAidlInterface != null) {
            try {
                mIChatAidlInterface.sendReceiptMsg(msgBean);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public void sendPendingMsg(String toUserId, String content) {

        if (isConnectService && mIChatAidlInterface != null) {
            try {
                mIChatAidlInterface.sendPendingMsg(toUserId, content);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

    }

    public void pushTransferPercent(int percent) {
        if (isConnectService && mIChatAidlInterface != null) {
            try {
                mIChatAidlInterface.onTransferPercents(percent);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public int getConnectStatus() {

        L.d(TAG, " getConnectStatus  mIChatAidlInterface : " + mIChatAidlInterface);

        if (isConnectService && mIChatAidlInterface != null) {
            try {
                return mIChatAidlInterface.getConnectStatus();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return 0x01;
    }

    public void setUserList(List<String> userList) {
        if (isConnectService && mIChatAidlInterface != null) {
            try {
                mIChatAidlInterface.setUserList(userList);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public void setMyUserId(String myUserId) {
        if (isConnectService && mIChatAidlInterface != null) {
            try {
                mIChatAidlInterface.setMyUserId(myUserId);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public void setKey(String key) {
        if (isConnectService && mIChatAidlInterface != null) {
            try {
                mIChatAidlInterface.setKey(key);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public void checkFriendsRelationship(String userId, MsgBean msgBean) {
        if (isConnectService && mIChatAidlInterface != null) {
            try {
                mIChatAidlInterface.checkFriendsRelationship(userId, msgBean);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    private class MsgChatServiceConnection implements ServiceConnection {

        @Override public void onServiceConnected(ComponentName componentName, IBinder iBinder) {

            mIChatAidlInterface = IChatAidlInterface.Stub.asInterface(iBinder);

            isConnectService = true;

            final Flowable<FriendListBean> flowable = DefaultRequestService.createCommonRequestService().getNetFriendList(RequestConstants.RELATION_TYPE_FRIEND);
            flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).subscribe(new InterceptorSubscribe<FriendListBean>() {

                @Override
                public void onNext(FriendListBean friendListBean) {

                    super.onNext(friendListBean);

                    initChat(friendListBean);

                }

                @Override public void onError(Throwable t) {
                    super.onError(t);

                    initChat(null);

                }
            });

            L.d(TAG, " MsgChatServiceConnection onServiceConnected ");
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            isConnectService = false;
            isStartConnectService = false;
            pushStatus(ChatService.ConnectStatus.STOP);
            try {

                if (mIChatAidlInterface != null) {
                    mIChatAidlInterface.unregisterMainProcessCallback();
                }

                if (TheLApp.getContext() != null && !"".equals(UserUtils.getMyUserId())) {
                    startService(TheLApp.getContext());
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            L.d(TAG, " MsgChatServiceConnection onServiceDisconnected ");

        }
    }

    private void initChat(FriendListBean friendListBean) {

        try {
            if (iMsgStatusCallbackList != null && iMsgStatusCallbackList.size() > 0) {

                for (Map.Entry<String, IMsgStatusCallback> entry : iMsgStatusCallbackList.entrySet()) {

                    IMsgStatusCallback iMsgStatusCallback = entry.getValue();

                    String key = entry.getKey();

                    registerCallback(key, iMsgStatusCallback);

                }
            }

            UserInfoTable userInfoTable = ShareFileUtils.getUserInfoTable();

            mIChatAidlInterface.registerMainProcessCallback(new IMainProcessCallback());
            mIChatAidlInterface.setKey(UserUtils.getUserKey());
            mIChatAidlInterface.setMyUserId(UserUtils.getMyUserId());
            mIChatAidlInterface.insertUserInfo(userInfoTable);
            if (friendListBean != null && friendListBean.data != null && friendListBean.data.list != null) {
                mIChatAidlInterface.setUserList(friendListBean.data.list);
            }
            mIChatAidlInterface.init();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public List<MsgTable> getStrangerMsg() {
        if (mIChatAidlInterface != null) {
            try {
                return mIChatAidlInterface.getStrangerMsg();

            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public void insertMsgTable(MsgTable msgTable) {
        if (mIChatAidlInterface != null) {
            try {
                mIChatAidlInterface.insertMsgTable(msgTable);

            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public void insertNewMsg(MsgBean msgBean, String tableName, int hasRead) {
        if (mIChatAidlInterface != null) {
            try {
                mIChatAidlInterface.insertNewMsg(msgBean, tableName, hasRead);

            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public void insertChatTable(MsgBean msgBean, String tableName) {

        if (mIChatAidlInterface != null) {
            try {
                mIChatAidlInterface.insertChatTable(msgBean, tableName);

            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public MsgTable getMsgTableByUserId(String userId) {

        if (mIChatAidlInterface != null) {
            try {
                return mIChatAidlInterface.getMsgTableByUserId(userId);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    public List<MsgBean> findMsgsByUserIdAndPage(String toUserId, String limit, String offset) {

        if (mIChatAidlInterface != null) {
            try {
                return mIChatAidlInterface.findMsgsByUserIdAndPage(toUserId, limit, offset);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    public List<MsgBean> getMsgListByTableName(String tableName) {

        if (mIChatAidlInterface != null) {
            try {
                return mIChatAidlInterface.getMsgListByTableName(tableName);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    public List<MsgBean> getMsgListByTableNameAsync(String tableName) {

        if (mIChatAidlInterface != null) {
            try {

                return mIChatAidlInterface.getMsgListByTableNameAsync(tableName);

            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    public MsgBean getMsgByUserIdAndPacketId(String userId, String packetId) {

        if (mIChatAidlInterface != null) {
            try {
                mIChatAidlInterface.getMsgByUserIdAndPacketId(userId, packetId);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    public void updateMsgTableWithWink(MsgTable msgTable) {

        if (mIChatAidlInterface != null) {
            try {
                mIChatAidlInterface.updateMsgTableWithWink(msgTable);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

    }

    public void updateMsgUnread(MsgTable msgTable) {

        if (mIChatAidlInterface != null) {
            try {
                mIChatAidlInterface.updateMsgUnread(msgTable);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

    }

    public long deleteMsgById(String tableName, String packetId) {

        if (mIChatAidlInterface != null) {
            try {
                mIChatAidlInterface.deleteMsgById(tableName, packetId);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        return -1;
    }

    public MsgBean getLastMsg(String tableName) {

        if (mIChatAidlInterface != null) {
            try {
                return mIChatAidlInterface.getLastMsg(tableName);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        return null;

    }

    public MsgTable findMsgTableByOtherUserId(String otherUserId) {

        if (mIChatAidlInterface != null) {
            try {
                return mIChatAidlInterface.findMsgTableByOtherUserId(otherUserId);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    public List<MsgTable> getMsgList(int isStranger) {

        if (mIChatAidlInterface != null) {
            try {
                return mIChatAidlInterface.getMsgList(isStranger);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    public MsgTable insertRequestDataToMsgTable(MsgBean msgBean, int unReadCount) {
        if (mIChatAidlInterface != null) {
            try {
                mIChatAidlInterface.insertRequestDataToMsgTable(msgBean, unReadCount);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public int findRequestCountToMsgTable() {
        if (mIChatAidlInterface != null) {
            try {
                return mIChatAidlInterface.findRequestCountToMsgTable();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return -1;
    }

    public void changeRelationship(String userId, MsgBean msgBean, int isStranger) {
        if (mIChatAidlInterface != null) {
            try {
                mIChatAidlInterface.changeRelationship(userId, msgBean, isStranger);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public void deleteStranger() {
        if (mIChatAidlInterface != null) {
            try {
                mIChatAidlInterface.deleteStranger();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean isStranger(String userId) {
        if (mIChatAidlInterface != null) {
            try {
                return mIChatAidlInterface.isStranger(userId);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public long getLastMsgTime(String userId) {

        L.d(TAG, " getLastMsgTime mIChatAidlInterface : " + mIChatAidlInterface);

        if (mIChatAidlInterface != null) {
            try {
                return mIChatAidlInterface.getLastMsgTime(userId);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return -1;
    }

    public long stickTop(MsgTable msgTable) {
        if (mIChatAidlInterface != null) {
            try {
                return mIChatAidlInterface.stickTop(msgTable);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return -1;
    }

    public long resetStickTop(MsgTable msgTable) {
        if (mIChatAidlInterface != null) {
            try {
                return mIChatAidlInterface.resetStickTop(msgTable);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return -1;
    }

    public long getStickTopCount() {
        if (mIChatAidlInterface != null) {
            try {
                return mIChatAidlInterface.getStickTopCount();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return -1;
    }

    public void removeMsgTable(String userId) {
        if (mIChatAidlInterface != null) {
            try {
                mIChatAidlInterface.removeMsgTable(userId);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public void updateMsgTable(MsgBean msgBean) {
        if (mIChatAidlInterface != null) {
            try {
                mIChatAidlInterface.updateMsgTable(msgBean);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public void deleteMsgTable(String userId) {
        if (mIChatAidlInterface != null) {
            try {
                mIChatAidlInterface.deleteMsgTable(userId);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public UserInfoTable getUserInfo() {
        if (mIChatAidlInterface != null) {
            try {
                return mIChatAidlInterface.getUserInfo();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public String getMsgCursor() {
        if (mIChatAidlInterface != null) {
            try {
                mIChatAidlInterface.getMsgCursor();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return "0";
    }

    public void setMsgCursor(String cursor) {
        if (mIChatAidlInterface != null) {
            try {
                mIChatAidlInterface.setMsgCursor(cursor);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public void updateStrangerUnreadCount(int count, int isWinked) {
        if (mIChatAidlInterface != null) {
            try {
                mIChatAidlInterface.updateStrangerUnreadCount(count, isWinked);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public void winkMsgSaveToDB(MsgBean msgBean) {
        if (mIChatAidlInterface != null) {
            try {
                mIChatAidlInterface.winkMsgSaveToDB(msgBean);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public void insertMsg(MsgBean msgBean, String tableName) {
        if (mIChatAidlInterface != null) {
            try {
                mIChatAidlInterface.insertMsg(msgBean, tableName);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public void createTable(String tableName) {
        if (mIChatAidlInterface != null) {
            try {
                mIChatAidlInterface.createTable(tableName);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public void attachToOldDB(String path, String name) {
        if (mIChatAidlInterface != null) {
            try {
                mIChatAidlInterface.attachToOldDB(path, name);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public void detachToOldDB(String name) {
        if (mIChatAidlInterface != null) {
            try {
                mIChatAidlInterface.detachToOldDB(name);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public void tableTransfer(String newDBName, String newTableName, String oldDBName, String oldTableName) {
        if (mIChatAidlInterface != null) {
            try {
                mIChatAidlInterface.tableTransfer(newDBName, newTableName, oldDBName, oldTableName);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public void insertUserInfo(UserInfoTable userInfoTable) {
        if (mIChatAidlInterface != null) {
            try {
                mIChatAidlInterface.insertUserInfo(userInfoTable);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public void dropTable(String tableName) {
        if (mIChatAidlInterface != null) {
            try {
                mIChatAidlInterface.dropTable(tableName);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public long getStrangerWinkOrUnWinkCount(int isWinked) {

        if (mIChatAidlInterface != null) {
            try {
                return mIChatAidlInterface.getStrangerWinkOrUnWinkCount(isWinked);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        return 0;

    }

    public void deleteWinkMsg(WinkMsgTable winkMsgTable) {
        if (mIChatAidlInterface != null) {
            try {
                mIChatAidlInterface.deleteWinkMsg(winkMsgTable);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public void updateWinkMsg(WinkMsgTable winkMsgTable) {
        if (mIChatAidlInterface != null) {
            try {
                mIChatAidlInterface.updateWinkMsg(winkMsgTable);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public List<WinkMsgTable> getWinkList(int limit, int offset) {
        if (mIChatAidlInterface != null) {
            try {
                return mIChatAidlInterface.getWinkList(limit, offset);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public long getWinkMsgCount() {
        if (mIChatAidlInterface != null) {
            try {
                return mIChatAidlInterface.getWinkMsgCount();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    public List<MsgTable> getAllMsgTables() {
        if (mIChatAidlInterface != null) {
            try {
                return mIChatAidlInterface.getAllMsgTables();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public void insertWinkMsg(WinkMsgTable winkMsgTable) {
        if (mIChatAidlInterface != null) {
            try {
                mIChatAidlInterface.insertWinkMsg(winkMsgTable);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public void wink(MsgBean msgBean) {

        String userId = DBUtils.getMainProcessOtherUserId(msgBean);

        L.d("winkMsg", " userId : " + userId);

        if (userId != null) {

            MsgTable msgTable = getMsgTableByUserId(userId);

            L.d("winkMsg", " msgTable : " + msgTable);

            if (msgTable != null) {

                msgTable.isWinked = 0;

                int unreadCount = msgTable.unreadCount;

                if (unreadCount > 0) {
                    msgTable.unreadCount = unreadCount - 1;
                } else {
                    msgTable.unreadCount = 0;
                }

                updateMsgTableWithWink(msgTable);

                pushUpdateMsg(msgTable);

                L.d("winkMsg", " 1 : ");

            }

            MsgTable strangerMsgTable = getMsgTableByUserId(DBConstant.Message.STRANGER_TABLE_NAME);

            if (strangerMsgTable != null) {

                long count = getStrangerWinkOrUnWinkCount(1);

                L.d("winkMsg", " count : " + count);

                int unreadCount = strangerMsgTable.unreadCount;

                if (unreadCount > 0) {
                    strangerMsgTable.unreadCount = unreadCount - 1;
                } else {
                    strangerMsgTable.unreadCount = 0;
                }

                int isWinked;

                if (count > 0) {
                    isWinked = 1;
                } else {
                    isWinked = 0;
                }

                L.d("winkMsg", " isWinked : " + isWinked);

                strangerMsgTable.isWinked = isWinked;

                updateMsgTableWithWink(strangerMsgTable);

                pushUpdateMsg(strangerMsgTable);

            }

        }

    }

    public void clearWinksTable() {
        if (mIChatAidlInterface != null) {
            try {
                mIChatAidlInterface.clearWinksTable();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean isMsgCreated(MsgBean msgBean) {
        if (mIChatAidlInterface != null) {
            try {
                return mIChatAidlInterface.isMsgCreated(msgBean);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public int getUnreadMsgCount() {
        if (mIChatAidlInterface != null) {
            try {
                return mIChatAidlInterface.getUnreadMsgCount();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

}
