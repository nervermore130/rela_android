package com.thel.manager;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;

import com.thel.utils.L;

/**
 * Created by liuyun on 2017/9/15.
 */

public class MemoryManager {

    private static final String TAG = "MemoryManager";

    private static final int LOW_MEMORY = 20;

    private static final int M = 1000 * 1000;

    private static final MemoryManager ourInstance = new MemoryManager();

    public static MemoryManager getInstance() {
        return ourInstance;
    }

    private MemoryManager() {

    }

    public synchronized boolean isLowMemoryRunning(Activity activity) {

        ActivityManager activityManager = (ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE);

        int largeMemory = activityManager.getLargeMemoryClass();

        ActivityManager.MemoryInfo info = new ActivityManager.MemoryInfo();

        activityManager.getMemoryInfo(info);

        int availMem = (int) (info.availMem / M);

        return largeMemory - availMem <= LOW_MEMORY;
    }

    public void clearMemory() {

    }


}
