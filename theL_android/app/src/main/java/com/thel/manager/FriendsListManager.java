package com.thel.manager;

/**
 * Created by liuyun on 2017/11/13.
 */

public class FriendsListManager {
    private static final FriendsListManager ourInstance = new FriendsListManager();

    public static FriendsListManager getInstance() {
        return ourInstance;
    }

    private FriendsListManager() {

    }
}
