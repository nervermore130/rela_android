package com.thel.manager;

import androidx.annotation.NonNull;

import com.thel.BuildConfig;
import com.thel.app.TheLApp;
import com.thel.bean.AliIPsBean;
import com.thel.bean.BasicInfoBean;
import com.thel.bean.BasiduIPsBean;
import com.thel.bean.CDNSpeedBean;
import com.thel.bean.PiliIPsBean;
import com.thel.utils.ExecutorServiceUtils;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;
import com.thel.utils.PingUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.UserUtils;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.reactivex.Flowable;
import io.reactivex.Scheduler;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


/**
 * * Created by liuyun on 2018/7/19
 */

/**
 * 七牛ip调度文档 https://chushengtech.worktile.com/files/5b39da1f598be14999033f11/preview?from=attachment&version=1
 * <p>
 * 百度ip调度文档 https://chushengtech.worktile.com/files/5b39d765dd6009492926b82b/preview?from=attachment&version=1
 * <p>
 * 网宿ip调度文档 https://chushengtech.worktile.com/files/5b39d6f9598be14999033e58/preview?from=attachment&version=1
 */

public class CDNBalanceManager {

    private static final String TAG = "CDNBalanceManager";

    /**
     * 七牛推流域名
     */
    private static final String RTMP_PILI_URL = "pili-publish.rela.me";

    /**
     * 百度推流域名
     */
    private static final String RTMP_BAIDU_URL = "rtmp.push-baidu.rela.me";

    /**
     * 网宿推流域名
     */
    private static final String RTMP_WS_URL = "rtmp.push-ws.rela.me";

    /**
     * 阿里推流域名
     */
    private static final String RTMP_ALI_URL = "rtmp.push-ali.rela.me";

    /**
     * 七牛播放域名
     */
    private static final String PLAY_PILI_URL = "pili-live-hdl.rela.me";

    /**
     * 百度播放域名
     */
    private static final String PLAY_BAIDU_URL = "flv.live-baidu.rela.me";

    /**
     * 网宿播放域名
     */
    private static final String PLAY_WS_URL = "flv.live-ws.rela.me";

    /**
     * 阿里播放域名
     */
    private static final String PLAY_ALI_URL = "flv.live-ali.rela.me";

    /**
     * 多人连麦七牛播放域名
     */
    private static final String PLAY_PILI_MULTI_URL = "flv.pili-pull-baidu.rela.me";

    /**
     * 多人连麦百度播放域名
     */
    private static final String PLAY_BAIDU_MULTI_URL = "flv.live-baidu.rela.me";

    /**
     * 多人连麦网宿播放域名
     */
    private static final String PLAY_WS_MULTI_URL = "flv.ws-pull-baidu.rela.me";

    /**
     * 多人连麦阿里播放域名
     */
    private static final String PLAY_ALI_MULTI_URL = "flv.live-ali.rela.me";

    /**
     * 七牛获取ip的地址
     */
    private static final String PILI_IPS_URL = "http://pili-ipswitch.qiniuapi.com/v1/query/";

    /**
     * 百度获取ip的地址
     */
    private static final String BAIDU_IPS_URL = "http://180.76.76.112/v2/0016/";

    /**
     * 网宿获取ip的地址
     */
    private static final String WS_IPS_URL = "http://sdkoptedge.chinanetcenter.com";

    /**
     * 阿里获取ip的地址
     */
    private static final String ALI_IPS_URL = "http://httpdns.alicdn.com/dns_resolve_live";

    /**
     * ping的次数
     */
    private static final int PING_COUNT = 1;

    /**
     * 存放推流地址
     */
    private List<CDNSpeedBean> rtmpCDNList = new ArrayList<>();

    /**
     * 存放百度推流地址
     */
    private List<CDNSpeedBean> rtmpBaiduCDNList = new ArrayList<>();

    /**
     * 存放网宿推流地址
     */
    private List<CDNSpeedBean> rtmpWSCDNList = new ArrayList<>();

    /**
     * 存放七牛推流地址
     */
    private List<CDNSpeedBean> rtmpPiliCDNList = new ArrayList<>();

    /**
     * 存放阿里推流地址
     */
    private List<CDNSpeedBean> rtmpAliCDNList = new ArrayList<>();

    /**
     * 存放所有的播放地址
     */
    private List<CDNSpeedBean> playCDNList = new ArrayList<>();

    /**
     * 存放七牛播放地址
     */
    private List<CDNSpeedBean> piliCDNList = new ArrayList<>();

    /**
     * 存放百度播放地址
     */
    private List<CDNSpeedBean> baiduCDNList = new ArrayList<>();

    /**
     * 存放网宿播放地址
     */
    private List<CDNSpeedBean> wsCDNList = new ArrayList<>();

    /**
     * 存放阿里播放地址
     */
    private List<CDNSpeedBean> aliCDNList = new ArrayList<>();

    /**
     * 存放多人连麦七牛播放地址
     */
    private List<CDNSpeedBean> piliMultiCDNList = new ArrayList<>();

    /**
     * 存放多人连麦百度播放地址
     */
    private List<CDNSpeedBean> baiduMultiCDNList = new ArrayList<>();

    /**
     * 存放多人连麦网宿播放地址
     */
    private List<CDNSpeedBean> wsMultiCDNList = new ArrayList<>();

    /**
     * 存放多人连麦阿里播放地址
     */
    private List<CDNSpeedBean> aliMultiCDNList = new ArrayList<>();

    /**
     * 切换播放地址时的下标
     */
    private int index = 0;

    /**
     * 切换推流地址时的下标
     */
    private int rtmpIndex = 0;

    private String playIn = "";

    private String playOut = "";

    private List<BasicInfoBean.IpsInfoBean> flvInfos;

    private List<String> rtmpUrls;

    private List<BasicInfoBean.IpsInfoBean> multiFlvInfos;

    private int rtmpUrlSort = 0;

    private int liveUrlSort = 0;

    private OkHttpClient okHttpClient;

    private Scheduler pingSchedulers;

    private static final CDNBalanceManager ourInstance = new CDNBalanceManager();

    public static CDNBalanceManager getInstance() {
        return ourInstance;
    }

    private CDNBalanceManager() {
        pingSchedulers = Schedulers.from(ExecutorServiceUtils.getInstatnce().getFixedExecutorService());
    }

    /**
     * 初始化CDN数据
     *
     * @param rtmpUrls
     * @param flvInfos
     */
    public void initData(List<String> rtmpUrls, List<BasicInfoBean.IpsInfoBean> flvInfos, List<BasicInfoBean.IpsInfoBean> multiFlvInfos) {
        L.d(TAG, " initData rtmp size: " + rtmpUrls.size()+" flv size:"+flvInfos.size());
        this.rtmpUrls = rtmpUrls;

        this.flvInfos = flvInfos;

        this.multiFlvInfos = multiFlvInfos;

        rtmpUrlSort = ShareFileUtils.getInt(ShareFileUtils.RTMP_URL_SORT, 0);

        liveUrlSort = ShareFileUtils.getInt(ShareFileUtils.LIVE_URL_SORT, 0);

        initRtmpUrl(rtmpUrls);

        initPlayUrl(flvInfos);

    }

    /**
     * 获取速度最快的推流地址Pi
     *
     * @return String
     */
    public String getFastRtmpUrl() {

        String uid = UserUtils.getMyUserId();

        rtmpCDNList.clear();

        if (rtmpUrlSort == 0) {

            rtmpCDNList.addAll(rtmpPiliCDNList);

            rtmpCDNList.addAll(rtmpBaiduCDNList);

            rtmpCDNList.addAll(rtmpWSCDNList);

            rtmpCDNList.addAll(rtmpAliCDNList);

            Collections.sort(rtmpCDNList, sortComparator);

        } else {
            if (rtmpUrls != null) {
                for (String rtmpUrl : rtmpUrls) {

                    if (rtmpUrl.contains(RTMP_PILI_URL)) {
                        if (rtmpPiliCDNList != null && rtmpPiliCDNList.size() > 0) {
                            rtmpCDNList.add(rtmpPiliCDNList.get(0));
                        }
                    }

                    if (rtmpUrl.contains(RTMP_BAIDU_URL)) {
                        if (rtmpBaiduCDNList != null && rtmpBaiduCDNList.size() > 0) {
                            rtmpCDNList.add(rtmpBaiduCDNList.get(0));
                        }
                    }

                    if (rtmpUrl.contains(RTMP_WS_URL)) {
                        if (rtmpWSCDNList != null && rtmpWSCDNList.size() > 0) {
                            rtmpCDNList.add(rtmpWSCDNList.get(0));
                        }
                    }

                    if (rtmpUrl.contains(RTMP_ALI_URL)) {
                        if (rtmpAliCDNList != null && rtmpAliCDNList.size() > 0) {
                            rtmpCDNList.add(rtmpAliCDNList.get(0));
                        }
                    }
                }
            }

        }

        if (rtmpCDNList == null || rtmpCDNList.size() == 0) {
            return null;
        }

        if (rtmpCDNList.size() <= rtmpIndex) {
            rtmpIndex = 0;
        }

        CDNSpeedBean cdnSpeedBean = rtmpCDNList.get(rtmpIndex);

        rtmpIndex++;
        if (cdnSpeedBean == null) {
            return null;
        }
        if (RTMP_PILI_URL.equals(cdnSpeedBean.host)) {
            return getPiliRtmpUrl(cdnSpeedBean, uid);
        }

        if (RTMP_BAIDU_URL.equals(cdnSpeedBean.host)) {
            return getBaiduRtmpUrl(cdnSpeedBean, uid);
        }

        if (RTMP_WS_URL.equals(cdnSpeedBean.host)) {
            return getWsRtmpUrl(cdnSpeedBean);
        }

        if (RTMP_ALI_URL.equals(cdnSpeedBean.host)) {
            return getAliRtmpUrl(cdnSpeedBean);
        }

        return null;

    }

    public String getBaiduFastRtmpUrl() {

        String uid = UserUtils.getMyUserId();

        Collections.sort(rtmpCDNList, sortComparator);

        for (CDNSpeedBean cdnSpeedBean : rtmpCDNList) {
            if (cdnSpeedBean.host.equals(RTMP_BAIDU_URL)) {
                return getBaiduRtmpUrl(cdnSpeedBean, uid);
            }
        }
        return null;
    }

    public String getFastPlayIp() {
        playCDNList.clear();

        playCDNList.addAll(piliCDNList);

        playCDNList.addAll(wsCDNList);

        playCDNList.addAll(baiduCDNList);

        playCDNList.addAll(aliCDNList);

        Collections.sort(playCDNList, sortComparator);

        if (playCDNList.size() > 0) {
            return playCDNList.get(0).ip;
        }
        return null;
    }

    public String getFastRtmpIp() {
        if (rtmpCDNList != null && rtmpCDNList.size() > 0) {
            Collections.sort(rtmpCDNList, sortComparator);
            return getIpFromUrl(rtmpCDNList.get(0).ip);
        }
        return null;
    }

    /**
     * 获取速度最快的视频播放地址
     *
     * @return String
     */
    public String getFastVideoUrl(String uid) {

        try {

            if (flvInfos != null) {

                index++;

                L.d(TAG, " index : " + index);

                L.d(TAG, " plat url size : " + playCDNList.size());

                for (CDNSpeedBean cdnSpeedBean : playCDNList) {

                    L.d(TAG, " plat url : " + cdnSpeedBean.url);

                }

                if (liveUrlSort == 0) {

                    if (index >= playCDNList.size()) {
                        index = 0;
                    }

                    playCDNList.clear();

                    playCDNList.addAll(piliCDNList);

                    playCDNList.addAll(wsCDNList);

                    playCDNList.addAll(baiduCDNList);

                    playCDNList.addAll(aliCDNList);

                    Collections.sort(playCDNList, sortComparator);

                    if (playCDNList.size() <= 0) {
                        return null;
                    }

                    CDNSpeedBean cdnSpeedBean = playCDNList.get(index);

                    if (cdnSpeedBean.host.contains(PLAY_PILI_URL)) {

                        playOut = getCDNOut(playIn);

                        playIn = "qiniu_in";

                        umenEventPlayIn(playIn, playOut);

                        BasicInfoBean.IpsInfoBean ipsInfoBean = getPlayDataByHost(PLAY_PILI_URL);

                        if (ipsInfoBean == null) {
                            return null;
                        }

                        return getPiliPlayUrl(cdnSpeedBean.ip, ipsInfoBean.host, ipsInfoBean.bucket, ipsInfoBean.suffix, uid);

                    }

                    if (cdnSpeedBean.host.contains(PLAY_WS_URL)) {

                        playOut = getCDNOut(playIn);

                        playIn = "wangsu_in";

                        umenEventPlayIn(playIn, playOut);

                        return getWsPlayUrl(cdnSpeedBean.url, uid);

                    }

                    if (cdnSpeedBean.host.contains(PLAY_BAIDU_URL)) {

                        playOut = getCDNOut(playIn);

                        playIn = "baidu_in";

                        umenEventPlayIn(playIn, playOut);

                        BasicInfoBean.IpsInfoBean ipsInfoBean = getPlayDataByHost(PLAY_BAIDU_URL);

                        if (ipsInfoBean == null) {
                            return null;
                        }

                        return getBaiduPLayUrl(cdnSpeedBean.ip, ipsInfoBean.host, ipsInfoBean.bucket, ipsInfoBean.suffix, uid);
                    }

                    if (cdnSpeedBean.host.contains(PLAY_ALI_URL)) {

                        playOut = getCDNOut(playIn);

                        playIn = "ali_in";

                        umenEventPlayIn(playIn, playOut);

                        BasicInfoBean.IpsInfoBean ipsInfoBean = getPlayDataByHost(PLAY_ALI_URL);

                        if (ipsInfoBean == null) {
                            return null;
                        }

                        return getAliPlayUrl(cdnSpeedBean.ip, uid, ipsInfoBean.suffix);
                    }

                } else {

                    if (flvInfos.size() == index) {
                        index = 0;
                    }

                    BasicInfoBean.IpsInfoBean ipsInfoBean = flvInfos.get(index);

                    if (ipsInfoBean.host.equals(PLAY_PILI_URL)) {

                        Collections.sort(piliCDNList, sortComparator);

                        if (piliCDNList.size() == 0) {
                            return null;
                        }
                        CDNSpeedBean cdnSpeedBean = piliCDNList.get(0);

                        playOut = getCDNOut(playIn);

                        playIn = "qiniu_in";

                        umenEventPlayIn(playIn, playOut);

                        return getPiliPlayUrl(cdnSpeedBean.ip, ipsInfoBean.host, ipsInfoBean.bucket, ipsInfoBean.suffix, uid);

                    }

                    if (ipsInfoBean.host.equals(PLAY_WS_URL)) {

                        Collections.sort(wsCDNList, sortComparator);

                        if (wsCDNList.size() == 0) {
                            return null;
                        }

                        CDNSpeedBean cdnSpeedBean = wsCDNList.get(0);

                        playOut = getCDNOut(playIn);

                        playIn = "wangsu_in";

                        umenEventPlayIn(playIn, playOut);

                        return getWsPlayUrl(cdnSpeedBean.url, uid);

                    }

                    if (ipsInfoBean.host.equals(PLAY_BAIDU_URL)) {

                        Collections.sort(baiduCDNList, sortComparator);

                        if (baiduCDNList.size() == 0) {
                            return null;
                        }

                        CDNSpeedBean cdnSpeedBean = baiduCDNList.get(0);

                        playOut = getCDNOut(playIn);

                        playIn = "baidu_in";

                        umenEventPlayIn(playIn, playOut);

                        return getBaiduPLayUrl(cdnSpeedBean.ip, ipsInfoBean.host, ipsInfoBean.bucket, ipsInfoBean.suffix, uid);

                    }

                    if (ipsInfoBean.host.equals(PLAY_ALI_URL)) {

                        Collections.sort(aliCDNList, sortComparator);

                        if (aliCDNList.size() == 0) {
                            return null;
                        }

                        CDNSpeedBean cdnSpeedBean = aliCDNList.get(0);

                        playOut = getCDNOut(playIn);

                        playIn = "baidu_in";

                        umenEventPlayIn(playIn, playOut);

                        return getAliPlayUrl(cdnSpeedBean.ip, uid, ipsInfoBean.suffix);

                    }
                }
            }

            playOut = getCDNOut(playIn);

            playIn = "other_in";

            umenEventPlayIn(playIn, playOut);

            return null;

        } catch (Exception e) {

            L.d(TAG, " Exception : " + e.getMessage());

            e.printStackTrace();

            return null;
        }

    }

    /**
     * 获取速度最快的多人连麦的视频播放地址
     *
     * @return String
     */
    public String getFastMultiLinkMicVideoUrl(String uid) {

        if (multiFlvInfos != null) {

            if (index >= multiFlvInfos.size()) {
                index = 0;
            }

            BasicInfoBean.IpsInfoBean ipsInfoBean = multiFlvInfos.get(index);

            if (ipsInfoBean.host.equals(PLAY_PILI_MULTI_URL)) {

                Collections.sort(piliMultiCDNList, sortComparator);

                if (piliMultiCDNList.size() == 0) {
                    return null;
                }
                CDNSpeedBean cdnSpeedBean = piliMultiCDNList.get(0);

                playOut = playIn;

                playIn = "qiniu_in";

                umenEventPlayIn(playIn, playOut);

                return getPiliPlayUrl(cdnSpeedBean.ip, ipsInfoBean.host, ipsInfoBean.bucket, ipsInfoBean.suffix, uid);

            }

            if (ipsInfoBean.host.equals(PLAY_WS_MULTI_URL)) {

                Collections.sort(wsMultiCDNList, sortComparator);

                if (wsMultiCDNList.size() == 0) {
                    return null;
                }

                CDNSpeedBean cdnSpeedBean = wsMultiCDNList.get(0);

                playOut = playIn;

                playIn = "wangsu_in";

                umenEventPlayIn(playIn, playOut);

                return getWsPlayUrl(cdnSpeedBean.url, uid);

            }

            if (ipsInfoBean.host.equals(PLAY_BAIDU_MULTI_URL)) {

                Collections.sort(baiduMultiCDNList, sortComparator);

                if (baiduMultiCDNList.size() == 0) {
                    return null;
                }

                CDNSpeedBean cdnSpeedBean = baiduMultiCDNList.get(0);

                playOut = playIn;

                playIn = "baidu_in";

                umenEventPlayIn(playIn, playOut);

                return getBaiduPLayUrl(cdnSpeedBean.ip, ipsInfoBean.host, ipsInfoBean.bucket, ipsInfoBean.suffix, uid);

            }

            if (ipsInfoBean.host.equals(PLAY_ALI_MULTI_URL)) {

                Collections.sort(aliMultiCDNList, sortComparator);

                if (aliMultiCDNList.size() == 0) {
                    return null;
                }

                CDNSpeedBean cdnSpeedBean = aliMultiCDNList.get(0);

                playOut = playIn;

                playIn = "ali_in";

                umenEventPlayIn(playIn, playOut);

                return getAliPlayUrl(cdnSpeedBean.ip, uid, ipsInfoBean.suffix);

            }

        }

        playOut = playIn;

        playIn = "other_in";

        umenEventPlayIn(playIn, playOut);

        return null;
    }

    /**
     * 网络变化
     */
    public void setNetworkChanged() {

        L.d(TAG, " ---------setNetworkChanged-------- ");

        if (flvInfos != null && rtmpUrls != null) {

            initRtmpUrl(rtmpUrls);

            initPlayUrl(flvInfos);

        }

    }

    public String changeRtmpUrl() {
        index++;
        return getFastRtmpUrl();
    }

    private OkHttpClient getOkhttpClient() {
        if (okHttpClient == null) {
            okHttpClient = new OkHttpClient();
        }
        return okHttpClient;
    }

    private String getBaiduRtmpUrl(CDNSpeedBean cdnSpeedBean, String uid) {

        String suffix = BuildConfig.BUILD_TYPE.equals("debug") ? "test" : "";

        return "rtmp://" + cdnSpeedBean.ip + "/live/" + uid + suffix + "?wsHost=" + cdnSpeedBean.host;
    }

    private String getWsRtmpUrl(CDNSpeedBean cdnSpeedBean) {

        String uid = UserUtils.getMyUserId();

        String suffix = BuildConfig.BUILD_TYPE.equals("debug") ? "test" : "";

        return "rtmp://" + cdnSpeedBean.ip + "/live1/" + uid + suffix + "?wsHost=" + cdnSpeedBean.host;
    }

    private String getPiliRtmpUrl(CDNSpeedBean cdnSpeedBean, String uid) {

        String suffix = BuildConfig.BUILD_TYPE.equals("debug") ? "test" : "";

        return "rtmp://" + cdnSpeedBean.ip + "/rela-live/" + uid + suffix + "?domain=" + cdnSpeedBean.host;

    }

    private String getAliRtmpUrl(CDNSpeedBean cdnSpeedBean) {
        String uid = UserUtils.getMyUserId();

        String suffix = BuildConfig.BUILD_TYPE.equals("debug") ? "test" : "";

        return "rtmp://" + cdnSpeedBean.ip + ":" + cdnSpeedBean.port + "/live/" + uid + suffix + "?vhost=" + RTMP_ALI_URL + "&edge_push=on&domain=" + RTMP_ALI_URL;
    }

    /**
     * 初始化推流地址
     *
     * @param rtmpUrls
     */
    private void initRtmpUrl(List<String> rtmpUrls) {

        if (rtmpUrls != null) {

            if (rtmpCDNList != null) {

                rtmpCDNList.clear();

            }

            for (String rtmpUrl : rtmpUrls) {

                L.d(TAG, " rtmpUrl : " + rtmpUrl);

                L.d(TAG, " is pili rtmp url : " + rtmpUrl.contains(RTMP_PILI_URL));

                if (rtmpUrl.contains(RTMP_PILI_URL)) {

                    getPiliIPs(PILI_IPS_URL, rtmpUrl, true, false);

                }

                L.d(TAG, " is baidu rtmp url : " + rtmpUrl.contains(RTMP_BAIDU_URL));

                if (rtmpUrl.contains(RTMP_BAIDU_URL)) {

                    getBaiDuIps(BAIDU_IPS_URL, RTMP_BAIDU_URL, true, false);

                }

                L.d(TAG, " is ws rtmp url : " + rtmpUrl.contains(RTMP_WS_URL));

                if (rtmpUrl.contains(RTMP_WS_URL)) {

                    getWsIPs(WS_IPS_URL, rtmpUrl, true, false);

                }

                L.d(TAG, " is ali rtmp url : " + rtmpUrl.contains(RTMP_ALI_URL));

                if (rtmpUrl.contains(RTMP_ALI_URL)) {

                    getAliIPs(ALI_IPS_URL, rtmpUrl, true, false);

                }

            }
        }

    }

    /**
     * 初始化播放地址
     *
     * @param flvInfos
     */
    private void initPlayUrl(List<BasicInfoBean.IpsInfoBean> flvInfos) {

        if (flvInfos != null) {

            if (piliCDNList != null) {
                piliCDNList.clear();
            }

            if (wsCDNList != null) {
                wsCDNList.clear();
            }

            if (baiduCDNList != null) {
                baiduCDNList.clear();
            }

            if (aliCDNList != null) {
                aliCDNList.clear();
            }

            if (baiduMultiCDNList != null) {
                baiduMultiCDNList.clear();
            }

            if (wsMultiCDNList != null) {
                wsMultiCDNList.clear();
            }

            if (piliMultiCDNList != null) {
                piliMultiCDNList.clear();
            }

            if (aliMultiCDNList != null) {
                aliMultiCDNList.clear();
            }

            for (BasicInfoBean.IpsInfoBean ipsInfoBean : flvInfos) {
                if (ipsInfoBean.host.equals(PLAY_WS_URL)) {

                    getWsIPs(WS_IPS_URL, getWsRequestPlayerUrl(ipsInfoBean.host, ipsInfoBean.bucket, ipsInfoBean.suffix), false, false);

                }

                if (ipsInfoBean.host.equals(PLAY_BAIDU_URL)) {

                    getBaiDuIps(BAIDU_IPS_URL, RTMP_BAIDU_URL, false, false);

                }

                if (ipsInfoBean.host.equals(PLAY_PILI_URL)) {

                    String uid = UserUtils.getMyUserId();

                    String playUrl = getPiliPlayUrl(ipsInfoBean.host, ipsInfoBean.host, ipsInfoBean.bucket, ipsInfoBean.suffix, uid);

                    getPiliIPs(PILI_IPS_URL, playUrl, false, false);

                }

                if (ipsInfoBean.host.equals(PLAY_ALI_URL)) {

                    String uid = UserUtils.getMyUserId();

                    String playUrl = getAliPlayUrl(PLAY_ALI_URL, uid, ipsInfoBean.suffix);

                    getAliIPs(ALI_IPS_URL, playUrl, false, false);

                }
            }

            for (BasicInfoBean.IpsInfoBean ipsInfoBean : multiFlvInfos) {
                if (ipsInfoBean.host.equals(PLAY_WS_MULTI_URL)) {

                    getWsIPs(WS_IPS_URL, getWsRequestPlayerUrl(ipsInfoBean.host, ipsInfoBean.bucket, ipsInfoBean.suffix), false, true);

                }

                if (ipsInfoBean.host.equals(PLAY_BAIDU_MULTI_URL)) {

                    getBaiDuIps(BAIDU_IPS_URL, RTMP_BAIDU_URL, false, true);

                }

                if (ipsInfoBean.host.equals(PLAY_PILI_MULTI_URL)) {

                    String uid = UserUtils.getMyUserId();

                    String playUrl = getPiliPlayUrl(ipsInfoBean.host, ipsInfoBean.host, ipsInfoBean.bucket, ipsInfoBean.suffix, uid);

                    getPiliIPs(PILI_IPS_URL, playUrl, false, true);

                }

                if (ipsInfoBean.host.equals(PLAY_ALI_MULTI_URL)) {

                    String uid = UserUtils.getMyUserId();

                    String playUrl = getAliPlayUrl(PLAY_ALI_URL, uid, ipsInfoBean.suffix);

                    getAliIPs(ALI_IPS_URL, playUrl, false, true);

                }
            }
        }

    }

    /**
     * 获取百度播放地址
     *
     * @param ip
     * @param host
     * @param bucket
     * @param suffix
     * @param uid
     * @return
     */
    private String getBaiduPLayUrl(String ip, String host, String bucket, String suffix, String uid) {

        return "http://" + ip + "/r/" + host + "/" + bucket + "/" + uid + suffix + ".flv";
    }

    /**
     * 获取七牛播放地址
     *
     * @param ip
     * @param host
     * @param bucket
     * @param suffix
     * @param uid
     * @return
     */
    private String getPiliPlayUrl(String ip, String host, String bucket, String suffix, String uid) {

        return "http://" + ip + "/" + bucket + "/" + uid + suffix + ".flv?domain=" + host;
    }

    /**
     * 获取网宿播放地址
     *
     * @param url
     * @param uid
     * @return
     */
    private String getWsPlayUrl(String url, String uid) {

        if (url == null) {
            return null;
        }

        String myUid = UserUtils.getMyUserId();

        return url.replace(myUid, uid);
    }

    /**
     * 获取阿里的播放地址
     *
     * @param uid
     * @param suffix
     * @return
     */
    private String getAliPlayUrl(String host, String uid, String suffix) {
        return "http://" + host + "/" + PLAY_ALI_URL + "/live/" + uid + suffix + ".flv";
    }

    /**
     * 获取网宿请求地址
     *
     * @param host
     * @param bucket
     * @param suffix
     * @return
     */
    private String getWsRequestPlayerUrl(String host, String bucket, String suffix) {

        String uid = UserUtils.getMyUserId();

        return "http://" + host + "/" + bucket + "/" + uid + suffix + ".flv";

    }

    /**
     * 获取百度ip
     *
     * @param url
     * @param streamUrl
     * @param isRtmp
     */
    private void getBaiDuIps(String url, String streamUrl, final boolean isRtmp, final boolean isMultiLinkMic) {

        final Request request = new Request.Builder().url(url).get().build();

        HttpUrl.Builder urlBuilder = request.url().newBuilder();

        urlBuilder.addEncodedQueryParameter("dn", streamUrl);

        Request newRequest = request.newBuilder().url(urlBuilder.build()).build();

        Call call = getOkhttpClient().newCall(newRequest);

        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                L.d(TAG, "onFailure: ");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                String result = response.body().string();

                L.d(TAG, " 百度 result: " + result);

                try {

                    JSONObject jsonObject = new JSONObject(result);

                    String data = jsonObject.optString("data", "");

                    JSONObject dataJson = new JSONObject(data);

                    String dataStr = dataJson.optString(RTMP_BAIDU_URL, "");

                    BasiduIPsBean ips = GsonUtils.getObject(dataStr, BasiduIPsBean.class);

                    if (ips != null && ips.ip != null) {

                        for (String ip : ips.ip) {

                            if (ip == null || ip.length() == 0) {
                                break;
                            }

                            CDNSpeedBean cdnSpeedBean = new CDNSpeedBean();

                            cdnSpeedBean.ip = ip;

                            if (isRtmp) {

                                cdnSpeedBean.host = RTMP_BAIDU_URL;

                                rtmpBaiduCDNList.add(cdnSpeedBean);

                                rtmpCDNList.add(cdnSpeedBean);

                            } else {
                                if (isMultiLinkMic) {

                                    cdnSpeedBean.host = PLAY_BAIDU_MULTI_URL;

                                    baiduMultiCDNList.add(cdnSpeedBean);
                                } else {
                                    cdnSpeedBean.host = PLAY_BAIDU_URL;

                                    baiduCDNList.add(cdnSpeedBean);
                                }

                            }


                            ping(cdnSpeedBean);

                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

    }

    /**
     * 获取网宿ip
     *
     * @param url
     * @param streamUrl
     * @param isRtmp
     */
    private void getWsIPs(String url, String streamUrl, final boolean isRtmp, final boolean isMultiLinkMic) {

        final Request request = new Request.Builder().headers(getWsHeaders(streamUrl)).url(url).get().build();
        Call call = getOkhttpClient().newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                L.d(TAG, " getWsIPs onFailure: ");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                String result = response.body().string();

                L.d(TAG, " 网宿 result: " + result);

                if (result.length() > 0) {

                    String[] urls = result.split("\n");

                    for (String url : urls) {

                        String ip = getIpFromUrl(url);

                        if (ip == null || ip.length() == 0) {
                            break;
                        }

                        CDNSpeedBean cdnSpeedBean = new CDNSpeedBean();

                        cdnSpeedBean.ip = ip;

                        if (isRtmp) {

                            cdnSpeedBean.url = url;

                            cdnSpeedBean.host = RTMP_WS_URL;

                            rtmpWSCDNList.add(cdnSpeedBean);

                            rtmpCDNList.add(cdnSpeedBean);

                        } else {

                            if (isMultiLinkMic) {

                                cdnSpeedBean.host = PLAY_WS_MULTI_URL;

                                wsMultiCDNList.add(cdnSpeedBean);
                            } else {

                                cdnSpeedBean.url = url;

                                cdnSpeedBean.host = PLAY_WS_URL;

                                wsCDNList.add(cdnSpeedBean);

                            }

                        }

                        ping(cdnSpeedBean);

                    }
                }


            }
        });

    }

    /**
     * 获取七牛ip
     *
     * @param url
     * @param streamUrl
     * @param isRtmp
     */
    private void getPiliIPs(String url, String streamUrl, final boolean isRtmp, final boolean isMultiLinkMic) {

        if (isRtmp) {
            url = url + "publish";
        } else {
            url = url + "play";

        }


        final Request request = new Request.Builder().url(url).get().build();

        HttpUrl.Builder urlBuilder = request.url().newBuilder();

        urlBuilder.addEncodedQueryParameter("stream", streamUrl);

        Request newRequest = request.newBuilder().url(urlBuilder.build()).build();

        Call call = getOkhttpClient().newCall(newRequest);

        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                L.d(TAG, "onFailure: ");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                String result = response.body().string();

                L.d(TAG, " 七牛的数据 : " + result);

                PiliIPsBean piliIPsBean = GsonUtils.getObject(result, PiliIPsBean.class);

                if (piliIPsBean != null && piliIPsBean.items != null) {

                    for (PiliIPsBean.PiliIPBean ip : piliIPsBean.items) {

                        if (ip.ip == null || ip.ip.length() == 0) {
                            break;
                        }

                        CDNSpeedBean cdnSpeedBean = new CDNSpeedBean();

                        cdnSpeedBean.ip = ip.ip;

                        if (isRtmp) {

                            cdnSpeedBean.host = RTMP_PILI_URL;
                            try {
                                rtmpPiliCDNList.add(cdnSpeedBean);
                                rtmpCDNList.add(cdnSpeedBean);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            try {
                                if (isMultiLinkMic) {
                                    cdnSpeedBean.host = PLAY_PILI_MULTI_URL;
                                    piliMultiCDNList.add(cdnSpeedBean);
                                } else {
                                    cdnSpeedBean.host = PLAY_PILI_URL;
                                    piliCDNList.add(cdnSpeedBean);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        ping(cdnSpeedBean);

                    }
                }


            }
        });

    }

    private void getAliIPs(String url, String streamUrl, final boolean isRtmp, final boolean isMultiLinkMic) {

        final Request request = new Request.Builder().headers(getAliHeaders(streamUrl)).url(url).get().build();
        Call call = getOkhttpClient().newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                L.d(TAG, " getAliIPs onFailure: ");
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {

                if (response.body() != null) {

                    String result = response.body().string();

                    L.d(TAG, " 阿里的数据 : " + result);

                    if (result != null && result.length() > 0) {
                        AliIPsBean aliIPsBean = GsonUtils.getObject(result, AliIPsBean.class);

                        if (aliIPsBean != null && aliIPsBean.content != null && aliIPsBean.content.list != null) {
                            for (AliIPsBean.AliIPsItemBean aliIPsItemBean : aliIPsBean.content.list) {

                                if (aliIPsItemBean.ip == null || aliIPsItemBean.ip.length() == 0) {
                                    break;
                                }

                                CDNSpeedBean cdnSpeedBean = new CDNSpeedBean();

                                cdnSpeedBean.ip = aliIPsItemBean.ip;
                                cdnSpeedBean.port = aliIPsItemBean.port;

                                try {
                                    if (isRtmp) {

                                        cdnSpeedBean.url = url;

                                        cdnSpeedBean.host = RTMP_ALI_URL;

                                        rtmpAliCDNList.add(cdnSpeedBean);

                                        rtmpAliCDNList.add(cdnSpeedBean);

                                    } else {

                                        if (isMultiLinkMic) {

                                            cdnSpeedBean.host = PLAY_ALI_MULTI_URL;

                                            aliMultiCDNList.add(cdnSpeedBean);
                                        } else {

                                            cdnSpeedBean.url = url;

                                            cdnSpeedBean.host = PLAY_ALI_URL;

                                            aliCDNList.add(cdnSpeedBean);

                                        }
                                    }
                                } catch (Exception e) {

                                }
                                ping(cdnSpeedBean);
                            }
                        }
                    }

                }

            }
        });
    }

    /**
     * 获取网宿请求头
     *
     * @param streamUrl
     * @return
     */
    private Headers getWsHeaders(String streamUrl) {
        return new Headers.Builder().add("WS_URL", streamUrl).add("WS_RETIP_NUM", "2").add("WS_URL_TYPE", "2").build();
    }

    private Headers getAliHeaders(String url) {
        return new Headers.Builder().add("Forward_stream", url).build();
    }

    /**
     * ping ip
     *
     * @param cdnSpeedBean
     */
    private void ping(final CDNSpeedBean cdnSpeedBean) {
//        float ping = PingUtils.ping(PING_COUNT, cdnSpeedBean.ip);
        Flowable.fromCallable(new Callable<Float>() {
            @Override
            public Float call() throws Exception {
                try {
                    return PingUtils.ping(PING_COUNT, cdnSpeedBean.ip);
                } catch (Exception e) {
                    return Float.valueOf(-1);
                }
            }
        }).subscribeOn(pingSchedulers).subscribe(new Consumer<Float>() {
            @Override public void accept(Float time) {
                cdnSpeedBean.time = time;
            }
        });
    }

    /**
     * 在url截取出ip地址
     *
     * @param str
     * @return
     */
    public String getIpFromUrl(String str) {

        String regex = "[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}";

        // 匹配1 和匹配2均可实现Ip判断的效果
        Pattern pattern = Pattern.compile(regex);

        Matcher matcher = pattern.matcher(str);

        while (matcher.find()) {

            return matcher.group(0);

        }

        return null;
    }

    /**
     * 根据时间降序排序
     */
    private Comparator<CDNSpeedBean> sortComparator = new Comparator<CDNSpeedBean>() {
        @Override
        public int compare(CDNSpeedBean o1, CDNSpeedBean o2) {
            try {

                if (o1.time == -1 || o2.time == -1) {
                    return -1;
                }

                if (o1.time > o2.time) {
                    return 1;
                }

                if (o1.time < o2.time) {
                    return -1;
                }

                return 0;

            } catch (Exception e) {
                e.printStackTrace();
                return -1;
            }

        }
    };

    private void umenEventPlayIn(String in, String out) {

        if (in != null && in.length() > 0) {

            MobclickAgent.onEvent(TheLApp.context, "play_cdn_in", in);
        }

        if (out != null && out.length() > 0) {

            MobclickAgent.onEvent(TheLApp.context, "play_cdn_out", out);

        }

    }

    public String getCDNOut(String cdnIn) {

        if (cdnIn.equals("qiniu_in")) {
            return "qiniu_out";
        }

        if (cdnIn.equals("wangsu_in")) {
            return "wangsu_out";
        }

        if (cdnIn.equals("baidu_in")) {
            return "baidu_out";
        }

        if (cdnIn.equals("other_in")) {
            return "other_out";
        }

        if (cdnIn.equals("ali_in")) {
            return "ali_out";
        }

        return "other_out";
    }

    public void test() {

        for (int i = 0; i < rtmpCDNList.size(); i++) {

            CDNSpeedBean item = rtmpCDNList.get(i);

            L.d(TAG, "ip : " + item.ip + " ,time : " + item.time);
        }
    }

    public boolean isOtherCDN(String pubUrl) {

        if (pubUrl.contains(RTMP_PILI_URL)) {

            return false;
        }

        if (pubUrl.contains(RTMP_BAIDU_URL)) {

            return false;
        }


        if (pubUrl.contains(RTMP_WS_URL)) {

            return false;

        }

        return !pubUrl.contains(RTMP_ALI_URL);
    }

    public BasicInfoBean.IpsInfoBean getPlayDataByHost(String host) {
        for (BasicInfoBean.IpsInfoBean ipsInfoBean : flvInfos) {
            if (ipsInfoBean.host.equals(host)) {
                return ipsInfoBean;
            }
        }
        return null;
    }

    //http://1.1.1.1/bucket/(<userId>+<suffix>).flv?domain=<host> 七牛拉流
    //rtmp://1.1.1.1/rela-live/<userId>?domain=<host> 七牛推流

    //http://180.163.150.90/rtmp.push-ws.rela.me/live1/103351722test?wsiphost=ipdb http://220.177.176.17/rtmp.push-ws.rela.me/live1/103351722test?wsiphost=ipdb
    //http://flv.live-ws.rela.me/<bucket>/<自己的uid+suffix>.flv

    //百度推流 直接把服务器返回的地址的host更换成百度返回的ip地址
    //百度拉流 http://<ip>/r/<加速域名>/<bucket>/<自己的uid+suffix>.flv
}
