package com.thel.db.table.message;

import android.os.Parcel;
import android.os.Parcelable;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class WinkMsgTable implements Parcelable {
    /**
     * 主键，autoincrement设置自增
     */
    @Id(autoincrement = true)
    @Property(nameInDb = "id")
    public Long id;

    /**
     * 用户名字
     */
    @Property(nameInDb = "userName")
    public String userName;

    /**
     * 用户的id
     */
    @Property(nameInDb = "userId")
    public String userId;

    /**
     * 头像
     */
    @Property(nameInDb = "avatar")
    public String avatar;

    /**
     * 是否已经挤眼
     */
    @Property(nameInDb = "isWinked")
    public int isWinked;

    /**
     * 挤眼时间
     */
    @Property(nameInDb = "msgTime")
    public long msgTime;

    /**
     * 是否是时间
     */
    @Property(nameInDb = "isTimeTip")
    public int isTimeTip;


    @Override public int describeContents() {
        return 0;
    }

    @Override public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.userName);
        dest.writeString(this.userId);
        dest.writeString(this.avatar);
        dest.writeInt(this.isWinked);
        dest.writeLong(this.msgTime);
        dest.writeInt(this.isTimeTip);
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAvatar() {
        return this.avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getIsWinked() {
        return this.isWinked;
    }

    public void setIsWinked(int isWinked) {
        this.isWinked = isWinked;
    }

    public long getMsgTime() {
        return this.msgTime;
    }

    public void setMsgTime(long msgTime) {
        this.msgTime = msgTime;
    }

    public int getIsTimeTip() {
        return this.isTimeTip;
    }

    public void setIsTimeTip(int isTimeTip) {
        this.isTimeTip = isTimeTip;
    }

    public WinkMsgTable() {
    }

    protected WinkMsgTable(Parcel in) {
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.userName = in.readString();
        this.userId = in.readString();
        this.avatar = in.readString();
        this.isWinked = in.readInt();
        this.msgTime = in.readLong();
        this.isTimeTip = in.readInt();
    }

    @Generated(hash = 1179643208)
    public WinkMsgTable(Long id, String userName, String userId, String avatar, int isWinked, long msgTime,
                        int isTimeTip) {
        this.id = id;
        this.userName = userName;
        this.userId = userId;
        this.avatar = avatar;
        this.isWinked = isWinked;
        this.msgTime = msgTime;
        this.isTimeTip = isTimeTip;
    }

    public static final Parcelable.Creator<WinkMsgTable> CREATOR = new Parcelable.Creator<WinkMsgTable>() {
        @Override public WinkMsgTable createFromParcel(Parcel source) {
            return new WinkMsgTable(source);
        }

        @Override public WinkMsgTable[] newArray(int size) {
            return new WinkMsgTable[size];
        }
    };
}
