package com.thel.db.table.message;

import android.os.Parcel;
import android.os.Parcelable;

import com.thel.utils.GsonUtils;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Unique;

/**
 * Created by chad
 * Time 18/3/20
 * Email: wuxianchuang@foxmail.com
 * Description: TODO 首页消息表
 */

@Entity
public class MsgTable implements Parcelable {

    /**
     * 主键，autoincrement设置自增
     */
    @Id(autoincrement = true)
    @Property(nameInDb = "id")
    public Long id;

    /**
     * 消息ID
     */
    @NotNull
    @Property(nameInDb = "packetId")
    public String packetId;

    /**
     * 对方的ID
     * 唯一，默认索引
     */
    @Unique
    @NotNull
    @Property(nameInDb = "userId")
    public String userId;

    /**
     * 对方的昵称
     */
    @NotNull
    @Property(nameInDb = "userName")
    public String userName;

    /**
     * 对方的头像
     */
    @NotNull
    @Property(nameInDb = "avatar")
    public String avatar;

    /**
     * 消息发送时间
     */
    @NotNull
    @Property(nameInDb = "msgTime")
    public Long msgTime;

    /**
     * 消息内容
     */
    @Property(nameInDb = "msgText")
    public String msgText;

    /**
     * 消息类型
     */
    @NotNull
    @Property(nameInDb = "msgType")
    public String msgType;

    /**
     * 未读消息数
     */
    @Property(nameInDb = "unreadCount")
    public int unreadCount;

    /**
     * 挤眼
     */
    @Property(nameInDb = "isWinked")
    public int isWinked;    //1表示已挤眼 0表示未挤眼

    /**
     * 是否是陌生人
     */
    @Property(nameInDb = "isStranger")
    public int isStranger = 0;//1表示陌生人 0表示好友

    /**
     * 发送者的头像
     */
    @Property(nameInDb = "toAvatar")
    public String toAvatar;

    /**
     * 接收者的头像
     */
    @Property(nameInDb = "fromAvatar")
    public String fromAvatar;

    /**
     * 发送者的id
     */
    @Property(nameInDb = "fromUserId")
    public String fromUserId;

    /**
     * 接收者的id
     */
    @Property(nameInDb = "toUserId")
    public String toUserId;

    /**
     * 发送者的nickname
     */
    @Property(nameInDb = "toUserNickname")
    public String toUserNickname;

    /**
     * 接收者的nickname
     */
    @Property(nameInDb = "fromNickname")
    public String fromNickname;

    /**
     * 消息来源
     */
    @Property(nameInDb = "msgDirection")
    public String msgDirection;

    /**
     * 消息分类 好友消息 0 系统消息 1 关注消息 3 陌生人消息 4 请求消息 5
     */
    @Property(nameInDb = "isSystem")
    public int isSystem = 0;

    /**
     * 聊天的背景图
     */
    @Property(nameInDb = "chatBg")
    public String chatBg;

    @Override public int describeContents() {
        return 0;
    }

    @Override public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.packetId);
        dest.writeString(this.userId);
        dest.writeString(this.userName);
        dest.writeString(this.avatar);
        dest.writeValue(this.msgTime);
        dest.writeString(this.msgText);
        dest.writeString(this.msgType);
        dest.writeInt(this.unreadCount);
        dest.writeInt(this.isWinked);
        dest.writeInt(this.isStranger);
        dest.writeString(this.toAvatar);
        dest.writeString(this.fromAvatar);
        dest.writeString(this.fromUserId);
        dest.writeString(this.toUserId);
        dest.writeString(this.toUserNickname);
        dest.writeString(this.fromNickname);
        dest.writeString(this.msgDirection);
        dest.writeInt(this.isSystem);
        dest.writeString(this.chatBg);
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPacketId() {
        return this.packetId;
    }

    public void setPacketId(String packetId) {
        this.packetId = packetId;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAvatar() {
        return this.avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Long getMsgTime() {
        return this.msgTime;
    }

    public void setMsgTime(Long msgTime) {
        this.msgTime = msgTime;
    }

    public String getMsgText() {
        return this.msgText;
    }

    public void setMsgText(String msgText) {
        this.msgText = msgText;
    }

    public String getMsgType() {
        return this.msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public int getUnreadCount() {
        return this.unreadCount;
    }

    public void setUnreadCount(int unreadCount) {
        this.unreadCount = unreadCount;
    }

    public int getIsWinked() {
        return this.isWinked;
    }

    public void setIsWinked(int isWinked) {
        this.isWinked = isWinked;
    }

    public int getIsStranger() {
        return this.isStranger;
    }

    public void setIsStranger(int isStranger) {
        this.isStranger = isStranger;
    }

    public String getToAvatar() {
        return this.toAvatar;
    }

    public void setToAvatar(String toAvatar) {
        this.toAvatar = toAvatar;
    }

    public String getFromAvatar() {
        return this.fromAvatar;
    }

    public void setFromAvatar(String fromAvatar) {
        this.fromAvatar = fromAvatar;
    }

    public String getFromUserId() {
        return this.fromUserId;
    }

    public void setFromUserId(String fromUserId) {
        this.fromUserId = fromUserId;
    }

    public String getToUserId() {
        return this.toUserId;
    }

    public void setToUserId(String toUserId) {
        this.toUserId = toUserId;
    }

    public String getToUserNickname() {
        return this.toUserNickname;
    }

    public void setToUserNickname(String toUserNickname) {
        this.toUserNickname = toUserNickname;
    }

    public String getFromNickname() {
        return this.fromNickname;
    }

    public void setFromNickname(String fromNickname) {
        this.fromNickname = fromNickname;
    }

    public String getMsgDirection() {
        return this.msgDirection;
    }

    public void setMsgDirection(String msgDirection) {
        this.msgDirection = msgDirection;
    }

    public int getIsSystem() {
        return this.isSystem;
    }

    public void setIsSystem(int isSystem) {
        this.isSystem = isSystem;
    }

    public String getChatBg() {
        return this.chatBg;
    }

    public void setChatBg(String chatBg) {
        this.chatBg = chatBg;
    }

    public MsgTable() {
    }

    protected MsgTable(Parcel in) {
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.packetId = in.readString();
        this.userId = in.readString();
        this.userName = in.readString();
        this.avatar = in.readString();
        this.msgTime = (Long) in.readValue(Long.class.getClassLoader());
        this.msgText = in.readString();
        this.msgType = in.readString();
        this.unreadCount = in.readInt();
        this.isWinked = in.readInt();
        this.isStranger = in.readInt();
        this.toAvatar = in.readString();
        this.fromAvatar = in.readString();
        this.fromUserId = in.readString();
        this.toUserId = in.readString();
        this.toUserNickname = in.readString();
        this.fromNickname = in.readString();
        this.msgDirection = in.readString();
        this.isSystem = in.readInt();
        this.chatBg = in.readString();
    }

    @Generated(hash = 4348876)
    public MsgTable(Long id, @NotNull String packetId, @NotNull String userId, @NotNull String userName,
            @NotNull String avatar, @NotNull Long msgTime, String msgText, @NotNull String msgType,
            int unreadCount, int isWinked, int isStranger, String toAvatar, String fromAvatar,
            String fromUserId, String toUserId, String toUserNickname, String fromNickname,
            String msgDirection, int isSystem, String chatBg) {
        this.id = id;
        this.packetId = packetId;
        this.userId = userId;
        this.userName = userName;
        this.avatar = avatar;
        this.msgTime = msgTime;
        this.msgText = msgText;
        this.msgType = msgType;
        this.unreadCount = unreadCount;
        this.isWinked = isWinked;
        this.isStranger = isStranger;
        this.toAvatar = toAvatar;
        this.fromAvatar = fromAvatar;
        this.fromUserId = fromUserId;
        this.toUserId = toUserId;
        this.toUserNickname = toUserNickname;
        this.fromNickname = fromNickname;
        this.msgDirection = msgDirection;
        this.isSystem = isSystem;
        this.chatBg = chatBg;
    }

    public static final Parcelable.Creator<MsgTable> CREATOR = new Parcelable.Creator<MsgTable>() {
        @Override public MsgTable createFromParcel(Parcel source) {
            return new MsgTable(source);
        }

        @Override public MsgTable[] newArray(int size) {
            return new MsgTable[size];
        }
    };
}
