package com.thel.db;

import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.thel.constants.TheLConstants;
import com.thel.db.daos.DaoSession;
import com.thel.db.daos.MsgTableDao;
import com.thel.db.daos.UserInfoTableDao;
import com.thel.db.daos.WinkMsgTableDao;
import com.thel.db.table.message.MsgTable;
import com.thel.db.table.message.UserInfoTable;
import com.thel.db.table.message.WinkMsgTable;
import com.thel.manager.ChatServiceManager;
import com.thel.modules.main.messages.bean.MsgBean;
import com.thel.service.chat.ChatService;
import com.thel.utils.L;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by chad
 * Time 18/3/20
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */

/**
 * greenDao 操作符
 * 1. > : gt
 * 2. < : lt
 * 3. >= : ge
 * 4. <= : le
 */

public class DBManager implements LocalDataSource {

    private static final String TAG = "DBManager";

    private static final long TEN_MIN = 10 * 60 * 1000;

    private List<String> userList = new ArrayList<>();

    private ChatService.ChatServiceBinder chatServiceBinder;

    private String userId = "";

    private DBManager() {

    }

    private static DBManager instance;

    public synchronized static DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }

        if (instance.messageDaoSession == null) {

            instance.messageDaoSession = DBUtils.createDB(instance.userId);

        } else {

            if (!DBUtils.getDbName().equals(DBUtils.getDbName(instance.userId))) {

                instance.messageDaoSession = DBUtils.createDB(instance.userId);

            }

        }

        return instance;
    }

    public void setBinder(ChatService.ChatServiceBinder chatServiceBinder) {
        this.chatServiceBinder = chatServiceBinder;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<String> getUserList() {
        return userList;
    }

    public void setUserList(List<String> userList) {
        this.userList.clear();
        this.userList.addAll(userList);
    }

    private DaoSession messageDaoSession = null;

    public DaoSession getDaoSession() {
        return messageDaoSession;
    }

    public synchronized void insertNewMsg(MsgBean msgBean, String tableName, int hasRead) {

        L.d(TAG, " tableName : " + tableName);

        try {

            if (msgBean == null || messageDaoSession == null || msgBean.msgType.equals(MsgBean.MSG_TYPE_ADD_TO_BLACK_LIST) || msgBean.msgType.equals(MsgBean.MSG_TYPE_REMOVE_FROM_BLACK_LIST)) {
                return;
            }

            MsgBean mb = getMsgByUserIdAndPacketId(msgBean.userId, msgBean.packetId);

            if (mb != null) {
                return;
            }

            L.d(TAG, " <<<<<<<<<<<< begin sql >>>>>>>>>>>>> : ");

            String otherUserId = DBUtils.getOtherUserId(msgBean);

            msgBean.userId = otherUserId;

            msgBean.isSystem = MsgBean.IS_NORMAL_MSG;

            int isStranger = isStranger(otherUserId) ? 1 : 0;

            if (otherUserId != null && otherUserId.equals(TheLConstants.RELA_ACCOUNT)) {
                isStranger = 0;
            }

            int unReadCount = hasRead == 0 ? 1 : 0;

            int isWinked = 0;

            if (msgBean.msgType.equals(DBConstant.Message.MSG_TYPE_WINK)) {
                isWinked = 1;
            }

            if (msgBean.msgType.equals(DBConstant.Message.MSG_TYPE_FOLLOW) || msgBean.msgType.equals(DBConstant.Message.MSG_TYPE_UN_FOLLOW)) {

                insertFollowMsg(msgBean, unReadCount);

            } else if (msgBean.msgType.equals(DBConstant.Message.MSG_TYPE_WINK)) {

                insertWink(msgBean, unReadCount, isWinked);

            } else {

                boolean isWarningMsg = msgBean.msgType.equals(MsgBean.MSG_TYPE_MAY_CHEAT_MSG_TYPE) ||
                        msgBean.msgType.equals(MsgBean.MSG_TYPE_MAY_AD_MSG_TYPE) ||
                        msgBean.msgType.equals(MsgBean.MSG_TYPE_MAY_SEXUAL_MSG_TYPE) ||
                        msgBean.msgType.equals(MsgBean.MSG_TYPE_MAY_HACKED_MSG_TYPE);

                insertChatTable(msgBean, tableName);

                if (!isWarningMsg) {
                    if (isStranger == 1) {
                        insertStrangerMsg(msgBean, unReadCount, isWinked, isStranger);
                    } else {
                        insertFriendMsg(msgBean, unReadCount, isWinked, isStranger);
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        L.d(TAG, " <<<<<<<<<<<<end sql>>>>>>>>>>>>> : ");
    }

    public List<MsgTable> getStrangerMsg() {
        MsgTableDao msgTableDao = messageDaoSession.getMsgTableDao();
        return msgTableDao.queryBuilder().where(MsgTableDao.Properties.IsStranger.eq(1)).build().list();
    }

    public MsgTable getFirstStrangerMsg() {

        MsgTable msgTable = null;

        if (messageDaoSession != null) {
            msgTable = messageDaoSession.getMsgTableDao().queryBuilder().where(MsgTableDao.Properties.IsStranger.eq(1)).orderDesc(MsgTableDao.Properties.MsgTime).limit(1).build().unique();
        }

        return msgTable;
    }

    /**
     * 将消息插入与某人的聊天表
     *
     * @param
     */

    public void insertChatTable(MsgBean msgBean, String tableName) {
        if (messageDaoSession != null && msgBean != null) {
            //判断与某人聊天表是否存在,不存在就创建聊天表
            createTable(tableName);
            insertMsg(msgBean, tableName);
        }

    }

    /**
     * 将信息插入与某人聊天表中
     *
     * @param msgBean
     * @param tableName
     */
    public void insertMsg(MsgBean msgBean, String tableName) {
        String sql = "replace into " + tableName + "(" +
                DBConstant.Message.F_PACKETID + "," +
                DBConstant.Message.F_USERID + "," +
                DBConstant.Message.F_MSG_DIRECTION + "," +
                DBConstant.Message.F_MSG_TYPE + "," +
                DBConstant.Message.F_MSG_TIME + "," +
                DBConstant.Message.F_MSG_TEXT + "," +
                DBConstant.Message.F_AVATAR + "," +
                DBConstant.Message.F_USERNAME + "," +
                DBConstant.Message.F_MSG_STATUS + "," +
                DBConstant.Message.F_RECEIVEDID + "," +
                DBConstant.Message.F_MSG_LNG + "," +
                DBConstant.Message.F_MSG_LAT + "," +
                DBConstant.Message.F_MSG_MAPURL + "," +
                DBConstant.Message.F_MSG_IMAGE + "," +
                DBConstant.Message.F_MSG_HEIGHT + "," +
                DBConstant.Message.F_MSG_WIDTH + "," +
                DBConstant.Message.F_VOICETIME + "," +
                DBConstant.Message.F_FILEPATH + "," +
                DBConstant.Message.F_FROMUSERID + "," +
                DBConstant.Message.F_FROMNICKNAME + "," +
                DBConstant.Message.F_FROMAVATAR + "," +
                DBConstant.Message.F_FROMMESSAGEUSER + "," +
                DBConstant.Message.F_TOUSERID + "," +
                DBConstant.Message.F_TONICKNAME + "," +
                DBConstant.Message.F_TOAVATAR + "," +
                DBConstant.Message.F_TOMESSAGEUSER + "," +
                DBConstant.Message.F_HADREAD + "," +
                DBConstant.Message.F_HADPLAYED +
                ") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        L.d(TAG, "replace chat message sql: " + sql);

        String[] args = {
                msgBean.packetId,
                msgBean.userId,
                msgBean.msgDirection,
                msgBean.msgType,
                String.valueOf(msgBean.msgTime),
                msgBean.msgText,
                msgBean.avatar,
                msgBean.userName,
                msgBean.msgStatus,
                msgBean.receivedId,
                msgBean.msgLng,
                msgBean.msgLat,
                msgBean.msgMapUrl,
                msgBean.msgImage,
                msgBean.msgHeight,
                msgBean.msgWidth,
                msgBean.voiceTime,
                msgBean.filePath,
                msgBean.fromUserId,
                msgBean.fromNickname,
                msgBean.fromAvatar,
                msgBean.fromMessageUser,
                msgBean.toUserId,
                msgBean.toUserNickname,
                msgBean.toAvatar,
                msgBean.toMessageUser,
                String.valueOf(msgBean.hadRead),
                String.valueOf(msgBean.hadPlayed)};

        messageDaoSession.getDatabase().execSQL(sql, args);
    }

    /**
     * 判断表是否存在
     *
     * @param tableName
     */
    public void createTable(String tableName) {
        if (!DBUtils.isTableExist(messageDaoSession.getDatabase(), tableName)) {
            String sql = "create table " + tableName + "("
                    + DBConstant.Message.F_ID + " integer primary key AUTOINCREMENT,"
                    + DBConstant.Message.F_PACKETID + " text UNIQUE,"
                    + DBConstant.Message.F_USERID + " text,"
                    + DBConstant.Message.F_MSG_DIRECTION + " text,"
                    + DBConstant.Message.F_MSG_TYPE + " Long,"
                    + DBConstant.Message.F_MSG_TIME + " text,"
                    + DBConstant.Message.F_MSG_TEXT + " text,"
                    + DBConstant.Message.F_AVATAR + " text,"
                    + DBConstant.Message.F_USERNAME + " text,"
                    + DBConstant.Message.F_MSG_STATUS + " text,"
                    + DBConstant.Message.F_RECEIVEDID + " text,"
                    + DBConstant.Message.F_MSG_LNG + " text,"
                    + DBConstant.Message.F_MSG_LAT + " text,"
                    + DBConstant.Message.F_MSG_MAPURL + " text,"
                    + DBConstant.Message.F_MSG_IMAGE + " text,"
                    + DBConstant.Message.F_MSG_HEIGHT + " text,"
                    + DBConstant.Message.F_MSG_WIDTH + " text,"
                    + DBConstant.Message.F_VOICETIME + " text,"
                    + DBConstant.Message.F_FILEPATH + " text,"
                    + DBConstant.Message.F_FROMUSERID + " text,"
                    + DBConstant.Message.F_FROMNICKNAME + " text,"
                    + DBConstant.Message.F_FROMAVATAR + " text,"
                    + DBConstant.Message.F_FROMMESSAGEUSER + " text,"
                    + DBConstant.Message.F_TOUSERID + " text,"
                    + DBConstant.Message.F_TONICKNAME + " text,"
                    + DBConstant.Message.F_TOAVATAR + " text,"
                    + DBConstant.Message.F_TOMESSAGEUSER + " text,"
                    + DBConstant.Message.F_HADREAD + " integer,"
                    + DBConstant.Message.F_HADPLAYED + " integer" + ")";
            Log.d(TAG, "create chat table sql: " + sql);
            messageDaoSession.getDatabase().execSQL(sql);
        }
    }

    public void insertMsgTable(MsgTable msgTable) {

        if (messageDaoSession != null) {

            MsgTable mt = getMsgTableByUserId(msgTable.userId);

            if (mt == null) {

                messageDaoSession.getMsgTableDao().insert(msgTable);

            }

        }

    }

    /**
     * 获取MsgTable中的陌生人列
     *
     * @return
     */
    public MsgTable getMsgTableByUserId(String userId) {

        MsgTable msgTable = null;

        if (messageDaoSession != null) {
            MsgTableDao msgTableDao = messageDaoSession.getMsgTableDao();
            msgTable = msgTableDao.queryBuilder().where(MsgTableDao.Properties.UserId.eq(userId)).build().unique();
        }
        return msgTable;
    }

    /**
     * 获取聊天列表
     *
     * @param toUserId
     * @param limit
     * @param offset
     * @return
     */
    public List<MsgBean> findMsgsByUserIdAndPage(String toUserId, String limit, String offset) {

        List<MsgBean> list = new ArrayList<>();

        if (messageDaoSession != null) {

            if (!DBUtils.isTableExist(messageDaoSession.getDatabase(), DBUtils.getChatTableName(toUserId))) {
                return list;
            }

            String sql = "select * from " + DBUtils.getChatTableName(toUserId) + " order by " + DBConstant.Message.F_ID + " desc" + " limit ? offset ?";

            Cursor cursor = messageDaoSession.getDatabase().rawQuery(sql, new String[]{limit, offset});

            if (cursor != null && cursor.moveToFirst()) {

                do {

                    list.add(DBUtils.fromCursor(cursor));

                } while (cursor.moveToNext());

                cursor.close();

            }

        }

        return list;

    }


    public List<MsgBean> getMsgListByTableName(String tableName) {

        L.d(TAG, " tableName : " + tableName);

        List<MsgBean> list = new ArrayList<>();

        L.d(TAG, " 1 : " + messageDaoSession);

        if (messageDaoSession != null) {

            L.d(TAG, " 2 : " + tableName);

            boolean isTableExist = DBUtils.isTableExist(messageDaoSession.getDatabase(), tableName);

            L.d(TAG, " 3 : " + isTableExist);

            if (!isTableExist) {
                return list;
            }

            String sql = "select * from " + tableName + " order by " + DBConstant.Message.F_MSG_TIME + " desc";

            L.d(TAG, " sql : " + sql);

            Cursor cursor = messageDaoSession.getDatabase().rawQuery(sql, null);

            if (cursor != null && cursor.moveToFirst()) {

                do {

                    list.add(DBUtils.fromCursor(cursor));

                } while (cursor.moveToNext());

                cursor.close();

            }

        }

        return list;

    }

    /**
     * 获取关注列表
     *
     * @return
     */
    public List<MsgBean> getMsgListByTableNameAsync(String tableName) {

        return getMsgListByTableName(tableName);

    }

    /**
     * 根据 userId 和 packetId 获取消息
     *
     * @param userId
     * @param packetId
     * @return
     */
    public MsgBean getMsgByUserIdAndPacketId(String userId, String packetId) {

        MsgBean msgBean = null;

        if (messageDaoSession != null) {

            if (!DBUtils.isTableExist(messageDaoSession.getDatabase(), DBUtils.getChatTableName(userId))) {
                return null;
            }

            String sql = "select * from " + DBUtils.getChatTableName(userId) + " where " + DBConstant.Message.F_PACKETID + " =?";

            Cursor cursor = messageDaoSession.getDatabase().rawQuery(sql, new String[]{packetId});

            if (cursor != null && cursor.moveToFirst()) {

                do {

                    msgBean = DBUtils.fromCursor(cursor);

                } while (cursor.moveToNext());

                cursor.close();

            }


        }

        return msgBean;
    }

    /**
     * 更新发送聊天消息的状态
     *
     * @param msgBean
     * @return
     */
    public long updateMsgSendingStatus(MsgBean msgBean) {

        long id = -1;

        if (msgBean == null) {
            return -1;
        }

        boolean isExist = DBUtils.isTableExist(messageDaoSession.getDatabase(), DBUtils.getChatTableName(msgBean));

        if (!isExist) {
            return -1;
        }

        String sql = "update " + DBUtils.getChatTableName(msgBean) + " set " + DBConstant.Message.F_MSG_STATUS + " =? where " + DBConstant.Message.F_PACKETID + " =?";

        Cursor cursor = messageDaoSession.getDatabase().rawQuery(sql, new String[]{msgBean.msgStatus, msgBean.packetId});

        if (cursor != null && cursor.moveToFirst()) {

            id = cursor.getLong(0);

            cursor.close();

        }

        return id;
    }

    /**
     * 修改消息为挤眼消息
     */
    public void updateMsgTableWithWink(MsgTable msgTable) {

        if (messageDaoSession != null && msgTable != null) {
            messageDaoSession.getMsgTableDao().update(msgTable);
        }

    }

    /**
     * 更新未读数
     *
     * @param msgTable
     */
    public void updateMsgUnread(MsgTable msgTable) {

        if (messageDaoSession != null && msgTable != null) {

            MsgTableDao msgTableDao = messageDaoSession.getMsgTableDao();

            int count = 0;

            if (msgTable.isStranger == 1) {

                MsgTable strangerMsgTable = msgTableDao.queryBuilder().where(MsgTableDao.Properties.UserId.eq(getStrangerTableName())).build().unique();

                if (strangerMsgTable != null) {

                    if (strangerMsgTable.unreadCount > msgTable.unreadCount) {
                        count = strangerMsgTable.unreadCount - msgTable.unreadCount;
                    }

                    strangerMsgTable.unreadCount = count;

                    msgTableDao.update(strangerMsgTable);
                }

            }

//            msgTable.unreadCount = 0;

            msgTableDao.update(msgTable);

            pushUpdateMsg(msgTable);
        }

    }

    public long deleteMsgById(String tableName, String packetId) {

        long count = -1;

        if (messageDaoSession != null) {

            String sql = "delete from " + tableName + " where " + DBConstant.Message.F_PACKETID + " =?";

            Cursor cursor = messageDaoSession.getDatabase().rawQuery(sql, new String[]{packetId});

            if (cursor != null && cursor.moveToFirst()) {

                do {

                    count = cursor.getLong(0);

                } while (cursor.moveToNext());

                cursor.close();

            }
        }

        return count;
    }

    public MsgBean getLastMsg(String tableName) {

        MsgBean msgBean = null;

        String sql = "select * from " + tableName + " order by " + DBConstant.Message.F_MSG_TIME + " desc limit 1";

        Cursor cursor = messageDaoSession.getDatabase().rawQuery(sql, null);

        if (cursor != null && cursor.moveToFirst()) {

            msgBean = DBUtils.fromCursor(cursor);

            cursor.close();

        }

        return msgBean;

    }

    public MsgTable findMsgTableByOtherUserId(String otherUserId) {

        MsgTable msgTable = null;

        if (messageDaoSession != null) {

            MsgTableDao msgTableDao = messageDaoSession.getMsgTableDao();

            msgTable = msgTableDao.queryBuilder().where(MsgTableDao.Properties.UserId.eq(otherUserId)).build().unique();

        }

        return msgTable;
    }

    public List<MsgTable> getMsgList(int isStranger) {

        List<MsgTable> msgTables = getMsgListByIsStranger(isStranger);

        return msgTables;
    }

    private List<MsgTable> getMsgListByIsStranger(int isStranger) {
        MsgTableDao msgTableDao = messageDaoSession.getMsgTableDao();
        return msgTableDao.queryBuilder().where(MsgTableDao.Properties.IsStranger.eq(isStranger)).orderDesc(MsgTableDao.Properties.MsgTime).build().list();
    }

    public String getFollowTableName() {
        return DBConstant.Message.FOLLOW_TABLE_NAME;
    }

    public String getStrangerTableName() {
        return DBConstant.Message.STRANGER_TABLE_NAME;
    }

    /**
     * 插入好友请求
     *
     * @param msgBean
     * @param unReadCount
     */
    public MsgTable insertRequestDataToMsgTable(MsgBean msgBean, int unReadCount) {

        if (messageDaoSession == null) {
            return null;
        }

        MsgTable msgTable = null;

        MsgTableDao msgTableDao = messageDaoSession.getMsgTableDao();

        if (msgTableDao != null) {
            msgTable = msgTableDao.queryBuilder().where(MsgTableDao.Properties.UserId.eq(DBConstant.Message.REQUEST_TABLE_NAME)).build().unique();

            if (msgTable != null) {
                msgTableDao.delete(msgTable);
            }
            msgTable = new MsgTable(
                    null, msgBean.packetId, DBConstant.Message.REQUEST_TABLE_NAME
                    , DBUtils.getToUserName(msgBean), msgBean.avatar
                    , msgBean.msgTime, msgBean.msgText
                    , msgBean.msgType, unReadCount, 0, 0
                    , msgBean.toAvatar, msgBean.fromAvatar, msgBean.fromUserId
                    , msgBean.toUserId, msgBean.toUserNickname
                    , msgBean.fromNickname, msgBean.msgDirection, msgBean.isSystem, "");
            messageDaoSession.getMsgTableDao().insert(msgTable);
        }
        pushUpdateMsg(msgTable);
        return msgTable;
    }

    /**
     * 获取请求数量
     *
     * @return
     */
    public int findRequestCountToMsgTable() {

        if (messageDaoSession == null) {
            return -1;
        }

        MsgTableDao msgTableDao = messageDaoSession.getMsgTableDao();

        MsgTable msgTable = msgTableDao.queryBuilder().where(MsgTableDao.Properties.MsgType.eq(MsgBean.MSG_TYPE_REQUEST)).build().unique();


        if (msgTable == null) {
            return -1;
        }

        messageDaoSession.getMsgTableDao().deleteInTx(msgTable);

        return 1;
    }

    /**
     * 切换好友关系
     *
     * @param userId
     * @param msgBean
     * @param isStranger
     */
    public void changeRelationship(String userId, final MsgBean msgBean, int isStranger) {

        //todo Attempt to write to field 'java.lang.String com.thel.db.table.message.MsgTable.fromNickname' on a null object reference

        try {

            if (userId != null && userId.equals(TheLConstants.RELA_ACCOUNT)) {
                isStranger = 0;
            }

            MsgTable msgTable;

            if (messageDaoSession != null) {

                MsgTableDao msgTableDao = messageDaoSession.getMsgTableDao();

                msgTable = msgTableDao.queryBuilder().where(MsgTableDao.Properties.UserId.eq(userId)).build().unique();

                if (msgTable != null) {

                    msgTable.isStranger = isStranger;

                    msgTableDao.update(msgTable);

                } else {

                    if (msgBean == null) {
                        return;
                    }

                    msgTable = new MsgTable(null, msgBean.packetId, DBUtils.getOtherUserId(msgBean)
                            , DBUtils.getOtherUserName(msgBean), msgBean.avatar
                            , msgBean.msgTime, msgBean.msgText
                            , msgBean.msgType, 1, msgBean.isWinked, 0
                            , msgBean.toAvatar, msgBean.fromAvatar, msgBean.fromUserId
                            , msgBean.toUserId, msgBean.toUserNickname
                            , msgBean.fromNickname, msgBean.msgDirection, MsgBean.IS_NORMAL_MSG, "");

                    msgTableDao.insert(msgTable);

                }

                MsgTable strangerMsgTable = msgTableDao.queryBuilder().where(MsgTableDao.Properties.IsSystem.eq(MsgBean.IS_STRANGER_MSG)).build().unique();

                if (isStranger == 1) {

                    if (strangerMsgTable == null) {

                        String strangerTableName = getStrangerTableName();

                        L.d(TAG, " changeRelationship getStrangerTableName strangerTableName : " + strangerTableName);

                        int unReadCount = msgTable.unreadCount;

                        String fromNickname = msgTable.fromNickname == null ? "" : msgTable.fromNickname;

                        messageDaoSession.getMsgTableDao().insert(new MsgTable(
                                null, msgTable.packetId, strangerTableName
                                , msgTable.userName, msgTable.avatar
                                , msgTable.msgTime, msgTable.msgText
                                , msgTable.msgType, unReadCount, msgTable.isWinked, 0
                                , msgTable.toAvatar, msgTable.fromAvatar, msgTable.fromUserId
                                , msgTable.toUserId, msgTable.toUserNickname
                                , fromNickname, msgTable.msgDirection, MsgBean.IS_STRANGER_MSG, ""));

                    } else {

                        strangerMsgTable.userName = msgTable.userName;
                        strangerMsgTable.msgType = msgTable.msgType;
                        strangerMsgTable.msgText = msgTable.msgText;
                        strangerMsgTable.msgTime = msgTable.msgTime;
                        if (strangerMsgTable.msgTime < DBConstant.STICK_TOP_BASE_TIME) {
                            strangerMsgTable.msgTime = msgTable.msgTime;
                        }
                        strangerMsgTable.unreadCount += msgTable.unreadCount;

                        if (msgTable.msgType.equals(DBConstant.Message.MSG_TYPE_WINK)) {
                            strangerMsgTable.isWinked = 1;
                        }

                        messageDaoSession.getMsgTableDao().update(strangerMsgTable);

                    }
                }

                if (isStranger == 0) {

                    List<MsgTable> msgTables = getMsgListByIsStranger(1);

                    if (msgTables == null || msgTables.size() == 0) {

                        deleteStranger();

                    } else {

                        List<MsgTable> list = getMsgListByIsStranger(1);


                        MsgTable mt = list.get(list.size() - 1);
                        strangerMsgTable.fromNickname = mt.userName;
                        strangerMsgTable.fromAvatar = mt.avatar;
                        strangerMsgTable.msgType = mt.msgType;
                        strangerMsgTable.msgText = mt.msgText;
                        if (strangerMsgTable.msgTime < DBConstant.STICK_TOP_BASE_TIME) {
                            strangerMsgTable.msgTime = mt.msgTime;
                        }
                        strangerMsgTable.isWinked = mt.isWinked;
                        messageDaoSession.getMsgTableDao().update(strangerMsgTable);

                        ChatServiceManager.getInstance().pushUpdateMsg(strangerMsgTable);

                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 删除陌生人列
     */
    public void deleteStranger() {
        if (messageDaoSession != null) {
            MsgTableDao msgTableDao = messageDaoSession.getMsgTableDao();

            MsgTable msgTable = msgTableDao.queryBuilder().where(MsgTableDao.Properties.UserId.eq(getStrangerTableName())).build().unique();

            if (msgTable != null) {

                msgTableDao.delete(msgTable);

            }

        }
    }

    /**
     * 获取陌生人未读数量
     *
     * @return
     */
    public int getStrangerUnReadCount() {
        List<MsgTable> msgTables = getMsgListByIsStranger(1);

        int countNum = 0;

        for (MsgTable msgTable : msgTables) {
            countNum += msgTable.unreadCount;
        }

        return countNum;
    }

    /**
     * 是否是陌生人
     *
     * @param userId
     * @return
     */
    public boolean isStranger(String userId) {

        if (userId == null) {
            return true;
        }

        //获取本地缓存好友列表
        List<String> friendList = ChatServiceManager.getInstance().getUserList();

        if (friendList == null) {

            friendList = this.userList;

        }

        for (int i = 0; i < friendList.size(); i++) {
            if (userId.equals(friendList.get(i))) {
                return false;
            }
            if (i == friendList.size() - 1) {
                return true;
            }

        }
        return true;

    }

    /**
     * 获取最后一条聊天的时间
     *
     * @return
     */
    public long getLastMsgTime(String userId) {

        if (userId == null) {
            return -1;
        }

        if (userId.equals(DBConstant.Message.STRANGER_TABLE_NAME)) {

            MsgTable msgTable = getFirstStrangerMsg();

            if (msgTable == null) {
                return -1;
            }

            userId = msgTable.userId;
        }

        long time = 0;

        String sql = "select * from " + DBUtils.getChatTableName(userId) + " order by " + DBConstant.Message.F_MSG_TIME + " desc limit 1";

        Cursor cursor = null;
        try {
            cursor = messageDaoSession.getDatabase().rawQuery(sql, null);
            if (cursor != null && cursor.moveToFirst()) {

                MsgBean msgBean = DBUtils.fromCursor(cursor);

                time = msgBean.msgTime;

                cursor.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }



        return time;

    }

    /**
     * 置顶
     *
     * @param msgTable
     * @return
     */
    public long stickTop(MsgTable msgTable) {

        long time = DBConstant.STICK_TOP_BASE_TIME + 1 + getStickTopCount();

        if (messageDaoSession != null) {

            MsgTableDao msgTableDao = messageDaoSession.getMsgTableDao();

            msgTable.msgTime = time;

            msgTableDao.update(msgTable);

        }

        return time;

    }

    /**
     * 取消置顶
     *
     * @param msgTable
     * @return
     */
    public long resetStickTop(MsgTable msgTable) {

        long time = getWinkLastMsgTime();

        L.d("ChatOperationsActivity", " time : " + time);

        if (messageDaoSession != null) {

            MsgTableDao msgTableDao = messageDaoSession.getMsgTableDao();

            msgTable.msgTime = time;

            msgTableDao.update(msgTable);

        }

        return time;
    }

    public long getWinkLastMsgTime() {

        long time = 0;

        if (messageDaoSession != null) {

            WinkMsgTableDao winkMsgTableDao = messageDaoSession.getWinkMsgTableDao();

            List<WinkMsgTable> winkMsgTable = winkMsgTableDao.queryBuilder().limit(1).orderDesc(WinkMsgTableDao.Properties.Id).list();

            if (winkMsgTable != null && winkMsgTable.size() > 0) {
                time = winkMsgTable.get(0).msgTime;
            }

        }

        return time;

    }

    /**
     * 获取置顶的数量
     *
     * @return
     */
    public long getStickTopCount() {

        long count = 0;

        if (messageDaoSession != null) {
            count = messageDaoSession.getMsgTableDao().queryBuilder().where(MsgTableDao.Properties.MsgTime.gt(DBConstant.STICK_TOP_BASE_TIME)).buildCount().count();
        }

        return count;
    }

    /**
     * 删除一列聊天
     *
     * @param userId
     */
    public void removeMsgTable(String userId) {
        MsgTableDao msgTableDao = messageDaoSession.getMsgTableDao();
        if (msgTableDao != null) {
            MsgTable msgTable = msgTableDao.queryBuilder().where(MsgTableDao.Properties.UserId.eq(userId)).build().unique();
            if (msgTable != null) {
                msgTableDao.delete(msgTable);
            }
        }
    }

    /**
     * 更新一列聊天
     *
     * @param msgBean
     */
    public void updateMsgTable(MsgBean msgBean) {

        MsgTableDao msgTableDao = messageDaoSession.getMsgTableDao();
        if (msgTableDao != null) {
            String userId = DBUtils.getOtherUserId(msgBean);
            MsgTable msgTable = msgTableDao.queryBuilder().where(MsgTableDao.Properties.UserId.eq(userId)).build().unique();
            if (msgTable != null) {
                msgTable.fromNickname = DBUtils.getOtherUserName(msgBean);
                msgTable.fromAvatar = DBUtils.getOtherAvatar(msgBean);
                msgTable.msgType = msgBean.msgType;
                msgTable.msgText = msgBean.msgText;
                if (msgTable.msgTime < DBConstant.STICK_TOP_BASE_TIME) {
                    msgTable.msgTime = msgBean.msgTime;
                }
                msgTableDao.update(msgTable);
                pushUpdateMsg(msgTable);
            }

            if (msgBean.msgType.equals(DBConstant.Message.MSG_TYPE_FOLLOW)) {
                MsgTable strangerMsgTable = msgTableDao.queryBuilder().where(MsgTableDao.Properties.UserId.eq(getStrangerTableName())).build().unique();

                if (strangerMsgTable != null) {
                    strangerMsgTable.fromNickname = DBUtils.getOtherUserName(msgBean);
                    strangerMsgTable.fromAvatar = DBUtils.getOtherAvatar(msgBean);
                    strangerMsgTable.msgType = msgBean.msgType;
                    strangerMsgTable.msgText = msgBean.msgText;
                    if (strangerMsgTable.msgTime < DBConstant.STICK_TOP_BASE_TIME) {
                        strangerMsgTable.msgTime = msgBean.msgTime;
                    }
                    msgTableDao.update(strangerMsgTable);
                    pushUpdateMsg(strangerMsgTable);
                }
            }

        }
    }

    public void deleteMsgTable(String userId) {

        if (messageDaoSession != null) {
            MsgTableDao msgTableDao = messageDaoSession.getMsgTableDao();
            MsgTable msgTable = msgTableDao.queryBuilder().where(MsgTableDao.Properties.UserId.eq(userId)).build().unique();
            if (msgTable != null) {

                msgTableDao.delete(msgTable);

                if (msgTable.isStranger == 1) {

                    List<MsgTable> msgTables = getStrangerMsg();

                    if (msgTables == null || msgTables.size() == 0) {

                        removeMsgTable(getStrangerTableName());

                        pushDeleteMsgTagble(getStrangerTableName());

                    }
                } else {

                    pushDeleteMsgTagble(userId);

                }
            }
        }

    }

    private synchronized void pushUpdateMsg(MsgTable msgTable) {

        if (chatServiceBinder != null) {
            chatServiceBinder.pushUpdateMsg(msgTable);
        }
    }

    private synchronized void pushDeleteMsgTagble(String userId) {

        if (chatServiceBinder != null) {
            chatServiceBinder.pushDeleteMsgTable(userId);
        }
    }

    /**
     * 插入用户消息
     *
     * @param userInfoTable
     */
    public void insertUserInfo(UserInfoTable userInfoTable) {

        if (messageDaoSession != null) {

            UserInfoTable uit = getUserInfo();

            UserInfoTableDao userInfoTableDao = messageDaoSession.getUserInfoTableDao();

            if (uit == null) {
                userInfoTable.token = "";
                userInfoTable.msgCursor = "0";
                userInfoTableDao.insert(userInfoTable);
            } else {
                uit.key = userInfoTable.key;
                uit.id = userInfoTable.id;
                uit.nickName = userInfoTable.nickName;
                uit.userName = userInfoTable.userName;
                uit.avatar = userInfoTable.avatar;
                uit.roleName = userInfoTable.roleName;
                uit.messageUser = userInfoTable.messageUser;
                uit.imIps = userInfoTable.imIps;
                userInfoTableDao.update(uit);
            }

        }

    }

    /**
     * 获取用户消息
     *
     * @return
     */
    public synchronized UserInfoTable getUserInfo() {

        try {

            List<UserInfoTable> userInfoTable;

            if (messageDaoSession != null) {

                UserInfoTableDao userInfoTableDao = messageDaoSession.getUserInfoTableDao();

                userInfoTable = userInfoTableDao.queryBuilder().build().list();

                if (userInfoTable != null && userInfoTable.size() > 0) {
                    return userInfoTable.get(0);
                }

                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new UserInfoTable();
    }

    public synchronized String getKey() {

        UserInfoTable userInfoTable = getUserInfo();

        if (userInfoTable == null) {
            return "";
        }

        return userInfoTable.key;
    }

    public synchronized String getMyUserId() {

        UserInfoTable userInfoTable = getUserInfo();

        if (userInfoTable == null) {
            return "";
        }

        return String.valueOf(userInfoTable.id);
    }

    public synchronized String getMsgCursor() {

        UserInfoTable userInfoTable = getUserInfo();

        if (userInfoTable == null) {
            return "0";
        }

        return String.valueOf(userInfoTable.msgCursor);
    }

    public synchronized void setMsgCursor(String cursor) {

        if (messageDaoSession != null) {
            UserInfoTable userInfoTable = getUserInfo();
            if (userInfoTable != null) {
                userInfoTable.msgCursor = cursor;
                messageDaoSession.getUserInfoTableDao().update(userInfoTable);
            }
        }

    }

    public synchronized String getIPs() {

        UserInfoTable userInfoTable = getUserInfo();

        if (userInfoTable == null) {
            return "";
        }

        return userInfoTable.imIps;
    }

    public void updateStrangerUnreadCount(int count, int isWinked) {

        if (messageDaoSession != null) {
            MsgTableDao msgTableDao = messageDaoSession.getMsgTableDao();
            MsgTable msgTable = msgTableDao.queryBuilder().where(MsgTableDao.Properties.UserId.eq(getStrangerTableName())).build().unique();
            if (msgTable != null) {
                msgTable.unreadCount = count;
                msgTable.isWinked = isWinked;
                msgTableDao.update(msgTable);
                pushUpdateMsg(msgTable);
            }
        }
    }

    public void winkMsgSaveToDB(MsgBean msgBean) {

        if (msgBean == null) {
            return;
        }

        try {

            int isStranger = DBUtils.isStranger(chatServiceBinder.getUserList(), DBUtils.getOtherUserId(msgBean));

            if (isStranger == 1) {
                insertStrangerMsg(msgBean, 0, 0, isStranger);
            } else {
                insertFriendMsg(msgBean, 0, 0, isStranger);
            }

            insertChatTable(msgBean, DBUtils.getChatTableName(msgBean));

            if (chatServiceBinder != null) {
                chatServiceBinder.pushNewMsgComing(msgBean);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public List<WinkMsgTable> getWinkList(int limit, int offset) {

        if (messageDaoSession != null) {

            return messageDaoSession.getWinkMsgTableDao().queryBuilder().limit(limit).offset(offset).orderDesc(WinkMsgTableDao.Properties.Id).build().list();
        }

        return null;
    }

    /**
     * 获取陌生人或好友挤眼数
     *
     * @param isWink
     * @return
     */
    public long getStrangerWinkOrUnWinkCount(int isWink) {

        long count = 0;

        if (messageDaoSession != null) {

            count = messageDaoSession.getMsgTableDao().queryBuilder().where(MsgTableDao.Properties.IsWinked.eq(isWink), MsgTableDao.Properties.IsStranger.eq(1)).buildCount().count();

        }

        return count;

    }

    public void insertWinkMsgByMsgBean(MsgBean msgBean, int isTimeTip) {

        WinkMsgTable winkMsgTable = new WinkMsgTable();

        winkMsgTable.avatar = DBUtils.getOtherAvatar(msgBean);

        winkMsgTable.userName = DBUtils.getOtherUserName(msgBean);

        winkMsgTable.isWinked = 1;

        winkMsgTable.userId = DBUtils.getOtherUserId(msgBean);

        winkMsgTable.isTimeTip = isTimeTip;

        winkMsgTable.msgTime = msgBean.msgTime;

        insertWinkMsg(winkMsgTable);

    }

    public void insertWinkMsg(WinkMsgTable winkMsgTable) {
        if (messageDaoSession != null) {
            WinkMsgTableDao winkMsgTableDao = messageDaoSession.getWinkMsgTableDao();
            if (winkMsgTableDao != null) {

                winkMsgTableDao.insert(winkMsgTable);

            }
        }
    }

    public void deleteWinkMsg(WinkMsgTable winkMsgTable) {
        if (messageDaoSession != null) {
            WinkMsgTableDao winkMsgTableDao = messageDaoSession.getWinkMsgTableDao();
            if (winkMsgTableDao != null) {
                winkMsgTableDao.delete(winkMsgTable);
            }

            long count = winkMsgTableDao.queryBuilder().buildCount().count();

            if (count == 0) {

                removeMsgTable(DBConstant.Message.WINK_TABLE_NAME);

                pushDeleteMsgTagble(DBConstant.Message.WINK_TABLE_NAME);

            }

        }
    }

    public void updateWinkMsg(WinkMsgTable winkMsgTable) {
        if (messageDaoSession != null) {
            WinkMsgTableDao winkMsgTableDao = messageDaoSession.getWinkMsgTableDao();
            if (winkMsgTableDao != null) {

                winkMsgTableDao.update(winkMsgTable);

            }
        }
    }

    public void attachToOldDB(String path, String name) {
        if (messageDaoSession != null) {
            messageDaoSession.getDatabase().execSQL(String.format("ATTACH DATABASE \'%s\' AS %s", path, name));
        }
    }

    public void detachToOldDB(String name) {
        if (messageDaoSession != null) {
            messageDaoSession.getDatabase().execSQL(String.format("DETACH DATABASE %s", name));
        }
    }

    /**
     * 老的数据库向新的数据库迁移数据
     *
     * @param newDBName
     * @param newTableName
     * @param oldDBName
     * @param oldTableName
     */
    public void tableTransfer(String newDBName, String newTableName, String oldDBName, String oldTableName) {
        if (messageDaoSession != null) {
            String sql = "insert into " + newDBName + "." + newTableName + " select * from " + oldDBName + "." + oldTableName;
            messageDaoSession.getDatabase().execSQL(sql);
        }
    }

    /**
     * 删除表
     *
     * @param tableName
     */
    public void dropTable(String tableName) {
        if (messageDaoSession != null) {
            String sql = "drop table if exists " + tableName;
            messageDaoSession.getDatabase().execSQL(sql);
        }
    }

    public void clearWinksTable() {
        if (messageDaoSession != null) {
            WinkMsgTableDao winkMsgTableDao = messageDaoSession.getWinkMsgTableDao();
            if (winkMsgTableDao != null) {
                winkMsgTableDao.deleteAll();
            }
        }
    }

    public List<MsgTable> getAllMsgTables() {
        if (messageDaoSession != null) {
            MsgTableDao msgTableDao = messageDaoSession.getMsgTableDao();
            if (msgTableDao != null) {
                return msgTableDao.queryBuilder().build().list();
            }
        }
        return null;
    }

    private void insertFollowMsg(MsgBean msgBean, int unReadCount) {

        String followTableName = getFollowTableName();

        MsgTable msgTable = getMsgTableByUserId(followTableName);

        msgBean.isSystem = MsgBean.IS_FOLLOW_MSG;

        msgBean.fromAvatar = DBUtils.getOtherAvatar(msgBean);

        if (msgTable != null) {
            msgTable.fromNickname = DBUtils.getOtherUserName(msgBean);
            msgTable.fromAvatar = DBUtils.getOtherAvatar(msgBean);
            msgTable.msgType = msgBean.msgType;
            msgTable.msgText = msgBean.msgText;
            if (msgTable.msgTime < DBConstant.STICK_TOP_BASE_TIME) {
                msgTable.msgTime = msgBean.msgTime;
            }
            msgTable.unreadCount += unReadCount;

            messageDaoSession.getMsgTableDao().update(msgTable);

            pushUpdateMsg(msgTable);

        } else {

            msgTable = new MsgTable(
                    null, msgBean.packetId, followTableName
                    , DBUtils.getOtherUserName(msgBean), DBUtils.getOtherAvatar(msgBean)
                    , msgBean.msgTime, msgBean.msgText
                    , msgBean.msgType, unReadCount, msgBean.isWinked, 0
                    , msgBean.toAvatar, msgBean.fromAvatar, msgBean.fromUserId
                    , msgBean.toUserId, msgBean.toUserNickname
                    , msgBean.fromNickname, msgBean.msgDirection, MsgBean.IS_FOLLOW_MSG, "");

            messageDaoSession.getMsgTableDao().insert(msgTable);

            pushUpdateMsg(msgTable);

        }

        insertChatTable(msgBean, followTableName);
    }

    private void insertWink(MsgBean msgBean, int unReadCount, int isWinked) {

        msgBean.userId = DBConstant.Message.WINK_TABLE_NAME;

        msgBean.isSystem = MsgBean.IS_WINK_MSG;

        MsgTable msgTable = findMsgTableByOtherUserId(msgBean.userId);

        if (msgTable != null) {

            if (msgBean.msgTime - msgTable.msgTime > TEN_MIN) {
                insertWinkMsgByMsgBean(msgBean, 1);
            }

            msgTable.unreadCount += unReadCount;
            if (msgTable.msgTime < DBConstant.STICK_TOP_BASE_TIME) {
                msgTable.msgTime = msgBean.msgTime;
            }

            messageDaoSession.getMsgTableDao().update(msgTable);

            pushUpdateMsg(msgTable);

        } else {

            msgTable = new MsgTable(
                    null, msgBean.packetId, msgBean.userId
                    , DBUtils.getOtherUserName(msgBean), DBUtils.getOtherAvatar(msgBean)
                    , msgBean.msgTime, msgBean.msgText
                    , msgBean.msgType, unReadCount, isWinked, 0
                    , msgBean.toAvatar, msgBean.fromAvatar, msgBean.fromUserId
                    , msgBean.toUserId, msgBean.toUserNickname
                    , msgBean.fromNickname, msgBean.msgDirection, msgBean.isSystem, "");

            messageDaoSession.getMsgTableDao().insert(msgTable);

            pushUpdateMsg(msgTable);

            insertWinkMsgByMsgBean(msgBean, 1);
        }

        insertWinkMsgByMsgBean(msgBean, 0);
    }

    public long getWinkMsgCount() {
        if (messageDaoSession != null) {
            return messageDaoSession.getWinkMsgTableDao().queryBuilder().buildCount().count();
        }
        return 0;
    }

    private void insertStrangerMsg(MsgBean msgBean, int unReadCount, int isWinked, int isStranger) {

        MsgTable msgTable = findMsgTableByOtherUserId(DBUtils.getOtherUserId(msgBean));

        if (msgTable == null) {

            msgBean.isSystem = MsgBean.IS_NORMAL_MSG;

            msgTable = new MsgTable(
                    null, msgBean.packetId, DBUtils.getOtherUserId(msgBean)
                    , DBUtils.getOtherUserName(msgBean), DBUtils.getOtherAvatar(msgBean)
                    , msgBean.msgTime, msgBean.msgText
                    , msgBean.msgType, unReadCount, isWinked, isStranger
                    , msgBean.toAvatar, msgBean.fromAvatar, msgBean.fromUserId
                    , msgBean.toUserId, msgBean.toUserNickname
                    , msgBean.fromNickname, msgBean.msgDirection, msgBean.isSystem, "");

            messageDaoSession.getMsgTableDao().insert(msgTable);

            pushUpdateMsg(msgTable);

        } else {
            msgTable.fromNickname = DBUtils.getOtherUserName(msgBean);
            msgTable.avatar = DBUtils.getOtherAvatar(msgBean);
            msgTable.msgType = msgBean.msgType;
            msgTable.msgText = msgBean.msgText;
            if (msgTable.msgTime < DBConstant.STICK_TOP_BASE_TIME) {
                msgTable.msgTime = msgBean.msgTime;
            }
            msgTable.unreadCount += unReadCount;
            if (isWinked > 0) {
                msgTable.isWinked = 1;
            }
            messageDaoSession.getMsgTableDao().update(msgTable);
            pushUpdateMsg(msgTable);

        }

        if (isStranger == 1) {

            msgTable = getMsgTableByUserId(DBConstant.Message.STRANGER_TABLE_NAME);

            //好友列表中否存在陌生人列
            if (msgTable != null) {

                msgTable.fromNickname = DBUtils.getOtherUserName(msgBean);
                msgTable.avatar = DBUtils.getOtherAvatar(msgBean);
                msgTable.msgType = msgBean.msgType;
                msgTable.msgText = msgBean.msgText;
                if (msgTable.msgTime < DBConstant.STICK_TOP_BASE_TIME) {
                    msgTable.msgTime = msgBean.msgTime;
                }
                msgTable.unreadCount += unReadCount;
                if (isWinked > 0) {
                    msgTable.isWinked = 1;
                }
                messageDaoSession.getMsgTableDao().update(msgTable);

                pushUpdateMsg(msgTable);


            } else {

                msgBean.isSystem = MsgBean.IS_STRANGER_MSG;

                msgTable = new MsgTable(
                        null, msgBean.packetId, DBConstant.Message.STRANGER_TABLE_NAME
                        , DBUtils.getOtherUserName(msgBean), DBUtils.getOtherAvatar(msgBean)
                        , msgBean.msgTime, msgBean.msgText
                        , msgBean.msgType, unReadCount, isWinked, 0
                        , msgBean.toAvatar, msgBean.fromAvatar, msgBean.fromUserId
                        , msgBean.toUserId, msgBean.toUserNickname
                        , msgBean.fromNickname, msgBean.msgDirection, msgBean.isSystem, "");

                messageDaoSession.getMsgTableDao().insert(msgTable);

                msgBean.userId = DBUtils.getOtherUserId(msgBean);

                pushUpdateMsg(msgTable);

            }
        }

    }

    private void insertFriendMsg(MsgBean msgBean, int unReadCount, int isWinked, int isStranger) {

        MsgTable msgTable = getMsgTableByUserId(DBUtils.getOtherUserId(msgBean));

        if (msgTable == null) {

            msgBean.userId = getStrangerTableName();

            msgTable = new MsgTable(
                    null, msgBean.packetId, DBUtils.getOtherUserId(msgBean)
                    , DBUtils.getOtherUserName(msgBean), DBUtils.getOtherAvatar(msgBean)
                    , msgBean.msgTime, msgBean.msgText
                    , msgBean.msgType, unReadCount, isWinked, isStranger
                    , msgBean.toAvatar, msgBean.fromAvatar, msgBean.fromUserId
                    , msgBean.toUserId, msgBean.toUserNickname
                    , msgBean.fromNickname, msgBean.msgDirection, msgBean.isSystem, "");

            messageDaoSession.getMsgTableDao().insert(msgTable);

            pushUpdateMsg(msgTable);


        } else {

            msgTable.fromNickname = DBUtils.getOtherUserName(msgBean);
            msgTable.avatar = DBUtils.getOtherAvatar(msgBean);
            msgTable.msgType = msgBean.msgType;
            msgTable.msgText = msgBean.msgText;
            if (msgTable.msgTime < DBConstant.STICK_TOP_BASE_TIME) {
                msgTable.msgTime = msgBean.msgTime;
            }
            msgTable.unreadCount += unReadCount;
            if (isWinked > 0) {
                msgTable.isWinked = 1;
            }
            messageDaoSession.getMsgTableDao().update(msgTable);

            pushUpdateMsg(msgTable);

        }

    }


    public synchronized String getToken() {

        UserInfoTable userInfoTable = getUserInfo();

        if (userInfoTable == null || userInfoTable.token == null) {
            return "";
        }

        return userInfoTable.token;
    }

    public synchronized void setToken(String token) {

        if (messageDaoSession != null) {
            UserInfoTable userInfoTable = getUserInfo();
            if (userInfoTable != null) {
                userInfoTable.token = token;
                messageDaoSession.getUserInfoTableDao().update(userInfoTable);
            }
        }

    }

    public boolean isMsgCreated(MsgBean msgBean) {

        String tableName = DBUtils.getChatTableName(msgBean);

        List<MsgBean> list = new ArrayList<>();

        if (messageDaoSession != null) {

            boolean isTableExist = DBUtils.isTableExist(messageDaoSession.getDatabase(), tableName);

            if (!isTableExist) {
                return false;
            }

            String sql = "select * from " + tableName + " where " + DBConstant.Message.F_MSG_TYPE + " =?";

            String[] args = {MsgBean.MSG_TYPE_STRANGER_WARNING};

            Cursor cursor = messageDaoSession.getDatabase().rawQuery(sql, args);

            if (cursor != null && cursor.moveToFirst()) {

                do {

                    list.add(DBUtils.fromCursor(cursor));

                } while (cursor.moveToNext());

                cursor.close();

            }

        }

        return list.size() > 0;
    }

    public synchronized int getUnreadMsgCount() {

        int unreadCount = 0;

        if (messageDaoSession != null) {

            List<MsgTable> msgTables = messageDaoSession.getMsgTableDao().queryBuilder().where(MsgTableDao.Properties.IsStranger.eq(0)).build().list();

            for (MsgTable ms : msgTables) {
                unreadCount += ms.unreadCount;
            }

        }

        return unreadCount;

    }

    public synchronized void disconnectDB() {
        if (messageDaoSession != null) {
            messageDaoSession.clear();
            messageDaoSession.getDatabase().close();
            messageDaoSession = null;
            userId = "";
            if (userList != null) {
                userList.clear();
            }
        }
    }

}
