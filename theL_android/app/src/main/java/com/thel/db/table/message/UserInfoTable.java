package com.thel.db.table.message;

import android.os.Parcel;
import android.os.Parcelable;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class UserInfoTable implements Parcelable {

    /**
     * 主键，autoincrement设置自增
     */
    @Id(autoincrement = true)
    @Property(nameInDb = "id")
    public Long _id;

    /**
     * 登录,注册获取用户标识
     */
    @Property(nameInDb = "key")
    public String key = "";

    /**
     * id
     */
    @Property(nameInDb = "userId")
    public String id;

    /**
     * 是否已填过用户资料，0：未填过 1：已填过
     */
    @Property(nameInDb = "isNew")
    public int isNew;

    /**
     * 是否已初始化过昵称、thelId、头像 0:未初始化 1：已初始化
     */
    @Property(nameInDb = "isInit")
    public int isInit;

    /********************  必填项 start ******************/

    /**
     * 昵称
     */
    @Property(nameInDb = "nickName")
    public String nickName;

    /**
     * 生日 yyyy-MM-dd
     */
    @Property(nameInDb = "birthday")
    public String birthday;

    /**
     * 头像地址
     */
    @Property(nameInDb = "avatar")
    public String avatar;

    /**
     * 身高
     */
    @Property(nameInDb = "height")
    public int height;

    /**
     * 体重
     */
    @Property(nameInDb = "weight")
    public int weight;

    @Property(nameInDb = "roleName")
    public String roleName;

    @Property(nameInDb = "purpose")
    public String purpose;
    /**
     * thelID
     */
    @Property(nameInDb = "userName")
    public String userName;
    @Property(nameInDb = "wantRole")
    public String wantRole;
    @Property(nameInDb = "professionalTypes")
    public String professionalTypes;
    @Property(nameInDb = "outLevel")
    public String outLevel;

    /**
     * 查询数据的下表
     */
    @Property(nameInDb = "msgCursor")
    public String msgCursor;

    @Property(nameInDb = "messageUser")
    public String messageUser;

    /**
     * 连接聊天时的验证的token
     */
    @Property(nameInDb = "token")
    public String token;

    /**
     * 聊天连接时的ip
     */
    @Property(nameInDb = "imIps")
    public String imIps;

    @Override public int describeContents() {
        return 0;
    }

    @Override public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this._id);
        dest.writeString(this.key);
        dest.writeString(this.id);
        dest.writeInt(this.isNew);
        dest.writeInt(this.isInit);
        dest.writeString(this.nickName);
        dest.writeString(this.birthday);
        dest.writeString(this.avatar);
        dest.writeInt(this.height);
        dest.writeInt(this.weight);
        dest.writeString(this.roleName);
        dest.writeString(this.purpose);
        dest.writeString(this.userName);
        dest.writeString(this.wantRole);
        dest.writeString(this.professionalTypes);
        dest.writeString(this.outLevel);
        dest.writeString(this.msgCursor);
        dest.writeString(this.messageUser);
        dest.writeString(this.token);
        dest.writeString(this.imIps);
    }

    public Long get_id() {
        return this._id;
    }

    public void set_id(Long _id) {
        this._id = _id;
    }

    public String getKey() {
        return this.key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getIsNew() {
        return this.isNew;
    }

    public void setIsNew(int isNew) {
        this.isNew = isNew;
    }

    public int getIsInit() {
        return this.isInit;
    }

    public void setIsInit(int isInit) {
        this.isInit = isInit;
    }

    public String getNickName() {
        return this.nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getBirthday() {
        return this.birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getAvatar() {
        return this.avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getHeight() {
        return this.height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWeight() {
        return this.weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getRoleName() {
        return this.roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getPurpose() {
        return this.purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getWantRole() {
        return this.wantRole;
    }

    public void setWantRole(String wantRole) {
        this.wantRole = wantRole;
    }

    public String getProfessionalTypes() {
        return this.professionalTypes;
    }

    public void setProfessionalTypes(String professionalTypes) {
        this.professionalTypes = professionalTypes;
    }

    public String getOutLevel() {
        return this.outLevel;
    }

    public void setOutLevel(String outLevel) {
        this.outLevel = outLevel;
    }

    public String getMsgCursor() {
        return this.msgCursor;
    }

    public void setMsgCursor(String msgCursor) {
        this.msgCursor = msgCursor;
    }

    public String getMessageUser() {
        return this.messageUser;
    }

    public void setMessageUser(String messageUser) {
        this.messageUser = messageUser;
    }

    public String getToken() {
        return this.token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getImIps() {
        return this.imIps;
    }

    public void setImIps(String imIps) {
        this.imIps = imIps;
    }

    public UserInfoTable() {
    }

    protected UserInfoTable(Parcel in) {
        this._id = (Long) in.readValue(Long.class.getClassLoader());
        this.key = in.readString();
        this.id = in.readString();
        this.isNew = in.readInt();
        this.isInit = in.readInt();
        this.nickName = in.readString();
        this.birthday = in.readString();
        this.avatar = in.readString();
        this.height = in.readInt();
        this.weight = in.readInt();
        this.roleName = in.readString();
        this.purpose = in.readString();
        this.userName = in.readString();
        this.wantRole = in.readString();
        this.professionalTypes = in.readString();
        this.outLevel = in.readString();
        this.msgCursor = in.readString();
        this.messageUser = in.readString();
        this.token = in.readString();
        this.imIps = in.readString();
    }

    @Generated(hash = 560559963)
    public UserInfoTable(Long _id, String key, String id, int isNew, int isInit, String nickName, String birthday,
            String avatar, int height, int weight, String roleName, String purpose, String userName,
            String wantRole, String professionalTypes, String outLevel, String msgCursor, String messageUser,
            String token, String imIps) {
        this._id = _id;
        this.key = key;
        this.id = id;
        this.isNew = isNew;
        this.isInit = isInit;
        this.nickName = nickName;
        this.birthday = birthday;
        this.avatar = avatar;
        this.height = height;
        this.weight = weight;
        this.roleName = roleName;
        this.purpose = purpose;
        this.userName = userName;
        this.wantRole = wantRole;
        this.professionalTypes = professionalTypes;
        this.outLevel = outLevel;
        this.msgCursor = msgCursor;
        this.messageUser = messageUser;
        this.token = token;
        this.imIps = imIps;
    }

    public static final Parcelable.Creator<UserInfoTable> CREATOR = new Parcelable.Creator<UserInfoTable>() {
        @Override public UserInfoTable createFromParcel(Parcel source) {
            return new UserInfoTable(source);
        }

        @Override public UserInfoTable[] newArray(int size) {
            return new UserInfoTable[size];
        }
    };
}
