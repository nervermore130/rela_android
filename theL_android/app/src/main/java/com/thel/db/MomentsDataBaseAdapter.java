package com.thel.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.thel.android.pay.Purchase;
import com.thel.app.TheLApp;
import com.thel.bean.StickerBean;
import com.thel.bean.StickerPackBean;
import com.thel.bean.video.VideoReportBean;
import com.thel.modules.main.me.bean.MatchBean;
import com.thel.utils.MD5Utils;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MomentsDataBaseAdapter {

    private static final String TAG = MomentsDataBaseAdapter.class.getSimpleName();

    /**
     * 数据库对象
     */
    private SQLiteDatabase db;
    private static String databaseName;
    /**
     * 数据库适配器对象
     */
    public static MomentsDataBaseAdapter dbAdapter = null;
    private MessageDataBaseHelper dbHelper;
    /**
     * 数据库版本号
     */
    private static final int DB_VERSION = 4;
    /**
     * 系统表sqlite_sequence
     */
    public static final String TB_NAME_SQLITE_SEQUENCE = "sqlite_sequence";
    /**
     * moment表名 *
     */
    public static final String TB_NAME_MOMENTS = "moments_v13";
    /**
     * 已经发送的日志表名
     */
    public static final String TB_NAME_SENT_MOMENTS = "sent_moments_v6";
    /**
     * 点赞表名 *
     */
    public static final String TB_NAME_WINK_COMMENTS = "winkComments";
    /**
     * 提到的用户表名 *
     */
    public static final String TB_NAME_MENTIONED_USERS = "mentionedUsers";
    /**
     * 提到的用户表名(发送的日志) *
     */
    public static final String TB_NAME_MENTIONED_USERS_FOR_SEND_MOMENT = "send_mentionedUsers_v1";

    /**
     * 我在使用的表情包列表
     */
    public static final String TB_NAME_STICKER_PACKS_IN_USE = "stickerPacksInUse";

    /**
     * 表情包里的表情
     */
    public static final String TB_NAME_STICKERS = "stickers";

    /**
     * 视频播放的数据统计
     */
    public static final String TB_NAME_VIDEO_REPORT = "videoReport";

    /**
     * 未同步的google wallet购买的商品订单，一旦表中有数据，就要遍历去同步
     */
    public static final String TB_NAME_IN_APP_ORDERS = "inAppOrders";

    /**
     * 签名
     */
    public static final String F_SIGN = "sign";

    /**
     * id
     */
    public static final String F_IDENTIFICATION = "id";

    /**
     * 背景图地址
     */
    public static final String F_BG_IMAGE = "bgImage";

    /*----------------------------------------字段--------------------------------------------*/

    /**
     * Moment表 *
     */
    public static final String F_ID = "_id";
    public static final String F_MOMENTS_ID = "momentsId"; // moment的唯一标识
    public static final String F_USER_BOARD_TOP = "userBoardTop"; // 日志是否在个人主页置顶
    public static final String F_USERID = "userId"; // 对方的id
    public static final String F_USER_ID = "_userId"; // 用户id外键
    public static final String F_NICKNAME = "nickname"; // 对方的name
    public static final String F_AVATAR = "avatar"; // 对方的头像地址
    public static final String F_COMMENT_NUM = "commentNum"; // 评论条数
    public static final String F_WINK_NUM = "winkNum"; // 点赞数量
    public static final String F_WINK_FLAG = "winkFlag"; // 是否点过赞
    public static final String F_FOLLOWER_STATUS = "followerStatus"; // 是否关注过该用户
    public static final String F_MY_SELF = "myself";// 是不是我自己的moment，1：是，0：否
    public static final String F_RELEASE_TIME = "releaseTime";// moment发布的时间
    public static final String F_RELEASE_THEMECLASS = "relwaseThemeClass";//话题分类
    // (目前没用，留作以后改版用)
    public static final String F_MOMENTS_TIME = "momentsTime"; // moment发布的时间（与服务器时间的偏移）
    public static final String F_MOMENTS_TYPE = "momentsType"; // 朋友圈的类型，目前有四种选择：text,
    // image, text_image，voice
    public static final String F_SHARE_TO = "shareTo"; // 发布的类型，1 代表 all, 2 代表
    // friends,
    // 3 代表 only me
    public static final String F_SECRET = "secret"; // 是否匿名（0 代表 不匿名，1 代表 匿名）
    public static final String F_MOMENTS_TEXT = "momentsText";// moment的文字内容
    public static final String F_THUMB_NAIL_URL = "thumbnailUrl"; // moment图片内容的缩略图
    public static final String F_IMAGE_URL = "imageUrl";// moment图片内容
    public static final String F_TAG = "tag";//
    public static final String F_TIMESTAMP = "timestamp";
    public static final String F_FILTER_TYPE = "filterType";// moments类型，关注还是发现
    public static final String F_UPLOADED_IMGS_URLS = "uploadedImgsUrls";// 预发送的日志，已经上传成功的图片地址
    public static final String F_IMGS_TO_UPLOAD = "imgsToUpload";// 预发送的日志，还未上传的图片
    public static final String F_MEDIA_SIZE = "mediaSize";// 视频大小
    public static final String F_PLAY_TIME = "playTime";// 视频播放时长（videoReport表中表示播放时间）
    public static final String F_VIDEO_URL = "videoUrl";// 视频在七牛上的路径
    public static final String F_PIXEL_WIDTH = "pixelWidth";// 视频宽度
    public static final String F_PIXEL_HEIGHT = "pixelHeight";// 视频高度
    public static final String F_LIVE_ID = "liveId";// 直播间ID
    public static final String F_ORDER_DATA = "data";// 订单数据
    public static final String F_ITEM_TYPE = "itemType";// google play item logType

    /**
     * 推荐用户卡片
     */
    public static final String F_CARD_USER_ID = "cardUserId";
    public static final String F_CARD_NICKNAME = "cardNickName";
    public static final String F_CARD_AVATAR = "cardAvatar";
    public static final String F_CARD_AGE = "cardAge";
    public static final String F_CARD_HEIGHT = "cardHeight";
    public static final String F_CARD_WEIGHT = "cardWeight";
    public static final String F_CARD_INTRO = "cardIntro";
    public static final String F_CARD_AFFECTION = "cardAffection";

    /**
     * 推荐日志（转发）
     */
    public static final String F_PARENT_ID = "parentId";

    /**
     * 音乐 *
     */
    public static final String F_SONG_ID = "songId";
    public static final String F_SONG_NAME = "songName";
    public static final String F_ARTIST_NAME = "artistName";
    public static final String F_ALBUM_NAME = "albumName";
    public static final String F_ALBUM_LOGO_100 = "albumLogo100";
    public static final String F_ALBUM_LOGO_444 = "albumLogo444";
    public static final String F_SONG_LOCATION = "songLocation";
    public static final String F_SONG_TO_URL = "toURL";

    /**
     * 点赞表 *
     */
    public static final String F_COMMENT_TIME = "commentTime";// 点赞的时间
    public static final String F_WINK_COMMENT_TYPE = "winkCommentType";// 点赞的类型，1-9中的某个整数

    /**
     * 话题信息
     */
    public static final String F_TOPIC_FLAG = "topicFlag";
    public static final String F_TOPIC_ID = "topicId";
    public static final String F_TOPIC_NAME = "topicName";
    public static final String F_TOPIC_COLOR = "topicColor";
    public static final String F_NOT_READ_NUM = "notReadNum";

    /**
     * 折叠
     */
    public static final String F_HIDE_FLAG = "hideFlag";
    public static final String F_HIDE_NUM = "hideNum";
    public static final String F_HIDE_TYPE = "hideType";

    public static final String F_MOMENTS_NUM = "momentsNum";
    public static final String F_JOIN_COUNT = "joinCount";

    /**
     * 匹配游戏推荐用户表
     */
    public static final String F_AGE = "age";
    public static final String F_HEIGHT = "height";
    public static final String F_WEIGHT = "weight";
    public static final String F_HOROSCOPE = "horoscope";
    public static final String F_BIRTHDAY = "birthday";
    public static final String F_INTRO = "intro";
    public static final String F_MATCH_COUNT = "matchCount";
    public static final String F_NICK_NAME = "nickName";
    public static final String F_PICLIST = "picList";
    public static final String F_PROFESSIONAL_TYPES = "professionalTypes";
    public static final String F_PURPOSE = "purpose";
    public static final String F_TAG_NAME = "tagName";
    public static final String F_TAG_COLOR = "tagColor";
    public static final String F_BG_IMG = "bgimg";
    public static final String F_COMMENT_LIST = "commentList";
    public static final String F_PARENT_MOMENT = "parentMoment";
    public static final String F_DELETE_FLAG = "deleteFlag";
    public static final String F_IS_ROLLBACK = "isRollback";

    /**
     * 话题日志相关
     */
    public static final String F_THEME_TAG_NAME = "themeTagName";

    /**
     * 表情title
     */
    public static final String F_TITLE = "title";
    /**
     * 表情包小缩略图
     */
    public static final String F_ICON = "icon";
    /**
     * 表情包背景图
     */
    public static final String F_COVER = "cover";
    /**
     * 缩略图
     */
    public static final String F_THUMBNAIL = "thumbnail";

    /**
     * 表情包简介
     */
    public static final String F_SUMMARY = "summary";
    /**
     * 表情包价格
     */
    public static final String F_PRICE = "price";
    /**
     * 表情包id
     */
    public static final String F_PACK_ID = "packId";
    /**
     * 表情label
     */
    public static final String F_LABEL = "label";
    /**
     * 表情地址
     */
    public static final String F_IMG = "img";

    /**
     * 日志id
     */
    public static final String F_MOMENT_ID = "momentId";
    /**
     * 播放的时长
     */
    public static final String F_PLAY_SECONDS = "playSeconds";
    /**
     * 是否全屏播放
     */
    public static final String F_SHOW_FULL = "showFull";

    public synchronized static MomentsDataBaseAdapter getInstance(Context context, String myUserId) {
        if (null == dbAdapter) {
            dbAdapter = new MomentsDataBaseAdapter();
        }
        databaseName = "moments_" + myUserId + ".db";
        return dbAdapter.openDataBase();
    }

    private MomentsDataBaseAdapter openDataBase() {
        if (dbHelper == null) {
            dbHelper = new MessageDataBaseHelper(TheLApp.getContext());
        }
        try {
            db = dbHelper.getWritableDatabase();
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
        return dbAdapter;
    }

    /**
     * 保存一条视频数据统计
     *
     * @param videoReportBean
     */
    public void saveVideoReport(VideoReportBean videoReportBean) {
        if (!isTableExist(TB_NAME_VIDEO_REPORT)) {
            createVideoReportDB(db);
        }
        try {
            db.insert(TB_NAME_VIDEO_REPORT, null, videoReportBean.getContentValues());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取视频统计数据
     *
     * @return
     */
    public String getVideoReport() {
        if (!isTableExist(TB_NAME_VIDEO_REPORT)) {
            createVideoReportDB(db);
        }
        VideoReportBean videoReportBean = null;
        String result = "";
        JSONArray jsonArray = new JSONArray();
        Cursor cursor = null;
        try {
            cursor = db.rawQuery("SELECT * FROM " + TB_NAME_VIDEO_REPORT, null);
            while (cursor.moveToNext()) {
                videoReportBean = new VideoReportBean();
                videoReportBean.fromCursor(cursor);
                jsonArray.put(videoReportBean.toJSON());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != cursor) {
                cursor.close();
            }
            // closeDataBase();
        }
        if (jsonArray.length() > 0)
            result = jsonArray.toString();
        return result;
    }

    public void clearVideoReport() {
        try {
            db.delete(TB_NAME_VIDEO_REPORT, null, null);
            ContentValues contentValues = new ContentValues();
            contentValues.put("seq", 0);
            String[] whereArgs = new String[]{TB_NAME_VIDEO_REPORT};
            db.update(TB_NAME_SQLITE_SEQUENCE, contentValues, "name = ?", whereArgs);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // closeDataBase();
        }
    }

    /**
     * 保存一张google wallet支付成功的订单
     *
     * @param purchase
     */
    public void saveInAppOrder(Purchase purchase) {
        if (!isTableExist(TB_NAME_IN_APP_ORDERS)) {
            createInAppOrdersDB(db);
        }
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(F_ID, purchase.getDeveloperPayload());
            contentValues.put(F_ITEM_TYPE, purchase.getItemType());
            contentValues.put(F_ORDER_DATA, purchase.getOriginalJson());
            contentValues.put(F_SIGN, purchase.getSignature());
            db.insert(TB_NAME_IN_APP_ORDERS, null, contentValues);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // closeDataBase();
        }
    }

    /**
     * 删除一条未同步的google支付订单
     *
     * @param outTradeNo
     */
    public void deleteInAppOrder(String outTradeNo) {
        if (!isTableExist(TB_NAME_IN_APP_ORDERS)) {
            createInAppOrdersDB(db);
            return;
        }
        try {
            db.delete(TB_NAME_IN_APP_ORDERS, F_ID + " = '" + outTradeNo + "'", null);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // closeDataBase();
        }
    }

    /**
     * 获取一条未同步的订单
     *
     * @param outTradeNo
     * @return
     */
    public Purchase getInAppOrder(String outTradeNo) {
        if (!isTableExist(TB_NAME_IN_APP_ORDERS)) {
            createInAppOrdersDB(db);
            return null;
        }
        Cursor cursor = null;
        try {
            cursor = db.rawQuery("SELECT * FROM " + TB_NAME_IN_APP_ORDERS + " WHERE " + F_ID + " = '" + outTradeNo + "'", null);
            if (cursor.moveToNext()) {
                return new Purchase(cursor.getString(cursor.getColumnIndex(F_ITEM_TYPE)), cursor.getString(cursor.getColumnIndex(F_ORDER_DATA)), cursor.getString(cursor.getColumnIndex(F_SIGN)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != cursor) {
                cursor.close();
            }
            // closeDataBase();
        }
        return null;
    }

    /**
     * 获取数据库中第一条订单
     *
     * @return
     */
    public Purchase getFirstInAppOrder() {
        if (!isTableExist(TB_NAME_IN_APP_ORDERS)) {
            createInAppOrdersDB(db);
            return null;
        }
        Cursor cursor = null;
        try {
            cursor = db.rawQuery("SELECT * FROM " + TB_NAME_IN_APP_ORDERS + " LIMIT 1", null);
            if (cursor.moveToNext()) {
                return new Purchase(cursor.getString(cursor.getColumnIndex(F_ITEM_TYPE)), cursor.getString(cursor.getColumnIndex(F_ORDER_DATA)), cursor.getString(cursor.getColumnIndex(F_SIGN)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != cursor) {
                cursor.close();
            }
            // closeDataBase();
        }
        return null;
    }

    /*****************************************************************/
    /**
     * 建表
     *
     * @param db
     */
    private void createTable(SQLiteDatabase db) {
        String sql = "create table if not exists " + TB_NAME_MOMENTS + "(" + F_ID + " integer primary key AUTOINCREMENT," + F_MOMENTS_ID + " text UNIQUE," + F_USERID + " integer," + F_USER_BOARD_TOP + " integer," + F_NICKNAME + " text," + F_AVATAR + " text," + F_COMMENT_NUM + " integer," + F_WINK_NUM + " integer," + F_WINK_FLAG + " integer," + F_FOLLOWER_STATUS + " integer," + F_MY_SELF + " integer," + F_RELEASE_TIME + " integer," + F_MOMENTS_TIME + " text," + F_MOMENTS_TYPE + " text," + F_SHARE_TO + " integer," + F_SECRET + " integer," + F_MOMENTS_TEXT + " text," + F_THUMB_NAIL_URL + " text," + F_IMAGE_URL + " text," + F_SONG_ID + " text," + F_SONG_NAME + " text," + F_ARTIST_NAME + " text," + F_ALBUM_NAME + " text," + F_ALBUM_LOGO_100 + " text," + F_ALBUM_LOGO_444 + " text," + F_SONG_LOCATION + " text," + " text," + F_SONG_TO_URL + " text," + F_TAG + " text," + F_TIMESTAMP + " integer," + F_TOPIC_FLAG + " integer," + F_TOPIC_ID + " text," + F_TOPIC_NAME + " text," + F_TOPIC_COLOR + " text," + F_NOT_READ_NUM + " integer," + F_HIDE_FLAG + " integer," + F_HIDE_NUM + " integer," + F_HIDE_TYPE + " integer," + F_COMMENT_LIST + " text," + F_PARENT_MOMENT + " text," + F_MOMENTS_NUM + " integer," + F_JOIN_COUNT + " integer," + F_FILTER_TYPE + " text," + F_MEDIA_SIZE + " integer," + F_PLAY_TIME + " integer," + F_PIXEL_WIDTH + " integer," + F_PIXEL_HEIGHT + " integer," + F_VIDEO_URL + " text," + F_LIVE_ID + " text," + F_THEME_TAG_NAME + " text," + F_CARD_USER_ID + " integer," + F_CARD_NICKNAME + " text," + F_CARD_AVATAR + " text," + F_CARD_AGE + " integer," + F_CARD_WEIGHT + " integer," + F_CARD_HEIGHT + " integer," + F_CARD_INTRO + " text," + F_PARENT_ID + " text," + F_RELEASE_THEMECLASS + " text," + F_CARD_AFFECTION + " text" + ")";

        db.execSQL(sql);

        sql = "create table if not exists " + TB_NAME_WINK_COMMENTS + "(" + F_ID + " integer primary key AUTOINCREMENT," + F_MOMENTS_ID + " text," + F_USERID + " integer," + F_NICKNAME + " text," + F_AVATAR + " text," + F_COMMENT_TIME + " text," + F_RELEASE_TIME + " integer," + F_WINK_COMMENT_TYPE + " integer," + F_TIMESTAMP + " integer," + F_FILTER_TYPE + " text" + ")";

        db.execSQL(sql);

        sql = "create table if not exists " + TB_NAME_MENTIONED_USERS + "(" + F_ID + " integer primary key AUTOINCREMENT," + F_MOMENTS_ID + " text," + F_USERID + " integer," + F_NICKNAME + " text," + F_TIMESTAMP + " integer," + F_FILTER_TYPE + " text" + ")";

        db.execSQL(sql);

        // 创建索引
        db.execSQL("CREATE INDEX IF NOT EXISTS [moments_id] On [" + TB_NAME_MOMENTS + "] ( " + "[momentsId] Collate BINARY ) ");
        db.execSQL("CREATE INDEX IF NOT EXISTS [filter_type] On [" + TB_NAME_MOMENTS + "] ( " + "[filterType] Collate BINARY ) ");
        db.execSQL("CREATE INDEX IF NOT EXISTS [wink_moments_id] On [" + TB_NAME_WINK_COMMENTS + "] ( " + "[momentsId] Collate BINARY ) ");
        db.execSQL("CREATE INDEX IF NOT EXISTS [wink_filter_type] On [" + TB_NAME_WINK_COMMENTS + "] ( " + "[filterType] Collate BINARY ) ");
        db.execSQL("CREATE INDEX IF NOT EXISTS [mentioned_moments_id] On [" + TB_NAME_MENTIONED_USERS + "] ( " + "[momentsId] Collate BINARY ) ");
        db.execSQL("CREATE INDEX IF NOT EXISTS [mentioned_filter_type] On [" + TB_NAME_MENTIONED_USERS + "] ( " + "[filterType] Collate BINARY ) ");
    }

    private void createSendMomentTables(SQLiteDatabase db) {
        String sql = "create table if not exists " + TB_NAME_SENT_MOMENTS + "(" + F_ID + " integer primary key AUTOINCREMENT," + F_MOMENTS_ID + " text," + F_USERID + " integer," + F_NICKNAME + " text," + F_AVATAR + " text," + F_COMMENT_NUM + " integer," + F_WINK_NUM + " integer," + F_WINK_FLAG + " integer," + F_MY_SELF + " integer," + F_RELEASE_TIME + " integer," + F_MOMENTS_TIME + " text," + F_MOMENTS_TYPE + " text," + F_SHARE_TO + " integer," + F_SECRET + " integer," + F_MOMENTS_TEXT + " text," + F_THUMB_NAIL_URL + " text," + F_IMAGE_URL + " text," + F_SONG_ID + " text," + F_SONG_NAME + " text," + F_ARTIST_NAME + " text," + F_ALBUM_NAME + " text," + F_ALBUM_LOGO_100 + " text," + F_ALBUM_LOGO_444 + " text," + F_SONG_LOCATION + " text," + " text," + F_SONG_TO_URL + " text," + F_TAG + " text," + F_TIMESTAMP + " integer," + F_TOPIC_FLAG + " integer," + F_TOPIC_ID + " text," + F_TOPIC_NAME + " text," + F_TOPIC_COLOR + " text," + F_UPLOADED_IMGS_URLS + " text," + F_MEDIA_SIZE + " integer," + F_PLAY_TIME + " integer," + F_PIXEL_WIDTH + " integer," + F_PIXEL_HEIGHT + " integer," + F_VIDEO_URL + " text," + F_IMGS_TO_UPLOAD + " text," + F_NOT_READ_NUM + " integer," + F_HIDE_FLAG + " integer," + F_HIDE_NUM + " integer," + F_HIDE_TYPE + " integer," + F_FILTER_TYPE + " text," + F_RELEASE_THEMECLASS + " text," + F_PARENT_ID + " text," + F_THEME_TAG_NAME + " text" + ")";

        db.execSQL(sql);

        sql = "create table if not exists " + TB_NAME_MENTIONED_USERS_FOR_SEND_MOMENT + "(" + F_ID + " integer primary key AUTOINCREMENT," + F_MOMENTS_ID + " text," + F_USERID + " integer," + F_NICKNAME + " text," + F_AVATAR + " text," + F_TIMESTAMP + " integer," + F_FILTER_TYPE + " text" + ")";

        db.execSQL(sql);

        createStickerPacksInUseDB(db);
        createStickersDB(db);
    }

    /**
     * 建我正在使用的表情包表
     */
    public void createStickerPacksInUseDB(SQLiteDatabase db) {
        String sql = "create table if not exists " + TB_NAME_STICKER_PACKS_IN_USE + "(" + F_ID + " integer primary key AUTOINCREMENT," + F_IDENTIFICATION + " integer UNIQUE," + F_TITLE + " text," + F_SUMMARY + " text," + F_PRICE + " text," + F_ICON + " text," + F_COVER + " text," + F_BG_IMAGE + " text," + F_THUMBNAIL + " text," + F_SIGN + " text" + ")";
        db.execSQL(sql);
    }

    /**
     * 建表情包中的表情表
     */
    public void createStickersDB(SQLiteDatabase db) {
        String sql = "create table if not exists " + TB_NAME_STICKERS + "(" + F_ID + " integer primary key AUTOINCREMENT," + F_IDENTIFICATION + " integer UNIQUE," + F_PACK_ID + " integer," + F_LABEL + " text," + F_IMG + " text," + F_THUMBNAIL + " text" + ")";
        db.execSQL(sql);
    }

    /**
     * 建表情包中的表情表
     */
    public void createVideoReportDB(SQLiteDatabase db) {
        String sql = "create table if not exists " + TB_NAME_VIDEO_REPORT + "(" + F_ID + " integer primary key AUTOINCREMENT," + F_USERID + " integer," + F_MOMENT_ID + " text," + F_PLAY_TIME + " integer," + F_PLAY_SECONDS + " integer," + F_SHOW_FULL + " boolean" + ")";
        db.execSQL(sql);
    }

    /**
     * 创建google wallet支付订单
     * id存outTradeNo
     *
     * @param db
     */
    public void createInAppOrdersDB(SQLiteDatabase db) {
        String sql = "CREATE TABLE IF NOT EXISTS " + TB_NAME_IN_APP_ORDERS + "(" + F_ID + " text PRIMARY KEY," + F_ITEM_TYPE + " TEXT," + F_ORDER_DATA + " TEXT," + F_SIGN + " TEXT" + ")";
        db.execSQL(sql);
    }

    /**
     * 判断表是否存在
     *
     * @param tabName
     * @return
     */
    public boolean isTableExist(String tabName) {
        boolean result = false;
        Cursor cursor = null;
        try {
            String sql = "select count(*) as c from sqlite_master where type ='table' and name ='" + tabName + "' ";
            cursor = db.rawQuery(sql, null);
            if (cursor.moveToNext()) {
                int count = cursor.getInt(0);
                if (count > 0) {
                    result = true;
                }
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    private class MessageDataBaseHelper extends SQLiteOpenHelper {
        public MessageDataBaseHelper(Context context) {
            super(context, databaseName, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            createTable(db);
            createSendMomentTables(db);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }

}
