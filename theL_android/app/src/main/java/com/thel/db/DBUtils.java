package com.thel.db;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;

import com.thel.app.TheLApp;
import com.thel.constants.TheLConstants;
import com.thel.db.daos.DaoMaster;
import com.thel.db.daos.DaoSession;
import com.thel.db.helper.MyGreenDaoDbHelper;
import com.thel.manager.ChatServiceManager;
import com.thel.modules.main.messages.bean.MsgBean;
import com.thel.modules.main.messages.db.MessageDataBaseAdapter;
import com.thel.utils.L;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;

import org.greenrobot.greendao.database.Database;

import java.util.List;

/**
 * Created by chad
 * Time 18/3/21
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */

public class DBUtils {

    private static String dbName = null;

    /**
     * 创建数据库
     *
     * @return
     */
    public static DaoSession createDB(String userId) {

        L.d("DBUtils", " getMyUserId : " + UserUtils.getMyUserId());

        L.d("DBUtils", " userId : " + userId);

        dbName = getDbName(userId);

        MyGreenDaoDbHelper openHelper = new MyGreenDaoDbHelper(TheLApp.context.getApplicationContext(), dbName);
        Database database = openHelper.getWritableDb();
        DaoMaster daoMaster = new DaoMaster(database);
        return daoMaster.newSession();
    }

    public static String getDbName(String userId) {
        return "message_" + userId + ".db";
    }

    public static String getDbName() {
        return dbName;
    }

    /**
     * 判断表是否存在
     *
     * @param tabName
     * @return
     */
    public static boolean isTableExist(Database db, String tabName) {

        L.d("DBUtils", " tabName : " + tabName);

        boolean result = false;

        Cursor cursor = null;
        try {

            L.d("DBUtils", " --------1------- : ");

            String sql = "select count(*) from sqlite_master where type ='table' and name ='" + tabName.trim() + "' ";
            cursor = db.rawQuery(sql, null);
            if (cursor.moveToNext()) {
                int count = cursor.getInt(0);
                if (count > 0) {
                    result = true;
                }
            }
        } catch (Exception e) {

            result = true;

            L.d("DBUtils", " --------2------- : ");

            e.printStackTrace();
        } finally {

            L.d("DBUtils", " --------3------- : ");

            if (null != cursor) {
                cursor.close();
            }
        }
        return result;
    }


    public static String getUserId(String toUserId, String fromUserId) {

        String userId;

        if (fromUserId.equals(Utils.getMyUserId())) {
            userId = toUserId;
        } else {
            userId = fromUserId;
        }
        return userId;

    }

    /**
     * Cursor转化成MsgBean
     *
     * @param cursor
     * @return
     */
    public static MsgBean fromCursor(Cursor cursor) {

        MsgBean msgBean = new MsgBean();

        msgBean.id = cursor.getInt(0);
        msgBean.packetId = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_PACKETID));
        msgBean.msgDirection = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_MSG_DIRECTION));
        msgBean.msgStatus = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_MSG_STATUS));
        msgBean.msgType = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_MSG_TYPE));
        msgBean.msgTime = cursor.getLong(cursor.getColumnIndex(MessageDataBaseAdapter.F_MSG_TIME));
        msgBean.msgText = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_MSG_TEXT));
        msgBean.avatar = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_AVATAR));
        msgBean.userId = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_USERID));
        msgBean.userName = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_USERNAME));
        msgBean.receivedId = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_RECEIVEDID));
        msgBean.voiceTime = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_VOICETIME));
        msgBean.filePath = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_FILEPATH));
        msgBean.msgImage = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_MSG_IMAGE));
        msgBean.msgHeight = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_MSG_HEIGHT));
        msgBean.msgWidth = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_MSG_WIDTH));
        msgBean.msgLng = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_MSG_LNG));
        msgBean.msgLat = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_MSG_LAT));
        msgBean.msgMapUrl = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_MSG_MAPURL));
        msgBean.fromUserId = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_FROMUSERID));
        msgBean.fromAvatar = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_FROMAVATAR));
        msgBean.fromNickname = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_FROMNICKNAME));
        msgBean.fromMessageUser = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_FROMMESSAGEUSER));
        msgBean.toUserId = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_TOUSERID));
        msgBean.toAvatar = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_TOAVATAR));
        msgBean.toUserNickname = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_TONICKNAME));
        msgBean.toMessageUser = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_TOMESSAGEUSER));
        msgBean.hadRead = cursor.getInt(cursor.getColumnIndex(MessageDataBaseAdapter.F_HADREAD));
        msgBean.hadPlayed = cursor.getInt(cursor.getColumnIndex(MessageDataBaseAdapter.F_HADPLAYED));
//        msgBean.isSystem = cursor.getInt(cursor.getColumnIndex(MessageDataBaseAdapter.F_MSG_RESERVED_ONE));
//        msgBean.isTop = cursor.getInt(cursor.getColumnIndex(MessageDataBaseAdapter.F_MSG_STICKY_TOP));

//        if (isSendingMsg) {
//            msgBean.sendTimes = cursor.getInt(cursor.getColumnIndex(MessageDataBaseAdapter.F_MSG_SEND_TIMES));
//        }

        return msgBean;
    }

    /**
     * MsgBean转化成ContentValues
     *
     * @param isSendingMsg
     * @param msgBean
     * @return
     */
    public static ContentValues getContentValues(boolean isSendingMsg, MsgBean msgBean) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(MessageDataBaseAdapter.F_PACKETID, msgBean.packetId);
        contentValues.put(MessageDataBaseAdapter.F_MSG_DIRECTION, msgBean.msgDirection);
        contentValues.put(MessageDataBaseAdapter.F_MSG_STATUS, msgBean.msgStatus);
        contentValues.put(MessageDataBaseAdapter.F_MSG_TYPE, msgBean.msgType);
        contentValues.put(MessageDataBaseAdapter.F_MSG_TIME, msgBean.msgTime);
        contentValues.put(MessageDataBaseAdapter.F_MSG_TEXT, msgBean.msgText);
        contentValues.put(MessageDataBaseAdapter.F_AVATAR, msgBean.avatar);
        contentValues.put(MessageDataBaseAdapter.F_USERID, msgBean.userId);
        contentValues.put(MessageDataBaseAdapter.F_USERNAME, msgBean.userName);
        contentValues.put(MessageDataBaseAdapter.F_RECEIVEDID, msgBean.receivedId);
        contentValues.put(MessageDataBaseAdapter.F_VOICETIME, msgBean.voiceTime);
        contentValues.put(MessageDataBaseAdapter.F_FILEPATH, msgBean.filePath);
        contentValues.put(MessageDataBaseAdapter.F_MSG_IMAGE, msgBean.msgImage);
        contentValues.put(MessageDataBaseAdapter.F_MSG_HEIGHT, msgBean.msgHeight);
        contentValues.put(MessageDataBaseAdapter.F_MSG_WIDTH, msgBean.msgWidth);
        contentValues.put(MessageDataBaseAdapter.F_MSG_LNG, msgBean.msgLng);
        contentValues.put(MessageDataBaseAdapter.F_MSG_LAT, msgBean.msgLat);
        contentValues.put(MessageDataBaseAdapter.F_MSG_MAPURL, msgBean.msgMapUrl);
        contentValues.put(MessageDataBaseAdapter.F_FROMUSERID, msgBean.fromUserId);
        contentValues.put(MessageDataBaseAdapter.F_FROMAVATAR, msgBean.fromAvatar);
        contentValues.put(MessageDataBaseAdapter.F_FROMNICKNAME, msgBean.fromNickname);
        contentValues.put(MessageDataBaseAdapter.F_FROMMESSAGEUSER, msgBean.fromMessageUser);
        contentValues.put(MessageDataBaseAdapter.F_TOUSERID, msgBean.toUserId);
        contentValues.put(MessageDataBaseAdapter.F_TOAVATAR, msgBean.toAvatar);
        contentValues.put(MessageDataBaseAdapter.F_TONICKNAME, msgBean.toUserNickname);
        contentValues.put(MessageDataBaseAdapter.F_TOMESSAGEUSER, msgBean.toMessageUser);
        contentValues.put(MessageDataBaseAdapter.F_HADREAD, msgBean.hadRead);
        contentValues.put(MessageDataBaseAdapter.F_HADPLAYED, msgBean.hadPlayed);
        contentValues.put(MessageDataBaseAdapter.F_MSG_RESERVED_ONE, msgBean.isSystem);
        contentValues.put(MessageDataBaseAdapter.F_MSG_STICKY_TOP, msgBean.isTop);

        if (isSendingMsg) {
            contentValues.put(MessageDataBaseAdapter.F_MSG_SEND_TIMES, msgBean.sendTimes);
        }

        return contentValues;
    }

    public static String getChatTableName(MsgBean msgBean) {
        return DBConstant.Message.CHAT_TABLE + getOtherUserId(msgBean);
    }

    public static String getOtherUserId(MsgBean msgBean) {

        if (msgBean.toUserId == null || msgBean.toUserId.length() == 0) {
            return msgBean.fromUserId;
        }

        String otherUserId;

        if (msgBean.toUserId.equals(DBManager.getInstance().getMyUserId())) {
            otherUserId = msgBean.fromUserId;
        } else {
            otherUserId = msgBean.toUserId;
        }

        return otherUserId;
    }

    public static String getOtherUserName(MsgBean msgBean) {

        if (msgBean.toUserNickname == null || msgBean.toUserNickname.length() == 0) {
            return msgBean.fromNickname;
        }

        String otherUserName;

        L.d("DBUtils", " msgBean.toUserNickname : " + msgBean.toUserNickname);

        if (msgBean.toUserId.equals(DBManager.getInstance().getMyUserId())) {
            otherUserName = msgBean.fromNickname;
        } else {
            otherUserName = msgBean.toUserNickname;
        }

        return otherUserName;
    }

    public static String getOtherAvatar(MsgBean msgBean) {

        if (msgBean.toAvatar == null || msgBean.toAvatar.length() == 0) {
            return msgBean.fromAvatar;
        }

        String otherAvatar;

        L.d("DBUtils", " msgBean.toAvatar : " + msgBean.toAvatar);

        if (msgBean.toUserId.equals(DBManager.getInstance().getMyUserId())) {
            otherAvatar = msgBean.fromAvatar;
        } else {
            otherAvatar = msgBean.toAvatar;

        }

        return otherAvatar;
    }

    public static String getMainProcessChatTableName(MsgBean msgBean) {
        return DBConstant.Message.CHAT_TABLE + getMainProcessOtherUserId(msgBean);
    }

    public static String getMainProcessOtherUserId(MsgBean msgBean) {

        String otherUserId;

        if (msgBean.fromUserId.equals(UserUtils.getMyUserId())) {
            otherUserId = msgBean.toUserId;
        } else {
            otherUserId = msgBean.fromUserId;
        }

        return otherUserId;
    }

    public static String getMainProcessOtherUserName(MsgBean msgBean) {

        String otherUserName;

        if (msgBean.fromUserId.equals(UserUtils.getMyUserId())) {
            otherUserName = msgBean.toUserNickname;
        } else {
            otherUserName = msgBean.fromNickname;
        }

        return otherUserName;
    }

    public static String getMainProcessOtherAvatar(MsgBean msgBean) {

        String otherUserName;

        if (msgBean.fromUserId.equals(UserUtils.getMyUserId())) {
            otherUserName = msgBean.toAvatar;
        } else {
            otherUserName = msgBean.fromAvatar;
        }

        return otherUserName;
    }

    public static String getChatTableName(String toUserId) {

        if (toUserId.equals(DBConstant.Message.REQUEST_TABLE_NAME)) {

            return DBConstant.Message.REQUEST_TABLE_NAME;

        }

        if (toUserId.equals(DBConstant.Message.STRANGER_TABLE_NAME)) {

            return DBConstant.Message.STRANGER_TABLE_NAME;

        }

        if (toUserId.equals(DBConstant.Message.WINK_TABLE_NAME)) {

            return DBConstant.Message.WINK_TABLE_NAME;

        }

        if (toUserId.equals(DBConstant.Message.FOLLOW_TABLE_NAME)) {

            return DBConstant.Message.FOLLOW_TABLE_NAME;

        }

        return DBConstant.Message.CHAT_TABLE + toUserId;
    }

    public static String getMainProcessChatTableName(String toUserId) {
        return DBConstant.Message.CHAT_TABLE + toUserId;
    }

    public static String getToUserName(MsgBean msgBean) {

        String toNickName;

        if (msgBean.msgDirection.equals("0")) {// 发出的
            toNickName = msgBean.toUserNickname;
        } else { // 收到的
            toNickName = msgBean.fromNickname;
        }

        return toNickName;
    }

    public static String getFollowTableName() {
        return DBConstant.Message.FOLLOW_TABLE_NAME;
    }

    public static String getStrangerTableName() {
        return DBConstant.Message.STRANGER_TABLE_NAME;
    }

    public static void sendRefreshUiBroadcast(MsgBean msgBean) {
        final Intent intent = new Intent();
        intent.setAction(TheLConstants.BROADCAST_CHAT_ACTIVITY_REFRESH_UI);
        intent.putExtra(TheLConstants.BUNDLE_KEY_MSG_BEAN, msgBean);
        TheLApp.getContext().sendBroadcast(intent);
    }

    public static int isStranger(List<String> userIds, String otherUserId) {

        for (String userId : userIds) {

            if (userId.equals(otherUserId)) {
                return 0;
            }

        }

        return 1;

    }

}
