package com.thel.db;

import com.thel.db.table.message.MsgTable;
import com.thel.utils.ShareFileUtils;

/**
 * Created by chad
 * Time 18/3/20
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */

public class DBConstant {

    public static final long STICK_TOP_BASE_TIME = 8000000000000l;

    /*********************************************消息数据库*********************************************************/
    public static class Message {

        //消息表名为 CHAT_TABLE_uid
        public static final String CHAT_TABLE = "CHAT_TABLE_";

        //发送失败的消息的表名
        public static final String FAILED_SEND_MSG_TABLE_NAME = "failed_send_msg_name";

        public static final String FOLLOW_TABLE_NAME = "follow_table";

        public static final String STRANGER_TABLE_NAME = "stranger_table_name";

        public static final String REQUEST_TABLE_NAME = "request_table_name";

        public static final String CIRCLE_REQUEST_CHAT_USER_ID = "circleRequest";

        public static final String WINK_TABLE_NAME = "wink_table_name";

        public static final String MATCH_TABLE_NAME = "match_table_name";

        public static final String MSG_HIDING = "msg_hiding_table_name";

        public static final String MSG_AMUSEMENT_PARK = "msg_amusement_park";//rela游乐园

        /*----------------------------------------表字段--------------------------------------------*/
        public static final String F_ID = "_id";
        public static final String F_PACKETID = "packetId";     // 消息的ID
        public static final String F_MSG_DIRECTION = "msgDirection"; // 消息的方向
        public static final String F_MSG_TYPE = "msgType";
        public static final String F_MSG_TIME = "msgTime";
        public static final String F_MSG_TEXT = "msgText";
        public static final String F_USERID = "userId"; // 对方的id
        public static final String F_USERNAME = "userName"; // 对方的name
        public static final String F_AVATAR = "avatar"; // 对方的头像地址
        public static final String F_MSG_UNREAD_COUNT = "unreadCount";
        public static final String F_MSG_STATUS = "msgStatus"; // 消息状态(发送成功,失败等)
        public static final String F_HADREAD = "hadread";//是否已读
        public static final String F_RECEIVEDID = "receivedId"; // 我自己的id
        public static final String F_MSG_LNG = "msgLng";
        public static final String F_MSG_LAT = "msgLat";
        public static final String F_MSG_MAPURL = "msgMapUrl";
        public static final String F_MSG_IMAGE = "msgImage";
        public static final String F_MSG_HEIGHT = "msgHeight";
        public static final String F_MSG_WIDTH = "msgWidth";
        public static final String F_VOICETIME = "voiceTime"; // 录音时长
        public static final String F_FILEPATH = "filePath";
        public static final String F_FROMUSERID = "fromUserId";
        public static final String F_FROMNICKNAME = "fromNickname";
        public static final String F_FROMAVATAR = "fromAvatar";
        public static final String F_FROMMESSAGEUSER = "fromMessageUser";
        public static final String F_TOUSERID = "toUserId";
        public static final String F_TONICKNAME = "toNickname";
        public static final String F_TOAVATAR = "toAvatar";
        public static final String F_TOMESSAGEUSER = "toMessageUser";
        public static final String F_HADPLAYED = "hadplayed";
        public static final String F_MSG_RESERVED_ONE = "msgReservedOne"; // 是否是系统消息(wink,follow)
        public static final String F_MSG_RESERVED_TWO = "msgReservedTwo"; // 保留字段
        public static final String F_MSG_RESERVED_THREE = "msgReservedThree";
        public static final String F_MSG_SEND_TIMES = "sendTimes";
        public static final String F_MSG_STICKY_TOP = "stickyTop";// 置顶时间

        /***************************************************消息发送状态*********************************************************/
        public static final String MSG_STATUS_FAILED = "0";
        public static final String MSG_STATUS_SUCCEEDED = "1";
        public static final String MSG_STATUS_SENDING = "2";
        public static final String MSG_STATUS_RECEIVED = "3";

        /***************************************************特殊消息类型*********************************************************/
        public static final String MSG_TYPE_FOLLOW = "follow";//关注/取消关注类型
        public static final String MSG_TYPE_POST = "post";//请求类型
        public static final String MSG_TYPE_WINK = "wink";
        public static final String MSG_TYPE_UN_FOLLOW= "unfollow";//取消关注

    }

    public static class WinkedStatus {

        /**
         * 已挤眼
         */
        public static final int WINK_YES = 0x01;
        /**
         * 未挤眼
         */
        public static final int WINK_NO = 0x00;

    }

    public static class StrangerStatus {
        /**
         * 已挤眼
         */
        public static final int STRANGER_YES = 0x01;
        /**
         * 未挤眼
         */
        public static final int STRANGER_NO = 0x00;
    }


    /*********************************************日志数据库*********************************************************/
    public static class Moment {
        public static final String DB_NAME_MOMENT = "moment_" + ShareFileUtils.getString(ShareFileUtils.ID, "") + ".db";

    }

    /*********************************************其它数据库*********************************************************/
    public static class Other {
        public static final String DB_NAME_OTHER = "other_" + ShareFileUtils.getString(ShareFileUtils.ID, "") + ".db";
    }


}
