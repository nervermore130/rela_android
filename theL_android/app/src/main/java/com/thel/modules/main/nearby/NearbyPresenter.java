package com.thel.modules.main.nearby;

/**
 * Created by waiarl on 2017/9/21.
 */

public class NearbyPresenter implements NearbyContract.Presenter {


    private final NearbyContract.View nearbyView;

    public NearbyPresenter(NearbyContract.View nearbyView) {
        this.nearbyView = nearbyView;
        nearbyView.setPresenter(this);
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {

    }
}
