package com.thel.modules.live.liverank;


import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.constants.TheLConstants;
import com.thel.imp.BaseFunctionFragment;
import com.thel.modules.live.bean.LiveGiftsRankingBean;
import com.thel.modules.live.bean.LiveGiftsRankingInfoBean;
import com.thel.ui.decoration.DefaultItemDivider;
import com.thel.utils.StringUtils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Setsail on 2016.5.19.
 */
public class LiveGiftRankingListFragment extends BaseFunctionFragment implements LiveGiftRankingContract.View {

    public static final String TAG_DAILY = "today";
    public static final String TAG_WEEKLY = "week";
    public static final String TAG_TOTAL = "all";
    public static final String TAG_MOUTH = "month";

    private LiveGiftRankingContract.Presenter presenter;
    private SwipeRefreshLayout swipe_container;
    private RecyclerView recyclerview;
    private LinearLayoutManager manager;
    private LiveGiftRankAdapter adapter;
    private List<LiveGiftsRankingBean> list = new ArrayList<>();

    private String userId;
    private String tag;
    private boolean isFirstCreate = false;// 用来判断是不是第一次创建页面，懒加载数据
    private TextView txt_default;
    private TextView txt_header;
    private SpannableString liveRankTitleSp;
    private ShowRankInterface showRankInterface;

    public static LiveGiftRankingListFragment getInstance(Bundle bundle) {
        LiveGiftRankingListFragment instance = new LiveGiftRankingListFragment();
        instance.setArguments(bundle);
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Bundle bundle = getArguments();
        userId = bundle.getString(TheLConstants.BUNDLE_KEY_USER_ID);
        tag = bundle.getString(TheLConstants.BUNDLE_KEY_FRAGMENT_TAG);
        isFirstCreate = true;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_live_show_rank, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        findViewById(view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        new LiveGiftRankingPresenter(this);
        setListener();
        if (getUserVisibleHint()) {
            isFirstCreate = false;
            if (swipe_container != null)
                swipe_container.post(new Runnable() {
                    @Override
                    public void run() {
                        swipe_container.setRefreshing(true);
                    }
                });
            refreshAll();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser && isFirstCreate) {
            isFirstCreate = false;
            if (swipe_container != null)
                swipe_container.post(new Runnable() {
                    @Override
                    public void run() {
                        swipe_container.setRefreshing(true);
                    }
                });
            refreshAll();
        }

        if (!TextUtils.isEmpty(tag))
            if (isVisibleToUser) {
                MobclickAgent.onPageStart(this.getClass().getName() + ":" + tag);
            } else {
                MobclickAgent.onPageEnd(this.getClass().getName() + ":" + tag);
            }
    }

    private void refreshAll() {
        if (presenter != null) {
            presenter.getRefreshData(userId, tag);
        }
    }

    @Override
    public void setPresenter(LiveGiftRankingContract.Presenter presenter) {
        this.presenter = presenter;
    }

    private void findViewById(View view) {
        swipe_container = view.findViewById(R.id.swipe_container);
        txt_default = view.findViewById(R.id.txt_default);
        txt_default.setText(R.string.info_list_empty);
        ViewUtils.initSwipeRefreshLayout(swipe_container);
        recyclerview = view.findViewById(R.id.recyclerview);
        manager = new LinearLayoutManager(getActivity());
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerview.setLayoutManager(manager);
        adapter = new LiveGiftRankAdapter(list);
        recyclerview.setAdapter(adapter);
        addHeader();
        recyclerview.addItemDecoration(new DefaultItemDivider(getContext(), LinearLayoutManager.VERTICAL, R.color.gray, 1, 3 + adapter.getHeaderLayoutCount()));
    }

    private void addHeader() {
        final View header = LayoutInflater.from(getContext()).inflate(R.layout.live_gift_rank_header_veiw, null);
        txt_header = header.findViewById(R.id.txt_header);
        txt_header.setVisibility(View.GONE);
        //adapter.addHeaderView(header);
    }


    private void setListener() {
        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshAll();
            }
        });
    }

    public interface ShowRankInterface {
        void setLiveRankTitleSp(String content, String rank, String dou);
    }

    public void setLiveRankLisener(ShowRankInterface showRankInterface) {
        this.showRankInterface = showRankInterface;
    }

    @Override
    public void showRefreshData(List<LiveGiftsRankingBean> dataList) {
        if (dataList == null || dataList.size() == 0) {
            return;
        }

        list.clear();
        list.addAll(dataList);
        adapter.setNewData(list);
    }

    @Override
    public void requestFinish() {
        if (swipe_container != null)
            swipe_container.postDelayed(new Runnable() {

                @Override
                public void run() {
                    if (swipe_container != null && swipe_container.isRefreshing())
                        swipe_container.setRefreshing(false);
                }
            }, 1000);
        closeLoading();
    }

    @Override
    public void showEmptyData(boolean show) {
        if (show) {
            txt_default.setVisibility(View.VISIBLE);
        } else {
            txt_default.setVisibility(View.GONE);
        }
    }

    /**
     * 设置头部
     *
     * @param rank
     * @param dou
     */
    @Override
    public void showHeader(String rank, String dou) {
        int strRes;
        if (TAG_DAILY.equals(tag)) {
            strRes = R.string.live_gift_rank_today_title;
        } else if (TAG_WEEKLY.equals(tag)) {
            strRes = R.string.live_gift_rank_weekly_title;
        } else {
            strRes = R.string.live_gift_rank_mouth_title;
        }
        final String content = StringUtils.getString(strRes, rank, dou);
        SpannableString liveRankTitleSp = getLiveRankTitleSp(content, rank, dou);
        this.liveRankTitleSp = liveRankTitleSp;

        if (showRankInterface != null) {
            showRankInterface.setLiveRankTitleSp(content, rank, dou);
        }

    }

    /***直播排名信息
     * @param rankInfo
     **/
    @Override
    public void showRankInfoView(LiveGiftsRankingInfoBean rankInfo) {
        showHeader(rankInfo.rank + "", rankInfo.delta + "");
    }

    public SpannableString getLiveRankData() {
        return liveRankTitleSp;

    }

    private SpannableString getLiveRankTitleSp(String content, String rank, String dou) {
        final SpannableString sp = new SpannableString(content);
        final int start1 = content.indexOf(rank);
        final int end1 = rank.length() + start1;
        final int start2 = content.lastIndexOf(dou);
        final int end2 = start2 + dou.length();
        sp.setSpan(new ForegroundColorSpan(getColor(R.color.live_gift_rank_header_sp_color)), start1, end1, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        sp.setSpan(new AbsoluteSizeSpan(15, true), start1, end1, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        sp.setSpan(new ForegroundColorSpan(getColor(R.color.live_gift_rank_header_sp_color)), start2, end2, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        sp.setSpan(new AbsoluteSizeSpan(15, true), start2, end2, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        return sp;
    }

    @Override
    public void onFollowStatusChanged(int followStatus, String userId, String nickName, String avatar) {
        super.onFollowStatusChanged(followStatus, userId, nickName, avatar);
        final int size = adapter.getData().size();
        final int headCount = adapter.getHeaderLayoutCount();
        for (int i = 0; i < size; i++) {
            final LiveGiftsRankingBean bean = adapter.getItem(i);
            if ((bean.user.id + "").equals(userId)) {
                bean.user.isFollow = followStatus;
                adapter.notifyItemChanged(i + headCount);
            }
        }
    }

    private int getColor(int color) {
        return ContextCompat.getColor(TheLApp.getContext(), color);
    }

}
