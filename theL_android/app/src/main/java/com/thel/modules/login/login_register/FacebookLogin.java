package com.thel.modules.login.login_register;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.thel.R;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.RfSBridgeHandlerFactory;
import com.thel.modules.login.email.EmailLoginActivity;
import com.thel.modules.main.MainActivity;
import com.thel.network.LoginInterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.api.loginapi.bean.SignInBean;
import com.thel.ui.LoadingDialogImpl;
import com.thel.utils.DialogUtil;
import com.thel.utils.GsonUtils;
import com.thel.utils.JsonUtils;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.ToastUtils;
import com.thel.utils.UserUtils;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONObject;

import io.flutter.plugin.common.MethodChannel;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import me.rela.rf_s_bridge.RfSBridgePlugin;
import me.rela.rf_s_bridge.RfSResultCallback;
import me.rela.rf_s_bridge.new_router.NewUniversalRouter;
import me.rela.rf_s_bridge.router.UniversalRouter;

public class FacebookLogin {

    private static FacebookLogin instance;

    private String nickName;

    private String avatar;

    private LoadingDialogImpl loadingDialog;

    private FacebookLogin() {


    }

    public static FacebookLogin getInstance() {
        if (instance == null) {
            instance = new FacebookLogin();
        }
        return instance;
    }

    public void login(Activity activity, CallbackManager callbackManager, RfSResultCallback rfSResultCallback) {

        loadingDialog = LoadingDialogImpl.LoadingDialogInstance.getInstance(activity);

        loadingDialog.showLoading();

        L.d("FacebookLogin", " activity : " + activity.getClass().getSimpleName());

        LoginButton fbLogin = new LoginButton(activity);
        fbLogin.setReadPermissions("public_profile");
        fbLogin.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {

                L.d("FacebookLogin", " ------onSuccess------ ");

                signIn(activity, loginResult.getAccessToken(), rfSResultCallback);
            }

            @Override
            public void onCancel() {

                L.d("FacebookLogin", " ------onCancel------ ");

                MobclickAgent.onEvent(activity, "user_cancel_fb_login");
                ToastUtils.showToastShort(activity, "facebook cancel");

                if (loadingDialog != null) {
                    loadingDialog.closeDialog();
                }

            }

            @Override
            public void onError(FacebookException error) {

                L.d("FacebookLogin", " onError " + error.getMessage());

                MobclickAgent.onEvent(activity, "fb_login_error");
                ToastUtils.showToastShort(activity, "The signature is wrong and the application is in the development mode");

                if (loadingDialog != null) {
                    loadingDialog.closeDialog();
                }
            }
        });

        fbLogin.performClick();
    }

    public void signIn(Activity activity, AccessToken accessToken, RfSResultCallback rfSResultCallback) {

        if (loadingDialog == null) {
            loadingDialog = LoadingDialogImpl.LoadingDialogInstance.getInstance(activity);
            loadingDialog.showLoading();
        }

        GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(final JSONObject jsonObject, GraphResponse graphResponse) {
                MobclickAgent.onEvent(activity, "fb_auth_succeed");// facebook注册转化率统计：facebook授权成功
                // FB性别： male or female
                String gender = JsonUtils.getString(jsonObject, "gender", "");
                if ("male".equals(gender)) {
                    MobclickAgent.onEvent(activity, "fb_male_login");// facebook登录性别是男性打点
                    final DialogInterface.OnClickListener listener0 = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    };
                    final DialogInterface.OnClickListener listener1 = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            MobclickAgent.onEvent(activity, "check_i_am_a_lady");// 点击我是女生按钮
                            dialog.dismiss();
                            String id = JsonUtils.getString(jsonObject, "id", "");
                            nickName = JsonUtils.getString(jsonObject, "name", "");
                            avatar = "https://graph.facebook.com/" + id + "/picture?logType=large";

                            MobclickAgent.onEvent(activity, "begin_request_loginOrRegister_interface");// 开始调用登录接口打点

                            fbSignIn(activity, id, accessToken.getToken(), avatar, nickName, rfSResultCallback);
                        }
                    };
                    DialogUtil.showConfirmDialog(activity, "", activity.getString(R.string.register_activity_test_hint), activity.getString(R.string.i_am_a_lady), activity.getString(R.string.info_quit), listener1, listener0);
                    return;
                }

                MobclickAgent.onEvent(activity, "fb_female_login");// 女性性别校验成功
                String id = JsonUtils.getString(jsonObject, "id", "");
                nickName = JsonUtils.getString(jsonObject, "name", "");
                avatar = "https://graph.facebook.com/" + id + "/picture?logType=large";

                MobclickAgent.onEvent(activity, "begin_request_loginOrRegister_interface");// 开始调用登录接口打点

                fbSignIn(activity, id, accessToken.getToken(), avatar, nickName, rfSResultCallback);

            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,link,email,gender");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void fbSignIn(Activity activity, String id, String token, String avatar, String nickName, RfSResultCallback rfSResultCallback) {
        RequestBusiness.getInstance()
                .signIn("fb", "", "", "", "", "", id, token, avatar, nickName, null)
                .onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new LoginInterceptorSubscribe<SignInBean>() {
                    @Override
                    public void onNext(SignInBean data) {
                        super.onNext(data);
                        if (data != null && data.data != null && activity != null) {
                            if (data.data.user != null) {

                                MobclickAgent.onEvent(activity, "register_or_login_succeed");// 手机注册转化率统计：注册或登录成功
                                ShareFileUtils.saveUserData(data);
                                ShareFileUtils.setBoolean(ShareFileUtils.HAS_LOGGED, true);

                                ToastUtils.showToastShort(activity, activity.getString(R.string.login_activity_success));

                                if (data.data.user.isNewUser()) {

                                    ShareFileUtils.setBoolean(ShareFileUtils.IS_NEW_USER, true);

                                    String resJson = "{\"code\":\"facebook\",\"data\":" + GsonUtils.createJsonString(data.data) + "}";

                                    if (rfSResultCallback != null) {
                                        rfSResultCallback.onResult(resJson);
                                    }
                                } else {

                                    ShareFileUtils.setBoolean(ShareFileUtils.IS_NEW_USER, false);

                                    NewUniversalRouter.sharedInstance.flutterPopToRoot(false);

                                    RfSBridgePlugin.getInstance().activeRpc("EventLogin", UserUtils.getMyUserId(), null);

                                    RfSBridgeHandlerFactory.getInstance().getFlutterPushImpl().pushUserInfo();

                                    RfSBridgeHandlerFactory.getInstance().getFlutterPushImpl().pushMyUserId();

                                    if (activity instanceof MainActivity) {
                                        MainActivity mainActivity = (MainActivity) activity;
                                        mainActivity.init();
                                    }
                                }

                            }

                            if (loadingDialog != null) {
                                loadingDialog.closeDialog();
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);

                        if (loadingDialog != null) {
                            loadingDialog.closeDialog();
                        }

                        ToastUtils.showCenterToastShort(activity, errDataBean.errdesc);
                    }
                });
    }

}
