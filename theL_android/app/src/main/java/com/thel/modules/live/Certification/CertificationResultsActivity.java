package com.thel.modules.live.Certification;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.constants.TheLConstants;
import com.thel.modules.live.bean.ZmxyCheckCallBackBean;
import com.thel.modules.main.home.moments.ReleaseLiveMomentActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * 认证结果的页面
 * Created by lingwei on 2018/3/20.
 */

public class CertificationResultsActivity extends BaseActivity {

    private TextView post_live_or_try;
    private TextView tx_verify_reason;
    private ImageView iv_verify_result;
    private TextView become_author;
    private int perm;
    private LinearLayout lin_back;
    private LinearLayout verify_success_view;
    private LinearLayout verify_filed_view;
    private TextView return_home;
    private LinearLayout ll_show_result;
    private int certificationType;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.certification_result_layout);
        certificationType = ShareFileUtils.getInt(ShareFileUtils.CERTIFICATION_TYPE, ZhimaCertificationActivity.LIVE_PREMISSION);
        initView();
        initData();
        setListener();
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void initView() {
        TextView txt_title = findViewById(R.id.txt_title);
        post_live_or_try = findView(R.id.post_live_or_try);
        tx_verify_reason = findView(R.id.tx_verify_reason);
        iv_verify_result = findView(R.id.iv_verify_result);
        become_author = findView(R.id.tx_rela_author);
        lin_back = findView(R.id.lin_back);
        verify_success_view = findView(R.id.verify_success);
        verify_filed_view = findView(R.id.verify_filed);
        ll_show_result = findView(R.id.ll_show_result);
        return_home = findView(R.id.return_home);
        LinearLayout lin_more = findView(R.id.lin_more);
        lin_more.setVisibility(View.GONE);
        txt_title.setText(R.string.certification_result);
        if (certificationType == ZhimaCertificationActivity.ONSEAT_AUTH) {
            return_home.setVisibility(View.INVISIBLE);
            post_live_or_try.setText(R.string.return_live);
        }
    }

    private void initData() {

        final String action = getIntent().getAction();
        if (Intent.ACTION_VIEW.equals(action)) {//如果是从外部进入
            Uri uri = getIntent().getData();
            if (uri != null) {
                L.d("芝麻认证", "进入");

                String params = uri.getQueryParameter("biz_content");
                String sign = uri.getQueryParameter("sign");
                L.d("芝麻认证", params + "---" + sign);

                postCheckCallback(params+"", sign+"");
            }
        }
        L.d("芝麻认证成功", "走了");


    }

    private void setListener() {

        post_live_or_try.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //认证失败 跳转到芝麻认证页面重新认证
                if (perm == 0) {
                    Intent intent = new Intent(CertificationResultsActivity.this, ZhimaCertificationActivity.class);
                    intent.putExtra(TheLConstants.BUNDLE_KEY_INTENT_FROM, certificationType);
                    startActivity(intent);
                    finish();
                } else {//认证成功 去发布日志
                    if (certificationType == ZhimaCertificationActivity.ONSEAT_AUTH) {
                        finish();
                    } else {
                        final Intent intent = new Intent(CertificationResultsActivity.this, ReleaseLiveMomentActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }
            }
        });
        lin_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
        return_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CertificationResultsActivity.this.finish();
            }
        });
    }

    private void postCheckCallback(String params, String sign) {
        Flowable<ZmxyCheckCallBackBean> flowable = RequestBusiness.getInstance().postCheckCallback(sign, params);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<ZmxyCheckCallBackBean>() {
            @Override
            public void onNext(ZmxyCheckCallBackBean data) {
                super.onNext(data);
                getCheckCallBackResult(data);
                L.d("芝麻认证成功", data.data.perm);

            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                L.d("芝麻认证失败", t.getMessage());
                ll_show_result.setVisibility(View.VISIBLE);
                verify_filed_view.setVisibility(View.VISIBLE);
                verify_success_view.setVisibility(View.GONE);
                post_live_or_try.setText(R.string.try_agin);
            }

            @Override
            public void onComplete() {
                super.onComplete();
            }
        });
    }

    private void getCheckCallBackResult(ZmxyCheckCallBackBean zmxyCheckCallBackBean) {
        if (zmxyCheckCallBackBean != null && zmxyCheckCallBackBean.data != null) {
            //验证失败
            this.perm = zmxyCheckCallBackBean.data.perm;
            ShareFileUtils.setInt(TheLConstants.live_permit, zmxyCheckCallBackBean.data.perm);
            ll_show_result.setVisibility(View.VISIBLE);
            if (zmxyCheckCallBackBean.data.perm == 0) {
                verify_filed_view.setVisibility(View.VISIBLE);
                verify_success_view.setVisibility(View.GONE);
                post_live_or_try.setText(R.string.try_agin);
                tx_verify_reason.setText(zmxyCheckCallBackBean.data.reason);
                iv_verify_result.setImageResource(R.mipmap.icn_verify_fail);
                GrowingIOUtil.track(CertificationResultsActivity.this, "zhimaResultPageView", "失败页面");

            } else {
                //验证成功
                verify_filed_view.setVisibility(View.GONE);
                verify_success_view.setVisibility(View.VISIBLE);
                post_live_or_try.setText(R.string.immediately_post_live);
                iv_verify_result.setImageResource(R.mipmap.icn_verify_success);
                become_author.setText(TheLApp.context.getString(R.string.become_rela_live_broadcaster, zmxyCheckCallBackBean.data.permRank));
                GrowingIOUtil.track(CertificationResultsActivity.this, "zhimaResultPageView", "成功页面");
            }
        }

    }
}
