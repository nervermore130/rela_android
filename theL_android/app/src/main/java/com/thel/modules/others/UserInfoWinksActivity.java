package com.thel.modules.others;

import android.content.Intent;
import android.os.Bundle;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.bean.gift.WinkBean;
import com.thel.bean.gift.WinkListBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.modules.main.me.match.eventcollect.collect.SocialLogUtils;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.ui.widget.DefaultEmptyView;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;
import com.thel.ui.widget.recyclerview.decoration.DefaultItemDivider;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * 用户信息的挤眼页面
 *
 * @author lingwei
 */
public class UserInfoWinksActivity extends BaseActivity {

    private LinearLayout lin_back;
    private TextView txt_title;
    private SwipeRefreshLayout swipe_container;
    private RecyclerView mRecyclerView;
    private DefaultEmptyView bg_my_winklist_default;
    // 当前的moment对象
    private String userId;
    private ArrayList<WinkBean> winkList = new ArrayList<WinkBean>();
    private int pageSize = 20;
    // 刷新类型，全部刷新（即下拉刷新）为1，还是加载下一页为2
    public int refreshType = 0;
    public final int REFRESH_TYPE_ALL = 1;
    public final int REFRESH_TYPE_NEXT_PAGE = 2;
    private static long lastClickTime = 0;
    private long curPage = 0;//当前页，默认是1
    private LinearLayoutManager manager;
    private boolean haveNextPage = false;
    private UserWinkListAdapter adapter;
    private DefaultEmptyView bg_her_winklist_default;
    private boolean isMySelf;
    private String from_page;
    private String from_page_id;
    private String pageId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.userinfo_winks_activity);
        findViewById();
        getIntentData();
        setListener();
    }

    private void getIntentData() {
        final Intent intent = getIntent();
        if (intent == null) {
            finish();
            return;
        }
        userId = intent.getStringExtra(TheLConstants.BUNDLE_KEY_USER_ID);
        isMySelf = ShareFileUtils.getString(ShareFileUtils.ID, "").equals(userId);
        from_page = intent.getStringExtra(TheLConstants.FROM_PAGE);
        from_page_id = intent.getStringExtra(TheLConstants.FROM_PAGE_ID);
        refreshType = REFRESH_TYPE_ALL;
        curPage = 0;
        processBusiness();
        pageId = Utils.getPageId();

    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(this.getClass().getName());
        MobclickAgent.onPause(this);
    }

    private void jumpToUserInfo(int userId) {
//        Intent intent = new Intent();
//        intent.setClass(this, UserInfoActivity.class);
//        intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, userId + "");
//        startActivity(intent);
        FlutterRouterConfig.Companion.gotoUserInfo(userId + "");
        SocialLogUtils.getInstance().traceSocialData("my.popularity", pageId, "head", from_page, from_page_id);
    }

    protected void findViewById() {
        lin_back = this.findViewById(R.id.lin_back);
        txt_title = this.findViewById(R.id.txt_title);
        this.findViewById(R.id.lin_more).setVisibility(View.GONE);
        swipe_container = this.findViewById(R.id.swipe_container);
        ViewUtils.initSwipeRefreshLayout(swipe_container);
        mRecyclerView = this.findViewById(R.id.recyclerview);
        bg_my_winklist_default = this.findViewById(R.id.bg_my_winklist_default);
        bg_her_winklist_default = this.findViewById(R.id.bg_her_winklist_default);
        bg_my_winklist_default.setVisibility(View.GONE);
        bg_her_winklist_default.setVisibility(View.GONE);
        manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.addItemDecoration(new DefaultItemDivider(this, LinearLayoutManager.VERTICAL, R.color.gray, 1, true, true));
        mRecyclerView.setHasFixedSize(true);

        adapter = new UserWinkListAdapter(winkList);
        mRecyclerView.setAdapter(adapter);
        swipe_container.post(new Runnable() {
            @Override
            public void run() {
                swipe_container.setRefreshing(true);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(this.getClass().getName());
        MobclickAgent.onResume(this);

    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    protected void processBusiness() {
        Flowable<WinkListBean> flowable = RequestBusiness.getInstance().getUserWinkList(userId, curPage + "", pageSize + "");
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<WinkListBean>() {

            @Override
            public void onNext(WinkListBean winkListBean) {
                super.onNext(winkListBean);
                refreshAll(winkListBean);

            }

            @Override
            public void onError(Throwable t) {
                loadMoreFiled();
                requestFinish();
                super.onError(t);

            }
        });

    }

    protected void setListener() {

        lin_back.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                ViewUtils.preventViewMultipleClick(arg0, 2000);
                finish();
            }
        });

        txt_title.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (MotionEvent.ACTION_DOWN == event.getAction()) {
                    if (System.currentTimeMillis() - lastClickTime < 300) {
                        ViewUtils.preventViewMultipleTouch(v, 2000);
                        if (mRecyclerView != null) {
                            mRecyclerView.scrollToPosition(0);
                        }
                    }
                    lastClickTime = System.currentTimeMillis();
                }
                return true;
            }
        });

        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                refreshType = REFRESH_TYPE_ALL;
                curPage = 0;
                processBusiness();
            }
        });
        adapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                jumpToUserInfo(adapter.getItem(position).userId);
            }
        });
        adapter.setOnLoadMoreListener(new BaseRecyclerViewAdapter.RequestLoadMoreListener() {//加载更多
            @Override
            public void onLoadMoreRequested() {
                mRecyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        if (haveNextPage) {
                            refreshType = REFRESH_TYPE_NEXT_PAGE;
                            processBusiness();
                        } else {
                            adapter.openLoadMore(0, false);
                            if (winkList.size() > 0) {
                                View view = getLayoutInflater().inflate(R.layout.load_more_footer_layout, (ViewGroup) mRecyclerView.getParent(), false);
                                ((TextView) view.findViewById(R.id.text)).setText(getString(R.string.info_no_more));
                                adapter.addFooterView(view);
                            }
                        }
                    }
                });
            }
        });
        adapter.setReloadMoreListener(new BaseRecyclerViewAdapter.ReloadMoreListener() {//加载更多失败后重新加载
            @Override
            public void reloadMore() {
                refreshType = REFRESH_TYPE_NEXT_PAGE;
                adapter.removeAllFooterView();
                adapter.openLoadMore(true);
                processBusiness();
            }


        });

    }

    public void refreshAll(WinkListBean result) {
        requestFinish();
        if (result.data != null) {
            if (REFRESH_TYPE_ALL == refreshType) {
                winkList.clear();
            }
            result.data.filterBlock();
            winkList.addAll(result.data.winkList);
            setDefaultView();
            // 刷新评论列表
            haveNextPage = result.data.haveNextPage;
            curPage = result.data.cursor;

            if (REFRESH_TYPE_ALL == refreshType) {
                adapter.removeAllFooterView();
                adapter.setNewData(winkList);
                if (haveNextPage) {
                    adapter.openLoadMore(winkList.size(), true);
                }
            } else {
                adapter.notifyDataChangedAfterLoadMore(true, winkList.size());
            }
            if (REFRESH_TYPE_ALL == refreshType) {//刷新回到顶部
                if (mRecyclerView.getChildCount() > 0) {
                    mRecyclerView.scrollToPosition(0);
                }
            }
            if (result.data.totalPopularity == 0) {

                txt_title.setText(getString(R.string.add_popularity_count_null));
            } else {
                txt_title.setText(getString(R.string.userinfo_activity_wink_even, result.data.totalPopularity + ""));

            }
        } else {
            txt_title.setText(getString(R.string.add_popularity_count_null));
        }
        requestFinish();
    }

    private void setDefaultView() {
        if (isMySelf) {

            if (winkList.size() == 0) {//如果没有下一页并且数据为0，就显示默认界面
                bg_my_winklist_default.setVisibility(View.VISIBLE);
            } else {
                bg_my_winklist_default.setVisibility(View.GONE);
            }
        } else {
            if (winkList.size() == 0) {//如果没有下一页并且数据为0，就显示默认界面
                bg_her_winklist_default.setVisibility(View.VISIBLE);
            } else {
                bg_her_winklist_default.setVisibility(View.GONE);
            }
        }
    }

    class UserWinkListAdapter extends BaseRecyclerViewAdapter<WinkBean> {

        public UserWinkListAdapter(List<WinkBean> data) {
            super(R.layout.userinfo_wink_list_item, data);
        }

        @Override
        protected void convert(BaseViewHolder helper, WinkBean bean) {

            if (bean.verified <= 0) {// 非加V用户，第一行为昵称和简介
                helper.setVisibility(R.id.line2, View.VISIBLE);
                helper.setVisibility(R.id.img_indentify, View.GONE);
                helper.setText(R.id.txt_desc, bean.intro);
            } else {// 加V用户，显示昵称、认证图标、认证信息
                helper.setVisibility(R.id.line2, View.VISIBLE);
                helper.setText(R.id.txt_desc, bean.verifyIntro);
                helper.setVisibility(R.id.img_indentify, View.VISIBLE);
                if (bean.verified == 1) {// 个人加V
                    helper.setImageResource(R.id.img_indentify, R.mipmap.icn_verify_person);
                } else {// 企业加V
                    helper.setImageResource(R.id.img_indentify, R.mipmap.icn_verify_enterprise);
                }
            }

            // 角色设定 0=unknow,1=t,2=p,3=h,5=bi
            if (bean.roleName.equals("0")) {
                helper.setImageResource(R.id.img_role, R.mipmap.icn_role_unknow);
            } else if (bean.roleName.equals("1")) {
                helper.setImageResource(R.id.img_role, R.mipmap.icn_role_t);
            } else if (bean.roleName.equals("2")) {
                helper.setImageResource(R.id.img_role, R.mipmap.icn_role_p);
            } else if (bean.roleName.equals("3")) {
                helper.setImageResource(R.id.img_role, R.mipmap.icn_role_h);
            } else if (bean.roleName.equals("5")) {
                helper.setImageResource(R.id.img_role, R.mipmap.icn_role_bi);
            } else if (bean.roleName.equals("6")) {
                helper.setImageResource(R.id.img_role, R.mipmap.icn_role_s);
            } else {
                helper.setImageResource(R.id.img_role, R.mipmap.icn_role_unknow);
            }

            helper.setImageUrl(R.id.img_thumb, bean.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE);

            helper.setText(R.id.txt_name, bean.nickName);
            helper.setText(R.id.txt_distance, getString(R.string.contribute_popularity, bean.popularity));
        }
    }

    private void requestFinish() {
        if (!isDestroyed() && swipe_container != null)
            swipe_container.postDelayed(new Runnable() {

                @Override
                public void run() {
                    if (swipe_container != null && swipe_container.isRefreshing())
                        swipe_container.setRefreshing(false);
                }
            }, 1000);
    }

    private void loadMoreFiled() {
        if (mRecyclerView != null && refreshType == REFRESH_TYPE_NEXT_PAGE) {
            mRecyclerView.post(new Runnable() {
                @Override
                public void run() {
                    adapter.loadMoreFailed((ViewGroup) mRecyclerView.getParent());
                }
            });
        }
    }

}
