package com.thel.modules.live.liverank;

import android.graphics.drawable.GradientDrawable;
import androidx.core.content.ContextCompat;
import android.view.View;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.constants.TheLConstants;
import com.thel.imp.follow.FollowStatusChangedImpl;
import com.thel.imp.follow.FollowStatusChangedListener;
import com.thel.modules.live.bean.LiveGiftsRankingBean;
import com.thel.ui.widget.UserLevelImageView;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;
import com.thel.utils.ToastUtils;
import com.thel.utils.Utils;

import java.util.List;

/**
 * Created by waiarl on 2017/10/26.
 */

public class LiveGiftRankAdapter extends BaseRecyclerViewAdapter<LiveGiftsRankingBean> {
    private final int radius;

    public LiveGiftRankAdapter(List<LiveGiftsRankingBean> list) {
        super(R.layout.adapter_live_gift_rank, list);
        radius = Utils.dip2px(TheLApp.getContext(), 10);
    }

    @Override
    protected void convert(BaseViewHolder helper, LiveGiftsRankingBean item) {
        final int position = helper.getLayoutPosition() - getHeaderLayoutCount();

        GradientDrawable myGrad = (GradientDrawable) helper.getView(R.id.txt_follow).getBackground();
        int nickNameColor;
        int followColor;
        int douColor;
        int rankRes;
        int imgRankVisible;
        int txtRankVisible;
        //设置背景
        setRelBg(helper.getView(R.id.rel_bg), position);
        if (position == 0) {//第一名
            nickNameColor = getColor(R.color.white);
            douColor = getColor(R.color.white);
            followColor = getColor(R.color.white);
            myGrad.setStroke(Utils.dip2px(TheLApp.getContext(), 1), getColor(R.color.white));
            myGrad.setColor(getColor(R.color.transparent));
            rankRes = R.mipmap.icn_live_rank_1;
            imgRankVisible = View.VISIBLE;
            txtRankVisible = View.GONE;
        } else if (position == 1) {//第二名
            nickNameColor = getColor(R.color.white);
            douColor = getColor(R.color.white);
            followColor = getColor(R.color.white);
            myGrad.setStroke(Utils.dip2px(TheLApp.getContext(), 1), getColor(R.color.white));
            myGrad.setColor(getColor(R.color.transparent));
            rankRes = R.mipmap.icn_live_rank_2;
            imgRankVisible = View.VISIBLE;
            txtRankVisible = View.GONE;
        } else if (position == 2) {//第三名
            nickNameColor = getColor(R.color.white);
            douColor = getColor(R.color.white);
            followColor = getColor(R.color.white);
            myGrad.setStroke(Utils.dip2px(TheLApp.getContext(), 1), getColor(R.color.white));
            myGrad.setColor(getColor(R.color.transparent));
            rankRes = R.mipmap.icn_live_rank_3;
            imgRankVisible = View.VISIBLE;
            txtRankVisible = View.GONE;
        } else {
            nickNameColor = getColor(R.color.text_color_dark_gray);
            douColor = getColor(R.color.text_color_light_gray);
            followColor = getColor(R.color.text_color_green);
            myGrad.setStroke(Utils.dip2px(TheLApp.getContext(), 1), getColor(R.color.text_color_green));
            myGrad.setColor(getColor(R.color.transparent));
            rankRes = R.mipmap.icn_live_rank_1;
            imgRankVisible = View.GONE;
            txtRankVisible = View.VISIBLE;
        }

        helper.setTextColor(R.id.txt_nickname, nickNameColor);
        helper.setText(R.id.txt_amount, TheLApp.getContext().getString(R.string.credits_unit2, item.gold));
        helper.setTextColor(R.id.txt_amount, douColor);
        helper.setImageResource(R.id.img_rank, rankRes);
        helper.setVisibility(R.id.img_rank, imgRankVisible);
        helper.setVisibility(R.id.txt_rank, txtRankVisible);
        if (Utils.isChaneseLanguage()) {
            helper.setText(R.id.txt_rank, (position + 1) + "");
        } else {
            helper.setText(R.id.txt_rank, (position + 1) + "");
        }
        helper.setTextColor(R.id.txt_follow, followColor);
        if (item.user.isFollow == 0 && !Utils.isMyself(item.user.id + "")) {
            helper.setText(R.id.txt_follow, TheLApp.context.getString(R.string.userinfo_activity_follow));
        } else {
            helper.setText(R.id.txt_follow, TheLApp.context.getString(R.string.userinfo_activity_followed));
        }
        setListener(helper, item);
        //榜单隐身
        if (item.user.isCloaking == 1) {
            helper.setVisibility(R.id.txt_follow, View.GONE);
            helper.setText(R.id.txt_nickname, TheLApp.context.getString(R.string.secret_guard));
            helper.setImageResource(R.id.avatar, R.mipmap.liveroom_invisible);
            helper.getView(R.id.img_level).setVisibility(View.GONE);

        } else {
            helper.setVisibility(R.id.txt_follow, View.VISIBLE);
            helper.setText(R.id.txt_nickname, item.user.nickName);
            helper.setCircleImageViewUrl(R.id.avatar, R.mipmap.icon_user, item.user.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE);
            ((UserLevelImageView) helper.getView(R.id.img_level)).initView(item.user.userLevel);
            helper.getView(R.id.img_level).setVisibility(View.VISIBLE);
        }
    }


    /**
     * 设置背景
     *
     * @param view
     * @param position
     */
    private void setRelBg(View view, int position) {
        int[] colors = new int[]{getColor(R.color.transparent), getColor(R.color.transparent)};
        if (position == 0) {
            colors = new int[]{getColor(R.color.live_rank_1_start),
                    getColor(R.color.live_rank_1_end)};
        } else if (position == 1) {
            colors = new int[]{getColor(R.color.live_rank_2_start),
                    getColor(R.color.live_rank_2_end)};
        } else if (position == 2) {
            colors = new int[]{getColor(R.color.live_rank_3_start),
                    getColor(R.color.live_rank_3_end)};
        }
        final GradientDrawable drawable = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, colors);
        drawable.setCornerRadius(radius);
        view.setBackground(drawable);
    }

    private int getColor(int color) {
        return ContextCompat.getColor(TheLApp.getContext(), color);
    }

    private void setListener(BaseViewHolder helper, final LiveGiftsRankingBean item) {
        helper.getView(R.id.txt_follow).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                helper.setText(R.id.txt_follow, TheLApp.context.getString(R.string.userinfo_activity_followed));
                FollowStatusChangedImpl.followUserWithNoDialog(item.user.id + "", FollowStatusChangedListener.ACTION_TYPE_FOLLOW, item.user.nickName, item.user.avatar);
                item.user.isFollow = 1;
            }
        });
        helper.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item.user.isCloaking == 1) {
                    ToastUtils.showCenterToastShort(mContext, mContext.getString(R.string.guard_hide));
                }
            }
        });

    }
}
