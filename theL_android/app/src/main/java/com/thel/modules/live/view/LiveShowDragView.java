package com.thel.modules.live.view;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.postprocessors.IterativeBoxBlurPostProcessor;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.thel.R;
import com.thel.manager.ImageLoaderManager;
import com.thel.utils.AppInit;
import com.thel.utils.ImageUtils;
import com.thel.utils.L;

/**
 * Created by waiarl on 2017/11/1.
 */

public class LiveShowDragView extends RelativeLayout {
    private Context mContext;
    private SimpleDraweeView img_bg;
    private String imageUrl;

    public LiveShowDragView(Context context) {
        this(context, null);
    }

    public LiveShowDragView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LiveShowDragView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }

    private void init() {
        inflate(mContext, R.layout.live_show_drag_view, this);
        img_bg = findViewById(R.id.img_bg);
        L.i("refresh", "init,img_bg=" + (img_bg == null));
    }

    public LiveShowDragView initView(String imageUrl) {
        if (!TextUtils.isEmpty(imageUrl) && !imageUrl.equals(this.imageUrl)) {

            setSimpleBlur(img_bg, imageUrl, AppInit.displayMetrics.widthPixels, AppInit.displayMetrics.heightPixels);

        }
        this.imageUrl = imageUrl;
        return this;
    }

    private void setSimpleBlur(SimpleDraweeView simpleDraweeView, String url, int width, int height) {
        if (simpleDraweeView != null && url != null) {
            simpleDraweeView.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(url, width, height))).setPostprocessor(new IterativeBoxBlurPostProcessor(10, 10)).build()).setAutoPlayAnimations(true).build());
        }
    }


    public LiveShowDragView setDefaultBgVisiable(int visiable) {
        img_bg.setVisibility(visiable);
        return this;
    }

    public LiveShowDragView clearDefaultBg() {

        return this;
    }
}
