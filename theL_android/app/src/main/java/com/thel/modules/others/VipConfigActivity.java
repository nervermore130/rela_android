package com.thel.modules.others;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.kyleduo.switchbutton.SwitchButton;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.base.BaseDataBean;
import com.thel.base.IntentFilterActivity;
import com.thel.bean.AdBean;
import com.thel.bean.user.VipConfigBean;
import com.thel.constants.TheLConstants;
import com.thel.growingio.GrowingIoConstant;
import com.thel.imp.vip.VipStatusChangerListener;
import com.thel.imp.vip.VipUtils;
import com.thel.modules.main.home.moments.web.WebViewActivity;
import com.thel.modules.main.me.aboutMe.BuyVipActivity;
import com.thel.modules.main.me.bean.LogInfoBean;
import com.thel.modules.main.me.match.eventcollect.collect.LiveLogUtils;
import com.thel.modules.main.me.match.eventcollect.collect.MatchLogUtils;
import com.thel.modules.main.me.match.eventcollect.collect.PayLogUtils;
import com.thel.modules.main.messages.utils.PushUtils;
import com.thel.modules.select_contacts.SelectContactsActivity;
import com.thel.modules.welcome.WelcomeActivity;
import com.thel.ui.widget.BannerLayout;
import com.thel.utils.BusinessUtils;
import com.thel.utils.DateUtils;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SizeUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 购买会员界面
 * Created by lingwei on 2017/9/23.
 */

public class VipConfigActivity extends BaseActivity implements VipConfigContract.View, View.OnClickListener, VipStatusChangerListener, SwitchButton.OnCheckedChangeListener {
    private LinearLayout lin_reload;
    private ImageView img_reload;
    private TextView txt_reload;
    private ScrollView scroll_main;
    private RelativeLayout rel_bottom;
    private boolean hasInitData = false;

    // 广告控件
    private BannerLayout banner;
    private TextView txt_expire_time;
    private TextView txt_vip_agreement;
    private TextView btn_purchase;
    private boolean isVip = false;
    private int showInfo = 0;
    private int hiding = 0;
    private int incognito = 0;
    private int followRemind = 0;
    private int liveHiding = 0;
    private int msgHiding = 0;

    private TextView btn_give_vip;//赠送会员
    //  是否购买会员
    private boolean hasBoughtVip = false;
    private VipConfigContract.Presenter vipPresenter;
    private VipConfigBean vipConfigBean;
    private ImageView checkbox_vip_show_effects;
    private VipUtils vipUtils;
    private RelativeLayout rl_have_vip_baner;
    private SimpleDraweeView img_avatar;
    private ImageView iv_vip_star_level;
    private ImageView img_vip;
    private TextView text_surplus_days;
    private TextView text_is_vip;
    private TextView tv_match_scroll;
    private SwitchButton checkbox_show_info;
    private SwitchButton checkbox_hide;
    private SwitchButton checkbox_unfollow_notify;
    private SwitchButton checkbox_no_trace;
    public static String SHOW_TYPE_RECALL = "RECALL";
    public static String SHOW_TYPE_LIKE = "LIKE";
    public static String SHOW_ANONYMOUS_BROWSING = "ANONYMOUS_BROWSING";
    public static String SHOW_MSG_HIDING = "msg_hiding";
    public static String SHOW_MSG_SNEAK_UP = "msg_sneak_up";
    public static String SHOW_MSG_TIME_MACHINE = "time_machine";
    public static String SHOW_MSG_MATCH_REVERT = "match_revert";
    public static String SHOW_UN_FOLLOW = "un_follow";
    public static String SHOW_SECRETLY_FOLLOW = "secretly_follow";
    public static String SHOW_NEARBY_FILTER = "nearby_filter";

    private String showMatchType;
    private RelativeLayout rl_who_like_me_dialog;
    private RelativeLayout rl_recall_dialog;
    private LinearLayout ll_config_vip;
    private ImageView iv_who_like_me_mark;
    private ImageView iv_recall_mark;
    private ImageView iv_show_info_mark;
    private ImageView iv_no_trace_mark;
    private ImageView iv_hide_mark;
    private ImageView iv_secretly_follow_mark;
    private ImageView iv_stick_moment_mark;
    private RelativeLayout rl_look_show_info;
    private RelativeLayout rl_no_trace;
    private RelativeLayout rl_hide;
    private RelativeLayout rl_secretly_follow;
    private RelativeLayout rl_stick_moment;
    private ImageView iv_unfollow_notify_mark;
    private RelativeLayout rl_unfollow_notify_dialog;
    private RelativeLayout rl_sneak_dialog;
    private SwitchButton checkbox_sneak;
    private ImageView iv_sneak_mark;
    private RelativeLayout rl_time_machine_dialog;
    private ImageView iv_time_machine_mark;

    private RelativeLayout rl_exposure;
    private ImageView iv_more_exposures_mark;
    private String latitude;
    private String longitude;
    private String pageId;
    private RelativeLayout rl_nearby_filter;
    private String from_page_id;
    private String from_page;
    private String fromPage;
    private String fromPageId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vip_config_activity);
        // 如果是从外部进入
        initIntent();

        findViewById();
        new VipConfigPresenter(this);
        vipPresenter.processBusiness();
        initBanner();
        setListener();
        pageId = Utils.getPageId();
        vipUtils = new VipUtils();
        vipUtils.registerReceiver(this);
        latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
        longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");

        traceBuyVipLog();
    }

    private void initIntent() {
        String action = getIntent().getAction();
        if (Intent.ACTION_VIEW.equals(action)) {
            if (!ShareFileUtils.getBoolean(ShareFileUtils.HAS_LOGGED, false)) {//如果没登陆
                startActivity(new Intent(this, WelcomeActivity.class));
                finish();
                return;
            }
        }
        Intent intent = getIntent();
        showMatchType = intent.getStringExtra("showType");
        fromPage = intent.getStringExtra("fromPage");
        fromPageId = intent.getStringExtra("fromPageId");
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            from_page_id = bundle.getString(ShareFileUtils.MATCH_FROM_PAGE_ID, "");
            from_page = bundle.getString(ShareFileUtils.MATCH_FROM_PAGE, "");

        }

    }

    private void initBanner() {
        banner = findViewById(R.id.no_vip_banner); // 获取Gallery组件
        banner.setVisibility(View.GONE);

        if (UserUtils.getUserVipLevel() > 0) {
            rl_have_vip_baner.setVisibility(View.VISIBLE);
            String userAvatar = UserUtils.getUserAvatar();
            if (!TextUtils.isEmpty(userAvatar)) {
                img_avatar.setImageURI(Uri.parse(userAvatar));
                switch (UserUtils.getUserVipLevel()) {
                    case 1:

                        img_vip.setImageResource(R.mipmap.icn_vip_1);
                        iv_vip_star_level.setImageResource(R.mipmap.icon_silk_vip1);
                        break;
                    case 2:
                        img_vip.setImageResource(R.mipmap.icn_vip_2);
                        iv_vip_star_level.setImageResource(R.mipmap.icon__silk_vip2);


                        break;
                    case 3:
                        img_vip.setImageResource(R.mipmap.icn_vip_3);
                        iv_vip_star_level.setImageResource(R.mipmap.icon__silk_vip3);

                        break;
                    case 4:
                        img_vip.setImageResource(R.mipmap.icn_vip_4);
                        iv_vip_star_level.setImageResource(R.mipmap.icon__silk_vip4);


                        break;
                }
            }

        } else {
            banner.setAutoPlay(false);

            banner.setShowIndicator(true);
            banner.setLayoutParams(new LinearLayout.LayoutParams(ListView.LayoutParams.MATCH_PARENT, (int) (TheLApp.getContext().getResources().getDisplayMetrics().widthPixels / TheLConstants.ad_aspect_ratio)));
            getAdsData();
            rl_have_vip_baner.setVisibility(View.GONE);
        }

    }

    private void getAdsData() {
        vipPresenter.loadAdvert();
    }

    @SuppressLint("WrongViewCast")
    private void findViewById() {
        scroll_main = findViewById(R.id.scroll_main);
        rel_bottom = findViewById(R.id.rel_bottom);
        lin_reload = findViewById(R.id.lin_reload);
        img_reload = findViewById(R.id.img_reload);
        txt_reload = findViewById(R.id.txt_reload);
        ((TextView) findViewById(R.id.txt_title)).setText(getString(R.string.vip_config_act_title));
        checkbox_no_trace = findViewById(R.id.checkbox_no_trace);

        checkbox_unfollow_notify = findViewById(R.id.checkbox_unfollow_notify);
        //  checkbox_vip_show_effects = (ImageView) findViewById(R.id.checkbox_vip_show_effects);
        txt_expire_time = findViewById(R.id.txt_expire_time);
        txt_vip_agreement = findViewById(R.id.txt_vip_agreement);
        txt_vip_agreement.setText(getString(R.string.vip_agreement_header) + "《" + getString(R.string.vip_agreement) + "》");
        btn_purchase = findViewById(R.id.btn_purchase);
        //新增赠送会员按钮
        btn_give_vip = findViewById(R.id.btn_give_vip);
        findViewById(R.id.img_more).setVisibility(View.GONE);
        rl_have_vip_baner = findViewById(R.id.rl_have_vip);
        img_avatar = findViewById(R.id.img_avatar);
        iv_vip_star_level = findViewById(R.id.iv_vip_star_level);
        img_vip = findViewById(R.id.img_vip);
        /**
         * 没有开通会员，热门功能显示查看，点击弹出详细功能图窗。
         * */
        checkbox_show_info = findViewById(R.id.checkbox_show_info);
        rl_look_show_info = findViewById(R.id.rl_look_show_info);//会员身份

        checkbox_no_trace = findViewById(R.id.checkbox_no_trace);
        rl_no_trace = findViewById(R.id.rl_no_trace);//无痕浏览

        //隐身
        checkbox_hide = findViewById(R.id.checkbox_hide);
        rl_hide = findViewById(R.id.rl_hide);

        rl_secretly_follow = findViewById(R.id.rl_secretly_follow);

        //置顶日志
        iv_stick_moment_mark = findViewById(R.id.iv_stick_moment_mark);
        rl_stick_moment = findViewById(R.id.rl_stick_moment);

        //取消关注提醒
        rl_unfollow_notify_dialog = findViewById(R.id.rl_unfollow_notify_dialog);
        iv_unfollow_notify_mark = findViewById(R.id.iv_unfollow_notify_mark);
        text_surplus_days = findViewById(R.id.text_surplus_days);
        text_is_vip = findViewById(R.id.text_is_vip);
        rl_who_like_me_dialog = findViewById(R.id.rl_who_like_me_dialog);
        //更多曝光
        rl_exposure = findViewById(R.id.rl_exposure);
        //悄悄查看
        rl_sneak_dialog = findViewById(R.id.rl_sneak_dialog);
        checkbox_sneak = findViewById(R.id.checkbox_sneak);
        iv_sneak_mark = findViewById(R.id.iv_sneak_mark);

        //时光机

        rl_time_machine_dialog = findViewById(R.id.rl_time_machine_dialog);
        iv_time_machine_mark = findViewById(R.id.iv_time_machine_mark);

        /**
         * 附近筛选
         * */
        rl_nearby_filter = findViewById(R.id.rl_nearby_filter);

        tv_match_scroll = findViewById(R.id.tv_match_scroll);
        rl_recall_dialog = findViewById(R.id.rl_recall_dialog);
        ll_config_vip = findViewById(R.id.ll_config_vip);
        iv_who_like_me_mark = findViewById(R.id.iv_who_like_me_mark);
        iv_recall_mark = findViewById(R.id.iv_recall_mark);
        iv_show_info_mark = findViewById(R.id.iv_show_info_mark);
        iv_no_trace_mark = findViewById(R.id.iv_no_trace_mark);
        iv_hide_mark = findViewById(R.id.iv_hide_mark);
        iv_secretly_follow_mark = findViewById(R.id.iv_secretly_follow_mark);
        iv_more_exposures_mark = findViewById(R.id.iv_more_exposures_mark);
        if (!TextUtils.isEmpty(showMatchType)) {

            ll_config_vip.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    ll_config_vip.getViewTreeObserver().removeOnPreDrawListener(this);
                    int width = ll_config_vip.getWidth();
                    int height = ll_config_vip.getHeight();
                    L.d("vipconfigActivityPrint", "height" + height);

                    L.d("vipconfigActivityPrint", "width" + width);

                    if (SHOW_TYPE_RECALL.equals(showMatchType)) {
                        scroll_main.scrollTo(0, (int) (height / 1.2));
                        rl_recall_dialog.setBackgroundColor(ContextCompat.getColor(TheLApp.context, R.color.yellow_light));
                    } else if (SHOW_TYPE_LIKE.equals(showMatchType)) {
                        scroll_main.scrollTo(0, (int) (height / 1.2));

                        rl_who_like_me_dialog.setBackgroundColor(ContextCompat.getColor(TheLApp.context, R.color.yellow_light));
                    } else if (SHOW_MSG_TIME_MACHINE.equals(showMatchType)) {
                        scroll_main.scrollTo(0, rl_time_machine_dialog.getTop() - SizeUtils.dip2px(TheLApp.context, 30));

                        rl_time_machine_dialog.setBackgroundColor(ContextCompat.getColor(TheLApp.context, R.color.yellow_light));
                    } else if (SHOW_MSG_HIDING.equals(showMatchType)) {
                        scroll_main.scrollTo(0, rl_hide.getTop() - SizeUtils.dip2px(TheLApp.context, 30));

                        rl_hide.setBackgroundColor(ContextCompat.getColor(TheLApp.context, R.color.yellow_light));
                    } else if (SHOW_MSG_MATCH_REVERT.equals(showMatchType)) {
                        scroll_main.scrollTo(0, rl_recall_dialog.getTop() - SizeUtils.dip2px(TheLApp.context, 30));

                        rl_recall_dialog.setBackgroundColor(ContextCompat.getColor(TheLApp.context, R.color.yellow_light));
                    }
                    if (SHOW_ANONYMOUS_BROWSING.equals(showMatchType)) {
                        rl_no_trace.setBackgroundColor(ContextCompat.getColor(TheLApp.context, R.color.yellow_light));
                    }

                    if (SHOW_MSG_SNEAK_UP.equals(showMatchType)) {
                        scroll_main.scrollTo(0, rl_sneak_dialog.getTop() - SizeUtils.dip2px(TheLApp.context, 30));
                        rl_sneak_dialog.setBackgroundColor(ContextCompat.getColor(TheLApp.context, R.color.yellow_light));

                    }

                    if (SHOW_UN_FOLLOW.equals(showMatchType)) {
                        scroll_main.scrollTo(0, rl_unfollow_notify_dialog.getTop() - SizeUtils.dip2px(TheLApp.context, 30));
                        rl_unfollow_notify_dialog.setBackgroundColor(ContextCompat.getColor(TheLApp.context, R.color.yellow_light));
                    }

                    if (SHOW_SECRETLY_FOLLOW.equals(showMatchType)) {
                        scroll_main.scrollTo(0, rl_secretly_follow.getTop() - SizeUtils.dip2px(TheLApp.context, 30));
                        rl_secretly_follow.setBackgroundColor(ContextCompat.getColor(TheLApp.context, R.color.yellow_light));
                    }

                    if (SHOW_NEARBY_FILTER.equals(showMatchType)) {
                        scroll_main.scrollTo(0, rl_nearby_filter.getTop() - SizeUtils.dip2px(TheLApp.context, 30));
                        rl_nearby_filter.setBackgroundColor(ContextCompat.getColor(TheLApp.context, R.color.yellow_light));
                    }

                    return false;
                }
            });


        }

    }

    private void setListener() {
        findViewById(R.id.lin_back).setOnClickListener(this);
        img_reload.setOnClickListener(this);
        btn_give_vip.setOnClickListener(this);
        btn_purchase.setOnClickListener(this);
        rl_look_show_info.setOnClickListener(this);
        rl_no_trace.setOnClickListener(this);
        rl_hide.setOnClickListener(this);
        iv_show_info_mark.setOnClickListener(this);
        iv_no_trace_mark.setOnClickListener(this);
        iv_hide_mark.setOnClickListener(this);
        rl_secretly_follow.setOnClickListener(this);
        iv_secretly_follow_mark.setOnClickListener(this);
        rl_stick_moment.setOnClickListener(this);
        iv_stick_moment_mark.setOnClickListener(this);
        iv_unfollow_notify_mark.setOnClickListener(this);
        rl_unfollow_notify_dialog.setOnClickListener(this);
        rl_exposure.setOnClickListener(this);
        rl_who_like_me_dialog.setOnClickListener(this);
        rl_recall_dialog.setOnClickListener(this);

        rl_sneak_dialog.setOnClickListener(this);
        rl_time_machine_dialog.setOnClickListener(this);
        iv_time_machine_mark.setOnClickListener(this);
        iv_sneak_mark.setOnClickListener(this);
        rl_nearby_filter.setOnClickListener(this);
        checkbox_show_info.setOnClickListener(this);
        checkbox_hide.setOnClickListener(this);
        checkbox_no_trace.setOnClickListener(this);
        checkbox_sneak.setOnClickListener(this);
        checkbox_unfollow_notify.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(this.getClass().getName());
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(this.getClass().getName());
        MobclickAgent.onPause(this);
    }

    @Override
    public void setPresenter(VipConfigContract.Presenter presenter) {
        this.vipPresenter = presenter;
    }

    @Override
    public void onClick(View view) {
        ViewUtils.preventViewMultipleClick(view, 2000);
        int viewId = view.getId();
        switch (viewId) {
            case R.id.lin_back:
                if (hasBoughtVip) {
                    setResult(RESULT_OK);
                }
                PushUtils.finish(VipConfigActivity.this);
                break;
            case R.id.img_reload:
                Animation anim = AnimationUtils.loadAnimation(TheLApp.getContext(), R.anim.rotate);
                LinearInterpolator lir = new LinearInterpolator();
                anim.setInterpolator(lir);
                view.setLayerType(View.LAYER_TYPE_HARDWARE, null);
                view.startAnimation(anim);
                txt_reload.setText(getString(R.string.info_reloading));
                vipPresenter.processBusiness();
                break;
            case R.id.checkbox_show_info:
                if (isVip) {
                    showLoading();
                    vipPresenter.setVipConfigs(liveHiding, showInfo, hiding, incognito, followRemind, msgHiding, 1);

                } else {// 不是vip则跳转到购买vip页面
                    MobclickAgent.onEvent(TheLApp.getContext(), "check_vip_badge");
                    gotoBuyVip();
                    checkbox_show_info.setChecked(false);
                }
                break;
            case R.id.checkbox_no_trace:
                if (isVip) {
                    showLoading();
                    vipPresenter.setVipConfigs(liveHiding, showInfo, hiding, incognito, followRemind, msgHiding, 3);
                } else {// 不是vip则跳转到购买vip页面
                    MobclickAgent.onEvent(TheLApp.getContext(), "check_vip_no_trace");
                    gotoBuyVip();
                    checkbox_no_trace.setChecked(false);
                }
                break;
            case R.id.checkbox_hide:
                if (isVip) {
                    showLoading();
                    vipPresenter.setVipConfigs(liveHiding, showInfo, hiding, incognito, followRemind, msgHiding, 2);
                } else {// 不是vip则跳转到购买vip页面
                    MobclickAgent.onEvent(TheLApp.getContext(), "check_vip_hide");
                    gotoBuyVip();
                    checkbox_hide.setChecked(false);
                }
                break;
            case R.id.checkbox_unfollow_notify:
                if (isVip) {
                    showLoading();
                    vipPresenter.setVipConfigs(liveHiding, showInfo, hiding, incognito, followRemind, msgHiding, 4);
                } else {// 不是vip则跳转到购买vip页面
                    MobclickAgent.onEvent(TheLApp.getContext(), "check_vip_unfollow_notify");
                    gotoBuyVip();
                    checkbox_unfollow_notify.setChecked(false);
                }
                break;
            case R.id.checkbox_sneak:
                if (isVip) {
                    showLoading();
                    vipPresenter.setVipConfigs(liveHiding, showInfo, hiding, incognito, followRemind, msgHiding, 5);
                } else {// 不是vip则跳转到购买vip页面
                    MobclickAgent.onEvent(TheLApp.getContext(), "check_vip_sneak");
                    gotoBuyVip();
                    checkbox_sneak.setChecked(false);
                }
                break;
          /*  case R.id.checkbox_vip_show_effects:
                if (isVip) {
                    showLoading();
                    vipPresenter.setVipConfigs(liveHiding, showInfo, hiding, incognito, followRemind, 0);
                } else {
                    MobclickAgent.onEvent(TheLApp.getContext(), "check_vip_live_effects");
                    gotoBuyVip();
                }
                break;*/
            case R.id.btn_purchase:
                gotoBuyVip();
                break;
            case R.id.btn_give_vip:
                gotoGiveVip();
                break;
            case R.id.iv_show_info_mark:
            case R.id.rl_look_show_info:
                VipDetailViewDialog dialog1 = new VipDetailViewDialog(this);
                dialog1.setSeeingType(VipDetailViewDialog.VIP_IDENTITY);
                dialog1.show();

                LogInfoBean logInfoBean = new LogInfoBean();
                logInfoBean.page = "vip";
                logInfoBean.page_id = pageId;
                logInfoBean.activity = "view";
                logInfoBean.from_page = from_page;
                logInfoBean.from_page_id = from_page_id;
                logInfoBean.lat = latitude;
                logInfoBean.lng = longitude;
                MatchLogUtils.getInstance().addLog(logInfoBean);


                break;
            case R.id.iv_no_trace_mark:
            case R.id.rl_no_trace:
                VipDetailViewDialog dialog2 = new VipDetailViewDialog(this);

                dialog2.setSeeingType(VipDetailViewDialog.VIP_NO_TRACE);
                dialog2.show();

                LogInfoBean logInfoBean2 = new LogInfoBean();
                logInfoBean2.page = "vip";
                logInfoBean2.page_id = pageId;
                logInfoBean2.activity = "view";
                logInfoBean2.from_page = from_page;
                logInfoBean2.from_page_id = from_page_id;
                logInfoBean2.lat = latitude;
                logInfoBean2.lng = longitude;
                MatchLogUtils.getInstance().addLog(logInfoBean2);


                break;
            case R.id.iv_hide_mark:
            case R.id.rl_hide:
                VipDetailViewDialog dialog3 = new VipDetailViewDialog(this);

                dialog3.setSeeingType(VipDetailViewDialog.VIP_HIDE);
                dialog3.show();

                LogInfoBean logInfoBean3 = new LogInfoBean();
                logInfoBean3.page = "vip";
                logInfoBean3.page_id = pageId;
                logInfoBean3.activity = "view";
                logInfoBean3.from_page = from_page;
                logInfoBean3.from_page_id = from_page_id;
                logInfoBean3.lat = latitude;
                logInfoBean3.lng = longitude;
                MatchLogUtils.getInstance().addLog(logInfoBean3);


                break;
            case R.id.iv_secretly_follow_mark:
            case R.id.rl_secretly_follow:
                VipDetailViewDialog dialog4 = new VipDetailViewDialog(this);

                dialog4.setSeeingType(VipDetailViewDialog.VIP_SECRETLY_FOLLOW);
                dialog4.show();
                LogInfoBean logInfoBean4 = new LogInfoBean();
                logInfoBean4.page = "vip";
                logInfoBean4.page_id = pageId;
                logInfoBean4.activity = "view";
                logInfoBean4.from_page = from_page;
                logInfoBean4.from_page_id = from_page_id;
                logInfoBean4.lat = latitude;
                logInfoBean4.lng = longitude;
                MatchLogUtils.getInstance().addLog(logInfoBean4);


                break;
            case R.id.iv_stick_moment_mark:
            case R.id.rl_stick_moment:
                VipDetailViewDialog dialog5 = new VipDetailViewDialog(this);

                dialog5.setSeeingType(VipDetailViewDialog.VIP_MOMENT_TOP);
                dialog5.show();


                LogInfoBean logInfoBean5 = new LogInfoBean();
                logInfoBean5.page = "vip";
                logInfoBean5.page_id = pageId;
                logInfoBean5.activity = "view";
                logInfoBean5.from_page = from_page;
                logInfoBean5.from_page_id = from_page_id;
                logInfoBean5.lat = latitude;
                logInfoBean5.lng = longitude;
                MatchLogUtils.getInstance().addLog(logInfoBean5);


                break;
            case R.id.iv_unfollow_notify_mark:
            case R.id.rl_unfollow_notify_dialog:
                VipDetailViewDialog dialog6 = new VipDetailViewDialog(this);

                dialog6.setSeeingType(VipDetailViewDialog.VIP_UNFOLLOW_REMIND);
                dialog6.show();

                LogInfoBean logInfoBean6 = new LogInfoBean();
                logInfoBean6.page = "vip";
                logInfoBean6.page_id = pageId;
                logInfoBean6.activity = "view";
                logInfoBean6.from_page = from_page;
                logInfoBean6.from_page_id = from_page_id;
                logInfoBean6.lat = latitude;
                logInfoBean6.lng = longitude;
                MatchLogUtils.getInstance().addLog(logInfoBean6);


                break;
            case R.id.rl_who_like_me_dialog:
                VipDetailViewDialog dialog7 = new VipDetailViewDialog(this);
                dialog7.setSeeingType(VipDetailViewDialog.VIP_WHO_LIKE_ME);
                dialog7.show();

                LogInfoBean logInfoBean7 = new LogInfoBean();
                logInfoBean7.page = "vip";
                logInfoBean7.page_id = pageId;
                logInfoBean7.activity = "view";
                logInfoBean7.from_page = from_page;
                logInfoBean7.from_page_id = from_page_id;
                logInfoBean7.lat = latitude;
                logInfoBean7.lng = longitude;
                MatchLogUtils.getInstance().addLog(logInfoBean7);


                break;
            case R.id.rl_recall_dialog:
                VipDetailViewDialog dialog8 = new VipDetailViewDialog(this);

                dialog8.setSeeingType(VipDetailViewDialog.VIP_RECALL);
                dialog8.show();

                LogInfoBean logInfoBean8 = new LogInfoBean();
                logInfoBean8.page = "vip";
                logInfoBean8.page_id = pageId;
                logInfoBean8.activity = "view";
                logInfoBean8.from_page = from_page;
                logInfoBean8.from_page_id = from_page_id;
                logInfoBean8.lat = latitude;
                logInfoBean8.lng = longitude;
                MatchLogUtils.getInstance().addLog(logInfoBean8);


                break;
            case R.id.rl_sneak_dialog:
            case R.id.iv_sneak_mark:
                VipDetailViewDialog dialog9 = new VipDetailViewDialog(this);

                dialog9.setSeeingType(VipDetailViewDialog.VIP_SNEAK);
                dialog9.show();

                LogInfoBean logInfoBean9 = new LogInfoBean();
                logInfoBean9.page = "vip";
                logInfoBean9.page_id = pageId;
                logInfoBean9.activity = "view";
                logInfoBean9.from_page = from_page;
                logInfoBean9.from_page_id = from_page_id;
                logInfoBean9.lat = latitude;
                logInfoBean9.lng = longitude;
                MatchLogUtils.getInstance().addLog(logInfoBean9);

                break;
            case R.id.rl_time_machine_dialog:
            case R.id.iv_time_machine_mark:
                VipDetailViewDialog dialog10 = new VipDetailViewDialog(this);

                dialog10.setSeeingType(VipDetailViewDialog.VIP_TIME_MACINE);
                dialog10.show();

                LogInfoBean logInfoBean10 = new LogInfoBean();
                logInfoBean10.page = "vip";
                logInfoBean10.page_id = pageId;
                logInfoBean10.activity = "view";
                logInfoBean10.from_page = from_page;
                logInfoBean10.from_page_id = from_page_id;
                logInfoBean10.lat = latitude;
                logInfoBean10.lng = longitude;
                MatchLogUtils.getInstance().addLog(logInfoBean10);

                break;
            case R.id.rl_nearby_filter:
                VipDetailViewDialog dialog11 = new VipDetailViewDialog(this);
                dialog11.setSeeingType(VipDetailViewDialog.VIP_NEARBY_FILTER);
                dialog11.show();

                LogInfoBean logInfoBean11 = new LogInfoBean();
                logInfoBean11.page = "vip";
                logInfoBean11.page_id = pageId;
                logInfoBean11.activity = "view";
                logInfoBean11.from_page = from_page;
                logInfoBean11.from_page_id = from_page_id;
                logInfoBean11.lat = latitude;
                logInfoBean11.lng = longitude;
                MatchLogUtils.getInstance().addLog(logInfoBean11);

                break;
            case R.id.rl_exposure:
                VipDetailViewDialog dialog12 = new VipDetailViewDialog(this);
                dialog12.setSeeingType(VipDetailViewDialog.VIP_EXPOSURE);
                dialog12.show();

                LogInfoBean logInfoBean12= new LogInfoBean();
                logInfoBean12.page = "vip";
                logInfoBean12.page_id = pageId;
                logInfoBean12.activity = "view";
                logInfoBean12.from_page = from_page;
                logInfoBean12.from_page_id = from_page_id;
                logInfoBean12.lat = latitude;
                logInfoBean12.lng = longitude;
                MatchLogUtils.getInstance().addLog(logInfoBean12);

                break;

            default:
                break;

        }
    }

    @Override
    public void getVipConfig(VipConfigBean vipConfigBean) {
        this.vipConfigBean = vipConfigBean;
        if (vipConfigBean.data != null) {
            hasInitData = true;

            refreshUI(vipConfigBean);
            // 刷新缓存数据
            UserUtils.setUserVipLevel(vipConfigBean.data.vipSetting.level);
        }

    }

    @Override
    public void getConfigResult(BaseDataBean baseDataBean) {
        closeLoading();
    }

    @Override
    public void getConfigResult(int liveHiding, int vipHiding, int hiding, int incognito, int followRemind, int msgHiding) {
        showInfo = vipHiding == -1 ? showInfo : vipHiding;
        this.hiding = hiding == -1 ? this.hiding : hiding;
        this.incognito = incognito == -1 ? this.incognito : incognito;
        this.followRemind = followRemind == -1 ? this.followRemind : followRemind;
        this.liveHiding = liveHiding == -1 ? this.liveHiding : liveHiding;
        this.msgHiding = msgHiding == -1 ? this.msgHiding : msgHiding;

        ShareFileUtils.setInt(ShareFileUtils.MSG_HIDING, msgHiding);
        ShareFileUtils.setInt(ShareFileUtils.VIP_INCOGNITO, incognito);

        //发送广播更新附近的人列表隐身状态
        Intent intent = new Intent();
        intent.setAction(TheLConstants.BROADCAST_ACTION_HIDING);
        intent.putExtra("hiding", hiding);
        sendBroadcast(intent);

        refreshCheckbox();
        closeLoading();

    }

    @Override
    public void refreshAdArea(AdBean adBean) {
        if (adBean == null || adBean.data == null || adBean.data.map_list == null) {
            return;
        }

        /** 原朋友圈广告 **/
        final List<AdBean.Map_list> adList = adBean.data.map_list;

        List<AdBean.Map_list> willDelete = new ArrayList<>();
        for (AdBean.Map_list bean : adList) {
            if (!bean.advertLocation.equals("vip")) {
                willDelete.add(bean);
            }
        }
        adList.removeAll(willDelete);

        if (!adList.isEmpty()) {
            List<String> advs = new ArrayList<>();
            for (int i = 0; i < adList.size(); i++) {
                advs.add(adList.get(i).advertURL);
            }
            banner.setVisibility(View.VISIBLE);
            banner.setViewUrls(advs);
            // 添加点击事件的监听
            banner.setOnBannerItemClickListener(new BannerLayout.OnBannerItemClickListener() {
                @Override
                public void onItemClick(int position) {
                    BusinessUtils.AdRedirect(adList.get(position));
                }
            });
        }
    }

    private void refreshUI(VipConfigBean vipConfigBean) {
        scroll_main.setVisibility(View.VISIBLE);
        rel_bottom.setVisibility(View.VISIBLE);
        lin_reload.setVisibility(View.GONE);

        showInfo = vipConfigBean.data.vipSetting.vipHiding;
        hiding = vipConfigBean.data.vipSetting.hiding;
        incognito = vipConfigBean.data.vipSetting.incognito;
        followRemind = vipConfigBean.data.vipSetting.followRemind;
        liveHiding = vipConfigBean.data.vipSetting.liveHiding;
        msgHiding = vipConfigBean.data.vipSetting.msgHiding;
        refreshCheckbox();
        if (vipConfigBean.data.vipSetting.level > 0) {
            isVip = true;
            txt_expire_time.setText(vipConfigBean.data.vipSetting.expireTime);
            /**
             * 有会员时将不显示查看  显示开关
             * */
            btn_purchase.setText(getString(R.string.renew_vip));

            int haveExpiredTime = haveExpiredTime(vipConfigBean.data.vipSetting.expireTime);
            if (haveExpiredTime > 0 && haveExpiredTime <= 5) {
                text_surplus_days.setTextColor(ContextCompat.getColor(TheLApp.context, R.color.white));
                text_surplus_days.setBackground(TheLApp.context.getResources().getDrawable(R.drawable.vip_remind_data));
                text_surplus_days.setText(TheLApp.context.getString(R.string.how_days_vip, haveExpiredTime));

            } else {
                text_surplus_days.setTextColor(ContextCompat.getColor(TheLApp.context, R.color.bg_gray));
                text_surplus_days.setBackground(null);
                text_surplus_days.setText(TheLApp.context.getString(R.string.how_days_vip, haveExpiredTime));

            }
        } else {
            isVip = false;
            txt_expire_time.setText(R.string.is_not_vip_yet);
            btn_purchase.setText(getString(R.string.buy_vip));
            if (!TextUtils.isEmpty(vipConfigBean.data.vipSetting.expireTime)) {   //会员已过期
                text_surplus_days.setVisibility(View.GONE);
                text_is_vip.setText(getString(R.string.vip_have_no_use));
            } else {  //没有开通过会员

            }
        }

        String str = getString(R.string.vip_agreement_header) + "《" + getString(R.string.vip_agreement) + "》";
        String strAgreement = getString(R.string.vip_agreement);
        SpannableString spannableString = new SpannableString(str);
        spannableString.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TheLApp.getContext(), WebViewActivity.class);
                intent.putExtra(WebViewActivity.URL, TheLConstants.VIP_AGREEMENT_PAGE_URL);
                intent.putExtra(WebViewActivity.NEED_SECURITY_CHECK, false);
                intent.putExtra("title", "");
                startActivity(intent);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setColor(getResources().getColor(R.color.text_color_green));
                ds.setUnderlineText(true);
            }

        }, str.indexOf(strAgreement), str.indexOf(strAgreement) + strAgreement.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        txt_vip_agreement.setText(spannableString);
        txt_vip_agreement.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void refreshCheckbox() {
        checkbox_show_info.setChecked(showInfo != 0);
        checkbox_hide.setChecked(hiding != 0);
        checkbox_no_trace.setChecked(incognito != 0);
        checkbox_unfollow_notify.setChecked(followRemind != 0);
        checkbox_sneak.setChecked(msgHiding != 0);

    }

    /**
     * 当前时间和会员到期时间比较，如果小于或者等于五的话将返回 0 ，小于或者等于零，将返回-1，大于五返回1
     **/
    private int haveExpiredTime(String levelExpireTime) {
        String nowToDay = DateUtils.getNowToDay();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date dt1 = null;
        Date dt2 = null;
        try {
            dt1 = sdf.parse(nowToDay);
            dt2 = sdf.parse(levelExpireTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long betweenDate = (dt2.getTime() - dt1.getTime()) / (1000 * 60 * 60 * 24);

        L.i("MefragmentData", "开始时间 " + dt1);
        L.i("MefragmentData", "结束时间 " + dt2);
        L.i("MefragmentData", "相隔天数 " + betweenDate);

        return (int) betweenDate;


    }

    /**
     * 续费会员
     */
    private void gotoBuyVip() {

        LogInfoBean logInfoBean = new LogInfoBean();
        logInfoBean.page = "vip";
        logInfoBean.page_id = pageId;
        logInfoBean.activity = "buy";
        logInfoBean.from_page = from_page;
        logInfoBean.from_page_id = from_page_id;
        logInfoBean.lat = latitude;
        logInfoBean.lng = longitude;
        MatchLogUtils.getInstance().addLog(logInfoBean);


        MobclickAgent.onEvent(TheLApp.getContext(), "renew_member_click");
        Intent intent = new Intent(this, BuyVipActivity.class);
        intent.putExtra("isVip", isVip);
        Bundle bundle = new Bundle();
        bundle.putString(ShareFileUtils.MATCH_FROM_PAGE_ID, pageId);
        bundle.putString(ShareFileUtils.MATCH_FROM_PAGE, "vip");
        intent.putExtras(bundle);
        startActivityForResult(intent, 1);

        PayLogUtils.getInstance().reportSoftOrVipPayLog(GrowingIoConstant.PAGE_LIVE_ROOM, pageId, "click", TheLConstants.IsFirstCharge);

    }

    /**
     * 赠送会员
     */
    private void gotoGiveVip() {
        MobclickAgent.onEvent(TheLApp.getContext(), "click_give_members");


        LogInfoBean logInfoBean = new LogInfoBean();
        logInfoBean.page = "vip";
        logInfoBean.page_id = pageId;
        logInfoBean.activity = "gift";
        logInfoBean.from_page = from_page;
        logInfoBean.from_page_id = from_page_id;
        logInfoBean.lat = latitude;
        logInfoBean.lng = longitude;
        MatchLogUtils.getInstance().addLog(logInfoBean);

        Intent intent = new Intent(this, SelectContactsActivity.class);
        intent.putExtra("give_vip", true);
        Bundle bundle = new Bundle();
        bundle.putString(ShareFileUtils.MATCH_FROM_PAGE_ID, pageId);
        bundle.putString(ShareFileUtils.MATCH_FROM_PAGE, "vip");
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            //todo processBusiness();
            hasBoughtVip = true;
        }
    }

    @Override
    public void finish() {

        super.finish();
    }

    @Override
    public void onVipStatusChanged(boolean isVip, int vipLevel) {
        if (isVip) {
            hasBoughtVip = true;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        vipUtils.unRegisterReceiver(this);
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        PushUtils.finish(VipConfigActivity.this);
    }

    public void traceBuyVipLog() {
        try {
            if (!TextUtils.isEmpty(fromPage) && !TextUtils.isEmpty(fromPageId)) {
                LogInfoBean logInfoBean = new LogInfoBean();
                logInfoBean.page = "vip";
                logInfoBean.page_id = pageId;
                logInfoBean.from_page = fromPage;
                logInfoBean.from_page_id = fromPageId;
                MatchLogUtils.getInstance().addLog(logInfoBean);

            }

        } catch (Exception e) {

        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

//        switch (buttonView.getId()) {
//            case R.id.checkbox_show_info:
//                if (isVip) {
//                    showLoading();
//                    vipPresenter.setVipConfigs(liveHiding, showInfo, hiding, incognito, followRemind, msgHiding, 1);
//
//                } else {// 不是vip则跳转到购买vip页面
//                    checkbox_show_info.setChecked(false);
//                    MobclickAgent.onEvent(TheLApp.getContext(), "check_vip_badge");
//                    gotoBuyVip();
//                }
//                break;
//            case R.id.checkbox_hide:
//                if (isVip) {
//                    showLoading();
//                    vipPresenter.setVipConfigs(liveHiding, showInfo, hiding, incognito, followRemind, msgHiding, 2);
//                } else {// 不是vip则跳转到购买vip页面
//                    checkbox_hide.setChecked(false);
//                    MobclickAgent.onEvent(TheLApp.getContext(), "check_vip_hide");
//                    gotoBuyVip();
//                }
//                break;
//            case R.id.checkbox_no_trace:
//                if (isVip) {
//                    showLoading();
//                    vipPresenter.setVipConfigs(liveHiding, showInfo, hiding, incognito, followRemind, msgHiding, 3);
//                } else {// 不是vip则跳转到购买vip页面
//                    checkbox_no_trace.setChecked(false);
//                    MobclickAgent.onEvent(TheLApp.getContext(), "check_vip_no_trace");
//                    gotoBuyVip();
//                }
//                break;
//            case R.id.checkbox_sneak:
//                if (isVip) {
//                    showLoading();
//                    vipPresenter.setVipConfigs(liveHiding, showInfo, hiding, incognito, followRemind, msgHiding, 5);
//                } else {// 不是vip则跳转到购买vip页面
//                    checkbox_sneak.setChecked(false);
//                    MobclickAgent.onEvent(TheLApp.getContext(), "check_vip_sneak");
//                    gotoBuyVip();
//                }
//                break;
//            case R.id.checkbox_unfollow_notify:
//                if (isVip) {
//                    showLoading();
//                    vipPresenter.setVipConfigs(liveHiding, showInfo, hiding, incognito, followRemind, msgHiding, 4);
//                } else {// 不是vip则跳转到购买vip页面
//                    checkbox_unfollow_notify.setChecked(false);
//                    MobclickAgent.onEvent(TheLApp.getContext(), "check_vip_unfollow_notify");
//                    gotoBuyVip();
//                }
//                break;
//            default:
//
//                break;
//        }

    }
}
