package com.thel.modules.main.discover.utils;

import android.graphics.Rect;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.thel.ui.widget.recyclerview.BaseInsertRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;

/**
 * Created by waiarl on 2017/11/6.
 */

public class DiscoverUtils {
    public static void setLiveRoomsRecyclerViewDecoration(RecyclerView recyclerview, final float padding) {
        recyclerview.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                if (parent.getAdapter() instanceof BaseInsertRecyclerViewAdapter) {
                    if (parent.getChildLayoutPosition(view) < ((BaseInsertRecyclerViewAdapter) parent.getAdapter()).getHeaderLayoutCount()) {
                        return;
                    }
                    final int pos = parent.getChildLayoutPosition(view) - ((BaseInsertRecyclerViewAdapter) parent.getAdapter()).getHeaderLayoutCount();
                    final int insertPos = ((BaseInsertRecyclerViewAdapter) parent.getAdapter()).getInsertViewposition();
                    if (pos < insertPos) {
                        if (pos % 2 == 0) {// 左边列
                            outRect.left = (int) padding;
                            outRect.right = (int) (padding / 2);
                        } else {// 右边列
                            outRect.left = (int) (padding / 2);
                            outRect.right = (int) padding;
                        }
                    } else if (pos == insertPos) {//插入的view
                        outRect.left = (int) padding;
                        outRect.right = (int) padding;
                        outRect.top = 0;
                        outRect.bottom = 0;
                        return;
                    } else {
                        if ((pos + insertPos) % 2 != 0) {// 左边列,没有insertView的时候，insertPos=-1;
                            outRect.left = (int) padding;
                            outRect.right = (int) (padding / 2);
                        } else {// 右边列
                            outRect.left = (int) (padding / 2);
                            outRect.right = (int) padding;
                        }
                    }
                    outRect.top = (int) padding;
                    outRect.bottom = 0;
                }
            }
        });
    }

    public static void setLiveClassifyRecyclerViewDecoration(RecyclerView recyclerview, final float padding) {
        recyclerview.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                if (parent.getAdapter() instanceof BaseRecyclerViewAdapter) {
                    if (parent.getChildLayoutPosition(view) < ((BaseRecyclerViewAdapter) parent.getAdapter()).getHeaderLayoutCount()) {
                        return;
                    }
                    final int pos = parent.getChildLayoutPosition(view) - ((BaseRecyclerViewAdapter) parent.getAdapter()).getHeaderLayoutCount();
                    final int insertPos = -1;
                    if (pos < insertPos) {
                        if (pos % 2 == 0) {// 左边列
                            outRect.left = (int) padding;
                            outRect.right = (int) (padding / 2);
                        } else {// 右边列
                            outRect.left = (int) (padding / 2);
                            outRect.right = (int) padding;
                        }
                    } else if (pos == insertPos) {//插入的view
                        outRect.left = (int) padding;
                        outRect.right = (int) padding;
                        outRect.top = 0;
                        outRect.bottom = 0;
                        return;
                    } else {
                        if ((pos + insertPos) % 2 != 0) {// 左边列,没有insertView的时候，insertPos=-1;
                            outRect.left = (int) padding;
                            outRect.right = (int) (padding / 2);
                        } else {// 右边列
                            outRect.left = (int) (padding / 2);
                            outRect.right = (int) padding;
                        }
                    }
                    outRect.top = (int) padding;
                    outRect.bottom = 0;
                }
            }
        });
    }
}
