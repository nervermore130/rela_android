package com.thel.modules.main.me.bean;

import com.thel.base.BaseDataBean;

import java.io.Serializable;
import java.util.List;

/**
 * Match bean 匹配埋点上报
 *
 * @author lingwei
 */
public class MatchReportLogsBean extends BaseDataBean implements Serializable {


    /**
     * /**{"page":"my","page_id":"xxxx_uid","from_page":"","from_page_id":"","user_id":1234,"session_id":"xxxx_uuid", "activity":"match","data":{"msg":3}, "lat":31.2343, "lng": 121.432}
     * 用户图片url以逗号分隔
     */

    public LogsDataBean data;
    public List<LogInfoBean> logs;

    public static class LogsDataBean implements Serializable {

        public String msg;
        public String rank_id;
        public String receiver_id;

    }

    public static class LogInfoBean {
        public String page;
        public String page_id;
        public String from_page;
        public String from_page_id;
        public String activity;
        public String lat;
        public String lng;
        public LogsDataBean data;

    }
}
