package com.thel.modules.live.view;

import android.app.Dialog;
import android.content.Context;

import androidx.annotation.NonNull;

import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.constants.TheLConstants;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.live.bean.LiveRoomBean;
import com.thel.utils.AppInit;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.StringUtils;
import com.thel.utils.ViewUtils;

/**
 * Created by waiarl on 2017/11/5.
 */

public class LiveShowCaptureCloseDialog extends Dialog {
    private static final String TAG = "LiveShowCaptureCloseDialog";
    private final Context mContext;
    private ImageView avatar;
    private TextView live_nickname;
    private TextView live_time;
    private TextView time;
    private TextView view_num;
    private TextView royalties_num;
    private TextView highest_ranking;
    private TextView rela_id_live;
    private TextView rela_id;
    private TextView newly_increased_fans;
    private LiveRoomBean liveRoomBean;
    private View txt_confirm;
    private TextView txt_income;
    private TextView txt_defeat;

    public LiveShowCaptureCloseDialog(@NonNull Context context) {
        this(context, R.style.CustomDialogFullscreen);
    }

    public LiveShowCaptureCloseDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        mContext = context;
        init();
    }

    private void init() {
        setCancelable(false);
        setCanceledOnTouchOutside(false);

        LayoutInflater flater = LayoutInflater.from(mContext);
        View view = flater.inflate(R.layout.live_show_capture_closed_dialog, null);
        setContentView(view);

        avatar = view.findViewById(R.id.avatar);
        live_nickname = view.findViewById(R.id.live_nickname);
        live_time = view.findViewById(R.id.live_time);
        time = view.findViewById(R.id.time);
        view_num = view.findViewById(R.id.view_num);
        royalties_num = view.findViewById(R.id.royalties_num);
        highest_ranking = view.findViewById(R.id.highest_ranking);
        rela_id_live = view.findViewById(R.id.rela_id_live);
        rela_id = view.findViewById(R.id.rela_id);
        final String relaId = ShareFileUtils.getString(ShareFileUtils.USER_THEL_ID, "");
        rela_id.setText("" + relaId);
        newly_increased_fans = view.findViewById(R.id.newly_increased_fans);
        txt_confirm = view.findViewById(R.id.txt_confirm);
        txt_income = view.findViewById(R.id.txt_income);
        txt_defeat = view.findViewById(R.id.txt_defeat);
        view.findViewById(R.id.txt_cancel).setOnClickListener(new View.OnClickListener() {//取消
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                dismiss();
            }
        });
    }

    public LiveShowCaptureCloseDialog initDialog(LiveRoomBean liveRoomBean, View.OnClickListener confirmListener) {

        L.d(TAG, " liveRoomBean.rank : " + liveRoomBean.rank);

        try {
            this.liveRoomBean = liveRoomBean;
            ImageLoaderManager.imageLoaderDefaultCircle(avatar, R.mipmap.icon_user, liveRoomBean.user.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE);
            live_nickname.setText(liveRoomBean.user.nickName);
            //live_time.setText(liveRoomBean.);
            view_num.setText(liveRoomBean.liveUsersCount + "");
            royalties_num.setText(liveRoomBean.gem);
            highest_ranking.setText(liveRoomBean.rank + "");
            txt_confirm.setOnClickListener(confirmListener);
            show();
            WindowManager.LayoutParams lp = getWindow().getAttributes();
            lp.gravity = Gravity.CENTER;
            lp.width = AppInit.displayMetrics.widthPixels; // 设置宽度
            lp.height = AppInit.displayMetrics.heightPixels;
            lp.dimAmount = 0;
            getWindow().setAttributes(lp);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return this;
    }

    /**
     * 刷新数据
     *
     * @param fans
     * @param livetime
     * @param bestrank
     * @return
     */
    public LiveShowCaptureCloseDialog refreshData(int fans, String livetime, int bestrank, double income, double defeat) {

        if (!TextUtils.isEmpty(livetime)) {
            time.setText(livetime + "");
        }
        newly_increased_fans.setText(fans + "");
        highest_ranking.setText(bestrank + "");
        setIncome(income);
        setDefeat(defeat);
        return this;
    }

    private void setDefeat(double defeat) {
        final float de = (float) (Math.round(defeat * 100));

        txt_defeat.setText(StringUtils.getString(R.string.defeated_more_broadcaster, de + "%"));
    }

    private void setIncome(double income) {
        final float de = (float) (Math.round(income * 10000)) / 100;

        String incomeStr = TheLApp.context.getString(R.string.royalties_gained, income + "");

        txt_income.setText(incomeStr);
    }

    public LiveShowCaptureCloseDialog initDialog(LiveRoomBean liveRoomBean, double todayIncome, double defeated, View.OnClickListener onClickListener) {
        setIncome(todayIncome);
        setDefeat(defeated);
        initDialog(liveRoomBean, onClickListener);
        return this;
    }
}
