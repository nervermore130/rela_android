package com.thel.modules.login.init_info;

import android.view.View;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;

import java.util.List;

/**
 * 初始化角色adapter
 * Created by lingwei on 2017/12/7.
 */

public class initRoleAdapter extends BaseRecyclerViewAdapter<String> {

    public initRoleAdapter(List<String> role) {
        super(R.layout.role_adapter_item_layout, role);
    }

    @Override
    protected void convert(BaseViewHolder helper, String item) {
        helper.setText(R.id.title, item);
        String[] role_Array = TheLApp.context.getResources().getStringArray(R.array.userinfo_role);
        if (item.equals(role_Array[7])) {
            helper.setVisibility(R.id.line, View.GONE);
        }
    }
}
