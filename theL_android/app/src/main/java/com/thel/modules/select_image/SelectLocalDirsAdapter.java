package com.thel.modules.select_image;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.thel.R;
import com.thel.app.TheLApp;

import java.util.List;

import video.com.relavideolibrary.Utils.DensityUtils;

/**
 * 选择图片adaper(目前是选单张，预留了选多张)
 *
 * @author Setsail
 */
public class SelectLocalDirsAdapter extends BaseAdapter {

    /**
     * 所有文件夹
     */
    private List<ImageFolderBean> mDirs;

    private LayoutInflater mInflater;

    public SelectLocalDirsAdapter(List<ImageFolderBean> mDatas) {
        this.mDirs = mDatas;

        mInflater = (LayoutInflater) TheLApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mDirs.size();
    }

    @Override
    public Object getItem(int arg0) {
        return mDirs.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        HoldView holdView;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_dir_item, parent, false);
            holdView = new HoldView();

            holdView.id_dir_item_image = convertView.findViewById(R.id.id_dir_item_image);
            holdView.id_dir_item_name = convertView.findViewById(R.id.id_dir_item_name);
            holdView.id_dir_item_count = convertView.findViewById(R.id.id_dir_item_count);

            convertView.setTag(holdView); // 把holdview缓存下来
        } else {
            holdView = (HoldView) convertView.getTag();
        }
        ImageFolderBean folder = mDirs.get(position);

        RequestOptions options = new RequestOptions().transform(new CenterCrop(), new RoundedCorners(DensityUtils.dp2px(4)));
        Glide.with(convertView.getContext()).load(folder.getFirstImagePath()).apply(options).into(holdView.id_dir_item_image);

        holdView.id_dir_item_name.setText(folder.getName());
        holdView.id_dir_item_count.setText(folder.getCount()+"");
        return convertView;
    }

    class HoldView {
        ImageView id_dir_item_image;
        TextView id_dir_item_name;
        TextView id_dir_item_count;
    }
}
