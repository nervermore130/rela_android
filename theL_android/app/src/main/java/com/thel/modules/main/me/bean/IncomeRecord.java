package com.thel.modules.main.me.bean;

import com.thel.base.BaseDataBean;

import java.io.Serializable;

/**
 * 收益记录
 * Created by Setsail on 2016/6/7.
 */
public class IncomeRecord extends BaseDataBean implements Serializable {
    public String id;
    /**
     * 收益
     */
    public double gemDelta;
    /**
     * 当前余额
     */
    public double gem;
    /**
     * 打赏的用户id
     */
    public long fromUserId;
    /**
     * 打赏内容
     */
    public String description;
    /**
     * 记录时间
     */
    public String createTime;
    /**
     * 解析后的日期
     */
    public String date;
    /**
     * 解析后的时间
     */
    public String time;
    /**
     * 是否显示头部
     */
    public boolean showHeader = false;

}
