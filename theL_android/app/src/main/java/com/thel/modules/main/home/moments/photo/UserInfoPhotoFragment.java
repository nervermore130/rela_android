package com.thel.modules.main.home.moments.photo;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.base.BaseFragment;
import com.thel.bean.ImgShareBean;
import com.thel.bean.user.MyImageBean;
import com.thel.bean.user.UserInfoPicBean;
import com.thel.constants.BundleConstants;
import com.thel.constants.TheLConstants;
import com.thel.modules.preview_image.AlbumPhotosPagerAdapter;
import com.thel.modules.preview_image.LocalPhotoSimplePagerAdapter;
import com.thel.modules.preview_image.PhotoNotSharePagerAdapter;
import com.thel.modules.preview_image.PhotoSimplePagerAdapter;
import com.thel.modules.select_image.ImageBean;
import com.thel.ui.widget.HackyViewPager;
import com.thel.utils.DialogUtil;
import com.thel.utils.SharedPrefUtils;
import com.thel.utils.ViewUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by liuyun on 2017/10/25.
 */

public class UserInfoPhotoFragment extends BaseFragment {

    @BindView(R.id.rel_title)
    RelativeLayout rel_title;

    @BindView(R.id.txt_title)
    TextView txt_title;

    @BindView(R.id.lin_back)
    LinearLayout lin_back;

    @BindView(R.id.lin_select)
    LinearLayout lin_select;

    @BindView(R.id.img_select)
    ImageView img_select;

    @BindView(R.id.done)
    Button done;

    @BindView(R.id.img_delete)
    ImageView img_delete;

    @BindView(R.id.btn_set)
    Button btn_set;

    @BindView(R.id.view_pager)
    HackyViewPager view_pager;

    private int position;
    private int totalPhotoCanSee;
    private boolean isDisplayTitle = true;

    private ArrayList<Integer> selectedPhotoIndexes;
    private boolean canDelete = false;

    private boolean canSelect = false;
    // 选择图片，多图预览时，用来保存已选的图片的url
    private ArrayList<String> selectedPhotoUrls;
    private ArrayList<String> photoUrls;
    private int selectAmountLimit = 0;

    // 设置功能
    private boolean canSet = false;
    private MyImageBean curMyImageBean;

    private ArrayList<?> dataList = null;

    private String rela_id;
    private boolean is_waterMark = true;
    private String userId;
    private UserInfoPicBean tempBean;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_photo_userinfo, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Bundle bundle = getArguments();

        int position = bundle.getInt(BundleConstants.POSITION);

        String momentId = bundle.getString(BundleConstants.MOMENTS_ID);

        String fromPage = bundle.getString("fromPage");

        if (fromPage == null) {
            rel_title.setVisibility(View.GONE);
            // 从其他页面，单纯展示图片

            ArrayList<String> photoUrls = bundle.getStringArrayList(TheLConstants.BUNDLE_KEY_PHOTOS);
            rela_id = bundle.getString(TheLConstants.BUNDLE_KEY_RELA_ID);
            is_waterMark = bundle.getBoolean(TheLConstants.IS_WATERMARK, true);
            ImgShareBean imageShareBean = (ImgShareBean) bundle.getSerializable(TheLConstants.BUNDLER_KEY_IMAGESHAREBEAN);
            position = bundle.getInt("position", 0);

            totalPhotoCanSee = photoUrls.size();
            PhotoSimplePagerAdapter photoSimplePagerAdapter = new PhotoSimplePagerAdapter(getContext(), photoUrls, rela_id, is_waterMark, imageShareBean);
            view_pager.setAdapter(photoSimplePagerAdapter);
            view_pager.setCurrentItem(position);

        } else if (fromPage.equals("DetailedMatchUserinfoActivity")) { //从匹配详情页面
            rel_title.setVisibility(View.GONE);
            // 从其他页面，单纯展示图片

            List<String> photoUrls = bundle.getStringArrayList(TheLConstants.BUNDLE_KEY_PHOTOS);
            rela_id = bundle.getString(TheLConstants.BUNDLE_KEY_RELA_ID);
            is_waterMark = bundle.getBoolean(TheLConstants.IS_WATERMARK, false);
            ImgShareBean imageShareBean = (ImgShareBean) bundle.getSerializable(TheLConstants.BUNDLER_KEY_IMAGESHAREBEAN);
            position = bundle.getInt("position", 0);

            totalPhotoCanSee = photoUrls.size();
            view_pager.setAdapter(new PhotoNotSharePagerAdapter(getContext(), photoUrls, rela_id, is_waterMark, imageShareBean));
            view_pager.setCurrentItem(position);
        } else if (fromPage.equals("UserInfoActivity")) { // 从用户详情页过来
            rel_title.setVisibility(View.GONE);
            ArrayList<UserInfoPicBean> userlist = (ArrayList<UserInfoPicBean>) bundle.getSerializable("userinfo");
            ArrayList<String> photoUrls = new ArrayList<>();
            position = bundle.getInt("position", 0);
            ImgShareBean imageShareBean = (ImgShareBean) bundle.getSerializable(TheLConstants.BUNDLER_KEY_USER_IMAGE);
            UserInfoPicBean userinfoPicBean = (UserInfoPicBean) bundle.getSerializable(TheLConstants.BUNDLE_KEY_PIC_BEAN);
            int totalPhoto = userlist.size();
            for (int i = 0; i < totalPhoto; i++) {// 过滤掉没有密钥的加密图片
                UserInfoPicBean tempBean = userlist.get(i);
                photoUrls.add(tempBean.picUrl);
            }
            totalPhotoCanSee = photoUrls.size();
            rela_id = bundle.getString(TheLConstants.BUNDLE_KEY_RELA_ID);
            userId = bundle.getString("userId");
            view_pager.setAdapter(new AlbumPhotosPagerAdapter(getContext(),"UserInfoActivity", photoUrls, rela_id, is_waterMark, false, imageShareBean, userinfoPicBean, userId));
            view_pager.setCurrentItem(position);
            view_pager.setOffscreenPageLimit(5);
        } else if (fromPage.equals("UploadImageActivity")) { // 从上传照片页过来
            canSet = true;
            btn_set.setVisibility(View.VISIBLE);
            dataList = (ArrayList<MyImageBean>) bundle.getSerializable("userinfo");
            position = bundle.getInt("position", 0);
            totalPhotoCanSee = dataList.size();

            AlbumPhotosPagerAdapter adapter = new AlbumPhotosPagerAdapter(getContext(), dataList, rela_id, is_waterMark, false, null);
            view_pager.setAdapter(adapter);
            view_pager.setCurrentItem(position);
            view_pager.setOffscreenPageLimit(5);

            curMyImageBean = (MyImageBean) dataList.get(position);

        } else if (fromPage.equals("WriteMomentActivity")) {
            canDelete = true;
            img_delete.setVisibility(View.VISIBLE);
            photoUrls = (ArrayList<String>) bundle.getSerializable(TheLConstants.BUNDLE_KEY_PHOTOS);
            selectedPhotoIndexes = new ArrayList<Integer>();
            for (int i = 0; i < photoUrls.size(); i++) {
                selectedPhotoIndexes.add(i);
            }
            position = bundle.getInt("position", 0);
            totalPhotoCanSee = photoUrls.size();
            ArrayList<ImageBean> imageBeans = new ArrayList<>();
            for (int i = 0; i < photoUrls.size(); i++) {
                ImageBean imageBean = new ImageBean();
                imageBean.imageName = photoUrls.get(i);
                imageBeans.add(imageBean);
            }
            LocalPhotoSimplePagerAdapter adapter = new LocalPhotoSimplePagerAdapter(imageBeans);
            view_pager.setAdapter(adapter);
            view_pager.setCurrentItem(position);
        } else if (fromPage.equals("SelectLocalImagesActivity")) {
            canSelect = true;
            lin_back.setVisibility(View.GONE);
            lin_select.setVisibility(View.VISIBLE);
            done.setVisibility(View.VISIBLE);
            photoUrls = new ArrayList<>();
            if (bundle.getBoolean("isPreview", true))
                photoUrls.addAll((ArrayList<String>) bundle.getSerializable(TheLConstants.BUNDLE_KEY_PHOTOS));
            else
                photoUrls.addAll(Arrays.asList(SharedPrefUtils.getStringWithoutEncrypt(SharedPrefUtils.FILE_PHOTO_PATH_CACHE, bundle.getString("dir"), "").split(",")));
            selectedPhotoUrls = (ArrayList<String>) bundle.getSerializable(TheLConstants.BUNDLE_KEY_SELECTED_IMGS);
            position = bundle.getInt("position", 0);
            if (selectedPhotoUrls.contains(photoUrls.get(position))) {
                img_select.setImageResource(R.mipmap.pictures_selected);
            } else {
                img_select.setImageResource(R.mipmap.picture_unselected);
            }
            selectAmountLimit = bundle.getInt(TheLConstants.BUNDLE_KEY_SELECT_AMOUNT, 0);
            totalPhotoCanSee = photoUrls.size();
            ArrayList<ImageBean> imageBeans = new ArrayList<>();
            for (int i = 0; i < photoUrls.size(); i++) {
                ImageBean imageBean = new ImageBean();
                imageBean.imageName = photoUrls.get(i);
                imageBeans.add(imageBean);
            }
            LocalPhotoSimplePagerAdapter adapter = new LocalPhotoSimplePagerAdapter(imageBeans);
            view_pager.setAdapter(adapter);
            if (position >= 0) {
                view_pager.setCurrentItem(position);
            }
            refreshSelectedAmount();
        }

        txt_title.setText((position + 1) + "/" + totalPhotoCanSee);

    }

    protected void setListener() {
        view_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int pos) {
                position = pos;
                txt_title.setText((pos + 1) + "/" + totalPhotoCanSee);
                if (canSelect) {// 可选择模式
                    if (selectedPhotoUrls != null) {
                        if (selectedPhotoUrls.contains(photoUrls.get(pos))) {
                            img_select.setImageResource(R.mipmap.pictures_selected);
                        } else {
                            img_select.setImageResource(R.mipmap.picture_unselected);
                        }
                    }
                } else if (canSet) {
                    curMyImageBean = (MyImageBean) dataList.get(pos);
                }
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int arg0) {

            }
        });

        // 失效了
        view_pager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                isDisplayTitle = !isDisplayTitle;
                rel_title.setVisibility(isDisplayTitle ? View.VISIBLE : View.GONE);
            }
        });

        img_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogUtil.showConfirmDialog(getActivity(), "", getString(R.string.uploadimage_activity_delete_confirm), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        arg0.dismiss();
                        photoUrls.remove(view_pager.getCurrentItem());
                        selectedPhotoIndexes.remove(view_pager.getCurrentItem());
                        if (selectedPhotoIndexes.size() == 0) {
                            getActivity().finish();
                            return;
                        }
                        totalPhotoCanSee = photoUrls.size();
                        view_pager.getAdapter().notifyDataSetChanged();
                        txt_title.setText((view_pager.getCurrentItem() + 1) + "/" + totalPhotoCanSee);
                    }
                });
            }
        });

        img_select.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (selectedPhotoUrls != null) {
                    if (selectedPhotoUrls.contains(photoUrls.get(view_pager.getCurrentItem()))) {//如果已选中，则置为未选
                        ((ImageView) view).setImageResource(R.mipmap.picture_unselected);
                        selectedPhotoUrls.remove(photoUrls.get(view_pager.getCurrentItem()));
                    } else {// 如果未选中，则置为已选
                        if (selectedPhotoUrls.size() == selectAmountLimit) {// 控制选择数量
                            return;
                        }
                        ((ImageView) view).setImageResource(R.mipmap.pictures_selected);
                        selectedPhotoUrls.add(photoUrls.get(view_pager.getCurrentItem()));
                    }
                    refreshSelectedAmount();
                }
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                if (selectedPhotoUrls != null) {
                    intent.putStringArrayListExtra(TheLConstants.BUNDLE_KEY_INDEX, selectedPhotoUrls);
                    getActivity().setResult(TheLConstants.RESULT_CODE_WRITE_MOMENT_DELETE_PICTURE, intent);
                    getActivity().finish();
                }
            }
        });


        btn_set.setOnClickListener(new View.OnClickListener() {
                                       @Override
                                       public void onClick(View view) {

                                           if (curMyImageBean.coverFlag == 1) {// 封面图
                                               DialogUtil.getInstance().showSelectionDialog(getActivity(), new String[]{TheLApp.getContext().getString(R.string.info_delete)}, new AdapterView.OnItemClickListener() {
                                                   @Override
                                                   public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
                                                       DialogUtil.getInstance().closeDialog();
                                                       switch (pos) {
                                                           case 0: // 删除
                                                               deletePhoto();
                                                               break;
                                                           default:
                                                               break;
                                                       }
                                                   }
                                               }, true, 1, null);
                                           } else if (curMyImageBean.isPrivate == 1) {// 隐私图，可以解锁和删除
                                               DialogUtil.getInstance().showSelectionDialog(getActivity(), new String[]{TheLApp.getContext().getString(R.string.uploadimage_activity_listitem_has_private),
                                                       TheLApp.getContext().getString(R.string.info_delete)}, new AdapterView.OnItemClickListener() {
                                                   @Override
                                                   public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
                                                       DialogUtil.getInstance().closeDialog();
                                                       switch (pos) {
                                                           case 0:// 取消加密
                                                               ((BaseActivity) getActivity()).showLoading();
                                                               //                                                               requestBussiness.setPrivateStatus(curMyImageBean.picId + "");
                                                               break;
                                                           case 1: // 删除
                                                               deletePhoto();
                                                               break;
                                                           default:
                                                               break;
                                                       }
                                                   }
                                               }, true, 1, null);
                                           } else {
                                               DialogUtil.getInstance().showSelectionDialog(getActivity(), new String[]{TheLApp.getContext().getString(R.string.info_delete),
                                                       TheLApp.getContext().getString(R.string.uploadimage_activity_listitem_cover)}, new AdapterView.OnItemClickListener() {
                                                   @Override
                                                   public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
                                                       DialogUtil.getInstance().closeDialog();
                                                       switch (pos) {
                                                           case 0:// 设为封面
                                                               deletePhoto();


                                                               break;
                                                           case 1: // 删除
                                                               ((BaseActivity) getActivity()).showLoading();
                                                               //                                                               requestBussiness.setCover(curMyImageBean.picId + "");
                                                               break;
                                                           default:
                                                               break;
                                                       }
                                                   }
                                               }, true, 1, null);
                                           }
                                       }
                                   }

        );

    }

    private void deletePhoto() {
        //        String tip = null;
        //        if (curMyImageBean.isPrivate == 0 && getPublicPhotoCount() == 1) {// 删除最后一张公开照片的时候要确认
        //            tip = TheLApp.getContext().getString(R.string.uploadimage_activity_delete_last_public_photo_confirm);
        //        } else {
        //            tip = TheLApp.getContext().getString(R.string.uploadimage_activity_delete_confirm);
        //        }
        String deleteMsg = TheLApp.getContext().getString(R.string.uploadimage_activity_delete_confirm);
        String type = curMyImageBean.picUrl.substring(curMyImageBean.picUrl.lastIndexOf("."));
        if (type.equals(".mp4")) {
            deleteMsg = getString(R.string.video_delete_confirm);
        }
        DialogUtil.showConfirmDialog(getActivity(), "", deleteMsg, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                ((BaseActivity) getActivity()).showLoading();
                //                requestBussiness.deleteImage(curMyImageBean.picId + "");
            }
        });
    }

    private int getPublicPhotoCount() {
        int publicPhotoCount = 0;
        for (Object photo : dataList) {
            if (((MyImageBean) photo).isPrivate == 0 || ((MyImageBean) photo).coverFlag == 1) {
                publicPhotoCount++;
            }
            if (publicPhotoCount > 1) {
                break;
            } else {
                continue;
            }
        }
        return publicPhotoCount;
    }

    @Override
    public void onDestroy() {
        if (canDelete) {
            Intent intent = new Intent();
            if (selectedPhotoIndexes != null) {
                intent.putIntegerArrayListExtra(TheLConstants.BUNDLE_KEY_INDEX, selectedPhotoIndexes);
                getActivity().setResult(TheLConstants.RESULT_CODE_WRITE_MOMENT_DELETE_PICTURE, intent);
            }
        }
        super.onDestroy();

    }

    public void refreshSelectedAmount() {
        int amount = selectedPhotoUrls.size();
        if (amount == 0) {
            done.setText(getString(R.string.info_done));
        } else {
            done.setText(getString(R.string.info_done) + " (" + amount + "/" + selectAmountLimit + ")");
        }
    }

}
