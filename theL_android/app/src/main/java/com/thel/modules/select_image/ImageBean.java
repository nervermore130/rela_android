package com.thel.modules.select_image;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by chad
 * Time 18/8/29
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */
public class ImageBean implements Parcelable {
    public String imageName = "";//图片路径
    public double width = 0;
    public double height = 0;
    public boolean select = true;
    public String videoPath = "";//视频路径
    public int videoTime = 0;

    public ImageBean() {
    }

    protected ImageBean(Parcel in) {
        imageName = in.readString();
        width = in.readDouble();
        height = in.readDouble();
        select = in.readByte() != 0;
        videoPath = in.readString();
        videoTime = in.readInt();
    }

    public static final Creator<ImageBean> CREATOR = new Creator<ImageBean>() {
        @Override
        public ImageBean createFromParcel(Parcel in) {
            return new ImageBean(in);
        }

        @Override
        public ImageBean[] newArray(int size) {
            return new ImageBean[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(imageName);
        parcel.writeDouble(width);
        parcel.writeDouble(height);
        parcel.writeByte((byte) (select ? 1 : 0));
        parcel.writeString(videoPath);
        parcel.writeInt(videoTime);
    }
}
