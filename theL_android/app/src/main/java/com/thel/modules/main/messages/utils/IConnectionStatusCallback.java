package com.thel.modules.main.messages.utils;

public interface IConnectionStatusCallback {
	void connectionStatusChanged(int connectedState, String reason);
}
