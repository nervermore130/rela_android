package com.thel.modules.main.messages.config;

import com.thel.modules.main.messages.db.MessageDataBaseAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kevin on 2017/9/18.
 */

public class MessageConstants {
    public static final String BROADCAST_UPDATE_UNREAD_MSG_COUNT = "com.thel.update.unread.msg.count";

    public static final String BROADCAST_KEY_USER_ID = "userId";
    public static final String BROADCAST_KEY_PACKET_ID = "receipt";
    /**
     * 热拉一些官方账号id
     */
    public static List<String> RELA_ACCOUNT_IDS = new ArrayList<String>() {
        {
            add("3568");
            add("101889133");
            add("102003761");
            add("103289335");
            add("101878188");
            add("102667989");
            add(MessageDataBaseAdapter.FOLLOW_TABLENAME);//关注消息
            add(MessageDataBaseAdapter.CIRCLE_REQUEST_CHAT_USER_ID);//密友伴侣
            add(MessageDataBaseAdapter.TB_NAME_SYSTEM_MSG);//系统消息表
            add(MessageDataBaseAdapter.SYSTEM_TABLENAME);//系统消息表
            add(MessageDataBaseAdapter.STRANGER_MSG_USER_ID);//系统消息表
        }
    };
}
