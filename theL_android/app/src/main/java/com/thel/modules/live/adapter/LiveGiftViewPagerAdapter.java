package com.thel.modules.live.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.List;

/**
 * Created by waiarl on 2017/10/26.
 */

public class LiveGiftViewPagerAdapter extends FragmentStatePagerAdapter {
    private final List<Fragment> list;

    public LiveGiftViewPagerAdapter(FragmentManager fm, List<Fragment> list) {
        super(fm);
        this.list=list;
    }

    @Override
    public Fragment getItem(int position) {
        return list.get(position);
    }

    @Override
    public int getCount() {
        return list.size();
    }
}
