package com.thel.modules.live.liveBigGiftAnimLayout.anim;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.Point;
import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.thel.R;
import com.thel.modules.live.liveBigGiftAnimLayout.LiveBigAnimUtils;
import com.thel.utils.Utils;

import java.util.Random;

/**
 * Created by the L on 2016/10/12.
 */

public class SnowmanAnimator {

    private final Context mContext;
    private final int mDuration;
    private final Handler mHandler;
    private final Random mRandom;
    //  private final int[] snowRes;
    private final String[] snowRes;
    private final int minSnowWidth;
    private final int differSnowCount;
    private final int differSnowWidth;
    private final int minDropDuration;
    private final int differDropDuration;
    private final int minRotateCycle;
    private final int differRotateCycle;
    private final int minSnowBallWidth;
    private final int differSnowBallWidth;
    private final int mInDuration;
    private final int mOutDuration;
    private final String foldPath;
    private final String snowmanRes;
    private final String driftRes;
    private boolean isPlaying;
    private int mWidth;
    private int mHeight;
    private int minSnowCount;
    private long rate;
    private long mRockDuration;

    public SnowmanAnimator(Context context, int duration) {
        mContext = context;
        mDuration = duration;
        mHandler = new Handler(Looper.getMainLooper());
        mRandom = new Random();
        // snowRes = new int[]{R.drawable.snowman_snow1, R.drawable.snowman_snowball, R.drawable.snowman_snow2};//雪花雪球资源
        foldPath = "anim/snowman";
        snowRes = new String[]{"snowman_snow1", "snowman_snowball", "snowman_snow2"};//雪花雪球资源
        snowmanRes = "snowman_snowman1";
        driftRes = "snowman_drift";
        minSnowCount = 3;//雪球最小数
        differSnowCount = 5;//雪球变化区间
        rate = 400;
        minSnowWidth = Utils.dip2px(mContext, 25);//最小雪花大小
        differSnowWidth = Utils.dip2px(mContext, 30);//雪球宽度变化区间
        minSnowBallWidth = Utils.dip2px(mContext, 20);//最小雪球宽度
        differSnowBallWidth = Utils.dip2px(mContext, 25);//雪球宽度变化区间
        minDropDuration = 3000;//最小掉落时间
        differDropDuration = 2000;//掉落时间变化区间
        minRotateCycle = 2000;//雪转动最小周期
        differRotateCycle = 3000;//雪转动周期变化区间
        mInDuration = 1000;//动画淡入时间
        mOutDuration = 1000;//动画淡出时间
        mRockDuration = 2000;//雪人摇晃时间
    }

    public int start(final ViewGroup parent) {
        isPlaying = true;
        mWidth = parent.getMeasuredWidth();
        mHeight = parent.getMeasuredHeight();
        if (mWidth == 0 || mHeight == 0) {
            return 0;
        }
        final RelativeLayout snowmanBackground = (RelativeLayout) RelativeLayout.inflate(mContext, R.layout.live_big_anim_snowman, null);
        snowmanBackground.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
        parent.addView(snowmanBackground);
        initBackground(snowmanBackground);
        setSnowmanBackground(snowmanBackground);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                isPlaying = false;
                snowmanBackground.clearAnimation();
                parent.removeView(snowmanBackground);
            }
        }, mDuration);
        return mDuration;
    }

    private void initBackground(RelativeLayout background) {
        final ImageView img_snowman = background.findViewById(R.id.img_snowman);
        final ImageView img_drift = background.findViewById(R.id.img_drift);
        LiveBigAnimUtils.setAssetBackground(img_snowman, foldPath, snowmanRes);
        LiveBigAnimUtils.setAssetImage(img_drift, foldPath, driftRes);
    }

    private void setSnowBackground(final RelativeLayout background) {
        background.post(new Runnable() {
            @Override
            public void run() {
                final int snowCount = mRandom.nextInt(differSnowCount) + minSnowCount;
                for (int i = 0; i < snowCount; i++) {
                    dropOneSnow(background);
                }
                if (isPlaying) {
                    background.postDelayed(this, rate);
                }
            }
        });
    }

    private void dropOneSnow(final RelativeLayout background) {
        if (!isPlaying) {
            return;
        }
        final ImageView img_snow = new ImageView(mContext);
        background.addView(img_snow);
        final int resIndex = mRandom.nextInt(snowRes.length);
        //final int res = snowRes[resIndex];
        int w = minSnowWidth + mRandom.nextInt(differSnowWidth);
        if (resIndex == 1) {//如果是雪球，换雪球随机大小
            w = minSnowBallWidth + mRandom.nextInt(differSnowBallWidth);
        }
        int h = getImageHeight(resIndex, w);
        //img_snow.setImageResource(snowRes[resIndex]);
        LiveBigAnimUtils.setAssetImage(img_snow, foldPath, snowRes[resIndex]);
        img_snow.setLayoutParams(new RelativeLayout.LayoutParams(w, h));
        Point startPoint = new Point(mRandom.nextInt(mWidth), 0 - h);
        Point endPoint = new Point(startPoint.x, mHeight);
        Path path = new Path();
        path.moveTo(startPoint.x, startPoint.y);
        path.lineTo(endPoint.x, endPoint.y);
        int cycle = 0;
        if (resIndex == 0 || resIndex == 2) {//如果是雪花，则旋转
            cycle = minRotateCycle + mRandom.nextInt(differRotateCycle);
        }
        final int dropDuration = minDropDuration + mRandom.nextInt(differDropDuration);
        FloatAnimation anim = new FloatAnimation(path, background, img_snow, dropDuration, cycle);
        anim.setDuration(dropDuration);
        anim.setInterpolator(new LinearInterpolator());
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                background.post(new Runnable() {
                    @Override
                    public void run() {
                        background.removeView(img_snow);
                    }
                });
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        img_snow.startAnimation(anim);
    }


    private void setSnowmanBackground(final ViewGroup background) {
        final ImageView img_snowman = background.findViewById(R.id.img_snowman);
        final RelativeLayout rel_snow = background.findViewById(R.id.rel_snow);
        final View rel_snowman = background.findViewById(R.id.rel_snowman);
        rel_snowman.setScaleX(0.1f);
        rel_snowman.setScaleY(0.1f);
        rel_snowman.animate().scaleXBy(1f).scaleY(1).setInterpolator(new LinearInterpolator()).setDuration(mInDuration).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (isPlaying) {
                    rockAnim(img_snowman);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        rel_snowman.postDelayed(new Runnable() {
            @Override
            public void run() {
                isPlaying = false;
                if (img_snowman.getBackground() instanceof AnimationDrawable) {
                    ((AnimationDrawable) img_snowman.getBackground()).stop();
                }
                rel_snowman.animate().alpha(0).setDuration(mOutDuration);
            }
        }, mDuration - mOutDuration);

        setSnowBackground(rel_snow);
    }

    private void rockAnim(View view) {
        final int w = view.getMeasuredWidth();
        final int h = view.getMeasuredHeight();
        ObjectAnimator oa = ObjectAnimator.ofFloat(view, "rotation", 0, -20, 0, 20, 0);
        view.setPivotX(w / 2);
        view.setPivotY(h);
        oa.setDuration(mRockDuration);
        oa.setRepeatCount(ObjectAnimator.INFINITE);
        oa.setRepeatMode(ObjectAnimator.RESTART);
        oa.setInterpolator(new LinearInterpolator());
        oa.start();
    }

    /**
     * 根据不同的资源使用不同比例获取高度
     */
    private int getImageHeight(int resId, int img_width) {
        int img_height;
        switch (resId) {
            case 0:
                img_height = img_width * 575 / 580;
                break;
            case 1:
                img_height = img_width;
                break;
            case 2:
                img_height = img_width * 340 / 296;
                break;
            default:
                img_height = img_width;
                break;
        }
        return img_height;
    }

    static class FloatAnimation extends Animation {
        private final int mCycle;
        private final int mDropDuration;
        private PathMeasure mPm;
        private View mView;
        private float mDistance;

        public FloatAnimation(Path path, View parent, View child, int dropDuration, int cycle) {
            mPm = new PathMeasure(path, false);
            mDistance = mPm.getLength();
            mView = child;
            parent.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            mCycle = cycle;
            mDropDuration = dropDuration;
        }

        @Override
        protected void applyTransformation(float factor, Transformation transformation) {
            Matrix matrix = transformation.getMatrix();
            mPm.getMatrix(mDistance * factor, matrix, PathMeasure.POSITION_MATRIX_FLAG);
            if (mCycle > 0) {
                mView.setRotation(360 * mDropDuration * factor / mCycle);
            }
            mView.setScaleX(1);
            mView.setScaleY(1);
            transformation.setAlpha(1);
        }
    }
}
