package com.thel.modules.main.nearby;

import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.view.View;
import android.widget.LinearLayout;

import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.modules.main.nearby.nearbyChild.NearbyChildFragment;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class HotStarActivity extends BaseActivity {

    public static final String TAG = "tag";
    public static final String TAG_HOT = "new";
    public static final String TAG_STAR = "hot";
    public static final String KEY_TAB = "key_tab";
    private String tag = TAG_HOT;

    private ViewPager viewpager;
    private TabLayout tablayout;
    private List<Fragment> list = new ArrayList<>();
    private List<String> titleList = new ArrayList<>();
    private Fragment hotChildFragment;
    private Fragment starChildFragment;
    private NearbyAdapter adapter;
    private LinearLayout lin_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hot_star);
        tag = getIntent().getStringExtra(KEY_TAB);
        titleList.add(getString(R.string.nearby_activity_new));
        titleList.add(getString(R.string.nearby_activity_hot));

        tablayout = findViewById(R.id.tablayout);
        viewpager = findViewById(R.id.viewpager);
        lin_back = findViewById(R.id.lin_back);
        lin_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        setTabs();
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void setTabs() {
        addFragment();
        adapter = new NearbyAdapter(getSupportFragmentManager(), list, titleList);
        viewpager.setAdapter(adapter);
        viewpager.setOffscreenPageLimit(2);
        final int size = titleList.size();
        for (int i = 0; i < size; i++) {
            final TabLayout.Tab tab = tablayout.newTab();
            setSingleLineTab(tab);
            tablayout.addTab(tab);
        }
        tablayout.setupWithViewPager(viewpager);

        tablayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                int position;
                if (tag.equals(TAG_HOT)) {
                    position = 0;
                } else {
                    position = 1;
                }
                tablayout.getTabAt(position).select();
            }
        }, 100);
    }

    private void setSingleLineTab(TabLayout.Tab tab) {
        try {
            final Field tabviewField = TabLayout.Tab.class.getDeclaredField("mView");
            tabviewField.setAccessible(true);
            final Object tabviewObj = tabviewField.get(tab);
            final Class tabviewClass = Class.forName("android.support.design.widget.TabLayout$TabView");
            final Field mDefaultMaxLines = tabviewClass.getDeclaredField("mDefaultMaxLines");
            mDefaultMaxLines.setAccessible(true);
            mDefaultMaxLines.set(tabviewObj, 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addFragment() {
        list.clear();
        hotChildFragment = initFragment(TAG_HOT);
        starChildFragment = initFragment(TAG_STAR);
        Collections.addAll(list, hotChildFragment, starChildFragment);
    }

    private Fragment initFragment(String tag) {
        final Bundle bundle = new Bundle();
        bundle.putString(TAG, tag);
        NearbyChildFragment nearbyChildFragment;
        nearbyChildFragment = NearbyChildFragment.getInstance(bundle);
        return nearbyChildFragment;
    }

    class NearbyAdapter extends FragmentStatePagerAdapter {

        private final List<Fragment> list;
        private final List<String> titliList;

        public NearbyAdapter(FragmentManager childFragmentManager, List<Fragment> list, List<String> titleList) {
            super(childFragmentManager);
            this.list = list;
            this.titliList = titleList;
        }

        @Override
        public Fragment getItem(int position) {
            return list.get(position);
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (titliList != null) {
                return titliList.get(position % titliList.size());
            }
            return "";
        }
    }
}
