package com.thel.modules.live.in;

/**
 * Created by waiarl on 2017/11/14.
 * 直播之定义view需要拥戴淡入淡出的的继承
 */

public interface LiveBaseView<T> {
    /*******************************************一下为接口继承方法******************************************************/

    T show();

    T hide();

    T destroyView();

    boolean isAnimating();

    void setAnimating(boolean isAnimating);

    void showShade(boolean show);
}
