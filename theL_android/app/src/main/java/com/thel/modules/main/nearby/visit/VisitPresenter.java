package com.thel.modules.main.nearby.visit;

import com.thel.bean.WhoSeenMeBean;
import com.thel.constants.TheLConstants;
import com.thel.modules.main.nearby.visit.VisitContract.Presenter;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.service.DefaultRequestService;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;
import com.thel.utils.UserUtils;

import org.reactivestreams.Subscription;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by waiarl on 2017/10/12.
 */

public class VisitPresenter implements Presenter {

    private final VisitContract.View view;

    public VisitPresenter(VisitContract.View view) {
        this.view = view;
        view.setPresenter(this);
    }

    @Override
    public void getWhoSeenMeData() {
        final Flowable<WhoSeenMeListNetBean> flowable = DefaultRequestService.createNearbyRequestService().getWhoSeenMeList(UserUtils.getUserVipLevel() > 0 ? "50" : "10", "1");
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<WhoSeenMeListNetBean>() {

            @Override
            public void onNext(WhoSeenMeListNetBean whoSeenMeListBean) {
                super.onNext(whoSeenMeListBean);

                if (whoSeenMeListBean != null && whoSeenMeListBean.data != null) {
                    final List<WhoSeenMeBean> list = whoSeenMeListBean.data.map_list;
                    if (list == null || list.size() == 0) {
                        view.EmptyWhoSeenMeData();
                    } else {
                        view.showWhoSeenMeData(whoSeenMeListBean.data);
                    }
                }
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
            }

            @Override
            public void onComplete() {
                view.requestFinish();
            }
        });
    }

    @Override
    public void getVisitData() {
        final Flowable<WhoSeenMeListNetBean> flowable = DefaultRequestService.createNearbyRequestService().getVisitList(UserUtils.getUserVipLevel() > 0 ? "50" : "10", "1");
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<WhoSeenMeListNetBean>() {

            @Override
            public void onNext(WhoSeenMeListNetBean whoSeenMeListBean) {
                super.onNext(whoSeenMeListBean);
                if (whoSeenMeListBean != null && whoSeenMeListBean.data != null) {
                    final List<WhoSeenMeBean> list = whoSeenMeListBean.data.map_list;
                    view.showVisitData(list);
                    if (list.size() == 0) {
                        view.EmptyVisitData();
                    }
                }
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
            }

            @Override
            public void onComplete() {
                view.requestFinish();
            }
        });
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {

    }
}
