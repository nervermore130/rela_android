package com.thel.modules.main.home.tag;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseAdapter;
import com.thel.base.BaseImageViewerActivity;
import com.thel.bean.gift.WinkCommentBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.topic.TopicMainPageBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.growingio.GrowingIoConstant;
import com.thel.imp.black.BlackUsersChangedListener;
import com.thel.imp.black.BlackUtils;
import com.thel.imp.like.MomentLikeStatusChangedListener;
import com.thel.imp.like.MomentLikeUtils;
import com.thel.imp.momentblack.MomentBlackListener;
import com.thel.imp.momentblack.MomentBlackUtils;
import com.thel.imp.momentdelete.MomentDeleteListener;
import com.thel.imp.momentdelete.MomentDeleteUtils;
import com.thel.imp.momentreport.MomentReportListener;
import com.thel.imp.momentreport.MomentReportUtils;
import com.thel.imp.wink.WinkStatusChangedListener;
import com.thel.imp.wink.WinkUtils;
import com.thel.manager.ListVideoVisibilityManager;
import com.thel.modules.main.home.moments.comment.MomentCommentActivity;
import com.thel.modules.main.home.moments.theme.ThemeDetailActivity;
import com.thel.modules.main.messages.utils.PushUtils;
import com.thel.modules.welcome.WelcomeActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.service.DefaultRequestService;
import com.thel.ui.adapter.MomentsAdapter;
import com.thel.ui.dialog.ReleaseMomentDialog;
import com.thel.ui.widget.MySwipeRefreshLayout;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.decoration.DefaultItemDivider;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.PhoneUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


/**
 * 标签详情页面
 *
 * @author Setsail
 */
public class TagDetailActivity extends BaseImageViewerActivity implements BaseRecyclerViewAdapter.RequestLoadMoreListener, BaseRecyclerViewAdapter.ReloadMoreListener, MomentBlackListener, MomentDeleteListener,
        WinkStatusChangedListener,//挤眼
        MomentLikeStatusChangedListener,//点赞
        BlackUsersChangedListener,//黑名单
        MomentReportListener {

    private LinearLayout lin_back;
    private LinearLayout lin_go_to_top;
    private TextView go_to_top_txt;
    private LinearLayout lin_do_more;

    private long lastClickTime = 0;

    public String topicId;
    public String topicName;

    private MySwipeRefreshLayout swipe_container;
    private RecyclerView listView;
    private TextView mDefault;
    public ArrayList<MomentsBean> momentsList = new ArrayList<>();

    // 刷新类型，全部刷新（即下拉刷新）为1，还是加载下一页为2
    public int refreshType = 0;
    public final int REFRESH_TYPE_ALL = 1;
    public final int REFRESH_TYPE_NEXT_PAGE = 2;
    public static boolean canLoadMore = false;
    public int currentCountForOnce = 0; // 每次请求服务器，返回的数据条数
    private int pageSize = 20;
    private int curPage = 1; // 将要请求的页号

    private LinearLayoutManager mLinearLayoutManager;
    private MomentsAdapter adapter;

    // 日志关注页签点击我关注的话题进来后，退出时要通知日志关注页签刷新数量
    private boolean needRefresh = false;

    private long startTime = 0;

    private MomentBlackUtils momentBlackUtils;
    private WinkUtils winkUtils;
    private BlackUtils blackUtils;
    private MomentDeleteUtils momentDeleteUtils;
    private MomentReportUtils momentReportUtils;
    private MomentLikeUtils momentLikeUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();

        // 如果是从外部进入
        String action = intent.getAction();
        if (Intent.ACTION_VIEW.equals(action)) {
            if (!ShareFileUtils.getBoolean(ShareFileUtils.HAS_LOGGED, false)) {//如果没登陆
                startActivity(new Intent(this, WelcomeActivity.class));
                finish();
                return;
            }
            Uri uri = intent.getData();
            if (uri != null) {
                topicName = uri.getQueryParameter("tagName");
            }
            if (TextUtils.isEmpty(topicName)) {
                finish();
                return;
            }
        } else {
            if (TextUtils.isEmpty(intent.getStringExtra(TheLConstants.BUNDLE_KEY_TOPIC_NAME))) {
                finish();
                return;
            } else {
                topicName = intent.getStringExtra(TheLConstants.BUNDLE_KEY_TOPIC_NAME);
                needRefresh = intent.getBooleanExtra(TheLConstants.BUNDLE_KEY_NEED_REFRESH, false);
            }
        }

        setLayout();

        findViewById();

        setListener();

        if (!topicName.startsWith("#")) topicName = "#" + topicName + "#";
        go_to_top_txt.setText(topicName);
        setListener();
        //
        processBusiness();
        registerReceiver();
    }

    protected void findViewById() {
        lin_back = findView(R.id.lin_back);
        lin_go_to_top = findView(R.id.go_to_top);
        go_to_top_txt = findView(R.id.go_to_top_txt);
        lin_do_more = findView(R.id.lin_do_more);
        swipe_container = findView(R.id.swipe_container);
        ViewUtils.initSwipeRefreshLayout(swipe_container);
        listView = findView(R.id.list_view);
        mLinearLayoutManager = new LinearLayoutManager(this);
        //如果可以确定每个item的高度是固定的，设置这个选项可以提高性能
        listView.setHasFixedSize(true);
        listView.setLayoutManager(mLinearLayoutManager);
        listView.addItemDecoration(new DefaultItemDivider(this, LinearLayoutManager.VERTICAL, R.color.bg_color, (int) TheLApp.getContext().getResources().getDimension(R.dimen.wide_divider_height), true, false));
        adapter = new MomentsAdapter(R.layout.item_moments, momentsList, GrowingIoConstant.ENTRY_TAG_PAGE);
        adapter.setOnLoadMoreListener(this);
        adapter.setReloadMoreListener(this);
        listView.setAdapter(adapter);
        mDefault = findView(R.id.txt_default);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(this.getClass().getName());
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(this.getClass().getName());
        MobclickAgent.onPause(this);
    }

    protected void processBusiness() {
        if (swipe_container != null)
            swipe_container.post(new Runnable() {
                @Override
                public void run() {
                    swipe_container.setRefreshing(true);
                }
            });
        loadMomentsData(curPage);
    }

    protected void setListener() {

        lin_back.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                ViewUtils.preventViewMultipleClick(arg0, 2000);
                PushUtils.finish(TagDetailActivity.this);
            }
        });

        lin_do_more.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ReleaseMomentDialog releaseMomentDialog = new ReleaseMomentDialog(TagDetailActivity.this);
                releaseMomentDialog.show();
            }
        });

        lin_go_to_top.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (MotionEvent.ACTION_DOWN == event.getAction()) {
                    if (System.currentTimeMillis() - lastClickTime < 300) {
                        ViewUtils.preventViewMultipleTouch(v, 2000);
                        scrollToTop();
                    }
                    lastClickTime = System.currentTimeMillis();
                }
                return true;
            }
        });

        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                refreshAll();
            }
        });

        adapter.setOnItemClickListener(new BaseAdapter.OnItemClickListener<MomentsBean>() {
            @Override
            public void onItemClick(View view, MomentsBean item, int position) {

                jumpToCommentPage(item, false);
            }
        });

        listView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {


                if (newState == RecyclerView.SCROLL_STATE_IDLE) {

                    if (adapter != null && adapter.getData() != null) {

                        ListVideoVisibilityManager.getInstance().calculatorItemPercent(mLinearLayoutManager, newState, adapter, TheLConstants.EntryConstants.ENTRY_HASHTAG);

                    }

                    try {
                        Glide.with(TagDetailActivity.this).resumeRequests();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {

                    try {
                        Glide.with(TagDetailActivity.this).pauseRequests();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

            }
        });
    }

    @Override
    public void onLoadMoreRequested() {
        listView.post(new Runnable() {
            @Override
            public void run() {
                if (currentCountForOnce > 0) {
                    refreshType = REFRESH_TYPE_NEXT_PAGE;
                    loadMomentsData(curPage);
                } else {
                    adapter.openLoadMore(0, false);
                    if (momentsList.size() > 0) {
                        View view = getLayoutInflater().inflate(R.layout.load_more_footer_layout, (ViewGroup) listView.getParent(), false);
                        ((TextView) view.findViewById(R.id.text)).setText(getString(R.string.info_no_more));
                        adapter.addFooterView(view);
                    }
                }
            }
        });
    }

    @Override
    public void reloadMore() {
        listView.post(new Runnable() {
            @Override
            public void run() {
                refreshType = REFRESH_TYPE_NEXT_PAGE;
                adapter.removeAllFooterView();
                adapter.openLoadMore(true);
                loadMomentsData(curPage);
            }
        });
    }

    protected void setLayout() {
        this.setContentView(R.layout.tag_detail_activity);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (!(PhoneUtils.getNetWorkType() == PhoneUtils.TYPE_NO)) {
                showLoading();
                refreshAll();
            } else {
                Toast.makeText(TheLApp.getContext(), TheLApp.getContext().getString(R.string.info_no_network), Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void refreshAll() {
        refreshType = REFRESH_TYPE_ALL;
        curPage = 1;
        loadMomentsData(curPage);
    }

    public void refreshMoments(List<MomentsBean> listBean) {
        if (refreshType == REFRESH_TYPE_ALL) {
            momentsList.clear();
        }
        momentsList.addAll(listBean);
        if (refreshType == REFRESH_TYPE_ALL) {
            adapter.removeAllFooterView();
            adapter.setNewData(momentsList);
            curPage = 2;
            if (currentCountForOnce > 0) {
                adapter.openLoadMore(momentsList.size(), true);
            } else {
                adapter.openLoadMore(momentsList.size(), false);
            }
            if (listView.getChildCount() > 0) {
                listView.scrollToPosition(0);
            }
        } else {
            curPage++;
            adapter.notifyDataChangedAfterLoadMore(true, momentsList.size());
        }
        setDefaultVisible();
    }

    /**
     * 设置是否默认显示页
     */
    private void setDefaultVisible() {
        if (currentCountForOnce == 0 && momentsList.size() == 0) {//当前momentsList中可能全部是被屏蔽的
            mDefault.setVisibility(View.VISIBLE);
            adapter.openLoadMore(0, false);
            listView.post(new Runnable() {
                @Override
                public void run() {
                    adapter.removeAllFooterView();
                }
            });
        } else {
            mDefault.setVisibility(View.GONE);
        }
    }

    public void scrollToTop() {
        if (listView != null && adapter != null) {
            listView.scrollToPosition(0);
        }
    }

    public void refresh() {
        // 刷新评论列表
        adapter.notifyDataSetChanged();
        adapter.setPageSize(momentsList.size());
        setDefaultVisible();
    }

    private void loadMomentsData(int curPage) {
        canLoadMore = false;

        Flowable<TopicMainPageBean> flowable = DefaultRequestService.createAllRequestService().getTopicMainPageHot(topicName, String.valueOf(curPage), String.valueOf(pageSize));
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<TopicMainPageBean>() {
            @Override
            public void onNext(TopicMainPageBean topicMainPageBean) {
                super.onNext(topicMainPageBean);
                if (topicMainPageBean != null && topicMainPageBean.momentsList != null) {
                    currentCountForOnce = topicMainPageBean.momentsList.size();
                    topicMainPageBean.filterBlockMoments();
                    refreshMoments(topicMainPageBean.momentsList);
                }
                requestFinished();
            }
        });

    }

    private void jumpToCommentPage(MomentsBean momentBean, boolean showKeyboard) {
//        Intent intent = new Intent();
        if (MomentsBean.MOMENT_TYPE_THEME.equals(momentBean.momentsType)) {
//            intent.setClass(TheLApp.getContext(), ThemeDetailActivity.class);
//
//            if (showKeyboard)
//                intent.putExtra("showKeyboard", true);
//            Bundle bundle = new Bundle();
//            bundle.putSerializable(TheLConstants.BUNDLE_KEY_MOMENT_BEAN, momentBean);
//            intent.putExtras(bundle);
//            startActivityForResult(intent, TheLConstants.BUNDLE_CODE_MOMENTS_ACTIVITY);
            FlutterRouterConfig.Companion.gotoThemeDetails(momentBean.momentsId);
        } else {
//            intent.setClass(TheLApp.getContext(), MomentCommentActivity.class);
            FlutterRouterConfig.Companion.gotoMomentDetails(momentBean.momentsId);
        }
    }

    private void requestFinished() {
        if (swipe_container != null)
            swipe_container.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (swipe_container != null && swipe_container.isRefreshing())
                        swipe_container.setRefreshing(false);
                }
            }, 1000);
        closeLoading();
    }

    @Override
    public void finish() {
        if (needRefresh) {
            setResult(TheLConstants.BUNDLE_CODE_TOPIC_MAIN_ACTIVITY);
        }
        super.finish();
    }

    //注册广播
    private void registerReceiver() {

        adapter.registerNewCommentNotify(this);

        momentBlackUtils = new MomentBlackUtils();
        momentBlackUtils.registerReceiver(this);

        winkUtils = new WinkUtils();
        winkUtils.registerReceiver(this);

        momentLikeUtils = new MomentLikeUtils();
        momentLikeUtils.registerReceiver(this);

        blackUtils = new BlackUtils();
        blackUtils.registerReceiver(this);

        momentDeleteUtils = new MomentDeleteUtils();
        momentDeleteUtils.registerReceiver(this);

        momentReportUtils = new MomentReportUtils();
        momentReportUtils.registerReceiver(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            adapter.unregisterNewCommentNotify(this);
            winkUtils.unRegisterReceiver(this);
            momentBlackUtils.unRegisterReceiver(this);
            momentLikeUtils.unRegisterReceiver(this);
            blackUtils.unRegisterReceiver(this);
            momentDeleteUtils.unRegisterReceiver(this);
            momentReportUtils.unRegisterReceiver(this);

            long endTime = System.currentTimeMillis();

            long watchTime = endTime - startTime;

            if (watchTime > 0) {
                watchTime = watchTime / 1000;
            } else {
                watchTime = 1;
            }

            GrowingIOUtil.uploadCommonVideoDuration(watchTime);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void filterListByMomentId(String momentId) {
        if (adapter != null) {
            List<MomentsBean> list = adapter.getData();
            if (list != null) {
                ListIterator<MomentsBean> iterable = list.listIterator();
                while (iterable.hasNext()) {
                    MomentsBean momentsBean = iterable.next();
                    if (String.valueOf(momentsBean.momentsId).equals(momentId)) {
                        iterable.remove();
                    }
                }
                adapter.notifyDataSetChanged();
                closeLoading();
            }

        }
    }

    private void filterListByUserId(String userId) {
        if (adapter != null) {
            List<MomentsBean> list = adapter.getData();
            if (list != null) {
                ListIterator<MomentsBean> iterable = list.listIterator();
                while (iterable.hasNext()) {
                    MomentsBean momentsBean = iterable.next();
                    if (String.valueOf(momentsBean.userId).equals(userId)) {
                        iterable.remove();
                    }
                }
                adapter.notifyDataSetChanged();
                closeLoading();
            }

        }
    }

    private void removeWinkComment(String myUserId, MomentsBean momentsBean) {
        List<WinkCommentBean> list = momentsBean.winkUserList;
        if (list != null) {
            ListIterator<WinkCommentBean> listIterator = list.listIterator();

            while (listIterator.hasNext()) {
                WinkCommentBean winkCommentBean = listIterator.next();
                if (myUserId.equals(String.valueOf(winkCommentBean.userId))) {
                    listIterator.remove();
                }
            }
        }
    }

    @Override
    public void onBlackOneMoment(String momentId) {
        filterListByMomentId(momentId);
    }

    @Override
    public void onBlackherMoment(String userId) {
        filterListByUserId(userId);
    }


    @Override
    public void onMomentDelete(String momentId) {
        filterListByMomentId(momentId);
    }

    @Override
    public void onMomentReport(String momentId) {
        filterListByMomentId(momentId);
    }

    @Override
    public void onWinkSucceess(String userId, String type) {

    }

    @Override
    public void onBlackUsersChanged(String userId, boolean isAdd) {
        filterListByUserId(userId);
    }

    @Override
    public void onLikeStatusChanged(String momentId, boolean like, String myUserId, WinkCommentBean myWinkUserBean) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        PushUtils.finish(TagDetailActivity.this);
    }
}
