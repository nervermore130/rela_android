package com.thel.modules.live.surface.watch

import android.os.Bundle
import android.os.Message
import android.text.TextUtils
import com.thel.R
import com.thel.app.TheLApp
import com.thel.bean.live.LinkMicResponseBean
import com.thel.bean.live.LiveMultiOnCreateBean
import com.thel.bean.live.LiveMultiSeatBean
import com.thel.bean.live.MultiSpeakersBean
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_ALL_UPGRADE_EFFECTS
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_AUDIENCE_LINK_MIC_RESPONSE
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_AUDIENCE_LINK_MIC_TIME_OUT
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_DAILY_LINK_MIC
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_DANMU_RECEIVE_MSG
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_ENCOUNTER_SB
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_ENCOUNTER_SPEAKERS
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_END_ENCOUNTER
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_FREE_BARRAGE
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_FREE_GOLD
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_GIFT_RECEIVE_MSG
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_GUEST_GIFT
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_INIT_MULTI
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_JOIN_USER
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_JOIN_VIP_USER
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_LINK_MIC_BUSY
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_LINK_MIC_CANCEL
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_LINK_MIC_HANGUP
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_LINK_MIC_STOP
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_LIVE_CLOSED
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_MIC_ADD
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_MIC_AGREE
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_MIC_CANCEL
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_MIC_DEL
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_MIC_OFF
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_MIC_ON
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_MIC_REQ
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_PK_GEM_NOTICE
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_PK_HANGUP_NOTICE
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_PK_START_NOTICE
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_PK_STOP_NOTICE
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_PK_SUMMARY_NOTICE
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_REFRESH_MSGS
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_REFRESH_SEAT
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_START_ENCOUNTER
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_STOP_SHOW
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_TOP_GIFT
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_TOP_TODAY
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_UPDATE_AUDIENCE_COUNT
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_UPDATE_BROADCASTER_NET_STATUS
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_UPGRADE_EFFECTS
import com.thel.modules.live.bean.*
import com.thel.modules.live.interfaces.IChatMessageDelegate
import com.thel.modules.live.utils.LiveGIOPush
import com.thel.modules.live.utils.LiveUtils
import com.thel.modules.live.view.expensive.TopGiftBean
import com.thel.utils.GsonUtils
import com.thel.utils.L
import com.thel.utils.UserUtils
import org.json.JSONException
import org.json.JSONObject

class AudienceMessageDelegateImpl : IChatMessageDelegate {

    private val tagName: String = "AudienceMessageDelegateImpl"

    private var broadcasterStatus = 0

    private var observer: LiveWatchObserver? = null

    private var liveRoomBean: LiveRoomBean? = null

    override fun init(observer: LiveWatchObserver, liveRoomBean: LiveRoomBean) {
        this.observer = observer
        this.liveRoomBean = liveRoomBean
    }

    override fun messageDelegate(code: String, payload: String) {
        if (!TextUtils.isEmpty(code)) {
            try {

                val liveRoomMsgBean: LiveRoomMsgBean?
                when (code) {
                    LiveRoomMsgBean.TYPE_MSG -> {
                        liveRoomMsgBean = GsonUtils.getObject(payload, LiveRoomMsgBean::class.java)

                        // 主播网络状态变化消息
                        if (LiveRoomMsgBean.TYPE_UPLOADER_STATUS == liveRoomMsgBean.type) {
                            val status = Integer.parseInt(liveRoomMsgBean.content)
                            if (status != broadcasterStatus) {
                                broadcasterStatus = status
                                observer?.sendEmptyMessage(UI_EVENT_UPDATE_BROADCASTER_NET_STATUS)
                            }
                        } else if (LiveRoomMsgBean.TYPE_MSG == liveRoomMsgBean.type || LiveRoomMsgBean.TYPE_NOTICE == liveRoomMsgBean.type) {

                            observer?.addMsg(liveRoomMsgBean)
                            observer?.sendEmptyMessage(UI_EVENT_REFRESH_MSGS)
                        }
                    }
                    LiveRoomMsgBean.TYPE_SYS_MSG -> {
                        liveRoomMsgBean = GsonUtils.getObject(payload, LiveRoomMsgBean::class.java)
                        if (LiveRoomMsgBean.TYPE_JOIN == liveRoomMsgBean.type || LiveRoomMsgBean.TYPE_BANED == liveRoomMsgBean.type || LiveRoomMsgBean.TYPE_LEAVE == liveRoomMsgBean.type) {
                            observer?.addMsg(liveRoomMsgBean)
                            observer?.sendEmptyMessage(UI_EVENT_REFRESH_MSGS)
                        }
                    }
                    LiveRoomMsgBean.TYPE_UPDATE -> {
                        observer?.sendMessage(getHandlerMessage(UI_EVENT_UPDATE_AUDIENCE_COUNT, GsonUtils.getObject(payload, ResponseGemBean::class.java)))

                    }
                    LiveRoomMsgBean.TYPE_CLOSE -> if (liveRoomBean?.user?.id.toString() == UserUtils.getMyUserId()) {
                        observer?.sendEmptyMessage(UI_EVENT_STOP_SHOW)
                    } else {
                        observer?.sendEmptyMessage(UI_EVENT_LIVE_CLOSED)
                    }
                    LiveRoomMsgBean.TYPE_GIFT_MSG, LiveRoomMsgBean.CODE_SPECIAL_GIFT -> {
                        val msg = Message.obtain()
                        msg.what = UI_EVENT_GIFT_RECEIVE_MSG
                        val bundle = Bundle()
                        bundle.putString("payload", payload)
                        bundle.putString("code", code)
                        msg.data = bundle
                        observer?.sendMessage(msg)
                    }
                    LiveRoomMsgBean.TYPE_DANMU_MSG -> {
                        observer?.sendMessage(getHandlerMessage(UI_EVENT_DANMU_RECEIVE_MSG, payload))

                        observer?.addMsg(GsonUtils.getObject(payload, LiveRoomMsgBean::class.java))
                        observer?.sendEmptyMessage(UI_EVENT_REFRESH_MSGS)
                    }
                    LiveRoomMsgBean.TYPE_FREE_BARRAGE -> {
                        observer?.sendEmptyMessage(UI_EVENT_FREE_BARRAGE)
                    }
                    LiveRoomMsgBean.TYPE_VISIT -> {
                        liveRoomMsgBean = GsonUtils.getObject(payload, LiveRoomMsgBean::class.java)
                        if (LiveRoomMsgBean.TYPE_JOIN == liveRoomMsgBean!!.type) {
                            observer?.sendMessage(getHandlerMessage(UI_EVENT_JOIN_USER, liveRoomMsgBean))
                        }
                    }
                    LiveRoomMsgBean.TYPE_VIP_VISIT -> {
                        liveRoomMsgBean = GsonUtils.getObject(payload, LiveRoomMsgBean::class.java)
                        if (LiveRoomMsgBean.TYPE_JOIN == liveRoomMsgBean!!.type) {
                            observer?.sendMessage(getHandlerMessage(UI_EVENT_JOIN_VIP_USER, liveRoomMsgBean))
                        }
                    }
                    LiveRoomMsgBean.TYPE_LIVE_PK -> receiverPkNoticeMsg(payload)
                    LiveRoomMsgBean.TYPE_CONNECT_MIC -> receiverLinkMicNoticeMsg(payload)
                    LiveRoomMsgBean.TYPE_FOLLOW -> {
                        liveRoomMsgBean = LiveUtils.getFollowMsgBean(payload)
                        if (liveRoomMsgBean != null && !TextUtils.isEmpty(liveRoomMsgBean.content)) {
                            observer?.addMsg(liveRoomMsgBean)
                            observer?.sendEmptyMessage(UI_EVENT_REFRESH_MSGS)
                        }
                    }
                    LiveRoomMsgBean.TYPE_RECOMM -> {
                        liveRoomMsgBean = LiveUtils.getRecommendMsgBean(payload)
                        if (liveRoomMsgBean != null && !TextUtils.isEmpty(liveRoomMsgBean.content)) {
                            observer?.addMsg(liveRoomMsgBean)
                            observer?.sendEmptyMessage(UI_EVENT_REFRESH_MSGS)
                        }
                    }
                    LiveRoomMsgBean.TYPE_SHARETO -> {
                        liveRoomMsgBean = LiveUtils.getSharetToMsgBean(payload)
                        if (liveRoomMsgBean != null && !TextUtils.isEmpty(liveRoomMsgBean.content)) {
                            observer?.addMsg(liveRoomMsgBean)
                            observer?.sendEmptyMessage(UI_EVENT_REFRESH_MSGS)
                        }
                    }
                    LiveRoomMsgBean.TYPE_GIFTCOMBO -> if (liveRoomBean != null && liveRoomBean?.softEnjoyBean != null) {

                        L.d(tagName, " liveRoomBean?.softEnjoyBean : " + liveRoomBean?.softEnjoyBean)

                        liveRoomMsgBean = LiveUtils.getSendGiftMsgBean(payload, liveRoomBean?.softEnjoyBean)

                        L.d(tagName, " liveRoomMsgBean : " + GsonUtils.createJsonString(liveRoomMsgBean))

                        if (liveRoomMsgBean != null && !TextUtils.isEmpty(liveRoomMsgBean.content)) {
                            observer?.addMsg(liveRoomMsgBean)
                            observer?.sendEmptyMessage(UI_EVENT_REFRESH_MSGS)
                        }
                    }
                    LiveRoomMsgBean.TYPE_TOPGIFT -> {
                        observer?.sendMessage(getHandlerMessage(UI_EVENT_TOP_GIFT, GsonUtils.getObject(payload, TopGiftBean::class.java)))
                    }
                    LiveRoomMsgBean.TYPE_AUDIO_BROADCAST_CODE -> {
                        val seat = GsonUtils.getObject(payload, LiveMultiSeatBean::class.java)
                        when (seat!!.method) {
                            LiveRoomMsgBean.TYPE_METHOD_ONSEAT -> {
                                observer?.sendMessage(getHandlerMessage(UI_EVENT_REFRESH_SEAT, seat))
                                LiveGIOPush.getInstance().setAudienceOnSeatCount()
                            }
                            LiveRoomMsgBean.TYPE_METHOD_OFFSEAT -> {
                                observer?.sendMessage(getHandlerMessage(UI_EVENT_REFRESH_SEAT, seat))
                            }
                            LiveRoomMsgBean.TYPE_METHOD_MUTE -> {
                                observer?.sendMessage(getHandlerMessage(UI_EVENT_REFRESH_SEAT, seat))
                            }
                            LiveRoomMsgBean.TYPE_METHOD_GUEST_GIFT -> {
                                observer?.sendMessage(getHandlerMessage(UI_EVENT_GUEST_GIFT, seat))
                            }
                            LiveRoomMsgBean.TYPE_METHOD_START_ENCOUNTER -> {
                                observer?.sendMessage(getHandlerMessage(UI_EVENT_START_ENCOUNTER, seat))
                            }
                            LiveRoomMsgBean.TYPE_METHOD_END_ENCOUNTER -> {
                                observer?.sendMessage(getHandlerMessage(UI_EVENT_END_ENCOUNTER, seat))
                            }
                            LiveRoomMsgBean.TYPE_METHOD_ENCOUNTERSB -> {
                                observer?.sendMessage(getHandlerMessage(UI_EVENT_ENCOUNTER_SB, seat))
                            }
                            LiveRoomMsgBean.TYPE_METHOD_UPLOAD_VOLUMN -> {
                                observer?.sendMessage(getHandlerMessage(UI_EVENT_ENCOUNTER_SPEAKERS, GsonUtils.getObject(payload, MultiSpeakersBean::class.java)))
                            }
                            LiveRoomMsgBean.TYPE_METHOD_MIC_ON -> {
                                observer?.sendEmptyMessage(UI_EVENT_MIC_ON)
                                isWaitMic(true)
                            }
                            LiveRoomMsgBean.TYPE_METHOD_MIC_OFF -> {
                                observer?.sendEmptyMessage(UI_EVENT_MIC_OFF)
                                isWaitMic(false)
                            }
                            LiveRoomMsgBean.TYPE_METHOD_MIC_ADD -> {
                                observer?.sendMessage(getHandlerMessage(UI_EVENT_MIC_ADD, GsonUtils.getObject(payload, LiveMultiSeatBean.CoupleDetail::class.java)))
                            }
                            LiveRoomMsgBean.TYPE_METHOD_MIC_DEL -> {
                                observer?.sendMessage(getHandlerMessage(UI_EVENT_MIC_DEL, seat))
                            }
                            LiveRoomMsgBean.TYPE_METHOD_MIC_REQ -> {
                                observer?.sendEmptyMessage(UI_EVENT_MIC_REQ)
                            }
                            LiveRoomMsgBean.TYPE_METHOD_MIC_CANCEL -> {
                                observer?.sendEmptyMessage(UI_EVENT_MIC_CANCEL)
                            }
                            LiveRoomMsgBean.TYPE_METHOD_MIC_AGREE -> {
                                observer?.sendMessage(getHandlerMessage(UI_EVENT_MIC_AGREE, seat))
                            }
                        }
                    }
                    LiveRoomMsgBean.FREE_GOLD//主播没有
                    -> {
                        val liveFreeGoldReeponceBean = GsonUtils.getObject(payload, LiveFreeGoldReeponceBean::class.java)
                        observer?.sendMessage(getHandlerMessage(UI_EVENT_FREE_GOLD, liveFreeGoldReeponceBean))
                    }
                    LiveRoomMsgBean.TYPE_TOP_TODAY -> {
                        observer?.sendMessage(getHandlerMessage(UI_EVENT_TOP_TODAY, GsonUtils.getObject(payload, TopTodayBean::class.java)))
                    }
                    LiveRoomMsgBean.TYPE_UPGRADE_EFFECTS -> {
                        observer?.sendMessage(getHandlerMessage(UI_EVENT_UPGRADE_EFFECTS, GsonUtils.getObject(payload, LiveRoomMsgBean::class.java)))
                    }
                    LiveRoomMsgBean.TYPE_ALL_UPGRADE_EFFECTS -> {
                        observer?.sendMessage(getHandlerMessage(UI_EVENT_ALL_UPGRADE_EFFECTS, GsonUtils.getObject(payload, LiveRoomMsgBean::class.java)))
                    }
                    else -> {
                    }
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

    private fun getHandlerMessage(what: Int, obj: Any): Message {
        val msg = Message.obtain()
        msg.what = what
        msg.obj = obj
        return msg
    }

    /**
     * 收到有关Pk的消息
     * 属于广播通知收到的消息
     *
     * @param payload
     */
    private fun receiverPkNoticeMsg(payload: String) {
        if (TextUtils.isEmpty(payload)) {
            return
        }
        try {
            val obj = JSONObject(payload)
            val method = obj.optString("method")
            when (method) {
                LiveRoomMsgBean.PK_NOTICE_METHOD_START -> receiverPkStartMsg(payload)
                LiveRoomMsgBean.PK_NOTICE_METHOD_SUMMARY -> receivePkSummaryMsg(payload)
                LiveRoomMsgBean.PK_NOTICE_METHOD_STOP -> receivePkStopMsg(payload)
                LiveRoomMsgBean.PK_NOTICE_METHOD_PKGEM -> receiverPkGiftMsg(payload)
                LiveRoomMsgBean.PK_NOTICE_METHOD_HANGUP -> receiveHangupMsg(payload)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }

    }

    /**
     * PK开始时 服务器发送PK开始通知给双方直播室内的所有人
     *
     * @param payload
     */
    private fun receiverPkStartMsg(payload: String) {
        if (TextUtils.isEmpty(payload)) {
            return
        }
        val bean = GsonUtils.getObject(payload, LivePkStartNoticeBean::class.java)
        val msg = Message.obtain()
        msg.what = UI_EVENT_PK_START_NOTICE
        msg.obj = bean
        observer?.sendMessage(msg)
    }

    /**
     * PK结束时 服务器发送 PK结束 和 双方助攻前三名 通知给双方直播室内的所有人
     *
     * @param payload
     */
    private fun receivePkStopMsg(payload: String) {
        if (TextUtils.isEmpty(payload)) {
            return
        }
        val msg = Message.obtain()
        msg.what = UI_EVENT_PK_STOP_NOTICE
        msg.obj = payload
        observer?.sendMessage(msg)
    }

    /**
     * 主播收到礼物时 服务器发送主播软妹币增量通知给双方直播室内的所有人:
     *
     * @param payload
     */
    private fun receiverPkGiftMsg(payload: String) {
        if (TextUtils.isEmpty(payload)) {
            return
        }
        val bean = GsonUtils.getObject(payload, LivePkGemNoticeBean::class.java)
        val msg = Message.obtain()
        msg.what = UI_EVENT_PK_GEM_NOTICE
        msg.obj = bean
        observer?.sendMessage(msg)
    }

    /**
     * 进入总结阶段 服务器发送 总结剩余时间 和 双方助攻前三名 给双方直播室内的所有人:
     *
     * @param payload
     */
    private fun receivePkSummaryMsg(payload: String) {
        if (TextUtils.isEmpty(payload)) {
            return
        }
        val bean = GsonUtils.getObject(payload, LivePkSummaryNoticeBean::class.java)
        val msg = Message.obtain()
        msg.what = UI_EVENT_PK_SUMMARY_NOTICE
        msg.obj = bean
        observer?.sendMessage(msg)
    }

    private fun receiveHangupMsg(payload: String) {
        if (TextUtils.isEmpty(payload)) {
            return
        }
        val bean = GsonUtils.getObject(payload, LivePkHangupBean::class.java)
        val msg = Message.obtain()
        msg.what = UI_EVENT_PK_HANGUP_NOTICE
        msg.obj = bean
        observer?.sendMessage(msg)
    }

    private fun receiverLinkMicNoticeMsg(payload: String) {

        L.d(tagName, " receiverLinkMicNoticeMsg payload : $payload")

        if (TextUtils.isEmpty(payload)) {
            return
        }

        val obj: JSONObject
        try {
            obj = JSONObject(payload)

            val method = obj.optString("method")

            if (method == "stop") {
                sendLinkMicStopMsg()
            } else if (method == "start") {
                val linkMicResponseBean = GsonUtils.getObject(payload, LinkMicResponseBean::class.java)
                linkMicResponseBean!!.isAudienceLinkMic = linkMicResponseBean.userId == UserUtils.getMyUserId()
                sendLinkMicBusyMsg(linkMicResponseBean)
            } else if (method == "guestResponse") {
                val audienceLinkMicResponseBean = GsonUtils.getObject(payload, AudienceLinkMicResponseBean::class.java)
                sendAudienceLinkMicResponse(audienceLinkMicResponseBean)
            } else if (method == "request") {//主播请求连麦
                L.d(tagName, " 主播请求连麦 ")
                val liveRoomMsgBean = GsonUtils.getObject(payload, LiveRoomMsgConnectMicBean::class.java)
                val msg = Message.obtain()
                msg.what = UI_EVENT_DAILY_LINK_MIC
                msg.obj = liveRoomMsgBean
                observer?.sendMessage(msg)
            } else if (method == "hangup") {
                val liveRoomMsgConnectMicBean = GsonUtils.getObject(payload, LiveRoomMsgConnectMicBean::class.java)
                val message = Message.obtain()
                message.obj = liveRoomMsgConnectMicBean
                message.what = UI_EVENT_LINK_MIC_HANGUP
                observer?.sendMessage(message)
            } else if (method == "cancel") {
                observer?.sendEmptyMessage(UI_EVENT_LINK_MIC_CANCEL)
            } else if (method == "timeout") {
                observer?.sendEmptyMessage(UI_EVENT_AUDIENCE_LINK_MIC_TIME_OUT)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    private fun sendLinkMicStopMsg() {
        val msg = Message.obtain()
        msg.what = UI_EVENT_LINK_MIC_STOP
        observer?.sendMessage(msg)
    }

    private fun sendLinkMicBusyMsg(linkMicResponseBean: LinkMicResponseBean) {
        val msg = Message.obtain()
        msg.what = UI_EVENT_LINK_MIC_BUSY
        msg.obj = linkMicResponseBean
        observer?.sendMessage(msg)
    }

    private fun sendAudienceLinkMicResponse(audienceLinkMicResponseBean: AudienceLinkMicResponseBean) {
        val msg = Message.obtain()
        msg.what = UI_EVENT_AUDIENCE_LINK_MIC_RESPONSE
        msg.obj = audienceLinkMicResponseBean
        observer?.sendMessage(msg)
    }

    private fun isWaitMic(isWaitMic: Boolean) {

        val liveRoomMsgBean = LiveRoomMsgBean()

        liveRoomMsgBean.type = LiveRoomMsgBean.TYPE_MIC_SORT
        if (isWaitMic) {
            liveRoomMsgBean.content = TheLApp.getContext().getString(R.string.msg_type_sort)
        } else {
            liveRoomMsgBean.content = TheLApp.getContext().getString(R.string.msg_type_freedom)
        }
        observer?.addMsg(liveRoomMsgBean)
        observer?.sendEmptyMessage(UI_EVENT_REFRESH_MSGS)
    }

}