package com.thel.modules.live.view.sound;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thel.base.BaseDialogFragment;
import com.thel.modules.live.in.LiveBaseView;

/**
 * Created by waiarl on 2018/1/23.
 */

public class LiveSoundBackListenDialogView extends BaseDialogFragment implements LiveBaseView<LiveSoundBackListenDialogView> {

    private LiveSoundBackListenDialogView instance;
    private LiveSoundBackListenView view;

    public LiveSoundBackListenDialogView getInstance(Bundle bundle) {
        instance = new LiveSoundBackListenDialogView();
        instance.setArguments(bundle);
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = new LiveSoundBackListenView(getContext());
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setListener();
    }

    private void setListener() {
        view.setCancelListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hide();
            }
        });
    }
    /***后台收听***/
    public void setBackListenListener(View.OnClickListener listener) {
        view.setBackListenListener(listener);
    }

    /*******************************************一下为接口继承方法******************************************************/
    @Override
    public LiveSoundBackListenDialogView show() {
        return null;
    }

    @Override
    public LiveSoundBackListenDialogView hide() {
        dismiss();
       return this;
    }

    @Override
    public LiveSoundBackListenDialogView destroyView() {
        return null;
    }

    @Override
    public boolean isAnimating() {
        return false;
    }

    @Override
    public void setAnimating(boolean isAnimating) {

    }

    @Override
    public void showShade(boolean show) {

    }
}
