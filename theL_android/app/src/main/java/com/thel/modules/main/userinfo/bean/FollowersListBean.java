package com.thel.modules.main.userinfo.bean;

import android.text.TextUtils;

import com.thel.base.BaseDataBean;
import com.thel.bean.FollowerBean;
import com.thel.imp.black.BlackUtils;
import com.thel.utils.ShareFileUtils;

import java.util.ArrayList;
import java.util.List;

public class FollowersListBean extends BaseDataBean {

    public List<FollowerBean> users = new ArrayList<>();
    /**
     * 粉丝总数
     */
    public int fansTotal;
    /**
     * 关注人数
     * */
    public int followersTotal;

    /**
     * 将屏蔽的用户过滤掉
     */
    public void filtBlockUsers() {

        String myUserId = ShareFileUtils.getString(ShareFileUtils.ID, "");

        if (TextUtils.isEmpty(myUserId)) {
            return;
        }

        List<String> blockUserIds = BlackUtils.getBlackList();

        if (blockUserIds.isEmpty()) {
            return;
        }

        List<FollowerBean> blockUserWinkBeans = new ArrayList<FollowerBean>();

        for (FollowerBean follower : users) {
            for (String userId : blockUserIds) {
                if ((follower.userId + "").equals(userId)) {
                    blockUserWinkBeans.add(follower);
                    break;
                }
            }

        }
        users.removeAll(blockUserWinkBeans);
    }
}