package com.thel.modules.main.messages.adapter;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.me.MyCircleRequestBean;
import com.thel.constants.TheLConstants;
import com.thel.utils.DateUtils;
import com.thel.utils.ImageUtils;
import com.thel.utils.ShareFileUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * 新的密友请求
 *
 * @author setsail
 */
public class MyCircleRequestListAdapter extends BaseAdapter {

    private ArrayList<MyCircleRequestBean> myCircleRequestBeans = new ArrayList<MyCircleRequestBean>();
    private LayoutInflater mInflater;
    private Context mContext;

    // 每多少时间间隔加一个时间标识
    private int interval = 60 * 10 * 1000;

    private long oneDayInMillis = 24 * 60 * 60 * 1000;

    public MyCircleRequestListAdapter(Context context, ArrayList<MyCircleRequestBean> myCircleRequestBeans) {
        mInflater = (LayoutInflater) TheLApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mContext = context;
        refreshAdapter(myCircleRequestBeans);
    }

    public void refreshAdapter(ArrayList<MyCircleRequestBean> requests) {
        this.myCircleRequestBeans = requests;

        if (this.myCircleRequestBeans.size() > 0) {
            SimpleDateFormat sdf0 = new SimpleDateFormat("MM-dd HH:mm");

            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(System.currentTimeMillis());

            Calendar calEndToday = (Calendar) cal.clone();
            calEndToday.set(Calendar.HOUR_OF_DAY, 23);
            calEndToday.set(Calendar.MINUTE, 59);
            calEndToday.set(Calendar.SECOND, 59);
            calEndToday.set(Calendar.MILLISECOND, 999);

            if (requests.get(0).handleTime > calEndToday.getTimeInMillis() - oneDayInMillis && requests.get(0).handleTime <= calEndToday.getTimeInMillis()) {// 今天的消息
                sdf0 = new SimpleDateFormat("'" + TheLApp.getContext().getString(R.string.chat_activity_today) + "' HH:mm");
            } else if (requests.get(0).handleTime > calEndToday.getTimeInMillis() - oneDayInMillis * 2 && requests.get(0).handleTime <= calEndToday.getTimeInMillis() - oneDayInMillis) {// 昨天的消息
                sdf0 = new SimpleDateFormat("'" + TheLApp.getContext().getString(R.string.chat_activity_yesterday) + "' HH:mm");
            }
            //            else if (requests.get(0).handleTime <= calEndToday.getTimeInMillis() - oneDayInMillis * 2 && requests.get(0).handleTime > calEndToday.getTimeInMillis() - oneDayInMillis * 7) {// 昨天之前一周以内的消息
            //                int day = DateUtils.getDayOfWeek(requests.get(0).handleTime);
            //                switch (day) {
            //                    case Calendar.SUNDAY:
            //                        sdf0 = new SimpleDateFormat("'" + TheLApp.getContext().getString(R.string.chat_activity_sun) + "' HH:mm");
            //                        break;
            //                    case Calendar.MONDAY:
            //                        sdf0 = new SimpleDateFormat("'" + TheLApp.getContext().getString(R.string.chat_activity_mon) + "' HH:mm");
            //                        break;
            //                    case Calendar.TUESDAY:
            //                        sdf0 = new SimpleDateFormat("'" + TheLApp.getContext().getString(R.string.chat_activity_tue) + "' HH:mm");
            //                        break;
            //                    case Calendar.WEDNESDAY:
            //                        sdf0 = new SimpleDateFormat("'" + TheLApp.getContext().getString(R.string.chat_activity_wed) + "' HH:mm");
            //                        break;
            //                    case Calendar.THURSDAY:
            //                        sdf0 = new SimpleDateFormat("'" + TheLApp.getContext().getString(R.string.chat_activity_thur) + "' HH:mm");
            //                        break;
            //                    case Calendar.FRIDAY:
            //                        sdf0 = new SimpleDateFormat("'" + TheLApp.getContext().getString(R.string.chat_activity_fri) + "' HH:mm");
            //                        break;
            //                    case Calendar.SATURDAY:
            //                        sdf0 = new SimpleDateFormat("'" + TheLApp.getContext().getString(R.string.chat_activity_sat) + "' HH:mm");
            //                        break;
            //                    default:
            //                        break;
            //                }
            //            }
            String time0 = sdf0.format(requests.get(0).handleTime);
            long timeMillis0 = requests.get(0).handleTime;
            requests.get(0).date = time0;
            for (int i = 1; i < requests.size(); i++) {
                if (requests.get(i).handleTime < timeMillis0 - interval) {
                    SimpleDateFormat sdf1 = new SimpleDateFormat("MM-dd HH:mm");
                    if (requests.get(i).handleTime > calEndToday.getTimeInMillis() - oneDayInMillis && requests.get(i).handleTime <= calEndToday.getTimeInMillis()) {// 今天的消息
                        sdf1 = new SimpleDateFormat("'" + TheLApp.getContext().getString(R.string.chat_activity_today) + "' HH:mm");
                    } else if (requests.get(i).handleTime > calEndToday.getTimeInMillis() - oneDayInMillis * 2 && requests.get(i).handleTime <= calEndToday.getTimeInMillis() - oneDayInMillis) {// 昨天的消息
                        sdf1 = new SimpleDateFormat("'" + TheLApp.getContext().getString(R.string.chat_activity_yesterday) + "' HH:mm");
                    } else if (requests.get(i).handleTime <= calEndToday.getTimeInMillis() - oneDayInMillis * 2 && requests.get(i).handleTime > calEndToday.getTimeInMillis() - oneDayInMillis * 7) {// 昨天之前一周以内的消息
                        int day = DateUtils.getDayOfWeek(requests.get(i).handleTime);
                        switch (day) {
                            case Calendar.SUNDAY:
                                sdf1 = new SimpleDateFormat("'" + TheLApp.getContext().getString(R.string.chat_activity_sun) + "' HH:mm");
                                break;
                            case Calendar.MONDAY:
                                sdf1 = new SimpleDateFormat("'" + TheLApp.getContext().getString(R.string.chat_activity_mon) + "' HH:mm");
                                break;
                            case Calendar.TUESDAY:
                                sdf1 = new SimpleDateFormat("'" + TheLApp.getContext().getString(R.string.chat_activity_tue) + "' HH:mm");
                                break;
                            case Calendar.WEDNESDAY:
                                sdf1 = new SimpleDateFormat("'" + TheLApp.getContext().getString(R.string.chat_activity_wed) + "' HH:mm");
                                break;
                            case Calendar.THURSDAY:
                                sdf1 = new SimpleDateFormat("'" + TheLApp.getContext().getString(R.string.chat_activity_thur) + "' HH:mm");
                                break;
                            case Calendar.FRIDAY:
                                sdf1 = new SimpleDateFormat("'" + TheLApp.getContext().getString(R.string.chat_activity_fri) + "' HH:mm");
                                break;
                            case Calendar.SATURDAY:
                                sdf1 = new SimpleDateFormat("'" + TheLApp.getContext().getString(R.string.chat_activity_sat) + "' HH:mm");
                                break;
                            default:
                                break;
                        }
                    }
                    String time1 = sdf1.format(requests.get(i).handleTime);
                    requests.get(i).date = time1;
                    timeMillis0 = requests.get(i).handleTime;
                }
            }
        }
    }

    @Override
    public int getCount() {
        return myCircleRequestBeans.size();
    }

    @Override
    public Object getItem(int position) {
        return myCircleRequestBeans.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void updateSingleRow(ListView listView, int position) {
        int firstVisiblePosition = listView.getFirstVisiblePosition();
        int lastVisiblePosition = listView.getLastVisiblePosition();
        if (position >= firstVisiblePosition && position <= lastVisiblePosition) {
            View view = listView.getChildAt(position - firstVisiblePosition);
            if (view.getTag() instanceof HoldView) {
                HoldView holdView = (HoldView) view.getTag();
                refreshItem(position - listView.getHeaderViewsCount(), holdView, false);
            }
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        HoldView holdView = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.my_circle_request_listitem, parent, false);
            holdView = new HoldView();
            holdView.txt_day = convertView.findViewById(R.id.txt_day);
            holdView.lin_img_left = convertView.findViewById(R.id.lin_img_left);
            holdView.img_thumb_left = convertView.findViewById(R.id.img_thumb_left);
            holdView.txt_nickname = convertView.findViewById(R.id.txt_nickname);
            holdView.txt_request_type = convertView.findViewById(R.id.txt_request_type);
            holdView.txt_anniversary_date = convertView.findViewById(R.id.txt_anniversary_date);
            holdView.txt_request_content = convertView.findViewById(R.id.txt_request_content);
            holdView.txt_status = convertView.findViewById(R.id.txt_status);
            holdView.lin_bottom = convertView.findViewById(R.id.lin_bottom);
            holdView.lin_operation = convertView.findViewById(R.id.lin_operation);
            holdView.lin_accept = convertView.findViewById(R.id.lin_accept);
            holdView.lin_refuse = convertView.findViewById(R.id.lin_refuse);

            convertView.setTag(holdView);
        } else {
            holdView = (HoldView) convertView.getTag();
        }

        refreshItem(position, holdView, true);

        return convertView;
    }

    private void refreshItem(int position, HoldView holdView, boolean refreshAvatar) {
        // 处理数据显示
        final MyCircleRequestBean tempRequestBean = myCircleRequestBeans.get(position);

        if (refreshAvatar) {// 刷新头像
            holdView.img_thumb_left.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(tempRequestBean.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE)));
        }

        if (ShareFileUtils.getString(ShareFileUtils.ID, "").equals(tempRequestBean.userId + "")) {
            holdView.img_thumb_left.setTag(tempRequestBean.receivedId + "");
        } else {
            holdView.img_thumb_left.setTag(tempRequestBean.userId + "");
        }

        holdView.img_thumb_left.setOnClickListener((View.OnClickListener) mContext);

        // 消息分块时间
        if (!TextUtils.isEmpty(tempRequestBean.date)) {
            holdView.txt_day.setVisibility(View.VISIBLE);
            holdView.txt_day.setText(tempRequestBean.date);
        } else {
            holdView.txt_day.setVisibility(View.GONE);
        }
        //昵称
        holdView.txt_nickname.setText(tempRequestBean.nickName);
        holdView.txt_anniversary_date.setVisibility(View.GONE);
        holdView.txt_request_content.setVisibility(View.VISIBLE);
        holdView.txt_request_type.setVisibility(View.VISIBLE);
        holdView.lin_bottom.setVisibility(View.VISIBLE);

        if (tempRequestBean.requestStatus == -3) {// 解除绑定的数据
            holdView.lin_bottom.setVisibility(View.GONE);
            holdView.txt_request_type.setVisibility(View.GONE);
            if (tempRequestBean.requestType == 0) {// partner请求
                holdView.txt_request_content.setText(TheLApp.getContext().getString(R.string.my_circle_requests_act_canceled_partner));
            } else {// bff请求
                holdView.txt_request_content.setText(TheLApp.getContext().getString(R.string.my_circle_requests_act_canceled_bff));
            }
        } else {// 解除绑定以外的数据
            if (!ShareFileUtils.getString(ShareFileUtils.ID, "").equals(tempRequestBean.receivedId + "")) {// 我发给别人的请求，只有接受和拒绝两种
                holdView.lin_bottom.setVisibility(View.GONE);
                holdView.txt_request_type.setVisibility(View.GONE);
                if (tempRequestBean.requestStatus == 1) {// 接受
                    if (tempRequestBean.requestType == 0) {// partner请求
                        holdView.txt_request_content.setText(TheLApp.getContext().getString(R.string.my_circle_requests_act_partner_request_accepted));
                    } else {// bff请求
                        holdView.txt_request_content.setText(TheLApp.getContext().getString(R.string.my_circle_requests_act_bff_request_accepted));
                    }
                } else {// 拒绝
                    if (tempRequestBean.requestType == 0) {// partner请求
                        holdView.txt_request_content.setText(TheLApp.getContext().getString(R.string.my_circle_requests_act_partner_request_refused));
                    } else {// bff请求
                        holdView.txt_request_content.setText(TheLApp.getContext().getString(R.string.my_circle_requests_act_bff_request_refused));
                    }
                }
            } else {// 别人发给我的请求
                if (tempRequestBean.requestStatus == 0) {// 待处理
                    holdView.lin_operation.setVisibility(View.VISIBLE);
                    holdView.txt_status.setVisibility(View.GONE);
                    holdView.lin_refuse.setOnClickListener((View.OnClickListener) mContext);
                    holdView.lin_refuse.setTag(position);
                    holdView.lin_accept.setOnClickListener((View.OnClickListener) mContext);
                    holdView.lin_accept.setTag(position);
                } else {
                    holdView.lin_operation.setVisibility(View.GONE);
                    holdView.txt_status.setVisibility(View.VISIBLE);
                    if (tempRequestBean.requestStatus == 1) {// 接受
                        holdView.txt_status.setText(TheLApp.getContext().getString(R.string.my_circle_requests_act_accepted));
                    } else if (tempRequestBean.requestStatus == -1) {// 拒绝
                        holdView.txt_status.setText(TheLApp.getContext().getString(R.string.my_circle_requests_act_refused));
                    } else if (tempRequestBean.requestStatus == -2) {// 取消
                        holdView.txt_status.setText(TheLApp.getContext().getString(R.string.my_circle_requests_act_canceled));
                    } else {// 过期
                        holdView.txt_status.setText(TheLApp.getContext().getString(R.string.my_circle_requests_act_out_of_date));
                    }
                }
                if (tempRequestBean.requestType == 0) {// partner请求
                    holdView.txt_request_type.setText(TheLApp.getContext().getString(R.string.my_circle_requests_act_partner_request));
                    holdView.txt_anniversary_date.setVisibility(View.VISIBLE);
                    if (tempRequestBean.anniversaryDate != null)
                        holdView.txt_anniversary_date.setText(TheLApp.getContext().getString(R.string.my_circle_requests_act_anniversary) + " " + tempRequestBean.anniversaryDate.replaceAll("-", "."));
                } else {// bff请求
                    holdView.txt_request_type.setText(TheLApp.getContext().getString(R.string.my_circle_requests_act_bff_request));
                }
                if (!TextUtils.isEmpty(tempRequestBean.requestComent)) {
                    holdView.txt_request_content.setText(tempRequestBean.requestComent);
                } else {
                    holdView.txt_request_content.setVisibility(View.GONE);
                }
            }
        }
    }

    class HoldView {
        TextView txt_day; // 日期
        LinearLayout lin_img_left;
        SimpleDraweeView img_thumb_left; // 对方头像
        TextView txt_nickname;
        TextView txt_request_type;
        TextView txt_anniversary_date;
        TextView txt_request_content;
        TextView txt_status;
        LinearLayout lin_bottom;
        LinearLayout lin_operation;
        LinearLayout lin_refuse;
        LinearLayout lin_accept;
    }

}
