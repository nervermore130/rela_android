package com.thel.modules.main.me.aboutMe;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.bean.ContactBean;
import com.thel.constants.TheLConstants;
import com.thel.utils.AppInit;
import com.thel.utils.DialogUtil;
import com.thel.utils.ImageUtils;

/**
 * 赠送Vip请求界面（填写内容）
 * Created by lingwei on 2017/10/25.
 */

public class GiveVipRequestActivity extends BaseActivity {
    private SimpleDraweeView img_background;
    private SimpleDraweeView img_avatar_left;
    private TextView txt_nickname;
    private EditText edt_say_something;
    private TextView txt_limit;
    private Button btn_send;
    private TextWatcher textWatcher;
    private CloseActivityReceiver closeReceiver;
    private ContactBean friend;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.give_vip_request_layout);
        registerReceiver();
        findViewById();
        getIntentData();
        setListener();
        initUI();
    }

    private void findViewById() {
        img_background = findViewById(R.id.img_background);
        img_avatar_left = findViewById(R.id.img_avatar_left);
        txt_nickname = findViewById(R.id.txt_nickname);
        txt_limit = findViewById(R.id.txt_limit);
        edt_say_something = findViewById(R.id.edt_say_something);
        btn_send = findViewById(R.id.btn_send);
    }

    private void setListener() {
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoBuyVip();
            }
        });
        textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                txt_limit.setText((100 - charSequence.length()) + "");
                if (charSequence.length() >= 100) {
                    DialogUtil.showToastShort(GiveVipRequestActivity.this, String.format(getString(R.string.info_words_length_limit), 100));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };
        edt_say_something.addTextChangedListener(textWatcher);
    }

    /**
     * 进入购买会员界面
     */
    private void gotoBuyVip() {
        Intent intent = new Intent(GiveVipRequestActivity.this, GiveVipActivity.class);
        Bundle bd = new Bundle();
        bd.putBoolean(TheLConstants.BUNDLE_KEY_GIVE_VIP, true);
        bd.putSerializable(TheLConstants.BUNDLE_KEY_FRIEND, friend);
        bd.putString(TheLConstants.BUNDLE_KEY_SAY_SOMETHING, edt_say_something.getText().toString().trim());
        intent.putExtras(bd);
        startActivity(intent);
    }

    private void getIntentData() {
        Intent intent = getIntent();
        friend = (ContactBean) intent.getSerializableExtra(TheLConstants.BUNDLE_KEY_CONTACT);
    }

    private void registerReceiver() {
        closeReceiver = new CloseActivityReceiver();
        registerReceiver(closeReceiver, new IntentFilter(TheLConstants.BROADCAST_ACTION_CLOSE_BEFORE_ACTIVITY));
    }

    class CloseActivityReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                boolean closeActivity = intent.getBooleanExtra(TheLConstants.BUNDLE_KEY_CLOSE_BEFORE_ACTIVITY, false);
                if (closeActivity) {
                    GiveVipRequestActivity.this.finish();
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unRegisterReceiver();

    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void unRegisterReceiver() {
        if (closeReceiver != null) {
            this.unregisterReceiver(closeReceiver);
        }
    }

    private void initUI() {
        img_avatar_left.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(friend.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE)));
        txt_nickname.setText(friend.nickName);
        img_background.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(friend.bgImage, AppInit.displayMetrics.widthPixels, getResources().getDimension(R.dimen.bg_height))));
        edt_say_something.setHint(String.format(getString(R.string.send_circle_request_act_hint), friend.nickName));
//        txt_title.setText(getString(R.string.gift_card));
    }
}
