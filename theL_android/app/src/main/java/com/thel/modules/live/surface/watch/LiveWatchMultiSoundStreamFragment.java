package com.thel.modules.live.surface.watch;

import android.net.Uri;
import android.opengl.GLSurfaceView;
import android.os.Bundle;

import androidx.annotation.Nullable;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.postprocessors.IterativeBoxBlurPostProcessor;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.pili.pldroid.player.AVOptions;
import com.pili.pldroid.player.PLMediaPlayer;
import com.pili.pldroid.player.PLOnAudioFrameListener;
import com.pili.pldroid.player.PLOnPreparedListener;
import com.thel.BuildConfig;
import com.thel.R;
import com.thel.base.BaseFragment;
import com.thel.bean.LiveInfoLogBean;
import com.thel.constants.TheLConstants;
import com.thel.manager.CDNBalanceManager;
import com.thel.modules.live.agora.OnJoinChannelListener;
import com.thel.modules.live.agora.RtcEngineHandler;
import com.thel.modules.live.bean.AgoraBean;
import com.thel.modules.live.bean.LiveRoomBean;
import com.thel.modules.live.in.LiveShowVideoIn;
import com.thel.modules.live.in.VideoPreparedListener;
import com.thel.modules.live.utils.LiveUtils;
import com.thel.utils.AppInit;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.ImageUtils;
import com.thel.utils.L;
import com.thel.utils.NetworkUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.ToastUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;

import java.io.IOException;

import io.agora.rtc.Constants;
import io.agora.rtc.IRtcEngineEventHandler;

/**
 * Created by chad
 * Time 18/5/7
 * Email: wuxianchuang@foxmail.com
 * Description: TODO 声网多人连麦观看端
 */
public class LiveWatchMultiSoundStreamFragment extends BaseFragment implements LiveShowVideoIn {

    private static final String TAG = "LiveWatchMultiSoundStreamFragment";

    //相遇是是否自动闭麦
    private boolean guestAutoMute = false;

    //当前麦状态，默认开启
    private boolean currentMute = true;

    private LiveRoomBean liveRoomBean;

    private SimpleDraweeView img_bg;

    private PLMediaPlayer mPLMediaPlayer = null;

    private LiveWatchObserver observer;

    private boolean isJoinChannel = false;

    private long startTime = 0;

    private String playUrl = "";

    private RtcEngineHandler mRtcEngineHandler;

    private OnJoinChannelListener mOnJoinChannelListener = new OnJoinChannelListener() {
        @Override public void onJoinChannelSuccess() {
            isJoinChannel = true;
        }

        @Override public void onLeaveChannelSuccess() {
            isJoinChannel = false;
        }

        @Override public void onUserJoined(int uid, SurfaceView view) {

        }

        @Override public void onUserOffline(int uid) {

        }

        @Override public void onAudioVolumeIndication(IRtcEngineEventHandler.AudioVolumeInfo[] speakers, int totalVolume) {

        }

        @Override public void onError(int error) {

        }
    };

    private RelativeLayout rel;

    public static LiveWatchMultiSoundStreamFragment newInstance(Bundle bundle) {
        LiveWatchMultiSoundStreamFragment fragment = new LiveWatchMultiSoundStreamFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Bundle bundle = getArguments();
        liveRoomBean = (LiveRoomBean) bundle.getSerializable(TheLConstants.BUNDLE_KEY_LIVE_ROOM);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_live_show_video_ks, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rel = view.findViewById(R.id.root_rl);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (observer != null) {
            observer.bindVideo(this);
        }

        L.d(TAG, "ksyMediaPlayer onActivityCreated ");

    }

    @Override
    public void onDestroy() {

        if (mRtcEngineHandler != null) {
            mRtcEngineHandler.leaveChannel();
            mRtcEngineHandler.destroy();
        }

        if (mPLMediaPlayer != null) {
            mPLMediaPlayer.release();
            mPLMediaPlayer = null;
        }
        uploadWatchTimeToGIO();

        super.onDestroy();
    }

    private void ksyPlayStream() {

        if (liveRoomBean != null && mPLMediaPlayer == null && getContext() != null) {

            AVOptions options = new AVOptions();
            // the unit of timeout is ms
            options.setInteger(AVOptions.KEY_PREPARE_TIMEOUT, 10 * 1000);
            options.setInteger(AVOptions.KEY_LIVE_STREAMING, 1);
            options.setInteger(AVOptions.KEY_MEDIACODEC, AVOptions.MEDIA_CODEC_SW_DECODE);
            options.setInteger(AVOptions.KEY_LOG_LEVEL, 5);
            options.setInteger(AVOptions.KEY_AUDIO_DATA_CALLBACK, 1);
            options.setInteger(AVOptions.KEY_FAST_OPEN, 1);
            options.setInteger(AVOptions.KEY_CACHE_BUFFER_DURATION, 500);
            options.setInteger(AVOptions.KEY_MAX_CACHE_BUFFER_DURATION, 4000);

            mPLMediaPlayer = new PLMediaPlayer(getContext(), options);
            mPLMediaPlayer.setOnPreparedListener(mPLOnPreparedListener);
            mPLMediaPlayer.setOnAudioFrameListener(mPLOnAudioFrameListener);

            playUrl = CDNBalanceManager.getInstance().getFastMultiLinkMicVideoUrl(String.valueOf(liveRoomBean.user.id));

            if (TextUtils.isEmpty(playUrl)) {
                playUrl = liveRoomBean.liveUrl;
            }

            L.d(TAG, " playUrl : " + playUrl);

            initLiveInfoLog(playUrl);

            mPLMediaPlayer.setVolume(1.0f, 1.0f);

            try {
                mPLMediaPlayer.setDataSource(playUrl);
                mPLMediaPlayer.prepareAsync();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void setClientRole(int role, AgoraBean agoraBean) {

        L.d(TAG, "-------setClientRole--------" + role);

        if (liveRoomBean != null) {
            //if 观众 else 嘉宾
            if (role == Constants.CLIENT_ROLE_AUDIENCE) {

                if (mPLMediaPlayer != null) {
                    mPLMediaPlayer.release();
                    mPLMediaPlayer = null;
                }

                ksyPlayStream();

                //                if (!TextUtils.isEmpty(liveRoomBean.liveUrl))
                //                    bdMetaDataRetriever.retriever(liveRoomBean.liveUrl, clientCtrl);

                if (isJoinChannel) {
                    if (mRtcEngineHandler != null) {
                        mRtcEngineHandler.leaveChannel();
                    }
                }

            } else if (role == Constants.CLIENT_ROLE_BROADCASTER) {
                if (!isJoinChannel && agoraBean != null && !TextUtils.isEmpty(agoraBean.channelId)) {
                    try {
                        //  uploadWatchTimeToGIO();

                        int uid = Integer.valueOf(UserUtils.getMyUserId());

                        mRtcEngineHandler = new RtcEngineHandler(getActivity(), RtcEngineHandler.LIVE_TYPE_AUDIO, agoraBean.appId);

                        mRtcEngineHandler.setOnJoinChannelSuccessListener(mOnJoinChannelListener);

                        mRtcEngineHandler.setAudioProfile(2, 1);

                        mRtcEngineHandler.setType(RtcEngineHandler.TYPE_MULTI_LINK_MIC);

                        String liveId = liveRoomBean != null ? liveRoomBean.liveId : "";

                        mRtcEngineHandler.init(agoraBean.token, agoraBean.channelId, null, uid, liveId);

                    } catch (Exception e) {
                        e.printStackTrace();
                        ToastUtils.showToastShort(getActivity(), "RtcEngine error");
                        if (getActivity() != null) {
                            getActivity().finish();
                        }
                    }

                    if (mPLMediaPlayer != null) {
                        mPLMediaPlayer.release();
                        mPLMediaPlayer = null;
                    }

                }
            }
        }
    }

    @Override
    public void muteLocalAudioStream(boolean mute) {

        if (mRtcEngineHandler != null && isJoinChannel) {
            this.currentMute = mute;
            mRtcEngineHandler.muteLocalAudioStream(mute);
        }
    }

    @Override
    public void autoOpenLocalAudioStream() {

        if (mRtcEngineHandler != null && isJoinChannel) {
            if (!currentMute) {
                currentMute = true;
                guestAutoMute = true;
                mRtcEngineHandler.muteLocalAudioStream(true);
            }
        }
    }

    @Override
    public void autoCloseLocalAudioStream() {

        if (mRtcEngineHandler != null && isJoinChannel) {
            if (guestAutoMute) {
                currentMute = false;
                guestAutoMute = false;
                mRtcEngineHandler.muteLocalAudioStream(false);
            }
        }
    }

    @Override
    public void levelChannel() {

        setClientRole(Constants.CLIENT_ROLE_AUDIENCE, null);
    }

    @Override public String getCurrentPlayUrl() {
        return playUrl == null ? liveRoomBean.liveUrl : playUrl;
    }

    @Override
    public void bindObserver(LiveWatchObserver observer) {
        this.observer = observer;
        observer.bindVideo(this);
    }

    @Override
    public String getScreenShotImagePath() {
        return null;
    }

    @Override
    public boolean pause() {
        return false;
    }

    @Override
    public boolean destory() {

        L.d(TAG, " destory ");

        return false;
    }

    @Override
    public boolean resume() {
        return false;
    }

    @Override
    public void setVideoPreparedListener(VideoPreparedListener listener) {

    }

    @Override
    public void liveClosed() {

        L.d(TAG, " liveClosed ");


    }

    public void setClientCtrl(LiveWatchMsgClientCtrl clientCtrl) {

    }

    public void refreshLiveShow(LiveRoomBean liveRoomBean) {
        if (liveRoomBean != null) {
            this.liveRoomBean = liveRoomBean;
            uploadWatchTimeToGIO();
            setSimpleBlur(img_bg, liveRoomBean.imageUrl, AppInit.displayMetrics.widthPixels, AppInit.displayMetrics.heightPixels);

            L.d("LiveWatchMultiSound", "------refreshLiveShow------");

            setClientRole(Constants.CLIENT_ROLE_AUDIENCE, null);
        }
    }

    /**
     * 上传观看直播时长到GrowingIO
     */
    private void uploadWatchTimeToGIO() {
        long endTime = System.currentTimeMillis();

        long watchTime = endTime - startTime;

        if (startTime > 0) {
            if (watchTime > 0) {
                watchTime = watchTime / 1000;
            } else {
                watchTime = 1;
            }
        } else {
            watchTime = 1;
        }

        L.d(TAG, " watchTime : " + watchTime);
        if (watchTime >= 120) {
            GrowingIOUtil.uploadWatchTimeLonger2M("liveWatchTimeLonger2M");
        }
        if (liveRoomBean != null) {
            if (LiveRoomBean.TYPE_VOICE == liveRoomBean.audioType) {
                if (LiveRoomBean.MULTI_LIVE == liveRoomBean.isMulti) {
                    GrowingIOUtil.uploadLivePageView(2);
                    //4.10.0上传时长有问题 改成新的方式
                    GrowingIOUtil.liveWatchTimeTrack(2, watchTime);
                } else {
                    GrowingIOUtil.uploadLivePageView(1);
                    GrowingIOUtil.liveWatchTimeTrack(1, watchTime);


                }
            }
        }
    }

    private PLOnPreparedListener mPLOnPreparedListener = new PLOnPreparedListener() {
        @Override public void onPrepared(int i) {
            startTime = System.currentTimeMillis();

            mPLMediaPlayer.start();
        }
    };

    private PLOnAudioFrameListener mPLOnAudioFrameListener = new PLOnAudioFrameListener() {
        @Override public void onAudioFrameAvailable(byte[] data, int size, int width, int height, int format, long ts) {

//            L.d(TAG, " hexString data : " + bytesToHexString(data));
//
//            String hexString = bytesToHexString(Arrays.copyOfRange(data, 19, 23));
//
//            L.d(TAG, " hexString : " + hexString);
//
//            String tsString = bytesToHexString(Arrays.copyOfRange(data, 23, 31));
//
//            L.d(TAG, " tsString : " + tsString);

//            if (format == PLOnVideoFrameListener.VIDEO_FORMAT_SEI && hexString != null && hexString.equals("ts64")) {
//
//                L.d(TAG, " hexString : " + hexString);
//
//            }
        }
    };

    private void initSoundView(String url) {
        if (img_bg == null) {
            img_bg = new SimpleDraweeView(getContext());
            rel.addView(img_bg, RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            setSimpleBlur(img_bg, url, AppInit.displayMetrics.widthPixels, AppInit.displayMetrics.heightPixels);
            ImageView imageView = new ImageView(getContext());
            imageView.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
            imageView.setImageResource(R.color.black_transparent_50);
            rel.addView(imageView);
        }
    }

    private void setSimpleBlur(SimpleDraweeView simpleDraweeView, String url, int width, int height) {
        if (simpleDraweeView != null && url != null) {
            simpleDraweeView.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(url, width, height))).setPostprocessor(new IterativeBoxBlurPostProcessor(10, 10)).build()).setAutoPlayAnimations(true).build());
        }
    }

    /**
     * Convert byte[] to hex string
     *
     * @param src byte[] data
     * @return hex string
     */
    public static String bytesToHexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder();
        if (src == null || src.length <= 0) {
            return null;
        }
        for (int i = 0; i < src.length; i++) {
            int v = src[i] & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
        }
        return stringBuilder.toString();
    }

    private void initLiveInfoLog(String playUrl) {

        LiveInfoLogBean.getInstance().getLiveChatAnalytics().time = LiveUtils.getLiveTime();
        LiveInfoLogBean.getInstance().getLiveChatAnalytics().liveUserId = String.valueOf(liveRoomBean.user.id);
        LiveInfoLogBean.getInstance().getLiveChatAnalytics().roomId = liveRoomBean.liveId;
        LiveInfoLogBean.getInstance().getLiveChatAnalytics().relaVersion = BuildConfig.VERSION_NAME;
        LiveInfoLogBean.getInstance().getLiveChatAnalytics().network = NetworkUtils.getAPNType();

        LiveInfoLogBean.getInstance().getLivePlayAnalytics().time = LiveUtils.getLiveTime();
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().userId = Utils.getMyUserId();
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().liveUserId = String.valueOf(liveRoomBean.user.id);
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().cdnDomain = playUrl;
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().inUrl = playUrl;
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().logType = TheLConstants.LiveInfoLogConstants.TYPE_FIRST_PLAY;
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().roomId = liveRoomBean.liveId;
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().relaVersion = BuildConfig.VERSION_NAME;
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().network = NetworkUtils.getAPNType();
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().liveUrlSort = ShareFileUtils.getInt(ShareFileUtils.LIVE_URL_SORT, -1);
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().forceServerLiveUrl = liveRoomBean.forceServerLiveUrl;
        LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getLivePlayAnalytics());
    }

    private void setLiveInfoLog(String playUrl) {
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().outUrl = LiveInfoLogBean.getInstance().getLivePlayAnalytics().inUrl;
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().inUrl = playUrl;
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().logType = TheLConstants.LiveInfoLogConstants.TYPE_PLAY_CHANGE_LOG;
        LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getLivePlayAnalytics());
    }

}
