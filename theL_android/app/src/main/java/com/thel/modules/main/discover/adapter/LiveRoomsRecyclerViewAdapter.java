package com.thel.modules.main.discover.adapter;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.view.SimpleDraweeView;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.modules.live.bean.LiveRoomBean;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;
import com.thel.utils.AppInit;

import java.util.List;

/**
 * 正在直播的adapter
 * Created by lingwei on 2017/11/6.
 */

public class LiveRoomsRecyclerViewAdapter extends BaseRecyclerViewAdapter<LiveRoomBean> {

    private final float radius;
    private final float padding;
    private int picSize;

    public LiveRoomsRecyclerViewAdapter(List<LiveRoomBean> data) {
        super(R.layout.adapter_live_rooms, data);
        radius = TheLApp.getContext().getResources().getDimension(R.dimen.live_new_user_item_radius);
        padding = TheLApp.getContext().getResources().getDimension(R.dimen.live_new_user_item_radius);
        picSize = (int) ((AppInit.displayMetrics.widthPixels - 3 * padding) / 2);
    }

    @Override
    protected void convert(BaseViewHolder holdView, final LiveRoomBean liveRoomBean) {
        final LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) holdView.getView(R.id.rel_preview).getLayoutParams();
        params.width = picSize;
        params.height = picSize;
        final SimpleDraweeView preview = holdView.getView(R.id.preview);
        // preview.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadii(radius, radius, radius, radius).setRoundingMethod(RoundingParams.RoundingMethod.OVERLAY_COLOR).setOverlayColor(ContextCompat.getColor(TheLApp.getContext(), R.color.white)));
        preview.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadius(radius));
        // 头像和昵称
        //  holdView.setImageUrl(R.id.avatar, liveRoomBean.user.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE);
        holdView.setText(R.id.nickname, liveRoomBean.user.nickName);
        // 观看人数
        holdView.setText(R.id.audience, liveRoomBean.liveUsersCount + "");
        // 预览图
        holdView.setImageUrl(R.id.preview, liveRoomBean.imageUrl, TheLApp.getContext().getResources().getDimension(R.dimen.moment_pic_thumbnail_big), TheLApp.getContext().getResources().getDimension(R.dimen.moment_pic_thumbnail_big));
        // 直播描述
        holdView.setText(R.id.text, liveRoomBean.text);

        //2.20新增， 主播推荐标签

        holdView.setVisibility(R.id.txt_label, View.GONE);

        if (!TextUtils.isEmpty(liveRoomBean.label)) {
            holdView.setVisibility(R.id.txt_label, View.VISIBLE);
            holdView.setText(R.id.txt_label, liveRoomBean.label + "");
            ((TextView) holdView.getView(R.id.txt_label)).setMaxLines(1);
        }
        holdView.setVisibility(R.id.txt_label_other, View.GONE);

        if (liveRoomBean.liveStatus == 1) {
            holdView.setVisibility(R.id.txt_label_other, View.VISIBLE);
            String content = TheLApp.context.getString(R.string.connecting_the_microphone);
            holdView.setText(R.id.txt_label_other, content);
        } else if (liveRoomBean.liveStatus == 2) {

            holdView.setVisibility(R.id.txt_label_other, View.VISIBLE);
            String content = TheLApp.context.getString(R.string.pk_now);
            holdView.setText(R.id.txt_label_other, content);
        }
        final String avatar = liveRoomBean.user != null ? liveRoomBean.user.avatar : "";
        holdView.setImageUrl(R.id.img_avatar, avatar, TheLConstants.AVATAR_SMALL_SIZE, TheLConstants.AVATAR_SMALL_SIZE);
        holdView.setOnClickListener(R.id.img_avatar, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoUserInfo(liveRoomBean);
            }
        });
    }

    private void gotoUserInfo(LiveRoomBean liveRoomBean) {
        if (liveRoomBean != null && liveRoomBean.user != null) {
            final String userId = liveRoomBean.user.id + "";
//            final Intent intent = new Intent(TheLApp.getContext(), UserInfoActivity.class);
//            final Bundle bundle = new Bundle();
//            bundle.putString(TheLConstants.BUNDLE_KEY_USER_ID, userId);
//            intent.putExtras(bundle);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            TheLApp.getContext().startActivity(intent);
            FlutterRouterConfig.Companion.gotoUserInfo(userId);
        }
    }
}
