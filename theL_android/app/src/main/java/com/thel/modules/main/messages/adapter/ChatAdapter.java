package com.thel.modules.main.messages.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;


import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.ksyun.media.player.KSYTextureView;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.message.MomentToChatBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.recommend.RecommendWebBean;
import com.thel.bean.user.UserCardBean;
import com.thel.constants.BundleConstants;
import com.thel.constants.QueueConstants;
import com.thel.constants.TheLConstants;
import com.thel.db.DBUtils;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.main.home.moments.web.WebViewActivity;
import com.thel.modules.main.messages.bean.MsgBean;
import com.thel.modules.main.messages.utils.MessagesUtils;
import com.thel.modules.others.VipConfigActivity;
import com.thel.ui.widget.ChatRecommendMomentView;
import com.thel.ui.widget.RecommendWebView;
import com.thel.ui.widget.UserCardView;
import com.thel.ui.widget.loadingIndicatorView.BallPulseIndicator;
import com.thel.utils.AppInit;
import com.thel.utils.DateUtils;
import com.thel.utils.EmojiUtils;
import com.thel.utils.GsonUtils;
import com.thel.utils.ImageUtils;
import com.thel.utils.JSONObjeatUtils;
import com.thel.utils.JsonUtils;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SizeUtils;
import com.thel.utils.StringUtils;
import com.thel.utils.UserUtils;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;

public class ChatAdapter extends BaseAdapter {

    private static final String TAG = "ChatAdapter";
    private final String pageId;

    private boolean isNormalMsg;
    private String toUserId;
    private List<MsgBean> msglist = new ArrayList<>();
    private LayoutInflater mInflater;
    private Context mContext;
    private int screenWidth;
    private String toAvatar; // 聊天对方的头像，不从消息数据list里取

    private Matcher urlMatcher = TheLConstants.URL_PATTERN.matcher("");

    // 每多少时间间隔加一个时间标识
    private int interval = 60 * 10 * 1000;

    private long oneDayInMillis = 24 * 60 * 60 * 1000;

    private boolean isFollow = false;

    private boolean isHasBG = false;

    public ChatAdapter(Context context, ArrayList<MsgBean> msglist, String toAvatar, String toUserId, boolean isNormalMsg, String pageId) {
        mInflater = (LayoutInflater) TheLApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mContext = context;
        this.toAvatar = toAvatar;
        screenWidth = AppInit.displayMetrics.widthPixels;// 屏幕宽度
        freshAdapter(msglist, toAvatar);
        this.toUserId = toUserId;
        this.isNormalMsg = isNormalMsg;
        this.pageId = pageId;
    }

    public void addData(MsgBean msgBean) {
        if (msglist != null) {
            msglist.add(msgBean);
            notifyDataSetChanged();
        }
    }

    public void isFollow(boolean isFollow) {
        this.isFollow = isFollow;
    }

    public void isHasBG(boolean isHasBG) {
        this.isHasBG = isHasBG;
    }

    public void freshAdapter(List<MsgBean> msglist, String toAvatar) {
        this.msglist.clear();
        this.msglist.addAll(msglist);
        this.toAvatar = toAvatar;

        if (this.msglist.size() > 0)
            try {
                SimpleDateFormat sdf0 = new SimpleDateFormat("MM-dd HH:mm");

                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(System.currentTimeMillis());

                Calendar calEndToday = (Calendar) cal.clone();
                calEndToday.set(Calendar.HOUR_OF_DAY, 23);
                calEndToday.set(Calendar.MINUTE, 59);
                calEndToday.set(Calendar.SECOND, 59);
                calEndToday.set(Calendar.MILLISECOND, 999);

                if (msglist.get(0).msgTime > calEndToday.getTimeInMillis() - oneDayInMillis && msglist.get(0).msgTime <= calEndToday.getTimeInMillis()) {// 今天的消息
                    sdf0 = new SimpleDateFormat("'" + TheLApp.getContext().getString(R.string.chat_activity_today) + "' HH:mm");
                } else if (msglist.get(0).msgTime > calEndToday.getTimeInMillis() - oneDayInMillis * 2 && msglist.get(0).msgTime <= calEndToday.getTimeInMillis() - oneDayInMillis) {// 昨天的消息
                    sdf0 = new SimpleDateFormat("'" + TheLApp.getContext().getString(R.string.chat_activity_yesterday) + "' HH:mm");
                }
                String time0 = sdf0.format(msglist.get(0).msgTime);
                long timeMillis0 = msglist.get(0).msgTime;
                msglist.get(0).date = time0;
                for (int i = 1; i < msglist.size(); i++) {
                    if (msglist.get(i).msgTime > timeMillis0 + interval) {
                        SimpleDateFormat sdf1 = new SimpleDateFormat("MM-dd HH:mm");
                        if (msglist.get(i).msgTime > calEndToday.getTimeInMillis() - oneDayInMillis && msglist.get(i).msgTime <= calEndToday.getTimeInMillis()) {// 今天的消息
                            sdf1 = new SimpleDateFormat("'" + TheLApp.getContext().getString(R.string.chat_activity_today) + "' HH:mm");
                        } else if (msglist.get(i).msgTime > calEndToday.getTimeInMillis() - oneDayInMillis * 2 && msglist.get(i).msgTime <= calEndToday.getTimeInMillis() - oneDayInMillis) {// 昨天的消息
                            sdf1 = new SimpleDateFormat("'" + TheLApp.getContext().getString(R.string.chat_activity_yesterday) + "' HH:mm");
                        } else if (msglist.get(i).msgTime <= calEndToday.getTimeInMillis() - oneDayInMillis * 2 && msglist.get(i).msgTime > calEndToday.getTimeInMillis() - oneDayInMillis * 7) {// 昨天之前一周以内的消息
                            int day = DateUtils.getDayOfWeek(msglist.get(i).msgTime);
                            switch (day) {
                                case Calendar.SUNDAY:
                                    sdf1 = new SimpleDateFormat("'" + TheLApp.getContext().getString(R.string.chat_activity_sun) + "' HH:mm");
                                    break;
                                case Calendar.MONDAY:
                                    sdf1 = new SimpleDateFormat("'" + TheLApp.getContext().getString(R.string.chat_activity_mon) + "' HH:mm");
                                    break;
                                case Calendar.TUESDAY:
                                    sdf1 = new SimpleDateFormat("'" + TheLApp.getContext().getString(R.string.chat_activity_tue) + "' HH:mm");
                                    break;
                                case Calendar.WEDNESDAY:
                                    sdf1 = new SimpleDateFormat("'" + TheLApp.getContext().getString(R.string.chat_activity_wed) + "' HH:mm");
                                    break;
                                case Calendar.THURSDAY:
                                    sdf1 = new SimpleDateFormat("'" + TheLApp.getContext().getString(R.string.chat_activity_thur) + "' HH:mm");
                                    break;
                                case Calendar.FRIDAY:
                                    sdf1 = new SimpleDateFormat("'" + TheLApp.getContext().getString(R.string.chat_activity_fri) + "' HH:mm");
                                    break;
                                case Calendar.SATURDAY:
                                    sdf1 = new SimpleDateFormat("'" + TheLApp.getContext().getString(R.string.chat_activity_sat) + "' HH:mm");
                                    break;
                                default:
                                    break;
                            }
                        }
                        String time1 = sdf1.format(msglist.get(i).msgTime);
                        msglist.get(i).date = time1;
                        timeMillis0 = msglist.get(i).msgTime;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }

    @Override
    public int getCount() {
        return msglist.size();
    }

    @Override
    public Object getItem(int position) {
        return msglist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void updateSingleRow(ListView listView, int position) {
        int firstVisiblePosition = listView.getFirstVisiblePosition();
        int lastVisiblePosition = listView.getLastVisiblePosition();
        if (position >= firstVisiblePosition && position <= lastVisiblePosition) {
            View view = listView.getChildAt(position - firstVisiblePosition);
            if (view.getTag() instanceof HoldView) {
                HoldView holdView = (HoldView) view.getTag();
                refreshItem(position - listView.getHeaderViewsCount(), holdView, false);
            }
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        HoldView holdView = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_chat, parent, false);
            holdView = new HoldView();
            holdView.root_ll = convertView.findViewById(R.id.root_ll);
            holdView.txt_day = convertView.findViewById(R.id.txt_day);
            holdView.lin_img_left = convertView.findViewById(R.id.lin_img_left);
            holdView.img_thumb_left = convertView.findViewById(R.id.img_thumb_left);
            holdView.lin_msg_text_left = convertView.findViewById(R.id.lin_msg_text_left);
            holdView.icon_type_left = convertView.findViewById(R.id.icon_type_left);
            holdView.txt_msg_text_left = convertView.findViewById(R.id.txt_msg_text_left);
            holdView.lin_msg_photo_left = convertView.findViewById(R.id.lin_msg_photo_left);
            holdView.img_msg_photo_left = convertView.findViewById(R.id.img_msg_photo_left);
            holdView.img_sticker_left = convertView.findViewById(R.id.img_sticker_left);
            holdView.txt_voice_time_left = convertView.findViewById(R.id.txt_voice_time_left);
            holdView.img_voice_left = convertView.findViewById(R.id.img_voice_left);
            holdView.lin_msg_voice_left = convertView.findViewById(R.id.lin_msg_voice_left); // 对方声音布局
            holdView.img_listened = convertView.findViewById(R.id.img_listened);
            holdView.img_reload_voice = convertView.findViewById(R.id.img_reload_voice);
            holdView.loading_voice_progress_bar = convertView.findViewById(R.id.loading_voice_progress_bar);
            holdView.lin_msg_voice_left_wrap = convertView.findViewById(R.id.lin_msg_voice_left_wrap);
            holdView.lin_msg_map_left = convertView.findViewById(R.id.lin_msg_map_left);
            holdView.img_msg_map_left = convertView.findViewById(R.id.img_msg_map_left);// 对方地理位置
            holdView.lin_match = convertView.findViewById(R.id.lin_match);
            holdView.img_match = convertView.findViewById(R.id.img_match);
            holdView.txt_match_date = convertView.findViewById(R.id.txt_match_date);
            holdView.txt_match_title = convertView.findViewById(R.id.txt_match_title);
            holdView.txt_title = convertView.findViewById(R.id.txt_title);
            holdView.txt_q1 = convertView.findViewById(R.id.txt_q1);
            holdView.txt_q2 = convertView.findViewById(R.id.txt_q2);
            holdView.txt_q3 = convertView.findViewById(R.id.txt_q3);
            holdView.lin_video_left = convertView.findViewById(R.id.lin_video_left);
            holdView.rel_video_left = convertView.findViewById(R.id.rel_video_left);
            holdView.img_cover_left = convertView.findViewById(R.id.img_cover_left);
            holdView.player_left = convertView.findViewById(R.id.player_left);
            holdView.progressbar_video_left = convertView.findViewById(R.id.progressbar_video_left);
            holdView.img_play_video_left = convertView.findViewById(R.id.img_play_video_left);
            holdView.lin_typing = convertView.findViewById(R.id.lin_typing);
            holdView.typing = convertView.findViewById(R.id.typing);
            holdView.user_card_view_left = convertView.findViewById(R.id.user_card_view_left);//我收到的名片
            holdView.img_wink_left = convertView.findViewById(R.id.img_wink_left);//3.0 挤眼信息右侧图标
            holdView.chatRecommendMomentView_left = convertView.findViewById(R.id.chat_recommend_momentview_left);//4.0.0我收到的推荐内容
            holdView.moment_text_left_tv = convertView.findViewById(R.id.moment_text_left_tv);
            holdView.moment_text_left_iv = convertView.findViewById(R.id.moment_text_left_iv);

            holdView.lin_img_right = convertView.findViewById(R.id.lin_img_right);
            holdView.lin_msg_text_right = convertView.findViewById(R.id.lin_msg_text_right);
            holdView.icon_type_right = convertView.findViewById(R.id.icon_type_right);
            holdView.txt_msg_text_right = convertView.findViewById(R.id.txt_msg_text_right);
            holdView.txt_name_left = convertView.findViewById(R.id.txt_name_left);
            holdView.img_thumb_right = convertView.findViewById(R.id.img_thumb_right);
            holdView.rel_msg_photo_right = convertView.findViewById(R.id.rel_msg_photo_right);
            holdView.img_msg_photo_right = convertView.findViewById(R.id.img_msg_photo_right);
            holdView.img_sticker_right = convertView.findViewById(R.id.img_sticker_right);
            holdView.img_type_right = convertView.findViewById(R.id.img_type_right);
            holdView.lin_msg_voice_right_wrap = convertView.findViewById(R.id.lin_msg_voice_right_wrap);
            holdView.lin_msg_voice_right = convertView.findViewById(R.id.lin_msg_voice_right);
            holdView.img_voice_right = convertView.findViewById(R.id.img_voice_right);
            holdView.txt_voice_time_right = convertView.findViewById(R.id.txt_voice_time_right);
            holdView.lin_msg_map_right = convertView.findViewById(R.id.lin_msg_map_right);
            holdView.img_msg_map_right = convertView.findViewById(R.id.img_msg_map_right);
            holdView.progressBar = convertView.findViewById(R.id.progress_bar);
            holdView.lin_video_right_wrap = convertView.findViewById(R.id.lin_video_right_wrap);
            holdView.rel_video_right = convertView.findViewById(R.id.rel_video_right);
            holdView.img_cover_right = convertView.findViewById(R.id.img_cover_right);
            holdView.player_right = convertView.findViewById(R.id.player_right);
            holdView.progressbar_video_right = convertView.findViewById(R.id.progressbar_video_right);
            holdView.img_play_video_right = convertView.findViewById(R.id.img_play_video_right);
            holdView.user_card_view_right = convertView.findViewById(R.id.user_card_view_right);//我发出的名片
            holdView.chatRecommendMomentView_right = convertView.findViewById(R.id.chat_recommend_momentview_right);//4.0.0我收到的推荐内容
            holdView.txt_warning = convertView.findViewById(R.id.txt_warning);
            holdView.chat_recommend_web_view_left = convertView.findViewById(R.id.chat_recommend_web_view_left);
            holdView.chat_recommend_web_view_right = convertView.findViewById(R.id.chat_recommend_web_view_right);
            holdView.moment_text_right_tv = convertView.findViewById(R.id.moment_text_right_tv);
            holdView.moment_text_right_iv = convertView.findViewById(R.id.moment_text_right_iv);
            holdView.send_status_tv = convertView.findViewById(R.id.send_status_tv);
            holdView.send_status = convertView.findViewById(R.id.send_status);
            holdView.match_new_rl = convertView.findViewById(R.id.match_new_rl);
            holdView.match_content_tv = convertView.findViewById(R.id.match_content_tv);
            holdView.match_left_iv = convertView.findViewById(R.id.match_left_iv);
            holdView.match_right_iv = convertView.findViewById(R.id.match_right_iv);
            holdView.renewal_fee_tv = convertView.findViewById(R.id.renewal_fee_tv);
            convertView.setTag(holdView);
        } else {
            holdView = (HoldView) convertView.getTag();
        }
        refreshItem(position, holdView, true);

        return convertView;
    }

    private void refreshItem(int position, HoldView holdView, boolean refreshAvatar) {
        // 处理数据显示

        if (position >= msglist.size()) {
            return;
        }

        final MsgBean tempMsgBean = msglist.get(position);

        L.d(TAG, " tempMsgBean type : " + tempMsgBean.msgType);

        L.d(TAG, " tempMsgBean msgText : " + tempMsgBean.msgText);


        try {
            tempMsgBean.msgText.replace("\\n", "").replaceAll("\\\\", "").replaceAll(" ", "");
        } catch (Exception e) {
            e.printStackTrace();
        }

        // 点击事件
        // 录音点击
        holdView.lin_msg_voice_right.setTag(R.id.lin_msg_voice_right, tempMsgBean);
        holdView.lin_msg_voice_right.setOnClickListener((OnClickListener) mContext);
        holdView.lin_msg_voice_right.setOnLongClickListener((View.OnLongClickListener) mContext);
        holdView.lin_msg_voice_left.setTag(R.id.lin_msg_voice_left, tempMsgBean);
        holdView.lin_msg_voice_left.setOnClickListener((OnClickListener) mContext);
        holdView.lin_msg_voice_left.setOnLongClickListener((View.OnLongClickListener) mContext);
        // 头像的点击
        holdView.img_thumb_right.setTag(R.id.img_thumb_right, tempMsgBean);
        holdView.img_thumb_right.setOnClickListener((OnClickListener) mContext);
        holdView.img_thumb_left.setTag(R.id.img_thumb_left, tempMsgBean);
        holdView.img_thumb_left.setOnClickListener((OnClickListener) mContext);
        // 地理位置点击进入地图
        holdView.img_msg_map_right.setTag(R.id.img_msg_map_right, tempMsgBean);
        holdView.img_msg_map_right.setOnClickListener((OnClickListener) mContext);
        holdView.img_msg_map_right.setOnLongClickListener((View.OnLongClickListener) mContext);
        holdView.img_msg_map_left.setTag(R.id.img_msg_map_left, tempMsgBean);
        holdView.img_msg_map_left.setOnClickListener((OnClickListener) mContext);
        holdView.img_msg_map_left.setOnLongClickListener((View.OnLongClickListener) mContext);
        // 点击图片
        holdView.img_msg_photo_right.setTag(R.id.img_msg_photo_right, tempMsgBean);
        holdView.img_msg_photo_right.setOnClickListener((OnClickListener) mContext);
        holdView.img_msg_photo_right.setOnLongClickListener((View.OnLongClickListener) mContext);
        holdView.img_msg_photo_left.setTag(R.id.img_msg_photo_left, tempMsgBean);
        holdView.img_msg_photo_left.setOnClickListener((OnClickListener) mContext);
        holdView.img_msg_photo_left.setOnLongClickListener((View.OnLongClickListener) mContext);

        // 点击贴图

        try {
            if (tempMsgBean.msgText != null) {
                holdView.img_sticker_right.setTag(R.id.img_sticker_right, tempMsgBean.msgText);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        holdView.img_sticker_right.setOnClickListener((OnClickListener) mContext);
        holdView.img_sticker_right.setOnLongClickListener((View.OnLongClickListener) mContext);
        holdView.img_sticker_left.setTag(R.id.img_sticker_left, tempMsgBean.msgText);
        holdView.img_sticker_left.setOnClickListener((OnClickListener) mContext);
        holdView.img_sticker_left.setOnLongClickListener((View.OnLongClickListener) mContext);
        // 发送失败重新发送
        holdView.img_type_right.setOnClickListener((OnClickListener) mContext);

        holdView.match_new_rl.setTag(R.id.match_new_rl, tempMsgBean);
        holdView.match_new_rl.setOnClickListener((OnClickListener) mContext);
        holdView.match_new_rl.setOnLongClickListener((View.OnLongClickListener) mContext);

        // 绿色的banner
        if (!TextUtils.isEmpty(tempMsgBean.date) && !tempMsgBean.msgType.equals(MsgBean.MSG_TYPE_TYPING)) {
            holdView.txt_day.setVisibility(View.VISIBLE);
            if (isHasBG) {
                holdView.txt_day.setTextColor(TheLApp.context.getResources().getColor(R.color.chat_time_text_bg));
                holdView.txt_day.setBackground(TheLApp.getContext().getResources().getDrawable(R.drawable.bg_border_corner_button_white_shape));
            } else {
                holdView.txt_day.setTextColor(TheLApp.context.getResources().getColor(R.color.theme_select_info));
                holdView.txt_day.setBackground(null);
            }
            String time = DateUtils.getFormatTimeFromMillis(tempMsgBean.msgTime, "MM-dd HH:mm");
            holdView.txt_day.setText(time);
        } else {
            holdView.txt_day.setVisibility(View.GONE);

        }

        holdView.moment_text_right_iv.setVisibility(View.GONE);
        holdView.moment_text_right_tv.setVisibility(View.GONE);
        holdView.moment_text_left_iv.setVisibility(View.GONE);
        holdView.moment_text_left_tv.setVisibility(View.GONE);
        holdView.txt_warning.setVisibility(View.GONE);
        holdView.lin_img_left.setVisibility(View.GONE);
        holdView.lin_img_right.setVisibility(View.GONE);
        holdView.send_status.setVisibility(View.GONE);
        holdView.renewal_fee_tv.setVisibility(View.GONE);
        /**4.0.0,陌生人警示语，类型为自己发出的消息**/

        if (isHasBG) {
            holdView.txt_warning.setBackground(TheLApp.getContext().getResources().getDrawable(R.drawable.bg_border_corner_button_white_shape));
            holdView.txt_warning.setTextColor(TheLApp.context.getResources().getColor(R.color.chat_time_text_bg));

        } else {
            holdView.txt_warning.setBackgroundResource(R.drawable.bg_warning_chat);
            holdView.txt_warning.setTextColor(TheLApp.context.getResources().getColor(R.color.white));
        }

        L.d(TAG, " toUserId : " + toUserId);

        if (MsgBean.MSG_TYPE_STRANGER_WARNING.equals(tempMsgBean.msgType)) {
            holdView.txt_warning.setVisibility(View.VISIBLE);
            holdView.txt_warning.setText(StringUtils.createStrangerMsgSpannableString(mContext, toUserId, TheLApp.getContext().getString(R.string.stranger_warning_tip), TheLApp.getContext().getString(R.string.stranger_block), TheLApp.getContext().getString(R.string.stranger_report)));
            holdView.txt_warning.setMovementMethod(LinkMovementMethod.getInstance());
        } else if (MsgBean.MSG_TYPE_MAY_CHEAT_MSG_TYPE.equals(tempMsgBean.msgType)) {
            holdView.txt_warning.setVisibility(View.VISIBLE);
            SpannableString spannableString = StringUtils.createStrangerMsgSpannableString(mContext, toUserId, TheLApp.getContext().getString(R.string.chat_cheat_tips), TheLApp.getContext().getString(R.string.stranger_block), TheLApp.getContext().getString(R.string.stranger_report));
            holdView.txt_warning.setText(spannableString);
            holdView.txt_warning.setMovementMethod(LinkMovementMethod.getInstance());
        } else if (MsgBean.MSG_TYPE_MAY_AD_MSG_TYPE.equals(tempMsgBean.msgType)) {
            holdView.txt_warning.setVisibility(View.VISIBLE);
            SpannableString spannableString = StringUtils.createStrangerMsgSpannableString(mContext, toUserId, TheLApp.getContext().getString(R.string.chat_ad_tips), TheLApp.getContext().getString(R.string.stranger_block), TheLApp.getContext().getString(R.string.stranger_report));
            holdView.txt_warning.setText(spannableString);
            holdView.txt_warning.setMovementMethod(LinkMovementMethod.getInstance());
        } else if (MsgBean.MSG_TYPE_MAY_SEXUAL_MSG_TYPE.equals(tempMsgBean.msgType)) {
            holdView.txt_warning.setVisibility(View.VISIBLE);
            SpannableString spannableString = StringUtils.createStrangerMsgSpannableString(mContext, toUserId, TheLApp.getContext().getString(R.string.chat_sexual_tips), TheLApp.getContext().getString(R.string.stranger_block), TheLApp.getContext().getString(R.string.stranger_report));
            holdView.txt_warning.setText(spannableString);
            holdView.txt_warning.setMovementMethod(LinkMovementMethod.getInstance());
        } else if (MsgBean.MSG_TYPE_MAY_HACKED_MSG_TYPE.equals(tempMsgBean.msgType)) {
            holdView.txt_warning.setVisibility(View.VISIBLE);
            SpannableString spannableString = StringUtils.createStrangerMsgSpannableString(mContext, toUserId, TheLApp.getContext().getString(R.string.chat_hacked_tips), TheLApp.getContext().getString(R.string.stranger_block), TheLApp.getContext().getString(R.string.stranger_report));
            holdView.txt_warning.setText(spannableString);
            holdView.txt_warning.setMovementMethod(LinkMovementMethod.getInstance());
        } else if (MsgBean.MSG_TYPE_RELATION_TIP.equals(tempMsgBean.msgType)) {//如果是好友状态改变消息
            holdView.txt_warning.setVisibility(View.VISIBLE);
            holdView.txt_warning.setText(tempMsgBean.msgText);
        } else if (MsgBean.MSG_TYPE_MATCH.equals(tempMsgBean.msgType)) {

            holdView.match_new_rl.setVisibility(View.VISIBLE);

            ImageLoaderManager.imageLoaderCircle(holdView.match_left_iv, R.mipmap.icon_user, UserUtils.getUserAvatar());

            ImageLoaderManager.imageLoaderCircle(holdView.match_right_iv, R.mipmap.icon_user, DBUtils.getMainProcessOtherAvatar(tempMsgBean));

            holdView.match_content_tv.setText(TheLApp.context.getResources().getString(R.string.xx_with_xx_match_success, UserUtils.getNickName(), DBUtils.getMainProcessOtherUserName(tempMsgBean)));

        } else if ("0".equals(tempMsgBean.msgDirection)) { // 发出的消息
            holdView.lin_img_left.setVisibility(View.GONE);
            holdView.lin_img_right.setVisibility(View.VISIBLE);
            if (refreshAvatar) {
                // 发出消息的头像
                String avatarUrl = ShareFileUtils.getString(ShareFileUtils.AVATAR, "");
                holdView.img_thumb_right.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(avatarUrl, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE)));
            }

            holdView.lin_msg_text_right.setVisibility(View.GONE);
            holdView.lin_msg_voice_right_wrap.setVisibility(View.GONE);
            holdView.rel_msg_photo_right.setVisibility(View.GONE);
            holdView.lin_msg_map_right.setVisibility(View.GONE);
            holdView.img_type_right.setVisibility(View.INVISIBLE);
            holdView.img_sticker_right.setVisibility(View.GONE);
            holdView.lin_video_right_wrap.setVisibility(View.GONE);
            holdView.icon_type_right.setVisibility(View.GONE);
            holdView.user_card_view_right.setVisibility(View.GONE);//我发出的名片消息
            holdView.chat_recommend_web_view_right.setVisibility(View.GONE);
            holdView.chatRecommendMomentView_right.setVisibility(View.GONE);
            holdView.match_new_rl.setVisibility(View.GONE);
            if (TextUtils.isEmpty(tempMsgBean.msgType)) {
                tempMsgBean.msgType = MsgBean.MSG_TYPE_TEXT;
            }
            if (tempMsgBean.msgType.equals(MsgBean.MSG_TYPE_TEXT)) { // 文字消息
                holdView.lin_msg_text_right.setVisibility(View.VISIBLE);
                holdView.txt_msg_text_right.setTextColor(TheLApp.context.getResources().getColor(R.color.white));
                final SpannableString spannableString = EmojiUtils.getInstace(EmojiUtils.DEFAULT_SIZE).getExpressionString(mContext, tempMsgBean.msgText);
                createUrlSpannableString(spannableString);
                holdView.txt_msg_text_right.setText(spannableString);
                holdView.txt_msg_text_right.setMovementMethod(LinkMovementMethod.getInstance());
            } else if (tempMsgBean.msgType.equals(MsgBean.MSG_TYPE_STICKER)) {// 大表情
                holdView.img_sticker_right.setVisibility(View.VISIBLE);
                holdView.img_sticker_right.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(MessagesUtils.getStickerUrl(tempMsgBean.msgText)))).build()).setAutoPlayAnimations(true).build());
                holdView.img_sticker_right.setTag(MessagesUtils.getStickerPackId(tempMsgBean.msgText));
                holdView.img_sticker_right.setOnClickListener((OnClickListener) mContext);
            } else if (tempMsgBean.msgType.equals(MsgBean.MSG_TYPE_IMAGE)) {
                holdView.rel_msg_photo_right.setVisibility(View.VISIBLE);
                holdView.img_msg_photo_right.setImageURI(Uri.parse(TheLConstants.FILE_PIC_URL + tempMsgBean.filePath));
            } else if (tempMsgBean.msgType.equals(MsgBean.MSG_TYPE_VOICE)) {
                holdView.lin_msg_voice_right_wrap.setVisibility(View.VISIBLE);
                holdView.txt_voice_time_right.setText(tempMsgBean.voiceTime + "\"");
                LinearLayout.LayoutParams para = (LinearLayout.LayoutParams) holdView.lin_msg_voice_right.getLayoutParams();
                para.height = android.widget.FrameLayout.LayoutParams.WRAP_CONTENT;
                para.width = (int) getVoiceLength(Integer.parseInt(tempMsgBean.voiceTime));
                holdView.lin_msg_voice_right.setLayoutParams(para);

                holdView.img_voice_right.setBackgroundResource(R.drawable.chat_voice_right_anim);
                AnimationDrawable animationDrawable = (AnimationDrawable) holdView.img_voice_right.getBackground();
                if (tempMsgBean.isPlaying == 1) {
                    animationDrawable.start();
                } else {
                    if (animationDrawable.isRunning()) {
                        animationDrawable.stop();
                        holdView.img_voice_right.setBackgroundResource(R.mipmap.icn_chat_voice_right_03);
                    }
                }
            } else if (MsgBean.MSG_TYPE_VIDEO.equals(tempMsgBean.msgType)) {// 发出的短视频
                final String[] paths = tempMsgBean.filePath.split(",");// 视频本地地址和预览图本地地址用逗号分隔保存的
                holdView.lin_video_right_wrap.setVisibility(View.VISIBLE);
                try {// 重绘后停止播放
                    holdView.player_right.setOnPreparedListener(null);
                    holdView.player_right.setOnCompletionListener(null);
                    holdView.player_right.seekTo(0);
                    if (holdView.player_right.isPlaying())
                        holdView.player_right.stop();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                holdView.img_cover_right.setVisibility(View.VISIBLE);
                holdView.img_play_video_right.setVisibility(View.VISIBLE);
                holdView.progressbar_video_right.setVisibility(View.GONE);
                holdView.player_right.setVisibility(View.INVISIBLE);
                holdView.img_cover_right.setImageURI(Uri.parse(TheLConstants.FILE_PIC_URL + paths[1]));
                holdView.rel_video_right.setOnClickListener((OnClickListener) mContext);
                holdView.rel_video_right.setOnLongClickListener((View.OnLongClickListener) mContext);
                holdView.rel_video_right.setTag(R.id.position, position);
                holdView.rel_video_right.setTag(R.id.rel_video_right, tempMsgBean);
            } else if (tempMsgBean.msgType.equals(MsgBean.MSG_TYPE_MAP)) {
                holdView.lin_msg_map_right.setVisibility(View.VISIBLE);
            } else if (tempMsgBean.msgType.equals(MsgBean.MSG_TYPE_SECRET_KEY)) {
                holdView.icon_type_right.setVisibility(View.VISIBLE);
                holdView.icon_type_right.setImageResource(R.mipmap.icn_chat_key);
                holdView.lin_msg_text_right.setVisibility(View.VISIBLE);
                holdView.txt_msg_text_right.setTextColor(ContextCompat.getColor(TheLApp.getContext(), R.color.white));
                holdView.txt_msg_text_right.setText(TheLApp.getContext().getString(R.string.message_text_secretKey));
            } else if (MsgBean.MSG_TYPE_HIDDEN_LOVE.equals(tempMsgBean.msgType)) {
                L.d("暗恋双方成功");
                holdView.lin_msg_text_right.setVisibility(View.VISIBLE);
                holdView.txt_msg_text_right.setTextColor(TheLApp.getContext().getResources().getColor(R.color.white));
                holdView.txt_msg_text_right.setText(TheLApp.getContext().getString(R.string.message_text_hiddenLove));
                holdView.icon_type_right.setVisibility(View.VISIBLE);
                holdView.icon_type_right.setImageResource(R.mipmap.icn_chat_love);
            } else if (tempMsgBean.msgType.equals(MsgBean.MSG_TYPE_SYSTEM)) {
                holdView.lin_msg_text_right.setVisibility(View.VISIBLE);
                holdView.txt_msg_text_right.setTextColor(ContextCompat.getColor(TheLApp.getContext(), R.color.white));
                holdView.txt_msg_text_right.setText(tempMsgBean.msgText);
                holdView.icon_type_right.setVisibility(View.VISIBLE);
                holdView.icon_type_right.setImageResource(R.mipmap.icn_chat_system);
                holdView.img_type_right.setTag("");
            } else if (MsgBean.MSG_TYPE_USER_CARD.equals(tempMsgBean.msgType)) {//发出的名片消息
                holdView.user_card_view_right.setVisibility(View.VISIBLE);

                final String txt = tempMsgBean.msgText;
                final UserCardBean userCardBean = GsonUtils.getObject(txt, UserCardBean.class);
                if (userCardBean != null) {

                    holdView.user_card_view_right.setTag(userCardBean.userId);
                    holdView.user_card_view_right.setOnClickListener((OnClickListener) mContext);
                    holdView.user_card_view_right.initView(userCardBean, true);
                }
            } else if (tempMsgBean.msgType.equals(MsgBean.MSG_TYPE_MOMENT_THEMEREPLY)) {
                holdView.chatRecommendMomentView_right.setVisibility(View.VISIBLE);

                String msgText = tempMsgBean.msgText;

                final MomentsBean momentsBean = JSONObjeatUtils.fromJson(msgText);
                if (momentsBean != null) {
                    holdView.chatRecommendMomentView_right.setTag(momentsBean.userId);
                    holdView.chatRecommendMomentView_right.initView(momentsBean, true, tempMsgBean.msgType);
                }

            } else if (tempMsgBean.msgType.equals(MsgBean.MSG_TYPE_MOMENT_THEME)) {
                holdView.chatRecommendMomentView_right.setVisibility(View.VISIBLE);

                String msgText = tempMsgBean.msgText;
                final MomentsBean momentsBean = JSONObjeatUtils.fromJson(msgText);
                if (momentsBean != null) {
                    holdView.chatRecommendMomentView_right.setTag(momentsBean.userId);
                    holdView.chatRecommendMomentView_right.initView(momentsBean, true, tempMsgBean.msgType);
                }

            } else if (tempMsgBean.msgType.equals(MsgBean.MSG_TYPE_MOMENT_TEXT)) {
                holdView.chatRecommendMomentView_right.setVisibility(View.VISIBLE);

                String msgText = tempMsgBean.msgText;
                final MomentsBean momentsBean = JSONObjeatUtils.fromJson(msgText);
                if (momentsBean != null) {
                    holdView.chatRecommendMomentView_right.setTag(momentsBean.userId);
                    holdView.chatRecommendMomentView_right.initView(momentsBean, true, tempMsgBean.msgType);
                }

            } else if (tempMsgBean.msgType.equals(MsgBean.MSG_TYPE_MOMENT_IMAGE) || tempMsgBean.msgType.equals(MsgBean.MSG_TYPE_MOMENT_AD)) {
                holdView.chatRecommendMomentView_right.setVisibility(View.VISIBLE);

                String msgText = tempMsgBean.msgText;
                final MomentsBean momentsBean = JSONObjeatUtils.fromJson(msgText);
                if (momentsBean != null) {
                    holdView.chatRecommendMomentView_right.setTag(momentsBean.userId);
                    holdView.chatRecommendMomentView_right.initView(momentsBean, true, tempMsgBean.msgType);
                }

            } else if (tempMsgBean.msgType.equals(MsgBean.MSG_TYPE_MOMENT_VIDEO)) {
                holdView.chatRecommendMomentView_right.setVisibility(View.VISIBLE);

                String msgText = tempMsgBean.msgText;
                final MomentsBean momentsBean = JSONObjeatUtils.fromJson(msgText);
                if (momentsBean != null) {
                    holdView.chatRecommendMomentView_right.setTag(momentsBean.userId);
                    holdView.chatRecommendMomentView_right.initView(momentsBean, true, tempMsgBean.msgType);
                }

            } else if (tempMsgBean.msgType.equals(MsgBean.MSG_TYPE_MOMENT_MUSIC)) {
                holdView.chatRecommendMomentView_right.setVisibility(View.VISIBLE);

                String msgText = tempMsgBean.msgText;
                final MomentsBean momentsBean = JSONObjeatUtils.fromJson(msgText);
                if (momentsBean != null) {
                    holdView.chatRecommendMomentView_right.setTag(momentsBean.userId);

                    holdView.chatRecommendMomentView_right.initView(momentsBean, true, tempMsgBean.msgType);
                }

            } else if (tempMsgBean.msgType.equals(MsgBean.MSG_TYPE_MOMENT_LIVE)) {
                holdView.chatRecommendMomentView_right.setVisibility(View.VISIBLE);

                String msgText = tempMsgBean.msgText;

                L.d(TAG, " msgText : " + msgText);

                final MomentsBean momentsBean = JSONObjeatUtils.fromJson(msgText);
                if (momentsBean != null) {
                    holdView.chatRecommendMomentView_right.setTag(momentsBean.userId);
                    holdView.chatRecommendMomentView_right.initView(momentsBean, true, tempMsgBean.msgType);
                }

            } else if (tempMsgBean.msgType.equals(MsgBean.MSG_TYPE_MOMENT_VOICE)) {
                holdView.chatRecommendMomentView_right.setVisibility(View.VISIBLE);

                String msgText = tempMsgBean.msgText;
                final MomentsBean momentsBean = JSONObjeatUtils.fromJson(msgText);
                if (momentsBean != null) {
                    holdView.chatRecommendMomentView_right.setTag(momentsBean.userId);
                    holdView.chatRecommendMomentView_right.initView(momentsBean, true, tempMsgBean.msgType);
                }

            } else if (tempMsgBean.msgType.equals(MsgBean.MSG_TYPE_MOMENT_USERCARD)) {
                holdView.chatRecommendMomentView_right.setVisibility(View.VISIBLE);

                String msgText = tempMsgBean.msgText;
                final MomentsBean momentsBean = JSONObjeatUtils.fromJson(msgText);
                if (momentsBean != null) {
                    holdView.chatRecommendMomentView_right.setTag(momentsBean.userId);

                    holdView.chatRecommendMomentView_right.initView(momentsBean, true, tempMsgBean.msgType);
                }

            } else if (MsgBean.MSG_TYPE_MOMENT_RECOMMEND.equals(tempMsgBean.msgType)) {
                holdView.chatRecommendMomentView_right.setVisibility(View.VISIBLE);

                String msgText = tempMsgBean.msgText;
                final MomentsBean momentsBean = JSONObjeatUtils.fromJson(msgText);
                if (momentsBean != null) {
                    holdView.chatRecommendMomentView_right.setTag(momentsBean.userId);

                    holdView.chatRecommendMomentView_right.initView(momentsBean, true, tempMsgBean.msgType);
                }

            } else if (MsgBean.MSG_TYPE_WEB.equals(tempMsgBean.msgType)) {
                holdView.chat_recommend_web_view_right.setVisibility(View.VISIBLE);
                final String msgText = tempMsgBean.msgText;
                final RecommendWebBean recommendWebBean = GsonUtils.getObject(msgText, RecommendWebBean.class);
                holdView.chat_recommend_web_view_right.initView(recommendWebBean);
            } else if (MsgBean.MSG_TYPE_MOMENT_WEB.equals(tempMsgBean.msgType)) {//网页日志
                holdView.chatRecommendMomentView_right.setVisibility(View.VISIBLE);
                String msgText = tempMsgBean.msgText;
                final MomentsBean momentsBean = JSONObjeatUtils.fromJson(msgText);
                if (momentsBean != null) {
                    holdView.chatRecommendMomentView_right.initView(momentsBean, true, tempMsgBean.msgType);
                }
            } else if (MsgBean.MSG_TYPE_FOLLOW.equals(tempMsgBean.msgType)) {//我发出的关注消息,4.0.1
                holdView.lin_msg_text_right.setVisibility(View.VISIBLE);
                holdView.txt_msg_text_right.setTextColor(TheLApp.getContext().getResources().getColor(R.color.white));
                holdView.txt_msg_text_right.setText(TheLApp.getContext().getString(R.string.message_text_follow));
                holdView.send_status_tv.setVisibility(View.GONE);
            } else if (MsgBean.MSG_TYPE_WINK.equals(tempMsgBean.msgType)) {//我发出的挤眼消息4.0.1
                holdView.lin_msg_text_right.setVisibility(View.VISIBLE);
                holdView.lin_msg_text_right.setVisibility(View.VISIBLE);
                String text = TheLApp.getContext().getString(R.string.wink_wink_msg_text);
                holdView.txt_msg_text_right.setTextColor(TheLApp.getContext().getResources().getColor(R.color.white));
                holdView.txt_msg_text_right.setText(text);
                holdView.send_status_tv.setVisibility(View.GONE);
            } else if (MsgBean.MSG_TYPE_MOMENT_TO_CHAT.equals(tempMsgBean.msgType)) {

                final MomentToChatBean momentToChatBean = GsonUtils.getObject(tempMsgBean.msgText, MomentToChatBean.class);

                holdView.lin_msg_text_right.setVisibility(View.VISIBLE);
                holdView.txt_msg_text_right.setTextColor(ContextCompat.getColor(TheLApp.getContext(), R.color.white));
                final SpannableString spannableString = EmojiUtils.getInstace(EmojiUtils.DEFAULT_SIZE).getExpressionString(mContext, momentToChatBean.chatText);
                createUrlSpannableString(spannableString);
                holdView.txt_msg_text_right.setText(spannableString);
                holdView.txt_msg_text_right.setMovementMethod(LinkMovementMethod.getInstance());

                if (momentToChatBean.imgUrl != null && momentToChatBean.imgUrl.length() > 0) {
                    holdView.moment_text_right_iv.setVisibility(View.VISIBLE);
                    holdView.moment_text_right_tv.setVisibility(View.GONE);
                    float radius = SizeUtils.dip2px(mContext, 5);
                    ImageLoaderManager.imageLoaderDefaultCorner(holdView.moment_text_right_iv, R.color.default_avatar_color, momentToChatBean.imgUrl, radius);
                    holdView.moment_text_right_iv.setOnClickListener(new OnMomentToChatClickListener(momentToChatBean));
                } else if (momentToChatBean.momentText != null && momentToChatBean.momentText.length() > 0) {
                    holdView.moment_text_right_iv.setVisibility(View.GONE);
                    holdView.moment_text_right_tv.setVisibility(View.VISIBLE);
                    holdView.moment_text_right_tv.setText(momentToChatBean.momentText);
                    holdView.moment_text_right_tv.setOnClickListener(new OnMomentToChatClickListener(momentToChatBean));
                } else {
                    holdView.moment_text_right_iv.setVisibility(View.GONE);
                    holdView.moment_text_right_tv.setVisibility(View.GONE);
                }

            } else {
                if (!TextUtils.isEmpty(tempMsgBean.msgType)) {// 当前版本不支持的消息类型
                    holdView.lin_msg_text_right.setVisibility(View.VISIBLE);
                    holdView.txt_msg_text_right.setTextColor(ContextCompat.getColor(TheLApp.getContext(), R.color.white));
                    holdView.txt_msg_text_right.setText(TheLApp.getContext().getString(R.string.info_version_outdate));
                }
            }

            if (isHasBG) {
                holdView.send_status_tv.setTextColor(TheLApp.context.getResources().getColor(R.color.chat_time_text_bg));
                holdView.send_status_tv.setBackground(TheLApp.getContext().getResources().getDrawable(R.drawable.bg_border_corner_button_white_shape));
            } else {
                holdView.send_status_tv.setTextColor(TheLApp.context.getResources().getColor(R.color.theme_select_info));
                holdView.send_status_tv.setBackground(null);
            }

            L.d(TAG, " tempMsgBean.msgStatus : " + tempMsgBean.msgStatus);

            if (tempMsgBean.msgStatus.equals(TheLConstants.MsgSendingStatusConstants.MSG_SUCCESS)) {
                holdView.send_status_tv.setVisibility(View.VISIBLE);
                holdView.progressBar.setVisibility(View.GONE);
                holdView.send_status_tv.setText(TheLApp.context.getResources().getString(R.string.unread));
            } else if (tempMsgBean.msgStatus.equals(TheLConstants.MsgSendingStatusConstants.MSG_SENDING) || tempMsgBean.msgStatus.equals(TheLConstants.MsgSendingStatusConstants.MSG_FAILED)) {
                holdView.send_status_tv.setVisibility(View.GONE);
                holdView.progressBar.setVisibility(View.VISIBLE);
            } else if (tempMsgBean.msgStatus.equals(TheLConstants.MsgSendingStatusConstants.MSG_READ)) {
                holdView.send_status_tv.setVisibility(View.VISIBLE);
                holdView.progressBar.setVisibility(View.GONE);
                holdView.send_status_tv.setText(TheLApp.context.getResources().getString(R.string.read));
            } else if (tempMsgBean.msgStatus.equals(TheLConstants.MsgSendingStatusConstants.MSG_ILLEGAL)) {
                holdView.progressBar.setVisibility(View.GONE);
                holdView.send_status_tv.setVisibility(View.GONE);
                holdView.send_status.setVisibility(View.VISIBLE);
                holdView.send_status.setImageResource(R.mipmap.btn_msg_failed_ban);
                holdView.txt_warning.setVisibility(View.VISIBLE);
                holdView.txt_warning.setText(TheLApp.context.getResources().getString(R.string.message_illegal));
            } else if (tempMsgBean.msgStatus.equals(TheLConstants.MsgSendingStatusConstants.MSG_CONFINE)) {
                holdView.progressBar.setVisibility(View.GONE);
                holdView.send_status_tv.setVisibility(View.GONE);
                holdView.send_status.setVisibility(View.VISIBLE);
                holdView.send_status.setImageResource(R.mipmap.btn_msg_failed_ban);
                holdView.txt_warning.setVisibility(View.VISIBLE);
                holdView.txt_warning.setText(TheLApp.context.getResources().getString(R.string.user_confine));
            } else if (tempMsgBean.msgStatus.equals(TheLConstants.MsgSendingStatusConstants.MSG_BLOCK)) {
                holdView.progressBar.setVisibility(View.GONE);
                holdView.send_status_tv.setVisibility(View.GONE);
                holdView.send_status.setVisibility(View.VISIBLE);
                holdView.send_status.setImageResource(R.mipmap.icn_error_red);
                holdView.txt_warning.setVisibility(View.VISIBLE);
                holdView.txt_warning.setText(TheLApp.context.getResources().getString(R.string.chat_activity_block_her));
            } else if (tempMsgBean.msgStatus.equals(TheLConstants.MsgSendingStatusConstants.MSG_BLACK)) {
                holdView.progressBar.setVisibility(View.GONE);
                holdView.send_status_tv.setVisibility(View.GONE);
                holdView.send_status.setVisibility(View.VISIBLE);
                holdView.send_status.setImageResource(R.mipmap.icn_error_red);
                holdView.txt_warning.setVisibility(View.VISIBLE);
                holdView.txt_warning.setText(TheLApp.context.getResources().getString(R.string.chat_activity_be_blocked));
            }

            if (tempMsgBean.msgType.equals(MsgBean.MSG_TYPE_WINK) || tempMsgBean.msgType.equals(MsgBean.MSG_TYPE_FOLLOW)) {
                holdView.send_status_tv.setVisibility(View.INVISIBLE);
            }
        } else { // 收到的消息

            holdView.lin_img_left.setVisibility(View.VISIBLE);
            holdView.lin_img_right.setVisibility(View.GONE);
            if (isFollow || tempMsgBean.isSystem == MsgBean.IS_WINK_MSG) {// 关注和挤眼消息要显示对方昵称
                holdView.txt_name_left.setVisibility(View.VISIBLE);
                holdView.txt_name_left.setText(tempMsgBean.fromNickname);
            } else {
                holdView.txt_name_left.setVisibility(View.GONE);
            }
            holdView.txt_msg_text_left.setText(tempMsgBean.msgText);
            if (refreshAvatar) {
                if (tempMsgBean.isSystem >= MsgBean.IS_SYSTEM_MSG || isFollow) {
                    holdView.img_thumb_left.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(toAvatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE)));// 收到消息的头像
                } else if (MsgBean.MSG_TYPE_LIKE.equals(tempMsgBean.msgType)) {// 配对消息，只有收到的消息
                    holdView.img_thumb_left.setImageURI(Uri.parse(TheLConstants.RES_PIC_URL + R.mipmap.icn_match));
                } else {
                    holdView.img_thumb_left.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(toAvatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE)));// 收到消息的头像
                }
            }

            holdView.lin_msg_text_left.setVisibility(View.GONE);
            holdView.lin_msg_voice_left.setVisibility(View.GONE);
            holdView.lin_msg_voice_left_wrap.setVisibility(View.GONE);
            holdView.lin_msg_photo_left.setVisibility(View.GONE);
            holdView.lin_msg_map_left.setVisibility(View.GONE);
            holdView.img_sticker_left.setVisibility(View.GONE);
            holdView.lin_match.setVisibility(View.GONE);
            holdView.lin_video_left.setVisibility(View.GONE);
            holdView.lin_typing.setVisibility(View.GONE);
            holdView.icon_type_left.setVisibility(View.GONE);
            holdView.user_card_view_left.setVisibility(View.GONE);//我收到的名片
            holdView.chatRecommendMomentView_left.setVisibility(View.GONE);//我收到的推荐消息
            holdView.chat_recommend_web_view_left.setVisibility(View.GONE);

            if (tempMsgBean.isTypingMsg) {
                holdView.lin_typing.setVisibility(View.VISIBLE);
                Drawable animation = holdView.typing.getBackground();
                if (animation == null) {
                    animation = new BallPulseIndicator();
                    ((BallPulseIndicator) animation).setColor(ContextCompat.getColor(TheLApp.getContext(), R.color.tab_normal));
                    ((BallPulseIndicator) animation).start();
                    holdView.typing.setBackgroundDrawable(animation);
                } else {
                    if (!((BallPulseIndicator) animation).isRunning()) {
                        ((BallPulseIndicator) animation).start();
                    }
                }
            } else if (tempMsgBean.msgType.equals(MsgBean.MSG_TYPE_TEXT) || tempMsgBean.msgType.equals(MsgBean.MSG_TYPE_LIKE)) { // 文字消息
                holdView.lin_msg_text_left.setVisibility(View.VISIBLE);
                holdView.txt_msg_text_left.setTextColor(TheLApp.getContext().getResources().getColor(R.color.text_color_light_black));
                final SpannableString spannableString = EmojiUtils.getInstace(EmojiUtils.DEFAULT_SIZE).getExpressionString(mContext, tempMsgBean.msgText);
                createUrlSpannableString(spannableString);
                holdView.txt_msg_text_left.setText(spannableString);
                holdView.txt_msg_text_left.setMovementMethod(LinkMovementMethod.getInstance());
            } else if (tempMsgBean.msgType.equals(MsgBean.MSG_TYPE_STICKER)) {
                holdView.img_sticker_left.setVisibility(View.VISIBLE);
                holdView.img_sticker_left.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(MessagesUtils.getStickerUrl(tempMsgBean.msgText)))).build()).setAutoPlayAnimations(true).build());
                holdView.img_sticker_left.setTag(MessagesUtils.getStickerPackId(tempMsgBean.msgText));
                holdView.img_sticker_left.setOnClickListener((OnClickListener) mContext);
            } else if (tempMsgBean.msgType.equals(MsgBean.MSG_TYPE_VOICE)) {
                holdView.lin_msg_voice_left.setVisibility(View.VISIBLE);
                holdView.lin_msg_voice_left_wrap.setVisibility(View.VISIBLE);
                holdView.txt_voice_time_left.setText(tempMsgBean.voiceTime + "\"");
                holdView.img_listened.setVisibility(View.GONE);
                holdView.img_reload_voice.setVisibility(View.GONE);
                holdView.loading_voice_progress_bar.setVisibility(View.GONE);
                if (tempMsgBean.hadPlayed == 0) {
                    holdView.img_listened.setVisibility(View.VISIBLE);
                } else if (tempMsgBean.hadPlayed == 2) {
                    holdView.img_reload_voice.setVisibility(View.VISIBLE);
                    holdView.img_reload_voice.setOnClickListener((OnClickListener) mContext);
                    holdView.img_reload_voice.setTag(R.id.img_reload_voice, tempMsgBean);
                } else if (tempMsgBean.hadPlayed == 3) {
                    holdView.loading_voice_progress_bar.setVisibility(View.VISIBLE);
                }

                LinearLayout.LayoutParams para = (LinearLayout.LayoutParams) holdView.lin_msg_voice_left.getLayoutParams();
                para.height = LinearLayout.LayoutParams.WRAP_CONTENT;
                para.width = (int) getVoiceLength(Integer.parseInt(tempMsgBean.voiceTime));
                holdView.lin_msg_voice_left.setLayoutParams(para);
                // 语音
                holdView.img_voice_left.setBackgroundResource(R.drawable.chat_voice_left_anim);
                AnimationDrawable animationDrawable = (AnimationDrawable) holdView.img_voice_left.getBackground();
                if (tempMsgBean.isPlaying == 1) {
                    animationDrawable.start();
                } else {
                    if (animationDrawable.isRunning()) {
                        animationDrawable.stop();
                        holdView.img_voice_left.setBackgroundResource(R.mipmap.icn_chat_voice_left_03);
                    }
                }
            } else if (MsgBean.MSG_TYPE_VIDEO.equals(tempMsgBean.msgType)) {// 收到的短视频
                final MsgBean.MsgVideoBean msgVideoBean = GsonUtils.getObject(tempMsgBean.msgText, MsgBean.MsgVideoBean.class);
                holdView.lin_video_left.setVisibility(View.VISIBLE);
                try {// 重绘后停止播放
                    holdView.player_left.setOnPreparedListener(null);
                    holdView.player_left.setOnCompletionListener(null);
                    holdView.player_left.seekTo(0);
                    if (holdView.player_left.isPlaying())
                        holdView.player_left.stop();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                holdView.img_cover_left.setVisibility(View.VISIBLE);
                holdView.img_play_video_left.setVisibility(View.VISIBLE);
                holdView.progressbar_video_left.setVisibility(View.GONE);
                holdView.player_left.setVisibility(View.INVISIBLE);
                holdView.img_cover_left.setImageURI(msgVideoBean.thumbnail);
                holdView.rel_video_left.setOnClickListener((OnClickListener) mContext);
                holdView.rel_video_left.setOnLongClickListener((View.OnLongClickListener) mContext);
                holdView.rel_video_left.setTag(R.id.position, position);
                holdView.rel_video_left.setTag(R.id.rel_video_left, tempMsgBean);
            } else if (tempMsgBean.msgType.equals(MsgBean.MSG_TYPE_IMAGE)) {
                holdView.lin_msg_photo_left.setVisibility(View.VISIBLE);
                holdView.img_msg_photo_left.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(tempMsgBean.filePath, TheLApp.getContext().getResources().getDimension(R.dimen.chat_pic_thumbnail_width), TheLApp.getContext().getResources().getDimension(R.dimen.chat_pic_thumbnail_height))));
            } else if (tempMsgBean.msgType.equals(MsgBean.MSG_TYPE_MAP)) {
                holdView.lin_msg_map_left.setVisibility(View.VISIBLE);
            } else if (tempMsgBean.msgType.equals(MsgBean.MSG_TYPE_SECRET_KEY)) {
                holdView.icon_type_left.setVisibility(View.VISIBLE);
                holdView.icon_type_left.setImageResource(R.mipmap.icn_chat_key);
                holdView.lin_msg_text_left.setVisibility(View.VISIBLE);
                holdView.txt_msg_text_left.setTextColor(TheLApp.getContext().getResources().getColor(R.color.text_color_light_black));
                holdView.txt_msg_text_left.setText(TheLApp.getContext().getString(R.string.message_text_secretKey));
            } else if (MsgBean.MSG_TYPE_HIDDEN_LOVE.equals(tempMsgBean.msgType)) {
                holdView.lin_msg_text_left.setVisibility(View.VISIBLE);
                holdView.txt_msg_text_left.setTextColor(TheLApp.getContext().getResources().getColor(R.color.text_color_light_black));
                holdView.txt_msg_text_left.setText(TheLApp.getContext().getString(R.string.message_text_hiddenLove));
                holdView.icon_type_left.setVisibility(View.VISIBLE);
                holdView.icon_type_left.setImageResource(R.mipmap.icn_chat_love);
            } else if (tempMsgBean.msgType.equals(MsgBean.MSG_TYPE_WINK)) {//挤眼
                holdView.lin_msg_text_left.setVisibility(View.VISIBLE);
                holdView.txt_msg_text_left.setTextColor(TheLApp.getContext().getResources().getColor(R.color.text_color_light_black));
                String text = TheLApp.getContext().getString(R.string.wink_wink_msg_text);
                holdView.send_status_tv.setVisibility(View.GONE);
                if (QueueConstants.WINK_TYPE_KISS.equals(tempMsgBean.msgText)) {
                    text = TheLApp.getContext().getString(R.string.wink_kiss_msg_text);
                } else if (QueueConstants.WINK_TYPE_DIAMOND.equals(tempMsgBean.msgText)) {
                    text = TheLApp.getContext().getString(R.string.wink_diamond_msg_text);
                } else if (QueueConstants.WINK_TYPE_FLOWER.equals(tempMsgBean.msgText)) {
                    text = TheLApp.getContext().getString(R.string.wink_flower_msg_text);
                } else if (QueueConstants.WINK_TYPE_LOVE.equals(tempMsgBean.msgText)) {
                    text = TheLApp.getContext().getString(R.string.wink_love_msg_text);
                }
                holdView.txt_msg_text_left.setText(text);
            } else if (tempMsgBean.msgType.equals(MsgBean.MSG_TYPE_FOLLOW)) {
                holdView.img_thumb_left.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(DBUtils.getMainProcessOtherAvatar(tempMsgBean), TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE)));// 收到消息的头像
                holdView.lin_msg_text_left.setVisibility(View.VISIBLE);
                holdView.txt_msg_text_left.setTextColor(TheLApp.getContext().getResources().getColor(R.color.text_color_light_black));
                holdView.txt_msg_text_left.setText(TheLApp.getContext().getString(R.string.message_text_follow));
            } else if (tempMsgBean.msgType.equals(MsgBean.MSG_TYPE_UNFOLLOW)) {
                holdView.lin_msg_text_left.setVisibility(View.VISIBLE);
                holdView.txt_msg_text_left.setTextColor(TheLApp.getContext().getResources().getColor(R.color.text_color_red));
                holdView.txt_msg_text_left.setText(TheLApp.getContext().getString(R.string.message_text_unfollow));
            } else if (tempMsgBean.msgType.equals(MsgBean.MSG_TYPE_SYSTEM)) {
                holdView.lin_msg_text_left.setVisibility(View.VISIBLE);
                holdView.txt_msg_text_left.setTextColor(TheLApp.getContext().getResources().getColor(R.color.text_color_light_black));
                holdView.txt_msg_text_left.setText(tempMsgBean.msgText);
                holdView.icon_type_left.setVisibility(View.VISIBLE);
                holdView.icon_type_left.setImageResource(R.mipmap.icn_chat_system);
            } else if (MsgBean.MSG_TYPE_GIFT.equals(tempMsgBean.msgType)) {// vip礼物卡片
                holdView.lin_match.setVisibility(View.VISIBLE);

                holdView.txt_title.setVisibility(View.GONE);
                holdView.txt_q1.setVisibility(View.GONE);
                holdView.txt_q2.setVisibility(View.GONE);
                holdView.txt_q3.setVisibility(View.GONE);
                holdView.txt_match_title.setTextColor(TheLApp.getContext().getResources().getColor(R.color.text_color_black));
                holdView.txt_match_date.setTextColor(TheLApp.getContext().getResources().getColor(R.color.text_color_gray));
                holdView.txt_title.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                holdView.txt_q1.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
                try {
                    JSONObject jsonObject = new JSONObject(tempMsgBean.msgText);
                    holdView.txt_match_date.setText(JsonUtils.getString(jsonObject, "date", ""));
                    String noteBody = JsonUtils.getString(jsonObject, "noteBody", "");
                    holdView.img_match.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(JsonUtils.getString(jsonObject, "img", ""), TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE))).build()).setAutoPlayAnimations(true).build());
                    holdView.img_match.getHierarchy().setPlaceholderImage(R.color.transparent);
                    holdView.txt_match_title.setText(JsonUtils.getString(jsonObject, "description", ""));
                    if (!TextUtils.isEmpty(noteBody)) {
                        holdView.txt_title.setVisibility(View.VISIBLE);
                        holdView.txt_title.setText(TheLApp.getContext().getString(R.string.message_info_vip_gift_title, tempMsgBean.fromNickname));
                        holdView.txt_q1.setVisibility(View.VISIBLE);
                        holdView.txt_q1.setText(noteBody);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (MsgBean.MSG_TYPE_MATCH.equals(tempMsgBean.msgType)) {// 匹配成功消息，带三个问题
                holdView.lin_match.setVisibility(View.VISIBLE);
                holdView.txt_match_date.setText(DateUtils.getFormatTimeFromMillis(tempMsgBean.msgTime, "yyyy.MM.dd"));
                holdView.txt_title.setVisibility(View.GONE);
                holdView.txt_q1.setVisibility(View.GONE);
                holdView.txt_q2.setVisibility(View.GONE);
                holdView.txt_q3.setVisibility(View.GONE);
                holdView.txt_match_title.setTextColor(TheLApp.getContext().getResources().getColor(R.color.roseo));
                holdView.txt_match_date.setTextColor(TheLApp.getContext().getResources().getColor(R.color.roseo_light));
                holdView.txt_title.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
                holdView.txt_q1.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                holdView.img_match.getHierarchy().setPlaceholderImage(R.mipmap.icn_message_match_icon);
                holdView.img_match.setController(null);
                try {
                    JSONObject jsonObject = new JSONObject(tempMsgBean.msgText);
                    JSONObject questions = new JSONObject(JsonUtils.getString(jsonObject, "questions", ""));
                    String q1 = JsonUtils.getString(questions, "question1", "");
                    String q2 = JsonUtils.getString(questions, "question2", "");
                    String q3 = JsonUtils.getString(questions, "question3", "");
                    int i = 0;
                    if (!TextUtils.isEmpty(q1)) {
                        i++;
                        holdView.txt_q1.setVisibility(View.VISIBLE);
                        holdView.txt_q1.setText(q1);
                    }
                    if (!TextUtils.isEmpty(q2)) {
                        i++;
                        holdView.txt_q2.setVisibility(View.VISIBLE);
                        holdView.txt_q2.setText(q2);
                    }
                    if (!TextUtils.isEmpty(q3)) {
                        i++;
                        holdView.txt_q3.setVisibility(View.VISIBLE);
                        holdView.txt_q3.setText(q3);
                    }
                    if (i > 0) {
                        holdView.txt_title.setVisibility(View.VISIBLE);
                        if (i == 1) {
                            holdView.txt_title.setText(String.format(TheLApp.getContext().getString(R.string.message_text_match_title_odd), tempMsgBean.fromNickname, i + ""));
                        } else {
                            holdView.txt_title.setText(String.format(TheLApp.getContext().getString(R.string.message_text_match_title_even), tempMsgBean.fromNickname, i + ""));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (MsgBean.MSG_TYPE_USER_CARD.equals(tempMsgBean.msgType)) {//收到的名片消息

                holdView.user_card_view_left.setVisibility(View.VISIBLE);
                final String txt = tempMsgBean.msgText;
                final UserCardBean userCardBean = GsonUtils.getObject(txt, UserCardBean.class);
                if (userCardBean != null) {
                    holdView.user_card_view_left.setTag(userCardBean.userId);
                    holdView.user_card_view_left.setOnClickListener((OnClickListener) mContext);
                    holdView.user_card_view_left.initView(userCardBean, false);//收到名片，isMySelf=false;
                }
            }
            /**4.0.0推荐日志**/
            else if (tempMsgBean.msgType.equals(MsgBean.MSG_TYPE_MOMENT_THEMEREPLY)) {
                L.d("moment_theme", tempMsgBean.msgType);
                holdView.progressBar.setVisibility(View.GONE);
                holdView.chatRecommendMomentView_left.setVisibility(View.VISIBLE);

                String msgText = tempMsgBean.msgText;
                final MomentsBean momentsBean = JSONObjeatUtils.fromJson(msgText);
                if (momentsBean != null) {
                    holdView.chatRecommendMomentView_left.setTag(momentsBean.userId);
                    holdView.chatRecommendMomentView_left.initView(momentsBean, false, tempMsgBean.msgType);
                }

            } else if (tempMsgBean.msgType.equals(MsgBean.MSG_TYPE_MOMENT_THEME)) {

                holdView.progressBar.setVisibility(View.GONE);
                holdView.chatRecommendMomentView_left.setVisibility(View.VISIBLE);

                String msgText = tempMsgBean.msgText;
                final MomentsBean momentsBean = JSONObjeatUtils.fromJson(msgText);
                if (momentsBean != null) {
                    holdView.chatRecommendMomentView_left.setTag(momentsBean.userId);
                    holdView.chatRecommendMomentView_left.initView(momentsBean, false, tempMsgBean.msgType);
                }

            } else if (tempMsgBean.msgType.equals(MsgBean.MSG_TYPE_MOMENT_THEME)) {

                holdView.progressBar.setVisibility(View.GONE);
                holdView.chatRecommendMomentView_left.setVisibility(View.VISIBLE);

                String msgText = tempMsgBean.msgText;
                final MomentsBean momentsBean = JSONObjeatUtils.fromJson(msgText);
                if (momentsBean != null) {
                    holdView.chatRecommendMomentView_left.setTag(momentsBean.userId);
                    holdView.chatRecommendMomentView_left.initView(momentsBean, false, tempMsgBean.msgType);
                }

            } else if (tempMsgBean.msgType.equals(MsgBean.MSG_TYPE_MOMENT_TEXT)) {
                holdView.progressBar.setVisibility(View.GONE);
                holdView.chatRecommendMomentView_left.setVisibility(View.VISIBLE);

                String msgText = tempMsgBean.msgText;
                final MomentsBean momentsBean = JSONObjeatUtils.fromJson(msgText);
                if (momentsBean != null) {
                    holdView.chatRecommendMomentView_left.setTag(momentsBean.userId);
                    holdView.chatRecommendMomentView_left.initView(momentsBean, false, tempMsgBean.msgType);
                }

            } else if (tempMsgBean.msgType.equals(MsgBean.MSG_TYPE_MOMENT_IMAGE)) {
                holdView.progressBar.setVisibility(View.GONE);
                holdView.chatRecommendMomentView_left.setVisibility(View.VISIBLE);

                String msgText = tempMsgBean.msgText;
                final MomentsBean momentsBean = JSONObjeatUtils.fromJson(msgText);
                if (momentsBean != null) {
                    holdView.chatRecommendMomentView_left.setTag(momentsBean.userId);
                    holdView.chatRecommendMomentView_left.initView(momentsBean, false, tempMsgBean.msgType);
                }

            } else if (tempMsgBean.msgType.equals(MsgBean.MSG_TYPE_MOMENT_VIDEO)) {
                holdView.progressBar.setVisibility(View.GONE);
                holdView.chatRecommendMomentView_left.setVisibility(View.VISIBLE);

                String msgText = tempMsgBean.msgText;
                final MomentsBean momentsBean = JSONObjeatUtils.fromJson(msgText);
                if (momentsBean != null) {
                    holdView.chatRecommendMomentView_left.setTag(momentsBean.userId);
                    holdView.chatRecommendMomentView_left.initView(momentsBean, false, tempMsgBean.msgType);
                }

            } else if (tempMsgBean.msgType.equals(MsgBean.MSG_TYPE_MOMENT_MUSIC)) {
                holdView.progressBar.setVisibility(View.GONE);
                holdView.chatRecommendMomentView_left.setVisibility(View.VISIBLE);

                String msgText = tempMsgBean.msgText;
                final MomentsBean momentsBean = JSONObjeatUtils.fromJson(msgText);
                if (momentsBean != null) {
                    holdView.chatRecommendMomentView_left.setTag(momentsBean.userId);
                    holdView.chatRecommendMomentView_left.initView(momentsBean, false, tempMsgBean.msgType);
                }

            } else if (tempMsgBean.msgType.equals(MsgBean.MSG_TYPE_MOMENT_LIVE)) {
                holdView.progressBar.setVisibility(View.GONE);
                holdView.chatRecommendMomentView_left.setVisibility(View.VISIBLE);

                String msgText = tempMsgBean.msgText;
                final MomentsBean momentsBean = JSONObjeatUtils.fromJson(msgText);
                if (momentsBean != null) {
                    holdView.chatRecommendMomentView_left.setTag(momentsBean.userId);
                    holdView.chatRecommendMomentView_left.initView(momentsBean, false, tempMsgBean.msgType);
                }

            } else if (tempMsgBean.msgType.equals(MsgBean.MSG_TYPE_MOMENT_VOICE)) {
                holdView.progressBar.setVisibility(View.GONE);
                holdView.chatRecommendMomentView_left.setVisibility(View.VISIBLE);

                String msgText = tempMsgBean.msgText;
                final MomentsBean momentsBean = JSONObjeatUtils.fromJson(msgText);
                if (momentsBean != null) {
                    holdView.chatRecommendMomentView_left.setTag(momentsBean.userId);
                    holdView.chatRecommendMomentView_left.initView(momentsBean, false, tempMsgBean.msgType);
                }

            } else if (tempMsgBean.msgType.equals(MsgBean.MSG_TYPE_MOMENT_USERCARD)) {
                holdView.progressBar.setVisibility(View.GONE);
                holdView.chatRecommendMomentView_left.setVisibility(View.VISIBLE);

                String msgText = tempMsgBean.msgText;
                final MomentsBean momentsBean = JSONObjeatUtils.fromJson(msgText);
                if (momentsBean != null) {
                    holdView.chatRecommendMomentView_left.setTag(momentsBean.userId);
                    holdView.chatRecommendMomentView_left.initView(momentsBean, false, tempMsgBean.msgType);
                }

            } else if (tempMsgBean.msgType.equals(MsgBean.MSG_TYPE_MOMENT_RECOMMEND)) {
                holdView.progressBar.setVisibility(View.GONE);
                holdView.chatRecommendMomentView_left.setVisibility(View.VISIBLE);

                String msgText = tempMsgBean.msgText;
                final MomentsBean momentsBean = JSONObjeatUtils.fromJson(msgText);
                if (momentsBean != null) {
                    holdView.chatRecommendMomentView_left.setTag(momentsBean.userId);
                    holdView.chatRecommendMomentView_left.initView(momentsBean, false, tempMsgBean.msgType);
                }
            } else if (MsgBean.MSG_TYPE_MOMENT_WEB.equals(tempMsgBean.msgType)) {//网页日志
                holdView.progressBar.setVisibility(View.GONE);
                holdView.chatRecommendMomentView_left.setVisibility(View.VISIBLE);
                String msgText = tempMsgBean.msgText;
                final MomentsBean momentsBean = JSONObjeatUtils.fromJson(msgText);
                holdView.chatRecommendMomentView_left.initView(momentsBean, false, tempMsgBean.msgType);
            } else if (MsgBean.MSG_TYPE_WEB.equals(tempMsgBean.msgType)) {
                holdView.progressBar.setVisibility(View.GONE);
                holdView.chat_recommend_web_view_left.setVisibility(View.VISIBLE);
                final String msgText = tempMsgBean.msgText;
                final RecommendWebBean recommendWebBean = GsonUtils.getObject(msgText, RecommendWebBean.class);
                L.d("recommendWebBean left:" + recommendWebBean);
                holdView.chat_recommend_web_view_left.initView(recommendWebBean);
            } else if (MsgBean.MSG_TYPE_MOMENT_TO_CHAT.equals(tempMsgBean.msgType)) {

                final MomentToChatBean momentToChatBean = GsonUtils.getObject(tempMsgBean.msgText, MomentToChatBean.class);

                holdView.lin_msg_text_left.setVisibility(View.VISIBLE);
                holdView.txt_msg_text_left.setTextColor(TheLApp.getContext().getResources().getColor(R.color.text_color_light_black));
                final SpannableString spannableString = EmojiUtils.getInstace(EmojiUtils.DEFAULT_SIZE).getExpressionString(mContext, momentToChatBean.chatText);
                createUrlSpannableString(spannableString);
                holdView.txt_msg_text_left.setText(spannableString);
                holdView.txt_msg_text_left.setMovementMethod(LinkMovementMethod.getInstance());

                if (momentToChatBean.imgUrl != null && momentToChatBean.imgUrl.length() > 0) {
                    holdView.moment_text_left_iv.setVisibility(View.VISIBLE);
                    holdView.moment_text_left_tv.setVisibility(View.GONE);
                    float radius = SizeUtils.dip2px(mContext, 5);
                    ImageLoaderManager.imageLoaderDefaultCorner(holdView.moment_text_left_iv, R.color.default_avatar_color, momentToChatBean.imgUrl, radius);
                    holdView.moment_text_left_iv.setOnClickListener(new OnMomentToChatClickListener(momentToChatBean));
                } else if (momentToChatBean.momentText != null && momentToChatBean.momentText.length() > 0) {
                    holdView.moment_text_left_iv.setVisibility(View.GONE);
                    holdView.moment_text_left_tv.setVisibility(View.VISIBLE);
                    holdView.moment_text_left_tv.setText(momentToChatBean.momentText);
                    holdView.moment_text_left_tv.setOnClickListener(new OnMomentToChatClickListener(momentToChatBean));
                } else {
                    holdView.moment_text_left_iv.setVisibility(View.GONE);
                    holdView.moment_text_left_tv.setVisibility(View.GONE);
                }

                if (momentToChatBean.momentText != null && momentToChatBean.momentText.length() > 0) {
                    holdView.moment_text_left_tv.setText(momentToChatBean.momentText);
                }

            } else if (MsgBean.MSG_TYPE_VIP_PUSH.equals(tempMsgBean.msgType)) {

                holdView.lin_msg_text_left.setVisibility(View.VISIBLE);
                holdView.txt_msg_text_left.setText(tempMsgBean.msgText);
                holdView.renewal_fee_tv.setVisibility(View.VISIBLE);

            } else {
                if (!TextUtils.isEmpty(tempMsgBean.msgType)) {// 当前版本不支持的消息类型
                    holdView.lin_msg_text_left.setVisibility(View.VISIBLE);
                    holdView.txt_msg_text_left.setTextColor(TheLApp.getContext().getResources().getColor(R.color.text_color_light_black));
                    holdView.txt_msg_text_left.setText(TheLApp.getContext().getString(R.string.info_version_outdate));
                }
            }
        }

        holdView.renewal_fee_tv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent vipConfigIntent = new Intent(v.getContext(), VipConfigActivity.class);
                vipConfigIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                v.getContext().startActivity(vipConfigIntent);
            }
        });

    }

    class HoldView {

        LinearLayout root_ll;

        TextView txt_day; // 日期

        LinearLayout lin_img_left;
        SimpleDraweeView img_thumb_left; // 对方头像
        LinearLayout lin_msg_text_left;
        ImageView icon_type_left;
        TextView txt_msg_text_left; // 对方内容
        LinearLayout lin_msg_photo_left;
        SimpleDraweeView img_msg_photo_left; // 对方图片
        SimpleDraweeView img_sticker_left; // 对方动图
        LinearLayout lin_msg_voice_left; // 对方声音布局
        ImageView img_voice_left;
        TextView txt_voice_time_left;// 对方声音时长文字
        LinearLayout lin_msg_voice_left_wrap; // 对方语音外布局
        ImageView img_listened; // 对方语音是否听过标记
        ImageView img_reload_voice; // 语音下载失败
        ProgressBar loading_voice_progress_bar; // 正在下载语音图标
        LinearLayout lin_msg_map_left;
        ImageView img_msg_map_left; // 对方地理位置
        LinearLayout lin_match; // 匹配消息区域
        SimpleDraweeView img_match;
        TextView txt_title;
        TextView txt_match_date;
        TextView txt_match_title;
        TextView txt_q1;
        TextView txt_q2;
        TextView txt_q3;
        LinearLayout lin_video_left;
        RelativeLayout rel_video_left;
        SimpleDraweeView img_cover_left;
        KSYTextureView player_left;
        ProgressBar progressbar_video_left;
        ImageView img_play_video_left;
        LinearLayout lin_typing;
        ImageView typing;
        TextView moment_text_left_tv;
        ImageView moment_text_left_iv;
        TextView renewal_fee_tv;

        LinearLayout lin_img_right;
        TextView txt_name_left;
        SimpleDraweeView img_thumb_right; // 我头像
        LinearLayout lin_msg_text_right;
        ImageView icon_type_right;
        TextView txt_msg_text_right; // 我内容
        RelativeLayout rel_msg_photo_right;
        SimpleDraweeView img_msg_photo_right; // 我图片
        SimpleDraweeView img_sticker_right; // 我动图
        LinearLayout lin_msg_voice_right_wrap;
        LinearLayout lin_msg_voice_right; // 我声音布局
        ImageView img_voice_right; // 我声音
        TextView txt_voice_time_right; // 我声音时长文字
        LinearLayout lin_msg_map_right;
        ImageView img_msg_map_right;// 我地理位置
        ImageView img_type_right; // 我消息类型
        ProgressBar progressBar;// 正在发送
        LinearLayout lin_video_right_wrap;
        RelativeLayout rel_video_right;
        SimpleDraweeView img_cover_right;
        KSYTextureView player_right;
        ProgressBar progressbar_video_right;
        ImageView img_play_video_right;
        UserCardView user_card_view_right;//我发出的名片消息 3.0.0
        UserCardView user_card_view_left;//我接受到的名片消息 3.0.0

        ImageView img_wink_left;//3.0 挤眼消息图标
        ChatRecommendMomentView chatRecommendMomentView_left;//对方发出的推荐内容4.0.0
        ChatRecommendMomentView chatRecommendMomentView_right;
        TextView txt_warning;//4.0.0 警告用语
        RecommendWebView chat_recommend_web_view_left;//4.0.0 推荐网页日志
        RecommendWebView chat_recommend_web_view_right;

        TextView moment_text_right_tv;
        ImageView moment_text_right_iv;
        TextView send_status_tv;
        ImageView send_status;

        RelativeLayout match_new_rl;
        ImageView match_left_iv;
        ImageView match_right_iv;
        TextView match_content_tv;

    }

    private double getVoiceLength(int voiceTime) {
        DisplayMetrics dm = new DisplayMetrics();
        dm = mContext.getResources().getDisplayMetrics();
        double voiceLength = 0.0;
        voiceLength = 0.22 * screenWidth + 0.32 * screenWidth * voiceTime / 60f;
        // voiceLength = 0.1 *screenWidth + 0.25 * screenWidth * 0.44 +
        // 0.44*0.75 * screenWidth * voiceTime/60f;
        return voiceLength;
    }

    private void createUrlSpannableString(SpannableString spannableString) {
        urlMatcher.reset(spannableString);
        List<Integer> startIndexsUrl = new ArrayList<>();
        List<Integer> endIndexsUrl = new ArrayList<>();
        while (urlMatcher.find()) {
            startIndexsUrl.add(urlMatcher.start());
            endIndexsUrl.add(urlMatcher.start() + urlMatcher.group().length());
        }
        if (!startIndexsUrl.isEmpty()) {
            for (int i = 0; i < startIndexsUrl.size(); i++) {
                final String url = spannableString.toString().substring(startIndexsUrl.get(i), endIndexsUrl.get(i));
                spannableString.setSpan(new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(TheLApp.getContext(), WebViewActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(BundleConstants.URL, url);
                        bundle.putString(BundleConstants.TITLE, "");
                        intent.putExtras(bundle);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        TheLApp.getContext().startActivity(intent);
                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        ds.setColor(Color.parseColor("#1F8284"));
                        ds.setUnderlineText(false); // 去掉下划线
                    }
                }, startIndexsUrl.get(i), endIndexsUrl.get(i), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }
    }

    private class OnMomentToChatClickListener implements OnClickListener {

        private MomentToChatBean momentToChatBean;

        public OnMomentToChatClickListener(MomentToChatBean momentToChatBean) {
            this.momentToChatBean = momentToChatBean;
        }

        @Override
        public void onClick(View v) {
//            Intent intent = new Intent();
//            intent.setClass(v.getContext(), MomentCommentActivity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, momentToChatBean.momentId);
//            v.getContext().startActivity(intent);
            FlutterRouterConfig.Companion.gotoMomentDetails(momentToChatBean.momentId);
        }
    }

}
