package com.thel.modules.live.utils;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.modules.live.bean.SoftGiftBean;


/**
 * @author lingwei
 */
public class FastGiftDialogView extends Dialog {

    private static final String TAG = "FastGiftDialogView";
    private Context mContext;
    private TextView tv_no_reminder;
    private TextView tv_reminder;
    private TextView fast_gift_cancel;
    private TextView tv_gift_tile;
    private View.OnClickListener reminderLitener;
    private View.OnClickListener noReminderLitener;
    private View.OnClickListener cancelLitener;

    public FastGiftDialogView(@NonNull Context context) {
        this(context, R.style.CustomDialogBottom);
    }

    public FastGiftDialogView(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        mContext = context;
        init();
        setListener();

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    private void init() {
        final View view = LayoutInflater.from(mContext).inflate(R.layout.live_watch_fast_gift_dialog_view, null);
        setContentView(view);
        tv_no_reminder = view.findViewById(R.id.tv_no_reminder);
        tv_reminder = view.findViewById(R.id.tv_reminder);
        fast_gift_cancel = view.findViewById(R.id.fast_gift_cancel);
        tv_gift_tile = view.findViewById(R.id.tv_gift_tile);
    }

    private void setListener() {


    }

    public void setReminderLitener(View.OnClickListener reminderLitener) {
        this.reminderLitener = reminderLitener;
        tv_reminder.setOnClickListener(reminderLitener);
    }

    public void setNoReminderLitener(View.OnClickListener noReminderLitener) {
        this.noReminderLitener = noReminderLitener;
        tv_no_reminder.setOnClickListener(noReminderLitener);
    }

    public void setCancelLitener(View.OnClickListener cancelLitener) {
        this.cancelLitener = cancelLitener;
        fast_gift_cancel.setOnClickListener(cancelLitener);
    }

    public void setData(SoftGiftBean softFastGiftBean) {
        tv_gift_tile.setText(TheLApp.context.getString(R.string.fast_gift_title, softFastGiftBean.gold + ""));
    }
}
