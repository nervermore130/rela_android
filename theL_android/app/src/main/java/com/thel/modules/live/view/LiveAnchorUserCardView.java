package com.thel.modules.live.view;

import android.app.Activity;
import android.content.Context;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.user.MyImageBean;
import com.thel.constants.TheLConstants;
import com.thel.imp.follow.FollowStatusChangedImpl;
import com.thel.imp.follow.FollowStatusChangedListener;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.live.adapter.LiveSimplePhotoListAdapter;
import com.thel.modules.live.bean.LiveUserCardBean;
import com.thel.modules.live.in.LiveBaseView;
import com.thel.modules.live.utils.LiveUtils;
import com.thel.modules.preview_image.AlbumPhotosPagerAdapter;
import com.thel.ui.widget.UserLevelImageView;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.zoom.VideoAndPhotoItemView;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.Utils;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by waiarl on 2017/4/25.
 */

public class LiveAnchorUserCardView extends FrameLayout implements FollowStatusChangedListener, LiveBaseView<LiveAnchorUserCardView> {
    private Context mContext;
    private LinearLayout lin_more_bottom;
    private FrameLayout root_rl;
    private ImageView avatar;
    private TextView txt_nickname;
    private TextView txt_winkNum;
    private RecyclerView recyclerView;
    private TextView txt_report;
    private TextView txt_follow;
    private LinearLayoutManager manager;
    private List<MyImageBean> picList = new ArrayList<>();
    private ArrayList<String> urlList = new ArrayList<>();
    private LiveSimplePhotoListAdapter adapter;
    private View view_blank;
    private LiveDragViewPager viewpager;
    private String rela_id;
    private boolean is_waterMark = true;
    private VideoAndPhotoItemView.DragListener dragDimissListener;
    private LiveUserCardBean user;
    private DismissListener dismissListener;
    private boolean isAnimating = false;
    private View view_shade;
    private TextView txt_fans_num;
    private UserLevelImageView img_user_level;
    private ImageView img_vip_level;
    private ImageView img_verify;
    private TextView txt_verify;
    private boolean isBroadcaster;
    private OnClickListener blockHerListener;
    private OnClickListener reportListener;

    public LiveAnchorUserCardView(Context context) {
        this(context, null);
    }

    public LiveAnchorUserCardView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LiveAnchorUserCardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        initData();
        init();
        setListener();
    }

    private void initData() {

    }


    private void init() {
        if (mContext == null) {
            mContext = TheLApp.getContext();
        }
        inflate(mContext, R.layout.live_room_anchor_user_card_view, this);
        view_shade = findViewById(R.id.view_shade);
        view_blank = findViewById(R.id.view_blank);
        lin_more_bottom = findViewById(R.id.lin_more_bottom);
        avatar = findViewById(R.id.avatar);
        txt_nickname = findViewById(R.id.txt_nickname);
        txt_winkNum = findViewById(R.id.txt_winkNum);
        recyclerView = findViewById(R.id.recyclerview);
        txt_report = findViewById(R.id.txt_report);
        txt_follow = findViewById(R.id.txt_follow);
        //  img_watcher = (ImageWatcher) findViewById(R.id.img_watcher);

        manager = new LinearLayoutManager(mContext);
        manager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView.setLayoutManager(manager);
        recyclerView.setHasFixedSize(true);
        adapter = new LiveSimplePhotoListAdapter(mContext, picList);
        recyclerView.setAdapter(adapter);
        viewpager = findViewById(R.id.viewpager);
        viewpager.setVisibility(View.GONE);

        txt_fans_num = findViewById(R.id.txt_fans_num);

        img_user_level = findViewById(R.id.img_user_level);
        img_vip_level = findViewById(R.id.img_vip_level);
        img_verify = findViewById(R.id.img_verify);
        txt_verify = findViewById(R.id.txt_verify);

        root_rl = findViewById(R.id.root_rl);

        GrowingIOUtil.setViewInfo(txt_follow, "自动弹出");
    }

    private void setListener() {
        adapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                // showWatcher(position);
                showViewpager(position);
            }
        });
        dragDimissListener = new VideoAndPhotoItemView.DragListener() {

            @Override
            public void dismiss(boolean dismiss) {
                if (dismiss) {
                    viewpager.setVisibility(View.GONE);
                    viewpager.setAdapter(null);
                    Utils.sendVideoPlayingBroadcast(false);//发送是否在播放视频广播，用于直播间的时候屏蔽主播声音
                }
            }
        };
        viewpager.setDragListener(new LiveDragViewPager.DragListener() {
            @Override
            public void dismiss(boolean dismiss) {
                if (dismiss) {
                    viewpager.setVisibility(View.GONE);
                    viewpager.setAdapter(null);
                    Utils.sendVideoPlayingBroadcast(false);//发送是否在播放视频广播，用于直播间的时候屏蔽主播声音
                }
            }
        });
        txt_report.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isBroadcaster && blockHerListener != null) {
                    blockHerListener.onClick(v);
                } else if (!isBroadcaster && reportListener != null) {
                    reportListener.onClick(v);
                }
            }
        });
    }

    public LiveAnchorUserCardView initView(final Activity activity, final LiveUserCardBean user, boolean canShare, final String shareTitle, final String shareContent, final String singleTitle, final String url, boolean isBroadcaster) {
        this.user = user;
        this.isBroadcaster = isBroadcaster;
        txt_nickname.setText(user.nickName);
        txt_winkNum.setText(TheLApp.getContext().getString(R.string.userinfo_activity_wink_even, user.winkNum + ""));
        final int fansNum = user.fansNum;
        //粉丝数
        initViewState();
        if (fansNum > 1) {
            txt_fans_num.setText(TheLApp.getContext().getString(R.string.userinfo_activity_fans_even, user.fansNum));
        } else {
            txt_fans_num.setText(TheLApp.getContext().getString(R.string.userinfo_activity_fans_old, user.fansNum));

        }
        //认证图标
        if (user.verifyType > 0) {
            if (user.verifyType == 2) {
                img_verify.setImageResource(R.mipmap.icn_verify_enterprise);
            } else {
                img_verify.setImageResource(R.mipmap.icn_verify_person);
            }
        }
        //
        //认证信息
        txt_verify.setText(user.verifyIntro + "");
        //根据是否认证显示相应的view
        if (user.verifyType > 0) {
            img_verify.setVisibility(View.VISIBLE);
            txt_verify.setVisibility(View.VISIBLE);
        } else {
            txt_fans_num.setVisibility(View.VISIBLE);
            txt_winkNum.setVisibility(View.VISIBLE);
        }


        if (isBroadcaster) {
            txt_report.setText(R.string.userinfo_activity_dialog_block_title);
        } else {
            txt_report.setText(R.string.userinfo_activity_dialog_report_title);
        }
        setFollowStatus(user);

        // avatar.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(user.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE)));
        ImageLoaderManager.imageLoaderDefaultCircle(avatar, R.mipmap.icon_user, user.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE);

        setUserLevelImg();
        setVipLevelImg();
        rela_id = user.userName;
        initImageWatcher(user.picList);
        return this;
    }

    public LiveAnchorUserCardView initView(final Activity activity, final LiveUserCardBean user, boolean canShare, final String shareTitle, final String shareContent, final String singleTitle, final String url, boolean isBroadcaster, OnClickListener blockHer, final OnClickListener followUser, final OnClickListener reportListener) {
        initView(activity, user, canShare, shareTitle, shareContent, singleTitle, url, isBroadcaster);
        this.blockHerListener = blockHer;
        this.reportListener = reportListener;
        return this;
    }

    public void setBlockHerListener(OnClickListener listener) {
        this.blockHerListener = listener;
    }

    public void setReportListener(OnClickListener listener) {
        this.reportListener = listener;
    }

    public void setViewBlankClickListener(OnClickListener listener) {
        view_blank.setOnClickListener(listener);
    }

    private void initViewState() {
        txt_fans_num.setVisibility(View.GONE);
        txt_winkNum.setVisibility(View.GONE);
        img_verify.setVisibility(View.GONE);
        txt_verify.setVisibility(View.GONE);
    }

    private void setVipLevelImg() {
        if (user == null) {
            return;
        }
        if (user.level > 0 && user.level < TheLConstants.VIP_LEVEL_RES.length) {
            int res = TheLConstants.VIP_LEVEL_RES[user.level];
            img_vip_level.setVisibility(View.VISIBLE);
            img_vip_level.setImageResource(res);
        } else {
            img_vip_level.setVisibility(View.GONE);
        }
    }

    private void setUserLevelImg() {
        if (user == null) {
            return;
        }
        final int user_level = user.userLevel;
        img_user_level.initView(user_level);
    }

    private void setFollowStatus(final LiveUserCardBean user) {
        if (!user.isFollow) {
            txt_follow.setText(TheLApp.getContext().getString(R.string.userinfo_activity_follow));
            txt_follow.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(final View v) {
                    FollowStatusChangedImpl.followUser(user.userId + "", FollowStatusChangedListener.ACTION_TYPE_FOLLOW, user.nickName, user.avatar);
                }
            });
        } else {
            txt_follow.setText(TheLApp.getContext().getString(R.string.userinfo_activity_followed));
            txt_follow.setOnClickListener(null);
        }
    }

    private void initImageWatcher(List<MyImageBean> picLists) {
        picList.clear();
        picList.addAll(picLists);
        urlList.clear();
        adapter.notifyDataSetChanged();
        if (picList.size() <= 0) {
            recyclerView.setVisibility(View.GONE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
        }
        final int size = picList.size();
        for (int i = 0; i < size; i++) {
            urlList.add(picList.get(i).picUrl);
        }
    }

    private void showViewpager(int position) {
        viewpager.setAdapter(new AlbumPhotosPagerAdapter(mContext, urlList, rela_id, is_waterMark, true, null, dragDimissListener));
        viewpager.setCurrentItem(position);
        viewpager.setOffscreenPageLimit(5);
        viewpager.setVisibility(View.VISIBLE);
    }


    @Override
    public void onFollowStatusChanged(int followStatus, String userId, String nickName, String avatar) {
        if (user != null && (user.userId + "").equals(userId)) {
            user.isFollow = followStatus == FOLLOW_STATUS_FOLLOW || followStatus == FOLLOW_STATUS_FRIEND;
            setFollowStatus(user);
        }
    }

    public interface DismissListener {
        void dismiss();
    }

    public void setDismissListener(DismissListener listener) {
        this.dismissListener = listener;
    }

    public LiveAnchorUserCardView show() {
        LiveUtils.animInLiveShowView(this);
        return this;
    }

    @Override
    public LiveAnchorUserCardView destroyView() {
        if (dismissListener != null) {
            dismissListener.dismiss();
        }
        isAnimating = false;
        clearAnimation();
        setTranslationX(0f);
        setTranslationY(0f);
        setVisibility(GONE);
        return this;
    }

    @Override
    public boolean isAnimating() {
        return isAnimating;
    }

    @Override
    public void setAnimating(boolean isAnimating) {
        this.isAnimating = isAnimating;
    }

    @Override
    public void showShade(boolean show) {
        view_shade.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public LiveAnchorUserCardView hide() {
        if (dismissListener != null) {
            dismissListener.dismiss();
        }
        viewpager.setVisibility(View.GONE);
        LiveUtils.animOutLiveShowView(this, false);
        return this;
    }
}
