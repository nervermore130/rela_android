package com.thel.modules.preview_video;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.constants.TheLConstants;
import com.thel.utils.DialogUtil;
import com.thel.utils.VideoPlayManager;
import com.umeng.analytics.MobclickAgent;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VideoPreviewActivity extends BaseActivity implements SurfaceHolder.Callback {

    @BindView(R.id.lin_delete)
    LinearLayout lin_delete;

    @BindView(R.id.playerProgress)
    ProgressBar playerProgress;

    @BindView(R.id.video_view)
    SurfaceView video_view;

    private String url;

    private android.os.Handler handler;
    final Runnable run = new Runnable() {
        @Override
        public void run() {
            if (VideoPlayManager.getInstance().getKsyMediaPlayer() != null) {
                playerProgress.setMax((int) VideoPlayManager.getInstance().getKsyMediaPlayer().getDuration());
                playerProgress.setProgress((int) VideoPlayManager.getInstance().getKsyMediaPlayer().getCurrentPosition());
                if (handler != null) handler.postDelayed(this, 10);
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_video_preview);

        url = getIntent().getStringExtra("url");
        if (TextUtils.isEmpty(url)) {
            finish();
            return;
        }

        ButterKnife.bind(this);

        video_view.getHolder().addCallback(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(this.getClass().getName());
        MobclickAgent.onResume(this);
        VideoPlayManager.getInstance().restart();
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(this.getClass().getName());
        MobclickAgent.onPause(this);
        VideoPlayManager.getInstance().pause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        VideoPlayManager.getInstance().releaseMediaPlayer();
        handler = null;
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return true;
    }

    @OnClick(R.id.lin_back)
    void back() {
        finish();
    }

    @OnClick(R.id.lin_delete)
    void delete() {
        DialogUtil.showConfirmDialog(VideoPreviewActivity.this, "", getString(R.string.video_preview_activity_confirm), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
                setResult(TheLConstants.RESULT_CODE_WRITE_MOMENT_DELETE_VIDEO);
                finish();
            }
        });
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        VideoPlayManager.getInstance().playVideo(url, video_view);
        if (handler == null) handler = new Handler();
            handler.post(run);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        VideoPlayManager.getInstance().releaseMediaPlayer();
        handler = null;
    }
}
