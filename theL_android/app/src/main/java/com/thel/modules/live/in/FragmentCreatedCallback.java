package com.thel.modules.live.in;

/**
 * Created by waiarl on 2017/11/3.
 */

public interface FragmentCreatedCallback {
    void created();
}
