package com.thel.modules.post;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.bean.user.UserInfoBean;
import com.thel.bean.user.UserInfoPicBean;
import com.thel.constants.TheLConstants;
import com.thel.data.local.FileHelper;
import com.thel.growingio.GIOShareTrackBean;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.main.userinfo.bean.UserInfoNetBean;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.service.DefaultRequestService;
import com.thel.utils.DialogUtil;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.ImageUtils;
import com.thel.utils.L;
import com.thel.utils.PermissionUtil;
import com.thel.utils.QRUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.ToastUtils;
import com.thel.utils.Utils;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import video.com.relavideolibrary.Utils.DensityUtils;

import static com.thel.utils.StringUtils.getString;

public class PosterActivity extends BaseActivity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.img_avatar)
    ImageView img_avatar;

    @BindView(R.id.qrcode)
    ImageView qrcode;

    @BindView(R.id.nickname)
    TextView nickname;

    @BindView(R.id.location)
    TextView location;

    @BindView(R.id.relaId)
    TextView relaId;

    @BindView(R.id.age)
    TextView age;

    @BindView(R.id.height)
    TextView height;

    @BindView(R.id.weight)
    TextView weight;

    @BindView(R.id.role)
    TextView role;

    @BindView(R.id.introduce)
    TextView introduce;

    @BindView(R.id.poster_model)
    LinearLayout poster_model;

    @BindView(R.id.share_btn)
    ImageView share_btn;

    @BindView(R.id.share_tv)
    TextView share_tv;

    @OnClick(R.id.img_back)
    void back() {
        PosterActivity.this.finish();
    }

    @OnClick(R.id.ll_share)
    void bottom() {

        PermissionUtil.requestStoragePermission(this, new PermissionUtil.PermissionCallback() {
            @Override
            public void granted() {
                SHARE_MEDIA platform = null;

                switch (shareType) {
                    case SHARE_TYPE_WX_CIRCLE:
                        platform = SHARE_MEDIA.WEIXIN_CIRCLE;
                        break;
                    case SHARE_TYPE_WX:
                        platform = SHARE_MEDIA.WEIXIN;
                        break;
                    case SHARE_TYPE_WEIBO:
                        platform = SHARE_MEDIA.SINA;
                        break;
                    case SHARE_TYPE_QQ:
                        platform = SHARE_MEDIA.QQ;
                        break;
                    case SHARE_TYPE_QZONE:
                        platform = SHARE_MEDIA.QZONE;
                        break;
                    case SHARE_TYPE_FACEBOOK:
                        shareFacebook();
                        return;
                    case DOWNLOAD:
                        download();
                        break;
                }
                share(platform);
            }

            @Override
            public void denied() {
                ToastUtils.showToastShort(PosterActivity.this, "缺少文件读写权限，无法分享");
            }

            @Override
            public void gotoSetting() {

            }
        });

    }

    public static final String URL_SHARE_MOMENT = "https://s.growingio.com/JYMBmB";

    public static final int SHARE_TYPE_WX_CIRCLE = 0x01;

    public static final int SHARE_TYPE_WX = 0x02;

    public static final int SHARE_TYPE_WEIBO = 0x03;

    public static final int SHARE_TYPE_QQ = 0x04;

    public static final int SHARE_TYPE_QZONE = 0x05;

    public static final int SHARE_TYPE_FACEBOOK = 0x06;

    public static final int DOWNLOAD = 0x07;

    private UserInfoBean mUserInfoBean;

    private String userId;

    private int shareType = -1;

    private GIOShareTrackBean gioShareTrackBean = new GIOShareTrackBean();

    private final int IMG_QUALITY = 100;

    private final float THUMB_SCALE = 0.3f;//缩略图比例

    private String imgPath;//分享的图片保存的本地路径

    private String zoomPath;//分享的图片缩略图保存的本地路径

    public static void gotoShare(String userId, int shareType) {
        Intent intent = new Intent(TheLApp.getContext(), PosterActivity.class);
        intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, userId);
        intent.putExtra("shareType", shareType);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        TheLApp.getContext().startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poster);

        ButterKnife.bind(this);

        shareType = getIntent().getIntExtra("shareType", -1);
        userId = getIntent().getStringExtra(TheLConstants.BUNDLE_KEY_USER_ID);
        if (TextUtils.isEmpty(userId) || shareType == -1) {
            finish();
        }

        getUserInfoData();

    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    @SuppressLint("SetTextI18n")
    private void initView() {
        ImageLoaderManager.imageLoaderCropCircleBorder(img_avatar, mUserInfoBean.avatar);
        if (!TextUtils.isEmpty(mUserInfoBean.nickName))
            nickname.setText(mUserInfoBean.nickName);
        else nickname.setVisibility(View.GONE);
        if (!TextUtils.isEmpty(mUserInfoBean.livecity))
            location.setText(mUserInfoBean.livecity);
        else location.setVisibility(View.GONE);
        if (!TextUtils.isEmpty(mUserInfoBean.userName))
            relaId.setText("Rela ID: " + mUserInfoBean.userName);
        else relaId.setVisibility(View.GONE);
        if (!TextUtils.isEmpty(mUserInfoBean.age))
            age.setText(mUserInfoBean.age + getString(R.string.updatauserinfo_activity_age_unit));
        else age.setVisibility(View.GONE);
        if (!TextUtils.isEmpty(mUserInfoBean.height))
            height.setText(mUserInfoBean.height + "cm");
        else height.setVisibility(View.GONE);
        if (!TextUtils.isEmpty(mUserInfoBean.weight))
            weight.setText(mUserInfoBean.weight + "kg");
        else weight.setVisibility(View.GONE);
        if (!TextUtils.isEmpty(mUserInfoBean.roleName))
            role.setText(ShareFileUtils.getRoleBean(Integer.parseInt(mUserInfoBean.roleName)).contentRes);
        else role.setVisibility(View.GONE);
        if (!TextUtils.isEmpty(mUserInfoBean.intro))
            introduce.setText(mUserInfoBean.intro);
        else introduce.setVisibility(View.GONE);

        final Bitmap bitmapQr = QRUtils.createQRImage(URL_SHARE_MOMENT, Utils.dip2px(this, 80), Utils.dip2px(this, 80));
        qrcode.setImageBitmap(bitmapQr);

        int width = recyclerView.getMeasuredWidth();
        recyclerView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, width));
        recyclerView.setLayoutManager(new GridLayoutManager(PosterActivity.this, 6));
        recyclerView.setHasFixedSize(false);

        ArrayList<UserInfoPicBean> picListTemp = new ArrayList<>();
        //过滤视频文件
        if (mUserInfoBean.picList != null)
            for (UserInfoPicBean bean : mUserInfoBean.picList) {
                if (!bean.picUrl.endsWith(".mp4")) picListTemp.add(bean);
            }
        if (picListTemp.size() == 0) {
            UserInfoPicBean bean = new UserInfoPicBean();
            bean.picUrl = mUserInfoBean.avatar;
            picListTemp.add(bean);
        }
        recyclerView.setAdapter(new ImageAdapter(PosterActivity.this, picListTemp, width));
        if(shareType == PosterActivity.DOWNLOAD) {
            share_btn.setVisibility(View.VISIBLE);
            share_tv.setText(R.string.info_save);
        }else {
            share_btn.setVisibility(View.GONE);
            share_tv.setText(R.string.shared_this_picture);
        }
    }

    private void getUserInfoData() {
        if (!TextUtils.isEmpty(userId)) {
            final Flowable<UserInfoNetBean> flowable = DefaultRequestService.createNearbyRequestService().getUserInfoBean(userId);
            flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<UserInfoNetBean>() {

                @Override
                public void onNext(UserInfoNetBean userInfoNetBean) {
                    super.onNext(userInfoNetBean);

                    L.d("userInfoNetBean", "onNext");

                    L.d("userInfoNetBean", " hasErrorCode " + hasErrorCode);

                    if (userInfoNetBean != null) {
                        L.d("userInfoNetBean", userInfoNetBean.toString());

                        if (userInfoNetBean.data != null) {
                            mUserInfoBean = userInfoNetBean.data;
                            initView();
                        }

                    }

                }

                @Override
                public void onError(Throwable t) {
                }

                @Override
                public void onComplete() {
                }
            });
        }

    }

    /**
     * facebook分享
     */
    private void shareFacebook() {
        final String path = getShareImagePath(poster_model, IMG_QUALITY);
        if (!TextUtils.isEmpty(path)) {
            if (ShareDialog.canShow(SharePhotoContent.class)) {
                final SharePhoto photo = new SharePhoto.Builder().setBitmap(BitmapFactory.decodeFile(path)).build();
                final SharePhotoContent content = new SharePhotoContent.Builder().addPhoto(photo).build();
                ShareDialog shareDialog = new ShareDialog(this);
                shareDialog.show(content);
            } else {
                DialogUtil.showToastShort(this, getString(R.string.install_facebook));
            }
        }
    }

    /**
     * 朋友圈，微信，qq，qq空间，新浪微博 分享
     *
     * @param platform
     */
    public void share(SHARE_MEDIA platform) {
        MobclickAgent.onEvent(this, "start_share");
        final String path = getShareImagePath(poster_model, IMG_QUALITY);
        final String thumb_path = getZoomBitmapPath(path, THUMB_SCALE, IMG_QUALITY);
        if (null != platform && !TextUtils.isEmpty(path)) {
            final UMImage image = new UMImage(this, BitmapFactory.decodeFile(path));
            image.setThumb(new UMImage(this, BitmapFactory.decodeFile(thumb_path)));
            new ShareAction(this).setPlatform(platform).withMedia(image).setCallback(umShareListener).share();
            gioShareTrackBean.shareBean.shareType = GrowingIOUtil.getShareType(platform);
            GrowingIOUtil.shareTrack(gioShareTrackBean);
        }
    }

    /**
     * 获取分享的图片
     *
     * @param view
     * @return
     */
    private String getShareImagePath(View view, int quality) {
        if (!TextUtils.isEmpty(imgPath))
            return imgPath;
        int width = view.getWidth();
        int height = view.getHeight();
        int radius = DensityUtils.dp2px(12);
        final Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();

        paint.setColor(Color.parseColor("#4BBABC"));
        //left top
        canvas.drawRect(0, 0, radius, radius, paint);
        //right top
        canvas.drawRect(width - radius, 0, width, radius, paint);

        paint.setColor(ContextCompat.getColor(this, R.color.white));
        //left bottom
        canvas.drawRect(0, height - radius, radius, height, paint);
        //right bottom
        canvas.drawRect(width - radius, height - radius, width, height, paint);
        view.draw(canvas);

        File dir = new File(TheLConstants.F_TAKE_PHOTO_ROOTPATH);
        if (!dir.exists()) {
            dir.mkdirs();
        }

        imgPath = FileHelper.getInstance().getCameraDir() + System.currentTimeMillis() + TheLConstants.MOMENT_POSTER_SHARE_IMG_END + ".jpg";


        L.d("MomentPoster", " imgPath -1 : " + imgPath);

        try {
            if (ImageUtils.savePic(bitmap, imgPath, quality)) {
                Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                Uri uri = Uri.fromFile(new File(imgPath));
                intent.setData(uri);
                this.sendBroadcast(intent);
                L.d("MomentPoster", " imgPath 0 : " + imgPath);
                return imgPath;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        imgPath = null;

        L.d("MomentPoster", " imgPath 1 : " + imgPath);

        return imgPath;
    }

    private String getZoomBitmapPath(String oldPath, float scale, int quality) {
        if (!TextUtils.isEmpty(zoomPath))
            return zoomPath;
        if (TextUtils.isEmpty(oldPath)) {
            zoomPath = null;
            return zoomPath;
        }
        final Bitmap bitmap = BitmapFactory.decodeFile(oldPath);
        if (null == bitmap) {
            return null;
        }
        try {
            final Bitmap bm = getZoomBitmap(bitmap, scale);
            zoomPath = TheLConstants.F_TAKE_PHOTO_ROOTPATH + System.currentTimeMillis() + TheLConstants.MOMENT_POSTER_SHARE_IMG_END + ".jpg";
            if (ImageUtils.savePic(bm, zoomPath, quality)) {
                return zoomPath;
            }
        } catch (Exception e) {
            bitmap.recycle();
            zoomPath = null;
            e.printStackTrace();
        }
        return zoomPath;
    }

    private Bitmap getZoomBitmap(Bitmap oldBitmap, float scale) {
        Bitmap bm = null;
        final float width = oldBitmap.getWidth();
        final float height = oldBitmap.getHeight();
        final Matrix matrix = new Matrix();
        matrix.postScale(scale, scale);
        bm = Bitmap.createBitmap(oldBitmap, 0, 0, (int) width, (int) height, matrix, true);
        return bm;
    }

    /**
     * 下载海报
     */
    private void download() {

        getShareImagePath(poster_model, IMG_QUALITY);

        if (!TextUtils.isEmpty(imgPath)) {
            setResult(Activity.RESULT_OK, getIntent());
            finish();
//            Toast.makeText(PosterActivity.this, getString(R.string.poster_download_success), Toast.LENGTH_SHORT).show();
        }
    }

    private UMShareListener umShareListener = new UMShareListener() {

        @Override
        public void onStart(SHARE_MEDIA share_media) {

        }

        @Override
        public void onResult(SHARE_MEDIA platform) {
            MobclickAgent.onEvent(PosterActivity.this, "share_succeeded");
        }

        @Override
        public void onError(SHARE_MEDIA platform, Throwable t) {
            if (t != null && t.getMessage() != null) {
                DialogUtil.showToastShort(PosterActivity.this, t.getMessage());
            }
        }

        @Override
        public void onCancel(SHARE_MEDIA platform) {
            //这里注销掉cancel提示，分享成功点击返回也会走到onCancel
//            DialogUtil.showToastShort(MomentPosterActivity.this, TheLApp.getContext().getString(R.string.my_circle_requests_act_canceled));
        }
    };

}
