package com.thel.modules.live.view;

import android.content.Context;
import androidx.viewpager.widget.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by waiarl on 2017/10/26.
 */

public class LiveDragViewPager extends ViewPager {
    private DragListener dragListener;

    public LiveDragViewPager(Context context) {
        super(context);
    }

    public LiveDragViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    float startX = 0, startY = 0, endX = 0, endY = 0;
    final int dis = 200;//滑动最小上下距离为200；

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        getParent().requestDisallowInterceptTouchEvent(true);
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                startX = event.getX();
                startY = event.getY();
                break;
            case MotionEvent.ACTION_UP:
                endX = event.getX();
                endY = event.getY();
                final float mx = endX - startX;
                final float my = endY - startY;
                if (Math.abs(mx) == 0 && my >= dis) {
                    if (dragListener != null) {
                        dragListener.dismiss(true);
                    }
                } else if (Math.abs(mx) > 0 && my >= dis) {
                    final double digree = Math.toDegrees(Math.atan(my / mx));
                    if (digree >= 70 || digree <= -70) {
                        if (dragListener != null) {
                            dragListener.dismiss(true);
                        }
                    }
                }
                break;
        }
        return super.dispatchTouchEvent(event);
    }

    public interface DragListener {
        void dismiss(boolean dismiss);
    }

    public void setDragListener(DragListener dragListener) {
        this.dragListener = dragListener;
    }

}
