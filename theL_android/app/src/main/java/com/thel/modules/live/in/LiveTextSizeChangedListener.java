package com.thel.modules.live.in;

/**
 * Created by waiarl on 2017/11/3.
 */

public interface LiveTextSizeChangedListener {
    int TEXT_SIZE_STANDARD = 0;//选择标准字体
    int TEXT_SIZE_LARGE = 1;//选择为大号字体
    int TEXT_SIZE_BIGGEST = 2;//选择为最大字体

    void textSizeChanged(int type);
}
