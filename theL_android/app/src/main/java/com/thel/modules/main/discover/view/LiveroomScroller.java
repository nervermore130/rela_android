package com.thel.modules.main.discover.view;

import android.content.Context;
import android.view.animation.Interpolator;
import android.widget.Scroller;

/**
 * 这个类为viewpager的时候的滚动类
 * 用来设置从一页到另一页跳转时候的滚动时间
 */

public class LiveroomScroller extends Scroller {
    //滚动过渡的时间
    private int mDuration = 0;

    public LiveroomScroller(Context context) {
        super(context);
    }

    public LiveroomScroller(Context context, Interpolator interpolator) {
        super(context, interpolator);
    }

    public LiveroomScroller(Context context, Interpolator interpolator, boolean flywheel) {
        super(context, interpolator, flywheel);
    }

    public LiveroomScroller setScrollDuration(int duration) {
        mDuration = duration;
        return this;
    }


    @Override
    public void startScroll(int startX, int startY, int dx, int dy, int duration) {
        super.startScroll(startX, startY, dx, dy, mDuration);
    }

    @Override
    public void startScroll(int startX, int startY, int dx, int dy) {
        super.startScroll(startX, startY, dx, dy, mDuration);
    }
}
