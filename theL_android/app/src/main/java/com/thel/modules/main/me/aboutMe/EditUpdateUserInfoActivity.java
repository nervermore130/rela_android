package com.thel.modules.main.me.aboutMe;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.base.BaseDataBean;
import com.thel.bean.CheckUserNameBean;
import com.thel.constants.TheLConstants;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.utils.DialogUtil;
import com.thel.utils.ViewUtils;

import org.reactivestreams.Subscription;

import java.util.Random;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * 修改昵称和填写详细自述
 * Created by lingwei on 2017/10/13.
 */

public class EditUpdateUserInfoActivity extends BaseActivity {

    private EditText edit_nickname;
    private ImageView lin_back;
    private ImageView img_more;
    private TextView txt_num;
    private TextView txt_title;
    private Handler mHandler;
    private String nickname;
    private String currentNickname;
    private EditText edit_intro_selef;
    private EditText currency;
    private String requestFrom;
    private String oldIntroContent;
    private EditText edit_relaId;
    private String relaId;
    private String oldRelaIdContent;
    private TextView tv_limit_days;
    private TextView tv_relaid;
    private Random mRandom;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_info_acitivity_layout);
        mHandler = new Handler(Looper.getMainLooper());
        mRandom = new Random();
        findViewById();
        getIntentData();
        setListener();

    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void findViewById() {
        lin_back = findViewById(R.id.lin_back);
        txt_title = findViewById(R.id.txt_title);
        img_more = findViewById(R.id.img_more);
        edit_nickname = findViewById(R.id.edit_nickname);
        txt_num = findViewById(R.id.txt_num);
        edit_intro_selef = findViewById(R.id.edit_intro_selef);
        edit_relaId = findViewById(R.id.edit_relaId);
        tv_limit_days = findViewById(R.id.tv_limit_days);
        tv_relaid = findViewById(R.id.tv_relaid);
        currency = findViewById(R.id.currency);
    }

    private void setListener() {
        lin_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                ViewUtils.hideSoftInput(EditUpdateUserInfoActivity.this);

            }
        });
        img_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewUtils.hideSoftInput(EditUpdateUserInfoActivity.this, img_more);

                if (requestFrom.equals("nickName")) {
                    saveNickName();
                } else if (requestFrom.equals("RelaId")) {
                    savaRelaId();
                } else if (requestFrom.equals("occupation")) {
                    final Intent intent = new Intent();
                    final Bundle bundle = new Bundle();
                    bundle.putString(TheLConstants.BUNDLE_KEY_CAREER, currency.getText().toString().trim());
                    intent.putExtras(bundle);
                    setResult(TheLConstants.RESULT_CODE_EDIT_CAREER, intent);
                    finish();
                } else if (requestFrom.equals("livecity")) {
                    final Intent intent = new Intent();
                    final Bundle bundle = new Bundle();
                    bundle.putString(TheLConstants.BUNDLE_KEY_LIVEIN, currency.getText().toString().trim());
                    intent.putExtras(bundle);
                    setResult(TheLConstants.RESULT_CODE_EDIT_LIVEIN, intent);
                    finish();
                } else if (requestFrom.equals("travel")) {
                    final Intent intent = new Intent();
                    final Bundle bundle = new Bundle();
                    bundle.putString(TheLConstants.BUNDLE_KEY_TRAVEL, currency.getText().toString().trim());
                    intent.putExtras(bundle);
                    setResult(TheLConstants.RESULT_CODE_EDIT_TRAVEL, intent);
                    finish();
                } else if (requestFrom.equals("food")) {
                    final Intent intent = new Intent();
                    final Bundle bundle = new Bundle();
                    bundle.putString(TheLConstants.BUNDLE_KEY_FOOD, currency.getText().toString().trim());
                    intent.putExtras(bundle);
                    setResult(TheLConstants.RESULT_CODE_EDIT_FOOD, intent);
                    finish();
                } else if (requestFrom.equals("other")) {
                    final Intent intent = new Intent();
                    final Bundle bundle = new Bundle();
                    bundle.putString(TheLConstants.BUNDLE_KEY_OTHER, currency.getText().toString().trim());
                    intent.putExtras(bundle);
                    setResult(TheLConstants.RESULT_CODE_EDIT_OTHER, intent);
                    finish();
                } else if (requestFrom.equals("intro")){
                    saveIntro();
                }
            }
        });
        edit_nickname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                txt_num.setText((12 - s.length()) + "");
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        edit_intro_selef.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                txt_num.setText((200 - s.length()) + "");

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        edit_relaId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                txt_num.setText((20 - s.length()) + "");

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        currency.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                txt_num.setText((15 - s.length()) + "");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void saveIntro() {
        String introContent = edit_intro_selef.getText().toString().trim();
        if (introContent.equals(oldIntroContent)) {
            finish();
            return;
        } else {
            final Intent intent = new Intent();
            final Bundle bundle = new Bundle();
            bundle.putString(TheLConstants.BUNDLE_KEY_INTROCONTENT, introContent);
            intent.putExtras(bundle);
            setResult(TheLConstants.RESULT_CODE_EDIT_INTRO, intent);
            finish();
        }

    }

    private void saveNickName() {

        nickname = edit_nickname.getText().toString().trim();
        if (nickname.equals(currentNickname)) {
            finish();
            return;
        }
        if (TextUtils.isEmpty(nickname)) {
            DialogUtil.showToastShort(this, getString(R.string.register_activity_nick_name_is_empty));
            return;
        }
        Flowable<BaseDataBean> flowable = RequestBusiness.getInstance().getNickNameTimeVerify(nickname);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
            @Override
            public void onNext(BaseDataBean data) {
                super.onNext(data);
                saveSuccess();
            }
        });

    }

    // 表单字段校验
    private boolean checkIsEmpty() {
        if (TextUtils.isEmpty(edit_relaId.getText().toString().trim())) {
            DialogUtil.showToastShort(EditUpdateUserInfoActivity.this, TheLApp.getContext().getString(R.string.info_is_empty));
            return true;
        } else {
            return false;
        }
    }

    private void savaRelaId() {
        relaId = edit_relaId.getText().toString().trim();
        // 校验用户名格式
        if (TextUtils.isEmpty(edit_relaId.getText().toString().trim())) {
            DialogUtil.showToastShort(EditUpdateUserInfoActivity.this, TheLApp.getContext().getString(R.string.info_is_empty));
        }
        if (!relaId.matches("[_A-Za-z0-9]{6,20}")) {
            DialogUtil.showToastShort(EditUpdateUserInfoActivity.this, getString(R.string.updatauserinfo_activity_username_illegal));
            return;
        }
        if (relaId.equals(oldRelaIdContent)) {
            finish();
            return;
        } else if (!TextUtils.isEmpty(edit_relaId.getText().toString().trim()) && !relaId.equals(oldRelaIdContent)) {
            showLoadingNoBack();
            checkUserName(relaId.toLowerCase());
        }

    }

    private void saveSuccess() {
        DialogUtil.showToastShort(this, getString(R.string.edit_nickname_tip));
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                final Intent intent = new Intent();
                final Bundle bundle = new Bundle();
                bundle.putString(TheLConstants.BUNDLE_KEY_NICKNAME, nickname);
                intent.putExtras(bundle);
                setResult(TheLConstants.RESULT_CODE_EDIT_NICKNAME, intent);
                finish();
            }
        }, 2000);
    }

    public void checkUserName(String username) {

        Flowable<CheckUserNameBean> flowable = RequestBusiness.getInstance().checkUserName(username);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<CheckUserNameBean>() {

            @Override
            public void onNext(CheckUserNameBean data) {
                super.onNext(data);
                checkUserNameResult(data);
            }

        });

    }

    private void checkUserNameResult(CheckUserNameBean data) {
        if (data.data != null && data.data.exists) {
            DialogUtil.showToastShort(this, getString(R.string.updatauserinfo_activity_username_exists));
            closeLoading();
           // setSuggestUserName();
            return;
        }
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                final Intent intent = new Intent();
                final Bundle bundle = new Bundle();
                bundle.putString(TheLConstants.BUNDLE_KEY_RELAID, relaId);
                intent.putExtras(bundle);
                setResult(TheLConstants.RESULT_CODE_EDIT_RELAID, intent);
                finish();
            }
        }, 2000);

    }

    /**
     * 设置建议rela id
     */
    private void setSuggestUserName() {
        final String st = edit_relaId.getText().toString().trim();
        final String str = st + mRandom.nextInt(10) + mRandom.nextInt(10) + mRandom.nextInt(10);
        edit_relaId.setText(str);
    }

    private void getIntentData() {
        final Intent intent = getIntent();
        requestFrom = intent.getStringExtra("requestFrom");
        if (requestFrom.equals("nickName")) {
            txt_title.setText(TheLApp.context.getString(R.string.register_activity_nick_name));
            edit_nickname.setVisibility(View.VISIBLE);
            edit_intro_selef.setVisibility(View.GONE);
            edit_relaId.setVisibility(View.GONE);
            tv_limit_days.setVisibility(View.VISIBLE);
            currentNickname = intent.getStringExtra(TheLConstants.BUNDLE_KEY_NICKNAME);
            if (edit_nickname != null) {
                edit_nickname.setText(currentNickname);
            }
            if (currentNickname != null && txt_num != null) {
                txt_num.setText((12 - currentNickname.length()) + "");
            }
        } else if (requestFrom.equals("intro")) {
            edit_nickname.setVisibility(View.GONE);
            edit_intro_selef.setVisibility(View.VISIBLE);
            edit_relaId.setVisibility(View.GONE);
            txt_title.setText(TheLApp.context.getString(R.string.personalized_signature));
            oldIntroContent = intent.getStringExtra("introContent");
            if (edit_intro_selef != null) {
                if (!TextUtils.isEmpty(oldIntroContent)) {
                    edit_intro_selef.setText(oldIntroContent);
                }
                txt_num.setText(200 - oldIntroContent.length() + "");
            }

        } else if (requestFrom.equals("RelaId")) {
            txt_title.setText("Rela Id");
            tv_limit_days.setVisibility(View.VISIBLE);
            tv_limit_days.setText(R.string.updatauserinfo_activity_username_tip);
            edit_relaId.setVisibility(View.VISIBLE);
            tv_relaid.setVisibility(View.VISIBLE);
            edit_nickname.setVisibility(View.GONE);
            edit_intro_selef.setVisibility(View.GONE);
            oldRelaIdContent = intent.getStringExtra("introContent");
            if (edit_relaId != null) {
                if (!TextUtils.isEmpty(oldRelaIdContent)) {
                    edit_relaId.setText(oldRelaIdContent);
                }
                txt_num.setText(20 - oldRelaIdContent.length() + "");
            }

        } else if (requestFrom.equals("occupation")) {
            txt_title.setText(TheLApp.context.getString(R.string.edituserinfo_activity_career_title));
            currency.setVisibility(View.VISIBLE);
            String career = intent.getStringExtra(TheLConstants.BUNDLE_KEY_CAREER);
            if (!TextUtils.isEmpty(career)) {
                currency.setText(career);
            }
            txt_num.setText(15 - career.length() + "");
        } else if (requestFrom.equals("livecity")) {
            String title = getString(R.string.updata_userinfo_live_place);
            txt_title.setText(title);
            currency.setVisibility(View.VISIBLE);
            currency.setHint(R.string.edit_userinfo_livecity);
            String data = intent.getStringExtra(TheLConstants.BUNDLE_KEY_LIVEIN);
            if (!TextUtils.isEmpty(data)) {
                currency.setText(data);
            }
            txt_num.setText(15 - data.length() + "");
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            DialogUtil.showConfirmDialog(EditUpdateUserInfoActivity.this, "", getString(R.string.updatauserinfo_activity_quit_confirm), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    EditUpdateUserInfoActivity.this.finish();
                }
            });
        }
        return super.onKeyDown(keyCode, event);
    }
}
