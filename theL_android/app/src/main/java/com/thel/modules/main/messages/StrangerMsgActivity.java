package com.thel.modules.main.messages;

import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.callback.IMsgCallback;
import com.thel.constants.TheLConstants;
import com.thel.db.DBConstant;
import com.thel.db.DBUtils;
import com.thel.db.table.message.MsgTable;
import com.thel.manager.ChatServiceManager;
import com.thel.modules.main.messages.adapter.MessagesAdapter;
import com.thel.modules.main.messages.bean.MsgBean;
import com.thel.service.chat.ChatService;
import com.thel.ui.dialog.DeleteMsgDialog;
import com.thel.utils.L;
import com.thel.utils.PinyinUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * @author kevin
 * @date 2017/9/29
 */

public class StrangerMsgActivity extends BaseActivity {

    private static final String TAG = "StrangerMsgActivity";

    private LinearLayout lin_go_to_top;
    private LinearLayout search_layout;
    private EditText edit_search;
    private MessagesAdapter adapter;
    public ListView listview;
    private LinearLayout bg_messages_default;
    private LinearLayout lin_xmpp_status;
    private TextView txt_xmpp_status;
    /**
     * 没有匹配的联系人提示
     */
    private TextView no_match_tip;
    private List<MsgTable> lastMsgWithUnreadList = new ArrayList<>();
    /**
     * 输入关键字过滤后的消息对象列表
     */
    private ArrayList<MsgTable> filterArr = new ArrayList<>();
    private String myUid;

    private TextWatcher textWatcher;
    private TextView txt_xmp_status_title;
    private Handler mHandler = new Handler();
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private ImageView not_connet_image;
    private RelativeLayout img_back;

    private ExecutorService refreshExecutorService = Executors.newSingleThreadExecutor();

    private static long lastClickTime = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stranger_msg_activity);
        findViewById();
        processBusiness();
        setListener();
        reCountStrangerUnReadCount();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(this.getClass().getName());
        MobclickAgent.onResume(this);

        refreshData();

        NotificationManager mgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (mgr != null) {
            mgr.cancel(TheLConstants.NOTIFICATION_ID_MSG);
        }

        if (!isDestroyed()) {

            runOnUiThread(new Runnable() {
                @Override public void run() {

                    showConnectStatusUI(ChatServiceManager.getInstance().getConnectStatus());

                }
            });
        }

        getStrangerList();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ChatServiceManager.getInstance().unRegisterCallback(TAG);
        if (textWatcher != null && edit_search != null) {
            edit_search.removeTextChangedListener(textWatcher);
        }
        ViewUtils.hideSoftInput(this, edit_search);

    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(this.getClass().getName());
        MobclickAgent.onPause(this);
    }

    protected void processBusiness() {
        myUid = ShareFileUtils.getString(ShareFileUtils.ID, "");
        ChatServiceManager.getInstance().registerCallback(TAG, mIMsgStatusCallback);
    }

    protected void setListener() {

        lin_go_to_top.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (MotionEvent.ACTION_DOWN == event.getAction()) {
                    if (System.currentTimeMillis() - lastClickTime < 300) {
                        ViewUtils.preventViewMultipleTouch(lin_go_to_top, 2000);
                        goToTop();
                    }
                    lastClickTime = System.currentTimeMillis();
                }
                return true;
            }
        });

        textWatcher = new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filterArr.clear();
                if (TextUtils.isEmpty(s.toString().trim())) {
                    adapter.refreshAdapter(lastMsgWithUnreadList);
                    no_match_tip.setVisibility(View.GONE);
                } else {
                    for (MsgTable msgBean : lastMsgWithUnreadList) {
                        String nickNamePinYin = null;
                        try {
                            if (MsgBean.MSG_DIRECTION_TYPE_OUTGOING.equals(msgBean.msgDirection)) {// 发出
                                nickNamePinYin = PinyinUtils.cn2Spell(msgBean.toUserNickname);
                            } else {// 收到
                                nickNamePinYin = PinyinUtils.cn2Spell(msgBean.fromNickname);
                            }
                        } catch (Exception e) {
                        }
                        if (nickNamePinYin != null && nickNamePinYin.contains(PinyinUtils.cn2Spell(s.toString().trim()))) {
                            filterArr.add(msgBean);
                        }
                    }
                    adapter.refreshAdapter(filterArr);
                    if (filterArr.isEmpty()) {
                        no_match_tip.setVisibility(View.VISIBLE);
                    } else {
                        no_match_tip.setVisibility(View.GONE);
                    }
                }
                adapter.notifyDataSetChanged();
            }
        };
        edit_search.addTextChangedListener(textWatcher);

        // 列表点击
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                MsgTable msgTable = filterArr.size() > 0 ? filterArr.get(position - 1) : lastMsgWithUnreadList.get(position - 1);

                if (MsgBean.MSG_TYPE_REQUEST.equals(msgTable.msgType) && TextUtils.isEmpty(msgTable.fromUserId)) {
                    startActivity(new Intent(StrangerMsgActivity.this, MyCircleRequestsActivity.class));
                } else {
                    msgTable.unreadCount = 0;
                    ChatServiceManager.getInstance().updateMsgUnread(msgTable);

                    String toUserId, toName, toAvatar = "";

                    toUserId = msgTable.userId;
                    toName = msgTable.userName;
                    toAvatar = msgTable.avatar;

                    Intent intent = new Intent(StrangerMsgActivity.this, ChatActivity.class);
                    intent.putExtra("toUserId", toUserId);
                    intent.putExtra("toName", toName);
                    intent.putExtra("toAvatar", toAvatar);
                    intent.putExtra("fromPage", "MessagesActivity");

                    L.d(TAG, " msgBeanClick.msgType : " + msgTable.msgType);

                    int isSystem;
                    if (toUserId.equals(TheLConstants.RELA_ACCOUNT) || toUserId.equals(TheLConstants.SYSTEM_USER)) {
                        isSystem = 1;
                    } else {
                        isSystem = 0;
                    }
                    intent.putExtra("isSystem", isSystem);

                    startActivity(intent);
                }
            }
        });

        listview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    int currentPosition = position - listview.getHeaderViewsCount();

                    if (lastMsgWithUnreadList.size() > currentPosition) {

                        showDeleteMenuAndReport(lastMsgWithUnreadList.get(currentPosition));

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            }
        });


        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                L.d("refresh", "img_back");
                finish();
            }
        });
    }

    protected void findViewById() {
        lin_go_to_top = findViewById(R.id.go_to_top);
        search_layout = (LinearLayout) LinearLayout.inflate(this, R.layout.search_common_contact_layout, null);
        edit_search = search_layout.findViewById(R.id.edit_search);
        listview = this.findViewById(R.id.listview);
        listview.addHeaderView(search_layout);
        bg_messages_default = findViewById(R.id.bg_messages_default);
        no_match_tip = findViewById(R.id.no_match_tip);
        txt_xmp_status_title = findViewById(R.id.go_to_top_txt);
        not_connet_image = findViewById(R.id.not_connet);
        img_back = findViewById(R.id.img_back);

        txt_xmp_status_title.setText(getString(R.string.stranger));

    }

    private void refreshData() {
        if (edit_search != null && !TextUtils.isEmpty(edit_search.getText().toString().trim())) {
            return;
        }
        refreshExecutorService.execute(new Runnable() {
            @Override
            public void run() {

            }
        });

    }

    /**
     * 删除这个对话（刷新界面）
     *
     * @param toUserId
     */
    private void removeChatOnUI(String toUserId) {
        int i = 0;
        for (MsgTable msgWithUnreadBean : lastMsgWithUnreadList) {
            if (toUserId.equals(msgWithUnreadBean.userId)) {
                lastMsgWithUnreadList.remove(i);
                adapter.refreshAdapter(lastMsgWithUnreadList);
                adapter.notifyDataSetChanged();
                break;
            }
            i++;
        }
        // 更新总未读消息数
        Intent intent = new Intent();
        intent.setAction(TheLConstants.BROADCAST_UPDATE_UNREAD_MSG_COUNT_COMPLETELY);
        TheLApp.getContext().sendBroadcast(intent);
    }

    public void goToTop() {
        if (listview != null) {
            listview.setSelection(0);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            StrangerMsgActivity.this.finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void finish() {
        super.finish();
        //返回message fragment，刷新聊天数据
        Intent intent = new Intent();
        intent.setAction(TheLConstants.BROADCAST_REFRESH_MESSAGE);
        sendBroadcast(intent);
    }

    public IMsgCallback mIMsgStatusCallback = new IMsgCallback() {
        @Override public void onConnectStatus(final int status) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showConnectStatusUI(status);
                }
            });
        }

        @Override public void onUpdateMsg(MsgTable msgTable) {

            L.d(TAG, " onUpdateMsg msgTable.userId : " + msgTable.userId);

            if (msgTable.userId.equals(DBConstant.Message.STRANGER_TABLE_NAME) || msgTable.isStranger == 0) {
                return;
            }

            if (lastMsgWithUnreadList != null) {

                for (int i = 0; i < lastMsgWithUnreadList.size(); i++) {

                    MsgTable mt = lastMsgWithUnreadList.get(i);

                    if (mt.userId != null && msgTable.userId != null) {
                        if (mt.userId.equals(msgTable.userId)) {
                            mt.msgTime = msgTable.getMsgTime();
                            mt.msgText = msgTable.msgText;
                            mt.msgType = msgTable.msgType;
                            mt.unreadCount = msgTable.unreadCount;
                            mt.isWinked = msgTable.isWinked;
                            L.d(TAG, " onUpdateMsg 找到旧的数据 更新 : " + msgTable.unreadCount);
                            break;
                        }
                    }
                    if (i == lastMsgWithUnreadList.size() - 1) {
                        L.d(TAG, " onUpdateMsg 列表没有这条数据 添加这条数据 : ");
                        lastMsgWithUnreadList.add(msgTable);
                    }
                }

            }

            if (lastMsgWithUnreadList.size() == 0) {
                lastMsgWithUnreadList.add(msgTable);
            }

            L.d(TAG, " onUpdateMsg notify : " + msgTable.unreadCount);

            if (!isDestroyed()) {

                runOnUiThread(new Runnable() {
                    @Override public void run() {

                        L.d(TAG, " onUpdateMsg adapter : " + adapter);

                        if (adapter != null && lastMsgWithUnreadList.size() > 0) {
                            adapter.refreshAdapter(lastMsgWithUnreadList);
                            adapter.notifyDataSetChanged();
                            bg_messages_default.setVisibility(View.GONE);
                        }

                    }
                });
            }

        }
    };

    private void showConnectStatusUI(int status) {
        switch (status) {
            case ChatService.ConnectStatus.STOP:
            case ChatService.ConnectStatus.FAILED:
            case ChatService.ConnectStatus.ERROR:
                txt_xmp_status_title.setText(StrangerMsgActivity.this.getText(R.string.not_cennet));
                not_connet_image.setVisibility(View.VISIBLE);
                break;
            case ChatService.ConnectStatus.CONNECTED:
                not_connet_image.setVisibility(View.GONE);
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        txt_xmp_status_title.setText(TheLApp.context.getResources().getText(R.string.get_messages));
                        txt_xmp_status_title.setText(TheLApp.context.getResources().getText(R.string.stranger));

                    }
                }, 500);
                break;
            case ChatService.ConnectStatus.RECONNECT:
            case ChatService.ConnectStatus.CONNECTING:
            default:
                not_connet_image.setVisibility(View.GONE);
                txt_xmp_status_title.setText(StrangerMsgActivity.this.getText(R.string.message_connecting));
                break;
        }
    }

    private void getStrangerList() {

        List<MsgTable> msgTables = ChatServiceManager.getInstance().getMsgList(1);

        if (msgTables == null || msgTables.size() == 0) {
            return;
        }

        Flowable<List<MsgTable>> flowable = Flowable.just(msgTables);

        flowable.onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<MsgTable>>() {
                    @Override public void accept(final List<MsgTable> strangerTables) {

                        if (!isDestroyed()) {

                            if (strangerTables != null && lastMsgWithUnreadList != null) {

                                lastMsgWithUnreadList.clear();
                                lastMsgWithUnreadList.addAll(strangerTables);
                                if (null == adapter) {
                                    adapter = new MessagesAdapter(lastMsgWithUnreadList, StrangerMsgActivity.this);
                                    listview.setAdapter(adapter);
                                } else {
                                    adapter.refreshAdapter(lastMsgWithUnreadList);
                                    adapter.notifyDataSetChanged();
                                }
                                // 更新UI
                                bg_messages_default.setVisibility(lastMsgWithUnreadList.size() == 0 ? View.VISIBLE : View.GONE);
                                edit_search.setEnabled(lastMsgWithUnreadList.size() != 0);
                                adapter.refreshAdapter(lastMsgWithUnreadList);
                            }
                        }
                    }
                });

    }

    private void reCountStrangerUnReadCount() {

        List<MsgTable> msgTables = ChatServiceManager.getInstance().getMsgList(1);

        if (msgTables == null || msgTables.size() == 0) {
            return;
        }

        Flowable<List<MsgTable>> flowable = Flowable.just(msgTables);

        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<List<MsgTable>>() {
            @Override public void accept(List<MsgTable> msgTables) {

                int unReadCount = 0;

                int isWinked = 0;

                for (MsgTable msgTable : msgTables) {
                    unReadCount += msgTable.unreadCount;
                    isWinked += msgTable.isWinked;
                }

                ChatServiceManager.getInstance().updateStrangerUnreadCount(unReadCount, isWinked);

            }
        });

    }

    private void showDeleteMenuAndReport(MsgTable msgTable) {

        DeleteMsgDialog deleteMsgDialog = new DeleteMsgDialog();

        deleteMsgDialog.setOnDeleteMsgListener(new IDeleteMsg(msgTable));

        deleteMsgDialog.show(getSupportFragmentManager(), DeleteMsgDialog.class.getName());

    }

    public class IDeleteMsg implements DeleteMsgDialog.OnDeleteMsgListener {

        private MsgTable msgTable;

        public IDeleteMsg(MsgTable msgTable) {
            this.msgTable = msgTable;
        }

        @Override public void onConfirm() {

            try {

                MsgTable mt = ChatServiceManager.getInstance().getMsgTableByUserId(msgTable.userId);

                String tableName = DBUtils.getChatTableName(msgTable.userId);
                ChatServiceManager.getInstance().dropTable(tableName);
                ChatServiceManager.getInstance().deleteMsgTable(msgTable.userId);

                if (mt != null) {
                    if (mt.isStranger == 1) {
                        List<MsgTable> msgTables = ChatServiceManager.getInstance().getStrangerMsg();

                        if (msgTables.size() <= 0) {
                            ChatServiceManager.getInstance().removeMsgTable(DBUtils.getStrangerTableName());
                        } else {

                            MsgTable msgTable = msgTables.get(msgTables.size() - 1);

                            MsgTable strangerMsgTable = ChatServiceManager.getInstance().getMsgTableByUserId(DBConstant.Message.STRANGER_TABLE_NAME);

                            strangerMsgTable.userName = msgTable.userName;
                            strangerMsgTable.msgType = msgTable.msgType;
                            strangerMsgTable.msgText = msgTable.msgText;
                            if (strangerMsgTable.msgTime < DBConstant.STICK_TOP_BASE_TIME) {
                                strangerMsgTable.msgTime = msgTable.msgTime;
                            }

                            ChatServiceManager.getInstance().updateMsgTableWithWink(strangerMsgTable);

                            ChatServiceManager.getInstance().pushUpdateMsg(strangerMsgTable);

                        }
                    }

                    lastMsgWithUnreadList.remove(msgTable);

                    adapter.refreshAdapter(lastMsgWithUnreadList);

                    adapter.notifyDataSetChanged();

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override public void onCancel() {

        }
    }

}
