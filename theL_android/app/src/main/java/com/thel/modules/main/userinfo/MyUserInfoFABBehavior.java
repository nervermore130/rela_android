package com.thel.modules.main.userinfo;

import android.content.Context;

import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.view.ViewCompat;
import androidx.core.view.ViewPropertyAnimatorListener;
import androidx.interpolator.view.animation.FastOutSlowInInterpolator;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;

/**
 * Created by the L on 2016/12/15.
 */

public class MyUserInfoFABBehavior extends CoordinatorLayout.Behavior {
    private static final Interpolator INTERPOLATOR = new FastOutSlowInInterpolator();

    public MyUserInfoFABBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onStartNestedScroll(CoordinatorLayout coordinatorLayout, View child, View directTargetChild, View target, int nestedScrollAxes) {
        final boolean start = nestedScrollAxes == ViewCompat.SCROLL_AXIS_VERTICAL || super.onStartNestedScroll(coordinatorLayout, child, directTargetChild, target, nestedScrollAxes);
        return start;
    }

    @Override
    public void onNestedScroll(CoordinatorLayout coordinatorLayout, View child, View target, int dxConsumed, int dyConsumed, int dxUnconsumed, int dyUnconsumed) {
        super.onNestedScroll(coordinatorLayout, child, target, dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed);
        if (child instanceof UserInfoFloatingLayout) {
            final UserInfoFloatingLayout floatingLayout = (UserInfoFloatingLayout) child;
            if (!floatingLayout.isNeverShow())
                if (dyConsumed > 0 && !floatingLayout.mIsAnimatingOut && child.getVisibility() == View.VISIBLE) {
                    animateOut(floatingLayout);
                } else if (dyConsumed < 0 && child.getVisibility() != View.VISIBLE) {
                    animateIn(floatingLayout);
                }
        }
    }

    //移出悬浮窗
    public static void animateOut(final UserInfoFloatingLayout child) {
        ViewCompat.animate(child).translationY(child.getHeight() + getMarginBottom(child)).setInterpolator(INTERPOLATOR).withLayer().setListener(new ViewPropertyAnimatorListener() {
            @Override
            public void onAnimationStart(View view) {
                child.mIsAnimatingOut = true;
            }

            @Override
            public void onAnimationEnd(View view) {
                child.mIsAnimatingOut = false;
                view.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationCancel(View view) {
                child.mIsAnimatingOut = false;
            }
        });
    }

    //悬浮窗进来
    public static void animateIn(UserInfoFloatingLayout child) {
        child.setVisibility(View.VISIBLE);
        ViewCompat.animate(child).translationY(0).setInterpolator(INTERPOLATOR).withLayer().setListener(null);
    }

    static int getMarginBottom(View child) {
        int marginBottom = 0;
        final ViewGroup.LayoutParams param = child.getLayoutParams();
        if (param instanceof ViewGroup.MarginLayoutParams) {
            marginBottom = ((ViewGroup.MarginLayoutParams) param).bottomMargin;
        }
        return marginBottom;
    }


}
