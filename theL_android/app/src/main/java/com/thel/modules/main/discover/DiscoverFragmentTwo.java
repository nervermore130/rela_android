package com.thel.modules.main.discover;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thel.R;
import com.thel.base.BaseFragment;
import com.thel.utils.L;

/**
 * Created by waiarl on 2017/11/6.
 */

public class DiscoverFragmentTwo extends BaseFragment {
    private static DiscoverFragmentTwo instance;
    private FragmentManager manager;
    private LiveRoomsListFragment liveRoomsListFragment;

    public static DiscoverFragmentTwo getInstance(Bundle bundle) {
        instance = new DiscoverFragmentTwo();
        instance.setArguments(bundle);
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_discover, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        L.d("discoverFragment " + "discover: " + view);
        findViewById(view);
        addFragment();

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void findViewById(View view) {

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
       /* if (liveRoomsListFragment != null) {
            liveRoomsListFragment.setVisiable(isVisibleToUser);
        }*/

       L.d("DiscoverFragmentTwo"," setUserVisibleHint isVisibleToUser : " + isVisibleToUser);

    }

    private void addFragment() {
        manager = getChildFragmentManager();
        if (liveRoomsListFragment == null) {
            final Bundle bundle = new Bundle();
            liveRoomsListFragment = LiveRoomsListFragment.getInstance(bundle);
        }
        manager.beginTransaction().replace(R.id.frame, liveRoomsListFragment).commit();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        /*if (liveRoomsListFragment != null) {
            liveRoomsListFragment.setVisiable(!hidden);
        }*/

        L.d("DiscoverFragmentTwo"," setUserVisibleHint hidden : " + hidden);

        if(liveRoomsListFragment!=null){
            liveRoomsListFragment.setUserVisibleHint(!hidden);
        }
    }

    public void showVoice() {
        if (liveRoomsListFragment != null && liveRoomsListFragment.isAdded()) {
            liveRoomsListFragment.showVoice();
        }
    }
}
