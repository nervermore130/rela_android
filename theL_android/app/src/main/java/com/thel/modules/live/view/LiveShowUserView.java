package com.thel.modules.live.view;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

import com.thel.R;
import com.thel.constants.TheLConstants;
import com.thel.modules.live.bean.LiveRoomMsgBean;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;
import com.thel.utils.ViewUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by the L on 2016/12/5.
 */

public class LiveShowUserView extends RecyclerView {
    private final Context mContext;
    private LinearLayoutManager manager;
    private LiveShowUserAdapter mAdapter;
    private List<LiveRoomMsgBean> list = new ArrayList<>();
    private OnItemClickListener itemClickListener;
    private boolean canScroll = true;
    private boolean isDragging = false;
    private int MAX_COUNT = 30;
    private Handler mHandler;
    private static final int CAN_SCROLL = 1;
    private static final long DELAY_DURATION = 2000;


    public LiveShowUserView(Context context) {
        this(context, null);
    }

    public LiveShowUserView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LiveShowUserView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mContext = context;
        init();
        setListener();
    }

    private void init() {
        manager = new LinearLayoutManager(mContext);
        manager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mAdapter = new LiveShowUserAdapter(list);
        setHasFixedSize(true);
        setLayoutManager(manager);
        setAdapter(mAdapter);
        addOnScrollListener(new MyScrollListener());
        mHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case CAN_SCROLL:
                        if (!isDragging) {
                            canScroll = true;
                        }
                        break;
                }
            }
        };
    }

    public LiveShowUserView initView(List<LiveRoomMsgBean> userInfoBeanList) {
        list.clear();
        list.addAll(userInfoBeanList);
        mAdapter.notifyDataSetChanged();
        return this;
    }

    public void joinNewUser(LiveRoomMsgBean bean) {
        if (null == bean) {
            return;
        }
        LiveRoomMsgBean msgBean;
        final int count = list.size();
        for (int i = 0; i < count; i++) {
            msgBean = list.get(i);
            if (msgBean.userId.equals(bean.userId)) {
                list.remove(i);
                break;
            }
        }
        list.add(0, bean);
        while (list.size() > MAX_COUNT) {
            list.remove(MAX_COUNT);
        }
        if (canScroll) {
            mAdapter.notifyDataSetChanged();
            scrollToPosition(0);
        }
    }

    private void setListener() {
        mAdapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                ViewUtils.preventViewMultipleClick(view, 2000);
                final LiveRoomMsgBean bean = mAdapter.getItem(position);
                if (null != bean && null != itemClickListener) {
                    itemClickListener.onItemClick(view, position, bean.userId);
                }
            }
        });
    }

    class LiveShowUserAdapter extends BaseRecyclerViewAdapter<LiveRoomMsgBean> {

        public LiveShowUserAdapter(List<LiveRoomMsgBean> data) {
            super(R.layout.live_show_user_item, data);
        }

        @Override
        protected void convert(BaseViewHolder helper, LiveRoomMsgBean item) {
            helper.setCircleImageViewUrl(R.id.img_avatar, R.mipmap.icon_user, item.avatar, TheLConstants.AVATAR_SMALL_SIZE, TheLConstants.AVATAR_SMALL_SIZE);
        }
    }

    /**
     * item 监听接口
     */
    public interface OnItemClickListener {
        void onItemClick(View view, int position, String userId);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.itemClickListener = listener;
    }

    class MyScrollListener extends OnScrollListener {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                isDragging = true;
                canScroll = false;
                mHandler.removeMessages(CAN_SCROLL);
            } else {
                if (isDragging) {
                    mHandler.sendEmptyMessageDelayed(CAN_SCROLL, DELAY_DURATION);
                }
                isDragging = false;
            }
        }
    }
}
