package com.thel.modules.main.me;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;

import com.thel.app.TheLApp;
import com.thel.base.BaseDataBean;
import com.thel.bean.moments.MomentsCheckBean;
import com.thel.constants.TheLConstants;
import com.thel.imp.picupload.PicUploadContract;
import com.thel.imp.picupload.PicUploadUtils;
import com.thel.modules.main.me.bean.MyInfoNetBean;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.RequestConstants;
import com.thel.network.service.DefaultRequestService;
import com.thel.utils.L;
import com.thel.utils.MD5Utils;
import com.thel.utils.ShareFileUtils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by lingwei on 2017/9/18.
 */

public class MePresenter implements MeContract.Presenter {
    private static final int TYPE_AVATAR = 1;//上传头像
    private static final int TYPE_BG = 2;//上传背景
    private final MeContract.View meView;
    private CompositeDisposable mDisposable;
    private MessageReceiver receiver;
    // 要上传的头像的图片的本地地址
    public String avatarPhotoPath = "";
    // 上传头像图片到七牛后的文件路径
    private String uploadAvatarPath = "";
    // 上传头像图片到七牛后的文件路径
    private String uploadBgPath = "";
    private String imageLocalPath;
    private int failedCount = 0;

    public MePresenter(MeContract.View meView) {
        this.meView = meView;
        meView.setPresenter(this);

    }

    @Override
    public void subscribe() {
    }

    @Override
    public void unSubscribe() {

    }

    @Override
    public void loadNetData() {
        getMyInfoData();
    }


    @Override
    public void registerReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(TheLConstants.BROADCAST_MOMENTS_CHECK_ACTION);
        intentFilter.addAction(TheLConstants.BROADCAST_CLEAR_MOMENT_MSG_COUNT_TOP);
        intentFilter.addAction(TheLConstants.BROADCAST_CLEAR_LIKE_ME_NEW_CHECK_ACTION);
        IntentFilter intentFilter2 = new IntentFilter();
        intentFilter2.addAction(TheLConstants.BROADCAST_UPDATA_RATIO);
        receiver = new MessageReceiver();
        UpdataUserInfoRatioReceiver ratioReceiver = new UpdataUserInfoRatioReceiver();
        TheLApp.context.registerReceiver(receiver, intentFilter);
        TheLApp.context.registerReceiver(ratioReceiver, intentFilter2);
    }

    @Override
    public void handlePhoto(String imagePath) {
        avatarPhotoPath = imagePath;

        if (TextUtils.isEmpty(avatarPhotoPath)) {
            return;
        }
        uploadAvatarPath = RequestConstants.UPLOAD_FILE_ROOT_PATH_AVATAR + ShareFileUtils.getString(ShareFileUtils.ID, "") + "/" + MD5Utils.calculateMD5(new File(avatarPhotoPath)) + ".jpg";
        //  getUploadToken(uploadAvatarPath);
        getUpLoadImage(avatarPhotoPath, uploadAvatarPath, TYPE_AVATAR);
    }

    @Override
    public void uploadImage(String imageLocalPath) {
        if (TextUtils.isEmpty(imageLocalPath)) {
            return;
        }
        this.imageLocalPath = imageLocalPath;
        uploadBgPath = RequestConstants.UPLOAD_FILE_ROOT_PATH_BG_IMG + ShareFileUtils.getString(ShareFileUtils.ID, "") + "/" + MD5Utils.calculateMD5(new File(imageLocalPath)) + ".jpg";
        //  getUploadToken(uploadBgPath);
        getUpLoadImage(imageLocalPath, uploadBgPath, TYPE_BG);
    }

    private void getUpLoadImage(String localPath, String uploadPath, final int type) {
        failedCount = 0;
        PicUploadUtils.uploadPic(localPath, uploadPath, new PicUploadContract.PicUploadCallback() {
            @Override
            public void uploadFailed(int index) {
                if (failedCount == 0) {
                    meView.requestFailed();
                    failedCount += 1;
                }
            }

            @Override
            public void uploadComplete(String imageUrls) {
                L.i("getUpLoadImage", "imageUrls=" + imageUrls + "");
                if (TextUtils.isEmpty(imageUrls)) {
                    meView.requestFailed();
                }
                if (TYPE_AVATAR == type) {
                    uploadAvatar(imageUrls);
                } else if (TYPE_BG == type) {
                    uploadBgImage(imageUrls);
                }

            }

            @Override
            public void uploadVideoComplete(String videoUrl, String videoWebp) {

            }
        }, false);
    }

    //个人资料完成度的广播接收器
    private class UpdataUserInfoRatioReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                if (TheLConstants.BROADCAST_UPDATA_RATIO.equals(intent.getAction())) {//收到更新资料的消息，刷新资料完成度
                    // getMyInfoData();
                    meView.refreshRatioUI();
                }
            }
        }
    }

    // 朋友圈消息的广播接收器
    private class MessageReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                if (TheLConstants.BROADCAST_MOMENTS_CHECK_ACTION.equals(intent.getAction())) {// 收到消息的时候, 更新朋友圈消息
                    try {
                        if (intent.getParcelableExtra(TheLConstants.BUNDLE_KEY_MOMENTS_CHECK_BEAN) != null) {
                            MomentsCheckBean checkBean = intent.getParcelableExtra(TheLConstants.BUNDLE_KEY_MOMENTS_CHECK_BEAN);
                            updateNewVisitorUI(checkBean);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (TheLConstants.BROADCAST_CLEAR_MOMENT_MSG_COUNT_TOP.equals(intent.getAction())) {// 清除朋友圈消息
                    clearMsgRemind();
                } else if (TheLConstants.BROADCAST_CLEAR_LIKE_ME_NEW_CHECK_ACTION.equals(intent.getAction())) { //清除匹配里的喜欢我的数
                    clearMatchRemindNum();
                }
            }
        }

    }

    private void clearMatchRemindNum() {
        meView.clearNewLikeMeNum();
    }


    private void updateNewVisitorUI(MomentsCheckBean checkBean) {
        meView.updateNewVisitorUI(checkBean);

    }

  /*  //在我的页面显示数字提醒
    private void updateMsgRemind() {
        int unreadMomentsMsg = ShareFileUtils.getInt(ShareFileUtils.UNREAD_MOMENTS_MSG_NUM, 0);
        // 如果有未读的朋友圈消息
        if (unreadMomentsMsg > 0) {
            meView.updateMsgRemind(unreadMomentsMsg);
        }
    }*/

    private void clearMsgRemind() {
        meView.clearMsgRemind();
    }

    /**
     * 加载我的信息
     */
    private void getMyInfoData() {
        if (!ShareFileUtils.getBoolean(ShareFileUtils.HAS_LOGGED, false)) {
            return;
        }
        String cursor = ShareFileUtils.getString(ShareFileUtils.MATCH_NEW_LIKE_ME_COUNT, "");

        Flowable<MyInfoNetBean> flowable = DefaultRequestService.createUserRequestService().getMyInfo(cursor);

        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<MyInfoNetBean>() {
            @Override
            public void onNext(MyInfoNetBean myInfoBean) {
                super.onNext(myInfoBean);
                if (myInfoBean != null && myInfoBean.data != null) {
                    meView.refreshUI(myInfoBean.data);
                }
            }

            @Override
            public void onComplete() {
                super.onComplete();
                meView.onRequestFinished();
            }
        });

    }

    private void uploadBgImage(final String bgimage) {
        Flowable<BaseDataBean> flowable = RequestBusiness.getInstance().uploadBgImage(bgimage);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
            @Override
            public void onNext(BaseDataBean baseDataBean) {
                super.onNext(baseDataBean);
                getMyInfoData();
                meView.onRequestFinished();
            }


            @Override
            public void onError(Throwable t) {
                L.d(" onError : " + t.getMessage());
            }

            @Override
            public void onComplete() {
            }
        });
    }

    private void uploadAvatar(final String avatar) {
        Flowable<BaseDataBean> flowable = RequestBusiness.getInstance().uploadAvatar(avatar);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
            @Override
            public void onNext(BaseDataBean baseDataBean) {
                super.onNext(baseDataBean);
                meView.uploadAvatar(avatar);
            }


            @Override
            public void onError(Throwable t) {
                L.d(" onError : " + t.getMessage());
            }

            @Override
            public void onComplete() {
            }
        });
    }

}
