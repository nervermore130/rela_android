package com.thel.modules.main.video_discover.pgc;

/**
 * Created by waiarl on 2018/3/16.
 */

public class PgcJudgeBean {
    public boolean haveUgc = false;

    public boolean havePgc = false;

    public boolean canShow() {
        return haveUgc && havePgc;
    }

    public PgcJudgeBean init() {
        haveUgc = false;
        havePgc = false;
        return this;
    }

}
