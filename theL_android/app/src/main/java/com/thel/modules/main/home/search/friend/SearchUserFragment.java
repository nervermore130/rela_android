package com.thel.modules.main.home.search.friend;

import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.SearchBean;
import com.thel.bean.SearchListBean;
import com.thel.bean.SearchUsersListBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.imp.follow.FollowStatusChangedImpl;
import com.thel.modules.main.home.search.BaseSearchFragment;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.network.RequestBusiness;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.utils.FireBaseUtils;
import com.thel.utils.GsonUtils;
import com.thel.utils.LocationUtils;
import com.thel.utils.ShareFileUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.thel.modules.main.home.community.ThemeSortListFragment.REFRESH_TYPE_ALL;
import static com.thel.modules.main.home.community.ThemeSortListFragment.REFRESH_TYPE_NEXT_PAGE;
import static com.thel.modules.main.home.search.friend.SearchUserAdapter.SEARCH_USER;

public class SearchUserFragment extends BaseSearchFragment implements View.OnClickListener {

    @BindView(R.id.list_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.lin_no_match)
    View lin_no_match;//空白页面显示

    private SearchUserAdapter mAdapter;

    private List<SearchBean> list = new ArrayList<SearchBean>();

    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();
    private SearchHistoryHeadView searchHistoryHeadView;

    public static SearchUserFragment newInstance(String type) {
        SearchUserFragment fragment = new SearchUserFragment();
        Bundle bundle = new Bundle();
        bundle.putString(SEARCH_TYPE, type);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int setContentView() {
        return R.layout.fragment_search_friend;
    }

    @Override
    protected void initView() {

        this.mEmptyView = lin_no_match;
    }

    @Override
    protected void initData() {

        initUserRecycler();
    }

    @Override
    protected void processBusiness(final int page, final int type) {
        super.processBusiness(page, type);

        final AMapLocationClient mLocationClient = new AMapLocationClient(TheLApp.getContext());
        mLocationClient.setLocationListener(new AMapLocationListener() {
            @Override
            public void onLocationChanged(AMapLocation aMapLocation) {

                if (aMapLocation != null && aMapLocation.getErrorCode() == 0) {
                    LocationUtils.saveLocation(aMapLocation.getLatitude(), aMapLocation.getLongitude(), aMapLocation.getCity());
                } else {
                    LocationUtils.saveLocation(0.0, 0.0, "");
                }

                if (!LocationUtils.isHasLocationPermission()) {
                    LocationUtils.saveLocation(0.0, 0.0, "");

                }

                searchNickname(page, type);

                mLocationClient.stopLocation();
                mLocationClient.onDestroy();
            }
        });
        final AMapLocationClientOption mLocationOption = new AMapLocationClientOption();
        mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Battery_Saving);
        mLocationClient.setLocationOption(mLocationOption);
        mLocationClient.startLocation();
    }

    private void searchNickname(int page, int type) {
        showLoading();
        Disposable searchDisposable = RequestBusiness.getInstance().searchNickname(mKey, curPage + "")
                .onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<SearchUsersListBean>() {
                    @Override
                    public void accept(SearchUsersListBean searchUsersListBean) {
                        refreshList(searchUsersListBean);
                        requestFinished();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) {
                        mAdapter.openLoadMore(list.size(), false);
                        lin_no_match.setVisibility(VISIBLE);
                        requestFinished();
                    }
                });

        if (mCompositeDisposable != null) {

            mCompositeDisposable.clear();

            mCompositeDisposable.add(searchDisposable);

        }

    }

    @Override
    public void refresh() {
        super.refresh();
        if (getActivity() != null && isAdded()) {
            if (TextUtils.isEmpty(mKey)) {//如果搜索为空值
                curPage = 1;
                list.clear();
                searchHistoryHeadView.setVisibility(VISIBLE);
                searchHistoryHeadView.refreshData();
                mAdapter.notifyDataSetChanged();
                lin_no_match.setVisibility(GONE);
            } else {
                processBusiness(1, REFRESH_TYPE_ALL);
                searchHistoryHeadView.refreshData();
            }
        }
    }

    private void initUserRecycler() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(TheLApp.getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        //如果可以确定每个item的高度是固定的，设置这个选项可以提高性能
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(null);
        mAdapter = new SearchUserAdapter(R.layout.search_friend_listitem, list, SEARCH_USER);
        searchHistoryHeadView = new SearchHistoryHeadView(getActivity());
        searchHistoryHeadView.setSearchHeadListener(new SearchHistoryHeadView.SearchHeadListener() {
            @Override
            public void historyItemClick(int position, SearchBean bean) {
//                Intent intent = new Intent();
//                intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, bean.userId + "");
//                intent.putExtra("fromPage", SearchUserFragment.class.getName());
//                intent.putExtra("index", position);
//                intent.setClass(getActivity(), UserInfoActivity.class);
//                startActivityForResult(intent, TheLConstants.BUNDLE_CODE_SEARCH_ACTIVITY);
                FlutterRouterConfig.Companion.gotoUserInfo(bean.userId+"");
            }
        });
        mAdapter.addHeaderView(searchHistoryHeadView);
        mRecyclerView.setAdapter(mAdapter);
        searchHistoryHeadView.refreshData();

        mAdapter.setOnLoadMoreListener(new BaseRecyclerViewAdapter.RequestLoadMoreListener() {//加载更多
            @Override
            public void onLoadMoreRequested() {
                mRecyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        if (currentCountForOnce > 0) {
                            processBusiness(curPage, REFRESH_TYPE_NEXT_PAGE);
                        } else {
                            mAdapter.openLoadMore(0, false);
                            if (list.size() > 0) {
                                mAdapter.closeLoadMore(getActivity(), mRecyclerView);
                            }
                        }
                    }
                });
            }
        });
        mAdapter.setReloadMoreListener(new BaseRecyclerViewAdapter.ReloadMoreListener() {//加载更多失败后重新加载
            @Override
            public void reloadMore() {
                refreshType = REFRESH_TYPE_NEXT_PAGE;
                mAdapter.removeAllFooterView();
                mAdapter.openLoadMore(true);
                processBusiness(curPage, REFRESH_TYPE_NEXT_PAGE);
            }


        });
        mAdapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                final SearchBean bean = mAdapter.getData().get(position);

                gotoUserActivity(bean);
            }
        });
        mAdapter.setOnRecyclerViewItemChildClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseRecyclerViewAdapter adapter, View view, int position) {
                switch (view.getId()) {
                    case R.id.txt_follow:
                        final int pos = (int) view.getTag() - adapter.getHeaderLayoutCount();
                        final SearchBean bean = mAdapter.getItem(pos);
                        followUser(bean);//关注/取消关注某个用户
                        break;
                }

            }
        });
    }

    /**
     * 关注/取消关注某个用户
     *
     * @param bean
     */
    private void followUser(final SearchBean bean) {
        if (bean.isFollow == SearchBean.FOLLOW_TYPE_UNFOLLOWED) {
            showLoading();

            FollowStatusChangedImpl.followUser(bean.userId + "", ACTION_TYPE_FOLLOW, bean.userName, bean.avatar);
            FireBaseUtils.uploadGoogle(TheLConstants.FireBaseConstant.ATTENTION, TheLApp.context);

        }
    }

    private void gotoUserActivity(SearchBean bean) {
        String json = ShareFileUtils.getString(TheLConstants.SEARCH_HISTORY_JSON, "");
        SearchListBean searchListBean = GsonUtils.getObject(json, SearchListBean.class);
        if (searchListBean == null)
            searchListBean = new SearchListBean();
        boolean isExist = false;
        for (SearchBean searchBean : searchListBean.list) {
            if (searchBean.userId == bean.userId) {
                isExist = true;
                break;
            }
        }
        if (!isExist) {
            searchListBean.list.add(0, bean);
            //最多只保存20位搜索用户
            if (searchListBean.list.size() > 20) {
                for (int i = 20; i < searchListBean.list.size(); i++) {
                    searchListBean.list.remove(i);
                }
            }
            String toJson = GsonUtils.createJsonString(searchListBean);
            ShareFileUtils.setString(TheLConstants.SEARCH_HISTORY_JSON, toJson);
        }

//        Intent intent = new Intent();
//        intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, bean.userId + "");
//        intent.putExtra("fromPage", SearchUserFragment.class.getName());
//        intent.setClass(getActivity(), UserInfoActivity.class);
//        startActivityForResult(intent, TheLConstants.BUNDLE_CODE_SEARCH_ACTIVITY);
        FlutterRouterConfig.Companion.gotoUserInfo(bean.userId+"");
    }

    private void refreshList(SearchUsersListBean searchListBean) {
        if (refreshType == REFRESH_TYPE_ALL) {
            list.clear();
        }
        if (searchListBean != null && searchListBean.userList != null) {
            currentCountForOnce = searchListBean.userList.size();
            list.addAll(searchListBean.userList);
        }
        if (refreshType == REFRESH_TYPE_ALL) {
            mAdapter.removeAllFooterView();
            mAdapter.setNewData(list);
            curPage = 2;
            if (list.size() > 0) {
                searchHistoryHeadView.setVisibility(GONE);
                mAdapter.openLoadMore(list.size(), true);
                lin_no_match.setVisibility(GONE);
            } else {
                searchHistoryHeadView.setVisibility(VISIBLE);
                mAdapter.openLoadMore(list.size(), false);
                lin_no_match.setVisibility(VISIBLE);
            }
        } else {
            curPage++;
            mAdapter.notifyDataChangedAfterLoadMore(true, list.size());
        }
        if (refreshType == REFRESH_TYPE_ALL) {
            if (mRecyclerView.getChildCount() > 0) {// 回到顶部
                mRecyclerView.scrollToPosition(0);
            }
        }
    }

    private void requestFinished() {

        closeLoading();
    }

    /**
     * 改变某个用户的关注状态
     *
     * @param followUserId 关注的用户的id
     * @param followStatus
     */
    private void changeFollowStatus(String followUserId, int followStatus) {
        final int count = mAdapter.getData().size();
        for (int i = 0; i < count; i++) {
            SearchBean searchBean = mAdapter.getItem(i);
            if (followUserId.equals(searchBean.userId + "")) {
                UpdateSearchBean(searchBean, true, followStatus);
                mAdapter.notifyItemChanged(i + mAdapter.getHeaderLayoutCount());
                break;
            }
        }
    }

    /**
     * 更新某个人的关注状态
     *
     * @param bean         要改变的人
     * @param sendPush     是否发送push
     * @param followStatus
     */
    private void UpdateSearchBean(SearchBean bean, boolean sendPush, int followStatus) {
        if (null != bean) {
            bean.isFollow = followStatus;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

        }
    }

    @Override
    public void onFollowStatusChanged(int followStatus, String userId, String nickName, String avatar) {
        super.onFollowStatusChanged(followStatus, userId, nickName, avatar);
        changeFollowStatus(userId, followStatus);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mCompositeDisposable != null) {
            mCompositeDisposable.clear();
        }
    }
}
