package com.thel.modules.main.userinfo.moment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseAdapter;
import com.thel.bean.gift.WinkCommentBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.moments.MomentsListBean;
import com.thel.constants.BundleConstants;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.growingio.GrowingIoConstant;
import com.thel.imp.AutoRefreshImp;
import com.thel.imp.BaseFunctionFragment;
import com.thel.manager.ListVideoVisibilityManager;
import com.thel.modules.main.MainActivity;
import com.thel.modules.main.home.ReleaseActivity;
import com.thel.modules.main.home.moments.ReleaseMomentActivity;
import com.thel.modules.main.home.moments.comment.MomentCommentActivity;
import com.thel.modules.main.home.moments.theme.ThemeDetailActivity;
import com.thel.modules.main.home.moments.web.WebViewActivity;
import com.thel.modules.main.me.aboutMe.MatchActivity;
import com.thel.modules.main.userinfo.MyLinearLayoutManager;
import com.thel.modules.main.userinfo.UserInfoContract;
import com.thel.modules.others.VipConfigActivity;
import com.thel.ui.adapter.MomentsAdapter;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.decoration.DefaultItemDivider;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;

import java.util.ArrayList;
import java.util.List;

import static com.thel.utils.ShareFileUtils.IS_RELEASE_MOMENT;

/**
 * Created by waiarl on 2018/4/17.
 */

public class UserInfoMomentFragment extends BaseFunctionFragment implements UserInfoContract.MomentView, AutoRefreshImp {


    private int type = TYPE_MOMENT;
    private UserInfoContract.MomentPresenter presenter;
    private RecyclerView recyclerview;
    private MyLinearLayoutManager manager;
    private MomentsAdapter adapter;
    private List<MomentsBean> momentsBeanList = new ArrayList<>();
    private boolean isFirstCreate = true;
    private String userId;
    private int curPage;
    private boolean haveNextPage = true;
    private MomentNumCallback momentNumCallback;
    private RelativeLayout layout_moment_empty;
    private TextView tx_post_moment;
    private ImageView iv_moment_empty_icon;
    private TextView tv_post_moment_text;

    public static UserInfoMomentFragment getInstance(Bundle bundle) {
        final UserInfoMomentFragment fragment = new UserInfoMomentFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Bundle bundle = getArguments();
        userId = bundle.getString(TheLConstants.BUNDLE_KEY_USER_ID);
        type = bundle.getInt(TheLConstants.BUNDLE_KEY_ACTION_TYPE, TYPE_MOMENT);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_user_info_moment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter = new UserInfoMomentPresenter(this);
        findViewById(view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        presenter.register(getContext());
        adapter.registerNewCommentNotify(getActivity());
        setListener();
        if (getUserVisibleHint()) {
            initData();
        }
    }

    @Override
    public void setPresenter(UserInfoContract.MomentPresenter presenter) {

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isFirstCreate && presenter != null) {
            initData();
        }
    }

    private void findViewById(View view) {
        recyclerview = view.findViewById(R.id.recyclerview);
        manager = new MyLinearLayoutManager(getContext());
        manager.setOrientation(RecyclerView.VERTICAL);
        recyclerview.setLayoutManager(manager);
        recyclerview.setHasFixedSize(true);
        adapter = new MomentsAdapter(R.layout.item_moments, momentsBeanList, GrowingIoConstant.ENTRY_PERSONAL_PAGE);
        adapter.setLayoutManager(manager);
        recyclerview.setAdapter(adapter);
        recyclerview.addItemDecoration(new DefaultItemDivider(TheLApp.context, LinearLayoutManager.VERTICAL, R.color.gray, (int) TheLApp.getContext().getResources().getDimension(R.dimen.wide_divider_height), true, false));
        layout_moment_empty = view.findViewById(R.id.layout_moment_empty);
        tx_post_moment = view.findViewById(R.id.tx_post_moment);

        iv_moment_empty_icon = view.findViewById(R.id.iv_moment_empty_icon);
        tv_post_moment_text = view.findViewById(R.id.tv_post_moment_text);
        if (!TextUtils.isEmpty(userId) && userId.equals(UserUtils.getMyUserId())) {
            tv_post_moment_text.setVisibility(View.VISIBLE);
            if (type == TYPE_VIDEO) {
                tx_post_moment.setText(R.string.post_moment_video_hint);
            } else {
                tx_post_moment.setText(R.string.post_moment_hint);
            }
        } else {
            tv_post_moment_text.setVisibility(View.GONE);
            if (type == TYPE_VIDEO) {
                tx_post_moment.setText(R.string.no_videos);
            } else {
                tx_post_moment.setText(R.string.no_moments);
            }
        }
    }

    private void setListener() {
        adapter.setOnItemClickListener(new BaseAdapter.OnItemClickListener<MomentsBean>() {
            @Override
            public void onItemClick(View view, MomentsBean item, int position) {

                Intent intent;

                Bundle bundle = new Bundle();

                if (MomentsBean.MOMENT_TYPE_THEME.equals(item.momentsType)) {
//                    intent = new Intent(getContext(), ThemeDetailActivity.class);
//                    if (item.parentMoment != null) {
//                        intent.putExtra(BundleConstants.THEMEPARTICIPATES, String.valueOf(item.parentMoment.joinTotal));
//                    } else {
//                        intent.putExtra(BundleConstants.THEMEPARTICIPATES, String.valueOf(item.joinTotal));
//                    }
//
//                    bundle.putSerializable(TheLConstants.BUNDLE_KEY_MOMENT_BEAN, item);
//                    intent.putExtras(bundle);
//
//                    if (getActivity() != null) {
//                        getActivity().startActivityForResult(intent, TheLConstants.BUNDLE_CODE_MOMENTS_ACTIVITY);
//                    }
                    FlutterRouterConfig.Companion.gotoThemeDetails(item.momentsId);
                } else if (MomentsBean.MOMENT_TYPE_AD.equals(item.momentsType)) {
                    if (item.appSchemeUrl != null) {

                        if (TheLConstants.SchemeConstant.SCHEME_PATH_VIP.equals(item.appSchemeUrl)) {

                            intent = new Intent(getContext(), VipConfigActivity.class);

                            if (getActivity() != null) {
                                getActivity().startActivityForResult(intent, TheLConstants.BUNDLE_CODE_MOMENTS_ACTIVITY);
                            }

                        } else if (TheLConstants.SchemeConstant.SCHEME_PATH_MATCH.equals(item.appSchemeUrl)) {

                            intent = new Intent(getContext(), MatchActivity.class);

                            if (getActivity() != null) {
                                getActivity().startActivityForResult(intent, TheLConstants.BUNDLE_CODE_MOMENTS_ACTIVITY);
                            }

                        } else if (TheLConstants.SchemeConstant.SCHEME_PATH_THEME.equals(item.appSchemeUrl)) {

                            if (getActivity() != null) {

                                intent = new Intent(view.getContext(), MainActivity.class);

                                intent.putExtra(TheLConstants.BUNDLE_KEY_WHERE_TO_GO, TheLConstants.MainFragmentPageConstants.FRAGMENT_HOME);

                                intent.putExtra(TheLConstants.BUNDLE_KEY_CHILD_PAGE, TheLConstants.MainFragmentPageConstants.FRAGMENT_THEME);

                                getActivity().startActivity(intent);

                            }


                        } else if (item.appSchemeUrl.contains(TheLConstants.SchemeConstant.SCHEME_PATH_THEMEDETAIL)) {

                            Uri uri = Uri.parse(item.appSchemeUrl);

                            String momentsId = uri.getQueryParameter("momentId");

//                            intent = new Intent(getContext(), ThemeDetailActivity.class);
//
//                            intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, momentsId);
//
//                            intent.putExtras(bundle);
//
//                            if (getActivity() != null) {
//                                getActivity().startActivityForResult(intent, TheLConstants.BUNDLE_CODE_MOMENTS_ACTIVITY);
//                            }
                            FlutterRouterConfig.Companion.gotoThemeDetails(momentsId);

                        } else if (item.appSchemeUrl.contains(TheLConstants.SchemeConstant.SCHEME_PATH_MOMENTDETAIL)) {

                            Uri uri = Uri.parse(item.appSchemeUrl);

                            String momentsId = uri.getQueryParameter("momentId");

//                            intent = new Intent(getContext(), MomentCommentActivity.class);
//
//                            bundle.putSerializable(TheLConstants.BUNDLE_KEY_MOMENT_ID, momentsId);
//                            intent.putExtras(bundle);
//                            if (getActivity() != null) {
//                                getActivity().startActivityForResult(intent, TheLConstants.BUNDLE_CODE_MOMENTS_ACTIVITY);
//                            }
                            FlutterRouterConfig.Companion.gotoMomentDetails(momentsId);
                        } else {
                            bundle.putString(BundleConstants.URL, item.adUrl);
                            intent = new Intent(getContext(), WebViewActivity.class);
                            intent.putExtras(bundle);
                            if (getActivity() != null) {
                                getActivity().startActivityForResult(intent, TheLConstants.BUNDLE_CODE_MOMENTS_ACTIVITY);
                            }
                        }
                    } else {
                        bundle.putString(BundleConstants.URL, item.adUrl);
                        intent = new Intent(getContext(), WebViewActivity.class);
                        intent.putExtras(bundle);
                        if (getActivity() != null) {
                            getActivity().startActivityForResult(intent, TheLConstants.BUNDLE_CODE_MOMENTS_ACTIVITY);
                        }
                    }

                } else {
//                    intent = new Intent(getContext(), MomentCommentActivity.class);
//                    bundle.putSerializable(TheLConstants.BUNDLE_KEY_MOMENT_BEAN, item);
//                    intent.putExtras(bundle);
//                    if (getActivity() != null) {
//                        getActivity().startActivityForResult(intent, TheLConstants.BUNDLE_CODE_MOMENTS_ACTIVITY);
//                    }
                    FlutterRouterConfig.Companion.gotoMomentDetails(item.momentsId);
                }


            }

        });
        adapter.setOnLoadMoreListener(new BaseRecyclerViewAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                if (haveNextPage) {
                    presenter.getLoadMoreListData(userId, curPage, type);
                } else {
                    adapter.closeLoadMore(getActivity(), recyclerview);
                }
            }
        });
        adapter.setReloadMoreListener(new BaseRecyclerViewAdapter.ReloadMoreListener() {
            @Override
            public void reloadMore() {
                adapter.removeAllFooterView();
                adapter.openLoadMore(true);
                presenter.getLoadMoreListData(userId, curPage, type);
            }
        });
        recyclerview.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {

                    if (adapter != null && adapter.getData() != null) {

                        ListVideoVisibilityManager.getInstance().calculatorItemPercent(manager, newState, adapter, TheLConstants.EntryConstants.ENTRY_USER);
                    }

                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

            }
        });
        /**
         * 当前日志或者视频没有数据  点击跳转到发布日志页面
         * **/
        tv_post_moment_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);

                if (type == 0) {  //日志
                    //打开发布日志的弹窗
                    if (UserUtils.isVerifyCell()) {
//                        UserInfoUtils.showReleaseMomentDialog(getActivity());
                        if (getActivity() != null) {
                            getActivity().startActivity(new Intent(getActivity(), ReleaseActivity.class));
                        }
                    }
                } else {
                    if (UserUtils.isVerifyCell()) {
                        ShareFileUtils.setBoolean(Utils.getMyUserId() + "_" + IS_RELEASE_MOMENT, true);
                        Intent intent = new Intent(TheLApp.getContext(), ReleaseMomentActivity.class);
                        intent.putExtra(TheLConstants.BUNDLE_KEY_RELEASE_TYPE, ReleaseMomentActivity.RELEASE_TYPE_VIDEO);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        TheLApp.getContext().startActivity(intent);
                    }
                }
            }
        });
    }

    private void initData() {
        isFirstCreate = false;
        refreshData();
    }

    @Override
    public void refreshData() {

        L.d("UserInfoBlack", " userId : " + userId);

        presenter.getRefreshMomentListData(userId, type);
    }

    @Override
    public void showRefreshListData(MomentsListBean momentsListBean) {
        curPage = 2;
        haveNextPage = momentsListBean.haveNextPage;
        this.momentsBeanList.clear();
        this.momentsBeanList.addAll(momentsListBean.momentsList);
        adapter.setNewData(momentsBeanList);
        if (Utils.getMyUserId().equals(userId) && momentsBeanList.size() == 0) {
            layout_moment_empty.setVisibility(View.VISIBLE);
            if (type == 0) {  //日志
                iv_moment_empty_icon.setImageResource(R.mipmap.bg_moment_empty);
                tv_post_moment_text.setText(TheLApp.context.getString(R.string.post_your_first_moment));
            } else {  //视频
                iv_moment_empty_icon.setImageResource(R.mipmap.bg_video_empty);
                tv_post_moment_text.setText(TheLApp.context.getString(R.string.post_your_first_video));
            }
        } else {
            adapter.openLoadMore(momentsBeanList.size(), true);

        }
        adapter.removeAllFooterView();

        if (adapter != null && adapter.getData() != null && manager != null) {

            ListVideoVisibilityManager.getInstance().calculatorItemPercent(manager, RecyclerView.SCROLL_STATE_IDLE, adapter, TheLConstants.EntryConstants.ENTRY_FOLLOWING);

        }

        if (momentsBeanList.size() > 0) {
            layout_moment_empty.setVisibility(View.GONE);
        } else {
            layout_moment_empty.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void showLoadMoreListData(MomentsListBean momentsListBean) {
        haveNextPage = momentsListBean.haveNextPage;
        this.momentsBeanList.addAll(momentsListBean.momentsList);
        adapter.notifyDataChangedAfterLoadMore(true, this.momentsBeanList.size());
        curPage += 1;
    }

    @Override
    public void emptyData(boolean empty) {
        if (empty) {
            layout_moment_empty.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void refreshMomentNum(int momentsTotalNum, int type) {
        if (momentNumCallback != null) {
            momentNumCallback.getMomentNum(momentsTotalNum, type);
        }
    }

    @Override
    public void loadMoreFailed() {
        recyclerview.post(new Runnable() {
            @Override
            public void run() {
                adapter.loadMoreFailed((ViewGroup) recyclerview.getParent());
            }
        });
    }

    @Override
    public void requestFinish() {

    }


    @Override
    public void onDestroy() {
        try {
            adapter.unregisterNewCommentNotify(getActivity());
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (getContext() != null) {
            presenter.unRegister(getContext());
        }

        super.onDestroy();

    }

    @Override
    public void tryRefreshData() {
        if (getUserVisibleHint()) {
            refreshData();
        }
    }

    public interface MomentNumCallback {
        void getMomentNum(int num, int type);
    }

    public void setMomentNumCallback(MomentNumCallback callback) {
        this.momentNumCallback = callback;
    }


    @Override
    public void onMomentDelete(String momentId) {
        super.onMomentDelete(momentId);
        if (TextUtils.isEmpty(momentId)) {
            return;
        }
        final List<MomentsBean> list = adapter.getData();
        final int size = list.size();
        for (int i = 0; i < size; i++) {
            if (list.get(i).equals(momentId)) {
                adapter.remove(i);
                return;
            }
        }
    }

    @Override
    public void onFollowStatusChanged(int followStatus, String userId, String nickName, String avatar) {
        super.onFollowStatusChanged(followStatus, userId, nickName, avatar);
        if (this.userId.equals(userId)) {
            if (adapter != null) {
                for (MomentsBean bean : adapter.getData()) {
                    bean.followerStatus = followStatus;
                }
                adapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onLikeStatusChanged(String momentId, boolean like, String myUserId, WinkCommentBean myWinkUserBean) {
        super.onLikeStatusChanged(momentId, like, myUserId, myWinkUserBean);

    }


    @Override
    public void onResume() {
        super.onResume();

        if (adapter != null && adapter.getData() != null && manager != null) {

            ListVideoVisibilityManager.getInstance().calculatorItemPercent(manager, RecyclerView.SCROLL_STATE_IDLE, adapter, TheLConstants.EntryConstants.ENTRY_FOLLOWING);

        }

    }

    public void stickStatusChange(String momentsId, int stick_status) {

        if (adapter == null) {
            return;
        }

        List<MomentsBean> momentsBeans = adapter.getData();

        for (int i = 0; i < momentsBeans.size(); i++) {
            MomentsBean momentsBean = momentsBeans.get(i);
            if (momentsId.equals(momentsBean.momentsId)) {
                momentsBean.userBoardTop = stick_status;
                momentsBeans.add(0, momentsBeans.remove(i));
                break;
            } else {
                momentsBean.userBoardTop = 0;
            }

        }
        adapter.notifyDataSetChanged();
    }
}
