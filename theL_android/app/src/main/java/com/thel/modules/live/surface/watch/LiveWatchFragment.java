package com.thel.modules.live.surface.watch;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.thel.R;
import com.thel.base.BaseFragment;
import com.thel.constants.TheLConstants;
import com.thel.modules.live.bean.LiveRoomBean;
import com.thel.modules.live.in.FragmentCreatedCallback;
import com.thel.modules.live.in.LiveBaseView;
import com.thel.modules.live.in.LiveShowCtrlListener;
import com.thel.modules.live.in.LiveSwitchListener;
import com.umeng.analytics.MobclickAgent;

/**
 * Created by lingwei on 2018/5/6.
 */

public class LiveWatchFragment extends BaseFragment implements LiveShowCtrlListener {
    public static final String TAG = LiveWatchFragment.class.getSimpleName();
    public LiveWatchViewFragment liveWatchViewFragment;
    private FragmentCreatedCallback fragmentCreatedCallback;
    private LiveWatchMsgClientCtrl liveShowMsgClentCtrl;
    private LiveWatchObserver observer;
    private LiveSwitchListener liveSwitchListener;
    private LiveWatchKsyStreamFragment liveShowVideoFragment;
    private LiveWatchMultiSoundStreamFragment mulitLiveAgroaFragment;

    public static LiveWatchFragment getInstance(Bundle bundle) {
        LiveWatchFragment instance = new LiveWatchFragment();
        instance.setArguments(bundle);
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_live_show, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initFragment();
        setListener();
        if (fragmentCreatedCallback != null) {
            fragmentCreatedCallback.created();
        }
    }

    private void initFragment() {
        final Bundle bundle = new Bundle();
        liveWatchViewFragment = new LiveWatchViewFragment();
        liveWatchViewFragment.setArguments(bundle);
        liveShowMsgClentCtrl = new LiveWatchMsgClientCtrl();
        observer = new LiveWatchObserver();
        observer.bindILive(liveWatchViewFragment);
        observer.bindILiveAudience(liveWatchViewFragment);
        liveWatchViewFragment.bindObserver(observer);
        liveShowMsgClentCtrl.bindObserver(observer);

        getChildFragmentManager().beginTransaction().add(R.id.frame_live_show_view, liveWatchViewFragment, LiveWatchViewFragment.class.getName()).commit();

        LiveRoomBean liveRoomBean = new LiveRoomBean();
        liveRoomBean.isMulti = LiveRoomBean.SINGLE_LIVE;
        switchLiveStream(liveRoomBean);
    }

    /**
     * 切换直播模式(金山推流和声网推流切换)
     */
    private void switchLiveStream(LiveRoomBean liveRoomBean) {
        //todo Caused by java.lang.IllegalStateException Can not perform this action after onSaveInstanceState
        try {
            if (liveRoomBean != null) {
                final Bundle bundle = new Bundle();
                bundle.putSerializable(TheLConstants.BUNDLE_KEY_LIVE_ROOM, liveRoomBean);
                if (liveRoomBean.audioType == LiveRoomBean.TYPE_VOICE && liveRoomBean.isMulti == LiveRoomBean.MULTI_LIVE) {//多人声音直播
                    if (mulitLiveAgroaFragment == null) {
                        mulitLiveAgroaFragment = LiveWatchMultiSoundStreamFragment.newInstance(bundle);
                        mulitLiveAgroaFragment.bindObserver(observer);
                        FragmentManager childFragmentManager = getChildFragmentManager();
                        FragmentTransaction fragmentTransaction = childFragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.frame_live_show_video, mulitLiveAgroaFragment, LiveWatchMultiSoundStreamFragment.class.getName()).commit();
                        childFragmentManager.executePendingTransactions();
                        liveShowVideoFragment = null;
                        observer.bindVideo(mulitLiveAgroaFragment);
                    }
                } else {
                    if (liveShowVideoFragment == null) {
                        liveShowVideoFragment = LiveWatchKsyStreamFragment.getInstance(bundle);
                        liveShowVideoFragment.bindObserver(observer);
                        FragmentManager childFragmentManager = getChildFragmentManager();
                        FragmentTransaction fragmentTransaction = childFragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.frame_live_show_video, liveShowVideoFragment).commit();
                        childFragmentManager.executePendingTransactions();
                        mulitLiveAgroaFragment = null;
                        observer.bindVideo(liveShowVideoFragment);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setListener() {
        liveWatchViewFragment.setLiveSwitchListener(liveSwitchListener);
    }

    @Override
    public void initLiveShow() {
        if (liveShowVideoFragment != null) {
            liveShowVideoFragment.initLiveShow();
        }
        if (liveWatchViewFragment != null) {
            liveWatchViewFragment.initLiveShow();
        }
        liveShowMsgClentCtrl.reset();
    }

    @Override
    public void refreshLiveShow(final LiveRoomBean liveRoomBean) {
        switchLiveStream(liveRoomBean);
        showLiveView(true);
        if (liveShowVideoFragment != null) {
            liveShowVideoFragment.refreshLiveShow(liveRoomBean);
        }
        if (mulitLiveAgroaFragment != null) {
            mulitLiveAgroaFragment.refreshLiveShow(liveRoomBean);
        }
        if (liveWatchViewFragment != null) {
            liveWatchViewFragment.refreshLiveShow(liveRoomBean);
        }
        if (liveShowMsgClentCtrl != null) {
            liveShowMsgClentCtrl.refreshClient(liveRoomBean);
        }

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (liveShowVideoFragment != null) {
            liveShowVideoFragment.dispatchTouchEvent(ev);
        }
        if (liveWatchViewFragment != null) {
            liveWatchViewFragment.dispatchTouchEvent(ev);
        }
        return false;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (liveShowVideoFragment != null) {
            liveShowVideoFragment.onKeyDown(keyCode, event);
        }
        if (liveWatchViewFragment != null) {
            return liveWatchViewFragment.onKeyDown(keyCode, event);
        }
        return true;
    }

    @Override
    public boolean showLiveView(boolean show) {
        // frame_live_show_view.setVisibility(show ? View.VISIBLE : View.GONE);
        if (liveWatchViewFragment != null) {
            if (show) {
                ((LiveBaseView) liveWatchViewFragment).show();
            } else {
                ((LiveBaseView) liveWatchViewFragment).hide();
            }
        }
        return show;
    }

    @Override
    public void VideoPrepared() {

    }


    public void setFragmentCreatedCallback(FragmentCreatedCallback callback) {
        this.fragmentCreatedCallback = callback;
    }

    @Override
    public void onDestroy() {
        if (observer != null) {
            observer.onDestroy();
        }
        if (liveShowMsgClentCtrl != null) {
            liveShowMsgClentCtrl.onDestroy();
        }
        observer = null;
        super.onDestroy();
    }

    /**
     * 设置跳转监听
     */
    public void setLiveSwitchListener(LiveSwitchListener liveSwitchListener) {
        this.liveSwitchListener = liveSwitchListener;
    }

    public LiveWatchMsgClientCtrl getLiveWatchMsgClientCtrl() {
        return liveShowMsgClentCtrl;
    }

    @Override
    public void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(this.getClass().getName());
        MobclickAgent.onResume(getContext());
    }

    @Override
    public void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(this.getClass().getName());
        MobclickAgent.onPause(getContext());
    }
}
