package com.thel.modules.select_image;

import android.content.Context;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.FitCenter;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.ui.widget.SquareImageView;
import com.thel.utils.L;
import com.thel.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import video.com.relavideolibrary.Utils.DensityUtils;

/**
 * 选择图片adaper
 *
 * @author Setsail
 */
public class SelectLocalImagesGridAdapter extends BaseAdapter {

    /**
     * 所有的图片
     */
    private List<ImageBean> mImgs;

    private LayoutInflater mInflater;

    private Context context;

    private int selectAmountLimit;

    private DisplayMetrics dm = TheLApp.getContext().getApplicationContext().getResources().getDisplayMetrics();
    private int size = (dm.widthPixels - Utils.dip2px(TheLApp.getContext(), 12)) / 3;

    public SelectLocalImagesGridAdapter(Context context, int selectAmountLimit) {
        this.selectAmountLimit = selectAmountLimit;
        this.context = context;
        this.mImgs = new ArrayList<>();
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    /**
     * 刷新列表
     */
    public void setData(List<ImageBean> mDatas) {
        if (mDatas != null) this.mImgs = mDatas;
        notifyDataSetChanged();
    }

    public List<ImageBean> getData() {
        return mImgs;
    }

    @Override
    public int getCount() {
        return mImgs.size();
    }

    @Override
    public Object getItem(int position) {
        if (position == 0) {
            return "";
        } else {
            return mImgs.get(position);
        }
    }

    @Override
    public long getItemId(int arg0) {
        return 0;
    }

    private HoldView holdView = null;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        holdView = null;
        int type = getItemViewType(position);

        if (type == TYPE_CAMERA) {
            convertView = mInflater.inflate(R.layout.select_local_images_grid_item, parent, false);
            convertView.setTag(null);
            SquareImageView imageView = convertView.findViewById(R.id.id_item_image);
            RequestOptions options = new RequestOptions().transform(new FitCenter(), new RoundedCorners(DensityUtils.dp2px(4)));
            Glide.with(context).load(R.mipmap.release_addpage_btn_shoot_photo).apply(options).into(imageView);
            convertView.findViewById(R.id.id_item_select).setVisibility(View.GONE);
        } else if (type == TYPE_NORMAL) {
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.select_local_images_grid_item, parent, false);
                holdView = new HoldView(convertView);
            } else {
                holdView = (HoldView) convertView.getTag();
                if (holdView == null) {
                    convertView = mInflater.inflate(R.layout.select_local_images_grid_item, parent, false);
                    holdView = new HoldView(convertView);
                }
            }

            final ImageBean imageBean = mImgs.get(position);

            L.d("ImageBean", " ImageBean toString : " + imageBean.toString());

            holdView.id_item_select.setVisibility(View.VISIBLE);

            ArrayList<ImageBean> mSelectedImgs = ((SelectLocalImagesActivity) context).getmSelectedImgs();
            boolean select = false;
            int selectIndex = 0;
            for (int i = 0; i < mSelectedImgs.size(); i++) {
                if (mSelectedImgs.get(i).imageName.equals(imageBean.imageName)) {
                    select = true;
                    selectIndex = i;
                    break;
                }
            }
            if (selectAmountLimit == mSelectedImgs.size()) {
                if (select) {
                    holdView.itemview.setAlpha(1f);
                    holdView.itemview.setEnabled(true);
                } else {
                    holdView.itemview.setAlpha(0.5f);
                    holdView.itemview.setEnabled(false);
                }
            } else {
                holdView.itemview.setAlpha(1f);
                holdView.itemview.setEnabled(true);
            }

            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) holdView.id_item_image.getLayoutParams();
            layoutParams.width = size;
            layoutParams.height = size;
            holdView.id_item_image.setLayoutParams(layoutParams);

            if (!TextUtils.isEmpty(imageBean.imageName)) {
                RequestOptions options = new RequestOptions().placeholder(R.mipmap.pictures_no).transform(new CenterCrop(), new RoundedCorners(DensityUtils.dp2px(4)));
                Glide.with(context).load(imageBean.imageName).apply(options).into(holdView.id_item_image);
                if (select) {
                    holdView.id_item_select.setBackgroundResource(R.drawable.circle_main_color);
                    holdView.id_item_select.setText(String.valueOf(selectIndex + 1));
                } else {
                    holdView.id_item_select.setBackgroundResource(R.mipmap.release_addpage_check_nor);
                    holdView.id_item_select.setText("");
                }
            }
        }

        return convertView;
    }

    private static final int TYPE_CAMERA = 0;
    private static final int TYPE_NORMAL = 1;

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_CAMERA;
        }
        return TYPE_NORMAL;
    }

    class HoldView {
        ImageView id_item_image;
        TextView id_item_select;
        RelativeLayout itemview;

        HoldView(View itemView) {
            itemview = itemView.findViewById(R.id.itemView);
            id_item_image = itemView.findViewById(R.id.id_item_image);
            id_item_select = itemView.findViewById(R.id.id_item_select);
            itemView.setTag(this);
        }

    }
}
