package com.thel.modules.live.adapter;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.modules.live.bean.LianGiftCountBean;

import java.util.Collection;

import video.com.relavideolibrary.BaseRecyclerAdapter;
import video.com.relavideolibrary.BaseViewHolder;

/**
 * Created by chad
 * Time 18/7/30
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */
public class LianGiftCountAdatper extends BaseRecyclerAdapter<LianGiftCountBean> {
    public LianGiftCountAdatper(RecyclerView recyclerView, Collection<LianGiftCountBean> list) {
        super(R.layout.item_lian_count, recyclerView, list);
    }

    @Override
    public void dataBinding(BaseViewHolder baseViewHolder, final LianGiftCountBean lianGiftCountBean, final int position) {

        ImageView starIv = baseViewHolder.getView(R.id.star_iv);

        LinearLayout root_ll = baseViewHolder.getView(R.id.root_ll);

        if (effectsThreshold >= 999 && lianGiftCountBean.count >= 999) {
            starIv.setVisibility(View.VISIBLE);
        } else {
            starIv.setVisibility(View.GONE);
        }

        TextView textView = baseViewHolder.getView(R.id.tag_tv);
        textView.setText(String.valueOf(lianGiftCountBean.count));
        textView.setSelected(lianGiftCountBean.select);
        starIv.setSelected(lianGiftCountBean.select);
        root_ll.setSelected(lianGiftCountBean.select);
        if (lianGiftCountBean.select) {
            textView.setTextColor(mContext.getResources().getColor(R.color.white));
        } else {
            textView.setTextColor(mContext.getResources().getColor(R.color.rela_color));
        }
        root_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (int i = 0; i < mData.size(); i++) {
                    if (i == position) {
                        mData.get(i).select = !lianGiftCountBean.select;

                        if (onClickListener != null) {
                            onClickListener.onClick(mData.get(i));
                        }

                    } else {
                        mData.get(i).select = false;
                    }
                }

                notifyDataSetChanged();
            }
        });
    }

    private int effectsThreshold;

    public void setEffectsThreshold(int effectsThreshold) {
        this.effectsThreshold = effectsThreshold;
        notifyDataSetChanged();
    }

    private OnClickListener onClickListener;

    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public interface OnClickListener {
        void onClick(LianGiftCountBean bean);
    }

}
