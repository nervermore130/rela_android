package com.thel.modules.update;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import androidx.core.content.FileProvider;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.liulishuo.filedownloader.BaseDownloadTask;
import com.liulishuo.filedownloader.FileDownloadSampleListener;
import com.liulishuo.filedownloader.FileDownloader;
import com.thel.BuildConfig;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.constants.TheLConstants;
import com.thel.utils.DialogUtil;
import com.thel.utils.PhoneUtils;

import java.io.File;

public class DownloadActivity extends BaseActivity {

    private final static int INSTALL_APK_REQUEST_CODE = 1;

    private TextView title;
    private ProgressBar pb;
    private TextView speedTv;
    private BaseDownloadTask downloadTask;
    private String path = TheLConstants.F_DOWNLOAD_ROOTPATH + "Rela.apk";
    private String apkUrl;


    private Handler mHander = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    title.setText(R.string.downloading);
                    break;
                case 1:
                    title.setText(R.string.download_failed);
                    speedTv.setText(String.format("%dKB/s", 0));
                    break;
                case 2:
                    title.setText(R.string.downloaded);
                    speedTv.setText("");
                    installApk(path);
                    break;
                default:
                    break;
            }
            return true;
        }
    });

    private FileDownloadSampleListener fileDownloadSampleListener = new FileDownloadSampleListener() {
        @Override
        protected void progress(BaseDownloadTask task, int soFarBytes, int totalBytes) {
            super.progress(task, soFarBytes, totalBytes);
            if (totalBytes == -1) {
                pb.setIndeterminate(true);
            } else {
                pb.setMax(totalBytes);
                pb.setProgress(soFarBytes);
            }
            speedTv.setText(String.format("%dKB/s", task.getSpeed()));
        }

        @Override
        protected void error(BaseDownloadTask task, Throwable e) {
            super.error(task, e);
            mHander.sendEmptyMessage(1);
        }

        @Override
        protected void paused(BaseDownloadTask task, int soFarBytes, int totalBytes) {
            super.paused(task, soFarBytes, totalBytes);
        }

        @Override
        protected void completed(BaseDownloadTask task) {
            super.completed(task);
            mHander.sendEmptyMessage(2);
        }

        @Override
        protected void warn(BaseDownloadTask task) {
            super.warn(task);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download);
        getIntentData();
        findViewById();
    }

    private void getIntentData() {
        apkUrl = getIntent().getStringExtra(TheLConstants.BUNDLE_KEY_APK_URL);
    }

    private void findViewById() {
        title = findViewById(R.id.title);
        pb = findViewById(R.id.progressBar);
        speedTv = findViewById(R.id.speed);

        File apkLocalFile = new File(path);

        if (apkLocalFile.exists()) {
            boolean isDeleteSuccess = apkLocalFile.delete();
            if (!isDeleteSuccess) {
                path = TheLConstants.F_DOWNLOAD_ROOTPATH + System.currentTimeMillis() + "_" + "Rela.apk";
            }
        }

        downloadTask = FileDownloader.getImpl().create(apkUrl).setPath(path).setListener(fileDownloadSampleListener);
        downloadTask.start();

        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (title.getText().toString().equals(TheLApp.getContext().getString(R.string.download_failed))) {
                    if (PhoneUtils.getNetWorkType() != PhoneUtils.TYPE_NO) {
                        mHander.sendEmptyMessage(0);
                        //  downloadTask = FileDownloader.getImpl().create(SharedPrefUtils.getString(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.LATEST_APK, apkUrl)).setPath(path).setListener(fileDownloadSampleListener);
                        downloadTask = FileDownloader.getImpl().create(apkUrl).setPath(path).setListener(fileDownloadSampleListener);
                        downloadTask.start();
                    }
                }
            }
        });
    }

    private void installApk(String filePath) {


        if (Build.VERSION.SDK_INT >= 26) {

            boolean installAllowed = getPackageManager().canRequestPackageInstalls();

            if (installAllowed) {
                openFile(new File(filePath));
            } else {
                Intent intent = new Intent(Settings.ACTION_MANAGE_UNKNOWN_APP_SOURCES);
                startActivityForResult(intent, INSTALL_APK_REQUEST_CODE);
            }

        } else {
            openFile(new File(filePath));
        }

    }

    private void openFile(File file) {
        if (Build.VERSION.SDK_INT > 23) {
            Uri uri = FileProvider.getUriForFile(DownloadActivity.this, BuildConfig.APPLICATION_ID + ".install", file);
            Intent intent = new Intent(Intent.ACTION_VIEW).setDataAndType(uri, "application/vnd.android.package-archive");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setDataAndType(getUriFromFile(file), "application/vnd.android.package-archive");
            startActivity(intent);
            finish();
        }

    }

    @Override
    public void finish() {
        super.finish();
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (title.getText().toString().equals(getString(R.string.downloaded)))
                finish();
            else
                DialogUtil.showConfirmDialog(this, "", getString(R.string.cancel_confirm), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        downloadTask.pause();
                        finish();
                    }
                });
        }
        return true;
    }

    public static Uri getUriFromFile(File file) {
        return Uri.fromFile(file);
    }

    @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == INSTALL_APK_REQUEST_CODE) {
            if (path != null) {
                openFile(new File(path));
            }
        }
    }
}
