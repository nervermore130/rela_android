package com.thel.modules.main.userinfo.recommandcard;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.bean.ReleaseMomentSucceedBean;
import com.thel.bean.user.UserCardBean;
import com.thel.constants.TheLConstants;
import com.thel.modules.main.messages.ChatActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.utils.AppInit;
import com.thel.utils.DialogUtil;
import com.thel.utils.ImageUtils;
import com.thel.utils.TextLimitWatcher;
import com.thel.utils.ViewUtils;

import java.io.Serializable;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by test1 on 2017/2/14.
 */

public class RecommendCardActivity extends BaseActivity {
    private String toAvatar;
    private String bg_imageurl;
    private SimpleDraweeView bg_image;
    private SimpleDraweeView img_recommend_avatar;
    private TextView send_card;
    private TextView tv_cancel;
    private EditText et_evaluate;
    private TextWatcher textWatcher;
    private UserCardBean userCardBean;
    private String toName;
    private String toUserId;
    private UserCardBean intentUsercardbean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recommend_card_layout);
        getIntentData();
        findViewById();
        initdata();
        setDoneButtonEnabled(true);
        setListener();

    }

    private void getIntentData() {
        Intent intent = getIntent();
        toAvatar = intent.getStringExtra("toAvatar");
        toName = intent.getStringExtra("toName");
        toUserId = intent.getStringExtra("toUserId");
        intentUsercardbean = (UserCardBean) intent.getSerializableExtra("usercardbean");
    }

    protected void findViewById() {
        bg_image = findViewById(R.id.bg_image);
        img_recommend_avatar = findViewById(R.id.img_recommend_avatar);
        send_card = findViewById(R.id.send_card);
        et_evaluate = findViewById(R.id.et_evaluate);
        tv_cancel = findViewById(R.id.tv_cancel);

    }

    protected void setListener() {
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewUtils.hideSoftInput(RecommendCardActivity.this, et_evaluate);
                finish();
            }
        });

        textWatcher = new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                String str = s.toString().trim();

//                if (str.length() > 0) {
//                    setDoneButtonEnabled(true);
//                } else {
//                    setDoneButtonEnabled(false);
//                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };
        et_evaluate.addTextChangedListener(textWatcher);
        //下一步
        send_card.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                final String content = et_evaluate.getText().toString().trim();
                String type = getIntent().getStringExtra("type");

                if (userCardBean == null) {
                    userCardBean = new UserCardBean();
                }
                userCardBean.avatar =intentUsercardbean.avatar ;
                userCardBean.bgImage = intentUsercardbean.bgImage;
                userCardBean.birthday = intentUsercardbean.birthday;
                userCardBean.userId = intentUsercardbean.userId;
                userCardBean.nickName = intentUsercardbean.nickName;
                userCardBean.horoscope = intentUsercardbean.horoscope;
                userCardBean.affection = intentUsercardbean.affection;
                userCardBean.recommendDesc = content;

                if (type.equals("chat")) {
                    Intent intent = new Intent(RecommendCardActivity.this, ChatActivity.class);
                    Bundle bundle = new Bundle();
                    intent.putExtra("toUserId", toUserId);
                    intent.putExtra("toAvatar", toAvatar);
                    intent.putExtra("toName", toName);
                    bundle.putSerializable("usercardbean", userCardBean);
                    intent.putExtras(bundle);
                    intent.putExtra("send", 1);
                    intent.putExtra("fromPage", "SendCardActivity");
                    startActivity(intent);
                } else if (type.equals("moment")) {
                    showLoading();

                    Flowable<ReleaseMomentSucceedBean> flowable = RequestBusiness.getInstance().
                            releaseUserCardMoment(intentUsercardbean.userId + "", content);

                    flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<ReleaseMomentSucceedBean>() {
                        @Override
                        public void onNext(ReleaseMomentSucceedBean data) {
                            super.onNext(data);
                            closeLoading();
                            DialogUtil.showToastShort(RecommendCardActivity.this, getResources().getString(R.string.post_moment_posted));
                            finish();
                        }

                        @Override
                        public void onComplete() {
                            super.onComplete();
                            closeLoading();
                        }
                    });
                }

//                Intent intent = new Intent(RecommendCardActivity.this, SendCardActivity.class);
//                if (userCardBean == null) {
//                    userCardBean = new UserCardBean();
//                }
//                if (content.length() != 0) {
//                    Bundle bundle = new Bundle();
//
//                    userCardBean.avatar = avatar;
//                    userCardBean.bgImage = bg_imageurl;
//                    userCardBean.birthday = birthday;
//                    userCardBean.userId = toUserId;
//                    userCardBean.nickName = toName;
//                    userCardBean.horoscope = mHoroscope;
//                    userCardBean.affection = affection;
//                    userCardBean.recommendDesc = content;
//                    bundle.putSerializable("usercardbean", userCardBean);
//                    intent.putExtra(TheLConstants.BUNDLE_KEY_PAGE_FROM, GrowingIoConstant.ENTRY_LONG_VIDEO);
//
//                    intent.putExtras(bundle);
//
//                    startActivity(intent);
//                    finish();
//                }

            }
        });
    }

    private void setDoneButtonEnabled(boolean enabled) {
        if (enabled) {
            send_card.setAlpha(1f);
            send_card.setEnabled(true);
        } else {
            send_card.setAlpha(0.5f);
            send_card.setEnabled(false);
        }
    }

    private void initdata() {
        if (intentUsercardbean!=null){
            img_recommend_avatar.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(intentUsercardbean.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE)));
            bg_image.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(intentUsercardbean.bgImage, AppInit.displayMetrics.widthPixels, AppInit.displayMetrics.heightPixels)));

        }
        //设置中英文字符长度
        textWatcher = new TextLimitWatcher(16, true, "");
        et_evaluate.addTextChangedListener(textWatcher);

    }

    @Override
    public void finish() {
        super.finish();
        ViewUtils.hideSoftInput(RecommendCardActivity.this, et_evaluate);
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }
}
