package com.thel.modules.main.nearby.visit;

import androidx.viewpager.widget.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by waiarl on 2017/10/12.
 */

class VisitAdapter extends PagerAdapter {
    private final List<ViewGroup> list;
    private final List<String> titleList;

    public VisitAdapter(List<ViewGroup> list, List<String> titleList) {
        this.list = list;
        this.titleList = titleList;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(list.get(position));
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        container.addView(list.get(position));
        return list.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (titleList != null ) {
            return titleList.get(position % titleList.size());
        }
        return "";
    }
}
