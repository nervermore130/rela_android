package com.thel.modules.live.liveBigGiftAnimLayout.anim;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.thel.R;
import com.thel.modules.live.liveBigGiftAnimLayout.LiveBigAnimUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by the L on 2016/10/13.
 */

public class KissAnimator extends BaseAnimator {
    private final Context mContext;
    private final int mDuration;
    //  private final int[] resId;
    private final String[] resId;
    private final int mKissFreq;
    private final Handler mHandler;
    private final int mStarScaleDur;
    private final int mStarScaleFreq;
    private final String foldPath;
    private final String starRes;
    private boolean isPlaying;

    public KissAnimator(Context context, int duratation) {
        mContext = context;
        mDuration = duratation;
        mHandler = new Handler(Looper.getMainLooper());
        //  resId = new int[]{R.drawable.lip_print1, R.drawable.lip_print2, R.drawable.lip_print3, R.drawable.lip_print4};
        foldPath = "anim/kiss";
        resId = new String[]{"lip_print1", "lip_print2", "lip_print3", "lip_print4"};
        starRes = "lip_star";
        mKissFreq = 500 / resId.length;
        mStarScaleDur = 700;
        mStarScaleFreq = 300;
    }

    public int start(final ViewGroup parent) {
        isPlaying = true;
        final RelativeLayout background = (RelativeLayout) RelativeLayout.inflate(mContext, R.layout.live_big_anim_kiss, null);
        parent.addView(background);
        background.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        initBackground(background);
        setBackground(background);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                isPlaying = false;
                background.clearAnimation();
                parent.removeView(background);
            }
        }, mDuration);
        return mDuration;
    }

    private void initBackground(RelativeLayout background) {
        final ImageView img_kiss = background.findViewById(R.id.img_kiss);
        final ImageView img_star1 = background.findViewById(R.id.img_star1);
        final ImageView img_star2 = background.findViewById(R.id.img_star2);
        final ImageView img_star3 = background.findViewById(R.id.img_star3);
        LiveBigAnimUtils.setAssetBackground(img_kiss, foldPath, resId[0]);
        LiveBigAnimUtils.setAssetImage(img_star1, foldPath, starRes);
        LiveBigAnimUtils.setAssetImage(img_star2, foldPath, starRes);
        LiveBigAnimUtils.setAssetImage(img_star3, foldPath, starRes);
    }

    private void setBackground(ViewGroup background) {
        ImageView img_kiss = background.findViewById(R.id.img_kiss);
        // LiveBigGiftAnimLayout.setFrameAnim(mContext, img_kiss, resId, mKissFreq);
        LiveBigAnimUtils.setFrameAnim(mContext, img_kiss, foldPath, resId, mKissFreq);
        List<View> stars = new ArrayList<>();
        stars.add(background.findViewById(R.id.img_star1));
        stars.add(background.findViewById(R.id.img_star2));
        stars.add(background.findViewById(R.id.img_star3));
        for (View v : stars) {
            setScaleAnim(v);
        }
    }

    private void setScaleAnim(final View v) {
        if (!isPlaying) {
            return;
        }
        AnimatorSet anim = new AnimatorSet();
        ObjectAnimator animX = ObjectAnimator.ofFloat(v, "scaleX", 1f, 0.3f, 1f, 0.7f, 1f);
        ObjectAnimator animY = ObjectAnimator.ofFloat(v, "scaleY", 1f, 0.3f, 1f, 0.7f, 1f);
        anim.play(animX).with(animY);
        anim.setDuration(mStarScaleDur);
        anim.setInterpolator(new LinearInterpolator());
        anim.start();
        anim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {


            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (isPlaying) {
                    v.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            setScaleAnim(v);
                        }
                    }, mStarScaleFreq);
                }

                if (mOnAnimEndListener != null) {
                    mOnAnimEndListener.onAnimEnd();
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

}
