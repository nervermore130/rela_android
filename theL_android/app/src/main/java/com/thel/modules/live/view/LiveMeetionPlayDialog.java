package com.thel.modules.live.view;

import android.app.Dialog;
import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.thel.R;

/**
 * Created by lingwei on 2018/5/9.
 */

public class LiveMeetionPlayDialog extends Dialog {
    private final Context mContext;
    private View close_dialog;
    private Button meeting_start;

    public LiveMeetionPlayDialog(@NonNull Context context) {
        this(context, R.style.CustomDialogBottom);
    }

    public LiveMeetionPlayDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        mContext = context;
        init();
        setLitener();
    }

    private void setLitener() {
        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dismiss();

            }
        });
    }

    private void init() {
        final View view = LayoutInflater.from(mContext).inflate(R.layout.live_meetion_play_rules_layout, null);
        setContentView(view);
        close_dialog = view.findViewById(R.id.close_dialog);
        meeting_start = view.findViewById(R.id.meeting_start);

    }

    public void setMeetingStartListener(View.OnClickListener meetingStartListener) {
        meeting_start.setOnClickListener(meetingStartListener);
    }


}
