package com.thel.modules.live.in;

import com.thel.bean.RejectMicBean;
import com.thel.modules.live.bean.LiveRoomMsgConnectMicBean;
import com.thel.modules.live.bean.LivePkFriendBean;
import com.thel.modules.live.bean.LivePkGemNoticeBean;
import com.thel.modules.live.bean.LivePkHangupBean;
import com.thel.modules.live.bean.LivePkInitBean;
import com.thel.modules.live.bean.LivePkRequestNoticeBean;
import com.thel.modules.live.bean.LivePkResponseNoticeBean;
import com.thel.modules.live.bean.LivePkStartNoticeBean;
import com.thel.modules.live.bean.LivePkSummaryNoticeBean;
import com.thel.modules.live.bean.LiveRoomMsgBean;
import com.thel.modules.live.bean.TopTodayBean;
import com.thel.modules.live.surface.show.LiveShowObserver;
import com.thel.modules.live.view.expensive.TopGiftBean;

import java.util.List;

/**
 * Created by waiarl on 2017/11/5.
 */

public interface LiveShowCaptureViewIn extends LiveShowViewPkIn,LiveShowViewMultiIn {
    void bindObserver(LiveShowObserver observer);
    /**
     * 更新观众数量
     */
    void updateAudienceCount();

    /**
     * 刷新消息
     */
    void refreshMsg();

    /**
     * 消息滑动到底部
     */
    void smoothScrollToBottom();

    void clearInput();

    /**
     * 网络状态差
     */
    void updateMyNetStatusBad();

    /**
     * 收到礼物
     *
     * @param payload
     */
    void parseGiftMsg(String payload, String code);

    /**
     * 收到弹幕
     */
    void showDanmu(String danmuMsg);

    void openInput(boolean open);

    void setDanmuResult(String result);


    /**
     * 会员加入
     *
     * @param liveRoomMsgBean
     */
    void joinVipUser(LiveRoomMsgBean liveRoomMsgBean);

    /**
     * 消息列表添加消息（收到消息）
     *
     * @param liveRoomMsgBean
     */
    void addLiveRoomMsg(LiveRoomMsgBean liveRoomMsgBean);

    void showTopGiftView(TopGiftBean topGiftBean);



    /**
     * 网速过慢
     */
    void networkSlow();//观众没有


    /**
     * 重置是否在底部
     */
    void resetIsBottom();//观众没有


    /**
     * 网络状态良好
     */
    void networkGood();//观众没有

    /**
     * 连接中断
     */
    void connectBreak();//观众没有

    /**
     * 连接失败
     */
    void connectFailed();//观众没有

    /**
     * 连接中
     */
    void connectiong();//观众没有

    /**
     * 连接成功
     */
    void connectSuccess();//观众没有





    /**
     * 有人加入房间
     *
     * @param liveRoomMsgBean
     */
    void joinUser(LiveRoomMsgBean liveRoomMsgBean);//观众没有


    /**
     * 关闭直播 消息返回
     *
     * @param json
     */
    void refreshCloseDialog(String json);//观众没有

    /**
     * 关闭进度条
     */
    void progressbarGone();//观众没有

    /**
     * 直播断开连接
     */
    void liveDisconnected();//观众没有

    /**
     * 直播网络警告
     *
     * @param text
     */
    void liveNetworkWaringNotify(String text);//观众没有



    void switchBeauty(boolean enable);//观众没有

    /**
     * 是否关闭直播确认alert
     */
    void showCloseAlert(); //观众没有
    /*********************************一下为直播pk有关方法*********************************************************/

    /**
     * 显示我申请Pk的view
     */
    void showIPkRequestView(LivePkFriendBean livePkFriendBean);//观众没有

    /**
     * 显示直播PK申请view
     *
     * @param pkRequestNoticeBean
     */
    void showPkRequestView(LivePkRequestNoticeBean pkRequestNoticeBean);//观众没有

    /**
     * 显示直播PK回应view
     *
     * @param pkResponseNoticeBean
     */
    void showPkResponseView(LivePkResponseNoticeBean pkResponseNoticeBean);//观众没有

    /**
     * 显示直播pk取消view
     *
     * @param pkCancelPayload
     */
    void showPkCancelView(String pkCancelPayload);//观众没有

    /**
     * pk请求超时
     */
    void showPkRequestTimeOutView(String userId);//观众没有


    /*********************************以上为直播pk有关方法*********************************************************/


    void showConnectMicDialog(LiveRoomMsgConnectMicBean lIveRoomMsgConnectMicBean);

    /**
     * 发送连麦请求成功
     *
     * @param toUserId
     * @param nickName
     * @param avatar
     */
    void showLinkMicSendSuccess(String toUserId, String nickName, String avatar, boolean dailyGuard);//观众没有

    /**
     * 对方同意连麦
     */
    void linkMicAccept();

    /**
     * 对方取消连麦
     */

    void lingMicCancel();

    /**
     * 显示连麦图层
     *
     * @param userName
     * @param toUserId
     */
    void showLinkMicLayer(String userName, String toUserId);//观众没有

    /**
     * 对方挂断连麦
     */

    void linkMicHangup();

    /**
     * 显示断开连接后的视图
     *
     * @param guardLinkMic 默认pk，当日榜用户挂断时为true
     */
    void showHangupView(String nickName, String avatar, boolean guardLinkMic);//观众没有

    /**
     * 直播Pk请求错误返回
     *
     * @param errorCode
     */
    void showPkRequestErrorView(String errorCode);//观众没有


    /**
     * 添加观众连麦
     *
     * @param livePkFriendBean
     */
    void videoLinkAdd(LivePkFriendBean livePkFriendBean);//观众没有

    /**
     * 删除观众连麦
     *
     * @param livePkFriendBean
     */
    void videoLinkDel(LivePkFriendBean livePkFriendBean);//观众没有

    void lockInput();//观众没有

    /**
     * 获取当前在直播间中的日榜列表
     */
    void getTopFansTodayList(List<LivePkFriendBean> list);//观众没有

    /**
     * 守护棒变动消息
     */
    void topToday(TopTodayBean topTodayBean);

    /**
     * 主播连麦时，观众已不再直播间
     */
    void andienceNotIn();//观众没有

    /**
     * 观众决绝连麦
     */
    void rejectMic(RejectMicBean rejectMicBean);//观众没有
}
