package com.thel.modules.live.liveBigGiftAnimLayout.anim;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.thel.R;
import com.thel.modules.live.liveBigGiftAnimLayout.LiveBigAnimUtils;

/**
 * Created by the L on 2016/10/13.
 */

public class PumpkinAnimator {
    private final Context mContext;
    private final int mDuration;
    // private final int[] resPumpkin;
    //private final int[] resBat;
    private final String[] resPumpkin;
    private final String[] resBat;
    private final int mPumpkinFreq;
    private final int mBatFreq;
    private final Handler mHandler;
    private final int[] rX;
    private final String foldPath;
    private int mWidth;
    private int mHeight;

    public PumpkinAnimator(Context context, int duration) {
        mContext = context;
        mDuration = duration;
        mHandler = new Handler(Looper.getMainLooper());
        // resPumpkin = new int[]{R.drawable.pumpkin_pumpkin1, R.drawable.pumpkin_pumpkin2};
        //resBat = new int[]{R.drawable.pumpkin_bat1, R.drawable.pumpkin_bat2};
        foldPath = "anim/pumpkin";
        resPumpkin = new String[]{"pumpkin_pumpkin1", "pumpkin_pumpkin2"};
        resBat = new String[]{"pumpkin_bat1", "pumpkin_bat2"};
        mPumpkinFreq = 400;
        mBatFreq = 250;
        rX = new int[]{-40, 55, 25};
    }

    public int start(final ViewGroup parent) {
        mWidth = parent.getMeasuredWidth();
        mHeight = parent.getMeasuredHeight();
        if (mWidth <= 0 || mHeight <= 0) {
            return 0;
        }
        final RelativeLayout background = (RelativeLayout) RelativeLayout.inflate(mContext, R.layout.live_big_anim_pumpkin, null);
        parent.addView(background);
        background.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        final ImageView img_pumpkin = background.findViewById(R.id.img_pumpkin);
        final ImageView img_bat1 = background.findViewById(R.id.img_bat1);
        final ImageView img_bat2 = background.findViewById(R.id.img_bat2);
        final ImageView img_bat3 = background.findViewById(R.id.img_bat3);
        LiveBigAnimUtils.setAssetBackground(img_pumpkin, foldPath, resPumpkin[0]);
        setFrameAnim(img_pumpkin, resPumpkin, mPumpkinFreq);
        startScaleAnim(img_pumpkin);
        setFrameAnim(img_bat1, resBat, mBatFreq);
        setFrameAnim(img_bat2, resBat, mBatFreq);
        setFrameAnim(img_bat3, resBat, mBatFreq);
        translateAnim(img_bat1, background, getPath(-mWidth / 10, -mHeight / 8, -mWidth / 4, -mHeight / 5, -3 * mWidth / 8, -mHeight / 3), rX[0]);
        translateAnim(img_bat2, background, getPath(mWidth / 8, -mHeight / 9, mWidth / 4, -mHeight / 5, 5 * mWidth / 12, -mHeight / 4), rX[1]);
        translateAnim(img_bat3, background, getPath(mWidth / 13, -mHeight / 7, mWidth / 8, -mHeight / 4, mWidth / 4, -mHeight * 3 / 8), rX[2]);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                img_pumpkin.clearAnimation();
                img_bat1.clearAnimation();
                img_bat2.clearAnimation();
                img_bat3.clearAnimation();
                parent.removeView(background);
            }
        }, mDuration);
        return mDuration;
    }

    private void translateAnim(final View view, final ViewGroup parent, Path path, int rX) {
        FloatAnimation anim = new FloatAnimation(parent, view, path, rX);
        anim.setInterpolator(new LinearInterpolator());
        anim.setDuration(mDuration);
        view.startAnimation(anim);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                parent.post(new Runnable() {
                    @Override
                    public void run() {
                        parent.removeView(view);
                    }
                });
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

    }

    private void startScaleAnim(final View pumpkin) {

        final ScaleAnimation scaleAnimationUp = new ScaleAnimation(1f, 1.5f, 1f, 1.5f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        scaleAnimationUp.setDuration(mPumpkinFreq);
        scaleAnimationUp.setInterpolator(new LinearInterpolator());
        scaleAnimationUp.setFillAfter(true);
        scaleAnimationUp.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                final ScaleAnimation scaleAnimation = new ScaleAnimation(1.5f, 1f, 1.5f, 1f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                scaleAnimation.setDuration(mPumpkinFreq);
                scaleAnimation.setInterpolator(new LinearInterpolator());
                scaleAnimationUp.setFillAfter(true);
                scaleAnimation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        pumpkin.startAnimation(scaleAnimationUp);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                pumpkin.startAnimation(scaleAnimation);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        pumpkin.startAnimation(scaleAnimationUp);
    }

    private Path getPath(int x1, int y1, int x2, int y2, int x3, int y3) {
        Path path = new Path();
        path.moveTo(0, 0);
        path.cubicTo(x1, y1, x2, y2, x3, y3);
        return path;
    }

    private void setFrameAnim(View view, String[] res, int freq) {
        LiveBigAnimUtils.setFrameAnim(mContext, view, foldPath, res, freq);
    }

    class FloatAnimation extends Animation {

        private final PathMeasure mPm;
        private final float mDistance;
        private final View mView;
        private final int mRx;

        public FloatAnimation(ViewGroup parent, View view, Path path, int rX) {
            mPm = new PathMeasure(path, false);
            mDistance = mPm.getLength();
            mView = view;
            parent.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            mRx = rX;
        }

        @Override
        protected void applyTransformation(float factor, Transformation t) {
            Matrix matrix = t.getMatrix();
            mPm.getMatrix(mDistance * factor, matrix, PathMeasure.POSITION_MATRIX_FLAG);
            if (factor < 0.4) {//先变大
                mView.setScaleX(1 + factor / 2);
                mView.setScaleY(1 + factor / 2);
            }
            if (factor > 0.4) {//变小，最小为0.2
                mView.setScaleX(1 + 0.4f - (factor - 0.4f) * 1.2f / 0.6f);
                mView.setScaleY(1 + 0.4f - (factor - 0.4f) * 1.2f / 0.6f);
            }
            if (factor > 0.7) {//透明至消失不见
                mView.setAlpha(1 - (factor - 0.7f) / 0.3f);
            }
            mView.setRotation(mRx * factor);
        }
    }

}
