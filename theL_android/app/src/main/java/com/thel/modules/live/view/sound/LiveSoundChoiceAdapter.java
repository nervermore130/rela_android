package com.thel.modules.live.view.sound;

import android.graphics.PorterDuff;

import com.thel.R;
import com.thel.modules.live.bean.LiveSoundChoiceAppBean;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;

import java.util.List;

/**
 * Created by waiarl on 2018/1/23.
 */

class LiveSoundChoiceAdapter extends BaseRecyclerViewAdapter<LiveSoundChoiceAppBean> {
    public LiveSoundChoiceAdapter(List<LiveSoundChoiceAppBean> list) {
        super(R.layout.live_sound_choice_adapter, list);
    }

    @Override
    protected void convert(BaseViewHolder helper, LiveSoundChoiceAppBean item) {
        helper.getView(R.id.rel_back).getBackground().setColorFilter(item.backColor, PorterDuff.Mode.SRC);
        helper.setImageResource(R.id.img_app, item.icon);
        helper.setText(R.id.txt_name, item.name);
    }
}
