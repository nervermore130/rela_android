package com.thel.modules.main.video_discover;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseFragment;
import com.thel.constants.TheLConstants;
import com.thel.imp.AutoRefreshImp;
import com.thel.imp.TittleClickListener;
import com.thel.modules.main.home.community.CommunityFragment;
import com.thel.modules.main.home.moments.ReleaseMomentActivity;
import com.thel.modules.main.home.moments.ReleaseThemeMomentActivity;
import com.thel.modules.main.video_discover.pgc.PgcVideoFallsFragment;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.StringUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.thel.utils.ShareFileUtils.IS_RELEASE_MOMENT;

/**
 * Created by waiarl on 2018/2/5.
 */

public class VideoDiscoverFragment extends BaseFragment {

    private static VideoDiscoverFragment instance;
    private TabLayout tablayout;
    private ImageView img_more;
    private ViewPager viewpager;
    private List<String> titleList = new ArrayList<>();
    private List<Fragment> list = new ArrayList<>();
    private VideoDiscoverAdapter adapter;
    private Fragment videoFallsFragment;
    private Fragment communityFragment;

    private final int TAB_VIDEO = 0;
    private final int TAB_COMMUNITY = 1;
    private int current_tab = TAB_VIDEO;

    public static VideoDiscoverFragment getInstance(Bundle bundle) {
        instance = new VideoDiscoverFragment();
        instance.setArguments(bundle);
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initData();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_video_discover, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        findViewById(view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setListener();
    }

    private void findViewById(View view) {
        tablayout = view.findViewById(R.id.tablayout);
        img_more = view.findViewById(R.id.img_more);
        viewpager = view.findViewById(R.id.viewpager);
        setTabs();
        initViewState();
    }

    private void initViewState() {
        setImageMore();
    }

    private void setImageMore() {
        if (TAB_VIDEO == current_tab) {
            img_more.setImageResource(R.mipmap.btn_discover_post_video);
        } else {
            img_more.setImageResource(R.mipmap.btn_discover_post_topic);
        }
    }

    private void initData() {
    }

    private void setTabs() {
        addFragment();
        adapter = new VideoDiscoverAdapter(getChildFragmentManager(), list, titleList);
        viewpager.setAdapter(adapter);
        final int size = titleList.size();
        for (int i = 0; i < size; i++) {
            tablayout.addTab(tablayout.newTab());
        }
        tablayout.setupWithViewPager(viewpager);
    }

    private void addFragment() {
        list.clear();
        titleList.clear();
        videoFallsFragment = getVideoFallsFragment();
        list.add(videoFallsFragment);
        titleList.add(StringUtils.getString(R.string.videos));
        if (Utils.isChaneseLanguage()) {
            communityFragment = getCommunityFragment();
            list.add(communityFragment);
            titleList.add(StringUtils.getString(R.string.info_topic));
        }
    }

    private Fragment getCommunityFragment() {
        final Bundle bundle = new Bundle();
        final CommunityFragment communityFragment = CommunityFragment.getInstance();
        return communityFragment;
    }

    private Fragment getVideoFallsFragment() {
        final Bundle bundle = new Bundle();
        final Fragment fragment = PgcVideoFallsFragment.getInstance(bundle);
        return fragment;
    }

    private void setListener() {
        img_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (current_tab == TAB_VIDEO) {//发布视频日志
                    tryWriteVideoMoment();
                } else {//发布话题日志
                    writeThemeMoment();
                }
            }
        });
        tablayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                current_tab = tab.getPosition();
                setImageMore();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                final int position = tab.getPosition();
                if (list.size() > position) {
                    final Fragment fragment = list.get(position);
                    if (fragment instanceof TittleClickListener) {
                        ((TittleClickListener) fragment).onTitleClick();
                    }
                }
            }
        });
    }

    private void writeThemeMoment() {
        if (UserUtils.isVerifyCell()) {
            ShareFileUtils.setBoolean(IS_RELEASE_MOMENT, true);
            ShareFileUtils.setBoolean(Utils.getMyUserId() + "_" + IS_RELEASE_MOMENT, true);
            final Intent intent = new Intent(TheLApp.getContext(), ReleaseThemeMomentActivity.class);
            intent.putExtra(TheLConstants.BUNDLE_KEY_RELEASE_TYPE, ReleaseThemeMomentActivity.RELEASE_TYPE_TOPIC);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            TheLApp.getContext().startActivity(intent);
        }
    }

    private void tryWriteVideoMoment() {
        if (UserUtils.isVerifyCell()) {
            writeVideoMoment();
        }
    }

    private void writeVideoMoment() {
        ShareFileUtils.setBoolean(Utils.getMyUserId() + "_" + IS_RELEASE_MOMENT, true);
        final Intent intent = new Intent(TheLApp.getContext(), ReleaseMomentActivity.class);
        intent.putExtra(TheLConstants.BUNDLE_KEY_RELEASE_TYPE, ReleaseMomentActivity.RELEASE_TYPE_VIDEO);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        TheLApp.getContext().startActivity(intent);
    }


    /***************************************************内部类***************************************/
    class VideoDiscoverAdapter extends FragmentStatePagerAdapter {

        private final List<Fragment> list;
        private final List<String> titliList;

        public VideoDiscoverAdapter(FragmentManager childFragmentManager, List<Fragment> list, List<String> titleList) {
            super(childFragmentManager);
            this.list = list;
            this.titliList = titleList;
        }

        @Override
        public Fragment getItem(int position) {
            return list.get(position);
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (titliList != null) {
                return titliList.get(position % titliList.size());
            }
            return "";
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        tryRefreshCurrentFragment();
    }

    /**
     * 尝试刷新当前的fragment
     */
    private void tryRefreshCurrentFragment() {
        if (viewpager == null || viewpager.getAdapter() == null) {
            return;
        }
        final int currentTab = viewpager.getCurrentItem();
        if (list != null && list.size() > currentTab) {
            final Fragment fragment = list.get(currentTab);
            if (fragment instanceof AutoRefreshImp) {
                ((AutoRefreshImp) fragment).tryRefreshData();
            }
        }
    }

}
