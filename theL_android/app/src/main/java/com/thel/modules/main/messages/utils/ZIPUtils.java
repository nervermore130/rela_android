package com.thel.modules.main.messages.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.zip.Inflater;

/**
 * Created by setsail on 16/1/12.
 */
public class ZIPUtils {

    /***
     * 解压Zip数据为String
     *
     * @param data
     * @return
     */
    public static String unZipByteToString(byte[] data, int bufferSize) {
        if (data == null)
            return null;
        byte[] result = unZipByte(data, bufferSize);
        String outputString = null;
        outputString = new String(result, 0, result.length, StandardCharsets.UTF_8);
        return outputString;
    }

    public static byte[] unZipByte(byte[] data, int bufferSize) {
        Inflater decompresser = new Inflater();
        decompresser.setInput(data);
        byte[] result = new byte[0];
        ByteArrayOutputStream o = new ByteArrayOutputStream(1);
        try {
            byte[] buf = new byte[bufferSize];
            int got = 0;
            while (!decompresser.finished()) {
                got = decompresser.inflate(buf);
                o.write(buf, 0, got);
            }
            result = o.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                o.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            decompresser.end();
        }
        return result;
    }

}
