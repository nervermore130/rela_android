package com.thel.modules.live.liverank;

import com.thel.base.BasePresenter;
import com.thel.base.BaseView;
import com.thel.modules.live.bean.LiveGiftsRankingBean;
import com.thel.modules.live.bean.LiveGiftsRankingInfoBean;

import java.util.List;

/**
 * Created by waiarl on 2017/10/26.
 */

public class LiveGiftRankingContract {
    interface Presenter extends BasePresenter {
        void getRefreshData(String userId, String tag);
    }

    interface View extends BaseView<Presenter> {
        void showRefreshData(List<LiveGiftsRankingBean> list);

        void requestFinish();

        void showEmptyData(boolean show);

        void showHeader(String rank, String dou);

        /***直播排名信息**/
        void showRankInfoView(LiveGiftsRankingInfoBean rankInfo);
    }
}
