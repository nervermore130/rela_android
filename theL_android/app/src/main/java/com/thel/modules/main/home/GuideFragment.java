package com.thel.modules.main.home;

import android.os.Bundle;
import android.os.SystemClock;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseFragment;
import com.thel.bean.InitRecommendUserBean;
import com.thel.bean.InitRecommendUserListBean;
import com.thel.imp.follow.FollowStatusChangedImpl;
import com.thel.imp.follow.FollowStatusChangedListener;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.service.DefaultRequestService;
import com.thel.ui.adapter.RecommendUserRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.tourguide.FrameLayoutWithHole;
import com.thel.ui.widget.tourguide.MyTourGuide;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SharedPrefUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import tourguide.tourguide.Overlay;
import tourguide.tourguide.Pointer;
import tourguide.tourguide.ToolTip;


/**
 * Created by liuyun on 2017/11/8.
 */

public class GuideFragment extends BaseFragment implements View.OnClickListener, FollowStatusChangedListener {
    public final String TAG = "GuideFragment";
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    @BindView(R.id.btn_new_user_start)
    TextView btn_new_user_start;

    @BindView(R.id.new_user_guide)
    LinearLayout new_user_guide;

    @BindView(R.id.txt_follow_guide)
    TextView txt_follow_guide;

    @BindView(R.id.bg_guide)
    RelativeLayout bg_guide;

    private boolean enabled;

    private RecommendUserRecyclerViewAdapter recommedUserRecyclerViewAdapter;

    private ArrayList<InitRecommendUserBean> initRecommendUsers = new ArrayList<>();

    private MyTourGuide tourGuide;

    private int followCountLimit = 5;// 要求最少关注人数

    private int curPage = 1; // 将要请求的页号

    private int currentCountForOnce = -1;

    public int refreshType = 0;

    public static final int REFRESH_TYPE_ALL = 1;

    public static final int REFRESH_TYPE_NEXT_PAGE = 2;

    private LinearLayoutManager mLinearLayoutManager;
    private LinearLayoutManager manager;
    private FollowStatusChangedImpl followStatusChangedImpl;
    /**
     * 判断新用户是不是关注满10个人了
     */
    private int initCount;
    private StartMomentListCallback callback;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_guide, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        refreshType = REFRESH_TYPE_ALL;
        curPage = 1;
        initRecommendUsersAdapter();
        getInitRecommendUsers();
        followStatusChangedImpl = new FollowStatusChangedImpl();
        followStatusChangedImpl.registerReceiver(this);
        setListener();
        initCount = SharedPrefUtils.getInt(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.INIT_CIRCLE_PAGE + ShareFileUtils.getString(ShareFileUtils.ID, ""), 100);
        setStartBtnEnabled(initCount >= 1);
    }

    private void setListener() {
        bg_guide.setOnClickListener(null);//遮挡底部fragment点击事件
        recommedUserRecyclerViewAdapter.setOnLoadMoreListener(new BaseRecyclerViewAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                mRecyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        if (currentCountForOnce > 0) {
                            // 列表滑动到底部加载下一组数据
                            refreshType = REFRESH_TYPE_NEXT_PAGE;
                            getInitRecommendUsers();
                        } else {
                            recommedUserRecyclerViewAdapter.openLoadMore(initRecommendUsers.size(), false);
                            if (getActivity() != null) {
                                View view = getActivity().getLayoutInflater().inflate(R.layout.load_more_footer_layout, (ViewGroup) mRecyclerView.getParent(), false);
                                ((TextView) view.findViewById(R.id.text)).setText(TheLApp.getContext().getString(R.string.info_no_more));
                                recommedUserRecyclerViewAdapter.addFooterView(view);
                            }
                        }
                    }
                });
            }
        });
        recommedUserRecyclerViewAdapter.setReloadMoreListener(new BaseRecyclerViewAdapter.ReloadMoreListener() {
            @Override
            public void reloadMore() {
                recommedUserRecyclerViewAdapter.removeAllFooterView();
                recommedUserRecyclerViewAdapter.openLoadMore(true);
                refreshType = REFRESH_TYPE_NEXT_PAGE;
                getInitRecommendUsers();
            }
        });
        recommedUserRecyclerViewAdapter.setOnFollowClickListener(new RecommendUserRecyclerViewAdapter.OnFollowClickListener() {
            @Override
            public void onFollowClick(InitRecommendUserBean initRecommendUserBean, int position) {
                clearTourGuide();
                final String userId1 = initRecommendUserBean.userId + "";
                final String nickName = initRecommendUserBean.nickName;
                final String avatar = initRecommendUserBean.avatar;
                FollowStatusChangedImpl.followUser(userId1 + "", FollowStatusChangedImpl.ACTION_TYPE_FOLLOW, nickName, avatar);

                if (callback != null) {
                    callback.onFollowedUser();
                }

            }

            @Override
            public void onMoreClick(InitRecommendUserBean initRecommendUserBean, int position) {

            }
        });
        btn_new_user_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                L.d(TAG, " initCount onClick : " + initCount);

                L.d(TAG, " initCount callback : " + callback);

                if (initCount >= 1) {
                    if (callback != null) {
                        callback.startMomentList();
                    }
                } else {
                    forceStopRecyclerViewScroll(mRecyclerView);
                    show_to_guide();
                }
            }
        });
    }

    private void initRecommendUsersAdapter() {
        recommedUserRecyclerViewAdapter = new RecommendUserRecyclerViewAdapter(this, initRecommendUsers);

        manager = new LinearLayoutManager(getContext());
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setAdapter(recommedUserRecyclerViewAdapter);


    }

    private void getInitRecommendUsers() {

        Flowable<InitRecommendUserListBean> flowable = DefaultRequestService.createAllRequestService().getInitRecommendUsers(String.valueOf(curPage));

        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<InitRecommendUserListBean>() {
            @Override
            public void onNext(InitRecommendUserListBean initRecommendUserListBean) {
                super.onNext(initRecommendUserListBean);
                try {
                    if (initRecommendUserListBean != null && initRecommendUserListBean.userList != null) {
                        if (refreshType == REFRESH_TYPE_ALL) {
                            // 下拉刷新将curPage置为2，表示加载下一页的页数
                            curPage = 2;
                            initRecommendUsers.clear();
                        } else {
                            curPage++;
                        }
                        initRecommendUsers.addAll(initRecommendUserListBean.userList);
                        currentCountForOnce = initRecommendUserListBean.userList.size();

                        if (recommedUserRecyclerViewAdapter == null)
                            initRecommendUsersAdapter();

                        if (refreshType == REFRESH_TYPE_ALL) {
                            recommedUserRecyclerViewAdapter.removeAllFooterView();
                            recommedUserRecyclerViewAdapter.openLoadMore(initRecommendUsers.size(), true);
                            recommedUserRecyclerViewAdapter.setNewData(initRecommendUsers);
                        } else {
                            recommedUserRecyclerViewAdapter.notifyDataChangedAfterLoadMore(true, initRecommendUsers.size());
                        }
                        recommedUserRecyclerViewAdapter.notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


    }

    @Override
    public void onClick(View v) {

    }

    public void clearTourGuide() {//关闭tourguide
        if (tourGuide != null) {
            tourGuide.cleanUp();
            btn_new_user_start.setEnabled(true);
        }
    }

    private void setStartBtnEnabled(boolean enabled) {
        this.enabled = enabled;
        if (enabled) {
            btn_new_user_start.setTextColor(ContextCompat.getColor(TheLApp.getContext(), R.color.white));
        } else {
            btn_new_user_start.setTextColor(ContextCompat.getColor(TheLApp.getContext(), R.color.white_alpha_50));
        }
    }

    /**
     * 使用tourguide在listview列表中定位可能出现问题，目前使用的固定所在位置，待改进
     */
    private void show_to_guide() {
        int fpos = manager.findFirstVisibleItemPosition();
        final int fvpos = manager.findFirstCompletelyVisibleItemPosition();
        int start = fpos - recommedUserRecyclerViewAdapter.getHeaderLayoutCount();
        start = start >= 0 ? start : 0;
        final int pos = getTourGuidePos(start, fpos == fvpos);
        if (pos < 0) {
            return;
        }
        manager.scrollToPositionWithOffset(pos, 0);
        final Overlay overlay = new Overlay().setBackgroundColor(ContextCompat.getColor(TheLApp.getContext(), R.color.view_shade_color)).disableClick(false);
        tourGuide = MyTourGuide.init(getActivity(), new MyTourGuide.GlobalLayoutCallback() {
            @Override
            public void onGlobalLayout() {
                final FrameLayoutWithHole overlay1 = tourGuide.getOverlay();
                L.i(TAG, "overlay1==null?" + (overlay1 == null));
                if (overlay1 != null) {
                    overlay1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            clearTourGuide();
                        }
                    });
                }
            }
        }).with(MyTourGuide.Technique.Click).setPointer(new Pointer()).setToolTip(new ToolTip().setBackgroundColor(TheLApp.getContext().getResources().getColor(R.color.tab_normal)).setTitle(TheLApp.getContext().getString(R.string.info_note)).setDescription(TheLApp.getContext().getString(R.string.follow_friend)).setGravity(Gravity.BOTTOM)).setOverlay(overlay).playOn(txt_follow_guide);

    }

    private int getTourGuidePos(int start, boolean boo) {
        int pos = -1;
        if (!boo) {//第一个完全可见条目不是第一个完全可见条目
            return getTourGuidePos(start + 1, true);
        }
        final int size = recommedUserRecyclerViewAdapter.getData().size();

        for (int i = start; i < size; i++) {
            final InitRecommendUserBean recommendUserBean = recommedUserRecyclerViewAdapter.getItem(i);
            if (recommendUserBean.followStatus != 1 && recommendUserBean.followStatus != 3) {
                pos = i;
                break;
            }
        }
        if (start == 0) {
            return pos;
        }
        if (pos == -1) {//在已经加载的bean中全部都是关注的（断网只有第一页列表没有第二页切当前条目几乎在最后）
            return getTourGuidePos(0, true);//start从0开始
        }
        return pos;
    }

    @Override
    public void onDestroy() {
        followStatusChangedImpl.unRegisterReceiver(this);
        super.onDestroy();
    }

    @Override
    public void onFollowStatusChanged(int followStatus, String userId, String nickName, String avatar) {
        if (TextUtils.isEmpty(userId) || (followStatus == FOLLOW_STATUS_NO || followStatus == FOLLOW_STATUS_FAN)) {
            return;
        }

        L.d(TAG, " initCount followStatus : " + followStatus);

        if (recommedUserRecyclerViewAdapter != null) {
            final int headCount = recommedUserRecyclerViewAdapter.getHeaderLayoutCount();
            final int size = recommedUserRecyclerViewAdapter.getData().size();
            boolean haveUserId = false;
            for (int i = 0; i < size; i++) {
                final InitRecommendUserBean bean = recommedUserRecyclerViewAdapter.getData().get(i);
                if ((bean.userId + "").equals(userId)) {
                    initCount += 1;
                    L.d(TAG, " userId initCount : " + initCount);
                    bean.followStatus = followStatus;
                    recommedUserRecyclerViewAdapter.notifyDataSetChanged();
//                    recommedUserRecyclerViewAdapter.notifyItemChanged(i + headCount);
                    haveUserId = true;
                }
            }
            if (haveUserId) {
                SharedPrefUtils.setInt(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.INIT_CIRCLE_PAGE + ShareFileUtils.getString(ShareFileUtils.ID, ""), initCount);
                setStartBtnEnabled(initCount >= 1);
            }
        }
    }

    public static void forceStopRecyclerViewScroll(RecyclerView mRecyclerView) {
        mRecyclerView.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_CANCEL, 0, 0, 0));
    }

    public interface StartMomentListCallback {
        void startMomentList();

        void onFollowedUser();
    }

    public void setStartMomentListCallback(StartMomentListCallback callback) {
        this.callback = callback;
    }

}
