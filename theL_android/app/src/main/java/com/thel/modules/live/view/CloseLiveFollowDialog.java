package com.thel.modules.live.view;

import android.app.Dialog;
import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.thel.R;

/**
 * Created by lingwei on 2019/1/10.
 * 关闭主播的直播间，提示关注弹窗
 */

public class CloseLiveFollowDialog extends Dialog {
    private final Context mContext;
    private View close_dialog;
    private Button button_follow_and_exit;
    private TextView tx_exit_live;

    public CloseLiveFollowDialog(@NonNull Context context) {
        this(context, R.style.CustomDialogBottom);
    }

    public CloseLiveFollowDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        mContext = context;
        init();
        setLitener();
    }

    private void setLitener() {
        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dismiss();

            }
        });
    }

    private void init() {
        final View view = LayoutInflater.from(mContext).inflate(R.layout.close_live_follow_layout, null);
        setContentView(view);
        close_dialog = view.findViewById(R.id.close_dialog);
        button_follow_and_exit = view.findViewById(R.id.button_follow_and_exit);
        tx_exit_live = view.findViewById(R.id.tx_exit_live);

    }

    public void setFollowAndExitListener(View.OnClickListener onClickListener) {
        button_follow_and_exit.setOnClickListener(onClickListener);
    }
    public void setExitLiveListener(View.OnClickListener onClickListener) {
        tx_exit_live.setOnClickListener(onClickListener);
    }




}
