package com.thel.modules.live.in;

/**
 * Created by waiarl on 2017/5/11.
 */

public interface StreamInfoListener extends StreamListener {

    void onInfo(int what, int arg1, int arg2);

}
