package com.thel.modules.live.view.sound;

import android.content.Context;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.modules.live.in.LiveBaseView;

/**
 * Created by waiarl on 2018/1/23.
 */

public class LiveSoundBackListenView extends LinearLayout implements LiveBaseView<LiveSoundBackListenView> {
    private final Context mContext;
    private TextView txt_back_listen;
    private TextView txt_cancel;

    public LiveSoundBackListenView(Context context) {
        this(context, null);
    }

    public LiveSoundBackListenView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LiveSoundBackListenView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }

    private void init() {
        inflate(mContext, R.layout.live_sound_back_listen_view, this);
        txt_back_listen = findViewById(R.id.txt_back_listen);
        txt_cancel = findViewById(R.id.txt_cancel);
    }

    /***后台收听***/
    public void setBackListenListener(OnClickListener listener) {
        txt_back_listen.setOnClickListener(listener);
    }

    /***取消***/
    public void setCancelListener(OnClickListener listener) {
        txt_cancel.setOnClickListener(listener);
    }


    /*******************************************一下为接口继承方法******************************************************/
    @Override
    public LiveSoundBackListenView show() {
        return null;
    }

    @Override
    public LiveSoundBackListenView hide() {
        return null;
    }

    @Override
    public LiveSoundBackListenView destroyView() {
        return null;
    }

    @Override
    public boolean isAnimating() {
        return false;
    }

    @Override
    public void setAnimating(boolean isAnimating) {

    }

    @Override
    public void showShade(boolean show) {

    }
}
