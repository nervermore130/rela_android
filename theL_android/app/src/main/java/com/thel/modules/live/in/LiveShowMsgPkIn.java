package com.thel.modules.live.in;

/**
 * Created by waiarl on 2017/12/13.
 * 直播观看端想消息接口
 */

public interface LiveShowMsgPkIn {

    /**
     * PK开始时 服务器发送PK开始通知给双方直播室内的所有人
     *
     * @param payload
     */
    void receiverPkStartMsg(String payload);

    /**
     * PK结束时 服务器发送 PK结束 和 双方助攻前三名 通知给双方直播室内的所有人
     *
     * @param payload
     */
    void receivePkStopMsg(String payload);


    /**
     * 主播收到礼物时 服务器发送主播软妹币增量通知给双方直播室内的所有人:
     *
     * @param payload
     */
    void receiverPkGiftMsg(String payload);

    /**
     * 进入总结阶段 服务器发送 总结剩余时间 和 双方助攻前三名 给双方直播室内的所有人:
     *
     * @param payload
     */
    void receivePkSummaryMsg(String payload);

    /**
     * 被挂断方收到服务器的PK挂断消息
     *
     * @param payload
     */
    void receiveHangupMsg(String payload);
}
