package com.thel.modules.live.bean;

/**
 * Created by waiarl on 2017/12/11.
 */

public class LivePkAeAnimBean {
    public int id;

    public float width;
    public float height;
    public String ae_anim_path;
    public String sound_path;

    public LivePkAeAnimBean() {
    }

    public LivePkAeAnimBean(int id,float width, float height, String ae_anim_path, String sound_path) {
        this.id = id;
        this.width = width;
        this.height = height;
        this.ae_anim_path = ae_anim_path;
        this.sound_path = sound_path;
    }

    @Override
    public String toString() {
        return "livePkAeAnimBean{" +
                "width=" + width +
                ", height=" + height +
                ", ae_anim_path='" + ae_anim_path + '\'' +
                ", sound_path='" + sound_path + '\'' +
                ", id=" + id +
                '}';
    }


}
