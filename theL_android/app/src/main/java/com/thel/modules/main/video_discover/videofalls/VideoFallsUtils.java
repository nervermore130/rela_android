package com.thel.modules.main.video_discover.videofalls;

import android.graphics.Rect;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.thel.bean.video.VideoBean;
import com.thel.imp.black.BlackUtils;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.utils.SharedPrefUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by waiarl on 2018/2/6.
 */

public class VideoFallsUtils {
    static long MINUTI = 60 * 1000;
    static long HOUR = 60 * MINUTI;
    static long DAY = HOUR * 24;
    static SimpleDateFormat minuteFormat = new SimpleDateFormat("mm");
    static SimpleDateFormat secondFormat = new SimpleDateFormat("ss");

    /**
     * 瀑布流分割线设置
     *
     * @param recyclerview
     * @param padding
     * @param spanCount
     */
    public static void setFallsRecyclerViewDecoration(RecyclerView recyclerview, final float padding, final int spanCount) {
        final int ctPadding = (int) (2 * padding);
        recyclerview.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                if (parent.getAdapter() instanceof BaseRecyclerViewAdapter) {
                    if (parent.getChildLayoutPosition(view) < ((BaseRecyclerViewAdapter) parent.getAdapter()).getHeaderLayoutCount()) {
                        return;
                    }
                    final int pos = parent.getChildLayoutPosition(view) - ((BaseRecyclerViewAdapter) parent.getAdapter()).getHeaderLayoutCount();
                    if (pos % spanCount == 0) {//最左边一列
                        outRect.left = ctPadding;
                        outRect.right = ctPadding / 3;
                    } else if (pos % spanCount == spanCount - 1) {
                        outRect.left = ctPadding / 3;
                        outRect.right = ctPadding;
                    } else {
                        outRect.left = ctPadding * 2 / 3;
                        outRect.right = ctPadding * 2 / 3;
                    }
                    outRect.top = ctPadding;
                    outRect.bottom = 0;
                }
            }
        });
    }


    public static void filtNearbyMoment(VideoMomentListBean momentsListBean) {
        //过滤黑名单和被屏蔽
        if (momentsListBean == null || momentsListBean.list == null || momentsListBean.list.isEmpty()) {
            return;
        }
        // List<String> blackList = Arrays.asList(SharedPrefUtils.getString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.BLACK_LIST, "").split(","));
        List<String> blackList = BlackUtils.getBlackList();

        List<String> trimBlacklist = new ArrayList<>(blackList);
        trimBlacklist = removeEmptyElem(trimBlacklist);
        List<String> reportUserList = Arrays.asList(SharedPrefUtils.getString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.REPORT_USER_LIST, "").split(","));
        List<String> trimReportUserList = new ArrayList<>(blackList);
        trimReportUserList = removeEmptyElem(trimReportUserList);
        List<String> blockMeList = BlackUtils.getblockMeList();
        List<String> trimBlockMeUserList = new ArrayList<>(blackList);
        trimBlockMeUserList = removeEmptyElem(blockMeList);
        List<String> blockUsers = new ArrayList<String>();
        if (trimBlacklist.size() > 0 && !TextUtils.isEmpty(trimBlacklist.get(0))) {
            blockUsers.addAll(trimBlacklist);
        }
        if (trimReportUserList.size() > 0 && !TextUtils.isEmpty(trimReportUserList.get(0))) {
            blockUsers.addAll(trimReportUserList);
        }
        if (trimBlockMeUserList.size() > 0 && !TextUtils.isEmpty(trimBlockMeUserList.get(0))) {
            blockUsers.addAll(trimBlockMeUserList);
        }
        if (blockUsers.size() > 0) {
            List<VideoBean> blockUserList = new ArrayList<>();
            for (VideoBean momentsBean : momentsListBean.list) {
                for (String userId : blockUsers) {
                    if (userId.equals(momentsBean.userId + "")) {
                        blockUserList.add(momentsBean);
                        break;
                    }
                }
            }
            momentsListBean.list.removeAll(blockUserList);
        }

    }

    private static List<String> removeEmptyElem(List<String> processList) {
        List<String> list = new ArrayList<String>();
        list.add("");
        processList.removeAll(list);
        return processList;
    }

    public static void setPgcFallsRecyclerViewDecoration(RecyclerView recyclerview, float padding, final int spanCount) {
        final int ctPadding = (int) (2 * padding);
        recyclerview.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                if (parent.getAdapter() instanceof BaseRecyclerViewAdapter) {
                    final BaseRecyclerViewAdapter<VideoBean> adapter = (BaseRecyclerViewAdapter<VideoBean>) parent.getAdapter();
                    final int headerCount = adapter.getHeaderLayoutCount();
                    if (parent.getChildLayoutPosition(view) < headerCount) {
                        return;
                    }
                    final int pos = parent.getChildLayoutPosition(view) - headerCount;
                    final int dataListSize = adapter.getData().size();
                    if (pos >= 0 && pos < dataListSize) {
                        final VideoBean bean = adapter.getItem(pos);
                        if (VideoBean.VIDEO_TYPE_PGC == bean.videoType) {
                            outRect.left = ctPadding;
                            outRect.right = ctPadding;
                        } else {
                            final int spanPos = getSpanPos(adapter.getData(), pos);
                            if (spanPos % spanCount == 0) {//最左边一列
                                outRect.left = ctPadding;
                                outRect.right = ctPadding / 2;
                            } else {
                                outRect.left = ctPadding / 2;
                                outRect.right = ctPadding;
                            }
                        }
                    }
                    outRect.top = ctPadding;
                    outRect.bottom = 0;
                }
            }
        });
    }

    private static int getSpanPos(List<VideoBean> data, int pos) {
        int spanPos = 0;
        final int startIndex = pos - 1;
        for (int i = startIndex; i >= 0; i--) {
            final VideoBean videoBean = data.get(i);
            if (VideoBean.VIDEO_TYPE_PGC == videoBean.videoType) {
                return spanPos;
            } else {
                spanPos++;
            }
        }
        return spanPos;
    }

    public static String getFormatPlayTime(int second) {
        final int t = second * 1000;
        if (second >= 60) {
            return minuteFormat.format(t) + "'" + secondFormat.format(t) + "\"";
        } else {
            return secondFormat.format(t) + "\"";
        }
    }
}
