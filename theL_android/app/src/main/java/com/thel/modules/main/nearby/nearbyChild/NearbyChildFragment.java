package com.thel.modules.main.nearby.nearbyChild;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.growingio.android.sdk.collection.GrowingIO;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseFragment;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.user.NearUserBean;
import com.thel.bean.user.NearUserListNetBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.imp.AutoRefreshImp;
import com.thel.imp.TittleClickListener;
import com.thel.imp.black.BlackUsersChangedListener;
import com.thel.imp.black.BlackUtils;
import com.thel.modules.main.me.bean.LogInfoBean;
import com.thel.modules.main.me.match.eventcollect.collect.LiveLogUtils;
import com.thel.modules.main.nearby.NearbyFragment;
import com.thel.modules.main.nearby.Utils.NearbyUtils;
import com.thel.modules.main.nearby.adapter.NearbyUserRecyclerViewAdapter;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.ui.widget.DefaultEmptyView;
import com.thel.ui.widget.MySwipeRefreshLayout;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.utils.L;
import com.thel.utils.LocationUtils;
import com.thel.utils.PhoneUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by waiarl on 2017/9/22.
 */

public class NearbyChildFragment extends BaseFragment implements NearbyChildContract.View, AutoRefreshImp, TittleClickListener, BlackUsersChangedListener {
    public NearbyChildContract.Presenter presenter;
    private MySwipeRefreshLayout swipe_container;
    protected RecyclerView mRecyclerView;
    protected NearbyUserRecyclerViewAdapter mAdapter;
    private GridLayoutManager manager;

    private final int SPAN_COUNT = 3;
    protected ArrayList<NearUserBean> list = new ArrayList<>();
    private boolean isFirstCreate = true;
    protected boolean haveNextPage;
    protected int curPage = 1;
    protected String tag;
    private long lastRefreshTime = 0;//上次刷新时间
    private BlackUtils blackUtils;
    protected DefaultEmptyView empty_view;
    private boolean isAutoRefresh = false;
    public TextView open_location;
    public RelativeLayout rel_default_gps;
    private Location lastLocation;
    public static String TAG = "NearbyChildFragmentLotivation";
    private int ret;
    private static final int PERMISSION_REQUEST_CODE_LOCATION = 1;
    private String fineLocationPermissionString;
    private String coarseLocationPermissionString;
    private int PERMISSIONS_REQUEST_READ_LOCATION = 100;
    private String pageId;
    private String from_page_id;
    private String from_page;
    private String latitude;
    private String longitude;

    public static NearbyChildFragment getInstance(Bundle bundle) {
        NearbyChildFragment instance = new NearbyChildFragment();
        instance.setArguments(bundle);
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Bundle bundle = getArguments();
        tag = bundle.getString(NearbyFragment.TAG);
        isFirstCreate = true;
        blackUtils = new BlackUtils();
        blackUtils.registerReceiver(this);
        from_page = ShareFileUtils.getString(ShareFileUtils.rootSwitchPage, "");
        from_page_id = ShareFileUtils.getString(ShareFileUtils.rootSwitchPageId, "");
        if (TextUtils.isEmpty(from_page)) {
            from_page = ShareFileUtils.getString(ShareFileUtils.nearPeoplePage, "");
            from_page_id = ShareFileUtils.getString(ShareFileUtils.nearPeoplePageId, "");
        }
        pageId = Utils.getPageId();
        latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
        longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");


    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_nearby_child, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        findViewById(view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        new NearbyChildPresenter(this, tag);
        setListener();
        if (getUserVisibleHint()) {
            initData();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        tryRefreshData();
        if (isVisibleToUser && isFirstCreate && presenter != null) {
            initData();
        }
        if (!TextUtils.isEmpty(tag)) {
            if (isVisibleToUser) {
                MobclickAgent.onEvent(getActivity(), "nearby_tabs_" + tag);// 统计页面的访问次数
                MobclickAgent.onPageStart(this.getClass().getName() + ":" + tag);
            } else {
                MobclickAgent.onPageEnd(this.getClass().getName() + ":" + tag);
            }
        }
    }

    @Override
    public void tryRefreshData() {
        if (getUserVisibleHint() && !isFirstCreate && !TextUtils.isEmpty(tag) && !(PhoneUtils.getNetWorkType() == PhoneUtils.TYPE_NO)) {//如果不是第一次创建且为可见状态,且有网络
            if (lastRefreshTime != 0 && System.currentTimeMillis() - lastRefreshTime > REFRESH_RATE && swipe_container != null && !swipe_container.isRefreshing()) {//如果大于刷新时间并且不再刷新状态，则刷新
                isAutoRefresh = true;
                refreshData();
            }
        }
    }

    protected void findViewById(View view) {
        swipe_container = view.findViewById(R.id.swipe_container);
        ViewUtils.initSwipeRefreshLayout(swipe_container);
        mRecyclerView = view.findViewById(R.id.recyclerview);
        manager = new GridLayoutManager(getActivity(), SPAN_COUNT);
        mRecyclerView.setLayoutManager(manager);
        mAdapter = new NearbyUserRecyclerViewAdapter(list, false, NearbyFragment.TAG_NEARBY.equals(tag));
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.registerReceiver(getActivity());
        mAdapter.setLogDatas(pageId, from_page, from_page_id, "near");
        empty_view = view.findViewById(R.id.empty_view);
        rel_default_gps = view.findViewById(R.id.rel_default_gps);
        open_location = view.findViewById(R.id.open_location);
    }

    @Override
    public void setPresenter(NearbyChildContract.Presenter presenter) {
        this.presenter = presenter;
    }

    private void initData() {
        isFirstCreate = false;
        if (null != swipe_container) {
            swipe_container.post(new Runnable() {
                @Override
                public void run() {
                    swipe_container.setRefreshing(true);
                }
            });
        }
        refreshData();
    }

    protected void refreshData() {
        lastRefreshTime = System.currentTimeMillis();
        presenter.getRefreshData();
        if (lastLocation == null) {
            requestGaodeMapClient();
        }
    }


    private void setListener() {

        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isAutoRefresh = false;
                refreshData();
            }
        });
        mAdapter.setOnLoadMoreListener(new BaseRecyclerViewAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                mRecyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        if (haveNextPage) {
                            presenter.getLoadMoreData(curPage);
                        } else {
                            mAdapter.openLoadMore(0, false);
                            if (list.size() > 0) {
                                View view = getActivity().getLayoutInflater().inflate(R.layout.load_more_footer_layout, (ViewGroup) mRecyclerView.getParent(), false);
                                ((TextView) view.findViewById(R.id.text)).setText(getString(R.string.info_no_more));
                                mAdapter.addFooterView(view);
                            }
                        }
                    }
                });
            }
        });
        mAdapter.setReloadMoreListener(new BaseRecyclerViewAdapter.ReloadMoreListener() {
            @Override
            public void reloadMore() {
                mRecyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.removeAllFooterView();
                        mAdapter.openLoadMore(true);
                        presenter.getLoadMoreData(curPage);
                    }
                });
            }
        });
        mAdapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                gotoUserInfoActivity(position);
            }
        });

    }

    private void gotoUserInfoActivity(int position) {
        try {

            final NearUserBean tempUser = mAdapter.getItem(position);
            if (tempUser.itemType == 1) return;
            int tempUserId = tempUser.userId;
//            Intent intent = new Intent();
//            intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, tempUserId + "");
//            intent.setClass(getActivity(), UserInfoActivity.class);
//            startActivity(intent);
            FlutterRouterConfig.Companion.gotoUserInfo(tempUserId+"");
            GrowingIO.getInstance().track("NearbyCell");
            traceNearbyNewFaceleLog("click", position, tempUser.rank_id, tempUser.userId + "", "near");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void traceNearbyNewFaceleLog(String activity, int index, String rank_id, String userid, String sort) {
        try {
            LogInfoBean logInfoBean = new LogInfoBean();
            logInfoBean.page = "around.list";
            logInfoBean.page_id = pageId;
            logInfoBean.activity = activity;
            logInfoBean.from_page_id = from_page_id;
            logInfoBean.from_page = from_page;
            logInfoBean.lat = latitude;
            logInfoBean.lng = longitude;

            LogInfoBean.LogsDataBean logsDataBean = new LogInfoBean.LogsDataBean();

            if (index != -1) {
                logsDataBean.index = index;
            }

            if (!TextUtils.isEmpty(rank_id)) {
                logsDataBean.rank_id = rank_id;
            }

            if (!TextUtils.isEmpty(userid)) {
                logsDataBean.user_id = userid;
            }
            logsDataBean.sort = sort;
            logInfoBean.data = logsDataBean;

            LiveLogUtils.getInstance().addLog(logInfoBean);

        } catch (Exception e) {

        }
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void showRefreshData(NearUserListNetBean nearUserListNetBean) {
        emptyData(false);
        if (nearUserListNetBean == null || nearUserListNetBean.data == null) {
            emptyData(true);
            return;
        }
        final NearUserListNetBean.NearUserListBean dataBean = nearUserListNetBean.data;
        haveNextPage = dataBean.map_list.size() > 0;
        NearbyUtils.filterBlack(dataBean.map_list);
        NearbyUtils.filterUser(dataBean.map_list);
        list.clear();
        list.addAll(dataBean.map_list);
        mAdapter.removeAllFooterView();
        mAdapter.setNewData(list);
        curPage = 2;
        if (haveNextPage) {
            mAdapter.openLoadMore(list.size(), true);
        } else {
            mAdapter.openLoadMore(list.size(), false);
            if (list.isEmpty()) {
                emptyData(true);
            }
        }
    }


    @Override
    public void showLoadMoreData(NearUserListNetBean nearUserListNetBean) {
        if (nearUserListNetBean == null || nearUserListNetBean.data == null) {
            return;
        }
        final NearUserListNetBean.NearUserListBean dataBean = nearUserListNetBean.data;
        haveNextPage = dataBean.map_list.size() > 0;
        NearbyUtils.filterUser(dataBean.map_list);
        NearbyUtils.filterBlack(dataBean.map_list);
        NearbyUtils.filterEquallyMoment(list, dataBean.map_list);
        list.addAll(dataBean.map_list);
        curPage++;
        mAdapter.notifyDataChangedAfterLoadMore(true, list.size());
    }

    @Override
    public void loadMoreFailed() {
        mRecyclerView.post(new Runnable() {
            @Override
            public void run() {
                mAdapter.loadMoreFailed((ViewGroup) mRecyclerView.getParent());

            }
        });
    }

    @Override
    public void emptyData(boolean empty) {
        empty_view.setVisibility(empty ? View.VISIBLE : View.GONE);
    }

    @Override
    public void requestFinish() {
        if (swipe_container != null)
            swipe_container.postDelayed(new Runnable() {

                @Override
                public void run() {
                    if (swipe_container != null && swipe_container.isRefreshing())
                        swipe_container.setRefreshing(false);
                }
            }, 1000);
    }

    @Override
    public void showNearbyMomentData(List<MomentsBean> momentsBeanList) {

    }

    @Override
    public void showNearbyPigData(List<NearUserBean> nearUserBeanList, boolean isRefresh, boolean hasMoreData) {

    }

    /**
     * 点击头部上滑到最上面
     */
    @Override
    public void onTitleClick() {
        if (swipe_container != null && !swipe_container.isRefreshing() && isVisible() && mRecyclerView != null) {
            mRecyclerView.smoothScrollToPosition(0);
        }
    }

    @Override
    public void onDestroy() {
        blackUtils.unRegisterReceiver(this);
        mAdapter.unRegisterReceiver(getActivity());

        super.onDestroy();
    }

    @Override
    public void onBlackUsersChanged(String userId, boolean isAdd) {
        if (TextUtils.isEmpty(userId) || userId.equals("0") || !isAdd) {
            return;
        }
        if (swipe_container != null && !swipe_container.isRefreshing() && mAdapter != null && mAdapter.getData() != null) {
            final List<NearUserBean> list = mAdapter.getData();
            for (int i = 0; i < list.size(); i++) {
                if ((list.get(i).userId + "").equals(userId)) {
                    mAdapter.remove(i);
                    i -= 1;
                }
            }
        }
    }

    protected boolean canShowNewLiveGuide() {
        return !isAutoRefresh;
        //return false;//需求未定，先不显示
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    private void requestGaodeMapClient() {
        L.i(TAG, "aMapLocation getErrorCode111111");

        final AMapLocationClient locationClient = new AMapLocationClient(TheLApp.getContext());
        locationClient.setLocationListener(new AMapLocationListener() {
            @Override
            public void onLocationChanged(AMapLocation aMapLocation) {

                if (aMapLocation != null && aMapLocation.getErrorCode() == 0) {
                    L.i(TAG, "aMapLocation getLatitude" + aMapLocation.getLatitude() + "aMapLocation.getLongitude() " + aMapLocation.getLongitude());

                    LocationUtils.saveLocation(aMapLocation.getLatitude(), aMapLocation.getLongitude(), aMapLocation.getCity());
                } else if (aMapLocation != null && aMapLocation.getErrorCode() == 12) { //定位权限没有打开
                    LocationUtils.saveLocation(0.0, 0.0, "");

                } else {
                    L.i(TAG, "aMapLocation getErrorCode" + aMapLocation.getErrorCode());

                }
                locationClient.stopLocation();
                locationClient.onDestroy();
            }
        });
        final AMapLocationClientOption mLocationOption = new AMapLocationClientOption();
        mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Battery_Saving);
        locationClient.setLocationOption(mLocationOption);
        locationClient.startLocation();
    }


}


