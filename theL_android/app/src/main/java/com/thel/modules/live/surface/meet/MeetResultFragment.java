package com.thel.modules.live.surface.meet;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.thel.R;
import com.thel.base.BaseFragment;
import com.thel.bean.live.LiveMultiSeatBean;
import com.thel.constants.TheLConstants;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.main.me.aboutMe.MatchSuccessParticleView;
import com.thel.ui.popupwindow.SharePopupWindow;
import com.thel.ui.widget.MeetAnimLayout;
import com.thel.ui.widget.MeetItemLayout;
import com.thel.utils.L;
import com.thel.utils.UserUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MeetResultFragment extends BaseFragment {

    @BindView(R.id.meet_top_left_view)
    MeetItemLayout meet_top_left_view;

    @BindView(R.id.meet_top_right_view)
    MeetItemLayout meet_top_right_view;

    @BindView(R.id.meet_bottom_left_view)
    MeetItemLayout meet_bottom_left_view;

    @BindView(R.id.meet_bottom_right_view)
    MeetItemLayout meet_bottom_right_view;

    @BindView(R.id.avatar_left_iv)
    ImageView avatar_left_iv;

    @BindView(R.id.user_name_left_tv)
    TextView user_name_left_tv;

    @BindView(R.id.avatar_right_iv)
    ImageView avatar_right_iv;

    @BindView(R.id.user_name_right_tv)
    TextView user_name_right_tv;

    @BindView(R.id.success_rl)
    RelativeLayout success_rl;

    @BindView(R.id.failed_rl)
    RelativeLayout failed_rl;

    @BindView(R.id.meet_failed_content_tv)
    TextView meet_failed_content_tv;

    @BindView(R.id.meet_failed_tv)
    TextView meet_failed_tv;

    @BindView(R.id.meet_success_tv)
    TextView meet_success_tv;

    @BindView(R.id.meet_success_content_tv)
    TextView meet_success_content_tv;

    @BindView(R.id.meet_failed_iv)
    ImageView meet_failed_iv;

    @BindView(R.id.play_with_friends_tv)
    TextView play_with_friends_tv;

    @BindView(R.id.meet_failed_play_with_friends_tv)
    TextView meet_failed_play_with_friends_tv;

    @BindView(R.id.meetSuccessView)
    MatchSuccessParticleView meetSuccessView;

    @BindView(R.id.meet_anim_view)
    MeetAnimLayout meet_anim_view;

    @BindView(R.id.meeting_failure_anim_view)
    LottieAnimationView meeting_failure_anim_view;

    private List<LiveMultiSeatBean.CoupleDetail> coupleDetail = new ArrayList<>();

    private int role = -1;//

    private int meet_result_role = -1;

    private boolean isMeetSuccess = true;

    private int meetSuccessPairCount = 0;

    private boolean isShowShareBtn = true;

    private int meetStatus = -1;

    private LiveMultiSeatBean liveMultiSeatBean;

    private boolean isFailure = false;
    /**
     * 播放成功动画
     */
    int[] src = new int[]{R.mipmap.match_color1, R.mipmap.match_color2, R.mipmap.match_color3, R.mipmap.match_color4, R.mipmap.match_color5};
    private List<LiveMultiSeatBean.CoupleDetail> allDetail = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_meet_result, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initData();

    }

    public void setData(List<LiveMultiSeatBean.CoupleDetail> coupleDetail) {

        this.coupleDetail = coupleDetail;

    }

    public void setMeetResultRole(int meet_result_role) {
        this.meet_result_role = meet_result_role;
    }

    public void isShowShareBtn(boolean isShowShareBtn) {
        this.isShowShareBtn = isShowShareBtn;
    }

    private void initData() {

        Intent intent = getActivity().getIntent();

        if (intent != null) {

            Bundle bundle = intent.getExtras();

            if (bundle != null) {

                liveMultiSeatBean = (LiveMultiSeatBean) bundle.getSerializable(TheLConstants.BUNDLE_KEY_LIVE_SEAT_ROOM);

                role = bundle.getInt(TheLConstants.BUNDLE_KEY_ROLE);
                if (coupleDetail.size() == 0) {
                    isMeetSuccess = false;
                    success_rl.setVisibility(View.GONE);
                    failed_rl.setVisibility(View.VISIBLE);
                    meeting_failure_anim_view.setAnimation("meet_failed_emoji.json");
                    meeting_failure_anim_view.playAnimation();
                } else {
                    isMeetSuccess = true;
                    success_rl.setVisibility(View.VISIBLE);
                    failed_rl.setVisibility(View.GONE);
                    meetSuccessView.initView(src);
                    meetSuccessView.start();
                }

                initUI();
            }
        }

    }

    private void initUI() {

        if (isShowShareBtn) {
            play_with_friends_tv.setVisibility(View.VISIBLE);
            meet_failed_play_with_friends_tv.setVisibility(View.GONE);

        } else {
            meet_failed_play_with_friends_tv.setVisibility(View.VISIBLE);
            play_with_friends_tv.setVisibility(View.GONE);

        }

        if (coupleDetail != null && coupleDetail.size() > 0) {

            meetSuccessPairCount = coupleDetail.size() / 2;

            //配对成功1对
            if (meetSuccessPairCount == 1) {

                setCount(1);

                meet_anim_view.setData(coupleDetail.get(0), coupleDetail.get(1));

            }

            //配对成功2对
            if (meetSuccessPairCount == 2) {
                setCount(2);

                meet_top_left_view.setData(coupleDetail.get(0), coupleDetail.get(1));

                meet_bottom_left_view.setData(coupleDetail.get(2), coupleDetail.get(3));

            }

            //配对成功3对
            if (meetSuccessPairCount == 3) {
                setCount(3);

                meet_top_left_view.setData(coupleDetail.get(0), coupleDetail.get(1));

                meet_top_right_view.setData(coupleDetail.get(2), coupleDetail.get(3));

                meet_bottom_left_view.setData(coupleDetail.get(4), coupleDetail.get(5));

            }

            //配对成功4对
            if (meetSuccessPairCount == 4) {
                setCount(4);

                meet_top_left_view.setData(coupleDetail.get(0), coupleDetail.get(1));

                meet_top_right_view.setData(coupleDetail.get(2), coupleDetail.get(3));

                meet_bottom_left_view.setData(coupleDetail.get(4), coupleDetail.get(5));

                meet_bottom_right_view.setData(coupleDetail.get(6), coupleDetail.get(7));

            }
        }

        L.d("MeetResultFragment", " role : " + role);

        L.d("MeetResultFragment", " isMeetSuccess : " + isMeetSuccess);

        L.d("MeetResultFragment", " meet_result_role : " + meet_result_role);


        switch (meet_result_role) {
            case TheLConstants.MeetResultConstant.MEET_RESULT_BROADCASTER:

                role = MeetResultActivity.BROADCASTER;

                if (isMeetSuccess) {
                    isFailure = false;
                    meet_success_tv.setText(getContext().getResources().getString(R.string.meet_success));
                    meet_success_content_tv.setText(getContext().getResources().getString(R.string.best_go_between));
                } else {
                    isFailure = true;

                    meet_failed_tv.setText(getContext().getResources().getString(R.string.no_meeting));
                    meet_failed_content_tv.setText(getContext().getResources().getString(R.string.result_fail));
                }
                break;
            case TheLConstants.MeetResultConstant.MEET_RESULT_GUEST_ME:
                if (isMeetSuccess) {
                    role = MeetResultActivity.GUEST;
                    isFailure = false;
                    meet_success_tv.setText(getContext().getResources().getString(R.string.meet_success));
                    meet_success_content_tv.setText(getContext().getResources().getString(R.string.meet_result_success_guest));
                } else {
                    if (allDetail != null && allDetail.size() > 0) {
                        isFailure = false;
                        role = MeetResultActivity.GUESTALL;
                    } else {
                        isFailure = true;
                    }

                    meet_failed_tv.setText(getContext().getResources().getString(R.string.bad_luck));
                    meet_failed_content_tv.setText(getContext().getResources().getString(R.string.girls_are_waiting_for_you));
                    ImageLoaderManager.imageLoader(meet_failed_iv, R.mipmap.bg_meet_success_bg);
                }
                break;
            case TheLConstants.MeetResultConstant.MEET_RESULT_GUEST_ALL:

                if (isMeetSuccess) {

                    meet_success_tv.setText(getContext().getResources().getString(R.string.meet_success));

                    isFailure = false;

                    switch (getMeetStatus()) {

                        case 1:

                            meet_failed_content_tv.setText(getContext().getResources().getString(R.string.result_no_meeting));
                            break;

                        case 2:
                            role = MeetResultActivity.GUEST;

                            meet_success_content_tv.setText(getContext().getResources().getString(R.string.the_luckiest_gil));
                            break;

                        case 3:
                            role = MeetResultActivity.GUEST;

                            meet_success_content_tv.setText(getContext().getResources().getString(R.string.see_other_ladys_meets));
                            break;

                        case 4:
                            role = MeetResultActivity.GUESTALL;

                            meet_success_content_tv.setText(getContext().getResources().getString(R.string.girls_are_waiting_for_you));
                            break;

                    }

                } else {
                    isFailure = true;
                    meet_failed_tv.setText(getContext().getResources().getString(R.string.no_meeting));
                    meet_failed_content_tv.setText(getContext().getResources().getString(R.string.result_no_meeting));
                }


                break;
            case TheLConstants.MeetResultConstant.MEET_RESULT_AUDIENCE:

                if (isMeetSuccess) {
                    role = MeetResultActivity.AUDIENCE;
                    isFailure = false;
                    meet_success_tv.setText(getContext().getResources().getString(R.string.meet_success));
                    meet_success_content_tv.setText(getContext().getResources().getString(R.string.meet_result_success_viewer));
                } else {
                    isFailure = true;
                    meet_failed_tv.setText(getContext().getResources().getString(R.string.no_meeting));
                    meet_failed_content_tv.setText(getContext().getResources().getString(R.string.miss_each_ohter));
                }
                break;
        }
    }

    //设置相遇成功数量
    private void setCount(int count) {
        switch (count) {
            case 1:
                meet_top_left_view.setVisibility(View.GONE);
                meet_top_right_view.setVisibility(View.GONE);
                meet_bottom_left_view.setVisibility(View.GONE);
                meet_bottom_right_view.setVisibility(View.GONE);
                meet_anim_view.setVisibility(View.VISIBLE);
                break;
            case 2:
                meet_top_left_view.setVisibility(View.VISIBLE);
                meet_top_right_view.setVisibility(View.GONE);
                meet_bottom_left_view.setVisibility(View.VISIBLE);
                meet_bottom_right_view.setVisibility(View.GONE);
                meet_anim_view.setVisibility(View.GONE);
                break;
            case 3:
                meet_top_left_view.setVisibility(View.VISIBLE);
                meet_top_right_view.setVisibility(View.VISIBLE);
                meet_bottom_left_view.setVisibility(View.VISIBLE);
                meet_bottom_right_view.setVisibility(View.GONE);
                meet_anim_view.setVisibility(View.GONE);

                break;
            case 4:
                meet_top_left_view.setVisibility(View.VISIBLE);
                meet_top_right_view.setVisibility(View.VISIBLE);
                meet_bottom_left_view.setVisibility(View.VISIBLE);
                meet_bottom_right_view.setVisibility(View.VISIBLE);
                meet_anim_view.setVisibility(View.GONE);
                break;
        }
    }

    @OnClick({R.id.meet_success_back_tv, R.id.meet_failed_back_tv})
    void onBackLiveRoom() {
        meetSuccessView.stop();
        getActivity().finish();
    }

    @OnClick(R.id.play_with_friends_tv)
    void onPlayWithFriends() {

        if (getView() != null && liveMultiSeatBean != null && role >= 0) {
            SharePopupWindow sharePopupWindow = new SharePopupWindow(getContext());

            sharePopupWindow.setData(liveMultiSeatBean, role, isFailure);
            sharePopupWindow.showAtLocation(getView(), Gravity.BOTTOM, 0, 0);
        }
    }

    @OnClick(R.id.meet_failed_play_with_friends_tv)
    void failedShare() {

        if (getView() != null && role >= 0 && liveMultiSeatBean == null || coupleDetail.size() == 0) {

            //2.在全场0匹配的情况下  背景同相遇成功，人像对方换成“？？？”，底部文案是“等待和你相遇”
            SharePopupWindow sharePopupWindow = new SharePopupWindow(getContext());
            sharePopupWindow.setData(liveMultiSeatBean, role, isFailure);
            sharePopupWindow.showAtLocation(getView(), Gravity.BOTTOM, 0, 0);

        }
    }

    public void setAllData(List<LiveMultiSeatBean.CoupleDetail> coupleDetails) {
        this.allDetail = coupleDetails;

    }

    private int getMeetStatus() {

        //全场0相遇
        if (coupleDetail.size() == 0) {
            return 1;
        }

        //只有自己相遇成功
        if (coupleDetail.size() == 2 && isMeetSuccessByMe()) {
            return 2;
        }

        //自己和别人都相遇成功
        if (coupleDetail.size() >= 4 && isMeetSuccessByMe()) {
            return 3;
        }

        //自己失败别人成功
        if (coupleDetail.size() >= 2 && !isMeetSuccessByMe()) {
            return 4;
        }

        return -1;

    }

    private boolean isMeetSuccessByMe() {
        if (coupleDetail != null) {

            for (int i = 0; i < coupleDetail.size(); i++) {

                final LiveMultiSeatBean.CoupleDetail cd = coupleDetail.get(i);

                final String userId = UserUtils.getMyUserId();

                if (userId.equals(cd.userId)) {

                    return true;
                }

            }

        }
        return false;
    }

}
