package com.thel.modules.live.liveBigGiftAnimLayout;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.LottieComposition;
import com.airbnb.lottie.OnCompositionLoadedListener;
import com.thel.R;
import com.thel.app.TheLApp;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by test1 on 2017/5/2.
 */

public class AeGiftAnimView {
    private final Context mContext;
    private int mDuration;
    private boolean isPlaying;
    private int mWidth;
    private final Handler mHandler;
    private int mHeight;
    private static OkHttpClient client;
    private LottieAnimationView animationView;
    private RelativeLayout background;
    private static final String TAG = "Download";

    public AeGiftAnimView(Context context, int duration) {
        mContext = context;
        mDuration = duration;
        mHandler = new Handler(Looper.getMainLooper());

    }

    public int start(final ViewGroup parent, int duration) {
        isPlaying = true;
        mDuration = duration;
        mWidth = parent.getMeasuredWidth();
        mHeight = parent.getMeasuredHeight();
        if (mWidth == 0 || mHeight == 0) {
            return 0;
        }
        background = new RelativeLayout(mContext);
        parent.addView(background);
        background.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
        setBackground(background);
        //执行完时间就将动画移除
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                isPlaying = false;
                parent.removeView(background);
            }
        }, mDuration);
        animationView.cancelAnimation();
        if (isPlaying) {
            animationView.playAnimation();
        }
        return mDuration;
    }

    private void setBackground(RelativeLayout background) {
        addAeAnimation(background);

    }

    private void addAeAnimation(RelativeLayout background) {
        View view = RelativeLayout.inflate(mContext, R.layout.add_aeanimation_layout, null);
        background.addView(view);
        view.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
        animationView = view.findViewById(R.id.animation_view);
    }

    public void loadUrl(String url) {
        final Request request;
        try {
            request = new Request.Builder().url(url).build();
        } catch (IllegalArgumentException e) {
            // onLoadError();
            Log.e(TAG, "invalid asset download url");
            return;
        }

        if (client == null) {
            client = new OkHttpClient();
        }
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(TAG, "invalid asset download url", e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) {
                    // onLoadError();
                    Log.e(TAG, "invalid asset download url");
                }

                try {
//                    JSONObject json = new JSONObject(response.body().string());

                    LottieComposition.Factory.fromJsonString(response.body().string(), new OnCompositionLoadedListener() {
                        @Override
                        public void onCompositionLoaded(LottieComposition composition) {
                            setComposition(composition);
                        }
                    });
                } catch (Exception e) {

                    Log.e(TAG, "invalid asset download url", e);
                }
            }
        });
    }

    private void setComposition(LottieComposition composition) {
        if (animationView != null && composition != null) {
            animationView.setComposition(composition);
        }
    }

}
