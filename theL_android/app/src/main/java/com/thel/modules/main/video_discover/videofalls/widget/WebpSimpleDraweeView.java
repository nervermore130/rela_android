package com.thel.modules.main.video_discover.videofalls.widget;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Animatable;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.AttributeSet;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.image.ImageInfo;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.thel.modules.main.video_discover.videofalls.RecyclerViewStateBean;
import com.thel.utils.ImageUtils;

/**
 * Created by waiarl on 2018/3/9.
 */

public class WebpSimpleDraweeView extends SimpleDraweeView {
    private final Context mContext;
    private WindowStatusChangedListener windowStatusChangedListener;
    private ControllerListener controllerListener;
    private volatile boolean isAttachedToWindow = false;
    private RecyclerViewStateBean recyclerViewStateBean;
    private RecyclerViewStateBean.RecyclerViewScrollStateChangedListener crollStateChangedListener;
    private float mFullHeight = 0;
    private float AutoPlayHeightPercent = 0.6f;
    private float AutoPlayHeight = 0;


    public WebpSimpleDraweeView(Context context) {
        this(context, null);
    }

    public WebpSimpleDraweeView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public WebpSimpleDraweeView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mContext = context;
        init();
    }

    private void init() {
        controllerListener = new ControllerListener();
        crollStateChangedListener = new RecyclerViewStateBean.RecyclerViewScrollStateChangedListener() {
            @Override
            public void stateChanged(int state) {
                if (state == RecyclerViewStateBean.State_IDLE && isAttachedToWindow) {
                    if (getController() != null) {
                        final Animatable animatable = getController().getAnimatable();
                        if (animatable != null && !animatable.isRunning() && canPlayAnim()) {
                            playAnim(animatable, true);
                        } else if (animatable != null && animatable.isRunning() && !canPlayAnim()) {
                            playAnim(animatable, false);

                        }
                    }
                }
            }
        };
    }

    public RecyclerViewStateBean.RecyclerViewScrollStateChangedListener getRecyclerViewScrollStateChangedListener() {
        return crollStateChangedListener;
    }

    public WebpSimpleDraweeView initRecyclerViewState(RecyclerViewStateBean recyclerViewStateBean) {
        this.recyclerViewStateBean = recyclerViewStateBean;
        if (recyclerViewStateBean != null) {
            recyclerViewStateBean.addWebpSimpleDraweeView(this);
        }
        return this;
    }

    public WebpSimpleDraweeView setFullHeight(float height) {
        this.mFullHeight = height;
        AutoPlayHeight = height * AutoPlayHeightPercent;
        return this;
    }

    public WebpSimpleDraweeView setImageUrl(String url, float width, float height) {
        setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(url, width, height))).build()).setAutoPlayAnimations(true).build());
        return this;
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        isAttachedToWindow = true;
        if (windowStatusChangedListener != null) {
            windowStatusChangedListener.onAttachedToWindow();
        }
        if (getController() != null) {
            final Animatable animatable = getController().getAnimatable();
            if (animatable != null && !animatable.isRunning()) {
                if (recyclerViewStateBean != null && recyclerViewStateBean.mState == RecyclerViewStateBean.State_IDLE && canPlayAnim()) {
                    playAnim(animatable, true);
                } else if (recyclerViewStateBean == null && canPlayAnim()) {
                    playAnim(animatable, true);
                }
            }
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        isAttachedToWindow = false;
        if (windowStatusChangedListener != null) {
            windowStatusChangedListener.onDetachedFromWindow();
        }
    }

    @Override
    protected void onWindowVisibilityChanged(int visibility) {
        super.onWindowVisibilityChanged(visibility);
        if (windowStatusChangedListener != null) {
            windowStatusChangedListener.onWindowVisibilityChanged(visibility);
        }
    }


    public interface WindowStatusChangedListener {
        void onAttachedToWindow();

        void onDetachedFromWindow();

        void onWindowVisibilityChanged(int visibility);
    }

    public void setWindowStatusChangedListener(WindowStatusChangedListener windowStatusChangedListener) {
        this.windowStatusChangedListener = windowStatusChangedListener;
    }

    public void setWebpUrl(String url) {
        setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(url)).build()).setControllerListener(controllerListener).build());

    }

    private boolean canPlayAnim() {
        final Rect localRect = new Rect();
        getLocalVisibleRect(localRect);
        final int visibleHeight = Math.abs(localRect.height());
        return visibleHeight >= AutoPlayHeight;
    }

    private void playAnim(@NonNull Animatable animatable, boolean play) {
        // enableOrDisableHardwareLayer(play);
        if (play) {
            animatable.start();
        } else {
            animatable.stop();
        }
    }

    private void enableOrDisableHardwareLayer(boolean hardware) {
        setLayerType(hardware ? LAYER_TYPE_HARDWARE : LAYER_TYPE_SOFTWARE, null);
    }

    class ControllerListener extends BaseControllerListener<ImageInfo> {

        @Override
        public void onFinalImageSet(String id, @Nullable ImageInfo imageInfo, @Nullable Animatable animatable) {
            if (animatable != null && !animatable.isRunning() && isAttachedToWindow) {
                if (recyclerViewStateBean != null && recyclerViewStateBean.mState == RecyclerViewStateBean.State_IDLE && canPlayAnim()) {
                    playAnim(animatable, true);
                } else if (recyclerViewStateBean == null && canPlayAnim()) {
                    playAnim(animatable, true);
                }
            }
        }
    }

}
