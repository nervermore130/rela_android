package com.thel.modules.live.view.expensive;

import com.thel.modules.live.bean.SoftGiftBean;

import java.io.Serializable;

/**
 * Created by waiarl on 2018/3/2.
 */

public class TopGiftBean implements Serializable {
    public String userId;
    public String nickName;
    public String avatar;

    public String toUserId;
    public String toNickName;
    public String toAvatar;

    public int id;

    public int userLevel;
    public SoftGiftBean softGiftBean;

    @Override
    public String toString() {
        return "MostExpensiveGiftBean{" +
                "userId='" + userId + '\'' +
                ", nickName='" + nickName + '\'' +
                ", avatar='" + avatar + '\'' +
                ", toUserId='" + toUserId + '\'' +
                ", toNickName='" + toNickName + '\'' +
                ", toAvatar='" + toAvatar + '\'' +
                ", id=" + id +
                ", softGiftBean=" + softGiftBean +
                '}';
    }
}
