package com.thel.modules.main.userinfo;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

/**
 * Created by the L on 2016/12/19.
 */

public class UserInfoFloatingLayout extends LinearLayout {
    private boolean neverShow = false;
    public boolean mIsAnimatingOut = false;

    public UserInfoFloatingLayout(Context context) {
        this(context, null);
    }

    public UserInfoFloatingLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public UserInfoFloatingLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public boolean isNeverShow() {
        return neverShow;
    }

    public void setNeverShow(boolean neverShow) {
        this.neverShow = neverShow;
    }
}
