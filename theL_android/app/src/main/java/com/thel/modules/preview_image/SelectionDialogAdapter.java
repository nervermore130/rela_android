package com.thel.modules.preview_image;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;

public class SelectionDialogAdapter extends BaseAdapter {

    private final int setlastItem;  //显示几个条目为红色  从0开始 0为三条 1 为1条 2不用变色
    private LayoutInflater mInflater;

    private String[] listData;

    private boolean setLastItemColor = false;

    public SelectionDialogAdapter(String[] listData, boolean setLastItemColor, int setLastItem) {
        this.listData = listData;
        this.setLastItemColor = setLastItemColor;
        this.setlastItem = setLastItem;
        mInflater = (LayoutInflater) TheLApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void refreshList(int selectPosition) {
    }

    public void updataList(int selectPosition, String[] listData) {
        this.listData = listData;
    }

    @Override
    public int getCount() {
        return listData.length;
    }

    @Override
    public Object getItem(int position) {
        return listData[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {
        convertView = mInflater.inflate(R.layout.selection_dialog_listitem, parent, false);
        TextView txt = convertView.findViewById(R.id.txt);

        txt.setText(listData[position]);
        //		if (position == listData.length - 1 && setLastItemColor) {
        //			txt.setTextColor(TheLApp.getContext().getResources()
        //					.getColor(R.color.orange));
        //		}
        //调整一下日志菜单顺序：举报（红字），隐藏这条日志（红字），隐藏她的全部日志（红字），收藏，分享，生成海报
        if (setlastItem == 0 && (position == 0 || position == 1 || position == 2) && setLastItemColor) {
            txt.setTextColor(TheLApp.getContext().getResources().getColor(R.color.red));
        } else if (setlastItem == 1 && position == 0 && setLastItemColor) {
            txt.setTextColor(TheLApp.getContext().getResources().getColor(R.color.red));
        } else {
            txt.setTextColor(TheLApp.getContext().getResources().getColor(R.color.dialog_color_text));
        }
        convertView.setBackgroundColor(0x00000000);// 更改整行的背景色
        return convertView;

    }

}
