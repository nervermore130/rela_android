package com.thel.modules.main.home;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thel.BuildConfig;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseFragment;
import com.thel.constants.TheLConstants;
import com.thel.imp.AutoRefreshImp;
import com.thel.imp.TittleClickListener;
import com.thel.modules.main.home.community.CommunityFragment;
import com.thel.modules.main.home.follow.FollowFragment;
import com.thel.modules.main.home.search.SearchActivity;
import com.thel.modules.test.TestActivity;
import com.thel.ui.widget.IndicatorDrawable;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.StringUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by liuyun on 2017/9/18.
 */

public class HomeFragment extends BaseFragment implements HomeContract.View {

    @BindView(R.id.search_iv)
    ImageView search_iv;

    @BindView(R.id.lin_write_moment)
    LinearLayout lin_write_moment;

    @BindView(R.id.listView_failed_moments)
    ExpandableListView listView_failed_moments;

    @BindView(R.id.lin_new_moments_remind)
    LinearLayout lin_new_moments_remind;

    @BindView(R.id.txt_new_moments_remind)
    TextView txt_new_moments_remind;

    @BindView(R.id.img_new)
    ImageView img_new;

    @BindView(R.id.music_frame)
    FrameLayout music_frame;

    @BindView(R.id.tablayout)
    TabLayout tablayout;

    @BindView(R.id.moments_viewpager)
    ViewPager moments_viewpager;

    private List<Fragment> fragmentList;

    private HomeContract.Presenter mPresenter;

    private FollowFragment mFollowFragment;
    private final int TAB_FOLLOW = 0;

    private final int TAB_VIDEO = 1;
    private final int TAB_COMMUNITY = 2;
    private List<String> titleList = new ArrayList<>();
    private int current_tab = TAB_FOLLOW;

    public static HomeFragment getInstance() {
        return new HomeFragment();
    }

    private MessageReceiver receiver;
    private String pageId;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        //开发版本测试专用
        if (BuildConfig.DEBUG && !L.IS_USE_BAIDU_TEST) {
            view.findViewById(R.id.right_area).setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
//                    startActivity(new Intent(getActivity(), AgoraTestActivity.class));
                    startActivity(new Intent(getActivity(), TestActivity.class));
                    return false;
                }
            });
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setTabs();

        new HomePresenter(this);

        setLisener();

        registerReceiver();

    }

    private void setLisener() {
        tablayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                current_tab = tab.getPosition();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                final int position = tab.getPosition();
                if (fragmentList.size() > position) {
                    final Fragment fragment = fragmentList.get(position);
                    if (fragment instanceof TittleClickListener) {
                        ((TittleClickListener) fragment).onTitleClick();
                    }
                }
            }
        });
        moments_viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                FollowFragment followFragment = (FollowFragment) fragmentList.get(0);
                CommunityFragment communityFragment = (CommunityFragment) fragmentList.get(1);
                if (position == 0) {
                    followFragment.setFromPageAndId("theme.list", pageId);
                } else {
                    communityFragment.setFromPageAndId("friendmoments", pageId);
                }
                //  fragment.setArguments();


            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
         /* follow_rb.setOnClickListener(new OnDoubleClickListener() {

            @Override
            protected void onDoubleClick(View v) {
                if (mFollowFragment != null) {
                    mFollowFragment.scrollToTop();
                }
            }
        });*/
    }

    private void setTabs() {
        initFragment();
        moments_viewpager.setAdapter(new HomeAdapter(getChildFragmentManager(), fragmentList, titleList));

        final int size = titleList.size();
        for (int i = 0; i < size; i++) {
            tablayout.addTab(tablayout.newTab());
        }
        moments_viewpager.setOffscreenPageLimit(2);
        View tabStripView = tablayout.getChildAt(0);
        tabStripView.setBackground(new IndicatorDrawable(tabStripView, ContextCompat.getColor(TheLApp.context, R.color.white)));

        tablayout.setupWithViewPager(moments_viewpager);
    }

    private void initFragment() {
        if (fragmentList == null) {
            fragmentList = new ArrayList<>();
        }

        showMusicFragment();

        fragmentList.clear();
        titleList.clear();

        //关注页面
        mFollowFragment = FollowFragment.getInstance();
        pageId = Utils.getPageId();

        ShareFileUtils.setString(ShareFileUtils.rootSwitchPage, "welcomePage");
        ShareFileUtils.setString(ShareFileUtils.rootSwitchPageId, pageId);

        fragmentList.add(mFollowFragment);
        titleList.add(StringUtils.getString(R.string.moments_activity_follow));

        //社区页面
        if (Utils.isChaneseLanguage()) {
            CommunityFragment communityFragment = CommunityFragment.getInstance();
            fragmentList.add(communityFragment);
            titleList.add(StringUtils.getString(R.string.info_topic));
        }

    }

    @Override
    public void showMusicFragment() {

        music_frame.setVisibility(View.GONE);

    }


    @Override
    public void setPresenter(HomeContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @OnClick(R.id.lin_add_friend)
    void jumpSearchActivity() {
        startActivity(new Intent(getContext(), SearchActivity.class));
    }

    @OnClick(R.id.right_area)
    void displayReleaseMomentDialog() {

        if (UserUtils.isVerifyCell()) {
//            ReleaseMomentDialog releaseMomentDialog = new ReleaseMomentDialog(getActivity());
//            releaseMomentDialog.show();

            if (getActivity() != null) {
                getActivity().startActivity(new Intent(getActivity(), ReleaseActivity.class));
            }
        }
    }

    @Override
    public Activity getHomeActivity() {
        return getActivity();
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe();
        tryRefreshCurrentFragment();

    }

    /**
     * 尝试刷新当前的fragment
     */
    private void tryRefreshCurrentFragment() {
        if (moments_viewpager == null || moments_viewpager.getAdapter() == null) {
            return;
        }
        final int currentTab = moments_viewpager.getCurrentItem();
        if (fragmentList != null && fragmentList.size() > currentTab) {
            final Fragment fragment = fragmentList.get(currentTab);
            if (fragment instanceof AutoRefreshImp) {
                ((AutoRefreshImp) fragment).tryRefreshData();
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onDestroy() {
        getActivity().unregisterReceiver(receiver);
        super.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unSubscribe();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);

        if (mFollowFragment != null) {

            mFollowFragment.setHiddenChanged(hidden);

        }

        L.d("HomeFragment", " onHiddenChanged : " + hidden);

    }

    public void refreshFollow() {
        if (mFollowFragment != null) {
            mFollowFragment.refreshData();
        }
    }

    public void showTheme() {
        if (moments_viewpager != null) {
            moments_viewpager.setCurrentItem(2);
        }
    }

    // 注册朋友圈消息广播接收器
    private void registerReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(TheLConstants.BROADCAST_GOTO_THEME_TAB);
        receiver = new MessageReceiver();
        getActivity().registerReceiver(receiver, intentFilter);
    }

    class MessageReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (TheLConstants.BROADCAST_GOTO_THEME_TAB.equals(intent.getAction())) {
                if (moments_viewpager != null && moments_viewpager.getAdapter() != null && moments_viewpager.getAdapter().getCount() > 1) {
                    moments_viewpager.setCurrentItem(3);
                }
            }

            if (TheLConstants.BROADCAST_GOTO_THEME_TAB.equals(intent.getAction())) {
                if (moments_viewpager != null && moments_viewpager.getAdapter() != null && moments_viewpager.getAdapter().getCount() > 1) {
                    moments_viewpager.setCurrentItem(3);
                }
            }

            if (TheLConstants.BROADCAST_GOTO_THEME_TAB.equals(intent.getAction())) {
                if (moments_viewpager != null && moments_viewpager.getAdapter() != null && moments_viewpager.getAdapter().getCount() > 1) {
                    moments_viewpager.setCurrentItem(3);
                }
            }

            if (TheLConstants.BROADCAST_GOTO_THEME_TAB.equals(intent.getAction())) {
                if (moments_viewpager != null && moments_viewpager.getAdapter() != null && moments_viewpager.getAdapter().getCount() > 1) {
                    moments_viewpager.setCurrentItem(3);
                }
            }

            if (TheLConstants.BROADCAST_GOTO_THEME_TAB.equals(intent.getAction())) {
                if (moments_viewpager != null && moments_viewpager.getAdapter() != null && moments_viewpager.getAdapter().getCount() > 1) {
                    moments_viewpager.setCurrentItem(3);
                }
            }
        }
    }


    /***************************************************内部类***************************************/
    class HomeAdapter extends FragmentStatePagerAdapter {

        private final List<Fragment> list;
        private final List<String> titliList;

        public HomeAdapter(FragmentManager childFragmentManager, List<Fragment> list, List<String> titleList) {
            super(childFragmentManager);
            this.list = list;
            this.titliList = titleList;
        }

        @Override
        public Fragment getItem(int position) {
            return list.get(position);
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (titliList != null) {
                return titliList.get(position % titliList.size());
            }
            return "";
        }
    }
}
