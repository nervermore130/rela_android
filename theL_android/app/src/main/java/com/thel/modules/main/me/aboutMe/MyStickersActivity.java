package com.thel.modules.main.me.aboutMe;

import android.content.Intent;
import android.database.MatrixCursor;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.bean.StickerPackBean;
import com.thel.constants.TheLConstants;
import com.thel.imp.sticker.StickerContract;
import com.thel.imp.sticker.StickerUtils;
import com.thel.modules.main.home.moments.comment.MomentCommentActivity;
import com.thel.modules.main.home.moments.comment.MomentCommentReplyActivity;
import com.thel.ui.widget.DragSortListView.DragSortCursorAdapter;
import com.thel.ui.widget.DragSortListView.DragSortListView;
import com.thel.ui.widget.DragSortListView.SimpleDragSortCursorAdapter;
import com.thel.utils.ImageUtils;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 我的表情管理页面
 * Created by lingwei on 2017/11/2.
 */

public class MyStickersActivity extends BaseActivity {
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.lin_back)
    LinearLayout linBack;
    @BindView(R.id.txt_title)
    TextView txtTitle;
    @BindView(R.id.img_more)
    ImageView imgMore;
    @BindView(R.id.lin_more)
    LinearLayout linMore;
    @BindView(R.id.title_layout)
    RelativeLayout titleLayout;
    @BindView(R.id.listView)
    com.thel.ui.widget.DragSortListView.DragSortListView listView;
    @BindView(R.id.wrapLayout)
    LinearLayout wrapLayout;
    private DragSortCursorAdapter adapter;
    private RelativeLayout header;
    // 初始化的序列，最后比对一下是否数据发生了变化
    private ArrayList<Integer> initIndexs = new ArrayList<Integer>();
    private ArrayList<Integer> sortIndexs = new ArrayList<Integer>();
    private ArrayList<Integer> removedPackIds = new ArrayList<>();
    private ArrayList<StickerPackBean> listPlus = new ArrayList<StickerPackBean>();
    private MatrixCursor cursor;
    /**
     * 数据序列
     */
    private Map<Integer, StickerPackBean> sortMap = new HashMap<Integer, StickerPackBean>();
    private List<StickerPackBean> stickBeanList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_stickers_activity);
        ButterKnife.bind(this);
        initData();
        setListener();
    }

    private void setListener() {
        header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MyStickersActivity.this, MyPurchasedStickerPacksActivity.class));
            }
        });


        listView.setDropListener(new DragSortListView.DropListener() {
            @Override
            public void drop(int from, int to) {
                adapter.drop(from, to);
                resort(adapter.getCursorPositions());
            }
        });
        listView.setRemoveListener(new DragSortListView.RemoveListener() {
            @Override
            public void remove(int which) {
                removedPackIds.add((int) listPlus.get(which).id);
                listPlus.remove(which);
                adapter.remove(which);
                resort(adapter.getCursorPositions());
            }
        });
    }

    private void resort(ArrayList<Integer> sorts) {
        sortIndexs.clear();
        sortIndexs.addAll(sorts);
    }

    @Override
    protected void onResume() {
        super.onResume();
        listPlus.clear();
        //初始化數據
        List<String> stickerIds = StickerUtils.getMyStickerIdList();
        StickerUtils.getMySpStickFileList(new StickerContract.GetStickerListCallback<StickerPackBean>() {
            @Override
            public void getStickerList(List<StickerPackBean> stList) {
                stickBeanList = stList;
                listPlus.addAll(stickBeanList);
                if (listPlus.size() > 0) {
                    cursor = new MatrixCursor(new String[]{"_id", "title", "summary", "thumbnail"});
                    int i = 0;
                    for (StickerPackBean stickerPackBean : listPlus) {
                        sortIndexs.add(i);
                        sortMap.put(i, stickerPackBean);
                        cursor.newRow().add(i).add(stickerPackBean.title).add(stickerPackBean.summary).add(ImageUtils.buildNetPictureUrl(stickerPackBean.thumbnail, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE));
                        i++;
                    }
                    initIndexs.addAll(sortIndexs);
                    adapter.changeCursor(cursor);
                }
            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(this.getClass().getName());
        MobclickAgent.onPause(this);
        // 比对用户是不是修改了数据
        boolean hasChanged = false;
        if (initIndexs.size() == sortIndexs.size()) {
            for (int i = 0; i < initIndexs.size(); i++) {
                if (initIndexs.get(i) != sortIndexs.get(i)) {
                    hasChanged = true;
                    break;
                }
            }
        } else {//如果移除了表情，则要在setResult的时候添加参数，让表情商店刷新
            hasChanged = true;
        }
        if (hasChanged) {
            //// TODO: 2017/11/7  聊天刷新
            MomentCommentActivity.needRefreshStickers = true;
            MomentCommentReplyActivity.needRefreshStickers = true;
            ArrayList<StickerPackBean> arr = new ArrayList<>();
            for (Integer index : sortIndexs) {
                arr.add(sortMap.get(index));
            }
            for (Integer id : removedPackIds) {//sp中删除
                StickerUtils.deleteOneStickerId(id + "");
            }

        }

    }

    private void initData() {
        header = (RelativeLayout) RelativeLayout.inflate(this, R.layout.go_to_inner_page_layout, null);
        header.setPadding((int) getResources().getDimension(R.dimen.margin_horizontal), header.getPaddingTop(), (int) getResources().getDimension(R.dimen.margin_horizontal), header.getPaddingBottom());
        ((TextView) header.findViewById(R.id.txt_inner_page)).setText(getString(R.string.my_sticker_act_purchased));
        ((ImageView) header.findViewById(R.id.img_logo_go_to_inner_page)).setImageResource(R.mipmap.icn_me_sticker_my);

        int[] ids = {R.id.txt_name, R.id.txt_desc, R.id.img};
        String[] cols = {"title", "summary", "thumbnail"};
        adapter = new SimpleDragSortCursorAdapter(this, R.layout.my_stickers_list_item, null, cols, ids, 0);
        listView.addHeaderView(header);
        listView.setAdapter(adapter);
        txtTitle.setText(getString(R.string.my_sticker_act_title));
    }

    @OnClick(R.id.lin_back)
    public void onViewClicked() {
        finish();
    }

    @Override
    public void finish() {
        if (initIndexs.size() != sortIndexs.size()) {// 如果移除了表情，则要在setResult的时候加参数，让表情商店页面刷新
            Intent intent = new Intent();
            intent.putIntegerArrayListExtra("ids", removedPackIds);
            setResult(RESULT_OK, intent);
        }
        super.finish();
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

}
