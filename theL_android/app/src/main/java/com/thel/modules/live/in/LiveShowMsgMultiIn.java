package com.thel.modules.live.in;

import io.agora.rtc.IRtcEngineEventHandler;

/**
 * Created by chad
 * Time 18/5/9
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */
public interface LiveShowMsgMultiIn {
    void onSeat(int seatNum);//上麦

    void offSeat(int seatNum);//下麦

    void mute(int seatNum, String micStatus);//禁麦/开麦/闭麦

    void guestGift(int giftId, int combo, int seatNum);//用户给嘉宾送礼物

    void startEncounter();//开始相遇倒计时

    void endEncounter();//结束相遇

    void encounterSb(int seatNum);//点击配对某人

    void uploadAudioVolumn(IRtcEngineEventHandler.AudioVolumeInfo[] speakers);//说话声音音量提示

    void muteAnchor(boolean isMute);

    /**
     * 请求上麦
     */
    void requestSortMic();

    /**
     * 取消上麦
     */
    void cancelSortMic();

    /**
     * 获取排麦列表
     */
    void requestMicList();

    /**
     * 获取排麦列表
     */
    void getMicSortList();

    /**
     * 观众发起连麦请求
     */
    void requestLinkMicByAudience();

    /**
     * 观众取消连麦请求
     */
    void cancelLinkMIcByAudience();

    /**
     * 挂断连麦
     *
     * @param userId
     */
    void linkMicHangup(String code, String userId);

    /**
     * 获取连麦用户列表
     */
    void requestLinkMicAudienceList();

    /**
     * 嘉宾离开直播间
     */
    void guestLeaveRoom();

    /**
     * AR礼物回执
     */
    void arGiftReceipt(String payload);

    void getTopFansTodayList();

}
