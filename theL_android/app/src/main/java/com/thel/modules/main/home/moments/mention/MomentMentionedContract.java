package com.thel.modules.main.home.moments.mention;

import com.thel.base.BasePresenter;
import com.thel.base.BaseView;
import com.thel.bean.topic.TopicFollowerListBean;

/**
 * Created by liuyun on 2017/10/24.
 */

public class MomentMentionedContract {

    interface View extends BaseView<MomentMentionedContract.Presenter> {

        boolean isActive();

        void showList(TopicFollowerListBean data);

    }

    interface Presenter extends BasePresenter {

        void loadMentions(String momentId);

    }

}
