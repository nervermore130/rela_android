package com.thel.modules.main.me.timemachine;

import com.thel.base.BaseDataBean;
import com.thel.bean.moments.MomentListDataBean;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestConstants;
import com.thel.network.service.DefaultRequestService;
import com.thel.utils.L;
import com.thel.utils.MD5Utils;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class TimeMachinePresenter implements TimeMachineContract.Presenter {

    private TimeMachineContract.View mView;

    public TimeMachinePresenter(TimeMachineContract.View mView) {
        this.mView = mView;

        mView.setPresenter(this);
    }


    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {

    }

    @Override
    public void getMoments(int cursor, int limit) {
        Flowable<MomentListDataBean> flowable = DefaultRequestService.createMomentRequestService().getDeleteMoments(cursor, limit);

        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<MomentListDataBean>() {

            @Override
            public void onNext(MomentListDataBean data) {
                super.onNext(data);

                if (data != null && data.data != null) {

                    mView.showMomentList(data.data);

                }

            }
        });

    }

    @Override
    public void recoverMoment(String momentId) {
        Map<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_ID, momentId);
        DefaultRequestService.createMomentRequestService().indifferent(MD5Utils.generateSignatureForMap(data))
                .onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new InterceptorSubscribe<BaseDataBean>() {
                    @Override
                    public void onNext(BaseDataBean data) {
                        super.onNext(data);
                    }
                });
    }
}
