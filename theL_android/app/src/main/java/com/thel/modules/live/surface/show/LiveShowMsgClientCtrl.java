package com.thel.modules.live.surface.show;

import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.GsonBuilder;
import com.thel.app.TheLApp;
import com.thel.bean.LiveInfoLogBean;
import com.thel.bean.RejectMicBean;
import com.thel.bean.TopTodayListBean;
import com.thel.bean.live.LinkMicRequestBean;
import com.thel.bean.live.LiveMultiSeatBean;
import com.thel.bean.live.MultiSpeakersBean;
import com.thel.callback.IConnectMic;
import com.thel.constants.TheLConstants;
import com.thel.modules.live.bean.AgoraBean;
import com.thel.modules.live.bean.LiveChatBean;
import com.thel.modules.live.bean.LivePkFriendBean;
import com.thel.modules.live.bean.LivePkGemNoticeBean;
import com.thel.modules.live.bean.LivePkHangupBean;
import com.thel.modules.live.bean.LivePkInitBean;
import com.thel.modules.live.bean.LivePkRequestNoticeBean;
import com.thel.modules.live.bean.LivePkResponseNoticeBean;
import com.thel.modules.live.bean.LivePkStartNoticeBean;
import com.thel.modules.live.bean.LivePkSummaryNoticeBean;
import com.thel.modules.live.bean.LiveRoomBean;
import com.thel.modules.live.bean.LiveRoomMsgBean;
import com.thel.modules.live.bean.LiveRoomMsgConnectMicBean;
import com.thel.modules.live.bean.RequestSendDanmuBean;
import com.thel.modules.live.bean.TopTodayBean;
import com.thel.modules.live.in.LiveShowCaptureMsgIn;
import com.thel.modules.live.utils.LinkMicOrPKRefuseUtils;
import com.thel.modules.live.utils.LiveGIOPush;
import com.thel.modules.live.utils.LiveUtils;
import com.thel.modules.live.view.expensive.TopGiftBean;
import com.thel.receiver.NetworkReceiver;
import com.thel.chat.tlmsgclient.IMsgClient;
import com.thel.chat.tlmsgclient.MsgClient;
import com.thel.chat.tlmsgclient.MsgListener;
import com.thel.chat.tlmsgclient.MsgPacket;
import com.thel.chat.tlmsgclient.RemoteRequest;
import com.thel.chat.tlmsgclient.ResponseCallback;
import com.thel.utils.DeviceUtils;
import com.thel.utils.ExecutorServiceUtils;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;
import com.thel.utils.PhoneUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.UmentPushUtils;
import com.thel.utils.UserUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import io.agora.rtc.IRtcEngineEventHandler;
import io.reactivex.Flowable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

import static com.thel.modules.live.LiveCodeConstants.*;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_ANCHOR_PK_CANCEL_NOTICE;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_ANCHOR_PK_LOWER_VERSION;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_ANCHOR_PK_REQUEST_PK_OK;

/**
 * Created by waiarl on 2017/11/5.
 */

public class LiveShowMsgClientCtrl implements LiveShowCaptureMsgIn, IConnectMic {
    private final LiveRoomBean liveRoomBean;
    private final static String TAG = "LiveShowMsgClientCtrl";
    private final ExecutorService sendPkMsgExecutorService;
    public IMsgClient client;
    private boolean isConnecting = false;
    private LiveShowObserver observer;
    private boolean connected = false;
    private boolean isReconnect = false;
    private boolean isReconnecting = false;
    private boolean isAutoPinging = false;
    private boolean isErrorPush = false;

    private String host;
    private int port;
    private int ipIndex = 0;
    private List<LiveChatBean.HostBean> hosts;

    private ExecutorService autoPingEcutorService = Executors.newSingleThreadExecutor();

    private long pingTimeout = 5000;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    private final SendMsgRunnable sendMsgRunnable;
    private final SendDanmuRunnable sendDanmuRunnable;

    public LiveShowMsgClientCtrl(LiveRoomBean liveRoomBean) {
        this.liveRoomBean = liveRoomBean;
        sendPkMsgExecutorService = Executors.newSingleThreadExecutor();
        sendMsgRunnable = new SendMsgRunnable();
        sendDanmuRunnable = new SendDanmuRunnable();
        NetworkReceiver.getInstance().register(LiveShowMsgClientCtrl.class.getName(), mNetworkListener);
        hosts = liveRoomBean.livechat.hosts;
        ipIndex = 0;
        changeIP();
    }


    @Override
    public void bindObserver(LiveShowObserver observer) {
        this.observer = observer;
        observer.bindMsgClient(this);
        observer.bindConnectMic(this);
    }


    @Override
    public void tryCreateClient() {
        if (!isConnecting) {
            createClient(host, port);
        }
    }

    private void createClient(String host, int port) {
        if (client == null) {
            try {
                UmentPushUtils.onMsgEvent(TheLApp.context, "live_host_msg_start_connect_android"); //  Android直播消息开始连接

                LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().log = "status:StartConnect,host:" + host + ",port:" + port;

                LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics());

                client = new MsgClient(host, port);
                client.setMsgListener(new MsgListener() {
                    @Override
                    public void onConnect(IMsgClient client) {
                        try {

                            LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().log = "status:ConnectSuccess";

                            LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics());

                            UmentPushUtils.onMsgEvent(TheLApp.context, "live_host_msg_connect_success_android"); //  Android直播消息连接成功

                            String body = getInitJsonString();

                            UmentPushUtils.onMsgEvent(TheLApp.context, "live_host_msg_start_auth_android"); //  Android直播消息开始认证

                            L.d(TAG, " init body : " + body);

                            LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().log = "status:StartAuth";

                            LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics());

                            client.request("init", body).enqueue(new InterceptorResponseCallback() {
                                @Override
                                public void onError(Throwable error) {

                                    L.d(TAG, " createClient error : " + error.getMessage());

                                    LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().log = "status:AuthTimeOut";

                                    LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics());

                                    connected = false;
                                    isConnecting = false;
                                    isReconnecting = false;
                                    UmentPushUtils.onMsgEvent(TheLApp.context, "live_msg_fail", "auth_timeout"); //  Android  认证超时
                                    LiveInfoLogBean.getInstance().getLiveChatAnalytics().errorReason = "auth_timeout";
                                    pushErrorLog();
                                    error.printStackTrace();
                                    postConnectionFailed();
                                    reconnect();
                                }

                                @Override
                                public void onResponse(MsgPacket packet) {
                                    super.onResponse(packet);

                                    L.d(TAG, " createClient onResponse getCode : " + packet.getCode());

                                    isConnecting = false;
                                    isReconnecting = false;
                                    if ("OK".equals(packet.getCode())) {

                                        LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().log = "status:AuthSuccess";

                                        LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics());

                                        UmentPushUtils.onMsgEvent(TheLApp.context, "live_host_msg_auth_success_android"); //  Android  直播消息认证成功

                                        connected = true;
                                        Log.i(TAG, "消息服务连接成功");
                                        postConnectSucceed();
                                        observer.sendEmptyMessageDelayed(UI_EVENT_AUTO_PING, 30 * 1000);
                                        isReconnect = true;
                                        initPkBusyMsg(packet);
                                        initPing();
                                    } else {

                                        LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().log = "status:AuthFail_" + packet.getCode();

                                        LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics());

                                        if ("no_such_channel".equals(packet.getCode())) {
                                            observer.sendEmptyMessage(UI_EVENT_STOP_SHOW);
                                        }
                                        LiveInfoLogBean.getInstance().getLiveChatAnalytics().errorReason = "live_msg_fail_" + packet.getCode();
                                        UmentPushUtils.onMsgEvent(TheLApp.context, "live_msg_fail", packet.getCode()); //  Android  认证失败
                                        pushErrorLog();

                                    }
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();

                            LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().log = "status:ConnectFail";

                            LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics());

                        }
                    }

                    @Override
                    public void onRemoteRequest(RemoteRequest request) {

                        L.d("LiveShowMsgClientCtrl", " request.getCode() : " + request.getCode() + "\n request.getPayload() : " + request.getPayload());

                        String code = request.getCode();

                        String payload = request.getPayload();

                        if (!TextUtils.isEmpty(request.getCode())) {
                            try {

                                LiveRoomMsgBean liveRoomMsgBean;
                                Message msg;

                                switch (code) {

                                    case LiveRoomMsgBean.TYPE_MSG:
                                        liveRoomMsgBean = GsonUtils.getObject(request.getPayload(), LiveRoomMsgBean.class);
                                        if (LiveRoomMsgBean.TYPE_MSG.equals(liveRoomMsgBean.type) || LiveRoomMsgBean.TYPE_NOTICE.equals(liveRoomMsgBean.type)) {
                                            observer.add(liveRoomMsgBean);
                                            observer.sendEmptyMessage(UI_EVENT_REFRESH_MSGS);
                                        }
                                        break;
                                    case LiveRoomMsgBean.TYPE_SYS_MSG:
                                        liveRoomMsgBean = GsonUtils.getObject(request.getPayload(), LiveRoomMsgBean.class);
                                        if (LiveRoomMsgBean.TYPE_JOIN.equals(liveRoomMsgBean.type) || LiveRoomMsgBean.TYPE_BANED.equals(liveRoomMsgBean.type) || LiveRoomMsgBean.TYPE_LEAVE.equals(liveRoomMsgBean.type)) {
                                            observer.add(liveRoomMsgBean);
                                            observer.sendEmptyMessage(UI_EVENT_REFRESH_MSGS);
                                        }
                                        break;
                                    case LiveRoomMsgBean.TYPE_UPDATE:
                                        liveRoomBean.liveUsersCount = new JSONObject(request.getPayload()).getInt("userCount");
                                        liveRoomBean.rank = new JSONObject(request.getPayload()).optInt("rank", 0);
                                        liveRoomBean.gem = new JSONObject((request.getPayload())).optString("gem", "0");
                                        liveRoomBean.nowGem = new JSONObject((request.getPayload())).optString("nowGem", liveRoomBean.nowGem + "");
                                        observer.sendEmptyMessage(UI_EVENT_UPDATE_AUDIENCE_COUNT);
                                        break;
                                    case LiveRoomMsgBean.TYPE_CLOSE:
                                        observer.sendEmptyMessage(UI_EVENT_STOP_SHOW);
                                        break;
                                    case LiveRoomMsgBean.TYPE_GIFT_MSG:
                                    case LiveRoomMsgBean.CODE_SPECIAL_GIFT:
                                        msg = Message.obtain();
                                        msg.what = UI_EVENT_GIFT_RECEIVE_MSG;
                                        Bundle bundle = msg.getData();
                                        bundle.putString("payload", request.getPayload());
                                        bundle.putString("code", request.getCode());
                                        msg.setData(bundle);
                                        observer.sendMessage(msg);
                                        break;
                                    case LiveRoomMsgBean.TYPE_DANMU_MSG:
                                        msg = Message.obtain();
                                        msg.what = UI_EVENT_DANMU_RECEIVE_MSG;
                                        msg.obj = request.getPayload();
                                        observer.sendMessage(msg);
                                        break;
                                    case LiveRoomMsgBean.TYPE_VISIT:
                                        liveRoomMsgBean = GsonUtils.getObject(request.getPayload(), LiveRoomMsgBean.class);
                                        if (LiveRoomMsgBean.TYPE_JOIN.equals(liveRoomMsgBean.type)) {//用户加入房间
                                            msg = Message.obtain();
                                            msg.what = UI_EVENT_JOIN_USER;
                                            msg.obj = liveRoomMsgBean;
                                            observer.sendMessage(msg);
                                        }
                                        break;
                                    case LiveRoomMsgBean.TYPE_VIP_VISIT:
                                        liveRoomMsgBean = GsonUtils.getObject(request.getPayload(), LiveRoomMsgBean.class);
                                        if (LiveRoomMsgBean.TYPE_JOIN.equals(liveRoomMsgBean.type)) {//用户加入房间
                                            msg = Message.obtain();
                                            msg.what = UI_EVENT_JOIN_VIP_USER;
                                            msg.obj = liveRoomMsgBean;
                                            observer.sendMessage(msg);
                                        }
                                        break;
                                    case LiveRoomMsgBean.TYPE_LIVE_PK:
                                        receiverPkNoticeMsg(payload);
                                        break;
                                    case LiveRoomMsgBean.TYPE_CONNECT_MIC:
                                        final LiveRoomMsgConnectMicBean liveRoomMsgConnectMicBean = GsonUtils.getObject(request.getPayload(), LiveRoomMsgConnectMicBean.class);
                                        msg = Message.obtain();
                                        msg.what = UI_EVENT_ANCHOR_LINK_MIC_REQUEST;
                                        msg.obj = liveRoomMsgConnectMicBean;
                                        observer.sendMessage(msg);
                                        break;
                                    case LiveRoomMsgBean.NETWORK_WARNING_NOTIFY://观众没有
                                        liveRoomMsgBean = GsonUtils.getObject(request.getPayload(), LiveRoomMsgBean.class);
                                        if (liveRoomMsgBean != null && !TextUtils.isEmpty(liveRoomMsgBean.content)) {
                                            msg = Message.obtain();
                                            msg.what = UI_EVENT_ANCHOR_LIVE_NETWORK_WARNING_NOTIFY;
                                            msg.obj = liveRoomMsgBean.content;
                                            observer.sendMessage(msg);
                                        }
                                        break;
                                    case LiveRoomMsgBean.TYPE_FOLLOW:
                                        liveRoomMsgBean = LiveUtils.getFollowMsgBean(request.getPayload());
                                        if (liveRoomMsgBean != null && !TextUtils.isEmpty(liveRoomMsgBean.content)) {
                                            observer.add(liveRoomMsgBean);
                                            observer.sendEmptyMessage(UI_EVENT_REFRESH_MSGS);
                                        }
                                        break;
                                    case LiveRoomMsgBean.TYPE_RECOMM:
                                        liveRoomMsgBean = LiveUtils.getRecommendMsgBean(request.getPayload());
                                        if (liveRoomMsgBean != null && !TextUtils.isEmpty(liveRoomMsgBean.content)) {
                                            observer.add(liveRoomMsgBean);
                                            observer.sendEmptyMessage(UI_EVENT_REFRESH_MSGS);
                                        }
                                        break;
                                    case LiveRoomMsgBean.TYPE_SHARETO:
                                        liveRoomMsgBean = LiveUtils.getSharetToMsgBean(request.getPayload());
                                        if (liveRoomMsgBean != null && !TextUtils.isEmpty(liveRoomMsgBean.content)) {
                                            observer.add(liveRoomMsgBean);
                                            observer.sendEmptyMessage(UI_EVENT_REFRESH_MSGS);
                                        }
                                        break;
                                    case LiveRoomMsgBean.TYPE_GIFTCOMBO:
                                        if (liveRoomBean != null && liveRoomBean.softEnjoyBean != null) {

                                            L.d(TAG, " LiveRoomMsgBean request.getPayload() : " + request.getPayload());

                                            liveRoomMsgBean = LiveUtils.getSendGiftMsgBean(request.getPayload(), liveRoomBean.softEnjoyBean);
                                            if (liveRoomMsgBean != null && !TextUtils.isEmpty(liveRoomMsgBean.content)) {
                                                observer.add(liveRoomMsgBean);
                                                observer.sendEmptyMessage(UI_EVENT_REFRESH_MSGS);
                                            }
                                        }
                                        break;
                                    case LiveRoomMsgBean.TYPE_TOPGIFT:
                                        msg = Message.obtain();
                                        msg.what = UI_EVENT_TOP_GIFT;
                                        msg.obj = GsonUtils.getObject(request.getPayload(), TopGiftBean.class);
                                        observer.sendMessage(msg);
                                        break;
                                    case LiveRoomMsgBean.TYPE_AUDIO_BROADCAST_CODE:
                                        LiveMultiSeatBean seat = GsonUtils.getObject(request.getPayload(), LiveMultiSeatBean.class);
                                        msg = Message.obtain();
                                        switch (seat.method) {
                                            case LiveRoomMsgBean.TYPE_METHOD_ONSEAT:
                                                msg.obj = seat;
                                                msg.what = UI_EVENT_REFRESH_SEAT;
                                                observer.sendMessage(msg);
                                                LiveGIOPush.getInstance().setAudienceOnSeatCount();
                                                break;
                                            case LiveRoomMsgBean.TYPE_METHOD_OFFSEAT:
                                                msg.obj = seat;
                                                msg.what = UI_EVENT_REFRESH_SEAT;
                                                observer.sendMessage(msg);
                                                break;
                                            case LiveRoomMsgBean.TYPE_METHOD_MUTE:
                                                msg.obj = seat;
                                                msg.what = UI_EVENT_REFRESH_SEAT;
                                                observer.sendMessage(msg);
                                                break;
                                            case LiveRoomMsgBean.TYPE_METHOD_GUEST_GIFT:
                                                msg.obj = seat;
                                                msg.what = UI_EVENT_GUEST_GIFT;
                                                observer.sendMessage(msg);
                                                break;
                                            case LiveRoomMsgBean.TYPE_METHOD_START_ENCOUNTER://观众没有
                                                msg.obj = seat;
                                                msg.what = UI_EVENT_START_ENCOUNTER;
                                                observer.sendMessage(msg);
                                                break;
                                            case LiveRoomMsgBean.TYPE_METHOD_END_ENCOUNTER://观众没有
                                                msg.obj = seat;
                                                msg.what = UI_EVENT_END_ENCOUNTER;
                                                observer.sendMessage(msg);
                                                break;
                                            case LiveRoomMsgBean.TYPE_METHOD_ENCOUNTERSB:
                                                msg.obj = seat;
                                                msg.what = UI_EVENT_ENCOUNTER_SB;
                                                observer.sendMessage(msg);
                                                break;
                                            case LiveRoomMsgBean.TYPE_METHOD_UPLOAD_VOLUMN://观众没有
                                                msg.obj = GsonUtils.getObject(request.getPayload(), MultiSpeakersBean.class);
                                                msg.what = UI_EVENT_ENCOUNTER_SPEAKERS;
                                                observer.sendMessage(msg);
                                                break;
                                            case LiveRoomMsgBean.TYPE_METHOD_MIC_ON:
                                                msg.what = UI_EVENT_MIC_ON;
                                                observer.sendMessage(msg);
                                                break;
                                            case LiveRoomMsgBean.TYPE_METHOD_MIC_OFF:
                                                msg.what = UI_EVENT_MIC_OFF;
                                                observer.sendMessage(msg);
                                                break;
                                            case LiveRoomMsgBean.TYPE_METHOD_MIC_ADD:
                                                msg.obj = GsonUtils.getObject(request.getPayload(), LiveMultiSeatBean.CoupleDetail.class);
                                                msg.what = UI_EVENT_MIC_ADD;
                                                observer.sendMessage(msg);
                                                break;
                                            case LiveRoomMsgBean.TYPE_METHOD_MIC_DEL:
                                                msg.obj = seat;
                                                msg.what = UI_EVENT_MIC_DEL;
                                                observer.sendMessage(msg);
                                                break;
                                            case LiveRoomMsgBean.TYPE_METHOD_MIC_REQ:
                                                msg.what = UI_EVENT_MIC_REQ;
                                                observer.sendMessage(msg);
                                                break;
                                            case LiveRoomMsgBean.TYPE_METHOD_MIC_CANCEL:
                                                msg.what = UI_EVENT_MIC_CANCEL;
                                                observer.sendMessage(msg);
                                                break;
                                            case LiveRoomMsgBean.TYPE_METHOD_MIC_AGREE:
                                                msg.obj = seat;
                                                msg.what = UI_EVENT_MIC_AGREE;
                                                observer.sendMessage(msg);
                                                break;
                                        }
                                        break;
                                    case LiveRoomMsgBean.TYPE_TOP_TODAY:
                                        Message message = observer.obtainMessage();
                                        message.what = UI_EVENT_TOP_TODAY;
                                        message.obj = GsonUtils.getObject(request.getPayload(), TopTodayBean.class);
                                        observer.sendMessage(message);
                                        break;
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onClosed(IMsgClient client) {
                        Log.e(TAG, "连接被关闭了");
                        connected = false;
                        isConnecting = false;
                        isReconnecting = false;
                        postConnectionInterrupted();

                        LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().log = "status:Disconnected errorReason: 服务端断开连接";

                        LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics());

                    }

                    @Override
                    public void onError(Exception ex) {
                        connected = false;
                        isConnecting = false;
                        isReconnecting = false;
                        Log.e(TAG, "连接出错了");
                        if (ex != null) {
                            ex.printStackTrace();
                        }
                        postConnectionInterrupted();
                        reconnect();

                        LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().log = "status:Disconnected errorReason: 连接报错" + (ex != null ? ex.getMessage() : "");

                        LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics());

                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
        }

        connect();
    }


    /**
     * init 的param json 数据
     *
     * @return
     */
    private String getInitJsonString() {
        double lt = 0.0;
        double lg = 0.0;
        try {
            lt = Double.parseDouble(ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0"));
            lg = Double.parseDouble(ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        final double lat = lt;
        final double lng = lg;
        final JSONObject obj = new JSONObject() {
            {
                try {

                    put("version", TheLConstants.MSG_CLIENT_VERSION);
                    put("os", "Android");
                    put("appver", DeviceUtils.getVersionCode(TheLApp.getContext()) + "");
                    put("deviceId", ShareFileUtils.getString(ShareFileUtils.DEVICE_ID, UUID.randomUUID().toString()));
                    put("lat", lat);
                    put("lng", lng);
                    put("lang", DeviceUtils.getLanguage());
                    put("channel", liveRoomBean.livechat.channel);
                    put("token", liveRoomBean.livechat.token);
                    put("userId", Integer.valueOf(UserUtils.getMyUserId()));
                    put("nickName", ShareFileUtils.getString(ShareFileUtils.USER_NAME, ""));
                    put("avatar", ShareFileUtils.getString(ShareFileUtils.AVATAR, ""));
                    put("isOwner", true);
                    put("autoReconnect", isReconnect);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        return obj.toString();
    }

    public void connect() {

        L.d("LiveRoomMsgBean", " connect : ");

        if (!(PhoneUtils.getNetWorkType() == PhoneUtils.TYPE_NO)) {
            if (!isConnecting) {
                isConnecting = true;

                L.d("LiveRoomMsgBean", " isConnecting : " + isConnecting);

                postConnecting();
                try {
                    client.connect();
                } catch (Exception e) {
                    e.printStackTrace();
                    postConnectionFailed();
                    reconnect();
                }
            }
        } else {
            isConnecting = false;
            postConnectionFailed();
            reconnect();
        }
    }


    private void reconnect() {

        L.d("LiveRoomMsgBean", " reconnect : ");

        connected = false;
        if (!isReconnecting) {
            isReconnecting = true;
            disconnect();
            pushErrorMsg();
            Log.i(TAG, "2s后开始重新连接");
            observer.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Log.i(TAG, "开始重新连接");
//                    connect();
                    tryCreateClient();
                }
            }, 2000);
        }
    }

    private void disconnect() {

        if (!isErrorPush) {
            //聊天连接失败发送一次友盟上报
            UmentPushUtils.onMsgEvent(TheLApp.context, "live_host_msg_fail_one_android");
            isErrorPush = true;
        }

        connected = false;
        try {
            if (client != null) {
                client.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        client = null;

    }

    private void pushErrorMsg() {

        LiveInfoLogBean.getInstance().getLiveChatAnalytics().errorReason = "connect_fail";

        pushErrorLog();

        changeIP();
    }

    @Override
    public void sendCloseMsg() {
        Log.e(TAG, "发close消息");
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (client != null) {
                        client.request("close", "").enqueue(new InterceptorResponseCallback() {
                            @Override
                            public void onError(Throwable error) {
                                error.printStackTrace();
                            }

                            @Override
                            public void onResponse(MsgPacket packet) {
                                super.onResponse(packet);
                                Log.e(TAG, "主播关闭了直播");
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    // mStreamer.stopStream();
                    observer.sendEmptyMessage(UI_EVENT_ANCHOR_FINISH);
                }
            }
        }).start();
    }

    @Override
    public void banHer(final String userId) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (client != null) {
                        client.request("ban", new JSONObject().put("userId", userId).toString()).enqueue(new InterceptorResponseCallback() {
                            @Override
                            public void onError(Throwable error) {
                                error.printStackTrace();
                            }

                            @Override
                            public void onResponse(MsgPacket packet) {
                                super.onResponse(packet);
                                Log.e(TAG, "屏蔽成功");
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    @Override
    public void autoPingServer() {
        if (PhoneUtils.getNetWorkType() == PhoneUtils.TYPE_NO) {// 没网络不ping
            isAutoPinging = false;
            postConnectionFailed();
            reconnect();
            return;
        }
        if (!isAutoPinging) {
            if (client == null) {
                isAutoPinging = false;
                return;
            }
            isAutoPinging = true;
            autoPingEcutorService.execute(new Runnable() {

                @Override
                public void run() {

                    LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().log = "status:StartPing";

                    LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics());

                    isAutoPinging = false;
                    if (client != null) {
                        client.request("ping", "", pingTimeout).enqueue(new InterceptorResponseCallback() {
                            @Override
                            public void onError(Throwable error) {
                                // ping不通，重连
                                Log.e(TAG, "ping不通，重连");
                                isAutoPinging = false;
                                isConnecting = false;
                                isReconnecting = false;
                                postConnectionInterrupted();
                                reconnect();
                                if (error != null) {
                                    error.printStackTrace();
                                }
                                UmentPushUtils.onMsgEvent(TheLApp.context, "live_host_msg_ping_timeout_android");

                                LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().log = "status:PingTimeOut";

                                LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics());

                            }

                            @Override
                            public void onResponse(MsgPacket packet) {
                                isAutoPinging = false;

                                LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().log = "status:PingResult code:" + packet.getCode();

                                LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics());

                                if (!"pong".equals(packet.getCode())) {

                                    LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().log = "status:PingFail";

                                    LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics());

                                    // ping不通，重连
                                    Log.e(TAG, "ping不通，重连");
                                    isConnecting = false;
                                    isReconnecting = false;
                                    postConnectionInterrupted();
                                    reconnect();
                                    UmentPushUtils.onMsgEvent(TheLApp.context, "live_host_msg_ping_fail_android");

                                } else {

                                    LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().log = "status:PingSuccess";

                                    LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics());

                                    Log.i(TAG, "ping成功");
                                    observer.sendEmptyMessageDelayed(UI_EVENT_AUTO_PING, 10 * 1000);
                                }
                            }
                        });
                    }

                }
            });
        }

    }

    /**
     * 关闭直播弹窗
     */
    private Runnable closeRunnable = new Runnable() {
        @Override
        public void run() {
            try {
                if (client != null) {
                    client.request("statistics", "").enqueue(new InterceptorResponseCallback() {
                        @Override
                        public void onError(Throwable error) {
                        }

                        @Override
                        public void onResponse(MsgPacket packet) {

                            if ("OK".equals(packet.getCode())) {
                                Message msg = Message.obtain();
                                msg.what = UI_EVENT_ANCHOR_CLOSE_DIALOG;
                                msg.obj = new String(packet.getPayload());
                                observer.sendMessage(msg);
                            }
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private void postConnectSucceed() {
        LiveRoomMsgBean liveRoomMsgBean = new LiveRoomMsgBean();
        liveRoomMsgBean.type = LiveRoomMsgBean.TYPE_CONNECT_SUCCEED;
        observer.add(liveRoomMsgBean);
        observer.sendEmptyMessage(UI_EVENT_REFRESH_MSGS);
    }

    private void postConnecting() {

        LiveRoomMsgBean liveRoomMsgBean = new LiveRoomMsgBean();
        liveRoomMsgBean.type = LiveRoomMsgBean.TYPE_CONNECTING;
        observer.add(liveRoomMsgBean);
        if (null != observer)  //fabric #10595
        {
            observer.sendEmptyMessage(UI_EVENT_REFRESH_MSGS);
        }
    }

    private void postConnectionFailed() {
        //  Android连接失败
        UmentPushUtils.onMsgEvent(TheLApp.context, "live_msg_fail", "live_host_msg_connect_fail_android");
        LiveInfoLogBean.getInstance().getLiveChatAnalytics().errorReason = "connect_fail";
        pushErrorLog();
        LiveRoomMsgBean liveRoomMsgBean = new LiveRoomMsgBean();
        liveRoomMsgBean.type = LiveRoomMsgBean.TYPE_CONNECT_FAILED;
        observer.add(liveRoomMsgBean);
        if (null != observer) {
            observer.sendEmptyMessage(UI_EVENT_REFRESH_MSGS);
        }
    }

    private void postConnectionInterrupted() {
        //  Android连接失败
        UmentPushUtils.onMsgEvent(TheLApp.context, "live_msg_fail", "live_host_msg_connect_fail_android");
        LiveInfoLogBean.getInstance().getLiveChatAnalytics().errorReason = "connect_fail";
        pushErrorLog();

        LiveRoomMsgBean liveRoomMsgBean = new LiveRoomMsgBean();
        liveRoomMsgBean.type = LiveRoomMsgBean.TYPE_CONNECT_INTERRUPTED;
        observer.add(liveRoomMsgBean);
        if (null != observer) {
            observer.sendEmptyMessage(UI_EVENT_REFRESH_MSGS);
        }
    }

    @Override
    public void onDestroy() {
        if (client != null) {
            autoPingEcutorService.shutdown();
            disconnect();
            client = null;
        }
        if (compositeDisposable != null) {
            compositeDisposable.clear();
        }

        LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().log = "status:Disconnected errorReason: 手动断开连接";

        LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics());

        NetworkReceiver.getInstance().unregister(LiveShowMsgClientCtrl.class.getName());
    }

    @Override
    public void exucuteColseRunable() {
        if (autoPingEcutorService != null) {
            autoPingEcutorService.execute(closeRunnable);
        }
    }

    /************************************************一下为直播间Pk的方法**************************************************************/
    private void initPkBusyMsg(MsgPacket packet) {
        final String payload = new String(packet.getPayload());
        if (TextUtils.isEmpty(payload)) {
            return;
        }

        try {
            final JSONObject object = new JSONObject(payload);
            final String status = object.optString("status", "");
            if (status.equals("pk_busy") || status.equals("pk_summary")) {//假设PK中断断线重连回来
                final LivePkInitBean livePkInitBean = GsonUtils.getObject(payload, LivePkInitBean.class);
                final Message message = Message.obtain();
                message.what = UI_EVENT_PK_INIT_MSG;
                message.obj = livePkInitBean;
                observer.sendMessage(message);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * 收到有关Pk的消息
     * 属于广播通知收到的消息
     * <p>
     * 接收方 request->start->summary->hangup->stop
     * <p>
     * 发起方 response->start->stop
     *
     * @param payload
     */
    private void receiverPkNoticeMsg(String payload) {
        if (TextUtils.isEmpty(payload)) {
            return;
        }
        try {
            final JSONObject obj = new JSONObject(payload);

            final String method = obj.optString("method");

            L.d(TAG, " receiverPkNoticeMsg method : " + method);

            L.d(TAG, " receiverPkNoticeMsg payload : " + payload);

            switch (method) {
                case LiveRoomMsgBean.PK_NOTICE_METHOD_REQUEST:
                    receivePkRequest(payload);
                    break;
                case LiveRoomMsgBean.PK_NOTICE_METHOD_RESPONSE:
                    receivePkResponse(payload);
                    break;
                case LiveRoomMsgBean.PK_NOTICE_METHOD_START:
                    receiverPkStartMsg(payload);
                    break;
                case LiveRoomMsgBean.PK_NOTICE_METHOD_CANCEL:
                    receivePkCancelMsg(payload);
                    break;
                case LiveRoomMsgBean.PK_NOTICE_METHOD_HANGUP:
                    receiveHangupMsg(payload);
                    break;
                case LiveRoomMsgBean.PK_NOTICE_METHOD_TIMEOUT:
                    receivePkTimeoutMsg(payload);
                    break;
                case LiveRoomMsgBean.PK_NOTICE_METHOD_SUMMARY:
                    receivePkSummaryMsg(payload);
                    break;
                case LiveRoomMsgBean.PK_NOTICE_METHOD_STOP:
                    receivePkStopMsg(payload);
                    break;
                case LiveRoomMsgBean.PK_NOTICE_METHOD_PKGEM:
                    receiverPkGiftMsg(payload);
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * 发起Pk 请求
     *
     * @param toUserId pk接受放的用户id
     * @param x
     * @param y
     * @param width
     * @param height
     */
    @Override
    public void sendPkRequest(final String toUserId, final float x, final float y, final float width, final float height) {
        try {
            final JSONObject obj = new JSONObject() {
                {
                    put("method", LiveRoomMsgBean.PK_NOTICE_METHOD_REQUEST);
                    put("toUserId", toUserId);
                    put("x", x);
                    put("y", y);
                    put("height", height);
                    put("width", width);
                }
            };
            final String code = LiveRoomMsgBean.MSG_SEND_CODE_TYPE_PK;
            sendPkMsgExecutorService.execute(new SendPkRequestRunnable(code, obj.toString(), toUserId));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 收到Pk请求
     *
     * @param payload
     */
    @Override
    public void receivePkRequest(String payload) {
        if (TextUtils.isEmpty(payload)) {
            return;
        }
        final LivePkRequestNoticeBean bean = GsonUtils.getObject(payload, LivePkRequestNoticeBean.class);
        final Message msg = Message.obtain();
        msg.what = UI_EVENT_ANCHOR_PK_REQUEST_NOTICE;
        msg.obj = bean;
        observer.sendMessage(msg);
    }

    /**
     * 响应Pk 请求
     *
     * @param toUserId pk接受方的用户id
     * @param result   yes: 同意, no: 不同意
     * @param x
     * @param y
     * @param width
     * @param height
     */
    @Override
    public void sendPkResponse(final String toUserId, final String result, final float x, final float y, final float width, final float height) {
        try {
            final JSONObject obj = new JSONObject() {
                {
                    put("method", LiveRoomMsgBean.PK_NOTICE_METHOD_RESPONSE);
                    put("toUserId", toUserId);
                    put("result", result);
                    put("x", x);
                    put("y", y);
                    put("height", height);
                    put("width", width);

                }
            };
            final String code = LiveRoomMsgBean.MSG_SEND_CODE_TYPE_PK;
            sendPkMsgExecutorService.execute(new SendPkResponseRunnable(code, obj.toString()));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 发起PK方收到对方回应
     *
     * @param payload
     */
    @Override
    public void receivePkResponse(String payload) {
        if (TextUtils.isEmpty(payload)) {
            return;
        }
        final LivePkResponseNoticeBean bean = GsonUtils.getObject(payload, LivePkResponseNoticeBean.class);
        final Message msg = Message.obtain();
        msg.what = UI_EVENT_ANCHOR_PK_RESPONSE_NOTICE;
        msg.obj = bean;
        observer.sendMessage(msg);
        if (bean.result.equals(LivePkResponseNoticeBean.LIVE_PK_RESPONSE_RESULT_NO)) {//如果被拒绝，则存入sp
//            LivePkUtils.saveOnePkRefuser(bean.fromUserId);
            LinkMicOrPKRefuseUtils.addPkRefuse(bean.fromUserId);
        }
    }

    /**
     * PK开始时 服务器发送PK开始通知给双方直播室内的所有人
     *
     * @param payload
     */
    @Override
    public void receiverPkStartMsg(String payload) {
        if (TextUtils.isEmpty(payload)) {
            return;
        }
        final LivePkStartNoticeBean bean = GsonUtils.getObject(payload, LivePkStartNoticeBean.class);
        final Message msg = Message.obtain();
        msg.what = UI_EVENT_ANCHOR_PK_START_NOTICE;
        msg.obj = bean;
        observer.sendMessage(msg);
    }

    /**
     * 发送PK取消申请
     *
     * @param toUserId
     */
    @Override
    public void sendPkCancel(final String toUserId) {
        try {
            final JSONObject obj = new JSONObject() {
                {
                    put("method", LiveRoomMsgBean.PK_NOTICE_METHOD_CANCEL);
                    put("toUserId", toUserId);
                }
            };
            final String code = LiveRoomMsgBean.MSG_SEND_CODE_TYPE_PK;
            sendPkMsgExecutorService.execute(new SendPkCancelRunnable(code, obj.toString()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 收到取消PK取消的请求
     *
     * @param payload
     */
    @Override
    public void receivePkCancelMsg(String payload) {
        if (TextUtils.isEmpty(payload)) {
            return;
        }
        final Message msg = Message.obtain();
        msg.what = UI_EVENT_ANCHOR_PK_CANCEL_NOTICE;
        msg.obj = payload;
        observer.sendMessage(msg);
    }

    /**
     * 发送PK挂断申请
     *
     * @param toUserId
     */
    @Override
    public void sendPkHangup(final String toUserId) {
        try {
            final JSONObject obj = new JSONObject() {
                {
                    put("method", LiveRoomMsgBean.PK_NOTICE_METHOD_HANGUP);
                    put("toUserId", toUserId);
                }
            };
            final String code = LiveRoomMsgBean.MSG_SEND_CODE_TYPE_PK;
            sendPkMsgExecutorService.execute(new SendPkHangupRunnable(code, obj.toString()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 被挂断方收到服务器的PK挂断消息
     *
     * @param payload
     */
    @Override
    public void receiveHangupMsg(String payload) {
        if (TextUtils.isEmpty(payload)) {
            return;
        }
        final LivePkHangupBean bean = GsonUtils.getObject(payload, LivePkHangupBean.class);
        final Message msg = Message.obtain();
        msg.what = UI_EVENT_ANCHOR_PK_HANGUP_NOTICE;
        msg.obj = bean;
        observer.sendMessage(msg);
    }

    /**
     * 收到Pk超时消息
     *
     * @param payload
     */
    @Override
    public void receivePkTimeoutMsg(String payload) {
        if (TextUtils.isEmpty(payload)) {
            return;
        }
        try {
            final JSONObject obj = new JSONObject(payload);
            final String toUserId = obj.optString("toUserId");
            final Message msg = Message.obtain();
            msg.what = UI_EVENT_ANCHOR_PK_REQUEST_TIME_OUT;
            msg.obj = toUserId;
            observer.sendMessage(msg);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    /**
     * 进入总结阶段 服务器发送 总结剩余时间 和 双方助攻前三名 给双方直播室内的所有人:
     *
     * @param payload
     */
    @Override
    public void receivePkSummaryMsg(String payload) {
        if (TextUtils.isEmpty(payload)) {
            return;
        }
        final LivePkSummaryNoticeBean bean = GsonUtils.getObject(payload, LivePkSummaryNoticeBean.class);
        final Message msg = Message.obtain();
        msg.what = UI_EVENT_ANCHOR_PK_SUMMARY_NOTICE;
        msg.obj = bean;
        observer.sendMessage(msg);
    }

    /**
     * PK结束时 服务器发送 PK结束 和 双方助攻前三名 通知给双方直播室内的所有人
     *
     * @param payload
     */
    @Override
    public void receivePkStopMsg(String payload) {
        if (TextUtils.isEmpty(payload)) {
            return;
        }
        final Message msg = Message.obtain();
        msg.what = UI_EVENT_ANCHOR_PK_STOP_NOTICE;
        msg.obj = payload;
        observer.sendMessage(msg);
    }

    /**
     * 主播收到礼物时 服务器发送主播软妹币增量通知给双方直播室内的所有人:
     *
     * @param payload
     */
    @Override
    public void receiverPkGiftMsg(String payload) {
        if (TextUtils.isEmpty(payload)) {
            return;
        }
        final LivePkGemNoticeBean bean = GsonUtils.getObject(payload, LivePkGemNoticeBean.class);
        final Message msg = Message.obtain();
        msg.what = UI_EVENT_ANCHOR_PK_GEM_NOTICE;
        msg.obj = bean;
        observer.sendMessage(msg);
    }

    /**
     * 发送直播PK申请消息
     */
    class SendPkRequestRunnable implements Runnable {
        private final String code;
        private final String json;
        private final String userId;

        public SendPkRequestRunnable(String code, String json, String userId) {
            this.code = code;
            this.json = json;
            this.userId = userId;
        }

        @Override
        public void run() {
            if (client != null) {
                client.request(code, json).enqueue(new InterceptorResponseCallback() {
                    @Override
                    public void onError(Throwable error) {
                        error.printStackTrace();
                        isConnecting = false;
                        isReconnecting = false;
                        postConnectionInterrupted();
                        reconnect();
                    }

                    @Override
                    public void onResponse(MsgPacket packet) {
                        if (LiveRoomMsgBean.LIVE_MSG_RESPONSE_CODE_OK.equals(packet.getCode())) {//正确返回
                            observer.sendEmptyMessage(UI_EVENT_ANCHOR_PK_REQUEST_PK_OK);
                        } else if (LiveRoomMsgBean.TYPE_LIVE_PK_RESPONSE_CODE_NOT_LIVING.equals(packet.getCode())) {//主播不在直播
                            final Message msg = Message.obtain();
                            msg.what = UI_EVENT_ANCHOR_PK_NOT_LIVING;
                            msg.obj = userId;
                            observer.sendMessage(msg);
                        } else if (LiveRoomMsgBean.TYPE_LIVE_PK_RESPONSE_CODE_IN_LINKMIC.equals(packet.getCode())) {//主播在连麦中
                            final Message msg = Message.obtain();
                            msg.what = UI_EVENT_ANCHOR_PK_IN_LINKMIC;
                            msg.obj = userId;
                            observer.sendMessage(msg);
                        } else if (LiveRoomMsgBean.TYPE_LIVE_PK_RESPONSE_CODE_IN_PK.equals(packet.getCode())) {//主播在Pk中
                            final Message msg = Message.obtain();
                            msg.what = UI_EVENT_ANCHOR_PK_IN_PK;
                            msg.obj = userId;
                            observer.sendMessage(msg);
                        } else if (LiveRoomMsgBean.TYPE_LIVE_PK_RESPONSE_CODE_LOW_CLI_VER.equals(packet.getCode())) {//过低的客户端版本
                            observer.sendEmptyMessage(UI_EVENT_ANCHOR_PK_LOWER_VERSION);
                            //todo
                        } else if (LiveRoomMsgBean.TYPE_LIVE_PK_RESPONSE_CODE_500.equals(packet.getCode())) {//服务端错误
                            observer.sendEmptyMessage(UI_EVENT_ANCHOR_PK_CODE_500);
                        } else if (LiveRoomMsgBean.TYPE_LIVE_PK_RESPONSE_CODE_REJECT.equals(packet.getCode())) {//上次发起PK被该主播拒绝，需等待10分钟才能对该主播再次发起PK
                            final String payload = new String(packet.getPayload());
                            try {
                                final JSONObject obj = new JSONObject(payload);
                                final String leftTime = obj.optString("leftTime");
                                final Message msg = Message.obtain();
                                msg.what = UI_EVENT_ANCHOR_PK_REQUEST_REJECT;
                                msg.obj = leftTime;
                                observer.sendMessage(msg);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            }
        }
    }

    /**
     * 接收方响应PK请求:
     */
    class SendPkResponseRunnable implements Runnable {
        private final String code;
        private final String json;

        public SendPkResponseRunnable(String code, String json) {
            this.code = code;
            this.json = json;
        }

        @Override
        public void run() {
            if (client != null) {
                client.request(code, json).enqueue(new InterceptorResponseCallback() {
                    @Override
                    public void onError(Throwable error) {
                        error.printStackTrace();
                        isConnecting = false;
                        isReconnecting = false;
                        postConnectionInterrupted();
                        reconnect();
                    }

                    @Override
                    public void onResponse(MsgPacket packet) {
                        if (LiveRoomMsgBean.LIVE_MSG_RESPONSE_CODE_OK.equals(packet.getCode())) {//正确返回
                            //todo
                        } else if (LiveRoomMsgBean.TYPE_LIVE_PK_RESPONSE_CODE_500.equals(packet.getCode())) {//服务端错误
                            //todo
                        }
                    }
                });
            }
        }
    }

    /**
     * PK发起方取消PK
     */
    class SendPkCancelRunnable implements Runnable {
        private final String code;
        private final String json;

        public SendPkCancelRunnable(String code, String json) {
            this.code = code;
            this.json = json;
        }

        @Override
        public void run() {
            if (client != null) {
                client.request(code, json).enqueue(new InterceptorResponseCallback() {
                    @Override
                    public void onError(Throwable error) {
                        error.printStackTrace();
                        isConnecting = false;
                        isReconnecting = false;
                        postConnectionInterrupted();
                        reconnect();
                    }

                    @Override
                    public void onResponse(MsgPacket packet) {
                        if (LiveRoomMsgBean.LIVE_MSG_RESPONSE_CODE_OK.equals(packet.getCode())) {//正确返回
                            //todo
                        } else if (LiveRoomMsgBean.TYPE_LIVE_PK_RESPONSE_CODE_500.equals(packet.getCode())) {//服务端错误
                            //todo
                        }
                    }
                });
            }
        }
    }

    /**
     * PK挂断消息
     */
    class SendPkHangupRunnable implements Runnable {
        private final String code;
        private final String json;

        public SendPkHangupRunnable(String code, String json) {
            this.code = code;
            this.json = json;
        }

        @Override
        public void run() {
            if (client != null) {
                client.request(code, json).enqueue(new InterceptorResponseCallback() {
                    @Override
                    public void onError(Throwable error) {
                        error.printStackTrace();
                        isConnecting = false;
                        isReconnecting = false;
                        postConnectionInterrupted();
                        reconnect();
                    }

                    @Override
                    public void onResponse(MsgPacket packet) {
                        if (LiveRoomMsgBean.LIVE_MSG_RESPONSE_CODE_OK.equals(packet.getCode())) {//正确返回
                            //todo
                        } else if (LiveRoomMsgBean.TYPE_LIVE_PK_RESPONSE_CODE_500.equals(packet.getCode())) {//服务端错误
                            //todo
                        }
                    }
                });
            }
        }
    }


    /************************************************以上为直播间Pk的方法**************************************************************/

    /************************************************连麦方法begin********************************************************************/

    @Override
    public void connectMic(final String method, final String toUserId, final float x, final float y, final float height, final float width, final String nickName, final String avatar, boolean dailyGuard) {
        sendPkMsgExecutorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    if (client != null) {

                        L.d("LiveShowMsgClientCtrl", " method : " + method + " toUserId : " + toUserId + " x : " + x + " y : " + y + " height : " + height + " width : " + width);

                        LinkMicRequestBean.ConnectBean linkMicRequestBean = new LinkMicRequestBean.ConnectBean(method, toUserId, x, y, height, width);

                        String payload = GsonUtils.createJsonString(linkMicRequestBean);

                        client.request("linkmic", payload).enqueue(new InterceptorResponseCallback() {
                            @Override
                            public void onError(Throwable error) {

                                L.d("LiveShowMsgClientCtrl", " Connect error message : " + error.getMessage());

                            }

                            @Override
                            public void onResponse(MsgPacket packet) {
                                super.onResponse(packet);
                                L.d("LiveShowMsgClientCtrl1", " getPayload : " + new String(packet.getPayload()));
                                L.d("LiveShowMsgClientCtrl1", " getCode : " + packet.getCode());
                                L.d("LiveShowMsgClientCtrl1", " nickName : " + nickName);
                                L.d("LiveShowMsgClientCtrl1", " avatar : " + avatar);

                                if (packet.getCode().equals("OK")) {
                                    final Message msg = Message.obtain();
                                    msg.what = UI_EVENT_ANCHOR_LINK_MIC_RESPONSE;
                                    Bundle bundle = new Bundle();
                                    bundle.putBoolean("dailyGuard", dailyGuard);
                                    bundle.putString("toUserId", toUserId);
                                    bundle.putString("nickName", nickName);
                                    bundle.putString("avatar", avatar);
                                    msg.setData(bundle);
                                    observer.sendMessage(msg);
                                } else if (packet.getCode().equals("linkmic_reject")) {
                                    //观众拒绝连麦
                                    Message message = observer.obtainMessage();
                                    message.what = UI_EVENT_ANCHOR_REJECT_LINK_MIC;
                                    message.obj = GsonUtils.getObject(new String(packet.getPayload()), RejectMicBean.class);
                                    observer.sendMessage(message);
                                } else if (packet.getCode().equals("low_cli_ver")) {//版本号过低
                                    //观众不在直播间
                                    observer.sendEmptyMessage(UI_EVENT_ANCHOR_ANDIENCE_NOT_IN);
                                }
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void rejectConnectMic(final String method, final String toUserId, final String result, final float x, final float y, final float height, final float width) {

        sendPkMsgExecutorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    if (client != null) {

                        LinkMicRequestBean.ResponseConnectBean responseConnectBean = new LinkMicRequestBean.ResponseConnectBean(method, toUserId, result, x, y, height, width);

                        String payload = GsonUtils.createJsonString(responseConnectBean);

                        L.d("LiveShowMsgClientCtrl", " Response payload : " + payload);

                        client.request("linkmic", payload).enqueue(new InterceptorResponseCallback() {
                            @Override
                            public void onError(Throwable error) {

                                L.d("LiveShowMsgClientCtrl", " Response error message : " + error.getMessage());

                            }

                            @Override
                            public void onResponse(MsgPacket packet) {
                                super.onResponse(packet);

                                String code = packet.getCode();

                                if (code != null && code.equals("OK")) {

                                    final Message msg = Message.obtain();
                                    msg.what = UI_EVENT_LINK_MIC_IS_ACCEPT;
                                    msg.obj = result;
                                    observer.sendMessage(msg);
                                }

                            }
                        });


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    public void cancelConnectMic(final String method, final String toUserId) {

        sendPkMsgExecutorService.execute(new Runnable() {
            @Override
            public void run() {
                try {

                    if (client != null) {

                        LinkMicRequestBean.CancelBean cancelBean = new LinkMicRequestBean.CancelBean(method, toUserId);

                        String payload = GsonUtils.createJsonString(cancelBean);

                        client.request("linkmic", payload).enqueue(new InterceptorResponseCallback() {
                            @Override
                            public void onError(Throwable error) {

                                L.d("LiveShowMsgClientCtrl", " error message : " + error.getMessage());

                            }

                            @Override
                            public void onResponse(MsgPacket packet) {
                                super.onResponse(packet);

                                L.d("LiveShowMsgClientCtrl", " Cancel getCode : " + packet.getCode());

                                L.d("LiveShowMsgClientCtrl", " Cancel getPayload : " + Arrays.toString(packet.getPayload()));

                            }
                        });

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    public void hangupConnectMic(final String method, final String toUserId) {

        sendPkMsgExecutorService.execute(new Runnable() {
            @Override
            public void run() {
                try {

                    if (client != null) {

                        LinkMicRequestBean.HangupBean hangupBean = new LinkMicRequestBean.HangupBean(method, toUserId);

                        String payload = GsonUtils.createJsonString(hangupBean);

                        client.request("linkmic", payload).enqueue(new InterceptorResponseCallback() {
                            @Override
                            public void onError(Throwable error) {

                                L.d("LiveShowMsgClientCtrl", " Hangup error message : " + error.getMessage());

                            }

                            @Override
                            public void onResponse(MsgPacket packet) {
                                super.onResponse(packet);

                                L.d("LiveShowMsgClientCtrl", " Hangup getCode : " + packet.getCode());

                                L.d("LiveShowMsgClientCtrl", " Hangup getPayload : " + Arrays.toString(packet.getPayload()));

                            }
                        });
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    public void responseAudienceLinkMic(final String code, final String body) {
        sendPkMsgExecutorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    if (client != null) {
                        client.request(code, body).enqueue(new InterceptorResponseCallback() {
                            @Override
                            public void onError(Throwable error) {
                                L.d(MULTI_TAG, "onError");
                            }

                            @Override
                            public void onResponse(MsgPacket packet) {
                                super.onResponse(packet);
                                L.d(MULTI_TAG, "payload: " + new String(packet.getPayload()));
                                if ("OK".equals(packet.getCode())) {
                                    L.d(MULTI_TAG, "responseAudienceLinkMic success");
//                                    observer.sendEmptyMessage(UI_EVENT_REFRESH_SEAT);
                                }
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /************************************************连麦方法end********************************************************************/

    /************************************************多人连麦方法start********************************************************************/
    public static final String MULTI_TAG = "audiobroadcast";

    @Override
    public void onSeat(int seatNum) {

    }

    @Override
    public void offSeat(final int seatNum) {
        sendPkMsgExecutorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    if (client != null) {
                        client.request("audiobroadcast", new JSONObject().put("method", LiveRoomMsgBean.TYPE_METHOD_OFFSEAT).put("seatNum", seatNum).toString()).enqueue(new InterceptorResponseCallback() {
                            @Override
                            public void onError(Throwable error) {
                                L.d(MULTI_TAG, "onError");
                            }

                            @Override
                            public void onResponse(MsgPacket packet) {
                                super.onResponse(packet);
                                L.d(MULTI_TAG, "payload: " + new String(packet.getPayload()));
                                if ("OK".equals(packet.getCode())) {
                                    L.d(MULTI_TAG, "onSeat success");
//                                    observer.sendEmptyMessage(UI_EVENT_REFRESH_SEAT);
                                }
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void mute(final int seatNum, final String micStatus) {
        sendPkMsgExecutorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    if (client != null) {
                        client.request("audiobroadcast", new JSONObject()
                                .put("method", LiveRoomMsgBean.TYPE_METHOD_MUTE)
                                .put("micStatus", micStatus)
                                .put("seatNum", seatNum).toString()).enqueue(new InterceptorResponseCallback() {
                            @Override
                            public void onError(Throwable error) {
                                L.d(MULTI_TAG, "onError");
                            }

                            @Override
                            public void onResponse(MsgPacket packet) {
                                super.onResponse(packet);
                                L.d(MULTI_TAG, "payload: " + new String(packet.getPayload()));
                                if ("OK".equals(packet.getCode())) {
                                    L.d(MULTI_TAG, "onSeat success");
//                                    observer.sendEmptyMessage(UI_EVENT_REFRESH_SEAT);
                                }
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void guestGift(int giftId, int combo, int seatNum) {

    }

    @Override
    public void startEncounter() {
        sendPkMsgExecutorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    if (client != null) {
                        client.request("audiobroadcast", new JSONObject()
                                .put("method", LiveRoomMsgBean.TYPE_METHOD_START_ENCOUNTER).toString()).enqueue(new InterceptorResponseCallback() {
                            @Override
                            public void onError(Throwable error) {
                                L.d(MULTI_TAG, "onError");
                            }

                            @Override
                            public void onResponse(MsgPacket packet) {
                                super.onResponse(packet);
                                L.d(MULTI_TAG, "payload: " + new String(packet.getPayload()));
                                if ("OK".equals(packet.getCode())) {
                                    L.d(MULTI_TAG, "startEncounter success");
//                                    observer.sendEmptyMessage(UI_EVENT_REFRESH_SEAT);
                                }
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void endEncounter() {

    }

    @Override
    public void encounterSb(int seatNum) {

    }

    @Override
    public void uploadAudioVolumn(final IRtcEngineEventHandler.AudioVolumeInfo[] speakers) {
        sendPkMsgExecutorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    if (client != null) {

                        MultiSpeakersBean multiSpeakersBean = new MultiSpeakersBean();
                        multiSpeakersBean.method = LiveRoomMsgBean.TYPE_METHOD_UPLOAD_VOLUMN;
                        for (int i = 0; i < speakers.length; i++) {
                            if (speakers[i].volume > 28) {
                                MultiSpeakersBean.Speakers bean = new MultiSpeakersBean.Speakers();
                                bean.uid = speakers[i].uid;
                                bean.volumn = speakers[i].volume;
                                multiSpeakersBean.speakers.add(bean);
                            }
                        }
                        if (multiSpeakersBean.speakers.size() > 0) {
                            String json = new GsonBuilder().create().toJson(multiSpeakersBean);
                            L.d(MULTI_TAG, "uploadAudioVolumn: " + json);
                            if (client != null) {
                                client.request("audiobroadcast", json).enqueue(new InterceptorResponseCallback() {
                                    @Override
                                    public void onError(Throwable error) {
                                        L.d(MULTI_TAG, "onError");
                                    }

                                    @Override
                                    public void onResponse(MsgPacket packet) {
                                        L.d(MULTI_TAG, "payload: " + new String(packet.getPayload()));
                                        if ("OK".equals(packet.getCode())) {
                                            L.d(MULTI_TAG, "uploadAudioVolumn success");
//                                    observer.sendEmptyMessage(UI_EVENT_REFRESH_SEAT);
                                        }
                                    }
                                });
                            }

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void muteAnchor(boolean isMute) {
        ExecutorServiceUtils.getInstatnce().exec(new Runnable() {
            @Override
            public void run() {

                int key = isMute ? 1 : 0;

                String requestJson = "{\"method\":\"hostMuted\",\"key\":" + isMute + "}";
                if (client != null) {
                    client.request("audiobroadcast", requestJson).enqueue(new ResponseCallback() {
                        @Override
                        public void onError(Throwable throwable) {

                        }

                        @Override
                        public void onResponse(MsgPacket msgPacket) {

                        }
                    });
                }
            }
        });
    }

    @Override
    public void requestSortMic() {

    }

    @Override
    public void cancelSortMic() {

    }

    @Override
    public void requestMicList() {

    }

    @Override
    public void getMicSortList() {
        sendPkMsgExecutorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    if (client != null) {
                        client.request("audiobroadcast", new JSONObject()
                                .put("method", LiveRoomMsgBean.TYPE_METHOD_MIC_LIST).toString()).enqueue(new InterceptorResponseCallback() {
                            @Override
                            public void onError(Throwable error) {
                                L.d(MULTI_TAG, "onError");
                            }

                            @Override
                            public void onResponse(MsgPacket packet) {
                                super.onResponse(packet);
                                if ("OK".equals(packet.getCode())) {
                                    L.d(MULTI_TAG, "getMicSortList success");
                                    LiveMultiSeatBean seat = GsonUtils.getObject(new String(packet.getPayload()), LiveMultiSeatBean.class);
                                    Message msg = Message.obtain();
                                    msg.obj = seat;
                                    msg.what = UI_EVENT_MIC_LIST;
                                    observer.sendMessage(msg);
                                }
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void requestLinkMicByAudience() {

    }

    @Override
    public void cancelLinkMIcByAudience() {

    }

    @Override
    public void linkMicHangup(String code, String userId) {

    }

    @Override
    public void requestLinkMicAudienceList() {

    }

    @Override
    public void guestLeaveRoom() {

    }

    @Override
    public void arGiftReceipt(String payload) {
        sendPkMsgExecutorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    if (client != null) {
                        client.request("argift_receipt", payload).enqueue(new InterceptorResponseCallback() {
                            @Override
                            public void onError(Throwable error) {
                                L.d(MULTI_TAG, "onError");
                            }

                            @Override
                            public void onResponse(MsgPacket packet) {
                                super.onResponse(packet);
                                L.d(MULTI_TAG, "payload: " + new String(packet.getPayload()));
                                if ("OK".equals(packet.getCode())) {
                                    L.d(MULTI_TAG, "arGiftReceipt success");
                                }
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void getTopFansTodayList() {
        sendPkMsgExecutorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    if (client != null) {
                        client.request("top_fans_today_list", new JSONObject().toString()).enqueue(new InterceptorResponseCallback() {
                            @Override
                            public void onError(Throwable error) {
                                L.d(MULTI_TAG, "onError");
                            }

                            @Override
                            public void onResponse(MsgPacket packet) {
                                super.onResponse(packet);
                                L.d(MULTI_TAG, "payload: " + new String(packet.getPayload()));
                                if ("OK".equals(packet.getCode())) {
                                    L.d(MULTI_TAG, "getTopFansTodayList success");
                                    List<LivePkFriendBean> list = new GsonBuilder().create().fromJson(new String(packet.getPayload()), TopTodayListBean.class).list;
                                    Message message = observer.obtainMessage();
                                    message.what = UI_EVENT_GET_TOP_TODAY_LIST;
                                    message.obj = list;
                                    observer.sendMessage(message);
                                }
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }


    @Override
    public void openMicSort() {
        sendPkMsgExecutorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    if (client != null) {
                        client.request("audiobroadcast", new JSONObject()
                                .put("method", LiveRoomMsgBean.TYPE_METHOD_MIC_ON).toString()).enqueue(new InterceptorResponseCallback() {
                            @Override
                            public void onError(Throwable error) {
                                L.d(MULTI_TAG, "onError");
                            }

                            @Override
                            public void onResponse(MsgPacket packet) {
                                super.onResponse(packet);
                                L.d(MULTI_TAG, "payload: " + new String(packet.getPayload()));
                                if ("OK".equals(packet.getCode())) {
                                    L.d(MULTI_TAG, "openMicSort success");
                                    observer.sendEmptyMessage(UI_EVENT_REFRESH_SEAT);
                                }
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void closeMicSort() {
        sendPkMsgExecutorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    if (client != null) {
                        client.request("audiobroadcast", new JSONObject()
                                .put("method", LiveRoomMsgBean.TYPE_METHOD_MIC_OFF).toString()).enqueue(new InterceptorResponseCallback() {
                            @Override
                            public void onError(Throwable error) {
                                L.d(MULTI_TAG, "onError");
                            }

                            @Override
                            public void onResponse(MsgPacket packet) {
                                super.onResponse(packet);

                                L.d(MULTI_TAG, "payload: " + new String(packet.getPayload()));

                                if ("OK".equals(packet.getCode())) {
                                    L.d(MULTI_TAG, "closeMicSort success");
//                                    observer.sendEmptyMessage(UI_EVENT_REFRESH_SEAT);
                                }
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void agreeOnSeat(final String userId, final AgoraBean agoraBean) {
        sendPkMsgExecutorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    if (client != null) {

                        String agoraJson = GsonUtils.createJsonString(agoraBean);

                        L.d(TAG, " agreeOnSeat agoraJson : " + agoraJson);

                        client.request("audiobroadcast", new JSONObject()
                                .put("userId", userId)
                                .put("method", LiveRoomMsgBean.TYPE_METHOD_MIC_AGREE)
                                .put("agora", agoraJson).toString()).enqueue(new InterceptorResponseCallback() {
                            @Override
                            public void onError(Throwable error) {
                                L.d(MULTI_TAG, "onError");
                            }

                            @Override
                            public void onResponse(MsgPacket packet) {
                                super.onResponse(packet);
                                L.d(MULTI_TAG, "payload: " + new String(packet.getPayload()));
                                if ("OK".equals(packet.getCode())) {
                                    L.d(MULTI_TAG, "agreeOnSeat success");
//                                    observer.sendEmptyMessage(UI_EVENT_REFRESH_SEAT);
                                }
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void sendDanmu(String userLevel, String content, String barrageId) {
        sendDanmuRunnable.setDanmuContent(userLevel, content, barrageId);
        ExecutorServiceUtils.getInstatnce().exec(sendDanmuRunnable);
    }

    @Override
    public void sendMsg(String msg) {
        sendMsgRunnable.setMsgContent(msg);
        ExecutorServiceUtils.getInstatnce().exec(sendMsgRunnable);
    }

    /************************************************多人连麦方法end********************************************************************/

    private NetworkReceiver.NetworkListener mNetworkListener = new NetworkReceiver.NetworkListener() {
        @Override
        public void onNetworkEnable(boolean isNetworkEnable) {
            L.d(TAG, " isNetworkEnable : " + isNetworkEnable);
            if (isNetworkEnable) {
                disconnect();
                tryCreateClient();
                pushErrorMsg();
            }
        }
    };

    private void changeIP() {

        if (hosts == null || hosts.size() == 0) {
            host = liveRoomBean.livechat.host;
            port = liveRoomBean.livechat.port;
        } else {
            if (hosts.size() <= ipIndex) {
                ipIndex = 0;
            }
            LiveChatBean.HostBean hostBean = hosts.get(ipIndex);
            host = hostBean.host;
            port = hostBean.port;
            ipIndex++;
        }

    }

    private void pushErrorLog() {
        LiveInfoLogBean.getInstance().getLiveChatAnalytics().role = "host";
        LiveInfoLogBean.getInstance().getLiveChatAnalytics().cdnDomain = host;
        LiveInfoLogBean.getInstance().getLiveChatAnalytics().logType = TheLConstants.LiveInfoLogConstants.LIVE_MSG_ERROR_LOG;
        LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getLiveChatAnalytics());
    }

    private void initPing() {

        L.i(TAG, " initPing : ");

        if (compositeDisposable != null) {
            compositeDisposable.clear();
        }
        Disposable disposable = Flowable.interval(10, TimeUnit.SECONDS).onBackpressureDrop().subscribe(new Consumer<Long>() {
            @Override
            public void accept(Long aLong) throws Exception {
                ExecutorServiceUtils.getInstatnce().exec(new Runnable() {
                    @Override
                    public void run() {
                        if (client != null) {
                            client.request("ping", "", pingTimeout).enqueue(new InterceptorResponseCallback() {
                                @Override
                                public void onError(Throwable error) {
                                    // ping不通，重连
                                    Log.e(TAG, "ping不通，重连");
                                    isConnecting = false;
                                    isReconnecting = false;
                                    postConnectionInterrupted();
                                    reconnect();
                                    UmentPushUtils.onMsgEvent(TheLApp.context, "live_host_msg_ping_timeout_android");
                                }

                                @Override
                                public void onResponse(MsgPacket packet) {

                                    Log.e(TAG, " ping成功 " + packet.getCode());

                                    if (!"pong".equals(packet.getCode())) {
                                        // ping不通，重连
                                        Log.e(TAG, "ping不通，重连");
                                        isConnecting = false;
                                        isReconnecting = false;
                                        postConnectionInterrupted();
                                        reconnect();
                                        UmentPushUtils.onMsgEvent(TheLApp.context, "live_host_msg_ping_fail_android");

                                    }
                                }
                            });
                        }

                    }
                });
            }
        });

        compositeDisposable.add(disposable);
    }

    /**
     * 发送弹幕runnable
     */
    class SendDanmuRunnable implements Runnable {

        private String userLevel;

        private String content = "";

        private String barrageId;

        public void setDanmuContent(String userLevel, String content, String barrageId) {
            this.userLevel = userLevel;
            this.content = content;
            this.barrageId = barrageId;
        }

        @Override
        public void run() {
            try {
                if (client != null) {

                    RequestSendDanmuBean requestSendDanmuBean = new RequestSendDanmuBean();
                    requestSendDanmuBean.userLevel = userLevel;
                    requestSendDanmuBean.content = content;
                    requestSendDanmuBean.barrageId = barrageId;

                    String body = GsonUtils.createJsonString(requestSendDanmuBean);

                    observer.sendEmptyMessage(UI_EVENT_ANCHOR_LOCK_INPUT);
                    client.request("sendbarrage", body).enqueue(new InterceptorResponseCallback() {
                        @Override
                        public void onError(Throwable error) {
                            //TODU shibai bing jiesuo
                            observer.sendEmptyMessage(UI_EVENT_OPEN_INPUT_FALSE);
                            error.printStackTrace();
                            isConnecting = false;
                            isReconnecting = false;
                            L.d(TAG, " send danmu error");
                            postConnectionInterrupted();
                            reconnect();
                        }

                        @Override
                        public void onResponse(MsgPacket packet) {

                            L.d(TAG, " SendDanmuRunnable getPayload " + new String(packet.getPayload()));

                            if ("OK".equals(packet.getCode())) {//赠送礼物成功，打赏成功
                                Message msg = Message.obtain();
                                msg.what = UI_EVENT_OPEN_INPUT_TRUE;
                                msg.obj = new String(packet.getPayload());
                                observer.sendMessage(msg);
                            } else if (LiveRoomMsgBean.TYPE_BANED.equals(packet.getCode())) {// 被屏蔽了
                                observer.sendEmptyMessage(UI_EVENT_BEEN_BLOCKED);
                            }
                        }
                    });
                }
            } catch (Exception e) {
                // TODO shibai bing jiesuo
                observer.sendEmptyMessage(UI_EVENT_OPEN_INPUT_FALSE);
                e.printStackTrace();
            }
        }


    }

    /**
     * 普通消息
     */
    class SendMsgRunnable implements Runnable {
        private String msgContent = "";

        public void setMsgContent(String msgContent) {
            this.msgContent = msgContent;
        }

        @Override
        public void run() {
            try {
                if (client != null) {
                    client.request("send", new JSONObject().put("type", LiveRoomMsgBean.TYPE_MSG).put("content", msgContent + "").toString()).enqueue(new InterceptorResponseCallback() {
                        @Override
                        public void onError(Throwable error) {
                            error.printStackTrace();
                            isConnecting = false;
                            isReconnecting = false;
                            L.d(TAG, " send msg error");
                            postConnectionInterrupted();
                            reconnect();
                        }

                        @Override
                        public void onResponse(MsgPacket packet) {

                            L.d(TAG, " send msg code : " + packet.getCode());

                            if (LiveRoomMsgBean.TYPE_BANED.equals(packet.getCode())) {// 被屏蔽了
                                observer.sendEmptyMessage(UI_EVENT_BEEN_BLOCKED);
                            } else if (LiveRoomMsgBean.RESPONSE_TYPE_TOO_FAST.equals(packet.getCode())) {// 说话频率太快
                                observer.sendEmptyMessage(UI_EVENT_SPEAK_TO_FAST);
                            }
                        }
                    });
                    observer.sendEmptyMessage(UI_EVENT_CLEAR_INPUT);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


}
