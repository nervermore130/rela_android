package com.thel.modules.main.video_discover.pgc;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.thel.R;
import com.thel.bean.video.VideoBean;
import com.thel.modules.main.video_discover.autoplay.AutoPlayVideoCtrl;
import com.thel.modules.main.video_discover.autoplay.VideoAutoPlayImp;
import com.thel.modules.main.video_discover.videofalls.RecyclerViewStateBean;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;

import java.util.List;

/**
 * Created by waiarl on 2018/3/16.
 */

public class PgcVideoFallsAdapter extends BaseRecyclerViewAdapter<VideoBean> {
    public static final String TAG = PgcVideoFallsAdapter.class.getSimpleName();
    private final RecyclerViewStateBean recyclerViewStateBean;
    private final AutoPlayVideoCtrl autoPlayVideoCtrl;

    public PgcVideoFallsAdapter(List<VideoBean> data, RecyclerViewStateBean recyclerViewStateBean, AutoPlayVideoCtrl autoPlayVideoCtrl) {
        super(R.layout.adapter_video_up_falls, data);
        this.recyclerViewStateBean = recyclerViewStateBean;
        this.autoPlayVideoCtrl = autoPlayVideoCtrl;

    }

    @Override
    protected void convert(BaseViewHolder helper, VideoBean item) {
        final VideoFallAdapterViewImp view_pgc = helper.getView(R.id.view_pgc);
        final VideoFallAdapterViewImp view_ugc = helper.getView(R.id.view_ugc);
        final int position = helper.getLayoutPosition() - getHeaderLayoutCount();
        view_pgc.getView().setVisibility(View.GONE);
        view_ugc.getView().setVisibility(View.GONE);
        if (VideoBean.VIDEO_TYPE_PGC == item.videoType) {
            view_pgc.initView(item, position, recyclerViewStateBean);
            view_pgc.getView().setVisibility(View.VISIBLE);
            if (view_pgc instanceof VideoAutoPlayImp) {
                ((VideoAutoPlayImp) view_pgc).bindAutoPlayVideoCtrl(autoPlayVideoCtrl);
            }
        } else {
            view_ugc.initView(item, position, recyclerViewStateBean);
            view_ugc.getView().setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        final RecyclerView.LayoutManager manager = recyclerView.getLayoutManager();
        if (manager instanceof GridLayoutManager) {
            final GridLayoutManager gridManager = ((GridLayoutManager) manager);
            gridManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    int type = getItemViewType(position);
                    if (type == EMPTY_VIEW || type == HEADER_VIEW || type == FOOTER_VIEW || type == LOADING_VIEW) {
                        return gridManager.getSpanCount();
                    } else {
                        final int headerCount = getHeaderLayoutCount();
                        final int dataPos = position - headerCount;
                        if (dataPos < getData().size()) {
                            final VideoBean bean = getItem(dataPos);
                            if (VideoBean.VIDEO_TYPE_PGC == bean.videoType) {
                                return gridManager.getSpanCount();
                            }
                        }
                        return 1;
                    }
                }
            });
        }
    }

}
