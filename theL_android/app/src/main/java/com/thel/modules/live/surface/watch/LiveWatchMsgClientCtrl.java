package com.thel.modules.live.surface.watch;

import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.BasicInfoBean;
import com.thel.bean.LiveInfoLogBean;
import com.thel.bean.live.LinkMicRequestBean;
import com.thel.bean.live.LinkMicResponseBean;
import com.thel.bean.live.LiveChatCreateResBean;
import com.thel.bean.live.LiveMultiOnCreateBean;
import com.thel.bean.live.LiveMultiSeatBean;
import com.thel.callback.IConnectMic;
import com.thel.chat.live.LiveChatBuilder;
import com.thel.chat.live.LiveChatClientImpl;
import com.thel.chat.live.interfaces.ILiveChat;
import com.thel.chat.live.interfaces.OnConnectStatesListener;
import com.thel.chat.live.interfaces.OnMessageListener;
import com.thel.constants.TheLConstants;
import com.thel.modules.live.bean.AgoraBean;
import com.thel.modules.live.bean.AudienceLinkMicResponseBean;
import com.thel.modules.live.bean.LiveChatBean;
import com.thel.modules.live.bean.LivePkGemNoticeBean;
import com.thel.modules.live.bean.LivePkHangupBean;
import com.thel.modules.live.bean.LivePkInitBean;
import com.thel.modules.live.bean.LivePkStartNoticeBean;
import com.thel.modules.live.bean.LivePkSummaryNoticeBean;
import com.thel.modules.live.bean.LiveRoomBean;
import com.thel.modules.live.bean.LiveRoomMsgBean;
import com.thel.modules.live.bean.LiveRoomMsgConnectMicBean;
import com.thel.modules.live.bean.RequestSendDanmuBean;
import com.thel.modules.live.bean.SoftGiftBean;
import com.thel.modules.live.in.LiveShowMsgIn;
import com.thel.modules.live.interfaces.IChatMessageDelegate;
import com.thel.modules.live.surface.OnRelaMessageRequestListener;
import com.thel.modules.live.utils.LiveUtils;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.service.DefaultRequestService;
import com.thel.receiver.NetworkReceiver;
import com.thel.utils.DeviceUtils;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;
import com.thel.utils.MD5Utils;
import com.thel.utils.NetworkUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.ToastUtils;
import com.thel.utils.UmentPushUtils;
import com.thel.utils.UserUtils;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.agora.rtc.IRtcEngineEventHandler;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_AUDIENCE_BEAM_AVAILABLE_LEVEL_SIX;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_AUDIENCE_LINK_MIC_RESPONSE;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_AUTO_PING;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_BEEN_BLOCKED;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_CHANNEL_ID;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_CLEAR_INPUT;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_DAILY_LINK_MIC;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_GIFT_SEND_RESULT_MSG;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_GUEST_OFF_SEAT;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_GUEST_ONSEAR_AUTH;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_GUEST_ON_SEAT;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_INIT_MULTI;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_LINK_MIC_BUSY;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_LINK_MIC_CANCEL;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_LINK_MIC_HANGUP;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_LINK_MIC_IS_ACCEPT;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_LINK_MIC_STOP;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_LIVE_CLOSED;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_MIC_LIST;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_OPEN_INPUT_FALSE;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_OPEN_INPUT_TRUE;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_PK_GEM_NOTICE;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_PK_HANGUP_NOTICE;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_PK_INIT_MSG;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_PK_START_NOTICE;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_PK_STOP_NOTICE;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_PK_SUMMARY_NOTICE;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_REFRESH_MSGS;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_REQUEST;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_SPEAK_TO_FAST;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_USER_ID;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_USER_NAME;

/**
 * Created by waiarl on 2017/11/5.
 */

public class LiveWatchMsgClientCtrl implements LiveShowMsgIn, IConnectMic {

    private final static String TAG = "LiveWatchMsgClientCtrl";

    private LiveRoomBean liveRoomBean;

    private ILiveChat iLiveChat;

    private LiveWatchObserver observer;

    private String host;

    private int port;

    private int ipIndex = 0;

    private List<LiveChatBean.HostBean> hosts = null;
    //关注code
    public static final String FOLLOW_CODE = "add_fans";
    //推荐主播code
    public static final String RECOMMEND_CODE = "recomm";
    //分享直播code
    public static final String SHARE_TO_CODE = "shareto";

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    private String token = "";

    private IChatMessageDelegate mIChatMessageDelegate;

    private boolean isAudience = false;

    private boolean isReconnect = false;

    public LiveWatchMsgClientCtrl() {
    }

    public LiveWatchMsgClientCtrl refreshClient(LiveRoomBean liveRoomBean) {

        isReconnect = false;

        L.d(TAG, "refreshClient ");

        if (liveRoomBean == null) {

            ToastUtils.showToastShort(TheLApp.context, "服务器异常,请退出重试。");

            return this;
        }

        isAudience = !String.valueOf(liveRoomBean.user.id).equals(UserUtils.getMyUserId());

        L.d(TAG, "refreshClient isAudience : " + isAudience);

        compositeDisposable.clear();

        this.liveRoomBean = liveRoomBean;
        if (liveRoomBean.livechat != null && liveRoomBean.livechat.hosts != null) {
            hosts = liveRoomBean.livechat.hosts;
        }
        ipIndex = 0;
        changeIP();

        if (liveRoomBean.livechat != null) {
            tryCreateClient();
        }
        return this;
    }


    @Override
    public void bindObserver(LiveWatchObserver observer) {

        L.d(TAG, "bindObserver ");

        this.observer = observer;
        observer.bindMsgClient(this);
        observer.bindConnectMic(this);
    }

    @Override
    public void tryCreateClient() {

        isReconnect = false;

        L.d(TAG, "tryCreateClient liveRoomBean " + liveRoomBean);

        L.d(TAG, "tryCreateClient observer " + observer);

        //  Android直播消息开始连接

        postConnecting();

        UmentPushUtils.onMsgEvent(TheLApp.context, "live_msg_start_connect_android");

        LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().log = "status:StartConnect,host:" + host + ",port:" + port;

        LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics());

        if (liveRoomBean != null) {

            if (mIChatMessageDelegate == null) {

                if (!isAudience) {
                    mIChatMessageDelegate = new AnchorMessageDelegateImpl();
                } else {
                    mIChatMessageDelegate = new AudienceMessageDelegateImpl();
                }

                mIChatMessageDelegate.init(observer, liveRoomBean);
            }

            if (liveRoomBean.livechat.liveChatSwitch == 0) {
                NetworkReceiver.getInstance().register(LiveWatchMsgClientCtrl.class.getName(), mNetworkListener);
            }

            String userSig = TextUtils.isEmpty(liveRoomBean.livechat.userSig) ? "" : liveRoomBean.livechat.userSig;

            LiveChatBuilder liveChatBuilder = new LiveChatBuilder.Builder()
                    .type(liveRoomBean.livechat.liveChatSwitch)
                    .host(liveRoomBean.livechat.host)
                    .port(liveRoomBean.livechat.port)
                    .groupId(liveRoomBean.id)
                    .isAudience(isAudience)
                    .onConnectStatesListener(onConnectStatesListener)
                    .onMessageListener(onMessageListener)
                    .userId(UserUtils.getMyUserId())
                    .userSigId(userSig)
                    .build();

            iLiveChat = LiveChatClientImpl.Companion.createChatRoom(liveChatBuilder);

            if (!isAudience) {
                iLiveChat.createChatGroup();
            } else {
                iLiveChat.joinChatRoom();
            }

            observer.setIChatRoomRequest(new ChatRoomRequestImpl(iLiveChat, observer));

        }
    }

    private OnConnectStatesListener onConnectStatesListener = new OnConnectStatesListener() {
        @Override
        public void onConnectSuccess() {

            L.d(TAG, " -------onConnectSuccess------- ");

            token = liveRoomBean.livechat.token;

            postConnectSucceed();

            init();

            isReconnect = true;
        }

        @Override
        public void onClose() {

            L.d(TAG, " -------onClose------- ");

            postConnectionInterrupted();

//            LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().log = "status:Disconnected errorReason: 服务端断开连接";
//
//            LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics());

        }

        @Override
        public void onError(@Nullable String exDes) {

            L.d(TAG, " -------onError------- " + exDes);
            postConnectionInterrupted();

            LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().log = exDes;
            LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().logType = "tencent_error_log";
            LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics());

        }
    };

    private OnMessageListener onMessageListener = new OnMessageListener() {
        @Override
        public void onFailed() {

        }

        @Override
        public void onSuccess(@NotNull String code, @NotNull String result) {

            messageResponse(code, result);

        }
    };

    @Override
    public void init() {
        try {

            LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().log = "status:ConnectSuccess";
//
//            LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics());
//            //  Android直播消息连接成功
            UmentPushUtils.onMsgEvent(TheLApp.context, "live_msg_connect_success_android");

            String body = getInitJsonString();

            L.d(TAG, " init body : " + body);

            //  Android  直播消息开始认证
            UmentPushUtils.onMsgEvent(TheLApp.context, "live_msg_start_auth_android");

//            LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().log = "status:StartAuth";
//
//            LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics());

            iLiveChat.sendMsg("init", body, new OnRelaMessageRequestListener() {
                @Override
                public void onMessageError(@Nullable Throwable error) {

                    if (error != null) {
                        L.d(TAG, " init onError : " + error.getMessage());
                    }

//                    LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().log = "status:AuthTimeOut";
//
//                    LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics());
//
//                    UmentPushUtils.onMsgEvent(TheLApp.context, "live_msg_fail", "auth_timeout"); //  Android  认证超时
//
//                    LiveInfoLogBean.getInstance().getLiveChatAnalytics().errorReason = "auth_timeout";
//
//                    pushErrorLog();

                }

                @Override
                public void onMessageSuccess(@NotNull String code, @NotNull String payload) {
                    L.d(TAG, " init onSuccess code : " + code);

                    L.d(TAG, " init onSuccess payload : " + payload);

                    if ("OK".equals(code)) {

//                        LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().log = "status:AuthSuccess";
//
//                        LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics());

                        UmentPushUtils.onMsgEvent(TheLApp.context, "live_msg_auth_success_android"); //  Android  直播消息认证成功

                        observer.sendEmptyMessageDelayed(UI_EVENT_AUTO_PING, 30 * 1000);

                        initPkBusyMsg(code, payload);
                        initLinkMicMsg(code, payload);
                        initMultiMicMsg(code, payload);
                        if (liveRoomBean != null && liveRoomBean.livechat != null && liveRoomBean.livechat.liveChatSwitch == 0) {
                            initPing();
                        }
                    } else {

//                        LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().log = "status:AuthFail_" + code;
//
//                        LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics());

//                        UmentPushUtils.onMsgEvent(TheLApp.context, "live_msg_fail", code); //  Android  认证失败

//                        if ("no_such_channel".equals(code)) {
//                            observer.sendEmptyMessage(UI_EVENT_LIVE_CLOSED);
//                        }
//
//                        if ("require_channel_id".equals(code)) {
//                            observer.sendEmptyMessage(UI_EVENT_CHANNEL_ID);
//                        }
//
//                        if ("invalid_request".equals(code)) {
//                            observer.sendEmptyMessage(UI_EVENT_REQUEST);
//                        }
//
//                        if ("require_user_id".equals(code)) {
//                            observer.sendEmptyMessage(UI_EVENT_USER_ID);
//                        }
//
//                        if ("require_nickname".equals(code)) {
//                            observer.sendEmptyMessage(UI_EVENT_USER_NAME);
//                        }
//
//                        LiveInfoLogBean.getInstance().getLiveChatAnalytics().errorReason = "live_msg_fail_" + code;
//
//                        pushErrorLog();

                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();

//            LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().log = "status:ConnectFail";
//
//            LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics());
        }
    }


    private void messageResponse(String code, String payload) {

        L.d(TAG, " messageResponse code : " + code);

        L.d(TAG, " messageResponse payload : " + payload);

        L.d(TAG, " messageResponse mIChatMessageDelegate : " + mIChatMessageDelegate);

        if (!TextUtils.isEmpty(code) && !TextUtils.isEmpty(payload)) {
            if (liveRoomBean != null && liveRoomBean.user != null) {
                if (mIChatMessageDelegate != null) {
                    mIChatMessageDelegate.messageDelegate(code, payload);
                }
            }
        }

    }

    private void disconnect() {

        if (observer != null) {
            observer.removeMessages(UI_EVENT_AUTO_PING);
        }

        try {
            if (iLiveChat != null) {
                iLiveChat.closeChatRoom();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * init 的param json 数据
     *
     * @return
     */
    private String getInitJsonString() {
        double lt = 0.0;
        double lg = 0.0;
        try {
            lt = Double.parseDouble(ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0"));
            lg = Double.parseDouble(ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        final double lat = lt;
        final double lng = lg;
        final JSONObject obj = new JSONObject() {
            {
                try {

                    LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().token = token;

                    put("version", TheLConstants.MSG_CLIENT_VERSION);
                    put("os", "Android");
                    put("appver", DeviceUtils.getVersionCode(TheLApp.getContext()) + "");
                    put("deviceId", UserUtils.getDiviceId());
                    put("lat", lat);
                    put("lng", lng);
                    put("lang", DeviceUtils.getLanguage());
                    put("channel", liveRoomBean.livechat.channel);
                    put("token", liveRoomBean.livechat.token);
                    put("userId", ShareFileUtils.getString(ShareFileUtils.ID, ""));
                    put("nickName", ShareFileUtils.getString(ShareFileUtils.USER_NAME, ""));
                    put("avatar", ShareFileUtils.getString(ShareFileUtils.AVATAR, ""));
                    put("isOwner", !isAudience);
                    put("reconnect", isReconnect);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        return obj.toString();
    }


    @Override
    public void autoPingServer() {

    }

    @Override
    public void onDestroy() {
        disconnect();
        NetworkReceiver.getInstance().unregister(LiveWatchMsgClientCtrl.class.getName());
        if (compositeDisposable != null) {
            compositeDisposable.clear();
        }
//        LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().log = "status:Disconnected errorReason: 手动断开连接";
//
//        LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics());
    }


    @Override
    public void reset() {
        disconnect();
        ipIndex = 0;
    }


    @Override
    public void sendSoftGift(SoftGiftBean softGiftBean, int id, String type, int combo, String hostUserId) {

        L.d(TAG, " sendSoftGift hostUserId ：" + hostUserId);

        try {

            if (iLiveChat != null) {

                String body;
                if (softGiftBean.isArGift == 1) {
                    body = new JSONObject()
                            .put("id", id)
                            .put("isArGift", true)
                            .put("arGiftId", softGiftBean.arGiftId)
                            .put("combo", combo)
                            .put("hostUserId", hostUserId).toString();
                } else {
                    body = new JSONObject()
                            .put("id", id)
                            .put("combo", combo)
                            .put("hostUserId", hostUserId).toString();
                }

                iLiveChat.sendMsg(type, body, new OnRelaMessageRequestListener() {
                    @Override
                    public void onMessageError(@Nullable Throwable error) {

                        L.d(TAG, " sendSoftGift onMessageError ：" + error.getMessage());

                    }

                    @Override
                    public void onMessageSuccess(@NotNull String code, @NotNull String payload) {
                        L.d(TAG, " SendGiftRunnable onResponse getPayload : " + payload);

                        L.d(TAG, " SendGiftRunnable onResponse getCode : " + code);

                        if ("OK".equals(code)) {//赠送礼物成功，打赏成功
                            Message msg = Message.obtain();
                            msg.what = UI_EVENT_GIFT_SEND_RESULT_MSG;
                            msg.obj = payload;
                            observer.sendMessage(msg);
                            String myUserid = ShareFileUtils.getString(ShareFileUtils.ID, "");
                            String brocaster_id = ShareFileUtils.getString(ShareFileUtils.Brocaster_ID, "");
                            try {

                                if (iLiveChat != null && !TextUtils.isEmpty(softGiftBean.resource)) {

                                    String body = new JSONObject().put("userId", myUserid).put("toUserId", brocaster_id).put("giftId", softGiftBean.id).put("action", "receipt").toString();

                                    iLiveChat.sendMsg("gift_report", body, new OnRelaMessageRequestListener() {
                                        @Override
                                        public void onMessageError(@Nullable Throwable error) {

                                        }

                                        @Override
                                        public void onMessageSuccess(@NotNull String code, @NotNull String payload) {

                                        }
                                    });

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        } else if (LiveRoomMsgBean.TYPE_BANED.equals(code)) {// 被屏蔽了
                            observer.sendEmptyMessage(UI_EVENT_BEEN_BLOCKED);
                        }
                    }
                });

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void postGiftplayStatus(String userId, String toUserId, int giftid) {
        try {

            if (iLiveChat != null) {
                String brocaster_id = ShareFileUtils.getString(ShareFileUtils.Brocaster_ID, "");

                String body = new JSONObject().put("userId", userId).put("toUserId", brocaster_id).put("giftId", giftid).put("action", "play").toString();

                iLiveChat.sendMsg("gift_report", body, new OnRelaMessageRequestListener() {
                    @Override
                    public void onMessageError(@Nullable Throwable error) {

                    }

                    @Override
                    public void onMessageSuccess(@NotNull String code, @NotNull String payload) {
                        L.d("LiveWatchMsgClientCtrl", " PlayGiftRunnable onResponse getPayload : " + payload);

                        L.d("LiveWatchMsgClientCtrl", " PlayGiftRunnable onResponse getCode : " + code);
                    }
                });

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sendMsg(String msg) {

        try {
            if (iLiveChat != null) {
                iLiveChat.sendMsg("send", new JSONObject().put("type", LiveRoomMsgBean.TYPE_MSG).put("content", msg + "").toString(), new OnRelaMessageRequestListener() {
                    @Override
                    public void onMessageError(@Nullable Throwable error) {
                        L.d(TAG, " send msg error" + error.getMessage());
                    }

                    @Override
                    public void onMessageSuccess(@NotNull String code, @NotNull String payload) {
                        L.d(TAG, " send msg code : " + code);

                        if (LiveRoomMsgBean.TYPE_BANED.equals(code)) {// 被屏蔽了
                            observer.sendEmptyMessage(UI_EVENT_BEEN_BLOCKED);
                        } else if (LiveRoomMsgBean.RESPONSE_TYPE_TOO_FAST.equals(code)) {// 说话频率太快
                            observer.sendEmptyMessage(UI_EVENT_SPEAK_TO_FAST);
                        }
                    }
                });

                observer.sendEmptyMessage(UI_EVENT_CLEAR_INPUT);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void sendPlayStickFailMsg() {
        try {
            if (iLiveChat != null) {
                iLiveChat.sendMsg("gift_fail_play", "", new OnRelaMessageRequestListener() {
                    @Override
                    public void onMessageError(@Nullable Throwable error) {
                        L.d(TAG, " send gift_fail_play error :" + error);
                    }

                    @Override
                    public void onMessageSuccess(@NotNull String code, @NotNull String payload) {
                        L.d(TAG, " send gift_fail_play successful code:" + code + " payload:" + payload);
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sendDanmu(String userLevel, String content, String barrageId) {

        try {
            if (iLiveChat != null) {

                RequestSendDanmuBean requestSendDanmuBean = new RequestSendDanmuBean();
                requestSendDanmuBean.content = content;
                requestSendDanmuBean.barrageId = barrageId;
                requestSendDanmuBean.type = "msg";

                String body = GsonUtils.createJsonString(requestSendDanmuBean);

                iLiveChat.sendMsg("sendbarrage", body, new OnRelaMessageRequestListener() {
                    @Override
                    public void onMessageError(@Nullable Throwable error) {
                        observer.sendEmptyMessage(UI_EVENT_OPEN_INPUT_FALSE);
                    }

                    @Override
                    public void onMessageSuccess(@NotNull String code, @NotNull String payload) {
                        if ("OK".equals(code)) {//赠送礼物成功，打赏成功
                            Message msg = Message.obtain();
                            msg.what = UI_EVENT_OPEN_INPUT_TRUE;
                            msg.obj = payload;
                            observer.sendMessage(msg);
                        } else if (LiveRoomMsgBean.TYPE_BANED.equals(code)) {// 被屏蔽了
                            observer.sendEmptyMessage(UI_EVENT_BEEN_BLOCKED);
                        }
                    }
                });

            }
        } catch (Exception e) {
            // TODO shibai bing jiesuo
            observer.sendEmptyMessage(UI_EVENT_OPEN_INPUT_FALSE);
            e.printStackTrace();
        }
    }

    @Override
    public void addFollowFans() {
        try {
            if (iLiveChat != null) {
                iLiveChat.sendMsg(FOLLOW_CODE, "", new OnRelaMessageRequestListener() {
                    @Override
                    public void onMessageError(@Nullable Throwable error) {

                    }

                    @Override
                    public void onMessageSuccess(@NotNull String code, @NotNull String payload) {

                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void shareToLive() {
        try {
            if (iLiveChat != null) {
                iLiveChat.sendMsg(SHARE_TO_CODE, "", new OnRelaMessageRequestListener() {
                    @Override
                    public void onMessageError(@Nullable Throwable error) {

                    }

                    @Override
                    public void onMessageSuccess(@NotNull String code, @NotNull String payload) {

                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void recommendLive() {
        try {
            if (iLiveChat != null) {
                iLiveChat.sendMsg(RECOMMEND_CODE, "", new OnRelaMessageRequestListener() {
                    @Override
                    public void onMessageError(@Nullable Throwable error) {

                    }

                    @Override
                    public void onMessageSuccess(@NotNull String code, @NotNull String payload) {

                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void postConnectionFailed() {

        L.i(TAG, "--------postConnectionFailed-------");

        LiveRoomMsgBean liveRoomMsgBean = new LiveRoomMsgBean();
        liveRoomMsgBean.type = LiveRoomMsgBean.TYPE_CONNECT_FAILED;
        observer.addMsg(liveRoomMsgBean);
        observer.sendEmptyMessage(UI_EVENT_REFRESH_MSGS);
    }

    private void postConnectSucceed() {

        L.i(TAG, "--------postConnectSucceed-------");

        LiveRoomMsgBean liveRoomMsgBean = new LiveRoomMsgBean();
        liveRoomMsgBean.type = LiveRoomMsgBean.TYPE_CONNECT_SUCCEED;
        observer.addMsg(liveRoomMsgBean);
        observer.sendEmptyMessage(UI_EVENT_REFRESH_MSGS);
    }

    private void postConnecting() {

        L.i(TAG, "--------postConnecting-------");

        LiveRoomMsgBean liveRoomMsgBean = new LiveRoomMsgBean();
        liveRoomMsgBean.type = LiveRoomMsgBean.TYPE_CONNECTING;
        observer.addMsg(liveRoomMsgBean);
        observer.sendEmptyMessage(UI_EVENT_REFRESH_MSGS);
    }

    private void postConnectionInterrupted() {

        L.i(TAG, "--------postConnectionInterrupted-------");

        LiveRoomMsgBean liveRoomMsgBean = new LiveRoomMsgBean();
        liveRoomMsgBean.type = LiveRoomMsgBean.TYPE_CONNECT_INTERRUPTED;
        observer.addMsg(liveRoomMsgBean);
        observer.sendEmptyMessage(UI_EVENT_REFRESH_MSGS);
    }

    private void postSortMicType(boolean isWaitMic) {
        LiveRoomMsgBean liveRoomMsgBean = new LiveRoomMsgBean();

        liveRoomMsgBean.type = LiveRoomMsgBean.TYPE_MIC_SORT;
        if (isWaitMic) {
            liveRoomMsgBean.content = TheLApp.getContext().getString(R.string.msg_type_sort);
        } else {
            liveRoomMsgBean.content = TheLApp.getContext().getString(R.string.msg_type_freedom);
        }
        observer.addMsg(liveRoomMsgBean);
        observer.sendEmptyMessage(UI_EVENT_REFRESH_MSGS);
    }

    @Override
    public void connectMic(String method, String toUserId, float x, float y, float height,
                           float width, String nickName, String avatar, boolean dailyGuardduo) {

    }

    @Override
    public void rejectConnectMic(String method, String toUserId, String result, float x,
                                 float y, float height, float width) {

        try {
            if (iLiveChat != null) {

                LinkMicRequestBean.ResponseConnectBean responseConnectBean = new LinkMicRequestBean.ResponseConnectBean(method, toUserId, result, x, y, height, width);

                String body = GsonUtils.createJsonString(responseConnectBean);

                L.d(TAG, " Response body : " + body);

                iLiveChat.sendMsg("linkmic", body, new OnRelaMessageRequestListener() {
                    @Override
                    public void onMessageError(@Nullable Throwable error) {

                    }

                    @Override
                    public void onMessageSuccess(@NotNull String code, @NotNull String payload) {

                        L.d(TAG, " Response body onMessageSuccess code : " + code);

                        L.d(TAG, " Response body onMessageSuccess payload : " + payload);

                        if (code.equals("OK")) {

                            final Message msg = Message.obtain();
                            msg.what = UI_EVENT_LINK_MIC_IS_ACCEPT;
                            msg.obj = result;
                            Bundle bundle = new Bundle();
                            bundle.putString("payload", payload);
                            msg.setData(bundle);
                            observer.sendMessage(msg);
                        }
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void cancelConnectMic(String method, String toUserId) {
        try {

            if (iLiveChat != null) {

                LinkMicRequestBean.CancelBean cancelBean = new LinkMicRequestBean.CancelBean(method, toUserId);

                String payload = GsonUtils.createJsonString(cancelBean);


                iLiveChat.sendMsg("linkmic", payload, new OnRelaMessageRequestListener() {
                    @Override
                    public void onMessageError(@Nullable Throwable error) {
                        L.d(TAG, " error message : " + error.getMessage());

                    }

                    @Override
                    public void onMessageSuccess(@NotNull String code, @NotNull String payload) {
                        L.d(TAG, " onMessageSuccess getCode : " + code);

                        L.d(TAG, " onMessageSuccess getPayload : " + payload);
                    }
                });

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void hangupConnectMic(String method, String toUserId) {
        try {

            if (iLiveChat != null) {

                LinkMicRequestBean.HangupBean hangupBean = new LinkMicRequestBean.HangupBean(method, toUserId);

                String payload = GsonUtils.createJsonString(hangupBean);

                iLiveChat.sendMsg("linkmic", payload, new OnRelaMessageRequestListener() {
                    @Override
                    public void onMessageError(@Nullable Throwable error) {
                        L.d(TAG, " Hangup error message : " + error.getMessage());

                    }

                    @Override
                    public void onMessageSuccess(@NotNull String code, @NotNull String payload) {
                        L.d(TAG, " Hangup getCode : " + code);

                        L.d(TAG, " Hangup getPayload : " + payload);
                    }
                });

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void responseAudienceLinkMic(String method, String body) {
        try {
            if (iLiveChat != null) {

                iLiveChat.sendMsg(method, body, new OnRelaMessageRequestListener() {
                    @Override
                    public void onMessageError(@Nullable Throwable error) {
                        L.d(TAG, "onError");

                    }

                    @Override
                    public void onMessageSuccess(@NotNull String code, @NotNull String payload) {
                        L.d(TAG, " onMessageSuccess payload: " + payload);
                        if ("OK".equals(code)) {
                            L.d(TAG, "responseAudienceLinkMic success");
                        }
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 上报服务端礼物播放了的消息
     */
    class playGiftRunnable implements Runnable {

        private String userId;
        private String toUserid;
        private int giftid;

        public void setPlayGiftMsg(String userId, String toUserid, int id) {
            this.userId = userId;
            this.toUserid = toUserid;
            this.giftid = id;
        }

        @Override
        public void run() {

            try {

                if (iLiveChat != null) {
                    String brocaster_id = ShareFileUtils.getString(ShareFileUtils.Brocaster_ID, "");

                    String body = new JSONObject().put("userId", userId).put("toUserId", brocaster_id).put("giftId", giftid).put("action", "play").toString();

                    iLiveChat.sendMsg("gift_report", body, new OnRelaMessageRequestListener() {
                        @Override
                        public void onMessageError(@Nullable Throwable error) {

                        }

                        @Override
                        public void onMessageSuccess(@NotNull String code, @NotNull String payload) {
                            L.d("LiveWatchMsgClientCtrl", " PlayGiftRunnable onResponse getPayload : " + payload);

                            L.d("LiveWatchMsgClientCtrl", " PlayGiftRunnable onResponse getCode : " + code);
                        }
                    });

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /***************************************************一下为直播PK消息***************************************************/
    private void initPkBusyMsg(@NotNull String code, @NotNull String payload) {
        if (!TextUtils.isEmpty(payload)) {
            try {
                final JSONObject object = new JSONObject(payload);
                final String status = object.optString("status", "");
                if (status.equals("pk_busy") || status.equals("pk_summary")) {//假设PK中断断线重连回来
                    final LivePkInitBean livePkInitBean = GsonUtils.getObject(payload, LivePkInitBean.class);
                    final Message message = Message.obtain();
                    message.what = UI_EVENT_PK_INIT_MSG;
                    message.obj = livePkInitBean;
                    observer.sendMessage(message);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 收到有关Pk的消息
     * 属于广播通知收到的消息
     *
     * @param payload
     */
    private void receiverPkNoticeMsg(String payload) {
        if (TextUtils.isEmpty(payload)) {
            return;
        }
        try {
            final JSONObject obj = new JSONObject(payload);
            final String method = obj.optString("method");
            switch (method) {
                case LiveRoomMsgBean.PK_NOTICE_METHOD_START:
                    receiverPkStartMsg(payload);
                    break;
                case LiveRoomMsgBean.PK_NOTICE_METHOD_SUMMARY:
                    receivePkSummaryMsg(payload);
                    break;
                case LiveRoomMsgBean.PK_NOTICE_METHOD_STOP:
                    receivePkStopMsg(payload);
                    break;
                case LiveRoomMsgBean.PK_NOTICE_METHOD_PKGEM:
                    receiverPkGiftMsg(payload);
                    break;
                case LiveRoomMsgBean.PK_NOTICE_METHOD_HANGUP:
                    receiveHangupMsg(payload);
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * 被挂断方收到服务器的PK挂断消息
     *
     * @param payload
     */
    @Override
    public void receiveHangupMsg(String payload) {
        if (TextUtils.isEmpty(payload)) {
            return;
        }
        final LivePkHangupBean bean = GsonUtils.getObject(payload, LivePkHangupBean.class);
        final Message msg = Message.obtain();
        msg.what = UI_EVENT_PK_HANGUP_NOTICE;
        msg.obj = bean;
        observer.sendMessage(msg);
    }

    /**
     * PK开始时 服务器发送PK开始通知给双方直播室内的所有人
     *
     * @param payload
     */
    @Override
    public void receiverPkStartMsg(String payload) {
        if (TextUtils.isEmpty(payload)) {
            return;
        }
        final LivePkStartNoticeBean bean = GsonUtils.getObject(payload, LivePkStartNoticeBean.class);
        final Message msg = Message.obtain();
        msg.what = UI_EVENT_PK_START_NOTICE;
        msg.obj = bean;
        observer.sendMessage(msg);
    }

    /**
     * PK结束时 服务器发送 PK结束 和 双方助攻前三名 通知给双方直播室内的所有人
     *
     * @param payload
     */
    @Override
    public void receivePkStopMsg(String payload) {
        if (TextUtils.isEmpty(payload)) {
            return;
        }
        final Message msg = Message.obtain();
        msg.what = UI_EVENT_PK_STOP_NOTICE;
        msg.obj = payload;
        observer.sendMessage(msg);
    }

    /**
     * 主播收到礼物时 服务器发送主播软妹币增量通知给双方直播室内的所有人:
     *
     * @param payload
     */
    @Override
    public void receiverPkGiftMsg(String payload) {
        if (TextUtils.isEmpty(payload)) {
            return;
        }
        final LivePkGemNoticeBean bean = GsonUtils.getObject(payload, LivePkGemNoticeBean.class);
        final Message msg = Message.obtain();
        msg.what = UI_EVENT_PK_GEM_NOTICE;
        msg.obj = bean;
        observer.sendMessage(msg);
    }

    /**
     * 进入总结阶段 服务器发送 总结剩余时间 和 双方助攻前三名 给双方直播室内的所有人:
     *
     * @param payload
     */
    @Override
    public void receivePkSummaryMsg(String payload) {
        if (TextUtils.isEmpty(payload)) {
            return;
        }
        final LivePkSummaryNoticeBean bean = GsonUtils.getObject(payload, LivePkSummaryNoticeBean.class);
        final Message msg = Message.obtain();
        msg.what = UI_EVENT_PK_SUMMARY_NOTICE;
        msg.obj = bean;
        observer.sendMessage(msg);
    }

    /***************************************************一上为直播PK消息***************************************************/


    private void receiverLinkMicNoticeMsg(String payload) {

        L.d(TAG, " receiverLinkMicNoticeMsg payload : " + payload);

        if (TextUtils.isEmpty(payload)) {
            return;
        }

        final JSONObject obj;
        try {
            obj = new JSONObject(payload);

            final String method = obj.optString("method");

            if (method.equals("stop")) {
                sendLinkMicStopMsg();
            } else if (method.equals("start")) {
                final LinkMicResponseBean linkMicResponseBean = GsonUtils.getObject(payload, LinkMicResponseBean.class);
                linkMicResponseBean.isAudienceLinkMic = linkMicResponseBean.userId.equals(UserUtils.getMyUserId());
                sendLinkMicBusyMsg(linkMicResponseBean);
            } else if (method.equals("guestResponse")) {
                final AudienceLinkMicResponseBean audienceLinkMicResponseBean = GsonUtils.getObject(payload, AudienceLinkMicResponseBean.class);
                sendAudienceLinkMicResponse(audienceLinkMicResponseBean);
            } else if (method.equals("request")) {//主播请求连麦

                L.d(TAG, " 主播请求连麦 ");
                final LiveRoomMsgConnectMicBean liveRoomMsgBean = GsonUtils.getObject(payload, LiveRoomMsgConnectMicBean.class);
                final Message msg = Message.obtain();
                msg.what = UI_EVENT_DAILY_LINK_MIC;
                msg.obj = liveRoomMsgBean;
                observer.sendMessage(msg);
            } else if (method.equals("hangup")) {
                LiveRoomMsgConnectMicBean liveRoomMsgConnectMicBean = GsonUtils.getObject(payload, LiveRoomMsgConnectMicBean.class);
                Message message = observer.obtainMessage();
                message.obj = liveRoomMsgConnectMicBean;
                message.what = UI_EVENT_LINK_MIC_HANGUP;
                observer.sendMessage(message);
            } else if (method.equals("cancel")) {
                observer.sendEmptyMessage(UI_EVENT_LINK_MIC_CANCEL);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void initLinkMicMsg(@NotNull String code, @NotNull String payload) {
        L.d(TAG, " initLinkMicMsg payload : " + payload);
        if (TextUtils.isEmpty(payload)) {
            return;
        }

        LinkMicResponseBean linkMicResponseBean = GsonUtils.getObject(payload, LinkMicResponseBean.class);

        if (linkMicResponseBean != null && linkMicResponseBean.status != null && linkMicResponseBean.status.equals("linkmic_busy")) {
            linkMicResponseBean.isAudienceLinkMic = false;
            sendLinkMicBusyMsg(linkMicResponseBean);
        }

    }

    private void sendLinkMicBusyMsg(LinkMicResponseBean linkMicResponseBean) {
        final Message msg = Message.obtain();
        msg.what = UI_EVENT_LINK_MIC_BUSY;
        msg.obj = linkMicResponseBean;
        observer.sendMessage(msg);
    }

    private void sendLinkMicStopMsg() {
        final Message msg = Message.obtain();
        msg.what = UI_EVENT_LINK_MIC_STOP;
        observer.sendMessage(msg);
    }

    private void sendAudienceLinkMicResponse(AudienceLinkMicResponseBean
                                                     audienceLinkMicResponseBean) {
        final Message msg = Message.obtain();
        msg.what = UI_EVENT_AUDIENCE_LINK_MIC_RESPONSE;
        msg.obj = audienceLinkMicResponseBean;
        observer.sendMessage(msg);
    }

    /******************************************多人连麦start*************************************************/

    /**
     * 多人连麦进直播间初始化
     */
    private void initMultiMicMsg(@NotNull String code, @NotNull String payload) {
        if (TextUtils.isEmpty(payload)) {
            return;
        }
        LiveMultiOnCreateBean liveMultiInitBean = GsonUtils.getObject(payload, LiveMultiOnCreateBean.class);
        L.d("LiveWatchMsgClientCtrl", "code: " + code);
        L.d("LiveWatchMsgClientCtrl", "liveMultiInitBean: " + liveMultiInitBean);
        if (liveMultiInitBean != null && liveMultiInitBean.seats != null && liveMultiInitBean.status != null && liveMultiInitBean.status.equals("audiobroadcast")) {
            final Message msg = Message.obtain();
            msg.what = UI_EVENT_INIT_MULTI;
            msg.obj = liveMultiInitBean;
            observer.sendMessage(msg);
            postSortMicType(liveMultiInitBean.isWaitMic);
        }
    }

    @Override
    public void onSeat(final int seatNum) {

        try {
            if (iLiveChat != null) {

                iLiveChat.sendMsg("audiobroadcast", new JSONObject().put("method", LiveRoomMsgBean.TYPE_METHOD_ONSEAT).put("seatNum", seatNum).toString(), new OnRelaMessageRequestListener() {
                    @Override
                    public void onMessageError(@Nullable Throwable error) {

                    }

                    @Override
                    public void onMessageSuccess(@NotNull String code, @NotNull String payload) {
                        if ("OK".equals(code)) {

                            AgoraBean agoraBean = GsonUtils.getObject(payload, AgoraBean.class);

                            Message msg = Message.obtain();

                            msg.obj = agoraBean;

                            msg.what = UI_EVENT_GUEST_ON_SEAT;

                            observer.sendMessage(msg);

                            L.d(TAG, "onSeat success");
                            //                                    observer.sendEmptyMessage(UI_EVENT_REFRESH_SEAT);
                        } else if ("onseat_auth".equals(code)) {//上麦需要身份验证
                            Message msg = Message.obtain();

                            msg.what = UI_EVENT_GUEST_ONSEAR_AUTH;

                            observer.sendMessage(msg);
                        }
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void offSeat(final int seatNum) {
        try {
            if (iLiveChat != null) {
                iLiveChat.sendMsg("audiobroadcast", new JSONObject().put("method", LiveRoomMsgBean.TYPE_METHOD_OFFSEAT).put("seatNum", seatNum).toString(), new OnRelaMessageRequestListener() {
                    @Override
                    public void onMessageError(@Nullable Throwable error) {

                    }

                    @Override
                    public void onMessageSuccess(@NotNull String code, @NotNull String payload) {
                        if ("OK".equals(code)) {

                            Message msg = Message.obtain();

                            msg.what = UI_EVENT_GUEST_OFF_SEAT;

                            observer.sendMessage(msg);

                            L.d(TAG, "onSeat success");
                            //                                    observer.sendEmptyMessage(UI_EVENT_REFRESH_SEAT);
                        }
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void mute(final int seatNum, final String micStatus) {

        try {
            if (iLiveChat != null) {
                iLiveChat.sendMsg("audiobroadcast", new JSONObject().put("method", LiveRoomMsgBean.TYPE_METHOD_MUTE).put("micStatus", micStatus).put("seatNum", seatNum).toString(), new OnRelaMessageRequestListener() {
                    @Override
                    public void onMessageError(@Nullable Throwable error) {

                    }

                    @Override
                    public void onMessageSuccess(@NotNull String code, @NotNull String payload) {
                        L.d(TAG, "payload: " + payload);
                        if ("OK".equals(code)) {
                            L.d(TAG, "onSeat success");
                            //                                    observer.sendEmptyMessage(UI_EVENT_REFRESH_SEAT);
                        }
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void guestGift(final int giftId, final int combo, final int seatNum) {

        try {
            if (iLiveChat != null) {

                String body = new JSONObject().put("method", LiveRoomMsgBean.TYPE_METHOD_GUEST_GIFT).put("id", giftId).put("combo", combo).put("seatNum", seatNum).toString();

                L.d(TAG, " guestGift body : " + body);

                iLiveChat.sendMsg("audiobroadcast", body, new OnRelaMessageRequestListener() {

                    @Override
                    public void onMessageSuccess(@NotNull String code, @NotNull String payload) {
                        L.d(TAG, " guestGift payload: " + payload);

                        L.d(TAG, " guestGift code: " + code);

                        if ("OK".equals(code)) {
                            L.d(TAG, "guestGift success");
                            String myUserid = ShareFileUtils.getString(ShareFileUtils.ID, "");
                            String brocaster_id = ShareFileUtils.getString(ShareFileUtils.Brocaster_ID, "");

                            Message msg = Message.obtain();
                            msg.what = UI_EVENT_GIFT_SEND_RESULT_MSG;
                            msg.obj = payload;
                            observer.sendMessage(msg);

                            try {

                                if (iLiveChat != null) {

                                    String body = new JSONObject().put("userId", myUserid).put("toUserId", brocaster_id).put("giftId", giftId).put("action", "receipt").toString();

                                    iLiveChat.sendMsg("gift_report", body, new OnRelaMessageRequestListener() {

                                        @Override
                                        public void onMessageSuccess(@NotNull String code, @NotNull String payload) {

                                        }

                                        @Override
                                        public void onMessageError(@Nullable Throwable error) {

                                        }
                                    });
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onMessageError(@Nullable Throwable error) {

                    }
                });

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void startEncounter() {

    }

    @Override
    public void endEncounter() {

    }

    @Override
    public void encounterSb(final int seatNum) {

        try {
            if (iLiveChat != null) {

                String body = new JSONObject().put("method", LiveRoomMsgBean.TYPE_METHOD_ENCOUNTERSB).put("seatNum", seatNum).toString();

                iLiveChat.sendMsg("audiobroadcast", body, new OnRelaMessageRequestListener() {
                    @Override
                    public void onMessageError(@Nullable Throwable error) {
                        L.d(TAG, "onError");

                    }

                    @Override
                    public void onMessageSuccess(@NotNull String code, @NotNull String payload) {
                        L.d(TAG, "payload: " + payload);
                        if ("OK".equals(code)) {
                            L.d(TAG, "encounterSb success");
                            //                                    observer.sendEmptyMessage(UI_EVENT_REFRESH_SEAT);
                        }
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void uploadAudioVolumn(IRtcEngineEventHandler.AudioVolumeInfo[] speakers) {

    }

    @Override
    public void muteAnchor(boolean isMute) {

    }

    @Override
    public void requestSortMic() {

        try {
            if (iLiveChat != null) {

                String body = new JSONObject().put("method", LiveRoomMsgBean.TYPE_METHOD_MIC_REQ).toString();

                iLiveChat.sendMsg("audiobroadcast", body, new OnRelaMessageRequestListener() {
                    @Override
                    public void onMessageError(@Nullable Throwable error) {

                    }

                    @Override
                    public void onMessageSuccess(@NotNull String code, @NotNull String payload) {
                        L.d(TAG, "payload: " + payload);
                        if ("OK".equals(code)) {
                            L.d(TAG, "requestSortMic success");
                            //                                    observer.sendEmptyMessage(UI_EVENT_REFRESH_SEAT);
                        } else if ("onseat_auth".equals(code)) {//上麦需要身份验证
                            Message msg = Message.obtain();

                            msg.what = UI_EVENT_GUEST_ONSEAR_AUTH;

                            observer.sendMessage(msg);
                        }
                    }
                });

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void cancelSortMic() {

        try {
            if (iLiveChat != null) {

                String body = new JSONObject().put("method", LiveRoomMsgBean.TYPE_METHOD_MIC_CANCEL).toString();

                iLiveChat.sendMsg("audiobroadcast", body, new OnRelaMessageRequestListener() {
                    @Override
                    public void onMessageError(@Nullable Throwable error) {

                    }

                    @Override
                    public void onMessageSuccess(@NotNull String code, @NotNull String payload) {
                        L.d(TAG, "payload: " + payload);
                        if ("OK".equals(code)) {
                            L.d(TAG, "cancelSortMic success");
                            //                                    observer.sendEmptyMessage(UI_EVENT_REFRESH_SEAT);
                        }
                    }
                });

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void requestMicList() {

        try {
            if (iLiveChat != null) {

                String body = new JSONObject().put("method", LiveRoomMsgBean.TYPE_METHOD_MIC_LIST).toString();

                iLiveChat.sendMsg("audiobroadcast", body, new OnRelaMessageRequestListener() {
                    @Override public void onMessageSuccess(@NotNull String code, @NotNull String payload) {
                        if ("OK".equals(code)) {
                            L.d(TAG, "getMicSortList success");
                            LiveMultiSeatBean seat = GsonUtils.getObject(payload, LiveMultiSeatBean.class);
                            Message msg = Message.obtain();
                            msg.obj = seat;
                            msg.what = UI_EVENT_MIC_LIST;
                            observer.sendMessage(msg);
                        }
                    }

                    @Override public void onMessageError(@Nullable Throwable error) {

                    }

                });

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getMicSortList() {

    }

    /******************************************多人连麦end*************************************************/


    @Override
    public void requestLinkMicByAudience() {

        if (!NetworkUtils.isNetworkConnected(TheLApp.context)) {
            return;
        }

        try {
            if (iLiveChat != null) {

                String body = "{\n" + "\"method\":\"guestRequest\"\n" + "}";

                L.d(TAG, " body : " + body);

                iLiveChat.sendMsg(LiveRoomMsgBean.TYPE_CONNECT_MIC, body, new OnRelaMessageRequestListener() {
                    @Override
                    public void onMessageSuccess(@NotNull String code, @NotNull String payload) {
                        L.d(TAG, "payload: " + payload);
                        L.d(TAG, "code: " + code);

                        if ("OK".equals(code)) {
                            L.d(TAG, "getMicSortList success");
                            LiveMultiSeatBean seat = GsonUtils.getObject(payload, LiveMultiSeatBean.class);
                            Message msg = Message.obtain();
                            msg.obj = seat;
                            msg.what = UI_EVENT_MIC_LIST;
                            observer.sendMessage(msg);
                        }

                        if ("no_permission".equals(code)) {
                            Message msg = Message.obtain();
                            msg.what = UI_EVENT_AUDIENCE_BEAM_AVAILABLE_LEVEL_SIX;
                            observer.sendMessage(msg);
                        }
                    }

                    @Override
                    public void onMessageError(@Nullable Throwable error) {

                    }

                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void cancelLinkMIcByAudience() {

        if (!NetworkUtils.isNetworkConnected(TheLApp.context)) {
            return;
        }

        try {
            if (iLiveChat != null) {

                String body = "{\n" + "\"toUserId\":\"" + UserUtils.getMyUserId() + "\",\n" + "\"method\":\"guestCancel\"\n" + "}";

                L.d(TAG, " body : " + body);

                iLiveChat.sendMsg(LiveRoomMsgBean.TYPE_CONNECT_MIC, body, new OnRelaMessageRequestListener() {
                    @Override
                    public void onMessageError(@Nullable Throwable error) {

                    }

                    @Override
                    public void onMessageSuccess(@NotNull String code, @NotNull String payload) {
                        L.d(TAG, "payload: " + payload);
                        L.d(TAG, "code: " + code);

                        if ("OK".equals(code)) {
                            L.d(TAG, "cancel linkMic success");

                        }
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void linkMicHangup(final String code, final String userId) {

        if (!NetworkUtils.isNetworkConnected(TheLApp.context)) {
            return;
        }

        try {

            if (iLiveChat != null) {

                LinkMicRequestBean.HangupBean hangupBean = new LinkMicRequestBean.HangupBean(code, userId);

                String body = GsonUtils.createJsonString(hangupBean);


                iLiveChat.sendMsg("linkmic", body, new OnRelaMessageRequestListener() {
                    @Override
                    public void onMessageError(@Nullable Throwable error) {
                        L.d(TAG, " Hangup error message : " + error.getMessage());

                    }

                    @Override
                    public void onMessageSuccess(@NotNull String code, @NotNull String payload) {

                        L.d(TAG, " Hangup getCode : " + code);

                        L.d(TAG, " Hangup getPayload : " + payload);
                    }
                });

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void requestLinkMicAudienceList() {

        if (!NetworkUtils.isNetworkConnected(TheLApp.context)) {
            return;
        }

        try {

            if (iLiveChat != null) {

                String body = "{\n" + "\"method\":\"guestList\"\n" + "}";

                iLiveChat.sendMsg("linkmic", body, new OnRelaMessageRequestListener() {
                    @Override
                    public void onMessageError(@Nullable Throwable error) {
                        L.d(TAG, " guestList error message : " + error.getMessage());
                    }

                    @Override
                    public void onMessageSuccess(@NotNull String code, @NotNull String payload) {

                        L.d(TAG, " guestList getCode : " + code);

                        L.d(TAG, " guestList getPayload : " + payload);

                        LiveMultiSeatBean seat = GsonUtils.getObject(payload, LiveMultiSeatBean.class);
                        Message msg = Message.obtain();
                        msg.obj = seat;
                        msg.what = UI_EVENT_MIC_LIST;
                        observer.sendMessage(msg);
                    }
                });
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void guestLeaveRoom() {

        L.d(TAG, " guestLeaveRoom iLiveChat : " + iLiveChat);
        try {

            if (iLiveChat != null) {

                iLiveChat.sendMsg("leave", "", new OnRelaMessageRequestListener() {
                    @Override
                    public void onMessageError(@Nullable Throwable error) {

                    }

                    @Override
                    public void onMessageSuccess(@NotNull String code, @NotNull String payload) {

                    }
                });
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void arGiftReceipt(String payload) {

    }

    @Override
    public void getTopFansTodayList() {

    }

    private NetworkReceiver.NetworkListener mNetworkListener = new NetworkReceiver.NetworkListener() {
        @Override
        public void onNetworkEnable(boolean isNetworkEnable) {

            L.d(TAG, " isNetworkEnable : " + isNetworkEnable);

            if (isNetworkEnable) {

                disconnect();

//                pushData();

                tryCreateClient();

            }
        }
    };

    private void pushData() {
        //  Android  连接失败
        UmentPushUtils.onMsgEvent(TheLApp.context, "live_msg_fail", "live_msg_connect_fail_android");

        LiveInfoLogBean.getInstance().getLiveChatAnalytics().errorReason = "connect_fail";

        pushErrorLog();

        changeIP();
    }

    private void changeIP() {

        if (hosts == null || hosts.size() == 0) {
            if (liveRoomBean != null && liveRoomBean.livechat != null) {
                host = liveRoomBean.livechat.host;
                port = liveRoomBean.livechat.port;
            }
        } else {
            if (hosts.size() <= ipIndex) {
                ipIndex = 0;
            }
            LiveChatBean.HostBean hostBean = hosts.get(ipIndex);
            host = hostBean.host;
            port = hostBean.port;
            ipIndex++;
        }
    }

    private void pushErrorLog() {
        LiveInfoLogBean.getInstance().getLiveChatAnalytics().role = "audience";
        LiveInfoLogBean.getInstance().getLiveChatAnalytics().cdnDomain = host;
        LiveInfoLogBean.getInstance().getLiveChatAnalytics().logType = TheLConstants.LiveInfoLogConstants.LIVE_MSG_ERROR_LOG;
        LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getLiveChatAnalytics());
    }

    private void initPing() {

        L.i(TAG, " initPing : ");

        if (compositeDisposable != null) {
            compositeDisposable.clear();
        }
        Disposable disposable = Flowable.interval(10, TimeUnit.SECONDS).onBackpressureDrop().subscribe(new Consumer<Long>() {
            @Override
            public void accept(Long aLong) {

                L.i(TAG, " ------------ping--------- " + iLiveChat);

                if (iLiveChat != null) {

                    LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().log = "status:StartPing";

                    LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics());
                    try {

                        iLiveChat.sendMsg("ping", "", new OnRelaMessageRequestListener() {
                            @Override
                            public void onMessageError(@Nullable Throwable error) {

                                L.i(TAG, " Ping onMessageError : " + error.getMessage());

                                UmentPushUtils.onMsgEvent(TheLApp.context, "live_msg_ping_timeout_android");

                                LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().log = "status:PingTimeOut";

                                LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics());
                            }

                            @Override
                            public void onMessageSuccess(@NotNull String code, @NotNull String payload) {
                                LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().log = "status:PingResult code:" + code;

                                LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics());

                                L.i(TAG, " Ping onResponse : " + code);

                                if (!"pong".equals(code)) {


                                    LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().log = "status:PingFail";

                                    LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics());

                                    // ping不通，重连
                                    UmentPushUtils.onMsgEvent(TheLApp.context, "live_msg_ping_ip_fail_android"); //  Android  ping IP失败

                                    UmentPushUtils.onMsgEvent(TheLApp.context, "live_msg_ping_fail_android");
                                } else {
                                    LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().log = "status:PingSuccess";

                                    LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics());
                                }
                            }
                        });

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        });

        compositeDisposable.add(disposable);
    }

    @Override
    public void acceptFreeGold() {
        try {
            if (iLiveChat != null) {

                iLiveChat.sendMsg("agree_free_gold", "", new OnRelaMessageRequestListener() {
                    @Override
                    public void onMessageError(@Nullable Throwable error) {

                    }

                    @Override
                    public void onMessageSuccess(@NotNull String code, @NotNull String payload) {
                        L.d(TAG, " acceptFreeGold getCode : " + code);

                        //{"goldBalance":5}
                        L.d(TAG, " acceptFreeGold getPayload : " + payload);

                        if (code.equals("OK")) {
                            Message msg = Message.obtain();
                            msg.what = UI_EVENT_GIFT_SEND_RESULT_MSG;
                            msg.obj = payload;
                            observer.sendMessage(msg);
                        }
                    }
                });
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void rejectFreeGold() {
        try {
            if (iLiveChat != null) {

                iLiveChat.sendMsg("reject_free_gold", "", new OnRelaMessageRequestListener() {
                    @Override
                    public void onMessageError(@Nullable Throwable error) {

                    }

                    @Override
                    public void onMessageSuccess(@NotNull String code, @NotNull String payload) {

                    }
                });
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getToken() {
        return token;
    }

    @Override public void showCloseDialog() {

    }

    private Map<String, String> getJoinChatInfo() {

        Map<String, String> data = new HashMap<>();

        data.put("os", "Android");
        data.put("appver", DeviceUtils.getVersionCode(TheLApp.getContext()) + "");
        data.put("deviceId", UserUtils.getDiviceId());
        data.put("lang", DeviceUtils.getLanguage());
        data.put("channel", liveRoomBean.id);
        data.put("token", liveRoomBean.livechat.token);
        data.put("userId", UserUtils.getMyUserId());
        data.put("nickName", UserUtils.getNickName());
        data.put("avatar", UserUtils.getUserAvatar());

        return data;

    }

    private void createTencentChatRoom() {

        L.d(TAG, " createTencentChatRoom ");

        DefaultRequestService
                .createChatRequestService()
                .createChat(MD5Utils.generateSignatureForMap(getJoinChatInfo()))
                .onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new InterceptorSubscribe<LiveChatCreateResBean>() {
                    @Override
                    public void onNext(LiveChatCreateResBean data) {
                        super.onNext(data);

                        L.d(TAG, "创建直播间成功" + data.toString());

                        postConnectSucceed();

                        if (observer != null) {
                            try {
                                observer.addMsg(data.getData().getChannelInfo().getExtra_msg());
                                observer.sendEmptyMessage(UI_EVENT_REFRESH_MSGS);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        iLiveChat.joinChatRoom();

                    }

                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                        L.d(TAG, "创建直播间失败" + t.getMessage());

                    }
                });

    }

    private void joinChatRoom() {

        L.d(TAG, " joinTencentChatRoom ");

        DefaultRequestService
                .createChatRequestService()
                .joinChat(MD5Utils.generateSignatureForMap(getJoinChatInfo()))
                .onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new InterceptorSubscribe<LiveChatCreateResBean>() {
                    @Override
                    public void onNext(LiveChatCreateResBean data) {
                        super.onNext(data);

                        L.d(TAG, "加入直播间成功");

                        if (observer != null) {
                            try {
                                observer.addMsg(data.getData().getChannelInfo().getExtra_msg());
                                observer.sendEmptyMessage(UI_EVENT_REFRESH_MSGS);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                        L.d(TAG, "加入直播间失败" + t.getMessage());

                    }
                });
    }

    private void destroyChatRoom() {

        L.d(TAG, " destroyTencentChatRoom ");

        Map<String, String> data = new HashMap<>();
        data.put("token", liveRoomBean.livechat.token);
        data.put("userId", UserUtils.getMyUserId());

        DefaultRequestService
                .createChatRequestService()
                .destroyChat(MD5Utils.generateSignatureForMap(data))
                .onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new InterceptorSubscribe<BasicInfoBean>() {
                    @Override
                    public void onNext(BasicInfoBean data) {
                        super.onNext(data);

                        L.d(TAG, "解散直播间成功");

                    }

                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                        L.d(TAG, "解散直播间失败" + t.getMessage());

                    }
                });

    }

    private void leaveChatRoom() {

        L.d(TAG, " leaveTencentChatRoom ");

        Map<String, String> data = new HashMap<>();
        data.put("token", liveRoomBean.livechat.token);
        data.put("userId", UserUtils.getMyUserId());

        DefaultRequestService
                .createChatRequestService()
                .leaveChat(MD5Utils.generateSignatureForMap(data))
                .onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new InterceptorSubscribe<BasicInfoBean>() {
                    @Override public void onNext(BasicInfoBean data) {
                        super.onNext(data);

                        L.d(TAG, "离开直播间成功");

                    }

                    @Override public void onError(Throwable t) {
                        super.onError(t);
                        L.d(TAG, "离开直播间失败" + t.getMessage());

                    }
                });
    }

    private void sendGiftByApi(Map<String, String> data) {

        L.d(TAG, " sendGiftByApi " + data.toString());

        DefaultRequestService
                .createChatRequestService()
                .sendGift(MD5Utils.generateSignatureForMap(data))
                .onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new InterceptorSubscribe<BasicInfoBean>() {
                    @Override public void onNext(BasicInfoBean data) {
                        super.onNext(data);
                        L.d(TAG, "赠送礼物成功");
                    }

                    @Override public void onError(Throwable t) {
                        super.onError(t);
                        L.d(TAG, "赠送礼物失败" + t.getMessage());
                    }
                });
    }

}
