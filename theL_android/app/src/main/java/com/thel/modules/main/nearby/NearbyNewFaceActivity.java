package com.thel.modules.main.nearby;

import android.content.Intent;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.growingio.android.sdk.collection.GrowingIO;
import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.bean.user.NearUserBean;
import com.thel.bean.user.NearUserListNetBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.modules.main.nearby.adapter.NearbyUserRecyclerViewAdapter;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.service.DefaultRequestService;
import com.thel.ui.widget.DefaultEmptyView;
import com.thel.ui.widget.MySwipeRefreshLayout;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.utils.ViewUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class NearbyNewFaceActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener, BaseRecyclerViewAdapter.RequestLoadMoreListener, BaseRecyclerViewAdapter.ReloadMoreListener, BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener {

    @BindView(R.id.swipe_container)
    MySwipeRefreshLayout swipe_container;

    @BindView(R.id.recyclerview)
    RecyclerView mRecyclerView;

    @BindView(R.id.empty_view)
    DefaultEmptyView empty_view;

    @OnClick(R.id.lin_back)
    void back() {
        finish();
    }

    private NearbyUserRecyclerViewAdapter mAdapter;

    private int cursor = 0;

    private boolean haveNextPage = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nearby_new_face);
        ButterKnife.bind(this);

        ViewUtils.initSwipeRefreshLayout(swipe_container);
        GridLayoutManager manager = new GridLayoutManager(this, 3);
        mRecyclerView.setLayoutManager(manager);
        mAdapter = new NearbyUserRecyclerViewAdapter(null, false);
        mRecyclerView.setAdapter(mAdapter);

        swipe_container.setOnRefreshListener(this);
        mAdapter.setOnLoadMoreListener(this);

        mAdapter.setReloadMoreListener(this);
        mAdapter.setOnRecyclerViewItemClickListener(this);

        swipe_container.setRefreshing(true);
        getNearbyPigData(true);
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    @Override
    public void onRefresh() {
        getNearbyPigData(true);
    }

    @Override
    public void onLoadMoreRequested() {
        mRecyclerView.post(new Runnable() {
            @Override
            public void run() {
                if (haveNextPage) {
                    getNearbyPigData(false);
                } else {
                    mAdapter.openLoadMore(0, false);
                    if (mAdapter.getData().size() > 0) {
                        View view = getLayoutInflater().inflate(R.layout.load_more_footer_layout, (ViewGroup) mRecyclerView.getParent(), false);
                        ((TextView) view.findViewById(R.id.text)).setText(getString(R.string.info_no_more));
                        mAdapter.addFooterView(view);
                    }
                }
            }
        });
    }

    @Override
    public void reloadMore() {
        mRecyclerView.post(new Runnable() {
            @Override
            public void run() {
                mAdapter.removeAllFooterView();
                mAdapter.openLoadMore(true);
                getNearbyPigData(false);
            }
        });
    }

    @Override
    public void onItemClick(View view, int position) {
        gotoUserInfoActivity(position);
    }

    private void gotoUserInfoActivity(int position) {
        try {

            final NearUserBean tempUser = mAdapter.getItem(position);
            if (tempUser.itemType == 1) return;
            int tempUserId = tempUser.userId;
//            Intent intent = new Intent();
//            intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, tempUserId + "");
//            intent.setClass(this, UserInfoActivity.class);
//            startActivity(intent);
            FlutterRouterConfig.Companion.gotoUserInfo(tempUserId+"");
            GrowingIO.getInstance().track("NearbyCell");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void getNearbyPigData(final boolean isRefresh) {
        if (isRefresh) {
            cursor = 0;
            haveNextPage = true;
        }
        final Flowable<NearUserListNetBean> flowable = DefaultRequestService.createNearbyRequestService().getNearbyPigList(cursor, 15);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<NearUserListNetBean>() {
            @Override
            public void onNext(NearUserListNetBean momentsListBean) {
                super.onNext(momentsListBean);
                swipe_container.setRefreshing(false);
                if (momentsListBean != null && momentsListBean.data != null) {
                    cursor = momentsListBean.data.cursor;
                    if (isRefresh && momentsListBean.data.map_list == null || momentsListBean.data.map_list.size() == 0) {
                        empty_view.setVisibility(View.VISIBLE);
                    } else {
                        empty_view.setVisibility(View.GONE);
                    }
                }
                if (momentsListBean != null && momentsListBean.data != null && momentsListBean.data.map_list != null) {
                    momentsListBean.data.filterBlock();
                    if (isRefresh) {
                        mAdapter.setNewData(momentsListBean.data.map_list);
                    } else {
                        if (cursor == 0) haveNextPage = false;
                        mAdapter.addData(momentsListBean.data.map_list);
                    }
                }
            }

        });
    }
}
