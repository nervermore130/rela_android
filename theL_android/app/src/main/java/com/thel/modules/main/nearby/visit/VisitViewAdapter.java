package com.thel.modules.main.nearby.visit;

import android.text.TextUtils;
import android.view.View;

import com.thel.R;
import com.thel.bean.WhoSeenMeBean;
import com.thel.constants.TheLConstants;
import com.thel.modules.main.nearby.Utils.NearbyUtils;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;

import java.util.List;

/**
 * Created by waiarl on 2017/10/12.
 */

class VisitViewAdapter extends BaseRecyclerViewAdapter<WhoSeenMeBean> {
    private final boolean whoSeenMe;

    /**
     * @param visitBeanList
     * @param whoSeenMe     谁看过我（右侧显示时间），我看过谁 右侧显示箭头
     */
    public VisitViewAdapter(List<WhoSeenMeBean> visitBeanList, boolean whoSeenMe) {
        super(R.layout.adapter_visit_view, visitBeanList);
        this.whoSeenMe = whoSeenMe;
    }

    @Override
    protected void convert(BaseViewHolder helper, WhoSeenMeBean bean) {
        if (bean.verifyType <= 0) {// 非加V用户，显示两行，第一行为昵称和简介，第二行为在线状态、距离、感情状态
            helper.getView(R.id.line2).setVisibility(View.VISIBLE);
            helper.getView(R.id.img_indentify).setVisibility(View.GONE);
            helper.setText(R.id.txt_desc, bean.intro + "");
            helper.setText(R.id.txt_distance, bean.distance);
        } else {// 加V用户，显示一行，显示昵称、认证图标、认证信息
            helper.getView(R.id.line2).setVisibility(View.VISIBLE);
            helper.setText(R.id.txt_desc, bean.verifyIntro);
            helper.setVisibility(R.id.img_indentify, View.VISIBLE);
            if (bean.verifyType == 1) {// 个人加V
                helper.setImageResource(R.id.img_indentify, R.mipmap.icn_verify_person);
            } else {// 企业加V
                helper.setImageResource(R.id.img_indentify, R.mipmap.icn_verify_enterprise);

            }
        }
        if (bean.level>0){
            helper.getView(R.id.img_vip).setVisibility(View.VISIBLE);

            switch (bean.level) {
                case 1:
                    helper.setImageResource(R.id.img_vip,R.mipmap.icn_vip_1);

                    break;
                case 2:
                    helper.setImageResource(R.id.img_vip,R.mipmap.icn_vip_2);

                    break;
                case 3:
                    helper.setImageResource(R.id.img_vip,R.mipmap.icn_vip_3);

                    break;
                case 4:
                    helper.setImageResource(R.id.img_vip,R.mipmap.icn_vip_4);

                    break;
            }
        }else {
            helper.getView(R.id.img_vip).setVisibility(View.GONE);

        }


        // 角色设定 0=unknow,1=t,2=p,3=h,5=bi
        if (bean.roleName.equals("0")) {
            helper.setImageResource(R.id.img_role, R.mipmap.icn_role_unknow);
        } else if (bean.roleName.equals("1")) {
            helper.setImageResource(R.id.img_role, R.mipmap.icn_role_t);
        } else if (bean.roleName.equals("2")) {
            helper.setImageResource(R.id.img_role, R.mipmap.icn_role_p);
        } else if (bean.roleName.equals("3")) {
            helper.setImageResource(R.id.img_role, R.mipmap.icn_role_h);
        } else if (bean.roleName.equals("5")) {
            helper.setImageResource(R.id.img_role, R.mipmap.icn_role_bi);
        } else if (bean.roleName.equals("6")) {
            helper.setImageResource(R.id.img_role, R.mipmap.icn_role_s);
        } else {
            helper.setImageResource(R.id.img_role, R.mipmap.icn_role_unknow);
        }

        // 头像
        helper.setImageUrl(R.id.img_thumb, bean.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE);
        // 用户名
        helper.setText(R.id.txt_name, bean.nickName);
        final String date = NearbyUtils.getWhoSeenMeDate(bean.create_date);
        if (whoSeenMe && !TextUtils.isEmpty(date)) {
            helper.setVisibility(R.id.txt_date, View.VISIBLE);
            helper.setVisibility(R.id.iv_next, View.GONE);
            helper.setText(R.id.txt_date, date);
        } else {
            helper.setVisibility(R.id.txt_date, View.GONE);
            helper.setVisibility(R.id.iv_next, View.VISIBLE);
        }
    }
}
