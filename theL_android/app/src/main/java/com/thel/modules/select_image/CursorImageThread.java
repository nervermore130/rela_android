package com.thel.modules.select_image;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.constants.TheLConstants;
import com.thel.utils.L;
import com.thel.utils.SharedPrefUtils;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created by chad
 * Time 17/9/28
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */

@TargetApi(16)
public class CursorImageThread extends Thread {

    private static final String TAG = "CursorImageThread";

    private Context context;

    public CursorImageThread(Context context, CursorImageListener cursorImageListener) {
        this.context = context;
        this.cursorImageListener = cursorImageListener;
    }

    public interface CursorImageListener {
        void cursorImageComplete(List<ImageBean> allPhotos, List<ImageFolderBean> mImageFolders);

        void cursorImageFailed();
    }

    private CursorImageListener cursorImageListener;

    @Override
    public void run() {

        String[] whereArgs = {"image/jpeg", "image/png", "image/jpg"};

        String where = MediaStore.Images.Media.MIME_TYPE + "=? or "
                + MediaStore.Images.Media.MIME_TYPE + "=? or "
                + MediaStore.Images.Media.MIME_TYPE + "=?";

        final String[] projectionPhotos = {
                MediaStore.Images.Media._ID
                , MediaStore.Images.Media.BUCKET_ID
                , MediaStore.Images.Media.BUCKET_DISPLAY_NAME
                , MediaStore.Images.Media.DATA
                , MediaStore.Images.Media.DATE_TAKEN
                , MediaStore.Images.Media.ORIENTATION
                , MediaStore.Images.Thumbnails.DATA
                , MediaStore.MediaColumns.WIDTH
                , MediaStore.MediaColumns.HEIGHT};
        // 只查询jpeg和png的图片
        Cursor mCursor = MediaStore.Images.Media.query(
                TheLApp.getContext().getContentResolver()
                , MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                , projectionPhotos
                , where
                , whereArgs
                , MediaStore.Images.Media.DATE_TAKEN + " DESC");

        if (mCursor == null) {
            ((Activity) context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (cursorImageListener != null) cursorImageListener.cursorImageFailed();
                }
            });
            return;
        }

        /**
         * 扫描拿到所有的图片文件夹
         */
        final List<ImageFolderBean> mImageFolders = new ArrayList<>();
        final List<ImageBean> allPhotos = new ArrayList<>();
        HashSet<String> mDirPaths = new HashSet<>();
        // 把图片路径都保存在本地文件里，不用intent传，因为图片太多的时候会闪退
        StringBuilder photoPaths = new StringBuilder();

        L.d(TAG, " mCursor  : " + mCursor.toString());

        while (mCursor.moveToNext()) {
            // 获取图片的路径
            String path = mCursor.getString(mCursor.getColumnIndex(MediaStore.Images.Media.DATA));

            // 排除thle的文件夹
            if (path == null || path.contains(TheLConstants.F_THEL_ROOTPATH) || path.contains(File.separator + ".")) {
                continue;
            }

            // 排除不合法的文件
            String strName = path.toLowerCase();
            if (!strName.endsWith(".jpg") && !strName.endsWith(".png") && !strName.endsWith(".jpeg")) {
                continue;
            }

            //判断文件是否存在
            if (!new File(path).exists()) continue;

            ImageBean imageBean = new ImageBean();

            imageBean.imageName = path;

            imageBean.imageName = path;

            imageBean.width = mCursor.getInt(mCursor.getColumnIndex(MediaStore.MediaColumns.WIDTH));

            imageBean.height = mCursor.getInt(mCursor.getColumnIndex(MediaStore.MediaColumns.HEIGHT));

            allPhotos.add(imageBean);

            photoPaths.append(path).append(",");

            // 获取该图片的父路径名
            File parentFile = new File(path).getParentFile();
            if (parentFile == null) {
                continue;
            }
            String dirPath = parentFile.getAbsolutePath();
            ImageFolderBean imageFolder = null;
            // 利用一个HashSet防止多次扫描同一个文件夹（不加这个判断，图片多起来还是相当恐怖的~~）
            if (mDirPaths.contains(dirPath)) {
                continue;
            } else {
                mDirPaths.add(dirPath);
                // 初始化imageFloder
                imageFolder = new ImageFolderBean();
                imageFolder.setDir(dirPath);
                imageFolder.setFirstImagePath(path);
            }

            String[] str = parentFile.list(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String filename) {
                    String str = filename.toLowerCase();
                    return str.endsWith(".jpg") || str.endsWith(".png") || str.endsWith(".jpeg");
                }
            });

            int picSize = 0;
            if (str != null) {
                picSize = str.length;
            }

            imageFolder.setCount(picSize);
            mImageFolders.add(imageFolder);

        }

        if (photoPaths.length() > 0) {
            photoPaths.deleteCharAt(photoPaths.length() - 1);
            SharedPrefUtils.setStringWithoutEncrypt(SharedPrefUtils.FILE_PHOTO_PATH_CACHE, SharedPrefUtils.PHOTO_PATH_CACHE_ALL, photoPaths.toString());
        }

        // 加上『所有图片』的文件夹
        ImageFolderBean allPhotosDir = new ImageFolderBean();
        if (allPhotos.size() > 0) {
            allPhotosDir.setFirstImagePath(allPhotos.get(0).imageName);
            allPhotosDir.setCount(allPhotos.size());
        } else {
            allPhotosDir.setFirstImagePath("");
            allPhotosDir.setCount(0);
        }
        allPhotosDir.setName(context.getString(R.string.select_local_images_activity_all_photos));
        allPhotosDir.isAllPhotos = true;
        mImageFolders.add(0, allPhotosDir);

        mCursor.close();

        ((Activity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (cursorImageListener != null) {
                    cursorImageListener.cursorImageComplete(allPhotos, mImageFolders);
                }
            }
        });
    }
}
