package com.thel.modules.main.home.moments.theme;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.facebook.CallbackManager;
import com.facebook.drawee.view.SimpleDraweeView;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.base.BaseAdapter;
import com.thel.base.BaseDataBean;
import com.thel.base.IntentFilterActivity;
import com.thel.bean.ThemeCommentListBeanNew;
import com.thel.bean.gift.WinkCommentBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.moments.MomentsBeanNew;
import com.thel.bean.moments.MomentsListBean;
import com.thel.bean.theme.ThemeCommentAddBean;
import com.thel.bean.theme.ThemeCommentBean;
import com.thel.constants.BundleConstants;
import com.thel.constants.MomentTypeConstants;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.growingio.GrowingIoConstant;
import com.thel.imp.black.BlackUsersChangedListener;
import com.thel.imp.black.BlackUtils;
import com.thel.imp.like.MomentLikeStatusChangedListener;
import com.thel.imp.like.MomentLikeUtils;
import com.thel.imp.momentblack.MomentBlackListener;
import com.thel.imp.momentblack.MomentBlackUtils;
import com.thel.imp.momentdelete.MomentDeleteListener;
import com.thel.imp.momentdelete.MomentDeleteUtils;
import com.thel.imp.momentreport.MomentReportListener;
import com.thel.imp.momentreport.MomentReportUtils;
import com.thel.imp.picupload.PicUploadContract;
import com.thel.imp.picupload.PicUploadUtils;
import com.thel.imp.wink.WinkStatusChangedListener;
import com.thel.imp.wink.WinkUtils;
import com.thel.manager.ImageLoaderManager;
import com.thel.manager.ListVideoVisibilityManager;
import com.thel.modules.main.home.moments.ReleaseMomentActivity;
import com.thel.modules.main.home.moments.comment.MomentCommentActivity;
import com.thel.modules.main.me.aboutMe.UpdateUserInfoActivity;
import com.thel.modules.main.me.bean.LogInfoBean;
import com.thel.modules.main.me.bean.MyInfoBean;
import com.thel.modules.main.me.match.eventcollect.collect.LiveLogUtils;
import com.thel.modules.main.messages.utils.PushUtils;
import com.thel.modules.main.userinfo.MyLinearLayoutManager;
import com.thel.modules.preview_image.UserInfoPhotoActivity;
import com.thel.modules.preview_video.VideoPreviewActivity;
import com.thel.modules.select_image.SelectLocalImagesActivity;
import com.thel.modules.welcome.WelcomeActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestConstants;
import com.thel.network.service.DefaultRequestService;
import com.thel.ui.adapter.MomentsAdapter;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;
import com.thel.ui.widget.recyclerview.decoration.DefaultItemDivider;
import com.thel.utils.BusinessUtils;
import com.thel.utils.DialogUtil;
import com.thel.utils.ImageUtils;
import com.thel.utils.L;
import com.thel.utils.MD5Utils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SharedPrefUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.UMShareAPI;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import video.com.relavideolibrary.Utils.Constant;
import video.com.relavideolibrary.onRelaVideoActivityResultListener;

import static android.view.View.VISIBLE;

/**
 * 话题详情评论页面
 *
 * @author Setsail
 */
public class ThemeDetailActivity extends BaseActivity implements MomentBlackListener, MomentDeleteListener, WinkStatusChangedListener,//挤眼
        MomentLikeStatusChangedListener,//点赞
        BlackUsersChangedListener,//黑名单
        MomentReportListener ,
        onRelaVideoActivityResultListener {

    private static final String TAG = "ThemeDetailActivity";

    private ImageView img_share;
    private TextView title_txt;
    private SwipeRefreshLayout swipe_container;
    private LinearLayout default_page;
    private EditText input;
    private TextView submit;
    private ImageView img_select_photo;

    private ThemeCommentAddBean sendMoment;
    private ProgressBar progressbar_video;
    private ProgressBar progressbar_music;
    private MomentBlackUtils momentBlackUtils;
    private WinkUtils winkUtils;
    private BlackUtils blackUtils;
    private MomentDeleteUtils momentDeleteUtils;
    private MomentReportUtils momentReportUtils;
    private MomentLikeUtils momentLikeUtils;
    private TextWatcher textWatcher;
    private String videoMainColor;
    private RecyclerView recyclerView;
    private MomentsAdapter adapter;
    private LinearLayoutManager manager;

    private MomentsBean headMoment;

    private enum themeTypeEnum {
        TEXT, IMAGE, VOICE, VIDEO
    }

    /**
     * 发布图片评论
     */
    private RecyclerView selectedPicsRecyclerView;
    private SelectPicsRecyclerViewAdapter selectPicsRecyclerViewAdapter;
    private List<PicBean> selectedPics = new ArrayList<>();
    // 判断是不是已经选择了一张以上图片，因为点击按钮也是放在recyclerview中的，所以当数据源的count > 1时，才表示已经选择了图片
    private final int HAVE_SELECTED_PIC_COUNT = 1;
    private final int SELECT_PIC_LIMIT = 6;

    public String momentsId;

    private CallbackManager callbackManager;
    private LinearLayout lin_update_user_info;
    //话题回复类型，先默认为文本类型
    private String themeReplyClass = ThemeCommentBean.TYPE_CLASS_TEXT;

    //视频
    private String videoPath;
    private int playTime;
    private int pixelWidth;
    private int pixelHeight;
    private String videoThumnail;

    private LinearLayout lin_bottom;
    private LinearLayout lin_imgs;
    private RelativeLayout rel_video;
    private ImageView img_video;
    private ImageView img_video_play;
    private LinearLayout lin_music;
    private ImageView img_music;
    private ImageView img_music_play;
    private TextView txt_song_name;
    private TextView txt_song_album;
    private TextView txt_song_artist;

    private boolean haveNextPage;
    private int pageSize = 20;
    private int cursor = 0; // 当前数据下标
    private List<MomentsBean> momentsBeanList = new ArrayList<>();
    private String pageId;
    private String from_page_id;
    private String from_page;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_theme_detail);

        /** 分享初始化 **/
        callbackManager = CallbackManager.Factory.create();
        /** 分享初始化 **/

        Intent intent = getIntent();
        pageId = Utils.getPageId();

      // 如果是从外部进入
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            from_page_id = bundle.getString(ShareFileUtils.MATCH_FROM_PAGE_ID, "");
            from_page = bundle.getString(ShareFileUtils.MATCH_FROM_PAGE, "");

        }
        initViews();

        setListeners();


        String action = intent.getAction();
        if (Intent.ACTION_VIEW.equals(action)) {
            if (!ShareFileUtils.getBoolean(ShareFileUtils.HAS_LOGGED, false)) {//如果没登陆
                startActivity(new Intent(this, WelcomeActivity.class));
                finish();
                return;
            }
            Uri uri = intent.getData();
            if (uri != null) {
                momentsId = uri.getQueryParameter("momentId");
            }
        } else {
            if (intent.getSerializableExtra(TheLConstants.BUNDLE_KEY_MOMENT_BEAN) != null) {// 从moments页面进入
                MomentsBean momentBean = (MomentsBean) intent.getSerializableExtra(TheLConstants.BUNDLE_KEY_MOMENT_BEAN);
                momentsId = momentBean.momentsId;
            } else if (intent.getStringExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID) != null) {// 从消息提醒进入
                momentsId = intent.getStringExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID);
            }
            if (intent.getBooleanExtra("showKeyboard", false)) {
                input.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        showKeyboard();
                    }
                }, 150);
            }
        }
        getThemeMomentInfoV2(momentsId);
        initRegster();


    }

    public void traceThemeDetailLog(String page, String activity, String momentsId, String commentType, String text) {
        String latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
        String longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");
        try {
            LogInfoBean logInfoBean = new LogInfoBean();
            logInfoBean.page = page;
            logInfoBean.page_id = pageId;
            logInfoBean.activity = activity;
            logInfoBean.from_page = from_page;
            logInfoBean.from_page_id = from_page_id;
            logInfoBean.lat = latitude;
            logInfoBean.lng = longitude;

            LogInfoBean.LogsDataBean logsDataBean = new LogInfoBean.LogsDataBean();
            logsDataBean.moment_id = momentsId;
            logsDataBean.comment_type = commentType;
            logsDataBean.text = text;
            logInfoBean.data = logsDataBean;

            LiveLogUtils.getInstance().addLog(logInfoBean);

        } catch (Exception e) {

        }
    }

    private void initRegster() {

        momentBlackUtils = new MomentBlackUtils();
        momentBlackUtils.registerReceiver(this);

        winkUtils = new WinkUtils();
        winkUtils.registerReceiver(this);

        momentLikeUtils = new MomentLikeUtils();
        momentLikeUtils.registerReceiver(this);

        blackUtils = new BlackUtils();
        blackUtils.registerReceiver(this);

        momentDeleteUtils = new MomentDeleteUtils();
        momentDeleteUtils.registerReceiver(this);

        momentReportUtils = new MomentReportUtils();
        momentReportUtils.registerReceiver(this);

        adapter.registerNewCommentNotify(this);
    }

    private void getThemeMomentInfoV2(String momentId) {
        Flowable<MomentsBeanNew> flowable = DefaultRequestService.createAllRequestService().getThemeMomentInfo(momentId);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<MomentsBeanNew>() {
            @Override
            public void onNext(MomentsBeanNew result) {
                super.onNext(result);
                if (result != null && result.data != null) {
                    momentsId = result.data.momentsId;
                    headMoment = result.data;
                    momentsBeanList.add(headMoment);
                    if (MomentsBean.MOMENT_TYPE_THEME.equals(result.data.momentsType) || MomentsBean.MOMENT_TYPE_THEME_REPLY.equals(result.data.momentsType) || ShareFileUtils.getString(ShareFileUtils.ID, "").equals(result.data.userId + "")) {
                        img_share.setVisibility(VISIBLE);
                        refreshTitle(result.data.participates);
                        //是否被收藏,如果已经被收藏，则为true,要收藏，否则，要删除
                        boolean isCollect = BusinessUtils.isCollected(result.data.momentsId);

                        if (isCollect) {
                            img_share.setImageResource(R.mipmap.icn_collection_press);

                        } else {
                            img_share.setImageResource(R.mipmap.icn_collection_normal);

                        }
                    }
                }

                loadThemeDetail(cursor);
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(this.getClass().getName());
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(this.getClass().getName());
        MobclickAgent.onPause(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            winkUtils.unRegisterReceiver(this);
            momentBlackUtils.unRegisterReceiver(this);
            momentLikeUtils.unRegisterReceiver(this);
            blackUtils.unRegisterReceiver(this);
            momentDeleteUtils.unRegisterReceiver(this);
            momentReportUtils.unRegisterReceiver(this);
            adapter.unregisterNewCommentNotify(this);
        } catch (Exception e) {
        }
    }

    @Override
    public void onRelaVideoActivityResult(Intent intent) {
        Bundle bundle = intent.getExtras();
        videoPath = bundle.getString(video.com.relavideolibrary.Utils.Constant.BundleConstants.RESULT_VIDEO_PATH);
        playTime = Integer.parseInt(bundle.getString(video.com.relavideolibrary.Utils.Constant.BundleConstants.RESULT_VIDEO_DURATION));
        pixelWidth = Integer.parseInt(bundle.getString(video.com.relavideolibrary.Utils.Constant.BundleConstants.RESULT_VIDEO_WIDTH));
        pixelHeight = Integer.parseInt(bundle.getString(video.com.relavideolibrary.Utils.Constant.BundleConstants.RESULT_VIDEO_HEIGHT));
        videoThumnail = bundle.getString(video.com.relavideolibrary.Utils.Constant.BundleConstants.RESULT_VIDEO_THUMB);
        videoMainColor = bundle.getString(Constant.BundleConstants.RESULT_VIDEO_MAIN_COLOR);
        ImageLoaderManager.imageLoader(img_music, R.drawable.bg_waterfull_item_shape, Uri.parse(TheLConstants.FILE_PIC_URL + videoThumnail));
        img_video_play.setVisibility(View.VISIBLE);
        themeReplyClass = ThemeCommentBean.TYPE_CLASS_VIDEO;
        showTypeView(themeTypeEnum.VIDEO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);

        callbackManager.onActivityResult(requestCode, resultCode, data);
        L.d("refresh", "resultCode=" + requestCode + ",requestCode=" + requestCode);
        if (resultCode == TheLConstants.RESULT_CODE_BLOCK_THIS_MOMENT) {
            Intent intent = new Intent();
            intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, momentsId);
            setResult(TheLConstants.RESULT_CODE_BLOCK_THIS_MOMENT, intent);
            finish();
        } else if (resultCode == TheLConstants.RESULT_CODE_REPORT_MOMENT_SUCCEED && requestCode != TheLConstants.BUNDLE_CODE_THEME_DETAIL_FRAGMENT) {
            Intent intent = new Intent();
            intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, momentsId);
            setResult(TheLConstants.RESULT_CODE_REPORT_MOMENT_SUCCEED, intent);
            finish();
        } else if (resultCode == TheLConstants.RESULT_CODE_TAKE_PHOTO) {//选择图片
            String photoPath = data.getStringExtra(TheLConstants.BUNDLE_KEY_IMAGE_OUTPUT_PATH);
            if (!TextUtils.isEmpty(photoPath)) {
                selectedPhoto(photoPath);
            }
        } else if (resultCode == TheLConstants.RESULT_CODE_WRITE_MOMENT_DELETE_PICTURE) {// 删除图片
            if (data != null) {
                final ArrayList<Integer> remainIndexes = data.getIntegerArrayListExtra(TheLConstants.BUNDLE_KEY_INDEX);
                final ArrayList<PicBean> remainPicBeans = new ArrayList<>();
                if (remainIndexes != null) {// 如果有删除图片
                    if (selectedPics.get(0).isSelectBtn) {// 如果没选满6张
                        remainPicBeans.add(selectedPics.get(0));
                        for (Integer i : remainIndexes) {
                            remainPicBeans.add(selectedPics.get(i + 1));
                        }
                    } else {// 如果选满6张
                        for (Integer i : remainIndexes) {
                            remainPicBeans.add(selectedPics.get(i));
                        }
                        if (remainPicBeans.size() < SELECT_PIC_LIMIT)
                            remainPicBeans.add(0, new PicBean(true));
                    }
                }
                selectedPics.clear();
                selectedPics.addAll(remainPicBeans);
                showTypeView(themeTypeEnum.IMAGE);
                selectPicsRecyclerViewAdapter.notifyDataSetChanged();
                if (selectedPics.size() == HAVE_SELECTED_PIC_COUNT) {//没有图片，并且是删除图片操作，则当前日志回复类型为文本类型
                    setDoneSendButtonEnabled(false);

                    showTypeView(themeTypeEnum.TEXT);
                }
            }
        } else if (resultCode == TheLConstants.RESULT_CODE_SELECT_LOCAL_IMAGE) {
            String photoPath = data.getStringExtra(TheLConstants.BUNDLE_KEY_LOCAL_IMAGE_PATH);
            if (!TextUtils.isEmpty(photoPath)) {
                selectedPhoto(photoPath);
            } else {
                DialogUtil.showToastShort(this, TheLApp.getContext().getString(R.string.info_rechoise_photo));
            }
        }

    }


    private void showTypeView(themeTypeEnum type) {
        lin_bottom.setEnabled(true);
        lin_imgs.setVisibility(View.VISIBLE);
        lin_music.setVisibility(View.GONE);
        rel_video.setVisibility(View.GONE);
        progressbar_video.setVisibility(View.GONE);
        img_video_play.setVisibility(View.VISIBLE);
        progressbar_music.setVisibility(View.GONE);
        img_music_play.setVisibility(View.VISIBLE);
        img_select_photo.setEnabled(false);
        selectedPicsRecyclerView.setVisibility(View.GONE);
        switch (type) {
            case TEXT:
                themeReplyClass = ThemeCommentBean.TYPE_CLASS_TEXT;
                videoPath = null;
                selectedPics.clear();
                lin_imgs.setVisibility(View.GONE);
                img_select_photo.setEnabled(true);
                break;
            case VOICE:
                lin_music.setVisibility(View.VISIBLE);
                videoPath = null;
                selectedPics.clear();
                break;
            case IMAGE:
                selectedPicsRecyclerView.setVisibility(View.VISIBLE);
                videoPath = null;
                break;
            case VIDEO:
                rel_video.setVisibility(View.VISIBLE);
                selectedPics.clear();
                setDoneSendButtonEnabled(true);
                break;
        }
    }

    /**
     * 选好图片后刷新界面
     *
     * @param path
     */
    private void selectedPhoto(String path) {
        showTypeView(themeTypeEnum.IMAGE);
        themeReplyClass = ThemeCommentBean.TYPE_CLASS_IMAGE;
        selectedPicsRecyclerView.setVisibility(VISIBLE);
        String[] picUrlsTemp = path.split(",");
        boolean haveSelectedLegalPhoto = false;
        final int count = picUrlsTemp.length;
        for (int i = 0; i < count; i++) {
            if (ImageUtils.isImageLegal(picUrlsTemp[i], 235, 235)) {
                String outputName = "pic" + i + ImageUtils.getPicName();
                ImageUtils.handleLocalPic(picUrlsTemp[i], TheLConstants.F_TAKE_PHOTO_ROOTPATH, outputName, TheLConstants.MAX_PIC_WIDTH, TheLConstants.MAX_PIC_HEIGHT, TheLConstants.PIC_QUALITY, false);
                PicBean picBean = new PicBean();
                picBean.isSelectBtn = false;
                picBean.localUrl = TheLConstants.F_TAKE_PHOTO_ROOTPATH + outputName;
                selectedPics.add(picBean);
                haveSelectedLegalPhoto = true;
            } else {
                DialogUtil.showToastShort(this, getString(R.string.info_illegal_photo));
            }
        }
        final int size = selectedPics.size();
        if (size > 0 && size < SELECT_PIC_LIMIT && !selectedPics.get(0).isSelectBtn) {//如果总图片小于6张并且第一张不是选择图片，则第一张添加选择图片
            PicBean picBean = new PicBean();
            picBean.isSelectBtn = true;
            selectedPics.add(0, picBean);
        }
        if (size > 0) {
            setDoneSendButtonEnabled(true);

        }
        if (selectedPics.size() == SELECT_PIC_LIMIT + 1) {
            selectedPics.remove(0);
        }

        if (haveSelectedLegalPhoto) {
            selectPicsRecyclerViewAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void finish() {
        ViewUtils.hideSoftInput(this, input);

        super.finish();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void initViews() {
        findViewById(R.id.lin_back).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                ViewUtils.preventViewMultipleClick(arg0, 2000);
                close();
            }
        });

        recyclerView = findViewById(R.id.recycler_view);
        initRecycler();
        swipe_container = findView(R.id.swipe_container);
        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                momentsBeanList.clear();
                cursor = 0;
                haveNextPage = true;
                adapter.removeAllFooterView();
                getThemeMomentInfoV2(momentsId);
            }
        });

        ViewUtils.initSwipeRefreshLayout(swipe_container);
        title_txt = findView(R.id.title_txt);
        img_share = findView(R.id.img_share);
        input = findView(R.id.input);
        img_select_photo = findView(R.id.img_select_photo);
        img_select_photo.setEnabled(true);
        submit = findView(R.id.send);
        selectedPicsRecyclerView = findView(R.id.rv_pics);
        //设置布局管理器
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(TheLApp.getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        selectedPicsRecyclerView.setLayoutManager(linearLayoutManager);
        //如果可以确定每个item的高度是固定的，设置这个选项可以提高性能
        selectedPicsRecyclerView.setHasFixedSize(true);
        selectedPicsRecyclerView.setItemAnimator(null);
        initSelectedRecyclerView();

        default_page = this.findViewById(R.id.default_page);
        ((ImageView) (default_page.findViewById(R.id.default_image))).setImageResource(R.mipmap.stay_filters_default);
        ((TextView) (default_page.findViewById(R.id.default_text))).setText(getString(R.string.moment_comments_error));

        lin_update_user_info = findViewById(R.id.lin_update_user_info);
        lin_update_user_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                Intent intent = new Intent(ThemeDetailActivity.this, UpdateUserInfoActivity.class);
                Bundle bundle = new Bundle();
                MyInfoBean userBean = ShareFileUtils.getUserData();
                bundle.putSerializable("my_info", userBean);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        /**发布话题回复日志区域**/
        lin_bottom = findViewById(R.id.lin_bottom);
        lin_imgs = findViewById(R.id.lin_imgs);
        progressbar_video = findViewById(R.id.progressbar_video);
        rel_video = findViewById(R.id.rel_video);
        img_video = findViewById(R.id.img_video);
        img_video_play = findViewById(R.id.img_video_play);
        lin_music = findViewById(R.id.lin_music);
        img_music = findViewById(R.id.img_music);
        img_music_play = findViewById(R.id.img_music_play);
        progressbar_music = findViewById(R.id.progressbar_music);
        txt_song_name = findViewById(R.id.txt_song_name);
        txt_song_album = findViewById(R.id.txt_song_album);
        txt_song_artist = findViewById(R.id.txt_song_artist);

    }

    private void initRecycler() {
        adapter = new MomentsAdapter(R.layout.item_moments, momentsBeanList, GrowingIoConstant.ENTRY_THEME_DETAIL_PAGE, "theme.detail", pageId, from_page, from_page_id, new MomentsAdapter.ThemeListCommentListener() {
            @Override
            public void onclick(MomentsBean momentsBean, int position) {
                if (position == 0) {
                    showKeyboard();
                    recyclerView.scrollToPosition(adapter.getItemCount() - 1);
                } else {
                    if (UserUtils.isVerifyCell()) {
//                        Intent intent;

                        if (MomentsBean.MOMENT_TYPE_THEME.equals(momentsBean.momentsType)) {
//                            intent = new Intent(ThemeDetailActivity.this, ThemeDetailActivity.class);
//                            if (momentsBean.parentMoment != null) {
//                                intent.putExtra(BundleConstants.THEMEPARTICIPATES, String.valueOf(momentsBean.parentMoment.joinTotal));
//                            } else {
//                                intent.putExtra(BundleConstants.THEMEPARTICIPATES, String.valueOf(momentsBean.joinTotal));
//                            }
//                            Bundle bundle = new Bundle();
//
//                            bundle.putSerializable(TheLConstants.BUNDLE_KEY_MOMENT_BEAN, momentsBean);
//                            intent.putExtras(bundle);
//                            ThemeDetailActivity.this.startActivityForResult(intent, TheLConstants.BUNDLE_CODE_MOMENTS_ACTIVITY);
                            FlutterRouterConfig.Companion.gotoThemeDetails(momentsBean.momentsId);
                        } else {
//                            intent = new Intent(ThemeDetailActivity.this, MomentCommentActivity.class);
//                            intent.putExtra(BundleConstants.FROME_WRITE_COMMENT, MomentCommentActivity.WriteMomentComment);
                            FlutterRouterConfig.Companion.gotoMomentDetails(momentsBean.momentsId);
                        }

                    }
                }
            }
        });

        adapter.setShowRecommendView(false);

        adapter.setOnItemClickListener(new BaseAdapter.OnItemClickListener<MomentsBean>() {
            @Override
            public void onItemClick(View view, MomentsBean item, int position) {
//                Intent intent;
//                intent = new Intent(ThemeDetailActivity.this, MomentCommentActivity.class);
//                Bundle bundle = new Bundle();
//                bundle.putSerializable(TheLConstants.BUNDLE_KEY_MOMENT_BEAN, item);
//                intent.putExtras(bundle);
//                ThemeDetailActivity.this.startActivityForResult(intent, TheLConstants.BUNDLE_CODE_MOMENTS_ACTIVITY);
                FlutterRouterConfig.Companion.gotoMomentDetails(item.momentsId);
            }

        });

        manager = new MyLinearLayoutManager(ThemeDetailActivity.this);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(manager);
        recyclerView.addItemDecoration(new DefaultItemDivider(TheLApp.context, LinearLayoutManager.VERTICAL, R.color.bg_color, (int) TheLApp.getContext().getResources().getDimension(R.dimen.wide_divider_height), true, false));
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        adapter.openLoadMore(true);
        adapter.setOnLoadMoreListener(new BaseRecyclerViewAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {

                if (haveNextPage) {
                    loadThemeDetail(cursor);
                } else {
                    recyclerView.post(new Runnable() {
                        @Override
                        public void run() {
                            adapter.openLoadMore(adapter.getData().size(), false);
                            View view = LayoutInflater.from(ThemeDetailActivity.this).inflate(R.layout.load_more_footer_layout, (ViewGroup) recyclerView.getParent(), false);
                            ((TextView) view.findViewById(R.id.text)).setText(getString(R.string.info_no_more));
                            adapter.addFooterView(view);
                        }
                    });

                }
            }
        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {

                    if (adapter != null && adapter.getData() != null) {

                        ListVideoVisibilityManager.getInstance().calculatorItemPercent(manager, newState, adapter, TheLConstants.EntryConstants.ENTRY_THEMECOMMENT);

                    }

                    try {
                        Glide.with(ThemeDetailActivity.this).resumeRequests();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {

                    try {
                        Glide.with(ThemeDetailActivity.this).pauseRequests();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

            }
        });

        recyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                dismissKeyboard();
                return false;
            }
        });

    }

    private void initSelectedRecyclerView() {
        final PicBean picBean = new PicBean();
        picBean.isSelectBtn = true;
        selectedPics.add(picBean);
        selectPicsRecyclerViewAdapter = new SelectPicsRecyclerViewAdapter(selectedPics);
        selectPicsRecyclerViewAdapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (position == 0) {
                    if (selectedPics.size() < SELECT_PIC_LIMIT || selectedPics.get(0).isSelectBtn) {// 未选满6张，点选图片
                        Intent intent = new Intent(ThemeDetailActivity.this, SelectLocalImagesActivity.class);
                        intent.putExtra(TheLConstants.BUNDLE_KEY_SELECT_AMOUNT, SELECT_PIC_LIMIT - selectedPics.size() + 1);
                        startActivityForResult(intent, TheLConstants.BUNDLE_CODE_WRITE_MOMENT_ACTIVITY);
                    } else {// 已选满6张，点击进入编辑
                        editSelectedPhotos(position);
                    }
                } else
                    editSelectedPhotos(position);
            }
        });
        selectedPicsRecyclerView.setAdapter(selectPicsRecyclerViewAdapter);
        selectedPicsRecyclerView.addItemDecoration(new DefaultItemDivider(TheLApp.getContext(), LinearLayoutManager.HORIZONTAL, R.color.transparent, 10, true, true));
    }

    private void editSelectedPhotos(int index) {
        Intent i = new Intent(this, UserInfoPhotoActivity.class);
        i.putExtra("fromPage", ReleaseMomentActivity.class.getName());
        ArrayList<String> photoUrls = new ArrayList<>();
        for (PicBean pic : selectedPics) {
            if (!pic.isSelectBtn)
                photoUrls.add(pic.localUrl);
        }
        if (selectedPics.get(0).isSelectBtn)
            i.putExtra("position", index - 1);
        else
            i.putExtra("position", index);
        i.putStringArrayListExtra(TheLConstants.BUNDLE_KEY_PHOTOS, photoUrls);
        startActivityForResult(i, TheLConstants.BUNDLE_CODE_WRITE_MOMENT_ACTIVITY);
    }

    /**
     * 话题回复类型选择（图片，视频，音乐）
     */
    private void showThemeReplyCommentTypeDialog() {
        int limit = SELECT_PIC_LIMIT;
        if (selectedPics.size() > 0 && selectedPics.get(0).isSelectBtn) {
            limit = SELECT_PIC_LIMIT - selectedPics.size() + 1;
            if (limit < 0) {
                limit = 0;
            }
        }
        DialogUtil.showThemeReplyCommentTypeDialog(this, limit);
    }

    /**
     * 这些监听是在数据加载成功后创建
     */
    private void setListeners() {
        L.d("setListeners", "-----------setListeners---------");

        img_select_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showThemeReplyCommentTypeDialog();
            }
        });

        // 发布评论
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (UserUtils.isVerifyCell()) {
                    ViewUtils.preventViewMultipleClick(v, 2000);
                    BusinessUtils.playSound(R.raw.sound_send);
                    submitComment();
                }
            }
        });

        textWatcher = new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String str = s.toString().trim();

                if (str.length() > 0) {
                    setDoneSendButtonEnabled(true);
                } else {
                    setDoneSendButtonEnabled(false);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };
        input.addTextChangedListener(textWatcher);
        img_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewUtils.preventViewMultipleClick(view, 1000);
                final boolean isCollect = !BusinessUtils.isCollected(momentsId);//是否被收藏,如果已经被收藏，则为true,要收藏，否则，要删除
                collection(isCollect);
            }
        });

        rel_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoVideoPreview();
            }
        });
        lin_music.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoMusicPreview();
            }
        });
    }

    private void setDoneSendButtonEnabled(boolean enabled) {
        if (enabled) {
            submit.setTextColor(ContextCompat.getColor(ThemeDetailActivity.this, R.color.tab_normal));
        } else {
            submit.setTextColor(ContextCompat.getColor(ThemeDetailActivity.this, R.color.gray));
        }
    }

    /**
     * 音乐预览
     */
    private void gotoMusicPreview() {
    }

    /**
     * 视频预览
     */
    private void gotoVideoPreview() {
        if (TextUtils.isEmpty(videoPath)) {
            return;
        }
        Intent i = new Intent(this, VideoPreviewActivity.class);
        i.putExtra("url", videoPath);
        startActivityForResult(i, TheLConstants.BUNDLE_CODE_WRITE_MOMENT_ACTIVITY);

    }

    private void loadThemeDetail(final int cur) {
        Flowable<ThemeCommentListBeanNew> flowable = DefaultRequestService.createAllRequestService().getThemeCommentList(String.valueOf(cur), String.valueOf(pageSize), momentsId);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<ThemeCommentListBeanNew>() {
            @Override
            public void onNext(ThemeCommentListBeanNew data) {
                super.onNext(data);
                if (data != null && data.data != null && data.data.list != null && !data.data.list.isEmpty()) {

                    MomentsListBean momentsListBean = new MomentsListBean();
                    List<MomentsBean> momentsBeans = new ArrayList<>();

                    for (int i = 0; i < data.data.list.size(); i++) {
                        ThemeCommentBean themeCommentBean = data.data.list.get(i);
                        MomentsBean momentsBean = ThemeCommentBean.themeCommentBeanToMomentsBean(themeCommentBean);
                        momentsBeans.add(momentsBean);
                    }
                    momentsListBean.momentsList = momentsBeans;
                    momentsListBean.filterBlockMoments();
                    MomentsAdapter.filterUserMoments(momentsListBean.momentsList);
                    MomentsAdapter.filterBlack(momentsListBean.momentsList);
                    MomentsAdapter.filterAdMoment(momentsListBean.momentsList);
                    if (adapter != null && adapter.getData() != null && adapter.getData().size() > 0) {
                        MomentsAdapter.filterEquallyMoment(adapter.getData(), momentsListBean.momentsList);
                    }
                    momentsBeanList.addAll(momentsListBean.momentsList);
                    cursor = data.data.cursor;
                    haveNextPage = data.data.haveNextPage;
                }

                if (momentsBeanList != null) {

                    adapter.notifyDataChangedAfterLoadMore(true, momentsBeanList.size());

                }

                requestFinished();
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                requestFinished();
            }
        });
    }

    private void releaseFailed() {
        DialogUtil.showToastShort(this, getString(R.string.info_wrong));
        requestFinished();
    }

    private void requestFinished() {
        swipe_container.postDelayed(new Runnable() {

            @Override
            public void run() {
                if (swipe_container != null && swipe_container.isRefreshing())
                    swipe_container.setRefreshing(false);
            }
        }, 1000);
        closeLoading();
    }

    private void close() {
        if (!TextUtils.isEmpty(input.getText().toString().trim()) || selectedPics.size() > HAVE_SELECTED_PIC_COUNT) {
            DialogUtil.showConfirmDialog(ThemeDetailActivity.this, null, getString(R.string.moments_release_moment_discard_tip), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    PushUtils.finish(ThemeDetailActivity.this);
                    ViewUtils.hideSoftInput(ThemeDetailActivity.this, input);
                }
            });
        } else {
            PushUtils.finish(ThemeDetailActivity.this);
            ViewUtils.hideSoftInput(this, input);

        }
    }

    /**
     * 关闭键盘
     */
    public void dismissKeyboard() {
        ViewUtils.hideSoftInput(this, input);
    }

    public void showKeyboard() {
        ViewUtils.showSoftInput(this, input);
    }

    // key是请求的唯一表示，value是要上传图片的本地路径
    private Map<String, String> requestMD5s = new HashMap<>();
    // key是要上传图片的本地路径，value图片上传到七牛的地址
    private Map<String, String> picUploadPaths = new HashMap<>();
    // 上传七牛成功的图片URL
    private JSONObject uploadedImgsJson = new JSONObject();
    // 即将上传的图片本地地址
    private ArrayList<String> photosToUpload = new ArrayList<>();

    private String myComment;

    /**
     * 先封装日志
     */
    private void submitComment() {
        sendMoment = new ThemeCommentAddBean();
        if (TextUtils.isEmpty(momentsId)) {
            return;
        }
        requestMD5s.clear();
        picUploadPaths.clear();
        uploadedImgsJson = new JSONObject();
        photosToUpload.clear();
        myComment = input.getText().toString().trim();
        final int lengthLimit = getResources().getInteger(R.integer.theme_reply_word_count_limit);
        if (myComment.length() > lengthLimit) {
            DialogUtil.showToastShort(TheLApp.getContext(), getString(R.string.info_words_length_limit, lengthLimit));
            return;
        }
        if (ThemeCommentBean.TYPE_CLASS_IMAGE.equals(themeReplyClass) && selectedPics.size() <= HAVE_SELECTED_PIC_COUNT) {//图片日志且没图片
            themeReplyClass = ThemeCommentBean.TYPE_CLASS_TEXT;//变为纯文本日志
        }
        if (ThemeCommentBean.TYPE_CLASS_VIDEO.equals(themeReplyClass) && TextUtils.isEmpty(videoPath)) {//视频日志没视频
            themeReplyClass = ThemeCommentBean.TYPE_CLASS_TEXT;//变为纯文本日志
        }
        if (ThemeCommentBean.TYPE_CLASS_VOICE.equals(themeReplyClass)) {//音乐日志无音乐
            themeReplyClass = ThemeCommentBean.TYPE_CLASS_TEXT;//转变为纯文本日志
        }
        if (ThemeCommentBean.TYPE_CLASS_TEXT.equals(themeReplyClass) && TextUtils.isEmpty(myComment)) {//文本日志切位空
            DialogUtil.showToastShort(this, getString(R.string.moment_comments_empty_tip));
            return;
        }

        sendMoment.momentsText = myComment;
        sendMoment.momentsType = MomentsBean.MOMENT_TYPE_THEME_REPLY;
        sendMoment.parentId = momentsId;
        sendMoment.themeReplyClass = themeReplyClass;
        sendMoment.imgsToUpload = "";
        sendMoment.uploadedImgsUrls = "";

        L.d(TAG, " themeReplyClass : " + themeReplyClass);

        //如果是图片日志（到了这里肯定有图片）封装图片日志
        if (ThemeCommentBean.TYPE_CLASS_IMAGE.equals(themeReplyClass)) {
            final StringBuilder picUrlSB = new StringBuilder();
            for (PicBean pic : selectedPics) {

                L.d(TAG, " localUrl : " + pic.localUrl);

                if (!pic.isSelectBtn && !TextUtils.isEmpty(pic.localUrl)) {
                    picUrlSB.append(pic.localUrl);
                    picUrlSB.append(",");
                }
            }

            L.d(TAG, " picUrlSB : " + picUrlSB.toString());

            if (picUrlSB.length() > 0) {
                picUrlSB.deleteCharAt(picUrlSB.length() - 1);
                sendMoment.imageUrlList = picUrlSB.toString();
                sendMoment.imgsToUpload = sendMoment.imageUrlList;
            }
        }
        //封装视频日志
        if (ThemeCommentBean.TYPE_CLASS_VIDEO.equals(themeReplyClass)) {
            sendMoment.imageUrlList = videoThumnail;
            sendMoment.videoUrl = videoPath;
            sendMoment.playTime = playTime;
            sendMoment.pixelWidth = pixelWidth != 0 ? pixelWidth : 540;
            sendMoment.pixelHeight = pixelHeight != 0 ? pixelHeight : 540;
            try {
                sendMoment.mediaSize = new File(videoPath).length() / 1000;
            } catch (Exception e) {
                e.printStackTrace();
            }
            sendMoment.imgsToUpload = videoPath + "," + videoThumnail;
        }

        tryReleaseComment();
    }

    /**
     * 发布日志
     */
    private void tryReleaseComment() {
        if (sendMoment == null) {
            return;
        }
        //图片日志或者视频日志
        if (ThemeCommentBean.TYPE_CLASS_IMAGE.equals(sendMoment.themeReplyClass) || ThemeCommentBean.TYPE_CLASS_VIDEO.equals(sendMoment.themeReplyClass)) {
            final boolean isReleaseVideoMoment = ThemeCommentBean.TYPE_CLASS_VIDEO.equals(sendMoment.themeReplyClass);
            PicUploadUtils.uploadPic(sendMoment.imgsToUpload, new PicUploadContract.PicUploadCallback() {
                @Override
                public void uploadFailed(int dex) {
                    releaseFailed();
                }

                @Override
                public void uploadComplete(String imageUrls) {
                    sendMoment.imageUrlList = imageUrls;
                    releaseComment();
                }

                @Override
                public void uploadVideoComplete(String videoUrl, String webpPath) {
                    sendMoment.videoUrl = videoUrl;
                    sendMoment.videoWebp = webpPath;
                    sendMoment.videoColor = videoMainColor;
                }
            }, isReleaseVideoMoment);
        } else {
            releaseComment();
        }
        showSubmitDialog(sendMoment.themeReplyClass);
        if (sendMoment != null) {
            traceThemeDetailLog("theme.detail", "comment", momentsId, ThemeCommentBean.TYPE_CLASS_IMAGE, sendMoment.momentsText);

        }

    }

    /**
     * 显示提交加载对话框
     *
     * @param themeReplyClass
     */
    private void showSubmitDialog(String themeReplyClass) {
        if (ThemeCommentBean.TYPE_CLASS_IMAGE.equals(themeReplyClass) && selectedPicsRecyclerView.getVisibility() == View.VISIBLE && selectPicsRecyclerViewAdapter.getData().size() > 0) {
            final PicBean bean = selectPicsRecyclerViewAdapter.getItem(0);
            bean.isShowingBar = true;
            selectPicsRecyclerViewAdapter.notifyItemChanged(0 + selectPicsRecyclerViewAdapter.getHeaderLayoutCount());
        }
        if (ThemeCommentBean.TYPE_CLASS_VIDEO.equals(themeReplyClass)) {
            if (rel_video.getVisibility() == View.VISIBLE) {
                progressbar_video.setVisibility(View.VISIBLE);
                img_video_play.setVisibility(View.GONE);
            }
        }
        if (ThemeCommentBean.TYPE_CLASS_VOICE.equals(themeReplyClass)) {
            if (lin_music.getVisibility() == View.VISIBLE) {
                progressbar_music.setVisibility(View.VISIBLE);
                img_music_play.setVisibility(View.GONE);
            }
        }
        setBottomEnable(false);
    }

    private void setBottomEnable(boolean enable) {
        if (null == lin_bottom) {
            return;
        }
        if (lin_bottom instanceof ViewGroup) {
            ViewGroup viewGroup = lin_bottom;
            LinkedList<ViewGroup> queue = new LinkedList<ViewGroup>();
            queue.add(viewGroup);
            // 遍历viewGroup
            while (!queue.isEmpty()) {
                ViewGroup current = queue.removeFirst();
                current.setEnabled(enable);
                for (int i = 0; i < current.getChildCount(); i++) {
                    if (current.getChildAt(i) instanceof ViewGroup) {
                        queue.addLast((ViewGroup) current.getChildAt(i));
                    } else {
                        current.getChildAt(i).setEnabled(enable);
                    }
                }
            }
        } else {
            lin_bottom.setEnabled(enable);
        }
    }

    private void releaseComment() {
        if (sendMoment == null) {
            return;
        }
        HashMap<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_MOMENT_TEXT, sendMoment.momentsText + "");
        data.put(RequestConstants.I_MOMENTS_TYPE, sendMoment.momentsType + "");
        data.put("parentId", sendMoment.parentId + "");
        data.put("themeReplyClass", sendMoment.themeReplyClass + "");
        //图片日志
        if (ThemeCommentBean.TYPE_CLASS_IMAGE.equals(sendMoment.themeReplyClass)) {
            data.put("imageUrlList", sendMoment.imageUrlList + "");
        }
        //视频
        if (ThemeCommentBean.TYPE_CLASS_VIDEO.equals(sendMoment.themeReplyClass)) {
            data.put("imageUrlList", sendMoment.imageUrlList + "");
            data.put("videoUrl", sendMoment.videoUrl + "");
            data.put("pixelWidth", sendMoment.pixelWidth + "");
            data.put("pixelHeight", sendMoment.pixelHeight + "");
            data.put("mediaSize", sendMoment.mediaSize + "");
            data.put("playTime", (sendMoment.playTime / 1000) + "");
            data.put("videoColor", sendMoment.videoColor);
            data.put("videoWebp", sendMoment.videoWebp);
        }
        //音乐
        if (ThemeCommentBean.TYPE_CLASS_VOICE.equals(sendMoment.themeReplyClass)) {
            data.put(RequestConstants.I_SONG_ID, sendMoment.songId + "");
            data.put(RequestConstants.I_SONG_NAME, sendMoment.songName);
            data.put(RequestConstants.I_ARTIST_NAME, sendMoment.artistName);
            data.put(RequestConstants.I_ALBUM_NAME, sendMoment.albumName);
            data.put(RequestConstants.I_ALBUM_LOGO_100, sendMoment.albumLogo100);
            data.put(RequestConstants.I_ALBUM_LOGO_444, sendMoment.albumLogo444);
            data.put(RequestConstants.I_SONG_LOCATION, sendMoment.songLocation);
            data.put(RequestConstants.I_SONG_TO_URL, sendMoment.toURL);
        }

        Flowable<BaseDataBean> flowable = DefaultRequestService.createAllRequestService().releaseThemesComment(MD5Utils.generateSignatureForMap(data));

        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
            @Override
            public void onNext(BaseDataBean data) {
                super.onNext(data);
                if (!hasErrorCode) {
                    // 添加评论后需要刷新moments列表
                    // 清空输入框
                    input.setText("");
                    resetInput();
                    sendReleaseMomentBroad();

                    if (sendMoment != null) {

                        MomentsBean momentsBean = new MomentsBean();

                        momentsBean.momentsId = MomentsBean.SEND_MOMENT_FLAG;

                        if (sendMoment.imageUrlList != null && sendMoment.imageUrlList.length() > 0) {
                            momentsBean.momentsType = MomentTypeConstants.MOMENT_TYPE_TEXT_IMAGE;
                        }

                        if (sendMoment.videoUrl != null && sendMoment.videoUrl.length() > 0) {
                            momentsBean.momentsType = MomentTypeConstants.MOMENT_TYPE_VIDEO;
                        }

                        momentsBean.imageUrl = sendMoment.imageUrlList;
                        momentsBean.videoUrl = sendMoment.videoUrl;
                        momentsBean.pixelWidth = sendMoment.pixelWidth;
                        momentsBean.pixelHeight = sendMoment.pixelHeight;
                        momentsBean.mediaSize = sendMoment.mediaSize;
                        momentsBean.playTime = sendMoment.playTime / 1000;
                        momentsBean.videoColor = sendMoment.videoColor;
                        momentsBean.videoWebp = sendMoment.videoWebp;
                        momentsBean.themeReplyClass = sendMoment.themeReplyClass;
                        momentsBean.momentsText = sendMoment.momentsText;
                        momentsBean.nickname = UserUtils.getUserName();
                        momentsBean.avatar = UserUtils.getUserAvatar();
                        momentsBean.releaseTime = System.currentTimeMillis();
                        momentsBean.myself = 1;

                        adapter.add(momentsBean);
                        adapter.notifyDataSetChanged();

                    }

                    recyclerView.scrollToPosition(adapter.getItemCount() - 1);
                    DialogUtil.showToastShort(ThemeDetailActivity.this, getString(R.string.post_moment_posted));

                }
            }

            @Override
            public void onComplete() {
                super.onComplete();
                setBottomEnable(true);
                closeLoading();
            }
        });

    }

    private void sendReleaseMomentBroad() {
        // 发刷新tab界面的广播
        Intent intent1 = new Intent();
        intent1.setAction(TheLConstants.BROADCAST_FAILED_MOMENTS_CHECK_ACTION);
        TheLApp.getContext().sendBroadcast(intent1);
    }

    private void clearSelectedPhotos() {
        selectedPics.clear();
        selectedPics.add(new PicBean(true));
        selectPicsRecyclerViewAdapter.notifyDataSetChanged();
    }

    private void resetInput() {
        input.setHint(getString(R.string.moments_comment_input_hint));
        clearSelectedPhotos();
        showTypeView(themeTypeEnum.TEXT);
        setBottomEnable(true);
    }

    private void refreshTitle(long count) {

        L.d("ThemeDetailActivity", " count : " + count);

        if (count > 1) {
            title_txt.setText(TheLApp.getContext().getString(R.string.hot_themes_join_even, count));
        } else {
            count = 1;
            title_txt.setText(TheLApp.getContext().getString(R.string.hot_themes_join_odd, count));
        }
    }

    private class SelectPicsRecyclerViewAdapter extends BaseRecyclerViewAdapter<PicBean> {

        public SelectPicsRecyclerViewAdapter(List<PicBean> list) {
            super(R.layout.theme_restore_layout, list);
        }

        @Override
        protected void convert(BaseViewHolder helper, PicBean bean) {

            final SimpleDraweeView simpleDraweeView = helper.getView(R.id.pic);
            final ProgressBar bar = helper.getView(R.id.progressbar_pic);
            bar.setVisibility(View.GONE);
            simpleDraweeView.setLayoutParams(new RelativeLayout.LayoutParams(getResources().getDimensionPixelSize(R.dimen.theme_reply_select_photo_size), getResources().getDimensionPixelSize(R.dimen.theme_reply_select_photo_size)));

            if (bean.isSelectBtn) {// 选择按钮
                simpleDraweeView.getHierarchy().setPlaceholderImage(R.mipmap.bg_feed_addpic);
                simpleDraweeView.setImageURI("");
            } else {// 非选择按钮，图片
                simpleDraweeView.getHierarchy().setPlaceholderImage(R.color.pic_placeholder_color);
                simpleDraweeView.setImageURI(TheLConstants.FILE_PIC_URL + bean.localUrl);
            }
            if (bean.isShowingBar) {//如果要显示bar，则显示bar(上传图片的时候进度)
                bar.setVisibility(View.VISIBLE);
            }

        }
    }

    private class PicBean {

        public boolean isSelectBtn;
        public String localUrl;
        public boolean isShowingBar = false;

        public PicBean() {
        }

        /**
         * 构造方法，直接赋值是否是选择按钮
         *
         * @param boo
         */
        public PicBean(boolean boo) {
            this.isSelectBtn = boo;
        }
    }

    private void collection(boolean isCollect) {

        Map<String, String> map = new HashMap<>();

        if (isCollect) {           //收藏
            headMoment.participates++;
            img_share.setImageResource(R.mipmap.icn_collection_press);
            map.put(RequestConstants.FAVORITE_COUNT, momentsId);
            map.put(RequestConstants.FAVORITE_TYPE, RequestConstants.FAVORITE_TYPE_MOM);

            Flowable<BaseDataBean> flowable = DefaultRequestService.createMomentRequestService().getFavoriteCreate(MD5Utils.generateSignatureForMap(map));
            flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
                @Override
                public void onNext(BaseDataBean data) {
                    super.onNext(data);
                    if (!BusinessUtils.isCollected(momentsId)) {
                        String collectList = SharedPrefUtils.getString(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.FAVORITE_MOMENT_LIST, "");
                        String id = "[" + ShareFileUtils.getString(ShareFileUtils.ID, "") + "_" + momentsId + "]";
                        collectList += id;
                        SharedPrefUtils.setString(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.FAVORITE_MOMENT_LIST, collectList);
                    }
                    DialogUtil.showToastShort(ThemeDetailActivity.this, getString(R.string.collection_success));
                }
            });
        } else {//取消收藏
            headMoment.participates--;
            img_share.setImageResource(R.mipmap.icn_collection_normal);

            map.put(RequestConstants.FAVORITE_ID, "");
            map.put(RequestConstants.FAVORITE_COUNT, momentsId);
            map.put(RequestConstants.FAVORITE_TYPE, RequestConstants.FAVORITE_TYPE_MOM);

            Flowable<BaseDataBean> flowable = DefaultRequestService.createMomentRequestService().deleteFavoriteMoment(MD5Utils.generateSignatureForMap(map));
            flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
                @Override
                public void onNext(BaseDataBean data) {
                    super.onNext(data);
                    if (BusinessUtils.isCollected(momentsId)) {
                        String collectList = SharedPrefUtils.getString(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.FAVORITE_MOMENT_LIST, "");
                        String id = "[" + ShareFileUtils.getString(ShareFileUtils.ID, "") + "_" + momentsId + "]";
                        collectList = collectList.replace(id, "");
                        SharedPrefUtils.setString(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.FAVORITE_MOMENT_LIST, collectList);
                    }
                    DialogUtil.showToastShort(ThemeDetailActivity.this, getString(R.string.collection_canceled));
                }
            });
        }
        refreshTitle(headMoment.participates);
    }

    private void filterListByMomentId(String momentId) {
        if (adapter != null) {
            List<MomentsBean> list = adapter.getData();
            if (list != null) {
                //如果是第一条（父item）
                if (list.get(0).momentsId.equals(momentId)) {
                    finish();
                } else {
                    ListIterator<MomentsBean> iterable = list.listIterator();
                    while (iterable.hasNext()) {
                        MomentsBean momentsBean = iterable.next();
                        if (String.valueOf(momentsBean.momentsId).equals(momentId)) {
                            iterable.remove();
                        }
                    }
                    adapter.notifyDataSetChanged();
                    closeLoading();
                }
            }

        }
    }

    private void filterListByUserId(String userId) {
        if (adapter != null) {
            List<MomentsBean> list = adapter.getData();
            if (list != null) {
                //如果是第一条（父item）
                if (String.valueOf(list.get(0).userId).equals(userId)) {
                    finish();
                } else {
                    ListIterator<MomentsBean> iterable = list.listIterator();
                    while (iterable.hasNext()) {
                        MomentsBean momentsBean = iterable.next();
                        if (String.valueOf(momentsBean.userId).equals(userId)) {
                            iterable.remove();
                        }
                    }
                    adapter.notifyDataSetChanged();
                    closeLoading();
                }
            }

        }
    }

    private void removeWinkComment(String myUserId, MomentsBean momentsBean) {
        List<WinkCommentBean> list = momentsBean.winkUserList;
        if (list != null) {
            ListIterator<WinkCommentBean> listIterator = list.listIterator();

            while (listIterator.hasNext()) {
                WinkCommentBean winkCommentBean = listIterator.next();
                if (myUserId.equals(String.valueOf(winkCommentBean.userId))) {
                    listIterator.remove();
                }
            }
        }
    }

    @Override
    public void onBlackOneMoment(String momentId) {
        filterListByMomentId(momentId);
    }

    @Override
    public void onBlackherMoment(String userId) {
        filterListByUserId(userId);
    }


    @Override
    public void onMomentDelete(String momentId) {
        filterListByMomentId(momentId);
    }

    @Override
    public void onMomentReport(String momentId) {
        filterListByMomentId(momentId);
    }

    @Override
    public void onWinkSucceess(String userId, String type) {

    }

    @Override
    public void onBlackUsersChanged(String userId, boolean isAdd) {
        filterListByUserId(userId);
    }

    @Override
    public void onLikeStatusChanged(String momentId, boolean like, String myUserId, WinkCommentBean myWinkUserBean) {
//        if (adapter != null) {
//            List<MomentsBean> list = adapter.getData();
//            if (list != null) {
//                ListIterator<MomentsBean> iterable = list.listIterator();
//                while (iterable.hasNext()) {
//                    MomentsBean momentsBean = iterable.next();
//                    if (momentsBean.momentsId.equals(momentId)) {
//                        if (like) {
//                            momentsBean.winkUserList.add(0, myWinkUserBean);
//                            momentsBean.winkFlag = MomentsBean.HAS_WINKED;
//                            momentsBean.winkNum++;
//                        } else {
//                            removeWinkComment(myUserId, momentsBean);
//                            momentsBean.winkFlag = MomentsBean.HAS_NOT_WINKED;
//                            momentsBean.winkNum--;
//                        }
//                    }
//
//                }
//            }
//            adapter.notifyDataSetChanged();
//        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        PushUtils.finish(ThemeDetailActivity.this);
    }

    @Override
    public void onFollowStatusChanged(int followStatus, String userId, String nickName, String avatar) {
        super.onFollowStatusChanged(followStatus, userId, nickName, avatar);
        // refreshFollowStatus(followStatus);
        if (adapter != null) {
            for (MomentsBean bean : adapter.getData()) {
                if ((bean.userId + "").equals(userId)) {
                    bean.followerStatus = followStatus;

                }
            }
            adapter.notifyDataSetChanged();
            if ((headMoment.userId + "").equals(userId)) {
                if (followStatus==1){
                    traceThemeFollowLog("theme.detail", "follow_themer", headMoment.momentsId, headMoment.userId);

                }else {
                    traceThemeFollowLog("theme.detail", "unfollow_themer", headMoment.momentsId, headMoment.userId);

                }
            }else {
                if (followStatus==1){
                    traceThemeFollowLog("theme.detail", "follow_replyer", headMoment.momentsId, headMoment.userId);

                }else {
                    traceThemeFollowLog("theme.detail", "unfollow_replyer", headMoment.momentsId, headMoment.userId);

                }
            }
        }

    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    public void traceThemeFollowLog(String page, String activity, String momentsId, int receiver_id) {
        String latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
        String longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");
        try {
            LogInfoBean logInfoBean = new LogInfoBean();
            logInfoBean.page = page;
            logInfoBean.page_id = pageId;
            logInfoBean.activity = activity;
            logInfoBean.from_page = from_page;
            logInfoBean.from_page_id = from_page_id;
            logInfoBean.lat = latitude;
            logInfoBean.lng = longitude;

            LogInfoBean.LogsDataBean logsDataBean = new LogInfoBean.LogsDataBean();
            logsDataBean.theme_id = momentsId;
            logsDataBean.receiver_id = receiver_id + "";
            logInfoBean.data = logsDataBean;

            LiveLogUtils.getInstance().addLog(logInfoBean);

        } catch (Exception e) {

        }
    }
}
