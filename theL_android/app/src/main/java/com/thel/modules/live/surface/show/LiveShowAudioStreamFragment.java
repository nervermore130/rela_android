package com.thel.modules.live.surface.show;

import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.postprocessors.IterativeBoxBlurPostProcessor;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.thel.BuildConfig;
import com.thel.R;
import com.thel.base.BaseFragment;
import com.thel.bean.LiveInfoLogBean;
import com.thel.constants.TheLConstants;
import com.thel.modules.live.agora.OnJoinChannelListener;
import com.thel.modules.live.agora.RtcEngineHandler;
import com.thel.modules.live.bean.LivePkHangupBean;
import com.thel.modules.live.bean.LivePkInitBean;
import com.thel.modules.live.bean.LivePkRequestNoticeBean;
import com.thel.modules.live.bean.LivePkResponseNoticeBean;
import com.thel.modules.live.bean.LivePkStartNoticeBean;
import com.thel.modules.live.bean.LivePkSummaryNoticeBean;
import com.thel.modules.live.bean.LiveRoomBean;
import com.thel.modules.live.bean.LiveRoomMsgConnectMicBean;
import com.thel.modules.live.bean.SoftGiftBean;
import com.thel.modules.live.in.LiveShowCaptureStreamIn;
import com.thel.modules.live.surface.watch.LiveWatchObserver;
import com.thel.modules.live.utils.LiveStatus;
import com.thel.modules.live.utils.LiveUtils;
import com.thel.modules.live.view.SoundSurfaceView;
import com.thel.utils.AppInit;
import com.thel.utils.ImageUtils;
import com.thel.utils.L;
import com.thel.utils.NetworkUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.ToastUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;

import io.agora.rtc.IRtcEngineEventHandler;
import io.agora.rtc.plugin.rawdata.MediaDataAudioObserver;
import io.agora.rtc.plugin.rawdata.MediaDataObserverPlugin;
import io.agora.rtc.plugin.rawdata.MediaPreProcessing;

import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_ANCHOR_LIVE_CONNECT_SUCCESS;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_ANCHOR_PROGRESSBAR_GONE;
import static java.lang.Math.abs;

/**
 * 声音直播(推流端)
 *
 * @author waiarl
 * @date 2018/1/22
 */

public class LiveShowAudioStreamFragment extends BaseFragment implements LiveShowCaptureStreamIn, MediaDataAudioObserver {
    private static final String TAG = "LiveShowAudioStream";

    private SoundSurfaceView sound_view;

    private LiveRoomBean liveRoomBean;

    //获取原始音频buffer
    private MediaDataObserverPlugin mediaDataObserverPlugin;

    private LiveWatchObserver observer;

    private SimpleDraweeView img_bg;

    private RelativeLayout root_rl;

    private RtcEngineHandler mRtcEngineHandler;

    private OnJoinChannelListener mOnJoinChannelSuccessListener = new OnJoinChannelListener() {

        @Override
        public void onJoinChannelSuccess() {

            if (observer != null) {
                observer.sendEmptyMessage(UI_EVENT_ANCHOR_PROGRESSBAR_GONE);
                observer.sendEmptyMessage(UI_EVENT_ANCHOR_LIVE_CONNECT_SUCCESS);
            }
        }

        @Override
        public void onLeaveChannelSuccess() {
            if (getActivity() == null) {
                return;
            }

            if (observer != null) {
                observer.sendColseMsg();
                observer.postDelayed(new Runnable() {//如果连接中断，则在1秒钟后关闭直播
                    @Override
                    public void run() {

                    }
                }, 1000);
            }
        }

        @Override
        public void onUserJoined(int uid, SurfaceView surfaceView) {
            if (mediaDataObserverPlugin != null) {
                mediaDataObserverPlugin.addDecodeBuffer(uid, 1382400);//720P
            }
        }

        @Override
        public void onUserOffline(int uid) {

            if (mediaDataObserverPlugin != null) {
                mediaDataObserverPlugin.removeDecodeBuffer(uid);//720P
            }
        }

        @Override
        public void onAudioVolumeIndication(final IRtcEngineEventHandler.AudioVolumeInfo[] speakers, int totalVolume) {

            if (observer != null) {
                observer.uploadAudioVolume(speakers);
            }
        }

        @Override
        public void onError(int error) {
            Log.e(TAG, "error: " + error);
        }

    };

    public static LiveShowAudioStreamFragment getInstance(Bundle bundle) {
        LiveShowAudioStreamFragment instance = new LiveShowAudioStreamFragment();
        instance.setArguments(bundle);
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Bundle bundle = getArguments();
        liveRoomBean = (LiveRoomBean) bundle.getSerializable(TheLConstants.BUNDLE_KEY_LIVE_ROOM);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_live_show_video_ks, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        root_rl = view.findViewById(R.id.root_rl);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (observer != null) {
            observer.bindLiveShowCaptureStreamIn(this);
        }

        if (liveRoomBean != null && liveRoomBean.agora != null && !TextUtils.isEmpty(String.valueOf(liveRoomBean.agora.channelId))) {
            //创建 RtcEngine 对象
            try {

                mediaDataObserverPlugin = MediaDataObserverPlugin.the();
                MediaPreProcessing.setCallback(mediaDataObserverPlugin);
                MediaPreProcessing.setAudioRecordByteBUffer(mediaDataObserverPlugin.byteBufferAudioRecord);
                MediaPreProcessing.setAudioPlayByteBUffer(mediaDataObserverPlugin.byteBufferAudioPlay);
                MediaPreProcessing.setBeforeAudioMixByteBUffer(mediaDataObserverPlugin.byteBufferBeforeAudioMix);
                MediaPreProcessing.setAudioMixByteBUffer(mediaDataObserverPlugin.byteBufferAudioMix);
                mediaDataObserverPlugin.addAudioObserver(this);

                int uid = Integer.valueOf(UserUtils.getMyUserId());

                mRtcEngineHandler = new RtcEngineHandler(getActivity(), RtcEngineHandler.LIVE_TYPE_AUDIO, liveRoomBean.agora.appId);

                mRtcEngineHandler.setOnJoinChannelSuccessListener(mOnJoinChannelSuccessListener);

                mRtcEngineHandler.enableAudioVolumeIndication(300, 3);

                mRtcEngineHandler.init(liveRoomBean.agora.token, liveRoomBean.agora.channelId, liveRoomBean.pubUrl, uid, liveRoomBean.liveId);

                setLiveInfoLog(liveRoomBean.pubUrl, TheLConstants.LiveInfoLogConstants.TYPE_FIRST_PUSH);

            } catch (Exception e) {
                e.printStackTrace();
                ToastUtils.showToastShort(getActivity(), "RtcEngine error");
                getActivity().finish();
            }
        } else {
            L.d(TAG, "channelId no exists");
        }

        initBackgroundView();
        initSoundView();
    }

    private void initBackgroundView() {
        if (img_bg == null && root_rl != null) {
            img_bg = new SimpleDraweeView(getContext());
            root_rl.addView(img_bg, RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            setSimpleBlur(img_bg, liveRoomBean.imageUrl, AppInit.displayMetrics.widthPixels, AppInit.displayMetrics.heightPixels);
            ImageView imageView = new ImageView(getContext());
            imageView.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
            imageView.setImageResource(R.color.black_transparent_50);
            root_rl.addView(imageView);
        }

    }

    private void initSoundView() {
        if (sound_view == null && liveRoomBean.isMulti == LiveRoomBean.SINGLE_LIVE) {
            sound_view = new SoundSurfaceView(getActivity());
            root_rl.addView(sound_view, RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            sound_view.start();
        }
    }

    private void showSoundView(byte[] bytes) {
        final int length = bytes.length;
        long sum = 0;
        for (int i = 0; i < length; i++) {
            sum += abs(bytes[i]);
        }
        final double volume = sum / (double) length / 1.28 * 8;
//        L.i(TAG, "voluem=" + volume);
        if (sound_view != null) {
            sound_view.initView(volume);
        }
    }

    private void setSimpleBlur(SimpleDraweeView simpleDraweeView, String url, int width, int height) {
        simpleDraweeView.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(url, width, height))).setPostprocessor(new IterativeBoxBlurPostProcessor(10, 10)).build()).setAutoPlayAnimations(true).build());
    }

    @Override
    public void onDestroy() {

        if (mRtcEngineHandler != null) {
            mRtcEngineHandler.leaveChannel();
            mRtcEngineHandler.destroy();
        }
        if (mediaDataObserverPlugin != null) {
            mediaDataObserverPlugin.removeAudioObserver(this);
            mediaDataObserverPlugin.removeAllBuffer();
        }
        MediaPreProcessing.releasePoint();
        if (sound_view != null) {
            sound_view.destory();
        }
        super.onDestroy();
    }

    @Override
    public void bindObserver(LiveWatchObserver observer) {
        this.observer = observer;
        observer.bindLiveShowCaptureStreamIn(this);
    }

    @Override
    public void muteLocalAudioStream(boolean mute) {
        if (mRtcEngineHandler != null) {
            mRtcEngineHandler.muteLocalAudioStream(mute);
        }
    }

    @Override
    public void playEffect(int soundId) {
        if (mRtcEngineHandler != null) {
            String[] fileName = {"laugh.mp3", "brava.wav", "fool.mp3", "mock.wav", "applaude.wav"};
            String filePath = "/assets/multimic/" + fileName[soundId];
            mRtcEngineHandler.playEffect(soundId, filePath);
        }
    }

    @Override
    public LiveStatus getListStatus() {
        return null;
    }

    @Override
    public void switchStricker(SoftGiftBean giftBean) {

    }

    @Override public void acceptAudienceLinkMic() {

    }

    @Override
    public void finishStream() {
        if (mRtcEngineHandler != null) {
            mRtcEngineHandler.leaveChannel();
            mRtcEngineHandler.destroy();
        }
        if (mediaDataObserverPlugin != null) {
            mediaDataObserverPlugin.removeAudioObserver(this);
            mediaDataObserverPlugin.removeAllBuffer();
        }
        MediaPreProcessing.releasePoint();
        if (getActivity() != null) {
            getActivity().finish();
        }
    }

    @Override
    public void onRecordAudioFrame(byte[] data, int videoType, int samples, int bytesPerSample, int channels, int samplesPerSec, long renderTimeMs, int bufferLength) {
        L.i(TAG, "onRecordAudioFrame samples :" + samples + " bytesPerSample:" + bytesPerSample + " channels:" + channels + " samplesPerSec:" + samplesPerSec);

        showSoundView(data);
    }

    @Override
    public void onPlaybackAudioFrame(byte[] data, int videoType, int samples, int bytesPerSample, int channels, int samplesPerSec, long renderTimeMs, int bufferLength) {
        L.i(TAG, "onPlaybackAudioFrame samples :" + samples + " bytesPerSample:" + bytesPerSample + " channels:" + channels + " samplesPerSec:" + samplesPerSec + " bufferLength: " + bufferLength);

    }

    @Override
    public void onPlaybackAudioFrameBeforeMixing(byte[] data, int videoType, int samples, int bytesPerSample, int channels, int samplesPerSec, long renderTimeMs, int bufferLength) {

    }

    @Override
    public void onMixedAudioFrame(byte[] data, int videoType, int samples, int bytesPerSample, int channels, int samplesPerSec, long renderTimeMs, int bufferLength) {
        L.i(TAG, "onMixedAudioFrame samples :" + samples + " bytesPerSample:" + bytesPerSample + " channels:" + channels + " samplesPerSec:" + samplesPerSec + " bufferLength: " + bufferLength);

    }

    @Override
    public void switchCamera() {

    }

    @Override
    public void adjustBeauty(String key, float progress) {

    }

    @Override
    public void showPkResponse(LivePkResponseNoticeBean pkResponseNoticeBean) {

    }

    @Override
    public void showPkStart(LivePkStartNoticeBean pkStartNoticeBean) {

    }

    @Override
    public void showPkCancel(String pkCancelPayload) {

    }

    @Override
    public void showPkHangup(LivePkHangupBean pkHangupBean) {

    }

    @Override
    public void showPkSummary(LivePkSummaryNoticeBean pkSummaryNoticeBean) {

    }

    @Override
    public void showPkStop(String pkStopPayload) {

    }

    @Override
    public void finishPk() {

    }

    @Override
    public void showPkRequest(LivePkRequestNoticeBean pkRequestNoticeBean) {

    }

    @Override
    public void showPkStream() {

    }

    @Override
    public void showPkInitStream(LivePkInitBean livePkInitBean) {

    }

    @Override
    public void audienceLinkMicResponse() {

    }

    @Override
    public void linkMicRequest(LiveRoomMsgConnectMicBean liveRoomMsgConnectMicBean) {

    }

    @Override
    public void linkMicResponse(LiveRoomMsgConnectMicBean liveRoomMsgConnectMicBean) {

    }

    @Override
    public void linkMickStart() {

    }

    @Override
    public void linkMicHangup() {

    }

    @Override
    public void linkMicHangupByOwn() {

    }

    @Override
    public void linkMicStop() {

    }

    private void setLiveInfoLog(String rtmpUrl, String type) {

        LiveInfoLogBean.getInstance().getLiveChatAnalytics().time = LiveUtils.getLiveTime();
        LiveInfoLogBean.getInstance().getLiveChatAnalytics().liveUserId = String.valueOf(liveRoomBean.user.id);
        LiveInfoLogBean.getInstance().getLiveChatAnalytics().roomId = liveRoomBean.liveId;
        LiveInfoLogBean.getInstance().getLiveChatAnalytics().relaVersion = BuildConfig.VERSION_NAME;
        LiveInfoLogBean.getInstance().getLiveChatAnalytics().network = NetworkUtils.getAPNType();

        LiveInfoLogBean.getInstance().getLivePlayAnalytics().time = LiveUtils.getLiveTime();
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().userId = Utils.getMyUserId();
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().liveUserId = String.valueOf(liveRoomBean.user.id);
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().logType = type;
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().inUrl = rtmpUrl;
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().outUrl = null;
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().cdnDomain = rtmpUrl;
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().relaVersion = BuildConfig.VERSION_NAME;
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().network = NetworkUtils.getAPNType();
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().roomId = liveRoomBean.liveId;
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().rtmpUrlSort = ShareFileUtils.getInt(ShareFileUtils.RTMP_URL_SORT, -1);
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().forceServerLiveUrl = liveRoomBean.forceServerLiveUrl;

        if (liveRoomBean.agora != null && liveRoomBean.agora.channelId != null) {
            LiveInfoLogBean.getInstance().getLivePlayAnalytics().channelId = liveRoomBean.agora.channelId;
        }

        LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getLivePlayAnalytics());

    }

}
