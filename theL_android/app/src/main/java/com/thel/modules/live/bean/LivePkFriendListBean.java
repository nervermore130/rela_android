package com.thel.modules.live.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by waiarl on 2017/11/29.
 * 直播Pk列表bean
 */

public class LivePkFriendListBean {
    /**
     * pk失败等待时间，单位  秒
     */
    public int pkFailAwaitTime;
    /**
     * 连麦失败等待时间，单位 秒
     */
    public int linkMicFailAwaitTime;

    public List<LivePkFriendBean> list = new ArrayList<>();
}
