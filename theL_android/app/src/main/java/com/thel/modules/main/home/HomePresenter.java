package com.thel.modules.main.home;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by liuyun on 2017/9/18.
 */

public class HomePresenter implements HomeContract.Presenter {

    private CompositeDisposable mDisposable;

    private HomeContract.View mHomeView;


    public HomePresenter(HomeContract.View mHomeView) {

        this.mHomeView = mHomeView;

        mDisposable = new CompositeDisposable();

        this.mHomeView.setPresenter(this);

    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {

    }

}
