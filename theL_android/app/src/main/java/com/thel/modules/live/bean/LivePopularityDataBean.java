package com.thel.modules.live.bean;

import com.thel.base.BaseDataBean;

import java.util.List;

/**
 * Created by lingwei on 2017/11/19.
 */

public class LivePopularityDataBean extends BaseDataBean {
    /**
     * data : {"anchorToday":[{"id":"232763","score":"1513.15","nickName":"方丈","avatar":"http://static.rela.me/app/avatar/232763/92f3ca70f87ecb41fe74bd5bb4e11f8a.jpg"}],"anchorWeek":[],"guardToday":[],"guardWeek":[],"topLink":"http://www.rela.me/act/anchorlist"}
     */


    /**
     * anchorToday : [{"id":"232763","score":"1513.15","nickName":"方丈","avatar":"http://static.rela.me/app/avatar/232763/92f3ca70f87ecb41fe74bd5bb4e11f8a.jpg"}]
     * anchorWeek : []
     * guardToday : []
     * guardWeek : []
     * topLink : http://www.rela.me/act/anchorlist
     */

    public String topLink;
    public List<AnchorTodayBean> anchorToday;
    public List<AnchorTodayBean> anchorWeek;
    public List<AnchorTodayBean> guardToday;
    public List<AnchorTodayBean> guardWeek;

    public static class AnchorTodayBean {
        /**
         * id : 232763
         * score : 1513.15
         * nickName : 方丈
         * avatar : http://static.rela.me/app/avatar/232763/92f3ca70f87ecb41fe74bd5bb4e11f8a.jpg
         */

        public String id;
        public String score;
        public String nickName;
        public String avatar;
        public int isCloaking;
    }
}
