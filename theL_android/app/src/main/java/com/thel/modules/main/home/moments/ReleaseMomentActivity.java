package com.thel.modules.main.home.moments;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.reflect.TypeToken;
import com.growingio.android.sdk.collection.GrowingIO;
import com.thel.R;
import com.thel.TagBean;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.bean.ContactBean;
import com.thel.bean.FirstHotTopicBean;
import com.thel.bean.LiveInfoLogBean;
import com.thel.bean.MentionedUserBean;
import com.thel.bean.ReleaseMomentListBean;
import com.thel.bean.me.MyCircleFriendListBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.growingio.GrowingIoConstant;
import com.thel.modules.preview_image.UserInfoPhotoActivity;
import com.thel.modules.preview_video.VideoPreviewActivity;
import com.thel.modules.select_contacts.SelectContactsActivity;
import com.thel.modules.select_contacts.SelectUserinfoAdapter;
import com.thel.modules.select_image.SelectLocalImagesActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.service.ReleaseMomentsService;
import com.thel.ui.widget.MyPagerGalleryView;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.decoration.DefaultItemDivider;
import com.thel.utils.AppInit;
import com.thel.utils.DialogUtil;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.GsonUtils;
import com.thel.utils.ImageUtils;
import com.thel.utils.L;
import com.thel.utils.PermissionUtil;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SharedPrefUtils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.UMShareAPI;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import video.com.relavideolibrary.RelaVideoSDK;
import video.com.relavideolibrary.Utils.Constant;
import video.com.relavideolibrary.onRelaVideoActivityResultListener;


/**
 * 进入ReleaseMomentActivity，必须设置对应类型
 */
public class ReleaseMomentActivity extends BaseActivity implements View.OnClickListener, onRelaVideoActivityResultListener {

    public static final String RELEASE_TYPE_PHOTOS = "photos";
    public static final String RELEASE_TYPE_TEXT = "text";
    public static final String RELEASE_TYPE_VIDEO = "video";
    public static final String RELEASE_TYPE_LOVE = "love";

    @BindView(R.id.lin_back)
    LinearLayout lin_back;

    @BindView(R.id.title)
    TextView title;

    @BindView(R.id.img_done)
    TextView img_done;

    @BindView(R.id.container)
    LinearLayout container;

    @BindView(R.id.container1)
    LinearLayout container1;

    private ImageView shareToLogo, mention_arrow;

    private RelativeLayout mention_content, shareToContent, add_topic, rel_top;

    private TextView shareToType, add_topic_txt, txt_love_days, txt_fall_in_love, adgallerytxt;

    private RecyclerView selectedPicsRecyclerView, portrait;

    private EditText addText;

    private LinearLayout ovalLayout;

    private SimpleDraweeView add, img_avatar_right, img_avatar_left, img_background;

    private MyPagerGalleryView gallery;

    private Integer[] shareIconArr;

    private int shareToPostion = 0;

    private List<String> shareToTipsList, shareToList;

    private List<PicBean> selectedPics = new ArrayList<>();

    private SelectPicsRecyclerViewAdapter selectPicsRecyclerViewAdapter;

    // 判断是不是已经选择了一张以上图片，因为点击按钮也是放在recyclerview中的，所以当数据源的count > 1时，才表示已经选择了图片
    private final int HAVE_SELECTED_PIC_COUNT = 1;

    private final int SELECT_PIC_LIMIT = 9;

    private Handler mHandler = new Handler(Looper.getMainLooper());

    private String choosedTag = "", releaseType = "", takePhotoPath;

    private ArrayList<ContactBean> mentionList = new ArrayList<>();

    private SelectUserinfoAdapter selectUserinfoAdapter;

    // 防止多次点击选择图片
    private boolean isSelectingPhoto = false;

    // 选择的图片地址
    private String bgPicUrl;

    private MyCircleFriendListBean.Paternerd myCirclePartnerBean = null;

    // 这个标识用来判断刚进来是不是录制了视频，如果没有录制，则关闭发布日志页面，如果录制过了，则留在发布日志页面
    private boolean hasRecordVideo = false;

    // 要发布的视频文件路径和缩略图
    private String videoPath;
    private String videoThumnail;
    private String videoMainColor;
    //视频长宽
    private int pixelWidth = 540;
    private int pixelHeight = 540;
    // 播放时长
    private int playTime;

    private MomentsBean mMomentsBean;

    private TextWatcher addTextTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence s, int i, int i1, int i2) {
            String str = s.toString().trim();
            if (str.length() >= getResources().getInteger(R.integer.moment_word_count_limit)) {
                DialogUtil.showToastShort(ReleaseMomentActivity.this, getString(R.string.updatauserinfo_activity_intro_hint2, getResources().getInteger(R.integer.moment_word_count_limit)));
            }
            if (!releaseType.equals(RELEASE_TYPE_LOVE) && !releaseType.equals(RELEASE_TYPE_VIDEO)) {
                if (str.length() > 0) {
                    setDoneButtonEnabled(true);
                } else if (selectedPics.size() <= HAVE_SELECTED_PIC_COUNT) {
                    setDoneButtonEnabled(false);
                }
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
            String str = s.toString();
            s.setSpan(new ForegroundColorSpan(Color.BLACK), 0, str.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
            if (!TextUtils.isEmpty(choosedTag) && s.toString().contains(choosedTag)) {
                s.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ReleaseMomentActivity.this, R.color.tag_color)), str.indexOf(choosedTag), str.indexOf(choosedTag) + choosedTag.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
            }
        }
    };
    private ReleaseBroadcast releaseBroadcast;
    private ArrayList<TagBean> tagList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //VmPolicy方式处理FileUriExposedException
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        setContentView(R.layout.activity_release_moment);

        ButterKnife.bind(this);

        releaseType = getIntent().getStringExtra(TheLConstants.BUNDLE_KEY_RELEASE_TYPE);

        mMomentsBean = (MomentsBean) getIntent().getSerializableExtra(TheLConstants.BUNDLE_KEY_MOMENT_BEAN);

        releaseBroadcast = new ReleaseBroadcast();

        registerReceiver(releaseBroadcast, new IntentFilter(TheLConstants.BROADCAST_ACTION_SELECT_TAG));

        dynamicInflateView();

        initEvent();

        initData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(this.getClass().getName());
        MobclickAgent.onResume(this);
//        if (addText != null) {
//            addText.setFocusable(true);
//            addText.setFocusableInTouchMode(true);
//            addText.requestFocus();
//            mHandler.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    ViewUtils.showSoftInput(ReleaseMomentActivity.this, addText);
//                }
//            }, 200);
//        }
        isSelectingPhoto = false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(this.getClass().getName());
        MobclickAgent.onPause(this);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            close();
        }
        return true;
    }

    @Override
    public void finish() {
        ViewUtils.hideSoftInput(this, addText);
        super.finish();
    }

    @Override
    protected void onDestroy() {
        if (RELEASE_TYPE_LOVE.equals(releaseType)) {
            // 回收所有图片
            gallery.freeBitmapFromIndex(-1);
        }
        if (releaseBroadcast != null)
            unregisterReceiver(releaseBroadcast);
        super.onDestroy();
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    @SuppressLint("NewApi")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == RESULT_OK) {
                switch (requestCode) {
                    case TheLConstants.BUNDLE_CODE_WRITE_MOMENT_ACTIVITY:
                        break;
                    case TheLConstants.BUNDLE_CODE_SHARE_PERMISSION:
                        shareToPostion = data.getIntExtra("position", 0) - 1;
                        shareToType.setText(shareToList.get(shareToPostion));
//                        shareToLogo.setImageResource(shareIconArr[shareToPostion]);
                        if (!releaseType.equals(RELEASE_TYPE_LOVE)) {
                            if (shareToPostion == 2) {//当是自己,并且不是发布话题的时候，是不能提及的
                                mentionList.clear();
                                refreshMentionList();
                                mention_content.setVisibility(View.GONE);
                            } else {
                                mention_content.setVisibility(View.VISIBLE);
                            }
                        }
                        break;
                    default:
                        break;
                }
            } else if (resultCode == RESULT_CANCELED) {// 取消录制视频
                if (RELEASE_TYPE_VIDEO.equals(releaseType) && !hasRecordVideo)
                    finish();
            } else if (resultCode == TheLConstants.RESULT_CODE_TAKE_PHOTO) {// 拍摄照片
                takePhotoPath = data.getStringExtra(TheLConstants.BUNDLE_KEY_IMAGE_OUTPUT_PATH);
                if (releaseType.equals(RELEASE_TYPE_LOVE)) {
                    if (!TextUtils.isEmpty(takePhotoPath)) {
                        bgPicUrl = ImageUtils.handlePhoto1(takePhotoPath, TheLConstants.F_TAKE_PHOTO_ROOTPATH, ImageUtils.getPicName(), false, TheLConstants.MAX_PIC_WIDTH, TheLConstants.MAX_PIC_HEIGHT, TheLConstants.PIC_QUALITY, 3);
                        if (releaseType.equals(RELEASE_TYPE_LOVE)) {
                            img_background.setImageURI(Uri.parse(TheLConstants.FILE_PIC_URL + bgPicUrl));
                            adgallerytxt.setVisibility(View.INVISIBLE);
                        }
                    }
                } else {
                    if (!TextUtils.isEmpty(takePhotoPath)) {
                        LiveInfoLogBean.getInstance().getReleaseMomentFailedAnalytics().reason += "\n添加一张拍摄的照片";
                        selectedPhoto(takePhotoPath);
                    } else {
                        LiveInfoLogBean.getInstance().getReleaseMomentFailedAnalytics().reason += "\n拍摄照片失败";
                        Toast.makeText(this, getString(R.string.info_rechoise_photo), Toast.LENGTH_SHORT).show();
                    }
                }
            } else if (resultCode == TheLConstants.RESULT_CODE_WRITE_MOMENT_DELETE_PICTURE) {// 删除图片
                if (data != null) {
                    final ArrayList<Integer> remainIndexes = data.getIntegerArrayListExtra(TheLConstants.BUNDLE_KEY_INDEX);
                    final ArrayList<PicBean> remainPicBeans = new ArrayList<>();
                    if (remainIndexes != null) {// 如果有删除图片
                        if (selectedPics.get(0).isSelectBtn) {// 如果没选满6张
                            remainPicBeans.add(selectedPics.get(0));
                            for (Integer i : remainIndexes) {
                                remainPicBeans.add(selectedPics.get(i + 1));
                            }
                        } else {// 如果选满6张
                            for (Integer i : remainIndexes) {
                                remainPicBeans.add(selectedPics.get(i));
                            }
                            if (remainPicBeans.size() < SELECT_PIC_LIMIT)
                                remainPicBeans.add(0, new PicBean(true));
                        }
                    }
                    selectedPics.clear();
                    selectedPics.addAll(remainPicBeans);
                    selectPicsRecyclerViewAdapter.notifyDataSetChanged();
                    if (selectedPics.size() == 1)
                        setDoneButtonEnabled(false);
                }
            } else if (resultCode == TheLConstants.RESULT_CODE_WRITE_MOMENT_DELETE_VIDEO) {// 删除视频
                add.getHierarchy().setPlaceholderImage(R.color.transparent);
                add.setController(null);
                videoPath = "";
                videoThumnail = "";
                playTime = 0;
                if (TextUtils.isEmpty(addText.getText().toString())) {
                    setDoneButtonEnabled(false);
                }
            } else if (resultCode == TheLConstants.RESULT_CODE_WRITE_MOMENT_SELECT_CONTACT) {// 选择@联系人
                if (data.getSerializableExtra(TheLConstants.BUNDLE_KEY_FRIENDS_LIST) != null) {
                    mentionList.clear();
                    mentionList.addAll((ArrayList<ContactBean>) data.getSerializableExtra(TheLConstants.BUNDLE_KEY_FRIENDS_LIST));
                    Collections.reverse(mentionList);
                    refreshMentionList();
                }
            } else if (resultCode == TheLConstants.RESULT_CODE_SELECT_LOCAL_IMAGE) {// 选择了图片
                String photoPath = data.getStringExtra(TheLConstants.BUNDLE_KEY_LOCAL_IMAGE_PATH);

                if (!TextUtils.isEmpty(photoPath)) {
                    if (releaseType.equals(RELEASE_TYPE_LOVE)) {
                        bgPicUrl = ImageUtils.handlePhoto1(photoPath, TheLConstants.F_TAKE_PHOTO_ROOTPATH, ImageUtils.getPicName(), false, TheLConstants.MAX_PIC_WIDTH, TheLConstants.MAX_PIC_HEIGHT, TheLConstants.PIC_QUALITY, 3);
                        if (releaseType.equals(RELEASE_TYPE_LOVE)) {
                            img_background.setImageURI(Uri.parse(TheLConstants.FILE_PIC_URL + bgPicUrl));
                            adgallerytxt.setVisibility(View.INVISIBLE);
                        }
                    } else {

                        LiveInfoLogBean.getInstance().getReleaseMomentFailedAnalytics().reason += "\n 添加一张相册图片成功";

                        selectedPhoto(photoPath);
                    }
                } else {

                    LiveInfoLogBean.getInstance().getReleaseMomentFailedAnalytics().reason += "\n 选择图片失败";

                    Toast.makeText(this, getString(R.string.info_rechoise_photo), Toast.LENGTH_SHORT).show();
                }

            } else if (resultCode == TheLConstants.RESULT_CODE_WRITE_MOMENT_SELECT_TOPIC) {// 选择话题
                String lastChoosedTag = choosedTag;
                choosedTag = data.getStringExtra(TheLConstants.BUNDLE_KEY_TOPIC_NAME);
                if (!TextUtils.isEmpty(choosedTag)) {

                    add_topic_txt.setText(choosedTag);

                    SpannableString sp = new SpannableString(choosedTag);
                    sp.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ReleaseMomentActivity.this, R.color.tag_color)), 0, sp.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                    int index;
                    Editable edit;
                    index = addText.getSelectionStart();// 获取光标所在位置
                    edit = addText.getEditableText();// 获取EditText的文字
                    // 先删除之前已经选择过的标签
                    if (!TextUtils.isEmpty(lastChoosedTag) && edit.toString().contains(lastChoosedTag)) {//如果输入框内的文字包含之前选择的话题，则替换第一个
                        edit.replace(edit.toString().indexOf(lastChoosedTag), edit.toString().indexOf(lastChoosedTag) + lastChoosedTag.length(), sp);
                    } else {
                        if (index < 0 || index >= edit.length()) {
                            edit.append(sp);
                        } else {
                            edit.insert(index, sp);// 光标所在位置插入文字
                        }
                    }
                }
            } else if (resultCode == TheLConstants.RESULT_CODE_CANCEL_RELEASE_MOMENT) {
                if (RELEASE_TYPE_PHOTOS.equals(releaseType)) {
                    finish();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        ViewUtils.preventViewMultipleClick(v, 1000);
        switch (v.getId()) {
            case R.id.img_done:
                releaseMoment();
                break;
            case R.id.add:    // 上传第一张照片
                if (RELEASE_TYPE_VIDEO.equals(releaseType)) {
                    if (TextUtils.isEmpty(videoPath)) {
                        PermissionUtil.requestCameraPermission(ReleaseMomentActivity.this, new PermissionUtil.PermissionCallback() {
                            @Override
                            public void granted() {
                                PermissionUtil.requestStoragePermission(ReleaseMomentActivity.this, new PermissionUtil.PermissionCallback() {
                                    @Override
                                    public void granted() {
                                        RelaVideoSDK.startVideoGalleryActivity(ReleaseMomentActivity.this, ReleaseMomentActivity.this);

                                    }

                                    @Override
                                    public void denied() {
                                        ReleaseMomentActivity.this.finish();
                                    }

                                    @Override
                                    public void gotoSetting() {
                                        ReleaseMomentActivity.this.finish();
                                    }
                                });
                            }

                            @Override
                            public void denied() {
                                ReleaseMomentActivity.this.finish();
                            }

                            @Override
                            public void gotoSetting() {
                                ReleaseMomentActivity.this.finish();
                            }
                        });
                    } else {

                        Intent i = new Intent(this, VideoPreviewActivity.class);
                        i.putExtra("url", videoPath);
                        startActivityForResult(i, TheLConstants.BUNDLE_CODE_WRITE_MOMENT_ACTIVITY);
                    }
                }
                break;
            case R.id.lin_back:
                close();
                break;
            case R.id.mention_content:
                if (releaseType.equals(RELEASE_TYPE_LOVE))
                    return;
                Intent intent = new Intent(ReleaseMomentActivity.this, SelectContactsActivity.class);
                intent.putExtra(TheLConstants.BUNDLE_KEY_FRIENDS_LIST, mentionList);
                startActivityForResult(intent, TheLConstants.BUNDLE_CODE_WRITE_MOMENT_ACTIVITY);
                break;
            case R.id.share_to_content:
                Intent intentShare = new Intent(this, MomentPermissionActivity.class);
                intentShare.putExtra("position", shareToPostion + 1);
                startActivityForResult(intentShare, TheLConstants.BUNDLE_CODE_SHARE_PERMISSION);
                break;
            case R.id.add_topic:
                FlutterRouterConfig.Companion.gotoTagList(tagList);
                break;
        }
    }

    private void refreshMentionList() {
        if (selectUserinfoAdapter == null) {
            selectUserinfoAdapter = new SelectUserinfoAdapter(this, mentionList);
            portrait.addItemDecoration(new DefaultItemDivider(TheLApp.getContext(), LinearLayoutManager.HORIZONTAL, R.color.transparent, 10, true, true));

            //设置布局管理器
            portrait.setLayoutManager(new LinearLayoutManager(TheLApp.getContext(), LinearLayoutManager.HORIZONTAL, false));
            //如果可以确定每个item的高度是固定的，设置这个选项可以提高性能
            portrait.setHasFixedSize(true);
            portrait.setAdapter(selectUserinfoAdapter);
        } else
            selectUserinfoAdapter.notifyDataSetChanged();
    }

    private void initEvent() {

        LiveInfoLogBean.getInstance().clearReleaseMoment();

        if (!TextUtils.isEmpty(releaseType)) {
            switch (releaseType) {
                case RELEASE_TYPE_PHOTOS:

                    LiveInfoLogBean.getInstance().getReleaseMomentFailedAnalytics().reason = "开始发送图片日志";

                    LiveInfoLogBean.getInstance().getReleaseMomentFailedAnalytics().logType = "release_moment_failed";

                    if (mMomentsBean == null) {

                        LiveInfoLogBean.getInstance().getReleaseMomentFailedAnalytics().reason += "\n跳转到图片选择页";

                        Intent intent = new Intent(ReleaseMomentActivity.this, SelectLocalImagesActivity.class);
                        intent.putExtra(TheLConstants.BUNDLE_KEY_SELECT_AMOUNT, SELECT_PIC_LIMIT);
                        intent.putExtra("direct_release", true);// 控制如果不选择照片或音乐，则直接关闭发布日志页面
                        startActivityForResult(intent, TheLConstants.BUNDLE_CODE_WRITE_MOMENT_ACTIVITY);
                    } else {
                        selectedPhoto(mMomentsBean.imageUrl);
                        if (mMomentsBean.momentsText != null) {
                            addText.setText(mMomentsBean.momentsText);
                        }
                    }
                    break;
                case RELEASE_TYPE_TEXT:
                    if (mMomentsBean != null && mMomentsBean.momentsText != null) {
                        addText.setText(mMomentsBean.momentsText);
                    }
                    break;
                case RELEASE_TYPE_VIDEO:
                    if (mMomentsBean == null) {
                        PermissionUtil.requestCameraPermission(ReleaseMomentActivity.this, new PermissionUtil.PermissionCallback() {
                            @Override
                            public void granted() {
                                PermissionUtil.requestStoragePermission(ReleaseMomentActivity.this, new PermissionUtil.PermissionCallback() {
                                    @Override
                                    public void granted() {
                                        RelaVideoSDK.startVideoGalleryActivity(ReleaseMomentActivity.this, ReleaseMomentActivity.this);
                                    }

                                    @Override
                                    public void denied() {
                                        ReleaseMomentActivity.this.finish();
                                    }

                                    @Override
                                    public void gotoSetting() {
                                        ReleaseMomentActivity.this.finish();
                                    }
                                });
                            }

                            @Override
                            public void denied() {
                                ReleaseMomentActivity.this.finish();
                            }

                            @Override
                            public void gotoSetting() {
                                ReleaseMomentActivity.this.finish();
                            }
                        });
                        SharedPrefUtils.setBoolean(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.VIDEO_GUIDE, true);
                    } else {
                        initVideoUI(mMomentsBean);
                        if (mMomentsBean.momentsText != null) {
                            addText.setText(mMomentsBean.momentsText);
                        }
                    }
                    break;
                case RELEASE_TYPE_LOVE:
                    myCirclePartnerBean = (MyCircleFriendListBean.Paternerd) getIntent().getSerializableExtra("partner");

                    if (myCirclePartnerBean != null) {

                        img_avatar_right.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(myCirclePartnerBean.paternerAvatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE)));
                        img_avatar_left.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(myCirclePartnerBean.userAvatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE)));
                        txt_fall_in_love.setText(String.format(getString(R.string.write_love_moment_act_anniversary), myCirclePartnerBean.userNickName, myCirclePartnerBean.paternerNickName));
                    }
                    String loveDays;
                    try {
                        int days = Integer.parseInt(myCirclePartnerBean.days);
                        if (days == 1) {
                            loveDays = myCirclePartnerBean.days + TheLApp.getContext().getString(R.string.my_circle_activity_day_odd);
                        } else {
                            loveDays = myCirclePartnerBean.days + TheLApp.getContext().getString(R.string.my_circle_activity_day_even);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        if (myCirclePartnerBean != null) {
                            loveDays = myCirclePartnerBean.days + TheLApp.getContext().getString(R.string.my_circle_activity_day_even);
                        } else {
                            loveDays = "";
                        }
                    }
                    txt_love_days.setText(loveDays);

                    choosedTag = getString(R.string.write_love_moment_act_tag);
                    TagBean tagBean = new TagBean();
                    tagBean.topicId = 376844;
                    tagBean.topicName = choosedTag;
                    tagBean.isSelect = true;
                    tagList.add(tagBean);
                    add_topic_txt.setText(choosedTag);

                    ContactBean contactBean = new ContactBean();
                    if (myCirclePartnerBean != null) {
                        contactBean.userId = myCirclePartnerBean.paternerId + "";
                        contactBean.avatar = myCirclePartnerBean.paternerAvatar;
                        contactBean.nickName = myCirclePartnerBean.paternerNickName;
                    }

                    contactBean.nickNamePinYin = "";
                    mentionList.add(contactBean);
                    refreshMentionList();

                    gallery.start(TheLApp.getContext(), null, new int[]{R.mipmap.img_memorial_sticker_00, R.mipmap.img_memorial_sticker_01, R.mipmap.img_memorial_sticker_02, R.mipmap.img_memorial_sticker_03, R.mipmap.img_memorial_sticker_04, R.mipmap.img_memorial_sticker_05, R.mipmap.img_memorial_sticker_06, R.mipmap.img_memorial_sticker_07, R.mipmap.img_memorial_sticker_08, R.mipmap.img_memorial_sticker_09, R.mipmap.img_memorial_sticker_10}, 0, ovalLayout, R.drawable.dot_focused, R.drawable.dot_normal, adgallerytxt, new String[]{}, ViewGroup.LayoutParams.MATCH_PARENT);

                    gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            if (!isSelectingPhoto) {
                                isSelectingPhoto = true;
                                startActivityForResult(new Intent(ReleaseMomentActivity.this, SelectLocalImagesActivity.class), TheLConstants.BUNDLE_CODE_SELECT_PHOTO);
                            }
                        }
                    });

                    setDoneButtonEnabled(true);
                    break;
            }
        }

    }

    private void initVideoUI(MomentsBean momentsBean) {

        hasRecordVideo = true;
        videoPath = momentsBean.videoUrl;
        playTime = momentsBean.playTime;
        pixelWidth = momentsBean.pixelWidth;
        pixelHeight = momentsBean.pixelHeight;
        videoThumnail = momentsBean.thumbnailUrl;
        videoMainColor = momentsBean.videoColor;

        add.getHierarchy().setPlaceholderImage(R.drawable.bg_waterfull_item_shape);
        add.setImageURI(Uri.parse(TheLConstants.FILE_PIC_URL + videoThumnail));
        setDoneButtonEnabled(true);
    }

    /**
     * 根据releaseType动态加载布局
     */
    private void dynamicInflateView() {

        if (!TextUtils.isEmpty(releaseType)) {
            View release_moment_edittext = View.inflate(this, R.layout.release_moment_edittext, null);
            selectedPicsRecyclerView = release_moment_edittext.findViewById(R.id.rv_pics);
            add = release_moment_edittext.findViewById(R.id.add);
            add.setOnClickListener(this);
            addText = release_moment_edittext.findViewById(R.id.write_moment_add_text);
            addText.addTextChangedListener(addTextTextWatcher);

            View release_moment_option = View.inflate(this, R.layout.release_moment_option, null);
            add_topic = release_moment_option.findViewById(R.id.add_topic);
            add_topic.setOnClickListener(this);
            add_topic_txt = release_moment_option.findViewById(R.id.add_topic_txt);
            shareToContent = release_moment_option.findViewById(R.id.share_to_content);
            shareToContent.setOnClickListener(this);
            shareToType = release_moment_option.findViewById(R.id.share_to_txt);
            mention_content = release_moment_option.findViewById(R.id.mention_content);
            mention_content.setOnClickListener(this);
            portrait = release_moment_option.findViewById(R.id.portrait);
            shareToLogo = release_moment_option.findViewById(R.id.share_to_logo);
            mention_arrow = release_moment_option.findViewById(R.id.mention_arrow);

            switch (releaseType) {
                case RELEASE_TYPE_PHOTOS:
                    initSelectedRecyclerView();
                    GrowingIOUtil.setViewInfo(img_done, "photo");
                    container.addView(release_moment_edittext);
                    container.addView(release_moment_option);
                    break;
                case RELEASE_TYPE_TEXT:
                    GrowingIOUtil.setViewInfo(img_done, "text");
                    release_moment_edittext.findViewById(R.id.rel_pics).setVisibility(View.GONE);
                    container.addView(release_moment_edittext);
                    container.addView(release_moment_option);
                    break;
                case RELEASE_TYPE_VIDEO:
                    GrowingIOUtil.setViewInfo(img_done, "video");

                    add.setVisibility(View.VISIBLE);
                    selectedPicsRecyclerView.setVisibility(View.GONE);
                    container.addView(release_moment_edittext);
                    container.addView(release_moment_option);
                    break;
                case RELEASE_TYPE_LOVE:
                    GrowingIOUtil.setViewInfo(img_done, "love");

                    View release_moment_love = View.inflate(this, R.layout.release_moment_love, null);
                    addText = release_moment_love.findViewById(R.id.write_moment_add_text);
                    addText.addTextChangedListener(addTextTextWatcher);
                    img_avatar_left = release_moment_love.findViewById(R.id.img_avatar_left);
                    img_avatar_right = release_moment_love.findViewById(R.id.img_avatar_right);
                    txt_fall_in_love = release_moment_love.findViewById(R.id.txt_fall_in_love);
                    txt_love_days = release_moment_love.findViewById(R.id.txt_love_days);
                    ovalLayout = release_moment_love.findViewById(R.id.ovalLayout);
                    rel_top = release_moment_love.findViewById(R.id.rel_top);
                    rel_top.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, AppInit.displayMetrics.widthPixels));
                    gallery = release_moment_love.findViewById(R.id.gallery);
                    img_background = release_moment_love.findViewById(R.id.img_background);
                    adgallerytxt = release_moment_love.findViewById(R.id.adgallerytxt);

                    mention_arrow.setVisibility(View.INVISIBLE);
                    title.setText(getString(R.string.memorial_day));

                    container.addView(release_moment_love);
                    container.addView(release_moment_option);
                    break;
            }
        }

        if (shareToType != null) {
            shareToType.setText(getResources().getStringArray(R.array.moments_share_to)[0]);

        }
        String topicName = getIntent().getStringExtra(TheLConstants.BUNDLE_KEY_TOPIC_NAME);
        if (addText != null && !TextUtils.isEmpty(topicName)) {// 从话题页面过来的
            addText.setText(topicName);
            addText.setSelection(topicName.length());
        }
        lin_back.setOnClickListener(this);
    }

    private void initData() {

//        RequestBusiness.getInstance().getFirstHotTopic().onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<FirstHotTopicBean>() {
//            @Override
//            public void onNext(FirstHotTopicBean baseDataBean) {
//                super.onNext(baseDataBean);
//                if (baseDataBean != null && baseDataBean.topicName != null && add_topic_txt != null)
//                    add_topic_txt.setText(baseDataBean.topicName);
//            }
//        });

        if (shareIconArr == null)
            shareIconArr = new Integer[]{R.mipmap.icn_feed_share_all, R.mipmap.icn_feed_share_friends, R.mipmap.icn_feed_share_private, R.mipmap.icn_feed_share_anonymous};

        if (shareToList == null) {
            String[] shareToArr = this.getResources().getStringArray(R.array.moments_share_to);
            shareToList = Arrays.asList(shareToArr);
        }

        if (shareToTipsList == null) {
            String[] shareToTipsArr = this.getResources().getStringArray(R.array.moments_share_to_tips);
            shareToTipsList = Arrays.asList(shareToTipsArr);
        }
    }

    private void initSelectedRecyclerView() {
        final PicBean picBean = new PicBean();
        picBean.isSelectBtn = true;
        selectedPics.add(picBean);
        selectPicsRecyclerViewAdapter = new SelectPicsRecyclerViewAdapter(selectedPics);
        selectPicsRecyclerViewAdapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                if (position == 0) {
                    if (selectedPics.size() < SELECT_PIC_LIMIT || selectedPics.get(0).isSelectBtn) {// 未选满6张，点选图片
                        Intent intent = new Intent(ReleaseMomentActivity.this, SelectLocalImagesActivity.class);
                        intent.putExtra(TheLConstants.BUNDLE_KEY_SELECT_AMOUNT, SELECT_PIC_LIMIT - selectedPics.size() + 1);
                        startActivityForResult(intent, TheLConstants.BUNDLE_CODE_WRITE_MOMENT_ACTIVITY);
                    } else {// 已选满6张，点击进入编辑
                        editSelectedPhotos(position);
                    }
                } else {
                    editSelectedPhotos(position);
                }
            }
        });
        selectedPicsRecyclerView.setVisibility(View.VISIBLE);
        selectedPicsRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        selectedPicsRecyclerView.setAdapter(selectPicsRecyclerViewAdapter);
        selectedPicsRecyclerView.addItemDecoration(new DefaultItemDivider(TheLApp.getContext(), LinearLayoutManager.HORIZONTAL, R.color.transparent, 10, true, true));
    }

    private void close() {
        if (addText != null && (!TextUtils.isEmpty(addText.getText().toString().trim()) || selectedPics.size() > HAVE_SELECTED_PIC_COUNT || !TextUtils.isEmpty(videoPath))) {
            DialogUtil.showConfirmDialog(ReleaseMomentActivity.this, null, getString(R.string.moments_write_moment_discard_tip), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    finish();
                }
            });
        } else {
            finish();
        }
    }

    private void editSelectedPhotos(int index) {
        Intent i = new Intent(ReleaseMomentActivity.this, UserInfoPhotoActivity.class);
        i.putExtra("fromPage", ReleaseMomentActivity.class.getName());
        ArrayList<String> photoUrls = new ArrayList<>();

        L.d("ReleaseMomentActivity", " selectedPics : " + selectedPics.size());

        for (PicBean pic : selectedPics) {
            if (!pic.isSelectBtn) {
                photoUrls.add(pic.localUrl);
            }
        }

        if (selectedPics.get(0).isSelectBtn) {
            i.putExtra("position", index - 1);
        } else {
            i.putExtra("position", index);
        }

        L.d("ReleaseMomentActivity", " photoUrls : " + photoUrls);

        i.putStringArrayListExtra(TheLConstants.BUNDLE_KEY_PHOTOS, photoUrls);
        startActivityForResult(i, TheLConstants.BUNDLE_CODE_WRITE_MOMENT_ACTIVITY);
    }

    /**
     * 选好图片后刷新界面
     *
     * @param path
     */
    private void selectedPhoto(String path) {
        String[] picUrlsTemp = path.split(",");
        boolean haveSelectedLegalPhoto = false;
        for (int i = 0; i < picUrlsTemp.length; i++) {
            if (ImageUtils.isImageLegal(picUrlsTemp[i], 235, 235)) {
                PicBean picBean = new PicBean();
                picBean.isSelectBtn = false;
                picBean.localUrl = zoomImage(picUrlsTemp[i]);
                selectedPics.add(picBean);
                haveSelectedLegalPhoto = true;
            } else {
                Toast.makeText(this, getString(R.string.info_illegal_photo), Toast.LENGTH_SHORT).show();
            }
        }
        if (selectedPics.size() == SELECT_PIC_LIMIT + 1)
            selectedPics.remove(0);
        if (haveSelectedLegalPhoto)
            selectPicsRecyclerViewAdapter.notifyDataSetChanged();

        if (selectedPics.size() > 1)
            setDoneButtonEnabled(true);
    }

    /**
     * 图片宽度超过1000时，先缩放再压缩
     */
    private String zoomImage(String imagePath) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true; // 只读入图边界
        BitmapFactory.decodeFile(imagePath, options);
        options.inJustDecodeBounds = false; // 读取全部图信息

        int zoom = 1;

        int width = options.outWidth;

        int height = options.outHeight;

        LiveInfoLogBean.getInstance().getReleaseMomentFailedAnalytics().reason += "\n width=" + width + " height=" + height;

        int whRadio = Math.round((float) height / (float) width);

        int hwRadio = Math.round((float) width / (float) height);

        if (whRadio > 3 || hwRadio > 3) {
            //长图GIO打点
            JSONObject obj = new JSONObject() {
                {
                    try {
                        put(GrowingIoConstant.PHOTO_TYPE, GrowingIoConstant.LONG_PHOTO);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            };
            GrowingIO.getInstance().track(GrowingIoConstant.PHOTO_COUNT, obj);
        } else {
            JSONObject obj = new JSONObject() {
                {
                    try {
                        put(GrowingIoConstant.PHOTO_TYPE, GrowingIoConstant.NO_LONG_PHOTO);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            };
            GrowingIO.getInstance().track(GrowingIoConstant.PHOTO_COUNT, obj);
        }

        int reqWidth = 1000;

        if (width > reqWidth) {
            zoom = Math.round((float) width / (float) reqWidth);
        }

        options.inSampleSize = zoom;
        Bitmap bitmap = BitmapFactory.decodeFile(imagePath, options);
        try {
            File file = new File(imagePath);
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, bos);
            bos.flush();
            bos.close();
        } catch (Exception e) {

            LiveInfoLogBean.getInstance().getReleaseMomentFailedAnalytics().reason += "\n 缩放图片报错 : " + e.getMessage();

            e.printStackTrace();
        }
        return imagePath;
    }

    private void setDoneButtonEnabled(boolean enabled) {
        if (enabled) {
            img_done.setAlpha(1.0f);
//            img_done.setImageResource(R.mipmap.btn_done);
            img_done.setOnClickListener(this);
        } else {
            img_done.setAlpha(0.5f);
//            img_done.setImageResource(R.mipmap.btn_done_disable);
            img_done.setOnClickListener(null);
        }
    }

    private void releaseMoment() {
        MomentsBean sendMoment = new MomentsBean();
        sendMoment.userId = Integer.valueOf(ShareFileUtils.getString(ShareFileUtils.ID, "0"));
        sendMoment.nickname = ShareFileUtils.getString(ShareFileUtils.USER_NAME, "");
        sendMoment.releaseTime = System.currentTimeMillis();
        sendMoment.avatar = ShareFileUtils.getString(ShareFileUtils.AVATAR, "");
        sendMoment.momentsId = System.currentTimeMillis() + MomentsBean.SEND_MOMENT_FLAG;// 预发送的日志id标识
        sendMoment.momentsText = addText.getText().toString().trim();
        if (!TextUtils.isEmpty(sendMoment.momentsText)) {// 有可能是文字日志
            sendMoment.momentsType = MomentsBean.MOMENT_TYPE_TEXT;
        } else {
            sendMoment.momentsType = MomentsBean.MOMENT_TYPE_IMAGE;
        }
        // 封装图片日志
        StringBuilder picsUrlSB = new StringBuilder();
        for (PicBean pic : selectedPics) {
            if (!pic.isSelectBtn && !TextUtils.isEmpty(pic.localUrl)) {
                picsUrlSB.append(pic.localUrl);
                picsUrlSB.append(",");
            }
        }
        if (picsUrlSB.length() > 0) {
            picsUrlSB.deleteCharAt(picsUrlSB.length() - 1);
            sendMoment.imageUrl = picsUrlSB.toString();
            if (TextUtils.isEmpty(sendMoment.momentsText)) {
                sendMoment.momentsType = MomentsBean.MOMENT_TYPE_IMAGE;
            } else {
                sendMoment.momentsType = MomentsBean.MOMENT_TYPE_TEXT_IMAGE;
            }
        }

        sendMoment.shareTo = shareToPostion == 3 ? 1 : shareToPostion + 1;
        sendMoment.secret = shareToPostion == 3 ? MomentsBean.IS_SECRET : MomentsBean.IS_NOT_SECRET;

        // 封装视频日志
        if (!TextUtils.isEmpty(videoPath) && !TextUtils.isEmpty(videoThumnail)) {
            sendMoment.momentsType = MomentsBean.MOMENT_TYPE_VIDEO;
            // 上传视频的话，将视频地址和图片地址一起放在imgsToUpload里，用逗号分隔
            sendMoment.imageUrl = videoPath + "," + videoThumnail;
            //            sendMoment.imgsToUpload = sendMoment.imageUrl;
            sendMoment.playTime = playTime;
            sendMoment.videoUrl = videoPath;
            sendMoment.thumbnailUrl = videoThumnail;
            sendMoment.videoColor = videoMainColor;
            sendMoment.pixelWidth = pixelWidth != 0 ? pixelWidth : 540;
            sendMoment.pixelHeight = pixelHeight != 0 ? pixelHeight : 540;
            try {
                sendMoment.mediaSize = new File(videoPath).length() / 1000;
            } catch (Exception e) {
                e.printStackTrace();
            }

            //通知首页添加一个本地视频数据
            Intent intent1 = new Intent();
            intent1.setAction(TheLConstants.BROADCAST_RELEASE_NEW_VIDEO_ACTION);
            TheLApp.getContext().sendBroadcast(intent1);
        }

        // 封装提及用户
        if (!mentionList.isEmpty()) {
            Collections.reverse(mentionList);
            for (ContactBean contact : mentionList) {
                MentionedUserBean atUser = new MentionedUserBean();
                atUser.userId = Integer.valueOf(contact.userId);
                atUser.nickname = contact.nickName;
                atUser.momentsId = sendMoment.releaseTime + "";
                atUser.avatar = contact.avatar;
                sendMoment.atUserList.add(atUser);
            }
        }

        if (tagList != null && !tagList.isEmpty()) {
            String tagListStr = "";
            for (int i = 0; i < tagList.size(); i++) {
                tagListStr = tagListStr + tagList.get(i).topicId + ",";
            }
            if (!TextUtils.isEmpty(tagListStr)) {
                tagListStr = tagListStr.substring(0, tagListStr.length() - 1);
            }
            sendMoment.tagList = tagListStr;
        }

        if (releaseType.equals(RELEASE_TYPE_LOVE)) {
            GrowingIOUtil.setViewInfo(img_done, "love");

            ovalLayout.setVisibility(View.INVISIBLE);
            adgallerytxt.setVisibility(View.INVISIBLE);
            Bitmap bitmap = Bitmap.createBitmap(rel_top.getWidth(), rel_top.getHeight(), Bitmap.Config.RGB_565);
            //利用bitmap生成画布
            Canvas canvas = new Canvas(bitmap);
            //把view中的内容绘制在画布上
            rel_top.draw(canvas);
            File file = new File(TheLConstants.F_TAKE_PHOTO_ROOTPATH);
            if(!file.exists() && !file.isDirectory()) file.mkdir();
            String imgPath = TheLConstants.F_TAKE_PHOTO_ROOTPATH + System.currentTimeMillis() + "_love.jpg";//七牛上传不支持文件名中有中文
            if (ImageUtils.savePic(bitmap, imgPath, TheLConstants.PIC_QUALITY)) {
                sendMoment.imageUrl = imgPath;
            }
            if (!TextUtils.isEmpty(sendMoment.momentsText)) {
                sendMoment.momentsType = MomentsBean.MOMENT_TYPE_TEXT_IMAGE;
            } else {
                sendMoment.momentsType = MomentsBean.MOMENT_TYPE_IMAGE;
            }
        }

        //将发布内容以json的形式保存本地
        String releaseContent = ShareFileUtils.getString(TheLConstants.RELEASE_CONTENT_KEY, "");
        String toJson;
        int releaseIndex;
        if (TextUtils.isEmpty(releaseContent)) {//之前没有未发布内容
            List<MomentsBean> list = new ArrayList<>();
            list.add(sendMoment);
            ReleaseMomentListBean releaseTagListBean = new ReleaseMomentListBean();
            releaseTagListBean.list = list;
            toJson = GsonUtils.createJsonString(releaseTagListBean);
            releaseIndex = 0;
        } else {//有未发布内容
            ReleaseMomentListBean releaseTagListBean = GsonUtils.getObject(releaseContent, ReleaseMomentListBean.class);
            if (releaseTagListBean != null && releaseTagListBean.list != null) {
                releaseTagListBean.list.add(sendMoment);
                toJson = GsonUtils.createJsonString(releaseTagListBean);
                releaseIndex = releaseTagListBean.list.size() - 1;
            } else {
                releaseIndex = -1;
                toJson = null;
            }
        }
        if (toJson != null && releaseIndex != -1) {
            ShareFileUtils.setString(TheLConstants.RELEASE_CONTENT_KEY, toJson);
            L.d("release", toJson);
            Intent intent = new Intent(this, ReleaseMomentsService.class);
            // 要发的日志标识，用发布时间作为标识
            intent.putExtra(ReleaseMomentsService.MOMENT_INDEX, releaseIndex);
            TheLApp.getContext().startService(intent);
        }

        finish();
    }

    @Override
    public void onRelaVideoActivityResult(Intent intent) {
        hasRecordVideo = true;
        Bundle bundle = intent.getExtras();
        videoPath = bundle.getString(video.com.relavideolibrary.Utils.Constant.BundleConstants.RESULT_VIDEO_PATH);
        playTime = Integer.parseInt(bundle.getString(video.com.relavideolibrary.Utils.Constant.BundleConstants.RESULT_VIDEO_DURATION));
        pixelWidth = Integer.parseInt(bundle.getString(video.com.relavideolibrary.Utils.Constant.BundleConstants.RESULT_VIDEO_WIDTH));
        pixelHeight = Integer.parseInt(bundle.getString(video.com.relavideolibrary.Utils.Constant.BundleConstants.RESULT_VIDEO_HEIGHT));
        videoThumnail = bundle.getString(video.com.relavideolibrary.Utils.Constant.BundleConstants.RESULT_VIDEO_THUMB);
        videoMainColor = bundle.getString(Constant.BundleConstants.RESULT_VIDEO_MAIN_COLOR);
        add.getHierarchy().setPlaceholderImage(R.drawable.bg_waterfull_item_shape);
        add.setImageURI(Uri.parse(TheLConstants.FILE_PIC_URL + videoThumnail));
        setDoneButtonEnabled(true);
    }

    class ReleaseBroadcast extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (Objects.equals(intent.getAction(), TheLConstants.BROADCAST_ACTION_SELECT_TAG)) {
                String tagListJsonString = intent.getStringExtra("tagJsonString");
                L.d("getTagList", "flutterJsonString :" + tagListJsonString);
                ArrayList<TagBean> tagListNew = GsonUtils.getGson().fromJson(tagListJsonString, new TypeToken<List<TagBean>>() {
                }.getType());
                if (tagListNew == null) tagListNew = new ArrayList<>();
                boolean exist = false;
                if (tagList.size() == 1) {
                    TagBean tagBean = tagList.get(0);//默认纪念日标签
                    if (tagBean.topicId == 376844) {
                        for (int i = 0; i < tagListNew.size(); i++) {
                            if (tagListNew.get(i).topicId == tagBean.topicId) {
                                exist = true;
                                break;
                            }
                        }
                    }
                }
                if (!exist) tagList.addAll(tagListNew);
                else tagList = tagListNew;
                if (tagList == null || tagList.size() == 0) {
                    choosedTag = "";
                    add_topic_txt.setText("");
                } else {
                    StringBuilder tagString = new StringBuilder();
                    for (int i = 0; i < tagList.size(); i++) {
                        tagString.append(tagList.get(i).topicName);
                    }
                    if (!tagString.toString().isEmpty()) {
                        choosedTag = tagString.toString();
                        if (!TextUtils.isEmpty(choosedTag)) {
                            add_topic_txt.setText(choosedTag);
                        }
                    }
                }
            }
        }
    }
}
