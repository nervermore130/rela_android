package com.thel.modules.live.surface.watch.horizontal;

import com.thel.BuildConfig;
import com.thel.bean.LiveAdInfoBean;
import com.thel.modules.live.bean.LiveRoomNetBean;
import com.thel.modules.live.bean.LiveUserCardNetBean;
import com.thel.modules.live.bean.SoftEnjoyNetBean;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.service.DefaultRequestService;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class LiveWatchHorizontalPresenter implements LiveWatchHorizontalContract.Presenter {

    private LiveWatchHorizontalContract.View view;

    private CompositeDisposable mCompositeDisposable;

    public static final int ANCHOR_CARD = 1;

    public static final int USER_CARD = 0;

    @Override public void subscribe() {

        L.d("HorizontalPresenter", " subscribe : ");

        mCompositeDisposable = new CompositeDisposable();
    }

    @Override public void unSubscribe() {

        L.d("HorizontalPresenter", " unSubscribe : ");

        if (mCompositeDisposable != null) {
            mCompositeDisposable.clear();
            mCompositeDisposable = null;
        }
    }

    public LiveWatchHorizontalPresenter(LiveWatchHorizontalContract.View view) {
        this.view = view;
        view.setPresenter(this);
    }


    @Override public void getLiveRoomDetail(String userId, String liveId) {
        final Flowable<LiveRoomNetBean> flowable = DefaultRequestService.createLiveRequestService().getLiveRoomDetail(liveId, userId);

        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<LiveRoomNetBean>() {
            @Override
            public void onNext(final LiveRoomNetBean data) {
                super.onNext(data);
                if (!hasErrorCode && data != null && data.data != null) {

                    data.data.setSoftEnjoyBean();

                    if (view != null && view.isActive()) {

                        view.initPlayer(data.data);

                        view.initUI(data.data);

                        view.initChat(data.data);

                    }


                } else if (hasErrorCode) {
                    if (data != null && data.errcode.equals("97")) {//已经被对方给屏蔽
                        if (view != null && view.isActive()) {
                            view.showBlockAlertDialog();
                        }

                    }
                }
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                if (errDataBean != null && errDataBean.errcode.equals("97")) {
                    if (view != null && view.isActive()) {
                        view.showBlockAlertDialog();
                    }
                }
            }

            @Override
            public void onComplete() {
                super.onComplete();
                if (view != null && view.isActive()) {
                    view.requestFinish();
                }
            }
        });
    }

    @Override public void getLiveUserCard(String userId, int type) {

        final Flowable<LiveUserCardNetBean> flowable = DefaultRequestService.createLiveRequestService().getLiveUserCardBean(userId);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<LiveUserCardNetBean>() {
            @Override
            public void onNext(LiveUserCardNetBean liveUserCardNetBean) {
                super.onNext(liveUserCardNetBean);
                if (view != null && view.isActive()) {
                    if (type == ANCHOR_CARD) {
                       // view.showLiveAnchorUserCard(liveUserCardNetBean.data);
                        view.showLiveUserCard(liveUserCardNetBean.data);

                    } else {
                        view.showLiveUserCard(liveUserCardNetBean.data);
                    }
                }
            }
        });

    }

    @Override public void getLiveGiftList() {

        String etag = ShareFileUtils.getString(ShareFileUtils.GIFT_ETAG, "");

        final Flowable<SoftEnjoyNetBean> flowable = DefaultRequestService.createLiveRequestService().getLiveGiftList(etag, BuildConfig.APPLICATION_ID);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<SoftEnjoyNetBean>() {
            @Override
            public void onNext(SoftEnjoyNetBean data) {

                if (data != null && data.data != null) {

                    L.d("LiveShowPresenter", " data.data : " + data.data.list.toString());

                    ShareFileUtils.setString(ShareFileUtils.GIFT_ETAG, data.data.md5);

                    if (data.data.list != null) {
                        if (data.data.list.size() <= 0) {
                            String json = ShareFileUtils.getString(ShareFileUtils.GIFT_LIST, "{}");
                            SoftEnjoyNetBean softEnjoyNetBean = GsonUtils.getObject(json, SoftEnjoyNetBean.class);
                            if (softEnjoyNetBean != null && softEnjoyNetBean.data != null) {
                                view.showLiveGiftList(softEnjoyNetBean.data);
                            }
                        } else {
                            String json = GsonUtils.createJsonString(data);
                            ShareFileUtils.setString(ShareFileUtils.GIFT_LIST, json);
                            view.showLiveGiftList(data.data);
                        }
                    }

                }


            }
        });

    }

    @Override public void getLiveAdInfo(String url) {

        L.d("getLiveAdInfo"," url : " + url);

        Flowable.just(url)
                .onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .map(new Function<String, String>() {
                    @Override
                    public String apply(String s) {
                        return executeHttpGet(s);
                    }
                }).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<String>() {
                    @Override
                    public void onSubscribe(Subscription s) {
                        s.request(1);
                    }

                    @Override
                    public void onNext(String s) {

                        L.d("getLiveAdInfo"," s : " + s);

                        LiveAdInfoBean liveAdInfoBean = GsonUtils.getObject(s, LiveAdInfoBean.class);
                        view.showAdView(liveAdInfoBean);
                    }

                    @Override
                    public void onError(Throwable t) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }

    @Override public void getSingleGiftDetail(int id) {

    }

    @Override public void initChatListUpdateTimer() {

        if (mCompositeDisposable == null) {
            mCompositeDisposable = new CompositeDisposable();
        }

        Disposable disposable = Flowable.interval(200, TimeUnit.MILLISECONDS).onBackpressureDrop().observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Long>() {
            @Override public void accept(Long aLong) {

                if (view != null && view.isActive()) {
                    view.updateChatList();
                }
            }
        });

        mCompositeDisposable.add(disposable);

    }


    public String executeHttpGet(String str) {
        String result = null;
        try {
            URL url = new URL(str);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Origin", "http://www.rela.me");
            conn.setRequestMethod("GET");
            conn.setConnectTimeout(3000);
            int code = conn.getResponseCode();
            if (code == 200) {
                InputStream is = conn.getInputStream();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                int len = 0;
                byte[] bys = new byte[1024];
                while ((len = is.read(bys)) != -1) {
                    baos.write(bys, 0, len);
                    result = new String(baos.toByteArray());
                }
                baos.close();
                is.close();
            }
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }

        return result;
    }

}
