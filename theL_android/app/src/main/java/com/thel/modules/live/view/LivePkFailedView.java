package com.thel.modules.live.view;

import android.content.Context;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.modules.live.in.LiveBaseView;

/**
 * Created by waiarl on 2017/12/15.
 * 直播PK失败方view
 */

public class LivePkFailedView extends LinearLayout implements LiveBaseView<LivePkFailedView> {
    private final Context mContext;
    private ImageView img_pk_failed;
    private TextView txt_pk_failed;
    private LinearLayout lin_pk_failed;

    public LivePkFailedView(Context context) {
        this(context, null);
    }

    public LivePkFailedView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LivePkFailedView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }

    private void init() {
        inflate(mContext, R.layout.live_pk_failed_view, this);
        img_pk_failed = findViewById(R.id.img_pk_failed);
        txt_pk_failed = findViewById(R.id.txt_pk_failed);
        lin_pk_failed = findViewById(R.id.lin_pk_failed);
    }

    public LivePkFailedView initView(String content, int id) {
        txt_pk_failed.setText(content + "");
        img_pk_failed.setImageResource(id);
        return this;
    }

    public LivePkFailedView setText(String content) {
        txt_pk_failed.setText(content + "");
        return this;

    }

    public LivePkFailedView setImageResource(int id) {
        img_pk_failed.setImageResource(id);
        return this;

    }

    public LivePkFailedView setImageVisiable(int visiable) {
        img_pk_failed.setVisibility(visiable);
        return this;

    }

    public LivePkFailedView setTextVisiablel(int visiable) {
        txt_pk_failed.setVisibility(visiable);
        return this;

    }

    @Override
    public LivePkFailedView show() {
        setVisibility(View.VISIBLE);
        return this;
    }

    @Override
    public LivePkFailedView hide() {
        setVisibility(View.GONE);
        return this;
    }

    @Override
    public LivePkFailedView destroyView() {
        setVisibility(View.GONE);
        return this;
    }

    /**
     * 对提前结束PK
     *
     * @return
     */
    public LivePkFailedView showPkHangupFailedView() {
        lin_pk_failed.setBackgroundResource(R.mipmap.live_pk_failed_bg);
        setImageResource(R.mipmap.live_pk_failed_icon);
        setText(getString(R.string.end_pk_advance));
        img_pk_failed.setVisibility(View.VISIBLE);
        txt_pk_failed.setVisibility(View.VISIBLE);
        return this;
    }

    /**
     * 在总结过程中对方失败（为挂断）
     *
     * @return
     */
    public LivePkFailedView showPkSummaryFailedView() {
        setImageResource(R.mipmap.live_pk_icn_failed);
        setText("");
        txt_pk_failed.setVisibility(View.GONE);
        return this;
    }

    /**
     * 对方总结过程中结束Pk
     *
     * @return
     */
    public LivePkFailedView showPkSummaryHangupView() {
        lin_pk_failed.setBackgroundResource(R.mipmap.live_pk_over_bg);
        setImageResource(R.mipmap.live_pk_failed_icon);
        setText(getString(R.string.end_pk));
        img_pk_failed.setVisibility(View.VISIBLE);
        txt_pk_failed.setVisibility(View.VISIBLE);
        return this;
    }


    @Override
    public boolean isAnimating() {
        return false;
    }

    @Override
    public void setAnimating(boolean isAnimating) {

    }

    @Override
    public void showShade(boolean show) {

    }

    private String getString(int id) {
        return TheLApp.getContext().getString(id);
    }
}
