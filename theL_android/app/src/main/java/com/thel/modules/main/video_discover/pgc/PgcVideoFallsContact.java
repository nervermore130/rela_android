package com.thel.modules.main.video_discover.pgc;

import com.thel.base.BasePresenter;
import com.thel.base.BaseView;
import com.thel.bean.AdBean;
import com.thel.modules.main.video_discover.videofalls.VideoMomentListBean;

import java.util.List;

/**
 * Created by waiarl on 2018/3/15.
 */

public class PgcVideoFallsContact {
    interface Presenter extends BasePresenter {
        void getRefreshData();

        void getLoadMoreData(int cursor);

        void getAdsData();

        void getRefreshPgcData();

        void getLoadMorePgcData(int cursor);
    }

    interface View extends BaseView<PgcVideoFallsContact.Presenter> {
        void showRefreshData(VideoMomentListBean videoMomentListBean);

        void showMoreData(VideoMomentListBean videoMomentListBean);

        void emptyData();

        void loadMoreFailed();

        void requestFinished();

        void showAdsData(List<AdBean.Map_list> adList);

        void showRefreshPgcData(VideoMomentListBean videoMomentListBean);

        void showMorePgcData(VideoMomentListBean videoMomentListBean);

        void emptyPgcData();

        void loadRefreshPgcFailed(int failedCursor);

        void loadMorePgcFailed(int failedCursor);

    }
}
