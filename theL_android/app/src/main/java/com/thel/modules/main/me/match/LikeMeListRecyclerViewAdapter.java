package com.thel.modules.main.me.match;

import android.net.Uri;

import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.thel.R;
import com.thel.constants.TheLConstants;
import com.thel.modules.main.me.bean.MatchBean;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;
import com.thel.utils.AppInit;
import com.thel.utils.ImageUtils;
import com.thel.utils.MomentUtils;

import java.util.List;

import static com.thel.app.TheLApp.getContext;

/**
 * 喜欢我的列表
 */

public class LikeMeListRecyclerViewAdapter extends BaseRecyclerViewAdapter<MatchBean> {

    private float photoWidth; // 图片宽度
    private float photoHeigth; // 图片高度
    private float radius; // 图片高度
    private RelativeLayout.LayoutParams layoutParams;

  /*  private HidingBraodcast hidingBraodcast;

    private class HidingBraodcast extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (TheLConstants.BROADCAST_ACTION_HIDING.equals(intent.getAction())) {
                mData.get(0).hiding = intent.getIntExtra("hiding", 0);
                notifyDataSetChanged();
            }
        }
    }
*/
/*    public void registerReceiver(Context context) {
        hidingBraodcast = new HidingBraodcast();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(TheLConstants.BROADCAST_ACTION_HIDING);
        context.registerReceiver(hidingBraodcast, intentFilter);
    }

    public void unRegisterReceiver(Context context) {
        if (hidingBraodcast != null) context.unregisterReceiver(hidingBraodcast);
    }*/

    public LikeMeListRecyclerViewAdapter(List<MatchBean> list) {
        super(R.layout.adapter_match_user_recyclerview, list);

        photoWidth = (AppInit.displayMetrics.widthPixels - getContext().getResources().getDimension(R.dimen.nearby_user_view_padding_2x) * 4) / 3;
        photoHeigth = photoWidth / 9 * 16;
        radius = getContext().getResources().getDimension(R.dimen.nearby_user_view_radius);
    }


    @Override
    protected void convert(BaseViewHolder holdView, final MatchBean userBean) {

        if (userBean.vipLevel > 0) {// vip用户
            holdView.setVisibility(R.id.img_vip, View.VISIBLE);
            if (userBean.vipLevel < TheLConstants.VIP_LEVEL_RES.length) {
                holdView.setImageUrl(R.id.img_vip, TheLConstants.RES_PIC_URL + TheLConstants.VIP_LEVEL_RES[userBean.vipLevel]);
            }
        } else {
            holdView.setVisibility(R.id.img_vip, View.GONE);

        }

        String nickName = generateNickname(userBean.nickName);
        holdView.setText(R.id.txt_nickname, nickName);

        final SimpleDraweeView imgThumb = holdView.getView(R.id.img_thumb);
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) imgThumb.getLayoutParams();
        lp.height = (int) photoHeigth;

        if (!TextUtils.isEmpty(userBean.activeTime)) {
            holdView.setText(R.id.txt_like_time, MomentUtils.getReleaseTimeShow(userBean.activeTime));

        }

        if (userBean.isSuperLike == 0) {
            holdView.setImageResource(R.id.img_like_tag, R.mipmap.btn_list_like);

        } else {
            holdView.setImageResource(R.id.img_like_tag, R.mipmap.btn_list_superlike);
            holdView.setBackgroundRes(R.id.ll_info, R.drawable.bg_who_likes_me);

        }
        if (userBean.isNewLike==1) {
            holdView.setVisibility(R.id.img_new_like, View.VISIBLE);
        }

        if (userBean.picList.size() > 0) {
            String thumb = userBean.picList.get(0).longThumbnailUrl;
            if (!TextUtils.isEmpty(thumb)) {//如果有url，设置背景色，没有，设置默认热拉色
                imgThumb.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(thumb, photoWidth, photoHeigth))).build()).setAutoPlayAnimations(true).build());
            } else {
                imgThumb.setImageResource(R.color.tab_normal);
            }

        }
        imgThumb.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadius(radius).setRoundingMethod(RoundingParams.RoundingMethod.OVERLAY_COLOR).setOverlayColor(ContextCompat.getColor(getContext(), R.color.white)));


    }

    private String generateNickname(String nickName) {
        if (TextUtils.isEmpty(nickName)) {
            return "***";
        } else {
            char codePoint = nickName.charAt(0);
            //是否包含表情符号
            if (!((codePoint == 0x0) || (codePoint == 0x9) || (codePoint == 0xA) || (codePoint == 0xD) || ((codePoint >= 0x20) && (codePoint <= 0xD7FF)) || ((codePoint >= 0xE000) && (codePoint <= 0xFFFD)) || ((codePoint >= 0x10000) && (codePoint <= 0x10FFFF)))) {

                return "?***";
            }
            return nickName.charAt(0) + "***";
        }
    }

}
