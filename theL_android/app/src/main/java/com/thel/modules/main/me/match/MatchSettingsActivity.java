package com.thel.modules.main.me.match;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.base.BaseDataBean;
import com.thel.constants.TheLConstants;
import com.thel.modules.main.MainActivity;
import com.thel.modules.main.me.adapter.UpdataUserInfoDialogAdapter;
import com.thel.modules.main.me.adapter.UpdataUserInfoMultiDialogAdapter;
import com.thel.modules.main.me.bean.LogInfoBean;
import com.thel.modules.main.me.bean.MatchSettingsBean;
import com.thel.modules.main.me.bean.MyInfoBean;
import com.thel.modules.main.me.bean.RoleBean;
import com.thel.modules.main.me.match.eventcollect.collect.MatchLogUtils;
import com.thel.modules.main.settings.SettingsActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.service.DefaultRequestService;
import com.thel.utils.DialogUtil;
import com.thel.utils.MD5Utils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MatchSettingsActivity extends BaseActivity {
    private final int TAB_ANY = 0;
    private final int TAB_SINGLE = 1;
    private int current_tab = TAB_ANY;

    @BindView(R.id.rel_setting_my_info)
    RelativeLayout rel_setting_my_info;

    @BindView(R.id.tx_option_any)
    TextView tx_option_any;

    @BindView(R.id.tx_option_single)
    TextView tx_option_single;

    @BindView(R.id.edit_role)
    TextView edit_role;
    private int role_cur_pos;

    @BindView(R.id.edit_lookingfor_role)
    TextView edit_lookingfor_role;
    private String latitude;
    private String longitude;
    private String pageId;

    @BindView(R.id.rel_role)
    RelativeLayout rel_role;

    @BindView(R.id.rel_lookingfor_role)
    RelativeLayout rel_lookingfor_role;

    @BindView(R.id.role_ll)
    LinearLayout role_ll;

    private ArrayList<String> role_list = new ArrayList<String>(); // 角色
    private ArrayList<String> role_list_index = new ArrayList<>();//角色对应的key
    private ArrayList<Integer> lookingForRoleSelectPositions = new ArrayList<Integer>(); // 寻找角色多选列表
    private ArrayList<Integer> lookingForRoleTemplist = new ArrayList<Integer>(); // 寻找角色多选临时列表(在dialog没点确定之前记录)
    private StringBuilder lookingForRoleSB = new StringBuilder(); // 寻找角色数组下标字符串拼接

    private int role_posiotn = 0;
    private int tempCurrent1 = 0; // 记录临时位置
    private int editWhich = 0; // 当前正在编辑的是1:年龄, 2:星座, 3:身高, 4:体重, 5:角色, 6:感情状态,7 寻找角色
    private String roleName;
    private String wantRole;
    private String from_page_id;
    private String from_page;

    @OnClick(R.id.lin_back)
    void finishThisPage() {
        finish();
    }

    @OnClick(R.id.tv_exit_match)
    void exitMatchGame() {   //弹出退出热拉速配的弹窗
        DialogUtil.showConfirmDialog(this, null, getString(R.string.remind_exit_rule), getString(R.string.info_quit), getString(R.string.info_no), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
                showLoading();
                exitMatch();
                //匹配埋点
                traceMatchSettings("match.set", "exit");

            }
        });

    }

    private void traceMatchSettings(String page, String activity) {

        LogInfoBean logInfoBean = new LogInfoBean();
        logInfoBean.page = page;
        logInfoBean.page_id = pageId;
        logInfoBean.activity = activity;
        logInfoBean.from_page = from_page;
        logInfoBean.from_page_id = from_page_id;
        logInfoBean.lat = latitude;
        logInfoBean.lng = longitude;

        MatchLogUtils.getInstance().addLog(logInfoBean);

    }

    private void traceMatchDataSettings(String page, String activity, String before, String after) {
        // //匹配埋点

        LogInfoBean logInfoBean = new LogInfoBean();
        logInfoBean.page = page;
        logInfoBean.page_id = pageId;
        logInfoBean.activity = activity;
        logInfoBean.from_page = from_page;
        logInfoBean.from_page_id = from_page_id;
        logInfoBean.lat = latitude;
        logInfoBean.lng = longitude;
        LogInfoBean.LogsDataBean logsDataBean = new LogInfoBean.LogsDataBean();
        logsDataBean.before = before;
        logsDataBean.after = after;
        logInfoBean.data = logsDataBean;
        MatchLogUtils.getInstance().addLog(logInfoBean);

    }

    private void submitMatchSettings() {
        Map<String, String> data = new HashMap<>();
        data.put("wantAffection", current_tab + "");
        data.put("wantRole", lookingForRoleSB.toString());
        data.put("roleName", role_cur_pos + "");

        DefaultRequestService.createAllRequestService().uploadMatchProfile(MD5Utils.generateSignatureForMap(data))
                .onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
            @Override
            public void onNext(BaseDataBean data) {
                super.onNext(data);
                Log.d("MatchSettingsPrint", "success" + data.toString());
                closeLoading();

            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                Log.d("MatchSettingsPrint", "error" + t.getMessage());
            }
        });

    }

    private void exitMatch() {
        Map<String, String> map = MD5Utils.getDefaultHashMap();

        DefaultRequestService.createAllRequestService().exitMatch(MD5Utils.generateSignatureForMap(map))
                .onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
            @Override
            public void onNext(BaseDataBean data) {
                super.onNext(data);
                Log.d("MatchSettingsPrint", data.toString());
                ShareFileUtils.setBoolean(ShareFileUtils.exitMatch, true);
                closeLoading();
                backToMe();

            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                Log.d("MatchSettingsPrint", t.getMessage());
            }
        });

    }

    /**
     * 重启热拉 返回到我的页面
     */
    private void backToMe() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(TheLConstants.BUNDLE_KEY_WHERE_TO_GO, TheLConstants.MainFragmentPageConstants.FRAGMENT_ME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.rel_setting_my_info)
    void gotoMyMatchInfo() {
        Intent intent = new Intent(this, DetailedMatchUserinfoActivity.class);
        intent.putExtra("isMyself", true);
        Bundle bundle = new Bundle();

        bundle.putString(ShareFileUtils.MATCH_FROM_PAGE_ID, pageId);
        bundle.putString(ShareFileUtils.MATCH_FROM_PAGE, "match.set");
        intent.putExtras(bundle);
        startActivity(intent);
        //匹配埋点
        traceMatchSettings("match.set", "info");
    }

    @OnClick(R.id.tx_option_any)
    void setAny() {
        // //匹配埋点
        traceMatchDataSettings("match.set", "affection", current_tab + "", TAB_ANY + "");
        current_tab = TAB_ANY;
        refreshWantAffection(current_tab);
        submitMatchSettings();
    }

    @OnClick(R.id.tx_option_single)
    void setSingle() {
        // //匹配埋点
        traceMatchDataSettings("match.set", "affection", current_tab + "", TAB_SINGLE + "");

        current_tab = TAB_SINGLE;
        refreshWantAffection(current_tab);
        submitMatchSettings();

    }

    @OnClick(R.id.rel_push_setting)
    void pushSetting(View v) {
        ViewUtils.preventViewMultipleClick(v, 2000);
        Intent intent = new Intent(MatchSettingsActivity.this, SettingsActivity.class);
        intent.putExtra(SettingsActivity.PAGE_TYPE_TAG, SettingsActivity.PAGE_TYPE_PUSH);
        Bundle bundle = new Bundle();

        bundle.putString(ShareFileUtils.MATCH_FROM_PAGE_ID, pageId);
        bundle.putString(ShareFileUtils.MATCH_FROM_PAGE, "match.set");
        intent.putExtras(bundle);

        startActivity(intent);
        // //匹配push埋点
        traceMatchSettings("match.set", "push");

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.match_settings_layout);
        ButterKnife.bind(this);
        initLocalData();
        initData();
        setListener();

    }

    private void setListener() {
        //我的角色
        rel_role.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewUtils.preventViewMultipleClick(view, 1000);
                editWhich = 5;
                displayChoiseRoleDialog(role_list, role_posiotn);
                traceMatchDataSettings("match.set", "role", roleName, role_cur_pos + "");

            }
        });
        //寻找角色
        rel_lookingfor_role.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewUtils.preventViewMultipleClick(view, 1000);
                editWhich = 7;
                displayMultiRoleChoiseDialog(role_list, lookingForRoleSelectPositions, lookingForRoleTemplist, lookingForRoleSB, edit_lookingfor_role);
                traceMatchDataSettings("match.set", "wantrole", wantRole, lookingForRoleSB.toString());

            }
        });
    }

    //寻找角色
    private void displayMultiRoleChoiseDialog(final ArrayList<String> list, final ArrayList<Integer> selectPos, final ArrayList<Integer> tempList, final StringBuilder sb, final TextView editText) {
        final Dialog dialog = new Dialog(MatchSettingsActivity.this, R.style.CustomDialogBottom);
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
        WindowManager windowManager = getWindowManager();
        Display display = windowManager.getDefaultDisplay();
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.gravity = Gravity.BOTTOM;
        lp.width = display.getWidth(); // 设置宽度
        dialog.getWindow().setAttributes(lp);

        LayoutInflater flater = LayoutInflater.from(MatchSettingsActivity.this);
        View view = flater.inflate(R.layout.single_choice_dialog, null);
        dialog.setContentView(view);

        LinearLayout lin_next = view.findViewById(R.id.lin_next);
        ListView listview = view.findViewById(R.id.listview);
        TextView txt_title = view.findViewById(R.id.txt_title);
        txt_title.setText(TheLApp.getContext().getString(R.string.updatauserinfo_activity_dialog_title_multichoice));

        tempList.clear();
        tempList.addAll(selectPos);
        final UpdataUserInfoMultiDialogAdapter adapter = new UpdataUserInfoMultiDialogAdapter(list, selectPos);
        listview.setAdapter(adapter);
        listview.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (list == role_list && position == role_list.size() - 1) {// 选寻找角色时要特殊处理，选了第一项就清除其他项，选择了其他项要清除第一项
                    tempList.clear();
                    tempList.add(role_list.size() - 1);
                } else {
                    if (list == role_list) {// 选寻找角色时要特殊处理，选了第一项就清除其他项，选择了其他项要清除第一项
                        tempList.remove(Integer.valueOf(role_list.size() - 1));
                    }
                    boolean remove = false;
                    for (int i = 0; i < tempList.size(); i++) {
                        if (tempList.get(i) == position) {
                            tempList.remove(i);
                            remove = true;
                            break;
                        }
                    }
                    if (!remove) {
                        tempList.add(position);
                    }
                }
                adapter.refreshList(tempList);
                adapter.notifyDataSetChanged();
            }
        });

        lin_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                try {
                    dialog.dismiss();
                    selectPos.clear();
                    selectPos.addAll(tempList);
                    sb.delete(0, sb.length());

                    if (!selectPos.isEmpty()) {
                        Collections.sort(selectPos);
                        String select = "";
                        for (int i = 0; i < selectPos.size(); i++) {
                            int j = selectPos.get(i);
                            String data = role_list_index.get(j);
                            int parseInt = Integer.parseInt(data);
                            sb.append(parseInt);
                            // select += roleMap.get(parseInt);
                            RoleBean roleBean = ShareFileUtils.getRoleBean(parseInt);
                            String contentRes = roleBean.contentRes;
                            if (contentRes != null) {
                                select += contentRes;
                            }
                            if (i != selectPos.size() - 1) {

                                select += ",";
                                sb.append(",");
                            }
                        }
                        editText.setText(select);
                        editText.setTextColor(ContextCompat.getColor(MatchSettingsActivity.this, R.color.update_have_edited_text));

                    } else {
                        editText.setText("");
                    }
                    submitMatchSettings();

                } catch (Exception e) {

                }
                //afterChoised();

            }
        });
    }

    private void displayChoiseRoleDialog(ArrayList<String> role_list, int curPosition) {
        final Dialog dialog = new Dialog(MatchSettingsActivity.this, R.style.CustomDialogBottom);
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
        WindowManager windowManager = getWindowManager();
        Display display = windowManager.getDefaultDisplay();
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.gravity = Gravity.BOTTOM;
        lp.width = display.getWidth(); // 设置宽度
        dialog.getWindow().setAttributes(lp);

        LayoutInflater flater = LayoutInflater.from(MatchSettingsActivity.this);
        View view = flater.inflate(R.layout.single_choice_dialog, null);
        dialog.setContentView(view);

        LinearLayout lin_next = view.findViewById(R.id.lin_next);
        ListView listview = view.findViewById(R.id.listview);

        final UpdataUserInfoDialogAdapter adapter = new UpdataUserInfoDialogAdapter(role_list, curPosition);
        listview.setAdapter(adapter);
        listview.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listview.smoothScrollByOffset(curPosition);
        tempCurrent1 = curPosition;
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                tempCurrent1 = position;
                adapter.refreshList(position);
                adapter.notifyDataSetChanged();
            }
        });

        lin_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                dialog.dismiss();
                switch (editWhich) {

                    case 5:
                        try {
                            role_posiotn = tempCurrent1;
                            String currentData = role_list_index.get(role_posiotn);
                            role_cur_pos = Integer.parseInt(currentData);
                            submitMatchSettings();
                        } catch (Exception ex) {

                        }

                        break;

                }
                afterChoised();
            }
        });
    }

    private void afterChoised() {
        switch (editWhich) {

            case 5:
                if (role_posiotn >= 0) {
                    edit_role.setText(role_list.get(role_posiotn) + "");
                    edit_role.setTextColor(ContextCompat.getColor(this, R.color.update_have_edited_text));
                }
                break;
        }
    }

    private void initLocalData() {

        String[] role_Array = this.getResources().getStringArray(R.array.userinfo_role);
        String[] role_index_array = this.getResources().getStringArray(R.array.userinfo_role_index);

        role_list = new ArrayList<String>(Arrays.asList(role_Array));
        role_list_index = new ArrayList<>(Arrays.asList(role_index_array));

        latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
        longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");
        pageId = Utils.getPageId();

        Bundle bundle = getIntent().getExtras();
        from_page_id = bundle.getString(ShareFileUtils.MATCH_FROM_PAGE_ID, "");
        from_page = bundle.getString(ShareFileUtils.MATCH_FROM_PAGE, "");


    }

    private void initData() {
        loadMatchProfileData();
    }

    private void loadMatchProfileData() {
        DefaultRequestService.createAllRequestService().getMatchProfile().onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<MatchSettingsBean>() {
            @Override
            public void onNext(MatchSettingsBean data) {
                super.onNext(data);
                refreshData(data);

            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
            }

            @Override
            public void onComplete() {
                super.onComplete();

            }
        });
    }

    private void refreshData(MatchSettingsBean settingsBean) {
        if (settingsBean != null && settingsBean.data != null) {
            // 角色
            if (!TextUtils.isEmpty(settingsBean.data.roleName)) {
                roleName = settingsBean.data.roleName;
                try {
                    role_cur_pos = Integer.parseInt(settingsBean.data.roleName);
                    if (role_cur_pos < 0) {
                        edit_role.setText(TheLApp.context.getString(R.string.your_lable));
                        edit_role.setTextColor(ContextCompat.getColor(this, R.color.tab_normal));

                    } else {
                        if (role_cur_pos == 7 || role_cur_pos == 6) {
                            role_cur_pos = 5;
                        }
                        edit_role.setTextColor(ContextCompat.getColor(this, R.color.update_have_edited_text));

                    }
                } catch (Exception e) {

                }
                // 由于是写死在客户端本地的，所以如果拓展则无法兼容旧版，这边做个容错，给用户提示更新

                RoleBean roleBean = ShareFileUtils.getRoleBean(role_cur_pos);

                if (roleBean != null) {
                    role_posiotn = roleBean.listPoi;
                    edit_role.setText(roleBean.contentRes);
                }
            } else {
                edit_role.setText(TheLApp.context.getString(R.string.your_lable));
                edit_role.setTextColor(ContextCompat.getColor(this, R.color.tab_normal));

            }

            // 寻找角色
            if (!TextUtils.isEmpty(settingsBean.data.wantRole)) {
                wantRole = settingsBean.data.wantRole;
                edit_lookingfor_role.setTextColor(ContextCompat.getColor(this, R.color.update_have_edited_text));
                int otherRoleCount = 0;

                try {
                    String[] lookingForRole = settingsBean.data.wantRole.split(",");
                    StringBuilder sb = new StringBuilder();// 寻找角色内容字符串拼接
                    lookingForRoleSB = new StringBuilder();
                    lookingForRoleSelectPositions.clear();
                    int size = lookingForRole.length;
                    ArrayList newRoleList = new ArrayList();

                    for (int i = 0; i < size; i++) {
                        int index = Integer.parseInt(lookingForRole[i]);
                        if ((index == 6 || index == 7 || index == 5)) {
                            if (otherRoleCount < 1) {
                                index = 5;
                                otherRoleCount++;
                            } else {
                                continue;
                            }
                        }
                        RoleBean roleBean = ShareFileUtils.getRoleBean(index);
                        if (roleBean != null) {
                            int listPoi = roleBean.listPoi;
                            if (listPoi == 5) {
                                newRoleList.add(listPoi);
                            } else {

                            }
                            sb.append(role_list.get(listPoi));
                            lookingForRoleSB.append(index);
                            lookingForRoleSelectPositions.add(listPoi);

                        } else {

                        }
                        if (i != size - 1) {
                            sb.append(",");
                            lookingForRoleSB.append(",");
                        }
                    }
                    String content = sb.toString();
                    if (content != null && content.length() > 0) {
                        if (content.endsWith(",")) {
                            content = content.substring(0, content.length() - 1);
                        }
                        edit_lookingfor_role.setText(content);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                edit_lookingfor_role.setText(TheLApp.context.getString(R.string.your_faborat_label));
                edit_lookingfor_role.setTextColor(ContextCompat.getColor(this, R.color.tab_normal));
            }

            current_tab = settingsBean.data.wantAffection;
            refreshWantAffection(current_tab);

            role_ll.setVisibility(settingsBean.data.hideRole == 1 ? View.GONE : View.VISIBLE);
        }

    }

    private void refreshWantAffection(int wantAffection) {
        if (wantAffection == TAB_ANY) {  //不限
            tx_option_any.setTextColor(ContextCompat.getColor(TheLApp.context, R.color.white));
            tx_option_any.setBackgroundResource(R.drawable.bg_border_corner_button_match_perference_checked_shape);
            tx_option_single.setTextColor(ContextCompat.getColor(TheLApp.context, R.color.tab_normal));
            tx_option_single.setBackgroundResource(R.drawable.bg_border_corner_button_match_perference_uncheck_shape);

        } else {
            tx_option_any.setTextColor(ContextCompat.getColor(TheLApp.context, R.color.tab_normal));
            tx_option_any.setBackgroundResource(R.drawable.bg_border_corner_button_match_perference_uncheck_shape);
            tx_option_single.setTextColor(ContextCompat.getColor(TheLApp.context, R.color.white));
            tx_option_single.setBackgroundResource(R.drawable.bg_border_corner_button_match_perference_checked_shape);


        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyInfoBean myInfoBean = ShareFileUtils.getUserData();
        if (myInfoBean != null) {
            // 角色
          /*  if (!TextUtils.isEmpty(myInfoBean.data.roleName)) {
                roleName = settingsBean.data.roleName;
                try {
                    role_cur_pos = Integer.parseInt(settingsBean.data.roleName);
                    if (role_cur_pos < 0) {
                        edit_role.setText(TheLApp.context.getString(R.string.your_lable));
                        edit_role.setTextColor(ContextCompat.getColor(this, R.color.tab_normal));

                    } else {
                        if (role_cur_pos == 7 || role_cur_pos == 6) {
                            role_cur_pos = 5;
                        }
                        edit_role.setTextColor(ContextCompat.getColor(this, R.color.update_have_edited_text));

                    }
                } catch (Exception e) {

                }
                // 由于是写死在客户端本地的，所以如果拓展则无法兼容旧版，这边做个容错，给用户提示更新

                RoleBean roleBean = ShareFileUtils.getRoleBean(role_cur_pos);

                if (roleBean != null) {
                    role_posiotn = roleBean.listPoi;
                    edit_role.setText(roleBean.contentRes);
                }
            } else {
                edit_role.setText(TheLApp.context.getString(R.string.your_lable));
                edit_role.setTextColor(ContextCompat.getColor(this, R.color.tab_normal));

            }

            // 寻找角色
            if (!TextUtils.isEmpty(settingsBean.data.wantRole)) {
                wantRole = settingsBean.data.wantRole;
                edit_lookingfor_role.setTextColor(ContextCompat.getColor(this, R.color.update_have_edited_text));
                int otherRoleCount = 0;

                try {
                    String[] lookingForRole = settingsBean.data.wantRole.split(",");
                    StringBuilder sb = new StringBuilder();// 寻找角色内容字符串拼接
                    lookingForRoleSB = new StringBuilder();
                    lookingForRoleSelectPositions.clear();
                    int size = lookingForRole.length;
                    ArrayList newRoleList = new ArrayList();

                    for (int i = 0; i < size; i++) {
                        int index = Integer.parseInt(lookingForRole[i]);
                        if ((index == 6 || index == 7 || index == 5)) {
                            if (otherRoleCount < 1) {
                                index = 5;
                                otherRoleCount++;
                            } else {
                                continue;
                            }
                        }
                        RoleBean roleBean = ShareFileUtils.getRoleBean(index);
                        if (roleBean != null) {
                            int listPoi = roleBean.listPoi;
                            if (listPoi == 5) {
                                newRoleList.add(listPoi);
                            } else {

                            }
                            sb.append(role_list.get(listPoi));
                            lookingForRoleSB.append(index);
                            lookingForRoleSelectPositions.add(listPoi);

                        } else {
                            sb.append("");

                        }
                        if (i != size - 1) {
                            sb.append(",");
                            lookingForRoleSB.append(",");
                        }
                    }
                    String content = sb.toString();
                    if (content != null && content.length() > 0) {
                        if (content.endsWith(",")) {
                            content = content.substring(0, content.length() - 1);
                        }
                        edit_lookingfor_role.setText(content);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                edit_lookingfor_role.setText(TheLApp.context.getString(R.string.your_faborat_label));
                edit_lookingfor_role.setTextColor(ContextCompat.getColor(this, R.color.tab_normal));
            }*/
        }
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return true;
    }

}
