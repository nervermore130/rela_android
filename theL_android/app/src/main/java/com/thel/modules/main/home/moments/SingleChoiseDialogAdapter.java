package com.thel.modules.main.home.moments;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;

import java.util.ArrayList;
import java.util.List;

public class SingleChoiseDialogAdapter extends BaseAdapter {

    private LayoutInflater mInflater;

    private int selectPosition;

    private List<String> listData = new ArrayList<String>();
    private List<String> listDataRight = new ArrayList<String>();
    private Integer[] logos = null;

    public SingleChoiseDialogAdapter(List<String> listData, Integer[] pics, List<String> listDataRight) {
        this.listData = listData;
        this.logos = pics;
        this.listDataRight = listDataRight;
        mInflater = (LayoutInflater) TheLApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public SingleChoiseDialogAdapter(List<String> listData, Integer[] pics, List<String> listDataRight, int selectPosition) {
        this.selectPosition = selectPosition;
        this.listData = listData;
        this.listDataRight = listDataRight;
        this.logos = pics;

        mInflater = (LayoutInflater) TheLApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void refreshList(int selectPosition) {
        this.selectPosition = selectPosition;
    }

    public void updataList(int selectPosition, ArrayList<String> listData, List<String> listDataRight) {
        this.selectPosition = selectPosition;
        this.listDataRight = listDataRight;
        this.listData = listData;
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {
        convertView = mInflater.inflate(R.layout.updateuserinfo_dialog_listitem, parent, false);
        TextView txt = convertView.findViewById(R.id.txt);
        TextView txt_right = convertView.findViewById(R.id.txt_right);
        ImageView img = convertView.findViewById(R.id.img);
        if (listDataRight != null && !listDataRight.isEmpty()) {
            txt_right.setText(listDataRight.get(position));
            txt_right.setVisibility(View.VISIBLE);
        } else {
            txt_right.setVisibility(View.GONE);
        }
        txt.setText(listData.get(position));
        if (logos != null && logos.length > 0) {
            img.setImageResource(logos[position]);
            img.setVisibility(View.VISIBLE);
        } else {
            img.setVisibility(View.GONE);
        }

        if (position == selectPosition) {// 如果当前的行就是ListView中选中的一行，就更改显示样式
            convertView.setBackgroundColor(0xffd5e8e9);// 更改整行的背景色
        }

        convertView.setTag(position);

        return convertView;
    }

}
