package com.thel.modules.main.userinfo;

import android.content.Context;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thel.R;

/**
 * Created by waiarl on 2018/4/17.
 */

public class UserInfoTabView extends LinearLayout {

    private final Context mContext;
    private TextView txt_title;
    private View view_bottom;
    private String title;
    private int unSelectedColor;
    private int selectedColor;

    public UserInfoTabView(Context context) {
        this(context, null);
    }

    public UserInfoTabView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public UserInfoTabView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
        setSelected(false);
    }

    private void init() {
        inflate(mContext, R.layout.user_info_tab_view, this);
        txt_title = findViewById(R.id.txt_title);
        view_bottom = findViewById(R.id.view_bottom);

        selectedColor = ContextCompat.getColor(mContext, R.color.black);
        unSelectedColor = ContextCompat.getColor(mContext, R.color.gray);
    }

    public UserInfoTabView initView(String title) {
        this.title = title;
        if (!TextUtils.isEmpty(title)) {
            txt_title.setText(title);
        }
        return this;
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);
        if (selected) {
            view_bottom.setVisibility(View.VISIBLE);
            txt_title.setTextColor(selectedColor);
        } else {
            view_bottom.setVisibility(View.INVISIBLE);
            txt_title.setTextColor(unSelectedColor);

        }
    }
}
