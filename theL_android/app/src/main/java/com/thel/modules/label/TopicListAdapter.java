package com.thel.modules.label;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.TopicBean;
import com.thel.constants.TheLConstants;

import java.util.ArrayList;

public class TopicListAdapter extends BaseAdapter {

    private ArrayList<TopicBean> topiclist = new ArrayList<TopicBean>();

    private LayoutInflater mInflater;

    private Context mContext;

    public TopicListAdapter(Context context, ArrayList<TopicBean> searchlist) {

        mContext = context;

        mInflater = (LayoutInflater) TheLApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        freshAdapter(searchlist);
    }

    public void freshAdapter(ArrayList<TopicBean> searchlist) {
        this.topiclist = searchlist;
    }

    @Override
    public int getCount() {
        return topiclist.size();
    }

    @Override
    public Object getItem(int position) {
        return topiclist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        HoldView holdView = null;
        //        if (convertView == null) {
        convertView = mInflater.inflate(R.layout.recent_and_hot_topic_listitem, parent, false);
        holdView = new HoldView();
        holdView.txt_create_new = convertView.findViewById(R.id.txt_create_new);
        holdView.topic_name = convertView.findViewById(R.id.topic_name);
        convertView.setTag(holdView); // 把holdview缓存下来
        //        } else {
        //            holdView = (HoldView) convertView.getTag();
        //        }

        TopicBean searchBean = topiclist.get(position);
        holdView.topic_name.setText(searchBean.topicName);
        holdView.topic_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra(TheLConstants.BUNDLE_KEY_TOPIC_NAME, ((TextView) view).getText());
                ((Activity)mContext).setResult(TheLConstants.RESULT_CODE_WRITE_MOMENT_SELECT_TOPIC, intent);
                ((Activity)mContext).finish();
            }
        });
        final GradientDrawable gradientDrawable = (GradientDrawable) holdView.topic_name.getBackground();
        if (searchBean.topicId.equals("-1")) {// 创建新的话题
            holdView.txt_create_new.setVisibility(View.VISIBLE);
            holdView.txt_create_new.setText(TheLApp.getContext().getString(R.string.search_topic_create_new));
            gradientDrawable.setColor(ContextCompat.getColor(TheLApp.getContext(), R.color.bg_green));
        } else {
            if (TextUtils.isEmpty(searchBean.topicColor)) {
                gradientDrawable.setColor(ContextCompat.getColor(TheLApp.getContext(), R.color.bg_green));
            } else {
                gradientDrawable.setColor(Color.parseColor("#" + searchBean.topicColor));
            }
        }
        return convertView;
    }

    class HoldView {
        TextView txt_create_new;
        TextView topic_name;
    }

}
