package com.thel.modules.main.me.match;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.Nullable;
import com.google.android.material.appbar.AppBarLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.SpannableString;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.base.BaseImageViewerActivity;
import com.thel.bean.BasicInfoNetBean;
import com.thel.bean.LikeResultBean;
import com.thel.bean.SuperlikeBean;
import com.thel.bean.moments.MomentListDataBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.moments.MomentsListBean;
import com.thel.bean.user.MyImageBean;
import com.thel.bean.user.MyImagesListBean;
import com.thel.bean.user.UserInfoBean;
import com.thel.constants.TheLConstants;
import com.thel.modules.main.home.moments.ReportActivity;
import com.thel.modules.main.me.aboutMe.UpdateUserInfoActivity;
import com.thel.modules.main.me.bean.LogInfoBean;
import com.thel.modules.main.me.bean.MatchBean;
import com.thel.modules.main.me.bean.RoleBean;
import com.thel.modules.main.me.match.eventcollect.collect.MatchLogUtils;
import com.thel.modules.main.userinfo.MyLinearLayoutManager;
import com.thel.modules.main.userinfo.UserInfoUtils;
import com.thel.modules.main.userinfo.bean.UserInfoNetBean;
import com.thel.modules.preview_image.UserInfoPhotoActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.service.DefaultRequestService;
import com.thel.ui.dialog.ActionSheetDialog;
import com.thel.ui.dialog.BuySuperlikeDialog;
import com.thel.ui.dialog.MatchSuperlikeDialog;
import com.thel.ui.widget.GuideView;
import com.thel.ui.widget.recyclerview.decoration.DefaultItemDivider;
import com.thel.utils.AppInit;
import com.thel.utils.DateUtils;
import com.thel.utils.DialogUtil;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.Utils;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static android.view.View.VISIBLE;
import static com.thel.app.TheLApp.getContext;

/**
 * 匹配用户的详细资料页面
 * Created by lingwei on 2018/1/17.
 */

public class DetailedMatchUserinfoActivity extends BaseImageViewerActivity implements AppBarLayout.OnOffsetChangedListener {
    private View lin_age;
    private View lin_info;
    private View lin_role;
    private View lin_lookfor;
    private View lin_career;
    private View lin_place;
    private View lin_intro;
    private TextView txt_age;
    private TextView txt_info;
    private TextView txt_role;
    private TextView txt_lookfor;
    private TextView txt_career;
    private TextView txt_place;
    private TextView txt_intro;
    private LinearLayout revise_info;
    private LinearLayout lin_weight;
    private TextView txt_weight;
    private MatchPhotosLayout viewpager_layout;
    private List<MomentsBean> momentsBeanList = new ArrayList<>();
    private MatchMomentsAdapter matchMomentsAdapter;
    private RecyclerView recyclerview;
    private MyLinearLayoutManager manager;
    private TextView tv_user_name;
    private ArrayList<MatchBean.PicListBean> userPhotolist = new ArrayList<>();
    private ImageView iv_vip;
    private LinearLayout ll_moments_list;
    private LinearLayout lin_more;
    private MatchBean matchBean;
    private TextView tv_distance_active_time;
    private LinearLayout lin_back;
    private LinearLayout ll_superlike_message;
    private TextView tv_her_superlike_note;
    private TextView tv_note_time;
    private ImageView match_dislike;
    private ImageView match_like;
    private ImageView match_revert;
    private ArrayList<MyImageBean> myPhotolist = new ArrayList<>();
    private boolean isMyself = false;
    private View btn_container;
    private ImageView iv_like_me;
    private int sumLikeCount;
    private AppBarLayout appbarlayout;
    private TextView txt_title;
    private View top_gradient;
    private TextView tv_moments_title;
    private TextView txt_update_myinfo_title;
    private ImageView iv_more;
    private String from;
    private ImageView match_superlike;
    private TextView superlike_count;
    private LottieAnimationView match_superlike_anim;
    private TextView tv_show_foot;
    private Calendar birthday = Calendar.getInstance();
    private GuideView updateInfoGuide;
    private Handler mHandler = new Handler(Looper.getMainLooper());
    public int RESULT_DISLIKE = 44;
    public int RESULT_LIKE = 11;
    public int RESULT_REVERT = 22;
    public int RESULT_SUPERLIKE = 33;
    private String latitude;
    private String longitude;
    private String pageId;
    private TextView txt_affection;
    private LinearLayout lin_affection;
    private SparseArray<RoleBean> relationshipMap = new SparseArray<RoleBean>();
    private String from_page_id;
    private String from_page;
    private String rank_id;
    private String receiver_id;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_userinfo_match_layout);
        getIntentData();
        findViewById();
        initLocalData();
        loadData();
        setLinsener();
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void refreshUserinfoUI(MatchBean matchBean) {
        /***
         * 用户的图片
         * */
        if (matchBean != null) {
            if (matchBean.picList != null) {
                userPhotolist.clear();
                userPhotolist.addAll(matchBean.picList);

            }

            /************************************基本资料********************************************************/

            String nickName = generateNickname(matchBean.nickName);

            tv_user_name.setText(nickName);
            //超级喜欢的留言
            if (!TextUtils.isEmpty(matchBean.superLikeNote) && !TextUtils.isEmpty(matchBean.superLikeTime)) {
                ll_superlike_message.setVisibility(VISIBLE);
                tv_her_superlike_note.setText(matchBean.superLikeNote);
                tv_note_time.setText(matchBean.superLikeTime);
            }

            //距离
            String distanceStr = "";
            if (!TextUtils.isEmpty(matchBean.distance)) {

                distanceStr = distanceStr + matchBean.distance;
                tv_distance_active_time.setText(distanceStr);
            }
            //活跃时间
            if (!TextUtils.isEmpty(matchBean.activeTime)) {

                String timeStr = "";
                String[] number = matchBean.activeTime.split("_");
                switch (number[0]) {
                    case "1"://second
                        timeStr = TheLApp.getContext().getResources().getString(R.string.active_second, number[1]);
                        break;
                    case "2"://minute
                        timeStr = TheLApp.getContext().getResources().getString(R.string.active_minute, number[1]);
                        break;
                    case "3"://hour
                        timeStr = TheLApp.getContext().getResources().getString(R.string.active_hour, number[1]);
                        break;
                    case "4"://day
                        timeStr = TheLApp.getContext().getResources().getString(R.string.active_day, number[1]);
                        break;
                }

                if (TextUtils.isEmpty(distanceStr)) {
                    tv_distance_active_time.setText(timeStr);

                } else {
                    tv_distance_active_time.setText(distanceStr + " | " + timeStr);

                }
            }

            //年龄星座
            if (matchBean.age != 0 && matchBean.age >= 18) {
                lin_age.setVisibility(VISIBLE);
                setAgeAndStar(lin_age, txt_age, matchBean);
            }

            //身高体重
            if (matchBean.height != 0) {
                lin_info.setVisibility(VISIBLE);
                setHeight(lin_info, txt_info, matchBean);
            }
            //体重
            if (matchBean.weight != 0) {
                lin_weight.setVisibility(VISIBLE);
                setWeight(lin_weight, txt_weight, matchBean);
            }

            // 角色设定
            setRole(lin_role, txt_role, matchBean.roleName);

            //寻找角色
            setLookfor(lin_lookfor, txt_lookfor, matchBean);

            //感情状态
            setAffection(lin_affection, txt_affection, matchBean);


            /************************************详细资料********************************************************/

            //职业描述
            if (!TextUtils.isEmpty(matchBean.occupation)) {
                lin_career.setVisibility(VISIBLE);

                setOccupation(lin_career, txt_career, matchBean.occupation);
            }
            //居住地
            if (!TextUtils.isEmpty(matchBean.livecity)) {
                lin_place.setVisibility(VISIBLE);
                setLiveCity(lin_place, txt_place, matchBean.livecity);
            }
            //详细自述
            if (!TextUtils.isEmpty(matchBean.intro)) {
                lin_intro.setVisibility(VISIBLE);
                setIntro(lin_intro, txt_intro, matchBean.intro);
            }

            //会员
            setVipImage(matchBean, iv_vip);

            //用户照片墙
            setUserPhotos();

        }
    }

    private void setAffection(LinearLayout lin_affection, TextView txt_affection, MatchBean matchBean) {
        if (!TextUtils.isEmpty(matchBean.affection)) {
            lin_affection.setVisibility(View.VISIBLE);
            String startstring = TheLApp.context.getString(R.string.updatauserinfo_activity_lovestatus);
            try {

                int affection = Integer.parseInt(matchBean.affection);
                RoleBean relationshipBean = relationshipMap.get(affection);

                if (relationshipBean != null) {
                    SpannableString spannaString = UserInfoUtils.getUserInfoSpannaString(startstring, relationshipBean.contentRes);
//是否有伴侣
                    if (matchBean.hasBindingPartner == 1) {
                        spannaString = UserInfoUtils.getUserInfoSpannaString(startstring, relationshipBean.contentRes + TheLApp.context.getString(R.string.has_binding_partner));
                    }

                    txt_affection.setText(spannaString);

                }


            } catch (Exception e) {
            }

        }

    }

    private String generateNickname(String nickName) {
        if (TextUtils.isEmpty(nickName)) {
            return "***";
        } else {
            char codePoint = nickName.charAt(0);
            //是否包含表情符号
            if (!((codePoint == 0x0) || (codePoint == 0x9) || (codePoint == 0xA) || (codePoint == 0xD) || ((codePoint >= 0x20) && (codePoint <= 0xD7FF)) || ((codePoint >= 0xE000) && (codePoint <= 0xFFFD)) || ((codePoint >= 0x10000) && (codePoint <= 0x10FFFF)))) {

                return "?***";
            }
            return nickName.charAt(0) + "***";
        }
    }

    private ArrayList<String> role_list = new ArrayList<String>(); // 角色

    private void initLocalData() {

        String[] role_Array = this.getResources().getStringArray(R.array.userinfo_role);
        role_list = new ArrayList<String>(Arrays.asList(role_Array));
        initRelationshipMap();

    }

    private void initRelationshipMap() {
        relationshipMap.put(1, new RoleBean(0, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[0]));
        relationshipMap.put(6, new RoleBean(1, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[1]));
        relationshipMap.put(7, new RoleBean(2, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[2]));
        relationshipMap.put(2, new RoleBean(3, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[3]));
        relationshipMap.put(3, new RoleBean(4, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[4]));
        relationshipMap.put(4, new RoleBean(5, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[5]));
        relationshipMap.put(5, new RoleBean(6, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[6]));
        relationshipMap.put(0, new RoleBean(7, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[7]));


    }

    private void getIntentData() {
        Intent intent = getIntent();
        from = intent.getStringExtra("from");

        matchBean = (MatchBean) intent.getSerializableExtra("MatchBean");
        isMyself = intent.getBooleanExtra("isMyself", false);
        latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
        longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            from_page_id = bundle.getString(ShareFileUtils.MATCH_FROM_PAGE_ID, "");
            from_page = bundle.getString(ShareFileUtils.MATCH_FROM_PAGE, "");
            rank_id = bundle.getString("rank_id", "");
            receiver_id = bundle.getString("receiver_id", "");
        }

        pageId = Utils.getPageId();
    }

    private void loadData() {
        if (isMyself) {
            tv_moments_title.setText(TheLApp.context.getString(R.string.my_moments));
            btn_container.setVisibility(View.GONE);
            txt_update_myinfo_title.setVisibility(VISIBLE);
            iv_more.setVisibility(View.GONE);
            tv_show_foot.setText(TheLApp.context.getString(R.string.match_my_moments_foot));
            String myId = ShareFileUtils.getString(ShareFileUtils.ID, "");
            sumLikeCount = ShareFileUtils.getInt(ShareFileUtils.SUM_LIKE_COUNT, 0);

            getUserInfoData(myId);
            getMoments(myId);

        } else {
            initSuperLikeData();
            iv_more.setVisibility(VISIBLE);
            tv_show_foot.setText(TheLApp.context.getString(R.string.match_moments_foot));
            txt_update_myinfo_title.setVisibility(View.GONE);
            tv_moments_title.setText(TheLApp.context.getString(R.string.match_her_moments));
            if (matchBean != null) {
                getMoments(matchBean.userId + "");
                refreshUserinfoUI(matchBean);
            }

        }
    }

    private void initSuperLikeData() {
        RequestBusiness.getInstance().getBasicInfo()
                .onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new InterceptorSubscribe<BasicInfoNetBean>() {
                    @Override
                    public void onNext(BasicInfoNetBean data) {
                        super.onNext(data);
                        if (data != null && data.data != null) {
                            ShareFileUtils.setInt(ShareFileUtils.SUPER_LIKE, data.data.superLikeRetain);

                            if (data.data.superLikeRetain > 0) {
                                superlike_count.setVisibility(VISIBLE);
                                superlike_count.setText(String.valueOf(data.data.superLikeRetain));
                            }
                        }
                    }
                });
    }

    private void getUserInfoData(String userId) {
        if (!TextUtils.isEmpty(userId)) {
            final Flowable<UserInfoNetBean> flowable = DefaultRequestService.createNearbyRequestService().getUserInfoBean(userId);
            flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<UserInfoNetBean>() {

                @Override
                public void onNext(UserInfoNetBean userInfoNetBean) {
                    super.onNext(userInfoNetBean);

                    L.d("userInfoNetBean", "onNext");

                    L.d("userInfoNetBean", " hasErrorCode " + hasErrorCode);

                    if (userInfoNetBean != null) {
                        L.d("userInfoNetBean", userInfoNetBean.toString());

                        if (userInfoNetBean.data != null) {
                            refreshMyInfoUI(userInfoNetBean.data);
                        }

                    }

                }

                @Override
                public void onError(Throwable t) {
                    if (t instanceof HttpException) {
                        HttpException httpException = (HttpException) t;
                    }
                }

                @Override
                public void onComplete() {
                }
            });
        }

    }

    private void refreshMyInfoUI(UserInfoBean myInfoBean) {
        if (myInfoBean != null) {
            try {
                matchBean = new MatchBean();
                matchBean.nickName = myInfoBean.nickName;
                if (!TextUtils.isEmpty(myInfoBean.weight)) {
                    matchBean.weight = Integer.parseInt(myInfoBean.weight);

                }
                if (!TextUtils.isEmpty(myInfoBean.height)) {
                    matchBean.height = Integer.parseInt(myInfoBean.height);
                }
                if (!TextUtils.isEmpty(myInfoBean.age)) {
                    matchBean.age = Integer.parseInt(myInfoBean.age);

                }
                matchBean.birthday = myInfoBean.birthday;
                matchBean.roleName = myInfoBean.roleName;
                matchBean.occupation = myInfoBean.occupation;
                matchBean.wantRole = myInfoBean.wantRole;
                matchBean.vipLevel = myInfoBean.level;
                matchBean.livecity = myInfoBean.livecity;
                matchBean.intro = myInfoBean.intro;

                getMyImagesList("match", "10", "1");
                //总喜欢我的人数
                if (sumLikeCount > 0) {
                    iv_like_me.setVisibility(VISIBLE);
                    tv_distance_active_time.setText(TheLApp.context.getString(R.string.been_liked_count_even, sumLikeCount));
                }
                refreshUserinfoUI(matchBean);
                //用户照片墙
                //setMyPhotos();
            } catch (Exception e) {

            }

        }
    }

    public void getMyImagesList(String entry, String pageSize, String curPage) {
        Flowable<MyImagesListBean> flowable = RequestBusiness.getInstance().getMyMatchImagesList(entry, pageSize, curPage);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<MyImagesListBean>() {
            @Override
            public void onNext(MyImagesListBean myImagesListBean) {
                super.onNext(myImagesListBean);
                addMyImageviewList(myImagesListBean);
            }

            @Override
            public void onError(Throwable t) {

            }

            @Override
            public void onComplete() {
            }
        });
    }

    private void addMyImageviewList(MyImagesListBean myImagesListBean) {
        if (myImagesListBean.data != null && myImagesListBean.data.imagesList != null) {
            myPhotolist.clear();
            myPhotolist.addAll(myImagesListBean.data.imagesList);
            final List<String> photoUrlList = new ArrayList<>();
            for (int i = 0; i < myPhotolist.size(); i++) {
                photoUrlList.add(myPhotolist.get(i).longThumbnailUrl);
            }
            viewpager_layout.setViewUrls(photoUrlList);
        }
    }

    private void setMyPhotos() {
        if (myPhotolist != null && myPhotolist.size() > 0) {
            final List<String> photoUrlList = new ArrayList<>();
            for (int i = 0; i < myPhotolist.size(); i++) {
                photoUrlList.add(myPhotolist.get(i).longThumbnailUrl);
            }
            viewpager_layout.setViewUrls(photoUrlList);
        }
    }

    private void getMoments(String userid) {
        if (!TextUtils.isEmpty(userid)) {

            final Flowable<MomentListDataBean> flowable = DefaultRequestService.createMomentRequestService().getMatchUserMomentsList(userid);
            flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<MomentListDataBean>() {
                @Override
                public void onNext(MomentListDataBean data) {
                    super.onNext(data);

                    if (!hasErrorCode && data.data != null) {

                        showMomentsListData(data.data);

                    }
                }

                @Override
                public void onError(Throwable t) {
                    super.onError(t);
                }

                @Override
                public void onComplete() {
                    super.onComplete();
                }
            });
        }

    }

    private void showMomentsListData(MomentsListBean momentsListBean) {
        if (momentsListBean.momentsList != null && momentsListBean.momentsList.size() > 0) {
            ll_moments_list.setVisibility(VISIBLE);
            momentsBeanList.clear();
            momentsBeanList.addAll(momentsListBean.momentsList);
            matchMomentsAdapter.setNewData(momentsBeanList);
            matchMomentsAdapter.openLoadMore(momentsBeanList.size(), true);
            matchMomentsAdapter.removeAllFooterView();

        }

    }

    // 角色
    private void setRole(View lin, TextView txt_role, String role) {
        if (TextUtils.isEmpty(role)) {
            return;
        } else {
            try {

                int index = Integer.parseInt(role);
                RoleBean roleBean = ShareFileUtils.getRoleBean(index);
                String startstring = getResources().getString(R.string.slef_identity) + ":";
                SpannableString spannaString;
                if (roleBean != null) {
                    spannaString = UserInfoUtils.getUserInfoSpannaString(startstring, roleBean.contentRes);

                } else {
                    spannaString = UserInfoUtils.getUserInfoSpannaString(startstring, "");
                }
                txt_role.setText(spannaString);
                lin.setVisibility(VISIBLE);
            } catch (NumberFormatException e) {
                return;
            }
        }
    }

    //寻找角色
    private void setLookfor(View lin_lookfor, TextView txt_lookfor, MatchBean userInfoBean) {
        if (TextUtils.isEmpty(userInfoBean.wantRole))
            return;
        int otherRoleCount = 0;
        try {
            String[] lookingForRole = userInfoBean.wantRole.split(",");
            StringBuilder sb = new StringBuilder();// 交友目的内容字符串拼接
            int size = lookingForRole.length;
            ArrayList newRoleList = new ArrayList();
            for (int i = 0; i < size; i++) {
                int index = Integer.parseInt(lookingForRole[i]); //1.2.3.4.5.6.7

                if ((index == 6 || index == 7 || index == 5)) {
                    if (otherRoleCount < 1) {
                        index = 5;
                        otherRoleCount++;
                    } else {
                        continue;
                    }
                }
                RoleBean roleBean = ShareFileUtils.getRoleBean(index);
                if (roleBean != null) {
                    int listPoi = roleBean.listPoi;
                    if (listPoi == 5) {
                        newRoleList.add(listPoi);
                    } else {

                    }
                    sb.append(role_list.get(listPoi));
                } else {
                }
                if (i != size - 1) {

                    sb.append(", ");
                }
            }

            int j = 0;
            for (int i = 0; i < sb.length(); i++) {
                if (newRoleList.contains(sb.charAt(i))) {
                    sb.deleteCharAt(i - j);
                }
            }
            String startstring = getResources().getString(R.string.userinfo_right_lookingfor_role);
            SpannableString spannaString = UserInfoUtils.getUserInfoSpannaString("", sb.toString());
            String content = spannaString.toString();
            if (content != null && content.length() > 0) {
                if (content.endsWith(",")) {
                    content = content.substring(0, content.length() - 1);
                }
                txt_lookfor.setText(content);
            }
            SpannableString spannaString2 = UserInfoUtils.getUserInfoSpannaString(startstring, content);
            txt_lookfor.setText(spannaString2);

            lin_lookfor.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //年龄星座
    private void setAgeAndStar(View lin_rela_id, TextView txt_age, MatchBean userInfoBean) {
        String startstring = getResources().getString(R.string.updatauserinfo_activity_age_with);
        setBirthday(userInfoBean.birthday);
        SpannableString spannaString = UserInfoUtils.getUserInfoSpannaString(startstring, getBirthdayStr(birthday));
        txt_age.setText(spannaString);
    }

    //年龄和星座
    private String getAgeAndStar(MatchBean userInfoBean) {
        if (!TextUtils.isEmpty(userInfoBean.birthday)) {
            return getBirthdayStr(birthday);
        } else {
            return "";
        }
    }

    private void setBirthday(String birth) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            birthday.setTime(sdf.parse(birth));
        } catch (Exception e) {
        }
    }

    /**
     * 格式：12岁,双子座
     *
     * @param birth
     * @return
     */
    private String getBirthdayStr(Calendar birth) {
        if (null == birth) {
            return "";
        }
        Calendar today = Calendar.getInstance();
        StringBuilder sb = new StringBuilder();

        try {
            int age = today.get(Calendar.YEAR) - birth.get(Calendar.YEAR);
            if (age < 18) {
                sb.append(18).append(getString(R.string.updatauserinfo_activity_age_unit)).append(", ").append(DateUtils.date2Constellation(birth));

            } else {
                sb.append(today.get(Calendar.YEAR) - birth.get(Calendar.YEAR)).append(getString(R.string.updatauserinfo_activity_age_unit)).append(", ").append(DateUtils.date2Constellation(birth));

            }
        } catch (Exception e) {
            return "";
        }

        return sb.toString();
    }


    //设置体重
    private void setWeight(LinearLayout lin_weight, TextView txt_weight, MatchBean userInfoBean) {
        String startstring = getResources().getString(R.string.detailed_filters_activity_weight);
        SpannableString spannaString = UserInfoUtils.getUserInfoSpannaString(startstring, getWeight(userInfoBean.weight));
        txt_weight.setText(spannaString);
    }

    //设置身高
    private void setHeight(View lin_info, TextView txt_info, MatchBean userInfoBean) {
        String startstring = getResources().getString(R.string.detailed_filters_activity_height);
        SpannableString spannaString = UserInfoUtils.getUserInfoSpannaString(startstring, getHeight(userInfoBean.height));
        txt_info.setText(spannaString);
    }

    //体重
    private String getWeight(int weight) {
        int weightUnits = ShareFileUtils.getInt(ShareFileUtils.WEIGHT_UNITS, 0); // 体重单位(0=kg, 1=Lbs)
        try {
            String w;

            if (weightUnits == 0) {
                w = Float.valueOf(weight).intValue() + " kg";
            } else {
                w = Utils.kgToLbs(weight + "") + " Lbs";
            }
            return w;
        } catch (Exception e) {
        }
        return "";
    }

    //身高
    private String getHeight(int height) {
        int heightUnits = ShareFileUtils.getInt(ShareFileUtils.HEIGHT_UNITS, 0); // 身高单位(0=cm, 1=Inches)
        try {
            String h;
            if (heightUnits == 0) {
                h = Float.valueOf(height).intValue() + " cm";
            } else {
                h = Utils.cmToInches(height + "") + " Inches";
            }

            return h;
        } catch (Exception e) {
        }
        return "";
    }

    private void findViewById() {
        lin_more = findViewById(R.id.lin_more);
        lin_back = findViewById(R.id.lin_back);
        appbarlayout = findViewById(R.id.appbarlayout);
        top_gradient = findViewById(R.id.top_gradient);
        txt_title = findViewById(R.id.txt_title);
        viewpager_layout = findViewById(R.id.photos_layout);
        final int height = (AppInit.displayMetrics.widthPixels);
        viewpager_layout.getLayoutParams().height = height;
        iv_vip = findViewById(R.id.iv_vip);
        lin_age = findViewById(R.id.lin_age);
        lin_info = findViewById(R.id.lin_info);
        lin_weight = findViewById(R.id.lin_weight);
        lin_role = findViewById(R.id.lin_role);
        lin_lookfor = findViewById(R.id.lin_lookfor);
        lin_career = findViewById(R.id.lin_career);
        lin_place = findViewById(R.id.lin_place);
        lin_intro = findViewById(R.id.lin_intro);
        tv_moments_title = findViewById(R.id.tv_moments_title);
        txt_update_myinfo_title = findViewById(R.id.txt_update_myinfo_title);
        iv_more = findViewById(R.id.iv_more);
        updateInfoGuide = findViewById(R.id.updateInfoGuide);

        txt_age = findViewById(R.id.txt_age);
        txt_info = findViewById(R.id.txt_info);
        txt_role = findViewById(R.id.txt_role);
        txt_lookfor = findViewById(R.id.txt_lookfor);
        txt_career = findViewById(R.id.txt_career);
        txt_place = findViewById(R.id.txt_place);
        txt_intro = findViewById(R.id.txt_intro);
        txt_weight = findViewById(R.id.txt_weight);
        txt_affection = findViewById(R.id.txt_affection);
        lin_affection = findViewById(R.id.lin_affection);

        revise_info = findViewById(R.id.revise_info);
        tv_user_name = findViewById(R.id.tv_user_name);
        tv_distance_active_time = findViewById(R.id.tv_distance_active_time);
        ll_moments_list = findViewById(R.id.ll_moments_list);
        iv_like_me = findViewById(R.id.iv_like_me);

        ll_superlike_message = findViewById(R.id.ll_superlike_message);
        tv_her_superlike_note = findViewById(R.id.tv_her_superlike_note);
        tv_note_time = findViewById(R.id.tv_note_time);
        match_dislike = findViewById(R.id.match_dislike);
        match_like = findViewById(R.id.match_like);
        match_revert = findViewById(R.id.match_revert);
        match_superlike = findViewById(R.id.match_superlike);
        superlike_count = findViewById(R.id.superlike_count);
        match_superlike_anim = findViewById(R.id.match_superlike_anim);
        tv_show_foot = findViewById(R.id.tv_show_foot);
        btn_container = findViewById(R.id.btn_container);

        /***
         * 初始化Adapter
         * */
        recyclerview = findViewById(R.id.recyclerview);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerview.setNestedScrollingEnabled(false);
        recyclerview.setLayoutManager(manager);
        recyclerview.setHasFixedSize(true);
        matchMomentsAdapter = new MatchMomentsAdapter(momentsBeanList);
        recyclerview.setAdapter(matchMomentsAdapter);
        recyclerview.addItemDecoration(new DefaultItemDivider(TheLApp.context, LinearLayoutManager.VERTICAL, R.color.gray, (int) getContext().getResources().getDimension(R.dimen.divider_height), true, true));

        txt_age.setTextIsSelectable(true);
        txt_info.setTextIsSelectable(true);
        txt_career.setTextIsSelectable(true);
        txt_place.setTextIsSelectable(true);
        txt_intro.setTextIsSelectable(true);
        if (isMyself) {
            /**
             * 如果是第一次显示修改资料弹窗提示
             * */
            boolean isShow = ShareFileUtils.getBoolean(ShareFileUtils.MATCH_UPDATE_USERINFO_GUIDE, true);

            if (updateInfoGuide != null && isShow) {
                updateInfoGuide.setVisibility(View.VISIBLE);
                updateInfoGuide.show(txt_update_myinfo_title, null);

                ShareFileUtils.setBoolean(ShareFileUtils.MATCH_UPDATE_USERINFO_GUIDE, false);
            }
        }

    }

    private void setLinsener() {
        appbarlayout.addOnOffsetChangedListener(this);
        viewpager_layout.setOnBannerItemClickListener(new MatchPhotosLayout.OnBannerItemClickListener() {
            @Override
            public void onItemClick(int position) {
                if (position < userPhotolist.size() && matchBean != null) {
                    goToUserPhotoActivity(position, matchBean);
                } else if (position < myPhotolist.size() && matchBean != null) {
                    goToMyPhotoActivity(position, matchBean);

                }
            }
        });
        lin_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();

                LogInfoBean logInfoBean = new LogInfoBean();
                logInfoBean.page = "match.info";
                logInfoBean.page_id = pageId;
                logInfoBean.activity = "back";
                logInfoBean.from_page = from_page;
                logInfoBean.from_page_id = from_page_id;
                logInfoBean.lat = latitude;
                logInfoBean.lng = longitude;
                MatchLogUtils.getInstance().addLog(logInfoBean);


            }
        });
        /**
         * 弹出举报框
         * */
        lin_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isMyself) {
                    new ActionSheetDialog(DetailedMatchUserinfoActivity.this).builder().setCancelable(true).setCanceledOnTouchOutside(true)
                            .addSheetItem(getString(R.string.userinfo_activity_dialog_report_title), ActionSheetDialog.SheetItemColor.RED, new ActionSheetDialog.OnSheetItemClickListener() {
                                @Override
                                public void onClick(int which) {
                                    if (matchBean != null && !TextUtils.isEmpty(matchBean.userId + "")) {
                                        gotoReportActivity(matchBean.userId + "");

                                    }
                                }
                            }).show();
                } else {
                    Intent intent = new Intent(DetailedMatchUserinfoActivity.this, UpdateUserInfoActivity.class);
                    startActivity(intent);

                    LogInfoBean logInfoBean = new LogInfoBean();
                    logInfoBean.page = "match.my";
                    logInfoBean.page_id = pageId;
                    logInfoBean.activity = "edit";
                    logInfoBean.from_page = from_page;
                    logInfoBean.from_page_id = from_page_id;
                    logInfoBean.lat = latitude;
                    logInfoBean.lng = longitude;
                    MatchLogUtils.getInstance().addLog(logInfoBean);

                }

            }
        });
        match_dislike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!TextUtils.isEmpty(from) && from.equals("WhoLikesMeActivity")) {
                    if (matchBean.isSuperLike == 1) {
                        /**
                         * 第一次超级不喜欢 点击忽略 只在喜欢我的列表里展示dialog
                         * **/
                        if (ShareFileUtils.getBoolean(ShareFileUtils.MATCH_FIRST_SUPER_UNLIKE, true)) {// 第一次超级喜欢 点下一组，给用户提示
                            ShareFileUtils.setBoolean(ShareFileUtils.MATCH_FIRST_SUPER_UNLIKE, false);
                            DialogUtil.showConfirmDialogNoTitle(DetailedMatchUserinfoActivity.this, TheLApp.getContext().getString(R.string.ignore_super_like), TheLApp.getContext().getString(R.string.info_no), TheLApp.getContext().getString(R.string.userinfo_activity_unfollow_operation), new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }, new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    finish();
                                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

                                    if (matchBean != null) {
                                        sendBrodCastRefreshList();
                                        likeOrNot(matchBean.userId, "unlike", matchBean.isRollback + "", matchBean);
                                        traceMatchInfoData("match.info", "unlike");

                                    }

                                }
                            });
                        } else {
                            finish();
                            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

                            if (matchBean != null) {
                                sendBrodCastRefreshList();

                                likeOrNot(matchBean.userId, "unlike", matchBean.isRollback + "", matchBean);
                                traceMatchInfoData("match.info", "unlike");


                            }

                        }
                    } else {
                        /**
                         * 普通的不喜欢
                         * **/
                        if (ShareFileUtils.getBoolean(ShareFileUtils.MATCH_FIRST_UNLIKE, true)) {// 第一次点下一组，给用户提示
                            ShareFileUtils.setBoolean(ShareFileUtils.MATCH_FIRST_UNLIKE, false);
                            DialogUtil.showConfirmDialogNoTitle(DetailedMatchUserinfoActivity.this, TheLApp.getContext().getString(R.string.dislike_tips), TheLApp.getContext().getString(R.string.info_no), TheLApp.getContext().getString(R.string.userinfo_activity_unfollow_operation), new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }, new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    finish();
                                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

                                    if (matchBean != null) {
                                        sendBrodCastRefreshList();

                                        likeOrNot(matchBean.userId, "unlike", matchBean.isRollback + "", matchBean);
                                        traceMatchInfoData("match.info", "unlike");

                                    }

                                }
                            });
                        } else {
                            finish();
                            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

                            if (matchBean != null) {
                                sendBrodCastRefreshList();

                                likeOrNot(matchBean.userId, "unlike", matchBean.isRollback + "", matchBean);
                                traceMatchInfoData("match.info", "unlike");

                            }
                        }
                    }


                } else {
                    setResult(RESULT_DISLIKE);
                    traceMatchInfoData("match.info", "unlike");

                    finish();
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);


                }


            }
        });
        match_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!TextUtils.isEmpty(from) && from.equals("WhoLikesMeActivity")) {
                    if (matchBean != null) {

                        likeOrNot(matchBean.userId, "like", matchBean.isRollback + "", matchBean);

                    }
                } else {
                    setResult(RESULT_LIKE);

                }
                traceMatchInfoData("match.info", "like");
                finish();
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);


            }
        });
        match_revert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LogInfoBean logInfoBean = new LogInfoBean();
                logInfoBean.page = "match.info";
                logInfoBean.page_id = pageId;
                logInfoBean.activity = "repent";
                logInfoBean.from_page = from_page;
                logInfoBean.from_page_id = from_page_id;
                logInfoBean.lat = latitude;
                logInfoBean.lng = longitude;
                MatchLogUtils.getInstance().addLog(logInfoBean);

            }
        });
        match_superlike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {

                    if (superlike_count.getVisibility() == View.VISIBLE) {

                        String latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
                        String longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");

                        LogInfoBean logInfoBean = new LogInfoBean();
                        logInfoBean.page = "match.info";
                        logInfoBean.page_id = pageId;
                        logInfoBean.activity = "slike";
                        logInfoBean.from_page = from_page;
                        logInfoBean.from_page_id = from_page_id;
                        LogInfoBean.LogsDataBean logsDataBean = new LogInfoBean.LogsDataBean();
                        logsDataBean.receiver_id = receiver_id;
                        logsDataBean.rank_id = rank_id;
                        logInfoBean.lat = latitude;
                        logInfoBean.lng = longitude;
                        MatchLogUtils.getInstance().addLog(logInfoBean);

                        MatchSuperlikeDialog matchSuperlikeDialog = MatchSuperlikeDialog.newInstance();
                        matchSuperlikeDialog.setSuperlikeListener(new MatchSuperlikeDialog.SuperlikeListener() {
                            @Override
                            public void superlike(String content) {
                                superlikeHttp(String.valueOf(matchBean.userId), content);
                                match_superlike_anim.playAnimation();
                                match_superlike_anim.addAnimatorListener(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        super.onAnimationEnd(animation);
                                        setResult(RESULT_SUPERLIKE);

                                        finish();
                                        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                                    }
                                });
                            }
                        });
                        matchSuperlikeDialog.show(getSupportFragmentManager(), MatchSuperlikeDialog.class.getName());
                    } else {
                        BuySuperlikeDialog buySuperlikeDialog = BuySuperlikeDialog.newInstance();
                        buySuperlikeDialog.setBuySuperlikeListener(new BuySuperlikeDialog.BuySuperlikeListener() {
                            @Override
                            public void success(int count) {
                                superlike_count.setVisibility(VISIBLE);
                                superlike_count.setText(String.valueOf(count));
                            }
                        });
                        buySuperlikeDialog.show(getSupportFragmentManager(), BuySuperlikeDialog.class.getName());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public void traceMatchInfoData(String page, String activity) {

        LogInfoBean logInfoBean = new LogInfoBean();
        logInfoBean.page = page;
        logInfoBean.page_id = pageId;
        logInfoBean.activity = activity;
        logInfoBean.from_page = from_page;
        logInfoBean.from_page_id = from_page_id;
        LogInfoBean.LogsDataBean logsDataBean = new LogInfoBean.LogsDataBean();
        logsDataBean.receiver_id = receiver_id;
        logsDataBean.rank_id = rank_id;
        logInfoBean.data = logsDataBean;

        logInfoBean.lat = latitude;
        logInfoBean.lng = longitude;
        MatchLogUtils.getInstance().addLog(logInfoBean);
    }

    private void superlikeHttp(String userId, String note) {
        Flowable<SuperlikeBean> flowable = RequestBusiness.getInstance().superlike(userId, note);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<SuperlikeBean>() {
            @Override
            public void onNext(SuperlikeBean likeResultBean) {
                super.onNext(likeResultBean);
                if (likeResultBean != null && likeResultBean.data != null) {
                    Intent intent1 = new Intent();
                    ShareFileUtils.setInt(ShareFileUtils.SUPER_LIKE, likeResultBean.data.superlikeRetain);

                    intent1.setAction(TheLConstants.BROADCAST_ACTION_SUPERLIKE);
                    sendBroadcast(intent1);
                    if (likeResultBean.data.superlikeRetain > 0) {

                        superlike_count.setVisibility(VISIBLE);
                        superlike_count.setText(String.valueOf(likeResultBean.data.superlikeRetain));
                    } else {

                        superlike_count.setVisibility(View.INVISIBLE);
                    }
                    if (likeResultBean.data.success == 1) {
                        Intent intent = new Intent(getContext(), MatchSuccessActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(MatchSuccessActivity.MATCH_SUCCESS_TYPE, MatchSuccessActivity.MATCH_SUCCESS_FROM_CARD_SLIDE);
                        bundle.putString(TheLConstants.BUNDLE_KEY_USER_AVATAR, matchBean.avatar);
                        bundle.putString(TheLConstants.BUNDLE_KEY_USER_ID, String.valueOf(matchBean.userId));
                        bundle.putString(TheLConstants.BUNDLE_KEY_NICKNAME, matchBean.nickName);
                        bundle.putString(ShareFileUtils.MATCH_FROM_PAGE_ID, pageId);
                        bundle.putString(ShareFileUtils.MATCH_FROM_PAGE, "match.info");

                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                }
            }

            @Override
            public void onError(Throwable t) {
            }

            @Override
            public void onComplete() {

            }
        });
    }

    /**
     * 举报用户
     *
     * @param userId
     */
    private void gotoReportActivity(String userId) {

        LogInfoBean logInfoBean = new LogInfoBean();
        logInfoBean.page = "match.info";
        logInfoBean.page_id = pageId;
        logInfoBean.activity = "report";
        logInfoBean.from_page = from_page;
        logInfoBean.from_page_id = from_page_id;
        logInfoBean.lat = latitude;
        logInfoBean.lng = longitude;
        LogInfoBean.LogsDataBean logsDataBean = new LogInfoBean.LogsDataBean();
        logsDataBean.receiver_id = receiver_id;
        logsDataBean.rank_id = rank_id;

        logInfoBean.data = logsDataBean;
        MatchLogUtils.getInstance().addLog(logInfoBean);


        Intent intent = new Intent(this, ReportActivity.class);
        intent.putExtra(TheLConstants.BUNDLE_KEY_REPORT_TYPE, ReportActivity.REPORT_TYPE_REPORT_MATCH_USER);
        intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, userId);
        Bundle bundle = new Bundle();
        bundle.putString(ShareFileUtils.MATCH_FROM_PAGE_ID, pageId);
        bundle.putString(ShareFileUtils.MATCH_FROM_PAGE, "match");
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void likeOrNot(long userId, final String like, String s, final MatchBean matchBean) {
        Flowable<LikeResultBean> flowable = RequestBusiness.getInstance().likeOrNot(userId, like, s);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<LikeResultBean>() {
            @Override
            public void onNext(LikeResultBean likeResultBean) {
                super.onNext(likeResultBean);
                getLikeResult(likeResultBean, matchBean);
            }

            @Override
            public void onError(Throwable t) {
            }

            @Override
            public void onComplete() {

            }
        });

    }

    private void getLikeResult(LikeResultBean likeResultBean, MatchBean matchBean) {
        if (likeResultBean != null && likeResultBean.data != null) {
            //互相喜欢
            try {
                int result = Integer.parseInt(likeResultBean.data.likeSuccess);
                if (result == 1) {
                    Intent intent = new Intent(getContext(), MatchSuccessActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(MatchSuccessActivity.MATCH_SUCCESS_TYPE, MatchSuccessActivity.MATCH_SUCCESS_FROM_CARD_SLIDE);
                    bundle.putString(TheLConstants.BUNDLE_KEY_USER_AVATAR, matchBean.avatar);
                    bundle.putString(TheLConstants.BUNDLE_KEY_USER_ID, String.valueOf(matchBean.userId));
                    bundle.putString(TheLConstants.BUNDLE_KEY_NICKNAME, matchBean.nickName);
                    bundle.putString(ShareFileUtils.MATCH_FROM_PAGE_ID, pageId);
                    bundle.putString(ShareFileUtils.MATCH_FROM_PAGE, "match.info");
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            } catch (Exception e) {

            }
        }
    }

    private void goToUserPhotoActivity(int position, MatchBean userinfoBean) {
        final List<String> photoUrlList = new ArrayList<>();

        if (userPhotolist != null && userPhotolist.size() > 0) {
            for (int i = 0; i < userPhotolist.size(); i++) {
                photoUrlList.add(userPhotolist.get(i).picUrl);
            }

        }

        Intent i = new Intent(this, UserInfoPhotoActivity.class);
        i.putExtra("fromPage", "DetailedMatchUserinfoActivity");
        i.putExtra(TheLConstants.IS_WATERMARK, false);
        i.putExtra("matchPicList", (Serializable) photoUrlList);
        i.putExtra("position", position);
        i.putExtra("isMyself", Utils.isMyself(userinfoBean.userId + ""));
        i.putExtra("userId", userinfoBean.userId + "");
        startActivity(i);
    }

    private void goToMyPhotoActivity(int position, MatchBean userinfoBean) {
        final List<String> photoUrlList = new ArrayList<>();

        if (myPhotolist != null && myPhotolist.size() > 0) {
            for (int i = 0; i < myPhotolist.size(); i++) {
                photoUrlList.add(myPhotolist.get(i).picUrl);
            }

        }

        Intent i = new Intent(this, UserInfoPhotoActivity.class);
        i.putExtra("fromPage", "DetailedMatchUserinfoActivity");

        i.putExtra(TheLConstants.IS_WATERMARK, false);
        i.putExtra("matchPicList", (Serializable) photoUrlList);
        i.putExtra("position", position);
        i.putExtra("isMyself", Utils.isMyself(userinfoBean.userId + ""));
        i.putExtra("userId", userinfoBean.userId + "");
        startActivity(i);
    }


    private void setUserPhotos() {
        if (userPhotolist != null && userPhotolist.size() > 0) {
            final List<String> photoUrlList = new ArrayList<>();
            for (int i = 0; i < userPhotolist.size(); i++) {
                photoUrlList.add(userPhotolist.get(i).longThumbnailUrl);
            }
            viewpager_layout.setViewUrls(photoUrlList);
        }

    }

    /**
     * 设置会员
     *
     * @param userInfoBean
     * @param img_vip
     */
    private void setVipImage(MatchBean userInfoBean, ImageView img_vip) {
        int level = userInfoBean.vipLevel;
        if (level >= TheLConstants.VIP_LEVEL_RES.length) {
            level = TheLConstants.USER_LEVEL_RES.length - 1;
        }
        if (level < 1) {
            img_vip.setVisibility(View.GONE);
        } else {
            img_vip.setVisibility(View.VISIBLE);
            img_vip.setImageResource(TheLConstants.VIP_LEVEL_RES[level]);
        }
    }

    //职业描述
    private void setOccupation(View lin_career, TextView txt_career, String occupation) {
        String startstring = getResources().getString(R.string.updatauserinfo_activity_career_type);
        SpannableString spannaString = UserInfoUtils.getUserInfoSpannaString(startstring, occupation);
        txt_career.setText(spannaString);

    }

    //居住地
    private void setLiveCity(View lin_place, TextView txt_place, String livecity) {
        String startstring = getResources().getString(R.string.updatauserinfo_activity_place);
        SpannableString spannaString = UserInfoUtils.getUserInfoSpannaString(startstring, livecity);
        txt_place.setText(spannaString);
    }

    //详细自述
    private void setIntro(View lin_intro, TextView txt_intro, String intro) {
        String startstring = getResources().getString(R.string.personalized_signature);
        SpannableString spannaString = UserInfoUtils.getUserInfoSpannaString(startstring + ":", intro);
        txt_intro.setText(spannaString);
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        final int totalRange = appBarLayout.getTotalScrollRange();

        final float start = (0 - totalRange) * 0.7f;
        final float end = (0 - totalRange) * 0.9f;
        if (verticalOffset < start && verticalOffset > end) {
            final float alp = (verticalOffset - end) / (totalRange * 0.2f);
            txt_title.setAlpha(1 - alp);
            txt_title.setText("");
            viewpager_layout.setAlpha(alp);
            top_gradient.setAlpha(alp);
        } else if (verticalOffset >= start) {
            txt_title.setAlpha(0);
            txt_title.setText("");
            top_gradient.setAlpha(1);
            viewpager_layout.setAlpha(1);

        } else if (verticalOffset <= end) {
            txt_title.setText(TheLApp.context.getString(R.string.updatauserinfo_activity_detail));
            viewpager_layout.setAlpha(0);
            txt_title.setAlpha(1);
            top_gradient.setAlpha(0);
        }
    }

    /**
     * 发送广播，如果不喜欢这个就刷新一下喜欢我的列表数据。
     */
    private void sendBrodCastRefreshList() {
        Intent intent = new Intent();
        intent.setAction(TheLConstants.BROADCAST_MATCH_LIKE_ME_LIST);
        sendBroadcast(intent);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //是否为图片预览返回
        boolean b = imageViewer.onKeyDown(keyCode, event);
        if (b) {
            return b;
        } else {
            return true;
        }

    }
}
