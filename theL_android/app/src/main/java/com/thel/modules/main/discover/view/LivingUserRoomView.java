package com.thel.modules.main.discover.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.live.LiveRoomsBean;
import com.thel.constants.TheLConstants;
import com.thel.modules.live.bean.LiveRoomBean;
import com.thel.modules.live.surface.watch.LiveWatchActivity;
import com.thel.modules.live.surface.watch.horizontal.LiveWatchHorizontalActivity;
import com.thel.modules.main.discover.adapter.LiveRoomsRecyclerViewAdapter;
import com.thel.modules.main.discover.utils.DiscoverUtils;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.ListIterator;

/**
 * 正在直播view
 * Created by lingwei on 2017/11/21.
 */

public class LivingUserRoomView extends LinearLayout {
    private final Context mContext;
    private RecyclerView recyclerview;
    private ArrayList<LiveRoomBean> list = new ArrayList<>();
    private LiveRoomsRecyclerViewAdapter adapter;
    private int cursor;
    private View tx_recommend_follow;
    private LoadDataCallBack callback;

    public LivingUserRoomView(Context context) {
        this(context, null);
    }

    public LivingUserRoomView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LivingUserRoomView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        init();
        setListener();
    }

    private void setListener() {
        adapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                gotoLiveShow(adapter.getItem(position), position);
            }
        });

       /* adapter.setOnLoadMoreListener(new BaseRecyclerViewAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                if (cursor != 0 && callback != null) {//说明有更多数据
                    callback.loadMore(cursor);

                } else {
                    adapter.removeAllFooterView();

                    adapter.openLoadMore(list.size(), false);
                    // adapter.closeLoadMore(getContext(), recyclerview);

                }

            }
        });
        adapter.setReloadMoreListener(new BaseRecyclerViewAdapter.ReloadMoreListener() {
            @Override
            public void reloadMore() {
                adapter.removeAllFooterView();
                //adapter.closeLoadMore(mContext, recyclerview);
                adapter.openLoadMore(true);
                //callback.loadMore(cursor);
            }
        });*/
    }

    private void init() {
        inflate(mContext, R.layout.fragment_live_class_list, this);
        tx_recommend_follow = findViewById(R.id.tx_recommend_follow);
        recyclerview = findViewById(R.id.recyclerview);
        adapter = new LiveRoomsRecyclerViewAdapter(list);
        recyclerview.setHasFixedSize(true);
        recyclerview.setLayoutManager(new GridLayoutManager(mContext, 2));
        final float padding = getResources().getDimension(R.dimen.live_new_user_item_padding);
        DiscoverUtils.setLiveClassifyRecyclerViewDecoration(recyclerview, padding);
        recyclerview.setAdapter(adapter);
        tx_recommend_follow.setVisibility(View.GONE);

    }

    public LivingUserRoomView initView(LiveRoomsBean bean) {
        if (bean == null && bean.list == null)
            return this;

        cursor = bean.cursor;
        list.clear();
        list.addAll(bean.list);
        adapter.setNewData(list);
        return this;
    }

    public void showMoreData(LiveRoomsBean bean) {
        if (bean == null && bean.list == null)
            return;
        cursor = bean.cursor;
        list.addAll(bean.list);
        adapter.openLoadMore(list.size(), true);
        adapter.notifyDataChangedAfterLoadMore(list, true);
    }

    /**
     * 跳转到直播页面
     *
     * @param position
     */
    private void gotoLiveShow(LiveRoomBean liveRoomNetBean, int position) {
        MobclickAgent.onEvent(TheLApp.getContext(), "enter_live_room_from_list");

        if (liveRoomNetBean.isLandscape == 0) {
            ArrayList<LiveRoomBean> cacheList = new ArrayList<>(list);
            ListIterator<LiveRoomBean> iterator = cacheList.listIterator();
            while (iterator.hasNext()) {
                LiveRoomBean lrb = iterator.next();
                if (lrb.isLandscape == 1) {
                    if (position < iterator.nextIndex()) {
                        position--;
                    }
                    iterator.remove();
                }
            }

            Intent intent = new Intent(TheLApp.getContext(), LiveWatchActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            final Bundle bundle = new Bundle();
            bundle.putString(LiveWatchActivity.FROM_PAGE, LiveWatchActivity.FROM_PAGE_LIVE_ROOM_LIST_FRAGMENT);
            bundle.putSerializable(TheLConstants.BUNDLE_KEY_LIVEROOMBEAN_LIST, cacheList);
            bundle.putInt(TheLConstants.BUNDLE_KEY_POSITION, position);
            bundle.putInt(TheLConstants.BUNDLE_KEY_CURSOR, cursor);
            intent.putExtras(bundle);
            TheLApp.getContext().startActivity(intent);
        } else {
            Intent intent = new Intent(getContext(), LiveWatchHorizontalActivity.class);
            intent.putExtra(TheLConstants.BUNDLE_KEY_ID, liveRoomNetBean.id);
            getContext().startActivity(intent);
        }


    }

    public void showRecommendTextVisiable(boolean show) {
        tx_recommend_follow.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    public interface LoadDataCallBack {
        void loadMore(int cursor);
    }

    public void setLoadDataCallBack(LoadDataCallBack callBack) {
        this.callback = callBack;
    }

}
