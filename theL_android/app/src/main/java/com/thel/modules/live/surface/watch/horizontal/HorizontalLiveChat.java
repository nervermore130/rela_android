package com.thel.modules.live.surface.watch.horizontal;

import android.text.TextUtils;

import com.thel.app.TheLApp;
import com.thel.bean.LiveInfoLogBean;
import com.thel.constants.TheLConstants;
import com.thel.modules.live.bean.LiveChatBean;
import com.thel.modules.live.bean.LiveRoomBean;
import com.thel.modules.live.bean.LiveRoomMsgBean;
import com.thel.modules.live.bean.RequestSendDanmuBean;
import com.thel.modules.live.bean.SoftGiftBean;
import com.thel.modules.live.surface.show.InterceptorResponseCallback;
import com.thel.modules.live.utils.LiveUtils;
import com.thel.modules.live.view.expensive.TopGiftBean;
import com.thel.chat.tlmsgclient.IMsgClient;
import com.thel.chat.tlmsgclient.MsgClient;
import com.thel.chat.tlmsgclient.MsgListener;
import com.thel.chat.tlmsgclient.MsgPacket;
import com.thel.chat.tlmsgclient.RemoteRequest;
import com.thel.utils.DeviceUtils;
import com.thel.utils.ExecutorServiceUtils;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.UmentPushUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import io.reactivex.Flowable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

import static com.thel.modules.live.surface.watch.LiveWatchMsgClientCtrl.FOLLOW_CODE;
import static com.thel.modules.live.surface.watch.LiveWatchMsgClientCtrl.RECOMMEND_CODE;
import static com.thel.modules.live.surface.watch.LiveWatchMsgClientCtrl.SHARE_TO_CODE;

public class HorizontalLiveChat {

    private static final String TAG = "HorizontalLiveChat";

    private static HorizontalLiveChat Instance = new HorizontalLiveChat();

    private IMsgClient mClient;

    private List<LiveChatBean.HostBean> hosts = null;

    private String host;

    private int port;

    private int ipIndex;

    private boolean isConnecting = false;

    private boolean isReconnect = false;

    private boolean isReconnecting = false;

    private int broadcasterStatus = 0;

    private long lastReceivedMsgTime;

    private LiveRoomBean mLiveRoomBean;

    private SendGiftRunnable sendGiftRunnable;

    private SendMsgRunnable sendMsgRunnable;

    private SendDanmuRunnable sendDanmuRunnable;

    private PlayGiftRunnable playGiftRunnable;

    private ILiveChatHorizontal mILiveChatHorizontal;

    private CompositeDisposable compositeDisposable;

    private HorizontalLiveChat() {

    }

    public static HorizontalLiveChat getInstance() {
        return Instance;
    }

    public void createClient(LiveRoomBean liveRoomBean, ILiveChatHorizontal iLiveChatHorizontal) {

        L.d(TAG, "--------createClient-------");

        this.mILiveChatHorizontal = iLiveChatHorizontal;

        this.mLiveRoomBean = liveRoomBean;

        initRunnable();

        if (liveRoomBean != null && liveRoomBean.livechat != null && liveRoomBean.livechat.hosts != null) {
            hosts = liveRoomBean.livechat.hosts;
        }

        ipIndex = 0;

        changeIP();

        connectChat();

    }

    private void connectChat() {
        try {

            L.d(TAG, " connectChat host : " + host + " port : " + port);

            UmentPushUtils.onMsgEvent(TheLApp.context, "live_msg_start_connect_android"); //  Android直播消息开始连接

            mClient = new MsgClient(host, port);

            mClient.setMsgListener(mMsgListener);

            mClient.connect();

            isConnecting = true;

            postConnecting();

        } catch (IOException e) {
            e.printStackTrace();

            isConnecting = false;

            postConnectionFailed();

            reconnect();

        }
    }

    private MsgListener mMsgListener = new MsgListener() {

        @Override public void onConnect(IMsgClient iMsgClient) {

            L.d(TAG, " onConnect ");

            initChat();

        }

        @Override public void onRemoteRequest(RemoteRequest remoteRequest) {

            L.i(TAG, " request.getCode() : " + remoteRequest.getCode() + " request.getPayload() : " + remoteRequest.getPayload());

            remoteRequest(remoteRequest);

        }

        @Override public void onClosed(IMsgClient iMsgClient) {

            L.d(TAG, " onClosed ");

            isConnecting = false;
            isReconnecting = false;
            postConnectionInterrupted();
            reconnect();
        }

        @Override public void onError(Exception e) {

            L.d(TAG, " onError ");

            isConnecting = false;
            isReconnecting = false;
            postConnectionInterrupted();
            reconnect();
            if (e != null) {
                e.printStackTrace();
            }
        }
    };

    private void initChat() {
        try {

            UmentPushUtils.onMsgEvent(TheLApp.context, "live_msg_connect_success_android"); //  Android直播消息连接成功

            String body = getInitJsonString();

            UmentPushUtils.onMsgEvent(TheLApp.context, "live_msg_start_auth_android"); //  Android  直播消息开始认证

            mClient.request("init", body).enqueue(new InterceptorResponseCallback() {
                @Override
                public void onError(Throwable error) {

                    L.d(TAG, " init onError ");

                    UmentPushUtils.onMsgEvent(TheLApp.context, "live_msg_fail", "auth_timeout"); //  Android  认证超时

                    LiveInfoLogBean.getInstance().getLiveChatAnalytics().errorReason = "auth_timeout";

                    pushErrorLog();

                    isConnecting = false;
                    isReconnecting = false;
                    if (error != null)
                        error.printStackTrace();
                    postConnectionFailed();
                    reconnect();
                }

                @Override
                public void onResponse(MsgPacket packet) {

                    L.d(TAG, " init onResponse " + packet.getCode());

                    initPingTimer();

                    isConnecting = false;
                    isReconnecting = false;
                    if ("OK".equals(packet.getCode())) {
                        UmentPushUtils.onMsgEvent(TheLApp.context, "live_msg_auth_success_android"); //  Android  直播消息认证成功

                        postConnectSucceed();

                        if (mILiveChatHorizontal != null) {

                            mILiveChatHorizontal.autoPingServer();

                        }

                        isReconnect = true;

                    } else {

                        UmentPushUtils.onMsgEvent(TheLApp.context, "live_msg_fail", packet.getCode()); //  Android  认证失败

                        if ("no_such_channel".equals(packet.getCode())) {
                            if (mILiveChatHorizontal != null) {

                                mILiveChatHorizontal.liveClosed();

                            }
                        }

                        if ("require_channel_id".equals(packet.getCode())) {

                            if (mILiveChatHorizontal != null) {

                                mILiveChatHorizontal.showDialogChannelId();

                            }

                        }

                        if ("invalid_request".equals(packet.getCode())) {
                            if (mILiveChatHorizontal != null) {

                                mILiveChatHorizontal.showDialogRequest();

                            }

                        }

                        if ("require_user_id".equals(packet.getCode())) {

                            if (mILiveChatHorizontal != null) {

                                mILiveChatHorizontal.showAlertUserId();

                            }
                        }

                        if ("require_nickname".equals(packet.getCode())) {

                            if (mILiveChatHorizontal != null) {

                                mILiveChatHorizontal.showAlertUserName();

                            }
                        }

                        LiveInfoLogBean.getInstance().getLiveChatAnalytics().errorReason = "live_msg_fail_" + packet.getCode();

                        pushErrorLog();

                    }

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void remoteRequest(RemoteRequest request) {

        if (!TextUtils.isEmpty(request.getCode())) {
            try {
                lastReceivedMsgTime = System.currentTimeMillis();// 刷新最后一条收到消息的时间
                if (LiveRoomMsgBean.TYPE_MSG.equals(request.getCode())) {// 收到普通消息
                    final LiveRoomMsgBean liveRoomMsgBean = GsonUtils.getObject(request.getPayload(), LiveRoomMsgBean.class);
                    if (LiveRoomMsgBean.TYPE_UPLOADER_STATUS.equals(liveRoomMsgBean.type)) {// 主播网络状态变化消息
                        int status = Integer.parseInt(liveRoomMsgBean.content);
                        if (status != broadcasterStatus) {
                            broadcasterStatus = status;
                            if (mILiveChatHorizontal != null) {
                                mILiveChatHorizontal.updateBroadcasterNetStatus(broadcasterStatus);
                            }
                        }
                    } else if (LiveRoomMsgBean.TYPE_MSG.equals(liveRoomMsgBean.type) || LiveRoomMsgBean.TYPE_NOTICE.equals(liveRoomMsgBean.type)) {

                        if (mILiveChatHorizontal != null) {
                            mILiveChatHorizontal.addLiveRoomMsg(liveRoomMsgBean);
                            mILiveChatHorizontal.refreshMsg();
                        }

                    }
                } else if (LiveRoomMsgBean.TYPE_SYS_MSG.equals(request.getCode())) {// 收到系统消息
                    final LiveRoomMsgBean liveRoomMsgBean = GsonUtils.getObject(request.getPayload(), LiveRoomMsgBean.class);
                    if (LiveRoomMsgBean.TYPE_JOIN.equals(liveRoomMsgBean.type) || LiveRoomMsgBean.TYPE_BANED.equals(liveRoomMsgBean.type) || LiveRoomMsgBean.TYPE_LEAVE.equals(liveRoomMsgBean.type)) {
                        if (mILiveChatHorizontal != null) {
                            mILiveChatHorizontal.addLiveRoomMsg(liveRoomMsgBean);
                            mILiveChatHorizontal.refreshMsg();
                        }
                    }
                } else if ("update".equals(request.getCode())) {// 收到房间信息更新消息
                    mLiveRoomBean.liveUsersCount = new JSONObject(request.getPayload()).getInt("userCount");
                    mLiveRoomBean.rank = new JSONObject(request.getPayload()).optInt("rank", 0);
                    mLiveRoomBean.gem = new JSONObject((request.getPayload())).optString("gem", mLiveRoomBean.gem);
                    if (mILiveChatHorizontal != null) {
                        mILiveChatHorizontal.updateAudienceCount();
                    }
                } else if ("close".equals(request.getCode())) {// 直播已关闭
                    if (mILiveChatHorizontal != null) {
                        mILiveChatHorizontal.liveClosed();
                    }
                } else if (LiveRoomMsgBean.TYPE_GIFT_MSG.equals(request.getCode()) || LiveRoomMsgBean.CODE_SPECIAL_GIFT.equals(request.getCode())) {//赠送礼物广播

                    if (mILiveChatHorizontal != null) {
                        mILiveChatHorizontal.parseGiftMsg(request.getCode(), request.getPayload());
                    }

                    //request.response("gift_report\n", request.getPayload());
                } else if (LiveRoomMsgBean.TYPE_DANMU_MSG.equals(request.getCode())) {//接受弹幕消息
                    if (mILiveChatHorizontal != null) {
                        mILiveChatHorizontal.showDanmu(request.getPayload());
                    }
                } else if (LiveRoomMsgBean.TYPE_FREE_BARRAGE.equals(request.getCode())) {
                    if (mILiveChatHorizontal != null) {
                        mILiveChatHorizontal.freeBarrage();
                    }
                } else if (LiveRoomMsgBean.TYPE_VISIT.equals(request.getCode())) {//新用户加入

                } else if (LiveRoomMsgBean.TYPE_VIP_VISIT.equals(request.getCode())) {//VIP用户加入
                    final LiveRoomMsgBean liveRoomMsgBean = GsonUtils.getObject(request.getPayload(), LiveRoomMsgBean.class);
                    if (LiveRoomMsgBean.TYPE_JOIN.equals(liveRoomMsgBean.type)) {//用户加入房间
                        if (mILiveChatHorizontal != null) {
                            mILiveChatHorizontal.joinVipUser(liveRoomMsgBean);
                        }
                    }
                } else if (LiveRoomMsgBean.TYPE_FOLLOW.equals(request.getCode())) {//关注
                    final LiveRoomMsgBean liveRoomMsgBean = LiveUtils.getFollowMsgBean(request.getPayload());
                    if (liveRoomMsgBean != null && !TextUtils.isEmpty(liveRoomMsgBean.content)) {
                        if (mILiveChatHorizontal != null) {
                            mILiveChatHorizontal.addLiveRoomMsg(liveRoomMsgBean);
                            mILiveChatHorizontal.refreshMsg();
                        }
                    }

                } else if (LiveRoomMsgBean.TYPE_RECOMM.equals(request.getCode())) {//推荐
                    final LiveRoomMsgBean liveRoomMsgBean = LiveUtils.getRecommendMsgBean(request.getPayload());
                    if (liveRoomMsgBean != null && !TextUtils.isEmpty(liveRoomMsgBean.content)) {
                        if (mILiveChatHorizontal != null) {
                            mILiveChatHorizontal.addLiveRoomMsg(liveRoomMsgBean);
                            mILiveChatHorizontal.refreshMsg();
                        }
                    }
                } else if (LiveRoomMsgBean.TYPE_SHARETO.equals(request.getCode())) {//分享
                    final LiveRoomMsgBean liveRoomMsgBean = LiveUtils.getSharetToMsgBean(request.getPayload());
                    if (liveRoomMsgBean != null && !TextUtils.isEmpty(liveRoomMsgBean.content)) {
                        if (mILiveChatHorizontal != null) {
                            mILiveChatHorizontal.addLiveRoomMsg(liveRoomMsgBean);
                            mILiveChatHorizontal.refreshMsg();
                        }
                    }
                } else if (LiveRoomMsgBean.TYPE_GIFTCOMBO.equals(request.getCode())) {//礼物连击
                    if (mLiveRoomBean != null && mLiveRoomBean.softEnjoyBean != null) {
                        final LiveRoomMsgBean liveRoomMsgBean = LiveUtils.getSendGiftMsgBean(request.getPayload(), mLiveRoomBean.softEnjoyBean);
                        if (liveRoomMsgBean != null && !TextUtils.isEmpty(liveRoomMsgBean.content)) {
                            if (mILiveChatHorizontal != null) {
                                mILiveChatHorizontal.addLiveRoomMsg(liveRoomMsgBean);
                                mILiveChatHorizontal.refreshMsg();
                            }
                        }
                    }
                } else if (LiveRoomMsgBean.TYPE_TOPGIFT.equals(request.getCode())) {
                    if (mILiveChatHorizontal != null) {
                        final TopGiftBean topGiftBean = GsonUtils.getObject(request.getPayload(), TopGiftBean.class);
                        mILiveChatHorizontal.showTopGiftView(topGiftBean);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void reconnect() {

        L.d(TAG, " ---------reconnect-------- : ");

        isReconnecting = true;
        disconnect();
        pushData();
        connectChat();
    }

    private void disconnect() {

        if (mILiveChatHorizontal != null) {
            mILiveChatHorizontal.autoPingServer();
        }

        try {
            if (mClient != null) {
                mClient.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        mClient = null;

    }


    private void pushData() {

        UmentPushUtils.onMsgEvent(TheLApp.context, "live_msg_fail", "live_msg_connect_fail_android"); //  Android  连接失败

        LiveInfoLogBean.getInstance().getLiveChatAnalytics().errorReason = "connect_fail";

        pushErrorLog();

        changeIP();
    }

    private void initRunnable() {

        sendGiftRunnable = new SendGiftRunnable();
        sendMsgRunnable = new SendMsgRunnable();
        sendDanmuRunnable = new SendDanmuRunnable();
        playGiftRunnable = new PlayGiftRunnable();

    }

    public void close() {
        if (mClient != null) {
            try {
                mClient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            mClient = null;
        }

        if (compositeDisposable != null) {
            compositeDisposable.clear();
        }

        isReconnect = false;

        isConnecting = false;

        isReconnecting = false;
    }

    public void sendSoftGift(SoftGiftBean softGiftBean, int id, String type, int comb) {
        sendGiftRunnable.setSoftGiftBean(softGiftBean, id, type, comb);
        ExecutorServiceUtils.getInstatnce().exec(sendGiftRunnable);
    }

    public void postGiftPlayStatus(String userId, String toUserId, int id) {
        playGiftRunnable.setPlayGiftMsg(userId, toUserId, id);
        ExecutorServiceUtils.getInstatnce().exec(playGiftRunnable);
    }

    public void sendMsg(String msg) {
        sendMsgRunnable.setMsgContent(msg);
        ExecutorServiceUtils.getInstatnce().exec(sendMsgRunnable);
    }

    public void sendDanmu(String userLevel, String content, String barrageId) {
        sendDanmuRunnable.setDanmuContent(userLevel, content, barrageId);
        ExecutorServiceUtils.getInstatnce().exec(sendDanmuRunnable);
    }

    public void addFollowFans() {
        ExecutorServiceUtils.getInstatnce().exec(new CommonRunnable(FOLLOW_CODE));
    }

    public void shareToLive() {
        ExecutorServiceUtils.getInstatnce().exec(new CommonRunnable(SHARE_TO_CODE));
    }

    public void recommendLive() {
        ExecutorServiceUtils.getInstatnce().exec(new CommonRunnable(RECOMMEND_CODE));
    }

    /**
     * 赠送礼物消息runnable
     */
    class SendGiftRunnable implements Runnable {

        private int id;
        private String type;//赠送礼物 类型（）
        private int combo;
        private SoftGiftBean softGiftBean;

        public void setSoftGiftBean(SoftGiftBean softGiftBean, int id, String type, int combo) {
            this.id = id;
            this.type = type;
            this.combo = combo;
            this.softGiftBean = softGiftBean;
        }

        @Override
        public void run() {
            try {
                if (mClient != null)
                    mClient.request(type, new JSONObject().put("id", id).put("combo", combo).toString()).enqueue(new InterceptorResponseCallback() {
                        @Override
                        public void onError(Throwable error) {

                            L.i(TAG, " send gift msg error : " + error.getMessage());

                            error.printStackTrace();
                            isConnecting = false;
                            isReconnecting = false;
                            postConnectionInterrupted();
                            reconnect();
                        }

                        @Override
                        public void onResponse(MsgPacket packet) {

                            L.d(TAG, " SendGiftRunnable onResponse getPayload : " + new String(packet.getPayload()));

                            L.d(TAG, " SendGiftRunnable onResponse getCode : " + packet.getCode());

                            if ("OK".equals(packet.getCode())) {//赠送礼物成功，打赏成功

                                if (mILiveChatHorizontal != null) {

                                    String result = new String(packet.getPayload());

                                    mILiveChatHorizontal.updateBalance(result);
                                }

                                String myUserid = ShareFileUtils.getString(ShareFileUtils.ID, "");
                                String brocaster_id = ShareFileUtils.getString(ShareFileUtils.Brocaster_ID, "");
                                try {

                                    if (mClient != null && !TextUtils.isEmpty(softGiftBean.resource)) {
                                        L.d("LiveWatchMsgClientCtrl", " SendGiftRunnable softBean: " + softGiftBean.toString());

                                        mClient.request("gift_report", new JSONObject().put("userId", myUserid).put("toUserId", brocaster_id).put("giftId", softGiftBean.id).put("action", "receipt").toString()).enqueue(new InterceptorResponseCallback());
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                            } else if (LiveRoomMsgBean.TYPE_BANED.equals(packet.getCode())) {// 被屏蔽了

                                if (mILiveChatHorizontal != null) {
                                    mILiveChatHorizontal.beenBlocked();
                                }

                            }
                        }
                    });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 上报服务端礼物播放了的消息
     */
    private class PlayGiftRunnable implements Runnable {

        private String userId;
        private String toUserid;
        private int giftid;

        public void setPlayGiftMsg(String userId, String toUserid, int id) {
            this.userId = userId;
            this.toUserid = toUserid;
            this.giftid = id;
        }

        @Override
        public void run() {
            try {
                if (mClient != null) {
                    String brocaster_id = ShareFileUtils.getString(ShareFileUtils.Brocaster_ID, "");

                    mClient.request("gift_report", new JSONObject().put("userId", userId).put("toUserId", brocaster_id).put("giftId", giftid).put("action", "play").toString()).enqueue(new InterceptorResponseCallback() {
                        @Override
                        public void onError(Throwable throwable) {
                            super.onError(throwable);
                        }

                        @Override
                        public void onResponse(MsgPacket packet) {
                            L.d("LiveWatchMsgClientCtrl", " PlayGiftRunnable onResponse getPayload : " + new String(packet.getPayload()));

                            L.d("LiveWatchMsgClientCtrl", " PlayGiftRunnable onResponse getCode : " + packet.getCode());

                            super.onResponse(packet);
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * 普通消息
     */
    private class SendMsgRunnable implements Runnable {
        private String msgContent = "";

        public void setMsgContent(String msgContent) {
            this.msgContent = msgContent;
        }

        @Override
        public void run() {
            try {
                if (mClient != null) {
                    mClient.request("send", new JSONObject().put("type", LiveRoomMsgBean.TYPE_MSG).put("content", msgContent + "").toString()).enqueue(new InterceptorResponseCallback() {
                        @Override
                        public void onError(Throwable error) {
                            error.printStackTrace();
                            isConnecting = false;
                            isReconnecting = false;
                            L.d(TAG, " send msg error");
                            postConnectionInterrupted();
                            reconnect();
                        }

                        @Override
                        public void onResponse(MsgPacket packet) {

                            L.d(TAG, " send msg code : " + packet.getCode());

                            if (LiveRoomMsgBean.TYPE_BANED.equals(packet.getCode())) {// 被屏蔽了
                                if (mILiveChatHorizontal != null) {
                                    mILiveChatHorizontal.beenBlocked();
                                }
                            } else if (LiveRoomMsgBean.RESPONSE_TYPE_TOO_FAST.equals(packet.getCode())) {// 说话频率太快
                                if (mILiveChatHorizontal != null) {
                                    mILiveChatHorizontal.speakTooFast();
                                }
                            }
                        }
                    });

                    if (mILiveChatHorizontal != null) {
                        mILiveChatHorizontal.clearInput();
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 发送弹幕runnable
     */
    private class SendDanmuRunnable implements Runnable {

        private String userLevel;

        private String content = "";

        private String barrageId;

        public void setDanmuContent(String userLevel, String content, String barrageId) {
            this.userLevel = userLevel;
            this.content = content;
            this.barrageId = barrageId;
        }

        @Override
        public void run() {
            try {
                if (mClient != null) {

                    RequestSendDanmuBean requestSendDanmuBean = new RequestSendDanmuBean();
                    requestSendDanmuBean.userLevel = userLevel;
                    requestSendDanmuBean.content = content;
                    requestSendDanmuBean.barrageId = barrageId;

                    String body = GsonUtils.createJsonString(requestSendDanmuBean);

                    if (mILiveChatHorizontal != null) {
                        mILiveChatHorizontal.lockInput();
                    }

                    mClient.request("sendbarrage", body).enqueue(new InterceptorResponseCallback() {
                        @Override
                        public void onError(Throwable error) {
                            //TODU shibai bing jiesuo
                            if (mILiveChatHorizontal != null) {
                                mILiveChatHorizontal.openInput();
                            }
                            error.printStackTrace();
                            isConnecting = false;
                            isReconnecting = false;
                            L.d(TAG, " send danmu error");
                            postConnectionInterrupted();
                            reconnect();
                        }

                        @Override
                        public void onResponse(MsgPacket packet) {

                            L.d(TAG, " SendDanmuRunnable getPayload " + new String(packet.getPayload()));

                            if ("OK".equals(packet.getCode())) {//赠送礼物成功，打赏成功

                                if (mILiveChatHorizontal != null) {

                                    String result = new String(packet.getPayload());

                                    mILiveChatHorizontal.setDanmuResult(result);

                                }

                            } else if (LiveRoomMsgBean.TYPE_BANED.equals(packet.getCode())) {// 被屏蔽了

                                if (mILiveChatHorizontal != null) {
                                    mILiveChatHorizontal.beenBlocked();
                                }

                            }
                        }
                    });
                }
            } catch (Exception e) {
                // TODO shibai bing jiesuo
                if (mILiveChatHorizontal != null) {
                    mILiveChatHorizontal.openInput();
                }
                e.printStackTrace();
            }
        }

    }

    private void initPingTimer() {

        if (compositeDisposable != null) {
            compositeDisposable.clear();
        } else {
            compositeDisposable = new CompositeDisposable();
        }

        Disposable disposable = Flowable.interval(30, TimeUnit.SECONDS).onBackpressureDrop().subscribe(new Consumer<Long>() {
            @Override public void accept(Long aLong) throws Exception {
                ExecutorServiceUtils.getInstatnce().exec(new PingRunnable());
            }
        });

        compositeDisposable.add(disposable);

    }

    private class PingRunnable implements Runnable {

        @Override public void run() {
            mClient.request("ping", "", 5000).enqueue(new InterceptorResponseCallback() {
                @Override
                public void onError(Throwable error) {
                    isConnecting = false;
                    isReconnecting = false;
                    postConnectionInterrupted();
                    reconnect();
                    if (error != null)
                        error.printStackTrace();
                    UmentPushUtils.onMsgEvent(TheLApp.context, "live_msg_ping_timeout_android");
                }

                @Override
                public void onResponse(MsgPacket packet) {
                    if (!"pong".equals(packet.getCode())) {
                        // ping不通，重连
                        UmentPushUtils.onMsgEvent(TheLApp.context, "live_msg_ping_ip_fail_android"); //  Android  ping IP失败
                        isConnecting = false;
                        isReconnecting = false;
                        postConnectionInterrupted();
                        reconnect();
                        UmentPushUtils.onMsgEvent(TheLApp.context, "live_msg_ping_fail_android");
                    }
                }
            });
        }
    }

    /**
     * 主要用于一些仅发请求不讲结果的消息发送
     * 目前包括 关注， 推荐主播，分享直播，
     */
    private class CommonRunnable implements Runnable {
        private final String code;

        public CommonRunnable(String code) {
            this.code = code;
        }

        @Override
        public void run() {
            try {
                if (mClient != null) {
                    mClient.request(code, "").enqueue(new InterceptorResponseCallback() {
                        @Override
                        public void onError(Throwable error) {

                        }

                        @Override
                        public void onResponse(MsgPacket packet) {

                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }


    private void postConnectionFailed() {

        L.i(TAG, "--------postConnectionFailed-------");

        LiveRoomMsgBean liveRoomMsgBean = new LiveRoomMsgBean();
        liveRoomMsgBean.type = LiveRoomMsgBean.TYPE_CONNECT_FAILED;
        if (mILiveChatHorizontal != null) {
            mILiveChatHorizontal.addLiveRoomMsg(liveRoomMsgBean);
            mILiveChatHorizontal.refreshMsg();
        }
    }

    private void postConnectSucceed() {

        L.i(TAG, "--------postConnectSucceed-------");

        LiveRoomMsgBean liveRoomMsgBean = new LiveRoomMsgBean();
        liveRoomMsgBean.type = LiveRoomMsgBean.TYPE_CONNECT_SUCCEED;
        if (mILiveChatHorizontal != null) {
            mILiveChatHorizontal.addLiveRoomMsg(liveRoomMsgBean);
            mILiveChatHorizontal.refreshMsg();
        }
    }

    private void postConnecting() {

        L.i(TAG, "--------postConnecting-------");

        LiveRoomMsgBean liveRoomMsgBean = new LiveRoomMsgBean();
        liveRoomMsgBean.type = LiveRoomMsgBean.TYPE_CONNECTING;
        if (mILiveChatHorizontal != null) {
            mILiveChatHorizontal.addLiveRoomMsg(liveRoomMsgBean);
            mILiveChatHorizontal.refreshMsg();
        }
    }

    private void postConnectionInterrupted() {

        L.i(TAG, "--------postConnectionInterrupted-------");

        LiveRoomMsgBean liveRoomMsgBean = new LiveRoomMsgBean();
        liveRoomMsgBean.type = LiveRoomMsgBean.TYPE_CONNECT_INTERRUPTED;
        if (mILiveChatHorizontal != null) {
            mILiveChatHorizontal.addLiveRoomMsg(liveRoomMsgBean);
            mILiveChatHorizontal.refreshMsg();
        }
    }


    private void pushErrorLog() {
        LiveInfoLogBean.getInstance().getLiveChatAnalytics().role = "audience";
        LiveInfoLogBean.getInstance().getLiveChatAnalytics().cdnDomain = host;
        LiveInfoLogBean.getInstance().getLiveChatAnalytics().logType = TheLConstants.LiveInfoLogConstants.LIVE_MSG_ERROR_LOG;
        LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getLiveChatAnalytics());
    }

    private void changeIP() {

        if (hosts == null || hosts.size() == 0) {
            if (mLiveRoomBean != null && mLiveRoomBean.livechat != null) {
                host = mLiveRoomBean.livechat.host;
                port = mLiveRoomBean.livechat.port;
            }
        } else {
            if (hosts.size() <= ipIndex) {
                ipIndex = 0;
            }
            LiveChatBean.HostBean hostBean = hosts.get(ipIndex);
            host = hostBean.host;
            port = hostBean.port;
            ipIndex++;
        }
    }

    /**
     * init 的param json 数据
     *
     * @return
     */
    private String getInitJsonString() {
        double lt = 0.0;
        double lg = 0.0;
        try {
            lt = Double.parseDouble(ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0"));
            lg = Double.parseDouble(ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        final double lat = lt;
        final double lng = lg;
        final JSONObject obj = new JSONObject() {
            {
                try {
                    put("version", TheLConstants.MSG_CLIENT_VERSION);
                    put("os", "Android");
                    put("appver", DeviceUtils.getVersionCode(TheLApp.getContext()) + "");
                    put("deviceId", ShareFileUtils.getString(ShareFileUtils.DEVICE_ID, UUID.randomUUID().toString()));
                    put("lat", lat);
                    put("lng", lng);
                    put("lang", DeviceUtils.getLanguage());
                    put("channel", mLiveRoomBean.livechat.channel);
                    put("token", mLiveRoomBean.livechat.token);
                    put("userId", ShareFileUtils.getString(ShareFileUtils.ID, ""));
                    put("nickName", ShareFileUtils.getString(ShareFileUtils.USER_NAME, ""));
                    put("avatar", ShareFileUtils.getString(ShareFileUtils.AVATAR, ""));
                    put("isOwner", false);
                    put("reconnect", isReconnect);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        return obj.toString();
    }

}
