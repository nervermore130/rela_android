package com.thel.modules.main.userinfo;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.user.UserInfoPicBean;
import com.thel.utils.AppInit;
import com.thel.utils.ImageUtils;

import java.util.ArrayList;
import java.util.List;

public class UserInfoGridAdapter extends BaseAdapter {

    private List<UserInfoPicBean> userlist = new ArrayList<UserInfoPicBean>();

    private LayoutInflater mInflater;

    private int size; // 图片宽度

    public UserInfoGridAdapter(List<UserInfoPicBean> userlist) {
        mInflater = (LayoutInflater) TheLApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        freshAdapter(userlist);
        int screenWidth = AppInit.displayMetrics.widthPixels;// 屏幕宽度

        // 这个数值是根据布局的margin、padding等计算出来的，具体要看user_info_main.xml布局
        size = (screenWidth - TheLApp.getContext().getResources().getDimensionPixelSize(R.dimen.moment_list_photo_margin) * 2) / 3;
    }

    public void freshAdapter(List<UserInfoPicBean> userlist) {
        this.userlist = userlist;
    }

    @Override
    public int getCount() {
        return userlist.size();
    }

    @Override
    public Object getItem(int position) {
        return userlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        HolderView holdView = null;
        if (null == convertView) {
            convertView = mInflater.inflate(R.layout.adapter_user_info_pic_item, parent, false);
            holdView = new HolderView();
            holdView.img_thumb = convertView.findViewById(R.id.img_thumb);
            convertView.setTag(holdView);
        } else {
            holdView = (HolderView) convertView.getTag();
        }
        final UserInfoPicBean bean = userlist.get(position);
        //ImageLoaderManager.imageLoader(holdView.img_thumb, R.drawable.bg_userinfo_photo_shape, userlist.get(position).longThumbnailUrl);
        holdView.img_thumb.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(bean.longThumbnailUrl, size, size))).build()).setAutoPlayAnimations(true).build());

        return convertView;
    }

    class HolderView {
        SimpleDraweeView img_thumb;
    }

}
