package com.thel.modules.live.liveBigGiftAnimLayout.anim;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.Point;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.thel.R;
import com.thel.modules.live.liveBigGiftAnimLayout.LiveBigAnimUtils;

import java.util.Random;

/**
 * Created by test1 on 17/1/5.
 */

public class YuanXiaoAnimator {
    private final Context mContext;
    private final int mDuration;
    private final Handler mHandler;
    //  private final int[] petalRes;
    // private final int[] smogRes;
    //private final int[] lanternRes;
    //private final int[] tangyuanRes;
    private final String[] petalRes;
    private final String[] smogRes;
    private final String[] lanternRes;
    private final String[] tangyuanRes;
    private final String treeRes;
    private final int tangyuanRate;
    private final int differPetalWidth;
    private final Random mRandom;
    private final int rate;
    private final int minpetalWidth;
    private final int differPetalCount;
    private final int minPetalCount;
    private final int treeDropDuration;
    private final int lanternDropDuration;
    private final int lanterRate;
    private final int differPetalBallWidth;
    private final int minPetalWidth;
    private final int minPetalBallWidth;
    private final int minRotateCycle;
    private final int differRotateCycler;
    private final int minDropDuration;
    private final String foldPath;
    private boolean isPlaying;
    private int mWidth;
    private int mHeight;
    private ImageView ty;

    //   构造方法 传入上下文以及执行时间
    public YuanXiaoAnimator(Context context, int duration) {
        this.mContext = context;
        this.mDuration = duration;
        mRandom = new Random();
        //花瓣变化区间
        differPetalCount = 5;
        //树出现的执行时间
        treeDropDuration = 800;
        //灯笼下落时间
        lanternDropDuration = 500;
        minPetalCount = 3;
        mHandler = new Handler(Looper.getMainLooper());
        ///存放花瓣的资源
        //        petalRes = new int[]{R.drawable.petal, R.drawable.petal1, R.drawable.petal2, R.drawable.petal3};
        //        lanternRes = new int[]{R.drawable.lantern1, R.drawable.lantern2, R.drawable.lantern3, R.drawable.lantern4, R.drawable.lantern5, R.drawable.lantern6, R.drawable.lantern7, R.drawable.lantern8};
        //        tangyuanRes = new int[]{R.drawable._bowl, R.drawable._bowl1, R.drawable._bowl2};
        //        smogRes = new int[]{R.drawable.smog1, R.drawable.smog2};
        foldPath = "anim/yuanxiao";
        petalRes = new String[]{"petal", "petal1", "petal2", "petal3"};
        lanternRes = new String[]{"lantern1", "lantern2", "lantern3", "lantern4", "lantern5", "lantern6", "lantern7", "lantern8"};
        tangyuanRes = new String[]{"_bowl", "_bowl1", "_bowl2"};
        smogRes = new String[]{"smog1", "smog2"};
        treeRes = "tree";
        //花瓣飘落每波间隔
        rate = 400;
        minPetalWidth = dip2px(mContext, 10);//最小花瓣大小
        differPetalBallWidth = dip2px(mContext, 7);//花瓣宽度变化区间
        //最小掉落时间
        minDropDuration = 3000;
        //最小花瓣宽度
        minPetalBallWidth = dip2px(mContext, 7);
        //花瓣宽度变化区间
        differPetalWidth = dip2px(mContext, 10);
        //花瓣转动最小周期
        minRotateCycle = 2000;
        //雪转动周期变化区间
        differRotateCycler = 3000;
        //灯笼动画间隔
        lanterRate = 200;
        //汤圆动画间隔
        tangyuanRate = 200;
        //最小的花瓣大小
        minpetalWidth = dip2px(mContext, 10);
        //花瓣宽度变化区间

    }

    //开始动画
    public int start(final ViewGroup parent) {

        isPlaying = true;
        mWidth = parent.getMeasuredWidth();
        mHeight = parent.getMeasuredHeight();
        if (mWidth == 0 || mHeight == 0) {
            return 0;
        }

        final RelativeLayout background = new RelativeLayout(mContext);
        background.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
        parent.addView(background);
        setBackground(background);
        //执行完时间就将动画移除掉
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                isPlaying = false;
                background.clearAnimation();
                parent.removeView(background);
            }
        }, mDuration);

        return mDuration;
    }

    private void setBackground(RelativeLayout background) {
        //添加树
        addBody(background);
        addTangYuan(background);
        addLanter(background);
        //添加花瓣
        addPetals(background);
    }

    private void addLanter(RelativeLayout background) {
        //添加灯笼效果
        final ImageView img_lanter = new ImageView(mContext);
        //  img_lanter.setBackgroundResource(R.drawable.lantern1);
        LiveBigAnimUtils.setAssetBackground(img_lanter, foldPath, lanternRes[0]);
        background.addView(img_lanter);
        ViewGroup.LayoutParams params = img_lanter.getLayoutParams();
        int w = Math.min(mWidth, mHeight);
        int h = w * 444 / 612;

        params.width = w;
        params.height = h;
        if (params instanceof RelativeLayout.LayoutParams) {
            ((RelativeLayout.LayoutParams) params).addRule(RelativeLayout.CENTER_HORIZONTAL);
        }
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(img_lanter, "translationY", -h, 0);
        objectAnimator.setDuration(lanternDropDuration);
        objectAnimator.start();
        objectAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                //  BigGiftAnimTyLayout.setFrameAnim(mContext, img_lanter, lanternRes, lanterRate);
                LiveBigAnimUtils.setFrameAnim(mContext, img_lanter, foldPath, lanternRes, lanterRate);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
    }

    private void addTangYuan(RelativeLayout background) {
        //添加汤圆
        final View img_smog_ty = RelativeLayout.inflate(mContext, R.layout.live_big_anim_yuanxiao_ty_layout, null);

        ty = img_smog_ty.findViewById(R.id.ty);
        final View smog = img_smog_ty.findViewById(R.id.smog);
        LiveBigAnimUtils.setAssetBackground(ty, foldPath, tangyuanRes[0]);
        LiveBigAnimUtils.setAssetBackground(smog, foldPath, smogRes[0]);

        background.addView(img_smog_ty);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) img_smog_ty.getLayoutParams();


        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        int measuredWidth = img_smog_ty.getMeasuredWidth();
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(img_smog_ty, "translationX", (mWidth + measuredWidth) / 2, 0);
        objectAnimator.setDuration(lanternDropDuration);
        objectAnimator.start();
        objectAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                //  BigGiftAnimTyLayout.setFrameAnim(mContext, ty, tangyuanRes, tangyuanRate);
                //BigGiftAnimTyLayout.setFrameAnim(mContext, smog, smogRes, tangyuanRate);
                LiveBigAnimUtils.setFrameAnim(mContext, ty, foldPath, tangyuanRes, tangyuanRate);
                LiveBigAnimUtils.setFrameAnim(mContext, smog, foldPath, smogRes, tangyuanRate);

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    public int dip2px(Context context, float dipValue) {
        DisplayMetrics dm = new DisplayMetrics();
        dm = mContext.getResources().getDisplayMetrics();
        return (int) (dipValue * dm.density + 0.5f);
    }

    //添加树
    private void addBody(RelativeLayout background) {
        //填充树布局
        final RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final int w = Math.min(mWidth, mHeight);
        final int h = w * 445 / 750;
        params.width = w;
        params.height = h;
        params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        params.addRule(RelativeLayout.CENTER_HORIZONTAL);
        final ImageView img_tree = new ImageView(mContext);
        // img_tree.setImageResource(R.drawable.tree);
        LiveBigAnimUtils.setAssetImage(img_tree, foldPath, treeRes);
        //img_tree.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        img_tree.setLayoutParams(params);
        background.addView(img_tree);
        ObjectAnimator objecAnimator = ObjectAnimator.ofFloat(img_tree, "translationX", -w, 0);
        objecAnimator.setDuration(treeDropDuration);
        objecAnimator.start();

    }

    /**
     * 下花瓣
     */
    private void addPetals(final ViewGroup background) {
        background.post(new Runnable() {
            @Override
            public void run() {
                int PetalCount = mRandom.nextInt(differPetalCount) + minPetalCount;
                for (int i = 0; i < PetalCount; i++) {
                    dropOnePetal(background);
                }
                if (isPlaying) {
                    background.postDelayed(this, rate);
                }
            }
        });
    }

    /**
     * 单个雪
     */
    private void dropOnePetal(final ViewGroup background) {
        if (!isPlaying) {
            return;
        }

        final ImageView img_petal = new ImageView(mContext);
        background.addView(img_petal);
        int resIndex = mRandom.nextInt(petalRes.length);
        //    int res = petalRes[resIndex];
        int w = minpetalWidth + mRandom.nextInt(differPetalWidth);
        if (resIndex == 0) {
            w = minpetalWidth + mRandom.nextInt(differPetalBallWidth);
        }
        int h = getImageHeight(resIndex, w);
        //   img_petal.setImageResource(petalRes[resIndex]);
        LiveBigAnimUtils.setAssetImage(img_petal, foldPath, petalRes[resIndex]);
        img_petal.setLayoutParams(new RelativeLayout.LayoutParams(w, h));
        Point startPoint = new Point(mRandom.nextInt(mWidth), 0 - h);
        Point endPoint = new Point(startPoint.x, mHeight);
        Path path = new Path();
        path.moveTo(startPoint.x, startPoint.y);
        path.lineTo(endPoint.x, endPoint.y);
        int cycle = 0;
        if (resIndex == 0 || resIndex == 2) {
            cycle = minRotateCycle + mRandom.nextInt(differRotateCycler);
        }
        int dropDuration = minDropDuration + mRandom.nextInt(differRotateCycler);
        FloatAnimation floatAnimation = new FloatAnimation(path, background, img_petal, dropDuration, cycle);
        floatAnimation.setDuration(dropDuration);
        floatAnimation.setInterpolator(new LinearInterpolator());
        floatAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                background.post(new Runnable() {
                    @Override
                    public void run() {
                        background.removeView(img_petal);
                    }
                });
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        img_petal.startAnimation(floatAnimation);

    }

    private int getImageHeight(int resId, int img_width) {
        int img_height;
        switch (resId) {
            case 1:
                img_height = img_width * 575 / 580;
                break;
            case 2:
                img_height = img_width;
                break;
            case 3:
                img_height = img_width * 340 / 296;
                break;
            default:
                img_height = img_width;
                break;
        }
        return img_height;
    }

    class FloatAnimation extends Animation {
        private final int mCycle;
        private final int mDropDuration;
        private PathMeasure mPm;
        private View mView;
        private float mDistance;

        public FloatAnimation(Path path, View parent, View child, int dropDuration, int cycle) {
            mPm = new PathMeasure(path, false);
            mDistance = mPm.getLength();
            mView = child;
            parent.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            mCycle = cycle;
            mDropDuration = dropDuration;
        }

        @Override
        protected void applyTransformation(float factor, Transformation transformation) {
            Matrix matrix = transformation.getMatrix();
            mPm.getMatrix(mDistance * factor, matrix, PathMeasure.POSITION_MATRIX_FLAG);
            if (mCycle > 0) {
                mView.setRotation(360 * mDropDuration * factor / mCycle);
            }
            mView.setScaleX(1);
            mView.setScaleY(1);
            transformation.setAlpha(1);
        }
    }

}
