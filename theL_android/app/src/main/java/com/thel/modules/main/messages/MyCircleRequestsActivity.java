package com.thel.modules.main.messages;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.base.BaseDataBean;
import com.thel.bean.me.MyCircleRequestBean;
import com.thel.bean.me.MyCircleRequestListBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.modules.main.messages.adapter.MyCircleRequestListAdapter;
import com.thel.modules.main.messages.bean.MsgBean;
import com.thel.modules.main.messages.db.DataBaseAdapter;
import com.thel.modules.main.messages.db.MessageDataBaseAdapter;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.utils.DialogUtil;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SharedPrefUtils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * 我的密友请求页面
 */
public class MyCircleRequestsActivity extends BaseActivity implements View.OnClickListener {

    private LinearLayout lin_more;
    private ListView listView;
    private LinearLayout lin_back;
    private TextView txt_title;
    private ImageView bg;
    private MyCircleRequestListAdapter adapter;

    private SwipeRefreshLayout swipe_container;
    private ArrayList<MyCircleRequestBean> listPlus = new ArrayList<MyCircleRequestBean>();

    public static boolean needRefresh = false;

    private int curPage = 1;
    private int pageSize = 20;
    private boolean canLoadNext = false;
    private int requestCountForOnce = 0;
    private boolean isRefreshAll = true;

    private int lastIndex = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_my_circle_requests);

        findViewById();

        adapter = new MyCircleRequestListAdapter(this, listPlus);
        listView.setAdapter(adapter);

        processBusiness();
        setListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(this.getClass().getName());
        MobclickAgent.onResume(this);
        if (needRefresh) {
            processBusiness();
        }

        // 设置背景，系统消息用『system』存储
        String bgPath = SharedPrefUtils.getString(SharedPrefUtils.FILE_NAME_CHAT_BG, MsgBean.MSG_TYPE_REQUEST, "");
        if (!TextUtils.isEmpty(bgPath)) {
            Bitmap bitmap = BitmapFactory.decodeFile(bgPath);
            bg.setImageBitmap(bitmap);
        } else {
            String bgPathAll = SharedPrefUtils.getString(SharedPrefUtils.FILE_NAME_CHAT_BG, SharedPrefUtils.CHAT_BG_ALL, "");
            if (!TextUtils.isEmpty(bgPathAll)) {
                Bitmap bgBitmap = BitmapFactory.decodeFile(bgPathAll);
                bg.setImageBitmap(bgBitmap);
            }
        }
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(this.getClass().getName());
        MobclickAgent.onPause(this);
    }

    protected void processBusiness() {
        canLoadNext = false;
        isRefreshAll = true;
        swipe_container.post(new Runnable() {
            @Override
            public void run() {
                swipe_container.setRefreshing(true);
            }
        });
        getMyCircleRequestData();
    }

    protected void setListener() {
        lin_back.setOnClickListener(this);
        lin_more.setOnClickListener(this);

        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                canLoadNext = false;
                isRefreshAll = true;
                curPage = 1;
                getMyCircleRequestData();
            }
        });

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE && canLoadNext && requestCountForOnce > 0 && lastIndex == adapter.getCount()) {
                    showLoading();
                    // 翻到最上，加载下一页
                    canLoadNext = false;
                    isRefreshAll = false;
                    getMyCircleRequestData();
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                lastIndex = firstVisibleItem + visibleItemCount - listView.getHeaderViewsCount();
            }
        });
    }

    protected void findViewById() {
        lin_more = findViewById(R.id.lin_more);
        lin_more.setVisibility(View.VISIBLE);
        lin_back = findViewById(R.id.lin_back);
        txt_title = findViewById(R.id.txt_title);
        txt_title.setText(getString(R.string.my_circle_requests_act_title));
        swipe_container = findViewById(R.id.swipe_container);
        ViewUtils.initSwipeRefreshLayout(swipe_container);
        listView = findViewById(R.id.listView);
        bg = findViewById(R.id.bg);

    }

    private void requestFinished() {
        canLoadNext = true;
        if (swipe_container != null) {
            swipe_container.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (swipe_container.isRefreshing())
                        swipe_container.setRefreshing(false);
                }
            }, 1500);
        }
    }

    @Override
    public void onClick(View view) {
        ViewUtils.preventViewMultipleClick(view, 2000);
        int viewId = view.getId();
        switch (viewId) {
            case R.id.lin_more:
                ViewUtils.preventViewMultipleClick(view, 2000);
                Intent intent1 = new Intent(MyCircleRequestsActivity.this, ChatOperationsActivity.class);
                intent1.putExtra(TheLConstants.BUNDLE_KEY_WIDTH, bg.getWidth());
                intent1.putExtra(TheLConstants.BUNDLE_KEY_HEIGHT, bg.getHeight());
                intent1.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, MsgBean.MSG_TYPE_REQUEST);
                startActivityForResult(intent1, TheLConstants.BUNDLE_CODE_MY_CIRCLE_REQUEST);
                break;
            case R.id.lin_back:
                MyCircleRequestsActivity.this.finish();
                break;
            case R.id.lin_refuse:
                final int pos = (Integer) view.getTag();
                String str;
                if (listPlus.get(pos).requestType == 0) {
                    str = String.format(getString(R.string.my_circle_requests_act_partner_refuse_confirm), listPlus.get(pos).nickName);
                } else {
                    str = String.format(getString(R.string.my_circle_requests_act_bff_refuse_confirm), listPlus.get(pos).nickName);
                }
                DialogUtil.showConfirmDialog(this, "", str, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        showLoading();
                        RequestBusiness.getInstance()
                                .refuseRequest(listPlus.get(pos).requestId + "", pos + "")
                                .observeOn(AndroidSchedulers.mainThread())
                                .onBackpressureDrop().subscribeOn(Schedulers.io())
                                .subscribe(new InterceptorSubscribe<BaseDataBean>() {
                                    @Override
                                    public void onNext(BaseDataBean data) {
                                        super.onNext(data);

                                        closeLoading();
                                        if (swipe_container != null && swipe_container.isRefreshing()) {
                                            swipe_container.setRefreshing(false);
                                        }

                                        try {
                                            listPlus.get(pos).requestStatus = -1;
                                            adapter.updateSingleRow(listView, pos + listView.getHeaderViewsCount());
                                            //  SendMsgUtils.getInstance().sendMessage(createMsgBean(listPlus.get(pos)));
                                            DataBaseAdapter.getInstance(TheLApp.getContext()).updateCircleRequestStatus(-1, listPlus.get(pos).requestId);
                                        } catch (Exception e) {
                                        }
                                    }

                                    @Override
                                    public void onError(Throwable t) {
                                        super.onError(t);
                                        requestFinished();
                                    }
                                });

                    }
                });
                break;
            case R.id.lin_accept:
                final int pos1 = (Integer) view.getTag();
                String str1;
                if (listPlus.get(pos1).requestType == 0) {
                    str1 = String.format(getString(R.string.my_circle_requests_act_partner_accept_confirm), listPlus.get(pos1).nickName);
                } else {
                    str1 = String.format(getString(R.string.my_circle_requests_act_bff_accept_confirm), listPlus.get(pos1).nickName);
                }
                DialogUtil.showConfirmDialog(this, "", str1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        showLoading();

                        RequestBusiness.getInstance()
                                .acceptRequest(listPlus.get(pos1).requestId + "", pos1 + "")
                                .observeOn(AndroidSchedulers.mainThread())
                                .onBackpressureDrop().subscribeOn(Schedulers.io())
                                .subscribe(new InterceptorSubscribe<BaseDataBean>() {
                                    @Override
                                    public void onNext(BaseDataBean data) {
                                        super.onNext(data);

                                        closeLoading();
                                        if (swipe_container != null && swipe_container.isRefreshing()) {
                                            swipe_container.setRefreshing(false);
                                        }

                                        try {
                                            listPlus.get(pos1).requestStatus = 1;
                                            adapter.updateSingleRow(listView, pos1 + listView.getHeaderViewsCount());
                                            //   SendMsgUtils.getInstance().sendMessage(createMsgBean(listPlus.get(pos)));
                                            DataBaseAdapter.getInstance(TheLApp.getContext()).updateCircleRequestStatus(1, listPlus.get(pos1).requestId);
                                        } catch (Exception e) {
                                        }
                                    }

                                    @Override
                                    public void onError(Throwable t) {
                                        super.onError(t);
                                        requestFinished();
                                    }
                                });
                    }
                });
                break;
            case R.id.img_thumb_left:
                String userId = (String) view.getTag();
//                Intent intent = new Intent();
//                intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, userId);
//                intent.setClass(MyCircleRequestsActivity.this, UserInfoActivity.class);
//                startActivity(intent);
                FlutterRouterConfig.Companion.gotoUserInfo(userId);
                break;
        }
    }

    private void getMyCircleRequestData() {
        RequestBusiness.getInstance()
                .getMyCircleRequestData(curPage, pageSize)
                .onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new InterceptorSubscribe<MyCircleRequestListBean>() {
                    @Override
                    public void onNext(final MyCircleRequestListBean myCircleRequestListBean) {
                        super.onNext(myCircleRequestListBean);

                        closeLoading();
                        if (swipe_container != null && swipe_container.isRefreshing()) {
                            swipe_container.setRefreshing(false);
                        }

                        if (isRefreshAll) {
                            // 刷新请求数量和最新请求
                            MessageDataBaseAdapter.getInstance(TheLApp.getContext(), ShareFileUtils.getString(ShareFileUtils.ID, "")).updateChatUnreadCount(MessageDataBaseAdapter.CIRCLE_REQUEST_CHAT_USER_ID, 0);
                            listPlus.clear();
                            curPage = 1;
                            // 存储第一页请求列表信息到数据库
                            new Thread(new Runnable() {

                                @Override
                                public void run() {
                                    DataBaseAdapter dbAdatper = DataBaseAdapter.getInstance(TheLApp.getContext());
                                    dbAdatper.saveCircleRequests(myCircleRequestListBean.data.requests);
                                }
                            }).start();
                        }
                        listPlus.addAll(myCircleRequestListBean.data.requests);
                        requestCountForOnce = myCircleRequestListBean.data.requests.size();
                        if (adapter == null) {
                            adapter = new MyCircleRequestListAdapter(MyCircleRequestsActivity.this, listPlus);
                            listView.setAdapter(adapter);
                        } else {
                            adapter.refreshAdapter(listPlus);
                            adapter.notifyDataSetChanged();
                        }
                        curPage++;
                        canLoadNext = true;
                    }

                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                        requestFinished();
                    }
                });
    }

}
