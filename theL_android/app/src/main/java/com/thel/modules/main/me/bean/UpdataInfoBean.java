package com.thel.modules.main.me.bean;

import com.thel.base.BaseDataBean;

/**
 * 更新资料
 * Created by lingwei on 2017/12/27.
 */

public class UpdataInfoBean extends BaseDataBean {

    /**
     * data : {"ratio":20}
     */

    public UpdataInfoDataBean data;

    public static class UpdataInfoDataBean {
        /**
         * ratio : 20
         */

        public int ratio = 0;
    }

    @Override
    public String toString() {
        return "UpdataInfoBean{" +
                "data=" + data +
                '}';
    }
}
