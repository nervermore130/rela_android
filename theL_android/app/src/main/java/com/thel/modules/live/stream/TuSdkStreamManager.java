package com.thel.modules.live.stream;

import android.hardware.Camera;
import android.opengl.GLSurfaceView;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.liulishuo.filedownloader.BaseDownloadTask;
import com.liulishuo.filedownloader.FileDownloadSampleListener;
import com.liulishuo.filedownloader.FileDownloader;
import com.qiniu.pili.droid.streaming.StreamStatusCallback;
import com.qiniu.pili.droid.streaming.StreamingProfile;
import com.qiniu.pili.droid.streaming.StreamingSessionListener;
import com.qiniu.pili.droid.streaming.StreamingState;
import com.thel.bean.LiveInfoLogBean;
import com.thel.constants.TheLConstants;
import com.thel.manager.CDNBalanceManager;
import com.thel.modules.live.bean.SoftGiftBean;
import com.thel.modules.live.utils.LiveUtils;
import com.thel.tusdk.plain.CameraConfig;
import com.thel.utils.L;
import com.thel.utils.LiveLagUtility;
import com.thel.utils.Utils;

import org.lasque.tusdk.modules.view.widget.sticker.StickerGroup;
import org.lasque.tusdk.modules.view.widget.sticker.StickerLocalPackage;
import org.lasque.tusdk.video.editor.TuSdkMediaEffectData;
import org.lasque.tusdk.video.editor.TuSdkMediaStickerEffectData;

import java.io.File;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import static org.lasque.tusdk.video.editor.TuSdkMediaEffectData.TuSdkMediaEffectDataType.TuSdkMediaEffectDataTypeSticker;

/**
 * Created by chad
 * Time 19/1/15
 * Email: wuxianchuang@foxmail.com
 * Description: TODO 涂图视频推流
 */
public class TuSdkStreamManager extends TuSdkPreviewManager implements StreamingSessionListener, StreamStatusCallback {

    public static final String TAG = "TuSdkStreamManager";

    private String oldRtmpUrl;

    private String inUrl;

    /**
     * AR播放列表
     */
    private List<SoftGiftBean> arGiftList = new ArrayList<>();

    private boolean mStreaming = false;

    private int oldVideoFps = 0;

    private long time = 0;

    private String pubUrl;

    private LiveLagUtility liveLagUtility = new LiveLagUtility();

    private long spaceTime = 0;

    private OnStrickerListener listener;

    public TuSdkStreamManager(CameraConfig mCameraConfig, GLSurfaceView glSurfaceView, String pubUrl, String oldRtmpUrl) {
        super(mCameraConfig, glSurfaceView);
        this.pubUrl = pubUrl;
        this.oldRtmpUrl = oldRtmpUrl;
    }

    @Override
    public void initStreamingManager() {
        super.initStreamingManager();

        mMediaStreamingManager.setNativeLoggingEnabled(false);
        mMediaStreamingManager.setStreamingSessionListener(this);
        mMediaStreamingManager.setStreamStatusCallback(this);
    }

    public void setOnStrickerListener(OnStrickerListener listener) {
        this.listener = listener;
    }

    public void startStreaming() {

        L.d(TAG, " ------startStreaming------" + mStreaming);

        if (!mStreaming) {
            mMediaStreamingManager.startStreaming();
            mStreaming = !mStreaming;
        }
    }

    public void stopStreaming() {
        if (mStreaming) {
            mMediaStreamingManager.stopStreaming();
            mStreaming = !mStreaming;
        }
    }

    private void setLiveInfoLog(String rtmpUrl) {

        LiveInfoLogBean.getInstance().getLivePlayAnalytics().time = LiveUtils.getLiveTime();
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().userId = Utils.getMyUserId();
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().logType = TheLConstants.LiveInfoLogConstants.TYPE_LIVE_CHANGE_LONG;
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().outUrl = inUrl;
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().inUrl = rtmpUrl;
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().host = rtmpUrl;

        LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getLivePlayAnalytics());

        inUrl = rtmpUrl;

    }

    /**
     * 贴纸轮询
     */
    private static final int MSG_INTERVAL = 0x00;
    /**
     * 贴纸下载完
     */
    private static final int MSG_DOWNLOAD = 0x01;
    /**
     * 贴纸播放完
     */
    private static final int MSG_PLAY_OVER = 0x02;

    private Handler mStrickerHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {

            if (mFilterEngine != null) {
                switch (msg.what) {
                    case MSG_PLAY_OVER:
                        //移除播放完的贴纸
                        List<TuSdkMediaEffectData> filterEffects = mFilterEngine.mediaEffectsWithType(TuSdkMediaEffectDataTypeSticker);


                        if (filterEffects != null && filterEffects.size() > 0) {
                            // 取消贴纸
                            mFilterEngine.removeMediaEffectsWithType(TuSdkMediaEffectDataTypeSticker);
                            mFilterEngine.removeAllLiveSticker();
                            arGiftList.remove(0);
                        }
                        mStrickerHandler.sendEmptyMessageDelayed(MSG_INTERVAL, 1000);
                        if (mOnTuSdkListener != null) {
                            mOnTuSdkListener.onFaceDetection(true);
                        }
                        break;
                    case MSG_INTERVAL:

                        if (arGiftList.size() > 0) {
                            SoftGiftBean stickBean = arGiftList.get(0);

                            String url = stickBean.arResource;

                            String fileName = url.substring(url.lastIndexOf("/") + 1);

                            String path = TheLConstants.F_AR_GIFT_ROOTPATH + fileName;

                            File file = new File(path);

                            if (!StickerLocalPackage.shared().containsGroupId(stickBean.arGiftId)) {
                                StickerGroup stickerGroup = new StickerGroup();
                                stickerGroup.groupId = stickBean.arGiftId;

                                if (file.exists()) {
                                    boolean result = RelaStickDownload.prepareSingleLocalSticker(fileName);
                                    if (result) {
                                        mStrickerHandler.sendEmptyMessage(MSG_DOWNLOAD);
                                    } else {
                                        arGiftList.remove(0);
                                        mStrickerHandler.sendEmptyMessageDelayed(MSG_INTERVAL, 1000);
                                    }
                                } else {
                                    FileDownloader.getImpl().create(url)
                                            .setPath(path)
                                            .setWifiRequired(false)
                                            .setForceReDownload(true)
                                            .setCallbackProgressTimes(0)
                                            .setListener(mQueueTarget)
                                            .start();
                                }
                            } else {
                                if (!file.exists()) {
                                    StickerLocalPackage.shared().removeDownloadWithIdt(stickBean.arGiftId);
                                    FileDownloader.getImpl().create(url)
                                            .setPath(path)
                                            .setWifiRequired(false)
                                            .setForceReDownload(true)
                                            .setCallbackProgressTimes(0)
                                            .setListener(mQueueTarget)
                                            .start();
                                } else {
                                    TuSdkMediaStickerEffectData stickerEffectData = new TuSdkMediaStickerEffectData(StickerLocalPackage.shared().getStickerGroup(stickBean.arGiftId));
                                    mFilterEngine.addMediaEffectData(stickerEffectData);
                                    mStrickerHandler.sendEmptyMessageDelayed(MSG_PLAY_OVER, stickBean.playTime);
                                    if (mOnTuSdkListener != null && mNoFace) {
                                        mOnTuSdkListener.onFaceDetection(false);
                                    }
                                }
                            }
                        } else {
                            mStrickerHandler.sendEmptyMessageDelayed(MSG_INTERVAL, 1000);
                        }
                        break;
                    case MSG_DOWNLOAD:
                        TuSdkMediaStickerEffectData stickerEffectData = new TuSdkMediaStickerEffectData(StickerLocalPackage.shared().getStickerGroup(arGiftList.get(0).arGiftId));
                        mFilterEngine.addMediaEffectData(stickerEffectData);
                        mStrickerHandler.sendEmptyMessageDelayed(MSG_PLAY_OVER, arGiftList.get(0).playTime);
                        if (mOnTuSdkListener != null && mNoFace) {
                            mOnTuSdkListener.onFaceDetection(false);
                        }
                        break;
                    default:
                        break;
                }
            } else {
                mStrickerHandler.sendEmptyMessageDelayed(MSG_INTERVAL, 1000);
            }
            return false;
        }
    });

    /**
     * 播放贴纸
     *
     * @param giftBean
     */
    public void addStricker(SoftGiftBean giftBean) {

        arGiftList.add(giftBean);

    }

    @Override
    public void onSurfaceDestroyed() {
        super.onSurfaceDestroyed();
        stopStreaming();
    }

    @Override
    public boolean onRecordAudioFailedHandled(int i) {
        Log.i(TAG, "onRecordAudioFailedHandled");
        return false;
    }

    @Override
    public boolean onRestartStreamingHandled(int i) {
        /**
         * When the network-connection is broken, StreamingState#DISCONNECTED will notified first,
         * and then invoked this method if the environment of restart streaming is ready.
         *
         * @return true means you handled the event; otherwise, given up and then StreamingState#SHUTDOWN
         * will be notified.
         */
        Log.i(TAG, "onRestartStreamingHandled");
        startStreaming();
        return true;
    }

    @Override
    public Camera.Size onPreviewSizeSelected(List<Camera.Size> list) {
        /**
         * You should choose a suitable size to avoid image scale
         * eg: If streaming size is 1280 x 720, you should choose a camera preview size >= 1280 x 720
         */
        Camera.Size size = null;
        if (list != null) {
            StreamingProfile.VideoEncodingSize encodingSize = mProfile.getVideoEncodingSize(mCameraConfig.mSizeRatio);
            for (Camera.Size s : list) {
                if (s.width >= encodingSize.width && s.height >= encodingSize.height) {
                    if (mEncodingConfig.mIsVideoSizePreset) {
                        size = s;
                        Log.d(TAG, "selected size :" + size.width + "x" + size.height);
                    }
                    break;
                }
            }
        }
        return size;
    }

    @Override
    public int onPreviewFpsSelected(List<int[]> list) {
        return -1;
    }

    @Override
    public void notifyStreamStatusChanged(StreamingProfile.StreamStatus streamStatus) {

        L.d(TAG, " ------notifyStreamStatusChanged------" + streamStatus);

        Log.d(TAG, "bitrate:" + streamStatus.totalAVBitrate / 1024 + " kbps"
                + "\naudio:" + streamStatus.audioFps + " fps"
                + "\nvideo:" + streamStatus.videoFps + " fps");

//        long currentTime = System.currentTimeMillis();

//        if (oldVideoFps != 0 && streamStatus.videoFps != 0 && oldVideoFps < 12 && streamStatus.videoFps < 12 && time != 0 && currentTime - time < 5000) {
//
//            String rtmpUrl = CDNBalanceManager.getInstance().getFastRtmpUrl();
//
//            L.d(TAG, " notifyStreamStatusChanged rtmpUrl : " + rtmpUrl);
//
//            setLiveInfoLog(rtmpUrl);
//
//            stopStreaming();
//            try {
//                if (rtmpUrl == null || rtmpUrl.length() <= 0) {
//                    rtmpUrl = oldRtmpUrl;
//                }
//                mProfile.setPublishUrl(rtmpUrl);
//            } catch (URISyntaxException e) {
//                e.printStackTrace();
//            }
//            startStreaming();
//
//        }
//
//        time = currentTime;
//
//        oldVideoFps = streamStatus.videoFps;

        long currentTime = System.currentTimeMillis();

        if (currentTime - spaceTime >= 3000) {

            L.d(TAG, "记录一次FPS");

            spaceTime = currentTime;

            liveLagUtility.addSample(streamStatus.videoFps);

            if (liveLagUtility.isLag()) {

                L.d(TAG, " 开始切换推送流地址");

                String rtmpUrl = CDNBalanceManager.getInstance().getFastRtmpUrl();

                stopStreaming();

                try {
                    if (rtmpUrl == null || rtmpUrl.length() <= 0) {
                        rtmpUrl = oldRtmpUrl;
                    }

                    L.d(TAG, " rtmpUrl : " + rtmpUrl);

                    mProfile.setPublishUrl(rtmpUrl);
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }

                startStreaming();

                liveLagUtility.reset();
            }
        }


    }

    @Override
    public void onStateChanged(StreamingState streamingState, Object extra) {
        super.onStateChanged(streamingState, extra);
        Log.i(TAG, "StreamingState streamingState:" + streamingState + ",extra:" + extra);
        switch (streamingState) {
            case READY:
                /**
                 * Start streaming when `READY`
                 */
                startStreaming();

                mStrickerHandler.sendEmptyMessage(MSG_INTERVAL);
                break;
            case IOERROR:
                /**
                 * Network-connection is unavailable when `startStreaming`.
                 * You can `startStreaming` later or just finish the streaming
                 */
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startStreaming();
                    }
                }, 2000);
                break;
            case INVALID_STREAMING_URL:
                L.d(TAG, " Invalid streaming url : " + extra);

                String rtmpUrl = CDNBalanceManager.getInstance().changeRtmpUrl();

                L.d(TAG, " INVALID_STREAMING_URL rtmpUrl : " + rtmpUrl);

                if (rtmpUrl == null) {

                    LiveInfoLogBean.getInstance().getLivePlayAnalytics().reason = LiveInfoLogBean.CHANGE_CDN_USE_HOST;

                    LiveInfoLogBean.getInstance().getLivePlayAnalytics().outUrl = LiveInfoLogBean.getInstance().getLivePlayAnalytics().inUrl;

                    LiveInfoLogBean.getInstance().getLivePlayAnalytics().inUrl = oldRtmpUrl;

                    pubUrl = oldRtmpUrl;


                } else {

                    LiveInfoLogBean.getInstance().getLivePlayAnalytics().reason = LiveInfoLogBean.CHANGE_CDN_USE_IP;

                    LiveInfoLogBean.getInstance().getLivePlayAnalytics().outUrl = LiveInfoLogBean.getInstance().getLivePlayAnalytics().inUrl;

                    LiveInfoLogBean.getInstance().getLivePlayAnalytics().inUrl = rtmpUrl;

                    pubUrl = rtmpUrl;

                }

                setLiveInfoLog(pubUrl);

                stopStreaming();

                try {
                    if (pubUrl == null) {
                        pubUrl = oldRtmpUrl;
                    }
                    mProfile.setPublishUrl(pubUrl);
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }

                startStreaming();

                break;
            case OPEN_CAMERA_FAIL:
                Log.e(TAG, "Open Camera Fail. id:" + extra);
                break;
            case CAMERA_SWITCHED:
                if (extra != null) {
                    Log.i(TAG, "current camera id:" + extra);
                }
                Log.i(TAG, "camera switched");
                final int currentCamId = (Integer) extra;
                break;
        }
    }

    @Override
    public String setPubUrl() {
        return pubUrl;
    }

    private final FileDownloadSampleListener mQueueTarget = new FileDownloadSampleListener() {

        @Override
        protected void completed(BaseDownloadTask task) {
            String fileName = task.getFilename();
            boolean result = RelaStickDownload.prepareSingleLocalSticker(fileName);
            L.d(TAG, "completed: " + "fileName: " + fileName + "---result: " + result);
            if (result) {
                for (SoftGiftBean bean : arGiftList) {
                    int groupId = Integer.parseInt(fileName.substring(fileName.lastIndexOf("_") + 1, fileName.lastIndexOf(".")));
                    if (bean.arGiftId == groupId) {
                        mStrickerHandler.sendEmptyMessage(MSG_DOWNLOAD);
                        break;
                    }
                }
            } else {
                arGiftList.remove(0);
                mStrickerHandler.sendEmptyMessageDelayed(MSG_INTERVAL, 1000);
            }
        }
    };

    public interface OnStrickerListener {
        void onStrickerDownloadFailed(SoftGiftBean stickBean);
    }
}
