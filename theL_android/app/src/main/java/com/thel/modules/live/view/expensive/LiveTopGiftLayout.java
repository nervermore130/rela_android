package com.thel.modules.live.view.expensive;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.Handler;
import androidx.annotation.NonNull;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import com.thel.utils.Utils;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by waiarl on 2018/3/2.
 */

public class LiveTopGiftLayout extends ViewGroup {
    private final Context mContext;
    public List<TopGiftBean> giftBeanList = new ArrayList<>();
    private boolean canShow = true;
    private int mWidth;
    private int mTopPadding;
    private int mStopX;
    private int mAnimInDuration;
    private Handler mHandler;
    private int mStayDuration;
    private TopGiftItemImp.TopGiftClickListener mTopGiftClickListener;
    private int mAnimOutDuration;

    public LiveTopGiftLayout(Context context) {
        this(context, null);
    }

    public LiveTopGiftLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LiveTopGiftLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final int size = getChildCount();
        for (int i = 0; i < size; i++) {
            final View view = getChildAt(i);
            measureChild(view, MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    private void init() {
        mTopPadding = Utils.dip2px(mContext, 5);
        mStopX = Utils.dip2px(mContext, 20);
        mAnimInDuration = 1000;
        mAnimOutDuration = 1000;
        mStayDuration = 10 * 1000;
        mHandler = new Handler();
    }

    public LiveTopGiftLayout receiveGift(@NonNull TopGiftBean topGiftBean) {
        giftBeanList.add(topGiftBean);
        callShow();
        return this;
    }

    private synchronized void callShow() {
        final int size = giftBeanList.size();
        if (size <= 0) {
            return;
        }
        if (canShow) {
            canShow = false;
            final TopGiftBean giftBean = giftBeanList.get(0);
            giftBeanList.remove(giftBean);
            showItemView(giftBean);
        }
    }

    private void showItemView(TopGiftBean topGiftBean) {
        final TopGiftItemImp itemView = new LiveTopGiftItemView(mContext).initView(topGiftBean);
        itemView.setTopGiftClickListener(mTopGiftClickListener);
        addView(itemView.getView());
    }


    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        mWidth = r - l;
        final int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            final TopGiftItemImp view = (TopGiftItemImp) getChildAt(i);
            if (!view.isAdded()) {
                view.setIsAdded(true);
                layout(view.getView(), -view.getView().getMeasuredWidth(), mTopPadding);
                animInView(view);
            }
        }
    }

    private void layout(View view, int left, int top) {
        view.layout(left, top, left + view.getMeasuredWidth(), top + view.getMeasuredHeight());
    }

    private void animInView(final TopGiftItemImp itemView) {
        final View view = itemView.getView();
        final int transX = mStopX + view.getMeasuredWidth();
        final int duration = mAnimInDuration;
        final ObjectAnimator oa = ObjectAnimator.ofFloat(view, View.TRANSLATION_X, transX);
        oa.setDuration(duration);
        oa.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationCancel(Animator animation) {
                super.onAnimationCancel(animation);
                if (view != null && view.getParent() == LiveTopGiftLayout.this) {
                    removeView(view);
                }
                canShow = true;
                callShow();
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                stayView(itemView);
            }
        });
        oa.start();
    }

    private void stayView(final TopGiftItemImp itemView) {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (itemView != null) {
                    AnimOutView(itemView);
                }
            }
        }, mStayDuration);
    }


    private void AnimOutView(final TopGiftItemImp itemView) {
        final View view = itemView.getView();
        final int transX = -view.getMeasuredWidth();
        final int duration = mAnimOutDuration;
        final ObjectAnimator oa = ObjectAnimator.ofFloat(view, View.TRANSLATION_X, transX);
        oa.setDuration(duration);
        oa.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationCancel(Animator animation) {
                super.onAnimationCancel(animation);
                if (view != null && view.getParent() == LiveTopGiftLayout.this) {
                    removeView(view);
                }
                canShow = true;
                callShow();
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                removeView(itemView);
            }
        });
        oa.start();
    }

    private void removeView(TopGiftItemImp itemView) {
        final View view = itemView != null ? itemView.getView() : null;
        if (view != null && view.getParent() == this) {
            removeView(view);
        }
        canShow = true;
        callShow();
    }

    public LiveTopGiftLayout setTopGiftClickListener(TopGiftItemImp.TopGiftClickListener listener) {
        this.mTopGiftClickListener = listener;
        return this;
    }
}
