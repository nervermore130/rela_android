package com.thel.modules.live.view;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.constants.TheLConstants;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.live.bean.GiftSendMsgBean;
import com.thel.ui.widget.UserLevelImageView;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;


/**
 * Created by waiarl on 2017/4/27.
 */

public class LiveBigGiftMsgView extends LinearLayout {
    private final Context mContext;
    private ImageView img_avatar;
    private TextView txt_gift;
    private ImageView icn_light;
    private RelativeLayout rl_alpha;
    private TextView txt_big_gift_nickname;
    private UserLevelImageView img_level;
    private ObjectAnimator animator;

    public LiveBigGiftMsgView(Context context) {
        this(context, null);
    }

    public LiveBigGiftMsgView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LiveBigGiftMsgView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        for (int i = 0; i < getChildCount(); i++) {
            View v = getChildAt(i);
            v.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    private void init() {
        inflate(mContext, R.layout.live_big_gift_msg_view, this);
        img_avatar = findViewById(R.id.big_gift_avatar);
        rl_alpha = findViewById(R.id.rl_alpha);
        icn_light = findViewById(R.id.icn_light);
        txt_gift = findViewById(R.id.big_gift_text);
        txt_big_gift_nickname = findViewById(R.id.txt_big_gift_nickname);
        img_level = findViewById(R.id.img_level);
    }

    public LiveBigGiftMsgView initView(GiftSendMsgBean giftSendMsgBean, OnClickListener listener) {
        // img_avatar.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(giftSendMsgBean.avatar, TheLConstants.AVATAR_SMALL_SIZE, TheLConstants.AVATAR_SMALL_SIZE)));
        ImageLoaderManager.imageLoaderDefaultCircle(img_avatar, R.mipmap.icon_user, giftSendMsgBean.avatar, TheLConstants.AVATAR_SMALL_SIZE, TheLConstants.AVATAR_SMALL_SIZE);
        txt_big_gift_nickname.setText(giftSendMsgBean.nickName);
        txt_gift.setText(giftSendMsgBean.action);
        img_avatar.setOnClickListener(listener);
        txt_gift.setOnClickListener(listener);
        txt_big_gift_nickname.setOnClickListener(listener);
        txt_gift.setAlpha(0.8f);
        icn_light.setAlpha(1f);
        if (giftSendMsgBean.iconSwitch == 1) {
            img_level.setVisibility(VISIBLE);
            setLevel(giftSendMsgBean.userLevel);
        } else {
            img_level.setVisibility(GONE);
        }
        measureChild(rl_alpha, MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
        int width = rl_alpha.getMeasuredWidth();
        measureChild(icn_light, MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
        int width1 = icn_light.getMeasuredWidth();
        float curTranslationX = width - (width1 / 1.5f) - Utils.dip2px(mContext, 12);
        animator = ObjectAnimator.ofFloat(icn_light, "translationX", 0, curTranslationX);
        animator.setDuration(1000);
        animator.setInterpolator(new LinearInterpolator());
        animator.setRepeatCount(ValueAnimator.INFINITE);
        animator.setRepeatMode(ValueAnimator.RESTART);
        animator.start();
        return this;
    }

    @Override protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (animator != null) {
            animator.cancel();
            animator = null;
        }
    }

    private void setLevel(int userLevel) {
        img_level.initView(userLevel);
    }


    public void destoryView() {
        img_avatar.setOnClickListener(null);
        txt_gift.setOnClickListener(null);
        txt_big_gift_nickname.setOnClickListener(null);
    }
}
