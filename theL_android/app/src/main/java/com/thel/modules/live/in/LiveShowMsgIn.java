package com.thel.modules.live.in;

import com.thel.modules.live.bean.SoftGiftBean;
import com.thel.modules.live.surface.watch.LiveWatchObserver;

/**
 * Created by waiarl on 2017/11/5.
 */

public interface LiveShowMsgIn extends LiveShowMsgPkIn, LiveShowMsgMultiIn {
    void bindObserver(LiveWatchObserver observer);


    /**
     * 试着创建连接
     */
    void tryCreateClient();

    /**
     * ping
     */
    void autoPingServer();


    void onDestroy();


    void reset();

    void init();

    /**
     * 发送礼物
     *
     * @param softGiftBean
     * @param id
     * @param type
     * @param comb
     */
    void sendSoftGift(SoftGiftBean softGiftBean, int id, String type, int comb, String hostUserId);

    /****
     * 上报礼物播放
     * */
    void postGiftplayStatus(String userId, String toUserId, int id);

    /**
     * 发送消息
     *
     * @param msg
     */
    void sendMsg(String msg);

    /**
     * 主播播放ar礼物失败
     *
     */
    void sendPlayStickFailMsg();
    /**
     * 弹幕
     *
     * @param userLevel 用户等级
     * @param content   弹幕内容
     * @param barrageId 弹幕id
     */
    void sendDanmu(String userLevel, String content, String barrageId);

    /**
     * 关注主播后，新增主播粉丝
     */
    void addFollowFans();

    /**
     * 分享主播后 向服务端发消息
     */
    void shareToLive();

    /**
     * 推荐主播后 向服务端发消息
     */
    void recommendLive();


    /**
     * 接受免费热拉豆
     */
    void acceptFreeGold();

    /**
     * 不接受热拉豆
     */
    void rejectFreeGold();


    /**
     * 获取当前正在连接聊天的token
     *
     * @return
     */
    String getToken();

    void showCloseDialog();
}
