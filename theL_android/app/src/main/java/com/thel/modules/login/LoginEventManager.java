package com.thel.modules.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.thel.R;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.RfSBridgeHandlerFactory;
import com.thel.modules.login.init_info.InitUserInfoActivity;
import com.thel.modules.login.login_register.LoginRegisterActivity;
import com.thel.modules.main.MainActivity;
import com.thel.network.api.loginapi.bean.SignInBean;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SharedPrefUtils;
import com.thel.utils.UserUtils;
import com.umeng.analytics.MobclickAgent;

import me.rela.rf_s_bridge.RfSBridgePlugin;

/**
 * Created by chad
 * Time 18/3/8
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */

public class LoginEventManager {
    /**
     * 邮箱登陆事件
     *
     * @param context
     * @param signInBean
     */
    public static void emailLoginCallback(Context context, SignInBean signInBean) {
        if (signInBean == null)
            return;
        if (signInBean.data == null) {
            Toast.makeText(context, signInBean.errdesc, Toast.LENGTH_SHORT).show();
            return;
        }

        Log.d("wxlogin", signInBean.data.user.toString());
        // 手机注册转化率统计：注册或登录成功
        MobclickAgent.onEvent(context, "register_or_login_succeed");
        ShareFileUtils.setBoolean(ShareFileUtils.HAS_LOGGED, true);
        ShareFileUtils.saveUserData(signInBean);

        Toast.makeText(context, context.getString(R.string.login_activity_success), Toast.LENGTH_SHORT).show();

        if (noBindPhone()) {
            LoginRegisterActivity.startActivity(context, LoginRegisterActivity.PHONE_VERIFY,false);
        } else {
            Intent intent = new Intent(context, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(TheLConstants.BUNDLE_KEY_WHERE_TO_GO, "ONE");
            context.startActivity(intent);
        }
    }

    /**
     * 手机登录,微信登录，脸书登录事件
     *
     * @param context
     * @param signInBean
     */
    public static void loginCallback(Context context, SignInBean signInBean) {
        if (signInBean == null || context == null)
            return;
        if (signInBean.data == null) {
            Toast.makeText(context, signInBean.errdesc, Toast.LENGTH_SHORT).show();
            return;
        }

        if (signInBean.data.user.isNewUser()) {// 新注册未初始化
            ShareFileUtils.saveUserData(signInBean);

            MobclickAgent.onEvent(context, "register_succeed");// 手机注册转化率统计：注册成功
            SharedPrefUtils.setInt(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.INIT_CIRCLE_PAGE + signInBean.data.user.id, 0);// 将新用户关注人数置为0

            Intent intent1 = new Intent(context, InitUserInfoActivity.class);
            Bundle bundle = new Bundle();
            // 把从社交APP获取的信息放进去
            bundle.putSerializable(TheLConstants.BUNDLE_KEY_USER_BEAN, signInBean);
            intent1.putExtras(bundle);
            context.startActivity(intent1);
        } else {

            MobclickAgent.onEvent(context, "register_or_login_succeed");// 手机注册转化率统计：注册或登录成功
            ShareFileUtils.saveUserData(signInBean);
            ShareFileUtils.setBoolean(ShareFileUtils.HAS_LOGGED, true);

            Toast.makeText(context, context.getString(R.string.login_activity_success), Toast.LENGTH_SHORT).show();

            //登录成功
            if (noBindPhone()) {
                LoginRegisterActivity.startActivity(context, LoginRegisterActivity.PHONE_VERIFY,false);
            } else {
                Intent intent = new Intent(context, MainActivity.class);
                intent.putExtra("isNew", false);
                intent.putExtra(TheLConstants.BUNDLE_KEY_WHERE_TO_GO, "ONE");
                context.startActivity(intent);
            }

            RfSBridgeHandlerFactory.getInstance().getFlutterPushImpl().pushUserInfo();

            RfSBridgeHandlerFactory.getInstance().getFlutterPushImpl().pushMyUserId();

            RfSBridgePlugin.getInstance().activeRpc("EventLogin", UserUtils.getMyUserId(), null);

        }
    }

    public static boolean noBindPhone() {
        String cell = ShareFileUtils.getString(ShareFileUtils.BIND_CELL, "");
        return TextUtils.isEmpty(cell) || "null".equals(cell);
    }
}
