package com.thel.modules.main.home.moments.winkcomment;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.CommentWinkUserBean;
import com.thel.bean.gift.WinkCommentListBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.imp.BaseFunctionFragment;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.service.DefaultRequestService;
import com.thel.ui.adapter.WinkCommentsAdapter;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.decoration.DefaultItemDivider;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by liuyun on 2017/10/24.
 */

public class WinkCommentsFragment extends BaseFunctionFragment implements BaseRecyclerViewAdapter.RequestLoadMoreListener, BaseRecyclerViewAdapter.ReloadMoreListener {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.lin_back)
    LinearLayout lin_back;

    @BindView(R.id.swipe_container)
    SwipeRefreshLayout swipe_container;

    @BindView(R.id.txt_title)
    TextView txt_title;

    @BindView(R.id.lin_more)
    LinearLayout lin_morelin_more;

    private int limit = 20;

    private int cursor = 0;

    private long lastClickTime = 0;

    private String momentId;

    private boolean haveNextPage = false;

    private WinkCommentsAdapter mAdapter;

    private ArrayList<CommentWinkUserBean> listPlus = new ArrayList<>();
    private CommentWinkUserBean winkUserBean;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_normal_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        recyclerView.addItemDecoration(new DefaultItemDivider(TheLApp.getContext(), LinearLayoutManager.VERTICAL, R.color.gray, 1, true, false));

        recyclerView.setLayoutManager(new LinearLayoutManager(TheLApp.getContext()));

        recyclerView.setHasFixedSize(true);

        ViewUtils.initSwipeRefreshLayout(swipe_container);

        initAdapter();
        lin_morelin_more.setVisibility(View.GONE);
        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                cursor = 0;
                loadData();
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        momentId = getArguments().getString(TheLConstants.BUNDLE_KEY_MOMENT_ID);

        loadData();
    }

    private void loadData() {

        swipe_container.post(new Runnable() {
            @Override
            public void run() {
                swipe_container.setRefreshing(true);
            }
        });

        Flowable<WinkCommentListBean> flowable = DefaultRequestService.createAllRequestService().getWinkComments(momentId, String.valueOf(limit), String.valueOf(cursor));
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<WinkCommentListBean>() {

            @Override
            public void onNext(WinkCommentListBean data) {
                super.onNext(data);

                if (cursor == 0) {
                    listPlus.clear();
                }
                haveNextPage = data.data.haveNextPage;
                listPlus.addAll(data.data.list);
                if (cursor == 0) {
                    mAdapter.removeAllFooterView();
                    mAdapter.openLoadMore(listPlus.size(), true);
                    mAdapter.setNewData(listPlus);
                } else {
                    mAdapter.notifyDataChangedAfterLoadMore(true, listPlus.size());
                }
                cursor = data.data.cursor;
                txt_title.setText(TheLApp.getContext().getString(data.data.num <= 1 ? R.string.like_count_odd : R.string.like_count_even, data.data.num));

                requestFinished();
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                recyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.loadMoreFailed((ViewGroup) recyclerView.getParent());
                    }
                });

                requestFinished();
            }
        });
    }

    @OnClick(R.id.lin_back)
    void back() {
        getActivity().finish();
    }

    @OnTouch(R.id.txt_title)
    boolean onTouch(MotionEvent event) {
        if (MotionEvent.ACTION_DOWN == event.getAction()) {
            if (System.currentTimeMillis() - lastClickTime < 300) {
                if (recyclerView != null) {
                    recyclerView.scrollToPosition(0);
                }
            }
            lastClickTime = System.currentTimeMillis();
        }
        return false;
    }

    protected void setListener() {


        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                cursor = 0;
                loadData();
            }
        });
    }

    private void initAdapter() {
        mAdapter = new WinkCommentsAdapter(listPlus);
       /* mAdapter.setOnRecyclerViewItemChildClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseRecyclerViewAdapter adapter, View view, int position) {
                switch (view.getId()) {
                    case R.id.txt_follow:
                        final int pos = (int) view.getTag() - adapter.getHeaderLayoutCount();
                        winkUserBean = mAdapter.getItem(pos);
                        showLoading();
                        FollowStatusChangedImpl.followUser(winkUserBean.userId + "", ACTION_TYPE_FOLLOW, winkUserBean.userName, winkUserBean.avatar);

                        break;
                }

            }
        });*/
        mAdapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {

            @Override
            public void onItemClick(View view, int position) {
                final CommentWinkUserBean bean = mAdapter.getItem(position - mAdapter.getHeaderLayoutCount());
//                Intent intent = new Intent(getContext(), UserInfoActivity.class);
//                intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, bean.userId + "");
//                startActivity(intent);
                FlutterRouterConfig.Companion.gotoUserInfo(bean.userId+"");
            }
        });
        mAdapter.setOnLoadMoreListener(this);
        mAdapter.setReloadMoreListener(this);
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onLoadMoreRequested() {
        recyclerView.post(new Runnable() {
            @Override
            public void run() {
                if (haveNextPage) {
                    // 列表滑动到底部加载下一组数据
                    loadData();
                } else {
                    mAdapter.openLoadMore(listPlus.size(), false);
                    if (getContext() != null) {
                        View view = LayoutInflater.from(getContext()).inflate(R.layout.load_more_footer_layout, (ViewGroup) recyclerView.getParent(), false);
                        ((TextView) view.findViewById(R.id.text)).setText(TheLApp.getContext().getString(R.string.info_no_more));
                        mAdapter.addFooterView(view);
                    }
                }
            }
        });
    }

    @Override
    public void reloadMore() {
        mAdapter.removeAllFooterView();
        mAdapter.openLoadMore(true);
        loadData();
    }

    @Override
    public void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(this.getClass().getName());
        MobclickAgent.onResume(getContext());
    }

    @Override
    public void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(this.getClass().getName());
        MobclickAgent.onPause(getContext());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onFollowStatusChanged(int followStatus, String userId, String nickName, String avatar) {
        super.onFollowStatusChanged(followStatus, userId, nickName, avatar);
        changeFollowStatus(userId, followStatus);
    }

    private void changeFollowStatus(String followUserId, int followStatus) {
        final int count = mAdapter.getData().size();
        for (int i = 0; i < count; i++) {
            final CommentWinkUserBean bean = mAdapter.getItem(i);
            if (followUserId.equals(bean.userId + "")) {
                /*if (bean.isFollow == SearchBean.FOLLOW_TYPE_UNFOLLOWED) {// 关注操作
                   // SendMsgUtils.getInstance().sendMessage(MsgUtils.createMsgBean(MsgBean.MSG_TYPE_FOLLOW, "", 1, bean.userId + "", bean.nickName, bean.avatar));
                    bean.isFollow = SearchBean.FOLLOW_TYPE_FOLLOWED;
                } else {//取消关注操作
                    bean.isFollow = SearchBean.FOLLOW_TYPE_UNFOLLOWED;
                }*/
                bean.followStatus = followStatus;
                mAdapter.notifyItemChanged(i + mAdapter.getHeaderLayoutCount());
                break;
            }
        }
    }

    private void requestFinished() {
        //        closeLoading();
        swipe_container.postDelayed(new Runnable() {

            @Override
            public void run() {
                if (swipe_container != null && swipe_container.isRefreshing())
                    swipe_container.setRefreshing(false);
            }
        }, 1000);
    }
}
