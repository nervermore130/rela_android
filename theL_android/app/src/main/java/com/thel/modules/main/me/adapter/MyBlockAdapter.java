package com.thel.modules.main.me.adapter;

import android.view.View;

import com.thel.R;
import com.thel.bean.user.BlockBean;
import com.thel.constants.TheLConstants;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;

import java.util.List;

/**
 * Created by lingwei on 2017/11/2.
 */

public class MyBlockAdapter extends BaseRecyclerViewAdapter<BlockBean> {

    public BlackUserLitener mBlackListener;

    public MyBlockAdapter(List<BlockBean> data) {
        super(R.layout.myblock_listitem, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, final BlockBean bean) {
        if (bean.verifyType <= 0) {//非加V用户，显示两行，第一行为昵称和简介，第二行为在线状态、距离、感情状态
            helper.setVisibility(R.id.line2, View.VISIBLE);
            helper.setVisibility(R.id.img_indentify, View.GONE);
            helper.setText(R.id.txt_desc, bean.userIntro);
            helper.setText(R.id.txt_distance, bean.getLoginTimeShow());
        } else {// 加V用户，显示一行，显示昵称、认证图标、认证信息
            helper.setVisibility(R.id.line2, View.GONE);
            helper.setText(R.id.txt_desc, bean.verifyIntro);
            helper.setVisibility(R.id.img_indentify, View.VISIBLE);
            if (bean.verifyType == 1) { //个人加v
                helper.setImageResource(R.id.img_indentify, R.mipmap.icn_verify_person);
            } else {//企业加v
                helper.setImageResource(R.id.img_indentify, R.mipmap.icn_verify_enterprise);
            }
        }
        // // 角色设定 0=unknow,1=t,2=p,3=h,5=bi
        if (("0").equals(bean.roleName)) {
            helper.setImageResource(R.id.img_role, R.mipmap.icn_role_unknow);
        } else if (("1").equals(bean.roleName)) {
            helper.setImageResource(R.id.img_role, R.mipmap.icn_role_t);
        } else if (("2").equals(bean.roleName)) {
            helper.setImageResource(R.id.img_role, R.mipmap.icn_role_p);
        } else if (("3").equals(bean.roleName)) {
            helper.setImageResource(R.id.img_role, R.mipmap.icn_role_h);
        } else if (("5").equals(bean.roleName)) {
            helper.setImageResource(R.id.img_role, R.mipmap.icn_role_bi);
        } else if (bean.roleName.equals("6")) {
            helper.setImageResource(R.id.img_role, R.mipmap.icn_role_s);
        } else {
            helper.setImageResource(R.id.img_role, R.mipmap.icn_role_unknow);
        }

        //头像
        helper.setImageUrl(R.id.img_thumb, bean.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE);

        helper.setText(R.id.txt_name, bean.userName);

        final int position = helper.getLayoutPosition() - getHeaderLayoutCount();
        helper.setOnClickListener(R.id.img_thumb, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBlackListener != null) {
                    mBlackListener.goToUserInfo(position, bean);

                }
            }
        });
        helper.setOnClickListener(R.id.txt_cancl, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBlackListener != null) {
                    mBlackListener.removeFromBlackList(position, bean);

                }
            }
        });

    }

    public interface BlackUserLitener {
        void removeFromBlackList(int pos, BlockBean bean);

        void goToUserInfo(int pos, BlockBean bean);
    }

    public void setRemoveFromBlackListener(BlackUserLitener blackListener) {
        this.mBlackListener = blackListener;
    }

}
