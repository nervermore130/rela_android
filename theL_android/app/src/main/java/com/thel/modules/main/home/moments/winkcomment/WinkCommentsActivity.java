package com.thel.modules.main.home.moments.winkcomment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.text.TextUtils;

import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.constants.TheLConstants;

/**
 * Created by liuyun on 2017/10/24.
 */

public class WinkCommentsActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_frame_normal);

        WinkCommentsFragment winkCommentsFragment = (WinkCommentsFragment) getSupportFragmentManager().findFragmentById(R.id.frame_content);

        if (winkCommentsFragment == null) {
            winkCommentsFragment = new WinkCommentsFragment();
            winkCommentsFragment.setArguments(getIntent().getExtras());
            getSupportFragmentManager().beginTransaction().add(R.id.frame_content, winkCommentsFragment, WinkCommentsFragment.class.getName()).commit();
        }

    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    public static void startActivity(Context context, String momentsId) {
        if (context == null || TextUtils.isEmpty(momentsId))
            return;
        Intent intent2 = new Intent(context, WinkCommentsActivity.class);
        intent2.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, momentsId);
        context.startActivity(intent2);
    }
}
