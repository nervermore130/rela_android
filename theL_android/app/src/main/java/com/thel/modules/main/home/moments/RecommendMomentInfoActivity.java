package com.thel.modules.main.home.moments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.pili.pldroid.player.PLOnCompletionListener;
import com.pili.pldroid.player.PLOnErrorListener;
import com.pili.pldroid.player.PLOnPreparedListener;
import com.pili.pldroid.player.widget.PLVideoTextureView;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseDataBean;
import com.thel.base.BaseImageViewerActivity;
import com.thel.bean.FriendRecommendDataBean;
import com.thel.bean.FriendRecommendListBean;
import com.thel.bean.ImgShareBean;
import com.thel.bean.MomentsDataBean;
import com.thel.bean.SharePosterBean;
import com.thel.bean.comment.CommentBean;
import com.thel.bean.gift.WinkCommentBean;
import com.thel.bean.moments.MomentParentBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.theme.ThemeClassBean;
import com.thel.constants.BundleConstants;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.growingio.GrowingIoConstant;
import com.thel.imp.momentblack.MomentBlackListener;
import com.thel.imp.momentblack.MomentBlackUtils;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.live.surface.watch.LiveWatchActivity;
import com.thel.modules.live.surface.watch.horizontal.LiveWatchHorizontalActivity;
import com.thel.modules.main.home.moments.comment.MomentCommentActivity;
import com.thel.modules.main.home.moments.theme.ThemeDetailActivity;
import com.thel.modules.main.home.moments.web.WebViewActivity;
import com.thel.modules.main.home.moments.winkcomment.WinkCommentsActivity;
import com.thel.modules.main.home.tag.TagDetailActivity;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.modules.others.VipConfigActivity;
import com.thel.modules.post.MomentPosterActivity;
import com.thel.modules.preview_image.UserInfoPhotoActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestConstants;
import com.thel.network.service.DefaultRequestService;
import com.thel.ui.TextSpan;
import com.thel.ui.TopicClickSpan;
import com.thel.ui.adapter.MomentLatestLikesAdapter;
import com.thel.ui.adapter.MomentsAdapter;
import com.thel.ui.adapter.RecommendMomentAdapter;
import com.thel.ui.decoration.DefaultItemDivider;
import com.thel.ui.widget.LatestCommentView;
import com.thel.ui.widget.LikeButtonView;
import com.thel.ui.widget.RecommendLiveUserView;
import com.thel.ui.widget.RecommendWebView;
import com.thel.ui.widget.SquareRelativeLayout;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.utils.AppInit;
import com.thel.utils.BusinessUtils;
import com.thel.utils.DeviceUtils;
import com.thel.utils.DialogUtil;
import com.thel.utils.DialogUtils;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.ImageUtils;
import com.thel.utils.MD5Utils;
import com.thel.utils.MomentUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SizeUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;
import com.thel.utils.VideoUtils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by waiarl on 2017/7/15.
 * 推荐日志详情（都谁谁谁推荐了本日志）
 */

public class RecommendMomentInfoActivity extends BaseImageViewerActivity implements View.OnClickListener, MomentBlackListener {
    private LinearLayout lin_back;
    private TextView title_txt;
    private ImageView img_share;
    private SwipeRefreshLayout swipe_container;
    private RecyclerView recyclerView;
    private String momentId;
    private int cursor = 1;
    private RelativeLayout momentView;
    private MomentsBean momentBean;

    private SimpleDraweeView img_thumb;
    private ImageView share_to;
    private TextView moment_user_name;
    private TextView moment_release_time;
    private TextView moment_content_text;
    private RelativeLayout moment_content_pic;
    private SimpleDraweeView pic;
    private SimpleDraweeView img_live;
    private LinearLayout lin_live_status;
    private TextView txt_livestatus;
    private SimpleDraweeView pic1;
    private SimpleDraweeView pic2;
    private SimpleDraweeView pic3;
    private SimpleDraweeView pic4;
    private SimpleDraweeView pic5;
    private SimpleDraweeView pic6;
    private TextView moment_opts_emoji_txt;
    private TextView moment_opts_comment_txt;
    private ImageView moment_opts_more;
    private LikeButtonView moment_opts_like;
    private ImageView moment_opts_comment;

    private MomentLatestLikesAdapter mMomentLatestLikesAdapter;
    private List<WinkCommentBean> mLikes = new ArrayList<>();

    // mentioned
    private LinearLayout moment_mentioned;
    private TextView moment_mentioned_text;

    // 音乐
    private LinearLayout moment_content_music;
    private SimpleDraweeView moment_content_music_pic;
    private ImageView img_play;
    private TextView song_name;
    private TextView album_name;
    private TextView artist_name;

    private ImageView moments_tag;

    // 根日志区域
    private LinearLayout lin_participate_theme;
    private SimpleDraweeView img;
    private TextView txt_theme_title;
    private TextView txt_theme_desc;
    private TextView txt_deleted;
    /**
     * 视频部分
     */
    private RelativeLayout rel_video;
    private PLVideoTextureView player;
    private ProgressBar progressbar_video;
    private SimpleDraweeView img_cover;
    private SimpleDraweeView img_play_video;
    private TextView txt_follow;//header中新增加关注按钮
    private TextView txt_play_times;//播放次数

    /**
     * 推荐用户以外的日志
     */
    private LinearLayout moment_content;

    /**
     * 推荐用户部分
     */
    private RelativeLayout rel_user_card;
    private ImageView img_user_card;
    private LinearLayout mask_user_card;
    private ImageView img_user_card_avatar;
    private TextView txt_user_card_nickname;
    private TextView txt_user_card_info;
    private TextView txt_user_card_intro;

    private LinearLayout lin_update_user_info;

    private RecommendWebView recommend_web_view;
    private RecommendLiveUserView live_user_liveview;

    private ImageView img_theme;
    private TextView txt_theme;
    private RelativeLayout rel_theme;
    private TextView select_theme;
    private RelativeLayout rel_select_theme;
    private TextView txt_theme_tag;
    private SquareRelativeLayout theme_content;
    private LinearLayout lin_latest_comments;
    private Matcher topicMatcher = TheLConstants.TOPIC_PATTERN.matcher("");
    private Matcher urlMatcher = TheLConstants.URL_PATTERN.matcher("");

    private List<MomentsBean> list = new ArrayList<>();
    private LinearLayoutManager manager;
    private RecommendMomentAdapter adapter;
    private MomentsBean operateComment;
    private Handler mHandler = new Handler(Looper.getMainLooper());


    public int refreshType = 0;
    public final int REFRESH_TYPE_ALL = 1;
    public final int REFRESH_TYPE_NEXT_PAGE = 2;
    public boolean haveNextPage = false;
    private int recommendCount = 0;
    private MomentBlackUtils momentBlackUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recommend_moment_info);

        initRefister();

        getIntentData();

        lin_back = findViewById(R.id.lin_back);
        title_txt = findViewById(R.id.title_txt);
        img_share = findViewById(R.id.img_share);
        swipe_container = findViewById(R.id.swipe_container);
        ViewUtils.initSwipeRefreshLayout(swipe_container);
        recyclerView = findViewById(R.id.recyclerview);
        manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(manager);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DefaultItemDivider(TheLApp.getContext(), LinearLayoutManager.VERTICAL, R.color.bg_color, 8, false, false, false));
        adapter = new RecommendMomentAdapter(list, this);
        momentView = (RelativeLayout) RelativeLayout.inflate(this, R.layout.moment_recommend_info_header, null);

        initHeaderView();

        recyclerView.setAdapter(adapter);

        adapter.addHeaderView(momentView);

        initDataState();

        setListener();

        getFriendRecommendDetail();


    }

    private void initRefister() {
        momentBlackUtils = new MomentBlackUtils();
        momentBlackUtils.registerReceiver(this);
        followStatusChangedImpl.registerReceiver(this);
    }

    private void getIntentData() {
        final Intent intent = getIntent();
        momentId = intent.getStringExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID);
        recommendCount = intent.getIntExtra(TheLConstants.BUNDLE_KEY_RECOMMEND_COUNT, 0);
        refreshTitle();
    }

    private void getFriendRecommendDetail() {
        Flowable<MomentsDataBean> flowable = DefaultRequestService.createAllRequestService().getFriendRecommendDetail(momentId);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<MomentsDataBean>() {
            @Override
            public void onNext(MomentsDataBean momentsBean) {
                super.onNext(momentsBean);
                refreshAll(momentsBean.data);
                loadFriendRecommendList(cursor);
            }
        });

        requestFinished();
    }

    private void loadFriendRecommendList(int cursor) {
        Flowable<FriendRecommendDataBean> flowable = DefaultRequestService.createAllRequestService().getMomentsFriendRecommendList(momentId, String.valueOf(cursor), String.valueOf(20));
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<FriendRecommendDataBean>() {
            @Override
            public void onNext(FriendRecommendDataBean friendRecommendListBean) {
                super.onNext(friendRecommendListBean);
                if (friendRecommendListBean.data != null) {
                    refreshList(friendRecommendListBean.data);
                }
            }
        });

        requestFinished();
    }


    private void initDataState() {
        img_share.setImageResource(R.mipmap.btn_send);
        img_share.setVisibility(View.GONE);
        title_txt.setText(getString(R.string.recommend_a_moment));
    }

    private void refreshAll(MomentsBean result) {
        momentBean = result;
        refreshHeader();
    }

    private void refreshTitle() {
        if (title_txt != null) {
            title_txt.setText(getString(R.string.people_recommend, recommendCount));
        }
    }

    /**
     * 更新list数据
     *
     * @param bean
     */
    private void refreshList(FriendRecommendListBean bean) {
        if (refreshType == REFRESH_TYPE_ALL) {//如果是刷新界面
            list.clear();
        }
        list.addAll(bean.list);
        cursor = bean.cursor;
        haveNextPage = bean.haveNextPage;
        if (REFRESH_TYPE_ALL == refreshType) {
            adapter.removeAllFooterView();
            adapter.setNewData(list);
            if (haveNextPage) {
                adapter.openLoadMore(list.size(), true);
            }
        } else {
            adapter.notifyDataChangedAfterLoadMore(true, list.size());
        }
        if (REFRESH_TYPE_ALL == refreshType) {//刷新回到顶部
            if (recyclerView.getChildCount() > 0) {
                recyclerView.scrollToPosition(0);
            }
        }
    }

    private void requestFinished() {
        recyclerView.post(new Runnable() {
            @Override
            public void run() {
                adapter.loadMoreFailed((ViewGroup) recyclerView.getParent());
            }
        });
        if (swipe_container != null)
            swipe_container.postDelayed(new Runnable() {

                @Override
                public void run() {
                    if (swipe_container != null && swipe_container.isRefreshing())
                        swipe_container.setRefreshing(false);
                }
            }, 1000);
        closeLoading();
    }

    private SpannableString buildThemeHeaderStr(String nickName, String str) {
        SpannableString spannableString = new SpannableString(nickName + " " + str);
        spannableString.setSpan(new TextSpan(12, R.color.text_color_gray), nickName.length(), nickName.length() + str.length() + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannableString;
    }

    private void jumpToUserInfo(String userId) {
//        Intent intent = new Intent();
//        intent.setClass(this, UserInfoActivity.class);
//        intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, userId + "");
//        startActivity(intent);
        FlutterRouterConfig.Companion.gotoUserInfo(userId);
    }

    @Override
    public void onClick(final View v) {
        switch (v.getId()) {
            case R.id.img_thumb:
                ViewUtils.preventViewMultipleClick(v, 1000);
                jumpToUserInfo((String) v.getTag());
                break;
            case R.id.moment_opts_more:
                ViewUtils.preventViewMultipleClick(v, 1000);
                operateComment = (MomentsBean) v.getTag(R.id.comment_tag);
                if (operateComment != null && !TextUtils.isEmpty(operateComment.momentsId)) {
                    final boolean isCollect = !BusinessUtils.isCollected(operateComment.momentsId);//是否被收藏,如果已经被收藏，则为true,要收藏，否则，要删除
                    String collectMsg = isCollect ? getString(R.string.collection) : getString(R.string.collection_cancel);//收藏/取消收藏 文案
                    if (ShareFileUtils.getString(ShareFileUtils.ID, "").equals(operateComment.userId + "")) {
                        DialogUtil.getInstance().showSelectionDialog(RecommendMomentInfoActivity.this, new String[]{collectMsg, getString(R.string.post_moment_share_to), getString(R.string.share_poster)}, new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                DialogUtil.getInstance().closeDialog();
                                switch (position) {

                                    case 0:
                                        MomentUtils.collect(isCollect, momentBean);
                                        break;
                                    case 1:
                                        share(0);

                                        break;
                                    case 2:
                                        share(1);

                                        break;

                                    default:
                                        break;
                                }
                            }
                        }, true, 1, null);
                    } else {
                        DialogUtil.getInstance().showSelectionDialog(RecommendMomentInfoActivity.this, new String[]{collectMsg, getString(R.string.post_moment_share_to), getString(R.string.share_poster), getString(R.string.info_report)}, new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                DialogUtil.getInstance().closeDialog();
                                switch (position) {
                                    case 0://2.17 收藏
                                        MomentUtils.collect(isCollect, momentBean);
                                        break;
                                    case 1:
                                        share(0);
                                        break;
                                    case 2:
                                        share(1);
                                        break;
                                    case 3:
                                        reportThemeComment();
                                        break;

                                    default:
                                        break;
                                }
                            }
                        }, false, 2, null);
                    }
                }
                break;
            case R.id.moment_opts_like:
                ViewUtils.preventViewMultipleClick(v, 1000);
                String momentsId = (String) v.getTag(R.id.moment_id_tag);
                int likeOrNot = (int) v.getTag(R.id.moment_like_tag);
                if (!TextUtils.isEmpty(momentsId)) {
                    if (likeOrNot == MomentsBean.HAS_NOT_WINKED) {
                        ((LikeButtonView) v).check();
                        likeMoment(momentsId);
                    } else {
                        unlikeMoment(momentsId);
                    }
                }
                break;
            case R.id.moment_opts_comment:
                jumpToCommentPage((String) v.getTag(R.id.moment_id_tag), true);
                break;
            case R.id.txt_follow://关注
                ViewUtils.preventViewMultipleClick(v, 1000);
                final int operatePosition = (int) v.getTag(R.id.comment_tag);
                if (operatePosition >= 0 && adapter.getData().size() > operatePosition) {
                    operateComment = adapter.getItem(operatePosition);
                    if (operateComment != null && !TextUtils.isEmpty(operateComment.momentsId)) {
                        //followUser(operateComment, operatePosition);
                        //                        FollowStatusChangedImpl.followUser(operateComment.userId + "", FollowStatusChangedImpl.ACTION_TYPE_FOLLOW, operateComment.nickname, operateComment.avatar);
                    }
                }
                break;
            case R.id.moment_content_music:
                //                String curMomentId = (String) v.getTag(R.id.moment_id_tag);
                //                if (XiamiSDKUtil.getInstance().getCurrentSong() == null || (!TextUtils.isEmpty(XiamiSDKUtil.getInstance().songMomentId) && !XiamiSDKUtil.getInstance().songMomentId.equals(curMomentId))) {// 新播放一首歌
                //                    XiamiSDKUtil.getInstance().songMomentId = curMomentId;
                //                    ((ImageView) v.findViewById(R.id.img_play)).setImageResource(R.drawable.btn_feed_pause_big);
                //                    XiamiSDKUtil.getInstance().mXiamiSDK.findSongById((Long) v.getTag(R.id.song_id_tag), new OnlineSongCallback() {
                //
                //                        @Override
                //                        public void onResponse(int failedCode, OnlineSong onlineSong) {
                //                            switch (failedCode) {
                //                                case 0:// 获取成功
                //                                    song = onlineSong;
                //                                    XiamiSDKUtil.getInstance().setSong(song);
                //                                    break;
                //
                //                                default:
                //                                    mHandler.post(new Runnable() {
                //                                        @Override
                //                                        public void run() {
                //                                            ((ImageView) v.findViewById(R.id.img_play)).setImageResource(R.drawable.btn_feed_play_big);
                //                                        }
                //                                    });
                //                                    return;
                //                            }
                //                        }
                //                    });
                //                } else {// 播放或暂停刚才的歌曲
                //                    // 播放/暂停
                //                    if (!XiamiSDKUtil.getInstance().isPlaying()) {
                //                        if (song == null) {
                //                            song = XiamiSDKUtil.getInstance().getSong();
                //                        }
                //                        XiamiSDKUtil.getInstance().play(song);
                //                        XiamiSDKUtil.getInstance().songMomentId = curMomentId;
                //                        ((ImageView) v.findViewById(R.id.img_play)).setImageResource(R.drawable.btn_feed_pause_big);
                //                    } else {
                //                        XiamiSDKUtil.getInstance().pause();
                //                        ((ImageView) v.findViewById(R.id.img_play)).setImageResource(R.drawable.btn_feed_play_big);
                //                    }
                //                }
                break;
        }
    }

    private void startPlayback() {
        player.setVisibility(VISIBLE);
        progressbar_video.setVisibility(VISIBLE);
        img_play_video.setVisibility(GONE);
        txt_play_times.setVisibility(View.GONE);
        player.setVideoPath(momentBean.videoUrl);
        player.requestFocus();
        uploadVideoCount(momentBean.momentsId);

    }

    /**
     * 刷新点赞状态，包括点赞列表
     */
    private void updateLikeStatus(int likePosition) {
        // 是否已点过赞
        if (momentBean.winkFlag != MomentsBean.HAS_NOT_WINKED) {
            moment_opts_like.setChecked(true);
        } else {
            moment_opts_like.setChecked(false);
        }
        // 点赞数
        if (momentBean.winkNum <= 0) {
            moment_opts_emoji_txt.setVisibility(GONE);
        } else {
            moment_opts_emoji_txt.setVisibility(VISIBLE);
            moment_opts_emoji_txt.setText(TheLApp.getContext().getString(momentBean.winkNum == 1 ? R.string.like_count_odd : R.string.like_count_even, momentBean.winkNum));
        }

        if (mMomentLatestLikesAdapter == null) {
            mMomentLatestLikesAdapter = new MomentLatestLikesAdapter(mLikes);
            mMomentLatestLikesAdapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    jumpToUserInfo(mMomentLatestLikesAdapter.getItem(position - mMomentLatestLikesAdapter.getHeaderLayoutCount()).userId);
                }
            });
        }
        if (likePosition >= 0) {
            if (momentBean.winkFlag != MomentsBean.HAS_NOT_WINKED) {
                mMomentLatestLikesAdapter.notifyItemInserted(likePosition);
            } else {
                mMomentLatestLikesAdapter.notifyItemRemoved(likePosition);
            }
        } else {
            mMomentLatestLikesAdapter.notifyDataSetChanged();
        }
    }

    private void updateCommentNum() {
        if (momentBean.commentNum <= 0) {
            moment_opts_comment_txt.setVisibility(GONE);
        } else {
            moment_opts_comment_txt.setVisibility(VISIBLE);
            moment_opts_comment_txt.setText(TheLApp.getContext().getString(momentBean.commentNum <= 1 ? R.string.comment_count_odd : R.string.comment_count_even, momentBean.commentNum));
        }
    }

    private void initHeaderView() {
        if (momentView == null) {
            return;
        }
        img_thumb = momentView.findViewById(R.id.img_thumb);
        share_to = momentView.findViewById(R.id.share_to_img);
        moment_user_name = momentView.findViewById(R.id.moment_user_name);
        moment_release_time = momentView.findViewById(R.id.moment_release_time);
        moment_content_text = momentView.findViewById(R.id.moment_content_text);
        moment_content_pic = momentView.findViewById(R.id.moment_content_pic);
        pic = momentView.findViewById(R.id.pic);
        img_live = momentView.findViewById(R.id.img_live);
        lin_live_status = momentView.findViewById(R.id.lin_live_status);
        txt_livestatus = momentView.findViewById(R.id.txt_livestatus);
        pic1 = momentView.findViewById(R.id.pic1);
        pic2 = momentView.findViewById(R.id.pic2);
        pic3 = momentView.findViewById(R.id.pic3);
        pic4 = momentView.findViewById(R.id.pic4);
        pic5 = momentView.findViewById(R.id.pic5);
        pic6 = momentView.findViewById(R.id.pic6);

        moment_content = momentView.findViewById(R.id.moment_content);

        rel_video = momentView.findViewById(R.id.rel_video);
        player = momentView.findViewById(R.id.player);
        progressbar_video = momentView.findViewById(R.id.progressbar_video);
        img_cover = momentView.findViewById(R.id.img_cover);
        img_play_video = momentView.findViewById(R.id.img_play_video);
        txt_play_times = momentView.findViewById(R.id.txt_play_times);

        rel_user_card = momentView.findViewById(R.id.rel_user_card);
        img_user_card = momentView.findViewById(R.id.img_user_card);
        mask_user_card = momentView.findViewById(R.id.mask_user_card);
        img_user_card_avatar = momentView.findViewById(R.id.img_user_card_avatar);
        txt_user_card_nickname = momentView.findViewById(R.id.txt_user_card_nickname);
        txt_user_card_info = momentView.findViewById(R.id.txt_user_card_info);
        txt_user_card_intro = momentView.findViewById(R.id.txt_user_card_intro);

        moment_content_music = momentView.findViewById(R.id.moment_content_music);
        moment_content_music_pic = momentView.findViewById(R.id.moment_content_music_pic);
        img_play = momentView.findViewById(R.id.img_play);
        song_name = momentView.findViewById(R.id.song_name);
        album_name = momentView.findViewById(R.id.album_name);
        artist_name = momentView.findViewById(R.id.artist_name);

        moment_opts_emoji_txt = momentView.findViewById(R.id.moment_opts_emoji_txt);
        moment_opts_comment_txt = momentView.findViewById(R.id.moment_opts_comment_txt);
        moment_opts_more = momentView.findViewById(R.id.moment_opts_more);
        moment_opts_like = momentView.findViewById(R.id.moment_opts_like);
        moment_opts_like.setLikeAnimImage(momentView.findViewById(R.id.img_like_anim));
        moment_opts_comment = momentView.findViewById(R.id.moment_opts_comment);

        moment_mentioned = momentView.findViewById(R.id.moment_mentioned);
        moment_mentioned_text = momentView.findViewById(R.id.moment_mentioned_text);

        moments_tag = momentView.findViewById(R.id.moments_tag);
        lin_participate_theme = momentView.findViewById(R.id.lin_participate_theme);
        img = momentView.findViewById(R.id.img);
        txt_theme_title = momentView.findViewById(R.id.txt_theme_title);
        txt_theme_desc = momentView.findViewById(R.id.txt_theme_desc);
        txt_deleted = momentView.findViewById(R.id.txt_deleted);
        txt_follow = momentView.findViewById(R.id.txt_follow);//新增加关注按钮
        recommend_web_view = momentView.findViewById(R.id.recommend_web_view);
        live_user_liveview = momentView.findViewById(R.id.live_user_liveview);
        //moment_parent_view = (MomentParentView) momentView.findViewById(R.id.moment_parent_view);
        //话题
        img_theme = momentView.findViewById(R.id.img_theme);
        txt_theme = momentView.findViewById(R.id.txt_theme);
        rel_theme = momentView.findViewById(R.id.rel_theme);
        select_theme = momentView.findViewById(R.id.select_theme);
        rel_select_theme = momentView.findViewById(R.id.rel_select_theme);
        txt_theme_tag = momentView.findViewById(R.id.txt_theme_tag);
        theme_content = momentView.findViewById(R.id.theme_content);
        //最近三条评论
        lin_latest_comments = momentView.findViewById(R.id.lin_latest_comments);
        //话题
        setViewSize();

    }

    private void setViewSize() {
        int bigSize = (int) (AppInit.displayMetrics.widthPixels - TheLApp.getContext().getResources().getDimension(R.dimen.moment_list_margin) * 2);
        final RelativeLayout.LayoutParams bigSizeParams = new RelativeLayout.LayoutParams(bigSize, bigSize);
        bigSizeParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        img_cover.setLayoutParams(bigSizeParams);
        player.setLayoutParams(bigSizeParams);
        moment_content_music_pic.setLayoutParams(bigSizeParams);
        rel_user_card.setLayoutParams(bigSizeParams);
        pic.setLayoutParams(bigSizeParams);
        rel_theme.setLayoutParams(bigSizeParams);
        img_theme.setLayoutParams(bigSizeParams);
    }

    private void setListener() {

        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                refreshType = REFRESH_TYPE_ALL;
                cursor = 0;
                getFriendRecommendDetail();
            }
        });
        moment_opts_comment.setOnClickListener(this);
        adapter.setOnLoadMoreListener(new BaseRecyclerViewAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                recyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        if (haveNextPage) {
                            // 列表滑动到底部加载下一组数据
                            loadFriendRecommendList(cursor);
                        } else {
                            adapter.openLoadMore(list.size(), false);

                            View view = getLayoutInflater().inflate(R.layout.load_more_footer_layout, (ViewGroup) recyclerView.getParent(), false);
                            ((TextView) view.findViewById(R.id.text)).setText(TheLApp.getContext().getString(R.string.info_no_more));
                            adapter.addFooterView(view);
                        }
                    }
                });
            }
        });
        adapter.setReloadMoreListener(new BaseRecyclerViewAdapter.ReloadMoreListener() {
            @Override
            public void reloadMore() {
                adapter.removeAllFooterView();
                adapter.openLoadMore(true);
                loadFriendRecommendList(cursor);
            }
        });
        adapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                gotoMomentDetail(adapter.getData().get(position));
            }
        });
        lin_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewUtils.preventViewMultipleClick(view, 1000);
                if (MomentsBean.MOMENT_TYPE_LIVE.equals(momentBean.momentsType) && !TextUtils.isEmpty(momentBean.liveId)) {// 直播日志
                    if (BusinessUtils.canIntoLiveRoom(TheLApp.getContext(), momentBean.userId + "")) {
                        Intent intent;
                        if (momentBean.isLandscape == 0) {
                            intent = new Intent(RecommendMomentInfoActivity.this, LiveWatchActivity.class);
                        } else {
                            intent = new Intent(TheLApp.getContext(), LiveWatchHorizontalActivity.class);
                        }
                        intent.putExtra(TheLConstants.BUNDLE_KEY_ID, momentBean.liveId);
                        intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, momentBean.userId + "");
                        intent.putExtra(LiveWatchActivity.FROM_PAGE, LiveWatchActivity.FROM_PAGE_MOMENT);
                        startActivity(intent);

                        GrowingIOUtil.postWatchLiveEntry(GrowingIoConstant.G_LiveEntry_Other);

                    }
                } else {
                    Intent intent = new Intent(RecommendMomentInfoActivity.this, UserInfoPhotoActivity.class);
                    ArrayList<String> photos = new ArrayList<String>();
                    photos.add(momentBean.imageUrl);
                    intent.putStringArrayListExtra(TheLConstants.BUNDLE_KEY_PHOTOS, photos);
                    intent.putExtra(TheLConstants.BUNDLE_KEY_RELA_ID, momentBean.userName);
                    ImgShareBean bean = Utils.getImageShareBean(momentBean);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(TheLConstants.BUNDLER_KEY_IMAGESHAREBEAN, bean);
                    intent.putExtras(bundle);
                    intent.putExtra("position", 0);
                    startActivity(intent);
                }
            }
        });

        pic1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewUtils.preventViewMultipleClick(view, 1000);
                Intent intent5 = new Intent(RecommendMomentInfoActivity.this, UserInfoPhotoActivity.class);
                ArrayList<String> photos = new ArrayList<String>();
                String[] photoUrls = momentBean.imageUrl.split(",");
                ImgShareBean bean = Utils.getImageShareBean(momentBean);
                for (int i = 0; i < photoUrls.length; i++) {
                    photos.add(photoUrls[i]);
                }
                intent5.putStringArrayListExtra(TheLConstants.BUNDLE_KEY_PHOTOS, photos);
                intent5.putExtra(TheLConstants.BUNDLE_KEY_RELA_ID, momentBean.userName);
                Bundle bundle = new Bundle();
                bundle.putSerializable(TheLConstants.BUNDLER_KEY_IMAGESHAREBEAN, bean);
                intent5.putExtras(bundle);

                intent5.putExtra("position", 0);
                startActivity(intent5);
            }
        });

        pic2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewUtils.preventViewMultipleClick(view, 1000);
                Intent intent5 = new Intent(RecommendMomentInfoActivity.this, UserInfoPhotoActivity.class);
                ArrayList<String> photos = new ArrayList<String>();
                String[] photoUrls = momentBean.imageUrl.split(",");
                ImgShareBean bean = Utils.getImageShareBean(momentBean);
                for (int i = 0; i < photoUrls.length; i++) {
                    photos.add(photoUrls[i]);
                }
                intent5.putStringArrayListExtra(TheLConstants.BUNDLE_KEY_PHOTOS, photos);
                intent5.putExtra(TheLConstants.BUNDLE_KEY_RELA_ID, momentBean.userName);
                Bundle bundle = new Bundle();
                bundle.putSerializable(TheLConstants.BUNDLER_KEY_IMAGESHAREBEAN, bean);
                intent5.putExtras(bundle);

                intent5.putExtra("position", 1);
                startActivity(intent5);
            }
        });

        pic3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewUtils.preventViewMultipleClick(view, 1000);
                Intent intent5 = new Intent(RecommendMomentInfoActivity.this, UserInfoPhotoActivity.class);
                ArrayList<String> photos = new ArrayList<String>();
                String[] photoUrls = momentBean.imageUrl.split(",");
                ImgShareBean bean = Utils.getImageShareBean(momentBean);

                for (int i = 0; i < photoUrls.length; i++) {
                    photos.add(photoUrls[i]);
                }
                intent5.putStringArrayListExtra(TheLConstants.BUNDLE_KEY_PHOTOS, photos);
                intent5.putExtra(TheLConstants.BUNDLE_KEY_RELA_ID, momentBean.userName);
                Bundle bundle = new Bundle();
                bundle.putSerializable(TheLConstants.BUNDLER_KEY_IMAGESHAREBEAN, bean);
                intent5.putExtras(bundle);

                intent5.putExtra("position", 2);
                startActivity(intent5);
            }
        });

        pic4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewUtils.preventViewMultipleClick(view, 1000);
                Intent intent5 = new Intent(RecommendMomentInfoActivity.this, UserInfoPhotoActivity.class);
                ArrayList<String> photos = new ArrayList<String>();
                ImgShareBean bean = Utils.getImageShareBean(momentBean);

                String[] photoUrls = momentBean.imageUrl.split(",");
                for (int i = 0; i < photoUrls.length; i++) {
                    photos.add(photoUrls[i]);
                }
                intent5.putStringArrayListExtra(TheLConstants.BUNDLE_KEY_PHOTOS, photos);
                intent5.putExtra(TheLConstants.BUNDLE_KEY_RELA_ID, momentBean.userName);
                Bundle bundle = new Bundle();
                bundle.putSerializable(TheLConstants.BUNDLER_KEY_IMAGESHAREBEAN, bean);
                intent5.putExtras(bundle);

                intent5.putExtra("position", 3);
                startActivity(intent5);
            }
        });

        pic5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewUtils.preventViewMultipleClick(view, 1000);
                Intent intent5 = new Intent(RecommendMomentInfoActivity.this, UserInfoPhotoActivity.class);
                ArrayList<String> photos = new ArrayList<String>();
                ImgShareBean bean = Utils.getImageShareBean(momentBean);
                String[] photoUrls = momentBean.imageUrl.split(",");
                for (int i = 0; i < photoUrls.length; i++) {
                    photos.add(photoUrls[i]);
                }
                intent5.putStringArrayListExtra(TheLConstants.BUNDLE_KEY_PHOTOS, photos);
                intent5.putExtra(TheLConstants.BUNDLE_KEY_RELA_ID, momentBean.userName);
                Bundle bundle = new Bundle();
                bundle.putSerializable(TheLConstants.BUNDLER_KEY_IMAGESHAREBEAN, bean);
                intent5.putExtra("position", 4);
                intent5.putExtras(bundle);

                startActivity(intent5);
            }
        });

        pic6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewUtils.preventViewMultipleClick(view, 1000);
                Intent intent5 = new Intent(RecommendMomentInfoActivity.this, UserInfoPhotoActivity.class);
                ArrayList<String> photos = new ArrayList<String>();
                ImgShareBean bean = Utils.getImageShareBean(momentBean);
                String[] photoUrls = momentBean.imageUrl.split(",");
                for (int i = 0; i < photoUrls.length; i++) {
                    photos.add(photoUrls[i]);
                }
                intent5.putStringArrayListExtra(TheLConstants.BUNDLE_KEY_PHOTOS, photos);
                intent5.putExtra(TheLConstants.BUNDLE_KEY_RELA_ID, momentBean.userName);
                Bundle bundle = new Bundle();
                bundle.putSerializable(TheLConstants.BUNDLER_KEY_IMAGESHAREBEAN, bean);
                intent5.putExtra("position", 5);
                intent5.putExtras(bundle);

                startActivity(intent5);
            }
        });

        moment_opts_emoji_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (momentBean != null) {
                    Intent intent = new Intent(RecommendMomentInfoActivity.this, WinkCommentsActivity.class);
                    intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, momentBean.momentsId);
                    RecommendMomentInfoActivity.this.startActivity(intent);
                }
            }
        });

        // 更多操作
        moment_opts_more.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final boolean isCollect = !BusinessUtils.isCollected(momentBean.momentsId);//是否被收藏,如果已经被收藏，则为true,要收藏，否则，要删除
                String collectMsg = isCollect ? getString(R.string.collection) : getString(R.string.collection_cancel);//收藏/取消收藏 文案
                if (momentBean.myself == MomentsBean.IS_MINE) {
                    String stick;
                    if (momentBean.userBoardTop == 0) {
                        if (UserUtils.getUserVipLevel() > 0) {
                            stick = getString(R.string.moments_stick_moment_vip);
                        } else {
                            stick = getString(R.string.moments_stick_moment);
                        }
                    } else {
                        if (UserUtils.getUserVipLevel() > 0) {
                            stick = getString(R.string.moments_unstick_moment_vip);
                        } else {
                            stick = getString(R.string.moments_unstick_moment);
                        }
                    }
                    String setPrivate;
                    if (momentBean.shareTo < MomentsBean.SHARE_TO_ONLY_ME) {
                        setPrivate = getString(R.string.moments_set_as_private);
                    } else {
                        setPrivate = getString(R.string.moments_set_as_public);
                    }
                    DialogUtil.getInstance().showSelectionDialog(RecommendMomentInfoActivity.this, new String[]{stick, setPrivate, collectMsg, getString(R.string.post_moment_share_to), getString(R.string.share_poster), getString(R.string.info_delete)}, new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            DialogUtil.getInstance().closeDialog();
                            switch (position) {
                                case 0:
                                    if (UserUtils.getUserVipLevel() > 0) {
                                        if (momentBean.userBoardTop == 0)
                                            MomentUtils.stickMomentDataWhitDialog(RecommendMomentInfoActivity.this, momentBean);
                                        else
                                            MomentUtils.unstickMomentDataWhitDialog(RecommendMomentInfoActivity.this, momentBean);
                                    } else {
                                        MobclickAgent.onEvent(TheLApp.getContext(), "check_top_moment");
                                        startActivity(new Intent(RecommendMomentInfoActivity.this, VipConfigActivity.class));
                                    }
                                    break;
                                case 1:
                                    if (momentBean.shareTo < MomentsBean.SHARE_TO_ONLY_ME) {
                                        MomentUtils.setMomentAsPrivate(RecommendMomentInfoActivity.this, momentBean, new MomentsAdapter.PrivateOrPublicMomentSubscribe(momentBean, adapter));
                                    } else {
                                        MomentUtils.setMomentAsPublic(RecommendMomentInfoActivity.this, momentBean, new MomentsAdapter.PrivateOrPublicMomentSubscribe(momentBean, adapter));
                                    }
                                    break;
                                case 2://2.17 收藏
                                    MomentUtils.collect(isCollect, momentBean);
                                    break;
                                case 3:
                                    share(0);
                                    break;
                                case 4:
                                    share(1);
                                    break;
                                case 5:
                                    MomentUtils.deleteMomentData(RecommendMomentInfoActivity.this, momentBean);
                                    break;

                                default:
                                    break;
                            }
                        }
                    }, true, 0, null);
                } else {
                   /* if (!TextUtils.isEmpty(fromPage) && UserInfoActivity.class.getName().equals(fromPage)) {// 如果是从用户详情点击进入，则只能举报，不能屏蔽
                        dialogUtils.showSelectionDialogWide(RecommendMomentInfoActivity.this, 3 / 4f, new String[]{collectMsg, getString(R.string.post_moment_share_to), getString(R.string.share_poster), getString(R.string.info_report)}, new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                dialogUtils.closeDialog();
                                switch (position) {
                                    case 0://2.17 收藏
                                        collect(isCollect);
                                        break;
                                    case 1:
                                        share(0);
                                        break;
                                    case 2:
                                        share(1);
                                        break;
                                    case 3:
                                        reportMoment();
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }, true, 1, null);
                    } else */
                    {
                        DialogUtils.showSelectionDialogWide(RecommendMomentInfoActivity.this, 3 / 4f, new String[]{getString(R.string.info_report), getString(R.string.my_block_user_moments_activity_block_moment), getString(R.string.my_block_user_moments_activity_block), collectMsg, getString(R.string.post_moment_share_to), getString(R.string.share_poster)}, new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                DialogUtils.dismiss();
                                switch (position) {
                                    case 0:
                                        MomentUtils.reportMoment(RecommendMomentInfoActivity.this, momentBean);

                                        break;
                                    case 1:
                                        // MomentUtils.blockThisMoment(RecommendMomentInfoActivity.this, momentBean);
                                        MomentBlackUtils.blackThisMoment(momentBean.momentsId);
                                        break;
                                    case 2:
                                        //   MomentUtils.blockHerMoments(RecommendMomentInfoActivity.this, momentBean);
                                        MomentBlackUtils.blackHerMoment(momentBean.userId + "");
                                        break;
                                    case 3:
                                        MomentUtils.collect(isCollect, momentBean);

                                        break;
                                    case 4:
                                        share(0);

                                        break;
                                    case 5:
                                        share(1);

                                        break;
                                    default:
                                        break;
                                }
                            }
                        }, true, 0, null);
                    }
                }

            }
        });
        // 点赞
        moment_opts_like.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                if (MomentUtils.isBlackOrBlock(momentBean.userId + "")) {
                    return;
                }
                if (momentBean.winkFlag == MomentsBean.HAS_NOT_WINKED) {
                    likeMoment();
                } else {
                    momentBean.winkFlag = MomentsBean.HAS_NOT_WINKED;
                    MomentUtils.unwinkMoment(momentBean.momentsId);
                }
            }
        });

        player.setOnPreparedListener(new PLOnPreparedListener() {
            @Override public void onPrepared(int i) {
                player.setVolume(0, 0);
                progressbar_video.setVisibility(GONE);
                player.start();
            }
        });

        player.setOnCompletionListener(new PLOnCompletionListener() {
            @Override public void onCompletion() {
                if (player != null) {
                    VideoUtils.saveVideoReport(momentBean.momentsId, (int) player.getDuration(), 0);
                    player.seekTo(0);
                    player.start();
                }
            }
        });

        player.setOnErrorListener(new PLOnErrorListener() {
            @Override public boolean onError(int i) {
                stopPlayback();
                return false;
            }
        });
        txt_follow.setOnClickListener(new View.OnClickListener() {//关注点击按钮
            @Override
            public void onClick(View v) {
                showLoadingNoBack();
            }
        });
    }

    private void gotoMomentDetail(MomentsBean momentsBean) {
//        final Intent intent = new Intent(this, MomentCommentActivity.class);
//        Bundle bundle = new Bundle();
//        bundle.putString(TheLConstants.BUNDLE_KEY_MOMENT_ID, momentsBean.momentsId);
//        intent.putExtras(bundle);
//        startActivity(intent);
        FlutterRouterConfig.Companion.gotoMomentDetails(momentsBean.momentsId);
    }


    private void refreshHeader() {

        // 头像和昵称
        if (momentBean.secret == MomentsBean.IS_SECRET) {// 是否匿名
            img_thumb.setImageURI(Uri.parse(TheLConstants.RES_PIC_URL + R.mipmap.icon_user_anonymous));
            moment_user_name.setText(R.string.moments_msgs_notification_anonymous);
            img_thumb.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // 屏蔽匿名用户头像的点击
                }
            });
        } else {
            img_thumb.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(momentBean.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE)));
            MomentUtils.setNicknameWithMomentType(moment_user_name, momentBean);
            img_thumb.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (momentBean != null) {
                        jumpToUserInfo(momentBean.userId + "");
                    }
                }
            });
        }
        // shareTo
        if (momentBean.shareTo == MomentsBean.SHARE_TO_FRIENDS) {
            share_to.setVisibility(VISIBLE);
            share_to.setImageResource(R.mipmap.icn_post_friends);
        } else if (momentBean.shareTo == MomentsBean.SHARE_TO_ONLY_ME) {
            share_to.setVisibility(VISIBLE);
            share_to.setImageResource(R.mipmap.icn_post_privacy);
        } else {
            share_to.setVisibility(GONE);
        }

        // 发布时间
        moment_release_time.setText(MomentUtils.getReleaseTimeShow(momentBean.momentsTime));
        // 推荐标签
        if (!TextUtils.isEmpty(momentBean.tag)) {
            if (momentBean.tag.equals(MomentsBean.TAG_TOP)) {
                moments_tag.setImageResource(R.mipmap.icn_feed_top);
                moments_tag.setVisibility(VISIBLE);
            } else if (momentBean.tag.equals(MomentsBean.TAG_RECOMMEND)) {
                moments_tag.setImageResource(R.mipmap.icn_feed_edited);
                moments_tag.setVisibility(VISIBLE);
            } else {
                moments_tag.setVisibility(GONE);
            }
        }

        // 是否是用户个人置顶(显示优先级高于推荐标签)
        if (momentBean.userBoardTop == 1) {
            moments_tag.setImageResource(R.mipmap.icn_feed_top);
            moments_tag.setVisibility(VISIBLE);
        }
        /** 日志主内容区域 **/
        lin_participate_theme.setVisibility(View.GONE);
        moment_content.setVisibility(View.GONE);
        rel_user_card.setVisibility(View.GONE);
        theme_content.setVisibility(View.GONE);
        moment_content_text.setVisibility(View.GONE);
        recommend_web_view.setVisibility(View.GONE);
        live_user_liveview.setVisibility(View.GONE);
        if (MomentsBean.MOMENT_TYPE_THEME.equals(momentBean.momentsType)) {// 话题日志，显示话题区域
            theme_content.setVisibility(View.VISIBLE);
            if (momentBean.momentsText != null)
                txt_theme.setText(momentBean.momentsText);
            if (momentBean.themeClass != null) {
                final ThemeClassBean themeClassBean = MomentUtils.getThemeByType(momentBean.themeClass);
                //  helper.setOnClickListener(R.id.rel_select_theme, null);
                if (themeClassBean != null) {
                    rel_select_theme.setVisibility(VISIBLE);
                    select_theme.setText(themeClassBean.text + " >");
                    rel_select_theme.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //                            Utils.gotoThemeTab(themeClassBean);
                        }
                    });
                } else {
                    rel_select_theme.setVisibility(GONE);

                }
            }
            txt_theme.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    DialogUtil.getInstance().showSelectionDialog(RecommendMomentInfoActivity.this, new String[]{TheLApp.getContext().getString(R.string.info_copy)}, new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            DialogUtil.getInstance().closeDialog();
                            switch (position) {
                                case 0:// 复制
                                    DeviceUtils.copyToClipboard(TheLApp.getContext(), momentBean.momentsText);
                                    break;

                                default:
                                    break;
                            }
                        }
                    }, false, 2, null);
                    return true;
                }
            });
            if (!TextUtils.isEmpty(momentBean.themeTagName)) {
                txt_theme_tag.setVisibility(VISIBLE);
                SpannableString sp = new SpannableString(momentBean.themeTagName);
                sp.setSpan(new ForegroundColorSpan(ContextCompat.getColor(RecommendMomentInfoActivity.this, R.color.tag_color)), 0, momentBean.themeTagName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                txt_theme_tag.setText(sp);
                txt_theme_tag.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        Intent intent = new Intent(TheLApp.getContext().getApplicationContext(), TagDetailActivity.class);
//                        intent.putExtra(TheLConstants.BUNDLE_KEY_TOPIC_NAME, momentBean.themeTagName);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        TheLApp.getContext().getApplicationContext().startActivity(intent);
                        FlutterRouterConfig.Companion.gotoTagDetails(Integer.parseInt(momentBean.topicId),momentBean.themeTagName);
                    }
                });
            } else {
                txt_theme_tag.setVisibility(GONE);
            }
            if (!TextUtils.isEmpty(momentBean.imageUrl)) {// 话题日志有可能不带背景图，所以要显示默认背景

                rel_theme.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray_mask));

                ImageLoaderManager.imageLoader(img_theme, R.mipmap.bg_default_tags, ImageUtils.buildNetPictureUrl(momentBean.imageUrl, TheLConstants.MOMENT_PIC_BIG_SIZE, TheLConstants.MOMENT_PIC_BIG_SIZE));

            } else {

                rel_theme.setBackgroundColor(ContextCompat.getColor(this, R.color.transparent));
                ImageLoaderManager.imageLoader(img_theme, R.mipmap.bg_default_tags);

            }

        } else {
            if (!TextUtils.isEmpty(momentBean.momentsText) && !MomentsBean.MOMENT_TYPE_USER_CARD.equals(momentBean.momentsType)) {// 推荐用户卡片类型的日志，momentText保存的是推荐理由，不显示在日志内容中
                /**4.0.0 由于网页日志的特殊性，所以要重新获取momentText**/
                final String momentText = MomentUtils.getMomentTextFromMoment(momentBean);

                moment_content_text.setVisibility(VISIBLE);
                // 检查是否有输入话题，有的话把话题标红
                topicMatcher.reset(momentText);
                List<Integer> startIndexs = new ArrayList<Integer>();
                List<Integer> endIndexs = new ArrayList<Integer>();
                // 检查是否有输入网址，有的话把网址替换样式
                urlMatcher.reset(momentText);
                while (topicMatcher.find()) {
                    startIndexs.add(topicMatcher.start());
                    endIndexs.add(topicMatcher.start() + topicMatcher.group().length());
                }
                List<Integer> startIndexsUrl = new ArrayList<Integer>();
                List<Integer> endIndexsUrl = new ArrayList<Integer>();
                while (urlMatcher.find()) {
                    startIndexsUrl.add(urlMatcher.start());
                    endIndexsUrl.add(urlMatcher.start() + urlMatcher.group().length());
                }
                if (!startIndexs.isEmpty() || !startIndexsUrl.isEmpty()) {
                    // 创建一个 SpannableString对象
                    SpannableString sp = new SpannableString(momentText);
                    for (int i = 0; i < startIndexs.size(); i++) {
                        // 设置高亮样式
                        sp.setSpan(new TopicClickSpan(sp.subSequence(startIndexs.get(i), endIndexs.get(i))), startIndexs.get(i), endIndexs.get(i), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                    }
                    for (int i = 0; i < startIndexsUrl.size(); i++) {
                        final String url = momentText.substring(startIndexsUrl.get(i), endIndexsUrl.get(i));
                        sp.setSpan(new ImageSpan(TheLApp.getContext(), MomentUtils.createUrlDrawble()), startIndexsUrl.get(i), endIndexsUrl.get(i), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        sp.setSpan(new ClickableSpan() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(TheLApp.getContext(), WebViewActivity.class);
                                intent.putExtra(BundleConstants.URL, url);
                                intent.putExtra("title", "");
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                TheLApp.getContext().startActivity(intent);
                            }
                        }, startIndexsUrl.get(i), endIndexsUrl.get(i), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    }
                    // SpannableString对象设置给TextView
                    moment_content_text.setText(sp);
                    moment_content_text.setMovementMethod(LinkMovementMethod.getInstance());
                } else {
                    moment_content_text.setText(momentText);
                    moment_content_text.setMovementMethod(LinkMovementMethod.getInstance());
                }
            } else {
                moment_content_text.setVisibility(GONE);
            }

            // 参与话题日志，要显示根话题日志区域
            final MomentParentBean parentMomentBean = momentBean.parentMoment;
            if (MomentsBean.MOMENT_TYPE_THEME_REPLY.equals(momentBean.momentsType)) {
                if (!parentMomentBean.momentsId.equals("0") && parentMomentBean.deleteFlag == 0) {
                    lin_participate_theme.setVisibility(VISIBLE);
                    txt_theme_title.setVisibility(VISIBLE);
                    txt_theme_desc.setVisibility(VISIBLE);
                    txt_deleted.setVisibility(GONE);
                    img.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(parentMomentBean.imgUrl, TheLConstants.ICON_MIDDLE_SIZE, TheLConstants.ICON_MIDDLE_SIZE))).build()).setAutoPlayAnimations(true).build());
                    txt_theme_title.setText(parentMomentBean.momentsText);
                    txt_theme_desc.setText(MomentUtils.buildDesc(parentMomentBean.joinTotal));
                    lin_participate_theme.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
//                            Intent intent = new Intent(RecommendMomentInfoActivity.this, ThemeDetailActivity.class);
//                            intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, parentMomentBean.momentsId + "");
//                            startActivity(intent);
                            FlutterRouterConfig.Companion.gotoThemeDetails(parentMomentBean.momentsId);
                        }
                    });
                } else if (parentMomentBean.deleteFlag == 1) {
                    lin_participate_theme.setVisibility(VISIBLE);
                    txt_theme_title.setVisibility(GONE);
                    txt_theme_desc.setVisibility(GONE);
                    txt_deleted.setVisibility(VISIBLE);
                    img.setImageURI(Uri.parse(TheLConstants.RES_PIC_URL + R.mipmap.icn_topic_delete));
                } else {
                    lin_participate_theme.setVisibility(GONE);
                }
            } else {
                lin_participate_theme.setVisibility(GONE);
            }
            recommend_web_view.setVisibility(GONE);
            live_user_liveview.setVisibility(GONE);
            if (MomentsBean.MOMENT_TYPE_USER_CARD.equals(momentBean.momentsType)) {// 推荐用户卡片日志
                rel_user_card.setVisibility(VISIBLE);
                moment_content.setVisibility(GONE);
                //                img_user_card_avatar.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(momentBean.cardAvatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE)));

                ImageLoaderManager.imageLoaderDefaultCircle(img_user_card_avatar, R.mipmap.icon_user, momentBean.cardAvatar, SizeUtils.dip2px(img_user_card_avatar.getContext(), 54), SizeUtils.dip2px(img_user_card_avatar.getContext(), 54));

                txt_user_card_nickname.setText(momentBean.cardNickName);
                txt_user_card_intro.setText(momentBean.cardIntro.replaceAll("\n", " "));
                txt_user_card_info.setText(MomentUtils.buildUserCardInfoStr(momentBean.cardAge, momentBean.cardAffection, momentBean.cardHeight, momentBean.cardWeight));
                mask_user_card.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray_mask));
                if (!TextUtils.isEmpty(momentBean.imageUrl)) {// 被推荐用户可能不带背景图，所以要显示默认背景
                    ImageLoaderManager.imageLoader(img_user_card, momentBean.imageUrl, TheLConstants.AVATAR_SMALL_SIZE, TheLConstants.AVATAR_SMALL_SIZE);
                } else {
                    ImageLoaderManager.imageLoader(img_user_card, R.mipmap.bg_topic_default);
                }
                rel_user_card.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        jumpToUserInfo(momentBean.cardUserId + "");
                    }
                });
            } else if (MomentsBean.MOMENT_TYPE_WEB.equals(momentBean.momentsType)) { //4.0.0.-推荐网页
                recommend_web_view.initView(momentBean);

            } else if (MomentsBean.MOMENT_TYPE_LIVE_USER.equals(momentBean.momentsType)) {//4.0.0推荐主播
                live_user_liveview.initView(momentBean);
            } else {// 其他日志(推荐日志按照普通文本日志处理)
                moment_content.setVisibility(VISIBLE);
                rel_user_card.setVisibility(GONE);
                rel_video.setVisibility(GONE);// 先隐藏视频区域
                img_live.setVisibility(GONE);
                lin_live_status.setVisibility(GONE);
                if (!TextUtils.isEmpty(momentBean.thumbnailUrl)) {// moment图片内容，包括视频\直播
                    if (!TextUtils.isEmpty(momentBean.videoUrl)) {
                        moment_content_pic.setVisibility(GONE);
                        rel_video.setVisibility(VISIBLE);
                        img_cover.setVisibility(VISIBLE);
                        img_play_video.setVisibility(VISIBLE);
                        progressbar_video.setVisibility(GONE);
                        player.setVisibility(GONE);
                        txt_play_times.setVisibility(View.VISIBLE);
                        String text;
                        if (momentBean.playTime > 0) {
                            text = getString(R.string.n_second, Utils.formatTime(momentBean.playTime)) + " / " + getString(R.string.play_count, momentBean.playCount);
                        } else {
                            text = getString(R.string.play_count, momentBean.playCount);
                        }
                        txt_play_times.setText(text);
                        img_cover.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(momentBean.thumbnailUrl, TheLConstants.MOMENT_PIC_BIG_SIZE, TheLConstants.MOMENT_PIC_BIG_SIZE)));
                        rel_video.setOnClickListener(this);
                        if (VideoUtils.willAutoPlayVideo()) {
                            startPlayback();
                        }
                    } else {
                        if (MomentsBean.MOMENT_TYPE_LIVE.equals(momentBean.momentsType) && !TextUtils.isEmpty(momentBean.liveId)) {
                            img_live.setVisibility(VISIBLE);
                            lin_live_status.setVisibility(VISIBLE);
                            if (momentBean.liveStatus != -1) {
                                txt_livestatus.setText(TheLApp.getContext().getString(R.string.now_watching, momentBean.liveStatus));
                            } else {
                                txt_livestatus.setText(R.string.live_ended);
                            }
                        }
                        String[] picUrls = momentBean.thumbnailUrl.split(",");
                        if (picUrls.length > 0) {
                            if (picUrls.length == 1) {
                                pic.setVisibility(VISIBLE);
                                pic.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(picUrls[0], TheLConstants.MOMENT_PIC_BIG_SIZE, TheLConstants.MOMENT_PIC_BIG_SIZE)));
                            } else {
                                pic.setVisibility(GONE);
                                pic1.setVisibility(VISIBLE);
                                pic1.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(picUrls[0], TheLConstants.MOMENT_PIC_BIG_SIZE, TheLConstants.MOMENT_PIC_BIG_SIZE)));
                                pic2.setVisibility(VISIBLE);
                                pic2.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(picUrls[1], TheLConstants.MOMENT_PIC_SMALL_SIZE, TheLConstants.MOMENT_PIC_SMALL_SIZE)));
                                if (picUrls.length > 2) {
                                    pic3.setVisibility(VISIBLE);
                                    pic3.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(picUrls[2], TheLConstants.MOMENT_PIC_SMALL_SIZE, TheLConstants.MOMENT_PIC_SMALL_SIZE)));
                                    if (picUrls.length > 3) {
                                        pic4.setVisibility(VISIBLE);
                                        pic4.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(picUrls[3], TheLConstants.MOMENT_PIC_SMALL_SIZE, TheLConstants.MOMENT_PIC_SMALL_SIZE)));
                                        if (picUrls.length > 4) {
                                            pic5.setVisibility(VISIBLE);
                                            pic5.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(picUrls[4], TheLConstants.MOMENT_PIC_SMALL_SIZE, TheLConstants.MOMENT_PIC_SMALL_SIZE)));
                                            if (picUrls.length > 5) {
                                                pic6.setVisibility(VISIBLE);
                                                pic6.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(picUrls[5], TheLConstants.MOMENT_PIC_SMALL_SIZE, TheLConstants.MOMENT_PIC_SMALL_SIZE)));
                                            }
                                        }
                                    }
                                }
                                setPicsLayoutParams(picUrls.length);
                            }
                        }
                        moment_content_pic.setVisibility(VISIBLE);
                    }
                } else {
                    moment_content_pic.setVisibility(GONE);
                }
            }
        }

        // mentioned
        if (momentBean.atUserList.size() > 0) {
            moment_mentioned_text.setText(MomentUtils.getMetionedShowString(momentBean.atUserList, momentBean.momentsId));
            moment_mentioned_text.setMovementMethod(LinkMovementMethod.getInstance());
            moment_mentioned.setVisibility(VISIBLE);
        } else {
            moment_mentioned.setVisibility(GONE);
        }

        // 音乐
        if (momentBean.songId != 0) {
            moment_content_music.setVisibility(VISIBLE);
            moment_content_music.setOnClickListener(this);
            moment_content_music.setTag(R.id.song_id_tag, momentBean.songId);
            moment_content_music.setTag(R.id.moment_id_tag, momentBean.momentsId);
            //            if (!TextUtils.isEmpty(XiamiSDKUtil.getInstance().songMomentId) && XiamiSDKUtil.getInstance().songMomentId.equals(momentBean.momentsId) && XiamiSDKUtil.getInstance().isPlaying()) {
            //                img_play.setImageResource(R.mipmap.btn_feed_pause_big);
            //            } else {
            //                img_play.setImageResource(R.mipmap.btn_feed_play_big);
            //            }
            if (momentBean.albumLogo444 != null)
                moment_content_music_pic.setImageURI(Uri.parse(momentBean.albumLogo444));
            song_name.setText(momentBean.songName);
            album_name.setText(getString(R.string.moments_album) + "《" + momentBean.albumName + "》");
            artist_name.setText(getString(R.string.moments_artist) + " " + momentBean.artistName);
        } else {
            moment_content_music.setVisibility(GONE);
        }

        mLikes.clear();
        mLikes.addAll(momentBean.winkUserList);
        updateLikeStatus(-1);
        updateCommentNum();

        /**是否显示关注按钮*/
        if (momentBean.followerStatus == 0 && momentBean.myself == 0 && momentBean.secret == MomentsBean.IS_NOT_SECRET) {
            txt_follow.setVisibility(VISIBLE);
        } else {
            txt_follow.setVisibility(GONE);
        }
        setLatestComment();
    }

    private void setLatestComment() {
        lin_latest_comments.removeAllViews();
        List<CommentBean> commentBeans = momentBean.commentList;
        if (commentBeans.size() > 0)
            lin_latest_comments.setVisibility(View.VISIBLE);
        else
            lin_latest_comments.setVisibility(GONE);
        for (int i = 0; i < commentBeans.size(); i++) {
            LatestCommentView latestCommentView = new LatestCommentView(this);
            if (i > 0) {
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.topMargin = Utils.dip2px(TheLApp.getContext(), 5);
                latestCommentView.setLayoutParams(params);
            }
            final String text;
            if (CommentBean.COMMENT_TYPE_STICKER.equals(commentBeans.get(i).commentType))
                text = TheLApp.getContext().getString(R.string.message_info_sticker);
            else
                text = commentBeans.get(i).commentText;
            latestCommentView.setText(commentBeans.get(i).nickname, text, commentBeans.get(i).userId + "", momentBean.momentsId, MomentsBean.MOMENT_TYPE_THEME.equals(momentBean.momentsType) ? "theme" : "moment");
            latestCommentView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    ((LatestCommentView) v).disableSpanClick();
                    DialogUtil.getInstance().showSelectionDialog(RecommendMomentInfoActivity.this, new String[]{TheLApp.getContext().getString(R.string.info_copy)}, new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            DialogUtil.getInstance().closeDialog();
                            switch (position) {
                                case 0:// 复制
                                    DeviceUtils.copyToClipboard(TheLApp.getContext(), text);
                                    break;

                                default:
                                    break;
                            }
                        }
                    }, false, 2, null);
                    return true;
                }
            });
            lin_latest_comments.addView(latestCommentView);
        }
    }

    /**
     * 当图片数大于1张时，要分2~6张不同的数量展示不同的布局
     *
     * @param count
     */
    private void setPicsLayoutParams(int count) {
        float photoMargin = TheLApp.getContext().getResources().getDimension(R.dimen.moment_list_photo_margin);
        int bigSize = (int) (AppInit.displayMetrics.widthPixels - TheLApp.getContext().getResources().getDimension(R.dimen.moment_list_margin) * 2);
        int smallSize = (int) (bigSize - photoMargin) / 2;
        int tinySize = (int) ((bigSize - photoMargin * 2) / 3);
        if (count == 2 || count == 5) {
            final RelativeLayout.LayoutParams smallSizeParamsLeft = new RelativeLayout.LayoutParams(smallSize, smallSize);
            final RelativeLayout.LayoutParams smallSizeParamsRight = new RelativeLayout.LayoutParams(smallSize, smallSize);
            smallSizeParamsRight.leftMargin = (int) photoMargin;
            smallSizeParamsRight.addRule(RelativeLayout.RIGHT_OF, R.id.pic1);
            pic1.setLayoutParams(smallSizeParamsLeft);
            pic2.setLayoutParams(smallSizeParamsRight);
            if (count == 5) {
                final RelativeLayout.LayoutParams params3 = new RelativeLayout.LayoutParams(tinySize, tinySize);
                params3.addRule(RelativeLayout.BELOW, R.id.pic1);
                params3.topMargin = (int) photoMargin;
                pic3.setLayoutParams(params3);
                final RelativeLayout.LayoutParams params4 = new RelativeLayout.LayoutParams(tinySize, tinySize);
                params4.addRule(RelativeLayout.BELOW, R.id.pic1);
                params4.addRule(RelativeLayout.RIGHT_OF, R.id.pic3);
                params4.topMargin = (int) photoMargin;
                params4.leftMargin = (int) photoMargin;
                pic4.setLayoutParams(params4);
                final RelativeLayout.LayoutParams params5 = new RelativeLayout.LayoutParams(tinySize, tinySize);
                params5.addRule(RelativeLayout.BELOW, R.id.pic1);
                params5.addRule(RelativeLayout.RIGHT_OF, R.id.pic4);
                params5.topMargin = (int) photoMargin;
                params5.leftMargin = (int) photoMargin;
                pic5.setLayoutParams(params5);
            }
        } else if (count == 3) {
            final RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, smallSize);
            pic1.setLayoutParams(params1);
            final RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(smallSize, smallSize);
            params2.topMargin = (int) photoMargin;
            params2.addRule(RelativeLayout.BELOW, R.id.pic1);
            final RelativeLayout.LayoutParams params3 = new RelativeLayout.LayoutParams(smallSize, smallSize);
            params3.leftMargin = (int) photoMargin;
            params3.topMargin = (int) photoMargin;
            params3.addRule(RelativeLayout.BELOW, R.id.pic1);
            params3.addRule(RelativeLayout.RIGHT_OF, R.id.pic2);
            pic2.setLayoutParams(params2);
            pic3.setLayoutParams(params3);
        } else if (count == 4) {
            final RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(smallSize, smallSize);
            pic1.setLayoutParams(params1);
            final RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(smallSize, smallSize);
            params2.leftMargin = (int) photoMargin;
            params2.addRule(RelativeLayout.RIGHT_OF, R.id.pic1);
            pic2.setLayoutParams(params2);
            final RelativeLayout.LayoutParams params3 = new RelativeLayout.LayoutParams(smallSize, smallSize);
            params3.topMargin = (int) photoMargin;
            params3.addRule(RelativeLayout.BELOW, R.id.pic1);
            pic3.setLayoutParams(params3);
            final RelativeLayout.LayoutParams params4 = new RelativeLayout.LayoutParams(smallSize, smallSize);
            params4.topMargin = (int) photoMargin;
            params4.leftMargin = (int) photoMargin;
            params4.addRule(RelativeLayout.BELOW, R.id.pic2);
            params4.addRule(RelativeLayout.RIGHT_OF, R.id.pic3);
            pic4.setLayoutParams(params4);
        } else if (count == 6) {
            int pic1Size = (int) (tinySize * 2 + photoMargin);
            final RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(pic1Size, pic1Size);
            pic1.setLayoutParams(params1);
            final RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(tinySize, tinySize);
            params2.addRule(RelativeLayout.RIGHT_OF, R.id.pic1);
            params2.leftMargin = (int) photoMargin;
            pic2.setLayoutParams(params2);
            final RelativeLayout.LayoutParams params3 = new RelativeLayout.LayoutParams(tinySize, tinySize);
            params3.addRule(RelativeLayout.RIGHT_OF, R.id.pic1);
            params3.addRule(RelativeLayout.BELOW, R.id.pic2);
            params3.leftMargin = (int) photoMargin;
            params3.topMargin = (int) photoMargin;
            pic3.setLayoutParams(params3);
            final RelativeLayout.LayoutParams params4 = new RelativeLayout.LayoutParams(tinySize, tinySize);
            params4.addRule(RelativeLayout.BELOW, R.id.pic1);
            params4.topMargin = (int) photoMargin;
            pic4.setLayoutParams(params4);
            final RelativeLayout.LayoutParams params5 = new RelativeLayout.LayoutParams(tinySize, tinySize);
            params5.addRule(RelativeLayout.RIGHT_OF, R.id.pic4);
            params5.addRule(RelativeLayout.BELOW, R.id.pic1);
            params5.leftMargin = (int) photoMargin;
            params5.topMargin = (int) photoMargin;
            pic5.setLayoutParams(params5);
            final RelativeLayout.LayoutParams params6 = new RelativeLayout.LayoutParams(tinySize, tinySize);
            params6.addRule(RelativeLayout.RIGHT_OF, R.id.pic5);
            params6.addRule(RelativeLayout.BELOW, R.id.pic1);
            params6.leftMargin = (int) photoMargin;
            params6.topMargin = (int) photoMargin;
            pic6.setLayoutParams(params6);
        }


    }


    /**
     * @param type 0: 分享链接  1: 分享海报
     */
    private void share(int type) {
        if (type == 0) {
            BusinessUtils.shareMoment(this, new DialogUtils(), momentBean, GrowingIoConstant.ENTRY_RECOMMEND_PAGE);
        } else {
            if (momentBean != null) {
                SharePosterBean sharePosterBean = new SharePosterBean();
                sharePosterBean.avatar = momentBean.avatar;
                sharePosterBean.momentsText = momentBean.momentsText;
                sharePosterBean.imageUrl = momentBean.imageUrl;
                sharePosterBean.nickname = momentBean.nickname;
                sharePosterBean.userName = momentBean.userName;
                sharePosterBean.from = MomentPosterActivity.FROM_MOMENT;
                MomentPosterActivity.gotoShare(sharePosterBean);
            }
        }
    }


    private void likeMoment() {
        moment_opts_like.check();
        //取消点赞声音
        //    BusinessUtils.playSound(R.raw.sound_emoji);

        MomentUtils.likeMoment(momentBean.momentsId);
    }

    private void stopPlayback() {
        if (player != null) {
            try {
                player.seekTo(0);
                if (player.isPlaying())
                    player.stopPlayback();
            } catch (Exception e) {
                e.printStackTrace();
            }
            player.setVisibility(GONE);
            progressbar_video.setVisibility(GONE);
            txt_play_times.setVisibility(View.VISIBLE);
            img_play_video.setVisibility(VISIBLE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(this.getClass().getName());
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(this.getClass().getName());
        MobclickAgent.onPause(this);
        stopPlayback();
    }

    private void reportThemeComment() {
        //        Intent intent = new Intent(this, ReportActivity.class);
        //        intent.putExtra(TheLConstants.BUNDLE_KEY_REPORT_TYPE, ReportActivity.REPORT_TYPE_ABUSE);
        //        intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, operateComment.momentsId);
        //        startActivityForResult(intent, TheLConstants.BUNDLE_CODE_THEME_DETAIL_FRAGMENT);

    }

    private void likeMoment(String momentsId) {
        BusinessUtils.playSound(R.raw.sound_emoji);
        MomentUtils.likeMoment(momentsId);
        likeMomentOrNot(momentsId);
    }


    private void unlikeMoment(String momentsId) {
        MomentUtils.unwinkMoment(momentsId);
        likeMomentOrNot(momentsId);
    }

    private void likeMomentOrNot(String momentsId) {
        if (adapter != null && recyclerView != null) {
            if (momentsId != null) {
                for (int i = 0; i < list.size(); i++) {
                    final MomentsBean commentBean = list.get(i);
                    if (momentsId.equals(commentBean.momentsId)) {
                        if (commentBean.winkFlag == 0) {
                            commentBean.winkFlag = 1;
                            commentBean.winkNum += 1;
                           /* if (!ShareFileUtils.getString(ShareFileUtils.ID, "").equals(commentBean.userId + ""))
                                SendMsgUtils.getInstance().sendMessage(MsgUtils.createMsgBean(MsgBean.MSG_TYPE_EMOJI, "", 1, commentBean.userId + "", commentBean.nickname, commentBean.avatar));*/
                        } else {
                            commentBean.winkFlag = 0;
                            commentBean.winkNum -= 1;
                        }
                        adapter.likeMomentOrNot(recyclerView, i);
                        break;
                    }
                }
            }
        }
    }

    private void jumpToCommentPage(String momentsId, boolean showKeyboard) {
        if (MomentUtils.isBlackOrBlock(momentBean.userId + "")) {
            return;
        }
//        Intent intent = new Intent();
//        intent.setClass(TheLApp.getContext(), MomentCommentActivity.class);
//        if (showKeyboard)
//            intent.putExtra("showKeyboard", true);
//        intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, momentsId);
//        startActivityForResult(intent, TheLConstants.BUNDLE_CODE_MOMENTS_ACTIVITY);
        FlutterRouterConfig.Companion.gotoMomentDetails(momentId);
    }

    /**
     * 刷新关注按钮
     */
    private void refreshFollowStatus(int operatePosition, MomentsBean operateComment) {
        operateComment.followerStatus = CommentBean.FOLLOW_TYPE_FOLLOWED;
        adapter.notifyItemChanged(operatePosition + adapter.getHeaderLayoutCount());
    }

    @Override
    protected void onDestroy() {
        try {
            momentBlackUtils.unRegisterReceiver(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    @Override
    public void onFollowStatusChanged(int followStatus, String userId, String nickName, String avatar) {
        super.onFollowStatusChanged(followStatus, userId, nickName, avatar);
        //推荐日志的关注状态
        if (momentBean != null && (momentBean.userId + "").equals(userId)) {
            momentBean.followerStatus = followStatus;
            if (momentBean.followerStatus == 0 && momentBean.myself == 0 && momentBean.secret == MomentsBean.IS_NOT_SECRET) {
                txt_follow.setVisibility(VISIBLE);
                txt_follow.setText(TheLApp.getContext().getString(R.string.userinfo_activity_follow));
                txt_follow.setBackgroundResource(R.drawable.bg_follow_round_button_blue);
            } else {
                txt_follow.setVisibility(View.VISIBLE);
                txt_follow.setText(TheLApp.getContext().getString(R.string.userinfo_activity_followed));
                txt_follow.setBackgroundResource(R.drawable.bg_followed_round_button_gray);
            }
        }
        if (adapter != null) {
            final int count = adapter.getData().size();
            final int headCount = adapter.getHeaderLayoutCount();
            for (int i = 0; i < count; i++) {
                MomentsBean bean = adapter.getItem(i);
                if ((bean.userId + "").equals(userId)) {
                    bean.followerStatus = followStatus;
                    adapter.notifyItemChanged(i + headCount);
                }
            }
        }
    }

    private void uploadVideoCount(String id) {
        HashMap<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_ID, id);
        data.put(RequestConstants.VIDEO_PLAY_COUNT, "1");
        Flowable<BaseDataBean> flowable = DefaultRequestService.createAllRequestService().postVideoPlayCountUpload(MD5Utils.generateSignatureForMap(data));
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
            @Override
            public void onNext(BaseDataBean data) {
                super.onNext(data);
            }
        });
    }

    @Override
    public void onBlackOneMoment(String momentId) {


    }

    @Override
    public void onBlackherMoment(String userId) {
    }

    public class PrivateOrPublicMomentSubscribe extends InterceptorSubscribe<BaseDataBean> {

        private MomentsBean momentsBean;

        private boolean isPrivate;

        public PrivateOrPublicMomentSubscribe(MomentsBean momentsBean) {
            this.momentsBean = momentsBean;
        }

        public void isPrivate(boolean isPrivate) {
            this.isPrivate = isPrivate;
        }

        @Override public void onNext(BaseDataBean data) {
            super.onNext(data);
            if (isPrivate) {
                momentsBean.shareTo = MomentsBean.SHARE_TO_ONLY_ME;
            } else {
                momentsBean.shareTo = MomentsBean.SHARE_TO_ALL;
            }
            adapter.notifyDataSetChanged();
        }
    }
}
