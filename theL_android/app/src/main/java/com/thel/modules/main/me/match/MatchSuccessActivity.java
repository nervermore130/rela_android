package com.thel.modules.main.me.match;

import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.base.BaseAdapter;
import com.thel.constants.TheLConstants;
import com.thel.db.DBUtils;
import com.thel.manager.ChatServiceManager;
import com.thel.modules.main.me.aboutMe.MatchSuccessParticleView;
import com.thel.modules.main.me.bean.LogInfoBean;
import com.thel.modules.main.me.match.eventcollect.collect.MatchLogUtils;
import com.thel.modules.main.messages.ChatActivity;
import com.thel.modules.main.messages.adapter.FastEmojiAdapter;
import com.thel.modules.main.messages.bean.MsgBean;
import com.thel.modules.main.messages.utils.MsgUtils;
import com.thel.ui.decoration.FastMsgDivider;
import com.thel.ui.popupwindow.SharePopupWindow;
import com.thel.ui.widget.MeetAnimLayout;
import com.thel.utils.DateUtils;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SizeUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MatchSuccessActivity extends BaseActivity {

    private final static String TAG = "MatchSuccessActivity";

    public final static String MATCH_SUCCESS_TYPE = "match_success_type";

    public final static String MATCH_SUCCESS_FROM_CHAT = "match_success_from_chat";

    public final static String MATCH_SUCCESS_FROM_CARD_SLIDE = "match_success_from_card_slide";

    @BindView(R.id.match_success_particle_view)
    MatchSuccessParticleView match_success_particle_view;

    @BindView(R.id.match_anim_view)
    MeetAnimLayout match_anim_view;

    @BindView(R.id.share_tv)
    TextView share_ll;

    @BindView(R.id.continue_tv)
    TextView continue_tv;

    @BindView(R.id.rl_chat)
    RelativeLayout chat_tv;

    @BindView(R.id.time_tv)
    TextView time_tv;

    @BindView(R.id.back_iv)
    ImageView back_iv;

    @BindView(R.id.fast_emoji_rv)
    RecyclerView fast_emoji_rv;

    @BindView(R.id.edit_chat_content)
    EditText edit_chat_content;

    @BindView(R.id.root_rl)
    RelativeLayout root_rl;

    private String avatar = "";

    private String nickName = "";

    private String userId = "";

    private String type = MATCH_SUCCESS_FROM_CARD_SLIDE;

    private int defaultSoftInputHeight = Utils.dip2px(TheLApp.getContext(), 200);

    /**
     * 播放成功动画
     */
    int[] src = new int[]{R.mipmap.match_color1, R.mipmap.match_color2, R.mipmap.match_color3, R.mipmap.match_color4, R.mipmap.match_color5};
    private String latitude;
    private String longitude;
    private String pageId;
    private String from_page_id;
    private String from_page;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_match_success);

        ButterKnife.bind(this);

        initView();
        initData();
        setLintener();
        // AndroidBug5497Workaround.assistActivity(this);


        if (type.equals(MATCH_SUCCESS_FROM_CARD_SLIDE)) {
            share_ll.setVisibility(View.VISIBLE);
            chat_tv.setVisibility(View.VISIBLE);
            continue_tv.setVisibility(View.VISIBLE);
            time_tv.setVisibility(View.GONE);
            back_iv.setVisibility(View.GONE);
//            insertMatchMsg();
        }

        if (type.equals(MATCH_SUCCESS_FROM_CHAT)) {
            share_ll.setVisibility(View.INVISIBLE);
            chat_tv.setVisibility(View.INVISIBLE);
            continue_tv.setVisibility(View.INVISIBLE);
            time_tv.setVisibility(View.VISIBLE);
            back_iv.setVisibility(View.VISIBLE);
        }

        match_success_particle_view.initView(src);

        match_success_particle_view.start();

        if (nickName != null) {
            match_anim_view.setData(avatar, nickName);
        }
        String year = DateUtils.getFormatTimeNow("yyyy");
        String month = DateUtils.monthFormat(DateUtils.getFormatTimeNow("MM"));
        String day = DateUtils.getFormatTimeNow("dd");

        try {
            time_tv.setText(month + "." + day + "." + year);

        } catch (Exception e) {

        }

    }

    /**
     * 关闭键盘
     */
    private void dismissKeyboard() {
        // 取消键盘
        ViewUtils.hideSoftInput(this, edit_chat_content);
    }

    /**
     * 显示软键盘
     */
    private void showKeyboard() {
        ViewUtils.showSoftInput(this, edit_chat_content);
    }

    private void setLintener() {
       /* edit_chat_content.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus && edit_chat_content.getText().toString().trim().isEmpty()) {// 输入框失去焦点后隐藏键盘
                    //  dismissKeyboard();
                    fast_emoji_rv.setVisibility(View.VISIBLE);
                    rel_more_bottom.setVisibility(GONE);
                } else {
                    rel_more_bottom.setVisibility(VISIBLE);

                    // showKeyboard();
                    // fast_emoji_rv.setVisibility(View.GONE);
                }
            }
        });*/
        // 输入法键盘完成事件
        edit_chat_content.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    if (!TextUtils.isEmpty(edit_chat_content.getText().toString().trim())) {
                        String matchMsg = edit_chat_content.getText().toString().trim();

                        Intent intent = new Intent(MatchSuccessActivity.this, ChatActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("toUserId", userId);
                        bundle.putString("toName", nickName);
                        bundle.putString("toAvatar", avatar);
                        bundle.putString("MatchMsg", matchMsg);
                        bundle.putString("fromPage", "MatchSuccessActivity");
                        intent.putExtras(bundle);
                        startActivity(intent);
                        ReportMatchLog();
                        finish();
                    }
                    return true;
                }
                return false;
            }
        });

        edit_chat_content.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        /*//输入框触摸的监听事件
        chat_tv.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //【注意：edt_user.getBottom()是指系统软键盘弹出来后的输入框的bottom值，，缺少顶部标题栏的区域高度，而且连续点击后值不变】
                Log.i(TAG, "{initViews}edt_user.getBottom()=" + chat_tv.getBottom());
                //计算相对于Windows的坐标
                int[] locationW = new int[2];
                chat_tv.getLocationInWindow(locationW);
                Log.i(TAG, "{onTouch}locationW=" + locationW[0] + ";" + locationW[1]);
                //计算相对于Screen的坐标
                int[] locationS = new int[2];
                chat_tv.getLocationOnScreen(locationS);
                Log.i(TAG, "{onTouch}locationS=" + locationS[0] + ";" + locationS[1]);
                Log.i(TAG, "{onTouch}edt_user.getMeasuredHeight()=" + chat_tv.getMeasuredHeight());//输入框的高度

                int edtBottom = locationW[1] + chat_tv.getMeasuredHeight();//输入框的底部的Y坐标值== topY + Height;
                showEditRect(chat_tv, edtBottom);
                return false;
            }
        });*/

       /* chat_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                showKeyboard();
                rel_more_bottom.setVisibility(VISIBLE);
                if (!TextUtils.isEmpty(edit_chat_content.getText().toString().trim())) {
                    fast_emoji_rv.setVisibility(View.GONE);
                } else {
                    fast_emoji_rv.setVisibility(View.VISIBLE);

                }
            }
        });*/
        root_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissKeyboard();

            }
        });
        //addLayoutListener(root_rl,chat_tv);

    }

    public void addLayoutListener(final View main, final View scroll) {
        main.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect rect = new Rect();
                main.getWindowVisibleDisplayFrame(rect);
                int mainInvisibleHeight = main.getRootView().getHeight() - rect.bottom;
                if (mainInvisibleHeight > 100) {
                    int[] location = new int[2];
                    scroll.getLocationInWindow(location);
                    int scrollHeight = (location[1] + scroll.getHeight()) - rect.bottom;
                    main.scrollTo(0, scrollHeight);
                } else {
                    main.scrollTo(0, 0);
                }
            }
        });
    }

    private void initView() {
        FastEmojiAdapter fastEmojiAdapter = new FastEmojiAdapter(this, FastEmojiAdapter.MSG_TYPE_IM);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        fast_emoji_rv.setLayoutManager(linearLayoutManager);
        fast_emoji_rv.addItemDecoration(new FastMsgDivider(SizeUtils.dip2px(TheLApp.context, 10)));
        fast_emoji_rv.setAdapter(fastEmojiAdapter);
        fastEmojiAdapter.setOnItemClickListener(new BaseAdapter.OnItemClickListener<String>() {
            @Override
            public void onItemClick(View view, String item, int position) {
                ViewUtils.preventViewMultipleClick(view, 1000);

                Intent intent = new Intent(MatchSuccessActivity.this, ChatActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("toUserId", userId);
                bundle.putString("toName", nickName);
                bundle.putString("toAvatar", avatar);
                bundle.putString("MatchMsg", item);
                bundle.putString("fromPage", "MatchSuccessActivity");
                intent.putExtras(bundle);
                startActivity(intent);

                ReportMatchLog();
                fast_emoji_rv.setVisibility(View.GONE);
                finish();
            }
        });


        /*edit_chat_content.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                View rootview = MatchSuccessActivity.this.getWindow().getDecorView(); // this = activity
                rootview.getWindowVisibleDisplayFrame(r);

                int screenHeight = rootview.getHeight();
                int heightDifference = screenHeight - r.bottom;
                if (heightDifference > 300) {
                    int softHeight = ShareFileUtils.getInt(ShareFileUtils.SOFT_KEYBOARD_HEIGHT, 0);
                    if (softHeight != heightDifference) {// 更新软键盘的高度
                        softHeight = heightDifference;
                        ShareFileUtils.setInt(ShareFileUtils.SOFT_KEYBOARD_HEIGHT, softHeight);
                    }
                    if (softHeight >= defaultSoftInputHeight) {
                        // ll_msg_content.scrollTo(0,softHeight);
                        rel_more_bottom.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, softHeight));
                    } else {
                        // ll_msg_content.scrollTo(0,defaultSoftInputHeight);

                        rel_more_bottom.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, defaultSoftInputHeight));
                    }
                    // 调整表情键盘的上间距
                    *//*  sendEmojiLayout.refreshPaddingTop();
         *//*
                //    hideBottomMore();
                }

            }
        });*/

     /*   int softHeight = ShareFileUtils.getInt(ShareFileUtils.SOFT_KEYBOARD_HEIGHT, 0);
        if (softHeight >= defaultSoftInputHeight) {
            rel_more_bottom.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, softHeight));
        } else {
            rel_more_bottom.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, defaultSoftInputHeight));
        }*/

    }


    private void ReportMatchLog() {
        LogInfoBean logInfoBean = new LogInfoBean();
        logInfoBean.page = "match.ok";
        logInfoBean.page_id = pageId;
        logInfoBean.activity = "chat";
        logInfoBean.from_page = from_page;
        logInfoBean.from_page_id = from_page_id;
        logInfoBean.lat = latitude;
        logInfoBean.lng = longitude;
        MatchLogUtils.getInstance().addLog(logInfoBean);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        match_success_particle_view.stop();
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void insertMatchMsg() {

        L.d(TAG, "-----insertMatchMsg------");
        if (userId != null && nickName != null) {
            final MsgBean msgBean = new MsgBean();
            msgBean.msgType = MsgBean.MSG_TYPE_MATCH;
            msgBean.packetId = MsgUtils.getMsgId();
            msgBean.msgText = TheLApp.getContext().getString(R.string.stranger_warning_tip);
            msgBean.msgTime = System.currentTimeMillis();
            msgBean.msgDirection = "0";
            msgBean.fromUserId = UserUtils.getMyUserId();
            msgBean.fromNickname = UserUtils.getNickName();
            msgBean.fromAvatar = ShareFileUtils.getString(ShareFileUtils.AVATAR, "");
            msgBean.toUserId = String.valueOf(userId);
            msgBean.toUserNickname = nickName;
            msgBean.toAvatar = avatar;
            msgBean.hadRead = 1; // 发出的消息一律为读过
            msgBean.msgStatus = TheLConstants.MsgSendingStatusConstants.MSG_READ;
            ChatServiceManager.getInstance().insertNewMsg(msgBean, DBUtils.getMainProcessChatTableName(String.valueOf(userId)), 1);
        }
    }

    private void initData() {
        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {

            nickName = bundle.getString(TheLConstants.BUNDLE_KEY_NICKNAME);

            userId = bundle.getString(TheLConstants.BUNDLE_KEY_USER_ID);

            avatar = bundle.getString(TheLConstants.BUNDLE_KEY_USER_AVATAR);

            type = bundle.getString(MATCH_SUCCESS_TYPE);

            from_page_id = bundle.getString(ShareFileUtils.MATCH_FROM_PAGE_ID, "");
            from_page = bundle.getString(ShareFileUtils.MATCH_FROM_PAGE, "");

            latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
            longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");
            pageId = Utils.getPageId();
        }

    }

    @OnClick(R.id.tv_send_match_msg)
    void goToChat() {

        String matchMsg = edit_chat_content.getText().toString().trim();
        if (!TextUtils.isEmpty(matchMsg)) {
            Intent intent = new Intent(MatchSuccessActivity.this, ChatActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("toUserId", userId);
            bundle.putString("toName", nickName);
            bundle.putString("toAvatar", avatar);
            bundle.putString("MatchMsg", matchMsg);
            bundle.putString("fromPage", "MatchSuccessActivity");
            intent.putExtras(bundle);
            startActivity(intent);
            ReportMatchLog();
            finish();
        }


    }

  /*  @OnClick(R.id.edit_chat_content)
    void removeFastEmoji() {
        fast_emoji_rv.setVisibility(View.GONE);
    }*/


    @OnClick({R.id.continue_tv, R.id.back_iv})
    void back() {
        Intent intent = new Intent();
        intent.setAction(TheLConstants.BROADCAST_MATCH_LIKE_ME_LIST);
        sendBroadcast(intent);
        finish();

        LogInfoBean logInfoBean = new LogInfoBean();
        logInfoBean.page = "match.ok";
        logInfoBean.page_id = pageId;
        logInfoBean.activity = "match";
        logInfoBean.from_page = from_page;
        logInfoBean.from_page_id = from_page_id;
        logInfoBean.lat = latitude;
        logInfoBean.lng = longitude;
        MatchLogUtils.getInstance().addLog(logInfoBean);
    }

    @OnClick(R.id.share_tv)
    void share() {
        SharePopupWindow sharePopupWindow = new SharePopupWindow(MatchSuccessActivity.this);
        sharePopupWindow.setMatchData(avatar, nickName, TheLApp.context.getString(R.string.share_friends));
        sharePopupWindow.showAtLocation(share_ll, Gravity.BOTTOM, 0, 0);

        LogInfoBean logInfoBean = new LogInfoBean();
        logInfoBean.page = "match.ok";
        logInfoBean.page_id = pageId;
        logInfoBean.activity = "share";
        logInfoBean.from_page = from_page;
        logInfoBean.from_page_id = from_page_id;
        logInfoBean.lat = latitude;
        logInfoBean.lng = longitude;
        MatchLogUtils.getInstance().addLog(logInfoBean);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (match_success_particle_view != null) {
            match_success_particle_view.stop();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (match_success_particle_view != null) {
            match_success_particle_view.start();
        }
    }

    @OnClick(R.id.root_rl)
    void onRootViewClick() {
        if (type.equals(MATCH_SUCCESS_FROM_CHAT)) {
            finish();
        }
    }

    @Override
    public void finish() {
        super.finish();
        dismissKeyboard();

    }
}
