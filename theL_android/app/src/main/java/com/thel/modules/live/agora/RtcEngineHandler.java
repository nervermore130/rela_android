package com.thel.modules.live.agora;

import android.app.Activity;
import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.Log;
import android.view.SurfaceView;
import android.view.ViewGroup;
import android.widget.TextView;

import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UploadManager;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.CameraConfigBean;
import com.thel.bean.LiveInfoLogBean;
import com.thel.bean.user.UploadTokenBean;
import com.thel.data.local.FileHelper;
import com.thel.modules.live.stream.RtcPreviewManager;
import com.thel.modules.live.stream.TuSdkPreviewManager;
import com.thel.modules.live.utils.LiveUtils;
import com.thel.modules.test.UserInfo;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.tusdk.plain.CameraConfig;
import com.thel.utils.L;
import com.thel.utils.ScreenUtils;
import com.thel.utils.UserUtils;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.microedition.khronos.egl.EGLContext;

import io.agora.rtc.Constants;
import io.agora.rtc.IRtcEngineEventHandler;
import io.agora.rtc.RtcEngine;
import io.agora.rtc.live.LiveTranscoding;
import io.agora.rtc.video.AgoraImage;
import io.agora.rtc.video.AgoraVideoFrame;
import io.agora.rtc.video.BeautyOptions;
import io.agora.rtc.video.VideoCanvas;
import io.agora.rtc.video.VideoEncoderConfiguration;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import video.com.relavideolibrary.camera.CameraView;

/**
 * @author liuyun
 */
public class RtcEngineHandler extends IRtcEngineEventHandler {

    private static final String TAG = "RtcEngineHandler";

    /**
     * 视频
     */
    public static final int LIVE_TYPE_VIDEO = 0x01;

    /**
     * 音频
     */
    public static final int LIVE_TYPE_AUDIO = 0x02;

    /**
     *
     */
    public static final int TYPE_DEFAULT = 0x01;

    /**
     * 主播PK
     */
    public static final int TYPE_PK = 0x02;

    /**
     * 主播连麦
     */
    public static final int TYPE_LINK_MIC = 0x03;

    /**
     * 观众连麦
     */
    public static final int TYPE_AUDIENCE_LINK_MIC = 0x04;

    /**
     * 多人连麦连麦
     */
    public static final int TYPE_MULTI_LINK_MIC = 0x05;

    public static final int BG_WIDTH = 540;

    public static final int BG_HEIGHT = 960;

    public static final int BG_PK_WIDTH = 360;

    public static final int BG_PK_HEIGHT = 640;

    private static final int BITRATE = 1200;

    private static final int FPS = 15;

    private static final int BEAUTY_EFFECT_DEFAULT_CONTRAST = 1;

    private static final float BEAUTY_EFFECT_DEFAULT_LIGHTNESS = .7f;

    private static final float BEAUTY_EFFECT_DEFAULT_SMOOTHNESS = .5f;

    private static final float BEAUTY_EFFECT_DEFAULT_REDNESS = .1f;

    private Map<Integer, UserInfo> mUserInfo = new HashMap<>();

    private int mBigUserId = 0;

    private LiveTranscoding mLiveTranscoding;

    private ExecutorService mExecutorService;

    private RtcEngine mRtcEngine;

    private Activity mActivity;

    private CameraView mCameraView;

    private OnJoinChannelListener mOnJoinChannelSuccessListener;

    private String appId;

    private String token;

    private String channel;

    private String pubUrl;

    private boolean isJoinChannel = false;

    private int uid;

    private int type = TYPE_DEFAULT;

    private UploadManager mUploadManager;

    private int live_type;

    private String logUrl;

    private String liveId;

    private GLSurfaceView glSurfaceView;

    private RtcPreviewManager rtcPreviewManager;

    public RtcEngineHandler(Activity activity, int live_type, String appId) {

        this.appId = appId;

        this.live_type = live_type;

        mActivity = activity;

        mExecutorService = Executors.newSingleThreadExecutor();

        initializeAgoraEngine();

        setupVideoProfile();

        initTranscoding();
    }

    public void init(String token, String channel, String pubUrl, int uid, String liveId, GLSurfaceView glSurfaceView) {

        this.glSurfaceView = glSurfaceView;

        init(token, channel, pubUrl, uid, liveId);
    }

    public void init(String token, String channel, String pubUrl, int uid, String liveId) {

        this.token = token;

        this.channel = channel;

        this.pubUrl = pubUrl;

        this.uid = uid;

        this.liveId = liveId;

        if (live_type == LIVE_TYPE_VIDEO) {
            setupLocalVideo(mActivity);
        } else {
            joinChannel(token, channel, uid);
        }
    }


    private void initializeAgoraEngine() {

        L.d(TAG, " initializeAgoraEngine appId : " + appId);

        L.d(TAG, " getSdkVersion : " + io.agora.rtc.RtcEngine.getSdkVersion());

        try {
            mRtcEngine = RtcEngine.create(mActivity, appId, this);

        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));

            throw new RuntimeException("NEED TO check rtc sdk init fatal error\n" + Log.getStackTraceString(e));
        }
        mRtcEngine.setChannelProfile(Constants.CHANNEL_PROFILE_LIVE_BROADCASTING);

    }

    private void setupVideoProfile() {

        L.d(TAG, " setupVideoProfile : ");

        mRtcEngine.enableVideo();

        if (mRtcEngine.isTextureEncodeSupported()) {
            mRtcEngine.setExternalVideoSource(true, true, true);
        } else {
            throw new RuntimeException("Can not work on device do not supporting texture" + mRtcEngine.isTextureEncodeSupported());
        }
//        mRtcEngine.setVideoProfile(Constants.VIDEO_PROFILE_360P, true);

        VideoEncoderConfiguration videoEncoderConfiguration = new VideoEncoderConfiguration();

        videoEncoderConfiguration.bitrate = BITRATE;

        videoEncoderConfiguration.dimensions = new VideoEncoderConfiguration.VideoDimensions(BG_PK_WIDTH, BG_PK_HEIGHT);

        videoEncoderConfiguration.frameRate = FPS;

        mRtcEngine.setVideoEncoderConfiguration(videoEncoderConfiguration);

//        if (L.DEBUG) {
//            mRtcEngine.disableAudio();
//        }

        logUrl = FileHelper.getInstance().getThelPath() + "/agora-rtc.txt";

        File logFile = new File(logUrl);

        if (logFile.exists()) {
            logFile.delete();
        }

        mRtcEngine.setLogFile(logUrl);

    }

    private void setupLocalVideo(Activity ctx) {

        L.d(TAG, " setupLocalVideo ctx : " + ctx.getClass().getSimpleName());

        rtcPreviewManager = new RtcPreviewManager(new CameraConfig(), glSurfaceView);

        rtcPreviewManager.initStreamingManager();

        rtcPreviewManager.onResume();

        rtcPreviewManager.setOnTuSdkListener(new TuSdkPreviewManager.OnTuSdkListener() {
            @Override
            public void onSurfaceCreated() {

                L.d(TAG, " setupLocalVideo onSurfaceCreated : ");

            }

            @Override
            public void onFrameAvailable(EGLContext eglContext, int textureId, int width, int height, float[] transformMatrix) {

                L.d(TAG, " setupLocalVideo onFrameAvailable : ");

                AgoraVideoFrame vf = new AgoraVideoFrame();
                vf.format = AgoraVideoFrame.FORMAT_TEXTURE_2D;
                vf.timeStamp = System.currentTimeMillis();
                vf.stride = height;
                vf.height = width;//涂图textureid传给声网，需要宽高互换一下，否则画面有问题
                vf.textureID = textureId;
                vf.syncMode = true;
                vf.eglContext11 = eglContext;
                vf.transform = transformMatrix;
                if (mRtcEngine != null) {
                    mRtcEngine.pushExternalVideoFrame(vf);
                }
            }

            @Override
            public void onFaceDetection(boolean visible) {

                L.d(TAG, " setupLocalVideo onFaceDetection : " + visible);

            }
        });

        joinChannel(token, channel, uid);
    }

    public void setType(int type) {
        this.type = type;
        if (type == TYPE_MULTI_LINK_MIC) {
            mLiveTranscoding.width = 16;
            mLiveTranscoding.height = 16;
            mLiveTranscoding.videoBitrate = 1;
        } else if (type == TYPE_PK || type == TYPE_LINK_MIC || type == TYPE_AUDIENCE_LINK_MIC) {
            mLiveTranscoding.width = 360;
            mLiveTranscoding.height = 640;
            mLiveTranscoding.videoBitrate = BITRATE;
        } else {
            mLiveTranscoding.width = BG_WIDTH;
            mLiveTranscoding.height = BG_HEIGHT;
            mLiveTranscoding.videoBitrate = BITRATE;
        }
    }

    public int getType() {
        return type;
    }

    public void joinChannel(String token, String channel, int uid) {

        L.d(TAG, " joinChannel : " + "\n token : " + token + " \n channel : " + channel + " \n uid " + uid + " \n logType : " + type);

        if (mRtcEngine != null) {

            mRtcEngine.setClientRole(Constants.CLIENT_ROLE_BROADCASTER);

            // if you do not specify the uid, we will generate the uid for you
            mRtcEngine.joinChannel(token, channel, "", uid);
        }

    }

    public void leaveChannel() {

        L.d(TAG, " leaveChannel : " + isJoinChannel);

        if (mRtcEngine != null) {
            if (pubUrl != null) {
                mRtcEngine.removePublishStreamUrl(pubUrl);
            }
            if (isJoinChannel) {
                mRtcEngine.leaveChannel();
            }
        }

        previewDestroy();

    }

    public void cameraDestroy() {
        if (mCameraView != null) {
            mCameraView.onDestroy();
        }

    }

    private void previewDestroy() {

        if (rtcPreviewManager != null) {
            rtcPreviewManager.onDestroy();
        }
    }

    public void destroy() {

        try {

            RtcEngine.destroy();

            if (rtcPreviewManager != null) {

                rtcPreviewManager.stopMediaStreaming();

                rtcPreviewManager.onDestroy();

            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        mRtcEngine = null;
    }

    public void stopMediaStreaming() {
        if (rtcPreviewManager != null) {

            rtcPreviewManager.stopMediaStreaming();

        }
    }

    public void resume() {
        if (rtcPreviewManager != null) {
            rtcPreviewManager.onResume();
        }
    }

    public void pause() {
        if (rtcPreviewManager != null) {
            rtcPreviewManager.onPause();
        }
    }

    public void beautiful(final boolean enable) {
        mExecutorService.execute(new Runnable() {
            @Override
            public void run() {
                BeautyOptions beautyOptions = new BeautyOptions(BEAUTY_EFFECT_DEFAULT_CONTRAST, BEAUTY_EFFECT_DEFAULT_LIGHTNESS, BEAUTY_EFFECT_DEFAULT_SMOOTHNESS, BEAUTY_EFFECT_DEFAULT_REDNESS);
                mRtcEngine.setBeautyEffectOptions(enable, beautyOptions);
            }
        });
    }

    public void swtichCamera() {
        mExecutorService.execute(new Runnable() {
            @Override
            public void run() {
                mRtcEngine.switchCamera();
            }
        });

    }

    public GLSurfaceView getCameraView() {

        return glSurfaceView;

    }

    public void muteLocalAudioStream(boolean mute) {

        L.d(TAG, " muteLocalAudioStream : " + mute);

        if (mRtcEngine != null) {
            mRtcEngine.muteLocalAudioStream(mute);
        }
    }

    public void playEffect(int soundId, String filePath) {
        if (mRtcEngine != null) {
            mRtcEngine.getAudioEffectManager().playEffect(soundId, filePath, 0, 1.0, 0, 100.0, true);
        }
    }

    public void setAudioProfile(int i, int i1) {

        L.d(TAG, " setAudioProfile i : " + i + " ,i1 : " + i1);

        if (mRtcEngine != null) {
            mRtcEngine.setAudioProfile(i, i1);
        }
    }

    @Override
    public void onError(int err) {
        super.onError(err);

        L.d(TAG, " onError err : " + err);

        if (mOnJoinChannelSuccessListener != null) {
            mOnJoinChannelSuccessListener.onError(err);
        }

    }

    @Override
    public void onRemoteVideoStats(RemoteVideoStats stats) {
        super.onRemoteVideoStats(stats);
    }

    @Override
    public void onJoinChannelSuccess(String channel, final int uid, int elapsed) {
        super.onJoinChannelSuccess(channel, uid, elapsed);

        L.d(TAG, "onJoinChannelSuccess channel: " + channel);

        L.d(TAG, "onJoinChannelSuccess pubUrl: " + pubUrl);

        L.d(TAG, "onJoinChannelSuccess mActivity: " + mActivity);

        isJoinChannel = true;

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mBigUserId = uid;
                UserInfo mUI = new UserInfo();
                mUI.uid = uid;
                mUserInfo.put(mBigUserId, mUI);

                L.d(TAG, "onJoinChannelSuccess mOnJoinChannelSuccessListener: " + mOnJoinChannelSuccessListener);

                if (mOnJoinChannelSuccessListener != null) {
                    mOnJoinChannelSuccessListener.onJoinChannelSuccess();
                }

                setTranscoding();

                if (pubUrl != null && mRtcEngine != null) {

                    mRtcEngine.addPublishStreamUrl(pubUrl, true);

                }


            }
        });

    }

    @Override
    public void onRejoinChannelSuccess(String channel, int uid, int elapsed) {
        super.onRejoinChannelSuccess(channel, uid, elapsed);

        L.d(TAG, " onRejoinChannelSuccess channel: " + channel);

        onJoinChannelSuccess(channel, uid, elapsed);
    }

    @Override
    public void onLeaveChannel(RtcStats stats) {
        super.onLeaveChannel(stats);

        L.d(TAG, " onLeaveChannel ");

        isJoinChannel = false;

        RtcEngine.destroy();

        mOnJoinChannelSuccessListener.onLeaveChannelSuccess();

        String path = "Agora/" + channel + "/" + UserUtils.getMyUserId() + ".log";

        L.d(TAG, " path " + path);

        getUploadToken(channel, logUrl);

    }

    @Override
    public void onUserJoined(final int uid, int elapsed) {
        super.onUserJoined(uid, elapsed);

        L.d(TAG, " onUserJoined uid " + uid);

        L.d(TAG, " onUserJoined RtcEngineHandler.this.uid " + RtcEngineHandler.this.uid);

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (uid != RtcEngineHandler.this.uid) {
                    UserInfo remoteUserInfo = new UserInfo();
                    remoteUserInfo.view = RtcEngine.CreateRendererView(mActivity);
                    remoteUserInfo.view.setZOrderOnTop(true);
                    remoteUserInfo.view.setZOrderMediaOverlay(true);
                    remoteUserInfo.uid = uid;
                    mUserInfo.put(uid, remoteUserInfo);

                    if (mOnJoinChannelSuccessListener != null) {
                        mOnJoinChannelSuccessListener.onUserJoined(uid, remoteUserInfo.view);
                    }

                    L.d(TAG, " onUserJoined live_type " + live_type);

                    if (live_type == LIVE_TYPE_VIDEO) {
                        mRtcEngine.setupRemoteVideo(new VideoCanvas(remoteUserInfo.view, Constants.RENDER_MODE_HIDDEN, uid));
                    }

                    setTranscoding();

                }
            }
        });
    }

    @Override
    public void onUserOffline(final int uid, int reason) { // Tutorial Step 7
        Log.d(TAG, "onUserOffline");
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mUserInfo.remove(uid);
                setTranscoding();

                if (mOnJoinChannelSuccessListener != null) {
                    mOnJoinChannelSuccessListener.onUserOffline(uid);
                }

            }
        });
    }

    @Override
    public void onRtmpStreamingStateChanged(String url, int state, int errCode) {
        super.onRtmpStreamingStateChanged(url, state, errCode);

        if (errCode != 0) {
            LiveInfoLogBean.getInstance().getAgoraAnalytics().userId = UserUtils.getMyUserId();
            LiveInfoLogBean.getInstance().getAgoraAnalytics().errorReason = String.valueOf(errCode);
            LiveInfoLogBean.getInstance().getAgoraAnalytics().channelId = channel;
            LiveInfoLogBean.getInstance().getAgoraAnalytics().logType = "agoraPushFail";
            LiveInfoLogBean.getInstance().getAgoraAnalytics().cdnDomain = url;
            LiveInfoLogBean.getInstance().getAgoraAnalytics().roomId = liveId;
            LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getAgoraAnalytics());
        }

        //当错误码为1， 10， 20，154时，进行重推
        //当错误码为0，19时，认为推流OK，不处理
        //当错误码为151时，需要更换流地址重推
        //当错误码为152时，删掉前面不打算使用的流，再增加推流地址  （监控显示：初生没有这种异常）
        //当错误码为156，153时，app逻辑有错误，请自检（初生也没有这种异常）
        //当错误码为157时 转码服务器与 CDN 完全断开连接，推流中断。
        if (pubUrl != null && mRtcEngine != null) {
            if (errCode == 1 || errCode == 10 || errCode == 20 || errCode == 154) {
                mRtcEngine.addPublishStreamUrl(pubUrl, true);
            } else if (errCode == 151) {
                //151重试不要超过三次，避免死循环，手动退出
                mRtcEngine.removePublishStreamUrl(pubUrl);
                setTranscoding();
                mRtcEngine.addPublishStreamUrl(pubUrl, true);
            }
        }

    }

    @Override
    public void onStreamMessageError(int uid, int streamId, int error, int missed, int cached) {
        super.onStreamMessageError(uid, streamId, error, missed, cached);

        L.d(TAG, " onStreamMessageError uid : " + uid + " ,streamId : " + streamId + " ,error : " + error + " ,missed : " + missed + " ,cached : " + cached);
    }

    @Override
    public void onAudioVolumeIndication(AudioVolumeInfo[] speakers, int totalVolume) {
        super.onAudioVolumeIndication(speakers, totalVolume);
        if (mOnJoinChannelSuccessListener != null) {
            mOnJoinChannelSuccessListener.onAudioVolumeIndication(speakers, totalVolume);
        }
    }

    public void setTranscoding() {

        L.d(TAG, " TranscodingUser setTranscoding logType : " + type);


        if (mRtcEngine != null && mLiveTranscoding != null) {

            ArrayList<UserInfo> videoUsers = getAllVideoUser(mUserInfo);

            ArrayList<LiveTranscoding.TranscodingUser> transCodingUsers = cdnLayout(mBigUserId, videoUsers);

            for (LiveTranscoding.TranscodingUser transCodingUser : transCodingUsers) {

                mLiveTranscoding.addUser(transCodingUser);

            }

            mLiveTranscoding.backgroundImage = getAgoraImage();

            mRtcEngine.setLiveTranscoding(mLiveTranscoding);
        }

        L.d(TAG, "onJoinChannelSuccess mUserInfo: " + mUserInfo.toString());

    }

    private void initTranscoding() {
        if (mLiveTranscoding == null) {
            mLiveTranscoding = new LiveTranscoding();
            mLiveTranscoding.videoFramerate = FPS;//15
            mLiveTranscoding.width = BG_WIDTH;//540
            mLiveTranscoding.height = BG_HEIGHT;//960
            mLiveTranscoding.videoBitrate = BITRATE;//1200
        }
    }

    private AgoraImage getAgoraImage() {

        AgoraImage agoraImage = new AgoraImage();

        agoraImage.width = BG_WIDTH;

        agoraImage.height = BG_HEIGHT;

        agoraImage.url = "https://static.rela.me/h5/pk_bg.jpg";

        agoraImage.x = 0;

        agoraImage.y = 0;

        return agoraImage;
    }

    private ArrayList<UserInfo> getAllVideoUser(Map<Integer, UserInfo> mUserInfo) {
        ArrayList<UserInfo> users = new ArrayList<>();
        for (Map.Entry<Integer, UserInfo> entry : mUserInfo.entrySet()) {
            UserInfo user = entry.getValue();
            users.add(user);
        }
        return users;
    }

    private ArrayList<LiveTranscoding.TranscodingUser> cdnLayout(int bigUserId, ArrayList<UserInfo> publishers) {

        ArrayList<LiveTranscoding.TranscodingUser> users = new ArrayList<>(publishers.size());

        users.add(getDefaultUser(bigUserId));

        for (UserInfo userInfo : publishers) {

            if (userInfo.uid == bigUserId) {
                continue;
            }

            users.add(getRemoteUser(userInfo));
        }

        return users;
    }


    public void setOnJoinChannelSuccessListener(OnJoinChannelListener mOnJoinChannelSuccessListener) {
        this.mOnJoinChannelSuccessListener = mOnJoinChannelSuccessListener;
    }

    public void enableAudioVolumeIndication(int i, int i1) {

        if (mRtcEngine != null) {
            mRtcEngine.enableAudioVolumeIndication(300, 3, true);
        }

    }

    private LiveTranscoding.TranscodingUser getDefaultUser(int bigUserId) {

        LiveTranscoding.TranscodingUser defaultUser = new LiveTranscoding.TranscodingUser();

        defaultUser.uid = bigUserId;

        defaultUser.alpha = 1;

        defaultUser.zOrder = 1;

        defaultUser.audioChannel = 0;

        switch (type) {

            case TYPE_DEFAULT:

                defaultUser.x = 0;

                defaultUser.y = 0;

                defaultUser.width = BG_WIDTH;

                defaultUser.height = BG_HEIGHT;

                break;
            case TYPE_PK:
            case TYPE_LINK_MIC:
            case TYPE_AUDIENCE_LINK_MIC:

                final float[] pkParams = LiveUtils.getPkParams(BG_PK_WIDTH, BG_PK_HEIGHT);

                defaultUser.x = 0;

                defaultUser.y = (int) (pkParams[1] / ScreenUtils.getScreenHeight(TheLApp.context) * BG_PK_HEIGHT);

                defaultUser.width = (int) pkParams[2];

                defaultUser.height = (int) pkParams[3];
                break;
            case TYPE_MULTI_LINK_MIC:
                defaultUser.width = 1;

                defaultUser.height = 1;

                defaultUser.x = 0;

                defaultUser.y = 0;
                break;
            default:
                break;
        }

        return defaultUser;
    }

    private LiveTranscoding.TranscodingUser getRemoteUser(UserInfo userInfo) {

        LiveTranscoding.TranscodingUser remoteUser = new LiveTranscoding.TranscodingUser();

        remoteUser.uid = userInfo.uid;

        remoteUser.zOrder = 2;

        remoteUser.audioChannel = 0;

        remoteUser.alpha = 1f;

        switch (type) {

            case TYPE_DEFAULT:

                remoteUser.x = 0;

                remoteUser.y = 0;

                remoteUser.width = BG_WIDTH;

                remoteUser.height = BG_HEIGHT;

                break;
            case TYPE_PK:
            case TYPE_LINK_MIC:
            case TYPE_AUDIENCE_LINK_MIC:
                final float[] pkParams = LiveUtils.getPkParams(BG_PK_WIDTH, BG_PK_HEIGHT);

                remoteUser.x = (int) pkParams[0];

                remoteUser.y = (int) (pkParams[1] / ScreenUtils.getScreenHeight(TheLApp.context) * BG_PK_HEIGHT);

                remoteUser.width = (int) pkParams[2];

                remoteUser.height = (int) pkParams[3];

                break;
            case TYPE_MULTI_LINK_MIC:
                remoteUser.width = 1;

                remoteUser.height = 1;

                remoteUser.x = 0;

                remoteUser.y = 0;

                break;
            default:
                break;
        }

        return remoteUser;
    }

    private void pushLogFile(String path, String localFilePath, String token) {

        if (localFilePath == null) {
            return;
        }

        if (mUploadManager == null) {
            mUploadManager = new UploadManager();
        }

        File file = new File(localFilePath);

        mUploadManager.put(file, path, token, new UpCompletionHandler() {
            @Override
            public void complete(String key, ResponseInfo info, JSONObject response) {

                if (info != null && info.statusCode == 200) {
                    L.d(TAG, "声网日志文件上传成功");
                } else {
                    L.d(TAG, "声网日志文件上传失败");
                }
            }
        }, null);
    }

    private void getUploadToken(final String path, final String localFilePath) {


        Flowable<UploadTokenBean> flowable = RequestBusiness.getInstance().getUploadToken(String.valueOf(System.currentTimeMillis()), "rela-agora-log", path);

        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<UploadTokenBean>() {

            @Override
            public void onNext(UploadTokenBean uploadTokenBean) {
                super.onNext(uploadTokenBean);

                if (uploadTokenBean != null && uploadTokenBean.data != null && uploadTokenBean.data.uploadToken != null) {

                    L.d(TAG, " onNext getUploadToken : " + uploadTokenBean.data.uploadToken);

                    pushLogFile(path, localFilePath, uploadTokenBean.data.uploadToken);
                }

            }

            @Override
            public void onError(Throwable t) {

                L.d(TAG, " onError getUploadToken : " + t.getMessage());

            }

        });
    }

}