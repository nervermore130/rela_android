package com.thel.modules.preview_image;

import android.content.Context;
import android.net.Uri;

import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.PagerAdapter;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.drawable.ProgressBarDrawable;
import com.facebook.drawee.drawable.ScalingUtils;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.generic.GenericDraweeHierarchyBuilder;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.ImgShareBean;
import com.thel.bean.user.MyImageBean;
import com.thel.bean.user.UserInfoPicBean;
import com.thel.ui.widget.zoom.VideoAndPhotoItemView;
import com.thel.utils.ImageUtils;
import com.thel.utils.Utils;

import java.util.ArrayList;

/**
 * 上传相册图片gridview的适配器
 *
 * @author setsail
 */
public class AlbumPhotosPagerAdapter extends PagerAdapter {

    private final Context mContext;
    private final String mRela_id;
    private final boolean mIs_watermark;
    private final boolean backTransparent;//背景是否设为透明
    private final ImgShareBean mImageShareBean;
    private String fromPage;
    private String mUserId;
    private UserInfoPicBean mUserinfoPicbean;
    private VideoAndPhotoItemView.DragListener dragListener;
    private int currentPosition = -1;
    private ArrayList<String> imageUrllist = new ArrayList<>();

    private LayoutInflater mInflater;
    private boolean isFirst = true;//是否是刚进来
    private ItemClickListener itemClickListener;

    public AlbumPhotosPagerAdapter(Context context, ArrayList<?> myImageslist, String rela_id, boolean is_watermark, boolean backTransparent, ImgShareBean imageShareBean, VideoAndPhotoItemView.DragListener... dragListener) {
        mContext = context;
        this.mRela_id = rela_id;
        this.mIs_watermark = is_watermark;
        mInflater = (LayoutInflater) TheLApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.backTransparent = backTransparent;
        this.mImageShareBean = imageShareBean;
        if (dragListener.length > 0) {
            this.dragListener = dragListener[0];
        }
        refreshAdapter(myImageslist);
    }

    public AlbumPhotosPagerAdapter(Context context, String fromPage, ArrayList<?> myImageslist, String rela_id, boolean is_watermark, boolean backTransparent, ImgShareBean imageShareBean, UserInfoPicBean userInfoPicBean, String userid, VideoAndPhotoItemView.DragListener... dragListener) {
        mContext = context;
        this.mRela_id = rela_id;
        this.mIs_watermark = is_watermark;
        mInflater = (LayoutInflater) TheLApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.backTransparent = backTransparent;
        this.mImageShareBean = imageShareBean;
        this.mUserinfoPicbean = userInfoPicBean;
        this.mUserId = userid;
        this.fromPage = fromPage;
        if (dragListener.length > 0) {
            this.dragListener = dragListener[0];
        }
        refreshAdapter(myImageslist);
    }

    public void refreshAdapter(ArrayList<?> myImageslist) {
        this.imageUrllist.clear();

        int listSize = myImageslist.size();
        for (int i = 0; i < listSize; i++) {
            if (myImageslist.get(i) instanceof MyImageBean) {
                this.imageUrllist.add(((MyImageBean) myImageslist.get(i)).picUrl);
            } else if (myImageslist.get(i) instanceof String) {
                this.imageUrllist.add((String) myImageslist.get(i));
            }
        }

    }

    @Override
    public int getCount() {
        return imageUrllist.size();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
        object = null;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View convertView = mInflater.inflate(R.layout.album_photo_listitem, container, false);
        final View lin_parent = convertView.findViewById(R.id.lin_parent);
        if (backTransparent) {
            lin_parent.setBackgroundColor(ContextCompat.getColor(mContext, R.color.black_shade_color));
        } else {
            lin_parent.setBackgroundColor(ContextCompat.getColor(mContext, R.color.black));
        }
        final VideoAndPhotoItemView imgProduct = convertView.findViewById(R.id.img_thumb);
        if (dragListener != null) {
            imgProduct.setDragListener(dragListener);
        }

        String url = imageUrllist.get(position);

        if (url != null && !url.endsWith(".mp4")) {//如果不是视频
            imgProduct.initView(VideoAndPhotoItemView.TYPE_PHOTO, imageUrllist.get(position));
            Uri uri;
            if (url.contains("http")) {
                uri = Uri.parse(ImageUtils.buildNetPictureUrl(imageUrllist.get(position)));
            } else {
                uri = Uri.parse("file://" + imageUrllist.get(position));
            }
            DraweeController ctrl = Fresco.newDraweeControllerBuilder()
                    .setImageRequest(ImageRequestBuilder.newBuilderWithSource(uri).build())
                    .setAutoPlayAnimations(true)
                    .setTapToRetryEnabled(true)
                    .build();
            GenericDraweeHierarchy hierarchy = new GenericDraweeHierarchyBuilder(TheLApp.getContext().getResources()).setActualImageScaleType(ScalingUtils.ScaleType.FIT_CENTER).setProgressBarImage(new ProgressBarDrawable()).build();

            imgProduct.setController(ctrl);
            imgProduct.setHierarchy(hierarchy);

            // imgProduct.enableDoubleTapZoomPhotoReport(mContext, mRela_id, mIs_watermark,mImageShareBean);
            imgProduct.enableDoubleTapZoom(mContext, mRela_id, mIs_watermark, mImageShareBean);

        } else {//是视频
            imgProduct.initView(VideoAndPhotoItemView.TYPE_VIDEO, imageUrllist.get(position));
            imgProduct.setTag(position);
            convertView.setTag(imgProduct);
            if (!TextUtils.isEmpty(fromPage) && fromPage.equals("UserInfoActivity")) { //从用户详情照片过来，有视频的话支持长按举报。
                imgProduct.enableDoubleTapZoomVideoReport(mContext, mRela_id, mUserId, mIs_watermark, mImageShareBean, mUserinfoPicbean);

            }
        }
        container.addView(convertView, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        imgProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemClickListener != null) {
                    itemClickListener.onItemClick(imgProduct, position);
                }
            }
        });
        return convertView;
    }

    /**
     * //设置当前显示的页面为 position
     *
     * @param position
     */
    public void setSelectPage(ViewGroup viewPager, int position) {
        boolean isPlaying = false;
        for (int i = 0; i < viewPager.getChildCount(); i++) {
            View convertView = viewPager.getChildAt(i);
            if (convertView != null && convertView.getTag() != null && convertView.getTag() instanceof VideoAndPhotoItemView) {
                if ((int) ((VideoAndPhotoItemView) (convertView.getTag())).getTag() == position) {//这个view如果是当前页
                    ((VideoAndPhotoItemView) convertView.getTag()).setActive(true);//播放视频
                    isPlaying = true;
                } else {
                    ((VideoAndPhotoItemView) convertView.getTag()).setActive(false);//取消播放
                }
            }
        }
        Utils.sendVideoPlayingBroadcast(isPlaying);//发送是否在播放视频广播，用于直播间的时候屏蔽主播声音
    }

    @Override
    public void notifyDataSetChanged() {
        currentPosition = -1;
        super.notifyDataSetChanged();
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        super.setPrimaryItem(container, position, object);
        if (currentPosition != position) {
            currentPosition = position;
            setSelectPage(container, position);
        }
    }

    public void setOnItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View v, int position);
    }
}
