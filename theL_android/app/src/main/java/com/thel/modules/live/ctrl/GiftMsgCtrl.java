package com.thel.modules.live.ctrl;

import com.thel.modules.live.bean.GiftMsgShowListBean;
import com.thel.modules.live.bean.GiftSendMsgBean;
import com.thel.modules.live.view.SoftGiftMsgLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by the L on 2016/5/31.
 */
public class GiftMsgCtrl implements SoftGiftMsgLayout.EmptySpaceCallBack {
    /**
     * 收到赠送礼物消息的显示的View
     */
    private SoftGiftMsgLayout msgView;//收到赠送礼物消息的显示的View
    /**
     * 全部要显示的消息集合
     */
    private List<GiftMsgShowListBean> totalMsgList = new ArrayList<>();//

    public GiftMsgCtrl(SoftGiftMsgLayout softGiftMsgLayout) {

        this.msgView = softGiftMsgLayout;
        msgView.setCallBackListener(this);
    }

    /**
     * 接受到消息，添加到队列中
     *
     * @param msgbean 接受到消息
     */
    public void receiveGiftMsg(GiftSendMsgBean msgbean) {
        for (int i = 0; i < totalMsgList.size(); i++) {
            GiftMsgShowListBean listBean = totalMsgList.get(i);
            /**
             * 只有当一个消息的,id，userid，与队列相同并且combo大于队里的combo时候，才看做是一个giftmsgshowlistbean里的一个连击追加
             */
            if (listBean.id == msgbean.id && listBean.userId.equals(msgbean.userId) && listBean.combo < msgbean.combo) {
                listBean.addGiftMsg(msgbean);//添加到队列集合里面
                return;
            }

        }
        //执行到这里说明没有添加到以往的集合里面，则新建集合队列对象
        GiftMsgShowListBean listbean = new GiftMsgShowListBean();
        listbean.addGiftMsg(msgbean);
        totalMsgList.add(listbean);
        msgView.remindMsg();//提醒msgview有新消息来
    }

    /**
     * 显示完毕，移除
     *
     * @param giftMsgShowListBean 显示完毕的消息
     */
    @Override
    public void showFinish(GiftMsgShowListBean giftMsgShowListBean) {
        totalMsgList.remove(giftMsgShowListBean);//移除
        giftMsgShowListBean = null;
    }

    /**
     * 回调，如果有空位的时候执行
     */
    @Override
    public void pushGiftMsgList() {
        for (int i = 0; i < totalMsgList.size(); i++) {
            if (!totalMsgList.get(i).isShow()) {//遍历消息队列，如果某个不在显示
                msgView.showMsg(totalMsgList.get(i));//显示
                return;//循环结束
            }
        }
    }

    public GiftMsgCtrl clearMsg() {
        totalMsgList.clear();
        return this;
    }

}
