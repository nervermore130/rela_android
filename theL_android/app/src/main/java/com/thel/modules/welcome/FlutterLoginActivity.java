package com.thel.modules.welcome;

import android.content.Intent;
import android.os.Bundle;

import com.facebook.CallbackManager;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseFlutterActivity;
import com.thel.flutter.OnActivityResultCallback;
import com.thel.flutter.RelaFlutterFragment;
import com.thel.flutter.bridge.RfSBridgeHandlerFactory;
import com.thel.flutter.plugin.FlutterEventHandler;
import com.thel.flutter.plugin.LoginMethodHandler;
import com.thel.ui.imageviewer.cropiwa.image.CropIwaResultReceiver;
import com.thel.utils.DeviceUtils;
import com.thel.utils.L;
import com.thel.utils.MD5Utils;
import com.thel.utils.PhoneUtils;
import com.thel.utils.ShareFileUtils;

import java.util.UUID;

import io.flutter.plugins.GeneratedPluginRegistrant;
import io.flutter.view.FlutterView;

public class FlutterLoginActivity extends BaseFlutterActivity {

    private CallbackManager callbackManager = CallbackManager.Factory.create();

    private OnActivityResultCallback onActivityResultCallback;

    private CropIwaResultReceiver cropResultReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        L.d("FlutterLoginActivity", "----------onCreate----------");

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_flutter_common);

        RelaFlutterFragment relaFlutterFragment = RelaFlutterFragment.getInstance();

        relaFlutterFragment.onRegisterPlugins(this);

        getSupportFragmentManager().beginTransaction().add(R.id.fragment_container_fl, relaFlutterFragment, "FlutterLoginActivity").commit();

        FlutterView flutterView = getFlutterView();

        flutterView.setInitialRoute("/");

        FlutterEventHandler.registerWith(flutterView, FlutterEventHandler.CHANNEL_NAME, getData());

        LoginMethodHandler.getInstance().registerWith(flutterView, this);

        cropResultReceiver = new CropIwaResultReceiver();
        cropResultReceiver.register(this);
        cropResultReceiver.setListener(LoginMethodHandler.getInstance().getLoginResultDelegate().getListener());

    }

    public CallbackManager getCallbackManager() {
        return callbackManager;
    }

    @Override public void setOnActivityResultCallback(OnActivityResultCallback onActivityResultCallback) {
        this.onActivityResultCallback = onActivityResultCallback;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (callbackManager != null) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
        if (onActivityResultCallback != null) {
            onActivityResultCallback.onActivityResult(requestCode, resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    protected void onPause() {

        super.onPause();
    }

    @Override
    protected void onDestroy() {

        LoginMethodHandler.getInstance().onDestroy();
        if (cropResultReceiver != null) {
            cropResultReceiver.unregister(this);
        }
        super.onDestroy();
    }

    private String getData() {

        String lat = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");

        String lng = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");

        String mobileOS = "Android " + PhoneUtils.getOSVersion();

        String clientVersion = DeviceUtils.getVersionCode(TheLApp.getContext()) + "";

        String language = DeviceUtils.getLanguageStr();

        String deviceId = ShareFileUtils.getString(ShareFileUtils.DEVICE_ID, UUID.randomUUID().toString());

        String apptype = ShareFileUtils.isGlobalVersion() ? "global" : "";

        return "{\"lat\":\"" + lat + "\",\"lng\":\"" + lng + "\",\"mobileOS\":\"" + mobileOS + "\",\"clientVersion\":\"" + clientVersion + "\",\"language\":\"" + language + "\",\"deviceId\":\"" + deviceId + "\",\"apptype\":\"" + apptype + "\",\"userAgent\":\"" + MD5Utils.getUserAgent() + "\"}";

    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }
}
