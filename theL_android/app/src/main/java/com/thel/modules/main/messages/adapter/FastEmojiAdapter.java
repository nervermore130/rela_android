package com.thel.modules.main.messages.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.thel.R;
import com.thel.base.BaseAdapter;
import com.thel.constants.TheLConstants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FastEmojiAdapter extends BaseAdapter<FastEmojiAdapter.FastEmojiViewHolder, String> {

    public static final int MSG_TYPE_IM = 0x01;

    public static final int MSG_TYPE_LIVE = 0x02;

    private List<String> msgList = new ArrayList<>();

    public FastEmojiAdapter(Context context, int type) {
        super(context);

        if (type == MSG_TYPE_IM) {
            msgList.addAll(Arrays.asList(TheLConstants.IM_FAST_EMOJI));
        }

        if (type == MSG_TYPE_LIVE) {
            msgList.addAll(Arrays.asList(TheLConstants.LIVE_FAST_EMOJI));
        }

    }

    @Override public void onBindViewHolder(@NonNull FastEmojiViewHolder holder, final int position) {
        super.onBindViewHolder(holder, position);

        String msg = msgList.get(position);

        holder.textView.setText(msg);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {

                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(v, msg, position);
                }
            }
        });

    }

    @Override public List<String> getData() {
        return msgList;
    }

    @Override public FastEmojiViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_fast_msg, parent, false);
        return new FastEmojiViewHolder(view);
    }

    @Override public int getItemCount() {
        return msgList.size();
    }

    public class FastEmojiViewHolder extends RecyclerView.ViewHolder {

        TextView textView;

        public FastEmojiViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.fast_msg_tv);
        }
    }

}
