package com.thel.modules.main.me.aboutMe;

import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.kyleduo.switchbutton.SwitchButton;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.modules.main.me.bean.UserLevelBean;
import com.thel.modules.main.me.bean.UserLevelSwitchBean;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.service.DefaultRequestService;
import com.thel.ui.widget.UserLevelImageView;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.ViewUtils;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * 我的等级页面
 * Created by lingwei on 2018/1/17.
 */

public class MyLevelActivity extends BaseActivity {

    private SimpleDraweeView img_avatar;
    private TextView txt_nickname;
    private TextView txt_experience_data;
    private TextView txt_soft_money;
    private TextView txt_total_watchtime;
    private TextView txt_total_share;
    private LinearLayout lin_back;
    private UserLevelImageView iv_my_level;
    private ProgressBar progressbar_level_data;

    private TextView txt_start_level_data;
    private TextView txt_end_level_data;
    private SwipeRefreshLayout swipe_refresh;
    private ImageView view_1;
    private ImageView view_2;
    private ImageView view_3;
    private ImageView view_4;
    private ImageView view_5;
    private ImageView view_6;
    private ImageView view_7;
    private ImageView view_8;
    private ImageView view_9;
    private ImageView view_10;
    private ImageView view_11;
    private ImageView view_12;
    private ImageView view_13;
    private ImageView view_14;
    private ImageView view_15;
    private SwitchButton img_live_hide_switch;
    private LinearLayout privilege_switch;
    private ImageView view_0_5;
    private ImageView line;
    private RelativeLayout rl_lieve_enter_specially;
    private SwitchButton img_user_level_hide;
    private int liveSpecially = 0;
    private int userLevelIcon = 0;
    private int isCloaking = 0;
    private TextView live_prompt;
    private TextView level_prompt;
    private int user_level;
    private String postType;
    private TextView level_list_prompt;
    private SwitchButton img_list_hide_siwtch;
    private ImageView view_26;
    private ImageView view_21;
    private ImageView view_30;
    private ImageView view_35;
    private ImageView view_enter_sound;
    private ImageView view36;
    private LinearLayout ll_exclusive_post_content;
    private LinearLayout ll_lv40;
    private ImageView iv_user_lever_forty;
    private ImageView iv_user_lever_specially_forty;
    private ImageView iv_user_lever_forty_gift;
    private ImageView iv_user_lever_bean;
    private LinearLayout ll_vip_forever;
    private LinearLayout ll_special_gift;
    private LinearLayout ll_return_bean;
    private TextView tv_coming_soon;
    private TextView tv_eleven;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_level_activity_layout);
        findViewById();
        processBusiness();
        setLinsener();

    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void findViewById() {
        img_avatar = findViewById(R.id.img_avatar);
        swipe_refresh = findViewById(R.id.swipe_refresh);
        ViewUtils.initSwipeRefreshLayout(swipe_refresh);
        //根据等级来显示图片是否灰亮
        view_0_5 = findViewById(R.id.iv_user_lever_icon);
        view_1 = findViewById(R.id.iv_user_lever_live_specially);
        view_2 = findViewById(R.id.iv_user_lever_mic);
        view_5 = findViewById(R.id.iv_user_lever_specially_danmu);
        view_3 = findViewById(R.id.iv_user_lever_hide);
        view_4 = findViewById(R.id.iv_user_lever_promotion);
        view_6 = findViewById(R.id.iv_user_lever_lists_hide);
        view_7 = findViewById(R.id.iv_user_lever_gift);
        view_8 = findViewById(R.id.iv_user_lever_week_gift);
        view_9 = findViewById(R.id.iv_user_lever_identity);
        view_10 = findViewById(R.id.iv_user_lever_specially_miusic);
        view_11 = findViewById(R.id.iv_user_lever_speak);
        view_12 = findViewById(R.id.iv_user_lever_charge_money_present);
        view_13 = findViewById(R.id.iv_user_lever_specially_miusic_thirty);
        view_14 = findViewById(R.id.iv_user_lever_speak_thirty);
        view_15 = findViewById(R.id.iv_user_lever_all_servive);
        view_21 = findViewById(R.id.iv_user_lever_twentyone_gift);
        view_26 = findViewById(R.id.iv_user_lever_twentysix_gift);
        view_30 = findViewById(R.id.iv_user_lever_thirty_gift);
        view_35 = findViewById(R.id.iv_user_lever_thirty_five_gift);
        view36 = findViewById(R.id.iv_user_lever_post_content);
        view_enter_sound = findViewById(R.id.iv_user_lever_specially_thirty_five);
        iv_user_lever_specially_forty = findViewById(R.id.iv_user_lever_specially_forty);
        iv_user_lever_forty_gift = findViewById(R.id.iv_user_lever_forty_gift);

        txt_nickname = findViewById(R.id.txt_nickname);
        txt_experience_data = findViewById(R.id.txt_experience_data);
        txt_soft_money = findViewById(R.id.txt_soft_money);
        txt_total_watchtime = findViewById(R.id.txt_total_watchtime);
        txt_total_share = findViewById(R.id.txt_total_share);
        iv_my_level = findViewById(R.id.iv_my_level);
        txt_start_level_data = findViewById(R.id.txt_start_level_data);
        txt_end_level_data = findViewById(R.id.txt_end_level_data);
        lin_back = findViewById(R.id.lin_back);
        progressbar_level_data = findViewById(R.id.progressbar_level_data);
        TextView txt_title = findViewById(R.id.txt_title);
        img_live_hide_switch = findViewById(R.id.img_live_hide);
        privilege_switch = findViewById(R.id.privilege_switch);
        img_list_hide_siwtch = findViewById(R.id.img_list_hide);
        rl_lieve_enter_specially = findViewById(R.id.rl_lieve_enter_specially);
        line = findViewById(R.id.line);
        img_user_level_hide = findViewById(R.id.img_user_level_hide);
        live_prompt = findViewById(R.id.live_prompt);
        level_prompt = findViewById(R.id.level_prompt);
        level_list_prompt = findViewById(R.id.level_list_prompt);

        ll_exclusive_post_content = findViewById(R.id.ll_exclusive_post_content);
        ll_lv40 = findViewById(R.id.ll_lv40);
        iv_user_lever_bean = findViewById(R.id.iv_user_lever_bean);
        ll_vip_forever = findViewById(R.id.ll_vip_forever);
        ll_special_gift = findViewById(R.id.ll_special_gift);
        ll_return_bean = findViewById(R.id.ll_return_bean);
        tv_coming_soon = findViewById(R.id.tv_coming_soon);
        tv_eleven = findViewById(R.id.tv_eleven);
        UserLevelImageView iv_user_lever_zero = findViewById(R.id.iv_user_lever_zero);
        iv_user_lever_zero.initView(0);
        View lin_more = findViewById(R.id.lin_more);
        lin_more.setVisibility(View.GONE);
        txt_title.setText(getString(R.string.me_level));

        TextView tv_lv_35 = findViewById(R.id.tv_lv_35);
        TextView tv_lv_30 = findViewById(R.id.tv_lv_30);
        TextView tv_lv_26 = findViewById(R.id.tv_lv_26);
        TextView tv_lv_21 = findViewById(R.id.tv_lv_21);
        TextView tv_lv_16 = findViewById(R.id.tv_lv_16);
        TextView tv_lv_11 = findViewById(R.id.tv_lv_11);
        TextView tv_lv_6 = findViewById(R.id.tv_lv_6);
        TextView tv_lv_1 = findViewById(R.id.tv_lv_1);

        tv_lv_1.setText(TheLApp.context.getString(R.string.above_lv_x, 1));
        tv_lv_6.setText(TheLApp.context.getString(R.string.above_lv_x, 6));
        tv_lv_11.setText(TheLApp.context.getString(R.string.above_lv_x, 11));
        tv_lv_16.setText(TheLApp.context.getString(R.string.above_lv_x, 16));
        tv_lv_21.setText(TheLApp.context.getString(R.string.above_lv_x, 21));
        tv_lv_26.setText(TheLApp.context.getString(R.string.above_lv_x, 26));
        tv_lv_30.setText(TheLApp.context.getString(R.string.above_lv_x, 30));
        tv_lv_35.setText(TheLApp.context.getString(R.string.above_lv_x, 35));

        /**
         * 5.1.0 如果开关为1的话，就展示，否则就不展示
         * */
        int levelPrivileges = ShareFileUtils.getInt(ShareFileUtils.LEVEL_PRIVILEGES, 0);
        if (levelPrivileges == 1) {
            ll_exclusive_post_content.setVisibility(View.VISIBLE);
            ll_lv40.setVisibility(View.VISIBLE);
            ll_vip_forever.setVisibility(View.VISIBLE);
            ll_special_gift.setVisibility(View.VISIBLE);
            ll_return_bean.setVisibility(View.VISIBLE);
            tv_coming_soon.setVisibility(View.GONE);
            tv_eleven.setVisibility(View.GONE);
        }


    }

    private void setLinsener() {
        lin_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        swipe_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadUserLevel();
            }
        });
        img_live_hide_switch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 500);
                postEntryLive(1 - liveSpecially, userLevelIcon, isCloaking, "liveSpecially");
            }
        });
        img_user_level_hide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ViewUtils.preventViewMultipleClick(view, 500);
                postEntryLive(liveSpecially, 1 - userLevelIcon, isCloaking, "userLevelIcon");
            }
        });
        //榜单隐身
        img_list_hide_siwtch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewUtils.preventViewMultipleClick(view, 500);
                postEntryLive(liveSpecially, userLevelIcon, 1 - isCloaking, "isCloaking");
            }
        });

    }

    private void postEntryLive(int live_entry, int user_level, int is_cloaking, String posttype) {
        this.postType = posttype;
        Flowable<UserLevelSwitchBean> flowable = RequestBusiness.getInstance().postLiveSetting(live_entry + "", user_level + "", is_cloaking + "");
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<UserLevelSwitchBean>() {
            @Override
            public void onNext(UserLevelSwitchBean levelBean) {
                super.onNext(levelBean);
                getLevelSwitchResult(levelBean);

            }

            @Override
            public void onError(Throwable t) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void getLevelSwitchResult(UserLevelSwitchBean levelBean) {
        if (levelBean != null && levelBean.data != null) {
            liveSpecially = levelBean.data.entrySwitch == -1 ? liveSpecially : levelBean.data.entrySwitch;
            userLevelIcon = levelBean.data.levelIconSwitch == -1 ? userLevelIcon : levelBean.data.levelIconSwitch;
            if (TextUtils.isEmpty(levelBean.data.isCloaking + "")) {
                isCloaking = 1 - isCloaking;
            } else {
                isCloaking = levelBean.data.isCloaking == -1 ? isCloaking : levelBean.data.isCloaking;

            }
            refreshCheckbox();
        }
    }

    private void refreshUiCheckbox() {
        img_user_level_hide.setChecked(userLevelIcon != 0);

        img_live_hide_switch.setChecked(liveSpecially != 0);

        img_list_hide_siwtch.setChecked(isCloaking != 0);

    }

    private void refreshCheckbox() {
        if ("userLevelIcon".equals(postType)) {
            img_user_level_hide.setChecked(userLevelIcon != 0);

        } else if ("liveSpecially".equals(postType)) {
            img_live_hide_switch.setChecked(liveSpecially != 0);
        } else if ("isCloaking".equals(postType)) {
            // img_list_hide_siwtch.setChecked(isCloaking != 0);
            //   localIsCloaking == -1 ? localIsCloaking : levelBean.data.isCloaking;
            img_list_hide_siwtch.setClickable(isCloaking != 0);
        }

    }

    private void processBusiness() {
        String avatar = ShareFileUtils.getString(ShareFileUtils.AVATAR, "");
        String nickname = ShareFileUtils.getString(ShareFileUtils.USER_NAME, "");
        if (!TextUtils.isEmpty(avatar)) {

            img_avatar.setImageURI(Uri.parse(avatar));
        } else {
            img_avatar.setImageResource(R.mipmap.icon_user);
        }
        txt_nickname.setText(nickname);

        int level = ShareFileUtils.getInt(ShareFileUtils.Level, 0);
        L.d("mylevelactivity", "level:" + level);

        refreshCacheLevel(level);
        loadUserLevel();

    }

    private void refreshCacheLevel(int level) {
        if (level >= 40) {
            iv_user_lever_specially_forty.setImageResource(R.mipmap.icn_lifevip_normal);
            iv_user_lever_forty_gift.setImageResource(R.mipmap.user_level_big_7);
            iv_user_lever_bean.setImageResource(R.mipmap.icn_credits_bean_normal);
            view_0_5.setImageResource(R.mipmap.icn_level_icon);
            view_1.setImageResource(R.mipmap.user_level_big_1);
            view_2.setImageResource(R.mipmap.user_lavel_mic);
            //   view_3.setImageResource(R.mipmap.user_level_big_4);
            view_4.setImageResource(R.mipmap.user_level_big_5);
            view_5.setImageResource(R.mipmap.user_level_big_3);
            view_6.setImageResource(R.mipmap.user_level_big_6);
            view_7.setImageResource(R.mipmap.user_level_big_7);
            view_8.setImageResource(R.mipmap.user_level_big_8);
            view_9.setImageResource(R.mipmap.user_level_big_9);
            view_10.setImageResource(R.mipmap.user_level_big_10);
            view_11.setImageResource(R.mipmap.user_level_big_11);
            view_12.setImageResource(R.mipmap.user_level_big_12);
            view_13.setImageResource(R.mipmap.user_level_big_13);
            view_14.setImageResource(R.mipmap.user_level_big_14);
            view_15.setImageResource(R.mipmap.user_level_big_15);
            view_26.setImageResource(R.mipmap.user_level_big_7);
            view_21.setImageResource(R.mipmap.user_level_big_7);
            view_30.setImageResource(R.mipmap.user_level_big_7);
            view_35.setImageResource(R.mipmap.user_level_big_7);
            view_enter_sound.setImageResource(R.mipmap.user_level_big_35);
        } else if (level >= 35 && level < 40) {
            view_0_5.setImageResource(R.mipmap.icn_level_icon);
            view_1.setImageResource(R.mipmap.user_level_big_1);
            view_2.setImageResource(R.mipmap.user_lavel_mic);
            //   view_3.setImageResource(R.mipmap.user_level_big_4);
            view_4.setImageResource(R.mipmap.user_level_big_5);
            view_5.setImageResource(R.mipmap.user_level_big_3);
            view_6.setImageResource(R.mipmap.user_level_big_6);
            view_7.setImageResource(R.mipmap.user_level_big_7);
            view_8.setImageResource(R.mipmap.user_level_big_8);
            view_9.setImageResource(R.mipmap.user_level_big_9);
            view_10.setImageResource(R.mipmap.user_level_big_10);
            view_11.setImageResource(R.mipmap.user_level_big_11);
            view_12.setImageResource(R.mipmap.user_level_big_12);
            view_13.setImageResource(R.mipmap.user_level_big_13);
            view_14.setImageResource(R.mipmap.user_level_big_14);
            view_15.setImageResource(R.mipmap.user_level_big_15);
            view_26.setImageResource(R.mipmap.user_level_big_7);
            view_21.setImageResource(R.mipmap.user_level_big_7);
            view_30.setImageResource(R.mipmap.user_level_big_7);
            view_35.setImageResource(R.mipmap.user_level_big_7);
            view_enter_sound.setImageResource(R.mipmap.user_level_big_35);
            view36.setImageResource(R.mipmap.user_level_big_16);
        } else if (level >= 30 && level < 35) {
            view_0_5.setImageResource(R.mipmap.icn_level_icon);
            view_1.setImageResource(R.mipmap.user_level_big_1);
            view_2.setImageResource(R.mipmap.user_lavel_mic);
            //   view_3.setImageResource(R.mipmap.user_level_big_4);
            view_4.setImageResource(R.mipmap.user_level_big_5);
            view_5.setImageResource(R.mipmap.user_level_big_3);
            view_6.setImageResource(R.mipmap.user_level_big_6);
            view_7.setImageResource(R.mipmap.user_level_big_7);
            view_8.setImageResource(R.mipmap.user_level_big_8);
            view_9.setImageResource(R.mipmap.user_level_big_9);
            view_10.setImageResource(R.mipmap.user_level_big_10);
            view_11.setImageResource(R.mipmap.user_level_big_11);
            view_12.setImageResource(R.mipmap.user_level_big_12);
            view_13.setImageResource(R.mipmap.user_level_big_13);
            view_14.setImageResource(R.mipmap.user_level_big_14);
            view_15.setImageResource(R.mipmap.user_level_big_15);
            view_26.setImageResource(R.mipmap.user_level_big_7);
            view_21.setImageResource(R.mipmap.user_level_big_7);
            view_30.setImageResource(R.mipmap.user_level_big_7);
        } else if (level >= 26 && level <= 29) {
            view_0_5.setImageResource(R.mipmap.icn_level_icon);
            view_1.setImageResource(R.mipmap.user_level_big_1);
            view_2.setImageResource(R.mipmap.user_lavel_mic);
            // view_3.setImageResource(R.mipmap.user_level_big_4);
            view_4.setImageResource(R.mipmap.user_level_big_5);
            view_5.setImageResource(R.mipmap.user_level_big_3);
            view_6.setImageResource(R.mipmap.user_level_big_6);
            view_7.setImageResource(R.mipmap.user_level_big_7);
            view_8.setImageResource(R.mipmap.user_level_big_8);
            view_9.setImageResource(R.mipmap.user_level_big_9);
            view_10.setImageResource(R.mipmap.user_level_big_10);
            view_11.setImageResource(R.mipmap.user_level_big_11);
            view_12.setImageResource(R.mipmap.user_level_big_12);
            view_26.setImageResource(R.mipmap.user_level_big_7);
            view_21.setImageResource(R.mipmap.user_level_big_7);
        } else if (level >= 21 && level <= 25) {
            view_0_5.setImageResource(R.mipmap.icn_level_icon);
            view_1.setImageResource(R.mipmap.user_level_big_1);
            view_2.setImageResource(R.mipmap.user_lavel_mic);
            view_3.setImageResource(R.mipmap.user_level_big_4);
            view_4.setImageResource(R.mipmap.user_level_big_5);
            view_5.setImageResource(R.mipmap.user_level_big_3);
            view_6.setImageResource(R.mipmap.user_level_big_6);
            view_7.setImageResource(R.mipmap.user_level_big_7);
            view_8.setImageResource(R.mipmap.user_level_big_8);
            view_9.setImageResource(R.mipmap.user_level_big_9);
            view_21.setImageResource(R.mipmap.user_level_big_7);

        } else if (level >= 16 && level <= 20) {
            view_0_5.setImageResource(R.mipmap.icn_level_icon);

            view_1.setImageResource(R.mipmap.user_level_big_1);
            view_2.setImageResource(R.mipmap.user_lavel_mic);
            //   view_3.setImageResource(R.mipmap.user_level_big_4);
            view_4.setImageResource(R.mipmap.user_level_big_5);
            view_5.setImageResource(R.mipmap.user_level_big_3);
            view_6.setImageResource(R.mipmap.user_level_big_6);
            view_7.setImageResource(R.mipmap.user_level_big_7);
        } else if (level >= 11 && level <= 15) {
            view_0_5.setImageResource(R.mipmap.icn_level_icon);

            view_1.setImageResource(R.mipmap.user_level_big_1);
            view_2.setImageResource(R.mipmap.user_lavel_mic);
            //     view_3.setImageResource(R.mipmap.user_level_big_4);
            view_4.setImageResource(R.mipmap.user_level_big_5);
        } else if (level >= 6 && level <= 10) {
            view_0_5.setImageResource(R.mipmap.icn_level_icon);

            view_1.setImageResource(R.mipmap.user_level_big_1);
            view_2.setImageResource(R.mipmap.user_lavel_mic);
        } else if (level >= 0 && level <= 5) {
            view_0_5.setImageResource(R.mipmap.icn_level_icon);

        }

    }

    private void loadUserLevel() {
        DefaultRequestService.createAllRequestService().getUserLevel().onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<UserLevelBean>() {
            @Override
            public void onNext(UserLevelBean data) {
                super.onNext(data);
                refreshUi(data);
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                requestFinished();
            }

            @Override
            public void onComplete() {
                super.onComplete();
                requestFinished();

            }
        });
    }

    private void refreshUi(UserLevelBean userLevelDataBean) {
        if (swipe_refresh != null && !swipe_refresh.isRefreshing()) {
            swipe_refresh.post(new Runnable() {
                @Override
                public void run() {
                    swipe_refresh.setRefreshing(true);
                }
            });
        }
        if (userLevelDataBean != null && userLevelDataBean.data != null) {

            liveSpecially = userLevelDataBean.data.entrySwitch;
            userLevelIcon = userLevelDataBean.data.levelIconSwitch;
            isCloaking = userLevelDataBean.data.isCloaking;
            final String expText = userLevelDataBean.data.nextLevelHasGot + "/" + userLevelDataBean.data.levelUpRequire;
            txt_experience_data.setText(expText + getString(R.string.level_current_exp));
            float watchTime = userLevelDataBean.data.watchTime;
            if (watchTime == 0.00) {
                txt_total_watchtime.setText(0 + getString(R.string.level_watch_hours));

            } else {
                txt_total_watchtime.setText(userLevelDataBean.data.watchTime + getString(R.string.level_watch_hours));

            }
            if (userLevelDataBean.data.shareCount <= 0) {
                txt_total_share.setText(getString(R.string.one_time, userLevelDataBean.data.shareCount));
            } else {
                txt_total_share.setText(getString(R.string.two_further_time, userLevelDataBean.data.shareCount));
            }
            txt_soft_money.setText(getString(R.string.soft_moneys, userLevelDataBean.data.diamond));
            refreshUiCheckbox();
/**
 * 等级图标大于0级开关可用，
 * 会员直播入场大于6级开关可用。
 * **/
            user_level = userLevelDataBean.data.level;
            refreshCacheLevel(user_level);

            if (user_level < 1) {
                level_prompt.setVisibility(View.VISIBLE);
                live_prompt.setVisibility(View.VISIBLE);
                level_list_prompt.setVisibility(View.VISIBLE);

                img_user_level_hide.setChecked(false);
                img_user_level_hide.setClickable(false);
                img_live_hide_switch.setClickable(false);
                img_live_hide_switch.setChecked(false);
                img_list_hide_siwtch.setChecked(false);
                img_list_hide_siwtch.setClickable(false);
            } else if (user_level > 0 && user_level < 6) {
                level_prompt.setVisibility(View.GONE);
                level_list_prompt.setVisibility(View.VISIBLE);

                live_prompt.setVisibility(View.VISIBLE);
                img_live_hide_switch.setClickable(false);
                img_live_hide_switch.setChecked(false);
                img_list_hide_siwtch.setChecked(false);
                img_list_hide_siwtch.setClickable(false);
            } else if (user_level >= 6 && user_level < 16) {
                level_list_prompt.setVisibility(View.VISIBLE);
                img_list_hide_siwtch.setChecked(false);
                img_list_hide_siwtch.setClickable(false);
                level_prompt.setVisibility(View.GONE);
                live_prompt.setVisibility(View.GONE);
            } else if (user_level >= 16) {
                level_list_prompt.setVisibility(View.GONE);
                level_prompt.setVisibility(View.GONE);
                live_prompt.setVisibility(View.GONE);
            }

            ShareFileUtils.setInt(ShareFileUtils.Level, user_level);

          /*  if (user_level >= 0 && user_level < TheLConstants.USER_LEVEL_RES.length) {
                int res = TheLConstants.USER_LEVEL_RES[user_level];
                iv_my_level.setImageResource(res);
            }*/
            iv_my_level.initView(user_level);
            float currentExpPercent = userLevelDataBean.data.currentExpPercent;
            int currentpercent = (int) (currentExpPercent * 100);
            txt_start_level_data.setText("Lv" + user_level);
            if (user_level >= 40) {
                txt_end_level_data.setText("Lv" + user_level);
            } else {
                int next_level = user_level + 1;
                txt_end_level_data.setText("Lv" + next_level);

            }
            progressbar_level_data.setProgress(currentpercent);

        }

    }

    private void requestFinished() {
        if (swipe_refresh != null)
            swipe_refresh.postDelayed(new Runnable() {

                @Override
                public void run() {
                    if (swipe_refresh != null && swipe_refresh.isRefreshing())
                        swipe_refresh.setRefreshing(false);
                }
            }, 1000);
    }

}
