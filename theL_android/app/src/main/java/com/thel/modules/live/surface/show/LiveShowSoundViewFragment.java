package com.thel.modules.live.surface.show;

import android.view.View;
import android.widget.LinearLayout;

import com.thel.R;

/**
 * Created by waiarl on 2018/1/22.
 * 单人声音直播
 */

public class LiveShowSoundViewFragment extends LiveShowBaseViewFragment {
    private LinearLayout lin_sound_view_bottom;
    private LinearLayout lin_sound_bottom_text;
    private LinearLayout lin_sound_bottom_music;

    @Override
    protected void findBottomView(View view) {
        super.findBottomView(view);
        lin_sound_view_bottom = (LinearLayout) findViewById(R.id.lin_sound_view_bottom);
        lin_sound_bottom_text = (LinearLayout) findViewById(R.id.lin_sound_bottom_text);
        lin_sound_bottom_music = (LinearLayout) findViewById(R.id.lin_sound_bottom_music);


        control_ll.setVisibility(View.GONE);
        lin_sound_view_bottom.setVisibility(View.VISIBLE);
    }

    @Override
    protected void setBottomListener() {
        super.setBottomListener();
        lin_sound_bottom_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTextSizeDialog();
            }
        });

        lin_sound_bottom_music.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMusicAppView();
            }
        });
    }

}
