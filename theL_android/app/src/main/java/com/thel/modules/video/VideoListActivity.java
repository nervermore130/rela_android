package com.thel.modules.video;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;

import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.modules.main.messages.utils.PushUtils;
import com.thel.utils.L;

/**
 * @author liuyun
 * @date 2018/2/6
 */

public class VideoListActivity extends BaseActivity {

    private VideoListFragment videoListFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);


        L.d("UserVideoListActivity","onCreate");

        setContentView(R.layout.activity_video_list);

        final Bundle bundle=getIntent().getExtras();

        videoListFragment = VideoListFragment.newInstance(bundle);

        getSupportFragmentManager().beginTransaction().add(R.id.frame_content, videoListFragment, VideoListFragment.class.getName()).commit();

    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    @Override protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        L.d("UserVideoListActivity","onNewIntent");
    }

    @Override public void onBackPressed() {
        super.onBackPressed();
        PushUtils.finish(VideoListActivity.this);
    }
}
