package com.thel.modules.live.view.expensive;

import android.view.View;

/**
 * Created by waiarl on 2018/3/2.
 */

public interface TopGiftItemImp<T extends View> {

    T initView(TopGiftBean topGiftBean);

    T getView();

    T setTopGiftClickListener(TopGiftClickListener listener);

    boolean isAdded();

    void setIsAdded(boolean isAdded);

    interface TopGiftClickListener {
        void clickSender(String userId);

        void clickReceiver(String userId);

        void clickJumpToLive(String userId);
    }
}
