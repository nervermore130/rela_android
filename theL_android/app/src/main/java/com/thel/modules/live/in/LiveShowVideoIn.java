package com.thel.modules.live.in;

import com.thel.modules.live.bean.AgoraBean;
import com.thel.modules.live.surface.watch.LiveWatchObserver;

/**
 * Created by waiarl on 2017/11/5.
 */

public interface LiveShowVideoIn {
    void bindObserver(LiveWatchObserver observer);


    String getScreenShotImagePath();

    boolean pause();

    boolean destory();

    boolean resume();

    void setVideoPreparedListener(VideoPreparedListener listener);

    /**
     * 直播结束
     */
    void liveClosed();

    /**
     * CLIENT_ROLE_AUDIENCE 角色设置为观众时，不可发言
     * CLIENT_ROLE_BROADCASTER 角色设置为主播时，可以发言
     *
     * @param role Constants.CLIENT_ROLE_AUDIENCE / Constants.CLIENT_ROLE_BROADCASTER
     */
    void setClientRole(int role, AgoraBean agoraBean);

    /**
     * 静音/取消静音。该方法用于允许/禁止往网络发送本地音频流。
     *
     * @param mute
     */
    void muteLocalAudioStream(boolean mute);

    /**
     * 开始相遇自动闭麦
     */
    void autoOpenLocalAudioStream();

    /**
     * 结束相遇自动恢复麦
     */
    void autoCloseLocalAudioStream();

    /**
     * 断开连麦
     */
    void levelChannel();

    /**
     * 获取当前正在播放的地址
     * @return
     */
    String getCurrentPlayUrl();

}
