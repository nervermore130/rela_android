package com.thel.modules.theme;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.theme.ThemeClassBean;

import java.util.ArrayList;
import java.util.Map;

import static com.thel.R.id.txt_name;

/**
 * Created by chad
 * Time 17/10/16
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */

public class ThemeListAdapter extends BaseAdapter {

    private final Context context;
    private final LayoutInflater mInflater;
    private ArrayList<ThemeClassBean> themeList = new ArrayList<>();
    private Map<Integer, Integer> themerResMap;


    public ThemeListAdapter(Context context, ArrayList<ThemeClassBean> beans, Map<Integer, Integer> themerResMap) {
        this.context = context;
        mInflater = LayoutInflater.from(TheLApp.getContext());
        themeList = beans;
        this.themerResMap = themerResMap;
    }

    @Override
    public int getCount() {

        return themeList.size();
    }

    @Override
    public Object getItem(int position) {
        return themeList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        HoldView holdView = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.theme_list_classify_item, parent, false);
            holdView = new ThemeListAdapter.HoldView();

            holdView.img = convertView.findViewById(R.id.img);
            holdView.txt_name = convertView.findViewById(txt_name);
            holdView.txt_desc = convertView.findViewById(R.id.txt_desc);
            holdView.line = convertView.findViewById(R.id.line);
            // holdView.rel_theme_click = (RelativeLayout)convertView.findViewById(R.id.rel_click_theme);
            holdView.line_long = convertView.findViewById(R.id.long_line);
            convertView.setTag(holdView); // 把holdview缓存下来
        } else {
            holdView = (HoldView) convertView.getTag();
        }
        refreshItem(position, holdView);
        return convertView;
    }

    private void refreshItem(int position, HoldView holdView) {
        ThemeClassBean bean = themeList.get(position);
        holdView.img.setImageResource(themerResMap.get(bean.res_Index));
        holdView.txt_desc.setText(bean.desc);
        holdView.txt_name.setText(bean.text);
        holdView.line.setVisibility(View.VISIBLE);
        if (position == themeList.size() - 1) {
            holdView.line.setVisibility(View.GONE);
            //                holdView.line_long.setVisibility(View.VISIBLE);
        }
        //holdView.rel_theme_click.setOnClickListener((View.OnClickListener) this);
    }

    class HoldView {
        SimpleDraweeView img;
        TextView txt_name;
        TextView txt_desc;
        ImageView line;
        //   RelativeLayout rel_theme_click;
        ImageView line_long;
    }
}
