package com.thel.modules.live.surface.watch.horizontal;

import com.thel.base.BasePresenter;
import com.thel.base.BaseView;
import com.thel.bean.LiveAdInfoBean;
import com.thel.modules.live.bean.LiveRoomBean;
import com.thel.modules.live.bean.LiveUserCardBean;
import com.thel.modules.live.bean.SoftEnjoyBean;

public class LiveWatchHorizontalContract {

    public interface Presenter extends BasePresenter {

        /**
         * 获取直播间信息
         *
         * @param userId
         * @param liveId
         */
        void getLiveRoomDetail(String userId, String liveId);

        /**
         * 获取名片
         *
         * @param userId
         */
        void getLiveUserCard(String userId, int type);

        /**
         * 获取礼物列表
         */
        void getLiveGiftList();

        void getLiveAdInfo(String url);

        void getSingleGiftDetail(int id);

        void initChatListUpdateTimer();

    }

    public interface View extends BaseView<LiveWatchHorizontalContract.Presenter> {

        boolean isActive();

        void initPlayer(LiveRoomBean data);

        void initUI(LiveRoomBean data);

        void initChat(LiveRoomBean data);

        void showBlockAlertDialog();

        void requestFinish();

        void updateChatList();

        void showLiveUserCard(LiveUserCardBean liveUserCardBean);

        void showLiveAnchorUserCard(LiveUserCardBean liveUserCardBean);

        void showLiveGiftList(SoftEnjoyBean data);

        void showAdView(LiveAdInfoBean liveAdInfoBean);

    }
}
