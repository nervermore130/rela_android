package com.thel.modules.theme;

import android.content.Intent;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.bean.theme.ThemeClassBean;
import com.thel.constants.TheLConstants;
import com.thel.modules.welcome.WelcomeActivity;
import com.thel.utils.ShareFileUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ThemeClassifyActivity extends BaseActivity {

    @BindView(R.id.theme_classify)
    ListView listView;

    private ThemeListAdapter adapter;

    private ArrayList<ThemeClassBean> listPlus = new ArrayList<>();

    private Map<Integer, Integer> themerResMap = new HashMap<Integer, Integer>() {
        {
         /*   put(0, R.mipmap.icn_topic_heart);
            put(1, R.mipmap.icn_topic_hand);
            put(2, R.mipmap.icn_dog);
            put(3, R.mipmap.icn_topic_smile);
            put(4, R.mipmap.icn_topic_leaves);
            put(5, R.mipmap.icn_topic_literary);
            put(6, R.mipmap.icn_topic_comeout);
            put(6, R.mipmap.icn_topic_work);
            put(7, R.mipmap.icn_30);
            put(8, R.mipmap.icn_topic_bi);
            put(9, R.mipmap.icn_topic_recourse);
            put(10, R.mipmap.icn_topic_other);*/

            put(0, R.mipmap.icn_topic_heart);
            put(1, R.mipmap.icn_topic_hand);
            put(2, R.mipmap.icn_topic_smile);
            put(3, R.mipmap.icn_topic_leaves);
            put(4, R.mipmap.icn_topic_literary);
            put(5, R.mipmap.icn_topic_comeout);
            put(6, R.mipmap.icn_topic_work);
            put(7, R.mipmap.icn_topic_bi);
            put(8, R.mipmap.icn_topic_recourse);
            put(9, R.mipmap.icn_topic_other);
            put(10, R.mipmap.icn_topic_dog);
            put(11, R.mipmap.icn_topic_30);

        }
    };

    public static Map<Integer, ThemeClassBean> themeClassBeanMap = new HashMap<Integer, ThemeClassBean>() {
        {
           /* put(0, new ThemeClassBean("hot", TheLApp.getContext().getResources().getString(R.string.theme_select_title), -1, TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify_info)[0]));
            put(1, new ThemeClassBean("love", TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify)[0], 0, TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify_info)[0]));
            //put(2, new ThemeClassBean("sex", TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify)[1], 1, TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify_info)[1]));
            put(3, new ThemeClassBean("joke", TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify)[3], 3, TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify_info)[2]));
            put(2, new ThemeClassBean("life", TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify)[2], 2, TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify_info)[3]));
            put(4, new ThemeClassBean("art", TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify)[5], 5, TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify_info)[4]));
            put(5, new ThemeClassBean("out", TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify)[6], 6, TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify_info)[5]));
            put(6, new ThemeClassBean("job", TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify)[8], 8, TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify_info)[6]));
            //  put(8, new ThemeClassBean("bisexual", TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify)[7], 7, TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify_info)[7]));
            put(8, new ThemeClassBean("30+", TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify)[7], 7, TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify_info)[7]));
            put(7, new ThemeClassBean("help", TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify)[8], 8, TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify_info)[8]));
            put(9, new ThemeClassBean("other", TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify)[9], 9, TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify_info)[9]));
            put(10, new ThemeClassBean("notalone", TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify)[4], 4, TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify_info)[4]));
           */
            put(0, new ThemeClassBean("hot", TheLApp.getContext().getResources().getString(R.string.theme_select_title), -1, TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify_info)[0]));
            put(1, new ThemeClassBean("love", TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify)[0], 0, TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify_info)[0]));
            //put(2, new ThemeClassBean("sex", TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify)[1], 1, TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify_info)[1]));
            put(4, new ThemeClassBean("joke", TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify)[2], 2, TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify_info)[2]));
            put(2, new ThemeClassBean("life", TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify)[3], 3, TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify_info)[3]));
            put(5, new ThemeClassBean("art", TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify)[4], 4, TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify_info)[4]));
            put(6, new ThemeClassBean("out", TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify)[5], 5, TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify_info)[5]));
            put(7, new ThemeClassBean("job", TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify)[6], 6, TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify_info)[6]));
            //  put(8, new ThemeClassBean("bisexual", TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify)[7], 7, TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify_info)[7]));
            put(9, new ThemeClassBean("help", TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify)[8], 8, TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify_info)[8]));
            put(10, new ThemeClassBean("other", TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify)[9], 9, TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify_info)[9]));
            put(3, new ThemeClassBean("notalone", TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify)[10], 10, TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify_info)[10]));
            put(8, new ThemeClassBean("30+", TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify)[11], 11, TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify_info)[11]));

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_theme_classify);


        ButterKnife.bind(this);

        // 如果是从外部进入
        String action = getIntent().getAction();
        if (Intent.ACTION_VIEW.equals(action)) {
            if (!ShareFileUtils.getBoolean(ShareFileUtils.HAS_LOGGED, false)) {//如果没登陆
                startActivity(new Intent(this, WelcomeActivity.class));
                finish();
                return;
            }
        }

        initList();
        initData();
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void initList() {
        final View footerView = new View(this);
        footerView.setBackgroundColor(ContextCompat.getColor(this, R.color.gray));
        footerView.setLayoutParams(new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, 1));
        listView.addFooterView(footerView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //  TextView txt_name = (TextView) view.getTag(R.id.txt_name);
                // Log.d("txt_name", txt_name.getText() + "");
                Intent intent = new Intent();
                // final ThemeClassBean bean = (ThemeClassBean) parent.getAdapter().getItem(position);

                final ThemeClassBean themeClassBean = (ThemeClassBean) parent.getAdapter().getItem(position);
                intent.putExtra(TheLConstants.BUNDLE_KEY_THEME_LIST, themeClassBean);
                setResult(TheLConstants.RESULT_CODE_SELECT_TYPE, intent);
                finish();
            }
        });
    }

    private void initData() {

        final int size = themeClassBeanMap.size();
        for (int i = 1; i < size; i++) {
            ThemeClassBean bean = themeClassBeanMap.get(i);
            listPlus.add(bean);
        }
        // map.put("")

        if (adapter == null) {
            adapter = new ThemeListAdapter(this, listPlus, themerResMap);
            listView.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
        }
    }

    @OnClick(R.id.lin_back)
    void back() {
        finish();
    }
}
