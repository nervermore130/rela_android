package com.thel.modules.main.settings;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterPushImpl;
import com.thel.flutter.bridge.RfSBridgeHandlerFactory;
import com.thel.modules.main.MainActivity;
import com.thel.modules.main.settings.adapter.LanguageSettingAdapter;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.decoration.DefaultItemDivider;
import com.thel.utils.DeviceUtils;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 语言设置界面
 * Created by lingwei on 2017/10/30.
 */

public class LanguageSettingActivity extends BaseActivity implements LanguageSettingAdapter.LanguageChangedListener {

    private static final String TAG = "LanguageSettingActivity";

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.lin_back)
    LinearLayout linBack;
    @BindView(R.id.txt_title)
    TextView txtTitle;
    @BindView(R.id.btn_save)
    TextView btnSave;
    @BindView(R.id.title_layout)
    RelativeLayout titleLayout;
    @BindView(R.id.recycleview)
    RecyclerView recycleview;
    private List<String> list = new ArrayList<>();//语言列表
    private int mCheckedPostion = -1;
    private int currentLanguage = -1;
    private LanguageSettingAdapter mAdapter;//adapter

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.language_setting_activity_layout);
        ButterKnife.bind(this);
        initList();
        getCurrentLanguage();//获取当前的语言
        initDataAdapter();
        setListener();
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void initDataAdapter() {
        txtTitle.setText(getString(R.string.setting_activity_language));
        mAdapter = new LanguageSettingAdapter(this, list, currentLanguage);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(RecyclerView.VERTICAL);
        recycleview.setHasFixedSize(true);
        recycleview.addItemDecoration(new DefaultItemDivider(this, LinearLayoutManager.VERTICAL, R.color.gray, 1, true, true));
        recycleview.setLayoutManager(manager);
        recycleview.setAdapter(mAdapter);
    }

    private void initList() {
        list.clear();
        for (LanguageBean bean : languages) {
            list.add(bean.value);
        }
    }

    private void setListener() {

        mAdapter.setLanguageChangedListener(this);
        mAdapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, final int position) {
                recycleview.post(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.notifyDataSetChanged(position);
                    }
                });
            }
        });
    }

    private void save() {
        if (mCheckedPostion == currentLanguage || mCheckedPostion < 0) {//如果语言没切换
            finish();
            return;
        }
        Resources resources = getResources();
        Configuration conf = resources.getConfiguration();
        DisplayMetrics dm = getResources().getDisplayMetrics();
        String mkey = languages[mCheckedPostion].key;
        updateLanguageShare(mkey);//更新缓存
        if (mkey.equals(TheLConstants.LANGUAGE_ZH_CN)) {//简体中文
            conf.locale = Locale.SIMPLIFIED_CHINESE;
        } else if (mkey.equals(TheLConstants.LANGUAGE_ZH_TW)) {//繁体中文 台湾
            conf.locale = Locale.TAIWAN;
        } else if (mkey.equals(TheLConstants.LANGUAGE_ZH_HK)) {//4.1.4新增香港繁体
            conf.locale = new Locale("zh", "HK");
        } else if (mkey.equals(TheLConstants.LANGUAGE_EN_US)) {//英文（美）
            conf.locale = Locale.US;
        } else if (mkey.equals(TheLConstants.LANGUAGE_FR_RFR)) {//法语（法国）
            conf.locale = Locale.FRANCE;
        } else if (mkey.equals(TheLConstants.LANGUAGE_TH_RTH)) {//泰语
            conf.locale = new Locale("th", "TH");
        } else if (mkey.equals(TheLConstants.LANGUAGE_ES)) {//西班牙语
            conf.locale = new Locale("es");
        } else if (mkey.equals(TheLConstants.LANGUAGE_JA_JP)) {//4.1.4 新增日语
            conf.locale = Locale.JAPAN;
        }

        L.d(TAG, " conf.locale.getLanguage() : " + conf.locale.getLanguage());

        L.d(TAG, " conf.locale.getCountry() : " + conf.locale.getCountry());

        L.d(TAG, " conf.locale : " + conf.locale.toString());

        L.d(TAG, "  DeviceUtils.getLanguageStr() : " + DeviceUtils.getLanguageStr());

        String local = conf.locale.getLanguage() + "-" + conf.locale.getCountry();

        L.d(TAG, " local : " + local);

        ShareFileUtils.setString(ShareFileUtils.localLanguage, local);

        RfSBridgeHandlerFactory.getInstance().getFlutterPushImpl().pushLanguage(local);

        getResources().updateConfiguration(conf, dm);

        restartApp();//重启热拉en
    }

    /**
     * 更新数据库语言
     *
     * @param mkey
     */
    private void updateLanguageShare(String mkey) {
        if (!TextUtils.isEmpty(mkey)) {
            SharedPrefUtils.setString(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.LANGUAGE_SETTING, mkey);
        }
    }

    /**
     * 获取当前语言
     */
    private void getCurrentLanguage() {
        String mLanguageKey = SharedPrefUtils.getString(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.LANGUAGE_SETTING, "");//获取缓存语言，userid+_+key
        if (!TextUtils.isEmpty(mLanguageKey)) {
            getCurrentLanguagePosition(mLanguageKey);
        }
        if (currentLanguage == -1) {//如果数据库没有数据，获取系统语言
            mLanguageKey = "";
            final String language = DeviceUtils.getLanguage();
            if (language.equals(getLanguageStr(Locale.SIMPLIFIED_CHINESE))) {//简体中文
                mLanguageKey = TheLConstants.LANGUAGE_ZH_CN;
            } else if (language.equals(getLanguageStr(Locale.TRADITIONAL_CHINESE))) {//繁体中文，台湾
                mLanguageKey = TheLConstants.LANGUAGE_ZH_TW;
            } else if (language.equals(getLanguageStr(new Locale("zh", "HK")))) {//4.1.4 新增 香港繁体
                mLanguageKey = TheLConstants.LANGUAGE_ZH_HK;
            } else if (language.equals(getLanguageStr(Locale.US))) {//英语（美）
                mLanguageKey = TheLConstants.LANGUAGE_EN_US;
            } else if (language.equals(getLanguageStr(Locale.FRANCE))) {//法语（法国）
                mLanguageKey = TheLConstants.LANGUAGE_FR_RFR;
            } else if (language.equals(getLanguageStr(new Locale("th", "TH")))) {//泰语
                mLanguageKey = TheLConstants.LANGUAGE_TH_RTH;
            } else if (language.equals(getLanguageStr(new Locale("es")))) {//西班牙语
                mLanguageKey = TheLConstants.LANGUAGE_ES;
            } else if (language.equals(getLanguageStr(Locale.JAPAN))) {//4.1.4新增 日语
                mLanguageKey = TheLConstants.LANGUAGE_JA_JP;
            }
            if (!TextUtils.isEmpty(mLanguageKey)) {
                getCurrentLanguagePosition(mLanguageKey);
            }
        }
    }

    /**
     * 获取语言字符串
     *
     * @param locale
     * @return
     */
    protected String getLanguageStr(Locale locale) {
        return DeviceUtils.getLanguageStr(locale);
    }

    /**
     * 获取当前语言位置
     *
     * @param mLanguageKey 语言key
     */
    private void getCurrentLanguagePosition(String mLanguageKey) {
        for (int i = 0; i < languages.length; i++) {
            if (mLanguageKey.equals(languages[i].key)) {
                mCheckedPostion = currentLanguage = i;//获得位置
                break;
            }
        }
    }

    /**
     * 重启热拉
     */
    private void restartApp() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(TheLConstants.BUNDLE_KEY_WHERE_TO_GO, TheLConstants.MainFragmentPageConstants.FRAGMENT_ME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    /**
     * 语言种类
     */
    public LanguageBean[] languages = {new LanguageBean(TheLConstants.LANGUAGE_ZH_CN, "简体中文"), //
            new LanguageBean(TheLConstants.LANGUAGE_ZH_TW, "繁體中文(台湾)"), //
            new LanguageBean(TheLConstants.LANGUAGE_ZH_HK, "繁體中文(香港)"), //4.1.4新增香港繁体
            new LanguageBean(TheLConstants.LANGUAGE_EN_US, "English"),//英语
            new LanguageBean(TheLConstants.LANGUAGE_FR_RFR, "Français"),//法语
            new LanguageBean(TheLConstants.LANGUAGE_TH_RTH, "ภาษาไทย"),//泰语
            new LanguageBean(TheLConstants.LANGUAGE_ES, "Español"),//西班牙语
            new LanguageBean(TheLConstants.LANGUAGE_JA_JP, "日本語")//4.1.4 新增日语
    };

    @Override
    public void languageChanged(int position) {
        mCheckedPostion = position;
    }

    @OnClick(R.id.lin_back)
    public void onImgBackClicked() {
        finish();
    }

    @OnClick(R.id.btn_save)
    public void onBtnSaveClicked() {
        save();
    }

    class LanguageBean {
        public String key;
        public String value;

        public LanguageBean(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }
}
