package com.thel.modules.live.surface.watch

import android.os.Message
import android.text.TextUtils
import com.thel.bean.live.LiveMultiSeatBean
import com.thel.bean.live.MultiSpeakersBean
import com.thel.modules.live.LiveCodeConstants
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_ANCHOR_LINK_MIC_REQUEST
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_ANCHOR_LIVE_NETWORK_WARNING_NOTIFY
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_ANCHOR_PK_CANCEL_NOTICE
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_ANCHOR_PK_GEM_NOTICE
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_ANCHOR_PK_HANGUP_NOTICE
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_ANCHOR_PK_REQUEST_NOTICE
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_ANCHOR_PK_REQUEST_TIME_OUT
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_ANCHOR_PK_RESPONSE_NOTICE
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_ANCHOR_PK_START_NOTICE
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_ANCHOR_PK_STOP_NOTICE
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_ANCHOR_PK_SUMMARY_NOTICE
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_DANMU_RECEIVE_MSG
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_ENCOUNTER_SB
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_ENCOUNTER_SPEAKERS
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_END_ENCOUNTER
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_GIFT_RECEIVE_MSG
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_GUEST_GIFT
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_JOIN_USER
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_JOIN_VIP_USER
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_MIC_ADD
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_MIC_AGREE
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_MIC_CANCEL
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_MIC_DEL
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_MIC_OFF
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_MIC_ON
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_MIC_REQ
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_REFRESH_MSGS
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_REFRESH_SEAT
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_START_ENCOUNTER
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_STOP_SHOW
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_TOP_GIFT
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_TOP_TODAY
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_UPDATE_AUDIENCE_COUNT
import com.thel.modules.live.bean.*
import com.thel.modules.live.interfaces.IChatMessageDelegate
import com.thel.modules.live.utils.LinkMicOrPKRefuseUtils
import com.thel.modules.live.utils.LiveGIOPush
import com.thel.modules.live.utils.LiveUtils
import com.thel.modules.live.view.expensive.TopGiftBean
import com.thel.utils.GsonUtils
import com.thel.utils.L
import org.json.JSONException
import org.json.JSONObject

class AnchorMessageDelegateImpl : IChatMessageDelegate {

    private val tagName: String = "AnchorMessageDelegateImpl"

    private var observer: LiveWatchObserver? = null

    private var liveRoomBean: LiveRoomBean? = null

    override fun init(observer: LiveWatchObserver, liveRoomBean: LiveRoomBean) {
        this.observer = observer
        this.liveRoomBean = liveRoomBean
    }

    override fun messageDelegate(code: String, payload: String) {

        L.d(tagName, " code : $code , payload : $payload")

        L.d(tagName, "observer iLive : ${observer?.iLive}")

        if (!TextUtils.isEmpty(code)) {
            try {

                val liveRoomMsgBean: LiveRoomMsgBean?
                val msg: Message

                when (code) {
                    LiveRoomMsgBean.TYPE_MSG -> {
                        liveRoomMsgBean = GsonUtils.getObject(payload, LiveRoomMsgBean::class.java)
                        if (LiveRoomMsgBean.TYPE_MSG == liveRoomMsgBean!!.type || LiveRoomMsgBean.TYPE_NOTICE == liveRoomMsgBean.type) {
                            observer?.addMsg(liveRoomMsgBean)
                            observer?.sendEmptyMessage(UI_EVENT_REFRESH_MSGS)
                        }
                    }
                    LiveRoomMsgBean.TYPE_SYS_MSG -> {
                        liveRoomMsgBean = GsonUtils.getObject(payload, LiveRoomMsgBean::class.java)
                        if (LiveRoomMsgBean.TYPE_JOIN == liveRoomMsgBean!!.type || LiveRoomMsgBean.TYPE_BANED == liveRoomMsgBean.type || LiveRoomMsgBean.TYPE_LEAVE == liveRoomMsgBean.type) {
                            observer?.addMsg(liveRoomMsgBean)
                            observer?.sendEmptyMessage(UI_EVENT_REFRESH_MSGS)
                        }
                    }
                    LiveRoomMsgBean.TYPE_UPDATE -> {
                        val responseGemBean = GsonUtils.getObject(payload, ResponseGemBean::class.java)
                        msg = Message.obtain()
                        msg.what = UI_EVENT_UPDATE_AUDIENCE_COUNT
                        msg.obj = responseGemBean
                        observer?.sendMessage(msg)
                    }
                    LiveRoomMsgBean.TYPE_CLOSE -> observer?.sendEmptyMessage(UI_EVENT_STOP_SHOW)
                    LiveRoomMsgBean.TYPE_GIFT_MSG, LiveRoomMsgBean.CODE_SPECIAL_GIFT -> {
                        msg = Message.obtain()
                        msg.what = UI_EVENT_GIFT_RECEIVE_MSG
                        val bundle = msg.data
                        bundle.putString("payload", payload)
                        bundle.putString("code", code)
                        msg.data = bundle
                        observer?.sendMessage(msg)
                    }
                    LiveRoomMsgBean.TYPE_DANMU_MSG -> {
                        msg = Message.obtain()
                        msg.what = UI_EVENT_DANMU_RECEIVE_MSG
                        msg.obj = payload
                        observer?.sendMessage(msg)

                        observer?.addMsg(GsonUtils.getObject(payload, LiveRoomMsgBean::class.java))
                        observer?.sendEmptyMessage(UI_EVENT_REFRESH_MSGS)
                    }
                    LiveRoomMsgBean.TYPE_VISIT -> {
                        liveRoomMsgBean = GsonUtils.getObject(payload, LiveRoomMsgBean::class.java)
                        if (LiveRoomMsgBean.TYPE_JOIN == liveRoomMsgBean!!.type) {//用户加入房间
                            msg = Message.obtain()
                            msg.what = UI_EVENT_JOIN_USER
                            msg.obj = liveRoomMsgBean
                            observer?.sendMessage(msg)
                        }
                    }
                    LiveRoomMsgBean.TYPE_VIP_VISIT -> {
                        liveRoomMsgBean = GsonUtils.getObject(payload, LiveRoomMsgBean::class.java)
                        if (LiveRoomMsgBean.TYPE_JOIN == liveRoomMsgBean!!.type) {//用户加入房间
                            msg = Message.obtain()
                            msg.what = UI_EVENT_JOIN_VIP_USER
                            msg.obj = liveRoomMsgBean
                            observer?.sendMessage(msg)
                        }
                    }
                    LiveRoomMsgBean.TYPE_LIVE_PK -> receiverPkNoticeMsg(payload)
                    LiveRoomMsgBean.TYPE_CONNECT_MIC -> {
                        val liveRoomMsgConnectMicBean = GsonUtils.getObject(payload, LiveRoomMsgConnectMicBean::class.java)
                        msg = Message.obtain()
                        msg.what = UI_EVENT_ANCHOR_LINK_MIC_REQUEST
                        msg.obj = liveRoomMsgConnectMicBean
                        observer?.sendMessage(msg)
                    }
                    LiveRoomMsgBean.NETWORK_WARNING_NOTIFY//观众没有
                    -> {
                        liveRoomMsgBean = GsonUtils.getObject(payload, LiveRoomMsgBean::class.java)
                        if (liveRoomMsgBean != null && !TextUtils.isEmpty(liveRoomMsgBean.content)) {
                            msg = Message.obtain()
                            msg.what = UI_EVENT_ANCHOR_LIVE_NETWORK_WARNING_NOTIFY
                            msg.obj = liveRoomMsgBean.content
                            observer?.sendMessage(msg)
                        }
                    }
                    LiveRoomMsgBean.TYPE_FOLLOW -> {
                        liveRoomMsgBean = LiveUtils.getFollowMsgBean(payload)
                        if (liveRoomMsgBean != null && !TextUtils.isEmpty(liveRoomMsgBean.content)) {
                            observer?.addMsg(liveRoomMsgBean)
                            observer?.sendEmptyMessage(UI_EVENT_REFRESH_MSGS)
                        }
                    }
                    LiveRoomMsgBean.TYPE_RECOMM -> {
                        liveRoomMsgBean = LiveUtils.getRecommendMsgBean(payload)
                        if (liveRoomMsgBean != null && !TextUtils.isEmpty(liveRoomMsgBean.content)) {
                            observer?.addMsg(liveRoomMsgBean)
                            observer?.sendEmptyMessage(UI_EVENT_REFRESH_MSGS)
                        }
                    }
                    LiveRoomMsgBean.TYPE_SHARETO -> {
                        liveRoomMsgBean = LiveUtils.getSharetToMsgBean(payload)
                        if (liveRoomMsgBean != null && !TextUtils.isEmpty(liveRoomMsgBean.content)) {
                            observer?.addMsg(liveRoomMsgBean)
                            observer?.sendEmptyMessage(UI_EVENT_REFRESH_MSGS)
                        }
                    }
                    LiveRoomMsgBean.TYPE_GIFTCOMBO -> if (liveRoomBean?.softEnjoyBean != null) {

                        liveRoomMsgBean = LiveUtils.getSendGiftMsgBean(payload, liveRoomBean?.softEnjoyBean)
                        if (liveRoomMsgBean != null && !TextUtils.isEmpty(liveRoomMsgBean.content)) {
                            observer?.addMsg(liveRoomMsgBean)
                            observer?.sendEmptyMessage(UI_EVENT_REFRESH_MSGS)
                        }
                    }
                    LiveRoomMsgBean.TYPE_TOPGIFT -> {
                        msg = Message.obtain()
                        msg.what = UI_EVENT_TOP_GIFT
                        msg.obj = GsonUtils.getObject(payload, TopGiftBean::class.java)
                        observer?.sendMessage(msg)
                    }
                    LiveRoomMsgBean.TYPE_AUDIO_BROADCAST_CODE -> {
                        val seat = GsonUtils.getObject(payload, LiveMultiSeatBean::class.java)
                        msg = Message.obtain()
                        when (seat!!.method) {
                            LiveRoomMsgBean.TYPE_METHOD_ONSEAT -> {
                                msg.obj = seat
                                msg.what = UI_EVENT_REFRESH_SEAT
                                observer?.sendMessage(msg)
                                LiveGIOPush.getInstance().setAudienceOnSeatCount()
                            }
                            LiveRoomMsgBean.TYPE_METHOD_OFFSEAT -> {
                                msg.obj = seat
                                msg.what = UI_EVENT_REFRESH_SEAT
                                observer?.sendMessage(msg)
                            }
                            LiveRoomMsgBean.TYPE_METHOD_MUTE -> {
                                msg.obj = seat
                                msg.what = UI_EVENT_REFRESH_SEAT
                                observer?.sendMessage(msg)
                            }
                            LiveRoomMsgBean.TYPE_METHOD_GUEST_GIFT -> {
                                msg.obj = seat
                                msg.what = UI_EVENT_GUEST_GIFT
                                observer?.sendMessage(msg)
                            }
                            LiveRoomMsgBean.TYPE_METHOD_START_ENCOUNTER//观众没有
                            -> {
                                msg.obj = seat
                                msg.what = UI_EVENT_START_ENCOUNTER
                                observer?.sendMessage(msg)
                            }
                            LiveRoomMsgBean.TYPE_METHOD_END_ENCOUNTER//观众没有
                            -> {
                                msg.obj = seat
                                msg.what = UI_EVENT_END_ENCOUNTER
                                observer?.sendMessage(msg)
                            }
                            LiveRoomMsgBean.TYPE_METHOD_ENCOUNTERSB -> {
                                msg.obj = seat
                                msg.what = UI_EVENT_ENCOUNTER_SB
                                observer?.sendMessage(msg)
                            }
                            LiveRoomMsgBean.TYPE_METHOD_UPLOAD_VOLUMN//观众没有
                            -> {
                                msg.obj = GsonUtils.getObject(payload, MultiSpeakersBean::class.java)
                                msg.what = UI_EVENT_ENCOUNTER_SPEAKERS
                                observer?.sendMessage(msg)
                            }
                            LiveRoomMsgBean.TYPE_METHOD_MIC_ON -> {
                                msg.what = UI_EVENT_MIC_ON
                                observer?.sendMessage(msg)
                            }
                            LiveRoomMsgBean.TYPE_METHOD_MIC_OFF -> {
                                msg.what = UI_EVENT_MIC_OFF
                                observer?.sendMessage(msg)
                            }
                            LiveRoomMsgBean.TYPE_METHOD_MIC_ADD -> {
                                msg.obj = GsonUtils.getObject(payload, LiveMultiSeatBean.CoupleDetail::class.java)
                                msg.what = UI_EVENT_MIC_ADD
                                observer?.sendMessage(msg)
                            }
                            LiveRoomMsgBean.TYPE_METHOD_MIC_DEL -> {
                                msg.obj = seat
                                msg.what = UI_EVENT_MIC_DEL
                                observer?.sendMessage(msg)
                            }
                            LiveRoomMsgBean.TYPE_METHOD_MIC_REQ -> {
                                msg.what = UI_EVENT_MIC_REQ
                                observer?.sendMessage(msg)
                            }
                            LiveRoomMsgBean.TYPE_METHOD_MIC_CANCEL -> {
                                msg.what = UI_EVENT_MIC_CANCEL
                                observer?.sendMessage(msg)
                            }
                            LiveRoomMsgBean.TYPE_METHOD_MIC_AGREE -> {
                                msg.obj = seat
                                msg.what = UI_EVENT_MIC_AGREE
                                observer?.sendMessage(msg)
                            }
                        }
                    }
                    LiveRoomMsgBean.TYPE_TOP_TODAY -> {
                        val message = Message.obtain()
                        message.what = UI_EVENT_TOP_TODAY
                        message.obj = GsonUtils.getObject(payload, TopTodayBean::class.java)
                        observer?.sendMessage(message)
                    }
                    LiveRoomMsgBean.TYPE_UPGRADE_EFFECTS -> {
                        observer?.sendMessage(getHandlerMessage(LiveCodeConstants.UI_EVENT_UPGRADE_EFFECTS, GsonUtils.getObject(payload, LiveRoomMsgBean::class.java)))
                    }
                    LiveRoomMsgBean.TYPE_ALL_UPGRADE_EFFECTS -> {
                        observer?.sendMessage(getHandlerMessage(LiveCodeConstants.UI_EVENT_ALL_UPGRADE_EFFECTS, GsonUtils.getObject(payload, LiveRoomMsgBean::class.java)))

                    }
                    else -> {

                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

    }

    private fun getHandlerMessage(what: Int, obj: Any): Message {
        val msg = Message.obtain()
        msg.what = what
        msg.obj = obj
        return msg
    }

    /**
     * 收到有关Pk的消息
     * 属于广播通知收到的消息
     *
     *
     * 接收方 request->start->summary->hangup->stop
     *
     *
     * 发起方 response->start->stop
     *
     * @param payload
     */
    private fun receiverPkNoticeMsg(payload: String) {
        if (TextUtils.isEmpty(payload)) {
            return
        }
        try {
            val obj = JSONObject(payload)

            val method = obj.optString("method")

            L.d(tagName, " receiverPkNoticeMsg method : $method")

            L.d(tagName, " receiverPkNoticeMsg payload : $payload")

            when (method) {
                LiveRoomMsgBean.PK_NOTICE_METHOD_REQUEST -> receivePkRequest(payload)
                LiveRoomMsgBean.PK_NOTICE_METHOD_RESPONSE -> receivePkResponse(payload)
                LiveRoomMsgBean.PK_NOTICE_METHOD_START -> receiverPkStartMsg(payload)
                LiveRoomMsgBean.PK_NOTICE_METHOD_CANCEL -> receivePkCancelMsg(payload)
                LiveRoomMsgBean.PK_NOTICE_METHOD_HANGUP -> receiveHangupMsg(payload)
                LiveRoomMsgBean.PK_NOTICE_METHOD_TIMEOUT -> receivePkTimeoutMsg(payload)
                LiveRoomMsgBean.PK_NOTICE_METHOD_SUMMARY -> receivePkSummaryMsg(payload)
                LiveRoomMsgBean.PK_NOTICE_METHOD_STOP -> receivePkStopMsg(payload)
                LiveRoomMsgBean.PK_NOTICE_METHOD_PKGEM -> receiverPkGiftMsg(payload)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }

    }

    /**
     * 收到Pk请求
     *
     * @param payload
     */
    private fun receivePkRequest(payload: String) {
        if (TextUtils.isEmpty(payload)) {
            return
        }
        val bean = GsonUtils.getObject(payload, LivePkRequestNoticeBean::class.java)
        val msg = Message.obtain()
        msg.what = UI_EVENT_ANCHOR_PK_REQUEST_NOTICE
        msg.obj = bean
        observer?.sendMessage(msg)
    }

    /**
     * 发起PK方收到对方回应
     *
     * @param payload
     */
    private fun receivePkResponse(payload: String) {
        if (TextUtils.isEmpty(payload)) {
            return
        }
        val bean = GsonUtils.getObject(payload, LivePkResponseNoticeBean::class.java)
        val msg = Message.obtain()
        msg.what = UI_EVENT_ANCHOR_PK_RESPONSE_NOTICE
        msg.obj = bean
        observer?.sendMessage(msg)
        if (bean!!.result == LivePkResponseNoticeBean.LIVE_PK_RESPONSE_RESULT_NO) {//如果被拒绝，则存入sp
            //            LivePkUtils.saveOnePkRefuser(bean.fromUserId);
            LinkMicOrPKRefuseUtils.addPkRefuse(bean.fromUserId)
        }
    }

    /**
     * PK开始时 服务器发送PK开始通知给双方直播室内的所有人
     *
     * @param payload
     */
    private fun receiverPkStartMsg(payload: String) {
        if (TextUtils.isEmpty(payload)) {
            return
        }
        val bean = GsonUtils.getObject(payload, LivePkStartNoticeBean::class.java)
        val msg = Message.obtain()
        msg.what = UI_EVENT_ANCHOR_PK_START_NOTICE
        msg.obj = bean
        observer?.sendMessage(msg)
    }

    /**
     * 收到取消PK取消的请求
     *
     * @param payload
     */
    private fun receivePkCancelMsg(payload: String) {
        if (TextUtils.isEmpty(payload)) {
            return
        }
        val msg = Message.obtain()
        msg.what = UI_EVENT_ANCHOR_PK_CANCEL_NOTICE
        msg.obj = payload
        observer?.sendMessage(msg)
    }

    /**
     * 被挂断方收到服务器的PK挂断消息
     *
     * @param payload
     */
    private fun receiveHangupMsg(payload: String) {
        if (TextUtils.isEmpty(payload)) {
            return
        }
        val bean = GsonUtils.getObject(payload, LivePkHangupBean::class.java)
        val msg = Message.obtain()
        msg.what = UI_EVENT_ANCHOR_PK_HANGUP_NOTICE
        msg.obj = bean
        observer?.sendMessage(msg)
    }

    /**
     * 收到Pk超时消息
     *
     * @param payload
     */
    private fun receivePkTimeoutMsg(payload: String) {
        if (TextUtils.isEmpty(payload)) {
            return
        }
        try {
            val obj = JSONObject(payload)
            val toUserId = obj.optString("toUserId")
            val msg = Message.obtain()
            msg.what = UI_EVENT_ANCHOR_PK_REQUEST_TIME_OUT
            msg.obj = toUserId
            observer?.sendMessage(msg)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

    }

    /**
     * 进入总结阶段 服务器发送 总结剩余时间 和 双方助攻前三名 给双方直播室内的所有人:
     *
     * @param payload
     */
    private fun receivePkSummaryMsg(payload: String) {
        if (TextUtils.isEmpty(payload)) {
            return
        }
        val bean = GsonUtils.getObject(payload, LivePkSummaryNoticeBean::class.java)
        val msg = Message.obtain()
        msg.what = UI_EVENT_ANCHOR_PK_SUMMARY_NOTICE
        msg.obj = bean
        observer?.sendMessage(msg)
    }

    /**
     * PK结束时 服务器发送 PK结束 和 双方助攻前三名 通知给双方直播室内的所有人
     *
     * @param payload
     */
    private fun receivePkStopMsg(payload: String) {
        if (TextUtils.isEmpty(payload)) {
            return
        }
        val msg = Message.obtain()
        msg.what = UI_EVENT_ANCHOR_PK_STOP_NOTICE
        msg.obj = payload
        observer?.sendMessage(msg)
    }

    /**
     * 主播收到礼物时 服务器发送主播软妹币增量通知给双方直播室内的所有人:
     *
     * @param payload
     */
    private fun receiverPkGiftMsg(payload: String) {
        if (TextUtils.isEmpty(payload)) {
            return
        }
        val bean = GsonUtils.getObject(payload, LivePkGemNoticeBean::class.java)
        val msg = Message.obtain()
        msg.what = UI_EVENT_ANCHOR_PK_GEM_NOTICE
        msg.obj = bean
        observer?.sendMessage(msg)
    }

}