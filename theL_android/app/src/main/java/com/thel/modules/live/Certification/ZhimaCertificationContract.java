package com.thel.modules.live.Certification;

import com.thel.base.BasePresenter;
import com.thel.base.BaseView;
import com.thel.modules.live.bean.ZmxyAuthBean;

/**
 * Created by lingwei on 2018/3/19.
 */

public class ZhimaCertificationContract {
    interface Presenter extends BasePresenter {

        void postZmAuthUrl(String userName, String idCardNumber, String callback);

    }

    interface View extends BaseView<ZhimaCertificationContract.Presenter> {

        void getAuthUrl(ZmxyAuthBean data);

    }
}
