package com.thel.modules.main.nearby.nearbypeople;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.RfSBridgeHandlerFactory;
import com.thel.modules.main.me.bean.LogInfoBean;
import com.thel.modules.main.me.match.eventcollect.collect.LiveLogUtils;
import com.thel.modules.main.me.match.eventcollect.collect.MatchLogUtils;
import com.thel.modules.main.nearby.NearbyFilterActivity;
import com.thel.modules.main.nearby.NearbyPeopleFragment;
import com.thel.ui.dialog.ActionSheetDialog;
import com.thel.ui.widget.CommonTitleBar;
import com.thel.ui.widget.OnMoreClickListener;
import com.thel.utils.FireBaseUtils;
import com.thel.utils.L;
import com.thel.utils.PhoneUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.Utils;

import org.jetbrains.annotations.NotNull;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

public class NearbyPeopleActivity extends BaseActivity {

    private static final String TAG = "NearbyPeopleActivity";

    private CommonTitleBar commonTitleBar;

    private NearbyPeopleFragment nearbyPeopleFragment;
    private String pageId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nearby_people);

        commonTitleBar = findViewById(R.id.commonTitleBar);

        commonTitleBar.setMOnMoreClickListener(new OnMoreClickListener() {
            @Override public void onMoreClick(@NotNull View view) {
                showMenu();
            }
        });

        Bundle bundle = new Bundle();
        bundle.putString(TheLConstants.BUNDLE_KEY_MOMENT_ID, "");
        bundle.putString("tag", "nearby");

        nearbyPeopleFragment = NearbyPeopleFragment.newInstance(bundle);

        getSupportFragmentManager().beginTransaction().add(R.id.content, nearbyPeopleFragment, NearbyPeopleFragment.class.getName()).commit();

        if (!PhoneUtils.isGpsOpen() || ContextCompat.checkSelfPermission(TheLApp.context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(TheLApp.context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            commonTitleBar.isShowMoreBtn(false);
        } else {
            commonTitleBar.isShowMoreBtn(true);
        }
        pageId = Utils.getPageId();

        FireBaseUtils.uploadGoogle(TheLConstants.FireBaseConstant.VIEW_ITEM_PEOPLE_NEARBY,this);

    }

    private void showMenu() {
        String wantRole = ShareFileUtils.getString(ShareFileUtils.WANT_ROLE, "");
        if (wantRole.contains("2")) {//p
            wantRole = getString(R.string.filter_p);
        } else if (wantRole.contains("1")) {//t
            wantRole = getString(R.string.filter_t);
        } else if (wantRole.contains("3")) {//h
            wantRole = getString(R.string.filter_h);
        } else {
            wantRole = getString(R.string.filter_p);
        }
        String ages = "";
        int age = ShareFileUtils.getInt(ShareFileUtils.USER_AGE, 0);
        if (age == 0) {
            ages = getString(R.string.nine_five_after);
        } else {
            ages = getString(R.string.contemporary);
        }
        String single = getString(R.string.single_like);
        String filter = getString(R.string.nearby_diy_filter_title);
        String[] arrays = {wantRole, ages, single, filter};

        ActionSheetDialog actionSheetDialog = new ActionSheetDialog(NearbyPeopleActivity.this).builder().setCancelable(true).setCanceledOnTouchOutside(true)
                .setTitle(getString(R.string.nearby_screen_title));
        for (int i = 0; i < arrays.length; i++) {
            actionSheetDialog.addSheetItem(arrays[i], ActionSheetDialog.SheetItemColor.BLACK, new ActionSheetDialog.OnSheetItemClickListener() {
                @Override
                public void onClick(int which) {
                    Intent intent = new Intent(NearbyPeopleActivity.this, NearbyFilterActivity.class);
                    switch (which) {
                        case 1:
                            intent.putExtra(NearbyFilterActivity.FROM_TYPE, NearbyFilterActivity.FROM_TYPE_WANT_ROLE);
                            break;
                        case 2:
                            intent.putExtra(NearbyFilterActivity.FROM_TYPE, NearbyFilterActivity.FROM_TYPE_AGE);
                            break;
                        case 3:
                            intent.putExtra(NearbyFilterActivity.FROM_TYPE, NearbyFilterActivity.FROM_TYPE_SINGLE);
                            break;
                        case 4:
                            intent.putExtra(NearbyFilterActivity.FROM_TYPE, NearbyFilterActivity.FROM_TYPE_FILTER);
                            traceNearbyUserLog("filter");
                            break;
                        default:
                            intent.putExtra(NearbyFilterActivity.FROM_TYPE, NearbyFilterActivity.FROM_TYPE_WANT_ROLE);
                            break;
                    }
                    intent.putExtra("pageId",pageId);
                    startActivity(intent);

                }
            });
        }
        actionSheetDialog.show();
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        L.d(TAG, " requestCode : " + requestCode);

        L.d(TAG, " resultCode : " + resultCode);

        if (requestCode == TheLConstants.PERMISSIONS_REQUEST_READ_LOCATION) {
            if (nearbyPeopleFragment != null) {
                nearbyPeopleFragment.emptyData(false);
                if (nearbyPeopleFragment.presenter != null) {
                    nearbyPeopleFragment.presenter.getRefreshData();
                }
            }
            RfSBridgeHandlerFactory.getInstance().getFlutterPushImpl().pushLocationState();
            if (commonTitleBar != null) {
                commonTitleBar.isShowMoreBtn(true);
            }
        }
    }
    public void traceNearbyUserLog(String activity) {
        try {

            LogInfoBean logInfoBean = new LogInfoBean();
            logInfoBean.page = "around.list";
            logInfoBean.page_id = pageId;
            logInfoBean.activity = activity;

            MatchLogUtils.getInstance().addLog(logInfoBean);

        } catch (Exception e) {

        }
    }
}
