package com.thel.modules.others;

import com.thel.base.BaseDataBean;
import com.thel.bean.AdBean;
import com.thel.bean.user.VipConfigBean;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.utils.L;

import org.reactivestreams.Subscription;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by lingwei on 2017/9/25.
 */

public class VipConfigPresenter implements VipConfigContract.Presenter {
    private final VipConfigContract.View vipView;

    public VipConfigPresenter(VipConfigContract.View vipView) {
        this.vipView = vipView;
        vipView.setPresenter(this);

    }

    @Override
    public void processBusiness() {
        Flowable<VipConfigBean> flowable = RequestBusiness.getInstance().getVipConfig();
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<VipConfigBean>() {

            @Override
            public void onNext(VipConfigBean vipConfigBean) {

                super.onNext(vipConfigBean);
                vipView.getVipConfig(vipConfigBean);

            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                L.d(" onError : " + t.getMessage());
            }

            @Override
            public void onComplete() {

            }
        });
    }

    @Override
    public void setVipConfigs(final int liveHiding, final int vipHiding, final int hiding, final int incognito, final int followRemind, final int msgHiding,final int flag) {
        final int[] arr = {liveHiding, vipHiding, hiding, incognito, followRemind,msgHiding};
        arr[flag] = 1 - arr[flag];
        Flowable<BaseDataBean> flowable = RequestBusiness.getInstance().vipConfig(arr[0], arr[1], arr[2], arr[3], arr[4], arr[5],flag + "");
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {

            @Override
            public void onNext(BaseDataBean baseDataBean) {
                super.onNext(baseDataBean);
                vipView.getConfigResult(arr[0], arr[1], arr[2], arr[3], arr[4],arr[5]);
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
            }

            @Override
            public void onComplete() {
                //  vipView.closeLoading;

            }
        });
    }

    @Override
    public void loadAdvert() {
        Flowable<AdBean> flowable = RequestBusiness.getInstance().getAd();
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<AdBean>() {

            @Override
            public void onNext(AdBean adBean) {
                super.onNext(adBean);
                vipView.refreshAdArea(adBean);
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
            }

            @Override
            public void onComplete() {
                //  vipView.closeLoading;

            }
        });
    }


    @Override
    public void subscribe() {
        processBusiness();

    }

    @Override
    public void unSubscribe() {
    }

}
