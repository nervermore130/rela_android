package com.thel.modules.test;

import android.view.SurfaceView;

/**
 * Created by chad
 * Time 18/6/15
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */
public class UserInfo {
    public int uid;
    public SurfaceView view;
    public int x;
    public int y;
    public int width;
    public int height;
}
