package com.thel.modules.live.surface.watch;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.os.Handler;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.base.IntentFilterActivity;
import com.thel.bean.live.LiveRoomsBean;
import com.thel.constants.TheLConstants;
import com.thel.growingio.GrowingIoConstant;
import com.thel.modules.live.bean.LiveFollowerUsersBean;
import com.thel.modules.live.bean.LiveRoomBean;
import com.thel.modules.live.bean.LiveRoomNetBean;
import com.thel.modules.live.bean.LiveRoomsNetBean;
import com.thel.modules.live.in.FragmentCreatedCallback;
import com.thel.modules.live.in.LiveSwitchListener;
import com.thel.modules.live.utils.LiveUtils;
import com.thel.modules.live.view.LiveShowDragView;
import com.thel.modules.live.view.LiveShowViewGroup;
import com.thel.modules.main.messages.utils.PushUtils;
import com.thel.modules.welcome.WelcomeActivity;
import com.thel.network.InterceptorConsumer;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.InterceptorThrowableConsumer;
import com.thel.network.service.DefaultRequestService;
import com.thel.utils.AppInit;
import com.thel.utils.DialogUtil;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.L;
import com.thel.utils.ScreenUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.flutter.embedding.android.DrawableSplashScreen;
import io.flutter.embedding.android.FlutterView;
import io.flutter.embedding.android.SplashScreen;
import io.flutter.embedding.android.SplashScreenProvider;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import me.rela.rf_s_bridge.new_router.NewUniversalRouter;
import me.rela.rf_s_bridge.new_router.containers.NewFlutterFragment;
import me.rela.rf_s_bridge.new_router.interfaces.OnFragmentListener;
import me.rela.rf_s_bridge.router.BoostFlutterView;
import me.rela.rf_s_bridge.router.UniversalRouter;

/**
 * Created by lingwei on 2018/5/6.
 */

public class LiveWatchActivity extends BaseActivity implements OnFragmentListener, SplashScreenProvider {
    private final String TAG = LiveWatchActivity.class.getSimpleName();

    public static final String FROM_PAGE = "from_page";//来至
    public static final String FROM_PAGE_FRIEND_LIVE_HOME_PAGE = "from_page_friend_live_home_page";//从关注直播页面来
    public static final String FROM_PAGE_LIVE_ROOM_LIST_FRAGMENT = "from_page_live_room_list_fragment";//从直播列表中来
    public static final String FROM_PAGE_MOMENT = "from_page_moment";//从日志中来
    public static final String FROM_PAGE_NEW_USER = "from_page_new_user";//新人直播列表
    public static final String FROM_PAGE_PUSH = "from_page_push";//从日志中来
    public static final String FROM_PAGE_WEB = "from_page_web";//从日志中来
    public static final String TYPE_HOT = "hot";
    public static final String FROM_PAGE_OUT_WEB_WIDTH_USERID = "from_page_out_web_userid";//从外部浏览器进入
    public static final String FROM_PAGE_OUT_WEB_WIDTH_LIVEID = "from_page_out_web_liveid";//从外部浏览器进入
    private LiveShowViewGroup live_show_view_group;
    private LiveShowDragView view_top;
    private LiveShowDragView view_convert;
    private LiveShowDragView view_bottom;
    private FrameLayout frame;
    private LiveWatchFragment liveWatchFragment;
    private String from_page;
    private List<LiveRoomBean> lists = new ArrayList<>();
    private int curentPosition;
    private int curCursor;
    private boolean fromList;
    private List<LiveFollowerUsersBean> livefollowerUserList = new ArrayList<>();
    private String liveId;
    private String userId;
    private List<String> userIdList = new ArrayList<>();
    private FragmentManager manager;
    private List<Bundle> bundleList = new ArrayList<>();
    private boolean canRequest = true;
    // private Map<Integer, String> bgMap = new HashMap<>();
    private Map<Bundle, String> bgMap = new HashMap<>();
    private View img_scroll;
    private View lin_scroll;
    private FrameLayout flutter_container_fl;
    private boolean isFinish = false;
    private int position = 0;
    private CompositeDisposable mCompositeDisposable;
    private ValueAnimator dialogValueAnimator;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_show);
        getIntentData(getIntent().getExtras());
        findViewById();
        initFragment();
        setListener();
        getWindow().setFormat(PixelFormat.TRANSLUCENT);//解决surfaceveiw第一次进入的时候会有闪屏现象
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        mCompositeDisposable = new CompositeDisposable();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
//        getIntentData(intent.getExtras());
    }

    public void isGuestMeeting(boolean isGuestMeeting) {
        if (live_show_view_group != null) {
            live_show_view_group.isGuestMeeting(isGuestMeeting);
        }
    }

    public void isAudienceLinkMic(boolean isAudienceLinkMic) {
        if (live_show_view_group != null) {
            live_show_view_group.isAudienceLinkMic(isAudienceLinkMic);
        }
    }

    private void getIntentData(Bundle bundle) {
        if (bundle == null) {
            bundle = new Bundle();
        }
        from_page = bundle.getString(FROM_PAGE);

        L.d(TAG, " from_page : " + from_page);

        if (null == from_page) {
            final String action = getIntent().getAction();
            if (Intent.ACTION_VIEW.equals(action)) {//如果是从外部进入
                if (!ShareFileUtils.getBoolean(ShareFileUtils.HAS_LOGGED, false)) {//如果没登陆
                    startActivity(new Intent(this, WelcomeActivity.class));
                    finish();
                    return;
                }
                Uri uri = getIntent().getData();
                if (uri != null) {
                    final String liveid = uri.getQueryParameter("liveId");
                    final String userid = uri.getQueryParameter("userId");
                    curentPosition = 0;
                    curCursor = 0;
                    fromList = false;
                    if (!TextUtils.isEmpty(userid)) {
                        from_page = FROM_PAGE_OUT_WEB_WIDTH_USERID;
                        userId = userid;
                        this.liveId = liveid;
                        initUserIdList(userid);
                        GrowingIOUtil.postWatchLiveEntry(GrowingIoConstant.G_LiveEntry_Other);

                        return;
                    } else if (!TextUtils.isEmpty(liveid)) {
                        liveId = liveid;
                        from_page = FROM_PAGE_OUT_WEB_WIDTH_LIVEID;

                        return;
                    } else {
                        finish();
                        return;
                    }
                } else {
                    finish();
                    return;
                }
            } else {//否则进行默认判断
                final String userid = bundle.getString(TheLConstants.BUNDLE_KEY_USER_ID);
                final String liveid = bundle.getString(TheLConstants.BUNDLE_KEY_ID);
                if (!TextUtils.isEmpty(userid)) {
                    from_page = FROM_PAGE_PUSH;
                } else if (!TextUtils.isEmpty(liveid)) {
                    from_page = FROM_PAGE_WEB;
                } else {
                    from_page = FROM_PAGE_LIVE_ROOM_LIST_FRAGMENT;
                }
            }
        }
        if (FROM_PAGE_LIVE_ROOM_LIST_FRAGMENT.equals(from_page)) {//从直播列表来
            if (bundle.getSerializable(TheLConstants.BUNDLE_KEY_LIVEROOMBEAN_LIST) != null) {
                lists = (List<LiveRoomBean>) bundle.getSerializable(TheLConstants.BUNDLE_KEY_LIVEROOMBEAN_LIST);
            }
            curentPosition = bundle.getInt(TheLConstants.BUNDLE_KEY_POSITION, 0);
            curCursor = bundle.getInt(TheLConstants.BUNDLE_KEY_CURSOR, 1);
            fromList = true;
            initUserIdList(lists);
            GrowingIOUtil.postWatchLiveEntry(GrowingIoConstant.G_LiveEntry_LiveList);
        } else if (FROM_PAGE_FRIEND_LIVE_HOME_PAGE.equals(from_page)) {
            if (bundle.getSerializable(TheLConstants.BUNDLE_KEY_LIVE_FOLLOWER_USER_BEAN_LIST) != null) {
                livefollowerUserList = (List<LiveFollowerUsersBean>) bundle.getSerializable(TheLConstants.BUNDLE_KEY_LIVE_FOLLOWER_USER_BEAN_LIST);
            }
            curentPosition = bundle.getInt(TheLConstants.BUNDLE_KEY_POSITION, 0);
            curCursor = 0;
            fromList = false;
            initUserIdLists(livefollowerUserList);
            GrowingIOUtil.postWatchLiveEntry(GrowingIoConstant.G_LiveEntry_FollowedP);

        } else if (FROM_PAGE_MOMENT.equals(from_page)) {
            liveId = bundle.getString(TheLConstants.BUNDLE_KEY_ID);
            userId = bundle.getString(TheLConstants.BUNDLE_KEY_USER_ID);
            curentPosition = 0;
            curCursor = 0;
            fromList = false;
            initUserIdList(userId);

            L.d(TAG, " userId : " + userId);

            L.d(TAG, " liveId : " + liveId);

        } else if (FROM_PAGE_NEW_USER.equals(from_page)) {//从新人直播列表来
            if (bundle.getSerializable(TheLConstants.BUNDLE_KEY_LIVEROOMBEAN_LIST) != null) {
                lists = (List<LiveRoomBean>) bundle.getSerializable(TheLConstants.BUNDLE_KEY_LIVEROOMBEAN_LIST);
            }
            curentPosition = bundle.getInt(TheLConstants.BUNDLE_KEY_POSITION, 0);
            curCursor = bundle.getInt(TheLConstants.BUNDLE_KEY_CURSOR, 1);
            fromList = false;
            initUserIdList(lists);
            GrowingIOUtil.postWatchLiveEntry(GrowingIoConstant.G_LiveEntry_LiveList);

        } else if (FROM_PAGE_WEB.equals(from_page)) {
            liveId = bundle.getString(TheLConstants.BUNDLE_KEY_ID);
            userId = bundle.getString(TheLConstants.BUNDLE_KEY_USER_ID);
            curentPosition = 0;
            curCursor = 0;
            fromList = false;
            GrowingIOUtil.postWatchLiveEntry(GrowingIoConstant.G_LiveEntry_Other);

        } else if (FROM_PAGE_PUSH.equals(from_page)) {
            liveId = bundle.getString(TheLConstants.BUNDLE_KEY_ID);
            userId = bundle.getString(TheLConstants.BUNDLE_KEY_USER_ID);

            curentPosition = 0;
            curCursor = 0;
            fromList = false;
            initUserIdList(userId);
            GrowingIOUtil.postWatchLiveEntry(GrowingIoConstant.G_LiveEntry_Push);

        }

    }

    private void initUserIdList(String userId) {
        userIdList.clear();
        userIdList.add(userId);
    }

    private void initUserIdLists(List<LiveFollowerUsersBean> livefollowerUserList) {
        userIdList.clear();
     /*   for (LiveFollowerUsersBean bean : livefollowerUserList) {
            userIdList.add(bean.userId);
        }*/
        for (LiveFollowerUsersBean bean : livefollowerUserList) {
            if (TextUtils.isEmpty(bean.userId)) {
                userIdList.add(bean.userId);

            } else {
                userIdList.add(bean.roomId);

            }
        }
    }

    private void initUserIdList(List<LiveRoomBean> list) {
        userIdList.clear();
        for (LiveRoomBean bean : list) {
            userIdList.add(bean.user.id + "");
        }
    }

    private void findViewById() {
        live_show_view_group = findViewById(R.id.live_show_view_group);
        view_top = findViewById(R.id.view_top);
        view_convert = findViewById(R.id.view_convert);
        view_bottom = findViewById(R.id.view_bottom);
        frame = findViewById(R.id.frame);
        live_show_view_group.setMeasureDimen(AppInit.displayMetrics.widthPixels, AppInit.displayMetrics.heightPixels);
        live_show_view_group.initView(view_top, view_convert, view_bottom, frame);
        lin_scroll = findViewById(R.id.lin_scroll);
        img_scroll = findViewById(R.id.img_scroll);

        lin_scroll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lin_scroll.setVisibility(View.GONE);
            }
        });
        flutter_container_fl = findViewById(R.id.flutter_container_fl);
    }

    private void initFragment() {
        final Bundle bundle = getLiveBundle();
        if (liveWatchFragment == null) {
            liveWatchFragment = LiveWatchFragment.getInstance(bundle);
        }
        manager = getSupportFragmentManager();
        manager.beginTransaction().add(R.id.frame, liveWatchFragment).commit();
        tryToGetNewLiveList();
        setScrollable();

        live_show_view_group.postDelayed(new Runnable() {
            @Override
            public void run() {
                showCanScroll(true);
            }
        }, 500);

    }

    private void tryToGetNewLiveList() {
        if (bundleList.size() - curentPosition <= 2) {//如果当前位置比列表总数少2，即当前为倒数第二页，则需请求下一页
            if (canRequest) {//防止多次请求
                getNewLiveList();
            }
        }
    }

    /**
     * 上下滑动提醒，只有在可以上下滑动并且第一次的时候显示
     *
     * @param canScroll
     */
    private void showCanScroll(boolean canScroll) {
        if (!SharedPrefUtils.getBoolean(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.LIVE_ROOM_SCROLL_NEW, false) && canScroll) {
            lin_scroll.setVisibility(View.VISIBLE);

            LiveUtils.playScrollAnim(img_scroll, lin_scroll);
            SharedPrefUtils.setBoolean(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.LIVE_ROOM_SCROLL_NEW, true);
        }
    }


    /**
     * 获取bundle 列表，返回当前bundle
     *
     * @return
     */
    private Bundle getLiveBundle() {
        bundleList.clear();
        Bundle bundle;
        if (FROM_PAGE_LIVE_ROOM_LIST_FRAGMENT.equals(from_page)) {//直播列表
            int size = lists.size();
            for (int i = 0; i < size; i++) {
                LiveRoomBean bean = lists.get(i);
                bundle = new Bundle();
                bundle.putSerializable(TheLConstants.BUNDLE_KEY_LIVE_ROOM, bean);
                bundle.putSerializable(FROM_PAGE, from_page);
                bundleList.add(bundle);
            }
        } else if (FROM_PAGE_FRIEND_LIVE_HOME_PAGE.equals(from_page)) {//关注主播列表
            final int size = livefollowerUserList.size();
            for (int i = 0; i < size; i++) {
                LiveFollowerUsersBean bean = livefollowerUserList.get(i);
                bundle = new Bundle();
                bundle.putString(TheLConstants.BUNDLE_KEY_USER_ID, bean.userId);
                bundle.putString(TheLConstants.BUNDLE_KEY_IMAGE_URL, bean.imageUrl);
                bundle.putString(FROM_PAGE, from_page);
                bundleList.add(bundle);
            }
        } else if (FROM_PAGE_MOMENT.equals(from_page)) {//日志
            bundle = new Bundle();
            bundle.putString(TheLConstants.BUNDLE_KEY_USER_ID, userId);
            bundle.putString(TheLConstants.BUNDLE_KEY_ID, liveId);
            bundle.putString(FROM_PAGE, from_page);
            bundleList.add(bundle);
        } else if (FROM_PAGE_NEW_USER.equals(from_page)) {//直播列表
            int size = lists.size();
            for (int i = 0; i < size; i++) {
                LiveRoomBean bean = lists.get(i);
                bundle = new Bundle();
                bundle.putSerializable(TheLConstants.BUNDLE_KEY_LIVE_ROOM, bean);
                bundle.putString(FROM_PAGE, from_page);
                bundleList.add(bundle);
            }
        } else if (FROM_PAGE_WEB.equals(from_page)) {//网页来没有userId，只有直播id
            bundle = new Bundle();
            bundle.putString(TheLConstants.BUNDLE_KEY_USER_ID, "");
            bundle.putString(TheLConstants.BUNDLE_KEY_ID, liveId);
            bundle.putString(FROM_PAGE, from_page);

            bundleList.add(bundle);
        } else if (FROM_PAGE_PUSH.equals(from_page)) {//推送进来
            bundle = new Bundle();
            bundle.putString(TheLConstants.BUNDLE_KEY_USER_ID, userId);
            bundle.putString(TheLConstants.BUNDLE_KEY_ID, liveId);
            bundle.putString(FROM_PAGE, from_page);
            bundleList.add(bundle);
        } else if (FROM_PAGE_OUT_WEB_WIDTH_USERID.equals(from_page)) {//外部网页进来带userId
            bundle = new Bundle();
            bundle.putString(TheLConstants.BUNDLE_KEY_USER_ID, userId);
            bundle.putString(TheLConstants.BUNDLE_KEY_ID, liveId);
            bundle.putString(FROM_PAGE, from_page);
            bundleList.add(bundle);
        } else if (FROM_PAGE_OUT_WEB_WIDTH_LIVEID.equals(from_page)) {//外部网页不带userid带liveid进来
            bundle = new Bundle();
            bundle.putString(TheLConstants.BUNDLE_KEY_USER_ID, "");
            bundle.putString(TheLConstants.BUNDLE_KEY_ID, liveId);
            bundle.putString(FROM_PAGE, from_page);
            bundleList.add(bundle);
        }
        final int size = bundleList.size();
        if (curentPosition >= size) {
            curentPosition = size - 1;
        }
        if (curentPosition < 0) {
            curentPosition = 0;
        }
        setBgMap(bundleList);
        setBgViews();
        Bundle mBundle;
        if (bundleList.size() > curentPosition) {
            mBundle = bundleList.get(curentPosition);
        } else {
            mBundle = new Bundle();
        }
        return mBundle;
    }

    /**
     * 设置背景图片，放到一个map里面
     *
     * @param bundles
     */
    private void setBgMap(List<Bundle> bundles) {
        final int length = bundles.size();
        for (int i = 0; i < length; i++) {
            final LiveRoomBean liveRoomsBean = (LiveRoomBean) bundles.get(i).getSerializable(TheLConstants.BUNDLE_KEY_LIVE_ROOM);
            final String imageUrl = bundles.get(i).getString(TheLConstants.BUNDLE_KEY_IMAGE_URL);
            if (liveRoomsBean != null) {
                bgMap.put(bundles.get(i), liveRoomsBean.imageUrl);
            } else if (imageUrl != null) {
                bgMap.put(bundles.get(i), imageUrl);
            }
        }
    }

    private void setListener() {
        live_show_view_group.setOnScrollPageChangedListener(new LiveShowViewGroup.ScrollPageChangedListener() {
            @Override
            public void scrollToPage(LiveShowViewGroup.ViewStatus status) {//执行在initView之后
                switch (status) {
                    case PREVIOUS:
                        setPreviousView();
                        break;
                    case NEXT:
                        setNextView();
                        break;
                }
            }

            @Override
            public void initView(View topView, View convertView, View bottomView) {
                final int size = bundleList.size();
                L.i("refresh", "initView: currentPosition=" + curentPosition + ",bgMap:" + bgMap.toString());
                if (curentPosition - 1 >= 0 && curentPosition - 1 < size && topView instanceof LiveShowDragView) {
                    ((LiveShowDragView) topView).initView(bgMap.get(bundleList.get(curentPosition - 1)));
                }
                if (curentPosition >= 0 && curentPosition < size && convertView instanceof LiveShowDragView) {
                    ((LiveShowDragView) convertView).initView(bgMap.get(bundleList.get(curentPosition)));
                }
                if (curentPosition + 1 >= 0 && curentPosition + 1 < size && bottomView instanceof LiveShowDragView) {
                    ((LiveShowDragView) bottomView).initView(bgMap.get(bundleList.get(curentPosition + 1)));
                }
                if (curentPosition >= 0 && curentPosition < size) {
                    //todo
                }
            }

            @Override
            public void onFling(LiveShowViewGroup.Direction direction) {
                switch (direction) {
                    case LEFT:
                        liveWatchFragment.showLiveView(true);
                        break;
                    case RIGHT:
                        liveWatchFragment.showLiveView(false);
                        break;
                }
            }

            @Override
            public void onPreScrollToPage(LiveShowViewGroup.ViewStatus mViewStatus) {
                switch (mViewStatus) {
                    case PREVIOUS:
                        curentPosition -= 1;
                        initLiveShow();
                        break;
                    case NEXT:
                        curentPosition += 1;
                        initLiveShow();
                        tryToGetNewLiveList();
                        break;
                }

            }
        });

        liveWatchFragment.setFragmentCreatedCallback(new FragmentCreatedCallback() {
            @Override
            public void created() {

                if (bundleList != null && bundleList.size() > curentPosition) {
                    getLiveRoomBean(bundleList.get(curentPosition), curentPosition);
                }
            }
        });
        liveWatchFragment.setLiveSwitchListener(new LiveSwitchListener() {
            @Override
            public boolean switchNext() {
                return scrollToNext();
            }

            @Override
            public boolean switchPrevious() {
                return scrollToPrevious();
            }

            @Override
            public boolean switchMostExpensiveLive(String userId) {
                gotoMostExpensiveLive(userId);
                return true;
            }
        });
    }

    /**
     * 初始化，在切换页面的时候
     */
    private void initLiveShow() {
        setScrollable();
        liveWatchFragment.initLiveShow();
    }

    /**
     * 设置显示上一个直播
     */
    private void setPreviousView() {
        refreshLiveShowFragment();
    }

    /**
     * 显示下一个直播
     */
    private void setNextView() {
        refreshLiveShowFragment();
    }

    /**
     * 刷新显示为当前直播内容
     */
    private void refreshLiveShowFragment() {
        final Bundle bundle = bundleList.get(curentPosition);
        getLiveRoomBean(bundle, curentPosition);
    }

    /**
     * 设置是否可以滑动
     */
    private void setScrollable() {
        final int size = bundleList.size();
        if (curentPosition == 0 && size == 1) {//如果是第一页并且只有一页，则不能滑动
            live_show_view_group.setScrollable(LiveShowViewGroup.Scroll_Limit.NONE);
        } else if (curentPosition == 0) {//否则如果是第一页，则只能上滑
            live_show_view_group.setScrollable(LiveShowViewGroup.Scroll_Limit.UP);
        } else if (curentPosition == size - 1) {//如果是最后一页，则只能下滑
            live_show_view_group.setScrollable(LiveShowViewGroup.Scroll_Limit.DOWN);
        } else {
            live_show_view_group.setScrollable(LiveShowViewGroup.Scroll_Limit.ALL);
        }
    }

    /**
     * 是否直播间已经存在，用来过滤重复的直播间
     *
     * @param bean
     * @return
     */
    private boolean roomExist(LiveRoomBean bean) {
        final int size = userIdList.size();
        for (int i = 0; i < size; i++) {
            if ((bean.user.id + "").equals(userIdList.get(i))) {
                return true;
            }
        }
        userIdList.add(bean.user.id + "");
        return false;
    }

    /**
     * 获取更多直播列表
     */
    private void getNewLiveList() {
        if (FROM_PAGE_NEW_USER.equals(from_page)) {//如果从新人直播列表进来，不进行网络请求
            return;
        }
        if (!fromList) {//不从直播列表进入请求列表第一页
            fromList = true;
            getLiveRoomsNetList(curCursor, TYPE_HOT);
        } else if (curCursor > 0) {//列表中没别的数据的时候也会返回cursor为 0，所以>0才请求列表
            getLiveRoomsNetList(curCursor, TYPE_HOT);
        }
    }

    /**
     * 获取更多直播间
     *
     * @param curCursor
     * @param typeHot
     */
    private void getLiveRoomsNetList(final int curCursor, final String typeHot) {
        canRequest = false;
        final Flowable<LiveRoomsNetBean> flowable = DefaultRequestService.createLiveRequestService().getLiveRoomsListBean(curCursor + "", typeHot);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<LiveRoomsNetBean>() {
            @Override
            public void onNext(LiveRoomsNetBean data) {
                super.onNext(data);
                if (data.data != null) {
                    refreshLiveRooms(data.data);
                }
            }

            @Override
            public void onComplete() {
                super.onComplete();
                requestFinish();
                /**
                 * 设置可以继续请求数据
                 */
                Flowable.timer(1000, TimeUnit.MILLISECONDS).subscribe(new InterceptorSubscribe<Long>() {
                    @Override
                    public void onNext(Long data) {
                        super.onNext(data);
                        canRequest = true;
                    }
                });
            }
        });
    }

    /**
     * 刷新直播间list列表
     *
     * @param liveRoomsBean
     */
    private void refreshLiveRooms(LiveRoomsBean liveRoomsBean) {
        curCursor = liveRoomsBean.cursor;
        if (liveRoomsBean.list.size() > 0) {
            addNewBundleList(liveRoomsBean.list);
        }
        setScrollable();
        setBgViews();
    }

    /**
     * 刷新直播间bundleList列表
     *
     * @param liveRoomBeans
     */
    private void addNewBundleList(ArrayList<LiveRoomBean> liveRoomBeans) {
        final int size = liveRoomBeans.size();
        final int bundleSize = bundleList.size();
        int index = 0;
        for (int i = 0; i < size; i++) {
            LiveRoomBean bean = liveRoomBeans.get(i);
            if (!roomExist(bean)) {
                final Bundle bundle = new Bundle();
                bundle.putSerializable(TheLConstants.BUNDLE_KEY_LIVE_ROOM, bean);
                bgMap.put(bundle, bean.imageUrl);
                bundleList.add(bundle);
            } else {
                index -= 1;
            }
            index += 1;
        }
    }

    /**
     * 请求结束
     */
    private void requestFinish() {
        closeLoading();
    }

    /**
     * 获取直播详情
     *
     * @param bundle
     */
    private void getLiveRoomBean(Bundle bundle, int position) {
        final LiveRoomBean liveRoomsBean = (LiveRoomBean) bundle.getSerializable(TheLConstants.BUNDLE_KEY_LIVE_ROOM);
        final String userId = bundle.getString(TheLConstants.BUNDLE_KEY_USER_ID);
        final String id = bundle.getString(TheLConstants.BUNDLE_KEY_ID);
        this.position = position;
        if (liveRoomsBean != null) {
            getLiveRoomDetail(liveRoomsBean.id, liveRoomsBean.user.id + "");
        } else if (!TextUtils.isEmpty(userId)) {
            getLiveRoomDetail("", userId);
        } else if (!TextUtils.isEmpty(id)) {
            getLiveRoomDetail(id, "");
        }
    }

    /**
     * 获取直播详情
     *
     * @param liveId
     * @param userId
     */
    public void getLiveRoomDetail(String liveId, String userId) {


        L.i(TAG, " liveId " + liveId + " userId " + userId + " position " + position);

        mCompositeDisposable.clear();

        final Flowable<LiveRoomNetBean> flowable = DefaultRequestService.createLiveRequestService().getLiveRoomDetail(liveId, userId);

        Disposable disposable = flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorConsumer<LiveRoomNetBean>() {
            @Override
            public void accept(LiveRoomNetBean data) throws Exception {
                super.accept(data);
                if (!hasErrorCode && data != null && data.data != null && data.data.user != null) {

                    L.i(TAG, "data=" + data.toString() + ",position=" + position);

                    if (String.valueOf(data.data.user.id).equals(userId) || String.valueOf(data.data.id).equals(liveId)) {
                        data.data.setSoftEnjoyBean();
                        bgMap.put(bundleList.get(position), data.data.imageUrl);
                        if (userIdList.isEmpty()) {//如果userIdList为空，说明是从一些直接传id的地方传过来的，此时为了防止列表重复需要把userid添加上去
                            userIdList.add(data.data.user.id + "");
                        }
                        refreshLiveShow(data.data);
                    }

                } else if (hasErrorCode) {
                    if (data != null && data.errcode.equals("97")) {//已经被对方给屏蔽
                        showBlockAlertDialog();
                    }
                }
                requestFinish();
            }
        }, new InterceptorThrowableConsumer() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                super.accept(throwable);
                if (errDataBean != null && errDataBean.errcode.equals("97")) {
                    showBlockAlertDialog();
                }
                requestFinish();
            }
        });

        mCompositeDisposable.add(disposable);

    }

    private void refreshLiveShow(LiveRoomBean data) {
        if (isFinish) {//如果已经关闭的话，就关闭掉
            return;
        }
        if (liveWatchFragment != null) {
            liveWatchFragment.refreshLiveShow(data);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (liveWatchFragment != null) {
            liveWatchFragment.dispatchTouchEvent(event);
        }
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            onUserInteraction();
        }
        if (getWindow().superDispatchTouchEvent(event)) {
            return true;
        }
        return onTouchEvent(event);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (!isShowExitDialog()) {
            Fragment fragment = getSupportFragmentManager().findFragmentByTag("livewatch_NewFlutterFragment");
            if (fragment == null) {
                if (liveWatchFragment != null) {
                    //相遇过程中嘉宾端禁止操作
                    if (liveWatchFragment.liveWatchViewFragment != null && liveWatchFragment.liveWatchViewFragment.isMeeting)
                        return true;
                    if (!liveWatchFragment.onKeyDown(keyCode, event)) {
                        return false;
                    }

                }
            } else {
                NewUniversalRouter.sharedInstance.nativePop();
                return true;
            }
        } else {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return true;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
            if (getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {//横屏
                live_show_view_group.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        live_show_view_group.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        live_show_view_group.setMeasureDimen(AppInit.displayMetrics.widthPixels, AppInit.displayMetrics.heightPixels);
                    }
                });
            }
        }
    }

    /**
     * 设置背景图片
     */
    public void setBgViews() {
        final List<View> views = live_show_view_group.getCtViews();
        final int ViewSize = views.size();
        final int size = bundleList.size();
        if (ViewSize > 2) {
            final View topView = views.get(0);
            final View convertView = views.get(1);
            final View bottomView = views.get(2);
            L.i("refresh", "initView: currentPosition=" + curentPosition);
            if (curentPosition - 1 >= 0 && curentPosition - 1 < size && topView instanceof LiveShowDragView) {
                ((LiveShowDragView) topView).initView(bgMap.get(bundleList.get(curentPosition - 1)));
            }
            if (curentPosition >= 0 && curentPosition < size && convertView instanceof LiveShowDragView) {
                ((LiveShowDragView) convertView).initView(bgMap.get(bundleList.get(curentPosition)));
            }
            if (curentPosition + 1 >= 0 && curentPosition + 1 < size && bottomView instanceof LiveShowDragView) {
                ((LiveShowDragView) bottomView).initView(bgMap.get(bundleList.get(curentPosition + 1)));
            }
        }
    }

    private void showBlockAlertDialog() {

        DialogUtil.showAlert(this, "", getString(R.string.info_had_bean_added_to_black), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });
    }

    /**
     * 滑动到上一个
     */
    public boolean scrollToPrevious() {
        return live_show_view_group.scrollToPrevious();
    }

    /**
     * 滑动到下一个
     */
    public boolean scrollToNext() {
        return live_show_view_group.scrollToNext();
    }

    /**
     * 跳转到全服最贵礼物直播
     *
     * @param userId
     */
    public void gotoMostExpensiveLive(String userId) {
        if (TextUtils.isEmpty(userId)) {
            return;
        }
        insertOneLiveIntoNext(userId);
        scrollToNext();
    }

    public void insertOneLiveIntoNext(@NonNull String userId) {
        if (TextUtils.isEmpty(userId)) {
            return;
        }
        setInsertLiveBundleList(userId);
        setBgViews();
    }

    private void setInsertLiveBundleList(@NonNull String userId) {
        //首先要去重
        final int repeatPosition = getRepeatedLivePosition(userId);
        if (repeatPosition < 0) {//说明当前列表没有这个userid的直播
            final Bundle bundle = new Bundle();
            bundle.putString(TheLConstants.BUNDLE_KEY_USER_ID, userId);
            bundleList.add(curentPosition + 1, bundle);
        } else if (repeatPosition < curentPosition) {//在当前直播上面
            final Bundle bundle = bundleList.get(repeatPosition);
            bundleList.remove(bundle);
            curentPosition -= 1;
            bundleList.add(curentPosition + 1, bundle);
        } else if (repeatPosition == curentPosition) {//是当前直播，不做处理
            return;
        } else if (repeatPosition == curentPosition + 1) {//是当前直播下面一个直播，不做处理
        } else {//否则
            final Bundle bundle = bundleList.get(repeatPosition);
            bundleList.remove(bundle);
            bundleList.add(curentPosition + 1, bundle);
        }
    }

    /**
     * 获取重复的位置
     *
     * @param optUserId
     * @return
     */
    private int getRepeatedLivePosition(@NonNull String optUserId) {
        int size = bundleList.size();
        for (int i = 0; i < size; i++) {
            final Bundle bundle = bundleList.get(i);
            final String userId = bundle.getString(TheLConstants.BUNDLE_KEY_USER_ID);
            final LiveRoomBean liveRoomBean = (LiveRoomBean) bundle.getSerializable(TheLConstants.BUNDLE_KEY_LIVE_ROOM);
            if (optUserId.equals(userId)) {
                return i;
            }
            if (liveRoomBean != null && liveRoomBean.user != null && optUserId.equals(liveRoomBean.user.id + "")) {
                return i;
            }
        }
        return -1;
    }

    public boolean isShowExitDialog() {

        if (liveWatchFragment != null && liveWatchFragment.isAdded() && liveWatchFragment.liveWatchViewFragment != null && liveWatchFragment.liveWatchViewFragment.isAdded()) {

            if (liveWatchFragment.liveWatchViewFragment.audienceLinkMicStatus == LiveWatchViewFragment.AUDIENCE_LINK_MIC_CONNECTING || liveWatchFragment.liveWatchViewFragment.audienceLinkMicStatus == LiveWatchViewFragment.AUDIENCE_LINK_MIC_SUCCESS) {

                String title = TheLApp.context.getResources().getString(R.string.exit);

                String content = TheLApp.context.getResources().getString(R.string.your_application_would_be_canceled);

                DialogUtil.showConfirmDialog(LiveWatchActivity.this, title, content, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        PushUtils.finish(LiveWatchActivity.this);
                        liveWatchFragment.liveWatchViewFragment.close();
                    }
                });

                return true;
            }

        }

        return false;

    }

    @Override
    public void onAddFragment() {

        //处理闪屏问题
        flutter_container_fl.setAlpha(0.01f);

        NewFlutterFragment newFlutterFragment = (NewFlutterFragment) getSupportFragmentManager().findFragmentByTag("livewatch_NewFlutterFragment");

        if (newFlutterFragment == null) {
            newFlutterFragment = new NewFlutterFragment.NewEngineFragmentBuilder().transparencyMode(FlutterView.TransparencyMode.transparent).renderMode(FlutterView.RenderMode.surface).build();
        }

        getSupportFragmentManager().beginTransaction().add(R.id.flutter_container_fl, newFlutterFragment, "livewatch_NewFlutterFragment").commit();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                flutter_container_fl.setAlpha(1);

            }
        }, 800);

    }

    @Override
    public void onRemoveFragment() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("livewatch_NewFlutterFragment");
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction().remove(fragment).commit();
            flutter_container_fl.setAlpha(1);
        }
    }

    @Override
    public SplashScreen provideSplashScreen() {
        Drawable manifestSplashDrawable = getSplashScreenFromManifest();
        if (manifestSplashDrawable != null) {
            return new DrawableSplashScreen(manifestSplashDrawable, ImageView.ScaleType.CENTER, 500L);
        } else {
            return null;
        }
    }

    private static String SPLASH_SCREEN_META_DATA_KEY = "io.flutter.embedding.android.SplashScreenDrawable";

    private Drawable getSplashScreenFromManifest() {
        try {
            @SuppressLint("WrongConstant") ActivityInfo activityInfo = getPackageManager().getActivityInfo(
                    getComponentName(),
                    PackageManager.GET_META_DATA | PackageManager.GET_ACTIVITIES);
            Bundle metadata = activityInfo.metaData;
            Integer splashScreenId = metadata != null ? metadata.getInt(SPLASH_SCREEN_META_DATA_KEY) : null;
            return splashScreenId != null
                    ? Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP
                    ? getResources().getDrawable(splashScreenId, getTheme())
                    : getResources().getDrawable(splashScreenId)
                    : null;
        } catch (PackageManager.NameNotFoundException e) {
            // This is never expected to happen.
            return null;
        }
    }

    @Override
    protected void onDestroy() {
        if (dialogValueAnimator != null) {
            dialogValueAnimator.cancel();
        }
        super.onDestroy();
    }
}
