package com.thel.modules.main.home.community;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseFragment;
import com.thel.bean.AdBean;
import com.thel.bean.theme.ThemeClassBean;
import com.thel.constants.BundleConstants;
import com.thel.constants.TheLConstants;
import com.thel.modules.main.home.moments.web.WebViewActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.ui.widget.BannerLayout;
import com.thel.ui.widget.MySwipeRefreshLayout;
import com.thel.ui.widget.emoji.OnRecyclerViewListener;
import com.thel.utils.BusinessUtils;
import com.thel.utils.L;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class CommunityFragment extends BaseFragment {

    @BindView(R.id.swipe_container)
    MySwipeRefreshLayout swipe_container;

    @BindView(R.id.appbarlayout)
    AppBarLayout appBarLayout;

    /**
     * 发现页广告
     **/
    @BindView(R.id.recyclerview_ads)
    RecyclerView rvAds;

    @BindView(R.id.wide_divider)
    View wide_divider;

    @BindView(R.id.tablayout)
    TabLayout tablayout;

    @BindView(R.id.view_pager)
    ViewPager view_pager;

    /**
     * 原朋友圈广告部分
     **/
    @BindView(R.id.banner)
    BannerLayout banner;

    private boolean isLoaded = false;

    private long refreshTime = 0;

    private long LastAdTime = 0;

    private long divideAdTime = 5 * 60 * 1000;//广告最短刷新时间为5分钟

    private List<String> titles = new ArrayList<>();

    private List<ThemeClassBean> themeBeans = new ArrayList<>();

    private ArrayList<AdBean.Map_list> adBeans = new ArrayList<>();

    private AdsRecyclerViewAdapter mAdapter;

    private ThemeSortAdapter sortAdapter;

    private SparseArray<ThemeClassBean> themeClassBeanMap = new SparseArray<ThemeClassBean>() {
        {
            put(0, new ThemeClassBean("hot", TheLApp.getContext().getResources().getString(R.string.theme_select_title), -1, TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify_info)[0]));
            put(1, new ThemeClassBean("new", TheLApp.getContext().getResources().getString(R.string.topic_main_page_tab_new), -1, TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify_info)[0]));

            put(2, new ThemeClassBean("love", TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify)[0], 0, TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify_info)[0]));
            //put(2, new ThemeClassBean("sex", TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify)[1], 1, TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify_info)[1]));
            put(5, new ThemeClassBean("joke", TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify)[2], 2, TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify_info)[2]));
            put(3, new ThemeClassBean("life", TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify)[3], 3, TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify_info)[3]));
            put(6, new ThemeClassBean("art", TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify)[4], 4, TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify_info)[4]));
            put(7, new ThemeClassBean("out", TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify)[5], 5, TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify_info)[5]));
            put(8, new ThemeClassBean("job", TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify)[6], 6, TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify_info)[6]));
            //  put(8, new ThemeClassBean("bisexual", TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify)[7], 7, TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify_info)[7]));
            put(9, new ThemeClassBean("help", TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify)[8], 8, TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify_info)[8]));
            put(10, new ThemeClassBean("other", TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify)[9], 9, TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify_info)[9]));
            put(4, new ThemeClassBean("notalone", TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify)[10], 10, TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify_info)[10]));
            put(8, new ThemeClassBean("30+", TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify)[11], 11, TheLApp.getContext().getResources().getStringArray(R.array.theme_select_classify_info)[11]));

        }
    };

    private static CommunityFragment INSTANCE;
    private String fromPage;
    private String fromePageid;
    private ThemeSortListFragment fragment;

    public static CommunityFragment getInstance() {
        return new CommunityFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_recommend, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        L.d("CommunityFragment", "--------onActivityCreated-------");

        initView();

        registerReceiver();
    }

    @Override
    public void onDestroy() {
        try {
            getActivity().unregisterReceiver(receiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    private void initBanner() {
        /** 原朋友圈广 **/
        banner.setVisibility(View.GONE);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ListView.LayoutParams.MATCH_PARENT, (int) (TheLApp.getContext().getResources().getDisplayMetrics().widthPixels / TheLConstants.ad_aspect_ratio));
        banner.setLayoutParams(params);

        setWideDividerVisible();

        getAdsData();
    }

    private void initViewPager() {

        int size = themeClassBeanMap.size();
        for (int i = 0; i < size; i++) {
            ThemeClassBean bean = themeClassBeanMap.get(i);
            String title = bean.text;
            themeBeans.add(bean);
            titles.add(title);
        }

        List<ThemeSortListFragment> list = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            fragment = ThemeSortListFragment.newInstance(themeBeans.get(i), i);
            fragment.setRrefreshFinishListener(new ThemeSortListFragment.RefreshFinishListener() {
                @Override
                public void refreshFinish() {
                    requestFinish();
                }
            });
            list.add(fragment);
        }
        fragment.setFromPageAndId(fromPage, fromePageid);
        sortAdapter = new ThemeSortAdapter(list, getChildFragmentManager(), titles);
        view_pager.setOffscreenPageLimit(3);
        view_pager.setAdapter(sortAdapter);

        final int length = titles.size();
        for (int i = 0; i < length; i++) {
            tablayout.addTab(tablayout.newTab());
        }
        tablayout.setupWithViewPager(view_pager);

        /**
         * 设tab下划线和字数一样长度
         * */
        tablayout.post(new Runnable() {
            @Override
            public void run() {
                try {
                    //拿到tabLayout的mTabStrip属性
                    Field mTabStripField = tablayout.getClass().getDeclaredField("mTabStrip");
                    mTabStripField.setAccessible(true);

                    LinearLayout mTabStrip = (LinearLayout) mTabStripField.get(tablayout);

                    int dp10 = Utils.dip2px(TheLApp.getContext(), 10);

                    for (int i = 0; i < mTabStrip.getChildCount(); i++) {
                        View tabView = mTabStrip.getChildAt(i);

                        //拿到tabView的mTextView属性
                        Field mTextViewField = tabView.getClass().getDeclaredField("mTextView");
                        mTextViewField.setAccessible(true);

                        TextView mTextView = (TextView) mTextViewField.get(tabView);

                        tabView.setPadding(0, 0, 0, 0);

                        //因为我想要的效果是   字多宽线就多宽，所以测量mTextView的宽度
                        int width = 0;
                        width = mTextView.getWidth();
                        if (width == 0) {
                            mTextView.measure(0, 0);
                            width = mTextView.getMeasuredWidth();
                        }

                        //设置tab左右间距为10dp  注意这里不能使用Padding 因为源码中线的宽度是根据 tabView的宽度来设置的
                        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) tabView.getLayoutParams();
                        params.width = width;
                        params.leftMargin = dp10;
                        params.rightMargin = dp10;
                        tabView.setLayoutParams(params);

                        tabView.invalidate();
                    }

                } catch (NoSuchFieldException | IllegalAccessException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    private void initView() {
        ViewUtils.initSwipeRefreshLayout(swipe_container);
        //解决SwipeRefreshLayout 与 CoordinatorLayout 嵌套冲突//
        //设置样式刷新显示的位置
        swipe_container.setProgressViewOffset(true, -20, 100);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {

                if (verticalOffset >= 0) {
                    swipe_container.setEnabled(true);
                } else {
                    swipe_container.setEnabled(false);
                }
            }
        });
        //解决SwipeRefreshLayout 与 CoordinatorLayout 嵌套冲突//

        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                if (System.currentTimeMillis() - LastAdTime >= divideAdTime) {//广告最短刷新时间为5分钟
                    getAd();
                }
                final int position = view_pager.getCurrentItem();
                final Fragment fragment = sortAdapter.getItem(position);
                if (fragment != null && fragment instanceof ThemeSortListFragment) {
                    ((ThemeSortListFragment) fragment).loadNetData(0, ThemeSortListFragment.REFRESH_TYPE_ALL);
                } else {
                    requestFinish();
                }
            }
        });

        //设置布局管理器
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(TheLApp.getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvAds.setLayoutManager(linearLayoutManager);
        //如果可以确定每个item的高度是固定的，设置这个选项可以提高性能
        rvAds.setHasFixedSize(true);
        rvAds.setItemAnimator(null);

    }

    private void requestFinish() {
        if (swipe_container != null) {
            swipe_container.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (swipe_container.isRefreshing())
                        swipe_container.setRefreshing(false);
                }
            }, 1500);
        }
    }

    private void setCurrentTab(ThemeClassBean bean) {
        final int size = themeBeans.size();
        for (int i = 0; i < size; i++) {
            if (themeBeans.get(i).type.equals(bean.type)) {
                view_pager.setCurrentItem(i);
                return;
            }
        }
    }

    private void refreshAdArea(AdBean adBean) {

        if (adBean == null || adBean.data == null || adBean.data.map_list == null)
            return;

        /** 原朋友圈广告 **/
        final List<AdBean.Map_list> adList = adBean.data.map_list;
        List<AdBean.Map_list> willDelete = new ArrayList<>();
        for (AdBean.Map_list bean : adList) {
            if (!bean.advertLocation.equals("friend"))
                willDelete.add(bean);
        }
        adList.removeAll(willDelete);
        if (!adList.isEmpty()) {
            List<String> advs = new ArrayList<String>();
            for (int i = 0; i < adList.size(); i++) {
                advs.add(adList.get(i).advertURL);
            }
            banner.setVisibility(View.VISIBLE);
            banner.setViewUrls(advs);
            // 添加点击事件的监听
            banner.setOnBannerItemClickListener(new BannerLayout.OnBannerItemClickListener() {
                @Override
                public void onItemClick(int position) {
                    BusinessUtils.AdRedirect(adList.get(position));
                }
            });
        }

        /** 热拉tv等区域 **/
        adBeans.clear();
        for (int i = 0; i < adList.size(); i++) {
            if (AdBean.ADV_LOCATION_FIND.equals(adList.get(i).advertLocation)) {
                adBeans.add(adList.get(i));
            }
        }
        if (adBeans.size() > 0) {
            rvAds.setVisibility(View.VISIBLE);
            if (mAdapter == null) {
                mAdapter = new AdsRecyclerViewAdapter(adBeans, getActivity());
                mAdapter.setOnRecyclerViewListener(new OnRecyclerViewListener() {
                    @Override
                    public void onItemClick(int position) {
                        AdBean.Map_list adBean = adBeans.get(position);
                        if (AdBean.ADV_DUMP_TYPE_LIVE_ROOMS.equals(adBean.dumpType)) {
                            Intent intent = new Intent();
                            intent.setAction(TheLConstants.BROADCAST_GOTO_LIVE_ROOMS_PAGE);
                            getActivity().sendBroadcast(intent);
                        } else {
                            Intent intent = new Intent(getActivity(), WebViewActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(BundleConstants.URL, adBeans.get(position).dumpURL);
                            bundle.putString(BundleConstants.SITE, "thelSite");
                            bundle.putString("title", adBeans.get(position).advertTitle);
                            intent.putExtras(bundle);
                            startActivity(intent);
                        }
                    }

                    @Override
                    public boolean onItemLongClick(int position) {
                        return false;
                    }
                });
                rvAds.setAdapter(mAdapter);
            } else {
                mAdapter.notifyDataSetChanged();
            }
        }
        setWideDividerVisible();

    }

    private void setWideDividerVisible() {
        wide_divider.setVisibility(View.GONE);
        if (banner.getVisibility() == View.VISIBLE || rvAds.getVisibility() == View.VISIBLE) {
            wide_divider.setVisibility(View.VISIBLE);
        }
    }

    private void getAdsData() {
        if (System.currentTimeMillis() - LastAdTime >= divideAdTime) {//广告最短刷新时间为5分钟
            if (swipe_container != null) {
                swipe_container.post(new Runnable() {
                    @Override
                    public void run() {
                        swipe_container.setRefreshing(true);
                    }
                });
            }

            getAd();
        }
    }

    private void getAd() {
        RequestBusiness.getInstance().getAd().onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<AdBean>() {
            @Override
            public void onNext(AdBean data) {
                super.onNext(data);
                LastAdTime = System.currentTimeMillis();
                refreshAdArea(data);
                requestFinish();
            }
        });
    }

    private MessageReceiver receiver;

    // 注册朋友圈消息广播接收器
    private void registerReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(TheLConstants.BROADCAST_GOTO_THEME_TAB);
        receiver = new MessageReceiver();
        getActivity().registerReceiver(receiver, intentFilter);
    }

    public void setFromPageAndId(String from_Page, String pageId) {
        this.fromPage = from_Page;
        this.fromePageid = pageId;

    }

    class MessageReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (TheLConstants.BROADCAST_GOTO_THEME_TAB.equals(intent.getAction())) {
                if (intent.getSerializableExtra(TheLConstants.BUNDLE_KEY_THEME_CLASS_BEAN) != null) {
                    ThemeClassBean bean = (ThemeClassBean) intent.getSerializableExtra(TheLConstants.BUNDLE_KEY_THEME_CLASS_BEAN);
                    setCurrentTab(bean);
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        long currentTime = System.currentTimeMillis();

        if (refreshTime != 0 && currentTime - refreshTime > 30 * 60 * 1000) {

            if (System.currentTimeMillis() - LastAdTime >= divideAdTime) {//广告最短刷新时间为5分钟
                getAd();
            }
            final int position = view_pager.getCurrentItem();
            final Fragment fragment = sortAdapter.getItem(position);
            if (fragment != null && fragment instanceof ThemeSortListFragment) {
                ((ThemeSortListFragment) fragment).loadNetData(0, ThemeSortListFragment.REFRESH_TYPE_ALL);
            } else {
                requestFinish();
            }

            refreshTime = currentTime;
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (!isLoaded && isVisibleToUser) {
            initViewPager();
            initBanner();
            isLoaded = true;
        }

        L.d("CommunityFragment", "--------setUserVisibleHint-------");

    }
}
