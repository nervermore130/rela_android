package com.thel.modules.main.video_discover.videofalls;

import com.thel.modules.main.video_discover.autoplay.RecyclerViewStateImp;
import com.thel.modules.main.video_discover.autoplay.VideoAutoPlayImp;
import com.thel.modules.main.video_discover.videofalls.widget.WebpSimpleDraweeView;

import java.util.Map;
import java.util.WeakHashMap;

/**
 * Created by waiarl on 2018/3/9.
 */

public class RecyclerViewStateBean implements RecyclerViewStateImp{

    public volatile int mState = State_IDLE;

    private Map<WebpSimpleDraweeView, RecyclerViewScrollStateChangedListener> scrollStateChangedListenerMap = new WeakHashMap<>();

    private Map<VideoAutoPlayImp,RecyclerViewScrollStateChangedListener> autoPlayImpRecyclerViewScrollStateChangedListenerMap=new WeakHashMap<>();

    public RecyclerViewStateBean setState(int state) {
        if (this.mState != state) {
            mState = state;
            for (RecyclerViewScrollStateChangedListener listener : scrollStateChangedListenerMap.values()) {
                if (listener != null) {
                    listener.stateChanged(state);
                }
            }
        }
        return this;
    }


    public void addWebpSimpleDraweeView(WebpSimpleDraweeView webpSimpleDraweeView) {
        scrollStateChangedListenerMap.put(webpSimpleDraweeView, webpSimpleDraweeView.getRecyclerViewScrollStateChangedListener());
    }
}
