package com.thel.modules.live.surface.show;

import android.animation.Animator;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.RectF;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.LiveAdInfoBean;
import com.thel.bean.LiveInfoLogBean;
import com.thel.bean.RejectMicBean;
import com.thel.bean.analytics.AnalyticsBean;
import com.thel.bean.gift.GIOGiftBean;
import com.thel.bean.live.LiveMultiOnCreateBean;
import com.thel.bean.live.LiveMultiSeatBean;
import com.thel.bean.live.MultiSpeakersBean;
import com.thel.bean.message.DanmuResultBean;
import com.thel.callback.imp.KeyboardChangeListener;
import com.thel.callback.imp.OnEditTouchListener;
import com.thel.constants.BundleConstants;
import com.thel.constants.TheLConstants;
import com.thel.growingio.GIOShareTrackBean;
import com.thel.growingio.GrowingIoConstant;
import com.thel.imp.BaseFunctionFragment;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.live.LiveConstant;
import com.thel.modules.live.adapter.LiveRoomChatAdapter;
import com.thel.modules.live.ads.LiveAdViewManager;
import com.thel.modules.live.bean.AudienceLinkMicRequestBean;
import com.thel.modules.live.bean.DanmuBean;
import com.thel.modules.live.bean.GiftSendMsgBean;
import com.thel.modules.live.bean.LivePkAeAnimBean;
import com.thel.modules.live.bean.LivePkAssisterBean;
import com.thel.modules.live.bean.LivePkFriendBean;
import com.thel.modules.live.bean.LivePkGemNoticeBean;
import com.thel.modules.live.bean.LivePkHangupBean;
import com.thel.modules.live.bean.LivePkInitBean;
import com.thel.modules.live.bean.LivePkRequestNoticeBean;
import com.thel.modules.live.bean.LivePkResponseNoticeBean;
import com.thel.modules.live.bean.LivePkStartNoticeBean;
import com.thel.modules.live.bean.LivePkSummaryNoticeBean;
import com.thel.modules.live.bean.LiveRoomBean;
import com.thel.modules.live.bean.LiveRoomMsgBean;
import com.thel.modules.live.bean.LiveRoomMsgConnectMicBean;
import com.thel.modules.live.bean.LiveTopFansBean;
import com.thel.modules.live.bean.LiveTopFansTodayBean;
import com.thel.modules.live.bean.LiveUserCardBean;
import com.thel.modules.live.bean.ResponseGemBean;
import com.thel.modules.live.bean.SoftEnjoyBean;
import com.thel.modules.live.bean.SoftEnjoyNetBean;
import com.thel.modules.live.bean.SoftGiftBean;
import com.thel.modules.live.bean.TopTodayBean;
import com.thel.modules.live.ctrl.GiftMsgCtrl;
import com.thel.modules.live.in.LivePkContract;
import com.thel.modules.live.in.LiveShowCtrlListener;
import com.thel.modules.live.in.LiveTextSizeChangedListener;
import com.thel.modules.live.in.OnMeetStatusListener;
import com.thel.modules.live.interfaces.ILive;
import com.thel.modules.live.interfaces.ILiveAnchor;
import com.thel.modules.live.liveBigGiftAnimLayout.LiveBigGiftAnimLayout;
import com.thel.modules.live.liveBigGiftAnimLayout.VideoAnimListener;
import com.thel.modules.live.livepkfriend.LiveLinkMicAudienceListFragment;
import com.thel.modules.live.livepkfriend.LiveLinkMicDialog;
import com.thel.modules.live.livepkfriend.LivePKFriendListView;
import com.thel.modules.live.liverank.LiveGiftRankDialogView;
import com.thel.modules.live.surface.LiveShowContract;
import com.thel.modules.live.surface.LiveShowPresenter;
import com.thel.modules.live.surface.meet.MeetResultActivity;
import com.thel.modules.live.surface.watch.LiveWatchObserver;
import com.thel.modules.live.utils.LiveGIOPush;
import com.thel.modules.live.utils.LiveShowDialogUtils;
import com.thel.modules.live.utils.LiveStatus;
import com.thel.modules.live.utils.LiveUtils;
import com.thel.modules.live.view.DanmuLayoutView;
import com.thel.modules.live.view.LinkMicInfoView;
import com.thel.modules.live.view.LinkMicOwnerView;
import com.thel.modules.live.view.LiveLookRankView;
import com.thel.modules.live.view.LiveMeetionPlayDialog;
import com.thel.modules.live.view.LiveNoticeShowDialog;
import com.thel.modules.live.view.LivePKBloodView;
import com.thel.modules.live.view.LivePkAssistsRankView;
import com.thel.modules.live.view.LivePkCountDownView;
import com.thel.modules.live.view.LivePkFailedView;
import com.thel.modules.live.view.LivePkMediaPlayer;
import com.thel.modules.live.view.LiveShowCaptureCloseDialog;
import com.thel.modules.live.view.LiveShowUserView;
import com.thel.modules.live.view.LiveUserCardDialogView;
import com.thel.modules.live.view.LiveUserCardView;
import com.thel.modules.live.view.LiveVipEnterView;
import com.thel.modules.live.view.SoftGiftMsgLayout;
import com.thel.modules.live.view.expensive.LiveTopGiftLayout;
import com.thel.modules.live.view.expensive.TopGiftBean;
import com.thel.modules.live.view.expensive.TopGiftItemImp;
import com.thel.modules.live.view.sound.LiveShowMicSortDialog;
import com.thel.modules.live.view.sound.LiveSoundChoiceDialogView;
import com.thel.modules.live.view.sound.LiveSoundCrowdDialogView;
import com.thel.modules.live.view.sound.LiveSoundEffectDialogView;
import com.thel.modules.main.home.moments.ReportActivity;
import com.thel.modules.main.home.moments.web.WebViewActivity;
import com.thel.modules.main.userinfo.BlockNetBean;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.api.commonapi.CommonApiBusiness;
import com.thel.tusdk.plain.CameraConfig;
import com.thel.ui.dialog.BeautifulDialog;
import com.thel.ui.dialog.ConnectMicDialog;
import com.thel.ui.dialog.ConnectMicDialog.CountDownOutListener;
import com.thel.ui.dialog.RechargeDialogFragment;
import com.thel.ui.dialog.UnidentifyFaceDialog;
import com.thel.ui.popupwindow.BeautyAndTextDialogView;
import com.thel.ui.widget.DanmuSelectedLayout;
import com.thel.ui.widget.GuideView;
import com.thel.ui.widget.LiveLevelUpView;
import com.thel.ui.widget.MeetItemViewGroup;
import com.thel.ui.widget.ScaleRecyclerView;
import com.thel.ui.widget.SoftInputViewGroup;
import com.thel.utils.DateUtils;
import com.thel.utils.DialogUtil;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.GsonUtils;
import com.thel.utils.JsonUtils;
import com.thel.utils.L;
import com.thel.utils.LinkMicScreenUtils;
import com.thel.utils.NotchUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SharedPrefUtils;
import com.thel.utils.SizeUtils;
import com.thel.utils.StringUtils;
import com.thel.utils.TextLimitWatcher;
import com.thel.utils.ToastUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;
import static com.thel.constants.TheLConstants.BUNDLE_KEY_NICKNAME;
import static com.thel.constants.TheLConstants.BUNDLE_KEY_USER_AVATAR;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_ANCHOR_FINISH;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_ANCHOR_RESET_IS_BOTTOM;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_ANCHOR_SCROLL_TO_BOTTOM;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_ANCHOR_UPDATE_MY_NET_STATUS_GOOD;
import static com.thel.modules.live.bean.LiveRoomMsgBean.TYPE_METHOD_OFFSEAT;
import static com.thel.modules.live.bean.LiveRoomMsgBean.TYPE_METHOD_ONSEAT;
import static com.thel.ui.dialog.ConnectMicDialog.TYPE_ACCEPT;
import static com.thel.ui.dialog.ConnectMicDialog.TYPE_CANCEL;
import static com.thel.ui.dialog.ConnectMicDialog.TYPE_HANGUP;
import static com.thel.ui.dialog.ConnectMicDialog.TYPE_I_NO_RESPONSE;
import static com.thel.ui.dialog.ConnectMicDialog.TYPE_REFUSE;
import static com.thel.ui.dialog.ConnectMicDialog.TYPE_REQUEST_NO_RESPONSE;

/**
 * Created by waiarl on 2017/11/3.
 */

public class LiveShowBaseViewFragment extends BaseFunctionFragment implements LiveShowCtrlListener, LiveShowContract.View, ILive, ILiveAnchor, View.OnClickListener {
    private final String TAG = LiveShowBaseViewFragment.class.getSimpleName();

    public final static int LINK_MIC_STATUS_NONE = -1;

    public final static int LINK_MIC_STATUS_AUDIENCE = 0;

    public final static int LINK_MIC_STATUS_FRIEND = 1;

    private TextView txt_my_status;
    private ImageView avatar;
    private TextView txt_nickname;
    private TextView txt_audience;
    private TextView txt_watch_live;
    private ImageView img_share;
    private ImageView img_close;
    private ScaleRecyclerView listview_chat;
    private SoftGiftMsgLayout giftMsgView;
    private GiftMsgCtrl giftMsgCtrl;
    private TextView txt_soft_money;
    private LiveBigGiftAnimLayout liveBigGiftAnimLayout;
    private LinearLayout soft_money_layout;
    private DanmuLayoutView danmu_layout_view;
    private LiveShowUserView live_show_user_view;
    private LiveVipEnterView live_vip_enter_view;
    private LiveLookRankView live_look_rank_view;

    private LinearLayout text_ll;
    private LinearLayout beauty_ll;
    private LinearLayout lens_ll;
    private RelativeLayout link_mic_ll;
    private LinearLayout pk_ll;

    //数据
    private LiveRoomBean liveRoomBean;
    private SoftEnjoyBean softEnjoyBean;
    private LiveShowContract.Presenter presenter;
    private String reportContent = "";//举报内容
    private boolean isBottom;

    private LiveShowDialogUtils dialogUtils;
    private LinearLayoutManager recyclerManager;
    private LiveRoomChatAdapter adapter;
    private List<LiveRoomMsgBean> msgList = new ArrayList<>();
    private List<GiftSendMsgBean> giftMsgWaitList = new ArrayList<>();//因为各种原因而 数据不全 不能显示的数据
    private List<GiftSendMsgBean> bigAnimGiftList = new ArrayList<>();//大动画播放列表
    private boolean isShowBigAnim = false;
    private boolean isGettingGiftData = false;//是否正在获取礼物列表数据
    private LiveWatchObserver observer;
    private boolean isRefreshingMsgList = false;
    private CallbackManager callbackManager;
    private LiveShowCaptureCloseDialog mCloseDialog;
    private boolean destoryed = false;
    private boolean isCreate = false;
    /***一下为Pk新增***/
    private LivePKFriendListView mPkFriendListViewDialog;
    private ConnectMicDialog dialog;
    private LivePKBloodView livePkBloodView;
    private LivePkCountDownView livePkCountDownView;
    private LivePkAssistsRankView livePkAssistsRandView;
    private TextView txt_finish_pk;
    private TextView txt_pk_other_name;
    private LivePkFriendBean livePkFriendBean;//直播PK的对方bean
    private LottieAnimationView live_pk_ae_view;
    private LivePkFailedView livePkFailedView;
    private ImageView img_live_pk;


    /***连麦 begin***/
    private LinkMicInfoView link_mic_layer;
    private boolean isLinkMic = false;
    /***连麦的 end***/

    private float ourGem = 0f;
    private float theriGem = 0f;
    /***本场新增软妹豆***/
    private TextView txt_now_gem;
    private boolean inPk = false;//是否在Pk
    private boolean inPkSummary = false;//是否是在pk总结阶段
    private int pk_fierce_sound_play_time = 0;

    /***直播间广告 begin***/
//    private View layout_live_ad;
//    private SimpleDraweeView ad_view;
//    private TextView top_tv;
//    private TextView offset_tv;
    private LiveAdViewManager liveAdViewManager;
    /***直播间广告 end***/

    private View view_pk_other;
    private LiveRoomMsgConnectMicBean liveRoomMsgConnectMicBean;
    protected LinearLayout control_ll;
    private double todayIncome = 0;
    private double defeated = 0;
    /***直播礼物排行榜***/
    private LiveGiftRankDialogView live_gift_ranking_dialog_view;
    /***用户名片***/
    private LiveUserCardDialogView live_user_card_dialog_view;
    private ImageView img_live_type;
    private String User_Type;
    private LiveTopGiftLayout live_top_gift_layout;

    private List<SoftGiftBean> offlineGiftList = new ArrayList<>();
    private MeetItemViewGroup multi_grid_view;
    private TextView mic_sort;
    private LinearLayout sound_effect;
    /**
     * 多人连麦
     **/
    private List<LiveMultiSeatBean> liveMultiSeatBeans = new ArrayList<>(8);
    private ImageView open_mic_iv;
    private LinearLayout open_mic_ll;
    private LottieAnimationView meetion_anim_bg;
    private TextView meetion_count_down;
    private LinearLayout ll_meetion;
    private LinearLayout lin_sound_bottom_music;
    private LinearLayout lin_sound_bottom_text;
    private LiveSoundChoiceDialogView mAppChoiceDialog;
    private LinearLayout ll_multi_view;
    private LinearLayout lin_multi_sound_view_bottom;
    private LinearLayout lin_sound_view_bottom;
    private LinearLayout lin_multi_sound_bottom_music;
    private LinearLayout lin_multi_sound_bottom_text;
//    private View layout_multi_live_ad;
    private LottieAnimationView user_is_talking;
    public boolean isMeeting = false;
    private boolean isMeetingInterval = false;//相遇是否在冷却中
//    private SimpleDraweeView ad_multi_view;
//    private TextView top_multi_tv;
    private TextView sort_mic_count;
    private OnMeetStatusListener mOnMeetStatusListener;
    private boolean hasShowMeetingTips = false;//第一次开启相遇，弹提示框
    private RelativeLayout mic_sort_ll;
    private LiveShowMicSortDialog liveSoundMicSortDialog;
    private boolean micSorting = false;//房间是否处于排麦模式
    private int sortMicListCount = 0;//排麦列表总数
    private static final int MIN_MEET_COUNT = 4;//开始相遇最小人数4人
    private static final int MEET_INTERVAL = 5 * 60;//相遇冷却时间
    private CameraConfig cameraConfigBean;
    private List<LivePkFriendBean> livePkFriendBeans = new ArrayList<>();
    private RelativeLayout mic_sort_bottom;
    private Handler mHandler = new Handler(Looper.getMainLooper());
    private TextView link_mic_count_tv;
    private int linkMicStatus = LINK_MIC_STATUS_NONE;
    private TextView live_notice;
    private LinkMicOwnerView link_mic_owner_view;
    private long startTime = 0;
    private LinearLayout ll_more;
    private LinearLayout ll_chat_view;
    private LinearLayout ll_multi_more;
    private LinearLayout ll_chat_multi_view;
    private RelativeLayout send_edit_msg;
    private ImageView img_switch_danmu;
    private EditText edit_input;
    private SoftInputViewGroup root_vg;
    private View lin_input;
    private KeyboardChangeListener.KeyBoardListener keyBoardListener;
    private KeyboardChangeListener.KeyBoardListener keyBoardListener2;
    private KeyboardChangeListener keyboardChangeListener;
    private RelativeLayout rel_down;
    private TextLimitWatcher textWatcher;
    private int barrageId = 0;//当barrageId为0是为普通消息，大于0时为弹幕消息
    private DanmuSelectedLayout danmu_selected_layout;
    private TextView send_tv;
    private LinearLayout ll_chat_sound_view;
    private UnidentifyFaceDialog mNoFaceDialog;
    private GuideView link_mic_guide_view;
    private int meetionStatus = BeautyAndTextDialogView.MEET_DEFAULT;//相遇状态
    private BeautyAndTextDialogView beautyAndTextDialogView;
    private LiveLevelUpView mLiveLevelUpView;
    private RelativeLayout rel_top;

    public static LiveShowBaseViewFragment getInstance(Bundle bundle) {
        LiveShowBaseViewFragment instance = new LiveShowBaseViewFragment();
        instance.setArguments(bundle);
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Bundle bundle = getArguments();
        cameraConfigBean = (CameraConfig) bundle.getSerializable(TheLConstants.BUNDLE_KEY_LIVE_ROOM_CAMERA_CONFIG);
        liveRoomBean = (LiveRoomBean) bundle.getSerializable(TheLConstants.BUNDLE_KEY_LIVE_ROOM);
        new LiveShowPresenter(this);
        refreshLiveShow(liveRoomBean);
        dialogUtils = new LiveShowDialogUtils();

        /** 分享初始化 **/
        callbackManager = CallbackManager.Factory.create();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_live_multi_capture_view, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initAnimVideoFrame(view);
        findViewById(view);
        initAdapter();
        initViewState();
        showPkFinishView();
    }

    private void initAnimVideoFrame(View view) {
        meetion_anim_bg = view.findViewById(R.id.meetion_anim_bg);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        isCreate = true;
        startTime = System.currentTimeMillis();
        if (liveRoomBean != null && liveRoomBean.activityInfoUrl != null) {
            presenter.getLiveAdInfo(liveRoomBean.activityInfoUrl);
        }
        refreshUI();
        setListener();
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {

            requestTopFansData();
            mHandler.postDelayed(runnable, 45000);
        }
    };

    private void requestTopFansData() {
        if (liveRoomBean != null && liveRoomBean.user != null) {
            presenter.getAnchorTopFans(liveRoomBean.user.id + "", 0);
        }
    }

    /**
     * 获取前三名粉丝信息
     */
    @Override
    public void refreshTopFans(LiveTopFansTodayBean topFansBean, int arriveToTopfans) {

        if (topFansBean != null && topFansBean.data != null) {
            live_look_rank_view.initView(topFansBean.data.topFans, LiveLookRankView.TYPE_CAPTURE);
        }

    }

    @Override public void sendShareMsg() {

    }

    @Override public void sendRecommendMsg() {

    }

    private void findViewById(View view) {
        findCommonView(view);
        findBottomView(view);
        findPkView(view);
        findAdView();
        findSoundMultiView();
    }

    private void findSoundMultiView() {
        //4.8.0 新增多人直播
        ll_multi_view = (LinearLayout) findViewById(R.id.ll_multi_view);
        //底部bottom按钮
        lin_multi_sound_view_bottom = (LinearLayout) findViewById(R.id.lin_multi_sound_view_bottom);
        multi_grid_view = (MeetItemViewGroup) findViewById(R.id.multi_grid_view);
        //相遇玩法
        //  mic_sort = (TextView) findViewById(R.id.mic_sort);
        //开始相遇 背景
        meetion_anim_bg = (LottieAnimationView) findViewById(R.id.meetion_anim_bg);

        meetion_count_down = (TextView) findViewById(R.id.meetion_count_down);

        ll_meetion = (LinearLayout) findViewById(R.id.ll_meetion);
        //音乐弹窗
        //  lin_multi_sound_bottom_music = (LinearLayout) findViewById(R.id.lin_multi_sound_bottom_music);
        //文字大小改变弹窗
        //  lin_multi_sound_bottom_text = (LinearLayout) findViewById(R.id.lin_multi_sound_bottom_text);
        ll_multi_more = (LinearLayout) findViewById(R.id.ll_multi_more);
        //多人连麦时 广告显示在聊天旁边
//        layout_multi_live_ad = findViewById(R.id.layout_multi_live_ad);
//        ad_multi_view = (SimpleDraweeView) findViewById(R.id.ad_multi_view);
//        top_multi_tv = (TextView) findViewById(R.id.top_multi_tv);
        sort_mic_count = (TextView) findViewById(R.id.sort_mic_count);
        link_mic_count_tv = (TextView) findViewById(R.id.link_mic_count_tv);
        live_notice = (TextView) findViewById(R.id.live_notice);
    }

    private void findAdView() {
        /***直播间广告 begin***/
//        layout_live_ad = findViewById(R.id.layout_live_ad);
//        ad_view = (SimpleDraweeView) findViewById(R.id.ad_view);
//        top_tv = (TextView) findViewById(R.id.top_tv);
//        offset_tv = (TextView) findViewById(R.id.offset_tv);

        ViewStub lefTopViewStub = (ViewStub) findViewById(R.id.live_ad_viewstub);
        ViewStub rightBottomViewStub = (ViewStub) findViewById(R.id.multi_live_ad_viewstub);
        liveAdViewManager = new LiveAdViewManager(lefTopViewStub, rightBottomViewStub);
        /***直播间广告 end***/

    }

    private void findCommonView(View view) {
        user_is_talking = (LottieAnimationView) findViewById(R.id.user_is_talking);
        user_is_talking.addAnimatorListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                user_is_talking.setVisibility(INVISIBLE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        txt_my_status = (TextView) findViewById(R.id.txt_my_status);
        avatar = (ImageView) findViewById(R.id.avatar);
        txt_nickname = (TextView) findViewById(R.id.txt_nickname);
        txt_audience = (TextView) findViewById(R.id.txt_audience);
        txt_watch_live = (TextView) findViewById(R.id.txt_watch_live);

        img_share = (ImageView) findViewById(R.id.img_share);
        img_close = (ImageView) findViewById(R.id.img_close);

        listview_chat = (ScaleRecyclerView) findViewById(R.id.listview_chat);

        //打赏消息显示界面
        giftMsgView = (SoftGiftMsgLayout) findViewById(R.id.gift_msg_view);
        giftMsgCtrl = new GiftMsgCtrl(giftMsgView);
        txt_soft_money = (TextView) findViewById(R.id.txt_soft_money);//主播的软妹币
        liveBigGiftAnimLayout = (LiveBigGiftAnimLayout) findViewById(R.id.big_gift_layout);//大动画布局

        soft_money_layout = (LinearLayout) findViewById(R.id.soft_money_layout);
        danmu_layout_view = (DanmuLayoutView) findViewById(R.id.danmu_layout_view);
        //用户加入
        live_show_user_view = (LiveShowUserView) findViewById(R.id.live_show_user_view);
        //3.1.0 新增：会员进入房间view
        live_vip_enter_view = (LiveVipEnterView) findViewById(R.id.live_vip_enter_view);
        //3.1.0 新增：直播日榜
        live_look_rank_view = (LiveLookRankView) findViewById(R.id.live_look_rank_view);

        /***本场新增软妹豆***/
        txt_now_gem = (TextView) findViewById(R.id.txt_now_gem);
        img_live_type = (ImageView) findViewById(R.id.img_live_type);

        live_top_gift_layout = (LiveTopGiftLayout) findViewById(R.id.live_top_gift_layout);
        root_vg = (SoftInputViewGroup) findViewById(R.id.root_vg);
        root_vg.setOnSoftInputShowListener(mOnSoftInputShowListener);
        root_vg.setNotInterceptViewsId(new int[]{R.id.send_edit_msg, R.id.danmu_layout});
        send_edit_msg = (RelativeLayout) findViewById(R.id.send_edit_msg);
        img_switch_danmu = (ImageView) findViewById(R.id.img_switch_danmu);
        edit_input = (EditText) findViewById(R.id.edit_input);
        lin_input = findViewById(R.id.lin_input);
        send_tv = (TextView) findViewById(R.id.send_tv);

        rel_down = (RelativeLayout) findViewById(R.id.rel_down);
        danmu_selected_layout = (DanmuSelectedLayout) findViewById(R.id.danmu_layout);
        mLiveLevelUpView = (LiveLevelUpView) findViewById(R.id.live_level_up_view);
        rel_top = (RelativeLayout) findViewById(R.id.rel_top);

        int marginTop = 0;

        try {
            marginTop = NotchUtils.getNotchHeight(TheLApp.context);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (marginTop == 0) {
            marginTop = SizeUtils.dip2px(TheLApp.context, 24);
        }

        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) rel_top.getLayoutParams();

        layoutParams.topMargin = marginTop;

        rel_top.setLayoutParams(layoutParams);
    }

    protected void findBottomView(View view) {
        lin_sound_view_bottom = (LinearLayout) findViewById(R.id.lin_sound_view_bottom);
        lin_sound_bottom_text = (LinearLayout) findViewById(R.id.lin_sound_bottom_text);
        lin_sound_bottom_music = (LinearLayout) findViewById(R.id.lin_sound_bottom_music);
        ll_chat_multi_view = (LinearLayout) findViewById(R.id.ll_chat_multi_view);
        control_ll = (LinearLayout) findViewById(R.id.control_ll);
       /* text_ll = (LinearLayout) findViewById(R.id.text_ll);
        beauty_ll = (LinearLayout) findViewById(R.id.beauty_ll);*/
        /**
         * 4.18.0-收纳了 文字大小设置和美颜开关 新增主播可以打文字
         * */

        ll_more = view.findViewById(R.id.ll_more);
        ll_chat_view = view.findViewById(R.id.ll_chat_view);
        lens_ll = (LinearLayout) findViewById(R.id.lens_ll);
        link_mic_ll = (RelativeLayout) findViewById(R.id.link_mic_ll);
        pk_ll = (LinearLayout) findViewById(R.id.pk_ll);
        //音效
        sound_effect = (LinearLayout) findViewById(R.id.sound_effect_ll);
        open_mic_iv = (ImageView) findViewById(R.id.open_mic_iv);
        open_mic_ll = (LinearLayout) findViewById(R.id.open_mic_ll);
        mic_sort_bottom = (RelativeLayout) findViewById(R.id.open_mic_ll_bottom);
        ll_chat_sound_view = (LinearLayout) findViewById(R.id.ll_chat_sound_view);
        link_mic_guide_view = (GuideView) findViewById(R.id.link_mic_guide_view);

    }

    private void findPkView(View view) {
        /***一下为直播PK新增变量***/

        //pk血条
        livePkBloodView = (LivePKBloodView) findViewById(R.id.live_pk_blood_view);
        //Pk倒计时
        livePkCountDownView = (LivePkCountDownView) findViewById(R.id.live_pk_count_down_view);
        //pk援助
        livePkAssistsRandView = (LivePkAssistsRankView) findViewById(R.id.live_pk_assists_rank_view);
        //结束Pk按钮（位置需要等到Pk开始的时候再固定）
        txt_finish_pk = (TextView) findViewById(R.id.txt_finish_pk);
        //Pk的时候的对方的名字
        txt_pk_other_name = (TextView) findViewById(R.id.txt_pk_other_name);
        //pk 的 AE动画
        live_pk_ae_view = (LottieAnimationView) findViewById(R.id.live_pk_ae_view);
        livePkFailedView = (LivePkFailedView) findViewById(R.id.live_pk_failed_view);
        view_pk_other = findViewById(R.id.view_pk_other);
        img_live_pk = (ImageView) findViewById(R.id.img_live_pk);
        /***一上为直播PK新增变量***/
        /***连麦View初始化 begin***/
        link_mic_layer = (LinkMicInfoView) findViewById(R.id.link_mic_layer);
        link_mic_owner_view = (LinkMicOwnerView) findViewById(R.id.link_mic_owner_view);
        /***连麦View初始化 end***/
    }

    protected View findViewById(int id) {
        final View container = getView();
        return container.findViewById(id);
    }

    private void initAdapter() {
        final int mTextSize = SharedPrefUtils.getInt(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.LIVE_SHOW_CAPTURE_TEXTSIZE, LiveTextSizeChangedListener.TEXT_SIZE_STANDARD);
        adapter = new LiveRoomChatAdapter(msgList, mTextSize);
        adapter.setLiveRoomBean(liveRoomBean);
        recyclerManager = new LinearLayoutManager(getActivity());
        recyclerManager.setOrientation(RecyclerView.VERTICAL);
        listview_chat.setLayoutManager(recyclerManager);
        listview_chat.setAdapter(adapter);


        for (int i = 0; i < 8; i++) {
            LiveMultiSeatBean liveMultiSeatBean = new LiveMultiSeatBean();
            liveMultiSeatBeans.add(liveMultiSeatBean);
        }
        //初始化多人连麦的座位adapter
        multi_grid_view.refreshAllSeats(liveMultiSeatBeans);
        multi_grid_view.setOnItemClickListener(new MeetItemViewGroup.OnItemClickListener() {
            @Override
            public void onItemClick(View view, final int position) {
                final LiveMultiSeatBean item = multi_grid_view.getData().get(position);
                String currentUserId = item.userId;
                if (!TextUtils.isEmpty(currentUserId)) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(TheLConstants.BUNDLE_KEY_SEAT_BEAN, item);
                    bundle.putString(TheLConstants.BUNDLE_KEY_USER_ID, String.valueOf(liveRoomBean.user.id));
                    final LiveSoundCrowdDialogView liveSoundCrowdDialogView = LiveSoundCrowdDialogView.newInstance(bundle);
                    liveSoundCrowdDialogView.setBanMicListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            liveSoundCrowdDialogView.dismiss();
                            if (observer != null) {
                                String micStatus;
                                if (item.micStatus.equals(LiveRoomMsgBean.TYPE_MIC_STATUS_CLOSE)) {
                                    micStatus = LiveRoomMsgBean.TYPE_MIC_STATUS_ON;
                                } else {
                                    micStatus = LiveRoomMsgBean.TYPE_MIC_STATUS_OFF;
                                }
                                observer.mute(position, micStatus);
                            }
                        }
                    });
                    liveSoundCrowdDialogView.setExitMicListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            liveSoundCrowdDialogView.dismiss();
                            if (observer != null) {
                                observer.offSeat(position);
                            }
                        }
                    });
                    liveSoundCrowdDialogView.setBanExitMicListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            liveSoundCrowdDialogView.dismiss();
                            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setMessage(R.string.ban_mic_tips);
                            builder.setPositiveButton(getString(R.string.phone_verify_ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (observer != null) {
                                        observer.offSeat(position);
                                        observer.banHer(item.userId);
                                    }
                                    dialog.dismiss();
                                }
                            });
                            builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            builder.create().show();
                        }
                    });
                    //进入个人主页
                    liveSoundCrowdDialogView.setEnterUserInfoListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            liveSoundCrowdDialogView.dismiss();
                            gotoUserInfoActivity(item.userId);
                        }
                    });
                    liveSoundCrowdDialogView.show(getChildFragmentManager(), LiveSoundCrowdDialogView.class.getName());
                }
            }
        });
    }

    private void gotoUserInfoActivity(String userId) {

        getLiveUserCard(userId, ReportActivity.REPORT_TYPE_AUDIENCE);

    }

    protected void initViewState() {
        //就设置Pk结束失败的view的布局
        LiveUtils.setLivePkFailedViewParam(livePkFailedView, LiveUtils.getPkUpLoadParams());
        LiveUtils.setLivePkFailedViewParam(view_pk_other, LiveUtils.getPkUpLoadParams());
        switchEditInputVisiable(false);
        send_edit_msg.setVisibility(View.GONE);
        if (keyboardChangeListener != null) {
            keyboardChangeListener.close();
        }
        clearInput();

        /***
         * 一进场就弹出分享主播弹窗
         * */
        share();
    }

    private void setListener() {
        mHandler.post(runnable);

        setCommonListener();
        setBottomListener();
        setPkListener();
        setLinkMicListener();
        setAdListener();
        setMultiSoundListener();
    }

    private void setMultiSoundListener() {
        //弹出音效框
        sound_effect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LiveSoundEffectDialogView effectDialogView = new LiveSoundEffectDialogView();
                effectDialogView.setOnItemClickListener(new LiveSoundEffectDialogView.OnItemClickListener() {
                    @Override
                    public void onItemClick(int position) {
                        if (observer != null) {
                            observer.playEffect(position);
                        }
                    }
                });
                effectDialogView.show(getActivity().getSupportFragmentManager(), LiveShowBaseViewFragment.class.getName());
            }
        });
        mic_sort_bottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putBoolean("micSorting", micSorting);
                bundle.putBoolean("full", guestCount() == 8);
                liveSoundMicSortDialog = LiveShowMicSortDialog.newInstance(bundle);
                liveSoundMicSortDialog.setMicSortListener(new LiveShowMicSortDialog.MicSortListener() {
                    @Override
                    public void onCreate() {
                        if (observer != null) {
                            observer.getMicSortList();
                        }
                    }

                    @Override
                    public void openMicSort() {
                        if (!micSorting && observer != null) {
                            observer.openMicSort();
                            micSorting = true;
                        }
                    }

                    @Override
                    public void clostMicSort() {
                        if (observer != null) {
                            observer.closeMicSort();
                            micSorting = false;
                            sort_mic_count.setVisibility(INVISIBLE);
                        }
                    }

                    @Override
                    public void agreeOnSeat(String userId) {
                        if (observer != null) {
                            observer.agreeOnSeat(userId, liveRoomBean.agora);
                        }
                    }
                });
                liveSoundMicSortDialog.show(getChildFragmentManager(), LiveShowMicSortDialog.class.getName());
            }
        });

        ll_multi_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                beautyAndTextDialogView = new BeautyAndTextDialogView();
                beautyAndTextDialogView.setMeetionStatus(meetionStatus, "");
                beautyAndTextDialogView.setTag(BeautyAndTextDialogView.TYPE_MULTI_VOICE);
                if (beautyAndTextDialogView != null) {
                    //背景音乐
                    beautyAndTextDialogView.setMeetionListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //开始相遇
                            meetionViewClick();
                        }
                    });

                    //文字设置大小
                    beautyAndTextDialogView.setTextSizeListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showTextSizeDialog();
                        }
                    });
                    beautyAndTextDialogView.show(getChildFragmentManager(), BeautyAndTextDialogView.class.getName());
                }
            }
        });
        ll_chat_multi_view.setOnClickListener(this);
        ll_chat_view.setOnClickListener(this);
        ll_chat_sound_view.setOnClickListener(this);

    }

    /**
     * 显示软键盘
     */
    private void showKeyboard() {
        if (null != getActivity()) {
            ViewUtils.showSoftInput(getContext(), edit_input);
        }
    }

    /**
     * 清空输入框
     */
    @Override
    public void clearInput() {
        edit_input.setText("");
    }

    /**
     * 发送弹幕返回结果
     *
     * @param danmuResult 发送弹幕成功返回的payload
     */
    @Override
    public void setDanmuResult(String danmuResult) {
        try {

            DanmuResultBean danmuResultBean = GsonUtils.getObject(danmuResult, DanmuResultBean.class);

            if (danmuResultBean.success) {
                openInput(true);
                if (danmuResultBean.goldBalance != -1) {
                    softEnjoyBean.gold = danmuResultBean.goldBalance;

                    if (danmu_selected_layout != null) {

                        danmu_selected_layout.setGold(danmuResultBean.goldBalance);
                    }
                }
            } else {
                openInput(false);
            }
        } catch (Exception e) {
            openInput(false);
            e.printStackTrace();
        }
    }

    /**
     * 解锁输入框
     *
     * @param clearInput 是否清空输入框
     */
    @Override
    public void openInput(boolean clearInput) {
    }

    /**
     * 改变输入框显示状态
     *
     * @param show
     */
    private void switchEditInputVisiable(boolean show) {
        if (show) {
            send_edit_msg.setVisibility(View.VISIBLE);
            lin_input.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.transparent));
        } else {
            send_edit_msg.setVisibility(View.GONE);
            // 添加send_edit_msg消失动画
            //AnimUtils.disappearSendEdit(send_edit_msg);
            lin_input.setBackgroundResource(R.drawable.live_chat_black_bg);
        }
    }

    private SoftInputViewGroup.OnSoftInputShowListener mOnSoftInputShowListener = new SoftInputViewGroup.OnSoftInputShowListener() {
        @Override
        public void onSoftInputShow(boolean isShow, boolean isShowDanmu) {

            L.d(TAG, " onSoftInputShow isShow : " + isShow);

            L.d(TAG, " onSoftInputShow isShowDanmu : " + isShowDanmu);

            if (isShow || isShowDanmu) {
                send_edit_msg.setVisibility(View.VISIBLE);
                if (LiveRoomBean.TYPE_VOICE == liveRoomBean.audioType) {
                    if (LiveRoomBean.MULTI_LIVE == liveRoomBean.isMulti) {
                        lin_multi_sound_view_bottom.setVisibility(View.GONE);
                    } else {

                        lin_sound_view_bottom.setVisibility(View.GONE);
                    }
                } else {

                    control_ll.setVisibility(View.GONE);
                }

            } else {
                send_edit_msg.setVisibility(View.GONE);

                if (LiveRoomBean.TYPE_VOICE == liveRoomBean.audioType) {
                    if (LiveRoomBean.MULTI_LIVE == liveRoomBean.isMulti) {
                        lin_multi_sound_view_bottom.setVisibility(View.VISIBLE);
                    } else {
                        lin_sound_view_bottom.setVisibility(VISIBLE);
                    }
                } else {
                    control_ll.setVisibility(VISIBLE);
                }
            }

            //显示弹幕选择界面
            if (isShowDanmu) {
                edit_input.setFocusable(false);
                edit_input.setCursorVisible(false);
            } else {
                edit_input.setFocusable(true);
                edit_input.setFocusableInTouchMode(true);
                edit_input.requestFocus();
                edit_input.setCursorVisible(true);
            }

            L.d(TAG, " onSoftInputShow edit_input.isFocusable() : " + edit_input.isFocusable());

        }
    };


    /**
     * 设置键盘监听
     */
    private void setKeyboardChangeListener() {
        keyBoardListener = new KeyboardChangeListener.KeyBoardListener() {//横屏状态下才设置监听
            @Override
            public void onKeyboardChange(boolean isShow, int keyboardHeight) {//键盘是否弹出监听
                if (isShow) {
                    listview_chat.setVisibility(View.VISIBLE);
                    setRelDownListener(true);
                } else {

                    setRelDownListener(false);
                    lin_input.setVisibility(View.GONE);
                    listview_chat.setVisibility(View.GONE);
                }
            }
        };
        keyBoardListener2 = new KeyboardChangeListener.KeyBoardListener() {//竖屏状态下的软键盘监听
            @Override
            public void onKeyboardChange(boolean isShow, int keyboardHeight) {//键盘是否弹出监听
                if (isShow) {
                    switchEditInputVisiable(true);
                    setRelDownListener(true);
                } else {
                    setRelDownListener(false);

                    observer.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            switchEditInputVisiable(false);

                        }
                    }, 300);

                }
            }
        };

        if (getActivity().getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {//横屏状态下才设置监听
            keyboardChangeListener.reset();
            keyboardChangeListener.setKeyBoardListener(keyBoardListener);
        } else {
            keyboardChangeListener.reset();
            keyboardChangeListener.setKeyBoardListener(keyBoardListener2);
        }

    }

    /**
     * 点击键盘以外消失键盘
     *
     * @param show
     */
    private void setRelDownListener(boolean show) {
        rel_down.setClickable(show);

    }

    /**
     * 显示音乐库
     */
    protected void showMusicAppView() {
        final Bundle bundle = new Bundle();
        mAppChoiceDialog = LiveSoundChoiceDialogView.getInstance(bundle);
        mAppChoiceDialog.show(getActivity().getSupportFragmentManager(), getClass().getSimpleName());

    }

    /**
     * 相遇按钮点击事件
     */
    private void meetionViewClick() {
        //第一次开始相遇，弹出相遇玩法的dialog
        if (!hasShowMeetingTips) {
            showMeetPlayDialog();
            hasShowMeetingTips = true;
            return;
        }
        if (isMeetingInterval) {
            if (beautyAndTextDialogView != null && beautyAndTextDialogView.isAdded()) {
                beautyAndTextDialogView.setMeetionTips(StringUtils.getString(R.string.meeting_interval));
            }
        } else {
            if (guestCount() >= MIN_MEET_COUNT) {
                if (observer != null) {
                    observer.startEncounter();
                    meetionStatus = BeautyAndTextDialogView.MEETING;
                    if (beautyAndTextDialogView != null && beautyAndTextDialogView.isAdded()) {
                        beautyAndTextDialogView.setMeetionStatus(meetionStatus, "");
                    }
                }
            } else {
                if (beautyAndTextDialogView != null && beautyAndTextDialogView.isAdded()) {
                    beautyAndTextDialogView.setMeetionTips(StringUtils.getString(R.string.meeting_guest_count));
                }
            }

        }
    }

    /**
     * 开始相遇背景播放动画
     */
    private void playBgAnima() {
        meetion_anim_bg.setVisibility(VISIBLE);
        meetion_anim_bg.setAnimation("live_bg_heart.json");
        meetion_anim_bg.loop(true);
        meetion_anim_bg.playAnimation();
    }

    private int guestCount() {

        if (multi_grid_view != null) {

            List<LiveMultiSeatBean> data = multi_grid_view.getData();

            if (data != null) {

                int count = 0;

                for (int i = 0; i < data.size(); i++) {

                    LiveMultiSeatBean liveMultiSeatBean = data.get(i);

                    if (liveMultiSeatBean.userId != null && liveMultiSeatBean.userId.length() > 0) {
                        count++;
                    }
                }

                return count;
            }
        }
        return 0;
    }

    private void showMeetPlayDialog() {
        final LiveMeetionPlayDialog meetionPlayDialog = new LiveMeetionPlayDialog(getActivity());
        meetionPlayDialog.setMeetingStartListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (guestCount() < MIN_MEET_COUNT) {
                    if (beautyAndTextDialogView != null && beautyAndTextDialogView.isAdded()) {
                        beautyAndTextDialogView.setMeetionTips(StringUtils.getString(R.string.meeting_guest_count));
                    }
                    meetionPlayDialog.dismiss();
                    return;
                }
                if (observer != null) {
                    observer.startEncounter();
                    meetionStatus = BeautyAndTextDialogView.MEETING;
                    if (beautyAndTextDialogView != null && beautyAndTextDialogView.isAdded()) {
                        beautyAndTextDialogView.setMeetionStatus(meetionStatus, "");
                    }
                }
                meetionPlayDialog.dismiss();
            }
        });
        meetionPlayDialog.show();
    }

    private void setCommonListener() {
        rel_down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissKeyboard();
            }
        });
        //输入限制

        textWatcher = new TextLimitWatcher(100, true, "");
        edit_input.addTextChangedListener(textWatcher);
        // 输入法键盘完成事件
        edit_input.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    if (!TextUtils.isEmpty(edit_input.getText().toString().trim())) {
                        if (barrageId <= 0) {
                            sendMsg();
                        } else {
                            sendDanmu();
                        }
                    }
                    return true;
                }
                return false;
            }
        });
        send_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(edit_input.getText().toString().trim())) {
                    if (barrageId <= 0) {
                        sendMsg();
                    } else {
                        sendDanmu();
                    }
                }
            }
        });
        //发送弹幕（取消发送弹幕）
        img_switch_danmu.setOnClickListener(new View.OnClickListener() {//弹幕按钮，在有礼物打赏信息（有余额信息后）才能点击
            @Override
            public void onClick(View v) {
                switchDanmuView();
            }
        });
        edit_input.setOnTouchListener(new OnEditTouchListener() {
            @Override
            protected void onTouch(boolean isSingleClick) {

                if (isSingleClick) {
                    root_vg.showKeyBoardAndHideDanmuView();
                }
            }

        });
        danmu_selected_layout.setOnDanmuSelectedListener(new DanmuSelectedLayout.OnDanmuSelectedListener() {
            @Override
            public void onDanmuSelected(String selectedType) {
                switch (selectedType) {
                    case DanmuSelectedLayout.TYPE_DANMU_CLOSE:

                        img_switch_danmu.setImageResource(R.mipmap.btn_dan);

                        barrageId = 0;

                        root_vg.hideDanmu(true);

                        edit_input.setHint(TheLApp.context.getResources().getString(R.string.chat_activity_input_hint));

                        break;
                    case DanmuSelectedLayout.TYPE_DANMU_LEVEL:

                        img_switch_danmu.setImageResource(R.mipmap.btn_level_barrage);

                        barrageId = 1;

                        setInputHint(2);

                        break;
                    case DanmuSelectedLayout.TYPE_DANMU_BOTTLE:

                        img_switch_danmu.setImageResource(R.mipmap.btn_bottle);

                        barrageId = 2;

                        setInputHint(3);

                        break;
                    case DanmuSelectedLayout.TYPE_DANMU_CAT:

                        img_switch_danmu.setImageResource(R.mipmap.btn_cat);

                        barrageId = 3;

                        setInputHint(3);

                        break;
                    case DanmuSelectedLayout.TYPE_DANMU_CUPID:

                        img_switch_danmu.setImageResource(R.mipmap.btn_cupid);

                        barrageId = 4;

                        setInputHint(3);

                        break;
                    default:

                        img_switch_danmu.setImageResource(R.mipmap.btn_dan);

                        barrageId = 0;

                        break;
                }
            }
        });

        //输入限制

        img_close.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                if (getActivity() != null && !getActivity().isDestroyed()) {
                    showLiveClosedDialog();
                }
            }
        });

        img_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                share();
            }
        });
        listview_chat.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                switch (newState) {
                    case RecyclerView.SCROLL_STATE_IDLE:
                        isBottom = recyclerManager.findLastVisibleItemPosition() + 1 >= recyclerManager.getItemCount();
                        isScrolling = false;
                        break;
                    case RecyclerView.SCROLL_STATE_DRAGGING:
                        isScrolling = true;
                        break;
                    case RecyclerView.SCROLL_STATE_SETTLING:
                        isScrolling = true;
                        break;
                }
            }
        });

        giftMsgView.setOnGiftItemClickListener(new SoftGiftMsgLayout.OnGiftItemClickListener() {
            @Override
            public void showUserMsg(View v, String userId) {
                getLiveUserCard(userId, ReportActivity.REPORT_TYPE_REPORT_USER);

            }
        });

        soft_money_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                showRankingList();
            }
        });

        danmu_layout_view.setOnItemClickListener(new DanmuLayoutView.OnItemClickListener() {//弹幕Item点击
            @Override
            public void onClick(View v, DanmuBean danmuBean) {
                if (!Utils.isMyself(danmuBean.userId)) {
                    setReportContent(danmuBean.nickName + ":" + danmuBean.content + "");
                    getLiveUserCard(danmuBean.userId, ReportActivity.REPORT_TYPE_AUDIENCE);
                }
            }
        });
        live_show_user_view.setOnItemClickListener(new LiveShowUserView.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, String userId) {
                getLiveUserCard(userId, ReportActivity.REPORT_TYPE_AUDIENCE);
            }
        });

        live_look_rank_view.setRankListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRankingList();
            }
        });
        live_look_rank_view.setOnUserClickListener(new LiveLookRankView.OnUserClickListener() {
            @Override
            public void onUserClick(LiveTopFansBean bean) {
                if (bean != null) {
                    getLiveUserCard(bean.userId, ReportActivity.REPORT_TYPE_AUDIENCE);
                }
            }
        });
        live_vip_enter_view.setOnUserClickListener(new LiveVipEnterView.OnUserClickListener() {
            @Override
            public void clickUser(String userId) {
                if (userId != null) {
                    getLiveUserCard(userId, ReportActivity.REPORT_TYPE_AUDIENCE);
                }
            }
        });

        adapter.setLiveChatListener(new LiveRoomChatAdapter.ChatItemClickListener() {
            @Override
            public void clickAvatar(View v, int position, LiveRoomMsgBean bean) {
                getLiveUserCard(bean.userId, ReportActivity.REPORT_TYPE_AUDIENCE);
                if (!TextUtils.isEmpty(bean.content)) {
                    setReportContent(bean.nickName + ":" + bean.content + "");
                }

            }

            @Override
            public void clickText(View v, int position, LiveRoomMsgBean bean) {
                if (!TextUtils.isEmpty(bean.content)) {
                    setReportContent(bean.nickName + ":" + bean.content + "");
                }
                getLiveUserCard(bean.userId, ReportActivity.REPORT_TYPE_AUDIENCE);
            }
        });
        liveBigGiftAnimLayout.setVideoAnimListener(new VideoAnimListener() {
            @Override
            public void complete() {
                playNextBigAnim();
            }
        });
        live_top_gift_layout.setTopGiftClickListener(new TopGiftItemImp.TopGiftClickListener() {
            @Override
            public void clickSender(String userId) {
                getLiveUserCard(userId, ReportActivity.REPORT_TYPE_AUDIENCE);
            }

            @Override
            public void clickReceiver(String userId) {
                getLiveUserCard(userId, ReportActivity.REPORT_TYPE_ANCHOR);

            }

            @Override
            public void clickJumpToLive(String userId) {

            }
        });

        live_notice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (liveRoomBean != null && !TextUtils.isEmpty(liveRoomBean.announcement)) {
                    LiveNoticeShowDialog liveNoticeShowDialog = LiveNoticeShowDialog.newInstance(liveRoomBean.announcement);
                    liveNoticeShowDialog.show(getChildFragmentManager(), LiveNoticeShowDialog.class.getName());
                }
            }
        });
    }

    private void setInputHint(int gold) {

        if (danmu_selected_layout.isFreeBarrage()) {
            gold = 0;
        }
        edit_input.setHint(TheLApp.context.getResources().getString(R.string.danmu_hint, gold));

    }

    protected void setBottomListener() {
        link_mic_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFriendListView(LiveConstant.TYPE_LINK_MIC);
            }
        });


        /***显示pk申请好友列表***/
        pk_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isLinkMic) {
                    showFriendListView(LiveConstant.TYPE_PK);

                }
            }
        });
        ll_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                BeautyAndTextDialogView beautyAndTextDialogView = new BeautyAndTextDialogView();
                beautyAndTextDialogView.setTag(BeautyAndTextDialogView.TYPE_VIDEO);

                if (beautyAndTextDialogView != null) {
                    //美颜
                    beautyAndTextDialogView.setBeautyListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            BeautifulDialog beautifulDialog = BeautifulDialog.newInstance(cameraConfigBean, false);
                            beautifulDialog.setBeautyListener(new BeautifulDialog.BeautyListener() {

                                @Override
                                public void adjustBeauty(String key, float process) {
                                    observer.adjustBeauty(key, process);
                                }

                                @Override
                                public void dialogClose(CameraConfig cameraConfig) {
                                    cameraConfigBean = cameraConfig;
                                }
                            });
                            beautifulDialog.show(getChildFragmentManager(), BeautifulDialog.class.getName());
                        }
                    });

                    //文字设置大小
                    beautyAndTextDialogView.setTextSizeListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showTextSizeDialog();
                        }
                    });
                    beautyAndTextDialogView.show(getChildFragmentManager(), BeautyAndTextDialogView.class.getName());
                }
            }
        });

/*
        text_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTextSizeDialog();
            }
        });

        beauty_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BeautifulDialog beautifulDialog = BeautifulDialog.newInstance(cameraConfigBean, false);
                beautifulDialog.setBeautyListener(new BeautifulDialog.BeautyListener() {

                    @Override
                    public void adjustBeauty(String key, float process) {
                        observer.adjustBeauty(key, process);
                    }

                    @Override
                    public void dialogClose(CameraConfig cameraConfig) {
                        cameraConfigBean = cameraConfig;
                    }
                });
                beautifulDialog.show(getChildFragmentManager(), BeautifulDialog.class.getName());
            }
        });*/

        lens_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                observer.switchCamera();
            }
        });
        lin_sound_bottom_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTextSizeDialog();

            }
        });
        lin_sound_bottom_music.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMusicAppView();

            }
        });
   /*     lin_multi_sound_bottom_music.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMusicAppView();
            }
        });*/

        open_mic_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tag = (String) open_mic_iv.getTag();
                if (tag.equals("open")) {
                    open_mic_iv.setTag("close");
                    open_mic_iv.setImageResource(R.mipmap.icon_mic_close);
                    observer.muteLocalAudioStream(true);
                } else {
                    open_mic_iv.setTag("open");
                    open_mic_iv.setImageResource(R.mipmap.icn_live_openmic);
                    observer.muteLocalAudioStream(false);

                }
            }
        });

    }

    /**
     * 发送弹幕
     */
    private void sendDanmu() {
        if (liveRoomBean.user.gold < liveRoomBean.barrageCost && danmu_selected_layout != null && !danmu_selected_layout.isFreeBarrage()) {//如果点击的价格高于余额
            String giftName = TheLApp.context.getResources().getString(R.string.danmu);
            showRechargeDialog(false, 0, giftName, liveRoomBean.barrageCost, liveRoomBean.user.gold);//显示去充值对话框
            return;
        }

        String content = edit_input.getText().toString().trim();

        observer.sendDanmu("30", content, String.valueOf(barrageId));

        if (danmu_selected_layout != null) {

            if (danmu_selected_layout.isFreeBarrage()) {
                if (barrageId == 0) {
                    edit_input.setHint(TheLApp.context.getResources().getString(R.string.chat_activity_input_hint));
                }

                if (barrageId == 1) {
                    edit_input.setHint(TheLApp.context.getResources().getString(R.string.danmu_hint, 2));
                }

                if (barrageId > 1) {
                    edit_input.setHint(TheLApp.context.getResources().getString(R.string.danmu_hint, 3));

                }
            }
            danmu_selected_layout.setMoney();

            edit_input.setText("");
        }

    }

    /**
     * 发送消息
     */
    private void sendMsg() {

        String msg = edit_input.getText().toString().trim();

        sendMsg(msg);
    }

    private void sendMsg(String msg) {

        if (msg.contains("卡")) {
            AnalyticsBean analyticsBean = LiveInfoLogBean.getInstance().getLiveLowSpeedAnalytics();
            analyticsBean.logType = "liveTxtUpload";
            analyticsBean.roomId = liveRoomBean.liveId;
            analyticsBean.liveUserId = UserUtils.getMyUserId();
            analyticsBean.reason = msg;
            // analyticsBean.inUrl = observer.getPlayUrl();
            LiveUtils.pushLivePointLog(analyticsBean);
        }

        observer.sendInputMsg(msg);
    }

    /**
     * 发送弹幕（取消发送弹幕）
     */
    private void switchDanmuView() {

        L.d(TAG, " root_vg.isDanmuShowing() : " + root_vg.isDanmuShowing());

        if (!root_vg.isDanmuShowing()) {
            textWatcher.setParams(60, true, getString(R.string.danmu_toast));
            final String inputSt = edit_input.getText().toString().trim();
            if (Utils.charCount(inputSt) > 60) {
                edit_input.setText(Utils.getLimitedStr(inputSt, 60));
                edit_input.setSelection(edit_input.getText().length());
            }
            root_vg.showDanmu();
        }

    }

    /**
     * 关闭键盘
     */
    private void dismissKeyboard() {
        // 取消键盘
        if (getActivity() != null) {
            ViewUtils.hideSoftInput(getActivity(), edit_input);
        }
    }

    /**
     * 充值对话框
     */
    private void showRechargeDialog(boolean isCombo, int comboCount, String giftName, int giftGold, long balance) {

        L.d(TAG, " isCombo : " + isCombo);

        L.d(TAG, " comboCount : " + comboCount);

        L.d(TAG, " giftName : " + giftName);

        L.d(TAG, " giftGold : " + giftGold);

        L.d(TAG, " balance : " + balance);

        if (getActivity() != null) {
            String tips;
            if (isCombo) {
                long difference_value = giftGold * comboCount - balance;
                tips = TheLApp.context.getResources().getString(R.string.balance_insufficient_2, comboCount, giftName, difference_value);
            } else {
                long difference_value = giftGold - balance;
                tips = TheLApp.context.getResources().getString(R.string.balance_insufficient_1, giftName, difference_value);
            }
          /*  GrowingIOUtil.payTrack(GrowingIoConstant.LIVE_PAGE_POPWIN);
            GrowingIOUtil.payEvar(GrowingIoConstant.G_LivePagePopWin);
         */
            RechargeDialogFragment rechargeDialogFragment = new RechargeDialogFragment();
            rechargeDialogFragment.setTips(tips);
            rechargeDialogFragment.show(getActivity().getSupportFragmentManager(), RechargeDialogFragment.class.getName());
        }
    }


    private void setPkListener() {
        /***关闭Pk***/
        txt_finish_pk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishPk();
            }
        });
        /**
         * 直播Pk倒计时监听
         */
        livePkCountDownView.setOnLivePkCountDownListener(new LivePkCountDownView.LivePkCountDownListener() {
            @Override
            public void countDownPeriod(int period) {
                switch (period) {
                    case LivePkCountDownView.PERIOD_MIDDLE:
                        playPkAeAnim(LiveConstant.LIVE_PK_AE_ANIM_MIDDLE);
                        break;
                    case LivePkCountDownView.PERIOD_LAST_MINUTE:
                        playLastMinuteAnim();//播放最后一分钟动画
                        break;
                    case LivePkCountDownView.PERIOD_LAST_TEN_SECOND:
                        showPkSound(LiveConstant.LIVE_PK_SOUND_LAST_TEN_SECOND);
                        break;
                    case LivePkCountDownView.PERIOD_SUMMARY_END:
                        tryFinishPkStream();
                        break;
                }
            }
        });
        txt_pk_other_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (livePkFriendBean != null) {
                    presenter.getLiveUserCard(livePkFriendBean.id + "");
                }
            }
        });
        livePkAssistsRandView.setOnAssisterClickListener(new LivePkContract.AssistRankClickListener() {
            @Override
            public void onAssisterClick(LivePkAssisterBean bean, int position) {
                if (bean != null) {
                    getLiveUserCard(bean.userId, ReportActivity.REPORT_TYPE_ANCHOR);
                }
            }
        });
        view_pk_other.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (livePkFriendBean != null) {
                    getLiveUserCard(livePkFriendBean.id, ReportActivity.REPORT_TYPE_ANCHOR);
                }
            }
        });
    }

    private void setLinkMicListener() {

        link_mic_layer.setOnLinkMicHangupClickListener(mOnLinkMicHangupClickListener);

        link_mic_owner_view.setOnLinkMicClickListener(mOnLinkMicHangupClickListener);

        link_mic_layer.setOnLinkMicNickNameClickListener(mOnLinkMicNickNameClickListener);
    }

    private LinkMicInfoView.OnLinkMicNickNameClickListener mOnLinkMicNickNameClickListener = new LinkMicInfoView.OnLinkMicNickNameClickListener() {

        @Override
        public void onClickNickName(String toUserId) {
            getLiveUserCard(toUserId, ReportActivity.REPORT_TYPE_ANCHOR);

        }
    };

    private LinkMicInfoView.OnLinkMicHangupClickListener mOnLinkMicHangupClickListener = new LinkMicInfoView.OnLinkMicHangupClickListener() {

        @Override
        public void onHangup(String toUserId) {


            DialogUtil.showConfirmDialog(getActivity(), "", getString(R.string.sure_end_mic), getString(R.string.info_ok), getString(R.string.info_no), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    if (observer != null && toUserId != null) {
                        observer.hangupConnectMic("hangup", toUserId);
                        linkMicHangup();
                    }
                }
            });

        }
    };

    private void setAdListener() {

    }

    private LiveLinkMicDialog liveLinkMicDialog;

    /**
     * 显示Pk请求好友列表view
     *
     * @param type
     */
    private void showFriendListView(int type) {
        if (getActivity() == null) {
            return;
        }

        if (type == LiveConstant.TYPE_PK) {

            mPkFriendListViewDialog = new LivePKFriendListView();

            mPkFriendListViewDialog.setOnFriendRequestClickListener(new LivePkContract.FriendRequestClickListener() {
                @Override
                public void onClickRequest(LivePkFriendBean friendBean, int type, int position) {

                    LiveInfoLogBean.getInstance().getLivePlayAnalytics().logType = TheLConstants.LiveInfoLogConstants.TYPE_PK;

                    if (friendBean == null) {
                        return;
                    }

                    final float[] paramArr = LiveUtils.getPkUpLoadParams();

                    observer.sendPkRequestMsg(friendBean.id, paramArr[0], paramArr[1], paramArr[2], paramArr[3]);

                    showIPkRequestView(friendBean);

                    livePkFriendBean = friendBean;

                    if (liveLinkMicDialog != null) {
                        liveLinkMicDialog.dismiss();
                    }

                    if (mPkFriendListViewDialog != null) {
                        mPkFriendListViewDialog.dismiss();
                    }
                }
            });

            mPkFriendListViewDialog.show(getActivity().getSupportFragmentManager(), LivePKFriendListView.class.getName());

        } else {

            liveLinkMicDialog = new LiveLinkMicDialog();

            liveLinkMicDialog.setOnFriendRequestClickListener(new LivePkContract.FriendRequestClickListener() {
                @Override
                public void onClickRequest(LivePkFriendBean friendBean, int type1, int position) {

                    L.d("LiveShowMsgClientCtrl1", " friendBean toString : " + friendBean.toString());

                    L.d("LiveShowMsgClientCtrl1", " type1 : " + type1);

                    L.d("LiveShowMsgClientCtrl1", " friendBean.nickname : " + friendBean.nickname);

                    L.d("LiveShowMsgClientCtrl1", " friendBean.avatar : " + friendBean.avatar);

                    if (type1 == LiveConstant.TYPE_FRIEND_LINK_MIC) {

                        LiveInfoLogBean.getInstance().getLivePlayAnalytics().logType = TheLConstants.LiveInfoLogConstants.TYPE_LINK_MIC;

                        RectF rectF = LinkMicScreenUtils.getChildVideoRect();

                        observer.connectMic(LiveRoomMsgBean.PK_NOTICE_METHOD_REQUEST, friendBean.id, rectF.left, rectF.top, rectF.bottom, rectF.right, friendBean.nickName, friendBean.avatar, false);

                    } else if (type1 == LiveConstant.TYPE_GUARD_AUDIENCE_LINK_MIC) {
                        LiveInfoLogBean.getInstance().getLivePlayAnalytics().logType = TheLConstants.LiveInfoLogConstants.TYPE_LINK_MIC;

                        RectF rectF = LinkMicScreenUtils.getChildVideoRect();

                        observer.connectMic(LiveRoomMsgBean.PK_NOTICE_METHOD_REQUEST, friendBean.id, rectF.left, rectF.top, rectF.bottom, rectF.right, friendBean.nickName, friendBean.avatar, true);

                        observer.acceptAudienceLinkMic();

                    } else if (type1 == LiveConstant.TYPE_AUDIENCE_LINK_MIC) {

                        RectF rectF = LinkMicScreenUtils.getChildVideoRect();

                        AudienceLinkMicRequestBean audienceLinkMicRespoenceBean = new AudienceLinkMicRequestBean();

                        audienceLinkMicRespoenceBean.method = LiveRoomMsgBean.METHOD_LINK_MIC_GUEST_RESPONSE;

                        audienceLinkMicRespoenceBean.x = rectF.left;

                        audienceLinkMicRespoenceBean.y = rectF.top;

                        audienceLinkMicRespoenceBean.width = rectF.right;

                        audienceLinkMicRespoenceBean.height = rectF.bottom;

                        audienceLinkMicRespoenceBean.toUserId = friendBean.id;

                        audienceLinkMicRespoenceBean.result = LiveRoomMsgBean.LINK_MIC_RESULT_YES;

                        String body = GsonUtils.createJsonString(audienceLinkMicRespoenceBean);

                        observer.responseAudienceLinkMic(LiveRoomMsgBean.TYPE_CONNECT_MIC, body);

                    }

                    liveLinkMicDialog.dismiss();

                }

            });

            liveLinkMicDialog.setMicAudienceListener(new LiveLinkMicAudienceListFragment.LinkMicAudienceListener() {
                @Override
                public void getTopFansTodayList() {
                    observer.getTopFansTodayList();
                }
            });

            liveLinkMicDialog.setLinkMicStatus(linkMicStatus);

            liveLinkMicDialog.setLinkMicData(livePkFriendBeans);

            liveLinkMicDialog.setUserId(String.valueOf(liveRoomBean.user.id));

            liveLinkMicDialog.show(getActivity().getSupportFragmentManager(), LiveLinkMicDialog.class.getName());

        }


    }

    /**
     * 设置举报数据
     *
     * @param content
     */
    private void setReportContent(String content) {
        reportContent = content;
    }

    protected void showTextSizeDialog() {
        if (getActivity() == null) {
            return;
        }
        dialogUtils.closeDialog();
        dialogUtils.showTextSizeDialog(getActivity(), new LiveTextSizeChangedListener() {

            @Override
            public void textSizeChanged(int type) {
                setChatTextSize(type);
            }
        });
    }

    private void setChatTextSize(int selectSize) {
        if (null != adapter) {
            SharedPrefUtils.setInt(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.LIVE_SHOW_CAPTURE_TEXTSIZE, selectSize);
            adapter.textSizeChanged(selectSize);
        }
    }

    private void share() {
        if (getActivity() == null) {
            return;
        }
        if (liveRoomBean.share != null) {
            MobclickAgent.onEvent(getActivity(), "broadcaster_click_share_button");
            final String material = LiveRoomBean.TYPE_VOICE == liveRoomBean.audioType ? GrowingIoConstant.MATERIAL_LIVE_VOICE : GrowingIoConstant.MATERIAL_LIVE_VIDEO;
            final GIOShareTrackBean trackBean = new GIOShareTrackBean(GrowingIoConstant.LIVE_SHARE, "", GrowingIoConstant.ENTRY_LIVE_CAPTURE, material);
            dialogUtils.showShareDialog(getActivity(), getString(R.string.invite_fans), liveRoomBean.share.title, liveRoomBean.share.text, liveRoomBean.share.title, liveRoomBean.share.link, liveRoomBean.user.avatar, false, true, callbackManager, new FacebookCallback() {
                @Override
                public void onSuccess(Object o) {
                    MobclickAgent.onEvent(getActivity(), "share_succeeded");
                }

                @Override
                public void onCancel() {

                }

                @Override
                public void onError(FacebookException error) {

                }
            }, true, trackBean, new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    if (ShareFileUtils.getBoolean(ShareFileUtils.LiNK_MIC_TIPS, true)) {

                        ShareFileUtils.setBoolean(ShareFileUtils.LiNK_MIC_TIPS, false);

                        link_mic_guide_view.show(link_mic_ll, null);
                    }
                }
            });
        }
    }

    private void showLiveClosedDialog() {

        if (getActivity() == null) {
            return;
        }

        dialogUtils.closeDialog();
        if (destoryed || mCloseDialog != null && mCloseDialog.isShowing()) {
            return;
        }
        mCloseDialog = new LiveShowCaptureCloseDialog(getActivity()).initDialog(liveRoomBean, todayIncome, defeated, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                if (observer != null) {
                    observer.sendColseMsg();
                    observer.sendEmptyMessage(UI_EVENT_ANCHOR_FINISH);
                }
                if (getActivity() != null) {
                    getActivity().finish();
                }
            }
        });
        observer.exucuteColseRunable();
    }

    private void refreshUI() {
        ImageLoaderManager.imageLoaderDefaultCircle(avatar, R.mipmap.icon_user, liveRoomBean.user.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE);
        txt_nickname.setText(liveRoomBean.user.nickName);
        txt_audience.setText(getString(R.string.now_watching, liveRoomBean.liveUsersCount));
        txt_watch_live.setText(liveRoomBean.liveUsersCount + "");

        txt_soft_money.setText(getString(R.string.money_soft, softEnjoyBean.gold + ""));

        live_look_rank_view.initView(liveRoomBean.topFans, LiveLookRankView.TYPE_CAPTURE);

        txt_now_gem.setVisibility(View.GONE);
//        layout_multi_live_ad.setVisibility(View.GONE);
//        layout_live_ad.setVisibility(View.GONE);
        liveAdViewManager.dismiss();
        if (LiveRoomBean.TYPE_VOICE == liveRoomBean.audioType) {
            img_live_type.setImageResource(R.mipmap.icon_voice_live);
            if (LiveRoomBean.MULTI_LIVE == liveRoomBean.isMulti) {
                ll_multi_view.setVisibility(VISIBLE);
                multi_grid_view.setVisibility(VISIBLE);
                lin_multi_sound_view_bottom.setVisibility(VISIBLE);
//                layout_multi_live_ad.setVisibility(VISIBLE);
            } else {
//                layout_live_ad.setVisibility(VISIBLE);

                lin_sound_view_bottom.setVisibility(VISIBLE);
            }
        } else {
//            layout_live_ad.setVisibility(VISIBLE);

            ll_multi_view.setVisibility(View.GONE);
            control_ll.setVisibility(VISIBLE);
            img_live_type.setImageResource(R.mipmap.icon_camera_live);
        }

        if (liveRoomBean.announcement == null || liveRoomBean.announcement.length() == 0) {
            live_notice.setVisibility(View.GONE);
        } else {
            live_notice.setVisibility(VISIBLE);
        }

        keyboardChangeListener = new KeyboardChangeListener(this);
        setKeyboardChangeListener();
        if (danmu_selected_layout != null) {
            danmu_selected_layout.setGold(liveRoomBean.user.gold);
        }

    }

    /**
     * 上传观看直播时长到GrowingIO
     */
    private void uploadWatchTimeToGIO() {
        long endTime = System.currentTimeMillis();

        long liveTime = endTime - startTime;

        if (startTime > 0) {
            if (liveTime > 0) {
                liveTime = liveTime / 1000;
            } else {
                liveTime = 1;
            }
        } else {
            liveTime = 1;
        }

        GrowingIOUtil.uploadLiveTimeAndCount(liveTime);

    }

    @Override
    public void initLiveShow() {

    }

    @Override
    public void refreshLiveShow(LiveRoomBean liveRoomBean) {
        this.liveRoomBean = liveRoomBean;
        liveRoomBean.setSoftEnjoyBean();
        softEnjoyBean = liveRoomBean.softEnjoyBean;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return false;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (dialogUtils != null && dialogUtils.isDialogShowing()) {
                dialogUtils.closeDialog();
                return false;
            } else if (null != mCloseDialog && mCloseDialog.isShowing()) {
                mCloseDialog.dismiss();
                return false;
            } else if (dialog != null && dialog.getDialog() != null && dialog.getDialog().isShowing()) {
                return false;
            } else if (inPk) {
                finishPk();
            } else if (isLinkMic) {
                finishLinkMic();
            } else {
                showLiveClosedDialog();
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean showLiveView(boolean show) {
        return false;
    }

    @Override
    public void VideoPrepared() {

    }


    /**
     * 获取个人名片
     */
    private void getLiveUserCard(String userId, String type) {
        this.User_Type = type;
        L.d("LiveShowCaptureView", " !Utils.isMyself(userId) : " + !Utils.isMyself(userId));


        if (LiveRoomBean.TYPE_VOICE == liveRoomBean.audioType) {
            if (!Utils.isMyself(userId)) {
                presenter.getLiveUserCard(userId);
            }
        } else {
            if (!Utils.isMyself(userId)) {
                presenter.getLiveUserCard(userId);
            }
        }
    }

    @Override
    public void setPresenter(LiveShowContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showLiveUserCard(final LiveUserCardBean liveUserCardBean) {
        if (getActivity() == null) {
            return;
        }
        //屏蔽观众并且弹出提示框
        final View.OnClickListener blockHerListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogUtils.closeDialog();
                if (live_user_card_dialog_view != null && live_user_card_dialog_view.getDialog() != null && live_user_card_dialog_view.getDialog().isShowing()) {
                    live_user_card_dialog_view.destroyView();

                }
                ViewUtils.preventViewMultipleClick(v, 1000);
                DialogUtil.showConfirmDialog(getActivity(), null, getString(R.string.need_block_user), getString(R.string.userinfo_activity_unfollow_operation), getString(R.string.info_no), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        arg0.dismiss();

                        final Flowable<BlockNetBean> flowable = CommonApiBusiness.pullUserIntoBlackList(liveUserCardBean.userId + "");
                        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BlockNetBean>() {
                            @Override
                            public void onNext(BlockNetBean data) {
                                super.onNext(data);

                                if (!hasErrorCode && data != null && data.data != null) {

                                    L.d("BlackUtils", " onNext : " + data.data.toString());

                                    banHer(liveUserCardBean.userId + "");

                                }
                            }
                        });


                    }
                });


            }
        };
        final View.OnClickListener reportListener = new ReportListener(liveUserCardBean.userId + "");
        final Bundle bundle = new Bundle();
        bundle.putSerializable(LiveUserCardDialogView.BUNDLE_KEY_LIVE_USER_CARD_BEAN, liveUserCardBean);
        bundle.putBoolean(LiveUserCardDialogView.BUNDLE_KEY_CAN_SHARE, liveRoomBean.user.id == liveUserCardBean.userId);
        bundle.putString(LiveUserCardDialogView.BUNDLE_KEY_SHARE_TITLE, liveRoomBean.share.title);
        bundle.putString(LiveUserCardDialogView.BUNDLE_KEY_CONTENT, liveRoomBean.share.text);
        bundle.putString(LiveUserCardDialogView.BUNDLE_KEY_SINGLE_TITLE, liveRoomBean.share.title);
        bundle.putString(LiveUserCardDialogView.BUNDLE_KEY_IMAGE_URL, liveRoomBean.share.link);
        bundle.putInt(LiveUserCardDialogView.BUNDLE_KEY_LIVE_TYPE, liveRoomBean.audioType);

        bundle.putBoolean(LiveUserCardDialogView.BUNDLE_KEY_IS_BROAD_CASTER, true);
        live_user_card_dialog_view = LiveUserCardDialogView.getInstance(bundle);
        live_user_card_dialog_view.setReportListener(reportListener);
        live_user_card_dialog_view.setBlockHerListener(blockHerListener);
        live_user_card_dialog_view.setDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                setReportContent("");
            }
        });
        live_user_card_dialog_view.setLiveUserCardListener(new LiveUserCardView.LiveUserCardListener() {
            @Override
            public void LinkMic(LiveUserCardBean liveUserCardBean) {

                live_user_card_dialog_view.dismiss();

                LiveInfoLogBean.getInstance().getLivePlayAnalytics().logType = TheLConstants.LiveInfoLogConstants.TYPE_LINK_MIC;

                RectF rectF = LinkMicScreenUtils.getChildVideoRect();

                observer.connectMic(LiveRoomMsgBean.PK_NOTICE_METHOD_REQUEST, String.valueOf(liveUserCardBean.userId), rectF.left, rectF.top, rectF.bottom, rectF.right, liveUserCardBean.nickName, liveUserCardBean.avatar, true);
            }
        });
        live_user_card_dialog_view.show(getActivity().getSupportFragmentManager(), getClass().getSimpleName());

    }

    /**
     * 屏蔽某个人
     *
     * @param userId
     */
    private void banHer(String userId) {
        observer.banHer(userId);
    }

    /**
     * 显示软妹豆贡献列表
     */
    private void showRankingList() {
        if (getActivity() == null) {
            return;
        }
        final Bundle bundle = new Bundle();
        bundle.putString(TheLConstants.BUNDLE_KEY_USER_ID, liveRoomBean.user.id + "");
        live_gift_ranking_dialog_view = LiveGiftRankDialogView.getInstance(bundle);
        live_gift_ranking_dialog_view.show(getActivity().getSupportFragmentManager(), getClass().getSimpleName());

    }

    @Override
    public void onFollowStatusChanged(int followStatus, String userId, String nickName, String avatar) {
        super.onFollowStatusChanged(followStatus, userId, nickName, avatar);
        if (live_user_card_dialog_view != null && live_user_card_dialog_view.getDialog() != null && live_user_card_dialog_view.getDialog().isShowing()) {
            live_user_card_dialog_view.onFollowStatusChanged(followStatus, userId, nickName, avatar);

        }
    }


    @Override
    public void bindObserver(LiveWatchObserver observer) {
        this.observer = observer;
    }

    @Override
    public void networkSlow() {
        txt_my_status.setText(getString(R.string.network_slow));
        if (txt_my_status.getVisibility() == View.GONE) {
            txt_my_status.setVisibility(VISIBLE);
            txt_my_status.postDelayed(new Runnable() {
                @Override
                public void run() {
                    txt_my_status.setVisibility(View.GONE);
                }
            }, 5000);
        }
    }

    @Override
    public void initMultiAdapter(LiveMultiOnCreateBean liveMultiOnCreateBean) {
        multi_grid_view.refreshAllSeats(liveMultiOnCreateBean.seats);
    }

    @Override
    public void refreshSeat(LiveMultiSeatBean seatBean) {

        L.d(TAG, " refreshSeat seatBean : " + seatBean.toString());

        multi_grid_view.refreshSeat(seatBean);

        if (seatBean == null || TextUtils.isEmpty(seatBean.method)) {
            return;
        }

        if (TYPE_METHOD_ONSEAT.equals(seatBean.method) || TYPE_METHOD_OFFSEAT.equals(seatBean.method)) {
            if (guestCount() == 8) {
                if (liveSoundMicSortDialog != null && liveSoundMicSortDialog.isAdded()) {
                    liveSoundMicSortDialog.agreeEnable(true);
                }
            } else {
                if (liveSoundMicSortDialog != null && liveSoundMicSortDialog.isAdded()) {
                    liveSoundMicSortDialog.agreeEnable(false);
                }
            }
        }
    }

    @Override
    public void guestGift(LiveMultiSeatBean seatBean) {

        L.d(TAG, " ---------guestGift-------- ");

        GiftSendMsgBean giftSendMsgBean = new GiftSendMsgBean();
        giftSendMsgBean.type = GiftSendMsgBean.GIFT_TYPE_GUEST;
        giftSendMsgBean.isMultiConnectMic = true;
        giftSendMsgBean.userId = seatBean.userId;
        giftSendMsgBean.nickName = seatBean.nickname;
        giftSendMsgBean.avatar = seatBean.avatar;
        giftSendMsgBean.id = seatBean.id;
        giftSendMsgBean.combo = seatBean.combo;
        giftSendMsgBean.userLevel = seatBean.userLevel;
        giftSendMsgBean.ownerGem = seatBean.ownerGem;
        giftSendMsgBean.toAvatar = seatBean.toAvatar;
        giftSendMsgBean.toNickname = seatBean.toNickname;
        giftSendMsgBean.toUserId = seatBean.toUserId;

        GIOGiftBean gioGiftBean = new GIOGiftBean();

        gioGiftBean.giftID = giftSendMsgBean.id;

        gioGiftBean.giftName = giftSendMsgBean.giftName;

        gioGiftBean.giftPrice = giftSendMsgBean.gold;

        if (softEnjoyBean == null) {//如果没有礼物列表数据，因为有些打赏的显示数据无法获取，所以要重新请求礼物列表数据，并且把这个消息放到消息队列里去，等请求完再显示
            putGiftMsgIntoWaitList(giftSendMsgBean);//把礼物消息放到等待消息队里中
            getgiftListNetData();
            return;
        }

        if (getGiftMsgDataFromMemory(giftSendMsgBean)) {//能够从本地内存获取数据
            showGiftMsgData(giftSendMsgBean);//显示消息
        } else {
            putGiftMsgIntoWaitList(giftSendMsgBean);//不能，则连网请求数据
            getSingleGift(giftSendMsgBean.id);
        }

        //刷新爱心
        seatBean.nickname = seatBean.toNickname;
        seatBean.avatar = seatBean.toAvatar;
        seatBean.userId = seatBean.toUserId;
        multi_grid_view.refreshSeat(seatBean);

    }

    @Override
    public void startEncounter(final int leftTime) {
        if (mOnMeetStatusListener != null) {

            if (multi_grid_view != null) {

                boolean isGuest = multi_grid_view.isGuest();

                int meetStatus = isGuest ? TheLConstants.MeetStatusConstant.MEET_STATUS_SUCCESS : TheLConstants.MeetStatusConstant.MEET_STATUS_FAILED;

                mOnMeetStatusListener.onMeetStatus(meetStatus);

            }
        }
        isMeeting = true;
        ll_meetion.setVisibility(VISIBLE);
        playBgAnima();
        Observable.interval(0, 1, TimeUnit.SECONDS).take(leftTime)//计时次数
                .observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(new Observer<Long>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Long aLong) {
                String time = String.valueOf(leftTime - aLong);

                String content = TheLApp.context.getString(R.string.count_down_meetion, time);

                int index = content.indexOf(time);

                SpannableString spannableString = new SpannableString(content);

                spannableString.setSpan(new AbsoluteSizeSpan(40, true), index, content.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                meetion_count_down.setText(spannableString);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {
                //相遇按钮冷却
                Observable.interval(0, 1, TimeUnit.SECONDS).take(MEET_INTERVAL)//计时次数
                        .observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(new Observer<Long>() {
                    @Override
                    public void onSubscribe(Disposable disposable) {

                    }

                    @Override
                    public void onNext(Long aLong) {
                        meetionStatus = BeautyAndTextDialogView.MEET_INTERVAL;
                        if (beautyAndTextDialogView != null && beautyAndTextDialogView.isAdded()) {
                            beautyAndTextDialogView.setMeetionStatus(meetionStatus, DateUtils.getFormatTimeFromMillis((MEET_INTERVAL - aLong) * 1000, "mm:ss"));
                        }
                        isMeetingInterval = true;
                    }

                    @Override
                    public void onError(Throwable throwable) {

                    }

                    @Override
                    public void onComplete() {
                        meetionStatus = BeautyAndTextDialogView.MEET_DEFAULT;
                        if (beautyAndTextDialogView != null && beautyAndTextDialogView.isAdded()) {
                            beautyAndTextDialogView.setMeetionStatus(meetionStatus, "");
                        }
                        isMeetingInterval = false;
                    }
                });
            }
        });
    }

    @Override
    public void endEncounter(LiveMultiSeatBean seatBean) {
        if (mOnMeetStatusListener != null) {
            mOnMeetStatusListener.onMeetStatus(TheLConstants.MeetStatusConstant.MEET_STATUS_FAILED);
        }
        isMeeting = false;
        ll_meetion.setVisibility(View.GONE);
        meetion_anim_bg.cancelAnimation();
        meetion_anim_bg.setVisibility(View.GONE);

        //相遇结束，跳转到结果页
        Bundle bundle = new Bundle();
        bundle.putSerializable(TheLConstants.BUNDLE_KEY_LIVE_SEAT_ROOM, seatBean);
        bundle.putSerializable(TheLConstants.BUNDLE_KEY_LIVEROOMBEAN, liveRoomBean);
        bundle.putInt(TheLConstants.BUNDLE_KEY_ROLE, MeetResultActivity.BROADCASTER);
        Intent intent = new Intent(getActivity(), MeetResultActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);

    }

    @Override
    public void speakers(MultiSpeakersBean speakersBean) {
        for (int i = 0; i < speakersBean.speakers.size(); i++) {
            if ((speakersBean.speakers.get(i).uid == liveRoomBean.user.userId
                    || speakersBean.speakers.get(i).uid == liveRoomBean.user.id)
                    && speakersBean.speakers.get(i).volumn > 28) {
                if (user_is_talking.getVisibility() == View.INVISIBLE) {
                    user_is_talking.setVisibility(VISIBLE);
                    user_is_talking.playAnimation();
                }
            }
        }
        multi_grid_view.refreshSeatAnimation(speakersBean);
    }

    @Override
    public void addSortMic(LiveMultiSeatBean.CoupleDetail seatBean) {
        sortMicListCount++;
        if (liveSoundMicSortDialog != null && liveSoundMicSortDialog.isAdded()) {
            liveSoundMicSortDialog.addUser(seatBean);
        }
        setUnreadCount(sort_mic_count, sortMicListCount);
    }

    @Override
    public void removeSortMic(String userId) {
        sortMicListCount--;
        if (liveSoundMicSortDialog != null && liveSoundMicSortDialog.isAdded()) {
            liveSoundMicSortDialog.removeByUserId(userId);
        }

        setUnreadCount(sort_mic_count, sortMicListCount);

    }

    @Override
    public void getMicSortList(List<LiveMultiSeatBean.CoupleDetail> list) {
        if (liveSoundMicSortDialog != null && liveSoundMicSortDialog.isAdded()) {
            liveSoundMicSortDialog.setData(list);
        }
        sortMicListCount = list.size();
    }

    @Override
    public void getTopFansTodayList(@NotNull ArrayList<LivePkFriendBean> list) {
        if (liveLinkMicDialog != null && liveLinkMicDialog.isAdded()) {
            liveLinkMicDialog.addDialyList(list);
        }
    }

    @Override
    public void andienceNotIn() {
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ToastUtils.showToastShort(getActivity(), TheLApp.getContext().getString(R.string.request_message_fail));
                }
            });
        }
    }

    @Override
    public void rejectMic(RejectMicBean rejectMicBean) {

        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (rejectMicBean != null && rejectMicBean.leftTime != null) {

                        String content = TheLApp.context.getResources().getString(R.string.invite_after_xx, Utils.formatTime(Long.parseLong(rejectMicBean.leftTime)));

                        ToastUtils.showToastShort(TheLApp.context, content);
                    }

                }
            });
        }
    }

    @Override
    public void topToday(TopTodayBean topTodayBean) {
        LiveRoomMsgBean liveRoomMsgBean = new LiveRoomMsgBean();
        liveRoomMsgBean.nickName = topTodayBean.nickname;
        liveRoomMsgBean.rank = topTodayBean.rank;
        liveRoomMsgBean.type = LiveRoomMsgBean.TYPE_TOP_TODAY;
        msgList.add(liveRoomMsgBean);
    }

    @Override
    public void sortMicOn() {

    }

    @Override
    public void sortMicOff() {
        sortMicListCount = 0;
        if (sort_mic_count != null) {
            setUnreadCount(sort_mic_count, 0);
        }
        if (liveSoundMicSortDialog != null) {
            liveSoundMicSortDialog.clearUser();
        }
    }

    @Override
    public void freeBarrage() {
        if (root_vg != null) {
            if (!root_vg.isDanmuShowing() && !root_vg.isSoftExtend()) {
                showKeyboard();
            }

        }

        if (danmu_selected_layout != null && !danmu_selected_layout.isFreeBarrage()) {
            if (danmu_selected_layout != null) {
                danmu_selected_layout.showFreeBarrage();
                if (softEnjoyBean != null) {
                    danmu_selected_layout.setGold(softEnjoyBean.gold);
                }
            }
        }

        /*if (bubble_guide_layout != null) {
            bubble_guide_layout.show();
        }*/
    }

    @Override
    public void onFaceDetection(boolean visible) {
        if (visible) {
            if (mNoFaceDialog != null && mNoFaceDialog.getDialog() != null
                    && mNoFaceDialog.getDialog().isShowing()) {
                //dialog is showing so do something
                mNoFaceDialog.dismiss();
                mNoFaceDialog = null;
            }
        } else {
            if (mNoFaceDialog == null) {
                mNoFaceDialog = new UnidentifyFaceDialog();
                mNoFaceDialog.show(getChildFragmentManager(), UnidentifyFaceDialog.class.getName());
            }
        }
    }

    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        switch (viewId) {
            case R.id.ll_chat_multi_view:
            case R.id.ll_chat_view:
            case R.id.ll_chat_sound_view:
                if (UserUtils.isVerifyCell()) {
                    ViewUtils.preventViewMultipleClick(v, 1000);
                    showKeyboard();
                }
                break;
        }
    }

    @Override
    public void upgradeEffects(@NotNull LiveRoomMsgBean liveRoomMsgBean) {

        if (null != live_vip_enter_view) {
            live_vip_enter_view.joinNewUser(liveRoomMsgBean);

            liveRoomMsgBean.type = LiveRoomMsgBean.TYPE_MSG;

            liveRoomMsgBean.content = StringUtils.getString(R.string.congratulations_being_promoted, liveRoomMsgBean.upgradeNickName, liveRoomMsgBean.upgradeUserLevel);

            addLiveRoomMsg(liveRoomMsgBean);
        }
    }

    @Override
    public void allUpgradeEffects(@NotNull LiveRoomMsgBean liveRoomMsgBean) {
        if (mLiveLevelUpView != null) {
            mLiveLevelUpView.showView(liveRoomMsgBean);
        }
    }

    /**
     * 举报监听
     */
    class ReportListener implements View.OnClickListener {
        String userId;

        public ReportListener(String userId) {
            this.userId = userId;
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getActivity(), ReportActivity.class);
            intent.putExtra(TheLConstants.BUNDLE_KEY_REPORT_TYPE, User_Type);
            intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, this.userId);
            intent.putExtra(TheLConstants.BUNDLE_KEY_REPORT_CONTENT, reportContent);
            getActivity().startActivity(intent);
        }
    }

    /******************************************一下为礼物显示逻辑*************************************************/
    /**
     * 用于判断AR礼物消息服务端是否重复发送
     */
    private List<Long> mARGiftList = new ArrayList<>();

    /**
     * 解析礼物
     *
     * @param payload
     */
    @Override
    public void parseGiftMsg(String payload, String code) {

        L.d(TAG, " parseGiftMsg payload : " + payload);

        L.d(TAG, " parseGiftMsg code : " + code);

        final GiftSendMsgBean msgBean = GsonUtils.getObject(payload, GiftSendMsgBean.class);

        if (code != null && code.equals(LiveRoomMsgBean.CODE_SPECIAL_GIFT)) {
            showGiftMsgData(msgBean);
            return;
        }

        if (msgBean.ownerGem != null && !msgBean.ownerGem.equals("-1")) {//如果服务器有返回，就更新主播软妹币
            txt_soft_money.setText(getString(R.string.money_soft, msgBean.ownerGem));
        }

        //AR Gift
        if (msgBean.isArGift) {

            //向服务端发送消息回执
            observer.arGiftReceipt(payload);

            //判断AR礼物是否重复发送
            boolean repeat = false;

            L.d(TAG, " parseGiftMsg msgBean.arGiftReceiptId : " + msgBean.arGiftReceiptId);

            L.d(TAG, " parseGiftMsg mARGiftList : " + mARGiftList.toString());

            for (Long arGiftReceiptId : mARGiftList) {
                if (arGiftReceiptId == msgBean.arGiftReceiptId) {
                    repeat = true;
                    break;
                }
            }

            L.d(TAG, " parseGiftMsg repeat : " + repeat);

            if (!repeat) {

                mARGiftList.add(msgBean.arGiftReceiptId);

                String json = ShareFileUtils.getString(ShareFileUtils.GIFT_LIST, "{}");

                L.subLog(TAG, " parseGiftMsg json : " + json);

                SoftEnjoyNetBean softEnjoyNetBean = GsonUtils.getObject(json, SoftEnjoyNetBean.class);

                SoftGiftBean giftBean = new SoftGiftBean();

                giftBean.arGiftId = msgBean.arGiftId;

                L.d(TAG, " parseGiftMsg giftBean.arGiftId : " + giftBean.arGiftId);

                if (softEnjoyNetBean != null && softEnjoyNetBean.data != null && softEnjoyNetBean.data.arlist != null) {
                    for (SoftGiftBean bean : softEnjoyNetBean.data.arlist) {
                        if (bean.arGiftId == msgBean.arGiftId) {

                            L.d(TAG, " parseGiftMsg bean.arResource : " + bean.arResource);

                            giftBean.playTime = bean.playTime;
                            giftBean.arResource = bean.arResource;
                            break;
                        }
                    }
                }

                L.d(TAG, " parseGiftMsg softEnjoyNetBean.data.arlist : " + softEnjoyNetBean.data.arlist);

                L.d(TAG, " parseGiftMsg giftBean.playTime : " + giftBean.playTime);

                if (giftBean.playTime == 0) {
                    giftBean.playTime = 3000;
                }
                observer.switchStricker(giftBean);
            }
        }

        //如果没有礼物列表数据，因为有些打赏的显示数据无法获取，所以要重新请求礼物列表数据，并且把这个消息放到消息队列里去，等请求完再显示
        if (softEnjoyBean == null) {
            //把礼物消息放到等待消息队里中
            putGiftMsgIntoWaitList(msgBean);
            getgiftListNetData();
            return;
        }
        //能够从本地内存获取数据
        if (getGiftMsgDataFromMemory(msgBean)) {
            //显示消息
            showGiftMsgData(msgBean);
        } else {
            //不能，则连网请求数据
            putGiftMsgIntoWaitList(msgBean);
            getSingleGift(msgBean.id);
        }
    }

    private void getSingleGift(int id) {
        //如果正在获取数据，则不重复获取
        if (isGettingGiftData) {
            return;
        }
        isGettingGiftData = true;
        presenter.getSingleGiftDetail(id);
        observer.postDelayed(new Runnable() {
            @Override
            public void run() {
                //万一请求失败，之类的，8秒后自动设置为false;
                isGettingGiftData = false;
            }
        }, 8000);
    }

    @Override
    public void addOfflineGift(SoftGiftBean softGiftBean) {

        L.d("SoftGiftMsgItemView", " addOfflineGift softGiftBean " + softGiftBean);

        offlineGiftList.remove(softGiftBean);
        offlineGiftList.add(softGiftBean);
        pushGiftMsgWaitList();
    }

    @Override
    public void showLiveAnchorUserCard(LiveUserCardBean liveUserCardBean) {

    }

    /**
     * 从本地内存中获取GiftSendMsgBean（要显示的收到的打赏的礼物的消息）的其他数据
     *
     * @param msgBean 收到的打赏的礼物数据
     * @return true；获取到数据，false，没获取到数据
     */
    private boolean getGiftMsgDataFromMemory(GiftSendMsgBean msgBean) {
        if (softEnjoyBean == null || softEnjoyBean.list == null || softEnjoyBean.list.isEmpty()) {
            getgiftListNetData();
            return false;
        }
        for (int i = 0; i < softEnjoyBean.list.size(); i++) {
            SoftGiftBean bean = softEnjoyBean.list.get(i);//从本地内存中获取余下数据
            if (msgBean.id == bean.id) {
                msgBean.img = bean.img;
                msgBean.icon = bean.icon;
                msgBean.action = bean.action;
                msgBean.giftName = bean.title;
                msgBean.mark = bean.resource;
                msgBean.gold = bean.gold;
                msgBean.playTime = bean.playTime;
                msgBean.videoUrl = bean.videoUrl;
                return true;//说明获取到了，返回true
            }
        }
        return isOffLineGift(msgBean);
    }

    private boolean isOffLineGift(GiftSendMsgBean msgBean) {

        L.d("SoftGiftMsgItemView", " isOffLineGift offlineGiftList " + offlineGiftList);

        final int size = offlineGiftList.size();
        for (int i = 0; i < size; i++) {
            SoftGiftBean bean = offlineGiftList.get(i);//从本地内存中获取余下数据
            if (msgBean.id == bean.id) {
                msgBean.img = bean.img;
                msgBean.icon = bean.icon;
                msgBean.action = bean.action;
                msgBean.giftName = bean.title;
                msgBean.mark = bean.resource;
                msgBean.gold = bean.gold;
                msgBean.playTime = bean.playTime;
                msgBean.videoUrl = bean.videoUrl;
                return true;//说明获取到了，返回true
            }
        }
        return false;
    }

    /**
     * 由于某种原因而暂时无法显示的消息
     * 把礼物消息放到等待消息队列中去
     * 比如最新小礼物，在开启直播的时候获取到的小礼物消息中没有这个礼物
     *
     * @param msgBean 放到等到队里中的消息
     */
    private void putGiftMsgIntoWaitList(GiftSendMsgBean msgBean) {
        giftMsgWaitList.add(msgBean);
    }

    /**
     * 处理等待的消息队列
     */
    private void pushGiftMsgWaitList() {
        for (int i = 0; i < giftMsgWaitList.size(); i++) {
            GiftSendMsgBean msgBean = giftMsgWaitList.get(i);
            if (getGiftMsgDataFromMemory(msgBean)) {//如果从新的内存中获取到数据
                showGiftMsgData(msgBean);//显示处理数据
                giftMsgWaitList.remove(i);//从等待队列的数据中移除
            } else {//依旧不能从新的内存中获取到数据（可能数据依旧不是最新）
                getSingleGift(msgBean.id);//连网请求数据
            }
        }
    }

    /**
     * 处理礼物赠送广播消息
     *
     * @param msgBean
     */
    private void showGiftMsgData(GiftSendMsgBean msgBean) {


        if (msgBean.giftName != null) {

            GIOGiftBean gioGiftBean = new GIOGiftBean();

            gioGiftBean.giftID = msgBean.id;

            gioGiftBean.giftName = msgBean.giftName;

            gioGiftBean.giftPrice = msgBean.gold;

            if (LiveRoomBean.TYPE_VOICE == liveRoomBean.audioType && LiveRoomBean.MULTI_LIVE == liveRoomBean.isMulti) {

                GrowingIOUtil.pushIncome(LiveStatus.LIVE_MULTI_LINK_MIC, gioGiftBean);

            } else {
                GrowingIOUtil.pushIncome(observer.getLiveStatus(), gioGiftBean);

            }
        }

        if (!TextUtils.isEmpty(msgBean.mark) || (msgBean.videoUrl != null && msgBean.videoUrl.endsWith(".mp4"))) {//如果大礼物标识不为空，说明是大动画
            putGiftMsgIntoBitAnimList(msgBean);//把礼物放到大动画播放列表中
        } else {
            receiveGiftMsg(msgBean);//处理接受到的礼物消息，显示
            //    addSmallGiftMsg(msgBean);
        }
    }

    /**
     * 把礼物放到大动画播放列表
     *
     * @param msgBean 要播放大动画的礼物
     */
    private void putGiftMsgIntoBitAnimList(GiftSendMsgBean msgBean) {
        final String mark = msgBean.mark;

        L.d(TAG, " mark : " + mark);

        L.d(TAG, " msgBean.videoUrl : " + msgBean.videoUrl);

        //如果标志在已有大动画里面
        if ((!TextUtils.isEmpty(mark) && (mark.equals(TheLConstants.BIG_ANIM_BALLOON)
                || mark.equals(TheLConstants.BIG_ANIM_CROWN)
                || mark.equals(TheLConstants.BIG_ANIM_STAR_SHOWER)
                || mark.equals(TheLConstants.BIG_ANIM_COIN_DROP)
                || mark.equals(TheLConstants.BIG_ANIM_HUG_HUG)
                || TheLConstants.BIG_ANIM_BUBBLE.equals(mark)
                || TheLConstants.BIG_ANIM_SNOWMAN.equals(mark)
                || TheLConstants.BIG_ANIM_KISS.equals(mark)
                || TheLConstants.BIG_ANIM_FERRIS_WHEEL.equals(mark)
                || TheLConstants.BIG_ANIM_PUMPKIN.equals(mark)
                || TheLConstants.BIG_ANIM_RING.equals(mark)
                || TheLConstants.BIG_ANIM_CHRISTMAS.equals(mark)
                || TheLConstants.BIG_ANIM_BOMB.equals(mark)
                || TheLConstants.BIG_ANIM_RICEBALL.equals(mark)
                || TheLConstants.BIG_ANIM_FIREWORK.equals(mark)
                || TheLConstants.BIG_ANIM_FIRECRACKER.equals(mark)
                || mark.endsWith(".json")))
                || (msgBean.videoUrl != null && msgBean.videoUrl.endsWith(".mp4"))) {
            bigAnimGiftList.add(msgBean);
            tryToPlayBigAnim();//试着播放大动画
        } else { //当小礼物显示
            receiveGiftMsg(msgBean);//处理接受到的礼物消息，显示
            //   addSmallGiftMsg(msgBean);
        }
    }

    /**
     * 试着播放大动画
     */
    private void tryToPlayBigAnim() {
        if (!isShowBigAnim && bigAnimGiftList.size() > 0) {//如果正在播放大动画或者大动画队列为0
            showBigGiftAnim(bigAnimGiftList.get(0));
        }
    }

    /**
     * 显示大动画
     *
     * @param giftSendMsgBean
     */
    public void showBigGiftAnim(final GiftSendMsgBean giftSendMsgBean) {
        int playTime = giftSendMsgBean.playTime;
        isShowBigAnim = true;
        if (liveBigGiftAnimLayout.playLocalAnim(giftSendMsgBean.mark)) {

        } else if ((!TextUtils.isEmpty(giftSendMsgBean.mark) && giftSendMsgBean.mark.endsWith(".json")) || (giftSendMsgBean.videoUrl != null && giftSendMsgBean.videoUrl.endsWith(".mp4"))) {//ae动画
            //LiveBigGiftAnimLayout.

            L.d("LiveBigGiftAnimLayout", " showBigGiftAnim : ");

            /***4.5.0新增，如果是视频礼物，播放视频***/
            if (!TextUtils.isEmpty(giftSendMsgBean.videoUrl) && !GiftSendMsgBean.VIDEO_URL_NULL.equals(giftSendMsgBean.videoUrl)) {
                liveBigGiftAnimLayout.playVideoAnim(giftSendMsgBean.videoUrl);
            } else {
                liveBigGiftAnimLayout.playAEanimator(giftSendMsgBean.mark, playTime);
            }

        } else {
            receiveGiftMsg(giftSendMsgBean);//如果大礼物中不存在（版本没更新），当小礼物显示
        }
        bigAnimGiftList.remove(0);//播放后，去掉第一个

        showBigGiftMsg(giftSendMsgBean);
        /***如果是大礼物视频礼物，则走视频结束回调,在setlistener方法里面***/
        if ((!TextUtils.isEmpty(giftSendMsgBean.mark) && giftSendMsgBean.mark.endsWith(".json")) && !TextUtils.isEmpty(giftSendMsgBean.videoUrl) && !GiftSendMsgBean.VIDEO_URL_NULL.equals(giftSendMsgBean.videoUrl)) {

        } else {
            observer.postDelayed(new Runnable() {
                @Override
                public void run() {
                    playNextBigAnim();
                }
            }, LiveBigGiftAnimLayout.duration + 1000);//大动画播放6秒
        }
    }

    private void setUnreadCount(TextView textView, int count) {

        L.d("videoLinkAdd", " sort_mic_count " + sort_mic_count);

        L.d("videoLinkAdd", " count " + count);

        L.d("videoLinkAdd", " livePkFriendBeans size " + livePkFriendBeans.size());

        if (count < 0) {
            count = 0;
        }

        if (sort_mic_count != null) {

            textView.setVisibility(VISIBLE);
            if (count > 99) {
                textView.setText("99+");
            } else if (count == 0) {
                textView.setVisibility(View.GONE);
            } else {
                textView.setText(String.valueOf(count));
            }
        }
    }

    /**
     * 播放下一个大动画
     */
    private void playNextBigAnim() {

        L.d(TAG, " playNextBigAnim : ");

        isShowBigAnim = false;
        hideBigGiftMsg();
        tryToPlayBigAnim();
    }

    private void receiveGiftMsg(GiftSendMsgBean giftSendMsgBean) {
        giftMsgCtrl.receiveGiftMsg(giftSendMsgBean);//如果大礼物中不存在（版本没更新），当小礼物显示

    }

    /**
     * 显示大礼物的消息信息
     *
     * @param giftSendMsgBean
     */
    private void showBigGiftMsg(final GiftSendMsgBean giftSendMsgBean) {

        if (giftSendMsgBean.avatar == null || giftSendMsgBean.nickName == null) {
            return;
        }

        View.OnClickListener onClickListener = null;
        if (!Utils.isMyself(giftSendMsgBean.userId)) {
            onClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getLiveUserCard(giftSendMsgBean.userId, ReportActivity.REPORT_TYPE_AUDIENCE);
                    reportContent = "";
                }
            };
        }
        live_vip_enter_view.receiveBigGiftMsg(giftSendMsgBean, onClickListener);
    }

    private void hideBigGiftMsg() {
        live_vip_enter_view.hideBigGiftText();
    }

    /**
     * 获取赠送礼物连网数据
     */
    private void getgiftListNetData() {
        if (isGettingGiftData) {//如果正在获取数据，则不重复获取
            return;
        }
        isGettingGiftData = true;
        presenter.getLiveGiftList();
        observer.postDelayed(new Runnable() {
            @Override
            public void run() {
                isGettingGiftData = false;//万一请求失败，之类的，8秒后自动设置为false;
            }
        }, 8000);
    }

    @Override
    public void showLiveGiftList(SoftEnjoyBean softEnjoyBeans) {
        this.softEnjoyBean = softEnjoyBeans;
        pushGiftMsgWaitList();
    }

    @Override
    public void showAdView(final LiveAdInfoBean liveAdInfoBean) {
        liveAdViewManager.show(liveRoomBean, liveAdInfoBean);
//        if (LiveRoomBean.MULTI_LIVE == liveRoomBean.isMulti) {
//            layout_multi_live_ad.setVisibility(VISIBLE);
//            layout_live_ad.setVisibility(View.GONE);
//
//        } else {
//            layout_live_ad.setVisibility(VISIBLE);
//            layout_multi_live_ad.setVisibility(View.GONE);
//
//            if (liveAdInfoBean.offset != null && liveAdInfoBean.offset.length() > 0) {
//                offset_tv.setVisibility(VISIBLE);
//                offset_tv.setText(liveAdInfoBean.offset);
//            }
//        }
//
//        if (liveAdInfoBean.icon != null) {
//            if (layout_live_ad.getVisibility() == VISIBLE) {
//
//                Uri uri = Uri.parse(liveAdInfoBean.icon);
//                DraweeController controller = Fresco.newDraweeControllerBuilder().setUri(uri).setAutoPlayAnimations(true).build();
//                ad_view.setController(controller);
//
//            } else {
//
//                Uri uri2 = Uri.parse(liveAdInfoBean.icon);
//                DraweeController controller2 = Fresco.newDraweeControllerBuilder().setUri(uri2).setAutoPlayAnimations(true).build();
//                ad_multi_view.setController(controller2);
//            }
//
//        }
//        if (liveAdInfoBean.rank != null && liveAdInfoBean.rank.length() > 0) {
//            top_tv.setText("NO." + liveAdInfoBean.rank);
//            top_multi_tv.setText("NO." + liveAdInfoBean.rank);
//
//        }
//
//        layout_live_ad.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (liveAdInfoBean.url != null) {
//                    Intent intent = new Intent(getContext(), WebViewActivity.class);
//                    Bundle bundle = new Bundle();
//                    bundle.putString(BundleConstants.URL, liveAdInfoBean.url);
//                    intent.putExtras(bundle);
//                    startActivity(intent);
//                }
//
//            }
//        });
//        layout_multi_live_ad.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (liveAdInfoBean.url != null) {
//                    Intent intent = new Intent(getContext(), WebViewActivity.class);
//                    Bundle bundle = new Bundle();
//                    bundle.putString(BundleConstants.URL, liveAdInfoBean.url);
//                    intent.putExtras(bundle);
//                    startActivity(intent);
//                }
//
//            }
//        });

    }


    /*******************************以上为礼物显示逻辑，一下为消息方法区******************************************************/
    /**
     * 显示弹幕
     *
     * @param danMsg
     */
    @Override
    public void showDanmu(String danMsg) {
        final DanmuBean danmuBean = GsonUtils.getObject(danMsg, DanmuBean.class);
        danmu_layout_view.receiveMsg(danmuBean);
    }

    @Override
    public void updateAudienceCount(@NotNull ResponseGemBean responseGemBean) {

        if (liveRoomBean != null) {

            liveRoomBean.gem = responseGemBean.getGem();

            liveRoomBean.liveUsersCount = responseGemBean.getUserCount();

            liveRoomBean.nowGem = responseGemBean.getNowGem();

            liveRoomBean.rank = responseGemBean.getRank();
        }

        txt_watch_live.setText(responseGemBean.getUserCount() + "");

        if (liveRoomBean.rank == 0) {
            txt_audience.setText("");
        } else if (liveRoomBean.rank == 1) //第一名
        {
            txt_audience.setText(getString(R.string.rank1, responseGemBean.getRank()) + " | ");
        } else if (liveRoomBean.rank == 2)//第二名
        {
            txt_audience.setText(getString(R.string.rank2, responseGemBean.getRank()) + " | ");
        } else if (liveRoomBean.rank == 3)//第三名
        {
            txt_audience.setText(getString(R.string.rank3, responseGemBean.getRank()) + " | ");
        } else                            //其他
        {
            txt_audience.setText(getString(R.string.rank, responseGemBean.getRank()) + " | ");
        }
        txt_soft_money.setText(getString(R.string.money_soft, responseGemBean.getGem()));
        //本场软妹豆
        txt_now_gem.setText(getString(R.string.present_royalties, responseGemBean.getNowGem()));
        txt_now_gem.setVisibility(VISIBLE);
    }

    @Override
    public synchronized void refreshMsg() {
        if (isRefreshingMsgList) {
            return;
        }

        int newMsgSize = msgCacheList.size();

        if (newMsgSize == 0) {
            return;
        }

        int startPosition = msgList.size();

        msgList.addAll(msgCacheList);

        isRefreshingMsgList = true;

        adapter.notifyItemRangeInserted(startPosition, startPosition + newMsgSize);
        if (observer != null) {
            if (isBottom) {
                observer.sendEmptyMessage(UI_EVENT_ANCHOR_SCROLL_TO_BOTTOM);
            } else {// 两秒后将isBottom设为true，避免出现有时候isBottom一直为false
                observer.sendEmptyMessageDelayed(UI_EVENT_ANCHOR_RESET_IS_BOTTOM, 2000);
            }
        }
        isRefreshingMsgList = false;

        msgCacheList.clear();

        smoothScrollToBottom();
    }

    @Override
    public void smoothScrollToBottom() {
        listview_chat.smoothScrollToPosition(adapter.getItemCount() - 1);
    }

    @Override
    public void resetIsBottom() {
        isBottom = true;
    }

    @Override
    public void updateMyNetStatusBad() {
        txt_my_status.setText(getString(R.string.network_slow));
        if (txt_my_status.getVisibility() == View.GONE) {
            txt_my_status.setVisibility(VISIBLE);
            observer.sendEmptyMessageDelayed(UI_EVENT_ANCHOR_UPDATE_MY_NET_STATUS_GOOD, 5000);
        }
    }


    @Override
    public void networkGood() {
        txt_my_status.setVisibility(View.GONE);
    }

    @Override
    public void connectBreak() {
        txt_my_status.setText(getString(R.string.livestream_disconnected));
        if (txt_my_status.getVisibility() == View.GONE) {
            txt_my_status.setVisibility(VISIBLE);
        }
    }

    @Override
    public void connectFailed() {
        txt_my_status.setText(getString(R.string.livestream_failed));
        if (txt_my_status.getVisibility() == View.GONE) {
            txt_my_status.setVisibility(VISIBLE);
        }
    }

    @Override
    public void connectiong() {
        txt_my_status.setText(getString(R.string.livestream_connecting));
        if (txt_my_status.getVisibility() == View.GONE) {
            txt_my_status.setVisibility(VISIBLE);
        }
    }

    @Override
    public void connectSuccess() {
        txt_my_status.setVisibility(View.GONE);

    }

    @Override
    public void joinUser(LiveRoomMsgBean liveRoomMsgBean) {
        if (null != live_show_user_view) {
            live_show_user_view.joinNewUser(liveRoomMsgBean);
        }
    }

    @Override
    public void joinVipUser(LiveRoomMsgBean liveRoomMsgBean) {

        LiveRoomMsgBean lrmb = GsonUtils.getObject("{\"userId\":\"3568\",\"avatar\":\"http://static.rela.me/app/avatar/3568/e866b803fb4d447c7305af5b7decca32.jpg-wh150\",\"nickName\":\"Rela\",\"level\":4,\"userLevel\":20,\"content\":\"OMG！恭喜在test074本直播间荣升至11等级，鼓掌散花！（多语言）\",\"upgradeUserId\":\"106759839\",\"upgradeAvatar\":\"http://static.rela.me/app/avatar/106759839/1ca17e3c5a4af6ec914d2164914aec2d.jpg\",\"upgradeNickName\":\"test074\",\"upgradeLevel\":0,\"upgradeUserLevel\":11}", LiveRoomMsgBean.class);

        if (null != live_vip_enter_view) {
            live_vip_enter_view.joinNewUser(liveRoomMsgBean);
        }
    }

    @Override
    public void refreshCloseDialog(String json) {

        L.d(TAG, " refreshCloseDialog  json : " + json);

        try {
            JSONObject obj = new JSONObject(json);
            final int fans = JsonUtils.getInt(obj, "fans", liveRoomBean.fans);

            L.d(TAG, " refreshCloseDialog  fans : " + fans);

            final String livetime = JsonUtils.getString(obj, "livetime", liveRoomBean.livetime);
            final int bestrank = JsonUtils.getInt(obj, "bestrank", liveRoomBean.bestrank);
            todayIncome = JsonUtils.getDouble(obj, "todayIncome", todayIncome);
            defeated = JsonUtils.getDouble(obj, "defeated", defeated);

            L.d(TAG, " refreshCloseDialog  todayIncome : " + todayIncome);

            L.d(TAG, " refreshCloseDialog  mCloseDialog : " + mCloseDialog);

            L.d(TAG, " refreshCloseDialog  mCloseDialog isShowing : " + mCloseDialog.isShowing());

            if (mCloseDialog != null && mCloseDialog.isShowing()) {
                mCloseDialog.refreshData(fans, livetime, bestrank, todayIncome, defeated);
            }
        } catch (JSONException e) {
            e.printStackTrace();

            L.d(TAG, " refreshCloseDialog  e : " + e.getMessage());

        }
    }

    @Override
    public void progressbarGone() {
//        progress_bar.setVisibility(View.GONE);
    }

    @Override
    public void liveDisconnected() {
        txt_my_status.setText(getString(R.string.livestream_failed));
        if (txt_my_status.getVisibility() == View.GONE) {
            txt_my_status.setVisibility(VISIBLE);
        }
    }

    @Override
    public void liveNetworkWaringNotify(String content) {
        if (TextUtils.isEmpty(content) || getActivity() == null) {
            return;
        }
        DialogUtil.showAlert(getActivity(), "", content, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
    }

    /****************************以上为消息方法区******************************************************************/

    List<LiveRoomMsgBean> msgCacheList = new ArrayList<>();

    @Override
    public void addLiveRoomMsg(final LiveRoomMsgBean liveRoomMsgBean) {
        L.d(TAG, " liveRoomMsgBean : " + liveRoomMsgBean);
        msgCacheList.add(liveRoomMsgBean);

        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (LiveRoomMsgBean.TYPE_GIFTCOMBO.equals(liveRoomMsgBean.type) && LiveRoomBean.TYPE_VOICE == liveRoomBean.audioType && LiveRoomBean.MULTI_LIVE == liveRoomBean.isMulti) {
                        if (liveRoomMsgBean.toUserId != null && !liveRoomMsgBean.toUserId.equals(String.valueOf(liveRoomBean.user.id))) {
                            if (multi_grid_view != null) {
                                SoftGiftBean softGiftBean = getGiftInfoByGiftId(liveRoomMsgBean.id);
                                if (getActivity() != null && !getActivity().isDestroyed() && softGiftBean != null) {
                                    multi_grid_view.setGift(softGiftBean.icon, liveRoomMsgBean.combo, liveRoomMsgBean.toUserId);
                                }
                            }
                        }
                    }
                }
            });
        }
    }

    private SoftGiftBean getGiftInfoByGiftId(int giftId) {

        if (softEnjoyBean == null || softEnjoyBean.list == null) {
            return null;
        }

        for (SoftGiftBean softGiftBean : softEnjoyBean.list) {

            if (softGiftBean.id == giftId) {
                return softGiftBean;
            }

        }

        return null;

    }

    @Override
    public void onDestroy() {
        stop();
        destoryed = true;
        isCreate = false;
        showPkFinishView();
        LivePkMediaPlayer.getInstance().release();
        if (liveBigGiftAnimLayout != null) {
            liveBigGiftAnimLayout.onDestory();
        }
        uploadWatchTimeToGIO();
        LiveGIOPush.getInstance().onDestroy();
        postLiveTimeEvent();
        if (observer != null) {
            observer.onDestroy();
        }
        observer = null;
        super.onDestroy();

    }

    /**
     * 主播端直播时长，增加直播类型／主播昵称/主播ID 维度
     **/
    private void postLiveTimeEvent() {
        long endTime = System.currentTimeMillis();

        long liveTime = endTime - startTime;

        if (startTime > 0) {
            if (liveTime > 0) {
                liveTime = liveTime / 1000;
            } else {
                liveTime = 1;
            }
        } else {
            liveTime = 1;
        }

        if (LiveRoomBean.TYPE_VOICE == liveRoomBean.audioType) {
            if (LiveRoomBean.MULTI_LIVE == liveRoomBean.isMulti) {
                GrowingIOUtil.postLiveTimeEvent(liveTime, 2, liveRoomBean.user.nickName, liveRoomBean.user.id + "");
            } else {
                GrowingIOUtil.postLiveTimeEvent(liveTime, 1, liveRoomBean.user.nickName, liveRoomBean.user.id + "");

            }
        } else {
            GrowingIOUtil.postLiveTimeEvent(liveTime, 0, liveRoomBean.user.nickName, liveRoomBean.user.id + "");

        }
    }

    private void stop() {
        mHandler.removeCallbacks(runnable);
    }

    @Override
    public void onStop() {
        super.onStop();
        stop();
    }

    @Override
    public void switchBeauty(boolean enable) {
        /*if (enable) {
            if (beautyAndTextPopupWindow != null) {
                beautyAndTextPopupWindow.setBeautyStyle(true);

            }
        } else {
            if (beautyAndTextPopupWindow != null) {
                beautyAndTextPopupWindow.setBeautyStyle(true);

            }

        }*/
    }

    @Override
    public void showCloseAlert() {
        if (getActivity() == null) {
            return;
        }
        try {
            DialogUtil.showAlert(getActivity(), "", getString(R.string.live_error), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    observer.sendColseMsg();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**********************************************一下为直播PK的方法***********************************************************/
    /**
     * 显示我申请Pk的view
     *
     * @param livePkFriendBean
     */
    @Override
    public void showIPkRequestView(final LivePkFriendBean livePkFriendBean) {
        if (dialog != null && dialog.getDialog() != null && dialog.getDialog().isShowing()) {//有请求正在
            //todo
        }
        dialog = new ConnectMicDialog();
        final Bundle bundle = new Bundle();
        bundle.putInt(TheLConstants.BUNDLE_KEY_ACTION_TYPE, ConnectMicDialog.DIALOG_TYPE_PK_I);
        bundle.putString(TheLConstants.BUNDLE_KEY_NICKNAME, livePkFriendBean.nickName);
        bundle.putString(TheLConstants.BUNDLE_KEY_USER_AVATAR, livePkFriendBean.avatar);
        dialog.setArguments(bundle);
        dialog.show(getFragmentManager(), ConnectMicDialog.class.getName());
        dialog.setOnButtonClickListener(new ConnectMicDialog.OnButtonClickListener() {
            @Override
            public void onClick(int type) {
                switch (type) {
                    case ConnectMicDialog.TYPE_CANCEL://取消请求
                        dialog.dismiss();
                        observer.sendPkRequestCancelMsg(livePkFriendBean.id);
                        break;
                    case TYPE_REQUEST_NO_RESPONSE://对方长时间没回应
                        dialog.dismiss();
                        break;
                    case ConnectMicDialog.TYPE_REQUEST_REFUSE://请求被拒绝后，点击 好的
                        dialog.dismiss();
                        break;
                }
            }
        });
        dialog.setCountDownOutListener(new CountDownOutListener() {
            @Override
            public void countDown(int type) {
                showPkBeginAnim();//倒计时结束，显示PK开始动画
            }
        });
    }

    /**
     * 显示PK开始动画
     */
    private void showPkBeginAnim() {
        //playPkAeAnim(LiveConstant.LIVE_PK_AE_ANIM_BEGIN);
        showPkSound(LiveConstant.LIVE_PK_AE_ANIM_STAR);
    }

    /**
     * 显示直播PK的aeView
     *
     * @param id
     */
    private void playPkAeAnim(final int id) {
        final LivePkAeAnimBean bean = LiveConstant.livePkAeAnimArr.get(id);
        if (bean == null) {
            return;
        }
        LiveUtils.setPkAeViewLayoutParam(live_pk_ae_view, bean, LiveUtils.getPkUpLoadParams());
        LiveUtils.playPkAeAnim(live_pk_ae_view, bean, new LivePkContract.LivePkAeAnimListener() {
            @Override
            public void onAnimStatus(View view, int status) {
                switch (status) {
                    case ANIM_START:
                        playPkSound(bean);
                        if (id == LiveConstant.LIVE_PK_AE_ANIM_MIDDLE) {//pk中场播放ae动画，pk Icon要消失，播放结束再出现
                            img_live_pk.setVisibility(View.GONE);
                        } else if (id == LiveConstant.LIVE_PK_AE_ANIM_DRAW || id == LiveConstant.LIVE_PK_AE_ANIM_VICTORY || id == LiveConstant.LIVE_PK_AE_ANIM_FAILED) {//结束动画都在开始时候让icon消失
                            img_live_pk.setVisibility(View.GONE);
                        }
                        break;
                    case ANIM_END:
                        if (id == LiveConstant.LIVE_PK_AE_ANIM_BEGIN) {//如果是Pk begin动画结束（Pk图标），则pk图标要显示
                            if (inPk) {
                                LiveUtils.setLivePkIconViewParam(img_live_pk, LiveUtils.getPkUpLoadParams());
                            }
                        } else if (id == LiveConstant.LIVE_PK_AE_ANIM_MIDDLE) {//pk中场播放ae动画，pk Icon要消失，播放结束再出现
                            if (inPk && !inPkSummary) {
                                img_live_pk.setVisibility(VISIBLE);
                            }
                        }
                        break;
                }
            }
        });
    }

    /**
     * 播放最后一分钟震动动画
     */
    private void playLastMinuteAnim() {
        LiveUtils.playPkLastMinuteAnimView(img_live_pk, live_pk_ae_view, LiveConstant.livePkAeAnimArr.get(LiveConstant.LIVE_PK_AE_ANIM_FIERCE), null);
    }

    /**
     * 播放直播Pk的声音
     *
     * @param bean
     */
    private void playPkSound(LivePkAeAnimBean bean) {
        if (bean == null) {
            return;
        }
        LiveUtils.playLivePkSound(bean, new LivePkContract.LivePkPlaySoundListener() {
            @Override
            public void onSoundPlay(MediaPlayer player, int status) {
                switch (status) {
                    case SOUND_END:
                        if (player != null) {
                            player.release();
                        }
                        break;
                }
            }
        });
    }

    /**
     * 播放直播Pk的声音
     *
     * @param id
     */
    private void showPkSound(final int id) {
        final LivePkAeAnimBean bean = LiveConstant.livePkAeAnimArr.get(id);
        String path = "";
        if (bean != null) {
            path = LiveConstant.LIVE_PK_SOUND_PATH + bean.sound_path;
        }

        final LivePkContract.LivePkPlaySoundListener listener = new LivePkContract.LivePkPlaySoundListener() {
            @Override
            public void onSoundPlay(MediaPlayer player, int status) {
                switch (status) {
                    case SOUND_END:
                        if (id == LiveConstant.LIVE_PK_SOUND_LAST_TEN_SECOND && player != null && pk_fierce_sound_play_time < 1) {//如果是最后10秒的声音播放结束(为了给服务器容错,但顶多播放两次就够了)
                            pk_fierce_sound_play_time += 1;
                            player.seekTo(0);
                            player.start();
                        } else if (id == LiveConstant.LIVE_PK_AE_ANIM_STAR) {//如果是开始的音乐播放(readyGo)，则紧接着播放Pk begin动画
                            playPkAeAnim(LiveConstant.LIVE_PK_AE_ANIM_BEGIN);//播放Pk begin动画 pk图标
                        }
                        break;
                }
            }
        };
        switch (id) {
            case LiveConstant.LIVE_PK_SOUND_LAST_TEN_SECOND:
                path = LiveConstant.LIVE_PK_SOUND_PATH + LiveConstant.LIVE_PK_SOUND_LAST_TEN_SECOND_PATH;
                break;
        }
        LiveUtils.playSound(path, listener);

    }

    /**
     * 显示直播PK申请view
     *
     * @param noticeBean
     */
    @Override
    public void showPkRequestView(final LivePkRequestNoticeBean noticeBean) {
        if (dialog != null && dialog.getDialog() != null && dialog.getDialog().isShowing()) {//说明当时正在显示别的请求
            return;
        }
        if (noticeBean == null) {
            return;
        }

        if (mPkFriendListViewDialog != null && mPkFriendListViewDialog.isAdded()) {
            mPkFriendListViewDialog.dismiss();
        }

        if (liveLinkMicDialog != null && liveLinkMicDialog.isAdded()) {
            liveLinkMicDialog.dismiss();
        }

        livePkFriendBean = LiveUtils.getLivePkFriendBean(noticeBean);
        final float[] paramArr = LiveUtils.getPkUpLoadParams();
        dialog = new ConnectMicDialog();
        final Bundle bundle = new Bundle();
        bundle.putInt(TheLConstants.BUNDLE_KEY_ACTION_TYPE, ConnectMicDialog.DIALOG_TYPE_PK_REQUEST);
        bundle.putString(TheLConstants.BUNDLE_KEY_NICKNAME, noticeBean.nickName);
        bundle.putString(TheLConstants.BUNDLE_KEY_USER_AVATAR, noticeBean.avatar);
        dialog.setArguments(bundle);
        dialog.show(getFragmentManager(), ConnectMicDialog.class.getName());
        dialog.setOnButtonClickListener(new ConnectMicDialog.OnButtonClickListener() {
            @Override
            public void onClick(int type) {
                switch (type) {
                    case ConnectMicDialog.TYPE_ACCEPT://点击接受请求
                        observer.sendPkResponseMsg(noticeBean.userId, LivePkResponseNoticeBean.LIVE_PK_RESPONSE_RESULT_YES, paramArr[0], paramArr[1], paramArr[2], paramArr[3]);
                        //结束请求的时候要界面变化，包括流变化和基本界面变化
                        observer.showPkStream();
                        showPkView();
                        dialog.showRequestAcceptView();
                        break;
                    case TYPE_REFUSE://点击拒绝
                        observer.sendPkResponseMsg(noticeBean.userId, LivePkResponseNoticeBean.LIVE_PK_RESPONSE_RESULT_NO, paramArr[0], paramArr[1], paramArr[2], paramArr[3]);
                        dialog.dismiss();
                        break;
                    case TYPE_I_NO_RESPONSE://自己长时间没回应后点击好的
                        dialog.dismiss();
                        break;
                    case TYPE_CANCEL:
                        dialog.dismiss();
                        break;
                }
            }
        });
        dialog.setCountDownOutListener(new CountDownOutListener() {
            @Override
            public void countDown(int type) {
                showPkBeginAnim();
            }
        });
    }

    /**
     * 显示直播PK回应view
     *
     * @param noticeBean
     */
    @Override
    public void showPkResponseView(LivePkResponseNoticeBean noticeBean) {
        if (dialog == null || dialog.getDialog() == null || !dialog.getDialog().isShowing()) {
            return;
        }
        if (LivePkResponseNoticeBean.LIVE_PK_RESPONSE_RESULT_YES.equals(noticeBean.result)) {//对方接受了PK请求
            dialog.showRequestAcceptView();
            showPkRequestAcceptView(noticeBean);
        } else {//对方拒绝了Pk申请
            dialog.showRequestRefuseView();
        }

    }

    /**
     * 显示PK被接受view  当PK被接受的一瞬间，画面就开始变化，而不是从接受到PKstart消息开始
     *
     * @param noticeBean
     */
    private void showPkRequestAcceptView(LivePkResponseNoticeBean noticeBean) {
        showPkView();
    }

    /**
     * 显示Pk的view(不包括倒计时，因为Pk还没开始)
     */
    private void showPkView() {
        livePkBloodView.initView().show();
        txt_pk_other_name.setVisibility(VISIBLE);
        if (livePkFriendBean != null) {
            txt_pk_other_name.setText(livePkFriendBean.nickName + "");
        }
        final RelativeLayout.LayoutParams chat_param = (RelativeLayout.LayoutParams) listview_chat.getLayoutParams();
        //聊天的右侧边距为pk margin

        chat_param.rightMargin = (int) TheLApp.getContext().getResources().getDimension(R.dimen.live_capture_chat_pk_right_margin);

        listview_chat.setLayoutParams(chat_param);
        //pk开始广告要消失,结束再出来
//        layout_live_ad.setVisibility(View.GONE);
        liveAdViewManager.dismiss();
        view_pk_other.setVisibility(VISIBLE);
    }

    /**
     * 显示直播PK开始view
     * 当收到PK开始消息时，才开始显示倒计时
     *
     * @param pkStartNoticeBean
     */
    @Override
    public void showPkStartView(LivePkStartNoticeBean pkStartNoticeBean) {
        inPk = true;
        livePkCountDownView.initView(pkStartNoticeBean.leftTime, LivePkCountDownView.TYPE_IN_PK, pkStartNoticeBean.leftTime).show();
        txt_finish_pk.setVisibility(VISIBLE);
        LiveUtils.setTxtFinishPkParam(txt_finish_pk);

    }

    /**
     * 显示直播pk取消view
     *
     * @param pkCancelPayload
     */
    @Override
    public void showPkCancelView(String pkCancelPayload) {
        if (dialog == null || dialog.getDialog() == null || !dialog.getDialog().isShowing()) {
            return;
        }
        dialog.showRequestCancelView();
    }

    /**
     * 显示直播挂断view
     * 对方提前挂断了PK，则自己胜利
     *
     * @param pkHangupBean
     */
    @Override
    public void showPkHangupView(LivePkHangupBean pkHangupBean) {
        if (pkHangupBean == null || getActivity() == null) {
            return;
        }

        if (LivePkHangupBean.HANGUP_STATUS_IN_PK == pkHangupBean.status) {//PK过程中结束
            livePkFailedView.showPkHangupFailedView().show();
        } else {//总结期间结束
            livePkFailedView.showPkSummaryHangupView().show();
        }
        if (dialog != null) {
            dialog.dismiss();
        }
        if (livePkFriendBean == null) {
            return;
        }

        dialog = new ConnectMicDialog();
        dialog.setOnButtonClickListener(new ConnectMicDialog.OnButtonClickListener() {
            @Override
            public void onClick(int type) {

                switch (type) {
                    case TYPE_HANGUP:
                        if (dialog != null) {
                            dialog.dismiss();
                        }
                        break;
                }
            }
        });

        Bundle bundle = new Bundle();
        bundle.putInt(TheLConstants.BUNDLE_KEY_ACTION_TYPE, ConnectMicDialog.DIALOG_TYPE_HANGUP);
        bundle.putString(BUNDLE_KEY_NICKNAME, livePkFriendBean.nickName);
        bundle.putString(BUNDLE_KEY_USER_AVATAR, livePkFriendBean.avatar);
        dialog.setArguments(bundle);
        dialog.show(getActivity().getSupportFragmentManager(), ConnectMicDialog.class.getName());

    }

    /**
     * 显示直播总结view
     *
     * @param pkSummaryNoticeBean
     */
    @Override
    public void showPkSummaryView(LivePkSummaryNoticeBean pkSummaryNoticeBean) {
        if (pkSummaryNoticeBean == null) {
            return;
        }
        if (!inPk) {
            return;
        }
        inPkSummary = true;
        if (pkSummaryNoticeBean.assisters.size() > 0) {
            livePkAssistsRandView.initView(Utils.getMyUserId(), pkSummaryNoticeBean.assisters).show();
        }
        livePkCountDownView.initView(pkSummaryNoticeBean.leftTime, LivePkCountDownView.TYPE_IN_SUMMARY).show();
        final String loserId = pkSummaryNoticeBean.loser;
        if (Utils.isMyself(loserId)) {//如果我是失败者
            playPkAeAnim(LiveConstant.LIVE_PK_AE_ANIM_FAILED);
        } else if (LivePkSummaryNoticeBean.NO_LOSER.equals(loserId)) {//平局
            playPkAeAnim(LiveConstant.LIVE_PK_AE_ANIM_DRAW);
        } else {//我胜利
            playPkAeAnim(LiveConstant.LIVE_PK_AE_ANIM_VICTORY);
        }
    }


    /**
     * 显示直播Pk关闭view
     *
     * @param pkStopPayload
     */
    @Override
    public void showPkStopView(String pkStopPayload) {
        showPkFinishView();
        LivePkMediaPlayer.getInstance().release();
    }

    /**
     * 显示直播收到软妹币变化
     *
     * @param pkGemNoticeBean
     */
    @Override
    public void showPkGemView(LivePkGemNoticeBean pkGemNoticeBean) {

        L.d(TAG, " -------showPkGemView-------" + pkGemNoticeBean);

        L.d(TAG, " showPkGemView userId " + pkGemNoticeBean.userId);


        if (pkGemNoticeBean == null || TextUtils.isEmpty(pkGemNoticeBean.userId)) {
            return;
        }
        if (pkGemNoticeBean.userId.equals(Utils.getMyUserId())) {//如果是送给自己的
            ourGem = pkGemNoticeBean.pkGem;
        } else {
            theriGem = pkGemNoticeBean.pkGem;
        }
        livePkBloodView.setOurPercent(ourGem, theriGem);

        livePkAssistsRandView.initView(Utils.getMyUserId(), pkGemNoticeBean.assisters).setVisibility(VISIBLE);

    }

    /**
     * pk断线重连的init消息
     *
     * @param livePkInitBean
     */
    @Override
    public void showLivePkInitView(LivePkInitBean livePkInitBean) {
        if (livePkInitBean == null) {
            return;
        }
        inPk = true;
        livePkFriendBean = LiveUtils.getLivePkFriendBean(livePkInitBean);
        showPkView();
        if (livePkInitBean.status.equals(LivePkInitBean.STATUS_PK_BUSY)) {//如果是在pk中
            livePkCountDownView.initView(livePkInitBean.leftTime, LivePkCountDownView.TYPE_IN_PK, livePkInitBean.totalTime).show();
            img_live_pk.setVisibility(VISIBLE);
        } else {//如果是在总结中
            inPkSummary = true;
            livePkCountDownView.initView(livePkInitBean.leftTime, LivePkCountDownView.TYPE_IN_SUMMARY, livePkInitBean.totalTime).show();
        }
        txt_finish_pk.setVisibility(VISIBLE);
        txt_pk_other_name.setVisibility(VISIBLE);
        txt_pk_other_name.setText(livePkInitBean.nickName + "");
        livePkBloodView.setOurPercent(livePkInitBean.selfGem, livePkInitBean.toGem);
        livePkAssistsRandView.initView(Utils.getMyUserId(), livePkInitBean.assisters).show();
        LiveUtils.setLivePkIconViewParam(img_live_pk, LiveUtils.getPkUpLoadParams());
    }

    /**
     * pk请求超时
     */
    @Override
    public void showPkRequestTimeOutView(String userId) {
        if (dialog != null && dialog.getDialog() != null && dialog.getDialog().isShowing()) {
            if (userId.equals(Utils.getMyUserId())) {//如果我是Pk接收方
                dialog.showITimeOutView();
            } else {
                dialog.showRequestTimeOutView();
            }
        }
    }

    private void finishPk() {
        if (getActivity() == null) {
            return;
        }
        DialogUtil.showConfirmDialog(getActivity(), "", getString(R.string.sure_end_pk), getString(R.string.info_ok), getString(R.string.info_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (livePkFriendBean != null) {
                    observer.sendPkHangUpMsg(livePkFriendBean.id);
                }
                showPkFinishView();
            }
        });

    }

    private void finishLinkMic() {
        if (getActivity() == null) {
            return;
        }
        DialogUtil.showConfirmDialog(getActivity(), "", getString(R.string.sure_end_mic), getString(R.string.info_ok), getString(R.string.info_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (observer != null && liveRoomMsgConnectMicBean != null && liveRoomMsgConnectMicBean.userId != null && liveRoomMsgConnectMicBean.userId.length() > 0) {
                    observer.hangupConnectMic("hangup", liveRoomMsgConnectMicBean.userId);
                    linkMicHangup();
                }
            }
        });

    }

    private void showPkFinishView() {
        livePkBloodView.destroyView();
        livePkAssistsRandView.destroyView();
        livePkCountDownView.destroyView();
        txt_pk_other_name.setVisibility(View.GONE);
        txt_finish_pk.setVisibility(View.GONE);
        img_live_pk.setVisibility(View.GONE);
        live_pk_ae_view.setVisibility(View.GONE);
        livePkFailedView.destroyView();
//        layout_live_ad.setVisibility(VISIBLE);
        liveAdViewManager.show();
        view_pk_other.setVisibility(View.GONE);

        inPk = false;
        inPkSummary = false;
        ourGem = 0f;
        theriGem = 0f;
        livePkFriendBean = null;
        pk_fierce_sound_play_time = 0;

        final RelativeLayout.LayoutParams chat_param = (RelativeLayout.LayoutParams) listview_chat.getLayoutParams();
        //结束Pk后聊天右边距为正常margin
        chat_param.rightMargin = (int) TheLApp.getContext().getResources().getDimension(R.dimen.live_capture_chat_common_right_margin);
        listview_chat.setLayoutParams(chat_param);

    }

    /**
     * 此方法为防止直播聊天断线而收不到PK关闭的消息防止的误差
     */
    private void tryFinishPkStream() {
        observer.postDelayed(new Runnable() {
            @Override
            public void run() {
                showPkFinishView();
                observer.stopPkStream();
            }
        }, 5000);
    }

    /**
     * 直播Pk请求错误返回
     *
     * @param errorCode
     */
    @Override
    public void showPkRequestErrorView(String errorCode) {
        String content = "";
        switch (errorCode) {
            case LiveRoomMsgBean.TYPE_LIVE_PK_RESPONSE_CODE_NOT_LIVING:
                content = StringUtils.getString(R.string.broadcaster_not_live);
                break;
            case LiveRoomMsgBean.TYPE_LIVE_PK_RESPONSE_CODE_IN_LINKMIC:
                content = StringUtils.getString(R.string.broadcaster_is_connecting);
                break;
            case LiveRoomMsgBean.TYPE_LIVE_PK_RESPONSE_CODE_IN_PK:
                content = StringUtils.getString(R.string.broadcaster_pk_now);
                break;
            case LiveRoomMsgBean.TYPE_LIVE_PK_RESPONSE_CODE_LOW_CLI_VER:
                content = StringUtils.getString(R.string.her_version_low);
                break;
            case LiveRoomMsgBean.TYPE_LIVE_PK_RESPONSE_CODE_REJECT:
                break;
            case LiveRoomMsgBean.TYPE_LIVE_PK_RESPONSE_CODE_500:
                break;
        }
        if (dialog != null && dialog.getDialog() != null && dialog.getDialog().isShowing()) {
            dialog.dismiss();
        }
        DialogUtil.showToastShort(TheLApp.getContext(), content);

    }

    @Override
    public void showTopGiftView(TopGiftBean topGiftBean) {
        if (topGiftBean == null || softEnjoyBean == null) {
            return;
        }
        final SoftGiftBean softGiftBean = LiveUtils.getGiftBeanById(topGiftBean.id, softEnjoyBean);
        if (softGiftBean == null) {
            return;
        }
        topGiftBean.softGiftBean = softGiftBean;
        live_top_gift_layout.receiveGift(topGiftBean);
    }

    /**********************************************以上为直播PK的方法***********************************************************/


    @Override
    public void videoLinkAdd(LivePkFriendBean livePkFriendBean) {

        if (isLinkMic) {
            livePkFriendBean.liveStatus = LivePkFriendBean.LIVE_STATUS_LINK;
        }

        livePkFriendBeans.add(livePkFriendBean);

        if (liveLinkMicDialog != null && liveLinkMicDialog.isAdded()) {

            liveLinkMicDialog.addItem(livePkFriendBean);

        }

        sortMicListCount++;

        setUnreadCount(link_mic_count_tv, sortMicListCount);


    }

    @Override
    public void videoLinkDel(LivePkFriendBean livePkFriendBean) {


        livePkFriendBeans.remove(livePkFriendBean);

        Iterator<LivePkFriendBean> iterator = livePkFriendBeans.iterator();

        while (iterator.hasNext()) {
            LivePkFriendBean lpb = iterator.next();
            if (lpb.id.equals(livePkFriendBean.id)) {
                iterator.remove();
            }
        }

        if (liveLinkMicDialog != null && liveLinkMicDialog.isAdded()) {
            liveLinkMicDialog.removeItem(livePkFriendBean);
        }
        if (sortMicListCount > 0) {
            sortMicListCount--;
        }
        setUnreadCount(link_mic_count_tv, sortMicListCount);
    }

    /**
     * 锁定输入框
     */
    @Override
    public void lockInput() {
        edit_input.setText("");

    }

    @Override
    public void showConnectMicDialog(LiveRoomMsgConnectMicBean liveRoomMsgConnectMicBean) {

        if (liveRoomMsgConnectMicBean != null) {

            L.d("ConnectMicDialog", " liveRoomMsgConnectMicBean.method : " + liveRoomMsgConnectMicBean.toString());

            switch (liveRoomMsgConnectMicBean.method) {
                case "request":

                    if (mPkFriendListViewDialog != null && mPkFriendListViewDialog.isAdded()) {

                        mPkFriendListViewDialog.dismiss();
                    }

                    if (liveLinkMicDialog != null && liveLinkMicDialog.isAdded()) {

                        liveLinkMicDialog.dismiss();
                    }

                    showRequestLinkMicDialog(liveRoomMsgConnectMicBean);
                    break;
                case "response":
                    if (connectMicDialog != null) {
                        connectMicDialog.showRequestAcceptView();
                    }
                    break;
                case "start":

                    this.liveRoomMsgConnectMicBean = liveRoomMsgConnectMicBean;

                    if (connectMicDialog != null) {
                        connectMicDialog.showRequestAcceptView();
                        linkMicStatus = LINK_MIC_STATUS_FRIEND;
                        if (liveLinkMicDialog != null) {
                            liveLinkMicDialog.setLinkMicStatus(LINK_MIC_STATUS_FRIEND);
                        }
                    } else {
                        final ConnectMicDialog connectMicDialog = new ConnectMicDialog();

                        Bundle bundle = new Bundle();
                        bundle.putInt(TheLConstants.BUNDLE_KEY_ACTION_TYPE, ConnectMicDialog.TYPE_AUDIENCE_LINK_MIC_RESPONSE);

                        bundle.putString(BUNDLE_KEY_NICKNAME, liveRoomMsgConnectMicBean.getNickName());
                        bundle.putString(BUNDLE_KEY_USER_AVATAR, liveRoomMsgConnectMicBean.avatar);

                        connectMicDialog.setArguments(bundle);
                        connectMicDialog.show(getActivity().getSupportFragmentManager(), ConnectMicDialog.class.getName());

                        linkMicStatus = LINK_MIC_STATUS_AUDIENCE;
                        if (liveLinkMicDialog != null) {
                            liveLinkMicDialog.setLinkMicStatus(LINK_MIC_STATUS_AUDIENCE);
                        }

                    }

                    if (link_mic_layer != null) {
                        link_mic_layer.show(liveRoomMsgConnectMicBean.getNickName(), liveRoomMsgConnectMicBean.userId, false);
                    }

                    break;
                case "cancel":
                    if (liveRoomMsgConnectMicBean.toUserId != null && liveRoomMsgConnectMicBean.fromUserId != null) {
                        if (connectMicDialog != null) {
                            connectMicDialog.cancelThisLinkMicByOwn();
                        }
                    } else {
                        if (connectMicDialog != null) {
                            connectMicDialog.showRequestCancelView();
                        }
                    }

                    linkMicStatus = LINK_MIC_STATUS_NONE;
                    if (liveLinkMicDialog != null) {
                        liveLinkMicDialog.setLinkMicStatus(LINK_MIC_STATUS_NONE);
                    }

                    break;
                case "timeout":

                    String toUserId = liveRoomMsgConnectMicBean.toUserId;

                    String fromUserId = liveRoomMsgConnectMicBean.fromUserId;

                    if (!TextUtils.isEmpty(toUserId) && !toUserId.equals(UserUtils.getMyUserId())) {
                        if (connectMicDialog != null) {
                            connectMicDialog.showRequestTimeOutView();
                        }
                    } else {
                        if (connectMicDialog != null) {
                            connectMicDialog.showITimeOutView();
                        }
                    }

                    linkMicStatus = LINK_MIC_STATUS_NONE;
                    if (liveLinkMicDialog != null) {
                        liveLinkMicDialog.setLinkMicStatus(LINK_MIC_STATUS_NONE);
                    }

                    break;

                case "hangup":
                    if (connectMicDialog != null) {
                        showHangupView(liveRoomMsgConnectMicBean.getNickName(), liveRoomMsgConnectMicBean.avatar, true);
                    }

                    linkMicStatus = LINK_MIC_STATUS_NONE;
                    if (liveLinkMicDialog != null) {
                        liveLinkMicDialog.setLinkMicStatus(LINK_MIC_STATUS_NONE);
                    }

                    break;
                case "stop":

                    break;

            }
        }

    }

    private ConnectMicDialog connectMicDialog;

    @Override
    public void linkMicAccept() {
        if (connectMicDialog != null) {
            connectMicDialog.showIAcceptView();
        }
    }

    @Override
    public void lingMicCancel() {
        if (connectMicDialog != null) {
            connectMicDialog.showRequestCancelView();
        }
    }

    @Override
    public void showLinkMicLayer(String userName, String toUserId) {

        L.d(TAG, " ----------showLinkMicLayer----------- ");

        if (link_mic_layer != null) {

            isLinkMic = true;

            link_mic_layer.show(userName, toUserId, true);

            link_mic_owner_view.show(toUserId);

            if (livePkFriendBeans != null && livePkFriendBeans.size() > 0) {
                Iterator<LivePkFriendBean> iterator = livePkFriendBeans.iterator();
                while (iterator.hasNext()) {
                    LivePkFriendBean lpb = iterator.next();
                    if (lpb.id.equals(toUserId)) {
                        iterator.remove();
                    } else {
                        lpb.liveStatus = LivePkFriendBean.LIVE_STATUS_LINK;
                    }
                }
            }

            if (liveLinkMicDialog != null && liveLinkMicDialog.isAdded()) {
                liveLinkMicDialog.startLinkMic(toUserId);
            }

            if (sortMicListCount > 0) {
                sortMicListCount--;
            } else {
                sortMicListCount = 0;
            }

            setUnreadCount(link_mic_count_tv, sortMicListCount);

        }
    }

    @Override
    public void linkMicHangup() {

        L.d(TAG, " ----------linkMicHangup----------- ");

        if (link_mic_layer != null) {

            isLinkMic = false;

            linkMicStatus = LINK_MIC_STATUS_NONE;

            link_mic_layer.hide();

            link_mic_owner_view.hide();

            final RelativeLayout.LayoutParams chat_param = (RelativeLayout.LayoutParams) listview_chat.getLayoutParams();
            //结束Pk后聊天右边距为正常margin
            chat_param.rightMargin = (int) TheLApp.getContext().getResources().getDimension(R.dimen.live_capture_chat_common_right_margin);

            listview_chat.setLayoutParams(chat_param);

            showPkFinishView();

        }
    }

    @Override
    public void showHangupView(String nickName, String avatar, boolean guardLinkMic) {

        if (connectMicDialog != null) {
            connectMicDialog.dismiss();
        }

        connectMicDialog = new ConnectMicDialog();
        connectMicDialog.setOnButtonClickListener(new ConnectMicDialog.OnButtonClickListener() {
            @Override
            public void onClick(int type) {

                switch (type) {
                    case TYPE_HANGUP:
                        if (connectMicDialog != null) {
                            connectMicDialog.dismiss();
                        }
                        break;
                }
            }
        });

        Bundle bundle = new Bundle();
        bundle.putInt(TheLConstants.BUNDLE_KEY_ACTION_TYPE, ConnectMicDialog.DIALOG_TYPE_HANGUP);

        if (liveRoomMsgConnectMicBean != null) {
            nickName = liveRoomMsgConnectMicBean.getNickName();
            avatar = liveRoomMsgConnectMicBean.avatar;
        }
        bundle.putBoolean("dailyGuard", guardLinkMic);
        bundle.putString(BUNDLE_KEY_NICKNAME, nickName);
        bundle.putString(BUNDLE_KEY_USER_AVATAR, avatar);

        connectMicDialog.setArguments(bundle);
        connectMicDialog.show(getActivity().getSupportFragmentManager(), ConnectMicDialog.class.getName());

    }


    @Override
    public void showLinkMicSendSuccess(final String toUserId, String nickName, String avatar, boolean dailyGuard) {

        if (connectMicDialog != null) {
            connectMicDialog.dismiss();
        }

        connectMicDialog = new ConnectMicDialog();
        connectMicDialog.setOnButtonClickListener(new ConnectMicDialog.OnButtonClickListener() {
            @Override
            public void onClick(int type) {

                switch (type) {
                    case TYPE_CANCEL:
                        observer.cancelConnectMic("cancel", toUserId);
                        if (connectMicDialog != null) {
                            connectMicDialog.dismiss();
                        }
                        break;
                    case TYPE_I_NO_RESPONSE:
                        if (connectMicDialog != null) {
                            connectMicDialog.dismiss();
                        }
                        break;
                    case TYPE_REQUEST_NO_RESPONSE:
                        if (connectMicDialog != null) {
                            connectMicDialog.dismiss();
                        }
                        break;
                }
            }
        });

        Bundle bundle = new Bundle();
        bundle.putInt(TheLConstants.BUNDLE_KEY_ACTION_TYPE, ConnectMicDialog.DIALOG_TYPE_MIC_I);
        bundle.putString(BUNDLE_KEY_NICKNAME, nickName);
        bundle.putString(BUNDLE_KEY_USER_AVATAR, avatar);
        bundle.putBoolean("dailyGuard", dailyGuard);
        connectMicDialog.setArguments(bundle);
        connectMicDialog.show(getActivity().getSupportFragmentManager(), ConnectMicDialog.class.getName());
    }

    /**
     * 相遇状态的监听
     *
     * @param mOnMeetStatusListener
     */
    public void setOnMeetStatusListener(OnMeetStatusListener mOnMeetStatusListener) {
        this.mOnMeetStatusListener = mOnMeetStatusListener;
    }

    private void showRequestLinkMicDialog(LiveRoomMsgConnectMicBean liveRoomMsgConnectMicBean) {
        final String toUserId = liveRoomMsgConnectMicBean.userId;
        final String userId = ShareFileUtils.getString(ShareFileUtils.ID, null);

        if (toUserId != null && userId != null) {

            if (!toUserId.equals(userId)) {

                if (connectMicDialog != null) {
                    connectMicDialog.dismiss();
                }

                connectMicDialog = new ConnectMicDialog();
                connectMicDialog.setOnButtonClickListener(new ConnectMicDialog.OnButtonClickListener() {
                    @Override
                    public void onClick(int type) {
                        switch (type) {
                            case TYPE_ACCEPT:
                                final RectF mVideoInfoRectF = LinkMicScreenUtils.getChildVideoRect();

                                L.d(TAG, " mVideoInfoRectF : " + mVideoInfoRectF);

                                observer.responseConnectMic("response", toUserId, "yes", mVideoInfoRectF.left, mVideoInfoRectF.top, mVideoInfoRectF.bottom, mVideoInfoRectF.right);
                                break;
                            case TYPE_REFUSE:
                                if (connectMicDialog != null) {
                                    connectMicDialog.dismiss();
                                }
                                observer.responseConnectMic("response", toUserId, "no", 0, 0, 0, 0);
                                break;
                            case TYPE_CANCEL:
                                if (connectMicDialog != null) {
                                    connectMicDialog.dismiss();
                                }
                                break;
                            case TYPE_REQUEST_NO_RESPONSE:
                                if (connectMicDialog != null) {
                                    connectMicDialog.dismiss();
                                }
                                break;
                            case TYPE_I_NO_RESPONSE:
                                if (connectMicDialog != null) {
                                    connectMicDialog.dismiss();
                                }
                                break;

                        }

                    }

                });
                Bundle bundle = new Bundle();
                bundle.putInt(TheLConstants.BUNDLE_KEY_ACTION_TYPE, ConnectMicDialog.DIALOG_TYPE_MIC_REQUEST);
                bundle.putString(BUNDLE_KEY_NICKNAME, liveRoomMsgConnectMicBean.getNickName());
                bundle.putString(BUNDLE_KEY_USER_AVATAR, liveRoomMsgConnectMicBean.avatar);
                connectMicDialog.setArguments(bundle);
                connectMicDialog.show(getActivity().getSupportFragmentManager(), ConnectMicDialog.class.getName());
            }

        }
    }

    private boolean isScrolling = false;

    private final List<LiveRoomMsgBean> msgBufferList = new ArrayList<>();
    private final List<LiveRoomMsgBean> msgBufferListS = new ArrayList<>();

    private void addMsg() {
        Flowable.interval(1000, TimeUnit.MILLISECONDS).onBackpressureDrop().observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Long>() {
            @Override
            public void accept(Long aLong) {
                if (!isScrolling && adapter != null && observer != null && msgBufferList.size() > 0) {
                    synchronized (msgBufferList) {
                        if (isRefreshingMsgList) {
                            return;
                        }

                        isRefreshingMsgList = true;
                        msgBufferListS.clear();
                        msgBufferListS.addAll(msgBufferList);
                        msgBufferList.clear();
                    }

                    final int start = msgList.size();
                    final int length = msgBufferListS.size();
                    msgList.addAll(msgBufferListS);
                    msgBufferList.clear();
                    final long bt = System.currentTimeMillis();
                    adapter.notifyItemRangeInserted(start, length);
                    if (isBottom) {
                        observer.sendEmptyMessage(UI_EVENT_ANCHOR_SCROLL_TO_BOTTOM);
                    } else // 两秒后将isBottom设为true，避免出现有时候isBottom一直为false
                    {
                        observer.sendEmptyMessageDelayed(UI_EVENT_ANCHOR_RESET_IS_BOTTOM, 2000);
                    }
                    isRefreshingMsgList = false;


                }
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        L.d("LiveShowCaptureViewFragment", " onPause ");
        if (liveBigGiftAnimLayout != null) {
            liveBigGiftAnimLayout.onPause();
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        L.d("LiveShowCaptureViewFragment", " onResume ");
        if (liveBigGiftAnimLayout != null) {
            liveBigGiftAnimLayout.onResume();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

}
