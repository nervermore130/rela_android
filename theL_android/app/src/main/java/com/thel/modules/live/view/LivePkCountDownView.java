package com.thel.modules.live.view;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.modules.live.in.LiveBaseView;

import java.text.SimpleDateFormat;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by waiarl on 2017/12/8.
 * 直播Pk倒计时view
 */

public class LivePkCountDownView extends LinearLayout implements LiveBaseView<LivePkCountDownView> {
    private final Context mContext;
    private final int COUNT_DOWN = 1;
    private final int COUNT_END = 2;
    private final int COUNT_SUMMARY_END = 3;
    private MyTimeTask task;
    private Timer timer;
    private TextView txt_count_down;
    private long delayed = 1000l;
    private long period = 1000l;

    public static final int TYPE_IN_PK = 0;//pk阶段
    public static final int TYPE_IN_SUMMARY = 1;//总结阶段
    /***倒计时到达的一些阶段***/

    public static final int PERIOD_MIDDLE = 1;//倒计时到中间，4分钟的时候
    public static final int PERIOD_LAST_MINUTE = 2;//倒计时到最后一分钟
    public static final int PERIOD_LAST_TEN_SECOND = 3;//倒计时到最后10秒
    public static final int PERIOD_SUMMARY_END = 4;//总结时间结束

    private long remainTime = 60 * 8;
    private long TIME_PERIOD_TOTAL = 8 * 60;
    private long TIME_PERIOD_MIDDLE = (long) (4 * 60);//倒计时4分钟
    private long TIME_PERIOD_LAST_MINUTE = 1 * 60;//倒计时一分钟
    private final long TIME_PERIOD_LAST_TEN_SECOND = 10;//倒计时十秒

    private int type = TYPE_IN_PK;
    private LivePkCountDownListener listener;
    private long totalTime;//pk总时间

    public LivePkCountDownView(Context context) {
        this(context, null);
    }

    public LivePkCountDownView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LivePkCountDownView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }

    private void init() {
        timer = new Timer();
        inflate(mContext, R.layout.live_pk_count_down_view, this);
        txt_count_down = findViewById(R.id.txt_count_down);
    }

    public LivePkCountDownView initView(int type) {
        this.remainTime = TIME_PERIOD_TOTAL;
        this.type = type;
        startCountDown();
        return this;
    }

    public LivePkCountDownView initView(long remainTime, int type, long totalTime) {
        TIME_PERIOD_TOTAL = totalTime;
        TIME_PERIOD_MIDDLE = TIME_PERIOD_TOTAL / 2;
        if (TIME_PERIOD_MIDDLE <= TIME_PERIOD_LAST_MINUTE) {
            TIME_PERIOD_LAST_MINUTE = TIME_PERIOD_MIDDLE - 10;
        }
        this.remainTime = remainTime;
        this.type = type;
        handler.removeCallbacksAndMessages(null);
        if (type == TYPE_IN_PK && remainTime < TIME_PERIOD_LAST_MINUTE && remainTime > 2) {//如果是最后一分钟进入，多2秒钟的缓冲余地
            if (listener != null) {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        listener.countDownPeriod(PERIOD_LAST_MINUTE);
                    }
                }, 500);
                if (remainTime < TIME_PERIOD_LAST_TEN_SECOND) {//如果是最后十秒钟进入
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            listener.countDownPeriod(PERIOD_LAST_TEN_SECOND);
                        }
                    }, 500);
                }
            }
        }
        startCountDown();
        return this;
    }

    public LivePkCountDownView initView(long remainTime, int type) {
        this.remainTime = remainTime;
        this.type = type;
        handler.removeCallbacksAndMessages(null);
        startCountDown();
        return this;
    }

    /**
     * 开始倒计时
     *
     * @RETURN
     */
    public LivePkCountDownView startCountDown() {
        showCountText();
        if (task != null) {
            task.cancel();
        }
        task = new MyTimeTask();
        timer.scheduleAtFixedRate(task, delayed, period);
        return this;
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case COUNT_DOWN:
                    showCountText();//显示倒计时
                    if (type == TYPE_IN_PK) {//如果是在pk阶段
                        if (listener != null) {
                            if (remainTime == TIME_PERIOD_MIDDLE) {//倒计时4分钟
                                listener.countDownPeriod(PERIOD_MIDDLE);
                            } else if (remainTime == TIME_PERIOD_LAST_MINUTE) {//最后一分钟
                                listener.countDownPeriod(PERIOD_LAST_MINUTE);
                            } else if (remainTime == TIME_PERIOD_LAST_TEN_SECOND) {//最后10秒
                                listener.countDownPeriod(PERIOD_LAST_TEN_SECOND);
                            }
                        }
                    }
                    break;
                case COUNT_END://显示正常内容
                    showCountEnd();
                    break;
                case COUNT_SUMMARY_END://总结时间结束
                    if (listener != null) {
                        listener.countDownPeriod(PERIOD_SUMMARY_END);
                    }
                    break;
            }
        }
    };

    /**
     * 倒计时结束
     */
    private void showCountEnd() {

    }

    /**
     * 显示倒计时
     */
    private void showCountText() {
        final String re = getFormatTime(remainTime);
        String counTxt = TheLApp.getContext().getString(R.string.count_down_pk, re);
        switch (type) {
            case TYPE_IN_SUMMARY:
                counTxt = TheLApp.getContext().getString(R.string.summary_time, re);
                break;
        }
        txt_count_down.setText(counTxt);
    }

    /**
     * 获取倒计时 为 mm:ss格式的字符串
     *
     * @param remainSecond 剩余的秒钟
     * @return
     */
    private String getFormatTime(long remainSecond) {
        if (remainSecond < 0) {
            remainSecond = 0;
        }
        return new SimpleDateFormat("mm:ss").format(remainSecond * 1000);
    }

    @Override
    public LivePkCountDownView show() {
        setVisibility(View.VISIBLE);
        return this;
    }

    @Override
    public LivePkCountDownView hide() {
        return null;
    }

    @Override
    public LivePkCountDownView destroyView() {
        handler.removeCallbacksAndMessages(null);
        if (task != null) {
            task.cancel();
        }
        handler.removeCallbacksAndMessages(null);
        setVisibility(View.GONE);
        return this;
    }

    @Override
    public boolean isAnimating() {
        return false;
    }

    @Override
    public void setAnimating(boolean isAnimating) {

    }

    @Override
    public void showShade(boolean show) {

    }


    class MyTimeTask extends TimerTask {

        @Override
        public void run() {
            if (remainTime > 0) {
                remainTime--;
                handler.sendEmptyMessage(COUNT_DOWN);
            }
            if (remainTime <= 0) {
                handler.sendEmptyMessage(COUNT_END);
                if (type == TYPE_IN_SUMMARY) {//如果是总结结束，为了防止消息服务器断开的万一，加一个结束PK的回调
                    handler.sendEmptyMessage(COUNT_SUMMARY_END);
                }
                cancel();
            }
        }
    }

    /**
     * 倒计时到达某个阶段监听
     */
    public interface LivePkCountDownListener {
        void countDownPeriod(int period);
    }

    /**
     * 设置倒计时监听
     *
     * @param listener
     */
    public void setOnLivePkCountDownListener(LivePkCountDownListener listener) {
        this.listener = listener;
    }

    @Override
    public void setVisibility(int visibility) {
        super.setVisibility(visibility);
        if (visibility != View.VISIBLE) {
        }
    }

    /**
     * 观看端只总结时，显示胜利者
     *
     * @param winnerName
     */
    public LivePkCountDownView setLiveShowSummaryView(String winnerName) {
        if (task != null) {
            task.cancel();
        }
        handler.removeCallbacksAndMessages(null);
        if (TextUtils.isEmpty(winnerName)) {
            destroyView();
            return this;
        }
        txt_count_down.setText(TheLApp.getContext().getString(R.string.live_pk_the_winner, winnerName));
        return this;
    }

    public LivePkCountDownView setLiveShowDrawText() {
        if (task != null) {
            task.cancel();
        }
        handler.removeCallbacksAndMessages(null);
        txt_count_down.setText("");
        destroyView();
        return this;
    }

}
