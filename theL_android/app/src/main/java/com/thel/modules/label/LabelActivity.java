package com.thel.modules.label;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.bean.RecentAndHotTagBean;
import com.thel.bean.RecentAndHotTopicsBean;
import com.thel.bean.TopicBean;
import com.thel.bean.TopicListBean;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.ui.LoadingDialogImpl;
import com.thel.utils.DialogUtil;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class LabelActivity extends BaseActivity implements AbsListView.OnScrollListener {

    @BindView(R.id.edit_search)
    EditText mSearchContent;

    @BindView(R.id.listview)
    ListView mSearchResult;

    @BindView(R.id.recent_and_hot_listview)
    ListView recent_and_hot_listview;

    @BindView(R.id.img_search)
    ImageView img_search;


    private LoadingDialogImpl loadingDialog;

    private int refreshType = 0;
    private static final int REFRESH_TYPE_ALL = 1;
    private static final int REFRESH_TYPE_NEXT_PAGE = 2;

    private int curPage = 1;
    private int currentCount = 0;

    private boolean canLoadNext = false;

    private TopicListAdapter adapter;
    private ArrayList<TopicBean> listPlus = new ArrayList<TopicBean>();

    private RecentAndHotTopicListAdapter recentAndHotTopicListAdapter;
    private ArrayList<RecentAndHotTagBean> recentAndHotTagBeans = new ArrayList<RecentAndHotTagBean>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_label);

        ButterKnife.bind(this);

        initView();

        loadingDialog = LoadingDialogImpl.LoadingDialogInstance.getInstance(this);

        loadingDialog.showLoading();
        RequestBusiness.getInstance()
                .getRecentAndHotTopics()
                .onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new InterceptorSubscribe<RecentAndHotTopicsBean>() {
                    @Override
                    public void onNext(RecentAndHotTopicsBean recentAndHotTopicsBean) {
                        super.onNext(recentAndHotTopicsBean);
                        loadingDialog.closeDialog();
                        if (recentAndHotTopicsBean != null) {
                            if (recentAndHotTopicsBean.recentTopicList != null)
                                recentAndHotTagBeans.addAll(recentAndHotTopicsBean.recentTopicList);
                            if (recentAndHotTopicsBean.hotTopicList != null)
                                recentAndHotTagBeans.addAll(recentAndHotTopicsBean.hotTopicList);
                        }

                        if (recentAndHotTagBeans.size() > 0) {
                            recentAndHotTopicListAdapter = new RecentAndHotTopicListAdapter(LabelActivity.this, recentAndHotTagBeans);
                            recent_and_hot_listview.setAdapter(recentAndHotTopicListAdapter);
                        }
                    }
                });
    }

    private void initView() {
        mSearchResult.setSelector(new ColorDrawable(Color.TRANSPARENT));
        mSearchResult.setOnScrollListener(this);

        mSearchContent.setHint(getString(R.string.search_topic_hint));

    }

    private void getTopic(String topicName) {
        loadingDialog.showLoading();
        RequestBusiness
                .getInstance()
                .searchTopics(topicName, curPage)
                .onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new InterceptorSubscribe<TopicListBean>() {
                    @Override
                    public void onNext(TopicListBean topicBean) {
                        super.onNext(topicBean);
                        loadingDialog.closeDialog();
                        if (topicBean == null || topicBean.suggestList == null) return;
                        if (refreshType == REFRESH_TYPE_ALL) {
                            listPlus.clear();
                            curPage = 2;
                        } else {
                            curPage++;
                        }
                        listPlus.addAll(topicBean.suggestList);
                        canLoadNext = true;
                        currentCount = listPlus.size();

                        if (curPage == 2 && currentCount == 0) {// 如果没有搜到标签
                            TopicBean newTopic = new TopicBean();
                            newTopic.topicId = "-1";
                            newTopic.topicName = "#" + mSearchContent.getText().toString().trim() + "#";
                            listPlus.add(newTopic);
                        }

                        if (curPage >= 2 && currentCount > 0 && topicBean.suggestList.size() == 0) {// 没有更多了
                            canLoadNext = false;
                            DialogUtil.showToastShort(LabelActivity.this, getString(R.string.info_no_more));
                        }

                        if (null == adapter) {
                            adapter = new TopicListAdapter(LabelActivity.this, listPlus);
                            mSearchResult.setAdapter(adapter);
                        } else {
                            adapter.freshAdapter(listPlus);
                            adapter.notifyDataSetChanged();
                        }
                        if (listPlus.size() > 0) {
                            mSearchResult.setVisibility(View.VISIBLE);
                        }
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(this.getClass().getName());
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(this.getClass().getName());
        MobclickAgent.onPause(this);
    }

    @Override
    public void finish() {
        ViewUtils.hideSoftInput(this, mSearchContent);
        super.finish();
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    @OnClick(R.id.img_search)
    void search() {
        // 点击搜索
        String content = mSearchContent.getText().toString().trim();
        if (TextUtils.isEmpty(content)) {
            // 如果关键字为空，则显示最近和热门话题
            // DialogUtil.showToastShort(SearchTopicActivity.this,
            // getString(R.string.info_is_empty));
            mSearchResult.setVisibility(View.GONE);
            return;
        }
        curPage = 1;
        refreshType = REFRESH_TYPE_ALL;
        getTopic(content);
    }

    @OnEditorAction(R.id.edit_search)
    boolean editorAction(int actionId) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            img_search.performClick();
            return true;
        }
        return false;
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        switch (scrollState) {
            // 当不滚动时
            case AbsListView.OnScrollListener.SCROLL_STATE_IDLE:
                // 判断滚动到底部
                if (view.getLastVisiblePosition() == view.getCount() - 1 && canLoadNext && currentCount > 0) {
                    showLoading();
                    canLoadNext = false;
                    // 列表滑动到底部加载下一组数据
                    refreshType = REFRESH_TYPE_NEXT_PAGE;
                    getTopic(mSearchContent.getText().toString().trim());
                }

                break;
        }
    }

    @Override
    public void onScroll(AbsListView absListView, int i, int i1, int i2) {

    }

    @OnClick(R.id.lin_back)
    void back() {
        finish();
    }

}
