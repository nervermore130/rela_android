package com.thel.modules.main.me.aboutMe;

import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.text.TextUtils;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

import com.google.gson.GsonBuilder;
import com.tencent.mm.opensdk.modelbiz.WXLaunchMiniProgram;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.bean.AdBean;
import com.thel.constants.TheLConstants;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.ui.widget.LollipopFixedWebView;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.ToastUtils;
import com.thel.utils.ViewUtils;

import org.reactivestreams.Subscription;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.thel.modules.main.home.moments.web.WebViewActivity.url_walkman;

public class WalkmanActivity extends BaseActivity {

    private static final String TAG = "WalkmanActivity";

    @BindView(R.id.webView)
    LollipopFixedWebView webView;

    @BindView(R.id.swipe_container)
    SwipeRefreshLayout swipe_container;

    @BindView(R.id.bottom)
    LinearLayout bottom;

    @OnClick(R.id.back)
    void back() {
        finish();
    }

    @OnClick(R.id.walkman)
    void walkman() {
        WXLaunchMiniProgram.Req req = new WXLaunchMiniProgram.Req();
        req.userName = TheLConstants.WX_MINIPROGRAM_RELA_WALKMAN_ID; // 填小程序原始id
        req.path = TheLConstants.WX_MINIPROGRAM_RELA_WALKMAN_PATH;//拉起小程序页面的可带参路径，不填默认拉起小程序首页
        req.miniprogramType = WXLaunchMiniProgram.Req.MINIPTOGRAM_TYPE_RELEASE;// 可选打开 开发版，体验版和正式版
        wxapi.sendReq(req);
    }

    private IWXAPI wxapi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_walkman);

        ButterKnife.bind(this);

        wxapi = WXAPIFactory.createWXAPI(this, TheLConstants.WX_APP_ID);
        if (wxapi.isWXAppInstalled()) {
            bottom.setVisibility(View.VISIBLE);
        } else {
            bottom.setVisibility(View.GONE);
        }

        webView.setBackgroundColor(getResources().getColor(R.color.white));
        WebSettings settings = webView.getSettings();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            settings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        settings.setJavaScriptEnabled(true);
        settings.setDomStorageEnabled(true);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                L.d(TAG, " url : " + url);

                if (TextUtils.isEmpty(url))
                    return true;

                if (url.contains(url_walkman)) {
                    IWXAPI wxapi = WXAPIFactory.createWXAPI(WalkmanActivity.this, TheLConstants.WX_APP_ID);
                    if (wxapi.isWXAppInstalled()) {

                        Uri uri = Uri.parse(url);

                        String path = uri.getQueryParameter("path");

                        WXLaunchMiniProgram.Req req = new WXLaunchMiniProgram.Req();
                        req.userName = TheLConstants.WX_MINIPROGRAM_RELA_WALKMAN_ID; // 填小程序原始id
                        req.path = path;//拉起小程序页面的可带参路径，不填默认拉起小程序首页
                        req.miniprogramType = WXLaunchMiniProgram.Req.MINIPTOGRAM_TYPE_RELEASE;// 可选打开 开发版，体验版和正式版
                        wxapi.sendReq(req);
                    } else {
                        ToastUtils.showToastShort(WalkmanActivity.this, "wechat not install");
                    }
                }

                return true;
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                handler.proceed();
                super.onReceivedSslError(view, handler, error);
            }
        });
        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                if (newProgress == 100) {
                    //隐藏进度条
                    swipe_container.setRefreshing(false);
                } else {
                    if (!swipe_container.isRefreshing())
                        swipe_container.setRefreshing(true);
                }

                super.onProgressChanged(view, newProgress);
            }
        });

        ViewUtils.initSwipeRefreshLayout(swipe_container);
        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                webView.loadUrl(webView.getUrl());
            }
        });

        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);


        RequestBusiness.getInstance().getAd().onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<AdBean>() {
            @Override
            public void onNext(AdBean data) {
                super.onNext(data);
                String json = GsonUtils.createJsonString(data);
                ShareFileUtils.setString(ShareFileUtils.ADS, json);
                final List<AdBean.Map_list> ad_list = data.data.map_list;
                if (ad_list != null && ad_list.size() > 0) {
                    for (AdBean.Map_list bean : ad_list) {
                        if (bean.advertLocation.equals("fm")) {
                            ShareFileUtils.setInt(ShareFileUtils.ME_ADS_ID, bean.id);

                            String adJson = ShareFileUtils.getString(ShareFileUtils.ADS, "");
                            if (!TextUtils.isEmpty(adJson)) {
                                if (data.data != null && data.data.map_list != null) {
                                    for (int i = 0; i < data.data.map_list.size(); i++) {
                                        AdBean.Map_list map_list = data.data.map_list.get(i);
                                        if (map_list.advertLocation.equals("fm")) {
                                            webView.loadUrl(map_list.dumpURL);
                                        }
                                    }
                                }
                            }
                        }

                    }
                }

            }
        });



    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }
}
