package com.thel.modules.main.userinfo;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.List;

/**
 * Created by waiarl on 2018/4/17.
 */

public class UserInfoMomentAdapter extends FragmentStatePagerAdapter {

    private final List<Fragment> list;
    private final List<String> titliList;

    public UserInfoMomentAdapter(FragmentManager childFragmentManager, List<Fragment> list, List<String> titleList) {
        super(childFragmentManager);
        this.list = list;
        this.titliList = titleList;
    }

    @Override
    public Fragment getItem(int position) {
        return list.get(position);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (titliList != null) {
            return titliList.get(position % titliList.size());
        }
        return "";
    }
}
