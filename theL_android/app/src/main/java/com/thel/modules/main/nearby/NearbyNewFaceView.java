package com.thel.modules.main.nearby;

import android.content.Context;
import android.content.Intent;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.growingio.android.sdk.collection.GrowingIO;
import com.thel.R;
import com.thel.bean.user.NearUserBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.modules.main.me.bean.LogInfoBean;
import com.thel.modules.main.me.match.eventcollect.collect.LiveLogUtils;
import com.thel.modules.main.nearby.adapter.NearbyUserRecyclerViewAdapter;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.utils.ShareFileUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by chad
 * Time 18/8/6
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */
public class NearbyNewFaceView extends LinearLayout {
    private final Context mContext;
    private RecyclerView recyclerView;
    private LinearLayout head_top;
    private List<NearUserBean> list = new ArrayList<>();
    private List<NearUserBean> momentsList = new ArrayList<>();
    private NearbyUserRecyclerViewAdapter adapter;
    private RelativeLayout more_pig_rl;
    private StaggeredGridLayoutManager manager;
    private final int NINE_PALACES_COUNT = 9;//附近九宫格最多
    private String pageId;
    private String from_page;
    private String from_page_id;


    public NearbyNewFaceView(Context context) {
        this(context, null);
    }

    public NearbyNewFaceView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NearbyNewFaceView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
        setListener();
    }

    private void init() {
        inflate(mContext, R.layout.nearby_moment_view, this);
        recyclerView = findViewById(R.id.recyclerview);
        more_pig_rl = findViewById(R.id.more_pig_rl);
        manager = new StaggeredGridLayoutManager(3, VERTICAL);
        recyclerView.setLayoutManager(manager);
        recyclerView.setHasFixedSize(true);
        adapter = new NearbyUserRecyclerViewAdapter(momentsList, false);

        recyclerView.setAdapter(adapter);
    }

    public NearbyNewFaceView initView(List<NearUserBean> beanList, boolean hasMoreData, String pageId, String from_page, String from_page_id) {
        if (beanList == null) {
            return this;
        }
        list.clear();
        list.addAll(beanList);
        momentsList.clear();
        final int size = list.size();
        if (size < NINE_PALACES_COUNT) {
            momentsList.addAll(list);
        } else {
            for (int i = 0; i < NINE_PALACES_COUNT; i++) {
                momentsList.add(list.get(i));
            }
        }
        if (hasMoreData) more_pig_rl.setVisibility(VISIBLE);
        else more_pig_rl.setVisibility(GONE);
        adapter.setNewData(momentsList);
        adapter.setLogDatas(pageId, from_page, from_page_id, "new");
        this.pageId = pageId;
        this.from_page = from_page;
        this.from_page_id = from_page_id;
        return this;
    }

    public List<NearUserBean> getAdapterData() {
        return adapter.getData();
    }

    public void addDatas(List<NearUserBean> data, boolean hasMoreData, String pageId, String from_page, String from_page_id) {
        for (int i = 0; i < data.size(); i++) {
            getAdapterData().add(data.get(i));
        }
        if (hasMoreData) more_pig_rl.setVisibility(VISIBLE);
        else more_pig_rl.setVisibility(GONE);
        adapter.notifyDataSetChanged();
        this.pageId = pageId;
        this.from_page = from_page;
        this.from_page_id = from_page_id;
    }


    /**
     * 过滤新添加的黑名单
     *
     * @param userId
     */
    public void filterNewBlack(String userId) {
        if (TextUtils.isEmpty(userId) || userId.equals("0")) {
            return;
        }
        int index = 0;
        final Iterator<NearUserBean> iter = adapter.getData().iterator();
        while (iter.hasNext()) {//adapter中移除
            final NearUserBean momentsBean = iter.next();
            if (userId.equals(momentsBean.userId + "")) {
                iter.remove();
                adapter.notifyItemRemoved(index);
                index -= 1;
            }
            index += 1;
        }
        final Iterator<NearUserBean> iter1 = list.iterator();
        while (iter1.hasNext()) {//原列表移除
            final NearUserBean momentsBean = iter1.next();
            if (userId.equals(momentsBean.userId + "")) {
                iter1.remove();
            }
        }
        final int listSize = list.size();
        final int aSize = adapter.getData().size();
        for (int i = aSize; i < NINE_PALACES_COUNT && i < listSize; i++) {
            adapter.getData().add(list.get(i));
        }
        adapter.notifyDataSetChanged();
    }

    private void setListener() {
        adapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                GrowingIO.getInstance().track("NewUserCell");
                gotoUserInfoActivity(position);
            }
        });

    }

    public void traceNearbyNewFaceleLog(String activity, int index, String rank_id, String userid, String sort) {
        try {
            String latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
            String longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");

            LogInfoBean logInfoBean = new LogInfoBean();
            logInfoBean.page = "around.list";
            logInfoBean.page_id = pageId;
            logInfoBean.activity = activity;
            logInfoBean.from_page_id = from_page_id;
            logInfoBean.from_page = from_page;
            logInfoBean.lat = latitude;
            logInfoBean.lng = longitude;

            LogInfoBean.LogsDataBean logsDataBean = new LogInfoBean.LogsDataBean();

            if (index != -1) {
                logsDataBean.index = index;
            }

            if (!TextUtils.isEmpty(rank_id)) {
                logsDataBean.rank_id = rank_id;
            }

            if (!TextUtils.isEmpty(userid)) {
                logsDataBean.user_id = userid;
            }
            logsDataBean.sort = sort;
            logInfoBean.data = logsDataBean;

            LiveLogUtils.getInstance().addLog(logInfoBean);

        } catch (Exception e) {

        }
    }


    private void gotoUserInfoActivity(int position) {
        try {

            final NearUserBean tempUser = adapter.getItem(position);
            int tempUserId = tempUser.userId;
//            Intent intent = new Intent();
//            intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, tempUserId + "");
//            intent.setClass(mContext, UserInfoActivity.class);
//            getContext().startActivity(intent);
            FlutterRouterConfig.Companion.gotoUserInfo(tempUserId+"");
            traceNearbyNewFaceleLog("click", position, tempUser.rank_id, tempUser.userId + "", "new");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void gotoNewFaceActivitiy() {
        Intent intent = new Intent(mContext, NearbyNewFaceActivity.class);
        mContext.startActivity(intent);
    }
}
