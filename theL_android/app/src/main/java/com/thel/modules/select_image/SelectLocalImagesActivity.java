package com.thel.modules.select_image;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.tbruyelle.rxpermissions2.RxPermissions;
import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.RfSBridgeHandlerFactory;
import com.thel.modules.corp_image.CropImage2Activity;
import com.thel.ui.LoadingDialogImpl;
import com.thel.utils.DeviceUtils;
import com.thel.utils.ExecutorServiceUtils;
import com.thel.utils.ImageUtils;
import com.thel.utils.L;
import com.thel.utils.PermissionUtil;
import com.thel.utils.PhoneUtils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.functions.Consumer;
import me.rela.rf_s_bridge.router.UniversalRouter;

public class SelectLocalImagesActivity extends BaseActivity implements CursorImageThread.CursorImageListener {

    private SelectLocalDirsFragment selectLocalDirsFragment;

    @OnClick(R.id.cancel)
    void cancel() {
        if (direct_release) {
            setResult(TheLConstants.RESULT_CODE_CANCEL_RELEASE_MOMENT);
        }
        finish();
    }

    @OnClick(R.id.txt_title)
    void folder(TextView v) {
        if (selectLocalDirsFragment == null) {
            selectLocalDirsFragment = new SelectLocalDirsFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("folder", (Serializable) mImageFolders);
            selectLocalDirsFragment.setArguments(bundle);
            selectLocalDirsFragment.setSelectLocalDirsListener(new SelectLocalDirsFragment.SelectLocalDirsListener() {
                @Override
                public void onSelectDir(int position) {
                    curDirIndex = position;
                    if (curDirIndex != 0) {
                        mImgDir = new File(mImageFolders.get(curDirIndex).getDir());
                    }
                    refreshImages();
                    getSupportFragmentManager().beginTransaction().remove(selectLocalDirsFragment).commit();
                    Drawable drawable = getResources().getDrawable(R.drawable.release_addpage_nav_btn_open_nor);
                    drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
                    v.setCompoundDrawables(null, null, drawable, null);
                }

                @Override
                public void onDismiss() {
                    getSupportFragmentManager().beginTransaction().remove(selectLocalDirsFragment).commit();
                    Drawable drawable = getResources().getDrawable(R.drawable.release_addpage_nav_btn_open_nor);
                    drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
                    v.setCompoundDrawables(null, null, drawable, null);
                }
            });
        }
        Drawable drawable = getResources().getDrawable(!selectLocalDirsFragment.isAdded() ? R.drawable.release_addpage_nav_btn_open_sel : R.drawable.release_addpage_nav_btn_open_nor);
        drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
        v.setCompoundDrawables(null, null, drawable, null);
        if (selectLocalDirsFragment.isAdded()) {
            getSupportFragmentManager().beginTransaction().remove(selectLocalDirsFragment).commit();
        } else {
            getSupportFragmentManager().beginTransaction().add(R.id.folder_container, selectLocalDirsFragment).commit();
        }
    }

    @OnClick(R.id.preview)
    void preview(View view) {
        ViewUtils.preventViewMultipleClick(view, 1000);
        Intent intent = new Intent(SelectLocalImagesActivity.this, SelectLocalImagesPreviewActivity.class);
        intent.putParcelableArrayListExtra(TheLConstants.BUNDLE_KEY_PHOTOS, mSelectedImgs);
        startActivityForResult(intent, REQUEST_CODE_PREVIEW);
    }

    @OnClick(R.id.complete)
    void complete() {
        showLoadingNoBack();
        ExecutorServiceUtils.getInstatnce().exec(() -> {

            StringBuilder urls = new StringBuilder();
            for (ImageBean imageBean : mSelectedImgs) {
                String url = imageBean.imageName;
                //修正图片方向
                String tempUrl = DeviceUtils.amendRotatePhoto(url);
                urls.append(tempUrl);
                urls.append(",");
            }
            if (urls.length() > 0) {
                urls.delete(urls.length() - 1, urls.length());
            }

            Intent intent = new Intent();
            intent.putExtra(TheLConstants.BUNDLE_KEY_LOCAL_IMAGE_PATH, urls.toString());
            intent.putExtra(TheLConstants.BUNDLE_KEY_IMAGE_OUTPUT_PATH, urls.toString());
            intent.putParcelableArrayListExtra(TheLConstants.BUNDLE_KEY_IMAGE_OUTPUT_INFO, mSelectedImgs);
            intent.putExtra(TheLConstants.BUNDLE_KEY_SELECT_AMOUNT, selectAmountLimit);
            setResult(TheLConstants.RESULT_CODE_SELECT_LOCAL_IMAGE, intent);

            SelectLocalImagesActivity.this.finish();

        });

    }

    @BindView(R.id.id_gridView)
    GridView gridView;

    public static final int REQUEST_CODE_PREVIEW = 10003;
    public static final int RESULT_CODE_PREVIEW = 10004;

    private SelectLocalImagesGridAdapter imagesGridAdapter;

    private int selectAmountLimit = 1;

    // 控制如果不选择照片或音乐，则直接关闭发布日志页面
    private boolean direct_release = false;

    // 如果外部有传入输出的图片名称，则用这个名称
    private String photoName;

    // 拍照保存的文件名
    private String localTempImgFileName;

    /**
     * 数据加载dialog
     */
    private LoadingDialogImpl loadingDialog;

    /**
     * 图片数量最多的文件夹
     */
    private File mImgDir;
    /**
     * 当前选择的文件夹的序列
     */
    private int curDirIndex = 0;

    /**
     * 所有的图片文件名
     */
    private ArrayList<ImageBean> mAllImgs = new ArrayList<>();

    /**
     * 已选择的图片的url
     */
    private ArrayList<ImageBean> mSelectedImgs = new ArrayList<>();

    /**
     * 扫描拿到所有的图片文件夹
     */
    private List<ImageFolderBean> mImageFolders = new ArrayList<>();

    private boolean isAvatar = false;

    private boolean isCover = false;

    public ArrayList<ImageBean> getmSelectedImgs() {
        return mSelectedImgs;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_select_local_images2);

        ButterKnife.bind(this);

        isAvatar = getIntent().getBooleanExtra("isAvatar", false);

        isCover = getIntent().getBooleanExtra("isCover", false);


        ArrayList<ImageBean> initImageList = getIntent().getParcelableArrayListExtra(TheLConstants.BUNDLE_KEY_PHOTO_INFO_LIST);

        if (initImageList != null) {
            mSelectedImgs.addAll(initImageList);
        }

        L.d("localImage", "mSelectedImgs: " + mSelectedImgs.toString());

        selectAmountLimit = getIntent().getIntExtra(TheLConstants.BUNDLE_KEY_SELECT_AMOUNT, 1);

        photoName = getIntent().getStringExtra(TheLConstants.BUNDLE_KEY_PHOTO_NAME);

        direct_release = getIntent().getBooleanExtra("direct_release", false);

        loadingDialog = LoadingDialogImpl.LoadingDialogInstance.getInstance(SelectLocalImagesActivity.this);

        initGridView();

        getImages();
    }

    private void initGridView() {
        imagesGridAdapter = new SelectLocalImagesGridAdapter(this, selectAmountLimit);
        gridView.setAdapter(imagesGridAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                ViewUtils.preventViewMultipleClick(gridView, 3000);
                if (position == 0) {
                    takePhoto();
                } else {
                    final ImageBean imageBean = mAllImgs.get(position);
                    if (isAvatar || isCover) {
                        Intent intent = new Intent(SelectLocalImagesActivity.this, CropImage2Activity.class);
                        intent.putExtra("isCover", isCover);
                        intent.putExtra("isAvatar", isAvatar);
                        intent.putExtra(TheLConstants.BUNDLE_KEY_LOCAL_IMAGE_PATH, imageBean.imageName);
                        startActivityForResult(intent, DeviceUtils.CUT_PHOTO);
                    } else {
                        boolean itemSelect = false;
                        for (int i = 0; i < mSelectedImgs.size(); i++) {
                            if (mSelectedImgs.get(i).imageName.equals(imageBean.imageName)) {
                                itemSelect = true;
                                break;
                            }
                        }
                        if (mSelectedImgs.size() < selectAmountLimit || itemSelect) {

                            boolean select = false;
                            for (int i = 0; i < mSelectedImgs.size(); i++) {
                                if (mSelectedImgs.get(i).imageName.equals(imageBean.imageName)) {
                                    select = true;
                                    break;
                                }
                            }
                            if (select) {
                                mSelectedImgs.remove(imageBean);
                            } else {
                                mSelectedImgs.add(imageBean);
                            }
                            setViewStatus();
                            imagesGridAdapter.notifyDataSetChanged();
                        }
                    }
                }
            }
        });
    }

    private void takePhoto() {
        PermissionUtil.requestCameraPermission(this, new PermissionUtil.PermissionCallback() {
            @Override
            public void granted() {
                if (TextUtils.isEmpty(photoName)) {
                    localTempImgFileName = ImageUtils.getPicName();
                } else {
                    localTempImgFileName = photoName;
                }
                DeviceUtils.captureImage(SelectLocalImagesActivity.this, localTempImgFileName);
            }

            @Override
            public void denied() {

            }

            @Override
            public void gotoSetting() {

            }
        });
    }

    /**
     * 利用ContentProvider扫描手机中的图片，此方法在运行在子线程中 完成图片的扫描，最终获得jpg最多的那个文件夹
     */
    private void getImages() {
        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            Toast.makeText(this, getString(R.string.info_insert_sdcard), Toast.LENGTH_SHORT).show();
            return;
        }

        loadingDialog.showLoading();
        PermissionUtil.requestStoragePermission(this, new PermissionUtil.PermissionCallback() {
            @Override
            public void granted() {
                new CursorImageThread(SelectLocalImagesActivity.this, SelectLocalImagesActivity.this).start();
            }

            @Override
            public void denied() {

            }

            @Override
            public void gotoSetting() {

            }
        });
    }

    private void refreshImages() {
        mSelectedImgs.clear();
        mAllImgs.clear();
        if (mImgDir != null) {
            List<File> files = Arrays.asList(mImgDir.listFiles());
            // 按时间排序
            Collections.sort(files, (lhs, rhs) -> {
                if (lhs.lastModified() > rhs.lastModified()) {
                    return -1;
                } else if (lhs.lastModified() < rhs.lastModified()) {
                    return 1;
                } else {
                    return 0;
                }
            });

            for (File file : files) {
                if (file.getName().endsWith(".png") || file.getName().endsWith(".jpg") || file.getName().endsWith(".jpeg")) {
                    ImageBean imageBean = new ImageBean();
                    imageBean.imageName = mImgDir.getAbsolutePath() + File.separator + file.getName();
                    mAllImgs.add(imageBean);
                }
            }
        }

        //第一个默认相机
        mAllImgs.add(0, new ImageBean());

        imagesGridAdapter.setData(mAllImgs);
    }

    private void setViewStatus() {
        TextView preview = findViewById(video.com.relavideolibrary.R.id.preview);
        TextView complete = findViewById(video.com.relavideolibrary.R.id.complete);
        preview.setAlpha(mSelectedImgs.size() == 0 ? 0.5f : 1f);
        complete.setAlpha(mSelectedImgs.size() == 0 ? 0.5f : 1f);
        preview.setEnabled(mSelectedImgs.size() != 0);
        complete.setEnabled(mSelectedImgs.size() != 0);
        if (mSelectedImgs.size() == 0) {
            complete.setText(R.string.complete);
        } else {
            complete.setText(getString(R.string.complete) + "(" + mSelectedImgs.size() + ")");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(this.getClass().getName());
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(this.getClass().getName());
        MobclickAgent.onPause(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        L.d("拿资源onActivityResult", " requestCode " + requestCode);

        L.d("拿资源onActivityResult", " resultCode " + resultCode);

        if (requestCode == REQUEST_CODE_PREVIEW && resultCode == RESULT_CODE_PREVIEW) {
            mSelectedImgs = data.getParcelableArrayListExtra(TheLConstants.BUNDLE_KEY_PHOTOS);
            complete();
        } else if (requestCode == DeviceUtils.TAKE_PHOTO && resultCode == Activity.RESULT_OK) {
            String path = TheLConstants.F_TAKE_PHOTO_ROOTPATH + localTempImgFileName;
            if (!TextUtils.isEmpty(path)) {
                //修正图片方向
                String tempPath = DeviceUtils.amendRotatePhoto(path);
                if (isAvatar || isCover) {
                    Intent intent = new Intent(this, CropImage2Activity.class);
                    intent.putExtra("isCover", isCover);
                    intent.putExtra("isAvatar", isAvatar);
                    intent.putExtra(TheLConstants.BUNDLE_KEY_LOCAL_IMAGE_PATH, tempPath);
                    startActivityForResult(intent, DeviceUtils.CUT_PHOTO);
                } else {
                    ImageBean imageBean = new ImageBean();
                    imageBean.imageName = tempPath;
                    mSelectedImgs.add(imageBean);
                    complete();
                }
            } else {
                Toast.makeText(this, getString(R.string.info_rechoise_photo), Toast.LENGTH_SHORT).show();
            }

        } else if (resultCode == TheLConstants.RESULT_CODE_WRITE_MOMENT_DELETE_PICTURE) {
//            if (data != null && imagesGridAdapter != null) {
//                mSelectedImgs.clear();
//                ArrayList<String> photoUrls = data.getStringArrayListExtra(TheLConstants.BUNDLE_KEY_INDEX);
//                mSelectedImgs.addAll(photoUrls);
//                imagesGridAdapter.notifyDataSetChanged();
//            }
        } else if (resultCode == Activity.RESULT_OK && requestCode == DeviceUtils.CUT_PHOTO) {

            if (data != null) {
                String imageUrl = data.getStringExtra(TheLConstants.BUNDLE_KEY_LOCAL_IMAGE_PATH);
                Intent intent = new Intent();
                intent.putExtra(TheLConstants.BUNDLE_KEY_LOCAL_IMAGE_PATH, imageUrl);
                setResult(DeviceUtils.CUT_PHOTO, intent);
            }

            SelectLocalImagesActivity.this.finish();
        }
    }

    @Override
    public void finish() {
        if (loadingDialog != null) {
            loadingDialog.destroyDialog();
        }
        loadingDialog = null;
        super.finish();
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (direct_release) {
                setResult(TheLConstants.RESULT_CODE_CANCEL_RELEASE_MOMENT);
            }
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void cursorImageComplete(List<ImageBean> allPhotos, List<ImageFolderBean> mImageFolders) {
        if (loadingDialog != null) {
            loadingDialog.closeDialog();

        }
        this.mAllImgs = (ArrayList<ImageBean>) allPhotos;

        this.mImageFolders = mImageFolders;

        //第一个默认相机
        mAllImgs.add(0, new ImageBean());

        imagesGridAdapter.setData(mAllImgs);

        setViewStatus();
    }

    @Override
    public void cursorImageFailed() {
        loadingDialog.closeDialog();
    }
}
