package com.thel.modules.video;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Rect;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.thel.R;
import com.thel.base.BaseFragment;
import com.thel.bean.video.VideoBean;
import com.thel.bean.video.VideoListBean;
import com.thel.bean.video.VideoListBeanNew;
import com.thel.constants.TheLConstants;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.service.DefaultRequestService;
import com.thel.ui.widget.CommonTitleBar;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;
import com.thel.utils.AppInit;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by the L on 2017/2/9.
 */
public class UserVideoListFragment extends BaseFragment {
    private String userId;
    private RecyclerView recyclerView;
    private StaggeredGridLayoutManager manager;
    private VideoListAdapter adapter;
    private List<VideoBean> videoList = new ArrayList<>();
    private int dividerWidth;
    private int divederW;
    private int limitSize = 20;
    private long curPage = 0;
    private boolean haveNextPage;
    private boolean isFirstPage = true;
    private CanScollCallBack canScrollBack;
    boolean canScroll = false;
    private String mVideoId;
    private RefreshDataReceiver receiver;
    private CommonTitleBar commonTitleBar;

    public static Fragment newInstance(String tag, Bundle bundle) {
        Fragment fragment = new UserVideoListFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Bundle bundle = getArguments();
        userId = bundle.getString(TheLConstants.BUNDLE_KEY_USER_ID);
        mVideoId = bundle.getString(TheLConstants.BUNDLE_KEY_MOMENT_ID);
        dividerWidth = 4;
        divederW = dividerWidth / 2;
        processBusiness();
        receiver = new RefreshDataReceiver();
        if (getActivity() != null) {
            getActivity().registerReceiver(receiver, new IntentFilter(TheLConstants.BROADCAST_REFRESH_VIDEOBEAN));

        }
    }

    private void processBusiness() {

        Flowable<VideoListBeanNew> flowable = DefaultRequestService.createAllRequestService().getVideoListData(userId, String.valueOf(curPage), String.valueOf(limitSize));

        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<VideoListBeanNew>() {
            @Override
            public void onNext(VideoListBeanNew data) {
                super.onNext(data);
                refreshData(data.data);
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);

                requestFinished();
            }
        });

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video_list_layout, container, false);
        findViewById(view);
        setListener();
        return view;
    }

    private void findViewById(View view) {
        recyclerView = view.findViewById(R.id.recyclerview);
        manager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(manager);
        recyclerView.setHasFixedSize(true);
        adapter = new VideoListAdapter(videoList, dividerWidth);
        recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                final int pos = parent.getChildLayoutPosition(view);
                if (pos % 2 == 0) {
                    outRect.set(0, 0, divederW, dividerWidth);
                } else {
                    outRect.set(divederW, 0, 0, dividerWidth);
                }
            }
        });
        recyclerView.setAdapter(adapter);
        commonTitleBar = view.findViewById(R.id.commonTitleBar);
        commonTitleBar.setTitle("8 视频");
        commonTitleBar.isShowMoreBtn(false);
    }

    private void setListener() {
        adapter.setOnLoadMoreListener(new BaseRecyclerViewAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                recyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        if (haveNextPage) {
                            processBusiness();
                        } else {
                            adapter.openLoadMore(videoList.size(), false);
                            if (getActivity() != null) {
                                View view = getActivity().getLayoutInflater().inflate(R.layout.load_more_footer_layout, (ViewGroup) recyclerView.getParent(), false);
                                ((TextView) view.findViewById(R.id.text)).setText(getString(R.string.info_no_more));
                                adapter.addFooterView(view);
                            }
                        }
                    }
                });
            }
        });
        adapter.setReloadMoreListener(new BaseRecyclerViewAdapter.ReloadMoreListener() {
            @Override
            public void reloadMore() {
                recyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        adapter.removeAllFooterView();
                        adapter.openLoadMore(true);
                        processBusiness();
                    }
                });

            }
        });
        adapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(getContext(), VideoListActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt(TheLConstants.BUNDLE_KEY_POSITION, position);
                bundle.putBoolean(TheLConstants.BUNDLE_KEY_HAVE_NEXT_PAGE, false);
                bundle.putSerializable(TheLConstants.BUNDLE_KEY_VIDEO_LIST, (Serializable) videoList);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    private void refreshData(VideoListBean videoListBean) {

        L.d("VideoList", " videoListBean data : " + GsonUtils.createJsonString(videoListBean));

        haveNextPage = videoListBean.haveNextPage;
        videoList.addAll(videoListBean.list);
        curPage = videoListBean.cursor;
        if (isFirstPage) {//初次请求数据
            isFirstPage = false;
            adapter.removeAllFooterView();
            adapter.setNewData(videoList);
            if (haveNextPage) {//如果有下一页
                adapter.openLoadMore(videoList.size(), true);
            }
        } else {
            adapter.notifyDataChangedAfterLoadMore(true, videoList.size());
        }
        final int count = videoList.size();
        //        if (count > 0) {
        canScroll(true);
        //        }
        if (canScrollBack != null) {
            canScrollBack.setVideoCount(count);
            final int playCount = getPlayCountFromList(videoList, mVideoId);
            if (playCount != 0) {
                canScrollBack.updateVideoPlayCount(playCount);
            }
        }
        if (commonTitleBar != null) {
            commonTitleBar.setTitle(videoListBean.count + " 视频");
        }
    }

    private int getPlayCountFromList(List<VideoBean> videoList, String mVideoId) {
        for (VideoBean bean : videoList) {
            if (bean.id.equals(mVideoId)) {
                return bean.playCount;
            }
        }
        return 0;
    }

    class VideoListAdapter extends BaseRecyclerViewAdapter<VideoBean> {

        private final int picSize;

        public VideoListAdapter(List<VideoBean> data, int dividerWidth) {
            super(R.layout.video_list_adapter_item, data);
            picSize = (AppInit.displayMetrics.widthPixels - dividerWidth) / 2;
        }

        @Override
        protected void convert(BaseViewHolder helper, VideoBean item) {
            final ViewGroup.LayoutParams param = helper.getConvertView().getLayoutParams();
            param.height = picSize;
            helper.setImageUrl(R.id.img_preview, item.image, TheLConstants.MAX_PIC_WIDTH, TheLConstants.MAX_PIC_WIDTH);
            final String txt = item.playTime + "s / " + getString(R.string.play_count, item.playCount);
            helper.setText(R.id.txt_info, txt);
        }
    }

    private void requestFinished() {
        if (recyclerView == null) {
            return;
        }
        recyclerView.post(new Runnable() {
            @Override
            public void run() {
                adapter.loadMoreFailed((ViewGroup) recyclerView.getParent());
            }
        });
    }

    private void canScroll(boolean b) {
        if (!canScroll) {
            canScroll = b;
            if (canScroll && canScrollBack != null) {
                canScrollBack.canScroll(canScroll);
            }
        }
    }

    public interface CanScollCallBack {
        /**
         * 是否可以下拉，回调
         *
         * @param canScroll
         */
        void canScroll(boolean canScroll);

        /**
         * 显示视频数量
         *
         * @param count
         */
        void setVideoCount(int count);

        /**
         * 更新点击进来的视频的播放次数
         *
         * @param playCount
         */
        void updateVideoPlayCount(int playCount);
    }

    public void setCanScrollCallBack(CanScollCallBack canScrollCallBack) {
        this.canScrollBack = canScrollCallBack;
    }

    class RefreshDataReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (TheLConstants.BROADCAST_REFRESH_VIDEOBEAN.equals(intent.getAction())) {
                if (intent.getSerializableExtra(TheLConstants.BUNDLE_KEY_VIDEO_BEAN) != null) {
                    final VideoBean bean = (VideoBean) intent.getSerializableExtra(TheLConstants.BUNDLE_KEY_VIDEO_BEAN);
                    final int size = adapter.getData().size();
                    final int headCount = adapter.getHeaderLayoutCount();
                    for (int i = 0; i < size; i++) {
                        final VideoBean b = adapter.getData().get(i);
                        if (b.id.equals(bean.id)) {
                            b.winkFlag = bean.winkFlag;
                            b.winkNum = bean.winkNum;
                            adapter.notifyItemChanged(i + headCount);
                            return;
                        }
                    }
                }

            }
        }
    }


    @Override
    public void onDestroy() {
        if (receiver != null && getActivity() != null) {
            try {
                getActivity().unregisterReceiver(receiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        super.onDestroy();
    }
}
