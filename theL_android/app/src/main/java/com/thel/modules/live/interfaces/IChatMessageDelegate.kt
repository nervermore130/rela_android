package com.thel.modules.live.interfaces

import com.thel.modules.live.bean.LiveRoomBean
import com.thel.modules.live.surface.watch.LiveWatchObserver

interface IChatMessageDelegate {

    fun init(observer: LiveWatchObserver, liveRoomBean: LiveRoomBean)

    fun messageDelegate(code: String, payload: String)

}