package com.thel.modules.main.me.bean;

import com.thel.base.BaseDataBean;

import java.io.Serializable;

/**
 * Base Tag bean
 *
 * @author lingwei
 */
public class BaseTagBean extends BaseDataBean implements Serializable {

    /**
     * 话题id
     */
    public String topicId;

    /**
     * 话题名称
     */
    public String topicName;

    /**
     * 话题背景色
     */
    public String topicColor;



}
