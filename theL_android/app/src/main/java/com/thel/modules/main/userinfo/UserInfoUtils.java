package com.thel.modules.main.userinfo;

import android.animation.Animator;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.ContactBean;
import com.thel.bean.user.UserCardBean;
import com.thel.bean.user.UserInfoBean;
import com.thel.constants.TheLConstants;
import com.thel.growingio.GrowingIoConstant;
import com.thel.imp.wink.WinkUtils;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.main.home.moments.ReportActivity;
import com.thel.modules.main.home.moments.SendCardActivity;
import com.thel.modules.main.home.moments.web.WebViewActivity;
import com.thel.modules.main.me.aboutMe.GiveVipActivity;
import com.thel.modules.main.me.aboutMe.UpdateUserInfoActivity;
import com.thel.modules.main.userinfo.recommandcard.RecommendCardActivity;
import com.thel.modules.others.VipConfigActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.ui.dialog.ReleaseMomentDialog;
import com.thel.utils.AppInit;
import com.thel.utils.DeviceUtils;
import com.thel.utils.DialogUtil;
import com.thel.utils.L;
import com.thel.utils.SharedPrefUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Flowable;

import static com.thel.modules.main.home.moments.SendCardActivity.FROM_USER_INFO;
import static com.thel.utils.StringUtils.getString;

/**
 * Created by waiarl on 2017/10/17.
 */

public class UserInfoUtils {
    private static final long smi = 1000;
    private static final long mmi = smi * 60;
    private static final long hmi = mmi * 60;

    /**
     * 个人页面右侧个人资料 拼接信息
     *
     * @param intro
     * @param content
     * @return
     */
    public static SpannableString getUserInfoSpannaString(final String intro, final String content) {
        if (TextUtils.isEmpty(content)) {
            return new SpannableString("");
        }
        final String st = intro + "" + content;
        final int start1 = st.indexOf(intro);
        final int end1 = start1 + intro.length();
        SpannableString sp = new SpannableString(st);
        sp.setSpan(new StyleSpan(Typeface.BOLD), start1, end1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return sp;
    }

    /**
     * 设置顶部图标
     *
     * @param userInfoBean
     * @param linearLayout
     */
    public static void setTitleIcons(UserInfoBean userInfoBean, LinearLayout linearLayout) {
        linearLayout.removeAllViews();
        if (userInfoBean.level > 0) {// vip信息
            final ImageView imageView = drawIconImageView((int) (TheLApp.getContext().getResources().getDimension(R.dimen.verify_vip_size)));
            switch (userInfoBean.level) {
                case 1:
                    imageView.setImageResource(R.mipmap.icn_vip_1);
                    break;
                case 2:
                    imageView.setImageResource(R.mipmap.icn_vip_2);
                    break;
                case 3:
                    imageView.setImageResource(R.mipmap.icn_vip_3);
                    break;
                case 4:
                    imageView.setImageResource(R.mipmap.icn_vip_4);
                    break;
            }
            linearLayout.addView(imageView);
        }
    }

    /**
     * 设置认证信息图标
     *
     * @param userInfoBean
     * @param linearLayout
     */
    public static void setAuthIcons(UserInfoBean userInfoBean, LinearLayout linearLayout) {
        linearLayout.removeAllViews();
        if (userInfoBean.verifyType >= 1) {// 加V用户，显示认证信息
            final ImageView imageView = drawIconImageView((int) (TheLApp.getContext().getResources().getDimension(R.dimen.verify_icon_size_big)));
            if (userInfoBean.verifyType == 1) {
                imageView.setImageResource(R.mipmap.icn_verify_person);
            } else {
                imageView.setImageResource(R.mipmap.icn_verify_enterprise);
            }
            linearLayout.addView(imageView);
        }
    }

    private static ImageView drawIconImageView(int size) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(size, size);
        params.setMargins(0, 0, Utils.dip2px(TheLApp.getContext(), 2), 0);
        ImageView imageView = new ImageView(TheLApp.getContext());
        imageView.setLayoutParams(params);
        return imageView;
    }

    /**
     * 完善个人信息对话框
     *
     * @param activity
     */
    public static void showMustUpateUserInfoDialog(final Activity activity) {
        if (activity == null) {
            return;
        }
        final Dialog dialog = new Dialog(activity, R.style.CustomDialog);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.gravity = Gravity.CENTER;
        lp.width = AppInit.displayMetrics.widthPixels;
        lp.height = AppInit.displayMetrics.heightPixels;
        dialog.getWindow().setAttributes(lp);

        LayoutInflater flater = LayoutInflater.from(TheLApp.getContext());
        View view = flater.inflate(R.layout.userinfo_must_update_user_info_dialog, null);
        dialog.setContentView(view);

        TextView txt_ok = view.findViewById(R.id.txt_ok);
        TextView txt_maybe_next_time = view.findViewById(R.id.txt_maybe_next_time);

        // 点击事件
        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                dialog.dismiss();
                Intent intent = new Intent(activity, UpdateUserInfoActivity.class);
                Bundle bundle = new Bundle();
                /*final MyInfoBean userBean = ShareFileUtils.getUserData();
                bundle.putSerializable("my_info", userBean);*/
                intent.putExtras(bundle);
                activity.startActivity(intent);
            }
        });

        txt_maybe_next_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                dialog.dismiss();
            }
        });

    }

    /**
     * 更多
     *
     * @param activity
     * @param presenter
     */
    public static void showMoreDialog(final Activity activity, final UserInfoContract.Presenter presenter, final UserInfoBean userInfoBean) {
        if (userInfoBean == null) {
            return;
        }
        final Dialog dialog = new Dialog(activity, R.style.CustomDialogFullscreen);
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();

        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.gravity = Gravity.CENTER;
        lp.width = AppInit.displayMetrics.widthPixels;
        lp.height = AppInit.displayMetrics.heightPixels;
        dialog.getWindow().setAttributes(lp);

        LayoutInflater flater = LayoutInflater.from(TheLApp.getContext());
        View view = flater.inflate(R.layout.userinfo_more_dialog, null);
        dialog.setContentView(view);
        LinearLayout report = view.findViewById(R.id.report);
        LinearLayout block = view.findViewById(R.id.block);
        LinearLayout send_hidden_love = view.findViewById(R.id.send_hidden_love);
        LinearLayout send_vip = view.findViewById(R.id.send_vip);
        TextView txt_report = view.findViewById(R.id.txt_report);
        View btnReport = view.findViewById(R.id.report_img);
        View btnBlock = view.findViewById(R.id.block_img);
        View btnHiddenLove = view.findViewById(R.id.send_hidden_love_img);
        ImageView img_vip_new = view.findViewById(R.id.img_vip_new);
        LinearLayout lin_secretly_follow = view.findViewById(R.id.lin_secretly_follow);//悄悄关注
        View img_secretly_follow = view.findViewById(R.id.secretly_follow_img);//悄悄关注图标
        ImageView img_secretly_follow_new = view.findViewById(R.id.img_secretly_follow_new);//悄悄关注new图标
/*
        if (DeviceUtils.getVersionCode(TheLApp.getContext()) == SharedPrefUtils.getInt(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.CURRENT_VERSION1, -1)) {
            img_vip_new.setVisibility(View.INVISIBLE);
            img_secretly_follow_new.setVisibility(View.INVISIBLE);
        } else {
            img_vip_new.setVisibility(View.VISIBLE);
            img_secretly_follow_new.setVisibility(View.VISIBLE);
        }*/

        if (userInfoBean.isBlack.equals("1")) {// 是否屏蔽了
            btnBlock.setSelected(true);
        } else {
            btnBlock.setSelected(false);
        }

        if (userInfoBean.isHiddenLove.equals("1") || userInfoBean.isHiddenLove.equals("3")) {// 是否暗恋了
            btnHiddenLove.setSelected(true);
        } else {
            btnHiddenLove.setSelected(false);
        }

        // 检查是否本地存储的举报数据中有该用户
        boolean hasReportedHer = false;
        final String[] reportUserList = SharedPrefUtils.getString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.REPORT_USER_LIST, "").split(",");
        for (String id : reportUserList) {
            if (userInfoBean.id.equals(id)) {
                hasReportedHer = true;
                break;
            }
        }

        if (hasReportedHer) { // 是否举报了
            txt_report.setText(TheLApp.getContext().getString(R.string.userinfo_activity_report_success));
            btnReport.setSelected(true);
        } else {
            txt_report.setText(TheLApp.getContext().getString(R.string.userinfo_activity_dialog_report_title));
            btnReport.setSelected(false);
        }
        if (userInfoBean.secretly > 0) {//1表示已经悄悄关注，0表示没有
            //设置图标
            img_secretly_follow.setSelected(true);
        } else {//没被悄悄关注
            //设置图标
            img_secretly_follow.setSelected(false);
        }

        // 点击事件
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        final boolean finalHasReportedHer = hasReportedHer;
        report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                if (finalHasReportedHer) { // 是否举报了
                    return;
                } else {
                    dialog.dismiss();
                    gotoReportActivity(activity, userInfoBean.id);
                }

            }
        });

        block.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);

                L.d("UserInfoPresenter", " userInfoBean.isBlack : " + userInfoBean.isBlack);

                L.d("UserInfoPresenter", " presenter : " + presenter);

                L.d("UserInfoPresenter", " userInfoBean.id : " + userInfoBean.id);

                if (!userInfoBean.isBlack.equals("1")) {
                    DialogUtil.showConfirmDialog(activity, "", TheLApp.getContext().getString(R.string.userinfo_activity_dialog_block), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            presenter.showLoading();
                            presenter.blackHer(userInfoBean.id);
                        }
                    });
                    dialog.dismiss();
                }
            }
        });

        send_hidden_love.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                if (userInfoBean.isBlack.equals("0") || TextUtils.isEmpty(userInfoBean.isBlack)) {
                    DialogUtil.showConfirmDialog(activity, "", TheLApp.getContext().getString(R.string.userinfo_activity_dialog_love), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            presenter.showLoading();
                            presenter.sendHiddenLove(userInfoBean.id);
                        }
                    });
                } else if (userInfoBean.isHiddenLove.equals("1")) {
                    DialogUtil.showConfirmDialog(activity, "", TheLApp.getContext().getString(R.string.userinfo_activity_dialog_cancel_love), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            presenter.showLoading();
                            presenter.sendHiddenLove(userInfoBean.id);
                        }
                    });
                } else if (userInfoBean.isHiddenLove.equals("3")) {
                    DialogUtil.showToastShort(activity, TheLApp.getContext().getString(R.string.userinfo_activity_dialog_cant_cancel_love));
                }

                dialog.dismiss();
            }
        });

        send_vip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                // img_new.setVisibility(View.INVISIBLE);
                gotoSendGift(activity, userInfoBean);//打开赠送会员界面
                dialog.dismiss();
            }
        });
        lin_secretly_follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                if (UserUtils.getUserVipLevel() <= 0) {//如果不是会员
                    MobclickAgent.onEvent(TheLApp.getContext(), "check_secretly_follow");
                    gotoBuyVip(activity);//跳转购买会员界面
                } else if (userInfoBean.followStatus == 1 || userInfoBean.followStatus == 3) {//已经关注对方||互相关注,则无法悄悄关注
                    DialogUtil.showAlert(activity, "", getString(R.string.have_follow_cannot_secretly_follow), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                } else if (userInfoBean.secretly > 0) {//大于0，表示已悄悄关注,点击取消悄悄关注
                    secretlyFollow(activity, false, userInfoBean.id, presenter);//取消悄悄关注
                } else {//没有悄悄关注
                    secretlyFollow(activity, true, userInfoBean.id, presenter);//悄悄关注
                }
                dialog.dismiss();
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                SharedPrefUtils.setInt(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.CURRENT_VERSION1, DeviceUtils.getVersionCode(TheLApp.getContext()));
                // dismissImgNew();//表示new的红点小时
            }
        });
    }

    /**
     * 举报用户
     *
     * @param activity
     * @param userId
     */
    private static void gotoReportActivity(Activity activity, String userId) {
        Intent intent = new Intent(activity, ReportActivity.class);
        intent.putExtra(TheLConstants.BUNDLE_KEY_REPORT_TYPE, ReportActivity.REPORT_TYPE_REPORT_USER);
        intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, userId);
        activity.startActivity(intent);
    }

    /**
     * 悄悄关注
     */
    public static void secretlyFollow(Context activity, boolean secretFollow, final String userId, final UserInfoContract.Presenter presenter) {
        final int actionType = secretFollow ? 1 : 0;//
        String msg_secretly_follow = getString(R.string.msg_secretly_follow);//悄悄关注提示信息
        String msg_cancel_secretly_follow = getString(R.string.msg_cancel_secretly_follow);//取消悄悄关注提示信息
        String btn_confirm_secretly = getString(R.string.secretly_follow);//悄悄关注确定
        String btn_cancel_secretly = getString(R.string.secretly_follow_not);//取消悄悄关注确定
        DialogUtil.showConfirmDialog(activity, "", secretFollow ? msg_secretly_follow : msg_cancel_secretly_follow,//悄悄关注/取消悄悄关注 msg
                secretFollow ? btn_confirm_secretly : btn_cancel_secretly, //悄悄关注/取消悄悄关注   确定按钮
                getString(R.string.info_no),//取消按钮
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        presenter.secretlyFollow(userId, actionType);

                    }
                });
    }

    /**
     * 跳转到购买会员界面
     */
    private static void gotoBuyVip(Activity activity) {
        final Intent intent = new Intent(activity, VipConfigActivity.class);
        activity.startActivity(intent);
    }

    /**
     * 赠送礼物（赠送会员）
     *
     * @param activity
     * @param userInfoBean
     */
    private static void gotoSendGift(Context activity, UserInfoBean userInfoBean) {
        final Intent intent = new Intent(activity, GiveVipActivity.class);
        final ContactBean contactBean = getContactByUserInfo(userInfoBean);
        final Bundle bundle = new Bundle();
        bundle.putSerializable(TheLConstants.BUNDLE_KEY_CONTACT, contactBean);
        intent.putExtras(bundle);
        activity.startActivity(intent);
    }

    public static ContactBean getContactByUserInfo(UserInfoBean userInfoBean) {
        final ContactBean contactBean = new ContactBean();
        contactBean.userId = userInfoBean.id;
        contactBean.nickName = userInfoBean.nickName;
        contactBean.avatar = userInfoBean.avatar;
        contactBean.bgImage = userInfoBean.bgImage;
        return contactBean;
    }

    /**
     * 发送名片
     *
     * @param userInfoBean
     * @param userId
     */
    public static void sendCard(Context context, UserInfoBean userInfoBean, String userId, final String tAge, final String tAffection, final String tHoroscope) {
        SharedPrefUtils.setBoolean(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.REMEMBER_CARD_FIRST_CLICK, true);
        if (userInfoBean != null) {
//            Intent intent = new Intent(context, RecommendCardActivity.class);
//            intent.putExtra("toAvatar", userInfoBean.avatar);
//            intent.putExtra("bg_image", userInfoBean.bgImage);
//            intent.putExtra("toUserId", userId);
//            intent.putExtra("toName", userInfoBean.nickName);
//            intent.putExtra("affection", tAffection);
//            intent.putExtra("birthday", tAge);
//            intent.putExtra("horoscope", tHoroscope);
//            context.startActivity(intent);

            Intent intent = new Intent(context, SendCardActivity.class);
            UserCardBean userCardBean = new UserCardBean();
            Bundle bundle = new Bundle();

            userCardBean.avatar = userInfoBean.avatar;
            userCardBean.bgImage = userInfoBean.bgImage;
            userCardBean.birthday = userInfoBean.age;
            userCardBean.userId = userId;
            userCardBean.nickName = userInfoBean.nickName;
            userCardBean.horoscope = tHoroscope;
            userCardBean.affection = tAffection;
//            userCardBean.recommendDesc = content;
            bundle.putSerializable("usercardbean", userCardBean);
            intent.putExtra(TheLConstants.BUNDLE_KEY_PAGE_FROM, GrowingIoConstant.ENTRY_LONG_VIDEO);
            intent.putExtra(TheLConstants.BUNDLE_KEY_INTENT_FROM, FROM_USER_INFO);
            intent.putExtras(bundle);

            context.startActivity(intent);
        }
    }

    /**
     * 发布日志
     *
     * @param
     */
    public static void showReleaseMomentDialog(Activity activity) {
        final ReleaseMomentDialog releaseMomentDialog = new ReleaseMomentDialog(activity);
        releaseMomentDialog.show();
    }

    /**
     * 获取挤眼倒计时
     *
     * @param calendar
     * @param isGiftWink
     * @return
     */
    public static String getWinkCountDown(Calendar calendar, boolean isGiftWink) {
        final long rdu = calendar.getTimeInMillis() - System.currentTimeMillis();
        if (rdu < 10) {//10毫秒的缓冲
            return "";
        }
        final long h = rdu / hmi;
        final long m = rdu % hmi / mmi;
        final String minute = m >= 10 ? m + "" : "0" + m;
        String content;
        String n_hour = h > 1 ? getString(R.string.n_hour_even, h + "") : getString(R.string.n_hour_old, h + "");
        String n_minute = m > 1 ? getString(R.string.n_minute_even, minute) : getString(R.string.n_minute_old, minute);
        if (isGiftWink) {
            content = getString(R.string.wink_popularity_remian_time) + n_hour + n_minute;
        } else {
            content = getString(R.string.wink_remain_time) + n_hour + n_minute;
        }
        return content;
    }

    /**
     * 播放挤眼动画
     *
     * @param logType
     * @param img_sendwink
     * @param fab_wink
     */
    private static String[] ae_jsons = {"wink.json", "flower_sendout.json", "mouth_sendout.json", "heart_sendout.json", "diamond_sendout.json"};

    public static void playWinkAnim(String type, final LottieAnimationView img_sendwink, final View fab_wink) {
        final String json = getAeJsonByType(type);
        if (!TextUtils.isEmpty(json)) {
            img_sendwink.setVisibility(View.VISIBLE);
            img_sendwink.setAnimation(json);
            img_sendwink.addAnimatorListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                    fab_wink.setEnabled(false);
                    // fab_gifts.setEnabled(false);
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    img_sendwink.setVisibility(View.GONE);
                    fab_wink.setEnabled(true);
                    // fab_gifts.setEnabled(true);
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                    img_sendwink.setVisibility(View.GONE);
                    fab_wink.setEnabled(true);
                    //fab_gifts.setEnabled(true);
                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
            img_sendwink.playAnimation();
        }
    }

    /**
     * 通过type获取挤眼动画json名字
     *
     * @param type
     * @return
     */
    private static String getAeJsonByType(String type) {
        switch (type) {
            case WinkUtils.WINK_TYPE_WINK:
                return ae_jsons[0];
            case WinkUtils.WINK_TYPE_FLOWER:
                return ae_jsons[1];
            case WinkUtils.WINK_TYPE_KISS:
                return ae_jsons[2];
            case WinkUtils.WINK_TYPE_LOVE:
                return ae_jsons[3];
            case WinkUtils.WINK_TYPE_DIAMOND:
                return ae_jsons[4];
        }
        return null;
    }

    public static void showAddWinksToast(int num, boolean show) {
        if (show) {
            String content = getString(R.string.add_popularity_count_old, num);
            if (num > 1)
                content = getString(R.string.add_popularity_count_even, num);
            final SpannableString sp = new SpannableString(content);
            final int start = 0;
            final int end = content.indexOf(num + "") + (num + "").length();
            sp.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            Flowable.timer(500, TimeUnit.MILLISECONDS).onBackpressureDrop().subscribe(new InterceptorSubscribe<Long>() {
                @Override
                public void onNext(Long data) {
                    super.onNext(data);
                    DialogUtil.showMyToastShort(TheLApp.getContext(), sp, Gravity.CENTER, 0, Utils.dip2px(TheLApp.getContext(), 80));

                }
            });

        }
    }

    public static void showVerifyInfoDialog(final Context context, final UserInfoBean userInfoBean, final UserInfoContract.Presenter presenter) {
        final int type = userInfoBean.verifyType;
        final int vipLevel = userInfoBean.level;
        final Dialog dialog = new Dialog(context, R.style.CustomDialog);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.gravity = Gravity.CENTER;
        lp.width = AppInit.displayMetrics.widthPixels;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);

        LayoutInflater flater = LayoutInflater.from(context);
        View view = flater.inflate(R.layout.userinfo_verify_info_dialog, null);
        dialog.setContentView(view);

        ImageView img_avatar = view.findViewById(R.id.img_avatar);
        int w = (int) (AppInit.displayMetrics.widthPixels * 0.7);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(w, w);
        img_avatar.setLayoutParams(params);
        TextView txt_nickname = view.findViewById(R.id.txt_nickname);
        TextView txt_info = view.findViewById(R.id.txt_info);
        TextView txt_cancel = view.findViewById(R.id.txt_cancel);
        ImageView img_type = view.findViewById(R.id.img_type);
        ImageView img_vip_level = view.findViewById(R.id.img_vip_level);
        TextView txt_redirect = view.findViewById(R.id.txt_redirect);
        TextView txt_report = view.findViewById(R.id.txt_report);
        if (Utils.isMyself(userInfoBean.id)) {
            txt_report.setVisibility(View.INVISIBLE);
        }

        if (type == 0) {// 普通用户
            txt_info.setVisibility(View.GONE);
            txt_redirect.setVisibility(View.INVISIBLE);
            img_type.setVisibility(View.GONE);
        } else if (type == 1) {
            txt_info.setVisibility(View.VISIBLE);
            txt_redirect.setVisibility(View.VISIBLE);
            img_type.setImageResource(R.mipmap.icn_verify_person);
            img_type.setVisibility(View.VISIBLE);
        } else {
            txt_info.setVisibility(View.VISIBLE);
            txt_redirect.setVisibility(View.VISIBLE);
            img_type.setImageResource(R.mipmap.icn_verify_enterprise);
            img_type.setVisibility(View.VISIBLE);
        }

        if (vipLevel > 0) {
            img_vip_level.setVisibility(View.VISIBLE);
            switch (vipLevel) {
                case 1:
                    img_vip_level.setImageResource(R.mipmap.icn_vip_1);
                    break;
                case 2:
                    img_vip_level.setImageResource(R.mipmap.icn_vip_2);
                    break;
                case 3:
                    img_vip_level.setImageResource(R.mipmap.icn_vip_3);
                    break;
                case 4:
                    img_vip_level.setImageResource(R.mipmap.icn_vip_4);
                    break;
            }
        } else {
            img_vip_level.setVisibility(View.GONE);
        }

        // img_avatar.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(userInfoBean.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE)));
        ImageLoaderManager.imageLoaderDefaultCircle(img_avatar, R.mipmap.icon_user, userInfoBean.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE);
        txt_nickname.setText(userInfoBean.nickName);
        txt_info.setText(userInfoBean.verifyIntro);

        // 点击事件
        txt_redirect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                dialog.dismiss();
                Intent intent = new Intent(context, WebViewActivity.class);
                intent.putExtra(WebViewActivity.URL, TheLConstants.HOW_TO_VERIFY_PAGE_URL);
                intent.putExtra(WebViewActivity.NEED_SECURITY_CHECK, false);
                context.startActivity(intent);
            }
        });

        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                dialog.dismiss();
            }
        });
        txt_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                presenter.postReportImageOrUser(userInfoBean.id, "avatar", 0);
                //   requestBussiness.postReportImageOrUser(new DefaultNetworkHelper(UserInfoActivity.this), userId, "avatar", 0);

            }
        });
    }
}
