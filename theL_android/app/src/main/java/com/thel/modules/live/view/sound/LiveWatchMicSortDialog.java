package com.thel.modules.live.view.sound;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseDialogFragment;
import com.thel.bean.live.LiveMultiSeatBean;
import com.thel.constants.TheLConstants;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.live.utils.LiveUtils;
import com.thel.utils.AppInit;
import com.thel.utils.SizeUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;

import java.util.Collection;
import java.util.List;

import video.com.relavideolibrary.BaseRecyclerAdapter;
import video.com.relavideolibrary.BaseViewHolder;

/**
 * Created by chad
 * Time 18/6/4
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */
public class LiveWatchMicSortDialog extends BaseDialogFragment {

    private SortAdapter sortAdapter;
    private RecyclerView mic_recycler;
    private TextView mic_sort_tip1;
    private TextView cancel_sort;
    private List<LiveMultiSeatBean.CoupleDetail> data;
    private boolean isInLinkMicing;//自己是否正在跟主播连麦

    public static LiveWatchMicSortDialog newInstance(Bundle bundle) {
        LiveWatchMicSortDialog liveWatchMicSortDialog = new LiveWatchMicSortDialog();
        liveWatchMicSortDialog.setArguments(bundle);
        return liveWatchMicSortDialog;
    }

    public void setInLinkMicing(boolean inLinkMicing) {
        this.isInLinkMicing = inLinkMicing;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            data = (List<LiveMultiSeatBean.CoupleDetail>) getArguments().getSerializable("sortList");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.live_watch_mic_sort_view, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mic_recycler = view.findViewById(R.id.mic_recycler);
        mic_sort_tip1 = view.findViewById(R.id.mic_sort_tip1);
        cancel_sort = view.findViewById(R.id.cancel_sort);
        cancel_sort.setVisibility(isInLinkMicing ? View.GONE : View.VISIBLE);

        cancel_sort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (micSortListener != null) micSortListener.cancelMicSort();
            }
        });
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mic_recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        mic_recycler.setHasFixedSize(false);
        sortAdapter = new SortAdapter(R.layout.sort_list_item, mic_recycler, data);
        mic_recycler.setAdapter(sortAdapter);
        setSortMicCountText(sortAdapter.getData().size());
    }

    @Override
    public void onResume() {
        super.onResume();
        int width = AppInit.displayMetrics.widthPixels;
        int height = SizeUtils.dip2px(getContext(), 300);
        getDialog().getWindow().setLayout(width, height);
    }

    private void setSortMicCountText(int count) {
        mic_sort_tip1.setText(TheLApp.context.getString(R.string.apply_for_link_mic1, count));
    }

    public void addUser(LiveMultiSeatBean.CoupleDetail liveMultiSeatBean) {
        if (liveMultiSeatBean != null) {
            if (liveMultiSeatBean.userId.equals(UserUtils.getMyUserId())) {
                //将本人放在第一行
                sortAdapter.addItem(0, liveMultiSeatBean);
            } else {
                sortAdapter.addItem(liveMultiSeatBean);
            }
            setSortMicCountText(sortAdapter.getData().size());
        }

    }

    public void removeByUserId(String userId) {
        int count = sortAdapter.getData().size();
        for (int i = 0; i < count; i++) {
            if (sortAdapter.getData().get(i).userId.equals(userId)) {
                sortAdapter.removeData(i);
                break;
            }
        }
        setSortMicCountText(sortAdapter.getData().size());
    }

    private class SortAdapter extends BaseRecyclerAdapter<LiveMultiSeatBean.CoupleDetail> {

        public SortAdapter(int layoutId, RecyclerView recyclerView, Collection<LiveMultiSeatBean.CoupleDetail> list) {
            super(layoutId, recyclerView, list);
        }

        @Override
        public void dataBinding(final BaseViewHolder baseViewHolder, final LiveMultiSeatBean.CoupleDetail liveMultiSeatBean, int i) {

            baseViewHolder.setVisible(R.id.agree, false);
            baseViewHolder.setVisible(R.id.txt_waiting, true);

            ImageView imageView = baseViewHolder.getView(R.id.avatar);
            ImageLoaderManager.imageLoaderCircle(imageView, R.mipmap.btn_avatar, liveMultiSeatBean.avatar);
            baseViewHolder.setText(R.id.nickname, liveMultiSeatBean.nickname);
            baseViewHolder.setText(R.id.nickname, buildNickname(liveMultiSeatBean.nickname, liveMultiSeatBean.vip, liveMultiSeatBean.userLevel, liveMultiSeatBean.iconSwitch));
        }

        private SpannableString buildNickname(String nickName, int vip, int userLevel, int iconSwitch) {
            SpannableString sp = null;
            String userLevelStr1;
            String userLevelStr2 = "";
            String vipStr1;
            String vipStr2 = "";
            int start1 = 0, end1 = 0, start2 = 0, end2 = 0;

            if (userLevel >= 0 && iconSwitch == 1) {
                userLevelStr1 = userLevel + "";
                userLevelStr2 = userLevelStr1 + " ";
                end1 = start1 + userLevelStr1.length();
            }

            if (vip > 0) {
                vipStr1 = vip + "";
                vipStr2 = vipStr1 + " ";
                start2 = start1 + iconSwitch == 1 ? userLevelStr2.length() : 0;
                end2 = start2 + vipStr1.length();
            }

            sp = new SpannableString((iconSwitch == 1 ? userLevelStr2 : "") + vipStr2 + nickName);

            //会员
            final Bitmap vipBitmap = getVipBitmap(vip);
            final MyIm myVipLevelIm = new MyIm(TheLApp.getContext(), vipBitmap);//为调整文字与会员图片保持居中
            sp.setSpan(myVipLevelIm, start2, end2, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);

            if (iconSwitch == 1) {
                //用户等级
                final Bitmap userLevelBitmap = getUserLevelBitmap(userLevel);
                final MyIm myUserLevelIm = new MyIm(TheLApp.getContext(), userLevelBitmap);
                sp.setSpan(myUserLevelIm, start1, end1, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
            }

            return sp;
        }

        /**
         * 获取用户等级bitmap
         *
         * @param userLevel
         * @return
         */
        private Bitmap getUserLevelBitmap(int userLevel) {
            Bitmap bitmap;
            if (userLevel >= TheLConstants.USER_LEVEL_RES.length) {
                userLevel = TheLConstants.USER_LEVEL_RES.length - 1;
            }
            if (userLevel < TheLConstants.USER_LEVEL_RES.length && userLevel >= 0) {
                bitmap = BitmapFactory.decodeResource(TheLApp.getContext().getResources(), TheLConstants.USER_LEVEL_RES[userLevel]);
            } else {
                bitmap = BitmapFactory.decodeResource(TheLApp.getContext().getResources(), TheLConstants.USER_LEVEL_RES[0]);

            }
            final int height = (int) TheLApp.getContext().getResources().getDimension(R.dimen.live_chat_level_icon_height);
            final float width = LiveUtils.getLevelImageWidth(height, userLevel);

            if (bitmap != null) {
                bitmap = Bitmap.createScaledBitmap(bitmap, (int) width, height, true);
            }
            return bitmap;
        }

        /**
         * 根据vip等级获取相应的vip
         *
         * @param vip
         * @return
         */
        private Bitmap getVipBitmap(int vip) {
            Bitmap bitmap;
            if (vip >= TheLConstants.VIP_LEVEL_RES.length) {
                vip = TheLConstants.VIP_LEVEL_RES.length - 1;
            }
            if (vip < TheLConstants.VIP_LEVEL_RES.length && vip > 0) {
                bitmap = BitmapFactory.decodeResource(TheLApp.getContext().getResources(), TheLConstants.VIP_LEVEL_RES[vip]);
            } else {
                bitmap = BitmapFactory.decodeResource(TheLApp.getContext().getResources(), TheLConstants.VIP_LEVEL_RES[1]);
            }
            if (bitmap != null) {
                bitmap = Bitmap.createScaledBitmap(bitmap, Utils.dip2px(TheLApp.getContext(), 17), Utils.dip2px(TheLApp.getContext(), 17), true);
            }
            return bitmap;
        }

        public class MyIm extends ImageSpan {

            public MyIm(Context arg0, Bitmap arg1) {
                super(arg0, arg1);
            }

            public int getSize(Paint paint, CharSequence text, int start, int end, Paint.FontMetricsInt fm) {
                Drawable d = getDrawable();
                Rect rect = d.getBounds();
                if (fm != null) {
                    Paint.FontMetricsInt fmPaint = paint.getFontMetricsInt();
                    int fontHeight = fmPaint.bottom - fmPaint.top;
                    int drHeight = rect.bottom - rect.top;

                    int top = drHeight / 2 - fontHeight / 4;
                    int bottom = drHeight / 2 + fontHeight / 4;

                    fm.ascent = -bottom;
                    fm.top = -bottom;
                    fm.bottom = top;
                    fm.descent = top;
                }
                return rect.right;
            }

            @Override
            public void draw(Canvas canvas, CharSequence text, int start, int end, float x, int top, int y, int bottom, Paint paint) {
                Drawable b = getDrawable();
                canvas.save();
                int transY = 0;
                transY = ((bottom - top) - b.getBounds().bottom) / 2 + top;
                canvas.translate(x, transY);
                b.draw(canvas);
                canvas.restore();
            }
        }
    }

    private MicSortListener micSortListener;

    public void setMicSortListener(MicSortListener micSortListener) {
        this.micSortListener = micSortListener;
    }

    public interface MicSortListener {

        void cancelMicSort();
    }
}
