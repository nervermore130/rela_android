package com.thel.modules.live;

import android.util.SparseArray;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.modules.live.bean.LivePkAeAnimBean;
import com.thel.modules.live.bean.LiveSoundChoiceAppBean;
import com.thel.utils.StringUtils;
import com.thel.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by waiarl on 2017/10/27.
 */

public class LiveConstant {
    public static Map<Integer, String> GIFT_NUM_MEANING = new HashMap<Integer, String>() {
        {
            put(11, TheLApp.getContext().getString(R.string.lian_1));
            put(52, TheLApp.getContext().getString(R.string.lian_52));
            put(99, TheLApp.getContext().getString(R.string.lian_99));
            put(100, TheLApp.getContext().getString(R.string.lian_100));
            put(222, TheLApp.getContext().getString(R.string.lian_222));
            put(299, TheLApp.getContext().getString(R.string.lian_299));
            put(520, TheLApp.getContext().getString(R.string.lian_520));
            put(666, TheLApp.getContext().getString(R.string.lian_666));
            put(999, TheLApp.getContext().getString(R.string.lian_999));
            put(1314, TheLApp.getContext().getString(R.string.lian_1314));
            put(3344, TheLApp.getContext().getString(R.string.lian_3344));
            put(9999, TheLApp.getContext().getString(R.string.lian_9999));
        }
    };
    /**
     * 直播中好友列表的显示类型
     * 0:pk,1:连麦
     */
    public static final int TYPE_PK = 0;//pk
    public static final int TYPE_LINK_MIC = 1;//连麦
    public static final int TYPE_FRIEND_LINK_MIC = 2;//好友连麦
    public static final int TYPE_AUDIENCE_LINK_MIC = 3;//观众连麦
    public static final int TYPE_GUARD_AUDIENCE_LINK_MIC = 6;//日榜观众连麦
    public static final int TYPE_REQUEST = 4;
    public static final int TYPE_RESPONSE = 5;
    /***直播Ae动画 id,本地设置***/
    public static final int LIVE_PK_AE_ANIM_BEGIN = 1;//开始
    public static final int LIVE_PK_AE_ANIM_STAR = 2;//闪光
    public static final int LIVE_PK_AE_ANIM_MIDDLE = 3;//中场
    public static final int LIVE_PK_AE_ANIM_FIERCE = 4;//激烈
    public static final int LIVE_PK_AE_ANIM_DRAW = 5;//平局
    public static final int LIVE_PK_AE_ANIM_VICTORY = 6;//胜利
    public static final int LIVE_PK_AE_ANIM_FAILED = 7;//失败
    /***pk声音****/
    public static final int LIVE_PK_SOUND_LAST_TEN_SECOND = 8;//pk最后10秒
    /***PK最后10秒倒计时路径***/
    public static final String LIVE_PK_SOUND_LAST_TEN_SECOND_PATH = "pk_fierce_10.mp3";//pk最后10秒

    /***pk的一些ae或者音乐文件存放路径***/
    public static final String LIVE_PK_AE_PATH = "pkae/";
    public static final String LIVE_PK_SOUND_PATH = "pksound/";
    /***直播Ae动画集合***/
    public static SparseArray<LivePkAeAnimBean> livePkAeAnimArr = new SparseArray<LivePkAeAnimBean>() {
        {
            put(LIVE_PK_AE_ANIM_BEGIN, new LivePkAeAnimBean(LIVE_PK_AE_ANIM_BEGIN, 750, 224, "pk_begin_750_224_15.json", "pk_begin_sound.mp3"));
            put(LIVE_PK_AE_ANIM_STAR, new LivePkAeAnimBean(LIVE_PK_AE_ANIM_STAR, 750, 496, "pk_begin_star_750_496_14.json", "pk_begin_backgound.mp3"));
            put(LIVE_PK_AE_ANIM_MIDDLE, new LivePkAeAnimBean(LIVE_PK_AE_ANIM_MIDDLE, 400, 227, "pk_middle_400_227_30.json", "pk_middle.mp3"));
            put(LIVE_PK_AE_ANIM_FIERCE, new LivePkAeAnimBean(LIVE_PK_AE_ANIM_FIERCE, 400, 224, "pk_fierce_400_224_15.json", "pk_fierce_10.mp3"));
            put(LIVE_PK_AE_ANIM_DRAW, new LivePkAeAnimBean(LIVE_PK_AE_ANIM_DRAW, 400, 561, "pk_draw_400_561.json", "pk_draw_sound.mp3"));
            put(LIVE_PK_AE_ANIM_VICTORY, new LivePkAeAnimBean(LIVE_PK_AE_ANIM_VICTORY, 400, 541, "pk_victory_400_541.json", "pk_victory.mp3"));
            put(LIVE_PK_AE_ANIM_FAILED, new LivePkAeAnimBean(LIVE_PK_AE_ANIM_FAILED, 400, 548, "pk_faild_400_548.json", "pk_faild.mp3"));
        }
    };

    public static final int APP_WANGYI = 0;
    public static final int APP_XIAMI = 1;
    public static final int APP_QQ = 2;
    public static final int APP_KUGOU = 3;

    public static final String PACKAGE_WANGYI = "com.netease.cloudmusic";
    public static final String PACKAGE_XIAMI = "fm.xiami.main";
    public static final String PACKAGE_QQ = "com.tencent.qqmusic";
    public static final String PACKAGE_KUGOU = "com.kugou.android";

    public static List<LiveSoundChoiceAppBean> soundChoiceAppBeanList = new ArrayList<LiveSoundChoiceAppBean>() {
        {
            add(new LiveSoundChoiceAppBean(APP_WANGYI, R.mipmap.ic_wangyi, StringUtils.getString(R.string.netease_music), Utils.getColor(R.color.app_wangyi), PACKAGE_WANGYI));
            add(new LiveSoundChoiceAppBean(APP_XIAMI, R.mipmap.ic_xiami, StringUtils.getString(R.string.xiami_music), Utils.getColor(R.color.app_xiami), PACKAGE_XIAMI));
            add(new LiveSoundChoiceAppBean(APP_QQ, R.mipmap.ic_qq, StringUtils.getString(R.string.qq_music), Utils.getColor(R.color.app_qq), PACKAGE_QQ));
            add(new LiveSoundChoiceAppBean(APP_KUGOU, R.mipmap.ic_kugou, StringUtils.getString(R.string.kugou_music), Utils.getColor(R.color.app_kugou), PACKAGE_KUGOU));
        }
    };

    public static List<LiveSoundChoiceAppBean> soundEffectBeanList = new ArrayList<LiveSoundChoiceAppBean>() {
        {
            add(new LiveSoundChoiceAppBean(APP_WANGYI, R.mipmap.ic_wangyi, StringUtils.getString(R.string.netease_music), Utils.getColor(R.color.app_wangyi), PACKAGE_WANGYI));
            add(new LiveSoundChoiceAppBean(APP_XIAMI, R.mipmap.ic_xiami, StringUtils.getString(R.string.xiami_music), Utils.getColor(R.color.app_xiami), PACKAGE_XIAMI));
            add(new LiveSoundChoiceAppBean(APP_QQ, R.mipmap.ic_qq, StringUtils.getString(R.string.qq_music), Utils.getColor(R.color.app_qq), PACKAGE_QQ));
            add(new LiveSoundChoiceAppBean(APP_KUGOU, R.mipmap.ic_kugou, StringUtils.getString(R.string.kugou_music), Utils.getColor(R.color.app_kugou), PACKAGE_KUGOU));
        }
    };

}
