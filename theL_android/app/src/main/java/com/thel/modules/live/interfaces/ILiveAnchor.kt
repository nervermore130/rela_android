package com.thel.modules.live.interfaces

import com.thel.bean.RejectMicBean
import com.thel.modules.live.bean.*
import java.util.ArrayList

interface ILiveAnchor {
    /**
     * 网速过慢
     */
    fun networkSlow()


    /**
     * 重置是否在底部
     */
    fun resetIsBottom()


    /**
     * 网络状态良好
     */
    fun networkGood()

    /**
     * 连接中断
     */
    fun connectBreak()

    /**
     * 连接失败
     */
    fun connectFailed()

    /**
     * 连接中
     */
    fun connectiong()

    /**
     * 连接成功
     */
    fun connectSuccess()

    /**
     * 有人加入房间
     *
     * @param liveRoomMsgBean
     */
    fun joinUser(liveRoomMsgBean: LiveRoomMsgBean)


    /**
     * 关闭直播 消息返回
     *
     * @param json
     */
    fun refreshCloseDialog(json: String)

    /**
     * 关闭进度条
     */
    fun progressbarGone()

    /**
     * 直播断开连接
     */
    fun liveDisconnected()

    /**
     * 直播网络警告
     *
     * @param text
     */
    fun liveNetworkWaringNotify(text: String)


    fun switchBeauty(enable: Boolean)

    /**
     * 是否关闭直播确认alert
     */
    fun showCloseAlert()

    /*********************************一下为直播pk有关方法*********************************************************/

    /**
     * 显示我申请Pk的view
     */
    fun showIPkRequestView(livePkFriendBean: LivePkFriendBean)

    /**
     * 显示直播PK申请view
     *
     * @param pkRequestNoticeBean
     */
    fun showPkRequestView(pkRequestNoticeBean: LivePkRequestNoticeBean)

    /**
     * 显示直播PK回应view
     *
     * @param pkResponseNoticeBean
     */
    fun showPkResponseView(pkResponseNoticeBean: LivePkResponseNoticeBean)

    /**
     * 显示直播pk取消view
     *
     * @param pkCancelPayload
     */
    fun showPkCancelView(pkCancelPayload: String)

    /**
     * pk请求超时
     */
    fun showPkRequestTimeOutView(userId: String)


    /*********************************以上为直播pk有关方法*********************************************************/

    /**
     * 发送连麦请求成功
     *
     * @param toUserId
     * @param nickName
     * @param avatar
     */
    fun showLinkMicSendSuccess(toUserId: String, nickName: String, avatar: String, dailyGuard: Boolean)

    /**
     * 显示连麦图层
     *
     * @param userName
     * @param toUserId
     */
    fun showLinkMicLayer(userName: String, toUserId: String)

    /**
     * 显示断开连接后的视图
     *
     * @param guardLinkMic 默认pk，当日榜用户挂断时为true
     */
    fun showHangupView(nickName: String, avatar: String, guardLinkMic: Boolean)

    /**
     * 直播Pk请求错误返回
     *
     * @param errorCode
     */
    fun showPkRequestErrorView(errorCode: String)


    /**
     * 添加观众连麦
     *
     * @param livePkFriendBean
     */
    fun videoLinkAdd(livePkFriendBean: LivePkFriendBean)

    /**
     * 删除观众连麦
     *
     * @param livePkFriendBean
     */
    fun videoLinkDel(livePkFriendBean: LivePkFriendBean)

    fun lockInput()

    /**
     * 获取当前在直播间中的日榜列表
     */
    fun getTopFansTodayList(list: ArrayList<LivePkFriendBean>)

    /**
     * 主播连麦时，观众已不再直播间
     */
    fun andienceNotIn()

    /**
     * 观众决绝连麦
     */
    fun rejectMic(rejectMicBean: RejectMicBean)

    fun linkMicHangup()

}