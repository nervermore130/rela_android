package com.thel.modules.live.view.sound;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.thel.R;
import com.thel.base.BaseDialogFragment;
import com.thel.bean.live.LiveMultiSeatBean;
import com.thel.constants.TheLConstants;
import com.thel.utils.UserUtils;

/**
 * 点击多人连麦 嘉宾的弹窗
 *
 * @author lingwei
 * @date 2018/5/6
 */

public class LiveSoundCrowdDialogView extends BaseDialogFragment {

    public static final int TYPE_LINK_MIC = 0x03;

    public static final int TYPE_SORT_MIC = 0x04;

    private TextView enter_user_info;
    private TextView ban_mic;
    private TextView exit_mic;
    private TextView ban_exit_mic;
    private TextView ask_sort_mic;
    private TextView cancel;
    private TextView ask_joining_mic;
    private TextView on_mic;
    private TextView send_gift_to_me;

    private LiveMultiSeatBean liveMultiSeatBean;
    private View.OnClickListener onSeatListener;
    private View.OnClickListener exitMicListener;
    private View.OnClickListener banMicListener;
    private View.OnClickListener banExitMicListener;
    private View.OnClickListener askSortMicListener;
    private View.OnClickListener enterUserInfoListener;
    private View.OnClickListener askJoiningMicListener;
    private View.OnClickListener onMicListener;
    private View.OnClickListener sendGiftListener;

    private int type = -1;

    private String userId;

    public static LiveSoundCrowdDialogView newInstance(Bundle bundle) {
        LiveSoundCrowdDialogView dialogView = new LiveSoundCrowdDialogView();
        dialogView.setArguments(bundle);
        return dialogView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            Bundle bundle = getArguments();
            liveMultiSeatBean = (LiveMultiSeatBean) bundle.getSerializable(TheLConstants.BUNDLE_KEY_SEAT_BEAN);
            userId = bundle.getString(TheLConstants.BUNDLE_KEY_USER_ID);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.multi_sound_choice_crowd_view, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setLisener();
    }

    public void setType(int type) {
        this.type = type;
    }


    private void init(View view) {
        enter_user_info = view.findViewById(R.id.enter_user_info);
        ban_mic = view.findViewById(R.id.ban_mic);
        exit_mic = view.findViewById(R.id.exit_mic);
        ban_exit_mic = view.findViewById(R.id.ban_exit_mic);
        ask_sort_mic = view.findViewById(R.id.ask_sort_mic);
        //视频，观众请求连麦
        ask_joining_mic = view.findViewById(R.id.ask_joining_mic);
        cancel = view.findViewById(R.id.cancel);
        on_mic = view.findViewById(R.id.on_mic);
        send_gift_to_me = view.findViewById(R.id.send_gift_to_me);

        if (type == TYPE_LINK_MIC) {
            on_mic.setVisibility(View.VISIBLE);
        } else if (type == TYPE_SORT_MIC) {
            ask_sort_mic.setVisibility(View.VISIBLE);
        } else {
            if (liveMultiSeatBean != null) {

                exit_mic.setVisibility(View.VISIBLE);
                if (userId.equals(UserUtils.getMyUserId())) {
                    send_gift_to_me.setVisibility(View.GONE);
                } else {
                    send_gift_to_me.setVisibility(View.VISIBLE);
                }
                if (!TextUtils.isEmpty(liveMultiSeatBean.userId) && !UserUtils.getMyUserId().equals(liveMultiSeatBean.userId)) {
                    enter_user_info.setVisibility(View.VISIBLE);
                    ban_mic.setVisibility(View.VISIBLE);
                    ban_exit_mic.setVisibility(View.VISIBLE);
                }
                //处于被主播禁卖状态
                if (liveMultiSeatBean.micStatus.equals("close")) {
                    ban_mic.setText(R.string.close_ban_mic);
                } else {
                    ban_mic.setText(R.string.ban_mic);
                }

                if (!TextUtils.isEmpty(liveMultiSeatBean.nickname)) {
                    enter_user_info.setText(getString(R.string.user_info, liveMultiSeatBean.nickname));
                }
            } else {//申请上麦
                ask_sort_mic.setVisibility(View.VISIBLE);
                ask_joining_mic.setVisibility(View.VISIBLE);
            }

        }
    }

    private void setLisener() {
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LiveSoundCrowdDialogView.this.dismiss();
            }
        });
        if (onSeatListener != null) {
            exit_mic.setText(R.string.on_mic);
            exit_mic.setOnClickListener(onSeatListener);
        }
        if (exitMicListener != null) {
            exit_mic.setText(R.string.exit_mic);
            exit_mic.setOnClickListener(exitMicListener);
        }
        if (banMicListener != null)
            ban_mic.setOnClickListener(banMicListener);
        if (enterUserInfoListener != null) {
            enter_user_info.setOnClickListener(enterUserInfoListener);
        }
        if (banExitMicListener != null) {
            ban_exit_mic.setOnClickListener(banExitMicListener);
        }

        if (askSortMicListener != null) {
            ask_sort_mic.setOnClickListener(askSortMicListener);
        }

        if (askJoiningMicListener != null) {
            ask_sort_mic.setVisibility(View.GONE);
            ask_joining_mic.setOnClickListener(askJoiningMicListener);
        }

        if (onMicListener != null) {
            on_mic.setOnClickListener(onMicListener);
        }

        if (sendGiftListener != null) {
            send_gift_to_me.setOnClickListener(sendGiftListener);
        }

    }

    /**
     * 上麦
     *
     * @param onSeatListener
     */
    public void setOnSeatListener(View.OnClickListener onSeatListener) {
        this.onSeatListener = onSeatListener;
    }

    /**
     * 下麦
     *
     * @param exitMicListener
     */
    public void setExitMicListener(View.OnClickListener exitMicListener) {
        this.exitMicListener = exitMicListener;
    }

    /**
     * 禁麦
     *
     * @param banMicListener
     */
    public void setBanMicListener(View.OnClickListener banMicListener) {
        this.banMicListener = banMicListener;
    }

    /**
     * 屏蔽并下麦
     *
     * @param banExitMicListener
     */
    public void setBanExitMicListener(View.OnClickListener banExitMicListener) {
        this.banExitMicListener = banExitMicListener;
    }

    /**
     * 申请上麦
     *
     * @param askSortMicListener
     */
    public void setAskSortMicListener(View.OnClickListener askSortMicListener) {
        this.askSortMicListener = askSortMicListener;
    }

    public void setEnterUserInfoListener(View.OnClickListener enterUserInfoListener) {
        this.enterUserInfoListener = enterUserInfoListener;
    }

    /**
     * 视频 ，观众申请上麦
     *
     * @param askJoiningMicListener
     */
    public void setAskJoiningMicListener(View.OnClickListener askJoiningMicListener) {
        this.askJoiningMicListener = askJoiningMicListener;
    }

    public void setOnMicListener(View.OnClickListener onMicListener) {
        this.onMicListener = onMicListener;
    }

    /**
     * 送礼物给我自己
     */
    public void setSendGiftListener(View.OnClickListener sendGiftListener) {
        this.sendGiftListener = sendGiftListener;
    }

}
