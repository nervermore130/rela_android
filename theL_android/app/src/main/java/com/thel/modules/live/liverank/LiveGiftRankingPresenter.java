package com.thel.modules.live.liverank;

import com.thel.modules.live.bean.LiveGiftsRankingNetBean;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.service.DefaultRequestService;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by waiarl on 2017/10/26.
 */

public class LiveGiftRankingPresenter implements LiveGiftRankingContract.Presenter {
    private final LiveGiftRankingContract.View view;

    public LiveGiftRankingPresenter(LiveGiftRankingContract.View view) {
        this.view = view;
        view.setPresenter(this);
    }

    @Override
    public void getRefreshData(String userId, String tag) {
        final Flowable<LiveGiftsRankingNetBean> flowable = DefaultRequestService.createLiveRequestService().getLiveGiftRankingList(userId, tag);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<LiveGiftsRankingNetBean>() {
            @Override
            public void onNext(LiveGiftsRankingNetBean data) {
                super.onNext(data);
                view.showEmptyData(true);
                if (data != null && data.data != null) {
                    if (data.data.list != null) {
                        view.showRefreshData(data.data.list);
                        if (data.data.list.size() > 0) {
                            view.showEmptyData(false);
                        }
                    }
                    if (data.data.rankInfo != null) {
                        view.showRankInfoView(data.data.rankInfo);
                    }
                }
            }

            @Override
            public void onComplete() {
                super.onComplete();
                view.requestFinish();
            }
        });
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {

    }
}
