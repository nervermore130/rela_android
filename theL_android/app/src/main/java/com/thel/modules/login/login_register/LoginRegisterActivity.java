package com.thel.modules.login.login_register;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.IntDef;
import androidx.fragment.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.modules.login.LoginActivityManager;
import com.thel.utils.ShareFileUtils;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginRegisterActivity extends BaseActivity {

    @BindView(R.id.txt_title)
    TextView mTitle;

    @BindView(R.id.img_back)
    ImageView mCloseBtn;
    private boolean isWeb;

    @OnClick(R.id.back)
    void close() {
        finish();
    }

    private int mType;

    private boolean wxVisible = true;
    private boolean fbVisible = true;
    private boolean emailVisible = true;
    private String typeTips;

    public static final int LOGIN = 0;
    public static final int REGISTER = 1;
    public static final int BUNDLE = 2;//绑定手机
    public static final int PHONE_VERIFY = 3;//实名认真（绑定手机）
    public static final int BUNDLE_CHANGE = 4;//换绑手机
    public static final String TYPE = "type";
    public static final String ISWEB ="isWeb";

    @IntDef({LOGIN, REGISTER, BUNDLE, PHONE_VERIFY, BUNDLE_CHANGE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface TYPE {
    }

    public static void startActivity(Context context, @TYPE int type,boolean isWeb) {

        Intent intent = new Intent(context, LoginRegisterActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(TYPE, type);
        intent.putExtra(ISWEB,isWeb);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LoginActivityManager.getInstanse().addActivity(this);
        setContentView(R.layout.activity_login_register);
        ButterKnife.bind(this);

        mType = getIntent().getIntExtra(TYPE, LOGIN);
        isWeb = getIntent().getBooleanExtra(ISWEB, false);
        initNumberFragment();

        initView();

    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void initNumberFragment() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        InputNumberFragment inputNumberFragment = InputNumberFragment.newInstance(mType,isWeb);
        fragmentTransaction.add(R.id.input_container, inputNumberFragment);
        fragmentTransaction.commit();
    }

    private void initView() {

        String title = "";
        switch (mType) {
            case LOGIN:
                title = getString(R.string.login_by_mobile);

                typeTips = getString(R.string.login_by_below);
                lazyInitLoginBtn();
                break;
            case REGISTER:
                title = getString(R.string.register_by_mobile);

                emailVisible = false;
                typeTips = getString(R.string.register_by_below);
                lazyInitLoginBtn();
                break;
            case PHONE_VERIFY:
                title = getString(R.string.phone_verify_title);

                mCloseBtn.setVisibility(View.GONE);
                break;
            case BUNDLE:
            case BUNDLE_CHANGE:
                title = getString(R.string.bundle_mobile);
                break;
        }

        mTitle.setText(title);
    }

    private void lazyInitLoginBtn() {
        if (!ShareFileUtils.isGlobalVersion()) {// 简体中文隐藏facebook的绑定按钮
            fbVisible = false;
        }else {
            wxVisible = false;
        }

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        LoginBtnFragment fragment = LoginBtnFragment.newInstance(typeTips, wxVisible, fbVisible, emailVisible);
        fragmentTransaction.add(R.id.login_btn_container, fragment);
        fragmentTransaction.commit();
    }
}
