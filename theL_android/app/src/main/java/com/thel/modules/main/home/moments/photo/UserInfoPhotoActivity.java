package com.thel.modules.main.home.moments.photo;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.MotionEvent;

import com.thel.R;
import com.thel.base.BaseDispatchTouchActivity;

/**
 * Created by liuyun on 2017/10/25.
 */

public class UserInfoPhotoActivity extends BaseDispatchTouchActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_frame_normal);

        UserInfoPhotoFragment userInfoPhotoFragment = (UserInfoPhotoFragment) getSupportFragmentManager().findFragmentById(R.id.frame_content);

        if (userInfoPhotoFragment == null) {

            userInfoPhotoFragment = new UserInfoPhotoFragment();

            userInfoPhotoFragment.setArguments(getIntent().getExtras());

            getSupportFragmentManager().beginTransaction().add(R.id.frame_content, userInfoPhotoFragment, UserInfoPhotoFragment.class.getName()).commit();
        }

    }

    /**
     * 上滑关闭
     */

    float startX = 0, startY = 0, endX = 0, endY = 0;
    final int dis = 400;//滑动最小上下距离为200；

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                startX = event.getX();
                startY = event.getY();
                break;
            case MotionEvent.ACTION_UP:
                endX = event.getX();
                endY = event.getY();
                final float mx = endX - startX;
                final float my = endY - startY;
                if (Math.abs(mx) == 0 && my >= dis) {
                    finish();
                } else if (Math.abs(mx) > 0 && my >= dis) {
                    final double digree = Math.toDegrees(Math.atan(my / mx));
                    if (digree >= 70 || digree <= -70) {
                        finish();
                    }
                }
                break;
        }
        return super.dispatchTouchEvent(event);
    }
}
