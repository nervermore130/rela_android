package com.thel.modules.main.home.community;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseFragment;
import com.thel.bean.theme.ThemeBean;
import com.thel.bean.theme.ThemeClassBean;
import com.thel.bean.theme.ThemeListBean;
import com.thel.constants.BundleConstants;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.modules.main.home.moments.theme.ThemeDetailActivity;
import com.thel.modules.main.me.bean.LogInfoBean;
import com.thel.modules.main.me.match.eventcollect.collect.LiveLogUtils;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.Utils;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ThemeSortListFragment extends BaseFragment {

    @BindView(R.id.list_view)
    RecyclerView mListView;

    public static final int REFRESH_TYPE_ALL = 0;
    public static final int REFRESH_TYPE_NEXT_PAGE = 1;

    private boolean haveNextPage = false;

    private int cursor;

    private int refreshType = REFRESH_TYPE_ALL;

    private ThemeClassBean bean;

    private HotThemesAdapter adapter;

    private List<ThemeBean> listPlusHotThemes = new ArrayList<>();
    private boolean isFirstCreate = false;


    private String pageId;
    private String from_page_id;
    private String from_page;
    private String rankId;

    private class NextConsumer extends InterceptorSubscribe<ThemeListBean> {
        @Override
        public void onNext(ThemeListBean data) {
            super.onNext(data);
            showThemeList(data);

            if (refreshFinishListener != null && isAdded()) {
                refreshFinishListener.refreshFinish();
            }
        }

        @Override
        public void onError(Throwable t) {
            super.onError(t);

            if (isAdded()) {
                mListView.post(new Runnable() {
                    @Override
                    public void run() {
                        adapter.loadMoreFailed((ViewGroup) mListView.getParent());
                    }
                });

                if (refreshFinishListener != null) {
                    refreshFinishListener.refreshFinish();
                }
            }

        }
    }

    public static ThemeSortListFragment newInstance(ThemeClassBean tag, int pos) {
        final ThemeSortListFragment fragment = new ThemeSortListFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("bean", tag);
        bundle.putInt("position", pos);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isFirstCreate = true;
        if (getArguments() != null) {
            Bundle bundle = getArguments();
            bean = (ThemeClassBean) bundle.getSerializable("bean");

            from_page = ShareFileUtils.getString(ShareFileUtils.homePage, "");
            from_page_id = ShareFileUtils.getString(ShareFileUtils.homePageId, "");
            pageId = Utils.getPageId();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_theme_sort_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initList();
        if (getUserVisibleHint()) {
            loadNetData(0, REFRESH_TYPE_ALL);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        try {
            if (isVisibleToUser && isFirstCreate) {
                loadNetData(0, REFRESH_TYPE_ALL);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initList() {
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(TheLApp.getContext());
        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mListView.setHasFixedSize(true);
        // mListView.addItemDecoration(new DefaultItemDivider(TheLApp.getContext(), LinearLayoutManager.VERTICAL, R.color.white, (int) TheLApp.getContext().getResources().getDimension(R.dimen.wide_divider_height), true, false));
        mListView.setLayoutManager(mLinearLayoutManager);
        if (bean != null) {
            adapter = new HotThemesAdapter(listPlusHotThemes, "theme.list", pageId, from_page, from_page_id, bean.type, rankId);

        } else {
            adapter = new HotThemesAdapter(listPlusHotThemes, "theme.list", pageId, from_page, from_page_id, "", rankId);

        }
        mListView.setAdapter(adapter);

        adapter.setOnLoadMoreListener(new BaseRecyclerViewAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                mListView.post(new Runnable() {
                    @Override
                    public void run() {
                        if (haveNextPage) {
                            // 列表滑动到底部加载下一组数据
                            loadNetData(cursor, REFRESH_TYPE_NEXT_PAGE);
                        } else {
                            adapter.openLoadMore(listPlusHotThemes.size(), false);
                            View view = LayoutInflater.from(getActivity()).inflate(R.layout.load_more_footer_layout, (ViewGroup) mListView.getParent(), false);
                            ((TextView) view.findViewById(R.id.text)).setText(getString(R.string.info_no_more));
                            adapter.addFooterView(view);
                        }
                    }
                });
            }
        });
        adapter.setReloadMoreListener(new BaseRecyclerViewAdapter.ReloadMoreListener() {
            @Override
            public void reloadMore() {
                refreshType = REFRESH_TYPE_NEXT_PAGE;
                adapter.removeAllFooterView();
                adapter.openLoadMore(true);
                loadNetData(cursor, REFRESH_TYPE_NEXT_PAGE);
            }
        });
        adapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                L.d("ThemeSortListFragment", " listPlusHotThemes.get(position) : " + listPlusHotThemes.get(position));

                if (listPlusHotThemes != null) {
                    ThemeBean themeBean = listPlusHotThemes.get(position);
                    if (themeBean != null) {
                        MobclickAgent.onEvent(TheLApp.getContext(), "discover_topic_moment");
//                        Intent intent = new Intent();
//                        intent.setClass(getActivity(), ThemeDetailActivity.class);
//                        intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, listPlusHotThemes.get(position).id + "");
//                        intent.putExtra(BundleConstants.THEMEPARTICIPATES, listPlusHotThemes.get(position).buildDesc());
//                        startActivity(intent);
                        FlutterRouterConfig.Companion.gotoThemeDetails(listPlusHotThemes.get(position).id + "");
                        traceThemeLog("theme.list", "click", position, listPlusHotThemes.get(position).id, rankId);

                    }
                }

            }
        });
    }

    public void traceThemeLog(String page, String activity, int position, String momentsId, String rankid) {
        String latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
        String longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");
        try {
            LogInfoBean logInfoBean = new LogInfoBean();
            logInfoBean.page = page;
            logInfoBean.page_id = pageId;
            logInfoBean.activity = activity;
            logInfoBean.from_page = from_page;
            logInfoBean.from_page_id = from_page_id;
            logInfoBean.lat = latitude;
            logInfoBean.lng = longitude;

            LogInfoBean.LogsDataBean logsDataBean = new LogInfoBean.LogsDataBean();
            logsDataBean.rank_id = rankid;
            logsDataBean.index = position;
            logsDataBean.theme_id = momentsId;
            logsDataBean.index = position;
            if (bean != null) {
                logsDataBean.sort = bean.type;

            }
            logInfoBean.data = logsDataBean;

            LiveLogUtils.getInstance().addLog(logInfoBean);

        } catch (Exception e) {

        }
    }

    public void setFromPageAndId(String from_Page, String pageId) {
        this.from_page = from_Page;
        this.from_page_id = pageId;

    }
    public void loadNetData(int curPage, int type) {
        isFirstCreate = false;
        refreshType = type;
        this.cursor = curPage;
        if (bean == null) {
            return;
        }
        if (bean.type.equals("hot")) {

            RequestBusiness.getInstance().getHotThemeList(curPage).onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new NextConsumer());
        } else if (bean.type.equals("new")) {
            RequestBusiness.getInstance().getNewThemesList(curPage).onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new NextConsumer());
        } else {
            RequestBusiness.getInstance().getThemesList(bean.type, curPage).onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new NextConsumer());
        }
    }

    private void showThemeList(ThemeListBean result) {

        if (refreshType == REFRESH_TYPE_ALL) {
            listPlusHotThemes.clear();
        }
        if (result.data.list != null)
            listPlusHotThemes.addAll(result.data.list);
        if (refreshType == REFRESH_TYPE_ALL) {
            adapter.removeAllFooterView();
            adapter.openLoadMore(listPlusHotThemes.size(), true);
            adapter.setNewData(listPlusHotThemes);
        } else {
            adapter.notifyDataChangedAfterLoadMore(true, listPlusHotThemes.size());
        }
        cursor = result.data.cursor;
        haveNextPage = result.data.haveNextPage;
        rankId = result.data.rankId;
    }

    private RefreshFinishListener refreshFinishListener;

    public interface RefreshFinishListener {
        void refreshFinish();
    }

    public void setRrefreshFinishListener(RefreshFinishListener listener) {
        this.refreshFinishListener = listener;
    }


}
