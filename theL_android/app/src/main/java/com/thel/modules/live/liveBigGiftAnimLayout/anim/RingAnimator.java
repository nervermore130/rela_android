package com.thel.modules.live.liveBigGiftAnimLayout.anim;

import android.animation.Animator;
import android.content.Context;
import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.thel.R;
import com.thel.modules.live.liveBigGiftAnimLayout.LiveBigAnimUtils;
import com.thel.ui.widget.MyAnimationDrawable;
import com.thel.utils.Utils;

import java.util.Random;

/**
 * Created by the L on 2016/10/13.
 */

public class RingAnimator {
    private final Context mContext;
    private final int mDuration;
    private final Handler mHandler;
    private final Random mRandom;
    //    private final int[] resDiamond;
    //    private final int[] resOpenBox;
    //    private final int[] resRing;
    //    private final int[] resHeart;
    private final String[] resDiamond;
    private final String[] resOpenBox;
    private final String[] resRing;
    private final String[] resHeart;

    private final int minDiamondCount;
    private final int diffDiamondCount;
    private final int rate;
    private final int minDiamondWidth;
    private final int differDiamondWidth;
    private final int minDropDuration;
    private final int differDropDuration;
    private final int boxFreq;
    private final int ringFreq;
    private final int minHeartWidth;
    private final int mInDuration;
    private final int mOutDuration;
    private final String foldPath;
    private int mWidth;
    private int mHeight;
    private boolean isPlaying;
    private boolean isFlowingHeart;
    private long flowHeartDuration;
    private long mHeartDuration;
    private long diaMissDuration;

    public RingAnimator(Context context, int duration) {
        mContext = context;
        mDuration = duration;
        mHandler = new Handler(Looper.getMainLooper());
        mRandom = new Random();
        //        resDiamond = new int[]{R.drawable.ring_drill1, R.drawable.ring_drill2, R.drawable.ring_drill3, R.drawable.ring_drill4, R.drawable.ring_drill5, R.drawable.ring_drill6, R.drawable.ring_drill7};
        //        resOpenBox = new int[]{R.drawable.ring_box1, R.drawable.ring_box2, R.drawable.ring_box3};
        //        resRing = new int[]{R.drawable.ring_ring1, R.drawable.ring_ring2};
        //        resHeart = new int[]{R.drawable.ring_heart1, R.drawable.ring_heart2, R.drawable.ring_heart3, R.drawable.ring_heart4, R.drawable.ring_heart5};
        foldPath = "anim/ring";
        resDiamond = new String[]{"ring_drill1", "ring_drill2", "ring_drill3", "ring_drill4", "ring_drill5", "ring_drill6", "ring_drill7"};
        resOpenBox = new String[]{"ring_box1", "ring_box2", "ring_box3"};
        resRing = new String[]{"ring_ring1", "ring_ring2"};
        resHeart = new String[]{"ring_heart1", "ring_heart2", "ring_heart3", "ring_heart4", "ring_heart5"};
        minDiamondCount = 5;
        diffDiamondCount = 5;
        minDropDuration = 300;//钻石下落最快时间
        differDropDuration = 1500;//下落时间变化区域
        rate = 200;//发散心/下落钻石周期
        minDiamondWidth = Utils.dip2px(mContext, 10);
        differDiamondWidth = Utils.dip2px(mContext, 35);
        minHeartWidth = Utils.dip2px(mContext, 8);//心的最小宽度
        boxFreq = 600 / resOpenBox.length;//打开盒子频率
        ringFreq = 500;//戒指闪烁频率
        flowHeartDuration = 2000;//发散心的时间
        mHeartDuration = 1200;//心的存在时间
        diaMissDuration = 2000;//钻石落下以后渐变消失时时间
        mInDuration = 1000;//动画淡入时间
        mOutDuration = 1000;//动画淡出时间
    }

    public int start(final ViewGroup parent) {
        isPlaying = true;
        isFlowingHeart = true;
        mWidth = parent.getMeasuredWidth();
        mHeight = parent.getMeasuredHeight();
        if (mWidth == 0 || mHeight == 0) {
            return 0;
        }
        final RelativeLayout background = (RelativeLayout) RelativeLayout.inflate(mContext, R.layout.live_big_anim_ring, null);
        parent.addView(background);
        background.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        initBackground(background);
        setBackground(background);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                isPlaying = false;
                background.clearAnimation();
                parent.removeView(background);
            }
        }, mDuration);
        return mDuration;
    }

    private void initBackground(RelativeLayout background) {
        final ImageView img_box = background.findViewById(R.id.img_box);
        final ImageView img_ring = background.findViewById(R.id.img_ring);
        LiveBigAnimUtils.setAssetBackground(img_box, foldPath, resOpenBox[0]);
        LiveBigAnimUtils.setAssetBackground(img_ring, foldPath, resRing[0]);
    }

    private void setBackground(final ViewGroup background) {
        final ImageView img_box = background.findViewById(R.id.img_box);
        final ImageView img_ring = background.findViewById(R.id.img_ring);
        img_box.setScaleX(0.1f);
        img_box.setScaleY(0.1f);
        img_box.animate().scaleXBy(1f).scaleY(1f).setDuration(mInDuration).setInterpolator(new LinearInterpolator()).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (isPlaying) {
                    openBox(img_box, background, img_ring);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        background.postDelayed(new Runnable() {
            @Override
            public void run() {
                isPlaying = false;
                img_box.animate().alpha(0).setDuration(mOutDuration);
                img_ring.animate().alpha(0).setDuration(mOutDuration);
            }
        }, mDuration - mOutDuration);
    }

    private void openBox(final View view, final ViewGroup background, final View img_ring) {
        final AnimationDrawable anim = new AnimationDrawable();
        //        anim.addFrame(ContextCompat.getDrawable(mContext, resOpenBox[0]), boxFreq);
        //        anim.addFrame(ContextCompat.getDrawable(mContext, resOpenBox[1]), boxFreq);
        //        anim.addFrame(ContextCompat.getDrawable(mContext, resOpenBox[2]), boxFreq);
        anim.addFrame(LiveBigAnimUtils.getDrawable(mContext, foldPath, resOpenBox[0]), boxFreq);
        anim.addFrame(LiveBigAnimUtils.getDrawable(mContext, foldPath, resOpenBox[1]), boxFreq);
        anim.addFrame(LiveBigAnimUtils.getDrawable(mContext, foldPath, resOpenBox[2]), boxFreq);
        final MyAnimationDrawable mAnim = new MyAnimationDrawable(anim) {
            @Override
            public void onAnimationEnd() {
                stop();
                setDiamondBackground(background);
            }
        };
        mAnim.setOneShot(true);
        view.setBackgroundDrawable(mAnim);
        mAnim.start();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                img_ring.setVisibility(View.VISIBLE);
                //   LiveBigGiftAnimLayout.setFrameAnim(mContext, img_ring, resRing, ringFreq);
                LiveBigAnimUtils.setFrameAnim(mContext, img_ring, foldPath, resRing, ringFreq);
            }
        }, boxFreq + 10);
    }

    private void setDiamondBackground(ViewGroup parent) {

        final RelativeLayout background = parent.findViewById(R.id.rel_diamond_background);
        background.post(new Runnable() {
            @Override
            public void run() {
                final int count = mRandom.nextInt(diffDiamondCount) + minDiamondCount;
                if (isFlowingHeart) {
                    for (int i = 0; i < count; i++) {
                        flowOneHeart(background);
                    }
                } else {
                    for (int i = 0; i < count; i++) {
                        dropOneDiamond(background);
                    }
                }
                if (isPlaying) {
                    background.postDelayed(this, rate);
                }
            }
        });
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                isFlowingHeart = false;
            }
        }, flowHeartDuration);
    }

    private void flowOneHeart(final ViewGroup background) {
        final ImageView iv = new ImageView(mContext);
        final int id = mRandom.nextInt(resHeart.length);
        //iv.setImageResource(resHeart[id]);
        LiveBigAnimUtils.setAssetImage(iv, foldPath, resHeart[id]);
        background.addView(iv);
        final int width = minHeartWidth;
        final int height = getImageHeight(resHeart[id], width);
        iv.setLayoutParams(new RelativeLayout.LayoutParams(width, height));
        final RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams) iv.getLayoutParams();
        param.addRule(RelativeLayout.CENTER_IN_PARENT);
        FloatHeartAnimation anim;
        final float scaleT = mRandom.nextFloat() * 4;
        final float alphaT = mRandom.nextFloat() * 7 / 3;
        anim = new FloatHeartAnimation(createPath(), background, iv, scaleT, alphaT);
        anim.setDuration(mHeartDuration);
        anim.setInterpolator(new LinearInterpolator());
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                background.post(new Runnable() {
                    @Override
                    public void run() {
                        background.removeView(iv);
                    }
                });

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        iv.startAnimation(anim);
    }

    private Path createPath() {
        Path p = new Path();
        p.moveTo(0, 0);
        PointF p1 = new PointF(-mWidth / 2 + mRandom.nextFloat() * mWidth, -mHeight / 2 + mRandom.nextFloat() * mHeight);
        PointF p2 = getPoint(p1);
        PointF p3 = getPoint(p2);
        p.cubicTo(p1.x, p1.y, p2.x, p2.y, p3.x, p3.y);
        return p;
    }

    private PointF getPoint(PointF point) {
        PointF p;
        if (point.x >= 0 && Math.abs(point.x) >= Math.abs(point.y)) {
            p = new PointF(point.x + mRandom.nextFloat() * mWidth / 8, point.y - mHeight / 4 + mRandom.nextFloat() * mHeight / 2);
        } else if (point.x <= 0 && Math.abs(point.x) >= Math.abs(point.y)) {
            p = new PointF(point.x - mRandom.nextFloat() * mWidth / 8, point.y - mHeight / 4 + mRandom.nextFloat() * mHeight / 2);
        } else if (point.y >= 0 && Math.abs(point.x) <= Math.abs(point.y)) {
            p = new PointF(point.x - mWidth / 4 + mRandom.nextFloat() * mWidth / 2, point.y + mRandom.nextFloat() * mHeight / 8);
        } else if (point.y <= 0 && Math.abs(point.x) <= Math.abs(point.y)) {
            p = new PointF(point.x - mWidth / 4 + mRandom.nextFloat() * mWidth / 2, point.y - mRandom.nextFloat() * mHeight / 8);
        } else {
            p = new PointF(point.x, point.y);
        }
        return p;
    }


    private void dropOneDiamond(final RelativeLayout background) {
        final ImageView img_diamond = new ImageView(mContext);
        background.addView(img_diamond);
        final int resIndex = mRandom.nextInt(resDiamond.length);
        int w = minDiamondWidth + mRandom.nextInt(differDiamondWidth);
        int h = getImageHeight(resDiamond[resIndex], w);
        //img_diamond.setImageResource(resDiamond[resIndex]);
        LiveBigAnimUtils.setAssetImage(img_diamond, foldPath, resDiamond[resIndex]);
        img_diamond.setLayoutParams(new RelativeLayout.LayoutParams(w, h));
        Point startPoint = new Point(mRandom.nextInt(mWidth), 0 - h);
        Point endPoint = new Point(startPoint.x, mHeight - h);
        Path path = new Path();
        path.moveTo(startPoint.x, startPoint.y);
        path.lineTo(endPoint.x, endPoint.y);
        final int dropDuration = minDropDuration + mRandom.nextInt(differDropDuration);
        FloatAnimation anim = new FloatAnimation(path, background, img_diamond);
        anim.setDuration(dropDuration);
        anim.setFillAfter(true);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                moveOneDiamond(background, img_diamond);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        img_diamond.startAnimation(anim);
    }

    private void moveOneDiamond(ViewGroup background, final View view) {
        view.post(new Runnable() {
            @Override
            public void run() {
                view.animate().alpha(0).setInterpolator(new LinearInterpolator()).setDuration(diaMissDuration).setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        if (null != view.getParent()) {
                            ((ViewGroup) view.getParent()).removeView(view);
                        }
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                });
            }
        });

    }

    static class FloatAnimation extends Animation {
        private PathMeasure mPm;
        private View mView;
        private float mDistance;

        public FloatAnimation(Path path, View parent, View child) {
            mPm = new PathMeasure(path, false);
            mDistance = mPm.getLength();
            mView = child;
            parent.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        }

        @Override
        protected void applyTransformation(float factor, Transformation transformation) {
            Matrix matrix = transformation.getMatrix();
            mPm.getMatrix(mDistance * factor, matrix, PathMeasure.POSITION_MATRIX_FLAG);
        }
    }

    static class FloatHeartAnimation extends Animation {
        private final float mScaleT;
        private PathMeasure mPm;
        private View mView;
        private float mDistance;

        public FloatHeartAnimation(Path path, View parent, View child, float scaleT, float alphaT) {
            mPm = new PathMeasure(path, false);
            mDistance = mPm.getLength();
            mView = child;
            parent.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            mScaleT = scaleT;
        }

        @Override
        protected void applyTransformation(float factor, Transformation transformation) {
            Matrix matrix = transformation.getMatrix();
            mPm.getMatrix(mDistance * factor, matrix, PathMeasure.POSITION_MATRIX_FLAG);
            mView.setScaleX(1 + factor * mScaleT);
            mView.setScaleY(1 + factor * mScaleT);
            if (factor >= 0.7)// 从70%开始淡出
                transformation.setAlpha(1 - (factor - 0.7f) / 0.3f);
        }
    }

    private int getImageHeight(String resId, int w) {
        int h;
        switch (resId) {
            case "ring_drill1":
                h = w * 47 / 70;
                break;
            case "ring_drill2":
                h = w * 65 / 70;
                break;
            case "ring_drill3":
                h = w * 69 / 70;
                break;
            case "ring_drill4":
                h = w * 70 / 62;
                break;
            case "ring_drill5":
                h = w * 55 / 70;
                break;
            case "ring_drill6":
                h = w * 69 / 70;
                break;
            case "ring_drill7":
                h = w * 62 / 70;
                break;
            case "ring_heart1":
                h = w * 34 / 40;
                break;
            case "ring_heart2":
                h = w * 40 / 37;
                break;
            case "ring_heart3":
                h = w * 24 / 40;
                break;
            case "ring_heart4":
                h = w * 39 / 40;
                break;
            case "ring_heart5":
                h = w * 37 / 40;
                break;
            case "ring_heart6":
                h = w * 38 / 40;
                break;
            default:
                h = w;
                break;
        }
        return h;
    }
}
