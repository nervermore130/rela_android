package com.thel.modules.main.userinfo;

import com.thel.bean.user.BlockBean;

/**
 * Created by waiarl on 2017/11/4.
 * 单个黑名单网络请求bean(单个屏蔽某个人的时候返回)
 */

public class BlockNetBean {
    public BlockBean data;
}
