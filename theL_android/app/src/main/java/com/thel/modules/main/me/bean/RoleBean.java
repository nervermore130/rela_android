package com.thel.modules.main.me.bean;

import com.thel.base.BaseDataBean;

/**
 * Created by lingwei on 2017/12/25.
 */

public class RoleBean extends BaseDataBean {
    public int listPoi;
    public String contentRes;
    public boolean isSelected = false;

    public RoleBean(int listPoi, String content) {
        this.listPoi = listPoi;
        this.contentRes = content;
    }

    public RoleBean() {
    }
}
