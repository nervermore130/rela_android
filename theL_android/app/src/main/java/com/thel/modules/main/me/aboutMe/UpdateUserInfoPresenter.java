package com.thel.modules.main.me.aboutMe;

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.thel.base.BaseDataBean;
import com.thel.bean.CheckUserNameBean;
import com.thel.bean.user.MyImagesListBean;
import com.thel.bean.user.UploadTokenBean;
import com.thel.modules.main.me.bean.MyInfoNetBean;
import com.thel.modules.main.me.bean.UpdataInfoBean;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.service.DefaultRequestService;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;
import com.thel.utils.MD5Utils;
import com.thel.utils.ShareFileUtils;

import org.reactivestreams.Subscription;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by lingwei on 2017/10/10.
 */

public class UpdateUserInfoPresenter implements UpdateUserInfoContract.Presenter {
    private final UpdateUserInfoContract.View updateUserInfoView;

    public UpdateUserInfoPresenter(UpdateUserInfoContract.View updateUserInfoView) {
        this.updateUserInfoView = updateUserInfoView;
        updateUserInfoView.setPresenter(this);
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {

    }

    @Override
    public void getDataFromPrepage() {

    }

    /**
     * 加载我的信息
     */
    @Override
    public void loadMyInfo() {
        if (!ShareFileUtils.getBoolean(ShareFileUtils.HAS_LOGGED, false)) {
            return;
        }
        Flowable<MyInfoNetBean> flowable = RequestBusiness.getInstance().getMyInfo();

        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<MyInfoNetBean>() {

            @Override
            public void onNext(MyInfoNetBean myInfoBean) {
                super.onNext(myInfoBean);
                updateUserInfoView.getMyInfo(myInfoBean.data);
            }
        });
    }

    @Override
    public void checkUserName(String username) {

        Flowable<CheckUserNameBean> flowable = RequestBusiness.getInstance().checkUserName(username);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<CheckUserNameBean>() {

            @Override
            public void onNext(CheckUserNameBean data) {
                super.onNext(data);
                updateUserInfoView.checkUserNameResult(data);
            }
        });

    }

    @Override
    public void updateUserInfo(String username, String birth, String lookingForRoleSB, String careerType, String avatarUrl, String nickname, String height, String weight, String role_cur_pos, String relationship_cur_pos, String purposeSB, String ethnicity_cur_pos, String occupation, String livecity, String intro) {
        Flowable<UpdataInfoBean> flowable = RequestBusiness.getInstance().updateUserInfo(username, birth, lookingForRoleSB, careerType, avatarUrl, nickname, height, weight, role_cur_pos, relationship_cur_pos, purposeSB, ethnicity_cur_pos, occupation, livecity, intro);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<UpdataInfoBean>() {

            @Override
            public void onNext(UpdataInfoBean updataInfoBean) {
                super.onNext(updataInfoBean);
                updateUserInfoView.getUpdateUserInfo(updataInfoBean);
            }

            @Override
            public void onError(Throwable t) {
                if (t instanceof HttpException) {
                    HttpException httpException = (HttpException) t;

                    try {
                        String body = httpException.response().errorBody().string();

                        BaseDataBean errorBaseData = GsonUtils.getObject(body, BaseDataBean.class);

                        if (errorBaseData != null && errorBaseData.errcode.equals("sensitive_words")) {//昵称违法
                            updateUserInfoView.popAlerSensitiveWords(errorBaseData.errdesc);

                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                L.d(" onError : " + t.getMessage());
            }

            @Override
            public void onComplete() {

            }
        });
    }

    @Override
    public void getMyImagesList(String pageSize, String curPage) {
        Flowable<MyImagesListBean> flowable = RequestBusiness.getInstance().getMyImagesList(pageSize, curPage);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<MyImagesListBean>() {

            @Override
            public void onNext(MyImagesListBean myImagesListBean) {
                super.onNext(myImagesListBean);
                updateUserInfoView.refreshMyPhoto(myImagesListBean);
            }

            @Override
            public void onComplete() {
                updateUserInfoView.closeLoadingDialog();
            }
        });
    }

    @Override
    public void getUploadToken(String clientTime, String bucket, String path, final String uploadType) {
        Flowable<UploadTokenBean> flowable = RequestBusiness.getInstance().getUploadToken(clientTime, bucket, path);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<UploadTokenBean>() {

            @Override
            public void onNext(UploadTokenBean uploadTokenBean) {
                super.onNext(uploadTokenBean);
                updateUserInfoView.uploadToken(uploadTokenBean, uploadType);
            }

        });
    }

    @Override
    public void getVideoUploadToken(String clientTime, String bucket, String path, String gifPath, String gifType, String gifKey) {
        RequestBusiness.getInstance().getVideoUploadToken(clientTime, bucket, path, gifPath, gifType, gifKey).onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<UploadTokenBean>() {
            @Override
            public void onNext(UploadTokenBean data) {
                super.onNext(data);
                updateUserInfoView.uploadToken(data, "video");
            }
        });
    }

    @Override
    public void uploadImageUrl(String imageUrl, String imageWidth, String imageHeight) {
        Flowable<UpdataInfoBean> flowable = RequestBusiness.getInstance().uploadImageUrl(imageUrl, imageWidth, imageHeight);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<UpdataInfoBean>() {

            @Override
            public void onNext(UpdataInfoBean updataInfoBean) {
                super.onNext(updataInfoBean);
                updateUserInfoView.addImage(updataInfoBean);
            }

        });
    }

    @Override
    public void sortImages(String imageIds) {

        Map<String, String> params = new HashMap<>();

        params.put("images", imageIds);

        Flowable<BaseDataBean> flowable = DefaultRequestService.createAllRequestService().sortImages(MD5Utils.generateSignatureForMap(params));

        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
            @Override
            public void onNext(BaseDataBean data) {
                super.onNext(data);

                L.d("UserInfoMoment", " onNext : ");

                if (updateUserInfoView != null) {
                    updateUserInfoView.sortImagesSuccess();
                }
            }

        });

    }

}
