package com.thel.modules.live.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by waiarl on 2017/12/4.
 * 进入总结阶段 服务器发送 总结剩余时间 和 双方助攻前三名 给双方直播室内的所有人
 * 总结的数据bean
 */

public class LivePkSummaryNoticeBean extends BaseLivePkBean implements Serializable {

    public static final String NO_LOSER = "no_loser";

    public String loser;// 失败者的userId，平局: "no_loser"
    public int leftTime;//总结剩余时间
    public List<LivePkAssisterBean> assisters = new ArrayList<>();
}
