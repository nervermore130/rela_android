package com.thel.modules.main.settings;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.Nullable;

import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.tencent.mm.opensdk.modelmsg.SendAuth;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.base.BaseDataBean;
import com.thel.bean.me.MyCircleActivity;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.manager.ActivityManager;
import com.thel.modules.login.login_register.LoginRegisterActivity;
import com.thel.modules.main.MainActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.utils.BusinessUtils;
import com.thel.utils.CacheCleanUtils;
import com.thel.utils.DialogUtil;
import com.thel.utils.JsonUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SharedPrefUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;

import org.json.JSONObject;

import java.io.File;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import me.rela.rf_s_bridge.RfSBridgePlugin;

/**
 * 系统设置页面
 * Created by lingwei on 2017/9/23.
 */

public class SettingActivity extends BaseActivity {
    public static final String TAG_SETTINGS = "settingsPrint";
    private TextView txt_bindstatus_phone;//绑定手机
    private TextView txt_phone_number;
    private TextView txt_bindstatus_wx; //绑定微信
    private TextView txt_bindstatus_fb;//绑定facabook
    private RelativeLayout rel_push_setting; // 推送设定
    private RelativeLayout rel_video_setting; // 视频设定
    private RelativeLayout rel_sound_setting; // 声音设定
    private RelativeLayout rel_my_setting; // 我的设定
    private RelativeLayout rel_password; // 账号密码
    private RelativeLayout rel_cleardata; // 清理缓存
    private TextView clearDataTxt; // 清理缓存
    private RelativeLayout rel_goto_udesk; // 跳转Udesk

    private RelativeLayout rel_quit; // 退出账号
    private LinearLayout lin_back; // back键
    private RelativeLayout rel_language_setting;//语言设置

    private RelativeLayout rel_mask;
    private String wxId;
    private IWXAPI wxApi;
    private String fbId;
    private LinkWxReceiver linkWxReceiver;

    private CallbackManager callbackManager;
    // 1:手机号 2:微信 3:facebook 4:微博
    private int rebindType = 0;
    private Handler mHandler = new Handler(Looper.getMainLooper());
    private RelativeLayout rel_help;
    private RelativeLayout rel_circle;
    private String ratio;
    public static boolean needRefreshMyRatio = false;
    private RelativeLayout rel_red_point_prompt;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_activity_layout);
        findViewById();
        initData();
        setLinstener();
        progressBusiness();
        registerReceiver();
    }

    private void initData() {
        FacebookSdk.setApplicationId(TheLConstants.FACE_BOOK_ID);
        FacebookSdk.sdkInitialize(TheLApp.getContext().getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        wxApi = WXAPIFactory.createWXAPI(TheLApp.getContext(), TheLConstants.WX_APP_ID, true);
        wxApi.registerApp(TheLConstants.WX_APP_ID);
        boolean show = ShareFileUtils.getBoolean(ShareFileUtils.IS_FIRST_SHOW_MASK, false);
        if (show) {
            rel_mask.setVisibility(View.GONE);
        }

    }

    private void registerReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(TheLConstants.BROADCAST_LINK_WX_SUCCEED);
        linkWxReceiver = new LinkWxReceiver();
        registerReceiver(linkWxReceiver, intentFilter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(linkWxReceiver);
        } catch (IllegalArgumentException e) {
        }
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    // 朋友圈消息的广播接收器
    private class LinkWxReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            wxId = intent.getStringExtra("openid");
            final String access_token = intent.getStringExtra("access_token");
            int sex = intent.getIntExtra("sex", 2);

            if (sex == 1) {// 如果是男性
                final DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                };
                final DialogInterface.OnClickListener listener1 = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        linkWx(access_token);
                    }
                };
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        DialogUtil.getInstance().showConfirmDialog(SettingActivity.this, "", getString(R.string.register_activity_test_hint), getString(R.string.i_am_a_lady), getString(R.string.info_quit), listener1, listener);
                    }
                });
                return;
            }
            linkWx(access_token);

        }
    }

    private void linkWx(String access_token) {
        if (!TextUtils.isEmpty(wxId) && !TextUtils.isEmpty(access_token)) {
            showLoadingNoBack();
            rebindType = 2;
            RequestBusiness.getInstance().rebind("wx", "", "", "", wxId, access_token, null).onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
                @Override
                public void onNext(BaseDataBean data) {
                    super.onNext(data);

                    if (rebindType == 2) {//绑定微信
                        closeLoading();

                        if (data.errcode.equals("has_binded_by_others")) {

                        } else {

                            DialogUtil.showToastShort(SettingActivity.this, getString(R.string.snsaccess_activity_binding));

                            ShareFileUtils.setString(ShareFileUtils.BIND_WX, wxId);
                            txt_bindstatus_wx.setText(TheLApp.getContext().getString(R.string.snsaccess_activity_binding));
                            txt_bindstatus_wx.setTextColor(0xff4BBABC);
                            txt_bindstatus_wx.setBackgroundResource(R.drawable.bg_border_shape_transparent);
                            refreshNeedCompleteUserInfo();
                            SettingActivity.this.setResult(RESULT_OK);
                        }
                    }
                }

                @Override
                public void onError(Throwable t) {
                    super.onError(t);
                    closeLoading();

                }

            });
        }
    }

    private void progressBusiness() {
        setBindStateUi();
        updateCacheSize();
    }

    /**
     * 计算缓存大小，并刷新显示（只统计universal image loader缓存的图片）
     */
    private void updateCacheSize() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    final long length = CacheCleanUtils.getTotalSizeOfFilesInDir(new File(getExternalCacheDir().getPath())) + CacheCleanUtils.getTotalSizeOfFilesInDir(new File(getCacheDir().getPath())) + CacheCleanUtils.getTotalSizeOfFilesInDir(new File(TheLConstants.F_THEL_ROOTPATH));
                    mHandler.post(new Runnable() {

                        @Override
                        public void run() {
                            clearDataTxt.setText(getString(R.string.setting_activity_cleardata) + " (" + length / (1024 * 1024) + "MB)");
                        }
                    });
                } catch (Exception e) {

                }
            }
        }).start();
    }

    private void setBindStateUi() {
        String cell = ShareFileUtils.getString(ShareFileUtils.BIND_CELL, "");
        if (!TextUtils.isEmpty(cell) && !"null".equals(cell)) {// 已绑定手机
            txt_bindstatus_phone.setText(TheLApp.getContext().getString(R.string.snsaccess_activity_change));
            txt_bindstatus_phone.setTextColor(0xff4BBABC);
            txt_bindstatus_phone.setBackgroundResource(R.drawable.bg_border_shape_transparent);
            if (cell.startsWith("+"))
                txt_phone_number.setText(cell);
            else {
                txt_phone_number.setText("+" + cell);
            }
            rel_mask.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(ShareFileUtils.getString(ShareFileUtils.BIND_WX, ""))) {// 已绑定微信
            txt_bindstatus_wx.setText(TheLApp.getContext().getString(R.string.snsaccess_activity_binding));
            txt_bindstatus_wx.setTextColor(0xff4BBABC);
            txt_bindstatus_wx.setBackgroundResource(R.drawable.bg_border_shape_transparent);
            rel_mask.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(ShareFileUtils.getString(ShareFileUtils.BIND_FB, ""))) {// 已绑定fb
            txt_bindstatus_fb.setText(TheLApp.getContext().getString(R.string.snsaccess_activity_binding));
            txt_bindstatus_fb.setTextColor(0xff4BBABC);
            txt_bindstatus_fb.setBackgroundResource(R.drawable.bg_border_shape_transparent);
            rel_mask.setVisibility(View.GONE);
        }

    }

    private void findViewById() {
        rel_quit = this.findViewById(R.id.rel_quit); // 退出账号
        txt_bindstatus_phone = this.findViewById(R.id.txt_bindstatus_phone);
        txt_phone_number = this.findViewById(R.id.txt_phone_number);
        txt_bindstatus_wx = this.findViewById(R.id.txt_bindstatus_wx);
        txt_bindstatus_fb = this.findViewById(R.id.txt_bindstatus_fb);
        rel_push_setting = findViewById(R.id.rel_push_setting);
        rel_video_setting = this.findViewById(R.id.rel_video_setting);
        rel_sound_setting = this.findViewById(R.id.rel_sound_setting);
        rel_my_setting = this.findViewById(R.id.rel_my_setting);
        rel_password = this.findViewById(R.id.rel_password); // 账号密码
        rel_cleardata = this.findViewById(R.id.rel_cleardata); // 清理缓存
        // rel_goto_udesk = (RelativeLayout) this.findViewById(R.id.rel_goto_udesk); // 跳转Udesk
        clearDataTxt = this.findViewById(R.id.clear_data_txt); // 清理缓存
        rel_quit = this.findViewById(R.id.rel_quit); // 退出账号
        lin_back = this.findViewById(R.id.lin_back); // back键
        rel_mask = this.findViewById(R.id.rel_mask);
        rel_circle = this.findViewById(R.id.rel_circle);//我的关系
        rel_language_setting = this.findViewById(R.id.rel_language_setting);
        rel_red_point_prompt = findViewById(R.id.rel_red_point_prompt);
        // rel_help = (RelativeLayout) this.findViewById(R.id.rel_help);
        if (ShareFileUtils.isGlobalVersion()) {// 国际版先隐藏微信的入口
            findViewById(R.id.border_wx).setVisibility(View.GONE);
            findViewById(R.id.rel_wx).setVisibility(View.GONE);
            final RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            params.topMargin = Utils.dip2px(this, 180);
            rel_mask.setLayoutParams(params);
        } else if (!ShareFileUtils.isGlobalVersion()) {// 简体中文隐藏facebook的绑定按钮
            findViewById(R.id.border_fb).setVisibility(View.GONE);
            findViewById(R.id.rel_fb).setVisibility(View.GONE);
            final RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            params.topMargin = Utils.dip2px(this, 180);
            rel_mask.setLayoutParams(params);
        }
    }

    private void setLinstener() {
        rel_quit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                displayConfirmDialog();
            }
        });

        rel_circle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i2 = new Intent(SettingActivity.this, MyCircleActivity.class);
                startActivity(i2);
            }
        });
        rel_mask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rel_mask.setVisibility(View.GONE);
                ShareFileUtils.setBoolean(ShareFileUtils.IS_FIRST_SHOW_MASK, true);
            }
        });
        lin_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                boolean needShowUpdateView = ShareFileUtils.getBoolean(ShareFileUtils.needShowUpdateView, false);
                if (needShowUpdateView) {
                    Intent intent = new Intent(SettingActivity.this, MainActivity.class);
                    intent.putExtra(TheLConstants.BUNDLE_KEY_WHERE_TO_GO, TheLConstants.MainFragmentPageConstants.FRAGMENT_ME);
                    startActivity(intent);
                    ShareFileUtils.setBoolean(ShareFileUtils.needShowUpdateView, false);
                }
                SettingActivity.this.finish();
            }
        });
        txt_bindstatus_phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewUtils.preventViewMultipleClick(view, 2000);
                rel_mask.setVisibility(View.GONE);
                if (getString(R.string.snsaccess_activity_change).equals(txt_bindstatus_phone.getText().toString())) {
                    DialogUtil.showConfirmDialog(SettingActivity.this, "", getString(R.string.setting_activity_confirm_cell), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            bindCell();
                        }
                    });
                } else {
                    bindCell();
                }
            }
        });
        txt_bindstatus_wx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                rel_mask.setVisibility(View.GONE);
                if (!wxApi.isWXAppInstalled()) {
                    DialogUtil.showToastShort(SettingActivity.this, getString(R.string.default_activity_wx_uninstalled));
                    return;
                }

                if (getString(R.string.snsaccess_activity_binding).equals(txt_bindstatus_wx.getText().toString())) {
                    DialogUtil.showConfirmDialog(SettingActivity.this, "", getString(R.string.setting_activity_confirm_wx), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            bindWx();
                        }
                    });
                } else {
                    bindWx();
                }
            }
        });
        rel_language_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                startActivity(new Intent(SettingActivity.this, LanguageSettingActivity.class));
            }
        });
        final LoginButton fbLogin = new LoginButton(this);
        fbLogin.setReadPermissions("public_profile");
        fbLogin.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(final JSONObject jsonObject, GraphResponse graphResponse) {
                        // FB性别： male or female
                        String gender = JsonUtils.getString(jsonObject, "gender", "");
                        if ("male".equals(gender)) {
                            final DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            };
                            final DialogInterface.OnClickListener listener1 = new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    showLoadingNoBack();
                                    fbId = JsonUtils.getString(jsonObject, "id", "");
                                    rebindType = 3;
                                    RequestBusiness.getInstance().rebind("fb", "", "", "", fbId, loginResult.getAccessToken().getToken(), null)
                                            .onBackpressureDrop()
                                            .subscribeOn(Schedulers.io())
                                            .observeOn(AndroidSchedulers.mainThread())
                                            .subscribe(new InterceptorSubscribe<BaseDataBean>() {
                                                @Override
                                                public void onNext(BaseDataBean data) {
                                                    super.onNext(data);

                                                    if (rebindType == 3) {//绑定fb
                                                        closeLoading();

                                                        if (data.errcode.equals("has_binded_by_others")) {
                                                        } else {

                                                            DialogUtil.showToastShort(SettingActivity.this, getString(R.string.snsaccess_activity_binding));
                                                            ShareFileUtils.setString(ShareFileUtils.BIND_FB, fbId);
                                                            txt_bindstatus_fb.setText(TheLApp.getContext().getString(R.string.snsaccess_activity_binding));
                                                            txt_bindstatus_fb.setTextColor(0xff4BBABC);
                                                            txt_bindstatus_fb.setBackgroundResource(R.drawable.bg_border_shape_transparent);
                                                            refreshNeedCompleteUserInfo();
                                                            SettingActivity.this.setResult(RESULT_OK);
                                                        }

                                                    }
                                                }
                                            });
                                }
                            };
                            DialogUtil.showConfirmDialog(SettingActivity.this, "", getString(R.string.register_activity_test_hint), listener1, listener);
                            LoginManager.getInstance().logOut();
                            return;
                        }
                        showLoadingNoBack();
                        fbId = JsonUtils.getString(jsonObject, "id", "");
                        rebindType = 3;
                        RequestBusiness.getInstance().rebind("fb", "", "", "", fbId, loginResult.getAccessToken().getToken(), null)
                                .onBackpressureDrop()
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(new InterceptorSubscribe<BaseDataBean>() {
                                    @Override
                                    public void onNext(BaseDataBean data) {
                                        super.onNext(data);
                                        if (rebindType == 3) {//绑定fb
                                            closeLoading();

                                            if (data.errcode.equals("has_binded_by_others")) {
                                            } else {
                                                ShareFileUtils.setString(ShareFileUtils.BIND_FB, fbId);
                                                txt_bindstatus_fb.setText(TheLApp.getContext().getString(R.string.snsaccess_activity_binding));
                                                txt_bindstatus_fb.setTextColor(0xff4BBABC);
                                                txt_bindstatus_fb.setBackgroundResource(R.drawable.bg_border_shape_transparent);
                                                refreshNeedCompleteUserInfo();
                                            }

                                        }
                                    }
                                });
                        LoginManager.getInstance().logOut();
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,link,email,gender");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException e) {
            }
        });
        txt_bindstatus_fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewUtils.preventViewMultipleClick(view, 2000);
                rel_mask.setVisibility(View.GONE);
                if (getString(R.string.snsaccess_activity_binding).equals(txt_bindstatus_fb.getText().toString())) {
                    DialogUtil.showConfirmDialog(SettingActivity.this, "", getString(R.string.setting_activity_confirm_fb), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            fbLogin.performClick();
                        }
                    });
                } else {
                    fbLogin.performClick();
                }
            }
        });
        rel_video_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                Intent intent = new Intent(SettingActivity.this, SettingsActivity.class);
                intent.putExtra(SettingsActivity.PAGE_TYPE_TAG, SettingsActivity.PAGE_TYPE_VIDEO);
                startActivity(intent);
            }
        });
        rel_sound_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                Intent intent = new Intent(SettingActivity.this, SettingsActivity.class);
                intent.putExtra(SettingsActivity.PAGE_TYPE_TAG, SettingsActivity.PAGE_TYPE_SOUND);
                startActivity(intent);
            }
        });
        rel_my_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                Intent intent = new Intent(SettingActivity.this, SettingsActivity.class);
                intent.putExtra(SettingsActivity.PAGE_TYPE_TAG, SettingsActivity.PAGE_TYPE_USER);
                startActivity(intent);

            }
        });
        rel_push_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                Intent intent = new Intent(SettingActivity.this, SettingsActivity.class);
                intent.putExtra(SettingsActivity.PAGE_TYPE_TAG, SettingsActivity.PAGE_TYPE_PUSH);
                startActivity(intent);
            }
        });
        /**
         * 清理内存
         * */
        rel_cleardata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                DialogUtil.showConfirmDialog(SettingActivity.this, getString(R.string.setting_activity_cleardata), getString(R.string.clear_cache_confirm), getString(R.string.clear_cache_clear), getString(R.string.info_no), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        arg0.dismiss();
                        showLoadingNoBack();
                        SharedPrefUtils.setString(SharedPrefUtils.FILE_AD, SharedPrefUtils.WELCOME_PAGE_AD_IMG, "");// 删除封面广告
                        CacheCleanUtils.cleanExternalCache(SettingActivity.this);
                        CacheCleanUtils.cleanInternalCache(SettingActivity.this);
                        CacheCleanUtils.deleteFilesByDirectory(TheLConstants.F_TAKE_PHOTO_ROOTPATH);
                        CacheCleanUtils.deleteFilesByDirectory(TheLConstants.F_MUSIC_ROOTPATH);
                        CacheCleanUtils.deleteFilesByDirectory(TheLConstants.F_VIDEO_ROOTPATH);
                        CacheCleanUtils.deleteFilesByDirectory(TheLConstants.F_VOICE_ROOTPATH);
                        CacheCleanUtils.deleteFilesByDirectory(TheLConstants.F_MSG_VOICE_ROOTPATH);
                        // 清除fresco缓存
                        ImagePipeline imagePipeline = Fresco.getImagePipeline();
                        imagePipeline.clearCaches();

                        Toast.makeText(SettingActivity.this, TheLApp.getContext().getString(R.string.setting_activity_clear_success), Toast.LENGTH_SHORT).show();
                        updateCacheSize();
                        closeLoading();
                    }
                });
            }
        });

        rel_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                Intent intent = new Intent(SettingActivity.this, ModifyPasswordActivity.class);
                startActivity(intent);
            }
        });
        /**
         * 红点提醒跳转
         * */
        rel_red_point_prompt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                Intent intent = new Intent(SettingActivity.this, SettingsActivity.class);
                intent.putExtra(SettingsActivity.PAGE_TYPE_TAG, SettingsActivity.PAGE_TYPE_RED_POINT);
                startActivity(intent);
            }
        });
    }

    private void refreshNeedCompleteUserInfo() {
        Intent intent = new Intent();
        intent.setAction(TheLConstants.BROADCAST_REFRESH_NEED_COMPLETE_USER_INFO);
        TheLApp.getContext().sendBroadcast(intent);
    }

    private void bindWx() {
        SendAuth.Req req = new SendAuth.Req();
        req.scope = "snsapi_userinfo";
        req.state = "thel_link";
        wxApi.sendReq(req);
    }

    private void bindCell() {
        //打开注册页面
        Intent intent = new Intent(SettingActivity.this, LoginRegisterActivity.class);
        String cell = ShareFileUtils.getString(ShareFileUtils.BIND_CELL, "");
        if (!TextUtils.isEmpty(cell) && !"null".equals(cell)) {// 已绑定手机
            intent.putExtra(LoginRegisterActivity.TYPE, LoginRegisterActivity.BUNDLE_CHANGE);  //更换绑定手机号码
        } else {
            intent.putExtra(LoginRegisterActivity.TYPE, LoginRegisterActivity.BUNDLE); //绑定手机
        }
        startActivityForResult(intent, TheLConstants.BUNDLE_CODE_SMS_REGISTER);
    }

    private void displayConfirmDialog() {
        DialogUtil.showConfirmDialog(this, "", TheLApp.getContext().getString(R.string.setting_activity_logout_dialog), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                RequestBusiness.getInstance().loadUserLogout().onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>());
                BusinessUtils.logoutAndClearUserData();
                RfSBridgePlugin.getInstance().activeRpc("EventLogout", "", null);

                Intent intent = new Intent(TheLApp.context, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                TheLApp.context.startActivity(intent);

                removeNotification();
            }
        });
    }

    /**
     * 通知栏上的消息消失
     */
    private void removeNotification() {
        try {
            NotificationManager mgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            if (mgr != null) {
                mgr.cancelAll();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (data != null) {
                String phone = data.getStringExtra("phone");
                String country = data.getStringExtra("country");
                if (!TextUtils.isEmpty(phone) && !TextUtils.isEmpty(country) && !"null".equals(phone) && !"null".equals(country)) {
                    phone = "+" + country + " " + phone;
                    ShareFileUtils.setString(ShareFileUtils.BIND_CELL, phone);
                    txt_bindstatus_phone.setText(TheLApp.getContext().getString(R.string.snsaccess_activity_change));
                    txt_bindstatus_phone.setTextColor(0xff4BBABC);
                    txt_bindstatus_phone.setBackgroundResource(R.drawable.bg_border_shape_transparent);
                    if (!TextUtils.isEmpty(phone)) {
                        if (phone.startsWith("+"))
                            txt_phone_number.setText(phone);
                        else
                            txt_phone_number.setText("+" + phone);
                    }
                    refreshNeedCompleteUserInfo();
                }
            }
        }
    }
}
