package com.thel.modules.main.home.follow;

import android.content.Context;

import com.thel.base.BasePresenter;
import com.thel.base.BaseView;
import com.thel.bean.RecommendListBean;
import com.thel.bean.live.LiveFollowingUsersBean;
import com.thel.bean.moments.MomentsCheckBean;
import com.thel.bean.moments.MomentsListBean;

import java.util.List;

/**
 * Created by liuyun on 2017/9/20.
 */

public class FollowContract {

    interface View extends BaseView<FollowContract.Presenter> {

        boolean isActive();

        void showMomentList(MomentsListBean momentsListBean);

        void showFollowUserLiveList(LiveFollowingUsersBean bean);

        void refreshData();

        void newFollowingMoments(MomentsCheckBean data);

        void releaseFailed();

        void loadDataFailed();

        void showRecommendUser(List<RecommendListBean.GuideDataBean> list);

        /**
         * 下载完apk后，通知Fragment去应用设置页面允许更新第三方渠道的apk
         *
         * @param apkUrl
         */
        void gotoAPKSetting(String apkUrl);

    }

    interface Presenter extends BasePresenter {

        void loadData(String curPage);

        void loadFollowingUsersLiveData();

        void register(Context context);

        void unRegister(Context context);

        void getUnreadMomentInfo();

        void getRecommendUserList();

    }


}
