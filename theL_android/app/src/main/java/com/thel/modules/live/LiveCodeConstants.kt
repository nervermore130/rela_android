package com.thel.modules.live

class LiveCodeConstants {

    companion object {
        const val UI_EVENT_UPDATE_AUDIENCE_COUNT = 0
        const val UI_EVENT_REFRESH_MSGS = 1
        const val UI_EVENT_SCROLL_TO_BOTTOM = 2
        const val UI_EVENT_CLEAR_INPUT = 3
        const val UI_EVENT_LIVE_CLOSED = 4
        const val UI_EVENT_AUTO_PING = 5
        const val UI_EVENT_RESET_IS_BOTTOM = 6
        const val UI_EVENT_BEEN_BLOCKED = 7
        const val UI_EVENT_HIDE_PREVIEW_IMAGE = 8
        const val UI_EVENT_LIVE_ERROR = 9
        const val UI_EVENT_SHOW_CACHING = 13
        const val UI_EVENT_HIDE_CACHING = 14
        const val UI_EVENT_UPDATE_BROADCASTER_NET_STATUS = 15
        const val UI_EVENT_UPDATE_MY_NET_STATUS_BAD = 16
        const val UI_EVENT_UPDATE_MY_NET_STATUS_GOOD = 17
        const val UI_EVENT_SPEAK_TO_FAST = 18
        const val UI_EVENT_GIFT_RECEIVE_MSG = 19//接受到收到礼物的消息广播
        const val UI_EVENT_GIFT_SEND_RESULT_MSG = 20//发送礼物返回结果消息
        const val UI_EVENT_LIVE_COMPELETE = 21
        const val UI_EVENT_DANMU_RECEIVE_MSG = 23//弹幕消息
        const val UI_EVENT_OPEN_INPUT_TRUE = 25//解锁并清空输入框,
        const val UI_EVENT_OPEN_INPUT_FALSE = 26//解锁但不清空输入框
        const val UI_EVENT_JOIN_USER = 27//有人加入直播间
        const val UI_EVENT_JOIN_VIP_USER = 28//有人加入直播间
        const val UI_EVENT_RECONNECTING = 30//重连
        const val UI_EVENT_CHANNEL_ID = 31//缺少房间号
        const val UI_EVENT_REQUEST = 32//异常请求
        const val UI_EVENT_USER_NAME = 33//需要昵称
        const val UI_EVENT_USER_ID = 34//需要id
        const val UI_EVENT_SHOW_PROGRESSBAR = 35//显示progressbar
        /***一下为直播新加的一些event */
        const val UI_EVENT_PK_START_NOTICE = 40//PK开始消息
        const val UI_EVENT_PK_SUMMARY_NOTICE = 41//Pk总结阶段 通知
        const val UI_EVENT_PK_STOP_NOTICE = 42//收到PK结束 通知
        const val UI_EVENT_PK_GEM_NOTICE = 43//主播收到礼物时 服务器发送主播软妹币增量通知给双方直播室内的所有人
        const val UI_EVENT_PK_INIT_MSG = 54//PK init 消息，用于断线重连
        const val UI_EVENT_PK_HANGUP_NOTICE = 55//挂断消息

        const val UI_EVENT_LINK_MIC_BUSY = 56
        const val UI_EVENT_LINK_MIC_STOP = 57
        const val UI_EVENT_TOP_GIFT = 58

        /***************4.8.0多人连麦 */
        const val UI_EVENT_REFRESH = 59//多人连麦服务端广播
        const val UI_EVENT_REFRESH_SEAT = 61//上麦，下麦，闭麦时，刷新座位
        const val UI_EVENT_INIT_MULTI = 62//多人连麦进直播间初始化
        const val UI_EVENT_GUEST_GIFT = 63//赠送嘉宾礼物通知
        const val UI_EVENT_START_ENCOUNTER = 64//开始相遇倒计时
        const val UI_EVENT_END_ENCOUNTER = 65//结束相遇
        const val UI_EVENT_ENCOUNTER_SB = 66//点击配对某人
        const val UI_EVENT_ENCOUNTER_SPEAKERS = 67//说话人音量
        /**
         * 4.8.2排麦
         */
        const val UI_EVENT_MIC_ON = 68//排麦开启
        const val UI_EVENT_MIC_OFF = 69//关闭排麦
        const val UI_EVENT_MIC_ADD = 70//排麦列表增加
        const val UI_EVENT_MIC_DEL = 71//排麦列表减少
        const val UI_EVENT_MIC_REQ = 72//请求上麦
        const val UI_EVENT_MIC_CANCEL = 73//取消上麦
        const val UI_EVENT_MIC_AGREE = 74//同意上麦
        const val UI_EVENT_MIC_LIST = 75//获取排麦列表

        const val UI_EVENT_AUDIENCE_LINK_MIC_RESPONSE = 76//观众连麦响应
        const val UI_EVENT_AUDIENCE_BEAM_AVAILABLE_LEVEL_SIX = 77//等级低于6级无法申请连麦
        /**
         * 获取观众连麦列表
         */
        const val UI_EVENT_AUDIENCE_LINK_MIC_LIST = 78

        const val UI_EVENT_AUDIENCE_REQUEST_SUCCESS = 79

        const val UI_EVENT_GIFT_SEND_SUCCESS_MSG = 80//发送礼物返回结果消息

        /**
         * 嘉宾离开频道
         */
        const val UI_EVENT_GUEST_LEVEL_CHANNEL = 81
        /**
         * 观众上麦
         */
        const val UI_EVENT_GUEST_ON_SEAT = 82

        /**
         * 观众下麦
         */
        const val UI_EVENT_GUEST_OFF_SEAT = 83

        /**
         * 免费弹幕消息
         */
        const val UI_EVENT_FREE_BARRAGE = 84
        /**
         * 上麦身份验证
         */
        const val UI_EVENT_GUEST_ONSEAR_AUTH = 85

        /**
         * 系统送豆
         */
        const val UI_EVENT_FREE_GOLD = 86

        /**
         * 脸部识别
         */
        const val UI_EVENT_NO_FACE = 87
        /**
         * 观众连麦日榜列表
         */
        const val UI_EVENT_GET_TOP_TODAY_LIST = 88
        /**
         * 主播发起的日榜连麦
         */
        const val UI_EVENT_DAILY_LINK_MIC = 89

        const val UI_EVENT_LINK_MIC_IS_ACCEPT = 90

        const val UI_EVENT_LINK_MIC_CANCEL = 91

        const val UI_EVENT_TOP_TODAY = 93

        const val UI_EVENT_LINK_MIC_HANGUP = 92//挂断消息

        const val UI_EVENT_STOP_SHOW = 94

        const val UI_EVENT_REFRESH_ROOM = 94//刷新直播间

        const val UI_EVENT_AUDIENCE_LINK_MIC_TIME_OUT = 170

        const val UI_EVENT_ANCHOR_UPDATE_AUDIENCE_COUNT = 10


        const val UI_EVENT_ANCHOR_REFRESH_MSGS = 11
        const val UI_EVENT_ANCHOR_SCROLL_TO_BOTTOM = 12
        const val UI_EVENT_ANCHOR_RESET_IS_BOTTOM = 113
        const val UI_EVENT_ANCHOR_AUTO_PING = 114
        const val UI_EVENT_ANCHOR_UPDATE_MY_NET_STATUS_BAD = 116
        const val UI_EVENT_ANCHOR_UPDATE_MY_NET_STATUS_GOOD = 118
        const val UI_EVENT_ANCHOR_STOP_SHOW = 117
        const val UI_EVENT_ANCHOR_LIVE_CONNECTING = 121//直播连接中
        const val UI_EVENT_ANCHOR_LIVE_CONNECT_BREAK = 22//直播连接中断
        const val UI_EVENT_ANCHOR_LIVE_CONNECT_FAILED = 123//直播连接失败
        const val UI_EVENT_ANCHOR_LIVE_CONNECT_SUCCESS = 24//直播连接成功
        const val UI_EVENT_ANCHOR_GIFT_RECEIVE_MSG = 125//发送礼物返回结果消息
        const val UI_EVENT_ANCHOR_DANMU_RECEIVE_MSG = 126//接受弹幕消息
        const val UI_EVENT_ANCHOR_JOIN_USER = 127//有人加入直播间
        const val UI_EVENT_ANCHOR_JOIN_VIP_USER = 128//vip用户入直播间
        const val UI_EVENT_ANCHOR_CLOSE_DIALOG = 29
        const val UI_EVENT_ANCHOR_PROGRESSBAR_GONE = 130//progressbar gone
        const val UI_EVENT_ANCHOR_FINISH = 131//finish();
        const val UI_EVENT_ANCHOR_LIVE_DISCONNECTED = 132//断开连接
        const val UI_EVENT_ANCHOR_LIVE_NETWORK_WARNING_NOTIFY = 133//网警提示
        const val UI_EVENT_ANCHOR_LIVE_STREAM_FRAME_SEND_SLOW = 134//网络状态不佳
        const val UI_EVENT_ANCHOR_ANCHOR_CLEAR_INPUT = 135
        const val UI_EVENT_ANCHOR_ANCHOR_OPEN_INPUT_TRUE = 36//解锁并清空输入框,
        const val UI_EVENT_ANCHOR_OPEN_INPUT_FALSE = 37//解锁但不清空输入框

        /***一下为直播新加的一些event */
        const val UI_EVENT_ANCHOR_PK_REQUEST_NOTICE = 140//收到PK申请通知
        const val UI_EVENT_ANCHOR_PK_RESPONSE_NOTICE = 141//收到PK回复通知
        const val UI_EVENT_ANCHOR_PK_START_NOTICE = 142//PK开始消息
        const val UI_EVENT_ANCHOR_PK_CANCEL_NOTICE = 143//收到PK取消通知
        const val UI_EVENT_ANCHOR_PK_HANGUP_NOTICE = 44//收到PK挂断 通知
        const val UI_EVENT_ANCHOR_PK_SUMMARY_NOTICE = 45//Pk总结阶段 通知
        const val UI_EVENT_ANCHOR_PK_STOP_NOTICE = 46//收到PK结束 通知
        const val UI_EVENT_ANCHOR_PK_GEM_NOTICE = 47//主播收到礼物时 服务器发送主播软妹币增量通知给双方直播室内的所有人
        const val UI_EVENT_ANCHOR_PK_REQUEST_PK_OK = 48//发送PK申请成功
        const val UI_EVENT_ANCHOR_PK_NOT_LIVING = 49//主播不在直播
        const val UI_EVENT_ANCHOR_PK_IN_LINKMIC = 50//主播连麦中
        const val UI_EVENT_ANCHOR_PK_IN_PK = 51//主播Pk中
        const val UI_EVENT_ANCHOR_PK_REQUEST_REJECT = 52//上次发起PK被该主播拒绝，需等待10分钟才能对该主播再次发起PK
        const val UI_EVENT_ANCHOR_LINK_MIC_RESPONSE = 155//发送连麦请求成功
        const val UI_EVENT_ANCHOR_PK_REQUEST_TIME_OUT = 154//PK申请超时
        const val UI_EVENT_ANCHOR_LINK_MIC_REQUEST = 53
        const val UI_EVENT_ANCHOR_LINK_MIC_IS_ACCEPT = 157
        const val UI_EVENT_ANCHOR_PK_INIT_MSG = 156//PK init 消息，用于断线重连
        const val UI_EVENT_ANCHOR_PK_LOWER_VERSION = 158//PK init 消息，用于断线重连
        const val UI_EVENT_ANCHOR_PK_CODE_500 = 159//PK 申请错误码500
        const val UI_EVENT_ANCHOR_TOP_GIFT = 60//全服大礼物
        const val UI_EVENT_ANCHOR_LOCK_INPUT = 178//锁定输入框
        const val UI_EVENT_ANCHOR_BEEN_BLOCKED = 164
        const val UI_EVENT_ANCHOR_SPEAK_TO_FAST = 166
        const val UI_EVENT_ANCHOR_ANDIENCE_NOT_IN = 100//观众不在直播间
        const val UI_EVENT_ANCHOR_REJECT_LINK_MIC = 169//观众拒绝连麦
        /**
         * 用户在直播间升级在当前直播间通知
         */
        const val UI_EVENT_UPGRADE_EFFECTS = 200

        /**
         * 用户在直播间升级在所有直播间通知
         */
        const val UI_EVENT_ALL_UPGRADE_EFFECTS = 201
    }
}