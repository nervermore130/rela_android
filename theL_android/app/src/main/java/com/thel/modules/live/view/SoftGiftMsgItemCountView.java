package com.thel.modules.live.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by the L on 2016/6/3.
 * 收到的礼物信息的连击数View
 */
public class SoftGiftMsgItemCountView extends LinearLayout {
    private Context mContext;
    private int childWidth = Utils.dip2px(TheLApp.getContext(), 15);
    private int childHeight = Utils.dip2px(TheLApp.getContext(), 21);
    private List<Integer> nums = new ArrayList<>();
    private int[] resId = {R.mipmap.icn_live_giftnumber_0, R.mipmap.icn_live_giftnumber_1, R.mipmap.icn_live_giftnumber_2, R.mipmap.icn_live_giftnumber_3, R.mipmap.icn_live_giftnumber_4, R.mipmap.icn_live_giftnumber_5, R.mipmap.icn_live_giftnumber_6, R.mipmap.icn_live_giftnumber_7, R.mipmap.icn_live_giftnumber_8, R.mipmap.icn_live_giftnumber_9};

    public SoftGiftMsgItemCountView(Context context) {
        super(context);
        init(context, null);
    }

    public SoftGiftMsgItemCountView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public SoftGiftMsgItemCountView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        mContext = context;
        if (attrs != null) {
            TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.SoftGiftMsgItemCountView);
            childWidth = typedArray.getDimensionPixelSize(R.styleable.SoftGiftMsgItemCountView_child_width, 15);
            childHeight = typedArray.getDimensionPixelSize(R.styleable.SoftGiftMsgItemCountView_child_width, 21);
            typedArray.recycle();
        }

        setOrientation(LinearLayout.HORIZONTAL);
        ImageView imgX = new ImageView(mContext);
        imgX.setImageResource(R.mipmap.icn_live_giftnumber_x);
        addView(imgX);
        setLayout(imgX);

    }

    /**
     * 刷新连击数
     *
     * @param count 连击数
     */
    public void refreshCombo(int count) {
        getNums(count);// 获取连击数的每一个数（倒序）集合
        int childCount = getChildCount();
        for (int i = 1; i < childCount; i++) {//删除第一个以外的子控件
            removeView(getChildAt(1));
        }
        for (int i = 0; i < nums.size(); i++) {//添加新控件
            ImageView iv = new ImageView(mContext);
            if (getResourceId(nums.get(nums.size() - i - 1)) != 0) {
                iv.setImageResource(getResourceId(nums.get(nums.size() - i - 1)));
                addView(iv);
                setLayout(iv);
            }
        }
    }


    /**
     * 给子ImageView设置宽高
     *
     * @param img 子控件
     */
    private void setLayout(ImageView img) {
        img.setLayoutParams(new LayoutParams(childWidth, childHeight));
    }

    /**
     * 获取连击数的每一个数（倒序）
     *
     * @param count 连击数
     */
    private void getNums(int count) {
        nums.clear();
        while (count > 0) {
            nums.add(count % 10);
            count = count / 10;
        }
    }

    /**
     * 获取资源Id
     *
     * @param num 数
     * @return 资源Id
     */
    private int getResourceId(int num) {
        return resId[num % resId.length];
    }

}
