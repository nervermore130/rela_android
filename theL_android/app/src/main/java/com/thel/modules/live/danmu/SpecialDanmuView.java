package com.thel.modules.live.danmu;

import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.live.bean.DanmuBean;
import com.thel.ui.widget.DanmuSelectedLayout;
import com.thel.ui.widget.LevelImageView;

public class SpecialDanmuView implements CreateDanmu {
    @Override public View createDanmuView(ViewGroup viewGroup, DanmuBean danmuBean) {
        return initSpecialDanmuView(viewGroup, danmuBean);
    }

    private View initSpecialDanmuView(ViewGroup viewGroup, DanmuBean danmuBean) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_danmu_special, viewGroup, true);

        RelativeLayout bg_rl = view.findViewById(R.id.bg_rl);

        ImageView user_avatar_iv = view.findViewById(R.id.user_avatar_iv);

        LevelImageView level_iv = view.findViewById(R.id.level_iv);

        TextView name_tv = view.findViewById(R.id.name_tv);

        TextView content_tv = view.findViewById(R.id.content_tv);

        LottieAnimationView animation_view = view.findViewById(R.id.animation_view);

        ImageLoaderManager.imageLoaderCircle(user_avatar_iv, R.mipmap.icon_user, danmuBean.avatar);

        level_iv.setLevel(danmuBean.userLevel);

        String nickName = danmuBean.nickName + ": ";

        name_tv.setText(nickName);

        content_tv.setText(danmuBean.content);

        int textColorRes = R.color.danmu_level_text;

        String animRes = "danmu_bottle.json";

        int bgRes = R.drawable.bg_danmu_bottle;

        if (danmuBean.barrageId.equals(DanmuSelectedLayout.TYPE_DANMU_BOTTLE)) {
            bgRes = R.drawable.bg_danmu_bottle;
            animRes = "danmu_bottle.json";
            textColorRes = R.color.danmu_level_text;
        }

        if (danmuBean.barrageId.equals(DanmuSelectedLayout.TYPE_DANMU_CAT)) {
            bgRes = R.drawable.bg_danmu_cat;
            animRes = "danmu_cat.json";
            textColorRes = R.color.danmu_cat_text;
        }

        if (danmuBean.barrageId.equals(DanmuSelectedLayout.TYPE_DANMU_CUPID)) {
            bgRes = R.drawable.bg_danmu_cupid;
            animRes = "danmu_cupid.json";
            textColorRes = R.color.danmu_cupid_text;
        }

        bg_rl.setBackgroundResource(bgRes);

        name_tv.setTextColor(ContextCompat.getColor(TheLApp.context, textColorRes));

        content_tv.setTextColor(ContextCompat.getColor(TheLApp.context, textColorRes));

        animation_view.loop(true);
        animation_view.setAnimation(animRes);
        animation_view.playAnimation();

        return new View(viewGroup.getContext());

    }
}
