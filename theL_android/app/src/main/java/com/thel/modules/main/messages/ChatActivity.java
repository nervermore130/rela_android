package com.thel.modules.main.messages;

import android.Manifest;
import android.animation.Animator;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Rect;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.ksyun.media.player.IMediaPlayer;
import com.ksyun.media.player.KSYMediaPlayer;
import com.ksyun.media.player.KSYTextureView;
import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UploadManager;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.thel.IMsgStatusCallback;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.base.BaseAdapter;
import com.thel.base.BaseDataBean;
import com.thel.bean.StickerBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.recommend.RecommendWebBean;
import com.thel.bean.user.UploadTokenBean;
import com.thel.bean.user.UserCardBean;
import com.thel.bean.user.UserCardV2Bean;
import com.thel.constants.TheLConstants;
import com.thel.db.DBConstant;
import com.thel.db.DBUtils;
import com.thel.db.table.message.MsgTable;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.imp.black.BlackUtils;
import com.thel.imp.follow.FollowStatusChangedImpl;
import com.thel.imp.follow.bean.SingleUserRelationNetBean;
import com.thel.imp.sticker.StickerContract.StickerChangedListener;
import com.thel.imp.sticker.StickerUtils;
import com.thel.manager.ActivityManager;
import com.thel.manager.ChatServiceManager;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.live.bean.LiveRoomBean;
import com.thel.modules.main.MainActivity;
import com.thel.modules.main.home.moments.ReleaseMomentActivity;
import com.thel.modules.main.me.aboutMe.StickerPackDetailActivity;
import com.thel.modules.main.me.match.MatchSuccessActivity;
import com.thel.modules.main.messages.adapter.ChatAdapter;
import com.thel.modules.main.messages.adapter.FastEmojiAdapter;
import com.thel.modules.main.messages.bean.ImageInfo;
import com.thel.modules.main.messages.bean.MsgBean;
import com.thel.modules.main.messages.bean.UploadTokenConsumerBean;
import com.thel.modules.main.messages.db.MessageDataBaseAdapter;
import com.thel.modules.main.messages.utils.MsgUtils;
import com.thel.modules.main.messages.utils.PushUtils;
import com.thel.modules.main.messages.utils.SendMsgUtils;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.modules.preview_image.UserInfoPhotoActivity;
import com.thel.modules.preview_map.LocationOnMapActivity;
import com.thel.modules.select_image.SelectLocalImagesActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.RequestConstants;
import com.thel.network.service.DefaultRequestService;
import com.thel.ui.ChatGuideLayout;
import com.thel.ui.decoration.FastMsgDivider;
import com.thel.ui.widget.ChatMatchTipsLayout;
import com.thel.ui.widget.ChatMomentLayout;
import com.thel.ui.widget.UnFollowTipsLayout;
import com.thel.ui.widget.emoji.PreviewGridView;
import com.thel.ui.widget.emoji.SendEmojiLayout;
import com.thel.utils.BusinessUtils;
import com.thel.utils.DeviceUtils;
import com.thel.utils.DialogUtil;
import com.thel.utils.EmojiUtils;
import com.thel.utils.ExecutorServiceUtils;
import com.thel.utils.FireBaseUtils;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.GsonUtils;
import com.thel.utils.ImageUtils;
import com.thel.utils.JsonUtils;
import com.thel.utils.L;
import com.thel.utils.LocationUtils;
import com.thel.utils.MD5Utils;
import com.thel.utils.MomentUtils;
import com.thel.utils.PermissionUtil;
import com.thel.utils.PhoneUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SharedPrefUtils;
import com.thel.utils.SizeUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.lasque.tusdk.core.http.HttpGet;
import org.lasque.tusdk.core.http.HttpResponse;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import butterknife.OnClick;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import video.com.relavideolibrary.RelaVideoSDK;
import video.com.relavideolibrary.onRelaVideoActivityResultListener;

import static com.thel.R.id.img_msg_photo_left;
import static com.thel.R.id.img_msg_photo_right;

/**
 * @author liuyun
 */
public class ChatActivity extends BaseActivity implements View.OnClickListener, View.OnLongClickListener, SensorEventListener, PreviewGridView.MyOnItemClickListener, AdapterView.OnItemClickListener, StickerChangedListener {

    private final String TAG = "ChatActivity";

    // 布局view
    private RelativeLayout rel_bottom;// 输入框、发送按钮
    private LinearLayout lin_options;// 语音、图片、视频等操作按钮L
    private RelativeLayout rel_more_bottom;
    private SendEmojiLayout sendEmojiLayout;// 表情键盘
    private TextView txt_nickname;
    private LinearLayout lin_back;
    private LinearLayout lin_more;
    private LinearLayout lin_msgs;
    private ImageView bg;
    private ListView listView;
    private ChatAdapter adapter;
    private ImageView btn_sticker;
    private EditText edit;
    private TextWatcher textWatcher;
    private TextView txt_send;
    private RelativeLayout rel_voice;
    private RelativeLayout voice_center;
    private ImageView volume;
    private ImageView volume1;
    private ImageView img_record;
    private ImageView img_cancel;
    private TextView txt_start_record_tip;
    private LinearLayout lin_voice_length;
    private TextView txt_voice_length;
    private TextView txt_cancel_record;
    // 底部更多
    private ImageView btn_gallery;
    private ImageView btn_location;
    private ImageView btn_voice;
    private ImageView btn_video;

    // 视频部分
    private KSYTextureView player;
    private ProgressBar progressbar_video;
    private ImageView img_play_video;

    private ChatMomentLayout chat_moment_layout;
    private ChatGuideLayout chat_guide_layout;
    private LinearLayout msg_hiding_tips_ll;

    private RecyclerView fast_emoji_rv;

    public static final String BROADCAST_KEY_USER_ID = "userId";
    public static final String BROADCAST_KEY_PACKET_ID = "receipt";
    private UserCardBean usercardbean;
    private static final int POLL_INTERVAL = 100;
    static final private double EMA_FILTER = 0.6;
    private final int RECORD_NO = 0; // 不在录音
    private final int RECORD_ING = 1; // 正在录音
    private final int RECODE_END = 2; // 完成录音
    private final int MAX_TIME = 60; // 最长录制时间，单位秒
    private final int MIN_TIME = 2; // 最短录制时间，单位秒
    private final int ATTENTION_TIME = 49; // 50秒开始倒计时提醒
    private final int pageSize = 20;
    private final int SELECT_PIC_LIMIT = 6;

    public static boolean needRefreshStickers = false;

    private long lastSendPendingMsgTime = 0;

    private String myUid; // 我的id
    private String myName; // 我的名字
    private String toUserId = ""; // 消息接受者的id
    private String toName; // 消息接受者的名字
    private String toMessageUser; // 消息接受者的用户名
    private String toAvatar; // 接收者的头像
    private int isSystem = MsgBean.IS_NORMAL_MSG; // 是否是系统消息(wink, follow)
    private String fromPage;
    private ArrayList<MsgBean> msglist = new ArrayList<>(); // 消息列表
    private ArrayList<String> photos = new ArrayList<>();// 聊天中包含的所有图片URL
    // 存储正在发送的视频消息
    private ArrayList<MsgBean> sendingVideoMsgs = new ArrayList<>();

    /**
     * 视频部分
     */
    private int playingVideoPosition = -1;

    // 录音相关
    private int record_state = 0; // 录音的状态
    private MediaRecorder mRecorder; // 录音机
    private int recordTime = 1; // 录音的时长，初始时长为1秒，因为结束录音会延迟1秒（为了避免录AAC丢最后录音的bug）
    private Thread recordThread;
    private String mFileName; // 录音文件名
    // 播放相关
    private MediaPlayer mediaPlayer; // 播放器
    private boolean playing = false; // 播放状态
    private MsgBean msgBeanPre; // 上次正在播放的msgBean
    // 消息相关
    private ChatReceiver receiver; // 消息广播接收器
    private boolean needRefresh = true;
    private int startIndex = 0;
    // 控制分页加载,标识是否已经加载了全部
    private boolean canLoadMore = true;
    private int defaultSoftInputHeight = Utils.dip2px(TheLApp.getContext(), 200);
    private AudioManager audioManager;
    private SensorManager mSensorManager;
    private Sensor mSensor;
    private Bitmap bgBitmap;
    private long lastPressTime = 0;
    private ScaleAnimation scaleAnim;
    private float volumeScale = 1.0f;
    private ScaleAnimation scaleAnim1;
    private float volumeScale1 = 1.0f;
    private double mEMA = 0.0;
    private ImageInfo imgInfo;
    // 由于录完一条语音后需要delay一秒，所以在这一秒期间必须禁用语音按钮的触摸事件
    private boolean disableTouch = false;

    public static final int FROM_PAGE_WEBVIEW = 1;//推荐网页
    public static final int FROM_PAGE_LIVE_USER = 2;//推荐主播

    private int FROM_PAGE = 0;

    private MomentsBean oldMomentBean;
    private UserCardV2Bean.UserCardV2DataBean simpleUserBean;
    private LottieAnimationView img_sendwink;

    private UnFollowTipsLayout mUnFollowTipsLayout;

    private ChatMatchTipsLayout mChatMatchTipsLayout;

    private MsgBean typingMsgBean;

    private static final int HIDE_TYPING_MSG = 1;

    private boolean isPending = false;

    /**
     * 是否被禁用
     */
    private boolean isConfine = false;

    /**
     * 是否有背景
     */
    private boolean isHasBG = false;

    /**
     * 是否显示发送日志到聊天的引导
     */
    private boolean isShowMomentToChatView = false;

    /**
     * 是否开始悄悄查看
     */
    private boolean isDefaultMsgHiding = false;

    private ExecutorService sendReceiptService = Executors.newSingleThreadExecutor();

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case HIDE_TYPING_MSG:
                    hideTypingMsg();
                    break;
            }

        }
    };

    private Runnable ImgThread = new Runnable() {
        @Override
        public void run() {
            recordTime = 1;
            while (record_state == RECORD_ING) {
                if (recordTime >= MAX_TIME && MAX_TIME != 0) {
                    imgHandle.sendEmptyMessage(0);
                } else {
                    try {
                        Thread.sleep(1000);
                        recordTime += 1; // 记录录音时间
                        if (record_state == RECORD_ING) {
                            imgHandle.sendEmptyMessage(1);
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        Handler imgHandle = new Handler() {
            @Override
            public void handleMessage(android.os.Message msg) {
                switch (msg.what) {
                    case 0:
                        // 录音超过60秒自动停止
                        if (record_state == RECORD_ING) {
                            record_state = RECODE_END;
                            initVoiceLayout();
                            final String packetId = saveVoiceMsg(TheLConstants.F_MSG_VOICE_ROOTPATH + toUserId + File.separator + mFileName, recordTime + "");
                            disableTouch = true;
                            mHandler.postDelayed(new Runnable() {// 延迟一秒再停止录音，否则会丢失语音

                                @Override
                                public void run() {

                                    stopRecording();

                                    uploadFile(TheLConstants.F_MSG_VOICE_ROOTPATH + toUserId + File.separator + mFileName, packetId, 1);
                                }

                            }, 1000);
                        }
                        break;
                    case 1:
                        // 录音未到60秒时
                        //                        if (recordTime > ATTENTION_TIME) { // 50秒以后
                        //                            txt_voice_length.setText("-" + (MAX_TIME - recordTime) + "\"");
                        //                        } else { // 50秒之前
                        final int time = recordTime - 1;
                        if (time < 10)
                            txt_voice_length.setText("00:0" + (time));
                        else
                            txt_voice_length.setText("00:" + (time));
                        //                        }
                        break;
                    default:
                        break;
                }
            }
        };
    };

    private Runnable mPollTask = new Runnable() {
        public void run() {
            double amp = getAmplitude();
            updateVolume(amp);
            updateVolume1(amp);
            mHandler.postDelayed(mPollTask, POLL_INTERVAL);
            sendPendingMsg(1);
        }
    };
    private StickerUtils stickerUtils;
    private String recommendText;
    private LinearLayout rl_editext;
    private RelativeLayout lin_contact_udesk;
    private TextView txt_distance;
    private ImageView avatar;
    private int followStatus = -1;
    private String matchMsg;
    private String pageId;

    @Override
    public void onStickerChanged() {
        needRefreshStickers = true;
    }

    private class UploadTokenConsumer extends InterceptorSubscribe<UploadTokenBean> {

        UploadTokenConsumerBean bean;

        public UploadTokenConsumer(UploadTokenConsumerBean bean) {
            this.bean = bean;
        }

        @Override
        public void onNext(UploadTokenBean uploadTokenBean) {
            super.onNext(uploadTokenBean);
            uploadVideoFileToCloud(bean.packetId, bean.videoPath, bean.uploadVideoPath, uploadTokenBean.data.uploadToken, bean.type, bean.videoThumnail);
        }

        @Override
        public void onError(Throwable t) {
            super.onError(t);
            sendVideoMsgFailed(bean.packetId);
        }
    }

    private class UploadTokenConsumer1 extends InterceptorSubscribe<UploadTokenBean> {

        String packetId;
        String uploadPath;
        String localPath;

        public UploadTokenConsumer1(String packetId, String uploadPath, String localPath) {
            this.packetId = packetId;
            this.uploadPath = uploadPath;
            this.localPath = localPath;
        }

        @Override
        public void onNext(UploadTokenBean uploadTokenBean) {
            super.onNext(uploadTokenBean);
            MsgBean msgToRefreshTemp = null;
            for (int i = msglist.size() - 1; i >= 0; i--) {
                if (!TextUtils.isEmpty(msglist.get(i).packetId) && msglist.get(i).packetId.equals(packetId)) {
                    msgToRefreshTemp = msglist.get(i);
                    break;
                }
            }

            L.d("ChatServiceManager", " onNext : ");


            if (msgToRefreshTemp != null) {
                final MsgBean msgToRefresh = msgToRefreshTemp;
                if (!TextUtils.isEmpty(uploadTokenBean.data.uploadToken)) {
                    // 上传日志图片
                    UploadManager uploadManager = new UploadManager();
                    File file = new File(localPath);
                    if (!file.exists()) {
                        L.d("file path not exists");
                    }
                    uploadManager.put(file, uploadPath, uploadTokenBean.data.uploadToken, new UpCompletionHandler() {
                        @Override
                        public void complete(String key, ResponseInfo info, JSONObject response) {

                            if (info != null && info.statusCode == 200 && uploadPath.equals(key)) {

                                // 发送语音或图片url
                                msgToRefresh.msgText = RequestConstants.FILE_BUCKET + key;
                                // 发送声音消息
                                sendMessage(msgToRefresh, TheLConstants.MsgSendingStatusConstants.MSG_SENDING);
                                refreshSingleItem(msgToRefresh);
                            } else {
                                msgToRefresh.msgStatus = MsgBean.MSG_STATUS_FAILED;
                                DialogUtil.showToastShort(ChatActivity.this, TheLApp.getContext().getString(R.string.message_network_error));
                                refreshSingleItem(msgToRefresh);
                            }

                            listView.setSelection(msglist.size() - 1);

                        }
                    }, null);
                } else {

                    L.d("ChatServiceManager", " file none : ");

                    msgToRefreshTemp.msgStatus = MsgBean.MSG_STATUS_FAILED;
                    DialogUtil.showToastShort(ChatActivity.this, TheLApp.getContext().getString(R.string.message_network_error));
                    final MessageDataBaseAdapter dbAdatper = MessageDataBaseAdapter.getInstance(TheLApp.getContext(), ShareFileUtils.getString(ShareFileUtils.ID, ""));
                    dbAdatper.updateMsgToFailed(toUserId, msgToRefreshTemp.packetId);
                    refreshSingleItem(msgToRefreshTemp);
                }
            }
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_chat2);

        BlackUtils.getNetBlackList();

        findViewById();

        setListener();

        needRefreshStickers = false;
        audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);

        pageId = Utils.getPageId();

        registerReceiver();// 注册收到消息时的广播接收器

        initData();

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);

        initData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(this.getClass().getName());
        MobclickAgent.onResume(this);
        if (mSensorManager != null) {
            mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_NORMAL);
        }

        // 设置背景，系统消息用『system』存储
        String bgPath = SharedPrefUtils.getString(SharedPrefUtils.FILE_NAME_CHAT_BG, toUserId, "");

        isHasBG = bgPath.length() > 0;

        if (adapter != null && msglist != null && msglist.size() > 0) {
            adapter.isHasBG(isHasBG);
            adapter.notifyDataSetChanged();
        }

        try {
            if (bgBitmap != null && !bgBitmap.isRecycled()) {
                bgBitmap.recycle();
            }
            if (!TextUtils.isEmpty(bgPath)) {
                if (bgPath.equals("reset")) {
                    bg.setImageResource(R.color.transparent);
                } else {
                    ImageLoaderManager.imageLoader(bg, R.color.white, bgPath);
                }
            } else {
                String bgPathAll = SharedPrefUtils.getString(SharedPrefUtils.FILE_NAME_CHAT_BG, SharedPrefUtils.CHAT_BG_ALL, "");
                if (!TextUtils.isEmpty(bgPathAll)) {
                    ImageLoaderManager.imageLoader(bg, R.color.white, bgPathAll);
                    bg.setImageBitmap(bgBitmap);
                } else {
                    bg.setImageResource(R.color.transparent);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (needRefreshStickers && sendEmojiLayout != null) {
            needRefreshStickers = false;
            sendEmojiLayout.refreshUI();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(this.getClass().getName());
        MobclickAgent.onPause(this);
        if (mSensorManager != null && mSensor != null) {
            mSensorManager.unregisterListener(this, mSensor);
        }
        mediaStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(receiver);
            stickerUtils.unRegisterReceiver(this);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        // 停止播放录音
        if (null != mediaPlayer) {
            try {
                mediaPlayer.release();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (audioManager != null) {
            audioManager.setMode(AudioManager.MODE_NORMAL);
        }
        if (bgBitmap != null && !bgBitmap.isRecycled()) {
            bgBitmap.recycle();
        }

        if (textWatcher != null && edit != null) {
            edit.removeTextChangedListener(textWatcher);
        }

        if (mIMsgStatusCallback != null) {
            ChatServiceManager.getInstance().unRegisterCallback(toUserId);
        }

        toUserId = null;

        L.d(TAG, " onDestroy toUserId : " + toUserId);

        ChatServiceManager.getInstance().setUserId(null);
    }

    @Override
    public void finish() {
        dismissKeyboard();
        if (edit != null && !TextUtils.isEmpty(edit.getText().toString().trim())) {// 如果输入框不为空，则保存草稿
            SharedPrefUtils.setString(SharedPrefUtils.FILE_NAME_CHAT_DRAFT, toUserId, edit.getText().toString().trim());
        } else {
            SharedPrefUtils.remove(SharedPrefUtils.FILE_NAME_CHAT_DRAFT, toUserId);
        }

        //返回message fragment，刷新聊天数据
        Intent intent = new Intent();
        intent.setAction(TheLConstants.BROADCAST_REFRESH_MESSAGE);
        sendBroadcast(intent);
        super.finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (rel_more_bottom.getVisibility() == View.VISIBLE) {
                hideBottomMore();
                return true;
            } else {
                return super.onKeyDown(keyCode, event);
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @SuppressLint("NewApi")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == TheLConstants.RESULT_CODE_TAKE_PHOTO) {

            String path = data.getStringExtra(TheLConstants.BUNDLE_KEY_IMAGE_OUTPUT_PATH);

            if (!TextUtils.isEmpty(path)) {
                imgInfo = handlePhoto(path, path.replace(TheLConstants.F_TAKE_PHOTO_ROOTPATH, ""));
                uploadFile(imgInfo.localPath, imgInfo.packetId, 0);
            } else {
                DialogUtil.showToastShort(ChatActivity.this, TheLApp.getContext().getString(R.string.info_rechoise_photo));
            }
        } else if (resultCode == TheLConstants.RESULT_CODE_SELECT_LOCAL_IMAGE) {
            String path = data.getStringExtra(TheLConstants.BUNDLE_KEY_LOCAL_IMAGE_PATH);

            if (!TextUtils.isEmpty(path)) {
                String[] urls = path.split(",");
                for (int i = 0; i < urls.length; i++) {

                    imgInfo = handlePhoto(urls[i], "i_" + ImageUtils.getPicName());
                    uploadFile(imgInfo.localPath, imgInfo.packetId, 0);
                }
            } else {
                DialogUtil.showToastShort(ChatActivity.this, TheLApp.getContext().getString(R.string.info_rechoise_photo));
            }
        } else if (resultCode == TheLConstants.RESULT_CODE_CLEAR_CHAT) {
            clearChat();
        }
    }

    @Override
    public void onAccuracyChanged(Sensor arg0, int arg1) {
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onSensorChanged(SensorEvent event) {
        try {
            if (mediaPlayer == null || !mediaPlayer.isPlaying()) {// 没播放语音的时候直接返回
                return;
            }
            if (audioManager.isWiredHeadsetOn()) {// 如果插入耳机，则直接返回
                return;
            }
            float range = event.values[0];
            if (range >= mSensor.getMaximumRange()) {
                audioManager.setMode(AudioManager.MODE_NORMAL);
                if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                    mediaStop();
                }
            } else {
                audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
                if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                    mediaStop();
                    mHandler.postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            if (msgBeanPre != null) {
                                playMedia(msgBeanPre);
                            }
                        }
                    }, 1000);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 聊天列表里各项控件的长按点击事件
     */
    @Override
    public boolean onLongClick(View v) {
        switch (v.getId()) {
            case R.id.lin_msg_voice_left:
            case R.id.lin_msg_voice_right:
            case R.id.img_msg_photo_right:
            case R.id.img_msg_photo_left:
            case R.id.img_sticker_right:
            case R.id.img_sticker_left:
            case R.id.img_msg_map_left:
            case R.id.img_msg_map_right:
            case R.id.rel_video_left:
            case R.id.rel_video_right:

                try {
                    MsgBean tempMsgBean = (MsgBean) v.findViewById(v.getId()).getTag(v.getId());
                    int firstPosition = listView.getFirstVisiblePosition();
                    if (tempMsgBean != null) {
                        handleMessageDialog(tempMsgBean, firstPosition);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
        }
        return true;
    }

    /**
     * 聊天列表里各项控件的点击事件
     */
    @OnClick
    public void onClick(final View v) {
        int viewId = v.getId();
        switch (viewId) {
            // 播放我发的录音
            case R.id.lin_msg_voice_right:
                ViewUtils.preventViewMultipleClick(v, 2000);
                if (PhoneUtils.hasSdcard()) {
                    MsgBean tempMsgBean = (MsgBean) v.findViewById(R.id.lin_msg_voice_right).getTag(R.id.lin_msg_voice_right);
                    if (!playing) { // 如果当前没播放录音
                        playMedia(tempMsgBean);
                        msgBeanPre = tempMsgBean;
                    } else { // 当前正在播放录音
                        if (mediaPlayer.isPlaying()) {
                            mediaPlayer.stop(); // 先停止
                            msgBeanPre.isPlaying = 0;
                            refreshSingleItem(msgBeanPre);
                            playing = false;
                            if (!msgBeanPre.filePath.equals(tempMsgBean.filePath)) {
                                playMedia(tempMsgBean); // 再播放下一条
                                msgBeanPre = tempMsgBean;
                            }
                        }
                    }
                    refreshSingleItem(tempMsgBean);
                } else {
                    DialogUtil.showToastShort(ChatActivity.this, TheLApp.getContext().getString(R.string.chat_activity_info_cannot_play_without_sdcard));
                }
                break;

            // 播放对方发的录音文件
            case R.id.lin_msg_voice_left:
                ViewUtils.preventViewMultipleClick(v, 2000);
                if (PhoneUtils.hasSdcard()) {
                    final MsgBean tempMsgBeanleft = (MsgBean) v.findViewById(R.id.lin_msg_voice_left).getTag(R.id.lin_msg_voice_left);
                    if (tempMsgBeanleft.hadPlayed == 0) {
                        PermissionUtil.requestStoragePermission(ChatActivity.this, new PermissionUtil.PermissionCallback() {
                            @Override
                            public void granted() {
                                DownloadVoiceAsyncTask download = new DownloadVoiceAsyncTask(tempMsgBeanleft);
                                download.executeOnExecutor(ExecutorServiceUtils.getInstatnce().getExecutorService());
                            }

                            @Override
                            public void denied() {

                            }

                            @Override
                            public void gotoSetting() {

                            }
                        });

                    } else if (tempMsgBeanleft.hadPlayed == 1) {
                        play(tempMsgBeanleft);
                    } else if (tempMsgBeanleft.hadPlayed == 2) {
                        PermissionUtil.requestStoragePermission(ChatActivity.this, new PermissionUtil.PermissionCallback() {
                            @Override
                            public void granted() {
                                DownloadVoiceAsyncTask download = new DownloadVoiceAsyncTask(tempMsgBeanleft);
                                download.executeOnExecutor(ExecutorServiceUtils.getInstatnce().getExecutorService());
                            }

                            @Override
                            public void denied() {

                            }

                            @Override
                            public void gotoSetting() {

                            }
                        });
                    }
                } else {
                    DialogUtil.showToastShort(ChatActivity.this, TheLApp.getContext().getString(R.string.chat_activity_info_cannot_play_without_sdcard));
                }
                break;
            case R.id.img_reload_voice:
                ViewUtils.preventViewMultipleClick(v, 2000);
                final MsgBean tempMsgBeanleft = (MsgBean) v.findViewById(R.id.img_reload_voice).getTag(R.id.img_reload_voice);
                PermissionUtil.requestStoragePermission(ChatActivity.this, new PermissionUtil.PermissionCallback() {
                    @Override
                    public void granted() {
                        DownloadVoiceAsyncTask download = new DownloadVoiceAsyncTask(tempMsgBeanleft);
                        download.executeOnExecutor(ExecutorServiceUtils.getInstatnce().getExecutorService());
                    }

                    @Override
                    public void denied() {

                    }

                    @Override
                    public void gotoSetting() {

                    }
                });
                break;
            // 点击头像
            case R.id.img_thumb_right:
                ViewUtils.preventViewMultipleClick(v, 2000);
                MsgBean thumbMsgBeanRight = (MsgBean) v.findViewById(R.id.img_thumb_right).getTag(R.id.img_thumb_right);
                String tempUserId = thumbMsgBeanRight.fromUserId;
//                Intent intent = new Intent();
//                intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, tempUserId);
//                intent.setClass(ChatActivity.this, UserInfoActivity.class);
//                this.startActivity(intent);
                FlutterRouterConfig.Companion.gotoUserInfo(tempUserId);
                break;
            case R.id.img_thumb_left:
                ViewUtils.preventViewMultipleClick(v, 2000);
                MsgBean thumbMsgBeanLeft = (MsgBean) v.findViewById(R.id.img_thumb_left).getTag(R.id.img_thumb_left);
                String tempUserIdLeft = thumbMsgBeanLeft.fromUserId;
                if (TheLConstants.SYSTEM_USER.equals(tempUserIdLeft) || MsgBean.MSG_TYPE_LIKE.equals(thumbMsgBeanLeft.msgType)) {// 系统通知账号不能点击，like消息不能点击
                    break;
                }
//                Intent intentLeft = new Intent();
//                intentLeft.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, tempUserIdLeft);
//                if (isSystem == MsgBean.IS_NORMAL_MSG) {
//                    intentLeft.putExtra("fromPage", "ChatActivity");
//                }
//                intentLeft.setClass(ChatActivity.this, UserInfoActivity.class);
//                this.startActivity(intentLeft);
                FlutterRouterConfig.Companion.gotoUserInfo(tempUserIdLeft);
                break;

            // 点击地理位置
            case R.id.img_msg_map_right:
                ViewUtils.preventViewMultipleClick(v, 2000);
                PermissionUtil.requestLocationPermission(ChatActivity.this, new PermissionUtil.PermissionCallback() {
                    @Override
                    public void granted() {
                        MsgBean mapRight = (MsgBean) v.findViewById(R.id.img_msg_map_right).getTag(R.id.img_msg_map_right);
                        Intent mapRightIntent = new Intent(ChatActivity.this, LocationOnMapActivity.class);
                        mapRightIntent.putExtra("lng", mapRight.msgLng);
                        mapRightIntent.putExtra("lat", mapRight.msgLat);
                        ChatActivity.this.startActivity(mapRightIntent);
                    }

                    @Override
                    public void denied() {

                    }

                    @Override
                    public void gotoSetting() {

                    }
                });
                break;
            case R.id.img_msg_map_left:
                ViewUtils.preventViewMultipleClick(v, 2000);
                PermissionUtil.requestLocationPermission(this, new PermissionUtil.PermissionCallback() {
                    @Override
                    public void granted() {
                        MsgBean mapLeft = (MsgBean) v.findViewById(R.id.img_msg_map_left).getTag(R.id.img_msg_map_left);
                        Intent mapLeftIntent = new Intent(ChatActivity.this, LocationOnMapActivity.class);
                        mapLeftIntent.putExtra("lng", mapLeft.msgLng);
                        mapLeftIntent.putExtra("lat", mapLeft.msgLat);
                        ChatActivity.this.startActivity(mapLeftIntent);
                    }

                    @Override
                    public void denied() {

                    }

                    @Override
                    public void gotoSetting() {

                    }
                });

                break;
            // 点击图片
            case img_msg_photo_right:
                ViewUtils.preventViewMultipleClick(v, 2000);

                photos.clear();

                for (MsgBean msgBean : msglist) {
                    if (msgBean.filePath != null && msgBean.filePath.length() > 0 && msgBean.msgType.equals(MsgBean.MSG_TYPE_IMAGE)) {
                        photos.add(msgBean.filePath);
                    }
                }


                MsgBean photoRight = (MsgBean) v.findViewById(img_msg_photo_right).getTag(img_msg_photo_right);

                Intent photoRightIntent = new Intent(this, UserInfoPhotoActivity.class);
                photoRightIntent.putStringArrayListExtra(TheLConstants.BUNDLE_KEY_PHOTOS, photos);
                photoRightIntent.putExtra(TheLConstants.IS_WATERMARK, false);
                photoRightIntent.putExtra("fromPage", ChatActivity.class.getName());

                for (int i = photos.size() - 1; i >= 0; i--) {

                    if (photoRight.filePath != null && photoRight.filePath.equals(photos.get(i))) {
                        photoRightIntent.putExtra("position", i);
                        startActivity(photoRightIntent);
                        break;
                    }
                }
                break;
            case img_msg_photo_left:

                ViewUtils.preventViewMultipleClick(v, 2000);

                photos.clear();

                for (MsgBean msgBean : msglist) {
                    if (msgBean.filePath != null && msgBean.filePath.length() > 0 && msgBean.msgType.equals(MsgBean.MSG_TYPE_IMAGE)) {
                        photos.add(msgBean.filePath);
                    }
                }

                MsgBean photoLeft = (MsgBean) v.findViewById(img_msg_photo_left).getTag(img_msg_photo_left);

                Intent photoLeftIntent = new Intent(this, UserInfoPhotoActivity.class);
                photoLeftIntent.putExtra("fromPage", ChatActivity.class.getName());
                photoLeftIntent.putStringArrayListExtra(TheLConstants.BUNDLE_KEY_PHOTOS, photos);
                photoLeftIntent.putExtra(TheLConstants.IS_WATERMARK, false);

                for (int i = photos.size() - 1; i >= 0; i--) {

                    if (photoLeft.filePath != null && photoLeft.filePath.equals(photos.get(i))) {
                        photoLeftIntent.putExtra("position", i);
                        startActivity(photoLeftIntent);
                        break;
                    }
                }
                break;
            case R.id.img_type_right:
                ViewUtils.preventViewMultipleClick(v, 2000);
                final String packetId = (String) v.getTag();

                L.d(TAG, " packetId : " + packetId);

                if (!TextUtils.isEmpty(packetId)) {
                    DialogUtil.showConfirmDialog(this, "", getString(R.string.chat_activity_resend_confirm), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                            MsgBean msg = ChatServiceManager.getInstance().getMsgByUserIdAndPacketId(toUserId, packetId);

                            String tableName = DBUtils.getChatTableName(toUserId);

                            L.d(TAG, " tableName : " + tableName);

                            ChatServiceManager.getInstance().deleteMsgById(tableName, packetId);

                            msg.packetId = MsgUtils.getMsgId();
                            msg.msgStatus = MsgBean.MSG_STATUS_SENDING;
                            msg.msgTime = System.currentTimeMillis();
                            if (MsgBean.MSG_TYPE_IMAGE.equals(msg.msgType)) {
                                if (TextUtils.isEmpty(msg.msgText)) {// 如果图片还没上传成功，则先上传图片
                                    updateMsgToSending(msg.packetId, true);
                                    uploadFile(msg.filePath, msg.packetId, 0);
                                } else {
                                    sendMessage(msg, TheLConstants.MsgSendingStatusConstants.MSG_SENDING);
                                    // 重发刷新界面
                                    updateMsgToSending(msg.packetId, false);
                                }

                            } else if (MsgBean.MSG_TYPE_VOICE.equals(msg.msgType)) {// 如果语音还没上传成功，则先上传图片
                                if (TextUtils.isEmpty(msg.msgText)) {
                                    updateMsgToSending(msg.packetId, true);
                                    uploadFile(msg.filePath, msg.packetId, 1);
                                } else {
                                    sendMessage(msg, TheLConstants.MsgSendingStatusConstants.MSG_SENDING);
                                    // 重发刷新界面
                                    updateMsgToSending(msg.packetId, false);
                                }
                            } else if (MsgBean.MSG_TYPE_VIDEO.equals(msg.msgType)) {// 如果视频或缩略图还没有上传成功，则先上传文件
                                try {
                                    JSONObject jsonObject = new JSONObject(msg.msgText);
                                    if (!TextUtils.isEmpty(jsonObject.optString(MsgBean.VIEDO_PATH)) && !TextUtils.isEmpty(jsonObject.optString(MsgBean.VIEDO_THUMBNAIL))) {
                                        sendMessage(msg, TheLConstants.MsgSendingStatusConstants.MSG_SENDING);
                                        // 重发刷新界面
                                        updateMsgToSending(msg.packetId, false);
                                    } else {
                                        sendingVideoMsgs.add(msg);
                                        updateMsgToSending(msg.packetId, true);
                                        final String[] paths = msg.filePath.split(",");// 视频本地地址和预览图本地地址用逗号分隔保存的
                                        final String path = paths[0];
                                        final String thumbnail = paths[1];
                                        if (TextUtils.isEmpty(jsonObject.optString(MsgBean.VIEDO_PATH))) {
                                            uploadVideo(path, thumbnail, msg.packetId);
                                        } else {
                                            uploadVideoThumbnail(msg.packetId, thumbnail);
                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    sendVideoMsgFailed(packetId);
                                }
                            } else {
                                sendMessage(msg, TheLConstants.MsgSendingStatusConstants.MSG_SENDING);
                                // 重发刷新界面
                                updateMsgToSending(msg.packetId, false);
                            }
                        }
                    });
                }
                break;
            case R.id.img_sticker_left:
            case R.id.img_sticker_right:
                ViewUtils.preventViewMultipleClick(v, 2000);
                Intent intent1 = new Intent(this, StickerPackDetailActivity.class);
                intent1.putExtra("id", (long) v.getTag());
                startActivity(intent1);
                break;
            case R.id.rel_video_right:// 点击发出的视频
                L.d(TAG, "----------rel_video_right----------");
                final int position = (int) v.getTag(R.id.position);
                startPlaybackLocal(position);
                break;
            case R.id.rel_video_left:// 点击收到的视频
                L.d(TAG, "----------rel_video_right----------");
                final int position1 = (int) v.getTag(R.id.position);
                startPlayback(position1);
                break;
            case R.id.user_card_view_right:
                String userid = (String) v.getTag();
//                Intent intent2 = new Intent();
//                intent2.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, userid);
//                intent2.setClass(ChatActivity.this, UserInfoActivity.class);
//                this.startActivity(intent2);
                FlutterRouterConfig.Companion.gotoUserInfo(userid);
                break;
            case R.id.user_card_view_left:
                String userid2 = (String) v.getTag();
//                Intent intent3 = new Intent();
//                intent3.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, userid2);
//                intent3.setClass(ChatActivity.this, UserInfoActivity.class);
//                this.startActivity(intent3);
                FlutterRouterConfig.Companion.gotoUserInfo(userid2);
                break;
            case R.id.chat_recommend_momentview_left:
                String recommend_left_id = (String) v.getTag();
//                Intent intent4 = new Intent();
//                intent4.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, recommend_left_id);
//                intent4.setClass(ChatActivity.this, UserInfoActivity.class);
//                this.startActivity(intent4);
                FlutterRouterConfig.Companion.gotoUserInfo(recommend_left_id);
                break;
            /**4.0.0 挤眼，回挤**/
            case R.id.img_wink_left:
                wink();
                //埋点 挤眼firebase
                FireBaseUtils.uploadGoogle(TheLConstants.FireBaseConstant.WINK, this);
                break;
            case R.id.match_new_rl:
                MsgBean msgBean = (MsgBean) v.findViewById(R.id.match_new_rl).getTag(R.id.match_new_rl);
                Intent intent5 = new Intent(ChatActivity.this, MatchSuccessActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(MatchSuccessActivity.MATCH_SUCCESS_TYPE, MatchSuccessActivity.MATCH_SUCCESS_FROM_CHAT);
                bundle.putString(TheLConstants.BUNDLE_KEY_USER_AVATAR, DBUtils.getMainProcessOtherAvatar(msgBean));
                bundle.putString(TheLConstants.BUNDLE_KEY_USER_ID, DBUtils.getMainProcessOtherUserId(msgBean));
                bundle.putString(TheLConstants.BUNDLE_KEY_NICKNAME, DBUtils.getMainProcessOtherUserName(msgBean));
                intent5.putExtras(bundle);
                startActivity(intent5);
                break;
            default:
                break;
        }
    }

    /**
     * 动态表情点击事件
     *
     * @param view
     */
    @Override
    public void onItemClick(View view) {
        StickerBean stickerBean = (StickerBean) view.findViewById(R.id.img).getTag();
        saveHistorySticker(stickerBean);
        // 组织文字消息
        MsgBean msgBean = new MsgBean();
        msgBean.msgType = MsgBean.MSG_TYPE_STICKER;
        msgBean.msgText = stickerBean.buildStickerMsgText();
        msgBean.msgTime = System.currentTimeMillis();
        msgBean.msgDirection = "0";

        // new message
        msgBean.fromUserId = myUid;
        msgBean.fromNickname = myName;
        msgBean.fromAvatar = ShareFileUtils.getString(ShareFileUtils.AVATAR, "");
        msgBean.fromMessageUser = ShareFileUtils.getString(ShareFileUtils.MESSAGE_USER, "");
        msgBean.toUserId = toUserId;
        msgBean.toUserNickname = toName;
        msgBean.toAvatar = toAvatar;
        msgBean.toMessageUser = toMessageUser;
        msgBean.hadRead = 1; // 发出的消息一律为读过

        sendMessage(msgBean, TheLConstants.MsgSendingStatusConstants.MSG_SENDING);// 发送动图消息
        refreshUi(msgBean);
    }

    /**
     * 静态表情点击事件
     *
     * @param parent
     * @param view
     * @param position
     * @param id
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        StickerBean emoji = (StickerBean) view.findViewById(R.id.img).getTag();
        if (emoji.isDeleteBtn) {
            int selection = edit.getSelectionStart();
            String text = edit.getText().toString();
            if (selection > 0) {
                String text2 = text.substring(0, selection);
                if (text2.endsWith("]")) {
                    int start = text2.lastIndexOf("[");
                    int end = selection;
                    edit.getText().delete(start, end);
                    return;
                }
                edit.getText().delete(selection - 1, selection);
            }
        }
        if (!TextUtils.isEmpty(emoji.label)) {
            SpannableString spannableString = EmojiUtils.getInstace(EmojiUtils.DEFAULT_SIZE).addEmoji(this, (int) emoji.id, emoji.label);
            int index = edit.getSelectionStart();//获取光标所在位置
            Editable editable = edit.getEditableText();//获取EditText的文字
            if (index < 0 || index >= edit.length()) {
                editable.append(spannableString);
            } else {
                editable.insert(index, spannableString);//光标所在位置插入文字
            }
        }
    }

    @Override
    public void onFollowStatusChanged(int followStatus, String userId, String nickName, String avatar) {
        super.onFollowStatusChanged(followStatus, userId, nickName, avatar);
        L.d("refresh", "followStatus=" + followStatus + ",toUserId=" + toUserId);
        if (toUserId.equals(userId)) {

            if (followStatus == FollowStatusChangedImpl.FOLLOW_STATUS_FOLLOW) {
                if (simpleUserBean != null) {
                    simpleUserBean.isFollow = true;
                }
                setResult(RESULT_OK);
            } else {
                if (simpleUserBean != null) {
                    simpleUserBean.isFollow = false;
                }
            }

            ChatServiceManager.getInstance().checkFriendsRelationship(userId, null);

        }
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void initData() {
        msglist.clear();
        photos.clear();
        startIndex = 0;
        needRefreshStickers = false;

        boolean hasChangedUser = getDataFromPrePage();
        if (hasChangedUser) {
            canLoadMore = true;
        }

        // 检查有没有草稿
        String draft = SharedPrefUtils.getString(SharedPrefUtils.FILE_NAME_CHAT_DRAFT, toUserId, "");
        if (!TextUtils.isEmpty(draft)) {
            edit.setText(EmojiUtils.getInstace(EmojiUtils.DEFAULT_SIZE).getExpressionString(this, draft));
            edit.setSelection(draft.length());
        }

        myUid = ShareFileUtils.getString(ShareFileUtils.ID, "");
        myName = ShareFileUtils.getString(ShareFileUtils.USER_NAME, "");

        L.d(TAG, " toUserId : " + toUserId);

        if (TheLConstants.SYSTEM_USER.equals(toUserId)) {// 系统通知账号不能回复
            rel_bottom.setVisibility(View.GONE);
            lin_options.setVisibility(View.GONE);
            lin_contact_udesk.setVisibility(View.GONE);
        } else if (TheLConstants.RELA_ACCOUNT.equals(toUserId)) {//如果是L君的ID,跳转udesk
            lin_options.setVisibility(View.GONE);
            rl_editext.setVisibility(View.GONE);
            rel_bottom.setVisibility(View.GONE);
            lin_contact_udesk.setVisibility(View.VISIBLE);

        } else {
            rl_editext.setVisibility(View.VISIBLE);
            lin_options.setVisibility(View.VISIBLE);
            lin_contact_udesk.setVisibility(View.GONE);
        }

        L.d(TAG, " isSystem : " + isSystem);

        L.d(TAG, " MsgBean.IS_SYSTEM_MSG : " + MsgBean.IS_SYSTEM_MSG);

        if (isSystem >= MsgBean.IS_SYSTEM_MSG) {
            rel_bottom.setVisibility(View.GONE);
            lin_options.setVisibility(View.GONE);
            startIndex += msglist.size();
        } else {
            if (TheLConstants.RELA_ACCOUNT.equals(toUserId)) {
                lin_options.setVisibility(View.GONE);
                rl_editext.setVisibility(View.GONE);
                rel_bottom.setVisibility(View.GONE);
                lin_contact_udesk.setVisibility(View.VISIBLE);
            } else {
                rel_bottom.setVisibility(View.VISIBLE);
                lin_options.setVisibility(View.VISIBLE);
            }

            if (MsgBean.MSG_TYPE_LIKE.equals(toUserId)) {// 配对
                initLikeUi();
            } else {
                sendEmojiLayout.initViews(findViewById(R.id.rel_preview));
            }

            startIndex += msglist.size();

        }

        // 刷新聊天列表
        if (adapter == null || hasChangedUser) {
            adapter = new ChatAdapter(this, msglist, toAvatar, toUserId, isSystem == MsgBean.IS_NORMAL_MSG, pageId);
        }
        listView.setAdapter(adapter);
        listView.setSelection(msglist.size());
        Intent intent = getIntent();
        int send = intent.getIntExtra("send", 0);
        if (send == 1) {
            sendUserCardMsg(usercardbean);
        }
        int recommend = intent.getIntExtra("recommend", 0);
        recommendText = intent.getStringExtra("recommendText");
        if (recommend == 2) {
            senRecommendMsg(oldMomentBean);
        }
        FROM_PAGE = intent.getIntExtra(TheLConstants.BUNDLE_KEY_INTENT_FROM, 0);

        L.d(TAG, " FROM_PAGE : " + FROM_PAGE);

        //推荐网页
        if (FROM_PAGE_WEBVIEW == FROM_PAGE) {
            final RecommendWebBean recommendWebBean = (RecommendWebBean) intent.getSerializableExtra(TheLConstants.BUNDLE_KEY_RECOMMEND_WEB_BEAN);
            recommendText = intent.getStringExtra("recommendText");
            if (recommendWebBean != null) {
                sendWebViewMsg(recommendWebBean);
            }
        } else if (FROM_PAGE_LIVE_USER == FROM_PAGE) {//推荐主播
            final LiveRoomBean liveRoomBean = (LiveRoomBean) intent.getSerializableExtra(TheLConstants.BUNDLE_KEY_LIVE_ROOM);
            recommendText = intent.getStringExtra("recommendText");
            if (liveRoomBean != null) {
                sendLiveRoomBeanMsg(liveRoomBean);
            }
        }
        // 发送回执，如果发送成功，则把消息标记为已读
        addStrangerWarningMsg(0);

        switch (isSystem) {
            case MsgBean.IS_FOLLOW_MSG:
                toUserId = DBConstant.Message.FOLLOW_TABLE_NAME;
                GrowingIOUtil.goToChat(true);
                getFollowData();
                break;
            case MsgBean.IS_NORMAL_MSG:
                getSingleUsersRelation();
                GrowingIOUtil.goToChat(false);
                getMsgData();
                break;
            default:
                GrowingIOUtil.goToChat(false);
                getMsgData();
                break;
        }

        setPageTitle(0);

        if (isSystem == MsgBean.IS_NORMAL_MSG && !TheLConstants.SYSTEM_USER.equals(toUserId)) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    final String str = SharedPrefUtils.getString(SharedPrefUtils.FILE_CHAT_CLOSE_FOLLOW, myUid, "");
                    boolean requestUserCard = true;
                    if (!TextUtils.isEmpty(str)) {
                        try {
                            String[] userIds = str.split(",");
                            for (String userId : userIds) {
                                if (userId.equals(toUserId)) {
                                    requestUserCard = false;
                                    break;
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (!TextUtils.isEmpty(toUserId) && requestUserCard)
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                FollowStatusChangedImpl.getSingeUserRelation(toUserId, toName, toAvatar);
                            }
                        });
                }
            }).start();

            getLiveRoomUserCard();
        }

        L.d(TAG, " mIMsgStatusCallback : " + mIMsgStatusCallback);

        if (mIMsgStatusCallback != null) {
            ChatServiceManager.getInstance().registerCallback(toUserId, mIMsgStatusCallback);
        }

        isDefaultMsgHiding = ShareFileUtils.getInt(ShareFileUtils.MSG_HIDING, 0) == 1 && !toUserId.equals(DBConstant.Message.FOLLOW_TABLE_NAME);


        if (isDefaultMsgHiding) {
            msg_hiding_tips_ll.setVisibility(View.VISIBLE);
        } else {
            msg_hiding_tips_ll.setVisibility(View.GONE);
        }
        if (matchMsg != null) {
            sendMsgToUser(matchMsg);
        }
    }

    private void findViewById() {

        rel_bottom = this.findViewById(R.id.rel_bottom);
        lin_options = findView(R.id.lin_options);
        rel_more_bottom = findView(R.id.rel_more_bottom);
        sendEmojiLayout = this.findViewById(R.id.sendEmojiLayout);
        btn_gallery = findView(R.id.btn_gallery);
        btn_location = findView(R.id.btn_location);
        btn_voice = findView(R.id.btn_voice);
        rl_editext = findViewById(R.id.rl_editext);
        lin_contact_udesk = findViewById(R.id.lin_contact_udesk);
        int softHeight = ShareFileUtils.getInt(ShareFileUtils.SOFT_KEYBOARD_HEIGHT, 0);
        if (softHeight >= defaultSoftInputHeight) {
            rel_more_bottom.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, softHeight));
        } else {
            rel_more_bottom.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, defaultSoftInputHeight));
        }
        txt_nickname = this.findViewById(R.id.txt_nickname);
        txt_distance = findViewById(R.id.txt_distance);
        avatar = findViewById(R.id.avatar);
        lin_back = this.findViewById(R.id.lin_back);
        lin_more = this.findViewById(R.id.lin_more);
        lin_msgs = this.findViewById(R.id.lin_msgs);
        bg = this.findViewById(R.id.bg);
        listView = this.findViewById(R.id.listview);
        btn_sticker = this.findViewById(R.id.btn_sticker);
        btn_video = findView(R.id.btn_video);
        edit = this.findViewById(R.id.edit);
        edit.requestFocus();
        edit.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                View rootview = ChatActivity.this.getWindow().getDecorView(); // this = activity
                rootview.getWindowVisibleDisplayFrame(r);

                int screenHeight = rootview.getHeight();
                int heightDifference = screenHeight - r.bottom;
                if (heightDifference > 300) {
                    int softHeight = ShareFileUtils.getInt(ShareFileUtils.SOFT_KEYBOARD_HEIGHT, 0);
                    if (softHeight != heightDifference) {// 更新软键盘的高度
                        softHeight = heightDifference;
                        ShareFileUtils.setInt(ShareFileUtils.SOFT_KEYBOARD_HEIGHT, softHeight);
                    }
                    if (softHeight >= defaultSoftInputHeight) {
                        rel_more_bottom.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, softHeight));
                    } else {
                        rel_more_bottom.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, defaultSoftInputHeight));
                    }
                    // 调整表情键盘的上间距
                    sendEmojiLayout.refreshPaddingTop();

                    hideBottomMore();
                }

            }
        });
        txt_send = findView(R.id.txt_send);
        // 录音
        rel_voice = findViewById(R.id.rel_voice);
        txt_start_record_tip = findView(R.id.txt_start_record_tip);
        voice_center = findViewById(R.id.voice_center);
        volume = findViewById(R.id.volume);
        volume1 = findViewById(R.id.volume1);
        lin_voice_length = findView(R.id.lin_voice_length);
        txt_voice_length = findView(R.id.txt_voice_length);
        img_record = findViewById(R.id.img_record);
        img_cancel = findViewById(R.id.img_cancel);
        txt_cancel_record = findViewById(R.id.txt_cancel_record);
        //挤眼
        img_sendwink = findViewById(R.id.img_sendwink);
        chat_moment_layout = findViewById(R.id.chat_moment_layout);
        chat_guide_layout = findViewById(R.id.chat_guide_layout);
        msg_hiding_tips_ll = findViewById(R.id.msg_hiding_tips_ll);
        mUnFollowTipsLayout = findViewById(R.id.un_follow_tips_layout);
        mChatMatchTipsLayout = findViewById(R.id.chat_match_tips_layout);

        fast_emoji_rv = findViewById(R.id.fast_emoji_rv);

        initFastMsgView();
    }

    private void initFastMsgView() {

        FastEmojiAdapter fastEmojiAdapter = new FastEmojiAdapter(ChatActivity.this, FastEmojiAdapter.MSG_TYPE_IM);

        fastEmojiAdapter.setOnItemClickListener(new BaseAdapter.OnItemClickListener<String>() {
            @Override
            public void onItemClick(View view, String item, int position) {

               /* MsgBean msgBean = new MsgBean();
                msgBean.msgType = "text";
                msgBean.msgText = item;
                msgBean.msgTime = System.currentTimeMillis();
                msgBean.msgDirection = "0";
                msgBean.fromUserId = myUid;
                msgBean.fromNickname = myName;
                msgBean.fromAvatar = ShareFileUtils.getString(ShareFileUtils.AVATAR, "");
                msgBean.fromMessageUser = ShareFileUtils.getString(ShareFileUtils.MESSAGE_USER, "");
                msgBean.toUserId = toUserId;
                msgBean.toUserNickname = toName;
                msgBean.toAvatar = toAvatar;
                msgBean.toMessageUser = toMessageUser;
                msgBean.hadRead = 1; // 发出的消息一律为读过

                sendMessage(msgBean, TheLConstants.MsgSendingStatusConstants.MSG_SENDING);

                refreshUi(msgBean);*/
                sendMsgToUser(item);
                fast_emoji_rv.setVisibility(View.GONE);
            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ChatActivity.this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        fast_emoji_rv.setLayoutManager(linearLayoutManager);
        fast_emoji_rv.addItemDecoration(new FastMsgDivider(SizeUtils.dip2px(TheLApp.context, 10)));
        fast_emoji_rv.setAdapter(fastEmojiAdapter);
    }

    public void sendMsgToUser(String msg) {
        MsgBean msgBean = new MsgBean();
        msgBean.msgType = "text";
        msgBean.msgText = msg;
        msgBean.msgTime = System.currentTimeMillis();
        msgBean.msgDirection = "0";
        msgBean.fromUserId = myUid;
        msgBean.fromNickname = myName;
        msgBean.fromAvatar = ShareFileUtils.getString(ShareFileUtils.AVATAR, "");
        msgBean.fromMessageUser = ShareFileUtils.getString(ShareFileUtils.MESSAGE_USER, "");
        msgBean.toUserId = toUserId;
        msgBean.toUserNickname = toName;
        msgBean.toAvatar = toAvatar;
        msgBean.toMessageUser = toMessageUser;
        msgBean.hadRead = 1; // 发出的消息一律为读过

        sendMessage(msgBean, TheLConstants.MsgSendingStatusConstants.MSG_SENDING);

        refreshUi(msgBean);
    }

    protected void setListener() {

        edit.setFocusable(true);
        edit.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {// 输入框失去焦点后隐藏键盘
                    dismissKeyboard();
                } else {
                    if (UserUtils.isVerifyCell()) {
                        // 键盘弹出后把对话列表滚到底部
                        mHandler.postDelayed(new Runnable() {

                            @Override
                            public void run() {
                                int bottom = listView.getBottom();
                                if (listView.getSelectedItemPosition() != bottom)
                                    listView.setSelection(bottom);
                            }
                        }, 100);
                    }
                }
            }
        });
        edit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (UserUtils.isVerifyCell()) {
                    // 键盘弹出后把对话列表滚到底部
                    mHandler.postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            listView.setSelection(listView.getBottom());
                        }
                    }, 100);
                }

            }
        });

        textWatcher = new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (TextUtils.isEmpty(s.toString().trim())) {
                    showAddButton();
                } else {
                    showSendButton();
                    if (s.length() >= getResources().getInteger(R.integer.chat_msg_max_length)) {
                        DialogUtil.showToastShort(ChatActivity.this, String.format(getString(R.string.info_words_length_limit), getResources().getInteger(R.integer.chat_msg_max_length)));
                    }
                    sendPendingMsg(0);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };
        edit.addTextChangedListener(textWatcher);

        // 标题回退按钮
        lin_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                PushUtils.finish(ChatActivity.this);
            }
        });

        // 标题更多按钮
        lin_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                Intent intent = new Intent(ChatActivity.this, ChatOperationsActivity.class);
                intent.putExtra(TheLConstants.BUNDLE_KEY_WIDTH, lin_msgs.getWidth());
                intent.putExtra(TheLConstants.BUNDLE_KEY_HEIGHT, lin_msgs.getHeight());

                String userId;

                if (isSystem == MsgBean.IS_FOLLOW_MSG) {
                    userId = DBUtils.getFollowTableName();
                } else {
                    userId = toUserId;
                }

                intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, userId);// 系统消息用『system』存储
                startActivityForResult(intent, TheLConstants.BUNDLE_CODE_CHAT_ACTIVITY);
            }
        });

        // 语音切换
        btn_voice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (UserUtils.isVerifyCell()) {
                    new RxPermissions(ChatActivity.this).request(Manifest.permission.RECORD_AUDIO).subscribe(new Consumer<Boolean>() {
                        @Override
                        public void accept(Boolean aBoolean) {

                            if (aBoolean) {

                                PermissionUtil.requestStoragePermission(ChatActivity.this, new PermissionUtil.PermissionCallback() {
                                    @Override
                                    public void granted() {
                                        dismissKeyboard();
                                        mHandler.postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                showVoiceLayout();
                                            }
                                        }, 100);
                                    }

                                    @Override
                                    public void denied() {

                                    }

                                    @Override
                                    public void gotoSetting() {

                                    }
                                });

                            }
                        }
                    });
                }


            }
        });

        btn_sticker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissKeyboard();
                if (UserUtils.isVerifyCell()) {
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            showStickerLayout();
                        }
                    }, 100);
                }
            }
        });

        // 加号->图库按钮
        btn_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                hideBottomMore();
                if (UserUtils.isVerifyCell()) {
                    if (PhoneUtils.hasSdcard()) {
                        Intent intent = new Intent(ChatActivity.this, SelectLocalImagesActivity.class);
                        intent.putExtra(TheLConstants.BUNDLE_KEY_SELECT_AMOUNT, SELECT_PIC_LIMIT);
                        startActivityForResult(intent, TheLConstants.BUNDLE_CODE_CHAT_ACTIVITY);
                    } else {
                        DialogUtil.showToastShort(ChatActivity.this, TheLApp.getContext().getString(R.string.chat_activity_info_photo_without_sdcard));
                    }
                }

            }
        });

        // 加号->位置按钮
        btn_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (UserUtils.isVerifyCell()) {
                    DialogUtil.showConfirmDialog(ChatActivity.this, "", getString(R.string.send_location_confirm), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            sendLocation();
                            dialog.dismiss();
                        }
                    });
                }
            }
        });

        // 录制视频按钮
        btn_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                if (UserUtils.isVerifyCell()) {
                    PermissionUtil.requestStoragePermission(ChatActivity.this, new PermissionUtil.PermissionCallback() {
                        @Override
                        public void granted() {
                            RelaVideoSDK.startVideoGalleryActivity(ChatActivity.this, new onRelaVideoActivityResultListener() {
                                @Override
                                public void onRelaVideoActivityResult(Intent intent) {
                                    Bundle bundle = intent.getExtras();
                                    final String videoPath = bundle.getString(video.com.relavideolibrary.Utils.Constant.BundleConstants.RESULT_VIDEO_PATH);
                                    final String thumbnail = bundle.getString(video.com.relavideolibrary.Utils.Constant.BundleConstants.RESULT_VIDEO_THUMB);
                                    final int pixelWidth = Integer.parseInt(bundle.getString(video.com.relavideolibrary.Utils.Constant.BundleConstants.RESULT_VIDEO_WIDTH));
                                    final int pixelHeight = Integer.parseInt(bundle.getString(video.com.relavideolibrary.Utils.Constant.BundleConstants.RESULT_VIDEO_HEIGHT));
                                    uploadVideo(videoPath, thumbnail, saveVideoMsg(videoPath, thumbnail, pixelWidth, pixelHeight));//上传视频路径到服务器
                                }
                            });
                        }

                        @Override
                        public void denied() {

                        }

                        @Override
                        public void gotoSetting() {

                        }
                    });
                }
            }
        });

        // 发送按钮
        txt_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String txt = edit.getText().toString().trim();
                if (TextUtils.isEmpty(txt))
                    return;
                ViewUtils.preventViewMultipleClick(v, 1000);
                BusinessUtils.playSound(R.raw.sound_send);
                // 组织文字消息
                MsgBean msgBean;

                if (chat_moment_layout.isShown() && oldMomentBean != null) {

                    String body = MomentUtils.getMomentToChat(txt, oldMomentBean);

                    msgBean = MsgUtils.createMsgBean(MsgBean.MSG_TYPE_MOMENT_TO_CHAT, body, 1, String.valueOf(oldMomentBean.userId), oldMomentBean.nickname, oldMomentBean.avatar);

                    chat_moment_layout.setVisibility(View.GONE);

                    chat_guide_layout.setVisibility(View.GONE);

                } else {
                    msgBean = new MsgBean();
                    msgBean.msgType = "text";
                    msgBean.msgText = txt;
                    msgBean.msgTime = System.currentTimeMillis();
                    msgBean.msgDirection = "0";
                    msgBean.fromUserId = myUid;
                    msgBean.fromNickname = myName;
                    msgBean.fromAvatar = ShareFileUtils.getString(ShareFileUtils.AVATAR, "");
                    msgBean.fromMessageUser = ShareFileUtils.getString(ShareFileUtils.MESSAGE_USER, "");
                    msgBean.toUserId = toUserId;
                    msgBean.toUserNickname = toName;
                    msgBean.toAvatar = toAvatar;
                    msgBean.toMessageUser = toMessageUser;
                    msgBean.hadRead = 1; // 发出的消息一律为读过
                }

                sendMessage(msgBean, TheLConstants.MsgSendingStatusConstants.MSG_SENDING);// 发送文字消息

                if (needRefresh) {
                    edit.setText("");
                }
                refreshUi(msgBean);
            }
        });

        listView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideBottomMore();
                v.requestFocus();
                return false;
            }
        });

        // 列表长按
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView parent, View view, int position, long id) {
                if (msglist != null) {
                    MsgBean msg = msglist.get(position);
                    int firstPosition = listView.getFirstVisiblePosition();
                    if (msg != null)
                        handleMessageDialog(msg, firstPosition);
                }
                return true;
            }
        });

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                switch (scrollState) {
                    // 当不滚动时
                    case AbsListView.OnScrollListener.SCROLL_STATE_IDLE:
                        // 判断滚动到顶部

                        L.d(TAG, " view.getFirstVisiblePosition() : " + view.getFirstVisiblePosition());

                        L.d(TAG, " canLoadMore : " + canLoadMore);

                        L.d(TAG, " isSystem : " + isSystem);


                        if (view.getFirstVisiblePosition() == 0 && canLoadMore) {
                            // 翻到最上，加载更多聊天记录
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    switch (isSystem) {
                                        case MsgBean.IS_FOLLOW_MSG:
                                            toUserId = DBConstant.Message.FOLLOW_TABLE_NAME;
                                            getFollowData();
                                            break;
                                        case MsgBean.IS_NORMAL_MSG:
                                            getMsgData();
                                            break;
                                        default:
                                            getMsgData();
                                            break;
                                    }
                                }
                            });
                        }

                        break;
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            }
        });

        // 按住说话
        voice_center.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (disableTouch) {
                    return true;
                }

                final int[] center_location = new int[2];
                voice_center.getLocationInWindow(center_location);
                final int center_Y = center_location[1];
                final int center_x = center_location[0];

                // 如果手指移动到取消区域，则显示叉叉和tip
                if (event.getRawY() >= center_Y && event.getRawY() <= center_Y + voice_center.getHeight() && event.getRawX() >= center_x && event.getRawX() <= center_x + voice_center.getWidth()) {
                    txt_cancel_record.setText(getString(R.string.chat_activity_slide_up_to_cancel));
                    img_record.setVisibility(View.VISIBLE);
                    img_cancel.setVisibility(View.GONE);
                    if (recordTime <= ATTENTION_TIME) { // 50秒以后
                        voice_center.setBackgroundResource(R.drawable.chat_voice_bg_center);
                    } else {
                        voice_center.setBackgroundResource(R.drawable.chat_voice_bg_overtime);
                    }
                } else {
                    txt_cancel_record.setText(getString(R.string.chat_activity_release_to_cancel));
                    img_record.setVisibility(View.GONE);
                    img_cancel.setVisibility(View.VISIBLE);
                    voice_center.setBackgroundResource(R.drawable.chat_voice_bg_cancel);
                }

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        if (System.currentTimeMillis() - lastPressTime <= 3000) {// 防止连续点击
                            return true;
                        }
                        lastPressTime = System.currentTimeMillis();

                        //// TODO: 17/11/7  XiamiSDKUtil
                        //                        if (XiamiSDKUtil.getInstance().isPlaying()) {// 停止播放音乐
                        //                            XiamiSDKUtil.getInstance().pause();
                        //                        }
                        if (PhoneUtils.hasSdcard()) {
                            if (record_state != RECORD_ING) {
                                txt_cancel_record.setVisibility(View.VISIBLE);
                                //                        txt_speech.setBackgroundResource(R.drawable.bg_imput_speak_press);
                                //                        txt_speech.setText(getString(R.string.chat_activity_speek_send));
                                record_state = RECORD_ING;
                                if (playing) {
                                    // 如果录音的时候,正在播放,则停止播放
                                    mediaStop();
                                }
                                txt_start_record_tip.setVisibility(View.GONE);
                                lin_voice_length.setVisibility(View.VISIBLE);
                                txt_voice_length.setText("00:00");
                                startRecording();
                                mHandler.postDelayed(mPollTask, POLL_INTERVAL);
                            }
                        } else {
                            Toast.makeText(ChatActivity.this, TheLApp.getContext().getString(R.string.chat_activity_info_record_without_sdcard), Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        if (PhoneUtils.hasSdcard()) {
                            if (record_state == RECORD_ING) {
                                record_state = RECODE_END;
                                volumeScale = 1.0f;
                                volumeScale1 = 1.0f;
                                mHandler.removeCallbacks(mPollTask);
                                if (event.getRawY() >= center_Y && event.getRawY() <= center_Y + voice_center.getHeight() && event.getRawX() >= center_x && event.getRawX() <= center_x + voice_center.getWidth()) {
                                    if (recordTime < MIN_TIME) {
                                        DialogUtil.showToastShort(ChatActivity.this, TheLApp.getContext().getString(R.string.chat_activity_info_record_within_one_second));
                                        record_state = RECORD_NO;
                                        stopRecording();
                                        deleteVoiceFile(mFileName);
                                    } else {
                                        final String packetId = saveVoiceMsg(TheLConstants.F_MSG_VOICE_ROOTPATH + toUserId + File.separator + mFileName, recordTime + "");
                                        disableTouch = true;
                                        mHandler.postDelayed(new Runnable() {// 延迟一秒再停止录音，否则会丢失语音

                                            @Override
                                            public void run() {

                                                stopRecording();

                                                uploadFile(TheLConstants.F_MSG_VOICE_ROOTPATH + toUserId + File.separator + mFileName, packetId, 1);
                                            }

                                        }, 1000);
                                    }
                                } else {
                                    //                            rel_voice.setVisibility(View.GONE);
                                    Toast.makeText(ChatActivity.this, getString(R.string.chat_activity_record_canceled), Toast.LENGTH_SHORT).show();
                                    record_state = RECORD_NO;
                                    stopRecording();
                                    deleteVoiceFile(mFileName);
                                }
                            }
                        }
                        initVoiceLayout();
                        break;
                }
                return true;
            }
        });
        lin_contact_udesk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                Utils.gotoUDesk();
            }
        });

        avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
//                Intent intentLeft = new Intent();
//                intentLeft.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, toUserId);
//                if (isSystem == MsgBean.IS_NORMAL_MSG) {
//                    intentLeft.putExtra("fromPage", "ChatActivity");
//                }
//                intentLeft.setClass(ChatActivity.this, UserInfoActivity.class);
//                startActivity(intentLeft);
                FlutterRouterConfig.Companion.gotoUserInfo(toUserId);
            }
        });
    }

    private void updateMsgToSending(String packetId, boolean refreshData) {
        if (refreshData) {
            final MessageDataBaseAdapter dbAdatper = MessageDataBaseAdapter.getInstance(TheLApp.getContext(), myUid);
            dbAdatper.updateMsgToSending(toUserId, packetId);
        }
        // 重发刷新界面
        for (int i = msglist.size() - 1; i >= 0; i--) {
            if (!TextUtils.isEmpty(msglist.get(i).packetId) && msglist.get(i).packetId.equals(packetId)) {
                msglist.get(i).msgStatus = MsgBean.MSG_STATUS_SENDING;
                msglist.get(i).msgTime = System.currentTimeMillis();
                refreshSingleItem(i);
                break;
            }
        }
    }

    /**
     * 开始录音
     */
    private void startRecording() {

        try {

            File fileDir = new File(TheLConstants.F_MSG_VOICE_ROOTPATH + toUserId);
            if (!fileDir.exists()) {
                fileDir.mkdirs();
            }

            if (mRecorder == null) {
                mRecorder = new MediaRecorder();
            } else {
                mRecorder.reset();
            }

            mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            mFileName = Calendar.getInstance().getTimeInMillis() + ".aac";
            mRecorder.setOutputFile(TheLConstants.F_MSG_VOICE_ROOTPATH + toUserId + File.separator + mFileName);
            mRecorder.prepare();
            recordTime = 1;
            mRecorder.start();
            mEMA = 0.0;
        } catch (Exception e) {
            e.printStackTrace();
        }

        mythread();
    }

    /**
     * 停止录音
     */
    private void stopRecording() {
        if (mRecorder != null) {
            try {
                mRecorder.stop();
                mRecorder.release();
                mRecorder = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        // 取消触摸屏蔽
        disableTouch = false;
    }

    // 录音计时线程
    private void mythread() {
        recordThread = new Thread(ImgThread);
        recordThread.start();
    }

    /**
     * 删除录音文件
     */
    private void deleteVoiceFile(String fileName) {
        File file = new File(TheLConstants.F_MSG_VOICE_ROOTPATH + toUserId, fileName);
        if (file.exists()) {
            file.delete();
        }
    }

    private void initVoiceLayout() {
        txt_cancel_record.setVisibility(View.GONE);
        voice_center.setBackgroundResource(R.drawable.chat_voice_bg_center);
        img_record.setVisibility(View.VISIBLE);
        img_cancel.setVisibility(View.GONE);
        txt_start_record_tip.setVisibility(View.VISIBLE);
        lin_voice_length.setVisibility(View.GONE);
        txt_voice_length.setText("00:00");
    }

    public void getLiveRoomUserCard() {
        DefaultRequestService.createLiveRequestService().getLiveRoomUserCard(toUserId).observeOn(AndroidSchedulers.mainThread()).onBackpressureDrop().subscribeOn(Schedulers.io()).subscribe(new InterceptorSubscribe<UserCardV2Bean>() {
            @Override
            public void onNext(UserCardV2Bean data) {
                super.onNext(data);

                L.d(TAG, " getLiveRoomUserCard : " + GsonUtils.createJsonString(data));

                simpleUserBean = data.data;
                setPageTitle(0);
                if (simpleUserBean != null) {

                    isConfine = simpleUserBean.confine != null && simpleUserBean.confine.equals("reject");

                }
            }
        });

    }

    private void mediaStop() {
        try {
            if (null != mediaPlayer && mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
                playing = false;
                msgBeanPre.isPlaying = 0;
                refreshSingleItem(msgBeanPre);
            }
        } catch (Exception e) {
            return;
        }
    }

    private void refreshSingleItem(MsgBean msgBean) {
        try {
            for (int i = 0; i < msglist.size(); i++) {
                if (msgBean.packetId.equals(msglist.get(i).packetId)) {
                    msglist.get(i).msgStatus = msgBean.msgStatus;
                    adapter.updateSingleRow(listView, i + listView.getHeaderViewsCount());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void refreshSingleItem(int index) {
        try {
            adapter.updateSingleRow(listView, index + listView.getHeaderViewsCount());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideBottomMore() {
        if (rel_more_bottom.getVisibility() == View.VISIBLE) {
            rel_more_bottom.setVisibility(View.GONE);
            sendEmojiLayout.setVisibility(View.GONE);
            rel_voice.setVisibility(View.GONE);
        }
    }

    /**
     * 发送位置
     */
    private void sendLocation() {
        hideBottomMore();

        PermissionUtil.requestLocationPermission(this, new PermissionUtil.PermissionCallback() {
            @Override
            public void granted() {
                LocationUtils.setUpLocation();

                String lng = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");
                String lat = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");

                MsgBean msgBean = new MsgBean();
                msgBean.msgType = "map";
                msgBean.msgLng = lng;
                msgBean.msgLat = lat;
                msgBean.msgTime = System.currentTimeMillis();
                msgBean.msgDirection = "0";
                msgBean.msgText = TheLApp.getContext().getString(R.string.message_info_location);

                // new message
                msgBean.fromUserId = myUid;
                msgBean.fromNickname = myName;
                msgBean.fromAvatar = ShareFileUtils.getString(ShareFileUtils.AVATAR, "");
                msgBean.fromMessageUser = ShareFileUtils.getString(ShareFileUtils.MESSAGE_USER, "");
                msgBean.toUserId = toUserId;
                msgBean.toUserNickname = toName;
                msgBean.toAvatar = toAvatar;
                msgBean.toMessageUser = toMessageUser;
                msgBean.hadRead = 1; // 发出的消息一律为读过

                sendMessage(msgBean, TheLConstants.MsgSendingStatusConstants.MSG_SENDING);// 发送位置消息

                refreshUi(msgBean);
            }

            @Override
            public void denied() {

            }

            @Override
            public void gotoSetting() {
            }
        });
    }

    private String saveVoiceMsg(String localPath, String voiceTime) {

        // 组织语音消息(用于存储db)
        MsgBean msgBean = new MsgBean();
        msgBean.packetId = MsgUtils.getMsgId();
        msgBean.msgType = MsgBean.MSG_TYPE_VOICE;
        msgBean.msgTime = System.currentTimeMillis();
        msgBean.msgDirection = "0";
        msgBean.msgStatus = MsgBean.MSG_STATUS_SENDING;
        msgBean.filePath = localPath;
        if (!TextUtils.isEmpty(voiceTime)) {
            msgBean.voiceTime = voiceTime;
        } else {
            msgBean.voiceTime = recordTime + "";
        }
        // msgBean.msgText = voiceBase64;

        // new message
        msgBean.fromUserId = myUid;
        msgBean.fromNickname = myName;
        msgBean.fromAvatar = ShareFileUtils.getString(ShareFileUtils.AVATAR, "");
        msgBean.fromMessageUser = ShareFileUtils.getString(ShareFileUtils.MESSAGE_USER, "");
        msgBean.toUserId = toUserId;
        msgBean.toUserNickname = toName;
        msgBean.toAvatar = toAvatar;
        msgBean.toMessageUser = toMessageUser;
        msgBean.hadRead = 1; // 发出的消息一律为读过
        msgBean.hadPlayed = 1; // 发出的消息一律为听过

        DBUtils.sendRefreshUiBroadcast(msgBean);

        refreshUi(msgBean);

        return msgBean.packetId;
    }

    private String saveVideoMsg(String localPath, String thumbnail, int videoWidth, int videoHeight) {

        // 组织语音消息(用于存储db)
        MsgBean msgBean = new MsgBean();
        msgBean.packetId = MsgUtils.getMsgId();
        msgBean.msgType = MsgBean.MSG_TYPE_VIDEO;
        msgBean.msgTime = System.currentTimeMillis();
        msgBean.msgDirection = "0";
        msgBean.msgStatus = MsgBean.MSG_STATUS_SENDING;
        msgBean.filePath = localPath + "," + thumbnail;// 把视频地址和缩略图用逗号分隔存到数据库
        msgBean.msgText = "{}";// 先将消息内容设为空json对象，等到缩略图和视频文件上传成功后，就添加到json对象中，并将filePath中的字段移除

        // new message
        msgBean.fromUserId = myUid;
        msgBean.fromNickname = myName;
        msgBean.fromAvatar = ShareFileUtils.getString(ShareFileUtils.AVATAR, "");
        msgBean.fromMessageUser = ShareFileUtils.getString(ShareFileUtils.MESSAGE_USER, "");
        msgBean.toUserId = toUserId;
        msgBean.toUserNickname = toName;
        msgBean.toAvatar = toAvatar;
        msgBean.toMessageUser = toMessageUser;
        msgBean.hadRead = 1; // 发出的消息一律为读过
        msgBean.hadPlayed = 1; // 发出的消息一律为听过
        msgBean.msgWidth = String.valueOf(videoWidth);
        msgBean.msgHeight = String.valueOf(videoHeight);

        DBUtils.sendRefreshUiBroadcast(msgBean);

        refreshUi(msgBean);

        sendingVideoMsgs.add(msgBean);

        return msgBean.packetId;
    }

    private void saveImg(ImageInfo imgInfo) {
        // 组织图片消息(用于存储db)
        MsgBean msgBean = new MsgBean();
        msgBean.packetId = MsgUtils.getMsgId();
        // 刷新界面要用到
        imgInfo.packetId = msgBean.packetId;
        // 保存刚发的图片消息的packetId
        msgBean.msgType = "image";
        msgBean.msgTime = System.currentTimeMillis();
        msgBean.msgDirection = "0";
        msgBean.msgStatus = MsgBean.MSG_STATUS_SENDING;
        msgBean.filePath = imgInfo.localPath; // 图片完整路径
        // msgBean.msgText = fileFullPath; // 发送base64字符串

        // new message
        msgBean.fromUserId = myUid;
        msgBean.fromNickname = myName;
        msgBean.fromAvatar = ShareFileUtils.getString(ShareFileUtils.AVATAR, "");
        msgBean.fromMessageUser = ShareFileUtils.getString(ShareFileUtils.MESSAGE_USER, "");
        msgBean.toUserId = toUserId;
        msgBean.toUserNickname = toName;
        msgBean.toAvatar = toAvatar;
        msgBean.toMessageUser = toMessageUser;
        msgBean.hadRead = 1; // 发出的消息一律为读过
        msgBean.msgHeight = imgInfo.imgHeight + "";
        msgBean.msgWidth = imgInfo.imgWidth + "";

        DBUtils.sendRefreshUiBroadcast(msgBean);
        refreshUi(msgBean);
    }

    /**
     * 上传视频路径到服务器,获取key
     *
     * @param videoPath     本地视频地址
     * @param videoThumnail 视频缩略图
     */
    private void uploadVideo(final String videoPath, final String videoThumnail, final String packetId) {

        // 获取视频文件的上传token
        final String fileType = videoPath.substring(videoPath.lastIndexOf("."));//文件类型
        final String uploadVideoPath = RequestConstants.UPLOAD_FILE_ROOT_PATH_VIDEO + ShareFileUtils.getString(ShareFileUtils.ID, "") + "/" + MD5Utils.calculateMD5(new File(videoPath)) + fileType;
        UploadTokenConsumerBean bean = new UploadTokenConsumerBean();
        bean.packetId = packetId;
        bean.type = 0;
        bean.videoPath = videoPath;
        bean.videoThumnail = videoThumnail;
        bean.uploadVideoPath = uploadVideoPath;
        RequestBusiness.getInstance().getUploadToken(System.currentTimeMillis() + "", RequestConstants.QINIU_BUCKET_VIDEO, uploadVideoPath).onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new UploadTokenConsumer(bean));
    }

    private void playMedia(final MsgBean msgBean) {
        try {
            //// TODO: 17/11/7  XiamiSDKUtil
            //            if (XiamiSDKUtil.getInstance().isPlaying()) {// 停止播放音乐
            //                XiamiSDKUtil.getInstance().pause();
            //            }
            mediaPlayer = new MediaPlayer();
            try {
                // 文件找不到的话抛出异常
                mediaPlayer.setDataSource(msgBean.filePath);
                mediaPlayer.prepare();
                mediaPlayer.start();
                playing = true;
                msgBean.isPlaying = 1;
            } catch (Exception e) {
                try {
                    if (MsgBean.MSG_DIRECTION_TYPE_INCOME.equals(msgBean.msgDirection)) {// 如果收到的消息播放失败，则设为失败，下次重新下载
                        // 刷新消息
                        MessageDataBaseAdapter msgAdapter = MessageDataBaseAdapter.getInstance(TheLApp.getContext(), ShareFileUtils.getString(ShareFileUtils.ID, ""));
                        msgAdapter.updateVoiceMsgToFailed(msgBean);
                        refreshSingleItem(msgBean);
                    }
                } catch (Exception e2) {
                }
            }
            // 播放完毕
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    playing = false;
                    msgBean.isPlaying = 0;
                    refreshSingleItem(msgBean);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveHistorySticker(StickerBean stickerBean) {
        try {
            JSONObject object = stickerBean.toJSON();
            JSONArray jsonArray = new JSONArray(SharedPrefUtils.getString(SharedPrefUtils.FILE_STICKERS, SharedPrefUtils.FILE_STICKERS, "[]"));
            for (int i = 0; i < jsonArray.length(); i++) {
                if (jsonArray.getJSONObject(i).getLong("id") == object.getLong("id")) {
                    JsonUtils.removeIndexOfJSONArray(jsonArray, i);
                    break;
                }
            }
            JSONArray tempArr = new JSONArray();
            tempArr.put(0, object);
            for (int i = 0; i < jsonArray.length(); i++) {
                tempArr.put(i + 1, jsonArray.get(i));
                if (i == 6) {// 最多8个历史记录（1页）
                    break;
                }
            }
            SharedPrefUtils.setString(SharedPrefUtils.FILE_STICKERS, SharedPrefUtils.FILE_STICKERS, tempArr.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendMessage(MsgBean msgBean, String status) {

        if (msgBean == null) {
            return;
        }

        // 没网络不发消息
        if (PhoneUtils.getNetWorkType() == PhoneUtils.TYPE_NO) {
            needRefresh = false;
            DialogUtil.showToastShort(this, getString(R.string.message_network_error));
            return;
        }

        if (!TextUtils.isEmpty(msgBean.toUserId) && BlackUtils.isBlack(msgBean.toUserId)) {
            needRefresh = false;
        }
        if (BlackUtils.isBlockMe(msgBean.toUserId)) {
            needRefresh = false;
        }

        msgBean.packetId = MsgUtils.getMsgId();
        msgBean.type = MsgBean.TYPE_MSG;
        msgBean.userId = toUserId;

        L.d(TAG, " sendMessage toUserId : " + toUserId);

        L.d(TAG, " sendMessage UserUtils.getMyUserId() : " + UserUtils.getMyUserId());

        L.d(TAG, " sendMessage BlackUtils.isBlack(toUserId) : " + BlackUtils.isBlack(toUserId));

        L.d(TAG, " sendMessage BlackUtils.isBlockMe(toUserId) : " + BlackUtils.isBlockMe(toUserId));

        if (isConfine) {
            msgBean.msgStatus = TheLConstants.MsgSendingStatusConstants.MSG_CONFINE;
            ChatServiceManager.getInstance().insertNewMsg(msgBean, DBUtils.getMainProcessChatTableName(msgBean), 1);
            adapter.addData(msgBean);
            listView.setSelection(msglist.size() - 1);
            dismissKeyboard();
            if (edit != null) {
                edit.setText("");
            }
        } else if (!TextUtils.isEmpty(toUserId) && BlackUtils.isBlack(toUserId)) {
            msgBean.msgStatus = TheLConstants.MsgSendingStatusConstants.MSG_BLOCK;
            ChatServiceManager.getInstance().insertNewMsg(msgBean, DBUtils.getMainProcessChatTableName(msgBean), 1);
            msglist.add(msgBean);
            adapter.freshAdapter(msglist, toAvatar);
            adapter.notifyDataSetChanged();
            listView.setSelection(msglist.size() - 1);
            dismissKeyboard();
            if (edit != null) {
                edit.setText("");
            }
        } else if (!TextUtils.isEmpty(toUserId) && BlackUtils.isBlockMe(toUserId)) {
            msgBean.msgStatus = TheLConstants.MsgSendingStatusConstants.MSG_BLACK;
            ChatServiceManager.getInstance().insertNewMsg(msgBean, DBUtils.getMainProcessChatTableName(msgBean), 1);
            msglist.add(msgBean);
            adapter.freshAdapter(msglist, toAvatar);
            adapter.notifyDataSetChanged();
            listView.setSelection(msglist.size() - 1);
            dismissKeyboard();
            if (edit != null) {
                edit.setText("");
            }

        } else {

            L.d(TAG, " sendMsgAndSendReceipts sendMessage ");

            sendMsgAndSendReceipts();

            msgBean.msgStatus = status;

            SendMsgUtils.getInstance().sendMessage(msgBean);

        }
    }

    /**
     * @return 是否换了聊天对象
     */
    private boolean getDataFromPrePage() {
        boolean hasChangedUser = false;
        Intent intent = getIntent();
        fromPage = intent.getStringExtra("fromPage");
        usercardbean = (UserCardBean) intent.getSerializableExtra("usercardbean");
        oldMomentBean = (MomentsBean) intent.getSerializableExtra(TheLConstants.BUNDLE_KEY_MOMENT_BEAN);

        L.d(TAG + 1, " fromPage : " + fromPage);

        if (null != fromPage && fromPage.equals("notification")) {

            // 从通知栏过来，下拉点击进入该页面
            MsgBean receiveMsgBean = getIntent().getParcelableExtra(TheLConstants.BUNDLE_KEY_MSG_BEAN);

            if (receiveMsgBean != null && (receiveMsgBean.fromUserId != null && !receiveMsgBean.fromUserId.equals(toUserId)) || isSystem != receiveMsgBean.isSystem)
                hasChangedUser = true;

            L.d(TAG + 1, " receiveMsgBean : " + receiveMsgBean.toString());

            String otherUserId = DBUtils.getMainProcessOtherUserId(receiveMsgBean);

            L.d(TAG + 1, " otherUserId : " + otherUserId);

            toUserId = otherUserId;
            toName = receiveMsgBean.fromNickname;
            toMessageUser = TheLConstants.MSG_ACCOUNT_USER_ID_STR + toUserId;
            toAvatar = receiveMsgBean.fromAvatar;

            //            MsgTable msgTable = DBManager.getInstance().getMsgTableByUserId(otherUserId);

            MsgTable msgTable = ChatServiceManager.getInstance().getMsgTableByUserId(otherUserId);

            L.d(TAG + 1, " msgTable : " + msgTable);

            if (msgTable != null) {
                toUserId = msgTable.userId;
                toName = msgTable.userName;
                toMessageUser = TheLConstants.MSG_ACCOUNT_USER_ID_STR + toUserId;
                toAvatar = msgTable.avatar;
            }

            if (receiveMsgBean.msgType.equals(MsgBean.MSG_TYPE_FOLLOW)) {
                isSystem = MsgBean.IS_FOLLOW_MSG;
            } else {
                isSystem = MsgBean.IS_NORMAL_MSG;
            }
            isSystem = receiveMsgBean.isSystem;
        } else if (null != fromPage && fromPage.equals("MatchSuccessActivity")) {
            if ((intent.getStringExtra("toUserId") != null && !intent.getStringExtra("toUserId").equals(toUserId)) || isSystem != intent.getIntExtra("isSystem", 0)) {
                // 从匹配结果页面跳转过来

                L.d(TAG, " MatchSuccessActivity intent.getStringExtra(toUserId) : " + intent.getStringExtra("toUserId"));

                if ((intent.getStringExtra("toUserId") != null && !intent.getStringExtra("toUserId").equals(toUserId)) || isSystem != intent.getIntExtra("isSystem", 0)) {
                    hasChangedUser = true;
                    toUserId = intent.getStringExtra("toUserId");
                    matchMsg = intent.getStringExtra("MatchMsg");
                    L.d(TAG, " MatchSuccessActivity toUserId : " + toUserId);

                    toName = intent.getStringExtra("toName");
                    toMessageUser = TheLConstants.MSG_ACCOUNT_USER_ID_STR + toUserId;
                    toAvatar = intent.getStringExtra("toAvatar");
                    isSystem = intent.getIntExtra("isSystem", 0);

                    isShowMomentToChatView = oldMomentBean != null && fromPage != null && fromPage.equals("moment_to_chat");

                    if (isShowMomentToChatView) {
                        chat_moment_layout.initData(oldMomentBean);
                    }

                }
            }

        } else {
            // 从个人详情页,Message页过来的,主动创建聊天,3.0.0从SendCardActivity页面跳转过来

            L.d(TAG, " findMsgsByUserIdAndPage intent.getStringExtra(toUserId) : " + intent.getStringExtra("toUserId"));

            if ((intent.getStringExtra("toUserId") != null && !intent.getStringExtra("toUserId").equals(toUserId)) || isSystem != intent.getIntExtra("isSystem", 0)) {
                hasChangedUser = true;
                toUserId = intent.getStringExtra("toUserId");

                L.d(TAG, " findMsgsByUserIdAndPage toUserId : " + toUserId);

                toName = intent.getStringExtra("toName");
                toMessageUser = TheLConstants.MSG_ACCOUNT_USER_ID_STR + toUserId;
                toAvatar = intent.getStringExtra("toAvatar");
                isSystem = intent.getIntExtra("isSystem", 0);

                isShowMomentToChatView = oldMomentBean != null && fromPage != null && fromPage.equals("moment_to_chat");

                if (isShowMomentToChatView) {
                    chat_moment_layout.initData(oldMomentBean);
                }

            }
        }

        L.d(TAG + 1, " toUserId : " + toUserId);

        ChatServiceManager.getInstance().setUserId(toUserId);

        ChatServiceManager.getInstance().checkFriendsRelationship(toUserId, null);

        return hasChangedUser;
    }

    /**
     * 刷新标题文字，0：用户名 1：正在打字 2：正在说话
     *
     * @param type
     */
    private void setPageTitle(int type) {
        try {
            if (isSystem == MsgBean.IS_SYSTEM_MSG) {
                txt_nickname.setText(TheLApp.getContext().getText(R.string.chat_activity_system_message));
                txt_nickname.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
                avatar.setVisibility(View.GONE);
                txt_distance.setVisibility(View.GONE);
            } else if (isSystem == MsgBean.IS_WINK_MSG) {
                txt_nickname.setText(TheLApp.getContext().getText(R.string.userinfo_activity_left_wink));
                txt_nickname.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
                avatar.setVisibility(View.GONE);
                txt_distance.setVisibility(View.GONE);
            } else if (isSystem == MsgBean.IS_FOLLOW_MSG) {
                txt_nickname.setText(TheLApp.getContext().getText(R.string.userinfo_activity_follow));
                txt_nickname.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
                avatar.setVisibility(View.GONE);
                txt_distance.setVisibility(View.GONE);
            } else {
                if (TheLConstants.SYSTEM_USER.equals(toUserId)) {
                    txt_nickname.setText(TheLApp.getContext().getText(R.string.info_notifications));
                    txt_nickname.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
                    avatar.setVisibility(View.GONE);
                    txt_distance.setVisibility(View.GONE);
                } else {
                    txt_nickname.setText(toName);
                    avatar.setVisibility(View.VISIBLE);
                    ImageLoaderManager.imageLoaderCircle(avatar, R.mipmap.default_avatra, toAvatar);
                    if (simpleUserBean != null) {
                        if (TextUtils.isEmpty(simpleUserBean.distance)) {
                            txt_distance.setVisibility(View.GONE);
                            txt_nickname.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
                        } else {
                            txt_distance.setVisibility(View.VISIBLE);
                            txt_distance.setText(simpleUserBean.distance == null ? "" : simpleUserBean.distance);
                            txt_nickname.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
                        }
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 关闭键盘
     */
    private void dismissKeyboard() {
        // 取消键盘
        ViewUtils.hideSoftInput(this, edit);
    }

    /**
     * 开启键盘
     */
    private void showKeyboard() {
        // 开启键盘
        ViewUtils.showSoftInput(this, edit);
        hideBottomMore();
    }

    private void hideTypingMsg() {
        if (removeTypingMsg()) {
            isPending = false;
            // 刷新列表
            adapter.freshAdapter(msglist, toAvatar);
            adapter.notifyDataSetChanged();
            listView.setSelection(msglist.size());
        }
    }

    private boolean removeTypingMsg() {
        int index = -1;
        for (int i = msglist.size() - 1; i >= 0; i--) {
            if (msglist.get(i).isTypingMsg) {
                index = i;
                startIndex -= 1;
                break;
            }
        }
        if (index > -1) {
            msglist.remove(index);
            // 重置startIndex
            if (msglist.size() == 0 && isSystem == MsgBean.IS_NORMAL_MSG) {
                startIndex = 0;
            }
            return true;
        }
        return false;
    }

    private void refreshUi(MsgBean msgBean) {

        L.d(TAG, "----------refreshUi----------" + needRefresh);

        L.d(TAG, "----------refreshUi---------- msglist size" + msglist.size());

        if (needRefresh) {
            // 刷新UI
//            addStrangerWarningMsg(0);
            msglist.add(msgBean);
            adapter.freshAdapter(msglist, toAvatar);
            refreshSingleItem(msgBean);
            listView.setSelection(msglist.size());
            // 序列要加一，避免分页加载条目重复
            startIndex += 1;

        } else {
            needRefresh = true;
        }
    }

    private void uploadFile(String path, final String packetId, int type) {

        L.d("ChatServiceManager", " uploadFile : ");

        String uploadPath;
        if (type == 0) {
            uploadPath = RequestConstants.UPLOAD_FILE_ROOT_PATH_CHAT + ShareFileUtils.getString(ShareFileUtils.ID, "") + "/" + MD5Utils.calculateMD5(new File(path)) + ".jpg";
        } else {
            uploadPath = RequestConstants.UPLOAD_FILE_ROOT_PATH_VOICE + ShareFileUtils.getString(ShareFileUtils.ID, "") + "/" + MD5Utils.calculateMD5(new File(path)) + ".aac";
        }
        RequestBusiness.getInstance().getUploadToken(System.currentTimeMillis() + "", "", uploadPath).onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new UploadTokenConsumer1(packetId, uploadPath, path));
    }

    /**
     * 添加陌生人警示语句
     */
    private void addStrangerWarningMsg(int pos) {
        if (singleNormalMsgSize(pos) && isSystem == MsgBean.IS_NORMAL_MSG) {//如果以前没有聊天记录或者只有一条聊天记录,并且这个本聊天不是系统消息
            if (ChatServiceManager.getInstance().isStranger(toUserId)) {//如果是陌生人
                final MsgBean msgBean = new MsgBean();
                msgBean.msgType = MsgBean.MSG_TYPE_STRANGER_WARNING;
                msgBean.msgText = TheLApp.getContext().getString(R.string.stranger_warning_tip);
                msgBean.msgTime = System.currentTimeMillis();
                msgBean.msgDirection = "0";

                // new message
                msgBean.fromUserId = myUid;
                msgBean.fromNickname = myName;
                msgBean.fromAvatar = ShareFileUtils.getString(ShareFileUtils.AVATAR, "");
                msgBean.fromMessageUser = ShareFileUtils.getString(ShareFileUtils.MESSAGE_USER, "");
                msgBean.toUserId = toUserId;
                msgBean.toUserNickname = toName;
                msgBean.toAvatar = toAvatar;
                msgBean.toMessageUser = toMessageUser;
                msgBean.hadRead = 1; // 发出的消息一律为读过
                msgBean.msgStatus = MsgBean.MSG_STATUS_RECEIVED;
                msgBean.msgDirection = "0";

                if (!ChatServiceManager.getInstance().isMsgCreated(msgBean)) {

                    ChatServiceManager.getInstance().insertChatTable(msgBean, DBUtils.getChatTableName(msgBean.toUserId));//todo 添加陌生人警示语句
                    msglist.add(msgBean);
                    L.d("refresh", "uiThread:refresh");
                    adapter.freshAdapter(msglist, toAvatar);
                    refreshSingleItem(msgBean);
                    if (listView.getLastVisiblePosition() + 2 == msglist.size()) {// 当前在底部则自动滚动到底部，否则不滚动
                        listView.setSelection(msglist.size());
                    } else {
                        adapter.notifyDataSetChanged();
                    }

                    // 序列要加一，避免分页加载条目重复
                    startIndex += 1;

                }

            }
        }
    }

    private boolean singleNormalMsgSize(int pos) {//如果只有一条消息
        int size = 0;
        for (MsgBean bean : msglist) {
            if (bean.msgType != null && MsgBean.MSG_TYPE_STRANGER_WARNING.endsWith(bean.msgType)) {
                return false;
            }
            if (bean.isSystem == MsgBean.IS_NORMAL_MSG) {
                size += 1;
                if (size > pos) {
                    L.d("refresh", "size=" + size + ",count=" + msglist.size());
                    return false;
                }
            }
        }
        L.d("refresh", "size=" + size + ",count=" + msglist.size());
        return size == pos;
    }

    private void clearChat() {
        if (isSystem == MsgBean.IS_FOLLOW_MSG) {
            ChatServiceManager.getInstance().removeMsgTable(DBUtils.getFollowTableName());
        } else {
            ChatServiceManager.getInstance().removeMsgTable(toUserId);
        }

        if (msglist != null) {
            msglist.clear();
            adapter.freshAdapter(msglist, toAvatar);
            adapter.notifyDataSetChanged();
        }

    }

    /**
     * 发送回执，如果发送成功，则把消息标记为已读
     *
     * @param msgBean
     */
    private void sendReceipt(MsgBean msgBean, boolean isMsgHidingSendMsg) {

        boolean isShouldSendReceipt = msgBean.isSystem == MsgBean.IS_NORMAL_MSG && MsgBean.MSG_DIRECTION_TYPE_INCOME.equals(msgBean.msgDirection) && (!isDefaultMsgHiding || isMsgHidingSendMsg) && !msgBean.msgStatus.equals(TheLConstants.MsgSendingStatusConstants.MSG_READ);

        if (isShouldSendReceipt) {

            ChatServiceManager.getInstance().sendReceiptMsg(msgBean);

        }
    }

    private void sendReceipts(final List<MsgBean> msgBeans, final boolean isMsgHidingSendMsg) {
        L.d(TAG, " msgBeans.size : " + msgBeans.size());
        sendReceiptService.execute(new Runnable() {
            @Override
            public void run() {
                for (MsgBean msg : msgBeans) {
                    sendReceipt(msg, isMsgHidingSendMsg);
                }
            }
        });
    }

    /**
     * 发送名片
     *
     * @param userCardBean 名片对象
     */
    private void sendUserCardMsg(UserCardBean userCardBean) {
        BusinessUtils.playSound(R.raw.sound_send);
        // 组织文字消息
        MsgBean msgBean = new MsgBean();
        msgBean.msgType = MsgBean.MSG_TYPE_USER_CARD;
        msgBean.msgText = userCardBean.toJson();
        msgBean.msgTime = System.currentTimeMillis();
        msgBean.msgDirection = "0";

        // new message
        msgBean.fromUserId = myUid;
        msgBean.fromNickname = myName;
        msgBean.fromAvatar = ShareFileUtils.getString(ShareFileUtils.AVATAR, "");
        msgBean.fromMessageUser = ShareFileUtils.getString(ShareFileUtils.MESSAGE_USER, "");
        msgBean.toUserId = toUserId;
        msgBean.toUserNickname = toName;
        msgBean.toAvatar = toAvatar;
        msgBean.toMessageUser = toMessageUser;
        msgBean.hadRead = 1; // 发出的消息一律为读过

        sendMessage(msgBean, TheLConstants.MsgSendingStatusConstants.MSG_SENDING);// 发送文字消息
        if (needRefresh) {
            edit.setText("");
        }
        refreshUi(msgBean);
    }

    /**
     * 发送推荐内容
     * 4.4.0 新增推荐内容 作为单独消息发送
     *
     * @param
     */
    private void senRecommendMsg(MomentsBean momentsBean) {
        MomentsBean recommendbean = momentsBean;
        // 组织文字消息
        MsgBean msgBean = new MsgBean();
        if (MomentsBean.MOMENT_TYPE_THEME_REPLY.equals(recommendbean.momentsType)) {
            msgBean.msgType = MsgBean.MSG_TYPE_MOMENT_THEMEREPLY;
        } else if (MomentsBean.MOMENT_TYPE_IMAGE.equals(recommendbean.momentsType) || MomentsBean.MOMENT_TYPE_TEXT_IMAGE.equals(recommendbean.momentsType)) {
            msgBean.msgType = MsgBean.MSG_TYPE_MOMENT_IMAGE;
        } else if (MomentsBean.MOMENT_TYPE_TEXT.equals(recommendbean.momentsType)) {
            msgBean.msgType = MsgBean.MSG_TYPE_MOMENT_TEXT;
        } else if (MomentsBean.MOMENT_TYPE_LIVE.equals(recommendbean.momentsType)) {
            msgBean.msgType = MsgBean.MSG_TYPE_MOMENT_LIVE;
        } else if (MomentsBean.MOMENT_TYPE_VOICE_LIVE.equals(recommendbean.momentsType)) {
            msgBean.msgType = MsgBean.MSG_TYPE_MOMENT_VOICE;
        } else if (MomentsBean.MOMENT_TYPE_THEME.equals(recommendbean.momentsType)) {
            msgBean.msgType = MsgBean.MSG_TYPE_MOMENT_THEME;
        } else if (MomentsBean.MOMENT_TYPE_VIDEO.equals(recommendbean.momentsType)) {
            msgBean.msgType = MsgBean.MSG_TYPE_MOMENT_VIDEO;
        } else if (MomentsBean.MOMENT_TYPE_VOICE.equals(recommendbean.momentsType) || MomentsBean.MOMENT_TYPE_TEXT_VOICE.equals(recommendbean.momentsType)) {
            msgBean.msgType = MsgBean.MSG_TYPE_MOMENT_MUSIC;
        } else if (MomentsBean.MOMENT_TYPE_USER_CARD.equals(recommendbean.momentsType)) {
            msgBean.msgType = MsgBean.MSG_TYPE_MOMENT_USERCARD;
        } else if (MomentsBean.MOMENT_TYPE_RECOMMEND.equals(recommendbean.momentsType)) {
            msgBean.msgType = MsgBean.MSG_TYPE_MOMENT_RECOMMEND;
        } else if (MomentsBean.MOMENT_TYPE_WEB.equals(recommendbean.momentsType)) {
            msgBean.msgType = MsgBean.MSG_TYPE_MOMENT_WEB;
            recommendbean = Utils.getWebRecommendBean(recommendbean);
        } else if (MomentsBean.MOMENT_TYPE_LIVE_USER.equals(recommendbean.momentsType)) {
            msgBean.msgType = MsgBean.MSG_TYPE_MOMENT_TEXT;
            recommendbean.imageUrl = "";
        } else if (MomentsBean.MOMENT_TYPE_AD.equals(recommendbean.momentsType)) {
            msgBean.msgType = MsgBean.MSG_TYPE_MOMENT_AD;
        }
        if (recommendbean == null) {
            return;
        }

        msgBean.msgText = recommendbean.toJson(recommendbean.momentsType).toString();
        msgBean.msgTime = System.currentTimeMillis();
        msgBean.msgDirection = "0";
        msgBean.msgStatus = TheLConstants.MsgSendingStatusConstants.MSG_SUCCESS;
        msgBean.packetId = MsgUtils.getMsgId();
        // new message
        msgBean.fromUserId = myUid;
        msgBean.fromNickname = myName;
        msgBean.fromAvatar = ShareFileUtils.getString(ShareFileUtils.AVATAR, "");
        msgBean.fromMessageUser = ShareFileUtils.getString(ShareFileUtils.MESSAGE_USER, "");
        msgBean.toUserId = toUserId;
        msgBean.toUserNickname = toName;
        msgBean.toAvatar = toAvatar;
        msgBean.toMessageUser = toMessageUser;
        msgBean.hadRead = 1; // 发出的消息一律为读过

        L.subLog(TAG, " send recommend msg 1 : " + GsonUtils.createJsonString(recommendbean));

        sendMessage(msgBean, TheLConstants.MsgSendingStatusConstants.MSG_SUCCESS);// 发送文字消息

        if (!TextUtils.isEmpty(momentsBean.msgText)) {

            // 组织文字消息
            MsgBean msgBean2 = new MsgBean();
            msgBean2.msgType = "text";
            msgBean2.msgText = momentsBean.msgText == null ? "" : momentsBean.msgText;
            msgBean2.msgTime = System.currentTimeMillis();
            msgBean2.msgDirection = "0";
            msgBean2.packetId = MsgUtils.getMsgId();

            // new message
            msgBean2.fromUserId = myUid;
            msgBean2.fromNickname = myName;
            msgBean2.fromAvatar = ShareFileUtils.getString(ShareFileUtils.AVATAR, "");
            msgBean2.fromMessageUser = ShareFileUtils.getString(ShareFileUtils.MESSAGE_USER, "");
            msgBean2.toUserId = toUserId;
            msgBean2.toUserNickname = toName;
            msgBean2.toAvatar = toAvatar;
            msgBean2.toMessageUser = toMessageUser;
            msgBean2.hadRead = 1; // 发出的消息一律为读过

            L.d(TAG, " send recommend msg 2 : " + GsonUtils.createJsonString(msgBean2));

            sendMessage(msgBean2, TheLConstants.MsgSendingStatusConstants.MSG_SUCCESS);// 发送文字消息

            refreshUi(msgBean2);
        }

        if (needRefresh) {
            edit.setText("");
        }

        refreshUi(msgBean);

        showRecommendDialog();
    }

    /**
     * 4.0.0，推荐网页
     * 4.4.0 新增推荐内容 作为单独消息发送
     *
     * @param recommendWebBean
     */
    private void sendWebViewMsg(RecommendWebBean recommendWebBean) {
        // 组织文字消息
        MsgBean msgBean = new MsgBean();
        msgBean.msgText = recommendWebBean.beJson().toString();
        msgBean.msgTime = System.currentTimeMillis();
        msgBean.msgDirection = "0";
        msgBean.msgType = MsgBean.MSG_TYPE_WEB;
        // new message
        msgBean.fromUserId = myUid;
        msgBean.fromNickname = myName;
        msgBean.fromAvatar = ShareFileUtils.getString(ShareFileUtils.AVATAR, "");
        msgBean.fromMessageUser = ShareFileUtils.getString(ShareFileUtils.MESSAGE_USER, "");
        msgBean.toUserId = toUserId;
        msgBean.toUserNickname = toName;
        msgBean.toAvatar = toAvatar;
        msgBean.toMessageUser = toMessageUser;
        msgBean.hadRead = 1; // 发出的消息一律为读过
        sendMessage(msgBean, TheLConstants.MsgSendingStatusConstants.MSG_SUCCESS);// 发送文字消息

        if (!TextUtils.isEmpty(recommendText)) {
            // 组织文字消息
            MsgBean msgBean2 = new MsgBean();
            msgBean2.msgType = "text";
            msgBean2.msgText = recommendText;
            msgBean2.msgTime = System.currentTimeMillis();
            msgBean2.msgDirection = "0";

            // new message
            msgBean2.fromUserId = myUid;
            msgBean2.fromNickname = myName;
            msgBean2.fromAvatar = ShareFileUtils.getString(ShareFileUtils.AVATAR, "");
            msgBean2.fromMessageUser = ShareFileUtils.getString(ShareFileUtils.MESSAGE_USER, "");
            msgBean2.toUserId = toUserId;
            msgBean2.toUserNickname = toName;
            msgBean2.toAvatar = toAvatar;
            msgBean2.toMessageUser = toMessageUser;
            msgBean2.hadRead = 1; // 发出的消息一律为读过

            refreshUi(msgBean2);

            sendMessage(msgBean2, TheLConstants.MsgSendingStatusConstants.MSG_SUCCESS);// 发送文字消息

        }

        if (needRefresh) {
            edit.setText("");
        }
        refreshUi(msgBean);
        showRecommendDialog();
    }

    /**
     * 4.0.0，推荐主播
     * 4.4.0 新增推荐内容 作为单独消息发送
     *
     * @param liveRoomBean
     */
    private void sendLiveRoomBeanMsg(LiveRoomBean liveRoomBean) {
        // 组织文字消息
        MsgBean msgBean = new MsgBean();
        msgBean.msgText = Utils.getMomentBeanFromLiveRoomBean(liveRoomBean);
        msgBean.msgTime = System.currentTimeMillis();
        msgBean.msgDirection = "0";
        msgBean.msgType = MsgBean.MSG_TYPE_MOMENT_LIVE;
        // new message
        msgBean.fromUserId = myUid;
        msgBean.fromNickname = myName;
        msgBean.fromAvatar = ShareFileUtils.getString(ShareFileUtils.AVATAR, "");
        msgBean.fromMessageUser = ShareFileUtils.getString(ShareFileUtils.MESSAGE_USER, "");
        msgBean.toUserId = toUserId;
        msgBean.toUserNickname = toName;
        msgBean.toAvatar = toAvatar;
        msgBean.toMessageUser = toMessageUser;
        msgBean.hadRead = 1; // 发出的消息一律为读过
        sendMessage(msgBean, TheLConstants.MsgSendingStatusConstants.MSG_SUCCESS);// 发送文字消息

        if (!TextUtils.isEmpty(recommendText)) {

            // 组织文字消息
            MsgBean msgBean2 = new MsgBean();
            msgBean2.msgType = "text";
            msgBean2.msgText = recommendText;
            msgBean2.msgTime = System.currentTimeMillis();
            msgBean2.msgDirection = "0";

            // new message
            msgBean2.fromUserId = myUid;
            msgBean2.fromNickname = myName;
            msgBean2.fromAvatar = ShareFileUtils.getString(ShareFileUtils.AVATAR, "");
            msgBean2.fromMessageUser = ShareFileUtils.getString(ShareFileUtils.MESSAGE_USER, "");
            msgBean2.toUserId = toUserId;
            msgBean2.toUserNickname = toName;
            msgBean2.toAvatar = toAvatar;
            msgBean2.toMessageUser = toMessageUser;
            msgBean2.hadRead = 1; // 发出的消息一律为读过

            sendMessage(msgBean2, TheLConstants.MsgSendingStatusConstants.MSG_SUCCESS);// 发送文字消息

            refreshUi(msgBean2);

        }

        if (needRefresh) {
            edit.setText("");
        }
        refreshUi(msgBean);

        showRecommendDialog();
    }

    private void showRecommendDialog() {
        DialogUtil.showToastShort(TheLApp.getContext(), getString(R.string.shared_moment));
    }

    private void showTypingMsg() {
        if (typingMsgBean == null) {
            typingMsgBean = new MsgBean();
            typingMsgBean.msgType = MsgBean.MSG_TYPE_TYPING;
            typingMsgBean.msgTime = System.currentTimeMillis();
            typingMsgBean.msgDirection = MsgBean.MSG_DIRECTION_TYPE_INCOME;
            typingMsgBean.isTypingMsg = true;
            typingMsgBean.fromUserId = toUserId;
            typingMsgBean.fromNickname = toName;
            typingMsgBean.fromAvatar = toAvatar;
        }

        if (!isPending) {
            isPending = true;
            refreshUi(typingMsgBean);
        }

        // 正在打字的标题只显示10秒
        mHandler.removeMessages(HIDE_TYPING_MSG);
        Message msg = new Message();
        msg.what = HIDE_TYPING_MSG;
        mHandler.sendMessageDelayed(msg, 10000);
    }

    protected void handleMessageDialog(final MsgBean msgBean, final int firstPostion) {

        L.d(TAG, " handleMessageDialog : ");

        String[] selectStrings;
        if (msgBean.msgType != null && msgBean.msgType.equals("text")) {
            selectStrings = new String[]{TheLApp.getContext().getString(R.string.chat_activity_info_delete), TheLApp.getContext().getString(R.string.chat_activity_info_copy)};
        } else {
            selectStrings = new String[]{TheLApp.getContext().getString(R.string.chat_activity_info_delete)};
        }

        DialogUtil.getInstance().showSelectionDialog(this, selectStrings, new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                DialogUtil.getInstance().closeDialog();
                switch (position) {
                    case 0:// 删除
                        int msgid = msgBean.id;

                        L.d(TAG, " isSystem : " + isSystem);

                        if (isSystem == MsgBean.IS_FOLLOW_MSG) {

                            ChatServiceManager.getInstance().deleteMsgById(DBUtils.getFollowTableName(), msgBean.packetId);

                            MsgBean msgBean1 = ChatServiceManager.getInstance().getLastMsg(DBUtils.getFollowTableName());

                            if (msgBean1 == null) {
                                ChatServiceManager.getInstance().removeMsgTable(DBUtils.getFollowTableName());
                            }

                            msglist.remove(msgBean);

                        } else {

                            String tableName = DBUtils.getChatTableName(toUserId);

                            ChatServiceManager.getInstance().deleteMsgById(tableName, msgBean.packetId);

                            msglist.remove(msgBean);

                            boolean isStranger = ChatServiceManager.getInstance().isStranger(toUserId);

                            L.d(TAG, " isStranger : " + isStranger);

                            MsgBean mb = ChatServiceManager.getInstance().getLastMsg(DBUtils.getChatTableName(toUserId));

                            L.d(TAG, " mb : " + mb);

                            if (mb == null) {
                                ChatServiceManager.getInstance().removeMsgTable(toUserId);
                                if (isStranger) {
                                    List<MsgTable> msgTables = ChatServiceManager.getInstance().getStrangerMsg();
                                    if (msgTables.size() == 0) {
                                        ChatServiceManager.getInstance().removeMsgTable(DBUtils.getStrangerTableName());
                                    } else {
                                        MsgTable msgTable = msgTables.get(msgTables.size() - 1);
                                        MsgBean msgBean1 = ChatServiceManager.getInstance().getLastMsg(DBUtils.getChatTableName(msgTable.userId));
                                        pushUpdateInfo(msgBean1);
                                    }

                                }
                            } else {
                                if (isStranger) {

                                    MsgTable msgTable = ChatServiceManager.getInstance().getMsgTableByUserId(DBConstant.Message.STRANGER_TABLE_NAME);

                                    if (msgTable != null) {
                                        msgTable.fromNickname = DBUtils.getMainProcessOtherUserName(mb);
                                        msgTable.fromAvatar = DBUtils.getMainProcessOtherAvatar(mb);
                                        msgTable.msgType = mb.msgType;
                                        msgTable.msgText = mb.msgText;
                                        if (msgTable.msgTime < DBConstant.STICK_TOP_BASE_TIME) {
                                            msgTable.msgTime = mb.msgTime;
                                        }
                                    }

                                    ChatServiceManager.getInstance().updateMsgTableWithWink(msgTable);
                                    ChatServiceManager.getInstance().pushUpdateMsg(msgTable);
                                }
                                pushUpdateInfo(mb);
                            }
                        }


                        // 删除列表中的该条数据
                        for (MsgBean msg : msglist) {
                            if (msg.id == msgid) {
                                msglist.remove(msg);
                                startIndex -= 1;
                                if (MsgBean.MSG_TYPE_IMAGE.equals(msg.msgType) && msgBean.filePath != null)
                                    for (int i = photos.size() - 1; i >= 0; i--) {
                                        if (msgBean.filePath.equals(photos.get(i))) {
                                            photos.remove(i);
                                            break;
                                        }
                                    }
                                break;
                            }
                        }
                        // 重置startIndex
                        if (msglist.size() == 0 && isSystem == MsgBean.IS_NORMAL_MSG) {
                            startIndex = 0;
                        }
                        // 刷新列表
                        adapter.freshAdapter(msglist, toAvatar);
                        adapter.notifyDataSetChanged();
                        listView.setSelection(firstPostion);

                        if (msgBean.msgType.equals(MsgBean.MSG_TYPE_VOICE)) {
                            deleteVoiceFile(msgBean.filePath);
                        }

                        break;
                    case 1: // 复制
                        DeviceUtils.copyToClipboard(ChatActivity.this, msgBean.msgText);
                        break;
                    default:
                        break;
                }
            }
        }, false, 2, null);

    }

    private void pushUpdateInfo(MsgBean msgBean) {
        ChatServiceManager.getInstance().updateMsgTable(msgBean);
    }

    /**
     * 发送视频文件失败
     *
     * @param packetId
     */
    private void sendVideoMsgFailed(String packetId) {
        removeSendingVideoMsg(packetId);
        final MessageDataBaseAdapter dbAdatper = MessageDataBaseAdapter.getInstance(TheLApp.getContext(), ShareFileUtils.getString(ShareFileUtils.ID, ""));
        dbAdatper.updateMsgToFailed(toUserId, packetId);
        // 刷新界面
        for (int i = msglist.size() - 1; i >= 0; i--) {
            if (!TextUtils.isEmpty(msglist.get(i).packetId) && msglist.get(i).packetId.equals(packetId)) {
                msglist.get(i).msgStatus = MsgBean.MSG_STATUS_FAILED;
                refreshSingleItem(i);
                break;
            }
        }
    }

    private void removeSendingVideoMsg(String packetId) {
        int removeIndex = -1;
        for (int i = 0; i < sendingVideoMsgs.size(); i++) {
            if (sendingVideoMsgs.get(i).packetId.equals(packetId)) {
                removeIndex = i;
                break;
            }
        }
        if (removeIndex > -1)
            sendingVideoMsgs.remove(removeIndex);
    }

    private ImageInfo handlePhoto(String imagePath, String localTempImgFileName) {
        File fileDir = new File(TheLConstants.F_TAKE_PHOTO_ROOTPATH);
        if (!fileDir.exists()) {
            fileDir.mkdirs();
        }

        int degree = ImageUtils.readPictureDegree(imagePath);

        ImageInfo imgInfo = new ImageInfo();

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true; // 只读入图边界
        BitmapFactory.decodeFile(imagePath, options);

        int zoom = 1;

        // 先判断照片的横竖
        boolean isLandscape = options.outWidth > options.outHeight;
        if (isLandscape) {
            zoom = (int) (options.outWidth / (float) TheLConstants.MAX_PIC_WIDTH);
        } else {
            zoom = (int) (options.outHeight / (float) TheLConstants.MAX_PIC_WIDTH);
        }

        if (zoom < 1) {
            zoom = 1;
            imgInfo.imgHeight = options.outHeight;
            imgInfo.imgWidth = options.outWidth;
            options.inSampleSize = zoom;
            options.inJustDecodeBounds = false; // 读取全部图信息
            Bitmap bitmap = BitmapFactory.decodeFile(imagePath, options);// 图片的压缩

            /**
             * 把图片旋转为正的方向
             */
            Bitmap newBitmap = ImageUtils.rotateImageView(degree, bitmap);

            imgInfo.imgHeight = options.outHeight;
            imgInfo.imgWidth = options.outWidth;

            String handlePhotoPath = TheLConstants.F_TAKE_PHOTO_ROOTPATH + localTempImgFileName;
            try {
                File file = new File(handlePhotoPath);
                BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
                newBitmap.compress(Bitmap.CompressFormat.JPEG, TheLConstants.PIC_QUALITY, bos);
                bos.flush();
                bos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            imgInfo.localPath = handlePhotoPath;
            saveImg(imgInfo);
        } else {
            options.inSampleSize = zoom;
            options.inJustDecodeBounds = false; // 读取全部图信息
            Bitmap bitmap = BitmapFactory.decodeFile(imagePath, options);// 图片的压缩

            /**
             * 把图片旋转为正的方向
             */
            Bitmap newBitmap = ImageUtils.rotateImageView(degree, bitmap);

            imgInfo.imgHeight = options.outHeight;
            imgInfo.imgWidth = options.outWidth;

            String handlePhotoPath = TheLConstants.F_TAKE_PHOTO_ROOTPATH + localTempImgFileName;
            try {
                File file = new File(handlePhotoPath);
                BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
                newBitmap.compress(Bitmap.CompressFormat.JPEG, TheLConstants.PIC_QUALITY, bos);
                bos.flush();
                bos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            imgInfo.localPath = handlePhotoPath;
            saveImg(imgInfo);
        }
        photos.add(imgInfo.localPath);
        return imgInfo;
    }

    /**
     * 初始化配对对话框的界面
     */
    private void initLikeUi() {
        rel_bottom.setVisibility(View.GONE);
        lin_options.setVisibility(View.GONE);
    }

    private void showAddButton() {
        txt_send.setTextColor(ContextCompat.getColor(this, R.color.text_color_light_gray));
    }

    private void showSendButton() {
        txt_send.setTextColor(ContextCompat.getColor(this, R.color.text_color_green));
    }

    private void showVoiceLayout() {
        if (rel_more_bottom.getVisibility() == View.GONE) {
            rel_more_bottom.setVisibility(View.VISIBLE);
            rel_voice.setVisibility(View.VISIBLE);
            // 面板弹出后把对话列表滚到底部
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    listView.setSelection(listView.getBottom());
                }
            }, 100);
        } else {
            if (rel_voice.getVisibility() == View.VISIBLE) {
                showKeyboard();
            } else {
                rel_voice.setVisibility(View.VISIBLE);
            }
        }
        sendEmojiLayout.setVisibility(View.GONE);
    }

    /**
     * 发送状态消息，0：正在打字 1：正在语音
     *
     * @param type
     */
    private void sendPendingMsg(int type) {
        long curTime = System.currentTimeMillis();
        // 两秒内只能发一次
        if (curTime - lastSendPendingMsgTime >= 2000 && !isDefaultMsgHiding && !isConfine && !BlackUtils.isBlack(toUserId) && !BlackUtils.isBlockMe(toUserId)) {
            lastSendPendingMsgTime = curTime;
            try {
                if (type == 0) {
                    ChatServiceManager.getInstance().sendPendingMsg(toUserId, new JSONObject().put("type", "text").toString());
                } else {
                    ChatServiceManager.getInstance().sendPendingMsg(toUserId, new JSONObject().put("type", "voice").toString());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void showStickerLayout() {
        if (rel_more_bottom.getVisibility() == View.GONE) {
            rel_more_bottom.setVisibility(View.VISIBLE);
            sendEmojiLayout.setVisibility(View.VISIBLE);
            // 面板弹出后把对话列表滚到底部
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    listView.setSelection(listView.getBottom());
                }
            }, 100);
        } else {
            if (sendEmojiLayout.getVisibility() == View.VISIBLE) {
                showKeyboard();
            } else {
                sendEmojiLayout.setVisibility(View.VISIBLE);
            }
        }
        rel_voice.setVisibility(View.GONE);
    }

    public double getAmplitude() {
        if (mRecorder != null) {
            return (mRecorder.getMaxAmplitude() / 2700.0);
        } else {
            return 0;
        }
    }

    private void updateVolume(double amp) {
        float last = volumeScale;
        switch ((int) amp) {
            case 0:
                volumeScale = 1.0f;
                break;
            case 1:
            case 2:
            case 3:
                volumeScale = 1.4f;
                break;
            case 4:
            case 5:
            case 6:
                volumeScale = 1.5f;
                break;
            case 7:
            case 8:
            case 9:
                volumeScale = 1.6f;
                break;
            default:
                volumeScale = 1.7f;
                break;
        }
        scaleAnim = new ScaleAnimation(last, volumeScale, last, volumeScale, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        scaleAnim.setDuration(POLL_INTERVAL + 50);
        volume.startAnimation(scaleAnim);
    }

    private void updateVolume1(double amp) {
        float last = volumeScale1;
        switch ((int) amp) {
            case 0:
                volumeScale1 = 1.0f;
                break;
            case 1:
            case 2:
            case 3:
                volumeScale1 = 1.2f;
                break;
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
                volumeScale1 = 1.3f;
                break;
            default:
                volumeScale1 = 1.4f;
                break;
        }
        scaleAnim1 = new ScaleAnimation(last, volumeScale1, last, volumeScale1, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        scaleAnim1.setDuration(POLL_INTERVAL + 50);
        volume1.startAnimation(scaleAnim1);
    }

    /**
     * 上传视频缩略图或视频文件到云存储服务器
     *
     * @param packetId
     * @param localPath
     * @param uploadPath
     * @param token
     * @param type       0：上传视频文件 1：上传视频缩略图
     */
    private void uploadVideoFileToCloud(final String packetId, String localPath, final String uploadPath, String token, final int type, final String videoThumnail) {
        UploadManager uploadManager = new UploadManager();
        uploadManager.put(new File(localPath), uploadPath, token, new UpCompletionHandler() {
            @Override
            public void complete(String key, ResponseInfo info, JSONObject response) {
                if (info != null && info.statusCode == 200 && uploadPath.equals(key)) {
                    for (final MsgBean msg : sendingVideoMsgs) {
                        if (msg.packetId.equals(packetId)) {
                            try {
                                JSONObject jsonObject = new JSONObject(msg.msgText);
                                if (type == 0) {
                                    if (TextUtils.isEmpty(jsonObject.optString(MsgBean.VIEDO_PATH))) {
                                        jsonObject.put(MsgBean.VIEDO_PATH, RequestConstants.VIDEO_BUCKET + uploadPath);
                                        msg.msgText = jsonObject.toString();
                                        // 刷新数据
                                        MessageDataBaseAdapter dbAdatper = MessageDataBaseAdapter.getInstance(TheLApp.getContext(), ShareFileUtils.getString(ShareFileUtils.ID, ""));
                                        dbAdatper.updateMsg(msg, toUserId);
                                    }
                                    uploadVideoThumbnail(packetId, videoThumnail);
                                } else {
                                    if (TextUtils.isEmpty(jsonObject.optString(MsgBean.VIEDO_THUMBNAIL))) {
                                        jsonObject.put(MsgBean.VIEDO_THUMBNAIL, RequestConstants.FILE_BUCKET + uploadPath);
                                        msg.msgText = jsonObject.toString();
                                        // 刷新数据
                                        MessageDataBaseAdapter dbAdatper = MessageDataBaseAdapter.getInstance(TheLApp.getContext(), ShareFileUtils.getString(ShareFileUtils.ID, ""));
                                        dbAdatper.updateMsg(msg, toUserId);
                                    }
                                    checkIfUploadVideoSucceed(packetId);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                sendVideoMsgFailed(packetId);
                            }
                            break;
                        }
                    }
                } else {
                    sendVideoMsgFailed(packetId);
                }
            }
        }, null);
    }

    private void uploadVideoThumbnail(final String packetId, final String videoThumnail) {
        // 获取视频缩略图的上传token
        final String uploadThumbnailPath = RequestConstants.UPLOAD_FILE_ROOT_PATH_CHAT + ShareFileUtils.getString(ShareFileUtils.ID, "") + "/" + MD5Utils.calculateMD5(new File(videoThumnail)) + ".jpg";
        UploadTokenConsumerBean bean = new UploadTokenConsumerBean();
        bean.type = 1;
        bean.packetId = packetId;
        bean.videoPath = videoThumnail;
        bean.uploadVideoPath = uploadThumbnailPath;
        bean.videoThumnail = "";
        RequestBusiness.getInstance().getUploadToken(System.currentTimeMillis() + "", "", uploadThumbnailPath).observeOn(AndroidSchedulers.mainThread()).onBackpressureDrop().subscribeOn(Schedulers.io()).subscribe(new UploadTokenConsumer(bean));
    }

    /**
     * 当上传视频缩略图或上传视频文件成功后，均会调此方法，检查是否缩略图和视频文件是否都上传成功了，如果成功了，则更新数据库并发送视频消息
     *
     * @param packetId
     */
    private void checkIfUploadVideoSucceed(String packetId) {
        for (final MsgBean msg : sendingVideoMsgs) {
            if (msg.packetId.equals(packetId)) {
                try {
                    JSONObject jsonObject = new JSONObject(msg.msgText);
                    if (!TextUtils.isEmpty(jsonObject.optString(MsgBean.VIEDO_PATH)) && !TextUtils.isEmpty(jsonObject.optString(MsgBean.VIEDO_THUMBNAIL))) {
                        removeSendingVideoMsg(packetId);
                        sendMessage(msg, TheLConstants.MsgSendingStatusConstants.MSG_SENDING);//发送视频
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    sendVideoMsgFailed(packetId);
                }
                break;
            }
        }
    }

    private void play(MsgBean msgBean) {
        if (!playing) {
            playMedia(msgBean);
            // 更新是否听过的状态
            if (msgBean.hadPlayed == 0 || msgBean.hadPlayed == 2) {
                msgBean.hadPlayed = 1;
                refreshSingleItem(msgBean);
                // 更新数据库
                MessageDataBaseAdapter dbAdatper = MessageDataBaseAdapter.getInstance(TheLApp.getContext(), myUid);
                dbAdatper.updataUnListenedMsg(toUserId, msgBean.id);
            }
            msgBeanPre = msgBean; // 把当前正在播放的msgBean存起来
        } else {
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.stop(); // 先停止
                msgBeanPre.isPlaying = 0;
                refreshSingleItem(msgBeanPre);
                playing = false;
                if (!msgBeanPre.filePath.equals(msgBean.filePath)) { // 如果两次点击不是同一条
                    playMedia(msgBean); // 再播放下一条
                    // 更新是否听过的状态
                    if (msgBean.hadPlayed == 0 || msgBean.hadPlayed == 2) {
                        msgBean.hadPlayed = 1;
                        refreshSingleItem(msgBean);
                        // 更新数据库
                        MessageDataBaseAdapter dbAdatper = MessageDataBaseAdapter.getInstance(TheLApp.getContext(), myUid);
                        dbAdatper.updataUnListenedMsg(toUserId, msgBean.id);
                    }
                    msgBeanPre = msgBean; // 把当前正在播放的msgBean存起来
                }
            }
        }
        refreshSingleItem(msgBean);// 更新数据，改变播放的动画

    }

    private String getVoiceFileName(MsgBean msg) {
        return msg.fromUserId + "_" + msg.msgTime;
    }

    private void wink() {
        RequestBusiness.getInstance().sendWink(toUserId, TheLConstants.WINK_TYPE.get(TheLConstants.WINK_TYPE_WINK)).observeOn(AndroidSchedulers.mainThread()).onBackpressureDrop().subscribeOn(Schedulers.io()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
            @Override
            public void onNext(BaseDataBean data) {
                super.onNext(data);
                // 发送一条消息
                /**4.1.0不在发送挤眼消息，但本地要存储挤眼消息**/

                L.d("winkMsg", " wink() : ");

                playWinkAnim("wink.json");

            }
        });
    }

    /**
     * ae动画
     *
     * @param json
     */
    private void playWinkAnim(String json) {
        img_sendwink.setVisibility(View.VISIBLE);
        img_sendwink.setAnimation(json);
        img_sendwink.addAnimatorListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                img_sendwink.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                img_sendwink.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        img_sendwink.playAnimation();
    }

    /**
     * 这里的入参position是adapter的position，所以有注意listview头的数量
     *
     * @param position
     */
    private void startPlaybackLocal(final int position) {
        final MsgBean temp = msglist.get(position);
        final String[] paths = temp.filePath.split(",");// 视频本地地址和预览图本地地址用逗号分隔保存的
        String url = paths[0];
        String videoThumbnail = TheLConstants.FILE_PIC_URL + paths[1];
        final String videoUrl = url;

        L.d(TAG, " videoUrl : " + videoUrl);

        if (playingVideoPosition == position && player != null && player.isPlaying()) {
            stopPlayback();
            DialogUtil.getInstance().showPlayVideoDialog(this, videoUrl, videoThumbnail, "");
            return;
        }
        View converView = ViewUtils.getListViewItemByPosition(position + listView.getHeaderViewsCount(), listView);

        L.d(TAG, " converView : " + converView);

        if (converView != null) {
            stopPlayback();
            player = converView.findViewById(R.id.player_right);
            progressbar_video = converView.findViewById(R.id.progressbar_video_right);
            img_play_video = converView.findViewById(R.id.img_play_video_right);
            player.setVisibility(View.VISIBLE);
            progressbar_video.setVisibility(View.VISIBLE);
            img_play_video.setVisibility(View.GONE);
            playingVideoPosition = position + listView.getHeaderViewsCount();
            try {
                player.setDataSource(videoUrl);
            } catch (IOException e) {
                e.printStackTrace();
            }
            player.prepareAsync();
            player.requestFocus();
            player.setOnPreparedListener(new IMediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(IMediaPlayer iMediaPlayer) {

                    L.d(TAG, "----------onPrepared----------");
                    player.setVideoScalingMode(KSYMediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
                    player.setVolume(0, 0);
                    progressbar_video.setVisibility(View.GONE);
                    player.start();
                }
            });
            player.setOnCompletionListener(new IMediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(IMediaPlayer iMediaPlayer) {

                    L.d(TAG, "----------onCompletion----------");


                    if (player != null) {
                        player.seekTo(0);
                        player.start();
                    }
                }
            });
            player.setOnErrorListener(new IMediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(IMediaPlayer iMediaPlayer, int i, int i1) {

                    L.d(TAG, "----------onError----------");

                    stopPlayback();
                    return false;
                }
            });
        }
    }

    /**
     * 这里的入参position是adapter的position，所以有注意listview头的数量
     *
     * @param position
     */
    private void startPlayback(final int position) {
        final MsgBean temp = msglist.get(position);
        String url = "";
        String videoThumbnail = "";
        try {
            // 视频地址和预览图是用json封装起来的
            final JSONObject jsonObject = new JSONObject(temp.msgText);
            url = jsonObject.optString(MsgBean.VIEDO_PATH);
            videoThumbnail = jsonObject.optString(MsgBean.VIEDO_THUMBNAIL);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String videoUrl = url;
        if (playingVideoPosition == position && player != null && player.isPlaying()) {
            stopPlayback();
            DialogUtil.getInstance().showPlayVideoDialog(this, videoUrl, videoThumbnail, "1");
            return;
        }
        View converView = ViewUtils.getListViewItemByPosition(position + listView.getHeaderViewsCount(), listView);
        if (converView != null) {
            stopPlayback();
            player = converView.findViewById(R.id.player_left);
            progressbar_video = converView.findViewById(R.id.progressbar_video_left);
            img_play_video = converView.findViewById(R.id.img_play_video_left);
            player.setVisibility(View.VISIBLE);
            progressbar_video.setVisibility(View.VISIBLE);
            img_play_video.setVisibility(View.GONE);
            playingVideoPosition = position + listView.getHeaderViewsCount();
            try {
                player.setDataSource(videoUrl);
            } catch (IOException e) {
                e.printStackTrace();
            }
            player.prepareAsync();
            player.requestFocus();
            player.setOnPreparedListener(new IMediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(IMediaPlayer iMediaPlayer) {
                    player.setVideoScalingMode(KSYMediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
                    player.setVolume(0, 0);
                    progressbar_video.setVisibility(View.GONE);
                    player.start();
                }
            });
            player.setOnCompletionListener(new IMediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(IMediaPlayer iMediaPlayer) {
                    if (player != null) {
                        player.seekTo(0);
                        player.start();
                    }
                }
            });
            player.setOnErrorListener(new IMediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(IMediaPlayer iMediaPlayer, int i, int i1) {
                    stopPlayback();
                    return false;
                }
            });
        }
    }

    private void stopPlayback() {
        playingVideoPosition = -1;
        if (player != null) {
            try {
                player.setOnPreparedListener(null);
                player.setOnCompletionListener(null);
                player.seekTo(0);
                if (player.isPlaying())
                    player.stop();
            } catch (Exception e) {
                e.printStackTrace();
            }
            player.setVisibility(View.GONE);
            progressbar_video.setVisibility(View.GONE);
            img_play_video.setVisibility(View.VISIBLE);
            player = null;
            progressbar_video = null;
            img_play_video = null;
        }
    }

    // 注册广播接收器
    private void registerReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(TheLConstants.BROADCAST_NEW_MESSAGE);
        intentFilter.addAction(TheLConstants.BROADCAST_PENDING_MSG_SPEAKING);
        intentFilter.addAction(TheLConstants.BROADCAST_PENDING_MSG_TYPING);
        receiver = new ChatReceiver();
        this.registerReceiver(receiver, intentFilter);
        followStatusChangedImpl.registerReceiver(this);
        stickerUtils = new StickerUtils();
        stickerUtils.registerReceiver(this);

    }

    // 消息的广播接收器
    private class ChatReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent != null) {

                L.d(TAG, " ChatReceiver onReceive action: " + intent.getAction());

                if (TheLConstants.BROADCAST_PENDING_MSG_TYPING.equals(intent.getAction()) || TheLConstants.BROADCAST_PENDING_MSG_SPEAKING.equals(intent.getAction())) {// 收到正在打字的消息
                    if (isSystem == MsgBean.IS_NORMAL_MSG && !TextUtils.isEmpty(toUserId) && toUserId.equals(intent.getStringExtra("userId"))) {
                        showTypingMsg();
                    }
                }
            }

        }
    }

    private class DownloadVoiceAsyncTask extends AsyncTask<String, String, String> {

        private MsgBean msg;

        public DownloadVoiceAsyncTask(MsgBean msgBean) {
            msg = msgBean;
        }

        @Override
        protected String doInBackground(String... params) {

            downloadVoice(msg);

            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
        }

    }

    private void downloadVoice(final MsgBean msg) {
        mHandler.post(new Runnable() {

            @Override
            public void run() {
                msg.hadPlayed = 3;
                refreshSingleItem(msg);
            }
        });

        // 连接地址
        try {
            // 如果缓存文件夹被删除，就要新建，否则FileOutputStream会报错
            File dir = new File(TheLConstants.F_MSG_VOICE_ROOTPATH + msg.fromUserId);
            if (!dir.exists()) {
                dir.mkdirs();
            }

//            HttpGet get = new HttpGet(msg.msgText);
//            DefaultHttpClient client = new DefaultHttpClient();
//            HttpResponse response = client.execute(get);
//            if (response.getStatusLine().getStatusCode() == 200) {
//                InputStream in = response.getEntity().getContent();
//                FileOutputStream f = new FileOutputStream(new File(TheLConstants.F_MSG_VOICE_ROOTPATH + msg.fromUserId, getVoiceFileName(msg)));
//
//                // 下载的代码
//                byte[] buffer = new byte[1024];
//                int len1 = 0;
//                while ((len1 = in.read(buffer)) > 0) {
//                    f.write(buffer, 0, len1);
//                }
//                in.close();
//                f.close();
//
//                msg.filePath = TheLConstants.F_MSG_VOICE_ROOTPATH + msg.fromUserId + File.separator + getVoiceFileName(msg);
//                msg.hadPlayed = 1;
//                // 刷新消息
//                MessageDataBaseAdapter adapter = MessageDataBaseAdapter.getInstance(TheLApp.getContext(), ShareFileUtils.getString(ShareFileUtils.ID, ""));
//                adapter.updateVoiceMsgPath(msg);
//                adapter.updataUnListenedMsg(toUserId, msg.id);
//                mHandler.post(new Runnable() {
//
//                    @Override
//                    public void run() {
//                        refreshSingleItem(msg);
//                    }
//                });
//                mHandler.post(new Runnable() {
//
//                    @Override
//                    public void run() {
//                        if (!playing) {
//                            play(msg);
//                        }
//                    }
//                });
//            }

            URL url = new URL(msg.msgText);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(4000);
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Charset", "utf-8");
            connection.connect();
            FileOutputStream outputStream = new FileOutputStream(new File(TheLConstants.F_MSG_VOICE_ROOTPATH + msg.fromUserId, getVoiceFileName(msg)));
            int responseCode = connection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                InputStream inputStream = connection.getInputStream();
                BufferedInputStream bfi = new BufferedInputStream(inputStream);
                int len;
                byte[] bytes = new byte[1024];
                while ((len = bfi.read(bytes)) != -1) {
                    outputStream.write(bytes, 0, len);
                }
                outputStream.close();
                inputStream.close();
                bfi.close();

                msg.filePath = TheLConstants.F_MSG_VOICE_ROOTPATH + msg.fromUserId + File.separator + getVoiceFileName(msg);
                msg.hadPlayed = 1;
                // 刷新消息
                MessageDataBaseAdapter adapter = MessageDataBaseAdapter.getInstance(TheLApp.getContext(), ShareFileUtils.getString(ShareFileUtils.ID, ""));
                adapter.updateVoiceMsgPath(msg);
                adapter.updataUnListenedMsg(toUserId, msg.id);
                mHandler.post(new Runnable() {

                    @Override
                    public void run() {
                        refreshSingleItem(msg);
                    }
                });
                mHandler.post(new Runnable() {

                    @Override
                    public void run() {
                        if (!playing) {
                            play(msg);
                        }
                    }
                });
            }

        } catch (Exception e) {
            msg.hadPlayed = 2;
            // 刷新消息
            MessageDataBaseAdapter msgAdapter = MessageDataBaseAdapter.getInstance(TheLApp.getContext(), ShareFileUtils.getString(ShareFileUtils.ID, ""));
            msgAdapter.updateVoiceMsgToFailed(msg);
            mHandler.post(new Runnable() {

                @Override
                public void run() {
                    refreshSingleItem(msg);
                }
            });
        }
    }

    private IMsgStatusCallback mIMsgStatusCallback = new IMsgStatusCallback.Stub() {

        @Override
        public void onConnectStatus(final int status) {

            L.d("ChatServiceManager", " onConnectStatus status : " + status);

        }

        @Override
        public void onMsgSendStatus(final MsgBean msgBean) {

            L.d("ChatServiceManager", " ----------onMsgSendStatus----------- ");

            if (!isDestroyed()) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (msglist != null) {

                            for (MsgBean mb : msglist) {

                                if (mb.getPacketId() != null && msgBean.getPacketId() != null && mb.getPacketId().equals(msgBean.getPacketId())) {
                                    mb.msgStatus = msgBean.msgStatus;
                                    break;
                                }

                            }

                            if (null != adapter) {
                                adapter.notifyDataSetChanged();
                            }

                        }

                    }
                });
            }

        }

        @Override
        public void onNewMsgComing(final MsgBean msgBean) {

            L.d(TAG, " onNewMsgComing msgBean : " + msgBean.toString());

            if (!isDestroyed()) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (MsgBean.MSG_TYPE_REMOVE_FROM_BLACK_LIST.equals(msgBean.msgType) || MsgBean.MSG_TYPE_ADD_TO_BLACK_LIST.equals(msgBean.msgType)) {

                            BlackUtils.saveMsgToSP(msgBean);

                            return;

                        }

                        String otherUserId = DBUtils.getMainProcessOtherUserId(msgBean);

                        boolean isOtherUserAndFollowMsg = !otherUserId.equals(UserUtils.getMyUserId()) && msgBean.msgType.equals(MsgBean.MSG_TYPE_FOLLOW);

                        boolean isOtherUserAndWinkMsg = !otherUserId.equals(UserUtils.getMyUserId()) && msgBean.msgType.equals(MsgBean.MSG_TYPE_WINK);

                        if (toUserId == null || !toUserId.equals(otherUserId) || isOtherUserAndFollowMsg || isOtherUserAndWinkMsg) {
                            return;
                        }

                        if (null != msglist) {

                            switch (isSystem) {
                                case MsgBean.IS_FOLLOW_MSG:
                                    if (msgBean.userId.equals(DBUtils.getStrangerTableName())) {
                                        msglist.add(msgBean);
                                    }
                                    break;
                                case MsgBean.IS_NORMAL_MSG:

                                    if (msglist.size() > 0) {
                                        MsgBean lastMsgBean = msglist.get(msglist.size() - 1);
                                        if (lastMsgBean.msgType != null && lastMsgBean.msgType.equals(MsgBean.MSG_TYPE_TYPING)) {
                                            msglist.set(msglist.size() - 1, msgBean);
                                            mHandler.removeMessages(HIDE_TYPING_MSG);
                                            isPending = false;
                                        } else {
                                            msglist.add(msgBean);
                                        }
                                    } else {
                                        msglist.add(msgBean);
                                    }

                                    break;
                                default:
                                    break;
                            }

                            if (null != adapter) {
                                adapter.freshAdapter(msglist, toAvatar);
                                adapter.notifyDataSetChanged();
                                listView.smoothScrollByOffset(msglist.size() - 1);
                            }

                            if (!isDefaultMsgHiding) {
                                sendReceipt(msgBean, false);
                            }
                        }

                    }
                });
            }
        }

        @Override
        public void onUpdateMsg(MsgTable msgTable) {

        }

        @Override
        public void onDeleteMsgTable(String userId) {

        }

        @Override
        public void onChatInfo() {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    try {

                        L.d(TAG, " onChatInfo toUserId : " + toUserId);

                        MsgTable msgTable = ChatServiceManager.getInstance().getMsgTableByUserId(toUserId);

                        L.d(TAG, " onChatInfo msgTable : " + msgTable);

                        if (msgTable != null) {
                            toUserId = msgTable.userId;
                            toName = msgTable.userName;
                            toMessageUser = TheLConstants.MSG_ACCOUNT_USER_ID_STR + toUserId;
                            toAvatar = msgTable.avatar;

                            if (msgTable.msgType.equals(MsgBean.MSG_TYPE_FOLLOW)) {
                                isSystem = MsgBean.IS_FOLLOW_MSG;
                            } else {
                                isSystem = MsgBean.IS_NORMAL_MSG;
                            }

                            if (txt_nickname != null) {
                                setPageTitle(0);
                            }

                            ChatServiceManager.getInstance().setUserId(toUserId);

                            switch (isSystem) {
                                case MsgBean.IS_FOLLOW_MSG:
                                    toUserId = DBConstant.Message.FOLLOW_TABLE_NAME;
                                    getFollowData();
                                    break;
                                case MsgBean.IS_NORMAL_MSG:
                                    getMsgData();
                                    break;
                                default:
                                    getMsgData();
                                    break;
                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

        }

        @Override
        public void onTransferPercents(int percents) {

        }
    };

    private int oldPosition = 0;

    private void getMsgData() {

        Flowable.create(new FlowableOnSubscribe<List<MsgBean>>() {
            @Override
            public void subscribe(FlowableEmitter<List<MsgBean>> e) throws Exception {

                List<MsgBean> msgBeanList = ChatServiceManager.getInstance().findMsgsByUserIdAndPage(toUserId, String.valueOf(20), String.valueOf(msglist.size()));

                e.onNext(msgBeanList);

                e.onComplete();

            }
        }, BackpressureStrategy.DROP).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(getConsumer(false));

    }

    private void sendMsgAndSendReceipts() {

        L.d(TAG, "---------sendMsgAndSendReceipts--------" + isDefaultMsgHiding);

        if (isDefaultMsgHiding && toUserId != null) {

            Flowable.create(new FlowableOnSubscribe<List<MsgBean>>() {
                @Override
                public void subscribe(FlowableEmitter<List<MsgBean>> e) throws Exception {

                    List<MsgBean> msgBeanList = ChatServiceManager.getInstance().findMsgsByUserIdAndPage(toUserId, String.valueOf(100), String.valueOf(0));

                    if (msgBeanList != null && msgBeanList.size() > 0) {
                        e.onNext(msgBeanList);
                        e.onComplete();
                    }

                }
            }, BackpressureStrategy.DROP).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<List<MsgBean>>() {
                @Override
                public void onSubscribe(Subscription s) {
                    s.request(Integer.MAX_VALUE);
                }

                @Override
                public void onNext(List<MsgBean> msgBeans) {
                    sendReceipts(msgBeans, true);
                }

                @Override
                public void onError(Throwable t) {

                }

                @Override
                public void onComplete() {

                }
            });

        }
    }

    private void getFollowData() {

        Flowable.create(new FlowableOnSubscribe<List<MsgBean>>() {
            @Override
            public void subscribe(FlowableEmitter<List<MsgBean>> e) throws Exception {
                List<MsgBean> msgBeanList = ChatServiceManager.getInstance().getMsgListByTableNameAsync(DBConstant.Message.FOLLOW_TABLE_NAME);
                e.onNext(msgBeanList);
                e.onComplete();
            }
        }, BackpressureStrategy.DROP).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(getConsumer(true));

        canLoadMore = false;

    }

    private Subscriber<List<MsgBean>> getConsumer(final boolean isFollow) {
        return new Subscriber<List<MsgBean>>() {
            @Override
            public void onSubscribe(Subscription s) {
                s.request(Integer.MAX_VALUE);
            }

            @Override
            public void onNext(List<MsgBean> msgBeans) {
                if (msgBeans != null && msgBeans.size() > 0) {
                    sendReceipts(msgBeans, false);
                    Collections.reverse(msgBeans);
                    msglist.addAll(0, msgBeans);
                    adapter.isHasBG(isHasBG);
                    adapter.isFollow(isFollow);
                    adapter.freshAdapter(msglist, toAvatar);
                    adapter.notifyDataSetChanged();
                    if (oldPosition > 0) {
                        listView.setSelection(oldPosition);
                    } else {
                        listView.setSelection(msglist.size() - 1);
                    }
                    oldPosition = msgBeans.size() - 1;

                    if (!isFollow) {
                        for (MsgBean msgBean : msgBeans) {
                            if (msgBean.msgText != null &&
                                    msgBean.msgStatus != null &&
                                    (msgBean.msgStatus.equals(TheLConstants.MsgSendingStatusConstants.MSG_SENDING) || msgBean.msgStatus.equals(TheLConstants.MsgSendingStatusConstants.MSG_FAILED))) {
                                SendMsgUtils.getInstance().sendMessage(msgBean);
                            }
                        }
                    } else {

                        if (msgBeans.size() == 0) {

                            if (isShowMomentToChatView) {

                                showMomentToChatGuide(false);

                            }

                            return;
                        }

                        if (isShowMomentToChatView) {

                            showMomentToChatGuide(true);

                        }

                        showFollowTips();
                        showMatchTips(msgBeans);
                    }

                }

                showFastEmoji(msgBeans, isFollow);

                canLoadMore = msgBeans.size() == 20;
            }

            @Override
            public void onError(Throwable t) {

            }

            @Override
            public void onComplete() {

            }

        };
    }

    private void showFastEmoji(List<MsgBean> msgBeans, boolean isFollow) {
        if (!isFollow) {
            if (msglist.size() == 0) {
                fast_emoji_rv.setVisibility(View.VISIBLE);
            }

            if (msglist.size() == 1) {
                MsgBean msgBean = msglist.get(0);
                if (msgBean.msgType.equals(MsgBean.MSG_TYPE_STRANGER_WARNING)) {
                    fast_emoji_rv.setVisibility(View.VISIBLE);
                }
            }

        }
    }

    private void showMatchTips(List<MsgBean> msgBeans) {

        if (msgBeans == null) {
            return;
        }

        boolean isMatchTipsClosed = ShareFileUtils.getBoolean(ShareFileUtils.CHAT_MATCH_FOLLOW_TIPS + toUserId, false);

        L.d(TAG, " followStatus " + followStatus);

        L.d(TAG, " isMatchTipsClosed " + isMatchTipsClosed);

        if (!isMatchTipsClosed && isHasMatchMsg(msgBeans) && (followStatus == 0 || followStatus == 2)) {
            mChatMatchTipsLayout.setVisibility(View.VISIBLE);
            mChatMatchTipsLayout.setData(toUserId, toName, toAvatar, followStatus);
        } else {
            mChatMatchTipsLayout.setVisibility(View.GONE);
        }
    }

    private void showFollowTips() {
        if (msglist.size() <= 20) {
            boolean isTipsClosed = ShareFileUtils.getBoolean(ShareFileUtils.CHAT_UN_FOLLOW_TIPS, false);
            if (hasUnFollowMsg(msglist) && mUnFollowTipsLayout != null && !isTipsClosed) {
                mUnFollowTipsLayout.setVisibility(View.VISIBLE);
            }
        }
    }

    private boolean isHasMatchMsg(List<MsgBean> msgBeans) {

        for (MsgBean msgBean : msgBeans) {

            if (msgBean.msgType.equals(MsgBean.MSG_TYPE_MATCH)) {
                return true;
            }

        }

        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        PushUtils.finish(ChatActivity.this);
    }

    private void showMomentToChatGuide(boolean isHasMsg) {

        if (!isHasMsg) {
            chat_guide_layout.show();
        } else {
            chat_guide_layout.setVisibility(View.GONE);
        }
    }

    private void getSingleUsersRelation() {
        Flowable<SingleUserRelationNetBean> flowable = DefaultRequestService.createCommonRequestService().getSingleUsersRelation(toUserId);

        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<SingleUserRelationNetBean>() {
            @Override
            public void onNext(SingleUserRelationNetBean data) {
                super.onNext(data);

                followStatus = data.data.status;

                showMatchTips(msglist);
            }
        });
    }

    private boolean hasUnFollowMsg(List<MsgBean> msgBeans) {

        for (MsgBean msgBean : msgBeans) {
            if (msgBean.msgType != null && msgBean.msgType.equals(DBConstant.Message.MSG_TYPE_UN_FOLLOW)) {
                return true;
            }
        }

        return false;
    }
}
