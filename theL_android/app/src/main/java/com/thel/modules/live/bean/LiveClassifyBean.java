package com.thel.modules.live.bean;

import com.thel.base.BaseDataBean;

import java.io.Serializable;

/**
 * 直播分类的bean
 * Created by lingwei on 2017/11/19.
 */

public class LiveClassifyBean extends BaseDataBean implements Serializable {
    public int id;

    public int nativeIcon;
    public String name;
    public String selectedIcon;
    public String icon;
    public int nativeUnSelectedIcon;

    public LiveClassifyBean(int id, String name, int nativeIcon, int unSelectedIcon, String icon, String selectedIcon) {
        this.id = id;
        this.name = name;
        this.nativeIcon = nativeIcon;
        this.nativeUnSelectedIcon = unSelectedIcon;
        this.icon = icon;
        this.selectedIcon = selectedIcon;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof LiveClassifyBean && ((LiveClassifyBean) obj).id == id;
    }

    @Override public String toString() {
        return "LiveClassifyBean{" +
                "id=" + id +
                ", nativeIcon=" + nativeIcon +
                ", name='" + name + '\'' +
                ", selectedIcon='" + selectedIcon + '\'' +
                ", icon='" + icon + '\'' +
                ", nativeUnSelectedIcon=" + nativeUnSelectedIcon +
                '}';
    }
}
