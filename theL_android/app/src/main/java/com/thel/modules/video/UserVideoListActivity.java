package com.thel.modules.video;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.bean.moments.MomentsBeanNew;
import com.thel.bean.video.VideoBean;
import com.thel.constants.TheLConstants;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.service.DefaultRequestService;
import com.thel.utils.DeviceUtils;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;
import com.thel.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fr.castorflex.android.verticalviewpager.VerticalViewPager;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import me.rela.rf_s_bridge.new_router.NewUniversalRouter;
import me.rela.rf_s_bridge.new_router.containers.NewFlutterFragment;
import me.rela.rf_s_bridge.new_router.interfaces.OnFragmentListener;
import me.rela.rf_s_bridge.router.BoostFlutterView;
import me.rela.rf_s_bridge.router.UniversalRouter;

import static com.thel.constants.TheLConstants.BUNDLE_KEY_MOMENT_ID;

/**
 * @author Liu Yun
 * @date 2017/2/9
 */

public class UserVideoListActivity extends BaseActivity implements OnFragmentListener {
    private static final String VIDEO_PLAY_FRAGMENT = "VIDEO_PLAY_FRAGMENT";
    private static final String VIDEO_LIST_FRAGMENT = "VIDEO_LIST_FRAGMENT";

    private VerticalViewPager viewpager;
    private FragmentManager manager;
    private ArrayList<Fragment> fragments = new ArrayList<>();
    private VideoFragment mVideoPlayFragment;
    private Fragment mVideoListFragment;
    private VideoAdapter adapter;
    private String userId;
    private VideoBean mVideoBean;
    private String entry;
    /**
     * 当前视频的播放次数,用于返回后更新.
     */
    public int mVideoPlayCount;
    private int currentItem;
    private float startY;
    private float endY;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_list_activity_layout);
        findViewById();
        getIntentData();
        setListener();
    }

    private void getIntentData() {
        final Bundle bd = getIntent().getExtras();
        if (bd != null) {
            userId = bd.getString(TheLConstants.BUNDLE_KEY_USER_ID);
            entry = bd.getString(TheLConstants.BUNDLE_KEY_ENTRY);

            if (entry == null) {
                entry = TheLConstants.EntryConstants.ENTRY_USERVIDEOS;
            }

            mVideoBean = (VideoBean) bd.getSerializable(TheLConstants.BUNDLE_KEY_VIDEO_BEAN);

            L.d("VideoFragment", " mVideoBean 0 : " + mVideoBean);

            if (mVideoBean != null) {
                mVideoBean.userId = userId;
                mVideoPlayCount = mVideoBean.playCount;
                addFragment();
            } else {
                String momentId = bd.getString(BUNDLE_KEY_MOMENT_ID);
                getMomentInfo(momentId);
            }
        }
    }

    private void findViewById() {
        viewpager = findViewById(R.id.viewpager);
        initTitleLayout();
    }

    private void getMomentInfo(String momentsId) {

        Flowable<MomentsBeanNew> flowable = DefaultRequestService.createAllRequestService().getMomentInfoV2(momentsId);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<MomentsBeanNew>() {
            @Override
            public void onNext(MomentsBeanNew result) {
                super.onNext(result);

                L.d("VideoFragment", " getMomentInfo onNext t : " + GsonUtils.createJsonString(result));

                L.d("VideoFragment", " getMomentInfo isFinishing() : " + isFinishing());

                if (!isFinishing() && result.data != null) {
                    mVideoBean = Utils.getVideoFromMoment(result.data);
                    mVideoPlayCount = mVideoBean.playCount;
                    addFragment();
                }
            }

            @Override public void onError(Throwable t) {
                super.onError(t);

                L.d("VideoFragment", " getMomentInfo onError : " + t.getMessage());

            }
        });

    }

    private void initTitleLayout() {

    }

    private void addFragment() {

        manager = getSupportFragmentManager();
        fragments.clear();
        mVideoPlayFragment = (VideoFragment) manager.findFragmentByTag(VIDEO_PLAY_FRAGMENT);
        if (mVideoPlayFragment == null) {
            Bundle bundle = new Bundle();
            bundle.putSerializable(TheLConstants.BUNDLE_KEY_VIDEO_BEAN, mVideoBean);
            mVideoPlayFragment = VideoFragment.newInstance(mVideoBean, VideoFragment.VIDEO_SINGER, 0);
            mVideoPlayFragment.setEntry(entry);

        }
        mVideoListFragment = manager.findFragmentByTag(VIDEO_LIST_FRAGMENT);
        if (mVideoListFragment == null) {
            Bundle bundle = new Bundle();
            bundle.putString(TheLConstants.BUNDLE_KEY_USER_ID, userId);
            bundle.putString(BUNDLE_KEY_MOMENT_ID, mVideoBean.id);
            mVideoListFragment = UserVideoListFragment.newInstance(VIDEO_LIST_FRAGMENT, bundle);
        }
        Collections.addAll(fragments, mVideoPlayFragment, mVideoListFragment);
        adapter = new VideoAdapter(fragments, manager);
        viewpager.setAdapter(adapter);
        viewpager.setCurrentItem(0);
    }

    private void setListener() {

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            startY = ev.getY();
        } else if (ev.getAction() == MotionEvent.ACTION_UP) {
            endY = ev.getY();
            WindowManager windowManager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
            Point size = new Point();
            windowManager.getDefaultDisplay().getSize(size);
            int height = size.y;

            if (currentItem == 0 && endY - startY >= (height / 4)) {
                UserVideoListActivity.this.finish();
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public void onAddFragment() {

        viewpager.postDelayed(new Runnable() {
            @Override public void run() {

                NewFlutterFragment newFlutterFragment = (NewFlutterFragment) getSupportFragmentManager().findFragmentByTag("video_NewFlutterFragment");

                if (newFlutterFragment == null) {
                    newFlutterFragment = new NewFlutterFragment.NewEngineFragmentBuilder().build();
                }

                getSupportFragmentManager().beginTransaction().add(R.id.flutter_container_fl, newFlutterFragment, "video_NewFlutterFragment").commit();
            }
        }, 500);
    }

    @Override
    public void onRemoveFragment() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("video_NewFlutterFragment");
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction().remove(fragment).commit();
        }
    }

    /***************************************  内部类 或固定代码 *****************************************/
    /**
     * SearchAdapter
     */
    private class VideoAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> fragments;

        public VideoAdapter(List<Fragment> fragments, FragmentManager manager) {
            super(manager);
            this.fragments = fragments;
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

    }

    @Override
    protected void onDestroy() {
        if (mVideoBean != null) {
            final Intent intent = new Intent();
            intent.putExtra(TheLConstants.BUNDLE_KEY_PLAY_COUNT, mVideoPlayCount);
            intent.putExtra(BUNDLE_KEY_MOMENT_ID, mVideoBean.id);
            setResult(TheLConstants.RESULT_CODE_VIDEO_PLAY_COUNT, intent);
        }
        if (mVideoPlayFragment != null) {
            mVideoPlayFragment.destroy();
        }
        super.onDestroy();
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("video_NewFlutterFragment");
        if (fragment != null) {
            NewUniversalRouter.sharedInstance.nativeRequestFlutterPop();
        } else {
            if (mVideoPlayFragment != null) {
                mVideoPlayFragment.back();
            }
        }
    }
}
