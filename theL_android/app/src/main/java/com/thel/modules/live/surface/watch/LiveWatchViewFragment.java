package com.thel.modules.live.surface.watch;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.RectF;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.ViewTreeObserver;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseAdapter;
import com.thel.bean.LiveAdInfoBean;
import com.thel.bean.LiveInfoLogBean;
import com.thel.bean.RecommendedModel;
import com.thel.bean.analytics.AnalyticsBean;
import com.thel.bean.live.LinkMicResponseBean;
import com.thel.bean.live.LiveMultiOnCreateBean;
import com.thel.bean.live.LiveMultiSeatBean;
import com.thel.bean.live.MultiSpeakersBean;
import com.thel.bean.message.DanmuResultBean;
import com.thel.callback.imp.KeyboardChangeListener;
import com.thel.callback.imp.OnEditTouchListener;
import com.thel.constants.BundleConstants;
import com.thel.constants.MomentTypeConstants;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.growingio.GIOShareTrackBean;
import com.thel.growingio.GIoGiftTrackBean;
import com.thel.growingio.GrowingIoConstant;
import com.thel.imp.BaseFunctionFragment;
import com.thel.imp.black.BlackUtils;
import com.thel.imp.follow.FollowStatusChangedImpl;
import com.thel.imp.gold.GoldChangedListener;
import com.thel.imp.gold.GoldUtils;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.live.Certification.ZhimaCertificationActivity;
import com.thel.modules.live.LiveConstant;
import com.thel.modules.live.adapter.LiveRoomChatAdapter;
import com.thel.modules.live.ads.LiveAdViewManager;
import com.thel.modules.live.bean.AudienceLinkMicResponseBean;
import com.thel.modules.live.bean.DanmuBean;
import com.thel.modules.live.bean.GiftSendMsgBean;
import com.thel.modules.live.bean.LivePkAeAnimBean;
import com.thel.modules.live.bean.LivePkAssisterBean;
import com.thel.modules.live.bean.LivePkFriendBean;
import com.thel.modules.live.bean.LivePkGemNoticeBean;
import com.thel.modules.live.bean.LivePkHangupBean;
import com.thel.modules.live.bean.LivePkInitBean;
import com.thel.modules.live.bean.LivePkStartNoticeBean;
import com.thel.modules.live.bean.LivePkSummaryNoticeBean;
import com.thel.modules.live.bean.LiveRoomBean;
import com.thel.modules.live.bean.LiveRoomMsgBean;
import com.thel.modules.live.bean.LiveRoomMsgConnectMicBean;
import com.thel.modules.live.bean.LiveTopFansBean;
import com.thel.modules.live.bean.LiveTopFansTodayBean;
import com.thel.modules.live.bean.LiveUserCardBean;
import com.thel.modules.live.bean.ResponseGemBean;
import com.thel.modules.live.bean.SendGiftSuccessBean;
import com.thel.modules.live.bean.SoftEnjoyBean;
import com.thel.modules.live.bean.SoftGiftBean;
import com.thel.modules.live.bean.TopTodayBean;
import com.thel.modules.live.ctrl.FirstChargeCtrl;
import com.thel.modules.live.ctrl.GiftMsgCtrl;
import com.thel.modules.live.in.GiftLianSendCallback;
import com.thel.modules.live.in.LiveBaseView;
import com.thel.modules.live.in.LivePkContract;
import com.thel.modules.live.in.LiveShowCtrlListener;
import com.thel.modules.live.in.LiveSwitchListener;
import com.thel.modules.live.in.LiveTextSizeChangedListener;
import com.thel.modules.live.interfaces.ILive;
import com.thel.modules.live.interfaces.ILiveAudience;
import com.thel.modules.live.liveBigGiftAnimLayout.LiveBigGiftAnimLayout;
import com.thel.modules.live.liveBigGiftAnimLayout.VideoAnimListener;
import com.thel.modules.live.liverank.LiveGiftRankDialogView;
import com.thel.modules.live.surface.LiveShowContract;
import com.thel.modules.live.surface.LiveShowPresenter;
import com.thel.modules.live.surface.meet.MeetResultActivity;
import com.thel.modules.live.utils.FastGiftDialogView;
import com.thel.modules.live.utils.LiveGIOPush;
import com.thel.modules.live.utils.LiveShowDialogUtils;
import com.thel.modules.live.utils.LiveUtils;
import com.thel.modules.live.view.CloseLiveFollowDialog;
import com.thel.modules.live.view.DanmuLayoutView;
import com.thel.modules.live.view.LinkMicInfoView;
import com.thel.modules.live.view.LiveLookRankView;
import com.thel.modules.live.view.LiveNoticeShowDialog;
import com.thel.modules.live.view.LivePKBloodView;
import com.thel.modules.live.view.LivePkAssistsRankView;
import com.thel.modules.live.view.LivePkCountDownView;
import com.thel.modules.live.view.LivePkFailedView;
import com.thel.modules.live.view.LivePkMediaPlayer;
import com.thel.modules.live.view.LiveShowCloseView;
import com.thel.modules.live.view.LiveShowViewGroup;
import com.thel.modules.live.view.LiveUserAnchorCardDialogView;
import com.thel.modules.live.view.LiveUserCardDialogView;
import com.thel.modules.live.view.LiveVipEnterView;
import com.thel.modules.live.view.LiveWatchLookRankView;
import com.thel.modules.live.view.SoftGiftListView;
import com.thel.modules.live.view.SoftGiftMsgLayout;
import com.thel.modules.live.view.expensive.LiveTopGiftLayout;
import com.thel.modules.live.view.expensive.TopGiftBean;
import com.thel.modules.live.view.expensive.TopGiftItemImp;
import com.thel.modules.live.view.sound.LiveSoundCrowdDialogView;
import com.thel.modules.live.view.sound.LiveWatchMicSortDialog;
import com.thel.modules.main.home.moments.ReportActivity;
import com.thel.modules.main.home.moments.SendCardActivity;
import com.thel.modules.main.home.moments.web.WebViewActivity;
import com.thel.modules.main.me.aboutMe.MyLevelActivity;
import com.thel.modules.main.me.match.eventcollect.collect.PayLogUtils;
import com.thel.modules.main.messages.adapter.FastEmojiAdapter;
import com.thel.modules.main.messages.utils.PushUtils;
import com.thel.ui.decoration.FastMsgDivider;
import com.thel.ui.dialog.ConnectMicDialog;
import com.thel.ui.dialog.FreeGoldDialogFragment;
import com.thel.ui.dialog.RechargeDialogFragment;
import com.thel.ui.popupwindow.GiftPopupWindow;
import com.thel.ui.widget.BubbleGuideLayout;
import com.thel.ui.widget.DanmuSelectedLayout;
import com.thel.ui.widget.FastGiftGuideLayout;
import com.thel.ui.widget.GuideView;
import com.thel.ui.widget.LiveLevelUpView;
import com.thel.ui.widget.MeetItemViewGroup;
import com.thel.ui.widget.ScaleRecyclerView;
import com.thel.ui.widget.SendGiftMentionGuideLayout;
import com.thel.ui.widget.SoftInputViewGroup;
import com.thel.ui.widget.TimeTextView;
import com.thel.utils.AppInit;
import com.thel.utils.DialogUtil;
import com.thel.utils.FireBaseUtils;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;
import com.thel.utils.LinkMicScreenUtils;
import com.thel.utils.NotchUtils;
import com.thel.utils.PermissionUtil;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SimpleDraweeViewUtils;
import com.thel.utils.SizeUtils;
import com.thel.utils.StringUtils;
import com.thel.utils.TextLimitWatcher;
import com.thel.utils.ToastUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.UMShareAPI;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import me.rela.rf_s_bridge.router.BoostFlutterView;
import me.rela.rf_s_bridge.router.UniversalRouter;
import video.com.relavideolibrary.Utils.DensityUtils;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;
import static com.thel.constants.TheLConstants.BROADCAST_LIVE_CLOSE;
import static com.thel.constants.TheLConstants.BUNDLE_KEY_NICKNAME;
import static com.thel.constants.TheLConstants.BUNDLE_KEY_USER_AVATAR;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_LIVE_CLOSED;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_RESET_IS_BOTTOM;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_SCROLL_TO_BOTTOM;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_UPDATE_MY_NET_STATUS_GOOD;
import static com.thel.ui.dialog.ConnectMicDialog.TYPE_ACCEPT;
import static com.thel.ui.dialog.ConnectMicDialog.TYPE_CANCEL;
import static com.thel.ui.dialog.ConnectMicDialog.TYPE_HANGUP;
import static com.thel.ui.dialog.ConnectMicDialog.TYPE_I_NO_RESPONSE;
import static com.thel.ui.dialog.ConnectMicDialog.TYPE_REFUSE;
import static com.thel.ui.dialog.ConnectMicDialog.TYPE_REQUEST_NO_RESPONSE;

/**
 * @author lingwei
 * @date 2018/5/6
 */

public class LiveWatchViewFragment extends BaseFunctionFragment implements LiveShowContract.View, LiveShowCtrlListener, ILive, ILiveAudience, LiveBaseView<LiveWatchViewFragment>, GoldChangedListener {
    private final String TAG = LiveWatchViewFragment.class.getSimpleName();

    public static final int AUDIENCE_LINK_MIC_NONE = -1;

    public static final int AUDIENCE_LINK_MIC_CONNECTING = 0;

    public static final int AUDIENCE_LINK_MIC_SUCCESS = 1;

    private ImageView avatar;
    private RelativeLayout lin_avatar;
    private TextView txt_nickname;
    private TextView txt_audience;
    private LinearLayout ll_txt_audience;
    private TextView txt_watch_live;
    private TextView txt_follow;
    private LiveWatchLookRankView live_look_rank_view;
    private ImageView rank_iv;
    private LinearLayout soft_money_layout;
    private TextView txt_soft_money;
    private ImageView img_voice_or_video;
    private ImageView img_close;
    private ImageView chat_iv;
    private TextView txt_my_status;
    private LiveVipEnterView live_vip_enter_view;
    private ScaleRecyclerView listview_chat;
    private View lin_input;
    private RelativeLayout send_edit_msg;
    private ImageView img_switch_danmu;
    private EditText edit_input;
    private ImageView img_emoji;
    //    private LinearLayout progress_bar;
    private FrameLayout caching;
    private View lin_right_bottom_buttons;
    private ImageView img_right_active;
    private View img_right_chat;
    private View img_right_share;
    private View img_right_gift;
    private SoftGiftMsgLayout giftMsgView;
    private GiftMsgCtrl giftMsgCtrl;
    private DanmuLayoutView danmu_layout_view;
    private LiveBigGiftAnimLayout liveBigGiftAnimLayout;
    private List<LiveRoomMsgBean> msgList = new ArrayList<>();
    private LiveRoomChatAdapter adapter;
    private LinearLayoutManager recyclerManager;
    private LiveShowContract.Presenter presenter;
    private ImageView img_send_anchor;
    private FragmentActivity mActivity;
    private TextLimitWatcher textWatcher;
    private LiveRoomBean liveRoomBean;
    private LinearLayout control_ll;

    /***直播间广告 begin***/
//    private View layout_live_ad;
//    private SimpleDraweeView ad_view;
//    private TextView top_tv;
//    private TextView offset_tv;
    private LiveAdViewManager liveAdViewManager;
    /***直播间广告 end***/

    private SoftEnjoyBean softEnjoyBean;
    private List<GiftSendMsgBean> giftMsgWaitList = new ArrayList<>();//因为各种原因而 数据不全 不能显示的数据
    private List<GiftSendMsgBean> bigAnimGiftList = new ArrayList<>();//大动画播放列表
    private boolean isShowBigAnim = false;
    private boolean isGettingGiftData = false;//是否正在获取礼物列表数据
    private boolean isDanmu = false;//是否发送弹幕
    private String reportContent = "";

    LiveShowDialogUtils dialogUtils;
    protected LiveWatchObserver observer;
    private CallbackManager callbackManager;

    private static String GIFT_SEND_TYPE_SEND = "sendgift";
    private static String GIFT_SEND_TYPE_COMBO = "combogift";

    private int combo = 0;//连击次数,同一礼物连续发送次数，最开始为0

    private boolean isRefreshingMsgList;//是否正在刷新消息列表
    private boolean isBottom;//是否在消息列表底部
    private boolean destroyed;

    private KeyboardChangeListener keyboardChangeListener;
    private KeyboardChangeListener.KeyBoardListener keyBoardListener;
    private KeyboardChangeListener.KeyBoardListener keyBoardListener2;
    private RelativeLayout rel_up;
    private boolean isAnimating = false;
    private RelativeLayout rel_down;
    private LiveShowCloseView live_show_close_view;
    private boolean isCreate = false;
    private boolean isAudienceLinkMic = false;

    /***一下为直播PK新增变量***/
    private LivePKBloodView livePkBloodView;
    private LivePkCountDownView livePkCountDownView;
    private LivePkAssistsRankView livePkAssistsRandView;
    private TextView txt_pk_other_name;
    private LottieAnimationView live_pk_ae_view;
    private ImageView img_live_pk;

    private LivePkFriendBean livePkFriendBean;//直播PK的对方bean
    private float ourGem = 0f;
    private float theriGem = 0f;
    private float[] pkStreamParams = new float[4];
    private int pk_fierce_sound_play_time = 0;
    boolean in_pk = false;
    boolean in_pk_summary = false;
    private LivePkFailedView livePkFailedView;
    private View view_pk_other;
    private RelativeLayout vip_enter_and_chat_view;
    private LottieAnimationView user_is_talking;
    private RelativeLayout rel_top;

    /***一上为直播PK新增变量***/

    private LinkMicInfoView link_mic_layer;
    private TextView send_tv;
    /***软妹豆变化监听***/
    private GoldUtils goldUtils;
    /***直播礼物排行榜***/
    private LiveGiftRankDialogView live_gift_ranking_dialog_view;
    /***个人名片***/
    private LiveUserCardDialogView live_user_card_dialog_view;
    /***主播推荐名片***/
    private LiveUserAnchorCardDialogView live_user_anchor_card_dialog_view;

    private ImageView img_live_type;
    /***直播跳转监听***/
    private LiveSwitchListener liveSwitchListener;
    private LiveTopGiftLayout live_top_gift_layout;
    private List<SoftGiftBean> offlineGiftList = new ArrayList<>();
    private long intoTime; //刚进入直播间
    private long outTime;

    private static final int GET_ANCHOR_TIME = 60 * 1000;//获取主播推荐名片时间
    private boolean liveClosed = false;
    private Runnable autoSwitchNextRunnable;//自动滑动到下一个直播
    private static final int AUTO_SWITCH_NEXT_TIME = 5000;
    private MeetItemViewGroup multi_grid_view;
    private DanmuSelectedLayout danmu_selected_layout;
    private BubbleGuideLayout bubble_guide_layout;
    private View layer_view;
    private TextView live_notice;//直播公告
    private TextView notice_board_tips;

    /**
     * 多人连麦
     **/
    private List<LiveMultiSeatBean> liveMultiSeatBeans = new ArrayList<>(8);
    private LottieAnimationView meeting_anim_bg;
    private LinearLayout ll_meetion;
    private TextView meetion_count_down;
    public boolean isMeeting = false;//是否在相遇中
    //    private View layout_multi_live_ad;
//    private SimpleDraweeView ad_multi_view;
//    private TextView top_multi_tv;
    private SoftInputViewGroup root_vg;
    private boolean micSorting = false;//房间是否处于排麦模式
    private boolean isInWaitMicList = false;//是否在排麦列表中
    private boolean isRequestSortMic = false;//是否是请求排麦
    private String micStatus = LiveRoomMsgBean.TYPE_MIC_STATUS_OFF;//当前麦状态
    private boolean isBan = false;//是否被主播屏蔽

    private GiftPopupWindow mGiftPopupWindow;
    private LiveWatchMicSortDialog liveWatchMicSortDialog;
    private FastGiftGuideLayout fast_gift_guide_view;
    private GuideView mGuideView;
    private RecyclerView fast_emoji_rv;
    private LiveLevelUpView mLiveLevelUpView;

    private Handler mHandler = new Handler(Looper.getMainLooper());
    /**
     * 观众连麦是否成功
     */
    public int audienceLinkMicStatus = AUDIENCE_LINK_MIC_NONE;
    private SendGiftMentionGuideLayout gift_guide;
    private static final int FITST_WATCH_TIME = 180 * 1000;//获取观看主播时间，引导打赏指向礼物  首次3分钟提示
    private static final int SECOND_WATCH_TIME = 300 * 1000;//获取观看主播时间，引导打赏指向礼物 第二次 5分钟提示
    private static final int LAST_WATCH_TIME = 600 * 1000;//获取观看主播时间，引导打赏指向礼物 最后一次 10分钟提示
    private static int nextTime = -1;
    private static boolean hasSend = false;  //已经送过礼物
    private boolean isFirstEnter = true;

    private int barrageId = 0;//当barrageId为0是为普通消息，大于0时为弹幕消息
    public String smallLiveAuthor;
    private LinearLayout ll_fast_gift;
    private SimpleDraweeView img_fast_gift;
    private SoftGiftBean softFastGiftBean;
    private LiveUserCardBean liveUserCardBeans;
    private String pageId;
    private String liveType = "";

    //首充
    private GuideView firstChargeGuideView;
    private boolean isFirstChargeMark;

    public static LiveWatchViewFragment getInstance(Bundle bundle) {
        LiveWatchViewFragment instance = new LiveWatchViewFragment();
        instance.setArguments(bundle);
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() == null) {
            return;
        }
        /** 分享初始化 **/
        callbackManager = CallbackManager.Factory.create();
        mActivity = getActivity();
        registerReceiver();
        pageId = Utils.getPageId();


    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_live_show_multi_widget_view, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        findViewById(view);
        initAdapter();
        initLiveShow();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        new LiveShowPresenter(this);
        isCreate = true;
        setListener();
        setRelDownListener(false);
        dialogUtils = new LiveShowDialogUtils();
        showFastGiftTip();
        FirstChargeCtrl.get().register(mFirstChargeCallBack);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mHandler.removeCallbacksAndMessages(null);
        FirstChargeCtrl.get().unregister(mFirstChargeCallBack);
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {

            requestTopFansData();
            mHandler.postDelayed(runnable, 45000);
        }
    };
    private Runnable getSendGiftPointRunnable = new Runnable() {
        @Override
        public void run() {
            if (isFirstEnter) {
                isFirstEnter = false;
            } else {
                if (nextTime == -1) {
                    nextTime = 0;

                } else if (nextTime == 0) {
                    nextTime = 1;
                } else if (nextTime == 1) {
                    nextTime = 2;
                }
                if (img_fast_gift != null && img_fast_gift.getVisibility() == VISIBLE) {
                    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) gift_guide.getLayoutParams();
                    layoutParams.rightMargin = SizeUtils.dip2px(TheLApp.context, 43 * 2);
                    gift_guide.show();

                } else {
                    gift_guide.show();

                }

            }
            if (!hasSend) {
                if (nextTime == 0) {
                    mHandler.postDelayed(getSendGiftPointRunnable, FITST_WATCH_TIME);

                } else if (nextTime == 1) {
                    mHandler.postDelayed(getSendGiftPointRunnable, SECOND_WATCH_TIME);

                } else if (nextTime == 2) {
                    mHandler.postDelayed(getSendGiftPointRunnable, LAST_WATCH_TIME);

                }
            }

        }
    };

    private void requestTopFansData() {
        if (liveRoomBean != null && liveRoomBean.user != null) {
            presenter.getAnchorTopFans(liveRoomBean.user.id + "", 0);
        }
    }

    /**
     * 获取前三名粉丝信息
     */
    @Override
    public void refreshTopFans(LiveTopFansTodayBean topFansBean, int arriveToTopfans) {

        ILive iLive = this;

        if (topFansBean != null && topFansBean.data != null) {

            live_look_rank_view.initView(topFansBean.data.topFans, LiveLookRankView.TYPE_SHOW);

            if (arriveToTopfans > 0 && live_look_rank_view.isShown()) {

                View view;

                switch (arriveToTopfans) {
                    case 1:
                        view = live_look_rank_view.getLevelOne();
                        break;
                    case 2:
                        view = live_look_rank_view.getLevelTwo();
                        break;
                    case 3:
                        view = live_look_rank_view.getLevelThree();
                        break;
                    default:
                        return;
                }

                String content = TheLApp.context.getResources().getString(R.string.you_are_in_top);

                mGuideView.show(view, content);

            }

        }

    }

    @Override
    public void sendShareMsg() {
        if (observer != null) {
            observer.shareToLive();
        }
    }

    @Override
    public void sendRecommendMsg() {
        if (observer != null) {
            observer.recommendLive();
        }
    }


    @Override
    public void bindObserver(LiveWatchObserver observer) {
        this.observer = observer;
    }

    private void findViewById(View rootView) {

        rel_up = rootView.findViewById(R.id.rel_up);
        rel_down = rootView.findViewById(R.id.rel_down);
        rel_top = rootView.findViewById(R.id.rel_top);
        avatar = rootView.findViewById(R.id.avatar);
        lin_avatar = rootView.findViewById(R.id.lin_avatar);
        txt_nickname = rootView.findViewById(R.id.txt_nickname);
        txt_audience = rootView.findViewById(R.id.txt_audience);
        ll_txt_audience = rootView.findViewById(R.id.ll_txt_audience);
        txt_watch_live = rootView.findViewById(R.id.txt_watch_live);
        txt_follow = rootView.findViewById(R.id.txt_follow);

        live_look_rank_view = rootView.findViewById(R.id.live_look_rank_view);
        rank_iv = rootView.findViewById(R.id.rank_iv);

        soft_money_layout = rootView.findViewById(R.id.soft_money_layout);
        soft_money_layout.setVisibility(View.INVISIBLE);
        txt_soft_money = rootView.findViewById(R.id.txt_soft_money);//主播的软妹币

        img_send_anchor = rootView.findViewById(R.id.img_send_anchor);
        img_close = rootView.findViewById(R.id.img_close);

        txt_my_status = rootView.findViewById(R.id.txt_my_status);


        //3.1.0 新增：会员进入房间view*/
        live_vip_enter_view = rootView.findViewById(R.id.live_vip_enter_view);
        listview_chat = rootView.findViewById(R.id.listview_chat);

        lin_input = rootView.findViewById(R.id.lin_input);
        //4.0.1 直播观看底部显示
        send_edit_msg = rootView.findViewById(R.id.send_edit_msg);
        img_switch_danmu = rootView.findViewById(R.id.img_switch_danmu);
        edit_input = rootView.findViewById(R.id.edit_input);

        img_voice_or_video = rootView.findViewById(R.id.img_voice_or_video); //4.9.0
        img_emoji = rootView.findViewById(R.id.img_emoji);

        //        progress_bar = (LinearLayout) rootView.findViewById(R.id.progress_bar);

        caching = rootView.findViewById(R.id.caching);
        caching.setVisibility(View.GONE);

        //右下角buttons
        lin_right_bottom_buttons = rootView.findViewById(R.id.lin_right_bottom_buttons);
        img_right_active = rootView.findViewById(R.id.img_right_active);
        img_right_chat = rootView.findViewById(R.id.img_right_chat);
        img_right_share = rootView.findViewById(R.id.img_right_share);
        img_right_gift = rootView.findViewById(R.id.img_right_gift);

        //打赏消息显示界面r
        giftMsgView = rootView.findViewById(R.id.gift_msg_view);
        giftMsgCtrl = new GiftMsgCtrl(giftMsgView);

        danmu_layout_view = rootView.findViewById(R.id.danmu_layout_view);

        liveBigGiftAnimLayout = rootView.findViewById(R.id.big_gift_layout);//大动画布局
        liveBigGiftAnimLayout.setLayoutParams(new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, AppInit.displayMetrics.heightPixels));
        live_show_close_view = rootView.findViewById(R.id.live_show_close_view);
        /***一下为直播PK新增变量***/

        //pk血条
        livePkBloodView = rootView.findViewById(R.id.live_pk_blood_view);
        //Pk倒计时
        livePkCountDownView = rootView.findViewById(R.id.live_pk_count_down_view);
        //pk援助
        livePkAssistsRandView = rootView.findViewById(R.id.live_pk_assists_rank_view);
        //Pk的时候的对方的名字
        txt_pk_other_name = rootView.findViewById(R.id.txt_pk_other_name);
        //pk 的 AE动画
        live_pk_ae_view = rootView.findViewById(R.id.live_pk_ae_view);
        img_live_pk = rootView.findViewById(R.id.img_live_pk);
        livePkFailedView = rootView.findViewById(R.id.live_pk_failed_view);
        view_pk_other = rootView.findViewById(R.id.view_pk_other);
        /***一上为直播PK新增变量***/

        chat_iv = rootView.findViewById(R.id.chat_iv);
        /***直播间广告 begin***/
//        layout_multi_live_ad = rootView.findViewById(R.id.layout_multi_live_ad);
//        ad_multi_view = rootView.findViewById(R.id.ad_multi_view);
//        top_multi_tv = rootView.findViewById(R.id.top_multi_tv);
//        layout_live_ad = rootView.findViewById(R.id.layout_live_ad);
//        ad_view = rootView.findViewById(R.id.ad_view);
//        top_tv = rootView.findViewById(R.id.top_tv);
//        offset_tv = rootView.findViewById(R.id.offset_tv);

        ViewStub lefTopViewStub = rootView.findViewById(R.id.live_ad_viewstub);
        ViewStub rightBottomViewStub = rootView.findViewById(R.id.multi_live_ad_viewstub);
        liveAdViewManager = new LiveAdViewManager(lefTopViewStub, rightBottomViewStub);
        /***直播间广告 end***/
        link_mic_layer = rootView.findViewById(R.id.link_mic_layer);

        L.d(TAG, " link_mic_layer.isShown : " + link_mic_layer.isShown());

        link_mic_layer.setVisibility(View.VISIBLE);

        send_tv = rootView.findViewById(R.id.send_tv);
        /***直播间分类图标***/
        img_live_type = rootView.findViewById(R.id.img_live_type);

        live_top_gift_layout = rootView.findViewById(R.id.live_top_gift_layout);
        //4.8.0 新增多人直播
        multi_grid_view = rootView.findViewById(R.id.multi_grid_view);
        meeting_anim_bg = rootView.findViewById(R.id.meeting_anim_bg);
        ll_meetion = rootView.findViewById(R.id.ll_meetion);
        meetion_count_down = rootView.findViewById(R.id.meetion_count_down);
        root_vg = rootView.findViewById(R.id.root_vg);
        root_vg.setOnSoftInputShowListener(mOnSoftInputShowListener);
        root_vg.setNotInterceptViewsId(new int[]{R.id.send_edit_msg, R.id.danmu_layout});
        control_ll = rootView.findViewById(R.id.control_ll);
        vip_enter_and_chat_view = rootView.findViewById(R.id.vip_enter_and_chat_view);

        gift_guide = rootView.findViewById(R.id.gift_mention_guide_layout);
        danmu_selected_layout = rootView.findViewById(R.id.danmu_layout);
        bubble_guide_layout = rootView.findViewById(R.id.bubble_guide_layout);
        layer_view = rootView.findViewById(R.id.layer_view);
        live_notice = rootView.findViewById(R.id.live_notice);
        notice_board_tips = rootView.findViewById(R.id.notice_board_tips);
        mGuideView = rootView.findViewById(R.id.guide_view);
        //快捷礼物
        ll_fast_gift = rootView.findViewById(R.id.ll_fast_gift);
        img_fast_gift = rootView.findViewById(R.id.img_fast_gift);
        fast_gift_guide_view = rootView.findViewById(R.id.fast_gift_guide_view);

        user_is_talking = rootView.findViewById(R.id.user_is_talking);
        user_is_talking.addAnimatorListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                user_is_talking.setVisibility(INVISIBLE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        fast_emoji_rv = rootView.findViewById(R.id.fast_emoji_rv);
        mLiveLevelUpView = rootView.findViewById(R.id.live_level_up_view);

//        LiveRoomMsgBean liveRoomMsgBean = GsonUtils.getObject("{\"userId\":\"106759839\",\"avatar\":\"http://static.rela.me/app/avatar/106759839/1ca17e3c5a4af6ec914d2164914aec2d.jpg\",\"nickName\":\"test074\",\"level\":0,\"userLevel\":37,\"content\":\"恭喜test074晋升至37级\"}",LiveRoomMsgBean.class);
//
//        mLiveLevelUpView.showView(liveRoomMsgBean);

        int marginTop = 0;

        try {
            marginTop = NotchUtils.getNotchHeight(TheLApp.context);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (marginTop == 0) {
            marginTop = SizeUtils.dip2px(TheLApp.context, 24);
        }

        rel_top.setPadding(0, marginTop, 0, 0);

        rel_down.setPadding(0, marginTop, 0, 0);

        firstChargeGuideView = rootView.findViewById(R.id.first_charge_guide_view);
        initFastMsgView();
    }

    protected void initAdapter() {
        adapter = new LiveRoomChatAdapter(msgList, LiveTextSizeChangedListener.TEXT_SIZE_STANDARD);
        recyclerManager = new LinearLayoutManager(getActivity());
        recyclerManager.setOrientation(RecyclerView.VERTICAL);
        listview_chat.setLayoutManager(recyclerManager);
        listview_chat.setAdapter(adapter);

        for (int i = 0; i < 8; i++) {
            LiveMultiSeatBean liveMultiSeatBean = new LiveMultiSeatBean();
            liveMultiSeatBeans.add(liveMultiSeatBean);
        }
        //初始化多人连麦的座位adapter
        multi_grid_view.refreshAllSeats(liveMultiSeatBeans);
        multi_grid_view.setOnItemClickListener(new MeetItemViewGroup.OnItemClickListener() {
            @Override
            public void onItemClick(View view, final int position) {
                //相遇过程中，观众不能上麦
                if (isMeeting && !isGuest()) {
                    return;
                }

                if (getActivity() == null) {
                    return;
                }

                final LiveMultiSeatBean item = multi_grid_view.getItem(position);
                if (item == null) {
                    return;
                }
                String myUserId = UserUtils.getMyUserId();
                String currentUserId = item.userId;
                if (TextUtils.isEmpty(currentUserId)) {//空座位

                    //我不在其他座位上，上麦
                    if (!multi_grid_view.isOnSeat(myUserId)) {

                        PermissionUtil.requestRecordPermission(getActivity(), new PermissionUtil.PermissionCallback() {
                            @Override
                            public void granted() {

                                if (micSorting) {

                                    //排麦模式，申请排麦
                                    requestSortMic();
                                } else {

                                    Bundle bundle = new Bundle();
                                    bundle.putSerializable(TheLConstants.BUNDLE_KEY_SEAT_BEAN, item);
                                    bundle.putString(TheLConstants.BUNDLE_KEY_USER_ID, String.valueOf(liveRoomBean.user.id));
                                    final LiveSoundCrowdDialogView liveSoundCrowdDialogView = LiveSoundCrowdDialogView.newInstance(bundle);
                                    liveSoundCrowdDialogView.setType(LiveSoundCrowdDialogView.TYPE_LINK_MIC);
                                    liveSoundCrowdDialogView.setOnMicListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            if (isBan) {
                                                ToastUtils.showToastShort(getActivity(), getString(R.string.live_someone_has_been_baned));
                                            } else {
                                                observer.onSeat(position);
                                            }
                                            liveSoundCrowdDialogView.dismiss();
                                        }
                                    });
                                    liveSoundCrowdDialogView.show(getChildFragmentManager(), LiveSoundCrowdDialogView.class.getName());
                                }

                            }

                            @Override
                            public void denied() {

                            }

                            @Override
                            public void gotoSetting() {

                            }
                        });

                    }
                } else {

                    L.d(TAG, " currentUserId : " + currentUserId);

                    L.d(TAG, " myUserId : " + myUserId);

                    if (currentUserId.equals(myUserId)) {

                        //当前座位就是我，下麦
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(TheLConstants.BUNDLE_KEY_SEAT_BEAN, item);
                        bundle.putString(TheLConstants.BUNDLE_KEY_USER_ID, String.valueOf(liveRoomBean.user.id));
                        final LiveSoundCrowdDialogView liveSoundCrowdDialogView = LiveSoundCrowdDialogView.newInstance(bundle);
                        liveSoundCrowdDialogView.setExitMicListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                L.d(TAG, " position : " + position);

                                observer.offSeat(position);
                                liveSoundCrowdDialogView.dismiss();
                                img_voice_or_video.setImageResource(R.mipmap.live_icon_sofa);
                            }
                        });
                        liveSoundCrowdDialogView.setSendGiftListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //送礼物给自己
                                showGiftLayoutViewForOther(item);
                                liveSoundCrowdDialogView.dismiss();
                            }
                        });
                        liveSoundCrowdDialogView.show(getChildFragmentManager(), LiveSoundCrowdDialogView.class.getName());
                    } else {

                        //当前座位不是我
                        showGiftLayoutViewForOther(item);
                    }
                }


            }
        });
    }

    private void initFastMsgView() {

        FastEmojiAdapter fastEmojiAdapter = new FastEmojiAdapter(getActivity(), FastEmojiAdapter.MSG_TYPE_LIVE);

        fastEmojiAdapter.setOnItemClickListener(new BaseAdapter.OnItemClickListener<String>() {
            @Override
            public void onItemClick(View view, String item, int position) {
                sendMsg(item);
                fast_emoji_rv.setVisibility(View.INVISIBLE);

            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        fast_emoji_rv.addItemDecoration(new FastMsgDivider(SizeUtils.dip2px(TheLApp.context, 10)));
        fast_emoji_rv.setLayoutManager(linearLayoutManager);
        fast_emoji_rv.setAdapter(fastEmojiAdapter);
    }

    @Override
    public void initLiveShow() {
        //  liveRoomBean = null;
        removePostRunnables();
        initViewState();
        initData();
        showPkFinishView();
        setButtonClickable(false);
        dismissKeyboard();
        if (keyboardChangeListener != null) {
            keyboardChangeListener.close();
        }
        destroyed = true;
        linkMicStop();
        if (txt_follow != null && followRunnable != null) {
            txt_follow.removeCallbacks(followRunnable);
        }
        fast_emoji_rv.setVisibility(VISIBLE);
        intoTime = System.currentTimeMillis();

    }

    private void removePostRunnables() {
        if (observer != null) {
            if (autoSwitchNextRunnable != null) {
                observer.removeCallbacks(autoSwitchNextRunnable);
                autoSwitchNextRunnable = null;
            }
        }
    }

    @Override
    public void refreshLiveShow(LiveRoomBean liveRoomBean) {

        if (liveRoomBean == null) {
            return;
        }

        this.liveRoomBean = liveRoomBean;
        this.softEnjoyBean = liveRoomBean.softEnjoyBean;
        destroyed = false;
        liveClosed = false;
        initFragment(liveRoomBean);
        keyboardChangeListener = new KeyboardChangeListener(this);
        setKeyboardChangeListener();
        getGiftListNetData();
        setActivityInfoUrl(liveRoomBean.activityInfoUrl);
        adapter.setLiveRoomBean(this.liveRoomBean);
        ShareFileUtils.setString(ShareFileUtils.Brocaster_ID, liveRoomBean.user.id + "");
        if (danmu_selected_layout != null) {
            danmu_selected_layout.setGold(liveRoomBean.gold);
        }
        if (liveRoomBean.user.isFollow == 0) {
            txt_follow.postDelayed(followRunnable, 60000);
        }
        FireBaseUtils.uploadGoogle(TheLConstants.FireBaseConstant.VIEW_ITEM_LIVE, mActivity);

    }

    private Runnable followRunnable = new Runnable() {
        @Override
        public void run() {
            if (mGuideView != null) {
                mGuideView.setVisibility(View.VISIBLE);
                String followStr = TheLApp.context.getResources().getString(R.string.follow_me_if_you_like_me);
                mGuideView.show(txt_follow, followStr);
            }
        }
    };

    public void initFragment(LiveRoomBean liveRoomBean) {
        //        progress_bar.setVisibility(View.GONE);
        if (liveRoomBean.active == 0) {
            observer.sendEmptyMessage(UI_EVENT_LIVE_CLOSED);
            return;
        }

        L.d(TAG, " initFragment : " + liveRoomBean.toString());

        this.softEnjoyBean = liveRoomBean.softEnjoyBean;
        this.liveRoomBean = liveRoomBean;
        ShareFileUtils.setString(ShareFileUtils.Brocaster_ID, liveRoomBean.user.id + "");

        refreshUI();
        showGiftListData();
        refreshFollowBtn(liveRoomBean.user.isFollow);
        showBlackMeDialog(liveRoomBean.user.id);
    }

    protected void initViewState() {
        rel_up.clearAnimation();
        rel_up.setVisibility(View.VISIBLE);
        rel_up.setTranslationX(0f);
        setAnimating(false);
        txt_follow.setVisibility(View.GONE);
        caching.setVisibility(View.GONE);
        soft_money_layout.setVisibility(View.INVISIBLE);
        switchEditInputVisiable(false);
        send_edit_msg.setVisibility(View.GONE);
        if (mActivity.getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {//竖屏状态下，右边buttons条不显示，永久显示输入框
            lin_right_bottom_buttons.setVisibility(View.GONE);
            lin_input.setVisibility(View.VISIBLE);
        } else {
            lin_right_bottom_buttons.setVisibility(View.VISIBLE);
            lin_input.setVisibility(View.GONE);
            send_edit_msg.setVisibility(View.GONE);
        }
        liveBigGiftAnimLayout.clearAnimation();
        danmu_layout_view.clearAnimation();
        danmu_layout_view.removeAllViews();
        live_show_close_view.destroyView();
        giftMsgView.destoryView();
        txt_soft_money.setText("");
        live_vip_enter_view.clearAnimation();
        live_vip_enter_view.removeAllViews();
//        layout_multi_live_ad.setVisibility(View.GONE);
//        layout_live_ad.setVisibility(View.GONE);
        liveAdViewManager.dismiss();
        multi_grid_view.setVisibility(View.GONE);
        clearInput();
        setMicViewStatus();
        if (multi_grid_view != null && getActivity() != null && !getActivity().isDestroyed()) {
            multi_grid_view.clearAllView();
        }
    }

    private void initData() {
        giftMsgWaitList.clear();
        bigAnimGiftList.clear();
        isShowBigAnim = false;
        isGettingGiftData = false;
        isDanmu = false;
        msgList.clear();
        adapter.notifyDataSetChanged();
        pkStreamParams = new float[4];
        if (giftMsgCtrl != null) {
            giftMsgCtrl.clearMsg();
        }
    }

    /**
     * 观众端 背景播放相遇动画
     */
    private void playBgAnima() {
        meeting_anim_bg.setVisibility(View.VISIBLE);
        meeting_anim_bg.setAnimation("live_bg_heart.json");
        meeting_anim_bg.loop(true);
        meeting_anim_bg.playAnimation();

    }

    private boolean canGetAnchor() {
        if (destroyed || !isCreate || liveClosed) {
            return false;
        }
        if (liveRoomBean == null || liveRoomBean.user == null || liveRoomBean.user.isFollow == FOLLOW_STATUS_FOLLOW || liveRoomBean.user.isFollow == FOLLOW_STATUS_FRIEND) {
            return false;
        }
        return !BlackUtils.isBlackOrBlock(liveRoomBean.user.id + "");
    }

    /**
     * 有快捷礼物的时候展示
     */
    private void showFastGiftTip() {
        if (fast_gift_guide_view == null) {
            return;
        }

        if (!ShareFileUtils.getBoolean(ShareFileUtils.FAST_GIFT_TIP, false)) {
            fast_gift_guide_view.setVisibility(View.VISIBLE);
            fast_gift_guide_view.show();

            handleFirstChargeDelay();
        }

    }


    @Override
    public void setPresenter(LiveShowContract.Presenter presenter) {
        this.presenter = presenter;
        presenter.registerReceiver(getActivity());
    }


    private void refreshUI() {
        /*if(img_url!=null){
            ImageLoaderManager.imageLoader(preview_img);
        }*/

        L.d(TAG, " refreshUI liveRoomBean.gem : " + liveRoomBean.gem);

        ImageLoaderManager.imageLoaderDefaultCircle(avatar, R.mipmap.icon_user, liveRoomBean.user.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE);
        final String nicknameStr = Utils.getLimitedStr(liveRoomBean.user.nickName, 14);
        txt_nickname.setText(nicknameStr);
        txt_watch_live.setText(liveRoomBean.liveUsersCount + "");
        soft_money_layout.setVisibility(View.VISIBLE);
        if (!TextUtils.isEmpty(liveRoomBean.gem)) {
            txt_soft_money.setText(getString(R.string.money_soft, liveRoomBean.gem));
        } else {
            txt_soft_money.setText("");
        }
        if (!TextUtils.isEmpty(liveRoomBean.rankListImage)) {
            img_right_active.setVisibility(View.VISIBLE);
            ImageLoaderManager.imageLoader(img_right_active, liveRoomBean.rankListImage, TheLConstants.ICON_MIDDLE_SIZE, TheLConstants.ICON_MIDDLE_SIZE);
        } else {
            img_right_active.setVisibility(View.GONE);
        }
        live_look_rank_view.initView(liveRoomBean.topFans, LiveLookRankView.TYPE_SHOW);

        if (LiveRoomBean.TYPE_VOICE == liveRoomBean.audioType) {
            if (LiveRoomBean.MULTI_LIVE == liveRoomBean.isMulti) {
                img_voice_or_video.setImageResource(R.mipmap.live_icon_sofa);
                liveType = GrowingIoConstant.MATERIAL_LIVE_MULTI;

            } else {
                liveType = GrowingIoConstant.MATERIAL_LIVE_VOICE;

                img_live_type.setImageResource(R.mipmap.icon_voice_live);
                img_voice_or_video.setImageResource(R.mipmap.icn_live_share);
            }
        } else {
            liveType = GrowingIoConstant.MATERIAL_LIVE_VIDEO;

            img_live_type.setImageResource(R.mipmap.icon_camera_live);
            img_voice_or_video.setImageResource(R.mipmap.icon_connect);
        }

        if (LiveRoomBean.TYPE_VOICE == liveRoomBean.audioType && LiveRoomBean.MULTI_LIVE == liveRoomBean.isMulti) {
            multi_grid_view.setVisibility(View.VISIBLE);
            ll_meetion.setVisibility(View.GONE);
//            layout_multi_live_ad.setVisibility(View.GONE);
//            layout_live_ad.setVisibility(View.GONE);
            liveAdViewManager.dismiss();

        } else {
            multi_grid_view.setVisibility(View.GONE);
            ll_meetion.setVisibility(View.GONE);
        }

        if (liveRoomBean != null && !TextUtils.isEmpty(liveRoomBean.announcement)) {
            live_notice.setVisibility(View.VISIBLE);
            showNoticeTips();
        } else {
            live_notice.setVisibility(View.INVISIBLE);
        }
    }


    private void setListener() {
        mHandler.post(runnable);

        mHandler.post(getSendGiftPointRunnable);


        avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (liveRoomBean != null && liveRoomBean.user != null) {
                    getLiveUserCard(liveRoomBean.user.id + "");
                }
            }
        });
        //关闭直播
        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (getActivity() != null) {

                    LiveWatchActivity activity = (LiveWatchActivity) getActivity();

                    if (!activity.isShowExitDialog()) {
                        //观看时长超过5分钟，就弹出提示框。
                        long outTime = System.currentTimeMillis();

                        if (outTime - intoTime >= FITST_WATCH_TIME && liveRoomBean != null && liveRoomBean.user.isFollow == 0) {
                            CloseLiveFollowDialog closeLiveFollowDialog = new CloseLiveFollowDialog(activity);
                            closeLiveFollowDialog.setFollowAndExitListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    FollowStatusChangedImpl.followUserWithNoDialog(liveRoomBean.user.id + "", ACTION_TYPE_FOLLOW, liveRoomBean.user.nickName, liveRoomBean.user.avatar);
                                    closeLiveShow();
                                    GrowingIOUtil.followLiveUser(GrowingIoConstant.LIVE_EXIT_GUIDE, GrowingIoConstant.LIVE_TARGET_HOST);

                                }
                            });
                            closeLiveFollowDialog.setExitLiveListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    closeLiveShow();

                                }
                            });
                            try {
                                closeLiveFollowDialog.show();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else {
                            closeLiveShow();

                        }
                    }
                }
            }
        });
        //关注
        txt_follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ViewUtils.preventViewMultipleClick(v, 2000);

                if (liveRoomBean != null && liveRoomBean.user != null) {
                    FollowStatusChangedImpl.followUserWithNoDialog(liveRoomBean.user.id + "", ACTION_TYPE_FOLLOW, liveRoomBean.user.nickName, liveRoomBean.user.avatar);
                    GrowingIOUtil.followLiveUser(GrowingIoConstant.LIVE_HOST_FOLLOW, GrowingIoConstant.LIVE_TARGET_HOST);
                    FireBaseUtils.uploadGoogle(TheLConstants.FireBaseConstant.ATTENTION, mActivity);

                }
            }
        });
        //发送名片
        img_send_anchor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                if (UserUtils.isVerifyCell()) {
                    img_close.setEnabled(false);
                    //关播防误触保护
                    v.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img_close.setEnabled(true);
                        }
                    }, 500);
                    sendCard();
                }
            }
        });
        //说点什么
        chat_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (UserUtils.isVerifyCell()) {
                    ViewUtils.preventViewMultipleClick(v, 1000);
                    showKeyboard();
                }
            }
        });

        //软妹币，点击后显示软妹币贡献列表
        soft_money_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                showRankingList();
            }
        });

        //获取个人名片
        giftMsgView.setOnGiftItemClickListener(new SoftGiftMsgLayout.OnGiftItemClickListener() {
            @Override
            public void showUserMsg(View v, String userId) {
                getLiveUserCard(userId);
            }
        });
        //弹幕点击，获取个人名片
        danmu_layout_view.setOnItemClickListener(new DanmuLayoutView.OnItemClickListener() {
            @Override
            public void onClick(View v, DanmuBean danmuBean) {
                if (!Utils.isMyself(danmuBean.userId)) {
                    setReportContent(danmuBean.nickName + ":" + danmuBean.content);
                }
                getLiveUserCard(danmuBean.userId);
            }
        });
        //输入限制
        textWatcher = new TextLimitWatcher(100, true, "");
        edit_input.addTextChangedListener(textWatcher);
        //横屏状态下输入法最下面隐藏状态，点击显示输入法
        // 输入法键盘完成事件
        edit_input.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    if (!TextUtils.isEmpty(edit_input.getText().toString().trim())) {
                        if (barrageId <= 0) {
                            sendMsg();
                        } else {
                            sendDanmu();
                        }
                    }
                    return true;
                }
                return false;
            }
        });

        edit_input.setOnTouchListener(new OnEditTouchListener() {
            @Override
            protected void onTouch(boolean isSingleClick) {

                if (isSingleClick) {
                    root_vg.showKeyBoardAndHideDanmuView();
                }
            }

        });

        img_right_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLandscapeKeyboard();
            }
        });
        //横屏分享
        img_right_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                share();
            }
        });
        //跳转到直播web页面
        img_right_active.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoWebViewActivity();
            }
        });

        //显示软妹豆贡献列表
        live_look_rank_view.setRankListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRankingList();
            }
        });
        //获取直播排行前三名名片
        live_look_rank_view.setOnUserClickListener(new LiveWatchLookRankView.OnUserClickListener() {
            @Override
            public void onUserClick(LiveTopFansBean bean) {
                if (bean != null) {
                    getLiveUserCard(bean.userId);
                }
            }
        });

        rank_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRankingList();
            }
        });

        //会员进入
        live_vip_enter_view.setOnUserClickListener(new LiveVipEnterView.OnUserClickListener() {
            @Override
            public void clickUser(String userId) {
                getLiveUserCard(userId);
            }
        });
        //显示礼物打赏界面
        img_emoji.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showGiftPW(null);

            }
        });
        //发送弹幕（取消发送弹幕）
        img_switch_danmu.setOnClickListener(new View.OnClickListener() {//弹幕按钮，在有礼物打赏信息（有余额信息后）才能点击
            @Override
            public void onClick(View v) {
                switchDanmuView();
            }
        });
        adapter.setLiveChatListener(new LiveRoomChatAdapter.ChatItemClickListener() {
            @Override
            public void clickAvatar(View v, int position, LiveRoomMsgBean bean) {
                if (!TextUtils.isEmpty(bean.content)) {
                    setReportContent(bean.nickName + ":" + bean.content);
                }
                if (!TextUtils.isEmpty(bean.userId)) {
                    getLiveUserCard(bean.userId);
                }
            }

            @Override
            public void clickText(View v, int position, LiveRoomMsgBean bean) {
                if (!TextUtils.isEmpty(bean.content)) {
                    setReportContent(bean.nickName + ":" + bean.content);
                }
                if (!TextUtils.isEmpty(bean.userId)) {
                    getLiveUserCard(bean.userId);
                }
            }
        });
        rel_down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissKeyboard();
            }
        });

        /**
         * 直播Pk倒计时监听
         */
        livePkCountDownView.setOnLivePkCountDownListener(new LivePkCountDownView.LivePkCountDownListener() {
            @Override
            public void countDownPeriod(int period) {
                switch (period) {
                    case LivePkCountDownView.PERIOD_MIDDLE:
                        playPkAeAnim(LiveConstant.LIVE_PK_AE_ANIM_MIDDLE);
                        break;
                    case LivePkCountDownView.PERIOD_LAST_MINUTE:
                        playLastMinuteAnim();
                        break;
                    case LivePkCountDownView.PERIOD_LAST_TEN_SECOND:
                        showPkSound(LiveConstant.LIVE_PK_SOUND_LAST_TEN_SECOND);
                        break;
                }
            }
        });
        /***直播对方的昵称点击***/
        txt_pk_other_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (livePkFriendBean != null) {
                    presenter.getLiveUserCard(livePkFriendBean.id + "");
                }
            }
        });
        livePkAssistsRandView.setOnAssisterClickListener(new LivePkContract.AssistRankClickListener() {
            @Override
            public void onAssisterClick(LivePkAssisterBean bean, int position) {
                if (bean != null) {
                    getLiveUserCard(bean.userId);
                }
            }
        });

        link_mic_layer.setOnLinkMicHangupClickListener(new LinkMicInfoView.OnLinkMicHangupClickListener() {

            @Override
            public void onHangup(String toUserId) {//小主播
                if (observer != null && toUserId != null) {
                    observer.linkMicHangup("hangup", toUserId);
                    hangup();
                }
            }

        });

        link_mic_layer.setOnLinkMicNickNameClickListener(new LinkMicInfoView.OnLinkMicNickNameClickListener() {
            @Override
            public void onClickNickName(String toUserId) {

                getSmallLiveUserCard(toUserId);

            }
        });

        send_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(edit_input.getText().toString().trim())) {
                    if (barrageId <= 0) {
                        sendMsg();
                    } else {
                        sendDanmu();
                    }
                }
            }
        });

        liveBigGiftAnimLayout.setVideoAnimListener(new VideoAnimListener() {
            @Override
            public void complete() {
                playNextBigAnim();
            }
        });
        live_top_gift_layout.setTopGiftClickListener(new TopGiftItemImp.TopGiftClickListener() {
            @Override
            public void clickSender(String userId) {
                getLiveUserCard(userId);
            }

            @Override
            public void clickReceiver(String userId) {
                getLiveUserCard(userId);

            }

            @Override
            public void clickJumpToLive(String userId) {

            }
        });
        /***
         * 观看端 （包括嘉宾和观众）有上下麦
         * **/
        img_voice_or_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);

                //单人声音直播时
                if (liveRoomBean != null && LiveRoomBean.TYPE_VOICE == liveRoomBean.audioType && LiveRoomBean.MULTI_LIVE != liveRoomBean.isMulti) {
                    share();
                    return;
                }

                //1.如果是视频直播，观众可以申请上麦
                if (getActivity() == null) {
                    return;
                }

                if (multi_grid_view.isGuest(UserUtils.getMyUserId())) {
                    muteAudio();
                } else {
                    PermissionUtil.requestRecordPermission(getActivity(), new PermissionUtil.PermissionCallback() {
                        @Override
                        public void granted() {
                            muteAudio();
                        }

                        @Override
                        public void denied() {

                        }

                        @Override
                        public void gotoSetting() {

                        }
                    });
                }
            }
        });

        danmu_selected_layout.setOnDanmuSelectedListener(new DanmuSelectedLayout.OnDanmuSelectedListener() {
            @Override
            public void onDanmuSelected(String selectedType) {
                switch (selectedType) {
                    case DanmuSelectedLayout.TYPE_DANMU_CLOSE:

                        img_switch_danmu.setImageResource(R.mipmap.btn_dan);

                        barrageId = 0;

                        root_vg.hideDanmu(true);

                        edit_input.setHint(TheLApp.context.getResources().getString(R.string.chat_activity_input_hint));

                        break;
                    case DanmuSelectedLayout.TYPE_DANMU_LEVEL:

                        img_switch_danmu.setImageResource(R.mipmap.btn_level_barrage);

                        barrageId = 1;

                        setInputHint(2);

                        break;
                    case DanmuSelectedLayout.TYPE_DANMU_BOTTLE:

                        img_switch_danmu.setImageResource(R.mipmap.btn_bottle);

                        barrageId = 2;

                        setInputHint(3);

                        break;
                    case DanmuSelectedLayout.TYPE_DANMU_CAT:

                        img_switch_danmu.setImageResource(R.mipmap.btn_cat);

                        barrageId = 3;

                        setInputHint(3);

                        break;
                    case DanmuSelectedLayout.TYPE_DANMU_CUPID:

                        img_switch_danmu.setImageResource(R.mipmap.btn_cupid);

                        barrageId = 4;

                        setInputHint(3);

                        break;
                    default:

                        img_switch_danmu.setImageResource(R.mipmap.btn_dan);

                        barrageId = 0;

                        break;
                }
            }
        });

        layer_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (root_vg.isSoftExtend()) {
                    root_vg.toggleInput();
                }

                if (root_vg.isDanmuShowing()) {
                    root_vg.hideDanmu(false);
                }

            }
        });

        live_notice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (liveRoomBean != null && !TextUtils.isEmpty(liveRoomBean.announcement)) {
                    LiveNoticeShowDialog liveNoticeShowDialog = LiveNoticeShowDialog.newInstance(liveRoomBean.announcement);
                    liveNoticeShowDialog.show(getChildFragmentManager(), LiveNoticeShowDialog.class.getName());
                }
            }
        });

        /**
         * 赠送快捷礼物
         * */
        ll_fast_gift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (softFastGiftBean == null) {
                    return;
                }
                int fastGiftNew = ShareFileUtils.getInt(ShareFileUtils.FAST_GIFT_ID_NEW, 0);
                if (fastGiftNew == softFastGiftBean.id && ShareFileUtils.getBoolean(ShareFileUtils.FAST_GIFT_NO_REMINDER, false)) {
                    sendFastGiftMsg(softFastGiftBean);

                } else {
                    /**
                     * 快捷礼物弹窗提示
                     * */
                    PayLogUtils.getInstance().reportShortLiveSoftPayLog(GrowingIoConstant.PAGE_LIVE_ROOM, pageId, "exposure_confirm_toast", TheLConstants.IsFirstCharge, "", "", -1);


                    FastGiftDialogView fastGiftDialogView = new FastGiftDialogView(getActivity());
                    fastGiftDialogView.setData(softFastGiftBean);

                    fastGiftDialogView.setNoReminderLitener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ShareFileUtils.setBoolean(ShareFileUtils.FAST_GIFT_NO_REMINDER, true);
                            ShareFileUtils.setInt(ShareFileUtils.FAST_GIFT_ID_NEW, softFastGiftBean.id);  //保存下一次不需要再提醒的礼物id

                            sendFastGiftMsg(softFastGiftBean);

                            fastGiftDialogView.dismiss();
                            /**
                             * 快捷礼物弹窗点击
                             * */
                            PayLogUtils.getInstance().reportShortLiveSoftPayLog(GrowingIoConstant.PAGE_LIVE_ROOM, pageId, "click_confirm_toast", TheLConstants.IsFirstCharge, "", "", 1);


                        }
                    });
                    fastGiftDialogView.setReminderLitener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ShareFileUtils.setBoolean(ShareFileUtils.FAST_GIFT_NO_REMINDER, false);

                            sendFastGiftMsg(softFastGiftBean);
                            fastGiftDialogView.dismiss();
                            PayLogUtils.getInstance().reportShortLiveSoftPayLog(GrowingIoConstant.PAGE_LIVE_ROOM, pageId, "click_confirm_toast", TheLConstants.IsFirstCharge, "", "", 2);

                        }
                    });

                    fastGiftDialogView.setCancelLitener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            fastGiftDialogView.dismiss();
                            PayLogUtils.getInstance().reportShortLiveSoftPayLog(GrowingIoConstant.PAGE_LIVE_ROOM, pageId, "click_confirm_toast", TheLConstants.IsFirstCharge, "", "", 0);

                        }
                    });
                    fastGiftDialogView.show();

                }

            }
        });
    }

    private void muteAudio() {

        try {

            if (LiveRoomBean.TYPE_VIDEO == liveRoomBean.audioType) {
                if (audienceLinkMicStatus == AUDIENCE_LINK_MIC_NONE) {
                    final LiveSoundCrowdDialogView liveSoundCrowdDialogView = LiveSoundCrowdDialogView.newInstance(null);
                    liveSoundCrowdDialogView.setAskJoiningMicListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            liveSoundCrowdDialogView.dismiss();

                            if (observer != null) {

                                observer.requestLinkMicByAudience();

                                audienceLinkMicStatus = AUDIENCE_LINK_MIC_CONNECTING;

                                isAudienceLinkMic(false);

                            }

                        }
                    });
                    liveSoundCrowdDialogView.show(getChildFragmentManager(), LiveSoundCrowdDialogView.class.getName());
                } else {
                    observer.requestLinkMicAudienceList();
                }

            } else if (LiveRoomBean.MULTI_LIVE == liveRoomBean.isMulti) {

                if (!multi_grid_view.isOnSeat(UserUtils.getMyUserId())) {
                    requestSortMic();
                } else {
                    String myUserId = UserUtils.getMyUserId();
                    List<LiveMultiSeatBean> data = multi_grid_view.getData();
                    for (int i = 0; i < data.size(); i++) {
                        LiveMultiSeatBean item = data.get(i);
                        if (myUserId.equals(item.userId)) {
                            if (item.micStatus.equals(LiveRoomMsgBean.TYPE_MIC_STATUS_CLOSE)) {
                                ToastUtils.showToastShort(getActivity(), getString(R.string.ban_mic_ed));
                            } else {

                                L.d("LiveWatchMultiSound", " item.micStatus : " + item.micStatus);

                                if (observer != null) {
                                    if (item.micStatus.equals(LiveRoomMsgBean.TYPE_MIC_STATUS_OFF)) {
                                        micStatus = LiveRoomMsgBean.TYPE_MIC_STATUS_ON;
                                        observer.muteLocalAudioStream(false);
                                    } else {
                                        micStatus = LiveRoomMsgBean.TYPE_MIC_STATUS_OFF;
                                        observer.muteLocalAudioStream(true);
                                    }
                                    setMicViewStatus();
                                    observer.mute(i, micStatus);
                                }
                            }
                            break;
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendFastGiftMsg(SoftGiftBean softGiftBean) {

        if (softEnjoyBean.gold < softGiftBean.gold) {//如果点击的价格高于余额
            showRechargeDialog(false, 0, softGiftBean.title, softGiftBean.gold, softEnjoyBean.gold, "快捷礼物");//显示去充值对话框

            return;
        }
        sendGift(softGiftBean, GIFT_SEND_TYPE_SEND, 1, liveRoomBean.user.id + "");
        hasSend = true;
        /***growing 埋点购买礼物 快捷礼物*/
        outTime = System.currentTimeMillis();  //用户送礼物的时间距离开始观看时间差值
        int allPlayDuration = (int) ((outTime - intoTime) / 1000);

        GIoGiftTrackBean giftTrackBean = new GIoGiftTrackBean(GrowingIoConstant.GUICK_GIFT, "", softGiftBean.id + "", softGiftBean.gold + "", softGiftBean.title, liveRoomBean.user.id + "", liveRoomBean.user.nickName, allPlayDuration + "", liveType);
        GrowingIOUtil.payGiftTrack(giftTrackBean);

    }

    private void sendGift(SoftGiftBean softGiftBean, String type, int combo, String hostUserId) {

        if (observer != null && liveRoomBean != null) {

            if (pushLiveImNotMatch()) {

                observer.sendSoftGiftBean(softGiftBean, softGiftBean.id, type, combo, hostUserId);
            } else {
                pushLiveImNotMatch();

                ToastUtils.showToastShort(getContext(), "发送礼物失败，请重试");
            }
        }
    }

    private boolean pushLiveImNotMatch() {

        try {

            String userId = liveRoomBean.user.id + "";

            String playUrl = observer.getPlayUrl();

            String token = observer.getToken();

            L.d(TAG, " pushLiveImNotMatch userId : " + userId);

            L.d(TAG, " pushLiveImNotMatch playUrl : " + playUrl);

            L.d(TAG, " pushLiveImNotMatch token : " + token);

            boolean isMatch = !TextUtils.isEmpty(token) && playUrl.contains(userId) && token.contains(userId);

            if (getActivity() != null && !isMatch) {

                LiveWatchActivity activity = (LiveWatchActivity) getActivity();

                String liveId = liveRoomBean.liveId;

                if (activity != null) {
                    activity.getLiveRoomDetail(liveId, userId);
                }


                LiveInfoLogBean.getInstance().getLiveImNotMatchAnalytics().logType = "liveImNotMatch";

                LiveInfoLogBean.getInstance().getLiveImNotMatchAnalytics().apiData = GsonUtils.createJsonString(liveRoomBean);

                LiveInfoLogBean.getInstance().getLiveImNotMatchAnalytics().token = token;

                LiveInfoLogBean.getInstance().getLiveImNotMatchAnalytics().hostUserId = userId;

                LiveInfoLogBean.getInstance().getLiveImNotMatchAnalytics().liveUrl = playUrl;

                LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getLiveImNotMatchAnalytics());

            }

            return true;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private void setInputHint(int gold) {

        if (danmu_selected_layout.isFreeBarrage()) {
            gold = 0;
        }
        edit_input.setHint(TheLApp.context.getResources().getString(R.string.danmu_hint, gold));

    }

    public void linkMicHangup() {
        if (observer != null && liveRoomBean != null && liveRoomBean.user != null) {
            observer.linkMicHangup("hangup", String.valueOf(liveRoomBean.user.userId));
        }
    }

    public void close() {
        if (observer != null && liveRoomBean != null && liveRoomBean.user != null) {
            if (audienceLinkMicStatus == LiveWatchViewFragment.AUDIENCE_LINK_MIC_SUCCESS) {
                observer.close("hangup", String.valueOf(liveRoomBean.user.userId));
            }
        }
    }

    private void setReportContent(String content) {
        reportContent = content;
    }

    /**
     * 设置键盘监听
     */
    private void setKeyboardChangeListener() {
        keyBoardListener = new KeyboardChangeListener.KeyBoardListener() {//横屏状态下才设置监听
            @Override
            public void onKeyboardChange(boolean isShow, int keyboardHeight) {//键盘是否弹出监听
                if (isShow) {
                    lin_right_bottom_buttons.setVisibility(View.GONE);
                    listview_chat.setVisibility(View.VISIBLE);
                    setRelDownListener(true);
                } else {
                    observer.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (lin_right_bottom_buttons.getVisibility() == View.GONE) {
                                lin_right_bottom_buttons.setVisibility(View.VISIBLE);
                            }
                        }
                    }, 300);
                    setRelDownListener(false);
                    lin_input.setVisibility(View.GONE);
                    listview_chat.setVisibility(View.GONE);
                }
            }
        };
        keyBoardListener2 = new KeyboardChangeListener.KeyBoardListener() {//竖屏状态下的软键盘监听
            @Override
            public void onKeyboardChange(boolean isShow, int keyboardHeight) {//键盘是否弹出监听
                if (isShow) {
                    switchEditInputVisiable(true);
                    setRelDownListener(true);
                } else {
                    setRelDownListener(false);

                    observer.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            switchEditInputVisiable(false);

                        }
                    }, 300);

                }
            }
        };

        if (mActivity.getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {//横屏状态下才设置监听
            keyboardChangeListener.reset();
            keyboardChangeListener.setKeyBoardListener(keyBoardListener);
        } else {
            keyboardChangeListener.reset();
            keyboardChangeListener.setKeyBoardListener(keyBoardListener2);
        }
        view_pk_other.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (livePkFriendBean != null) {
                    getLiveUserCard(livePkFriendBean.id);
                }
            }
        });
    }

    /**
     * 点击键盘以外消失键盘
     *
     * @param show
     */
    private void setRelDownListener(boolean show) {
        rel_down.setClickable(show);
        /*if (show) {
            rel_down.setClickable(true);
        } else {
            rel_down.setClickable(false);
        }*/
    }

    /**
     * 发送弹幕
     */
    private void sendDanmu() {
        if (softEnjoyBean.gold < liveRoomBean.barrageCost && danmu_selected_layout != null && !danmu_selected_layout.isFreeBarrage()) {//如果点击的价格高于余额
            String giftName = TheLApp.context.getResources().getString(R.string.danmu);
            showRechargeDialog(false, 0, giftName, liveRoomBean.barrageCost, softEnjoyBean.gold, "软妹豆余额不足");//显示去充值对话框
            return;
        }

        String content = edit_input.getText().toString().trim();

        observer.sendDanmu("30", content, String.valueOf(barrageId));

        if (danmu_selected_layout != null) {

            if (danmu_selected_layout.isFreeBarrage()) {
                if (barrageId == 0) {
                    edit_input.setHint(TheLApp.context.getResources().getString(R.string.chat_activity_input_hint));
                }

                if (barrageId == 1) {
                    edit_input.setHint(TheLApp.context.getResources().getString(R.string.danmu_hint, 2));
                }

                if (barrageId > 1) {
                    edit_input.setHint(TheLApp.context.getResources().getString(R.string.danmu_hint, 3));

                }
            }
            danmu_selected_layout.setMoney();

            edit_input.setText("");
        }

    }

    /**
     * 发送消息
     */
    private void sendMsg() {

        String msg = edit_input.getText().toString().trim();

//        if (pushLiveImNotMatch()) {
        sendMsg(msg);
//        }
    }

    private void sendMsg(String msg) {

        if (msg.contains("卡")) {
            AnalyticsBean analyticsBean = LiveInfoLogBean.getInstance().getLiveLowSpeedAnalytics();
            analyticsBean.logType = "liveTxtUpload";
            analyticsBean.roomId = liveRoomBean == null ? "" : liveRoomBean.liveId;
            analyticsBean.liveUserId = UserUtils.getMyUserId();
            analyticsBean.reason = msg;
            analyticsBean.inUrl = observer.getPlayUrl();
            LiveUtils.pushLivePointLog(analyticsBean);
        }
        if (observer != null) {
            observer.sendInputMsg(msg);
        }
    }

    /**
     * 发送弹幕（取消发送弹幕）
     */
    private void switchDanmuView() {

        L.d(TAG, " root_vg.isDanmuShowing() : " + root_vg.isDanmuShowing());

        if (!root_vg.isDanmuShowing()) {
            textWatcher.setParams(60, true, getString(R.string.danmu_toast));
            final String inputSt = edit_input.getText().toString().trim();
            if (Utils.charCount(inputSt) > 60) {
                edit_input.setText(Utils.getLimitedStr(inputSt, 60));
                edit_input.setSelection(edit_input.getText().length());
            }
            root_vg.showDanmu();
        }

    }

    /**
     * 关闭键盘
     */
    private void dismissKeyboard() {
        // 取消键盘
        if (mActivity != null) {
            ViewUtils.hideSoftInput(mActivity, edit_input);
        }
    }

    /**
     * 为嘉宾显示赠送礼物（打赏）界面
     *
     * @param item
     */
    private void showGiftLayoutViewForOther(LiveMultiSeatBean item) {
        if (item == null && avatar != null) {
            return;
        }
        dismissKeyboard();
        showGiftPW(item);
    }

    private void gotoWebViewActivity() {
        //todo
    }

    /**
     * 显示横屏输入法
     */
    private void showLandscapeKeyboard() {
        lin_right_bottom_buttons.setVisibility(View.GONE);
        lin_input.setVisibility(View.VISIBLE);
        showKeyboard();
    }

    /**
     * 获取个人名片
     *
     * @param userId
     */
    private void getLiveUserCard(String userId) {
        L.i(TAG, "getLiveUserCard,userId=" + userId);
        if (!Utils.isMyself(userId)) {
            presenter.getLiveUserCard(userId);
        }
    }

    /**
     * 获取小主播的个人名片
     *
     * @param userId
     */
    private void getSmallLiveUserCard(String userId) {
        this.smallLiveAuthor = userId;
        L.i(TAG, "getLiveUserCard,userId=" + userId);
        if (!Utils.isMyself(userId)) {
            presenter.getLiveUserCard(userId);
        }
    }

    /**
     * 点 连 字连击某个礼物
     *
     * @param softGiftBean
     */
    private void showLianGiftMsgDialog(final SoftGiftBean softGiftBean, final boolean isSendAnchor, final int seatNum) {

        if (dialogUtils != null && dialogUtils.isDialogShowing()) {
            dialogUtils.closeDialog();
        }
        dialogUtils.showLianGiftMsgDialog(mActivity, softGiftBean, GIFT_SEND_TYPE_COMBO, new GiftLianSendCallback() {

            @Override
            public void lianGift(SoftGiftBean softGiftBean, String type, int combo) {
                if (softEnjoyBean.gold < liveRoomBean.barrageCost) {//如果点击的价格高于余额
                    showRechargeDialog(true, combo, softGiftBean.title, softGiftBean.gold, softEnjoyBean.gold, "软妹豆余额不足");//显示去充值对话框
                    return;
                }

                if (isSendAnchor) {//送主播礼物
                    sendGift(softGiftBean, GIFT_SEND_TYPE_COMBO, combo, liveRoomBean.user.id + "");
                } else {//送观众礼物
                    if (pushLiveImNotMatch()) {
                        observer.guestGift(softGiftBean.id, combo, seatNum);
                    }
                }
                hasSend = true;
                /***growing 埋点购买礼物*/
                outTime = System.currentTimeMillis();  //用户送礼物的时间距离开始观看时间差值
                int allPlayDuration = (int) ((outTime - intoTime) / 1000);
                GIoGiftTrackBean giftTrackBean = new GIoGiftTrackBean(GrowingIoConstant.GIFT_PURCHASE, "", softGiftBean.id + "", combo * softGiftBean.gold + "", softGiftBean.title, liveRoomBean.user.id + "", liveRoomBean.user.nickName, allPlayDuration + "", liveType);
                GrowingIOUtil.payGiftTrack(giftTrackBean);
            }
        }, new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return false;
            }
        });

    }

    /**
     * 赠送礼物
     *
     * @param softGiftBean 赠送的礼物
     * @param timeTextView
     * @param sendAnchor
     * @param seatBean
     */
    private void sendGiftMsg(SoftGiftBean softGiftBean, TimeTextView timeTextView, boolean sendAnchor, LiveMultiSeatBean seatBean, int position) {

        /**
         *等级特权礼物和会员特权礼物
         * */

        if (softGiftBean.isPrivilegeGift >= 0 && softGiftBean.privilegeGrade >= 0) {

            if (softEnjoyBean.gold < softGiftBean.gold) {//如果点击的价格高于余额
                showRechargeDialog(false, 0, softGiftBean.title, softGiftBean.gold, softEnjoyBean.gold, "软妹豆余额不足");//显示去充值对话框
                return;
            } else {
                if (liveRoomBean.selfLevel < softGiftBean.privilegeGrade) {
                    ToastUtils.showToastShort(TheLApp.context, TheLApp.context.getString(R.string.live_gift_reqire, softGiftBean.privilegeGrade));
                    return;

                }
            }

        } else {

            ToastUtils.showToastShort(TheLApp.context, TheLApp.context.getString(R.string.live_gift_reqire, softGiftBean.privilegeGrade));
            return;

        }

        if (softGiftBean.isMemberGift > 0 && UserUtils.getUserVipLevel() == 0) {
            ToastUtils.showToastShort(TheLApp.context, TheLApp.context.getString(R.string.live_gift_reqire_vip));
            return;

        }

        if (softEnjoyBean.gold < softGiftBean.gold) {//如果点击的价格高于余额
            showRechargeDialog(false, 0, softGiftBean.title, softGiftBean.gold, softEnjoyBean.gold, "软妹豆余额不足");//显示去充值对话框
            return;
        }
        if (timeTextView.isCombo()) {//如果是处于连击状态
            combo++;
        } else {
            combo = 1;
        }

        if (sendAnchor) {//送主播礼物
            sendGift(softGiftBean, GIFT_SEND_TYPE_SEND, 1, liveRoomBean.user.id + "");
        } else {//送观众礼物
            if (pushLiveImNotMatch()) {
                observer.guestGift(softGiftBean.id, 1, seatBean.seatNum);
            }
        }
        hasSend = true;
        /***growing 埋点购买礼物*/
        outTime = System.currentTimeMillis();  //用户送礼物的时间距离开始观看时间差值
        int allPlayDuration = (int) ((outTime - intoTime) / 1000);
        GIoGiftTrackBean giftTrackBean = new GIoGiftTrackBean(GrowingIoConstant.GIFT_PURCHASE, "", softGiftBean.id + "", softGiftBean.gold + "", softGiftBean.title, liveRoomBean.user.id + "", liveRoomBean.user.nickName, allPlayDuration + "", liveType);
        GrowingIOUtil.payGiftTrack(giftTrackBean);
        GrowingIOUtil.payEvar(GrowingIoConstant.G_LiveGiftLis);

    }

    /**
     * 充值对话框
     */
    private void showRechargeDialog(boolean isCombo, int comboCount, String giftName, int giftGold, long balance, String fromAction) {

        L.d(TAG, " isCombo : " + isCombo);

        L.d(TAG, " comboCount : " + comboCount);

        L.d(TAG, " giftName : " + giftName);

        L.d(TAG, " giftGold : " + giftGold);

        L.d(TAG, " balance : " + balance);

        if (getActivity() != null) {
            String tips;
            if (isCombo) {
                long difference_value = giftGold * comboCount - balance;
                tips = TheLApp.context.getResources().getString(R.string.balance_insufficient_2, comboCount, giftName, difference_value);
            } else {
                long difference_value = giftGold - balance;
                tips = TheLApp.context.getResources().getString(R.string.balance_insufficient_1, giftName, difference_value);
            }
            GrowingIOUtil.payTrack(GrowingIoConstant.LIVE_PAGE_POPWIN);
            GrowingIOUtil.payEvar(GrowingIoConstant.G_LivePagePopWin);
            FireBaseUtils.uploadGoogle(TheLConstants.FireBaseConstant.VIEW_ITEM_PURCHASE_DETAILS, getActivity());
            RechargeDialogFragment rechargeDialogFragment = new RechargeDialogFragment();
            rechargeDialogFragment.setTips(tips);
            rechargeDialogFragment.setFromAction(fromAction, pageId);
            rechargeDialogFragment.show(getActivity().getSupportFragmentManager(), RechargeDialogFragment.class.getName());
            rechargeDialogFragment.setDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    L.d("RechargeDialogFragment", "printzoilala");

                }
            });

        }
    }

    /**
     * 显示软妹豆贡献列表
     */
    private void showRankingList() {
        L.i(TAG, "showRankingList");
        if (liveRoomBean == null || liveRoomBean.user == null || getActivity() == null) {
            return;
        }
        final Bundle bundle = new Bundle();
        bundle.putString(TheLConstants.BUNDLE_KEY_USER_ID, liveRoomBean.user.id + "");
        live_gift_ranking_dialog_view = LiveGiftRankDialogView.getInstance(bundle);
        live_gift_ranking_dialog_view.show(getActivity().getSupportFragmentManager(), getClass().getSimpleName());
    }

    /**
     * 分享
     */
    private void share() {
        if (liveRoomBean != null && liveRoomBean.share != null && getActivity() != null) {
            MobclickAgent.onEvent(getActivity(), "broadcaster_click_share_button");
            final String material = LiveRoomBean.TYPE_VOICE == liveRoomBean.audioType ? GrowingIoConstant.MATERIAL_LIVE_VOICE : GrowingIoConstant.MATERIAL_LIVE_VIDEO;
            final GIOShareTrackBean trackBean = new GIOShareTrackBean(GrowingIoConstant.LIVE_SHARE, "", GrowingIoConstant.ENTRY_LIVE_SHARE, material);
            dialogUtils.showShareDialog(getActivity(), getString(R.string.share_live), liveRoomBean.share.title, liveRoomBean.share.text, liveRoomBean.share.title, liveRoomBean.share.link, liveRoomBean.user.avatar, false, true, callbackManager, new FacebookCallback() {
                @Override
                public void onSuccess(Object o) {
                    MobclickAgent.onEvent(getActivity(), "share_succeeded");
                    observer.shareToLive();
                }

                @Override
                public void onCancel() {

                }

                @Override
                public void onError(FacebookException error) {

                }
            }, true, new LiveShowContract.LiveShareListener() {
                @Override
                public void shareSuccess() {
                    observer.shareToLive();
                }
            }, trackBean);
        }
    }

    /**
     * 显示软键盘
     */
    private void showKeyboard() {
        if (null != getActivity()) {
            ViewUtils.showSoftInput(mActivity, edit_input);
        }
    }

    /**
     * 发送名片
     */
    private void sendCard() {
        if (liveRoomBean == null) {
            return;
        }
        RecommendedModel recommendedModel = new RecommendedModel();
        recommendedModel.recommendedType = "liveUser";
        if (liveRoomBean.audioType == 1) {
            recommendedModel.msgType = "moment_voicelive";
        }
        recommendedModel.imageUrl = liveRoomBean.imageUrl;
        recommendedModel.liveId = liveRoomBean.id;
        recommendedModel.avatar = liveRoomBean.user.avatar;
        recommendedModel.userId = (int) liveRoomBean.user.id;
        recommendedModel.isLandscape = liveRoomBean.isLandscape == 1;
        recommendedModel.nickname = liveRoomBean.user.nickName;
        recommendedModel.momentsId = TextUtils.isEmpty(liveRoomBean.momentId) ? 0 : Long.parseLong(liveRoomBean.momentId);
        recommendedModel.momentsText = liveRoomBean.share.text;
        recommendedModel.title = liveRoomBean.share.title;
        recommendedModel.momentsType = LiveRoomBean.TYPE_VOICE == liveRoomBean.audioType ? MomentTypeConstants.MOMENT_TYPE_VOICE_LIVE : MomentTypeConstants.MOMENT_TYPE_LIVE;
        FlutterRouterConfig.Companion.nativeShowRecommendDialog(GsonUtils.createJsonString(recommendedModel));
    }

    /**
     * 显示关闭直播对话框
     */
    private void closeLiveShow() {
        if (shouldClose()) {
            //            mActivity.finish();
            PushUtils.finish(mActivity);
        }
    }

    /**
     * 关闭直播对话框
     */
    private void showLiveClosedDialog() {

        if (mGiftPopupWindow != null && mGiftPopupWindow.isShowing()) {
            mGiftPopupWindow.dismiss();
        }

        live_show_close_view.initView(liveRoomBean, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setAnimating(false);
                mActivity.finish();
            }
        }, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (liveSwitchListener != null) {
                    liveSwitchListener.switchNext();
                }
            }
        }).show();
        rel_up.clearAnimation();
        rel_up.setVisibility(View.VISIBLE);
        rel_up.setTranslationX(0f);
        setAnimating(true);//有这位移个对话框的时候，禁止左右
        autoSwitchNext();
        //自动关闭相遇结果页
        getActivity().sendBroadcast(new Intent(BROADCAST_LIVE_CLOSE));
    }

    private void autoSwitchNext() {

        if (observer != null && autoSwitchNextRunnable != null) {
            observer.removeCallbacks(autoSwitchNextRunnable);
            autoSwitchNextRunnable = null;
        }
        if (destroyed || !liveClosed || observer == null) {
            return;
        }
        autoSwitchNextRunnable = new Runnable() {
            @Override
            public void run() {
                if (destroyed || !liveClosed) {
                    return;
                }
                if (liveSwitchListener != null) {

                    ((LiveShowViewGroup) (getActivity().findViewById(R.id.live_show_view_group))).setScrollable(LiveShowViewGroup.Scroll_Limit.ALL);

                    liveSwitchListener.switchNext();
                }
            }
        };
        observer.postDelayed(autoSwitchNextRunnable, AUTO_SWITCH_NEXT_TIME);

    }

    /**
     * 改变输入框显示状态
     *
     * @param show
     */
    private void switchEditInputVisiable(boolean show) {
        if (show) {
            send_edit_msg.setVisibility(View.VISIBLE);
            lin_input.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.transparent));
        } else {
            send_edit_msg.setVisibility(View.GONE);
            // 添加send_edit_msg消失动画
            //AnimUtils.disappearSendEdit(send_edit_msg);
            lin_input.setBackgroundResource(R.drawable.live_chat_black_bg);
        }
    }


    /****************************一下为礼物区域*******************************************/

    /**
     * 解析礼物
     *
     * @param payload
     */
    @Override
    public void parseGiftMsg(String payload, String code) {

        L.d(TAG, " parseGiftMsg parseGiftMsg : " + payload);

        final GiftSendMsgBean msgBean = GsonUtils.getObject(payload, GiftSendMsgBean.class);
        if (msgBean == null) {
            return;
        }

        if (code != null && code.equals(LiveRoomMsgBean.CODE_SPECIAL_GIFT)) {
            showGiftMsgData(msgBean);
            return;
        }

        L.d(TAG, " parseGiftMsg msgBean.ownerGem : " + msgBean.ownerGem);

        if (!msgBean.ownerGem.equals("-1")) {//如果服务器有返回，就更新主播软妹币
            txt_soft_money.setText(getString(R.string.money_soft, msgBean.ownerGem));
        }

        L.d(TAG, " parseGiftMsg softEnjoyBean : " + softEnjoyBean);

        if (softEnjoyBean == null) {//如果没有礼物列表数据，因为有些打赏的显示数据无法获取，所以要重新请求礼物列表数据，并且把这个消息放到消息队列里去，等请求完再显示
            putGiftMsgIntoWaitList(msgBean);//把礼物消息放到等待消息队里中
            getGiftListNetData();
            return;
        }

        L.d(TAG, " parseGiftMsg getGiftMsgDataFromMemory(msgBean) : " + getGiftMsgDataFromMemory(msgBean));

        if (getGiftMsgDataFromMemory(msgBean)) {//能够从本地内存获取数据
            showGiftMsgData(msgBean);//显示消息
        } else {
            putGiftMsgIntoWaitList(msgBean);//不能，则连网请求数据
            getSingleGift(msgBean.id);
        }
    }

    private void getSingleGift(int id) {
        if (isGettingGiftData) {//如果正在获取数据，则不重复获取
            return;
        }
        isGettingGiftData = true;
        presenter.getSingleGiftDetail(id);
        observer.postDelayed(new Runnable() {
            @Override
            public void run() {
                isGettingGiftData = false;//万一请求失败，之类的，8秒后自动设置为false;
            }
        }, 8000);
    }

    @Override
    public void addOfflineGift(SoftGiftBean softGiftBean) {
        offlineGiftList.remove(softGiftBean);
        offlineGiftList.add(softGiftBean);
        pushGiftMsgWaitList();
    }


    /**
     * 从本地内存中获取GiftSendMsgBean（要显示的收到的打赏的礼物的消息）的其他数据
     *
     * @param msgBean 收到的打赏的礼物数据
     * @return true；获取到数据，false，没获取到数据
     */
    private boolean getGiftMsgDataFromMemory(GiftSendMsgBean msgBean) {
        if (softEnjoyBean == null || softEnjoyBean.list.isEmpty()) {
            getGiftListNetData();
            return false;
        }
        final int size = softEnjoyBean.list.size();

        L.d("parseGiftMsg", " getGiftMsgDataFromMemory size : " + size);

        for (int i = 0; i < size; i++) {
            SoftGiftBean bean = softEnjoyBean.list.get(i);//从本地内存中获取余下数据

            L.d("parseGiftMsg", " getGiftMsgDataFromMemory bean.id : " + bean.id);

            L.d("parseGiftMsg", " getGiftMsgDataFromMemory bean.videoUrl : " + bean.videoUrl);

            if (msgBean.id == bean.id) {
                msgBean.img = bean.img;
                msgBean.icon = bean.icon;
                msgBean.action = bean.action;
                msgBean.giftName = bean.title;
                msgBean.mark = bean.resource;
                msgBean.gold = bean.gold;
                msgBean.playTime = bean.playTime;
                msgBean.videoUrl = bean.videoUrl;
                return true;//说明获取到了，返回true
            }
        }
        return isOffLineGift(msgBean);
    }

    private boolean isOffLineGift(GiftSendMsgBean msgBean) {

        final int size = offlineGiftList.size();
        for (int i = 0; i < size; i++) {
            SoftGiftBean bean = offlineGiftList.get(i);//从本地内存中获取余下数据
            if (msgBean.id == bean.id) {
                msgBean.img = bean.img;
                msgBean.icon = bean.icon;
                msgBean.action = bean.action;
                msgBean.giftName = bean.title;
                msgBean.mark = bean.resource;
                msgBean.gold = bean.gold;
                msgBean.playTime = bean.playTime;
                msgBean.videoUrl = bean.videoUrl;
                return true;//说明获取到了，返回true
            }
        }
        return false;
    }


    /**
     * 由于某种原因而暂时无法显示的消息
     * 把礼物消息放到等待消息队列中去
     * 比如最新小礼物，在开启直播的时候获取到的小礼物消息中没有这个礼物
     *
     * @param msgBean 放到等到队里中的消息
     */
    private void putGiftMsgIntoWaitList(GiftSendMsgBean msgBean) {
        giftMsgWaitList.add(msgBean);
    }

    /**
     * 处理等待的消息队列
     */
    private void pushGiftMsgWaitList() {
        for (int i = 0; i < giftMsgWaitList.size(); i++) {
            GiftSendMsgBean msgBean = giftMsgWaitList.get(i);
            if (getGiftMsgDataFromMemory(msgBean)) {//如果从新的内存中获取到数据
                showGiftMsgData(msgBean);//显示处理数据
                giftMsgWaitList.remove(i);//从等待队列的数据中移除
            } else {//依旧不能从新的内存中获取到数据（可能数据依旧不是最新）
                getSingleGift(msgBean.id);//连网请求数据
            }
        }
    }

    /**
     * 处理礼物赠送广播消息
     *
     * @param msgBean
     */
    private void showGiftMsgData(GiftSendMsgBean msgBean) {

        L.d(TAG, " parseGiftMsg msgBean.mark : " + msgBean.mark);

        if (!TextUtils.isEmpty(msgBean.mark) || (msgBean.videoUrl != null && msgBean.videoUrl.endsWith(".mp4"))) {//如果大礼物标识不为空，说明是大动画
            putGiftMsgIntoBitAnimList(msgBean);//把礼物放到大动画播放列表中
        } else {
            giftMsgCtrl.receiveGiftMsg(msgBean);//处理接受到的礼物消息，显示
        }
    }

    /**
     * 把礼物放到大动画播放列表
     *
     * @param msgBean 要播放大动画的礼物
     */
    private void putGiftMsgIntoBitAnimList(GiftSendMsgBean msgBean) {
        final String mark = msgBean.mark;

        //如果标志在已有大动画里面
        if ((!TextUtils.isEmpty(mark) && (TheLConstants.BIG_ANIM_BALLOON.equals(mark)
                || TheLConstants.BIG_ANIM_CROWN.equals(mark)
                || TheLConstants.BIG_ANIM_STAR_SHOWER.equals(mark)
                || TheLConstants.BIG_ANIM_COIN_DROP.equals(mark)
                || TheLConstants.BIG_ANIM_HUG_HUG.equals(mark)
                || TheLConstants.BIG_ANIM_BUBBLE.equals(mark)
                || TheLConstants.BIG_ANIM_SNOWMAN.equals(mark)
                || TheLConstants.BIG_ANIM_KISS.equals(mark)
                || TheLConstants.BIG_ANIM_FERRIS_WHEEL.equals(mark)
                || TheLConstants.BIG_ANIM_PUMPKIN.equals(mark)
                || TheLConstants.BIG_ANIM_RING.equals(mark)
                || TheLConstants.BIG_ANIM_CHRISTMAS.equals(mark)
                || TheLConstants.BIG_ANIM_BOMB.equals(mark)
                || TheLConstants.BIG_ANIM_RICEBALL.equals(mark)
                || TheLConstants.BIG_ANIM_FIREWORK.equals(mark)
                || TheLConstants.BIG_ANIM_FIRECRACKER.equals(mark)
                || mark.endsWith(".json")))
                || (msgBean.videoUrl != null && msgBean.videoUrl.endsWith(".mp4"))) {
            bigAnimGiftList.add(msgBean);
            tryToPlayBigAnim();//试着播放大动画
        } else { //当小礼物显示
            giftMsgCtrl.receiveGiftMsg(msgBean);//处理接受到的礼物消息，显示
            //   addSmallGiftMsg(msgBean);
        }
    }

    /**
     * 试着播放大动画
     */
    private void tryToPlayBigAnim() {
        if (isShowBigAnim || bigAnimGiftList.size() <= 0) {//如果正在播放大动画或者大动画队列为0
            return;
        } else {
            showBigGiftAnim(bigAnimGiftList.get(0));//播放队列的第一个
        }
    }

    /**
     * 显示大动画
     *
     * @param giftSendMsgBean
     */
    public void showBigGiftAnim(final GiftSendMsgBean giftSendMsgBean) {
        int playTime = giftSendMsgBean.playTime;
        isShowBigAnim = true;
        /**
         * 上报礼物给后台大礼物播放
         * **/
        if (liveRoomBean != null) {
            observer.postGiftPlayStatus(giftSendMsgBean.userId, liveRoomBean.user.id + "", giftSendMsgBean.id);

        }

        if (liveBigGiftAnimLayout.playLocalAnim(giftSendMsgBean.mark)) {//如果是本地大礼物，播放本地大礼物

        } else if ((!TextUtils.isEmpty(giftSendMsgBean.mark) && giftSendMsgBean.mark.endsWith(".json")) || (giftSendMsgBean.videoUrl != null && giftSendMsgBean.videoUrl.endsWith(".mp4"))) {//ae动画
            //LiveBigGiftAnimLayout.
            /***4.5.0新增，如果是视频礼物，播放视频***/

            L.d(TAG, " giftSendMsgBean.videoUrl : " + giftSendMsgBean.videoUrl);

            if (!TextUtils.isEmpty(giftSendMsgBean.videoUrl) && !GiftSendMsgBean.VIDEO_URL_NULL.equals(giftSendMsgBean.videoUrl)) {
                liveBigGiftAnimLayout.playVideoAnim(giftSendMsgBean.videoUrl);
            } else {
                liveBigGiftAnimLayout.playAEanimator(giftSendMsgBean.mark, playTime);
            }

        } else {
            giftMsgCtrl.receiveGiftMsg(giftSendMsgBean);//如果大礼物中不存在（版本没更新），当小礼物显示
        }
        bigAnimGiftList.remove(0);//播放后，去掉第一个

        showBigGiftMsg(giftSendMsgBean);
        /***如果是大礼物视频礼物，则走视频结束回调,在setlistener方法里面***/
        if ((!TextUtils.isEmpty(giftSendMsgBean.mark) && giftSendMsgBean.mark.endsWith(".json")) || (!TextUtils.isEmpty(giftSendMsgBean.videoUrl) && !GiftSendMsgBean.VIDEO_URL_NULL.equals(giftSendMsgBean.videoUrl))) {

        } else {
            observer.postDelayed(new Runnable() {
                @Override
                public void run() {
                    playNextBigAnim();
                }
            }, LiveBigGiftAnimLayout.duration + 1000);//大动画播放6秒
        }
    }

    /**
     * 播放下一个大动画
     */
    private void playNextBigAnim() {
        isShowBigAnim = false;
        hideBigGiftMsg();
        tryToPlayBigAnim();
    }

    /**
     * 显示大礼物的消息信息
     *
     * @param giftSendMsgBean
     */
    private void showBigGiftMsg(final GiftSendMsgBean giftSendMsgBean) {
        if (giftSendMsgBean.avatar == null || giftSendMsgBean.nickName == null) {
            return;
        }
        View.OnClickListener onClickListener = null;
        if (!Utils.isMyself(giftSendMsgBean.userId)) {
            onClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getLiveUserCard(giftSendMsgBean.userId);
                    reportContent = "";
                }
            };
        }
        live_vip_enter_view.receiveBigGiftMsg(giftSendMsgBean, onClickListener);
    }

    private void hideBigGiftMsg() {
        live_vip_enter_view.hideBigGiftText();
    }

    /**
     * 获取赠送礼物连网数据
     */
    private void getGiftListNetData() {
        if (isGettingGiftData) {//如果正在获取数据，则不重复获取
            return;
        }
        isGettingGiftData = true;
        presenter.getLiveGiftList();
        observer.postDelayed(new Runnable() {
            @Override
            public void run() {
                isGettingGiftData = false;//万一请求失败，之类的，8秒后自动设置为false;
            }
        }, 8000);
    }

    /*********************************以上为礼物区域，一下为消息方法区*********************************************/
    @Override
    public void updateAudienceCount(@NotNull ResponseGemBean responseGemBean) {


        if (liveRoomBean != null && responseGemBean != null) {
            txt_watch_live.setText(responseGemBean.getUserCount() + "");
            if (responseGemBean.getRank() == 0) {
                txt_audience.setText("");
            } else if (responseGemBean.getRank() == 1) {
                txt_audience.setText(getString(R.string.rank1, responseGemBean.getRank()) + " | ");
            } else if (responseGemBean.getRank() == 2) {
                txt_audience.setText(getString(R.string.rank2, responseGemBean.getRank()) + " | ");
            } else if (responseGemBean.getRank() == 3) {
                txt_audience.setText(getString(R.string.rank3, responseGemBean.getRank()) + " | ");
            } else {
                txt_audience.setText(getString(R.string.rank, responseGemBean.getRank()) + " | ");
            }
            txt_soft_money.setText(getString(R.string.money_soft, responseGemBean.getGem()));
        }
    }

    /**
     * 刷新消息
     */
    @Override
    public synchronized void refreshMsg() {

        L.d(TAG, " refreshMsg isRefreshingMsgList : " + isRefreshingMsgList);

        if (isRefreshingMsgList) {
            return;
        }

        int newMsgSize = msgCacheList.size();

        L.d(TAG, " refreshMsg newMsgSize : " + newMsgSize);

        if (newMsgSize == 0) {
            return;
        }

        int startPosition = msgList.size();

        L.d(TAG, " refreshMsg startPosition : " + startPosition);

        msgList.addAll(msgCacheList);

        isRefreshingMsgList = true;

        adapter.notifyItemRangeInserted(startPosition, startPosition + newMsgSize);

        if (isBottom) {
            observer.sendEmptyMessage(UI_EVENT_SCROLL_TO_BOTTOM);
        } else {// 两秒后将isBottom设为true，避免出现有时候isBottom一直为false
            observer.sendEmptyMessageDelayed(UI_EVENT_RESET_IS_BOTTOM, 2000);
        }
        isRefreshingMsgList = false;

        msgCacheList.clear();
    }

    @Override
    public void smoothScrollToBottom() {
        final int count = adapter.getItemCount();
        if (count > 0) {
            listview_chat.smoothScrollToPosition(count - 1);
        }
    }

    /**
     * 曾送礼物后更新余额消息
     *
     * @param result
     */

    /**
     * 清空输入框
     */
    @Override
    public void clearInput() {
        edit_input.setText("");
    }

    @Override
    public void liveClosed() {
        showPkFinishView();
        liveClosed = true;
        if (!destroyed && dialogUtils != null && !dialogUtils.isDialogShowing()) {
            if ((mActivity.getRequestedOrientation() != ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)) {
                mActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                observer.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        showLiveClosedDialog();
                    }
                }, 500);
            } else {
                showLiveClosedDialog();
            }
        }
    }

    @Override
    public void isBottom(boolean isBottom) {
        this.isBottom = isBottom;
    }

    @Override
    public void beenBlocked() {
        DialogUtil.showToastShort(mActivity, getString(R.string.live_you_have_been_baned));
    }

    @Override
    public void hidePreviewImage() {
        //todo
    }

    @Override
    public void showCaching() {
        caching.setVisibility(View.VISIBLE);

    }

    @Override
    public void hideCaching() {
        caching.setVisibility(View.GONE);

    }

    //todo
    @Override
    public void updateBroadcasterNetStatus(int broadcasterStatus) {
        /*switch (broadcasterStatus) {
            case 0:
                txt_uploader_status.setVisibility(View.GONE);
                break;
            case 1:
                txt_uploader_status.setVisibility(View.VISIBLE);
                txt_uploader_status.setText(R.string.broadcater_network_slow);
                break;
            case 2:
                txt_uploader_status.setVisibility(View.VISIBLE);
                txt_uploader_status.setText(R.string.broadcater_network_unavailable);
                break;
        }*/
    }

    @Override
    public void updateMyNetStatusBad() {
        if (txt_my_status.getVisibility() == View.GONE) {
            txt_my_status.setVisibility(View.VISIBLE);
            observer.sendEmptyMessageDelayed(UI_EVENT_UPDATE_MY_NET_STATUS_GOOD, 3000);
        }
    }

    @Override
    public void updateMyNetStatusGood() {
        txt_my_status.setVisibility(View.GONE);
    }

    @Override
    public void speakTooFast() {
        DialogUtil.showToastShort(mActivity, getString(R.string.info_speak_to_fast));
    }

    @Override
    public void updateBalance(String result) {
        try {

            SendGiftSuccessBean senGiftSuccessBean = GsonUtils.getObject(result, SendGiftSuccessBean.class);

            int balance = senGiftSuccessBean.goldBalance;
            if (balance < 0) {
                return;
            }

            if (senGiftSuccessBean.arriveToTopfans > 0) {

                presenter.getAnchorTopFans(liveRoomBean.user.id + "", senGiftSuccessBean.arriveToTopfans);

            }

            softEnjoyBean.gold = balance;
            if (mGiftPopupWindow != null && mGiftPopupWindow.isShowing()) {
                mGiftPopupWindow.updateBalance(balance);
            }

            if (danmu_selected_layout != null) {
                danmu_selected_layout.setGold(balance);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 显示弹幕
     *
     * @param danMsg
     */
    @Override
    public void showDanmu(String danMsg) {

        L.d(TAG, " showDanmu danMsg : " + danMsg);

        final DanmuBean danmuBean = GsonUtils.getObject(danMsg, DanmuBean.class);
        danmu_layout_view.receiveMsg(danmuBean);
    }

    /**
     * 解锁输入框
     *
     * @param clearInput 是否清空输入框
     */
    @Override
    public void openInput(boolean clearInput) {
    }

    /**
     * 发送弹幕返回结果
     *
     * @param danmuResult 发送弹幕成功返回的payload
     */
    @Override
    public void setDanmuResult(String danmuResult) {

        try {

            DanmuResultBean danmuResultBean = GsonUtils.getObject(danmuResult, DanmuResultBean.class);

            if (danmuResultBean.success) {
                openInput(true);
                if (danmuResultBean.goldBalance != -1) {
                    softEnjoyBean.gold = danmuResultBean.goldBalance;
                    if (mGiftPopupWindow != null && mGiftPopupWindow.isShowing()) {

                        mGiftPopupWindow.updateBalance(danmuResultBean.goldBalance);
                    }
                    if (danmu_selected_layout != null) {

                        danmu_selected_layout.setGold(danmuResultBean.goldBalance);
                    }
                }
            } else {
                openInput(false);
            }
        } catch (Exception e) {
            openInput(false);
            e.printStackTrace();
        }
    }

    @Override
    public void joinVipUser(LiveRoomMsgBean liveRoomMsgBean) {
        if (null != live_vip_enter_view) {
            live_vip_enter_view.joinNewUser(liveRoomMsgBean);
        }
    }

    @Override
    public void reconnecting() {
        //        progress_bar.setVisibility(View.GONE);
    }

    @Override
    public void showDialogChannelId() {
        ToastUtils.showCautionToastShort(mActivity, getString(R.string.live_notfound));

    }

    @Override
    public void showDialogRequest() {
        ToastUtils.showCautionToastShort(mActivity, getString(R.string.something_is_wrong));

    }

    @Override
    public void showAlertUserId() {
        if (getActivity() == null) {
            return;
        }
        DialogUtil.showAlert(getActivity(), "", getString(R.string.create_rela_id), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getActivity().finish();
            }
        });
    }

    @Override
    public void showAlertUserName() {
        if (getActivity() == null) {
            return;
        }
        DialogUtil.showAlert(getActivity(), "", getString(R.string.create_username), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getActivity().finish();
            }
        });
    }

    List<LiveRoomMsgBean> msgCacheList = new ArrayList<>();

    @Override
    public void addLiveRoomMsg(final LiveRoomMsgBean liveRoomMsgBean) {

        L.d(TAG, " refreshMsg addLiveRoomMsg liveRoomMsgBean : " + liveRoomMsgBean.toString());

        try {
            msgCacheList.add(liveRoomMsgBean);
//            msgList.add(liveRoomMsgBean);
            if (LiveRoomMsgBean.TYPE_BANED.equals(liveRoomMsgBean.type)) {
                isBan = true;
            }

            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (LiveRoomMsgBean.TYPE_GIFTCOMBO.equals(liveRoomMsgBean.type) && LiveRoomBean.TYPE_VOICE == liveRoomBean.audioType && LiveRoomBean.MULTI_LIVE == liveRoomBean.isMulti) {
                            if (liveRoomMsgBean.toUserId != null && !liveRoomMsgBean.toUserId.equals(String.valueOf(liveRoomBean.user.id))) {
                                if (multi_grid_view != null) {
                                    SoftGiftBean softGiftBean = getGiftInfoByGiftId(liveRoomMsgBean.id);
                                    if (getActivity() != null && !getActivity().isDestroyed() && softGiftBean != null) {
                                        multi_grid_view.setGift(softGiftBean.icon, liveRoomMsgBean.combo, liveRoomMsgBean.toUserId);
                                    }
                                }
                            }
                        }
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showProgressbar(boolean show) {

        if (getActivity() != null && show) {

            String content = TheLApp.getContext().getResources().getString(R.string.network_slow);

            ToastUtils.showToastShort(getContext(), content);
        }
    }

    @Override
    public void linkMicBusy(LinkMicResponseBean linkMicResponseBean) {
        if (link_mic_layer != null && listview_chat != null) {

            LiveGIOPush.getInstance().setWatchLinkMicCount();

            link_mic_layer.show(linkMicResponseBean.nickName, linkMicResponseBean.userId, !linkMicResponseBean.isAudienceLinkMic);

        }
    }

    @Override
    public void linkMicStop() {

        L.d("LinkMicInfoView", " ---------linkMicStop-------- : ");

        if (link_mic_layer != null && listview_chat != null) {

            audienceLinkMicStatus = AUDIENCE_LINK_MIC_NONE;

            isAudienceLinkMic(false);

            link_mic_layer.hide();
        }

    }

    @Override
    public void linkMicStart(LinkMicResponseBean linkMicResponseBean) {

        LiveGIOPush.getInstance().setWatchLinkMicCount();

        if (link_mic_layer != null && listview_chat != null) {

            link_mic_layer.show(linkMicResponseBean.nickName, linkMicResponseBean.userId, !linkMicResponseBean.isAudienceLinkMic);

        }

        if (audienceLinkMicStatus == AUDIENCE_LINK_MIC_SUCCESS) {

            if (liveWatchMicSortDialog != null) {
                liveWatchMicSortDialog.dismiss();
            }

            if (connectMicDialog != null && connectMicDialog.isAdded()) return;
            final ConnectMicDialog connectMicDialog = new ConnectMicDialog();

            Bundle bundle = new Bundle();
            bundle.putInt(TheLConstants.BUNDLE_KEY_ACTION_TYPE, ConnectMicDialog.TYPE_AUDIENCE_LINK_MIC_REQUEST);

            bundle.putString(BUNDLE_KEY_NICKNAME, liveRoomBean.user.nickName);
            bundle.putString(BUNDLE_KEY_USER_AVATAR, liveRoomBean.user.avatar);

            connectMicDialog.setArguments(bundle);
            connectMicDialog.show(getActivity().getSupportFragmentManager(), ConnectMicDialog.class.getName());

        }
    }

    @Override
    public void showTopGiftView(TopGiftBean topGiftBean) {
        if (topGiftBean == null || softEnjoyBean == null) {
            return;
        }
        final SoftGiftBean softGiftBean = LiveUtils.getGiftBeanById(topGiftBean.id, softEnjoyBean);
        if (softGiftBean == null) {
            return;
        }
        topGiftBean.softGiftBean = softGiftBean;
        live_top_gift_layout.receiveGift(topGiftBean);
    }

    @Override
    public void showLevelBelowSix() {

        audienceLinkMicStatus = AUDIENCE_LINK_MIC_NONE;

        isAudienceLinkMic(false);

        String message = TheLApp.context.getResources().getString(R.string.request_level_six);

        if (getActivity() == null)
            return;
        DialogUtil.showConfirmDialog(getActivity(), "", message, getString(R.string.level_tips), getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent intent = new Intent(TheLApp.getContext(), MyLevelActivity.class);
                getActivity().startActivity(intent);
            }
        }, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void freeBarrage() {

        if (root_vg != null) {
            if (!root_vg.isDanmuShowing() && !root_vg.isSoftExtend()) {
                showKeyboard();
            }

        }

        if (danmu_selected_layout != null && !danmu_selected_layout.isFreeBarrage()) {
            if (danmu_selected_layout != null) {
                danmu_selected_layout.showFreeBarrage();
                if (softEnjoyBean != null) {
                    danmu_selected_layout.setGold(softEnjoyBean.gold);
                }
            }
        }

        if (bubble_guide_layout != null) {
            bubble_guide_layout.show();
        }

    }

    @Override
    public void onFaceDetection(boolean visible) {

    }

    @Override
    public void onSeatAuth() {
        DialogUtil.showConfirmDialog(getActivity(), null, getString(R.string.onseat_auth_tips), getString(R.string.info_back), getString(R.string.onseat_auth), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent intent = new Intent(getActivity(), ZhimaCertificationActivity.class);
                intent.putExtra(TheLConstants.BUNDLE_KEY_INTENT_FROM, ZhimaCertificationActivity.ONSEAT_AUTH);
                startActivity(intent);
            }
        });
    }

    @Override
    public void showFreeGoldView(int gold) {
        if (getActivity() != null) {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FreeGoldDialogFragment freeGoldDialogFragment = (FreeGoldDialogFragment) fragmentManager.findFragmentByTag(FreeGoldDialogFragment.class.getName());
            if (freeGoldDialogFragment == null) {
                freeGoldDialogFragment = new FreeGoldDialogFragment();
            }

            if (freeGoldDialogFragment.isAdded()) {
                freeGoldDialogFragment.dismiss();
            }

            freeGoldDialogFragment.setGold(gold);

            freeGoldDialogFragment.setOnAcceptListener(new FreeGoldDialogFragment.OnAcceptListener() {
                @Override
                public void onClose() {
                    observer.rejectFreeGold();
                }

                @Override
                public void onAccept() {
                    observer.acceptFreeGold();

                    showGiftPW(null);

                }
            });

            freeGoldDialogFragment.show(getActivity().getSupportFragmentManager(), FreeGoldDialogFragment.class.getName());
        }
    }

    private ConnectMicDialog connectMicDialog;

    private LiveRoomMsgConnectMicBean liveRoomMsgConnectMicBean;

    private void showRequestLinkMicDialog(LiveRoomMsgConnectMicBean liveRoomMsgConnectMicBean) {
        final String toUserId = liveRoomMsgConnectMicBean.userId;
        final String userId = ShareFileUtils.getString(ShareFileUtils.ID, null);

        if (toUserId != null && userId != null) {

            if (!toUserId.equals(userId)) {

                if (connectMicDialog != null) {
                    connectMicDialog.dismiss();
                }

                connectMicDialog = new ConnectMicDialog();
                connectMicDialog.setOnButtonClickListener(new ConnectMicDialog.OnButtonClickListener() {
                    @Override
                    public void onClick(int type) {
                        switch (type) {
                            case TYPE_ACCEPT:
                                final RectF mVideoInfoRectF = LinkMicScreenUtils.getChildVideoRect();

                                L.d(TAG, " mVideoInfoRectF : " + mVideoInfoRectF);

                                observer.responseConnectMic("response", toUserId, "yes", mVideoInfoRectF.left, mVideoInfoRectF.top, mVideoInfoRectF.bottom, mVideoInfoRectF.right);
                                break;
                            case TYPE_REFUSE:
                                if (connectMicDialog != null) {
                                    connectMicDialog.dismiss();
                                }
                                observer.responseConnectMic("response", toUserId, "no", 0, 0, 0, 0);
                                break;
                            case TYPE_CANCEL:
                            case TYPE_I_NO_RESPONSE:
                            case TYPE_REQUEST_NO_RESPONSE:
                                if (connectMicDialog != null) {
                                    connectMicDialog.dismiss();
                                }
                                break;

                        }

                    }

                });
                Bundle bundle = new Bundle();
                bundle.putInt(TheLConstants.BUNDLE_KEY_ACTION_TYPE, ConnectMicDialog.DIALOG_TYPE_MIC_REQUEST);
                bundle.putBoolean("dailyGuard", true);
                bundle.putString(BUNDLE_KEY_NICKNAME, liveRoomMsgConnectMicBean.getNickName());
                bundle.putString(BUNDLE_KEY_USER_AVATAR, liveRoomMsgConnectMicBean.avatar);
                connectMicDialog.setArguments(bundle);
                connectMicDialog.show(getActivity().getSupportFragmentManager(), ConnectMicDialog.class.getName());
            }

        }
    }

    private void showHangupView(String nickName, String avatar) {

        if (connectMicDialog != null) {
            connectMicDialog.dismiss();
        }

        connectMicDialog = new ConnectMicDialog();
        connectMicDialog.setOnButtonClickListener(new ConnectMicDialog.OnButtonClickListener() {
            @Override
            public void onClick(int type) {

                switch (type) {
                    case TYPE_HANGUP:
                        if (connectMicDialog != null) {
                            connectMicDialog.dismiss();
                        }
                        break;
                }
            }
        });

        Bundle bundle = new Bundle();
        bundle.putInt(TheLConstants.BUNDLE_KEY_ACTION_TYPE, ConnectMicDialog.DIALOG_TYPE_HANGUP);

        if (liveRoomMsgConnectMicBean != null) {
            nickName = liveRoomMsgConnectMicBean.getNickName();
            avatar = liveRoomMsgConnectMicBean.avatar;
        }

        bundle.putString(BUNDLE_KEY_NICKNAME, nickName);
        bundle.putString(BUNDLE_KEY_USER_AVATAR, avatar);

        connectMicDialog.setArguments(bundle);
        connectMicDialog.show(getActivity().getSupportFragmentManager(), ConnectMicDialog.class.getName());

    }

    @Override
    public void showConnectMicDialog(LiveRoomMsgConnectMicBean liveRoomMsgConnectMicBean) {
        if (liveRoomMsgConnectMicBean != null) {

            L.d("ConnectMicDialog", " liveRoomMsgConnectMicBean.method : " + liveRoomMsgConnectMicBean.method);

            switch (liveRoomMsgConnectMicBean.method) {
                case "request":

                    showRequestLinkMicDialog(liveRoomMsgConnectMicBean);
                    break;
                case "response":
                    if (connectMicDialog != null) {
                        connectMicDialog.showRequestAcceptView();
                    }
                    break;
                case "start":

                    this.liveRoomMsgConnectMicBean = liveRoomMsgConnectMicBean;

                    if (connectMicDialog != null) {
                        connectMicDialog.showRequestAcceptView();
                    } else {
                        final ConnectMicDialog connectMicDialog = new ConnectMicDialog();

                        Bundle bundle = new Bundle();
                        bundle.putInt(TheLConstants.BUNDLE_KEY_ACTION_TYPE, ConnectMicDialog.TYPE_AUDIENCE_LINK_MIC_RESPONSE);

                        bundle.putString(BUNDLE_KEY_NICKNAME, liveRoomMsgConnectMicBean.getNickName());
                        bundle.putString(BUNDLE_KEY_USER_AVATAR, liveRoomMsgConnectMicBean.avatar);

                        connectMicDialog.setArguments(bundle);
                        connectMicDialog.show(getActivity().getSupportFragmentManager(), ConnectMicDialog.class.getName());


                    }

                    if (link_mic_layer != null) {
                        link_mic_layer.show(liveRoomMsgConnectMicBean.getNickName(), liveRoomMsgConnectMicBean.userId, false);
                    }

                    break;
                case "cancel":
                    if (liveRoomMsgConnectMicBean.toUserId != null && liveRoomMsgConnectMicBean.fromUserId != null) {
                        if (connectMicDialog != null) {
                            connectMicDialog.cancelThisLinkMicByOwn();
                        }
                    } else {
                        if (connectMicDialog != null) {
                            connectMicDialog.showRequestCancelView();
                        }
                    }

                    break;
                case "timeout":

                    String toUserId = liveRoomMsgConnectMicBean.toUserId;

                    String fromUserId = liveRoomMsgConnectMicBean.fromUserId;

                    if (!TextUtils.isEmpty(toUserId) && !toUserId.equals(UserUtils.getMyUserId())) {
                        if (connectMicDialog != null) {
                            connectMicDialog.showRequestTimeOutView();
                        }
                    } else {
                        if (connectMicDialog != null) {
                            connectMicDialog.showITimeOutView();
                        }
                    }

                    break;

                case "hangup":
                    if (connectMicDialog != null) {
                        showHangupView(liveRoomMsgConnectMicBean.getNickName(), liveRoomMsgConnectMicBean.avatar);
                    }

                    break;
                case "stop":

                    break;

            }
        }
    }

    @Override
    public void linkMicAccept() {
        if (connectMicDialog != null) {
            connectMicDialog.showIAcceptView();
        }
    }

    @Override
    public void lingMicCancel() {
        if (connectMicDialog != null) {
            connectMicDialog.showRequestCancelView();
        }
    }

    @Override
    public void linkMicHangup(LiveRoomMsgConnectMicBean liveRoomMsgConnectMicBean) {
        if (connectMicDialog != null) {
            connectMicDialog.dismiss();
        }

        connectMicDialog = new ConnectMicDialog();
        connectMicDialog.setOnButtonClickListener(new ConnectMicDialog.OnButtonClickListener() {
            @Override
            public void onClick(int type) {

                switch (type) {
                    case TYPE_HANGUP:
                        if (connectMicDialog != null) {
                            connectMicDialog.dismiss();
                        }
                        break;
                }
            }
        });

        Bundle bundle = new Bundle();
        bundle.putInt(TheLConstants.BUNDLE_KEY_ACTION_TYPE, ConnectMicDialog.DIALOG_TYPE_HANGUP);

        bundle.putBoolean("mAudience", true);
        bundle.putString(BUNDLE_KEY_NICKNAME, liveRoomMsgConnectMicBean.fromNickname);
        bundle.putString(BUNDLE_KEY_USER_AVATAR, liveRoomMsgConnectMicBean.fromAvatar);

        connectMicDialog.setArguments(bundle);
        connectMicDialog.show(getActivity().getSupportFragmentManager(), ConnectMicDialog.class.getName());
    }

    @Override
    public void refreshLiveRoom() {
        if (getActivity() != null) {
            ToastUtils.showToastShort(getActivity(), "发送礼物失败，请重试");
            String userId = liveRoomBean.user.id + "";

            LiveWatchActivity activity = (LiveWatchActivity) getActivity();

            String liveId = liveRoomBean.liveId;

            if (activity != null) {
                activity.getLiveRoomDetail(liveId, userId);
            }
        }
    }

    @Override
    public void topToday(TopTodayBean topTodayBean) {
        LiveRoomMsgBean liveRoomMsgBean = new LiveRoomMsgBean();
        liveRoomMsgBean.nickName = topTodayBean.nickname;
        liveRoomMsgBean.rank = topTodayBean.rank;
        liveRoomMsgBean.type = LiveRoomMsgBean.TYPE_TOP_TODAY;
        msgList.add(liveRoomMsgBean);
    }

    @Override
    public void audienceLinkMicSuccess(AudienceLinkMicResponseBean audienceLinkMicResponseBean) {

        audienceLinkMicStatus = AUDIENCE_LINK_MIC_SUCCESS;

        if (getActivity() != null) {
            LiveWatchActivity activity = (LiveWatchActivity) getActivity();
            activity.isAudienceLinkMic(true);
        }

        if (link_mic_layer != null && listview_chat != null) {

            link_mic_layer.show(liveRoomBean.user.nickName, String.valueOf(liveRoomBean.user.userId), false);

        }

    }

    @Override
    public void hangup() {

        L.d(TAG, "--------hangup-------");

        if (link_mic_layer != null) {
            link_mic_layer.hide();
        }

        if (getActivity() != null) {
            LiveWatchActivity activity = (LiveWatchActivity) getActivity();
            activity.isAudienceLinkMic(false);
        }

    }

    @Override
    public void hangupByOwn() {

    }

    /****************************以上为消息方法区********************************************/

    @Override
    public void showLiveUserCard(LiveUserCardBean liveUserCardBean) {
        if (getActivity() == null) {
            return;
        }
        final Bundle bundle = new Bundle();
        this.liveUserCardBeans = liveUserCardBean;
        bundle.putSerializable(LiveUserCardDialogView.BUNDLE_KEY_LIVE_USER_CARD_BEAN, liveUserCardBean);
        bundle.putBoolean(LiveUserCardDialogView.BUNDLE_KEY_CAN_SHARE, liveRoomBean.user.id == liveUserCardBean.userId);
        bundle.putString(LiveUserCardDialogView.BUNDLE_KEY_SHARE_TITLE, liveRoomBean.share.title);
        bundle.putString(LiveUserCardDialogView.BUNDLE_KEY_CONTENT, liveRoomBean.share.text);
        bundle.putString(LiveUserCardDialogView.BUNDLE_KEY_SINGLE_TITLE, liveRoomBean.share.title);
        bundle.putString(LiveUserCardDialogView.BUNDLE_KEY_IMAGE_URL, liveRoomBean.share.link);
        bundle.putBoolean(LiveUserCardDialogView.BUNDLE_KEY_IS_BROAD_CASTER, false);
        bundle.putInt(LiveUserCardDialogView.BUNDLE_KEY_LIVE_TYPE, liveRoomBean.audioType);
        live_user_card_dialog_view = LiveUserCardDialogView.getInstance(bundle);
        live_user_card_dialog_view.setReportListener(new ReportListener(liveUserCardBean.userId + ""));
        live_user_card_dialog_view.setDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                setReportContent("");
            }
        });
        live_user_card_dialog_view.show(getActivity().getSupportFragmentManager(), getClass().getSimpleName());
    }

    @Override
    public void showLiveAnchorUserCard(LiveUserCardBean liveUserCardBean) {
        if (getActivity() == null) {
            return;
        }
        if (!canGetAnchor()) {
            return;
        }
        final Bundle bundle = new Bundle();
        bundle.putSerializable(LiveUserCardDialogView.BUNDLE_KEY_LIVE_USER_CARD_BEAN, liveUserCardBean);
        bundle.putBoolean(LiveUserCardDialogView.BUNDLE_KEY_CAN_SHARE, false);
        bundle.putString(LiveUserCardDialogView.BUNDLE_KEY_SHARE_TITLE, liveRoomBean.share.title);
        bundle.putString(LiveUserCardDialogView.BUNDLE_KEY_CONTENT, liveRoomBean.share.text);
        bundle.putString(LiveUserCardDialogView.BUNDLE_KEY_SINGLE_TITLE, liveRoomBean.share.title);
        bundle.putString(LiveUserCardDialogView.BUNDLE_KEY_IMAGE_URL, liveRoomBean.share.link);
        bundle.putBoolean(LiveUserCardDialogView.BUNDLE_KEY_IS_BROAD_CASTER, false);
        bundle.putInt(LiveUserCardDialogView.BUNDLE_KEY_LIVE_TYPE, liveRoomBean.audioType);
        live_user_anchor_card_dialog_view = LiveUserAnchorCardDialogView.getInstance(bundle);
        live_user_anchor_card_dialog_view.setReportListener(new ReportListener(liveUserCardBean.userId + ""));
        live_user_anchor_card_dialog_view.setDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                setReportContent("");
            }
        });
        live_user_anchor_card_dialog_view.show(getActivity().getSupportFragmentManager(), getClass().getSimpleName());
    }


    @Override
    public void showLiveGiftList(SoftEnjoyBean softEnjoyBean) {
        if (getActivity() == null || softEnjoyBean == null) {
            return;
        }
        this.softEnjoyBean.list = softEnjoyBean.list;

        this.softEnjoyBean.arlist = softEnjoyBean.arlist;

        if (softEnjoyBean.list.size() > 0) {
            for (int i = 0; i < softEnjoyBean.list.size(); i++) {
                if (softEnjoyBean.list.get(i).canFasterGift == 1) {
                    softFastGiftBean = new SoftGiftBean();
                    softFastGiftBean = softEnjoyBean.list.get(i);
                    ll_fast_gift.setVisibility(VISIBLE);
                    SimpleDraweeViewUtils.setImageUrl(img_fast_gift, softFastGiftBean.icon, 45, 45);
                    L.d("GuideView", "showLiveGiftList i:" + i);
                    if (firstChargeGuideView.isVisible()) {
                        firstChargeGuideView.reset();
                        showFirstChargeGuide();
                    }
                    break;
                }
            }
        }

        showGiftListData();//显示礼物打赏界面
    }

    @Override
    public void showAdView(final LiveAdInfoBean liveAdInfoBean) {
        liveAdViewManager.show(liveRoomBean, liveAdInfoBean);
//        try {
//
//            if (liveAdInfoBean != null) {
//
//                if (liveAdInfoBean.status <= 0) {
//                    layout_live_ad.setVisibility(View.GONE);
//                    layout_multi_live_ad.setVisibility(View.GONE);
//                    return;
//                }
//
//                if (LiveRoomBean.MULTI_LIVE == liveRoomBean.isMulti) {
//                    layout_multi_live_ad.setVisibility(View.VISIBLE);
//                    layout_live_ad.setVisibility(View.GONE);
//
//                } else {
//                    layout_multi_live_ad.setVisibility(View.GONE);
//                    layout_live_ad.setVisibility(View.VISIBLE);
//                    if (liveAdInfoBean.offset != null && liveAdInfoBean.offset.length() > 0) {
//                        offset_tv.setVisibility(View.VISIBLE);
//                        offset_tv.setText(liveAdInfoBean.offset);
//                    }
//
//                }
//
//                if (liveAdInfoBean.icon != null) {
//                    if (layout_live_ad.getVisibility() == View.VISIBLE) {
//
//                        Uri uri = Uri.parse(liveAdInfoBean.icon);
//                        DraweeController controller = Fresco.newDraweeControllerBuilder().setUri(uri).setAutoPlayAnimations(true).build();
//                        ad_view.setController(controller);
//
//                    } else {
//
//                        Uri uri2 = Uri.parse(liveAdInfoBean.icon);
//                        DraweeController controller2 = Fresco.newDraweeControllerBuilder().setUri(uri2).setAutoPlayAnimations(true).build();
//                        ad_multi_view.setController(controller2);
//                    }
//
//                }
//
//                if (liveAdInfoBean.rank != null && liveAdInfoBean.rank.length() > 0) {
//                    top_tv.setText("NO." + liveAdInfoBean.rank);
//                    top_multi_tv.setText("NO." + liveAdInfoBean.rank);
//                }
//
//
//                layout_live_ad.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        if (liveAdInfoBean.url != null) {
//                            Intent intent = new Intent(getContext(), WebViewActivity.class);
//                            Bundle bundle = new Bundle();
//                            bundle.putString(BundleConstants.URL, liveAdInfoBean.url);
//                            intent.putExtras(bundle);
//                            startActivity(intent);
//                        }
//
//                    }
//                });
//                layout_multi_live_ad.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        if (liveAdInfoBean.url != null) {
//                            Intent intent = new Intent(getContext(), WebViewActivity.class);
//                            Bundle bundle = new Bundle();
//                            bundle.putString(BundleConstants.URL, liveAdInfoBean.url);
//                            intent.putExtras(bundle);
//                            startActivity(intent);
//                        }
//                    }
//                });
//
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }


    /**
     * 获取到打赏礼物列表data
     * 初始化到giftListView中
     */
    private void showGiftListData() {
        pushGiftMsgWaitList();
        setButtonClickable(true);
    }

    /**
     * 一些按钮必须要在有数据以后才可以点击
     *
     * @param clickable
     */
    private void setButtonClickable(boolean clickable) {
        img_emoji.setClickable(clickable);
        img_right_gift.setClickable(clickable);
        img_switch_danmu.setClickable(clickable);
        avatar.setClickable(clickable);
        txt_nickname.setClickable(clickable);
        ll_txt_audience.setClickable(clickable);
    }

    private void refreshFollowBtn(int status) {
        if (status == 0) {
            txt_follow.setText(TheLApp.getContext().getString(R.string.userinfo_activity_follow));
            txt_follow.setBackgroundResource(R.drawable.bg_follow_round_button_blue);
            LiveUtils.appearTextFollow(txt_follow);
        } else {
            txt_follow.setText(TheLApp.getContext().getString(R.string.userinfo_activity_followed));
            txt_follow.setBackgroundResource(R.drawable.bg_followed_round_button_gray);

            LiveUtils.disappearTextFollow(txt_follow);

        }
    }

    private void showBlackMeDialog(long id) {
        if (getActivity() == null) {
            return;
        }
        if (BlackUtils.isBlockMe(id + "")) {
            DialogUtil.showAlert(getActivity(), "", getString(R.string.live_you_have_been_baned), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getActivity().finish();
                }
            });
        }
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return false;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && getActivity() != null) {
            LiveWatchActivity activity = (LiveWatchActivity) getActivity();

            if (!activity.isShowExitDialog()) {
                //观看时长超过5分钟，就弹出提示框。
                long outTime = System.currentTimeMillis();

                if (outTime - intoTime >= FITST_WATCH_TIME && liveRoomBean != null && liveRoomBean.user.isFollow == 0) {
                    CloseLiveFollowDialog closeLiveFollowDialog = new CloseLiveFollowDialog(activity);
                    closeLiveFollowDialog.setFollowAndExitListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            FollowStatusChangedImpl.followUserWithNoDialog(liveRoomBean.user.id + "", ACTION_TYPE_FOLLOW, liveRoomBean.user.nickName, liveRoomBean.user.avatar);
                            closeLiveShow();
                            GrowingIOUtil.followLiveUser(GrowingIoConstant.LIVE_EXIT_GUIDE, GrowingIoConstant.LIVE_TARGET_HOST);


                        }
                    });
                    closeLiveFollowDialog.setExitLiveListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            closeLiveShow();

                        }
                    });
                    closeLiveFollowDialog.show();
                    return false;

                } else {
                    closeLiveShow();
                    return shouldClose();

                }
            }
        }
        return true;
    }

    private boolean shouldClose() {
        return true;
    }

    @Override
    public boolean showLiveView(boolean show) {
        return show;
    }

    @Override
    public void VideoPrepared() {

    }

    /**
     * 软妹豆变化
     *
     * @param gold 当前软妹豆数量（这个数据仅做参考）
     */
    @Override
    public void onGoldChanged(long gold) {
        if (mGiftPopupWindow != null && mGiftPopupWindow.isShowing()) {
            if (softEnjoyBean != null) {
                softEnjoyBean.gold = (int) gold;
            }
            mGiftPopupWindow.updateBalance((int) gold);
        }

        if (danmu_selected_layout != null) {
            danmu_selected_layout.setGold((int) gold);
        }

    }

    /**
     * 多人连麦自己是否为嘉宾
     *
     * @return
     */
    public boolean isGuest() {
        boolean isGuest = false;
        String myUserId = UserUtils.getMyUserId();
        for (int i = 0; i < multi_grid_view.getData().size(); i++) {
            LiveMultiSeatBean seatBean = multi_grid_view.getData().get(i);
            if (seatBean.userId.equals(myUserId)) {
                isGuest = true;
                break;
            }
        }
        return isGuest;
    }

    private void setMicViewStatus() {

        L.d(TAG, " setMicViewStatus micStatus : " + micStatus);

        L.d(TAG, " setMicViewStatus isGuest() : " + isGuest());

        if (isGuest()) {//在座位上
            img_voice_or_video.setVisibility(View.VISIBLE);
            switch (micStatus) {
                case LiveRoomMsgBean.TYPE_MIC_STATUS_ON:
                    img_voice_or_video.setImageResource(R.mipmap.icn_live_openmic);
                    break;
                case LiveRoomMsgBean.TYPE_MIC_STATUS_OFF:
                case LiveRoomMsgBean.TYPE_MIC_STATUS_CLOSE:
                    img_voice_or_video.setImageResource(R.mipmap.icon_mic_close);
                    break;
            }
        } else {//不在座位上
            img_voice_or_video.setImageResource(R.mipmap.live_icon_sofa);
        }
    }

    private void requestSortMic() {

        if (micSorting) {
            if (isInWaitMicList) {
                if (observer != null) {
                    observer.requestMicList();
                    isRequestSortMic = false;
                }
            } else {
                final LiveSoundCrowdDialogView liveSoundCrowdDialogView = LiveSoundCrowdDialogView.newInstance(null);
                liveSoundCrowdDialogView.setType(LiveSoundCrowdDialogView.TYPE_SORT_MIC);
                liveSoundCrowdDialogView.setAskSortMicListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (isBan) {
                            ToastUtils.showToastShort(getActivity(), getString(R.string.live_someone_has_been_baned));
                        } else {
                            if (observer != null) {
                                observer.requestMicList();
                                isRequestSortMic = true;
                            }
                        }

                        liveSoundCrowdDialogView.dismiss();

                    }
                });
                liveSoundCrowdDialogView.show(getChildFragmentManager(), LiveSoundCrowdDialogView.class.getName());
            }
        } else {

            if (multi_grid_view.getNotSeatPosition() < 0) {
                ToastUtils.showToastShort(getContext(), TheLApp.context.getString(R.string.no_seats_available));
                return;
            }

            final int position = multi_grid_view.getNotSeatPosition();

            if (micSorting) {

                LiveMultiSeatBean liveMultiSeatBean = multi_grid_view.getItem(position);
                Bundle bundle = new Bundle();
                bundle.putSerializable(TheLConstants.BUNDLE_KEY_SEAT_BEAN, liveMultiSeatBean);
                bundle.putString(TheLConstants.BUNDLE_KEY_USER_ID, String.valueOf(liveRoomBean.user.id));

                final LiveSoundCrowdDialogView liveSoundCrowdDialogView = LiveSoundCrowdDialogView.newInstance(bundle);
                liveSoundCrowdDialogView.setType(LiveSoundCrowdDialogView.TYPE_LINK_MIC);
                liveSoundCrowdDialogView.setOnMicListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (isBan) {
                            ToastUtils.showToastShort(getActivity(), getString(R.string.live_someone_has_been_baned));
                        } else {
                            if (observer != null) {
                                observer.onSeat(position);
                                isRequestSortMic = true;

                            }
                        }

                        liveSoundCrowdDialogView.dismiss();

                    }
                });
                liveSoundCrowdDialogView.show(getChildFragmentManager(), LiveSoundCrowdDialogView.class.getName());

            } else {

                final LiveSoundCrowdDialogView liveSoundCrowdDialogView = LiveSoundCrowdDialogView.newInstance(null);
                liveSoundCrowdDialogView.setType(LiveSoundCrowdDialogView.TYPE_LINK_MIC);
                liveSoundCrowdDialogView.setOnMicListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (isBan) {
                            ToastUtils.showToastShort(getActivity(), getString(R.string.live_someone_has_been_baned));
                        } else {
                            if (observer != null) {
                                observer.onSeat(position);
                                isRequestSortMic = true;
                            }
                        }

                        liveSoundCrowdDialogView.dismiss();

                    }
                });
                liveSoundCrowdDialogView.show(getChildFragmentManager(), LiveSoundCrowdDialogView.class.getName());
            }

        }
    }

    /**
     * 排麦列表dialog
     */
    private void micSortListDialog(List<LiveMultiSeatBean.CoupleDetail> list) {
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).userId.equals(Utils.getMyUserId())) {
                    Collections.swap(list, i, 0);
                    break;
                }
            }
        }

        Bundle bundle = new Bundle();
        bundle.putSerializable("sortList", (Serializable) list);
        liveWatchMicSortDialog = LiveWatchMicSortDialog.newInstance(bundle);
        liveWatchMicSortDialog.setInLinkMicing(audienceLinkMicStatus == AUDIENCE_LINK_MIC_SUCCESS);
        liveWatchMicSortDialog.setMicSortListener(new LiveWatchMicSortDialog.MicSortListener() {
            @Override
            public void cancelMicSort() {

                audienceLinkMicStatus = AUDIENCE_LINK_MIC_NONE;

                isAudienceLinkMic(false);

                liveWatchMicSortDialog.dismiss();

                if (LiveRoomBean.MULTI_LIVE == liveRoomBean.isMulti) {
                    if (observer != null) {
                        observer.cancelSortMic();
                        isInWaitMicList = false;
                    }
                    setMicViewStatus();
                } else {
                    if (observer != null) {
                        observer.cancelLinkMicByAudience();
                    }
                }
            }
        });
        liveWatchMicSortDialog.show(getChildFragmentManager(), LiveWatchMicSortDialog.class.getName());
    }

    @Override
    public void initMultiAdapter(LiveMultiOnCreateBean liveMultiOnCreateBean) {
        multi_grid_view.refreshAllSeats(liveMultiOnCreateBean.seats);
        if (liveMultiOnCreateBean.leftTime > 0) {//正在相遇模式
            startEncounter(liveMultiOnCreateBean.leftTime);
        }
        micSorting = liveMultiOnCreateBean.isWaitMic;
        isInWaitMicList = liveMultiOnCreateBean.IsInWaitMicList;
        setMicViewStatus();
    }

    @Override
    public void refreshSeat(LiveMultiSeatBean seatBean) {
        try {
            multi_grid_view.refreshSeat(seatBean);
            if (seatBean != null) {
                String myUserId = UserUtils.getMyUserId();
                if (myUserId.equals(seatBean.userId)) {
                    micStatus = seatBean.micStatus;
                    setMicViewStatus();
                    switch (seatBean.method) {
                        case LiveRoomMsgBean.TYPE_METHOD_ONSEAT:
                            //我自己上麦
                            //成为嘉宾禁止切换直播间
                            ((LiveShowViewGroup) (getActivity().findViewById(R.id.live_show_view_group))).setScrollable(LiveShowViewGroup.Scroll_Limit.NONE);
                            if (liveWatchMicSortDialog != null && liveWatchMicSortDialog.isAdded()) {
                                liveWatchMicSortDialog.dismiss();
                            }
                            break;
                        case LiveRoomMsgBean.TYPE_METHOD_OFFSEAT:
                            //被主播下麦 弹一个提示
                            if (!seatBean.isSelf) {
                                ToastUtils.showToastShort(getActivity(), getString(R.string.exit_mic_ed));
                            }
                            //我自己下麦
                            ((LiveShowViewGroup) (getActivity().findViewById(R.id.live_show_view_group))).setScrollable(LiveShowViewGroup.Scroll_Limit.ALL);
                            break;
                        case LiveRoomMsgBean.TYPE_METHOD_MUTE:
                            if (seatBean.micStatus.equals(LiveRoomMsgBean.TYPE_MIC_STATUS_ON)) {
                            } else {
                                if (seatBean.micStatus.equals(LiveRoomMsgBean.TYPE_MIC_STATUS_CLOSE)) {
                                    ToastUtils.showToastShort(getActivity(), getString(R.string.ban_mic_ed));
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void guestGift(LiveMultiSeatBean seatBean) {

        GiftSendMsgBean giftSendMsgBean = new GiftSendMsgBean();
        giftSendMsgBean.isMultiConnectMic = true;
        giftSendMsgBean.type = GiftSendMsgBean.GIFT_TYPE_GUEST;
        giftSendMsgBean.userId = seatBean.userId;
        giftSendMsgBean.nickName = seatBean.nickname;
        giftSendMsgBean.avatar = seatBean.avatar;
        giftSendMsgBean.id = seatBean.id;
        giftSendMsgBean.combo = seatBean.combo;
        giftSendMsgBean.userLevel = seatBean.userLevel;
        giftSendMsgBean.ownerGem = seatBean.ownerGem;
        giftSendMsgBean.toAvatar = seatBean.toAvatar;
        giftSendMsgBean.toNickname = seatBean.toNickname;
        giftSendMsgBean.toUserId = seatBean.toUserId;

        if (softEnjoyBean == null) {//如果没有礼物列表数据，因为有些打赏的显示数据无法获取，所以要重新请求礼物列表数据，并且把这个消息放到消息队列里去，等请求完再显示
            putGiftMsgIntoWaitList(giftSendMsgBean);//把礼物消息放到等待消息队里中
            getGiftListNetData();
            return;
        }

        if (getGiftMsgDataFromMemory(giftSendMsgBean)) {//能够从本地内存获取数据
            showGiftMsgData(giftSendMsgBean);//显示消息
        } else {
            putGiftMsgIntoWaitList(giftSendMsgBean);//不能，则连网请求数据
            getSingleGift(giftSendMsgBean.id);
        }

        //刷新爱心
        seatBean.nickname = seatBean.toNickname;
        seatBean.avatar = seatBean.toAvatar;
        seatBean.userId = seatBean.toUserId;
        multi_grid_view.refreshSeat(seatBean);

    }

    @Override
    public void startEncounter(final int leftTime) {

        if (getActivity() != null) {
            LiveWatchActivity activity = (LiveWatchActivity) getActivity();
            activity.isGuestMeeting(isGuest());
        }

        isMeeting = true;
        ll_meetion.setVisibility(View.VISIBLE);
        playBgAnima();
        Observable.interval(0, 1, TimeUnit.SECONDS).take(leftTime)//计时次数
                .observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(new Consumer<Long>() {
            @Override
            public void accept(Long aLong) {

                String time = String.valueOf(leftTime - aLong);

                String content = String.format(TheLApp.context.getResources().getString(R.string.count_down_meetion), time);

                int index = content.indexOf(time);

                SpannableString spannableString = new SpannableString(content);

                spannableString.setSpan(new AbsoluteSizeSpan(32, true), index, content.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                meetion_count_down.setText(spannableString);

                if (leftTime - aLong == 1) {
                    //相遇过程中，与服务端断开链接时，延迟五秒自动结束相遇
                    observer.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (isMeeting) {
                                endEncounter(null);
                            }
                        }
                    }, 5000);
                }
            }
        });

        if (isGuest()) {
            Bundle bundle = new Bundle();
            bundle.putSerializable(TheLConstants.BUNDLE_KEY_LIVE_SEAT_ROOM, (Serializable) multi_grid_view.getData());
            bundle.putInt("leftTime", leftTime);
            LiveWatchMeetSelectFragment liveShowMeetSelectFragment = LiveWatchMeetSelectFragment.newInstance(bundle);
            liveShowMeetSelectFragment.setMeetSelectListener(new LiveWatchMeetSelectFragment.MeetSelectListener() {
                @Override
                public void encounterSb(int seatNum) {
                    if (observer != null) {
                        observer.encounterSb(seatNum);
                    }
                }
            });
            getChildFragmentManager().beginTransaction().add(R.id.frame_begin_meetion, liveShowMeetSelectFragment, LiveWatchMeetSelectFragment.class.getName()).commitAllowingStateLoss();
        }
    }

    @Override
    public void endEncounter(LiveMultiSeatBean seatBean) {
        try {
            if (getActivity() != null) {
                LiveWatchActivity activity = (LiveWatchActivity) getActivity();
                activity.isGuestMeeting(false);
            }
            isMeeting = false;
            ll_meetion.setVisibility(View.GONE);
            meeting_anim_bg.cancelAnimation();
            meeting_anim_bg.setVisibility(View.GONE);

            Bundle bundle = new Bundle();
            //相遇结束，关闭嘉宾选择页
            LiveWatchMeetSelectFragment liveShowMeetSelectFragment = (LiveWatchMeetSelectFragment) getChildFragmentManager().findFragmentByTag(LiveWatchMeetSelectFragment.class.getName());
            if (liveShowMeetSelectFragment != null) {
                bundle.putInt(TheLConstants.BUNDLE_KEY_ROLE, MeetResultActivity.GUEST);
                getChildFragmentManager().beginTransaction().remove(liveShowMeetSelectFragment).commitAllowingStateLoss();
            } else {
                bundle.putInt(TheLConstants.BUNDLE_KEY_ROLE, MeetResultActivity.AUDIENCE);
            }
            //相遇结束，跳转到结果页
            bundle.putSerializable(TheLConstants.BUNDLE_KEY_LIVE_SEAT_ROOM, seatBean);
            bundle.putSerializable(TheLConstants.BUNDLE_KEY_LIVEROOMBEAN, liveRoomBean);
            Intent intent = new Intent(getActivity(), MeetResultActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);

            if (liveShowMeetSelectFragment != null) {
                liveShowMeetSelectFragment.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void speakers(MultiSpeakersBean speakersBean) {
        for (int i = 0; i < speakersBean.speakers.size(); i++) {
            if ((speakersBean.speakers.get(i).uid == liveRoomBean.user.userId
                    || speakersBean.speakers.get(i).uid == liveRoomBean.user.id)
                    && speakersBean.speakers.get(i).volumn > 28) {
                if (user_is_talking.getVisibility() == View.INVISIBLE) {
                    user_is_talking.setVisibility(VISIBLE);
                    user_is_talking.playAnimation();
                }
            }
        }
        multi_grid_view.refreshSeatAnimation(speakersBean);
    }

    @Override
    public void addSortMic(LiveMultiSeatBean.CoupleDetail seatBean) {
        if (liveWatchMicSortDialog != null && liveWatchMicSortDialog.isAdded()) {
            liveWatchMicSortDialog.addUser(seatBean);
        }
    }

    @Override
    public void removeSortMic(String userId) {
        if (liveWatchMicSortDialog != null && liveWatchMicSortDialog.isAdded()) {
            liveWatchMicSortDialog.removeByUserId(userId);
        }
        if (userId.equals(UserUtils.getMyUserId())) {
            isInWaitMicList = false;
        }
    }

    @Override
    public void getMicSortList(List<LiveMultiSeatBean.CoupleDetail> list) {

        if (isRequestSortMic) {
            if (list.size() < 100) {
                if (observer != null) {
                    observer.requestSortMic();
                    isInWaitMicList = true;
                }
                setMicViewStatus();
                micSortListDialog(list);
            } else {
                ToastUtils.showToastShort(getActivity(), getString(R.string.mic_count_max));
            }
            isRequestSortMic = false;
        } else {

            micSortListDialog(list);
        }

    }

    @Override
    public void sortMicOn() {//主播开启排麦
        micSorting = true;
        setMicViewStatus();
    }

    @Override
    public void sortMicOff() {//主播关闭排麦
        micSorting = false;
        isInWaitMicList = false;
        setMicViewStatus();
        if (liveWatchMicSortDialog != null && liveWatchMicSortDialog.isAdded()) {
            liveWatchMicSortDialog.dismiss();
        }
    }

    @Override
    public void audienceLinkMicTimeOut() {
        if (connectMicDialog != null) {
            connectMicDialog.dismiss();
        }
    }

    @Override
    public void upgradeEffects(@NotNull LiveRoomMsgBean liveRoomMsgBean) {
        if (null != live_vip_enter_view) {
            live_vip_enter_view.joinNewUser(liveRoomMsgBean);
            liveRoomMsgBean.type = LiveRoomMsgBean.TYPE_MSG;

            liveRoomMsgBean.content = StringUtils.getString(R.string.congratulations_being_promoted, liveRoomMsgBean.upgradeNickName, liveRoomMsgBean.upgradeUserLevel);

            addLiveRoomMsg(liveRoomMsgBean);
        }

    }

    @Override
    public void allUpgradeEffects(@NotNull LiveRoomMsgBean liveRoomMsgBean) {
        if (mLiveLevelUpView != null) {
            mLiveLevelUpView.showView(liveRoomMsgBean);
        }
    }

    /**
     * 举报
     */
    class ReportListener implements View.OnClickListener {
        String userId;

        public ReportListener(String userId) {
            this.userId = userId;
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(mActivity, ReportActivity.class);
            intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, this.userId);
            if (userId.equals(liveRoomBean.user.id + "") || !TextUtils.isEmpty(smallLiveAuthor)) {//举报主播
                intent.putExtra(TheLConstants.BUNDLE_KEY_REPORT_TYPE, ReportActivity.REPORT_TYPE_ANCHOR);
                intent.putExtra(TheLConstants.BUNDLE_KEY_LOCAL_IMAGE_PATH, getReportImagePath());
                intent.putExtra(TheLConstants.BUNDLE_KEY_LIVE_ROOM_ID, liveRoomBean.liveId);
            } else {//举报观众
                intent.putExtra(TheLConstants.BUNDLE_KEY_REPORT_TYPE, ReportActivity.REPORT_TYPE_AUDIENCE);
                intent.putExtra(TheLConstants.BUNDLE_KEY_REPORT_CONTENT, reportContent);
                intent.putExtra(TheLConstants.BUNDLE_KEY_LIVE_ROOM_ID, liveRoomBean.liveId);

            }
            startActivity(intent);
        }

    }

    /**
     * 举报时截图
     * todo
     */
    private String getReportImagePath() {
        return observer.getScreenShotImagePath();
    }

    /**
     * 横竖屏
     *
     * @param uiRotate
     * @param streamRotate
     */
    private void setOrientation(int uiRotate, int streamRotate) {
        //todo
    }

    @Override
    public void onStop() {
        super.onStop();
        stop();
    }

    @Override
    public void onDestroy() {
        stop();
        destroyed = true;
        dismissKeyboard();
        showPkFinishView();
        isCreate = false;
        try {
            initData();
            initViewState();
        } catch (Exception e) {
            e.printStackTrace();
        }
        LivePkMediaPlayer.getInstance().release();
        if (liveBigGiftAnimLayout != null) {
            liveBigGiftAnimLayout.onDestory();
        }
        if (isGuest() && observer != null) {
            observer.guestLeaveRoom();
        }

        unregisterReceiver();

        LiveGIOPush.getInstance().onDestroy();

        if (presenter != null) {
            presenter.unRegister(getActivity());
        }
        super.onDestroy();
    }

    private void stop() {
        mHandler.removeCallbacks(runnable);
        mHandler.removeCallbacks(getSendGiftPointRunnable);
    }

    /***注册广播***/
    private void registerReceiver() {
        goldUtils = new GoldUtils();
        goldUtils.registerReceiver(this);
      /*  giftWinkSuccessReceiver = new GiftWinkSuccessReceiver();
        final IntentFilter giftFilter = new IntentFilter(TheLConstants.BROADCAST_GIFT_WINK_SUCCESS);
        getContext().registerReceiver(giftWinkSuccessReceiver, giftFilter);
*/
    }

    /***注销广播***/
    private void unregisterReceiver() {
        goldUtils.unRegisterReceiver(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        hidenKeyboard();
        if (liveBigGiftAnimLayout != null) {
            liveBigGiftAnimLayout.onPause();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (liveBigGiftAnimLayout != null) {
            liveBigGiftAnimLayout.onResume();
        }
    }

    private void hidenKeyboard() {
        if (mActivity != null) {
            ViewUtils.hideSoftInput(getActivity());
        }
    }

    @Override
    public void onFollowStatusChanged(int followStatus, String userId, String nickName, String avatar) {
        super.onFollowStatusChanged(followStatus, userId, nickName, avatar);
        if (liveRoomBean != null && liveRoomBean.user != null) {//由于是广播，很可能只是创建注册了，而数据还没获取
            if ((liveRoomBean.user.id + "").equals(userId)) {
                liveRoomBean.user.isFollow = followStatus;
                //                progress_bar.setVisibility(View.GONE);
                refreshFollowBtn(followStatus);
                if (followStatus == FollowStatusChangedImpl.FOLLOW_STATUS_FOLLOW) {
                    observer.addFollowFans();
                }
            }
        }
        if (live_user_card_dialog_view != null && live_user_card_dialog_view.getDialog() != null && live_user_card_dialog_view.getDialog().isShowing()) {
            live_user_card_dialog_view.onFollowStatusChanged(followStatus, userId, nickName, avatar);
        }
        if (live_show_close_view != null) {
            live_show_close_view.followStatusChanged(followStatus, userId, nickName, avatar);
        }
        if (live_user_anchor_card_dialog_view != null && live_user_anchor_card_dialog_view.getDialog() != null && live_user_anchor_card_dialog_view.getDialog().isShowing()) {
            live_user_anchor_card_dialog_view.onFollowStatusChanged(followStatus, userId, nickName, avatar);

        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(mActivity).onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == TheLConstants.REQUEST_CODE_RECHARGE) {//去充值返回
            if (resultCode == TheLConstants.RESULT_CODE_RECHARGE) {
                getGiftListNetData();
            }

            if (danmu_selected_layout != null) {

                danmu_selected_layout.rechargeSuccess();
            }

        } else if (resultCode == TheLConstants.RESULT_CODE_RECOMMEND_SUCCESS) {//推荐成功
//            if (observer != null) {
//                observer.recommendLive();
//            }
        }
    }

    /**********************************************淡入淡出实现************************************************/
    @Override
    public LiveWatchViewFragment show() {
        LiveUtils.animInLiveShowUIView(this, rel_up);
        return this;
    }

    @Override
    public LiveWatchViewFragment hide() {
        LiveUtils.animOutLiveShowUIView(this, rel_up);
        return this;
    }

    @Override
    public LiveWatchViewFragment destroyView() {
        return this;
    }

    @Override
    public boolean isAnimating() {
        return isAnimating;
    }

    @Override
    public void setAnimating(boolean isAnimating) {
        this.isAnimating = isAnimating;
    }

    @Override
    public void showShade(boolean show) {

    }
    /**********************************************淡入淡出实现************************************************/
    /**********************************************一下为直播PK的方法***********************************************************/

    /**
     * 显示直播PK开始view
     *
     * @param pkStartNoticeBean
     */
    @Override
    public void showPkStartView(LivePkStartNoticeBean pkStartNoticeBean) {
        in_pk = true;
        if (pkStartNoticeBean == null) {
            return;
        }
        livePkFriendBean = LiveUtils.getLivePkFriendBean(pkStartNoticeBean);
        pkStreamParams = LiveUtils.getLiveShowPkParam(pkStartNoticeBean.x, pkStartNoticeBean.y, pkStartNoticeBean.width, pkStartNoticeBean.height);
        livePkCountDownView.initView(pkStartNoticeBean.leftTime, LivePkCountDownView.TYPE_IN_PK, pkStartNoticeBean.leftTime).show();
        showPkView();
        showPkBeginAnim();
        LiveGIOPush.getInstance().setWatchPKCount();
    }

    /**
     * 显示PK开始动画
     */
    private void showPkBeginAnim() {
        //playPkAeAnim(LiveConstant.LIVE_PK_AE_ANIM_BEGIN);
        showPkSound(LiveConstant.LIVE_PK_AE_ANIM_STAR);
    }

    /**
     * 显示直播总结view
     *
     * @param pkSummaryNoticeBean
     */
    @Override
    public void showPkSummaryView(LivePkSummaryNoticeBean pkSummaryNoticeBean) {
        in_pk_summary = true;
        if (pkSummaryNoticeBean == null) {
            return;
        }
        livePkAssistsRandView.initView(liveRoomBean.user.id + "", pkSummaryNoticeBean.assisters).show();
        livePkCountDownView.initView(pkSummaryNoticeBean.leftTime, LivePkCountDownView.TYPE_IN_SUMMARY).show();
        final String loserId = pkSummaryNoticeBean.loser;
        if (String.valueOf(liveRoomBean.user.id).equals(loserId)) {//如果我是失败者
            playPkAeAnim(LiveConstant.LIVE_PK_AE_ANIM_FAILED);
            if (livePkFriendBean != null) {
                //  livePkCountDownView.setLiveShowSummaryView(livePkFriendBean.nickName).show();
            }
        } else if (LivePkSummaryNoticeBean.NO_LOSER.equals(loserId)) {//平局
            playPkAeAnim(LiveConstant.LIVE_PK_AE_ANIM_DRAW);
        } else {
            playPkAeAnim(LiveConstant.LIVE_PK_AE_ANIM_VICTORY);
        }
    }

    /**
     * 显示直播关闭view
     *
     * @param pkStopPayload
     */
    @Override
    public void showPkStopView(String pkStopPayload) {
        showPkFinishView();
        LivePkMediaPlayer.getInstance().release();
    }

    /**
     * 显示直播收到软妹币变化
     *
     * @param pkGemNoticeBean
     */
    @Override
    public void showPkGemView(LivePkGemNoticeBean pkGemNoticeBean) {
        if (pkGemNoticeBean == null || TextUtils.isEmpty(pkGemNoticeBean.userId)) {
            return;
        }
        if (pkGemNoticeBean.userId.equals(liveRoomBean.user.id + "")) {//如果是送给自己的
            ourGem = pkGemNoticeBean.pkGem;
        } else {
            theriGem = pkGemNoticeBean.pkGem;
        }
        livePkBloodView.setOurPercent(ourGem, theriGem);
        livePkAssistsRandView.initView(liveRoomBean.user.id + "", pkGemNoticeBean.assisters).setVisibility(View.VISIBLE);

    }

    /**
     * pk断线重连的init消息
     *
     * @param livePkInitBean
     */
    @Override
    public void showLivePkInitView(LivePkInitBean livePkInitBean) {
        if (livePkInitBean == null) {
            return;
        }
        if (liveRoomBean == null || liveRoomBean.user == null) {
            return;
        }
        LiveGIOPush.getInstance().setWatchPKCount();
        in_pk = true;
        livePkFriendBean = LiveUtils.getLivePkFriendBean(livePkInitBean);
        ourGem = livePkInitBean.selfGem;
        theriGem = livePkInitBean.toGem;
        pkStreamParams = LiveUtils.getLiveShowPkParam(livePkInitBean.x, livePkInitBean.y, livePkInitBean.width, livePkInitBean.height);
        showPkView();
        if (livePkInitBean.status.equals(LivePkInitBean.STATUS_PK_BUSY)) {//如果是在pk中
            livePkCountDownView.initView(livePkInitBean.leftTime, LivePkCountDownView.TYPE_IN_PK, livePkInitBean.totalTime).show();
            img_live_pk.setVisibility(View.VISIBLE);
        } else {//如果是在总结中
            in_pk_summary = true;
            livePkCountDownView.initView(livePkInitBean.leftTime, LivePkCountDownView.TYPE_IN_SUMMARY, livePkInitBean.totalTime).show();
            img_live_pk.setVisibility(View.GONE);
        }
        txt_pk_other_name.setVisibility(View.VISIBLE);
        txt_pk_other_name.setText(livePkInitBean.nickName + "");

        L.d("LivePKBloodView", " livePkInitBean.selfGem : " + livePkInitBean.selfGem);

        L.d("LivePKBloodView", " livePkInitBean.toGem : " + livePkInitBean.toGem);

        L.d("LivePKBloodView", " livePkInitBean.assisters.size() : " + livePkInitBean.assisters.size());

        livePkBloodView.setOurPercent(livePkInitBean.selfGem, livePkInitBean.toGem);
        if (livePkInitBean.assisters.size() > 0) {
            livePkAssistsRandView.initView(liveRoomBean.user.id + "", livePkInitBean.assisters).show();
        }
        LiveUtils.setLivePkIconViewParam(img_live_pk, pkStreamParams);

    }

    /**
     * 显示直播挂断view
     *
     * @param pkHangupBean
     */
    @Override
    public void showPkHangupView(LivePkHangupBean pkHangupBean) {
        if (pkHangupBean == null) {
            return;
        }
        if (LivePkHangupBean.HANGUP_STATUS_IN_PK == pkHangupBean.status) {//PK过程中结束
            livePkFailedView.showPkHangupFailedView().show();
        } else {//总结期间结束
            livePkFailedView.showPkSummaryHangupView().show();
        }
    }

    /**
     * 显示Pk的view(不包括倒计时，因为Pk还没开始)
     */
    private void showPkView() {
        LiveUtils.setPkShowBloodViewParams(livePkBloodView, pkStreamParams);
        livePkBloodView.initView().show();
        txt_pk_other_name.setVisibility(View.VISIBLE);
        if (livePkFriendBean != null) {
            txt_pk_other_name.setText(livePkFriendBean.nickName + "");
        }

        //pk开始广告要消失,结束再出来
//        layout_live_ad.setVisibility(View.GONE);
        liveAdViewManager.dismiss();
        view_pk_other.setVisibility(View.VISIBLE);

        //就设置Pk结束失败的view的布局
        LiveUtils.setLiveShowPkFailedViewParam(livePkFailedView, pkStreamParams);
        LiveUtils.setLiveShowPkFailedViewParam(view_pk_other, pkStreamParams);
    }

    private void showPkFinishView() {
        livePkBloodView.destroyView();
        livePkAssistsRandView.destroyView();
        livePkCountDownView.destroyView();
        txt_pk_other_name.setVisibility(View.GONE);
        img_live_pk.setVisibility(View.GONE);
        live_pk_ae_view.cancelAnimation();
        live_pk_ae_view.setVisibility(View.GONE);
        livePkFailedView.destroyView();
        view_pk_other.setVisibility(View.GONE);

        ourGem = 0f;
        theriGem = 0f;
        pkStreamParams = new float[4];
        livePkFriendBean = null;
        pk_fierce_sound_play_time = 0;

        LivePkMediaPlayer.getInstance().release();

        in_pk = false;
        in_pk_summary = false;
    }

    /**
     * 显示直播PK的aeView
     *
     * @param id
     */
    private void playPkAeAnim(final int id) {
        final LivePkAeAnimBean bean = LiveConstant.livePkAeAnimArr.get(id);
        if (bean == null) {
            return;
        }
        live_pk_ae_view.setVisibility(View.VISIBLE);
        LiveUtils.setPkAeViewLayoutParam(live_pk_ae_view, bean, pkStreamParams);
        LiveUtils.playPkAeAnim(live_pk_ae_view, bean, new LivePkContract.LivePkAeAnimListener() {
            @Override
            public void onAnimStatus(View view, int status) {
                switch (status) {
                    case ANIM_START:
                        playPkSound(bean);
                        if (id == LiveConstant.LIVE_PK_AE_ANIM_MIDDLE) {//pk中场播放ae动画，pk Icon要消失，播放结束再出现
                            img_live_pk.setVisibility(View.GONE);
                        } else if (id == LiveConstant.LIVE_PK_AE_ANIM_DRAW || id == LiveConstant.LIVE_PK_AE_ANIM_VICTORY || id == LiveConstant.LIVE_PK_AE_ANIM_FAILED) {//结束动画都在开始时候让icon消失
                            img_live_pk.setVisibility(View.GONE);
                        }
                        break;
                    case ANIM_END:
                        if (id == LiveConstant.LIVE_PK_AE_ANIM_BEGIN) {//如果是Pk begin动画结束（Pk图标），则pk图标要显示
                            if (in_pk) {
                                LiveUtils.setLivePkIconViewParam(img_live_pk, pkStreamParams);
                            }
                        } else if (id == LiveConstant.LIVE_PK_AE_ANIM_MIDDLE) {//pk中场播放ae动画，pk Icon要消失，播放结束再出现
                            if (in_pk && !in_pk_summary) {
                                img_live_pk.setVisibility(View.VISIBLE);
                            }
                        }
                        break;
                }
            }
        });


    }

    /**
     * 播放最后一分钟震动动画
     */
    private void playLastMinuteAnim() {
        LiveUtils.playPkLastMinuteAnimView(img_live_pk, live_pk_ae_view, LiveConstant.livePkAeAnimArr.get(LiveConstant.LIVE_PK_AE_ANIM_FIERCE), null);
    }

    /**
     * 播放直播Pk的声音
     *
     * @param bean
     */
    private void playPkSound(LivePkAeAnimBean bean) {
        if (bean == null) {
            return;
        }
        LiveUtils.playLivePkSound(bean, new LivePkContract.LivePkPlaySoundListener() {
            @Override
            public void onSoundPlay(MediaPlayer player, int status) {
                switch (status) {
                    case SOUND_END:
                        if (player != null) {
                            player.release();
                        }
                        break;
                }
            }
        });
    }

    /**
     * 播放直播Pk的声音
     *
     * @param id
     */
    private void showPkSound(final int id) {
        final LivePkAeAnimBean bean = LiveConstant.livePkAeAnimArr.get(id);
        String path = "";
        if (bean != null) {
            path = LiveConstant.LIVE_PK_SOUND_PATH + bean.sound_path;
        }

        final LivePkContract.LivePkPlaySoundListener listener = new LivePkContract.LivePkPlaySoundListener() {
            @Override
            public void onSoundPlay(MediaPlayer player, int status) {
                switch (status) {
                    case SOUND_END:
                        if (id == LiveConstant.LIVE_PK_SOUND_LAST_TEN_SECOND && player != null && pk_fierce_sound_play_time < 1) {//如果是最后10秒的声音播放结束(为了给服务器容错,但顶多播放两次就够了)
                            pk_fierce_sound_play_time += 1;
                            player.seekTo(0);
                            player.start();
                        } else if (id == LiveConstant.LIVE_PK_AE_ANIM_STAR) {//如果是开始的音乐播放(readyGo)，则紧接着播放Pk begin动画
                            playPkAeAnim(LiveConstant.LIVE_PK_AE_ANIM_BEGIN);//播放Pk begin动画 pk图标
                        }
                        break;
                }
            }
        };
        switch (id) {
            case LiveConstant.LIVE_PK_SOUND_LAST_TEN_SECOND:
                path = LiveConstant.LIVE_PK_SOUND_PATH + LiveConstant.LIVE_PK_SOUND_LAST_TEN_SECOND_PATH;
                break;
        }
        LiveUtils.playSound(path, listener);
    }


    /**********************************************以上为直播PK的方法***********************************************************/

    public void setActivityInfoUrl(String activityInfoUrl) {
        if (presenter != null && activityInfoUrl != null) {
            presenter.getLiveAdInfo(activityInfoUrl);
        }
    }

    /**
     * 设置跳转监听
     */
    public void setLiveSwitchListener(LiveSwitchListener liveSwitchListener) {
        this.liveSwitchListener = liveSwitchListener;
    }

    private SoftGiftListView.GiftOnClickListener mGiftOnClickListener = new SoftGiftListView.GiftOnClickListener() {
        @Override
        public void OnGiftClicked(ViewGroup parent, View view, TimeTextView timeTextView, int pageIndex, int columnIndex, int lineIndex, int currentPosition, int position, SoftGiftBean softGiftBean, boolean sendAnchor, LiveMultiSeatBean seatBean) {
            sendGiftMsg(softGiftBean, timeTextView, sendAnchor, seatBean, position);
        }
    };

    public SoftGiftListView.GiftLianClickListener mGiftLianClickListener = new SoftGiftListView.GiftLianClickListener() {
        @Override
        public void onGiftClicked(ViewGroup parent, View itemView, View clickView, int pageIndex, int columnIndex, int lineIndex, int currentPosition, int position, SoftGiftBean softGiftBean, boolean isSendAnchor, int seatNum) {
            showLianGiftMsgDialog(softGiftBean, isSendAnchor, seatNum);

        }
    };

    private SoftGiftListView.GuestsShowInfoListener mGuestsShowInfoListener = new SoftGiftListView.GuestsShowInfoListener() {
        @Override
        public void onGuestsClicked(String userid) {
            getLiveUserCard(userid);
        }
    };

    private SoftGiftListView.GotoRechargeSoftMoneyListener gotoRechargeSoftMoneyListener = new SoftGiftListView.GotoRechargeSoftMoneyListener() {
        @Override
        public void onGotoRechageClicked() {
//            gotoSoftMoneyActivity();
            if (getActivity() != null) {
                RechargeDialogFragment chargeDialogFragment = new RechargeDialogFragment();
                chargeDialogFragment.setFromAction("去充值", pageId);
                chargeDialogFragment.show(getActivity().getSupportFragmentManager(), RechargeDialogFragment.class.getName());

            }
        }
    };


    private void showGiftPW(LiveMultiSeatBean item) {

        if (softEnjoyBean != null) {
            mGiftPopupWindow = new GiftPopupWindow(getContext(), item, softEnjoyBean.gold, LiveRoomBean.TYPE_VIDEO == liveRoomBean.audioType, liveRoomBean.selfLevel);
            mGiftPopupWindow.setOnGiftClickListener(mGiftOnClickListener);
            mGiftPopupWindow.setOnGiftLianClickListener(mGiftLianClickListener);
            mGiftPopupWindow.setShowGuestsInfoDialogListener(mGuestsShowInfoListener);
            mGiftPopupWindow.setGotoSoftMoneyLinsener(gotoRechargeSoftMoneyListener);
            mGiftPopupWindow.showAtLocation(avatar.getRootView(), Gravity.BOTTOM, 0, 0);
        }

    }

    private SoftInputViewGroup.OnSoftInputShowListener mOnSoftInputShowListener = new SoftInputViewGroup.OnSoftInputShowListener() {
        @Override
        public void onSoftInputShow(boolean isShow, boolean isShowDanmu) {

            L.d(TAG, " onSoftInputShow isShow : " + isShow);

            L.d(TAG, " onSoftInputShow isShowDanmu : " + isShowDanmu);

            if (isShow || isShowDanmu) {
                send_edit_msg.setVisibility(View.VISIBLE);
                control_ll.setVisibility(View.GONE);
                if (liveRoomBean != null && liveRoomBean.audioType == LiveRoomBean.TYPE_VOICE && liveRoomBean.isMulti == LiveRoomBean.MULTI_LIVE) {
                    vip_enter_and_chat_view.setVisibility(View.INVISIBLE);
                }

                layer_view.setVisibility(View.VISIBLE);
            } else {
                send_edit_msg.setVisibility(View.GONE);
                control_ll.setVisibility(View.VISIBLE);
                if (liveRoomBean != null && liveRoomBean.audioType == LiveRoomBean.TYPE_VOICE && liveRoomBean.isMulti == LiveRoomBean.MULTI_LIVE) {
                    vip_enter_and_chat_view.setVisibility(View.VISIBLE);
                }

                if (bubble_guide_layout != null && bubble_guide_layout.isShown()) {
                    bubble_guide_layout.setVisibility(View.INVISIBLE);
                }

                layer_view.setVisibility(View.GONE);
            }

            //显示弹幕选择界面
            if (isShowDanmu) {
                edit_input.setFocusable(false);
                edit_input.setCursorVisible(false);
            } else {
                edit_input.setFocusable(true);
                edit_input.setFocusableInTouchMode(true);
                edit_input.requestFocus();
                edit_input.setCursorVisible(true);
            }

            L.d(TAG, " onSoftInputShow edit_input.isFocusable() : " + edit_input.isFocusable());

        }
    };


    private void startAnimator(int startY, final int x, final View view) {
        final ValueAnimator valueAnimator = ValueAnimator.ofInt(startY, startY + SizeUtils.dip2px(TheLApp.context, 10), startY);
        valueAnimator.setDuration(1000);
        valueAnimator.setRepeatCount(5);
        valueAnimator.setRepeatMode(ValueAnimator.RESTART);
        valueAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                if (view != null) {
                    view.setLayerType(View.LAYER_TYPE_HARDWARE, null);
                }

                L.d(TAG, " onAnimationStart : ");

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (view != null) {
                    view.setLayerType(View.LAYER_TYPE_NONE, null);
                    view.setVisibility(View.INVISIBLE);
                }

                valueAnimator.cancel();

                L.d(TAG, " onAnimationEnd : ");

            }

            @Override
            public void onAnimationCancel(Animator animation) {

                L.d(TAG, " onAnimationCancel : ");

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

                L.d(TAG, " onAnimationRepeat : ");

            }
        });
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int offset = (int) animation.getAnimatedValue();
                view.setY(offset);
                view.setX(x);

            }
        });
        valueAnimator.setInterpolator(new LinearInterpolator());
        valueAnimator.start();
    }

    private void isAudienceLinkMic(boolean isAudienceLinkMic) {
        if (getActivity() != null) {
            LiveWatchActivity activity = (LiveWatchActivity) getActivity();
            activity.isAudienceLinkMic(isAudienceLinkMic);
        }
    }

    private void showNoticeTips() {
        //公告栏气泡提示
        if (!ShareFileUtils.getBoolean(ShareFileUtils.LIVE_NOTICE_SHOW, false)) {

            ShareFileUtils.setBoolean(ShareFileUtils.LIVE_NOTICE_SHOW, true);

            notice_board_tips.setVisibility(View.VISIBLE);

            int guideWidth = notice_board_tips.getWidth();

            int[] screenArray = new int[2];

            live_notice.getLocationInWindow(screenArray);

            int marginTop = screenArray[1] + DensityUtils.dp2px(10) + live_notice.getHeight();

            int marginLeft = screenArray[0] - guideWidth / 2 + live_notice.getWidth() / 2;

            startAnimator(marginTop, marginLeft, notice_board_tips);
        }
    }

    private ViewTreeObserver.OnGlobalLayoutListener mOnGlobalLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {

        @Override
        public void onGlobalLayout() {

            L.d("MainFragment", "----------onGlobalLayout---------");

            if (mGuideView != null) {
                mGuideView.setVisibility(View.VISIBLE);
                String followStr = TheLApp.context.getResources().getString(R.string.follow_me_if_you_like_me);
                mGuideView.show(txt_follow, followStr);
            }

            if (txt_follow != null && mOnGlobalLayoutListener != null) {
                txt_follow.getRootView().getViewTreeObserver().removeOnGlobalLayoutListener(mOnGlobalLayoutListener);
            }
        }
    };

    private FirstChargeCtrl.FirstChargeCallBack mFirstChargeCallBack = new FirstChargeCtrl.FirstChargeCallBack() {
        @Override
        public void getCompleted(boolean isFirstCharge) {
            isFirstChargeMark = isFirstCharge;
            showFirstChargeGuideIfNeeded();
        }
    };

    private void handleFirstChargeDelay() {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                showFirstChargeGuideIfNeeded();
            }
        }, 5500);//在快捷礼物guide 5s的基础上延迟
    }

    private void showFirstChargeGuideIfNeeded() {
        L.d(TAG, " showFirstChargeGuideIfNeeded " + isFirstChargeMark + " fastGiftGuide:" + fast_gift_guide_view.getVisibility() + " " + FirstChargeCtrl.sIsLiveRoomFirstChargeAlreadyShow);
        if (!isAdded()) {
            return;
        }
        if (!isFirstChargeMark) {//
            return;
        }
        if (fast_gift_guide_view.getVisibility() == VISIBLE) {//如果快捷礼物引导动画正在展示
            return;
        }
        if (FirstChargeCtrl.sIsLiveRoomFirstChargeAlreadyShow) {
            return;
        }
        showFirstChargeGuide();
    }

    private void showFirstChargeGuide() {
        firstChargeGuideView.removeCallbacks(firstChargeGuideRunnable);
        firstChargeGuideView.postDelayed(firstChargeGuideRunnable, 1000);
    }

    /*
     * 通过postdelay方式缓解因firstcharge网络请求和LiveGift网络请求结果时间过于相近引起的闪烁
     * */
    private Runnable firstChargeGuideRunnable = new Runnable() {
        @Override
        public void run() {
            firstChargeGuideView.setArrowOffset(ll_fast_gift.getVisibility() != VISIBLE);
            firstChargeGuideView.show(img_emoji, TheLApp.getContext().getString(R.string.first_recharge_guide_text));
            FirstChargeCtrl.sIsLiveRoomFirstChargeAlreadyShow = true;
        }
    };


    private SoftGiftBean getGiftInfoByGiftId(int giftId) {

        if (softEnjoyBean == null || softEnjoyBean.list == null) {
            return null;
        }

        for (SoftGiftBean softGiftBean : softEnjoyBean.list) {

            if (softGiftBean.id == giftId) {
                return softGiftBean;
            }

        }

        return null;

    }

}
