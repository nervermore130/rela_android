package com.thel.modules.live.view;

import android.content.Context;

import androidx.annotation.Nullable;

import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.modules.live.in.LiveBaseView;
import com.thel.utils.L;

/**
 * Created by waiarl on 2017/11/29.
 * 直播PK血条 view
 */

public class LivePKBloodView extends LinearLayout implements LiveBaseView<LivePKBloodView> {

    private static final String TAG = LivePKBloodView.class.getSimpleName();
    private final Context mContext;
    private View lin_our;
    private View lin_their;
    private TextView txt_our;
    private TextView txt_their;
    private View img_light;
    private LinearLayout.LayoutParams ourParam;
    private float percent;
    private float theirGold;
    private float ourGold;


    public LivePKBloodView(Context context) {
        this(context, null);
    }

    public LivePKBloodView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LivePKBloodView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }

    private void init() {
        inflate(mContext, R.layout.live_pk_blood_view, this);
        lin_our = findViewById(R.id.lin_our);
        lin_their = findViewById(R.id.lin_their);
        txt_our = findViewById(R.id.txt_our);
        txt_their = findViewById(R.id.txt_their);
        img_light = findViewById(R.id.img_light);
        ourParam = (LayoutParams) lin_our.getLayoutParams();
    }

    public LivePKBloodView initView() {
        ourParam.width = LayoutParams.MATCH_PARENT;
        setOurPercent(0, 0);
        return this;
    }

    public LivePKBloodView initView(float ourGold, float theirGold) {
        setOurPercent(ourGold, theirGold);
        return this;
    }

    private void setGold(float ourGold, float theirGold) {
        final float og = (float) (Math.round(ourGold * 100)) / 100;
        final float tg = (float) (Math.round(theirGold * 100)) / 100;
        txt_our.setText(TheLApp.getContext().getString(R.string.my_credits, og + ""));
        txt_their.setText(TheLApp.getContext().getString(R.string.her_credits, tg + ""));
    }

    /**
     * 我方的血量百分比
     * 0-100；
     *
     * @return
     */
    public LivePKBloodView setOurPercent(float ourGold, float theirGold) {
        this.ourGold = ourGold;
        this.theirGold = theirGold;
        setGold(ourGold, theirGold);
        setBloodPercent();
        return this;
    }

    private void setBloodPercent() {

        L.d(TAG, " ourGold : " + ourGold);

        L.d(TAG, " theirGold : " + theirGold);

        if (ourGold == 0 && theirGold == 0) {
            percent = 50;
        } else if (ourGold == 0) {
            percent = 0;
        } else {
            percent = ourGold * 100 / (ourGold + theirGold);
        }

        L.d(TAG, " percent : " + percent);

        final int maxTextWidth = getMaxTextWidth();

        L.d(TAG, " maxTextWidth : " + maxTextWidth);

        final int remainWidth = getRemainWidth(maxTextWidth);

        L.d(TAG, " remainWidth : " + remainWidth);

        int widht = (int) (remainWidth * percent / 100) + maxTextWidth;

        L.i(TAG, "widht : " + widht);

        if (widht == 0) {
            widht = LayoutParams.MATCH_PARENT;
        }
        ourParam.width = widht;
        if (widht != LayoutParams.MATCH_PARENT) {
            ourParam.weight = 0;
        } else {
            ourParam.weight = 1;
        }

    }

    /**
     * 获取除了名字显示以及中间图片以外的剩余长度
     *
     * @param maxTextWidth
     * @return
     */
    private int getRemainWidth(int maxTextWidth) {
        final int img_light_w = img_light.getMeasuredWidth();
        final int mWidth = getMeasuredWidth();
        final int remainWidth = mWidth - 2 * maxTextWidth - img_light_w;
        return remainWidth;
    }

    private int getMaxTextWidth() {
        final int txt_our_w = txt_our.getWidth();
        final int txt_their_w = txt_their.getWidth();
        return Math.max(txt_our_w, txt_their_w);
    }

    @Override
    public LivePKBloodView show() {
        setVisibility(View.VISIBLE);
        return this;
    }

    @Override
    public LivePKBloodView hide() {
        return null;
    }

    @Override
    public LivePKBloodView destroyView() {
        ourParam.width = LayoutParams.MATCH_PARENT;
        setGold(0, 0);
        setVisibility(View.INVISIBLE);
        return this;
    }

    @Override
    public boolean isAnimating() {
        return false;
    }

    @Override
    public void setAnimating(boolean isAnimating) {

    }

    @Override
    public void showShade(boolean show) {

    }
}
