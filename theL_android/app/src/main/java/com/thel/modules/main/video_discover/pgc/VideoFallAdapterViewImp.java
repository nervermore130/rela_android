package com.thel.modules.main.video_discover.pgc;

import android.view.View;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.video.VideoBean;
import com.thel.modules.main.video_discover.videofalls.RecyclerViewStateBean;
import com.thel.utils.AppInit;

/**
 * Created by waiarl on 2018/3/19.
 */

public interface VideoFallAdapterViewImp<T extends View> {
    float divider = TheLApp.getContext().getResources().getDimension(R.dimen.nearby_user_view_padding_2x);
    float photoWidth = (AppInit.displayMetrics.widthPixels - divider * 3) / 2;
    float photoHeight = photoWidth * 16 / 9;
    float pgcWidth = AppInit.displayMetrics.widthPixels - divider * 2;
    float pgcHeight = pgcWidth * 9 / 16;
    float radius = TheLApp.getContext().getResources().getDimension(R.dimen.nearby_user_view_radius);
    long MINUTI = 60;
    long HOUR = 60 * MINUTI;
    long DAY = HOUR * 24;


    T initView(VideoBean videoBean, int position, RecyclerViewStateBean recyclerViewStateBean);

    T getView();
}
