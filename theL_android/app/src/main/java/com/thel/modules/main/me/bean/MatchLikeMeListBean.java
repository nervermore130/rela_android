package com.thel.modules.main.me.bean;

import com.thel.base.BaseDataBean;

import java.util.ArrayList;
import java.util.List;

public class MatchLikeMeListBean extends BaseDataBean {
    public MatchResultBean data;

    public static class MatchResultBean {
        /**
         * 错误标识
         */
        public String errcode = "";

        /**
         * 错误描述
         */
        public String errdesc = "";

        /**
         * 错误描述英文国际化
         */
        public String errdesc_en = "";

        /**
         * 是否成功 (成功为1，失败为0)
         */
        public String result = "";
        public MatchListDataBean data;

    }

    public static class MatchListDataBean {

        public int countdown;
        public String cursor;
        public boolean haveNextPage;
        public int notReplyMeCount;

        @Override
        public String toString() {
            return "MatchListDataBean{" + "countdown=" + countdown + ", matchList=" + matchList + '}';
        }

        public List<MatchBean> matchList = new ArrayList<>();
    }
}