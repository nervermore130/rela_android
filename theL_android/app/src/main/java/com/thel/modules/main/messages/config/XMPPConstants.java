package com.thel.modules.main.messages.config;

public class XMPPConstants {

    public static final String MSGSTATUS_SENDING = "2";
    public static final String MSGSTATUS_SUCCESS = "1";
    public static final String MSGSTATUS_FAIL = "0";

    public static final int XMPP_DISCONNECTED = 0;
    public static final int XMPP_CONNECTING = 1;
    public static final int XMPP_CONNECTED = 2;

    public static final String MSG_CONFIRM_TABLENAME = "msg_confirm";

}
