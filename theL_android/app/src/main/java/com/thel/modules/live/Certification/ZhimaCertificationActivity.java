package com.thel.modules.live.Certification;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.constants.BundleConstants;
import com.thel.constants.TheLConstants;
import com.thel.modules.live.bean.ZmxyAuthBean;
import com.thel.modules.main.home.moments.web.WebViewActivity;
import com.thel.utils.IDCardUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.ViewUtils;

import java.net.URLEncoder;
import java.util.List;

/**
 * 芝麻信用认证直播权限
 * Created by lingwei on 2018/3/16.
 */

public class ZhimaCertificationActivity extends BaseActivity implements ZhimaCertificationContract.View, TextWatcher {

    private EditText edit_real_user_name;
    private EditText edit_real_id_number;
    private TextView button_login;
    private ZhimaCertificationContract.Presenter presenter;
    private TextView tx_caution;
    private LinearLayout lin_back;
    private RelativeLayout other_way_certification;
    private int intent_from = LIVE_PREMISSION;
    public static final int LIVE_PREMISSION = 0;
    public static final int ONSEAT_AUTH = 1;//上麦认证

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.liveuser_certification_layout);
        initView();
        initData();
        setLintener();
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void initView() {
        new ZhimaCertificationPresenter(this);
        TextView txt_title = findViewById(R.id.txt_title);
        txt_title.setText(R.string.recruiting_anchor);
        ImageView banner = findViewById(R.id.banner_certification);
        edit_real_user_name = findViewById(R.id.edit_real_user_name);
        edit_real_id_number = findViewById(R.id.edit_real_id_number);
        button_login = findViewById(R.id.button_certification);
        tx_caution = findViewById(R.id.tx_caution);
        lin_back = findViewById(R.id.lin_back);
        View lin_more = findViewById(R.id.lin_more);
        lin_more.setVisibility(View.GONE);
        other_way_certification = findViewById(R.id.other_way_certification);
        edit_real_user_name.addTextChangedListener(this);
        edit_real_id_number.addTextChangedListener(this);
    }

    private void initData() {
        Intent intent = getIntent();
        intent_from = intent.getIntExtra(TheLConstants.BUNDLE_KEY_INTENT_FROM, LIVE_PREMISSION);
        /**
         * 认证失败回调到这个页面来 并将先前填的姓名和身份证号 重新回显到输入框中
         * */
        String id_card = ShareFileUtils.getString(ShareFileUtils.MY_ID_CARD, "");
        String my_real_name = ShareFileUtils.getString(ShareFileUtils.MY_REAL_NAME, "");
        edit_real_user_name.setText(my_real_name);
        edit_real_id_number.setText(id_card);

    }

    private void setLintener() {
        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1500);
                String idCardNumber = edit_real_id_number.getText().toString().trim();
                String userName = edit_real_user_name.getText().toString().trim();
                ShareFileUtils.setString(ShareFileUtils.MY_ID_CARD, idCardNumber);
                ShareFileUtils.setString(ShareFileUtils.MY_REAL_NAME, userName);
                ShareFileUtils.setInt(ShareFileUtils.CERTIFICATION_TYPE, intent_from);
                postAuth(userName, idCardNumber, "zhimaactivity://");

            }
        });
        lin_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        /**
         * 其他方式认证
         * */
        other_way_certification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1500);
                Intent intent = new Intent(ZhimaCertificationActivity.this, WebViewActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(BundleConstants.URL, TheLConstants.applyLivePermitPage);
                bundle.putBoolean(BundleConstants.NEED_SECURITY_CHECK, false);
                intent.putExtras(bundle);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

            }
        });

    }

    private void postAuth(final String userName, final String idCardNumber, final String callback) {
        presenter.postZmAuthUrl(userName, idCardNumber, callback);
    }

    /***
     * 判断身份证是否合法
     * */
    public static int isIDCard(String idcard) {

        int validatedAllIdcard = IDCardUtils.isValidatedAllIdcard(idcard);


        return validatedAllIdcard;
    }

    /**
     * 启动支付宝进行认证
     *
     * @param url 开放平台返回的URL
     */
    private void doVerify(String url) {
        if (hasApplication()) {
            Intent action = new Intent(Intent.ACTION_VIEW);
            StringBuilder builder = new StringBuilder();
            // 这里使用固定appid 20000067
            builder.append("alipays://platformapi/startapp?appId=20000067&url=");
            builder.append(URLEncoder.encode(url));
            action.setData(Uri.parse(builder.toString()));
            startActivity(action);
            finish();
        } else {
            // 处理没有安装支付宝的情况
            new AlertDialog.Builder(this).setMessage("是否下载并安装支付宝完成认证?").setPositiveButton("好的", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent action = new Intent(Intent.ACTION_VIEW);
                    action.setData(Uri.parse("https://m.alipay.com"));
                    startActivity(action);
                    finish();
                }
            }).setNegativeButton("算了", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).show();
        }
    }

    /**
     * 判断是否安装了支付宝
     *
     * @return true 为已经安装
     */
    private boolean hasApplication() {
        PackageManager manager = getPackageManager();
        Intent action = new Intent(Intent.ACTION_VIEW);
        action.setData(Uri.parse("alipays://"));
        List list = manager.queryIntentActivities(action, PackageManager.GET_RESOLVED_FILTER);
        return list != null && list.size() > 0;
    }

    @Override
    public void setPresenter(ZhimaCertificationContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void getAuthUrl(ZmxyAuthBean zmxyAuthBean) {
        if (zmxyAuthBean != null && zmxyAuthBean.data != null) {

            doVerify(zmxyAuthBean.data.authUrl);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
        boolean textlength = edit_real_id_number.getText().toString().trim().length() < 18;
        String idCardNumber = edit_real_id_number.getText().toString().trim();

        if (idCardNumber.length() > 0 && textlength) {
            tx_caution.setVisibility(View.VISIBLE);
            edit_real_id_number.setTextColor(ContextCompat.getColor(this, R.color.red));
            tx_caution.setText(TheLApp.context.getString(R.string.confirm_id_num_istrue));
            button_login.setEnabled(false);
        } else if (!textlength) {
            int certi = isIDCard(idCardNumber);

            if (certi == 0) {
                edit_real_id_number.setTextColor(Color.parseColor("#232323"));

                tx_caution.setVisibility(View.GONE);
                button_login.setEnabled(true);

            } else if (certi == 1) {
                edit_real_id_number.setTextColor(ContextCompat.getColor(this, R.color.red));
                tx_caution.setVisibility(View.VISIBLE);
                tx_caution.setText(TheLApp.context.getString(R.string.male_certification_prohibited));
                button_login.setEnabled(false);
            } else if (certi == 2) {
                edit_real_id_number.setTextColor(ContextCompat.getColor(this, R.color.red));

                tx_caution.setVisibility(View.VISIBLE);
                tx_caution.setText(TheLApp.context.getString(R.string.confirm_id_num_istrue));
                button_login.setEnabled(false);
            }
        }

    }

    //获取运行屏幕宽度
    public int getScreenWidth() {
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        //宽度 dm.widthPixels
        //高度 dm.heightPixels
        return dm.widthPixels;
    }
}
