package com.thel.modules.main.messages;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.kyleduo.switchbutton.SwitchButton;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.constants.TheLConstants;
import com.thel.db.DBConstant;
import com.thel.db.DBUtils;
import com.thel.db.table.message.MsgTable;
import com.thel.manager.ChatServiceManager;
import com.thel.modules.main.home.moments.ReportActivity;
import com.thel.modules.main.messages.bean.MsgBean;
import com.thel.modules.select_image.SelectLocalImagesActivity;
import com.thel.utils.DialogUtil;
import com.thel.utils.ImageUtils;
import com.thel.utils.L;
import com.thel.utils.SharedPrefUtils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChatOperationsActivity extends BaseActivity {

    private static final String TAG = "ChatOperationsActivity";

    @BindView(R.id.lin_back)
    LinearLayout lin_back;

    @BindView(R.id.set_bg_content)
    LinearLayout set_bg_content;

    @BindView(R.id.set_bg_to_all_content)
    LinearLayout set_bg_to_all_content;

    @BindView(R.id.clear_content)
    LinearLayout clear_content;

    @BindView(R.id.top_sb)
    SwitchButton top_sb;

    @OnClick(R.id.clear_content)
    public void clearContentClicked() {
        DialogUtil.showConfirmDialog(ChatOperationsActivity.this, "", getString(R.string.chat_activity_info_delete_messages), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                setResult(TheLConstants.RESULT_CODE_CLEAR_CHAT);
                if (toUserId != null) {

                    L.d("ChatOperationsActivity", " toUserId : " + toUserId);

                    String tableName;

                    if (toUserId.equals(DBConstant.Message.FOLLOW_TABLE_NAME)) {
                        tableName = DBConstant.Message.FOLLOW_TABLE_NAME;
                        ChatServiceManager.getInstance().dropTable(tableName);
                    } else if (toUserId.equals(DBConstant.Message.WINK_TABLE_NAME)) {
                        ChatServiceManager.getInstance().clearWinksTable();
                    } else {

                        MsgTable mt = ChatServiceManager.getInstance().getMsgTableByUserId(toUserId);

                        tableName = DBUtils.getChatTableName(toUserId);
                        ChatServiceManager.getInstance().dropTable(tableName);
                        ChatServiceManager.getInstance().deleteMsgTable(toUserId);

                        if (mt != null) {
                            if (mt.isStranger == 1) {
                                List<MsgTable> msgTables = ChatServiceManager.getInstance().getStrangerMsg();

                                if (msgTables.size() <= 0) {
                                    ChatServiceManager.getInstance().removeMsgTable(DBUtils.getStrangerTableName());
                                } else {

                                    MsgTable msgTable = msgTables.get(msgTables.size() - 1);

                                    MsgTable strangerMsgTable = ChatServiceManager.getInstance().getMsgTableByUserId(DBConstant.Message.STRANGER_TABLE_NAME);

                                    strangerMsgTable.userName = msgTable.userName;
                                    strangerMsgTable.msgType = msgTable.msgType;
                                    strangerMsgTable.msgText = msgTable.msgText;
                                    if (strangerMsgTable.msgTime < DBConstant.STICK_TOP_BASE_TIME) {
                                        strangerMsgTable.msgTime = msgTable.msgTime;
                                    }

                                    ChatServiceManager.getInstance().updateMsgTableWithWink(strangerMsgTable);

                                    ChatServiceManager.getInstance().pushUpdateMsg(strangerMsgTable);

                                }
                            }
                        }

                    }
                }
            }
        });
    }


    private String toUserId = "";
    private int width = 0;
    private int height = 0;

    private boolean setBgToAllChats = false;

    private DialogUtil dialogUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_operations);
        ButterKnife.bind(this);
        dialogUtils = new DialogUtil();
        toUserId = getIntent().getStringExtra(TheLConstants.BUNDLE_KEY_USER_ID);

        L.d("ChatOperationsActivity", " toUserId : " + toUserId);

        if (toUserId.equals(MsgBean.MSG_TYPE_REQUEST)) clear_content.setVisibility(View.INVISIBLE);
        width = getIntent().getIntExtra(TheLConstants.BUNDLE_KEY_WIDTH, 0);
        height = getIntent().getIntExtra(TheLConstants.BUNDLE_KEY_HEIGHT, 0);
        if (TextUtils.isEmpty(toUserId) || width == 0 || height == 0) {
            finish();
        }

        MsgTable msgTable = ChatServiceManager.getInstance().getMsgTableByUserId(toUserId);

        if (msgTable != null && msgTable.msgTime < DBConstant.STICK_TOP_BASE_TIME) {
            top_sb.setChecked(false);
        } else {
            top_sb.setChecked(true);
        }

        top_sb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                MsgTable msgTable = ChatServiceManager.getInstance().getMsgTableByUserId(toUserId);

                stickOrUnstickOnUI(msgTable);

            }
        });
    }

    /**
     * 置顶或取消置顶（刷新界面）
     */
    private void stickOrUnstickOnUI(MsgTable msgTable) {
        if (msgTable != null && msgTable.msgTime != 0) {
            if (DBConstant.STICK_TOP_BASE_TIME > msgTable.msgTime) {
                ChatServiceManager.getInstance().stickTop(msgTable);
            } else {
                ChatServiceManager.getInstance().resetStickTop(msgTable);
            }

            this.sendBroadcast(new Intent(TheLConstants.BROADCAST_CLICK_MESSAGE));

        }

    }

    private void displayPhotoChooseDialog() {
        dialogUtils.showSelectionDialog(this, new String[]{getString(R.string.uploadimage_activity_album),
                getString(R.string.info_reset)}, new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapter, View view, int position, long arg3) {
                ViewUtils.preventViewMultipleClick(view, 2000);
                dialogUtils.closeDialog();
                switch (position) {
                    case 0:
                        Intent intent = new Intent(ChatOperationsActivity.this, SelectLocalImagesActivity.class);
                        intent.putExtra(TheLConstants.BUNDLE_KEY_PHOTO_NAME, toUserId + ".jpeg");
                        startActivityForResult(intent, TheLConstants.BUNDLE_CODE_CHAT_OPERATIONS_ACTIVITY);
                        break;
                    case 1:
                        if (setBgToAllChats) {
                            SharedPrefUtils.clearData(SharedPrefUtils.FILE_NAME_CHAT_BG);
                        } else {
                            SharedPrefUtils.setString(SharedPrefUtils.FILE_NAME_CHAT_BG, toUserId, "reset");
                        }
                        DialogUtil.showToastShort(ChatOperationsActivity.this, getString(R.string.chat_activity_reset_succeed));
                        finish();
                        break;
                    default:
                        break;
                }
            }
        }, false, 2, null);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(this.getClass().getName());
        MobclickAgent.onResume(this);
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(this.getClass().getName());
        MobclickAgent.onPause(this);
    }

    @SuppressLint("NewApi")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == TheLConstants.RESULT_CODE_TAKE_PHOTO) {
            String path = data.getStringExtra(TheLConstants.BUNDLE_KEY_IMAGE_OUTPUT_PATH);
            if (!TextUtils.isEmpty(path)) {
                String outputPath = "";
                if (setBgToAllChats) {
                    outputPath = SharedPrefUtils.CHAT_BG_ALL + "_" + System.currentTimeMillis() + ".jpeg";
                } else {
                    outputPath = toUserId + "_" + System.currentTimeMillis() + ".jpeg";
                }
                if (ImageUtils.handleLocalPic(path, TheLConstants.F_CHAT_BG_ROOTPATH, outputPath, width, height, 60, true)) {
                    if (setBgToAllChats) {
                        SharedPrefUtils.clearData(SharedPrefUtils.FILE_NAME_CHAT_BG);
                        SharedPrefUtils.setString(SharedPrefUtils.FILE_NAME_CHAT_BG, SharedPrefUtils.CHAT_BG_ALL, TheLConstants.F_CHAT_BG_ROOTPATH + outputPath);
                    } else {
                        SharedPrefUtils.setString(SharedPrefUtils.FILE_NAME_CHAT_BG, toUserId, TheLConstants.F_CHAT_BG_ROOTPATH + outputPath);
                    }
                    DialogUtil.showToastShort(ChatOperationsActivity.this, getString(R.string.chat_activity_set_bg_succeed));
                    finish();
                } else {
                    DialogUtil.showToastShort(this, getString(R.string.uploadimage_activity_size_error));
                }
            }
        } else if (resultCode == TheLConstants.RESULT_CODE_SELECT_LOCAL_IMAGE) {
            String path = data.getStringExtra(TheLConstants.BUNDLE_KEY_LOCAL_IMAGE_PATH);
            if (null != path && path.length() > 1) {
                String outputPath = "";
                if (setBgToAllChats) {
                    outputPath = SharedPrefUtils.CHAT_BG_ALL + "_" + System.currentTimeMillis() + ".jpeg";
                } else {
                    outputPath = toUserId + "_" + System.currentTimeMillis() + ".jpeg";
                }
                if (ImageUtils.handleLocalPic(path, TheLConstants.F_CHAT_BG_ROOTPATH, outputPath, width, height, 60, true)) {
                    if (setBgToAllChats) {
                        SharedPrefUtils.clearData(SharedPrefUtils.FILE_NAME_CHAT_BG);
                        SharedPrefUtils.setString(SharedPrefUtils.FILE_NAME_CHAT_BG, SharedPrefUtils.CHAT_BG_ALL, TheLConstants.F_CHAT_BG_ROOTPATH + outputPath);
                    } else {
                        SharedPrefUtils.setString(SharedPrefUtils.FILE_NAME_CHAT_BG, toUserId, TheLConstants.F_CHAT_BG_ROOTPATH + outputPath);
                    }
                    DialogUtil.showToastShort(ChatOperationsActivity.this, getString(R.string.chat_activity_set_bg_succeed));
                    finish();
                } else {
                    DialogUtil.showToastShort(this, getString(R.string.uploadimage_activity_size_error));
                }

            } else {
                Toast.makeText(ChatOperationsActivity.this, TheLApp.getContext().getString(R.string.info_rechoise_photo), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @OnClick(R.id.set_bg_to_all_content)
    void bgToAllContent() {
        setBgToAllChats = true;
        displayPhotoChooseDialog();
    }

    @OnClick(R.id.set_bg_content)
    void bgContentClicked() {
        setBgToAllChats = false;
        displayPhotoChooseDialog();
    }

    @OnClick(R.id.lin_back)
    void backClicked() {
        ViewUtils.preventViewMultipleClick(lin_back, 2000);
        finish();
    }

    @OnClick(R.id.report_rl)
    void report() {
        Intent intent = new Intent(ChatOperationsActivity.this, ReportActivity.class);
        intent.putExtra(TheLConstants.BUNDLE_KEY_REPORT_TYPE, ReportActivity.REPORT_TYPE_REPORT_USER);
        intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, toUserId);
        startActivity(intent);
    }
}
