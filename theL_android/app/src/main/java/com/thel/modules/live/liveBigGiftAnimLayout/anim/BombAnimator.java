package com.thel.modules.live.liveBigGiftAnimLayout.anim;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PointF;
import android.os.Handler;
import android.os.Looper;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.thel.R;
import com.thel.modules.live.liveBigGiftAnimLayout.LiveBigAnimUtils;
import com.thel.modules.live.liveBigGiftAnimLayout.FrameAnimAssetSurfaceView;
import com.thel.utils.Utils;

import java.util.Random;

/**
 * Created by the L on 2016/12/9.
 */

public class BombAnimator {

    private final Context mContext;
    private final int mDuration;
    private final Handler mHandler;
    private final int rockRate;
    private final Random mRandom;
    private final int flowerRate;
    //  private final int[] bombRes;
    //private final int[] flowerRes;
    private final String foldPath;
    private final String defaultBomb;
    private final String[] bombRes;
    private final String[] flowerRes;
    private final int minFlowerWidth;
    private final int differFlowerWidth;
    private final int flowerDuration;
    private final int radius;
    private final float minMul;
    private final float differMul;
    private final int waveNum;
    private int flowerWave;
    private final int mintFlowerCount;
    private final int differFlowerCount;
    private int mWidth;
    private int mHeight;
    private boolean isPlaying;
    private float startScale;
    private int throwDuration;
    private long alphaDuration;

    public BombAnimator(Context context, int duration) {
        mContext = context;
        mDuration = duration;
        mHandler = new Handler(Looper.getMainLooper());
        mRandom = new Random();
        startScale = 0.05f;//炸弹初始比例
        throwDuration = 700;//炸弹抛出时间
        alphaDuration = 700;//炸弹渐变色时间

        //bombRes = new int[]{R.drawable.bomb_2, R.drawable.bomb_3, R.drawable.bomb_4, R.drawable.bomb_5, R.drawable.bomb_6, R.drawable.bomb_7, R.drawable.bomb_8, R.drawable.bomb_9, R.drawable.bomb_10, R.drawable.bomb_11, R.drawable.bomb_12, R.drawable.bomb_13, R.drawable.bomb_14, R.drawable.bomb_15};
        //flowerRes = new int[]{R.drawable.bomb_icon_1, R.drawable.bomb_icon_2, R.drawable.bomb_icon_3, R.drawable.bomb_icon_4, R.drawable.bomb_icon_5, R.drawable.bomb_icon_6, R.drawable.bomb_icon_7, R.drawable.bomb_icon_8, R.drawable.bomb_icon_9, R.drawable.bomb_icon_10, R.drawable.bomb_icon_11, R.drawable.bomb_icon_12};
        foldPath = "anim/bomb";
        defaultBomb = "bomb_1";
        bombRes = new String[]{"bomb_2", "bomb_3", "bomb_4", "bomb_5", "bomb_6", "bomb_7", "bomb_8", "bomb_9", "bomb_10", "bomb_11", "bomb_12", "bomb_13", "bomb_14", "bomb_15"};
        flowerRes = new String[]{"bomb_icon_1", "bomb_icon_2", "bomb_icon_3", "bomb_icon_4", "bomb_icon_5", "bomb_icon_6", "bomb_icon_7", "bomb_icon_8", "bomb_icon_9", "bomb_icon_10", "bomb_icon_11", "bomb_icon_12"};
        rockRate = 150;//炸弹帧动画频率
        mintFlowerCount = 40;//最小炸花数量
        differFlowerCount = 20;//炸花区间
        flowerRate = 30;//炸花间隔
        flowerDuration = 1000;//炸花存在时间
        minFlowerWidth = Utils.dip2px(mContext, 3);//炸花最小宽度
        differFlowerWidth = Utils.dip2px(mContext, 3);//炸花大小区间
        waveNum = 2;//炸花波数
        radius = Utils.dip2px(mContext, 50);//炸花开始位置圆半径
        minMul = 0.6f;//辐射距离最小倍数
        differMul = 1f;//辐射距离倍数变化区间
    }

    public int start(final ViewGroup parent) {
        isPlaying = true;
        mWidth = parent.getMeasuredWidth();
        mHeight = parent.getMeasuredHeight();
        if (mWidth == 0 || mHeight == 0) {
            return 0;
        }
        final RelativeLayout background = (RelativeLayout) RelativeLayout.inflate(mContext, R.layout.live_big_anim_bomb_layout, null);
        background.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
        parent.addView(background);
        final int ws = View.MeasureSpec.makeMeasureSpec(mWidth, View.MeasureSpec.EXACTLY);
        final int hs = View.MeasureSpec.makeMeasureSpec(mHeight, View.MeasureSpec.EXACTLY);
        background.measure(ws, hs);
        setBackground(background);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                isPlaying = false;
                background.clearAnimation();
                parent.removeView(background);
            }
        }, mDuration);
        return mDuration;
    }

    private void setBackground(ViewGroup background) {
        final ImageView img_bomb1 = background.findViewById(R.id.img_bomb1);
        LiveBigAnimUtils.setAssetImage(img_bomb1, foldPath, defaultBomb);
        final int w = img_bomb1.getMeasuredWidth();
        final int h = img_bomb1.getMeasuredHeight();
        img_bomb1.setScaleX(startScale);
        img_bomb1.setScaleY(startScale);
        final Path path = new Path();
        path.moveTo(mWidth * 3 / 8, 0);
        path.cubicTo(mWidth * 2 / 8, -h / 4, mWidth / 8, -h / 8, 0, 0);
        translateAnim(img_bomb1, background, path, startScale, w, h);
    }

    /**
     * 炸弹进入
     */
    private void translateAnim(final View view, final ViewGroup background, Path path, float scale, final int w, final int h) {
        FloatAnimation anim = new FloatAnimation(background, view, path, scale);
        anim.setInterpolator(new LinearInterpolator());
        anim.setDuration(throwDuration);
        view.startAnimation(anim);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                setBomb(view, background, w, h);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    /**
     * 炸弹变色
     */
    private void setBomb(final View view, final ViewGroup background, int w, int h) {
        final FrameAnimAssetSurfaceView img_bomb2 = background.findViewById(R.id.img_bomb2);
        img_bomb2.setVisibility(View.VISIBLE);
        img_bomb2.setAlpha(0f);
        final RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) img_bomb2.getLayoutParams();
        params.width = w;
        params.height = h;
        //  img_bomb2.setBackgroundResource(R.drawable.bomb_2);
        LiveBigAnimUtils.setAssetBackground(img_bomb2, foldPath, bombRes[0]);
        //  img_bomb2.initView(background, bombRes, rockRate, true);
        img_bomb2.initView(background, foldPath, bombRes, rockRate, true);
        img_bomb2.animate().alphaBy(1).setInterpolator(new LinearInterpolator()).setDuration(alphaDuration).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                view.setVisibility(View.GONE);
                background.removeView(view);
                animBomb(img_bomb2, background);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    /**
     * 炸弹震动
     */
    private void animBomb(final FrameAnimAssetSurfaceView view, final ViewGroup background) {
        final int count = bombRes.length;
        view.start();
        rockBomb(view);
        view.setOnAnimListener(new FrameAnimAssetSurfaceView.AnimListener() {
            @Override
            public void animFinished(ViewGroup parant, SurfaceView surfaceView) {
                parant.removeView(surfaceView);
            }

            @Override
            public void timeFinished(ViewGroup parent, SurfaceView surfaceView, boolean shouldFinished) {

            }

            @Override
            public void playIndex(int index) {
                if (index == count - 3) {
                    addFlower(background);
                }
            }
        });
    }

    /**
     * 添加炸花
     */
    private void addFlower(final ViewGroup background) {
        flowerWave = waveNum;
        final RelativeLayout rel_flower = background.findViewById(R.id.rel_flower);
        rel_flower.post(new Runnable() {
            @Override
            public void run() {
                flowerWave--;
                final int count = mintFlowerCount + mRandom.nextInt(differFlowerCount);
                for (int i = 0; i < count; i++) {
                    addOneFlower(rel_flower);
                }
                if (flowerWave > 0) {
                    rel_flower.postDelayed(this, flowerRate);
                }
            }
        });
    }

    /**
     * 单个炸花
     */
    private void addOneFlower(final ViewGroup background) {
        final ImageView img = new ImageView(mContext);
        background.addView(img);
        final RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams) img.getLayoutParams();
        final int length = flowerRes.length;
        // img.setImageResource(flowerRes[mRandom.nextInt(length)]);
        LiveBigAnimUtils.setAssetImage(img, foldPath, flowerRes[mRandom.nextInt(length)]);
        final int size = minFlowerWidth + mRandom.nextInt(differFlowerWidth);
        param.width = size;
        param.height = size;
        param.addRule(RelativeLayout.CENTER_IN_PARENT);
        final PointF pointF = getPoint(img);
        img.animate().translationX(pointF.x).translationY(pointF.y).setDuration(flowerDuration).setInterpolator(new LinearInterpolator() {
            @Override
            public float getInterpolation(float input) {
                if (input > 0.8) {
                    img.setAlpha(1f - (input - 0.8f) / 0.2f);
                }
                return super.getInterpolation(input);
            }
        }).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        background.removeView(img);
                    }
                });
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    /**
     * 炸花起始点，设置终点
     */
    private PointF getPoint(ImageView img) {
        final float startX = radius - mRandom.nextInt(radius * 2);
        final int sym = mRandom.nextInt(2) > 0 ? -1 : 1;
        final float startY = (float) Math.sqrt(radius * radius - startX * startX) * sym;
        final float mul = minMul + mRandom.nextFloat() * differMul;
        img.setTranslationX(startX);
        img.setTranslationY(startY);
        final float dis = (float) (Math.sqrt(mWidth * mWidth + mHeight * mHeight) / 2);
        return new PointF(dis * startX * mul / radius, dis * startY * mul / radius);
    }

    /**
     * 炸弹震动
     */
    private void rockBomb(View view) {
        final int count = bombRes.length;
        final int dur = rockRate * (count - 4);
        final int num = (int) (count * 2.5);
        final int baseCoord = Utils.dip2px(mContext, 10);
        final int differCoord = 2 * baseCoord;
        final float[] xr = new float[num + 1];
        final float[] yr = new float[num + 1];
        for (int i = 0; i < num; i++) {
            xr[i] = (baseCoord - mRandom.nextFloat() * differCoord);
            yr[i] = (baseCoord - mRandom.nextFloat() * differCoord);
        }
        xr[num] = 0;
        yr[num] = 0;
        AnimatorSet animatorSet = new AnimatorSet();
        ObjectAnimator ax = ObjectAnimator.ofFloat(view, "translationX", xr);
        ObjectAnimator ay = ObjectAnimator.ofFloat(view, "translationY", yr);
        animatorSet.playTogether(ax, ay);
        animatorSet.setDuration(dur);
        animatorSet.setInterpolator(new LinearInterpolator());
        animatorSet.start();
    }

    class FloatAnimation extends Animation {

        private final PathMeasure mPm;
        private final float mDistance;
        private final View mView;
        private final float mscale;

        public FloatAnimation(ViewGroup parent, View view, Path path, float scalse) {
            mPm = new PathMeasure(path, false);
            mDistance = mPm.getLength();
            mView = view;
            parent.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            mscale = scalse;
        }

        @Override
        protected void applyTransformation(float factor, Transformation t) {
            Matrix matrix = t.getMatrix();
            mPm.getMatrix(mDistance * factor, matrix, PathMeasure.POSITION_MATRIX_FLAG);
            mView.setScaleX(mscale + (1 - mscale) * factor);
            mView.setScaleY(mscale + (1 - mscale) * factor);
        }
    }
}
