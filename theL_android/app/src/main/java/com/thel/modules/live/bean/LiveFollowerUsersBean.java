package com.thel.modules.live.bean;

import com.thel.base.BaseDataBean;

import java.io.Serializable;

/**
 * Created by test1 on 2017/4/25.
 * 正在直播的关注用户列表
 */

public class LiveFollowerUsersBean extends BaseDataBean implements Serializable {
    /**
     * 主播ID
     */
    public String userId;
    /**
     * 用户昵称
     */
    public String nickName;
    /**
     * 直播背景
     */
    public String imageUrl;
    /**
     * 观看人数
     */
    public int count;
    /**
     * 用户头像
     */
    public String avatar;

    /**
     * 直播类型
     */
    public int audioType; //视频直播 0 声音直播 1  当type为guest时 1表示连麦中 2表示上麦中

    public String advertTitle;

    public String dumpURL;

    public String messageTo;

    public String type;//live activity guest 嘉宾

    /**
     * 1横屏 0竖屏
     */
    public int isLandscape;

    public String roomId; //直播房间号 只有type为guest时有值

    public String liveId;

    @Override public String toString() {
        return "LiveFollowerUsersBean{" +
                "userId='" + userId + '\'' +
                ", nickName='" + nickName + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", count=" + count +
                ", avatar='" + avatar + '\'' +
                ", audioType=" + audioType +
                ", advertTitle='" + advertTitle + '\'' +
                ", dumpURL='" + dumpURL + '\'' +
                ", messageTo='" + messageTo + '\'' +
                ", type='" + type + '\'' +
                ", isLandscape=" + isLandscape +
                ", roomId='" + roomId + '\'' +
                ", errcode='" + errcode + '\'' +
                ", errdesc='" + errdesc + '\'' +
                ", errdesc_en='" + errdesc_en + '\'' +
                ", result='" + result + '\'' +
                '}';
    }
}
