package com.thel.modules.main.messages.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseAdapter;
import com.thel.constants.TheLConstants;
import com.thel.db.table.message.WinkMsgTable;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.imp.wink.WinkUtils;
import com.thel.manager.ChatServiceManager;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.utils.AnimUtils;
import com.thel.utils.DateUtils;
import com.thel.utils.DialogUtil;
import com.thel.utils.FireBaseUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WinksAdapter extends BaseAdapter<RecyclerView.ViewHolder, WinkMsgTable> {

    private static final int NORMAL_TYPE = 1;

    private static final int TIME_TYPE = 2;

    private boolean isHasBG = false;

    public void isHasBG(boolean isHasBG) {
        this.isHasBG = isHasBG;
    }

    public WinksAdapter(Context context) {
        super(context);
    }

    @NonNull @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TIME_TYPE) {
            View timeView = mLayoutInflater.inflate(R.layout.item_winks_times, parent, false);
            return new WinksAdapter.WinksTimesViewHolder(timeView);
        } else {
            View view = mLayoutInflater.inflate(R.layout.messages_wink_listitem, parent, false);
            return new WinksAdapter.WinksViewHolder(view);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        super.onBindViewHolder(viewHolder, position);

        int viewType = getItemViewType(position);

        final WinkMsgTable winkMsgTable = getData().get(position);

        if (viewType == TIME_TYPE) {
            WinksAdapter.WinksTimesViewHolder holder = (WinksAdapter.WinksTimesViewHolder) viewHolder;

            String formatTime = DateUtils.getFormatTimeFromMillis(winkMsgTable.msgTime, "MM-dd HH:mm");

            holder.txt_day.setText(formatTime);

            if (isHasBG) {
                holder.txt_day.setTextColor(TheLApp.context.getResources().getColor(R.color.chat_time_text_bg));
                holder.txt_day.setBackground(TheLApp.getContext().getResources().getDrawable(R.drawable.bg_border_corner_button_white_shape));
            } else {
                holder.txt_day.setTextColor(TheLApp.context.getResources().getColor(R.color.theme_select_info));
                holder.txt_day.setBackground(null);
            }

            holder.itemView.setOnLongClickListener(v -> {
                showDeleteDialog(winkMsgTable, v.getContext());
                return true;
            });
        } else {
            final WinksAdapter.WinksViewHolder holdView = (WinksAdapter.WinksViewHolder) viewHolder;

            if (!WinkUtils.isTodayWinkedUser(winkMsgTable.userId) && winkMsgTable.isWinked == 1) {
                holdView.wink_iv.setVisibility(View.VISIBLE);
                holdView.wink_anim_view.setVisibility(View.VISIBLE);
            } else {
                holdView.wink_iv.setVisibility(View.INVISIBLE);
                holdView.wink_anim_view.setVisibility(View.INVISIBLE);
            }

            holdView.txt_msg_text_left.setTextColor(TheLApp.getContext().getResources().getColor(R.color.text_color_light_black));

            String text = TheLApp.getContext().getString(R.string.wink_wink_msg_text);

            holdView.txt_msg_text_left.setText(text);

            holdView.txt_name_left.setText(winkMsgTable.userName);

            ImageLoaderManager.imageLoaderCircle(holdView.avatar_iv, R.mipmap.icon_user, winkMsgTable.avatar);

            holdView.avatar_iv.setOnClickListener(v -> {
//                Intent intent = new Intent();
//                intent.setClass(v.getContext(), UserInfoActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, winkMsgTable.userId);
//                TheLApp.getContext().startActivity(intent);
                FlutterRouterConfig.Companion.gotoUserInfo(winkMsgTable.userId);
            });

            holdView.wink_iv.setOnClickListener(v -> {
                winkMsgTable.isWinked = 0;
                ChatServiceManager.getInstance().updateWinkMsg(winkMsgTable);
                AnimUtils.sendWink(holdView.wink_iv, holdView.wink_anim_view, winkMsgTable.userId, winkMsgTable.userName, winkMsgTable.avatar);
                FireBaseUtils.uploadGoogle(TheLConstants.FireBaseConstant.WINK, mContext);

            });

            holdView.itemView.setOnLongClickListener(v -> {
                showDeleteDialog(winkMsgTable, v.getContext());
                return true;
            });

        }

    }

    @Override
    public int getItemViewType(int position) {

        WinkMsgTable winkMsgTable = getData().get(position);
        if (winkMsgTable.isTimeTip == 1) {
            return TIME_TYPE;
        } else {
            return NORMAL_TYPE;
        }

    }


    public class WinksViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.avatar_iv)
        ImageView avatar_iv;
        @BindView(R.id.txt_name_left)
        TextView txt_name_left;
        @BindView(R.id.txt_msg_text_left)
        TextView txt_msg_text_left;
        @BindView(R.id.wink_iv)
        ImageView wink_iv;
        @BindView(R.id.wink_anim_view)
        LottieAnimationView wink_anim_view;

        public WinksViewHolder(View convertView) {
            super(convertView);

            ButterKnife.bind(this, convertView);

        }
    }

    public class WinksTimesViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_day)
        TextView txt_day;

        public WinksTimesViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    protected void showDeleteDialog(final WinkMsgTable winkMsgTable, Context context) {

        Activity activity = (Activity) context;

        String[] selectStrings = new String[]{TheLApp.getContext().getString(R.string.chat_activity_info_delete)};

        DialogUtil.getInstance().showSelectionDialog(activity, selectStrings, (parent, view, position, id) -> {
            DialogUtil.getInstance().closeDialog();
            switch (position) {
                case 0:// 删除
                    deleteItem(winkMsgTable);
                    break;
                default:
                    break;
            }
        }, false, 2, null);

    }

    private void deleteItem(WinkMsgTable winkMsgTable) {

        ChatServiceManager.getInstance().deleteWinkMsg(winkMsgTable);

        removeItem(winkMsgTable);

        int itemCount = getData().size();

        if (itemCount > 0) {

            WinkMsgTable wmt = getData().get(itemCount - 1);

            if (wmt.isTimeTip == 1) {

                removeItem(wmt);

                ChatServiceManager.getInstance().deleteWinkMsg(wmt);
            }

        }

        notifyDataSetChanged();

    }

}
