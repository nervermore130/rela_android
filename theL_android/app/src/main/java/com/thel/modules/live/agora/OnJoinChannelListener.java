package com.thel.modules.live.agora;

import android.view.SurfaceView;

import io.agora.rtc.IRtcEngineEventHandler;

/**
 * @author liuyun
 */
public interface OnJoinChannelListener {

    /**
     * 加入房间成功的回调
     */
    void onJoinChannelSuccess();

    /**
     * 离开房间成功
     */
    void onLeaveChannelSuccess();

    /**
     * 用户加入房间
     *
     * @param uid
     */
    void onUserJoined(int uid, SurfaceView view);

    /**
     * 用户离开房间
     *
     * @param uid
     */
    void onUserOffline(int uid);

    /**
     * 音频信息
     *
     * @param speakers
     * @param totalVolume
     */
    void onAudioVolumeIndication(final IRtcEngineEventHandler.AudioVolumeInfo[] speakers, int totalVolume);

    /**
     * 生网sdk的报错信息
     *
     * @param error
     */
    void onError(int error);

}
