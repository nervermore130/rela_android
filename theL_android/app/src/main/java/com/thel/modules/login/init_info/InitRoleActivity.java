package com.thel.modules.login.init_info;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.constants.TheLConstants;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 初始化选择角色的页面
 * Created by lingwei on 2017/12/5.
 */

public class InitRoleActivity extends BaseActivity {
    @BindView(R.id.lin_back)
    LinearLayout linBack;
    @BindView(R.id.live_role_recycler)
    RecyclerView liveRoleRecycler;
    private List<String> role = new ArrayList<>();
    private initRoleAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_init_role);
        ButterKnife.bind(this);
        initData();
        setListener();

    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void initData() {
        String[] role_Array = this.getResources().getStringArray(R.array.userinfo_role);
        role.clear();
        role.addAll(Arrays.asList(role_Array));

        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        liveRoleRecycler.setLayoutManager(manager);
        liveRoleRecycler.setHasFixedSize(true);
        adapter = new initRoleAdapter(role);
        liveRoleRecycler.setAdapter(adapter);
    }

    private void setListener() {
        adapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                String selectRole = adapter.getData().get(position);
                Intent intent = new Intent();
                intent.putExtra("role", selectRole);
                intent.putExtra("position", position);
                setResult(TheLConstants.RESULT_CODE_SELECT_TYPE, intent);
                finish();
            }
        });
        linBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
