package com.thel.modules.main.home.moments;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.bean.ContactBean;
import com.thel.bean.FirstHotTopicBean;
import com.thel.bean.MentionedUserBean;
import com.thel.bean.ReleaseMomentListBean;
import com.thel.bean.ReleasedCommentBeanV2New;
import com.thel.bean.comment.CommentBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.recommend.RecommendWebBean;
import com.thel.bean.theme.ThemeCommentBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.modules.label.LabelActivity;
import com.thel.modules.live.bean.LiveRoomBean;
import com.thel.modules.select_contacts.SelectContactsActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.RequestConstants;
import com.thel.network.service.DefaultRequestService;
import com.thel.service.ReleaseMomentsService;
import com.thel.utils.DialogUtil;
import com.thel.utils.GsonUtils;
import com.thel.utils.ImageUtils;
import com.thel.utils.L;
import com.thel.utils.MD5Utils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.ViewUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by waiarl on 2017/6/26.
 * 推荐日志后编辑的类
 */

public class WriteRecommendMomentActivity extends BaseActivity {
    private View img_back;
    private View txt_title;
    private TextView img_done;
    private View rel_pic;
    private SimpleDraweeView img_pic;
    private ImageView img_play;
    private TextView txt_content;
    private View lin_music_des;
    private TextView txt_music_name;
    private TextView txt_music_album;
    private TextView txt_music_artist;
    private EditText edit_content;
    private RelativeLayout add_topic;
    private TextView add_topic_txt;
    private RelativeLayout shareToContent;
    private ImageView shareToLogo;
    private TextView shareToType;
    private RelativeLayout mention_content;
    private SimpleDraweeView wink_comment1_portrait;
    private SimpleDraweeView wink_comment2_portrait;
    private SimpleDraweeView wink_comment3_portrait;
    private SimpleDraweeView wink_comment4_portrait;
    private SimpleDraweeView wink_comment5_portrait;
    private SimpleDraweeView wink_comment6_portrait;

    private MomentsBean oldMomentBean;


    private Handler mHandler = new Handler(Looper.getMainLooper());

    private int shareToPostion = 0;

    private List<String> shareToList = new ArrayList<>();
    private List<String> shareToTipsList = new ArrayList<>();
    private ArrayList<ContactBean> mentionList = new ArrayList<>();
    private DialogUtil dialogUtils;

    private String choosedTag = "";
    private TextWatcher textWatcher;


    public static final int FROM_PAGE_MOMENT = 0;//推荐日志
    public static final int FROM_PAGE_WEBVIEW = 1;//推荐网页
    public static final int FROM_PAGE_LIVEUSER = 3;//推荐主播
    private int FROM_PAGE = FROM_PAGE_MOMENT;
    private RecommendWebBean recommendWebBean;
    private LiveRoomBean liveuserbean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout();
        getIntentData();
        findViewById();
        initData();
        initView();
        setListener();
        getFirstHotTopic();
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void initData() {
        String[] shareToArr = this.getResources().getStringArray(R.array.moments_share_to);
        String[] shareToTipsArr = this.getResources().getStringArray(R.array.moments_share_to_tips);
        shareToList = new ArrayList<>(Arrays.asList(shareToArr));
        shareToTipsList = new ArrayList<>(Arrays.asList(shareToTipsArr));
        shareToType.setText(shareToList.get(0));

        dialogUtils = DialogUtil.getInstance();
    }

    protected void setLayout() {
        setContentView(R.layout.activity_write_recommand_moment);
    }

    protected void findViewById() {
        img_back = findViewById(R.id.img_back);
        txt_title = findViewById(R.id.txt_title);
        img_done = findViewById(R.id.img_done);
        rel_pic = findViewById(R.id.rel_pic);
        img_pic = findViewById(R.id.img_pic);
        img_play = findViewById(R.id.img_play);
        txt_content = findViewById(R.id.txt_content);
        lin_music_des = findViewById(R.id.lin_music_des);
        txt_music_name = findViewById(R.id.txt_music_name);
        txt_music_album = findViewById(R.id.txt_music_album);
        txt_music_artist = findViewById(R.id.txt_music_artist);
        edit_content = findViewById(R.id.edit_content);

        add_topic = findViewById(R.id.add_topic);
        add_topic_txt = findViewById(R.id.add_topic_txt);
        shareToContent = findViewById(R.id.share_to_content);
        shareToLogo = findViewById(R.id.share_to_logo);
        shareToType = findViewById(R.id.share_to_txt);
        mention_content = findViewById(R.id.mention_content);
        wink_comment1_portrait = this.findViewById(R.id.wink_comment1_portrait);
        wink_comment2_portrait = this.findViewById(R.id.wink_comment2_portrait);
        wink_comment3_portrait = this.findViewById(R.id.wink_comment3_portrait);
        wink_comment4_portrait = this.findViewById(R.id.wink_comment4_portrait);
        wink_comment5_portrait = this.findViewById(R.id.wink_comment5_portrait);
        wink_comment6_portrait = this.findViewById(R.id.wink_comment6_portrait);

        mention_content.setVisibility(View.VISIBLE);
    }

    private void getIntentData() {
        final Intent intent = getIntent();
        if (intent == null) {
            return;
        }
        FROM_PAGE = intent.getIntExtra(TheLConstants.BUNDLE_KEY_INTENT_FROM, FROM_PAGE_MOMENT);
        if (FROM_PAGE_MOMENT == FROM_PAGE) {
            oldMomentBean = (MomentsBean) intent.getSerializableExtra(TheLConstants.BUNDLE_KEY_MOMENT_BEAN);
        } else if (FROM_PAGE_LIVEUSER == FROM_PAGE) {
            liveuserbean = (LiveRoomBean) intent.getSerializableExtra(TheLConstants.BUNDLE_KEY_LIVEUSER_BEAN);
        } else if (FROM_PAGE_WEBVIEW == FROM_PAGE) {
            recommendWebBean = (RecommendWebBean) intent.getSerializableExtra(TheLConstants.BUNDLE_KEY_RECOMMEND_WEB_BEAN);
        }
    }

    protected void setListener() {
        add_topic.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
//                startActivityForResult(new Intent(WriteRecommendMomentActivity.this, LabelActivity.class), TheLConstants.BUNDLE_CODE_WRITE_MOMENT_ACTIVITY);
                FlutterRouterConfig.Companion.gotoTagList(new ArrayList<>());
            }
        });

        mention_content.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WriteRecommendMomentActivity.this, SelectContactsActivity.class);
                Collections.reverse(mentionList);
                intent.putExtra(TheLConstants.BUNDLE_KEY_FRIENDS_LIST, mentionList);
                startActivityForResult(intent, TheLConstants.BUNDLE_CODE_WRITE_MOMENT_ACTIVITY);
            }
        });
        shareToContent.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                mHandler.post(new Runnable() {

                    @Override
                    public void run() {
                        final Integer[] arr = new Integer[]{R.mipmap.icn_feed_share_all,
                                R.mipmap.icn_feed_share_friends,
                                R.mipmap.icn_feed_share_private,
                                R.mipmap.icn_feed_share_anonymous};
                        dialogUtils.showSingleChoiseDialog(WriteRecommendMomentActivity.this, "", shareToList, arr, shareToTipsList, shareToPostion, new AdapterView.OnItemClickListener() {

                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                dialogUtils.closeDialog();
                                shareToPostion = (Integer) view.getTag();
                                shareToType.setText(shareToList.get(shareToPostion));
                                shareToLogo.setImageResource(arr[shareToPostion]);
                                if (shareToPostion == 2) {//当自由自己的时候，是不能提及的
                                    mentionList.clear();
                                    refreshMentionContent();
                                    mention_content.setVisibility(View.GONE);
                                } else {
                                    mention_content.setVisibility(View.VISIBLE);
                                }
                            }
                        });
                    }
                });
            }
        });
        textWatcher = new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String str = s.toString().trim();
                if (str.length() >= getResources().getInteger(R.integer.moment_word_count_limit)) {
                    DialogUtil.showToastShort(WriteRecommendMomentActivity.this, getString(R.string.updatauserinfo_activity_intro_hint2, getResources().getInteger(R.integer.moment_word_count_limit)));
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String str = s.toString();
                s.setSpan(new ForegroundColorSpan(Color.BLACK), 0, str.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                if (!TextUtils.isEmpty(choosedTag) && s.toString().contains(choosedTag)) {
                    s.setSpan(new ForegroundColorSpan(ContextCompat.getColor(TheLApp.getContext(), R.color.tag_color)), str.indexOf(choosedTag), str.indexOf(choosedTag) + choosedTag.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                }
//                if (str.length() >= getResources().getInteger(R.integer.moment_word_count_limit) || str.trim().length() == 0) {
//                    setDoneButtonEnabled(false);
//                } else {
//                    setDoneButtonEnabled(true);
//                }
            }
        };
        edit_content.addTextChangedListener(textWatcher);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                ViewUtils.hideSoftInput(WriteRecommendMomentActivity.this, edit_content);

                close();
            }
        });
        img_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                releaseMoment();
            }
        });

    }

    /**
     * 发送日志
     */
    private void releaseMoment() {
        if (FROM_PAGE_MOMENT == FROM_PAGE) {

            if (!TextUtils.isEmpty(edit_content.getText().toString().trim())) {
                //4.7.3推荐日志时，将推荐文字作为日志的一条评论
                Intent intent = new Intent();
                intent.setAction(TheLConstants.BROADCAST_AUTO_RELEASE_NEW_RECOMMEND_COMMENT_ACTION);
                intent.putExtra(RequestConstants.I_TEXT, edit_content.getText().toString().trim());
                intent.putExtra(RequestConstants.I_TYPE, CommentBean.COMMENT_TYPE_TEXT);
                sendBroadcast(intent);
                releaseComment(edit_content.getText().toString().trim());
            }
            releaseRecommendMoment();//推荐日志

        } else if (FROM_PAGE_WEBVIEW == FROM_PAGE) {
            releaserWebMoment();
        } else if (FROM_PAGE_LIVEUSER == FROM_PAGE) {
            releaserLiveUserMoment();
        } else {
            releaseRecommendMoment();
        }
    }

    private void releaseComment(String releaseText) {
        Map<String, String> map = new HashMap<>();
        map.put(RequestConstants.I_TEXT, releaseText);
        map.put(RequestConstants.I_TYPE, CommentBean.COMMENT_TYPE_TEXT);
        map.put(RequestConstants.I_TO_USER_ID, "");
        map.put(RequestConstants.I_MOMENT_ID, oldMomentBean.momentsId);

        Flowable<ReleasedCommentBeanV2New> flowable = DefaultRequestService.createMomentRequestService().replyMoment(MD5Utils.generateSignatureForMap(map));
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<ReleasedCommentBeanV2New>() {
            @Override
            public void onNext(ReleasedCommentBeanV2New data) {
                super.onNext(data);
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
            }
        });
    }

    /**
     * 推荐主播
     */
    private void releaserLiveUserMoment() {
        if (liveuserbean == null) {
            return;
        }
        final String mUserId = ShareFileUtils.getString(ShareFileUtils.ID, "0");
        MomentsBean sendMoment = new MomentsBean();
        sendMoment.userId = (int) liveuserbean.user.id;
        sendMoment.nickname = ShareFileUtils.getString(ShareFileUtils.USER_NAME, "");
        sendMoment.releaseTime = System.currentTimeMillis();
        sendMoment.avatar = ShareFileUtils.getString(ShareFileUtils.AVATAR, "");
        sendMoment.momentsId = System.currentTimeMillis() + MomentsBean.SEND_MOMENT_FLAG;// 预发送的日志id标识
        sendMoment.momentsText = edit_content.getText().toString().trim();
        sendMoment.momentsType = MomentsBean.MOMENT_TYPE_LIVE_USER;
        sendMoment.shareTo = shareToPostion == 3 ? 1 : shareToPostion + 1;
        sendMoment.secret = shareToPostion == 3 ? MomentsBean.IS_SECRET : MomentsBean.IS_NOT_SECRET;

        // 封装提及用户
        if (!mentionList.isEmpty()) {
            Collections.reverse(mentionList);
            for (ContactBean contact : mentionList) {
                MentionedUserBean atUser = new MentionedUserBean();
                atUser.userId = Integer.valueOf(contact.userId);
                atUser.nickname = contact.nickName;
                atUser.momentsId = sendMoment.releaseTime + "";
                atUser.avatar = contact.avatar;
                sendMoment.atUserList.add(atUser);
            }
        }
        sendMoment.parentId = "";
        startReleaseMoment(sendMoment);
    }


    /**
     * 推荐网页
     */
    private void releaserWebMoment() {
        if (recommendWebBean == null) {
            return;
        }
        recommendWebBean.momentsText = edit_content.getText().toString().trim();
        MomentsBean sendMoment = new MomentsBean();
        sendMoment.userId = Integer.valueOf(ShareFileUtils.getString(ShareFileUtils.ID, "0"));
        sendMoment.nickname = ShareFileUtils.getString(ShareFileUtils.USER_NAME, "");
        sendMoment.releaseTime = System.currentTimeMillis();
        sendMoment.avatar = ShareFileUtils.getString(ShareFileUtils.AVATAR, "");
        sendMoment.momentsId = System.currentTimeMillis() + MomentsBean.SEND_MOMENT_FLAG;// 预发送的日志id标识
        sendMoment.momentsText = recommendWebBean.beJson().toString();
        sendMoment.momentsType = MomentsBean.MOMENT_TYPE_WEB;
        sendMoment.shareTo = shareToPostion == 3 ? 1 : shareToPostion + 1;
        sendMoment.secret = shareToPostion == 3 ? MomentsBean.IS_SECRET : MomentsBean.IS_NOT_SECRET;

        // 封装提及用户
        if (!mentionList.isEmpty()) {
            Collections.reverse(mentionList);
            for (ContactBean contact : mentionList) {
                MentionedUserBean atUser = new MentionedUserBean();
                atUser.userId = Integer.valueOf(contact.userId);
                atUser.nickname = contact.nickName;
                atUser.momentsId = sendMoment.releaseTime + "";
                atUser.avatar = contact.avatar;
                sendMoment.atUserList.add(atUser);
            }
        }
        sendMoment.parentId = "";
        startReleaseMoment(sendMoment);
    }

    /**
     * 推荐日志
     */
    private void releaseRecommendMoment() {
        MomentsBean sendMoment = new MomentsBean();
        sendMoment.userId = Integer.valueOf(ShareFileUtils.getString(ShareFileUtils.ID, "0"));
        sendMoment.nickname = ShareFileUtils.getString(ShareFileUtils.USER_NAME, "");
        sendMoment.releaseTime = System.currentTimeMillis();
        sendMoment.avatar = ShareFileUtils.getString(ShareFileUtils.AVATAR, "");
        sendMoment.momentsId = System.currentTimeMillis() + MomentsBean.SEND_MOMENT_FLAG;// 预发送的日志id标识
        sendMoment.momentsText = edit_content.getText().toString().trim();
        sendMoment.momentsType = MomentsBean.MOMENT_TYPE_RECOMMEND;
        sendMoment.shareTo = shareToPostion == 3 ? 1 : shareToPostion + 1;
        sendMoment.secret = shareToPostion == 3 ? MomentsBean.IS_SECRET : MomentsBean.IS_NOT_SECRET;

        // 封装提及用户
        if (!mentionList.isEmpty()) {
            Collections.reverse(mentionList);
            for (ContactBean contact : mentionList) {
                MentionedUserBean atUser = new MentionedUserBean();
                atUser.userId = Integer.valueOf(contact.userId);
                atUser.nickname = contact.nickName;
                atUser.momentsId = sendMoment.releaseTime + "";
                atUser.avatar = contact.avatar;
                sendMoment.atUserList.add(atUser);
            }
        }
        sendMoment.parentId = oldMomentBean.momentsId;
        startReleaseMoment(sendMoment);
    }

    private void startReleaseMoment(MomentsBean sendMoment) {
        if (sendMoment == null) {
            return;
        }
        //将发布内容以json的形式保存本地
        String releaseContent = ShareFileUtils.getString(TheLConstants.RELEASE_CONTENT_KEY, "");
        String toJson;
        int releaseIndex;
        if (TextUtils.isEmpty(releaseContent)) {//之前没有未发布内容
            List<MomentsBean> list = new ArrayList<>();
            list.add(sendMoment);
            ReleaseMomentListBean releaseTagListBean = new ReleaseMomentListBean();
            releaseTagListBean.list = list;
            toJson = GsonUtils.createJsonString(releaseTagListBean);
            releaseIndex = 0;
        } else {//有未发布内容
            ReleaseMomentListBean releaseTagListBean = GsonUtils.getObject(releaseContent, ReleaseMomentListBean.class);
            if (releaseTagListBean == null) {
                releaseTagListBean = new ReleaseMomentListBean();
            }
            releaseTagListBean.list.add(sendMoment);
            toJson = GsonUtils.createJsonString(releaseTagListBean);
            releaseIndex = releaseTagListBean.list.size() - 1;
        }
        ShareFileUtils.setString(TheLConstants.RELEASE_CONTENT_KEY, toJson);
        L.d("release", toJson);
        Intent intent = new Intent(this, ReleaseMomentsService.class);
        // 要发的日志标识，用发布时间作为标识
        intent.putExtra(ReleaseMomentsService.MOMENT_INDEX, releaseIndex);
        TheLApp.getContext().startService(intent);

        setResult(TheLConstants.RESULT_CODE_RECOMMEND_TO_MOMENT_SUCCESS);
        ViewUtils.hideSoftInput(WriteRecommendMomentActivity.this, edit_content);
        finish();
    }

    private void close() {
        if (!TextUtils.isEmpty(edit_content.getText().toString().trim())) {
            DialogUtil.showConfirmDialog(WriteRecommendMomentActivity.this, null, getString(R.string.moments_write_moment_discard_tip), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    finish();
                }
            });
        } else {
            finish();
        }
    }


  /*  private void setDoneButtonEnabled(boolean enabled) {
        img_done.setEnabled(enabled);
        img_done.setImageResource(enabled ? R.mipmap.btn_done : R.mipmap.btn_done_disable);
    }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == TheLConstants.RESULT_CODE_WRITE_MOMENT_SELECT_CONTACT) {// 选择@联系人
            if (data.getSerializableExtra(TheLConstants.BUNDLE_KEY_FRIENDS_LIST) != null) {
                mentionList.clear();
                mentionList.addAll((ArrayList<ContactBean>) data.getSerializableExtra(TheLConstants.BUNDLE_KEY_FRIENDS_LIST));
                Collections.reverse(mentionList);
                refreshMentionContent();
            }
        } else if (resultCode == TheLConstants.RESULT_CODE_WRITE_MOMENT_SELECT_TOPIC) {// 选择话题
            String lastChoosedTag = choosedTag;
            choosedTag = data.getStringExtra(TheLConstants.BUNDLE_KEY_TOPIC_NAME);
            if (!TextUtils.isEmpty(choosedTag)) {
                SpannableString sp = new SpannableString(choosedTag);
                sp.setSpan(new ForegroundColorSpan(ContextCompat.getColor(WriteRecommendMomentActivity.this, R.color.tag_color)), 0, sp.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                int index = edit_content.getSelectionStart();// 获取光标所在位置
                Editable edit = edit_content.getEditableText();// 获取EditText的文字
                // 先删除之前已经选择过的标签
                if (!TextUtils.isEmpty(lastChoosedTag) && edit.toString().contains(lastChoosedTag)) {//如果输入框内的文字包含之前选择的话题，则替换第一个
                    edit.replace(edit.toString().indexOf(lastChoosedTag), edit.toString().indexOf(lastChoosedTag) + lastChoosedTag.length(), sp);
                } else {
                    if (index < 0 || index >= edit.length()) {
                        edit.append(sp);
                    } else {
                        edit.insert(index, sp);// 光标所在位置插入文字
                    }
                }
            }
        }
    }

    private void refreshMentionContent() {
        hidePortraits();
        if (mentionList.size() > 0) {
            wink_comment1_portrait.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(mentionList.get(0).avatar, TheLConstants.AVATAR_SMALL_SIZE, TheLConstants.AVATAR_SMALL_SIZE)));
            wink_comment1_portrait.setTag(mentionList.get(0));
            wink_comment1_portrait.setVisibility(View.VISIBLE);
            if (mentionList.size() > 1) {
                wink_comment2_portrait.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(mentionList.get(1).avatar, TheLConstants.AVATAR_SMALL_SIZE, TheLConstants.AVATAR_SMALL_SIZE)));
                wink_comment2_portrait.setTag(mentionList.get(1));
                wink_comment2_portrait.setVisibility(View.VISIBLE);
                if (mentionList.size() > 2) {
                    wink_comment3_portrait.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(mentionList.get(2).avatar, TheLConstants.AVATAR_SMALL_SIZE, TheLConstants.AVATAR_SMALL_SIZE)));
                    wink_comment3_portrait.setTag(mentionList.get(2));
                    wink_comment3_portrait.setVisibility(View.VISIBLE);
                    if (mentionList.size() > 3) {
                        wink_comment4_portrait.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(mentionList.get(3).avatar, TheLConstants.AVATAR_SMALL_SIZE, TheLConstants.AVATAR_SMALL_SIZE)));
                        wink_comment4_portrait.setTag(mentionList.get(3));
                        wink_comment4_portrait.setVisibility(View.VISIBLE);
                        if (mentionList.size() > 4) {
                            wink_comment5_portrait.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(mentionList.get(4).avatar, TheLConstants.AVATAR_SMALL_SIZE, TheLConstants.AVATAR_SMALL_SIZE)));
                            wink_comment5_portrait.setTag(mentionList.get(4));
                            wink_comment5_portrait.setVisibility(View.VISIBLE);
                            if (mentionList.size() > 5) {
                                wink_comment6_portrait.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(mentionList.get(5).avatar, TheLConstants.AVATAR_SMALL_SIZE, TheLConstants.AVATAR_SMALL_SIZE)));
                                wink_comment6_portrait.setTag(mentionList.get(5));
                                wink_comment6_portrait.setVisibility(View.VISIBLE);
                            }
                        }

                    }

                }

            }
        }
    }

    private void hidePortraits() {
        wink_comment1_portrait.setVisibility(View.INVISIBLE);
        wink_comment2_portrait.setVisibility(View.INVISIBLE);
        wink_comment3_portrait.setVisibility(View.INVISIBLE);
        wink_comment4_portrait.setVisibility(View.INVISIBLE);
        wink_comment5_portrait.setVisibility(View.INVISIBLE);
        wink_comment6_portrait.setVisibility(View.INVISIBLE);
    }

    /***********************************************************************************************************/
    //
    private void initView() {
        rel_pic.setVisibility(View.GONE);
        img_play.setVisibility(View.GONE);
        lin_music_des.setVisibility(View.GONE);
        if (FROM_PAGE_MOMENT == FROM_PAGE) {
            initMomentBeanView();
        } else if (FROM_PAGE_LIVEUSER == FROM_PAGE) {
            initLiveUserView();
        } else if (FROM_PAGE_WEBVIEW == FROM_PAGE) {
            initRecommendWebBeanView();
        }
    }

    private void initLiveUserView() {
        setLiveUser(liveuserbean);
    }

    /**
     * 推荐网页
     */
    private void initRecommendWebBeanView() {
        if (recommendWebBean == null) {
            return;
        }
        rel_pic.setVisibility(View.VISIBLE);
        if (!TextUtils.isEmpty(recommendWebBean.imageUrl)) {
            setImage(recommendWebBean.imageUrl, img_pic);
        } else {
            img_pic.setImageResource(R.drawable.ic_launcher);
        }
        if (!TextUtils.isEmpty(recommendWebBean.title)) {
            txt_content.setText(recommendWebBean.title);
        } else {
            txt_content.setText(recommendWebBean.url);
        }


    }

    /**
     * 推荐日志
     */
    private void initMomentBeanView() {
        if (oldMomentBean == null) {
            return;
        }
        if (MomentsBean.MOMENT_TYPE_TEXT.equals(oldMomentBean.momentsType) || MomentsBean.MOMENT_TYPE_RECOMMEND.equals(oldMomentBean.momentsType)) {//纯文本（推荐日志也是纯文本日志）
            txt_content.setText(oldMomentBean.momentsText);
        } else if (MomentsBean.MOMENT_TYPE_IMAGE.equals(oldMomentBean.momentsType)) {//图片日志
            setPicView(false);
        } else if (MomentsBean.MOMENT_TYPE_TEXT_IMAGE.equals(oldMomentBean.momentsType)) {//带文本的图片日志
            setPicView(true);
        } else if (MomentsBean.MOMENT_TYPE_LIVE.equals(oldMomentBean.momentsType)) {//直播日志
            setPicView(true);
            setPlayImage(R.mipmap.btn_live_play);
        } else if (MomentsBean.MOMENT_TYPE_THEME.equals(oldMomentBean.momentsType)) {//话题日志
            setThemeView();
        } else if (MomentsBean.MOMENT_TYPE_THEME_REPLY.equals(oldMomentBean.momentsType)) {//话题回复日志
            if (TextUtils.isEmpty(getImageUrl(oldMomentBean))) {//如果不带图片
                txt_content.setText(oldMomentBean.momentsText);
            } else {
                setPicView(!TextUtils.isEmpty(oldMomentBean.momentsText));
                if (ThemeCommentBean.TYPE_CLASS_VIDEO.equals(oldMomentBean.themeReplyClass)) {
                    setPlayImage(R.mipmap.btn_play_video);
                } else if (ThemeCommentBean.TYPE_CLASS_VOICE.equals(oldMomentBean.themeReplyClass)) {
                    setPlayImage(R.mipmap.btn_feed_play);
                }
            }
        } else if (MomentsBean.MOMENT_TYPE_VIDEO.equals(oldMomentBean.momentsType)) {//视频日志
            setPicView(!TextUtils.isEmpty(oldMomentBean.momentsText));
            setPlayImage(R.mipmap.btn_play_video);
        } else if (MomentsBean.MOMENT_TYPE_USER_CARD.equals(oldMomentBean.momentsType)) {//推荐用户名片
            txt_content.setText(oldMomentBean.momentsText);
            if (TextUtils.isEmpty(oldMomentBean.imageUrl)) {//如果被推荐名片没有背景
                img_pic.setImageResource(R.color.tab_normal);
            } else {
                setImage(oldMomentBean.imageUrl, img_pic);
            }
        } else if (MomentsBean.MOMENT_TYPE_VOICE.equals(oldMomentBean.momentsType)) {//纯音乐日志
            setMusicView();
        } else if (MomentsBean.MOMENT_TYPE_TEXT_VOICE.equals(oldMomentBean.momentsType)) {//带文字的音乐日志
            // setPicView(true);
            setMusicView(true);
            //img_play.setVisibility(View.VISIBLE);
        } else if (MomentsBean.MOMENT_TYPE_WEB.equals(oldMomentBean.momentsType)) {//如果是网页日志
            try {
                final JSONObject obj = new JSONObject(oldMomentBean.momentsText);
                RecommendWebBean bean = new RecommendWebBean();
                bean.fromJson(obj);
                txt_content.setText(bean.momentsText);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else if (MomentsBean.MOMENT_TYPE_USER_CARD.equals(oldMomentBean.momentsType)) {//如果是推荐主播
            txt_content.setText(oldMomentBean.momentsText);
        } else {//默认当普通文本类型
            txt_content.setText(oldMomentBean.momentsText);
        }
    }

    private void setPlayImage(@NonNull int res) {
        img_play.setVisibility(View.VISIBLE);
        img_play.setImageResource(res);

    }

    private void setLiveUser(LiveRoomBean liveuserbean) {
        rel_pic.setVisibility(View.VISIBLE);
        String avatar = liveuserbean.user.avatar;
        if (!TextUtils.isEmpty(avatar)) {
            img_pic.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(avatar, TheLConstants.ICON_MIDDLE_SIZE, TheLConstants.ICON_MIDDLE_SIZE))).build()).setAutoPlayAnimations(true).build());
        }
        txt_content.setText(liveuserbean.user.nickName + " " + getString(R.string.is_on_live) + " " + liveuserbean.text);
    }

    /**
     * 音乐
     */
    private void setMusicView() {
        //setPicView(false);
        setMusicView(false);
        img_play.setVisibility(View.VISIBLE);
        img_play.setImageResource(R.mipmap.btn_feed_play);
        lin_music_des.setVisibility(View.VISIBLE);
        txt_music_name.setText(oldMomentBean.songName);
        txt_music_album.setText(TheLApp.getContext().getString(R.string.moments_album) + oldMomentBean.albumName);
        txt_music_artist.setText(TheLApp.getContext().getString(R.string.moments_artist) + oldMomentBean.artistName);
    }

    /**
     * 话题
     */
    private void setThemeView() {
        rel_pic.setVisibility(View.VISIBLE);
        txt_content.setText(oldMomentBean.momentsText);
        if (TextUtils.isEmpty(oldMomentBean.imageUrl)) {//没背景图片的话题
            setImage(TheLConstants.RES_PIC_URL + R.mipmap.bg_topic_default, img_pic);
        } else {
            setImage(oldMomentBean.imageUrl, img_pic);
        }
    }

    /**
     * 带图片的日志
     */
    private void setPicView(boolean hasText) {
        rel_pic.setVisibility(View.VISIBLE);
        setImage(getImageUrl(oldMomentBean), img_pic);
        if (hasText) {
            txt_content.setText(oldMomentBean.momentsText);
        } else {
            txt_content.setVisibility(View.GONE);
        }
    }

    /**
     * 带音乐图片的日志
     */
    private void setMusicView(boolean hasText) {
        rel_pic.setVisibility(View.VISIBLE);
        //  setImage(getImageUrl(oldMomentBean), img_pic);
        setMusicImage(getMusicImageUrl(oldMomentBean), img_pic);
        if (hasText) {
            txt_content.setText(oldMomentBean.momentsText);
        } else {
            txt_content.setVisibility(View.GONE);
        }
    }

    /**
     * 设置图片
     */
    private void setImage(String imageUrl, SimpleDraweeView img_pic) {
        if (!TextUtils.isEmpty(imageUrl)) {
            img_pic.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(imageUrl, TheLConstants.ICON_MIDDLE_SIZE, TheLConstants.ICON_MIDDLE_SIZE))).build()).setAutoPlayAnimations(true).build());
        }
    }

    /***
     * 設置音樂圖片
     */
    private void setMusicImage(String imageUrl, SimpleDraweeView img_pic) {
        if (!TextUtils.isEmpty(imageUrl)) {
            img_pic.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(imageUrl, TheLConstants.ICON_MIDDLE_SIZE, TheLConstants.ICON_MIDDLE_SIZE))).build()).setAutoPlayAnimations(true).build());
        }
    }

    /**
     * 获取第一张缩略图
     */
    private String getImageUrl(MomentsBean bean) {
        if (TextUtils.isEmpty(bean.thumbnailUrl)) {
            return "";
        }
        String[] picUrls = bean.thumbnailUrl.split(",");
        if (picUrls.length > 0) {
            return picUrls[0];
        }
        return "";
    }

    /***
     * 获取音乐日志缩略图
     */
    private String getMusicImageUrl(MomentsBean bean) {
        String picUril = bean.albumLogo444;
        if (!TextUtils.isEmpty(picUril)) {
            return picUril;
        }
        return "";
    }

    @Override
    public void onBackPressed() {
        close();
        super.onBackPressed();
    }

    private void getFirstHotTopic() {

        RequestBusiness
                .getInstance()
                .getFirstHotTopic()
                .onBackpressureDrop()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new InterceptorSubscribe<FirstHotTopicBean>() {
                    @Override
                    public void onNext(FirstHotTopicBean baseDataBean) {
                        super.onNext(baseDataBean);
                        if (baseDataBean != null && !TextUtils.isEmpty(baseDataBean.topicName))
                            add_topic_txt.setText(baseDataBean.topicName);
                    }
                });
    }

}
