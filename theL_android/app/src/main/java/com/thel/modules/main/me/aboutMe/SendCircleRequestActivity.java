package com.thel.modules.main.me.aboutMe;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.bean.ContactBean;
import com.thel.bean.me.MyCircleActivity;
import com.thel.bean.me.MyCircleRequestListBean;
import com.thel.constants.TheLConstants;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.RequestConstants;
import com.thel.utils.AppInit;
import com.thel.utils.DateUtils;
import com.thel.utils.DialogUtil;
import com.thel.utils.ImageUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SharedPrefUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

import org.reactivestreams.Subscription;

import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import tourguide.tourguide.Overlay;
import tourguide.tourguide.Pointer;
import tourguide.tourguide.ToolTip;
import tourguide.tourguide.TourGuide;

import static com.thel.R.id.edt_say_something;
import static com.thel.R.id.img_avatar_left;
import static com.thel.R.id.img_background;
import static com.thel.R.id.txt_date;

/**
 * 发送密友请求页面
 * Created by lingwei on 2017/10/18.
 */

public class SendCircleRequestActivity extends BaseActivity {
    @BindView(img_background)
    SimpleDraweeView imgBackground;
    @BindView(R.id.mask)
    TextView mask;
    @BindView(R.id.img_avatar_right)
    SimpleDraweeView imgAvatarRight;
    @BindView(R.id.avatar_area_right)
    RelativeLayout avatarAreaRight;
    @BindView(img_avatar_left)
    SimpleDraweeView imgAvatarLeft;
    @BindView(R.id.avatar_area_left)
    RelativeLayout avatarAreaLeft;

    @BindView(txt_date)
    TextView txtDate;
    @BindView(R.id.arrow)
    ImageView arrow;
    @BindView(R.id.rel_anniversary)
    RelativeLayout relAnniversary;
    @BindView(edt_say_something)
    EditText edtSaySomething;
    @BindView(R.id.txt_limit)
    TextView txtLimit;
    @BindView(R.id.btn_send)
    Button btnSend;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.lin_back)
    LinearLayout linBack;
    @BindView(R.id.txt_title)
    TextView txtTitle;
    @BindView(R.id.img_more)
    ImageView imgMore;
    @BindView(R.id.lin_more)
    LinearLayout linMore;
    @BindView(R.id.title_layout)
    RelativeLayout titleLayout;
    private String requesType;
    private TourGuide mTourGuide;
    private ContactBean contact;
    private DialogUtil dialogUtils;
    private Calendar anniversaryCal = Calendar.getInstance();
    private TextWatcher textWatcher;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.send_circle_request_layout);
        ButterKnife.bind(this);
        dialogUtils = new DialogUtil();

        initIntent();
        initUi();
        setListener();
    }

    private void setListener() {
        textWatcher = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                txtLimit.setText((100 - s.length()) + "");
                if (s.length() >= 100) {
                    DialogUtil.showToastShort(SendCircleRequestActivity.this, String.format(getString(R.string.info_words_length_limit), 100));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
        edtSaySomething.addTextChangedListener(textWatcher);
    }

    private void initUi() {
        imgAvatarRight.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(ShareFileUtils.getString(ShareFileUtils.AVATAR, ""), TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE)));
        imgAvatarLeft.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(contact.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE)));
        imgBackground.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(contact.bgImage, AppInit.displayMetrics.widthPixels, getResources().getDimension(R.dimen.bg_height))));
        edtSaySomething.setHint(String.format(getString(R.string.send_circle_request_act_hint), contact.nickName));
        txtDate.setText(1 + getString(R.string.my_circle_activity_day_odd));
    }

    private void initIntent() {
        Intent intent = getIntent();
        requesType = intent.getStringExtra(RequestConstants.I_REQUEST_TYPE);
        if (requesType.equals("0")) {
            txtTitle.setText(getString(R.string.send_circle_request_act_title_partner));
            relAnniversary.setVisibility(View.VISIBLE);
            if (!SharedPrefUtils.getBoolean(SharedPrefUtils.FILE_GUIDES, SharedPrefUtils.GUIDES_CHANGE_ANNIVERSARY, false)) {
                mTourGuide = TourGuide.init(this).with(TourGuide.Technique.Click).setPointer(new Pointer()).setToolTip(new ToolTip().setBackgroundColor(getResources().getColor(R.color.tab_normal)).setTitle(getString(R.string.info_note)).setDescription(getString(R.string.send_circle_request_act_tip))).setOverlay(new Overlay()).playOn(relAnniversary);

            }
        } else {
            txtTitle.setText(getString(R.string.send_circle_request_act_title_bbf));
            relAnniversary.setVisibility(View.GONE);
        }
        contact = (ContactBean) intent.getSerializableExtra("contact");

    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(this.getClass().getName());
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(this.getClass().getName());
        MobclickAgent.onPause(this);
    }

    @OnClick(R.id.rel_anniversary)
    public void onRelAnniversaryClicked() {
        if (mTourGuide != null) {
            mTourGuide.cleanUp();
            SharedPrefUtils.setBoolean(SharedPrefUtils.FILE_GUIDES, SharedPrefUtils.GUIDES_CHANGE_ANNIVERSARY, true);
        }
        Calendar minDate = Calendar.getInstance();
        Calendar maxDate = Calendar.getInstance();
        maxDate.set(Calendar.YEAR, maxDate.get(Calendar.YEAR) - 100);
        dialogUtils.showDatePicker(this, getString(R.string.send_circle_request_pick_date_title), anniversaryCal, maxDate.getTimeInMillis(), minDate.getTimeInMillis(), false, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogUtils.closeDialog();
                DatePicker dataPicker = (DatePicker) v.getTag();
                anniversaryCal.set(dataPicker.getYear(), dataPicker.getMonth(), dataPicker.getDayOfMonth());
                int days = getDays(anniversaryCal);
                if (days == 1) {
                    txtDate.setText(1 + getString(R.string.my_circle_activity_day_odd));

                } else {
                    txtDate.setText(days + getString(R.string.my_circle_activity_day_even));
                }
            }
        });


    }

    private int getDays(Calendar cal) {
        long timeMillion = new Date().getTime() - cal.getTimeInMillis();
        int days = (int) (timeMillion / (24l * 60 * 60 * 1000));
        return days == 0 ? days + 1 : days;
    }

    @OnClick(R.id.btn_send)
    public void onBtnSendClicked() {
       if (UserUtils.isVerifyCell()){
           showLoading();
           Flowable<MyCircleRequestListBean> flowable = RequestBusiness.getInstance().sendCircleRequest(requesType, contact.userId, DateUtils.getFormatTimeFromMillis(anniversaryCal.getTimeInMillis(), "yyyy-MM-dd"), edtSaySomething.getText().toString());
           flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<MyCircleRequestListBean>() {

               @Override
               public void onNext(MyCircleRequestListBean myCircleRequestListBean) {
                   super.onNext(myCircleRequestListBean);
                   if (!hasErrorCode) {
                       getRequestResult(myCircleRequestListBean);
                   }
               }

               @Override
               public void onComplete() {
                   closeLoading();
               }
           });
       }
    }

    private void getRequestResult(MyCircleRequestListBean myCircleRequestListBean) {

        MyCircleActivity.needRefresh = true;
        finish();
    }

    @OnClick(R.id.lin_back)
    public void onLinBackClicked() {
        finish();
    }

    @Override
    public void finish() {
        super.finish();
        ViewUtils.hideSoftInput(this, edtSaySomething);
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }
}
