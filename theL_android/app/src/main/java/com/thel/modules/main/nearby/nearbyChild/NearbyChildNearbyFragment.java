package com.thel.modules.main.nearby.nearbyChild;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.View;

import com.thel.bean.moments.MomentsBean;
import com.thel.bean.user.NearUserListNetBean;
import com.thel.imp.momentblack.MomentBlackListener;
import com.thel.imp.momentblack.MomentBlackUtils;
import com.thel.modules.main.nearby.NearbyMomentView;

import java.util.List;

/**
 * Created by waiarl on 2017/10/11.
 */

public class NearbyChildNearbyFragment extends NearbyChildFragment implements MomentBlackListener {
    private NearbyMomentView mNearbyMomentView;
    private MomentBlackUtils momentBlackUtils;
    private boolean headerReady = false;
    private boolean fallReady = false;

    public static NearbyChildFragment getInstance(Bundle bundle) {
        NearbyChildFragment instance = new NearbyChildNearbyFragment();
        instance.setArguments(bundle);
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        momentBlackUtils = new MomentBlackUtils();
        momentBlackUtils.registerReceiver(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        momentBlackUtils.unRegisterReceiver(this);
    }

    @Override
    protected void findViewById(View view) {
        super.findViewById(view);
        mNearbyMomentView = new NearbyMomentView(getActivity());
        mAdapter.addHeaderView(mNearbyMomentView);
    }

    @Override
    protected void refreshData() {
        headerReady = false;
        fallReady = false;
        super.refreshData();
        presenter.getNearbyMomentData();
    }

    @Override
    public void emptyData(boolean empty) {
        super.emptyData(empty && mNearbyMomentView.getAdapterData().size() == 0);
    }

    @Override
    public void showNearbyMomentData(List<MomentsBean> momentsBeanList) {
        super.showNearbyMomentData(momentsBeanList);
        headerReady = true;
        mNearbyMomentView.initView(momentsBeanList);
        if (mNearbyMomentView.getAdapterData().size() > 0) {
            emptyData(false);
        }
    }

    @Override
    public void showRefreshData(NearUserListNetBean nearUserListNetBean) {
        fallReady = true;
        super.showRefreshData(nearUserListNetBean);
    }

    @Override
    public void onBlackUsersChanged(String userId, boolean isAdd) {
        super.onBlackUsersChanged(userId, isAdd);
        mNearbyMomentView.filterNewBlack(userId);
    }

    @Override
    protected boolean canShowNewLiveGuide() {
        return super.canShowNewLiveGuide() && fallReady && headerReady;
    }

    @Override
    public void onBlackOneMoment(String momentId) {
        mNearbyMomentView.filterNewMoment(momentId);
    }

    @Override
    public void onBlackherMoment(String userId) {
        mNearbyMomentView.filterNewBlack(userId);
    }
}
