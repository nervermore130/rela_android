package com.thel.modules.main.home.follow;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.provider.Settings;

import com.thel.app.TheLApp;
import com.thel.base.BaseDataBean;
import com.thel.bean.BasicInfoBean;
import com.thel.bean.BasicInfoNetBean;
import com.thel.bean.RecommendListBean;
import com.thel.bean.live.LiveFollowingUsersBeanNew;
import com.thel.bean.moments.MomentsCheckBean;
import com.thel.bean.moments.MomentsListBean;
import com.thel.constants.TheLConstants;
import com.thel.modules.main.MainPresenter;
import com.thel.modules.main.home.follow.FollowContract.Presenter;
import com.thel.network.ErrorCode;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.service.DefaultRequestService;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SharedPrefUtils;
import com.thel.utils.UserUtils;

import org.reactivestreams.Publisher;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 *
 * @author liuyun
 * @date 2017/9/20
 */

public class FollowPresenter implements Presenter {

    private static final String TAG = "FollowPresenter";

    private FollowContract.View mFollowView;

    private CompositeDisposable mCompositeDisposable;

    private MomentsReleaseStatusReceiver mMomentsReleaseStatusReceiver;

    public FollowPresenter(FollowContract.View mFollowView) {

        mCompositeDisposable = new CompositeDisposable();

        mMomentsReleaseStatusReceiver = new MomentsReleaseStatusReceiver();

        this.mFollowView = mFollowView;

    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {

        mCompositeDisposable.clear();

    }

    @Override
    public void loadData(String curPage) {

        DefaultRequestService
                .createAllRequestService()
                .getMomentsList("", "moments", "20", curPage)
                .onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new InterceptorSubscribe<MomentsListBean>() {
                    @Override
                    public void onNext(MomentsListBean data) {
                        super.onNext(data);

                        if (mFollowView.isActive()) {
                            mFollowView.showMomentList(data);
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                        if (mFollowView.isActive()) {
                            mFollowView.loadDataFailed();
                        }
                    }
                });

    }

    @Override
    public void loadFollowingUsersLiveData() {
        DefaultRequestService
                .createAllRequestService()
                .getFollowingUsers()
                .onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new InterceptorSubscribe<LiveFollowingUsersBeanNew>() {
                    @Override
                    public void onNext(LiveFollowingUsersBeanNew liveFollowingUsersBean) {
                        super.onNext(liveFollowingUsersBean);
                        if (mFollowView.isActive()) {
                            mFollowView.showFollowUserLiveList(liveFollowingUsersBean.data);
                        }

                    }

                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                        if (mFollowView.isActive()) {
                            mFollowView.loadDataFailed();
                        }
                    }

                });
    }

    @Override
    public void register(Context context) {
        IntentFilter intentFilter = new IntentFilter();

        intentFilter.addAction(TheLConstants.BROADCAST_RELEASE_MOMENT_SUCCEED);
        intentFilter.addAction(TheLConstants.BROADCAST_RELEASE_MOMENT_FAIL);
        intentFilter.addAction(TheLConstants.BROADCAST_UPDATE_UNREAD_MSG_COUNT);
        intentFilter.addAction(TheLConstants.BROADCAST_ACTION_APK_DOWNLOADED);
        if (mMomentsReleaseStatusReceiver != null && context != null) {
            context.registerReceiver(mMomentsReleaseStatusReceiver, intentFilter);
        }
    }


    @Override
    public void unRegister(Context context) {
        if (mMomentsReleaseStatusReceiver != null && context != null) {
            context.unregisterReceiver(mMomentsReleaseStatusReceiver);
        }
    }

    @Override
    public void getUnreadMomentInfo() {

        L.d("RelaTimer", " getUnreadMomentInfo ");
        String cursor = ShareFileUtils.getString(ShareFileUtils.MATCH_NEW_LIKE_ME_COUNT, "");

        DefaultRequestService.createMsgRequestService()
                .checkMoments(cursor)
                .onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new InterceptorSubscribe<MomentsCheckBean>() {
                    @Override
                    public void onNext(MomentsCheckBean data) {
                        super.onNext(data);
                        if (mFollowView.isActive()) {
                            mFollowView.newFollowingMoments(data);
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                        if (mFollowView.isActive()) {
                            mFollowView.loadDataFailed();
                        }
                    }
                });
    }

    @Override
    public void getRecommendUserList() {
        DefaultRequestService
                .createAllRequestService()
                .getRecommendUsers("", "0", "10")
                .onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new InterceptorSubscribe<RecommendListBean>() {
                    @Override
                    public void onNext(RecommendListBean data) {
                        super.onNext(data);
                        if (data.data != null && data.data.list != null) {
                            if (mFollowView != null && mFollowView.isActive()) {
                                mFollowView.showRecommendUser(data.data.list);
                            }
                        }
                    }
                });
    }

    private class MomentsReleaseStatusReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {

                String action = intent.getAction();

                L.d(TAG, " action : " + action);

                if (action != null) {
                    switch (action) {
                        case TheLConstants.BROADCAST_RELEASE_MOMENT_SUCCEED:

                            if (mFollowView.isActive()) {
                                mFollowView.refreshData();
                            }

                            break;
                        case TheLConstants.BROADCAST_RELEASE_MOMENT_FAIL:

                            if (mFollowView.isActive()) {
                                mFollowView.releaseFailed();
                            }

                            break;
                        case TheLConstants.BROADCAST_UPDATE_UNREAD_MSG_COUNT:

                            break;
                        case TheLConstants.BROADCAST_ACTION_APK_DOWNLOADED:

                            String apkUrl = intent.getStringExtra("apkUrl");

                            if (mFollowView.isActive()) {
                                mFollowView.gotoAPKSetting(apkUrl);
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        }
    }

}
