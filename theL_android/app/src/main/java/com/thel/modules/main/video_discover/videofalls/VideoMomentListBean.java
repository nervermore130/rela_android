package com.thel.modules.main.video_discover.videofalls;

import com.thel.bean.video.VideoBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by waiarl on 2018/2/5.
 */

public class VideoMomentListBean {
    public int cursor;
    public boolean haveNextPage;
    public List<VideoBean> list = new ArrayList<>();
}
