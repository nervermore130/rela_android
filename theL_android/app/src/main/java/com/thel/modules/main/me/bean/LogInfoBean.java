package com.thel.modules.main.me.bean;

import com.thel.utils.MD5Utils;
import com.thel.utils.ShareFileUtils;

import java.io.Serializable;

public class LogInfoBean {

    public String page;
    public String page_id;
    public String from_page;
    public String from_page_id;
    public String activity;
    public String lat;
    public String lng;
    public String ua = MD5Utils.getUserAgent();
    public String user_id = ShareFileUtils.getString(ShareFileUtils.ID, "");
    public LogsDataBean data;

    public static class LogsDataBean implements Serializable {

        public String msg;
        public String rank_id;
        public String receiver_id;
        public String user_id;

        public String before;    //速配设置页面之前的选项
        public String after;   //速配设置页面当前选项
        public String push_type;//push的跳转类型
        public String pay_type;
        public String order_id;
        public String amount;

        public String data_id;
        public String share_id;
        public String data_type;
        public int index;
        public int viewer; //观看人数
        public String live_id;
        public String sort;
        public String version;//4.19.0
        public String moment_id; //日志id
        public String moment_type; //日志类型    image, text, video, live, share, theme, theme_reply, music, anniversare
        public String from_tab; //home/live/find/message/my
        public String comment_type;//话题评论类型 text video image voice
        public int redspot;  //是否有新的未查看的日志 未读消息数
        public String completion;//资料完成度
        public String text; //文本消息
        public String from_theme_id; //话题评论回复id
        public String theme_id;
        public String time;
        public String toast;//失败的提示
    }
}
