package com.thel.modules.live.bean

//{"userCount":444,"rank":2,"gem":"3304.31","nowGem":"1.89"}

data class ResponseGemBean(var userCount: Int, var rank: Int, var gem: String, var nowGem: String)