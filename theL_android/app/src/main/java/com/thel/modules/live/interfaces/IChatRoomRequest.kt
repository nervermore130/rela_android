package com.thel.modules.live.interfaces

import com.thel.modules.live.bean.AgoraBean
import io.agora.rtc.IRtcEngineEventHandler

interface IChatRoomRequest {

    fun banHer(userId: String)

    fun getMicSortList()

    fun openMicSort()

    fun closeMicSort()

    fun agreeOnSeat(userId: String, agora: AgoraBean)

    fun startEncounter()

    fun sendPkRequestMsg(userId: String, x: Float, y: Float, width: Float, height: Float)

    fun connectMic(method: String, toUserId: String, x: Float, y: Float, height: Float, width: Float, nickName: String, avatar: String, dailyGuard: Boolean)

    fun responseAudienceLinkMic(method: String, body: String)

    fun getTopFansTodayList()

    fun exucuteColseRunable()

    fun sendColseMsg()

    fun arGiftReceipt(payload: String)

    fun sendPkRequestCancelMsg(id: String)

    fun sendPkResponseMsg(toUserId: String, result: String, x: Float, y: Float, width: Float, height: Float)

    fun sendPkHangUpMsg(userId: String)

    fun cancelConnectMic(method: String, toUserId: String)

    fun uploadAudioVolume(speakers: Array<IRtcEngineEventHandler.AudioVolumeInfo>)

}