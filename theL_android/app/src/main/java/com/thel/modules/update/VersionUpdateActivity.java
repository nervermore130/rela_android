package com.thel.modules.update;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.thel.BuildConfig;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.bean.VersionBean;
import com.thel.constants.TheLConstants;
import com.thel.network.RequestConstants;
import com.thel.service.DownloadApkService;
import com.thel.utils.DeviceUtils;
import com.thel.utils.PermissionUtil;
import com.thel.utils.Utils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Locale;

public class VersionUpdateActivity extends BaseActivity implements View.OnClickListener {

    private TextView txt_version_content;
    private TextView find_new_version_tv;
    private TextView new_version_size_tv;
    private VersionBean bean;
    private ImageView img_update_now;
    private ImageView img_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.O && isTranslucentOrFloating()) {
            fixOrientation();
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_version_update);
        findViewById();
        getIntentData();
        setListener();
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            //立即更新
            case R.id.txt_update_now:

                PermissionUtil.requestStoragePermission(VersionUpdateActivity.this, new PermissionUtil.PermissionCallback() {
                    @Override
                    public void granted() {
//                        updateNow();
                        if (bean != null && bean.data != null) {
                            Intent intent = new Intent(VersionUpdateActivity.this, DownloadApkService.class);
                            intent.setAction(DownloadApkService.DOWNLOAD_START);
                            intent.putExtra("download_url", bean.data.url);
                            intent.putExtra("apk_version", bean.data.version);
                            startService(intent);
                            finish();
                        }
                    }

                    @Override
                    public void denied() {

                    }

                    @Override
                    public void gotoSetting() {

                    }
                });

                break;
            //稍后更新
            case R.id.txt_update_later:
                finish();
                break;
            default:
                finish();
                break;
        }
    }

    private void updateNow() {
        if (RequestConstants.APPLICATION_ID_LOCAL.equals(BuildConfig.APPLICATION_ID)) {
            Intent intent = new Intent(this, DownloadActivity.class);
            intent.putExtra(TheLConstants.BUNDLE_KEY_APK_URL, bean.data.url);
            startActivity(intent);
            finish();
        } else {
            Utils.directToAppStore(TheLApp.getContext());
        }
    }

    protected void findViewById() {
        find_new_version_tv = findViewById(R.id.find_new_version_tv);
        new_version_size_tv = findViewById(R.id.new_version_size_tv);
        txt_version_content = findViewById(R.id.txt_version_content);
        txt_version_content.setMovementMethod(ScrollingMovementMethod.getInstance());

    }

    protected void setListener() {
        findViewById(R.id.txt_update_now).setOnClickListener(this);
        findViewById(R.id.txt_update_later).setOnClickListener(this);
    }

    private void getIntentData() {
        Intent intent = getIntent();
        bean = (VersionBean) intent.getSerializableExtra(TheLConstants.BUNDLE_KEY_VERSIONBEAN);
        if (bean != null) {

            String new_version = getString(R.string.find_new_version) + " : " + bean.data.version;

            String version_size = getString(R.string.latest_version_size, bean.data.apkSize);

            find_new_version_tv.setText(new_version);

            new_version_size_tv.setText(version_size);

            final String language = DeviceUtils.getLanguage();

            String desc_text;

            if (language.equals(DeviceUtils.getLanguageStr(Locale.SIMPLIFIED_CHINESE))) {
                desc_text = bean.data.desc_cn;
            } else if (language.equals(DeviceUtils.getLanguageStr(Locale.TRADITIONAL_CHINESE))) {
                desc_text = bean.data.desc_tw;
            } else if (language.equals(DeviceUtils.getLanguageStr(Locale.US))) {
                desc_text = bean.data.desc_en;
            } else {
                desc_text = bean.data.desc_cn;
            }
            txt_version_content.setText(desc_text);

        }

    }

    @Override
    public void onBackPressed() {
        return;
    }


    private boolean isTranslucentOrFloating() {
        boolean isTranslucentOrFloating = false;
        try {
            int[] styleableRes = (int[]) Class.forName("com.android.internal.R$styleable").getField("Window").get(null);
            final TypedArray ta = obtainStyledAttributes(styleableRes);
            Method m = ActivityInfo.class.getMethod("isTranslucentOrFloating", TypedArray.class);
            m.setAccessible(true);
            isTranslucentOrFloating = (boolean) m.invoke(null, ta);
            m.setAccessible(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isTranslucentOrFloating;
    }

    private boolean fixOrientation() {
        try {
            Field field = Activity.class.getDeclaredField("mActivityInfo");
            field.setAccessible(true);
            ActivityInfo o = (ActivityInfo) field.get(this);
            o.screenOrientation = -1;
            field.setAccessible(false);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

}
