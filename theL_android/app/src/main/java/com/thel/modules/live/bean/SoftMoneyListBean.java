package com.thel.modules.live.bean;

import com.thel.base.BaseDataBean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by the L on 2016/5/23.
 * 商品列表 软妹币
 * 包括软妹币商品列表
 */
public class SoftMoneyListBean extends BaseDataBean implements Serializable {

    public Data data;

    public class Data {
        /**
         * 当前余额
         */
        public long gold;//当前余额

        public float gem;
        public int hasBuy;
        public String channel;

        /**
         * 商品列表
         */
        public List<SoftGold> list = new ArrayList<>();
    }
}
