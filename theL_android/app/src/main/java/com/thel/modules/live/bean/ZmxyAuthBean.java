package com.thel.modules.live.bean;

import com.thel.base.BaseDataBean;

import java.io.Serializable;

/**
 * Created by lingwei on 2018/3/19.
 */

public class ZmxyAuthBean extends BaseDataBean implements Serializable {

    /**
     * data : {"authUrl":"https://zmopenapi.zmxy.com.cn/openapi.do?app_id=300002241&channel=apppc&charset=UTF-8&method=zhima.customer.certification.certify&platform=zmop&sign=avBiJpcITPI9Q3Wr%2B0akX7gBdMYiXV597RNQR%2FhbrR0P6DJq1t%2B6ZhH75GigqlD7pJo4IWqksWuhmSBHBQ%2FBwp7liOgLzctKz1bYqNBC4tXzpP5brqcJ3221H4p8FoL3ec%2FQpgDv7xR2R%2B9%2BextD8hSCr73by%2BfhWr1Pgdj2RuU%3D&version=1.0&params=BzwTIY%2BKwBHLq3zlVnWXghwRRHg5%2FHZ3AHUVfO%2FHp7SD0uNVtrAuSR74f8D0UjpmigMqUqODzKgcgRKfBrMQEBGE8Dp1JHlMIFdch11P1Jb6af5%2Fv2GDusaA2TMgV6jrYjbfnwO8zZVyA2hdGis%2Bn8vak2866POiastFKV6uhbg%3D"}
     */

    public ZmxyAuthDataBean data;

    public class ZmxyAuthDataBean {
        /**
         * authUrl : https://zmopenapi.zmxy.com.cn/openapi.do?app_id=300002241&channel=apppc&charset=UTF-8&method=zhima.customer.certification.certify&platform=zmop&sign=avBiJpcITPI9Q3Wr%2B0akX7gBdMYiXV597RNQR%2FhbrR0P6DJq1t%2B6ZhH75GigqlD7pJo4IWqksWuhmSBHBQ%2FBwp7liOgLzctKz1bYqNBC4tXzpP5brqcJ3221H4p8FoL3ec%2FQpgDv7xR2R%2B9%2BextD8hSCr73by%2BfhWr1Pgdj2RuU%3D&version=1.0&params=BzwTIY%2BKwBHLq3zlVnWXghwRRHg5%2FHZ3AHUVfO%2FHp7SD0uNVtrAuSR74f8D0UjpmigMqUqODzKgcgRKfBrMQEBGE8Dp1JHlMIFdch11P1Jb6af5%2Fv2GDusaA2TMgV6jrYjbfnwO8zZVyA2hdGis%2Bn8vak2866POiastFKV6uhbg%3D
         */

        public String authUrl;
    }
}
