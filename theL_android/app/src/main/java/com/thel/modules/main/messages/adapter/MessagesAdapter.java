package com.thel.modules.main.messages.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.facebook.drawee.view.SimpleDraweeView;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.message.MomentToChatBean;
import com.thel.constants.TheLConstants;
import com.thel.db.DBConstant;
import com.thel.db.table.message.MsgTable;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.imp.wink.WinkUtils;
import com.thel.manager.ChatServiceManager;
import com.thel.modules.main.messages.bean.MsgBean;
import com.thel.modules.main.messages.bean.MsgWithUnreadBean;
import com.thel.modules.main.messages.db.MessageDataBaseAdapter;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.utils.AnimUtils;
import com.thel.utils.EmojiUtils;
import com.thel.utils.GsonUtils;
import com.thel.utils.ImageUtils;
import com.thel.utils.L;
import com.thel.utils.SharedPrefUtils;
import com.thel.utils.ViewUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MessagesAdapter extends BaseAdapter {

    private final static String TAG = "MessagesAdapter";

    private List<MsgTable> lastMsgList = new ArrayList<>();
    private List<String> imageUrllist = new ArrayList<>();
    private String[] imageUrls;

    private LayoutInflater mInflater;
    private Context context;

    public MessagesAdapter(List<MsgTable> lastMsgList, Context context) {
        this.context = context;
        mInflater = (LayoutInflater) TheLApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        refreshAdapter(lastMsgList);
    }

    public void refreshAdapter(List<MsgTable> lastMsgList) {
        this.lastMsgList.clear();
        this.lastMsgList.addAll(lastMsgList);
        this.imageUrllist.clear();

        int listSize = lastMsgList.size();
        for (int i = 0; i < listSize; i++) {
            MsgTable bean = lastMsgList.get(i);
            if (MsgBean.MSG_TYPE_REQUEST.equals(bean.msgType) && TextUtils.isEmpty(bean.fromUserId)) {// 密友请求
                this.imageUrllist.add("");
                continue;
            }
            String avater = "";
            if (bean.msgDirection.equals(MsgBean.MSG_DIRECTION_TYPE_OUTGOING)) { // 发出的消息
                avater = bean.toAvatar;
            } else {
                avater = bean.fromAvatar;
            }
            this.imageUrllist.add(avater);
        }

        this.imageUrls = imageUrllist.toArray(new String[listSize]);
    }

    @Override
    public int getCount() {
        return lastMsgList.size();
    }

    @Override
    public Object getItem(int position) {
        return lastMsgList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        HoldView holdView = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.messages_listitem, parent, false);
            holdView = new HoldView();
            holdView.img_thumb = convertView.findViewById(R.id.img_thumb);
            holdView.txt_nickname = convertView.findViewById(R.id.txt_nickname);
            holdView.txt_msgTime = convertView.findViewById(R.id.txt_msgTime);
            holdView.txt_msgContent = convertView.findViewById(R.id.txt_msgContent);
            holdView.txt_unread_count = convertView.findViewById(R.id.txt_unread_count);
            holdView.img_wink = convertView.findViewById(R.id.img_wink);
            holdView.img_sendwink = convertView.findViewById(R.id.img_sendwink);
            holdView.re_wink_or_match = convertView.findViewById(R.id.rl_wink_or_match);
            holdView.match_iv = convertView.findViewById(R.id.match_iv);
            holdView.root_view = convertView.findViewById(R.id.root_view);
            convertView.setTag(holdView); // 把holdview缓存下来
        } else {
            holdView = (HoldView) convertView.getTag();
        }

        MsgTable msgTable = lastMsgList.get(position);

        L.d("MessagesAdapter", " msgTable.msgType : " + msgTable.msgType);

        L.d("MessagesAdapter", " msgTable.msgText : " + msgTable.msgText);
        holdView.match_iv.setVisibility(View.GONE);
        holdView.img_wink.setVisibility(View.GONE);
        holdView.img_wink.setOnClickListener(null);
        holdView.re_wink_or_match.setVisibility(View.GONE);
        if (msgTable.msgTime > DBConstant.STICK_TOP_BASE_TIME) {
            convertView.setBackgroundColor(TheLApp.getContext().getResources().getColor(R.color.msg_sticky_top_bg));
        } else {
            convertView.setBackgroundColor(TheLApp.getContext().getResources().getColor(R.color.white));
        }
        holdView.txt_msgTime.setText("");
        holdView.img_thumb.setOnClickListener(null);

        if (MsgBean.MSG_TYPE_LIKE.equals(msgTable.msgType)) {// 配对喜欢消息
            holdView.img_thumb.setImageURI(Uri.parse(TheLConstants.RES_PIC_URL + R.mipmap.icn_match));
            holdView.txt_nickname.setText(TheLApp.getContext().getString(R.string.like_msgs_title));
            holdView.txt_msgContent.setText(msgTable.msgText);
            if (msgTable.unreadCount >= 99) {
                holdView.txt_unread_count.setText("99+");
            } else {
                holdView.txt_unread_count.setText(msgTable.unreadCount + "");
            }
            holdView.txt_unread_count.setVisibility(msgTable.unreadCount > 0 ? View.VISIBLE : View.INVISIBLE);
        } else {
            if (msgTable.isSystem == MsgBean.IS_SYSTEM_MSG) {// 其他系统消息，2.18.0版本暂时没有，以后可拓展
                holdView.img_thumb.setImageURI(Uri.parse(TheLConstants.RES_PIC_URL + R.mipmap.icn_notification));
            } else if (msgTable.isSystem == MsgBean.IS_WINK_MSG && msgTable.userId.equals(MessageDataBaseAdapter.WINK_TABLENAME)) {// 挤眼消息.4.0.0判断userid要等于表名
                holdView.img_thumb.setImageURI(Uri.parse(TheLConstants.RES_PIC_URL + R.mipmap.btn_info_wink_normal));
            } else if (msgTable.isSystem == MsgBean.IS_FOLLOW_MSG && msgTable.userId.equals(MessageDataBaseAdapter.FOLLOW_TABLENAME)) {// 关注消息，4.0.0判断userid要等于表名
                holdView.img_thumb.setImageURI(Uri.parse(TheLConstants.RES_PIC_URL + R.mipmap.icn_msg_follow));
                /**4.0.0 陌生人消息**/
            } else if (msgTable.isSystem == MsgBean.IS_STRANGER_MSG) {// 陌生人消息
                holdView.img_thumb.setImageURI(Uri.parse(TheLConstants.RES_PIC_URL + R.mipmap.btn_info_chat_stranger));
            } else {
                holdView.img_thumb.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(imageUrls[position], TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE)));
            }

            String toUserName = "";
            String draft = null;
            if (msgTable.msgDirection.equals(MsgBean.MSG_DIRECTION_TYPE_OUTGOING)) { // 发出的消息
                toUserName = msgTable.toUserNickname;
                draft = SharedPrefUtils.getString(SharedPrefUtils.FILE_NAME_CHAT_DRAFT, msgTable.toUserId, "");
            } else {
                toUserName = msgTable.fromNickname;
                draft = SharedPrefUtils.getString(SharedPrefUtils.FILE_NAME_CHAT_DRAFT, msgTable.fromUserId, "");
            }
            if (TheLConstants.SYSTEM_USER.equals(msgTable.fromUserId)) {// 系统通知账号发的消息显示 “系统消息”
                holdView.txt_nickname.setText(TheLApp.getContext().getString(R.string.info_notifications));
            } else {
                if (msgTable.isSystem == MsgBean.IS_WINK_MSG && msgTable.userId.equals(MessageDataBaseAdapter.WINK_TABLENAME)) {// 挤眼消息(如果是列表上的，userId= wink才行)
                    holdView.txt_nickname.setText(TheLApp.getContext().getString(R.string.popularity));
                } else if (msgTable.isSystem == MsgBean.IS_FOLLOW_MSG && msgTable.userId.equals(MessageDataBaseAdapter.FOLLOW_TABLENAME)) {// 关注消息(如果是列表上的，userId= follow才行)
                    holdView.txt_nickname.setText(TheLApp.getContext().getString(R.string.userinfo_activity_follow));
                    /**4.0.0 陌生人消息**/
                } else if (msgTable.isSystem == MsgBean.IS_STRANGER_MSG) {// 关注消息
                    holdView.txt_nickname.setText(TheLApp.getContext().getString(R.string.stranger_msg));
                } else {
                    holdView.txt_nickname.setText(toUserName);
                }
            }

            holdView.img_thumb.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    ViewUtils.preventViewMultipleClick(v, 2000);
                    MsgTable MsgTable = lastMsgList.get(position);
                    if (TheLConstants.SYSTEM_USER.equals(MsgTable.userId)) {// 系统消息和系统通知账号发的消息不可点击头像
                        return;
                    }
//                    Intent intent = new Intent();
//                    intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, MsgTable.userId);
//                    intent.setClass(context, UserInfoActivity.class);
//                    context.startActivity(intent);
                    FlutterRouterConfig.Companion.gotoUserInfo(MsgTable.userId);
                }
            });

            long time;

            if (msgTable.msgTime > DBConstant.STICK_TOP_BASE_TIME) {
                time = ChatServiceManager.getInstance().getLastMsgTime(msgTable.userId);
            } else {
                time = msgTable.msgTime;
            }

            L.d("MessagesAdapter", " time : " + time);

            holdView.txt_msgTime.setText(getTime(time));
            String msg_user_nikcname = "";
            if (msgTable.isSystem == MsgBean.IS_STRANGER_MSG) {
                msg_user_nikcname = msgTable.fromNickname + "：";
            }

            L.d(TAG, " msgTable.msgType : " + msgTable.msgType);

            L.d(TAG, " msgTable.isSystem : " + msgTable.isSystem);

            if (msgTable.isSystem == MsgBean.IS_NORMAL_MSG && !TextUtils.isEmpty(draft)) {// 如果有草稿
                String str = TheLApp.getContext().getString(R.string.message_info_draft);
                SpannableString sp = EmojiUtils.getInstace(EmojiUtils.DEFAULT_SIZE).getExpressionString(TheLApp.getContext(), str + draft);
                sp.setSpan(new ForegroundColorSpan(Color.RED), 0, str.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                holdView.txt_msgContent.setText(sp);
            } else if (MsgBean.MSG_TYPE_EMPTY.equals(msgTable.msgType) && msgTable.isSystem == MsgBean.IS_STRANGER_MSG) {//陌生人消息，切陌生人消息列表为空
                holdView.txt_msgContent.setText(TheLApp.getContext().getString(R.string.info_list_empty));
            } else if (MsgBean.MSG_TYPE_TEXT.equals(msgTable.msgType)) {
                holdView.txt_msgContent.setText(EmojiUtils.getInstace(EmojiUtils.DEFAULT_SIZE).getExpressionString(TheLApp.getContext(), msg_user_nikcname + msgTable.msgText));
            } else if (MsgBean.MSG_TYPE_IMAGE.equals(msgTable.msgType)) {
                holdView.txt_msgContent.setText(msg_user_nikcname + TheLApp.getContext().getString(R.string.message_info_photo));
            } else if (MsgBean.MSG_TYPE_STICKER.equals(msgTable.msgType)) {
                holdView.txt_msgContent.setText(msg_user_nikcname + TheLApp.getContext().getString(R.string.message_info_sticker));
            } else if (MsgBean.MSG_TYPE_GIFT.equals(msgTable.msgType)) {
                holdView.txt_msgContent.setText(msg_user_nikcname + TheLApp.getContext().getString(R.string.message_info_vip_gift));
              /*  holdView.txt_img.setVisibility(View.VISIBLE);
                holdView.txt_img.setImageResource(R.drawable.icn_chat_sentpremium);*/
                holdView.re_wink_or_match.setVisibility(View.VISIBLE);
                holdView.img_wink.setVisibility(View.VISIBLE);
                holdView.img_wink.setImageResource(R.mipmap.icn_chat_sentpremium);
            } else if (MsgBean.MSG_TYPE_VOICE.equals(msgTable.msgType)) {
                holdView.txt_msgContent.setText(msg_user_nikcname + TheLApp.getContext().getString(R.string.message_info_voice));
            } else if (MsgBean.MSG_TYPE_VIDEO.equals(msgTable.msgType)) {
                holdView.txt_msgContent.setText(msg_user_nikcname + TheLApp.getContext().getString(R.string.message_info_video));
            } else if (MsgBean.MSG_TYPE_MATCH.equals(msgTable.msgType)) {// 配对成功消息

                L.d(TAG, " ----------MSG_TYPE_MATCH---------- ");
                holdView.match_iv.setVisibility(View.VISIBLE);
                holdView.txt_msgContent.setText(msg_user_nikcname + TheLApp.getContext().getString(R.string.match_perfect_match_line1));
                holdView.re_wink_or_match.setVisibility(View.VISIBLE);
            } else if (MsgBean.MSG_TYPE_WINK.equals(msgTable.msgType)) {//&& !MessageDataBaseAdapter.STRANGER_MSG_USER_ID.equals(MsgTable.userId)) {// 挤眼消息(切不是陌生人消息)
                if (!MessageDataBaseAdapter.STRANGER_MSG_USER_ID.equals(msgTable.userId)) {// 挤眼消息(且不是陌生人消息)
                    holdView.txt_msgContent.setText(msg_user_nikcname + TheLApp.getContext().getString(R.string.message_info_wink));
                    /**4.0.0，显示挤眼图标，最新消息是挤眼且没对它挤过眼,且不是自己发出的消息**/
                    if (!WinkUtils.isTodayWinkedUser(msgTable.userId) && !msgTable.msgDirection.equals(MsgBean.MSG_DIRECTION_TYPE_OUTGOING)) {
                        holdView.re_wink_or_match.setVisibility(View.VISIBLE);
                        holdView.img_wink.setVisibility(View.VISIBLE);
                        holdView.img_wink.setImageResource(R.mipmap.icn_wink_history);
                        AnimUtils.setWnkCtrl(holdView.img_wink, holdView.img_sendwink, msgTable.userId, msgTable.fromNickname, imageUrllist.get(position),context);//能回挤的挤眼消息肯定是对方发过来来消息
                    }
                } else {//陌生人消息列表 挤眼消息
                    holdView.txt_msgContent.setText(msg_user_nikcname + TheLApp.getContext().getString(R.string.message_info_wink));
                }

               /* holdView.img_wink.setVisibility(View.VISIBLE);//4.0.0,在最下面做了判断
                holdView.img_wink.setImageResource(R.drawable.icn_wink_history);*/
            } else if (MsgBean.MSG_TYPE_FOLLOW.equals(msgTable.msgType)) {// 关注消息
                holdView.txt_msgContent.setText(msg_user_nikcname + TheLApp.getContext().getString(R.string.message_info_follow));
            } else if (MsgBean.MSG_TYPE_UNFOLLOW.equals(msgTable.msgType)) {// 取消关注消息
                holdView.txt_msgContent.setText(msg_user_nikcname + TheLApp.getContext().getString(R.string.message_info_unfollow));
            } else if (MsgBean.MSG_TYPE_MAP.equals(msgTable.msgType)) {// 地图
                holdView.txt_msgContent.setText(msg_user_nikcname + TheLApp.getContext().getString(R.string.message_info_location));
            } else if (MsgBean.MSG_TYPE_SECRET_KEY.equals(msgTable.msgType)) {// 密钥
                holdView.txt_msgContent.setText(msg_user_nikcname + TheLApp.getContext().getString(R.string.message_text_secretKey));
            } else if (MsgBean.MSG_TYPE_HIDDEN_LOVE.equals(msgTable.msgType)) {// 暗恋
                holdView.txt_msgContent.setText(msg_user_nikcname + TheLApp.getContext().getString(R.string.message_text_hiddenLove));
            } else if (MsgBean.MSG_TYPE_USER_CARD.equals(msgTable.msgType)) {// 推荐用户
                holdView.txt_msgContent.setText(msg_user_nikcname + TheLApp.getContext().getString(R.string.message_info_recommend_user));
            } else if (!TextUtils.isEmpty(getRecommendMsgTypeText(msgTable.msgType))) {
                holdView.txt_msgContent.setText(msg_user_nikcname + getRecommendMsgTypeText(msgTable.msgType));
            } else if (MsgBean.MSG_TYPE_REQUEST.equals(msgTable.msgType) && TextUtils.isEmpty(msgTable.fromUserId)) {// 密友请求的入口
                holdView.img_thumb.setImageURI(Uri.parse(TheLConstants.RES_PIC_URL + R.mipmap.icn_chat_request));
                holdView.txt_nickname.setText(TheLApp.getContext().getString(R.string.my_circle_requests_act_title));
                if (msgTable.unreadCount <= 0) {
                    holdView.txt_msgContent.setText(TheLApp.getContext().getString(R.string.message_activity_request_read));
                    holdView.txt_unread_count.setVisibility(View.INVISIBLE);
                } else {
                    if (msgTable.unreadCount == 1)
                        holdView.txt_msgContent.setText(String.format(TheLApp.getContext().getString(R.string.message_activity_request_unread_odd), msgTable.unreadCount));
                    else
                        holdView.txt_msgContent.setText(String.format(TheLApp.getContext().getString(R.string.message_activity_request_unread_even), msgTable.unreadCount));
                    if (msgTable.unreadCount >= 99) {
                        holdView.txt_unread_count.setText("99+");
                    } else {
                        holdView.txt_unread_count.setText(msgTable.unreadCount + "");
                    }
                    holdView.txt_unread_count.setVisibility(View.VISIBLE);
                }
            } else if (msgTable.msgType.equals(MsgBean.MSG_TYPE_MOMENT_TO_CHAT)) {
                final MomentToChatBean momentToChatBean = GsonUtils.getObject(msgTable.msgText, MomentToChatBean.class);
                holdView.txt_msgContent.setText(momentToChatBean.chatText);
            } else {
                holdView.txt_msgContent.setText(msg_user_nikcname + TheLApp.getContext().getString(R.string.info_version_outdate));// 未知
            }
            if (msgTable.unreadCount >= 99) {
                holdView.txt_unread_count.setText("99+");
            } else {
                holdView.txt_unread_count.setText(msgTable.unreadCount + "");
            }
            /**如果是陌生人消息，需要看是否陌生人里面有可以挤眼的消息来判定挤眼图标是否显示，但不能点击**/
            if (msgTable.isWinked == MsgWithUnreadBean.WINK_YES) {
                /**4.0.0，显示挤眼图标，最新消息是挤眼且没对它挤过眼,且不是自己发出的消息**/

                if (!WinkUtils.isTodayWinkedUser(msgTable.userId) && !msgTable.msgDirection.equals(MsgBean.MSG_DIRECTION_TYPE_OUTGOING)) {

                    holdView.re_wink_or_match.setVisibility(View.VISIBLE);
                    holdView.img_wink.setVisibility(View.VISIBLE);
                    holdView.img_wink.setImageResource(R.mipmap.icn_wink_history);
                    AnimUtils.setWnkCtrl(holdView.img_wink, holdView.img_sendwink, msgTable);//能回挤的挤眼消息肯定是对方发过来来消息
                }

            } else {
                holdView.img_wink.setVisibility(View.GONE);
                holdView.img_sendwink.setVisibility(View.GONE);
            }
            holdView.txt_unread_count.setVisibility(msgTable.unreadCount > 0 ? View.VISIBLE : View.INVISIBLE);
        }

//        convertView.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override public boolean onLongClick(View v) {
//                showDeleteMenuAndReport(msgTable, v);
//                return true;
//            }
//        });

        return convertView;
    }

    private String getRecommendMsgTypeText(String msgType) {
        return msgTypeTextMap.get(msgType);
    }

    private Map<String, String> msgTypeTextMap = new HashMap<String, String>() {
        {
            put(MsgBean.MSG_TYPE_MOMENT_TEXT, TheLApp.getContext().getString(R.string.recommend_a_moment));//推荐纯文本日志
            put(MsgBean.MSG_TYPE_MOMENT_IMAGE, TheLApp.getContext().getString(R.string.recommend_a_moment));//图片日志
            put(MsgBean.MSG_TYPE_MOMENT_MUSIC, TheLApp.getContext().getString(R.string.shared_a_song));//音乐日志
            put(MsgBean.MSG_TYPE_MOMENT_VIDEO, TheLApp.getContext().getString(R.string.recommend_a_moment));//视频日志
            put(MsgBean.MSG_TYPE_MOMENT_LIVE, TheLApp.getContext().getString(R.string.shared_a_live_steam));//直播
            put(MsgBean.MSG_TYPE_MOMENT_THEME, TheLApp.getContext().getString(R.string.shared_a_topic));//话题
            put(MsgBean.MSG_TYPE_MOMENT_THEMEREPLY, TheLApp.getContext().getString(R.string.recommend_a_moment));//话题参与
            put(MsgBean.MSG_TYPE_MOMENT_USERCARD, TheLApp.getContext().getString(R.string.recommend_a_moment));//推荐名片
            put(MsgBean.MSG_TYPE_MOMENT_RECOMMEND, TheLApp.getContext().getString(R.string.recommend_a_moment));//推荐日志
            put(MsgBean.MSG_TYPE_MOMENT_WEB, TheLApp.getContext().getString(R.string.shared_a_website));//推荐网页日志
            put(MsgBean.MSG_TYPE_WEB, TheLApp.getContext().getString(R.string.shared_a_website));//推荐网页
        }
    };

    class HoldView {
        SimpleDraweeView img_thumb;
        TextView txt_nickname;
        TextView txt_msgTime;
        TextView txt_msgContent;
        TextView txt_unread_count;
        ImageView txt_img;
        ImageView img_wink;//4.0.0，挤眼图标，还包括匹配等
        LottieAnimationView img_sendwink;
        RelativeLayout re_wink_or_match;
        ImageView match_iv;
        LinearLayout root_view;
    }

    private String getTime(Long milliseconds) {
        String todySDF = "HH:mm";
        String yesterDaySDF = "'" + TheLApp.getContext().getString(R.string.chat_activity_yesterday) + "' HH:mm";
        String otherSDF = "MM-dd";

        SimpleDateFormat sfd = null;
        String time = "";
        Calendar dateCalendar = Calendar.getInstance();
        dateCalendar.setTimeInMillis(milliseconds);
        Long nowMilliseconds = System.currentTimeMillis();
        Calendar targetCalendar = Calendar.getInstance();
        targetCalendar.setTimeInMillis(nowMilliseconds);
        targetCalendar.set(Calendar.HOUR_OF_DAY, 0);
        targetCalendar.set(Calendar.MINUTE, 0);
        if (dateCalendar.after(targetCalendar)) {
            sfd = new SimpleDateFormat(todySDF);
            time = sfd.format(milliseconds);
            return time;
        } /*else {//昨天的也只显示日期
            targetCalendar.add(Calendar.DATE, -1);
            if (dateCalendar.after(targetCalendar)) {
                sfd = new SimpleDateFormat(yesterDaySDF);
                time = sfd.format(milliseconds);
                return time;
            }
        }*/
        sfd = new SimpleDateFormat(otherSDF);
        time = sfd.format(milliseconds);
        return time;
    }



}
