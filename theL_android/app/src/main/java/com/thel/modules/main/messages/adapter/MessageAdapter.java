package com.thel.modules.main.messages.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;

import androidx.recyclerview.widget.RecyclerView;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.facebook.drawee.view.SimpleDraweeView;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.message.MomentToChatBean;
import com.thel.constants.TheLConstants;
import com.thel.db.DBConstant;
import com.thel.db.DBUtils;
import com.thel.db.table.message.MsgTable;
import com.thel.flutter.bridge.FlutterPushImpl;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.flutter.bridge.RfSBridgeHandlerFactory;
import com.thel.imp.wink.WinkUtils;
import com.thel.manager.ChatServiceManager;
import com.thel.modules.main.me.bean.LogInfoBean;
import com.thel.modules.main.me.match.eventcollect.collect.LiveLogUtils;
import com.thel.modules.main.messages.bean.MsgBean;
import com.thel.modules.main.messages.bean.MsgWithUnreadBean;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.utils.AnimUtils;
import com.thel.utils.DialogUtil;
import com.thel.utils.EmojiUtils;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;
import com.thel.utils.ScreenUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SharedPrefUtils;
import com.thel.utils.SizeUtils;
import com.thel.utils.ViewUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static android.content.DialogInterface.BUTTON_POSITIVE;
import static com.thel.constants.TheLConstants.SYSTEM_USER;


/**
 * Created by chad
 * Time 17/11/16
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.ViewHolder> {

    public static final String TAG = "MessageAdapter";
    private final String pageId;
    private final String from_page;
    private final String from_page_id;

    private Context mContext;

    private List<MsgTable> mData;

    public MessageAdapter(List<MsgTable> data, String pageId, String from_page, String from_page_id) {
        initData(data);
        this.pageId = pageId;
        this.from_page = from_page;
        this.from_page_id = from_page_id;
    }

    public List<MsgTable> getData() {
        return mData;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View view = LayoutInflater.from(mContext).inflate(R.layout.messages_listitem, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holdView, final int position) {

        try {

            final MsgTable msgTable = mData.get(position);

            holdView.msg_hiding_close_rl.setVisibility(View.GONE);

            holdView.txt_msgTime.setVisibility(View.VISIBLE);

            String userId = msgTable.userId;

            if (userId == null) {
                userId = "";
            }

            switch (userId) {
                case DBConstant.Message.WINK_TABLE_NAME:
                    holdView.txt_nickname.setText(TheLApp.getContext().getString(R.string.userinfo_activity_left_wink));
                    holdView.img_thumb.setImageURI(Uri.parse(TheLConstants.RES_PIC_URL + R.mipmap.icn_chat_wink));

                    break;
                case DBConstant.Message.FOLLOW_TABLE_NAME:
                    holdView.txt_nickname.setText(TheLApp.getContext().getString(R.string.userinfo_activity_follow));
                    holdView.img_thumb.setImageURI(Uri.parse(TheLConstants.RES_PIC_URL + R.mipmap.icn_msg_follow));

                    break;
                case DBConstant.Message.STRANGER_TABLE_NAME:

                    holdView.img_thumb.setImageURI(Uri.parse(TheLConstants.RES_PIC_URL + R.mipmap.icn_info_chat));

                    holdView.txt_nickname.setText(TheLApp.getContext().getString(R.string.stranger_msg));

                    break;
                case DBConstant.Message.REQUEST_TABLE_NAME:

                    holdView.img_thumb.setImageURI(Uri.parse(TheLConstants.RES_PIC_URL + R.mipmap.icn_chat_request));
                    holdView.txt_nickname.setText(TheLApp.getContext().getString(R.string.my_circle_requests_act_title));

                    break;
                case DBConstant.Message.MATCH_TABLE_NAME:
                    holdView.img_thumb.setImageURI(Uri.parse(TheLConstants.RES_PIC_URL + R.mipmap.icn_match));
                    holdView.txt_nickname.setText(TheLApp.getContext().getString(R.string.like_msgs_title));
                    holdView.txt_msgContent.setText(msgTable.msgText);
                    break;

                case DBConstant.Message.MSG_HIDING:
                    holdView.msg_hiding_close_rl.setVisibility(View.VISIBLE);
                    holdView.img_thumb.setImageURI(Uri.parse(TheLConstants.RES_PIC_URL + R.mipmap.btn_vip_invisible));
                    holdView.txt_nickname.setText(TheLApp.context.getString(R.string.vip_sneaking_title));
                    holdView.txt_msgContent.setText(TheLApp.context.getString(R.string.sneak_tips));
                    traceMessageLog("exposure", position, "", msgTable.userId);
                    break;
                case DBConstant.Message.MSG_AMUSEMENT_PARK:
                    holdView.msg_hiding_close_rl.setVisibility(View.VISIBLE);
                    holdView.img_thumb.setImageURI(Uri.parse(TheLConstants.RES_PIC_URL + R.mipmap.message_list_btn_youleyuan));
                    holdView.txt_nickname.setText(TheLApp.context.getString(R.string.amusement_park));
                    holdView.txt_msgContent.setText(TheLApp.context.getString(R.string.amusement_park_list_content));
                    holdView.txt_msgTime.setVisibility(View.GONE);
                    break;
                default:
                    holdView.img_thumb.setImageURI(Uri.parse(msgTable.avatar));
                    holdView.txt_nickname.setText(msgTable.userName);
                    holdView.txt_msgContent.setText(msgTable.msgText);
                    break;

            }

            String msg_user_nikcname = "";

            if (msgTable.isSystem == MsgBean.IS_STRANGER_MSG) {
                msg_user_nikcname = msgTable.fromNickname + "：";
            }

            String draft;
            if (MsgBean.MSG_DIRECTION_TYPE_OUTGOING.equals(msgTable.msgDirection)) { // 发出的消息
                draft = SharedPrefUtils.getString(SharedPrefUtils.FILE_NAME_CHAT_DRAFT, msgTable.toUserId, "");
            } else {
                draft = SharedPrefUtils.getString(SharedPrefUtils.FILE_NAME_CHAT_DRAFT, msgTable.fromUserId, "");
            }

            if (msgTable.isSystem == MsgBean.IS_NORMAL_MSG && !TextUtils.isEmpty(draft)) {
                String str = TheLApp.getContext().getString(R.string.message_info_draft);
                SpannableString sp = EmojiUtils.getInstace(EmojiUtils.DEFAULT_SIZE).getExpressionString(TheLApp.getContext(), str + draft);
                sp.setSpan(new ForegroundColorSpan(Color.RED), 0, str.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                holdView.txt_msgContent.setText(sp);
            } else {
                String content;
                switch (msgTable.msgType) {
                    case MsgBean.MSG_TYPE_TEXT:
                        holdView.txt_msgContent.setText(EmojiUtils.getInstace(EmojiUtils.DEFAULT_SIZE).getExpressionString(TheLApp.getContext(), msg_user_nikcname + msgTable.msgText));
                        break;
                    case MsgBean.MSG_TYPE_IMAGE:
                        holdView.txt_msgContent.setText(msg_user_nikcname + TheLApp.getContext().getString(R.string.message_info_photo));
                        break;
                    case MsgBean.MSG_TYPE_STICKER:
                        holdView.txt_msgContent.setText(msg_user_nikcname + TheLApp.getContext().getString(R.string.message_info_sticker));
                        break;
                    case MsgBean.MSG_TYPE_GIFT:
                        holdView.txt_msgContent.setText(msg_user_nikcname + TheLApp.getContext().getString(R.string.message_info_vip_gift));
                        holdView.re_wink_or_match.setVisibility(View.VISIBLE);
                        holdView.img_wink.setVisibility(View.VISIBLE);
                        holdView.img_wink.setImageResource(R.mipmap.icn_chat_sentpremium);
                        break;
                    case MsgBean.MSG_TYPE_VOICE:
                        holdView.txt_msgContent.setText(msg_user_nikcname + TheLApp.getContext().getString(R.string.message_info_voice));
                        break;
                    case MsgBean.MSG_TYPE_VIDEO:
                        holdView.txt_msgContent.setText(msg_user_nikcname + TheLApp.getContext().getString(R.string.message_info_video));
                        break;
                    case MsgBean.MSG_TYPE_MATCH:
                        holdView.txt_msgContent.setText(msg_user_nikcname + TheLApp.getContext().getString(R.string.match_perfect_match_line1));
                        holdView.re_wink_or_match.setVisibility(View.VISIBLE);
                        holdView.match_iv.setVisibility(View.VISIBLE);
                        break;
                    case MsgBean.MSG_TYPE_WINK:
                        holdView.txt_msgContent.setText(msg_user_nikcname + TheLApp.getContext().getString(R.string.message_info_wink));
                        break;
                    case MsgBean.MSG_TYPE_FOLLOW:
                        holdView.txt_msgContent.setText(msg_user_nikcname + TheLApp.getContext().getString(R.string.message_info_follow));
                        break;
                    case MsgBean.MSG_TYPE_UNFOLLOW:
                        holdView.txt_msgContent.setText(msg_user_nikcname + TheLApp.getContext().getString(R.string.message_info_unfollow));
                        break;
                    case MsgBean.MSG_TYPE_MAP:
                        holdView.txt_msgContent.setText(msg_user_nikcname + TheLApp.getContext().getString(R.string.message_info_location));
                        break;
                    case MsgBean.MSG_TYPE_SECRET_KEY:
                        holdView.txt_msgContent.setText(msg_user_nikcname + TheLApp.getContext().getString(R.string.message_text_secretKey));
                        break;
                    case MsgBean.MSG_TYPE_HIDDEN_LOVE:
                        holdView.txt_msgContent.setText(msg_user_nikcname + TheLApp.getContext().getString(R.string.message_text_hiddenLove));
                        break;
                    case MsgBean.MSG_TYPE_USER_CARD:
                        holdView.txt_msgContent.setText(msg_user_nikcname + TheLApp.getContext().getString(R.string.message_info_recommend_user));
                        break;
                    case MsgBean.MSG_TYPE_REQUEST:
                        if (msgTable.unreadCount == 1) {
                            holdView.txt_msgContent.setText(String.format(TheLApp.getContext().getString(R.string.message_activity_request_unread_odd), msgTable.unreadCount));
                        } else if (msgTable.unreadCount > 1) {
                            holdView.txt_msgContent.setText(String.format(TheLApp.getContext().getString(R.string.message_activity_request_unread_even), msgTable.unreadCount));
                        } else {
                            holdView.txt_msgContent.setText(TheLApp.getContext().getString(R.string.message_activity_request_read));
                        }
                        break;
                    case MsgBean.MSG_TYPE_LIKE:
                        holdView.txt_msgContent.setText(msgTable.msgText);
                        break;
                    case MsgBean.MSG_TYPE_MOMENT_TO_CHAT:
                        final MomentToChatBean momentToChatBean = GsonUtils.getObject(msgTable.msgText, MomentToChatBean.class);
                        holdView.txt_msgContent.setText(momentToChatBean.chatText);
                        break;
                    case MsgBean.MSG_TYPE_MSG_HIDING:

                        break;
                    case MsgBean.MSG_TYPE_VIP_PUSH:
                        holdView.txt_msgContent.setText(msgTable.msgText);
                        break;
                    case MsgBean.MSG_TYPE_AMUSEMENT_PARK_MSG_TYPE:
                        holdView.txt_msgContent.setText(TheLApp.context.getString(R.string.amusement_park_list_content));
                        break;
                    default:
                        content = msg_user_nikcname + TheLApp.getContext().getString(R.string.info_version_outdate);
                        holdView.txt_msgContent.setText(content);// 未知
                        break;

                }

            }

            long time;

            if (msgTable.msgTime > DBConstant.STICK_TOP_BASE_TIME) {
                time = ChatServiceManager.getInstance().getLastMsgTime(msgTable.userId);
                holdView.root_view.setBackgroundColor(TheLApp.getContext().getResources().getColor(R.color.msg_sticky_top_bg));
            } else {
                time = msgTable.msgTime;
                holdView.root_view.setBackgroundColor(TheLApp.getContext().getResources().getColor(R.color.white));
            }

            holdView.txt_msgTime.setText(getTime(time));

            String unreadCount;
            if (msgTable.unreadCount >= 99) {
                unreadCount = "99+";
            } else {
                unreadCount = String.valueOf(msgTable.unreadCount);
            }
            holdView.txt_unread_count.setText(unreadCount);

            holdView.txt_unread_count.setVisibility(msgTable.unreadCount > 0 ? View.VISIBLE : View.INVISIBLE);

            if (msgTable.isSystem == MsgBean.IS_NORMAL_MSG && msgTable.isWinked == MsgWithUnreadBean.WINK_YES) {
                holdView.re_wink_or_match.setVisibility(View.VISIBLE);
                holdView.img_wink.setVisibility(View.VISIBLE);
                holdView.txt_msgContent.setText(msg_user_nikcname + TheLApp.getContext().getString(R.string.message_info_wink));
                /**4.0.0，显示挤眼图标，最新消息是挤眼且没对它挤过眼,且不是自己发出的消息**/

                if (!WinkUtils.isTodayWinkedUser(msgTable.userId) && !msgTable.msgDirection.equals(MsgBean.MSG_DIRECTION_TYPE_OUTGOING)) {

                    holdView.re_wink_or_match.setVisibility(View.VISIBLE);
                    holdView.img_wink.setVisibility(View.VISIBLE);
                    holdView.img_wink.setImageResource(R.mipmap.icn_chat_wink);
                    AnimUtils.setWnkCtrl(holdView.img_wink, holdView.img_sendwink, msgTable);//能回挤的挤眼消息肯定是对方发过来来消息
                }

                if (msgTable.userId.equals(DBUtils.getStrangerTableName())) {
                    holdView.img_sendwink.setVisibility(View.GONE);
                }
            } else {
                holdView.re_wink_or_match.setVisibility(View.GONE);
                holdView.img_wink.setVisibility(View.GONE);
            }

            if (msgTable.isSystem == MsgBean.IS_NORMAL_MSG && !msgTable.userId.equals(SYSTEM_USER)) {
                holdView.img_thumb.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ViewUtils.preventViewMultipleClick(v, 2000);
                        MsgTable msgTable = mData.get(position);
                        // 系统消息和系统通知账号发的消息不可点击头像
                        if (SYSTEM_USER.equals(msgTable.userId) || DBConstant.Message.MSG_AMUSEMENT_PARK.equals(msgTable.userId)) {
                            return;
                        }
//                    Intent intent = new Intent();
//                    intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, msgTable.userId);
//                    intent.setClass(mContext, UserInfoActivity.class);
//                    mContext.startActivity(intent);
                        FlutterRouterConfig.Companion.gotoUserInfo(msgTable.userId);
                        traceMessageLog("click_message", position, "", msgTable.userId);

                    }
                });
            } else {
                holdView.img_thumb.setOnClickListener(null);
            }

            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) holdView.divider_iv.getLayoutParams();

            int width = ScreenUtils.getScreenWidth(TheLApp.context);

            if (position == getData().size() - 1) {
                params.width = width;
                params.height = 1;
                holdView.divider_iv.setLayoutParams(params);
            } else {
                params.width = width - SizeUtils.dip2px(TheLApp.context, 20);
                params.height = 1;
                holdView.divider_iv.setLayoutParams(params);
            }

            holdView.msg_hiding_close_iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    L.d(TAG, " msg_hiding_close_iv click : ");
                    if (msgTable.userId.equals(DBConstant.Message.MSG_AMUSEMENT_PARK)) {
                        DialogUtil.showConfirmDialog(mContext, mContext.getString(R.string.close_dialog_title), mContext.getString(R.string.close_dialog_message), mContext.getString(R.string.close_dialog_sure), mContext.getString(R.string.close_dialog_cancel), (DialogInterface.OnClickListener) (dialog, which) -> {
                            dialog.dismiss();
                            if (which == BUTTON_POSITIVE) {
                                if (mData != null) {
                                    for (MsgTable mt : mData) {
                                        if (msgTable.userId.endsWith(mt.userId)) {
                                            mData.remove(mt);
                                            ChatServiceManager.getInstance().deleteMsgTable(msgTable.userId);
                                            break;
                                        }
                                    }
                                }

                                notifyDataSetChanged();

                                int unreadCount = 0;
                                for (MsgTable msgTable : mData) {
                                    unreadCount = msgTable.unreadCount + unreadCount;
                                }
                                FlutterPushImpl.Companion.getMomentsCheckBean().notReadChat = unreadCount;
                                RfSBridgeHandlerFactory.getInstance().getFlutterPushImpl().pushNotRead();
                            }
                        });
                    } else {
                        if (mData != null) {
                            for (MsgTable mt : mData) {
                                if (msgTable.userId.endsWith(mt.userId)) {
                                    mData.remove(mt);
                                    ChatServiceManager.getInstance().deleteMsgTable(msgTable.userId);
                                    break;
                                }
                            }
                        }

                        notifyDataSetChanged();

                        ShareFileUtils.setBoolean(ShareFileUtils.IS_CLOSE_MSG_HIDING, true);
                        ShareFileUtils.setLong(ShareFileUtils.MSG_HIDING_CLOSE_TIME, System.currentTimeMillis());
                    }
                }
            });


            if (mOnItemClickListener != null) {
                //为ItemView设置监听器
                holdView.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int position = holdView.getLayoutPosition(); // 1
                        mOnItemClickListener.onItemClick(holdView.itemView, position, msgTable); // 2
                    }
                });
            }
            if (mOnItemLongClickListener != null) {
                holdView.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        int position = holdView.getLayoutPosition();
                        mOnItemLongClickListener.onItemLongClick(holdView.itemView, position, msgTable);
                        //返回true 表示消耗了事件 事件不会继续传递
                        return true;
                    }
                });
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void traceMessageLog(String activity, int index, String rank_id, String userid) {
        try {
            String latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
            String longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");

            LogInfoBean logInfoBean = new LogInfoBean();
            logInfoBean.page = "message.list";
            logInfoBean.page_id = pageId;
            logInfoBean.activity = activity;
            logInfoBean.from_page = from_page;
            logInfoBean.from_page_id = from_page_id;

            logInfoBean.lat = latitude;
            logInfoBean.lng = longitude;

            LogInfoBean.LogsDataBean logsDataBean = new LogInfoBean.LogsDataBean();

            if (index != -1) {
                logsDataBean.index = index;
            }

            if (!TextUtils.isEmpty(rank_id)) {
                logsDataBean.rank_id = rank_id;
            }

            if (!TextUtils.isEmpty(userid)) {
                logsDataBean.user_id = userid;
            }
            logInfoBean.data = logsDataBean;

            LiveLogUtils.getInstance().addLog(logInfoBean);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    private void initData(List<MsgTable> data) {
        if (data == null)
            data = new ArrayList<>();
        mData = data;

    }

    public void refreshData(List<MsgTable> data) {
        if (data == null)
            data = new ArrayList<>();
        mData = data;

        notifyDataSetChanged();
    }

    public void removeData(MsgTable msgTable) {
        if (mData != null) {
            mData.remove(msgTable);
            notifyDataSetChanged();
        }
    }

    public MsgTable getItem(int position) {
        return mData.get(position);
    }

    private void printMsg(String msg) {
        L.d(TAG, msg);
    }

    private String getTime(Long milliseconds) {
        String todySDF = "HH:mm";
        String yesterDaySDF = "'" + TheLApp.getContext().getString(R.string.chat_activity_yesterday) + "' HH:mm";
        String otherSDF = "MM-dd";

        SimpleDateFormat sfd = null;
        String time = "";
        Calendar dateCalendar = Calendar.getInstance();
        dateCalendar.setTimeInMillis(milliseconds);
        Long nowMilliseconds = System.currentTimeMillis();
        Calendar targetCalendar = Calendar.getInstance();
        targetCalendar.setTimeInMillis(nowMilliseconds);
        targetCalendar.set(Calendar.HOUR_OF_DAY, 0);
        targetCalendar.set(Calendar.MINUTE, 0);
        if (dateCalendar.after(targetCalendar)) {
            sfd = new SimpleDateFormat(todySDF);
            time = sfd.format(milliseconds);
            return time;
        }
        sfd = new SimpleDateFormat(otherSDF);
        time = sfd.format(milliseconds);
        return time;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        SimpleDraweeView img_thumb;
        TextView txt_nickname;
        TextView txt_msgTime;
        TextView txt_msgContent;
        TextView txt_unread_count;
        ImageView img_wink;//4.0.0，挤眼图标，还包括匹配等
        LottieAnimationView img_sendwink;
        RelativeLayout re_wink_or_match;
        RelativeLayout msg_hiding_close_rl;
        ImageView msg_hiding_close_iv;
        LinearLayout root_view;
        ImageView match_iv;
        ImageView divider_iv;

        public ViewHolder(View convertView) {
            super(convertView);
            img_thumb = convertView.findViewById(R.id.img_thumb);
            txt_nickname = convertView.findViewById(R.id.txt_nickname);
            txt_msgTime = convertView.findViewById(R.id.txt_msgTime);
            txt_msgContent = convertView.findViewById(R.id.txt_msgContent);
            txt_unread_count = convertView.findViewById(R.id.txt_unread_count);
            img_wink = convertView.findViewById(R.id.img_wink);
            img_sendwink = convertView.findViewById(R.id.img_sendwink);
            re_wink_or_match = convertView.findViewById(R.id.rl_wink_or_match);
            msg_hiding_close_rl = convertView.findViewById(R.id.msg_hiding_close_rl);
            msg_hiding_close_iv = convertView.findViewById(R.id.msg_hiding_close_iv);
            root_view = convertView.findViewById(R.id.root_view);
            match_iv = convertView.findViewById(R.id.match_iv);
            divider_iv = convertView.findViewById(R.id.divider_iv);
        }
    }

    private OnItemClickListener mOnItemClickListener;

    private OnItemLongClickListener mOnItemLongClickListener;

    public void setOnItemClickListener(OnItemClickListener mOnItemClickListener) {
        this.mOnItemClickListener = mOnItemClickListener;
    }

    public void setOnItemLongClickListener(OnItemLongClickListener mOnItemLongClickListener) {
        this.mOnItemLongClickListener = mOnItemLongClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position, MsgTable item);
    }

    public interface OnItemLongClickListener {
        void onItemLongClick(View view, int position, MsgTable item);
    }
}
