package com.thel.modules.live.surface;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.netease.LDNetDiagnoService.LDNetSocket;
import com.thel.BuildConfig;
import com.thel.bean.LiveAdInfoBean;
import com.thel.modules.live.bean.LiveTopFansTodayBean;
import com.thel.modules.live.bean.LiveUserCardNetBean;
import com.thel.modules.live.bean.SoftEnjoyNetBean;
import com.thel.modules.live.bean.SoftGiftBean;
import com.thel.modules.live.bean.SoftGiftNetBean;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestConstants;
import com.thel.network.service.DefaultRequestService;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * @author waiarl
 * @date 2017/10/27
 */

public class LiveShowPresenter implements LiveShowContract.Presenter {

    public static final String BR_SHARE = "br_share";

    public static final String BR_RECOMMEND = "br_recommend";

    private final LiveShowContract.View view;

    private int anchor_card = 1;

    private int user_card = 0;

    private ShareBroadcastReceiver mShareBroadcastReceiver;

    public LiveShowPresenter(LiveShowContract.View view) {
        this.view = view;
        view.setPresenter(this);
    }

    @Override
    public void getLiveUserCard(String userId) {
        getLiveUserCard(userId, user_card);
    }

    @Override
    public void getLiveAnchorUserCard(String userId) {
        getLiveUserCard(userId, anchor_card);
    }

    @Override
    public void getAnchorTopFans(String userId, int arriveToTopfans) {
        getTopFans(userId, arriveToTopfans);
    }

    @Override
    public void registerReceiver(Activity activity) {

        L.d("LiveShowPresenter", " registerReceiver : ");

        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(activity);

        IntentFilter filter = new IntentFilter();
        filter.addAction(BR_SHARE);
        filter.addAction(BR_RECOMMEND);
        mShareBroadcastReceiver = new ShareBroadcastReceiver();
        localBroadcastManager.registerReceiver(mShareBroadcastReceiver, filter);
    }

    @Override
    public void unRegister(Activity activity) {

        L.d("LiveShowPresenter", " unRegister : ");

        if (mShareBroadcastReceiver != null) {
            LocalBroadcastManager.getInstance(activity).unregisterReceiver(mShareBroadcastReceiver);
        }
    }

    private void getTopFans(String userId, final int arriveToTopfans) {
        final Flowable<LiveTopFansTodayBean> flowable = DefaultRequestService.createLiveRequestService().getTopFans(userId);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<LiveTopFansTodayBean>() {
            @Override
            public void onNext(LiveTopFansTodayBean topFansBean) {
                super.onNext(topFansBean);
                view.refreshTopFans(topFansBean, arriveToTopfans);
            }
        });
    }

    private void getLiveUserCard(String userId, final int type) {
        final Flowable<LiveUserCardNetBean> flowable = DefaultRequestService.createLiveRequestService().getLiveUserCardBean(userId);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<LiveUserCardNetBean>() {
            @Override
            public void onNext(LiveUserCardNetBean liveUserCardNetBean) {
                super.onNext(liveUserCardNetBean);
                if (type == anchor_card) {
                    view.showLiveAnchorUserCard(liveUserCardNetBean.data);
                } else {
                    view.showLiveUserCard(liveUserCardNetBean.data);

                }
            }
        });
    }

    @Override
    public void getLiveGiftList() {

        String etag = ShareFileUtils.getString(ShareFileUtils.GIFT_ETAG, "");

        etag = "";

        final Flowable<SoftEnjoyNetBean> flowable = DefaultRequestService.createLiveRequestService().getLiveGiftList(etag, BuildConfig.APPLICATION_ID);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<SoftEnjoyNetBean>() {
            @Override
            public void onNext(SoftEnjoyNetBean data) {

                if (data != null && data.data != null) {

                    L.d("LiveShowPresenter", " data.data : " + data.data.list.toString());

                    ShareFileUtils.setString(ShareFileUtils.GIFT_ETAG, data.data.md5);

                    if (data.data.list != null) {
                        if (data.data.list.size() <= 0) {
                            String json = ShareFileUtils.getString(ShareFileUtils.GIFT_LIST, "{}");
                            SoftEnjoyNetBean softEnjoyNetBean = GsonUtils.getObject(json, SoftEnjoyNetBean.class);
                            if (softEnjoyNetBean != null && softEnjoyNetBean.data != null) {
                                view.showLiveGiftList(softEnjoyNetBean.data);
                            }
                        } else {
                            String json = GsonUtils.createJsonString(data);
                            ShareFileUtils.setString(ShareFileUtils.GIFT_LIST, json);
                            view.showLiveGiftList(data.data);
                        }
                    }

                }


            }
        });
    }

    @Override
    public void getSingleGiftDetail(int id) {
        final Flowable<SoftGiftNetBean> flowable = DefaultRequestService.createLiveRequestService().getSingleLiveGiftList(id);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<SoftGiftNetBean>() {
            @Override
            public void onNext(SoftGiftNetBean data) {

                if (data != null && data.data != null) {
                    final SoftGiftBean softGiftBean = data.data;
                    view.addOfflineGift(softGiftBean);
                }
            }
        });
    }

    @Override
    public void getLiveAdInfo(String url) {

        Flowable.just(url)
                .onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .map(new Function<String, String>() {
                    @Override
                    public String apply(String s) {
                        return executeHttpGet(s);
                    }
                }).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<String>() {
                    @Override
                    public void onSubscribe(Subscription s) {
                        s.request(Integer.MAX_VALUE);
                    }

                    @Override
                    public void onNext(String s) {
                        LiveAdInfoBean liveAdInfoBean = GsonUtils.getObject(s, LiveAdInfoBean.class);
                        view.showAdView(liveAdInfoBean);
                        L.d("直播广告显示s" + s);

                    }

                    @Override
                    public void onError(Throwable t) {
                        L.d("直播广告显示onerror" + t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }

    @Override
    public void unBindView() {

    }

    @Override
    public void reBindView(LiveShowContract.View view) {

    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {

    }

    public String executeHttpGet(String str) {
        String result = null;
        try {
            URL url = new URL(str);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            String host = url.getHost();
            conn.setRequestProperty("Origin", "http://www.rela.me");
            conn.setRequestProperty("Host", host);
            conn.setRequestMethod("GET");
            conn.setConnectTimeout(15000);
            int code = conn.getResponseCode();
            if (code == 200) {
                InputStream is = conn.getInputStream();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                int len = 0;
                byte[] bys = new byte[1024];
                while ((len = is.read(bys)) != -1) {
                    baos.write(bys, 0, len);
                    result = new String(baos.toByteArray());
                }
                baos.close();
                is.close();
            }
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    class ShareBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            L.d("LiveShowPresenter", " action : " + action);

            if (action != null) {

                switch (action) {
                    case BR_SHARE:
                        if (view != null) {
                            view.sendShareMsg();
                        }
                        break;
                    case BR_RECOMMEND:
                        if (view != null) {
                            view.sendRecommendMsg();
                        }
                        break;
                    default:
                        break;
                }
            }

        }
    }

}
