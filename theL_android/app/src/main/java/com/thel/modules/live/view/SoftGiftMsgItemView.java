package com.thel.modules.live.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.constants.TheLConstants;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.live.bean.GiftMsgShowListBean;
import com.thel.modules.live.bean.GiftSendMsgBean;
import com.thel.ui.widget.UserLevelImageView;
import com.thel.utils.L;
import com.thel.utils.UserUtils;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by the L on 2016/5/27.
 * 赠送礼物单个item 布局View
 */
public class SoftGiftMsgItemView extends RelativeLayout {
    private Context mContext;
    private RelativeLayout rel_gift_sender_msg;//赠送人信息背景
    private ImageView img_sender;//赠送人头像
    private TextView txt_sender_name;//赠送人昵称
    private TextView txt_msg;//赠送的礼物信息
    private LinearLayout lin_gift_msg;//礼物信息背景
    private ImageView img_gift_icon;//礼物图标
    private SoftGiftMsgItemCountView lin_gift_count;//礼物数量布局
    private TextView txt_gift_count;//礼物数量
    private View img_x;//乘以  符号
    private SimpleDraweeView gift_count_bg_iv;

    private boolean isAdd = false;//是否已经被添加到布局，默认为false
    private GiftMsgShowListBean listBean;//要显示的礼物信息对象队列集合
    private ShowFinishCallBack callBack;//显示完毕接口回调
    private boolean isWaiting = false;//是否在等待数据；
    private Timer timer;//计时器，用于等待时间
    private MyTimeTask myTimeTask;//等待时间后的工作
    private final int WAIT_FINISH = 1;//等待结束
    private UserLevelImageView img_level;
    private ImageView to_user_avatar_iv;
    private boolean isDispatchTouchEvent = false;


    public SoftGiftMsgItemView(Context context) {
        this(context, null);
    }

    public SoftGiftMsgItemView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SoftGiftMsgItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch ((msg.what)) {
                case WAIT_FINISH://等待结束
                    if (listBean.msgList.size() > 0) {//如果有，则刷新连击
                        showCombo();
                    } else if (isWaiting) {//如果2.5秒后还在等待，说明连击中断
                        if (callBack != null) {
                            callBack.showFinish(SoftGiftMsgItemView.this, listBean);//连击中断，显示完毕
                        }
                    }
            }
        }
    };

    private void init() {
        inflate(mContext, R.layout.live_gift_send_layout_item, this);
        setLayerType(View.LAYER_TYPE_HARDWARE, null);
        rel_gift_sender_msg = findViewById(R.id.rel_gift_sender_msg);
        img_sender = findViewById(R.id.img_sender);
        txt_sender_name = findViewById(R.id.txt_sender_name);
        txt_msg = findViewById(R.id.txt_msg);
        lin_gift_msg = findViewById(R.id.lin_gift_msg);
        img_gift_icon = findViewById(R.id.img_gift_icon);
        lin_gift_count = findViewById(R.id.lin_gift_count);
        rel_gift_sender_msg.getBackground().setAlpha(186);
        gift_count_bg_iv = findViewById(R.id.gift_count_bg_iv);
        //4.6.0新增用户等级
        img_level = findViewById(R.id.img_level);
        to_user_avatar_iv = findViewById(R.id.to_user_avatar_iv);
    }

    /**
     * 初始化
     *
     * @param listBean 初始化的时候ListBean中的giftsendMsgbean集合必定不为0
     */
    public void initView(GiftMsgShowListBean listBean) {
        this.listBean = listBean;
        listBean.setShow(true);

        ImageLoaderManager.imageLoaderDefaultCircle(img_sender, R.mipmap.icon_user, listBean.avatar, TheLConstants.AVATAR_SMALL_SIZE, TheLConstants.AVATAR_SMALL_SIZE);

        if (listBean.type == GiftSendMsgBean.GIFT_TYPE_GUEST) {
            to_user_avatar_iv.setVisibility(VISIBLE);
            ImageLoaderManager.imageLoaderDefaultCircle(to_user_avatar_iv, R.mipmap.icon_user, listBean.toAvatar, TheLConstants.AVATAR_SMALL_SIZE, TheLConstants.AVATAR_SMALL_SIZE);
            String content;
            if (UserUtils.getMyUserId().equals(listBean.toUserId)) {
                content = listBean.action;
                to_user_avatar_iv.setVisibility(GONE);
            } else {
                content = String.format(getContext().getResources().getString(R.string.send_gift_to_somebody), listBean.toNickname, String.valueOf(listBean.combo), listBean.giftName);
            }
            txt_msg.setText(content);

        } else {
            to_user_avatar_iv.setVisibility(GONE);
            txt_msg.setText(listBean.action);
        }

        isDispatchTouchEvent = listBean.isMultiConnectMic;

        txt_sender_name.setText(listBean.nickName);
        final float size = TheLApp.getContext().getResources().getDimension(R.dimen.gift_icon_size);
        ImageLoaderManager.imageLoader(img_gift_icon, listBean.icon, (int) size, (int) size);
        lin_gift_count.refreshCombo(listBean.msgList.get(0).combo);
        timer = new Timer();
        listBean.setDataAddedListener(new GiftMsgShowListBean.DataAddedCallBack() {
            @Override
            public void remindDataAdded() {
                if (isWaiting) {//有新数据加入并且是等待显示的时候
                    isWaiting = false;
                    showCombo();
                }
            }
        });
        setLevelView();
    }

    /**
     * 显示连击数
     */
    public void showCombo() {

        //如果定时器还在自行，则取消
        if (myTimeTask != null) {
            myTimeTask.cancel();
            myTimeTask = null;
        }
        isWaiting = false;
        handler.removeMessages(WAIT_FINISH);
        lin_gift_count.refreshCombo(listBean.msgList.get(0).combo);

        ImageLoaderManager.imageLoaderGifAssents(gift_count_bg_iv, "numbermov.gif");

        playSmallGiftAnimator();

    }

    private ValueAnimator mValueAnimator = null;

    private void playSmallGiftAnimator() {

        if (mValueAnimator != null) {
            mValueAnimator.cancel();
            mValueAnimator = null;
        }

        lin_gift_count.setLayerType(View.LAYER_TYPE_HARDWARE, null);

        mValueAnimator = ValueAnimator.ofFloat(1f, 2f, 1f);
        mValueAnimator.setDuration(400);
        mValueAnimator.setInterpolator(new DecelerateInterpolator());
        mValueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float scale = (float) animation.getAnimatedValue();
                lin_gift_count.setScaleX(scale);
                lin_gift_count.setScaleY(scale);
            }
        });
        mValueAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                refreshCombo();
                lin_gift_count.setLayerType(View.LAYER_TYPE_NONE, null);

            }
        });
        mValueAnimator.start();

    }

    /**
     * 刷新连击
     */
    private void refreshCombo() {
        listBean.removeTop();
        if (listBean.msgList.size() > 0) {//如果消息队列中还有消息
            handler.post(new Runnable() {
                @Override
                public void run() {
                    showCombo();//显示连击数据
                }
            });//0.3秒后显示连击
        } else {//如果没有数据
            handler.post(new Runnable() {//如果没有，则等待0.3秒后再次判断
                @Override
                public void run() {
                    if (listBean.msgList.size() > 0) {//如果有，则刷新连击
                        showCombo();
                        return;
                    } else {
                        isWaiting = true;//如果没有，则处于等待中,
                        if (myTimeTask != null) {
                            myTimeTask.cancel();
                            myTimeTask = null;
                        }
                        myTimeTask = new MyTimeTask();
                        timer.schedule(myTimeTask, 2000);//等待2.2秒
                    }
                }
            });

        }
    }

    /**
     * 显示完毕接口回调
     */
    public interface ShowFinishCallBack {
        void showFinish(View v, GiftMsgShowListBean msgShowListBean);
    }

    public void setShowFinishListener(ShowFinishCallBack callBack) {
        this.callBack = callBack;
    }

    /**
     * 设置被添加
     *
     * @param bo
     */
    public void setAdd(boolean bo) {
        this.isAdd = bo;
    }

    public boolean isAdd() {
        return isAdd;
    }

    class MyTimeTask extends TimerTask {

        @Override
        public void run() {
            handler.sendEmptyMessage(WAIT_FINISH);//等待结束；
        }
    }

    /**
     * 礼物的打赏点击事件
     */
    public void setOnSenderClickListener(OnClickListener listener) {
        rel_gift_sender_msg.setOnClickListener(listener);
    }

    /**
     * 设置用户等级
     */
    private void setLevelView() {
        final int level = listBean.userLevel;
        img_level.initView(level);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return isDispatchTouchEvent;
    }
}
