package com.thel.modules.live.liveBigGiftAnimLayout.anim;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.thel.R;
import com.thel.modules.live.liveBigGiftAnimLayout.FrameAnimAssetSurfaceView;

/**
 * Created by test1 on 17/1/10.
 */

public class FireworkAnimator {
    private final Context mContext;
    private final int mDuration;
    private final Handler mHanler;
    private final int rate;
    private final String foldPath;
    private int mWidth;
    private int mHeight;
    // private final int[] resId;
    private final String[] resId;

    public FireworkAnimator(Context context, int duration) {
        this.mContext = context;
        this.mDuration = duration;
        mHanler = new Handler(Looper.getMainLooper());
        // resId = new int[]{R.drawable.firework1, R.drawable.firework2, R.drawable.firework3, R.drawable.firework4, R.drawable.firework5, R.drawable.firework6, R.drawable.firework7, R.drawable.firework8, R.drawable.firework9, R.drawable.firework10, R.drawable.firework11, R.drawable.firework12, R.drawable.firework13, R.drawable.firework14, R.drawable.firework15, R.drawable.firework16, R.drawable.firework17, R.drawable.firework18, R.drawable.firework19, R.drawable.firework20, R.drawable.firework21, R.drawable.firework22, R.drawable.firework23, R.drawable.firework24, R.drawable.firework25, R.drawable.firework19, R.drawable.firework20, R.drawable.firework21, R.drawable.firework22, R.drawable.firework23, R.drawable.firework25, R.drawable.firework19, R.drawable.firework20, R.drawable.firework21, R.drawable.firework22, R.drawable.firework23, R.drawable.firework25};
        foldPath = "anim/firework";
        resId = new String[]{"firework1", "firework2", "firework3", "firework4", "firework5", "firework6", "firework7", "firework8", "firework9", "firework10", "firework11", "firework12", "firework13", "firework14", "firework15", "firework16", "firework17", "firework18", "firework19", "firework20", "firework21", "firework22", "firework23", "firework24", "firework25", "firework19", "firework20", "firework21", "firework22", "firework23", "firework25", "firework19", "firework20", "firework21", "firework22", "firework23", "firework25"};

        rate = 200;
    }

    public int start(final ViewGroup parent) {
        mWidth = parent.getMeasuredWidth();
        mHeight = parent.getMeasuredHeight();
        if (mWidth == 0 || mHeight == 0) {
            return 0;
        }
        final RelativeLayout background = (RelativeLayout) RelativeLayout.inflate(mContext, R.layout.live_big_anim_firework_layout, null);
        background.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
        parent.addView(background);
        final int ws = View.MeasureSpec.makeMeasureSpec(mWidth, View.MeasureSpec.EXACTLY);
        final int hs = View.MeasureSpec.makeMeasureSpec(mHeight, View.MeasureSpec.EXACTLY);
        background.measure(ws, hs);
        setBackground(background);

        mHanler.postDelayed(new Runnable() {
            @Override
            public void run() {
                background.clearAnimation();
                parent.removeView(background);
            }
        }, mDuration);
        return mDuration;
    }

    private void setBackground(RelativeLayout background) {
        final FrameAnimAssetSurfaceView surfaceView = background.findViewById(R.id.firework);
        ViewGroup.LayoutParams params = surfaceView.getLayoutParams();
        final float scale = Math.min(mWidth / 750f, mHeight / 1334f);
        params.width = (int) (750 * scale);
        params.height = (int) (1334 * scale);
        surfaceView.initView(background, foldPath, resId, rate, true);
        mHanler.postDelayed(new Runnable() {
            @Override
            public void run() {
                surfaceView.start();
            }
        }, 100);
    }
}
