package com.thel.modules.login.init_info;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import androidx.core.content.ContextCompat;
import android.text.Editable;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UploadManager;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.bean.user.UploadTokenBean;
import com.thel.constants.QueueConstants;
import com.thel.constants.TheLConstants;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.login.LoginActivityManager;
import com.thel.modules.main.MainActivity;
import com.thel.modules.main.me.bean.RoleBean;
import com.thel.modules.main.me.bean.UpdataInfoBean;
import com.thel.modules.select_image.SelectLocalImagesActivity;
import com.thel.network.LoginInterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.RequestConstants;
import com.thel.network.api.loginapi.bean.SignInBean;
import com.thel.ui.imageviewer.cropiwa.image.CropIwaResultReceiver;
import com.thel.ui.widget.flowlayout.FlowLayout;
import com.thel.ui.widget.flowlayout.TagAdapter;
import com.thel.ui.widget.flowlayout.TagFlowLayout;
import com.thel.utils.DialogUtil;
import com.thel.utils.ImageUtils;
import com.thel.utils.L;
import com.thel.utils.MD5Utils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SizeUtils;
import com.thel.utils.ToastUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.thel.utils.DeviceUtils.getFilePathFromUri;
import static com.thel.utils.ShareFileUtils.IS_JUMP_UPDATE_USERINFO_ACTIVITY;
import static com.thel.utils.ShareFileUtils.IS_RELEASE_MOMENT;

/**
 * 初始化用户，填头像、昵称、thelID
 *
 * @author Setsail
 */
public class InitUserInfoActivity extends BaseActivity implements View.OnClickListener, TextWatcher {

    @BindView(R.id.tv_tips)
    TextView tv_tips;

    @BindView(R.id.txt_title)
    TextView txt_title;

    @BindView(R.id.button_login)
    Button button_login;

    @BindView(R.id.lin_back)
    LinearLayout lin_back;

    @BindView(R.id.lin_user_id)
    LinearLayout lin_user_id;

    @BindView(R.id.lin_nickname)
    LinearLayout lin_nickname;

    @BindView(R.id.img_avatar)
    ImageView img_avatar;

    @BindView(R.id.edit_name)
    EditText edit_name;

    @BindView(R.id.edit_user_name)
    EditText edit_user_name;

    @BindView(R.id.lin_user_role)
    LinearLayout lin_user_role;

    @BindView(R.id.edit_user_role)
    TextView edit_user_role;

    @BindView(R.id.flowLayout)
    TagFlowLayout mFlowLayout;

    private SignInBean userBean = null;

    // 要上传的头像的图片的本地地址
    private String avatarPhotoPath = "";
    // 原来的头像URL
    private String avatarUrl = "";

    // 上传头像图片到七牛后的文件路径
    private String uploadAvatarPath = "";
    private String uploadAvatarRequestMD5 = "";

    private String takePhotoPath;
    private String role;

    private String roleTag;
    private CropIwaResultReceiver cropResultReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LoginActivityManager.getInstanse().addActivity(this);

        setContentView(R.layout.activity_init_user_info);
        ButterKnife.bind(this);

        setTag();

        Log.d("InitUserInfoActivity", "onCreate");

        img_avatar.setOnClickListener(this);
        lin_nickname.setOnClickListener(this);
        lin_user_id.setOnClickListener(this);
        lin_user_role.setOnClickListener(this);
        button_login.setOnClickListener(this);
        edit_user_name.addTextChangedListener(this);
        edit_name.addTextChangedListener(this);

        initData();

        L.d("facebook", "onCreate InitUserInfoActivity");
        ShareFileUtils.setBoolean(IS_JUMP_UPDATE_USERINFO_ACTIVITY, false);
        ShareFileUtils.setBoolean(IS_RELEASE_MOMENT, false);

        cropResultReceiver = new CropIwaResultReceiver();
        cropResultReceiver.register(this);
        cropResultReceiver.setListener(new CropIwaResultReceiver.Listener() {
            @Override
            public void onCropSuccess(Uri croppedUri) {
                ImageLoaderManager.imageLoader(img_avatar, R.mipmap.btn_add_headsculpture_blue, croppedUri);
                avatarPhotoPath = getFilePathFromUri(InitUserInfoActivity.this, croppedUri);
                // 置为非空
                avatarUrl = " ";
                if (!checkIsEmpty()) {
                    button_login.setEnabled(true);
                }
                checkTips();
            }

            @Override
            public void onCropFailed(Throwable e) {
                ToastUtils.showToastShort(InitUserInfoActivity.this, "crop failed");
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cropResultReceiver.unregister(this);
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void initData() {
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (intent.getExtras() != null) {
            if (bundle.getSerializable(TheLConstants.BUNDLE_KEY_USER_BEAN) != null) {
                userBean = (SignInBean) bundle.getSerializable(TheLConstants.BUNDLE_KEY_USER_BEAN);
                avatarUrl = userBean.data.user.avatar;
            }
        }
        txt_title.setText(TheLApp.getContext().getString(R.string.updatauserinfo_activity_info));
        // 设置默认值
        if (userBean != null) {
            edit_name.setText(TextUtils.isEmpty(userBean.data.user.nickName) ? "" : userBean.data.user.nickName);
            if (!TextUtils.isEmpty(userBean.data.user.avatar)) {
                img_avatar.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(userBean.data.user.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE)));
            }
        }
        lin_back.setVisibility(View.GONE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(this.getClass().getName());
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(this.getClass().getName());
        MobclickAgent.onPause(this);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void finish() {
        ViewUtils.hideSoftInput(this, edit_name);
        super.finish();
    }

    @SuppressLint("NewApi")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == TheLConstants.RESULT_CODE_SELECT_TYPE) {
            role = data.getStringExtra("role");
            int position = data.getIntExtra("position", -1);
            ShareFileUtils.setInt(ShareFileUtils.rolePosition, position);
            if (!TextUtils.isEmpty(role)) {
                edit_user_role.setText(role);
                edit_user_role.setTextColor(ContextCompat.getColor(this, R.color.black));
                if (!checkIsEmpty()) {
                    button_login.setEnabled(true);
                } else {
                    button_login.setEnabled(false);

                }
            }
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_avatar:
                ViewUtils.preventViewMultipleClick(v, 2000);
                Intent intent = new Intent(InitUserInfoActivity.this, SelectLocalImagesActivity.class);
                intent.putExtra("isAvatar", true);
                startActivityForResult(intent, TheLConstants.BUNDLE_CODE_UPDATE_USER_INFO_ACTIVITY);
                break;
            case R.id.lin_nickname:
                if (edit_name != null) {
                    edit_name.requestFocus();
                    ViewUtils.showSoftInput(InitUserInfoActivity.this, edit_name);
                }
                break;
            case R.id.lin_user_id:
                if (edit_user_name != null) {
                    edit_user_name.requestFocus();
                    ViewUtils.showSoftInput(InitUserInfoActivity.this, edit_user_name);
                }
                break;
            case R.id.lin_user_role:
                startActivityForResult(new Intent(this, InitRoleActivity.class), TheLConstants.BUNDLE_CODE_ROLE_ACTIVITY);
                //   startActivity(new Intent(this,InitRoleActivity.class));
                break;
            case R.id.button_login:
                ViewUtils.preventViewMultipleClick(v, 2000);
//                String userName = edit_user_name.getText().toString().trim();
//                // 校验用户名格式
//                if (!userName.matches("[_A-Za-z0-9]{6,20}")) {
//                    DialogUtil.showToastShort(InitUserInfoActivity.this, getString(R.string.updatauserinfo_activity_username_illegal));
//                    return;
//                }
                if (!checkIsEmpty()) {
                    showLoadingNoBack();
                    MobclickAgent.onEvent(TheLApp.getContext(), "begin_check_rela_id");// 开始调用校验relaid是否重复接口
                    // 用户名重复校验
//                    RequestBusiness.getInstance().checkUserName(userName.toLowerCase()).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new LoginInterceptorSubscribe<CheckUserNameBean>() {
//                        @Override
//                        public void onNext(CheckUserNameBean data) {
//                            super.onNext(data);
//                            if (data.data.exists) {
//                                Toast.makeText(InitUserInfoActivity.this, getString(R.string.updatauserinfo_activity_username_exists_suggest), Toast.LENGTH_SHORT).show();
//                                closeLoading();
//                                setSuggestUserName();
//                                return;
//                            }
                    submit();
//                        }
//                    });
                }
                break;
        }
    }

    // 表单字段校验
    private boolean checkIsEmpty() {
        String userName = edit_name.getText().toString().trim();
        return TextUtils.isEmpty(avatarUrl) || TextUtils.isEmpty(userName) || userName.length() > 12 || TextUtils.isEmpty(roleTag);
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    tv_tips.setVisibility(View.VISIBLE);
                    tv_tips.setText(R.string.init_avatar);
                    if (TextUtils.isEmpty(avatarUrl))
                        img_avatar.setImageResource(R.mipmap.btn_add_headsculpture_yellow);
                }
            });
        }
    };

    private Handler handler = new Handler();

    private void checkTips() {
        handler.removeCallbacks(runnable);
        if (edit_name.length() > 12) {
            tv_tips.setVisibility(View.VISIBLE);
            tv_tips.setText(R.string.init_nickname_max);
            lin_nickname.setBackgroundResource(R.drawable.bg_border_corner_button_gray_2_stroke_shape);
            if (TextUtils.isEmpty(avatarUrl))
                img_avatar.setImageResource(R.mipmap.btn_add_headsculpture_blue);
        } else if (edit_name.length() == 0) {
            tv_tips.setVisibility(View.INVISIBLE);
            lin_nickname.setBackgroundResource(R.drawable.bg_border_corner_button_gray_2_shape);
            if (TextUtils.isEmpty(avatarUrl))
                img_avatar.setImageResource(R.mipmap.btn_add_headsculpture_blue);
        } else {
            lin_nickname.setBackgroundResource(R.drawable.bg_border_corner_button_gray_2_shape);
            if (TextUtils.isEmpty(avatarUrl)) {
                handler.postDelayed(runnable, 500);
            } else {
                tv_tips.setVisibility(View.INVISIBLE);
            }
        }
    }

    /**
     * 设置建议rela id
     */
    private void setSuggestUserName() {
        Random mRandom = new Random();
        final String st = edit_user_name.getText().toString().trim();
        final String str = st + mRandom.nextInt(10) + mRandom.nextInt(10) + mRandom.nextInt(10);
        edit_user_name.setText(str);
    }

    private void submit() {
        if (!TextUtils.isEmpty(avatarPhotoPath)) {// 上传头像
            uploadAvatarPath = RequestConstants.UPLOAD_FILE_ROOT_PATH_AVATAR + ShareFileUtils.getString(ShareFileUtils.ID, "") + "/" + MD5Utils.calculateMD5(new File(avatarPhotoPath)) + ".jpg";
            RequestBusiness.getInstance().getUploadToken(System.currentTimeMillis() + "", "", uploadAvatarPath).onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new MyConsumer(uploadAvatarPath, avatarPhotoPath));
        } else {
            submitUserInfo();
        }
    }

    private void submitUserInfo() {
        String userName = edit_user_name.getText().toString().trim();
        String nickname = edit_name.getText().toString().trim();
        String role = edit_user_role.getText().toString().trim();

        Log.d("InitUserInfoActivity", " avatarUrl : " + avatarUrl);

        MobclickAgent.onEvent(TheLApp.getContext(), "begin_request_commitInfo_interface");
        RequestBusiness.getInstance().initUserInfo(roleTag, userName.toLowerCase(), avatarUrl, nickname)
                .onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new LoginInterceptorSubscribe<UpdataInfoBean>() {

                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                        closeLoading();
                    }

                    @Override
                    public void onNext(UpdataInfoBean data) {
                        super.onNext(data);
                        closeLoading();

                        Toast.makeText(InitUserInfoActivity.this, getString(R.string.login_activity_success), Toast.LENGTH_SHORT).show();

                        if (data != null) {
                            ShareFileUtils.setString(ShareFileUtils.NO_RATIO, String.valueOf(data.data.ratio));
                        }

                        L.d("InitUserInfoActivity", " Utils.getMyUserId() : " + Utils.getMyUserId());

                        ShareFileUtils.setBoolean(Utils.getMyUserId() + "_" + IS_JUMP_UPDATE_USERINFO_ACTIVITY, false);
                        ShareFileUtils.setBoolean(Utils.getMyUserId() + "_" + IS_RELEASE_MOMENT, false);
                        ShareFileUtils.setBoolean(ShareFileUtils.IS_FIRST_START, true);
                        ShareFileUtils.setBoolean(Utils.getMyUserId() + "_" + ShareFileUtils.IS_SHOW_GUIDE_LAYOUT, true);

                        // 保存昵称、thelID、头像地址
                        ShareFileUtils.setString(ShareFileUtils.USER_NAME, edit_name.getText().toString().trim());
//                ShareFileUtils.setString(ShareFileUtils.USER_THEL_ID, edit_user_name.getText().toString().trim().toLowerCase());
                        ShareFileUtils.setString(ShareFileUtils.AVATAR, avatarUrl);
                        ShareFileUtils.setBoolean(ShareFileUtils.NEED_COMPLETE_USER_INFO, false);
                        ShareFileUtils.setBoolean(ShareFileUtils.HAS_LOGGED, true);
                        ShareFileUtils.setBoolean(ShareFileUtils.FIRSTREGISTER, true);//用来设置 匹配中的锁 第一次注册需要显示
                        MobclickAgent.onEvent(InitUserInfoActivity.this, "new_user_update_user_info");// 注册转化率统计：新用户填写个人资料成功

                        MobclickAgent.onEvent(InitUserInfoActivity.this, "enter_main_page");// 注册转化率统计：进入主界面
                        Intent intent4 = new Intent(InitUserInfoActivity.this, MainActivity.class);
                        intent4.putExtra(TheLConstants.BUNDLE_KEY_WHERE_TO_GO, "ONE");
                        startActivity(intent4);
                        LoginActivityManager.getInstanse().finishAllActivity();

                    }
                });
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//        checkTips();
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        checkTips();
        if (s.length() > 0) {
            if (!checkIsEmpty()) {
                button_login.setEnabled(true);
            }
        } else {
            button_login.setEnabled(false);
        }
    }

    private void handlePhoto(String imagePath) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true; // 只读入图边界
        BitmapFactory.decodeFile(imagePath, options);

        int zoom = 1;
        int quality = 100;

        // 先判断照片的横竖
        boolean isLandscape = options.outWidth > options.outHeight;
        if (isLandscape) {
            zoom = (int) (options.outWidth / (float) 200);
        } else {
            zoom = (int) (options.outHeight / (float) 200);
        }

        if (zoom < 1) {
            avatarPhotoPath = imagePath;
        } else {
            options.inSampleSize = zoom;
            options.inJustDecodeBounds = false; // 读取全部图信息
            Bitmap bitmap = BitmapFactory.decodeFile(imagePath, options); // 图片的压缩
            String handlePhotoPath = imagePath;
            try {
                File file = new File(handlePhotoPath);
                BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
                bitmap.compress(Bitmap.CompressFormat.JPEG, quality, bos);
                bos.flush();
                bos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            avatarPhotoPath = handlePhotoPath;
        }
        // 置为非空
        avatarUrl = " ";
        if (!checkIsEmpty()) {
            button_login.setEnabled(true);
        }
    }

    private void requestFailed() {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                closeLoading();
                DialogUtil.showToastShort(InitUserInfoActivity.this, getString(R.string.message_network_error));
            }
        });
    }

    public class MyConsumer extends LoginInterceptorSubscribe<UploadTokenBean> {

        String picUploadPath;

        String picLocalPath;

        public MyConsumer(String picUploadPath, String picLocalPath) {
            this.picUploadPath = picUploadPath;
            this.picLocalPath = picLocalPath;
        }

        @Override
        public void onError(Throwable t) {
            super.onError(t);
            closeLoading();
        }

        @Override
        public void onNext(UploadTokenBean uploadTokenBean) {
            super.onNext(uploadTokenBean);
            if (!TextUtils.isEmpty(uploadTokenBean.data.uploadToken)) {
                UploadManager uploadManager = new UploadManager();
                // 上传头像图片
                if (!TextUtils.isEmpty(uploadAvatarPath) && !TextUtils.isEmpty(avatarPhotoPath)) {
                    uploadManager.put(new File(avatarPhotoPath), uploadAvatarPath, uploadTokenBean.data.uploadToken, new UpCompletionHandler() {
                        @Override
                        public void complete(String key, ResponseInfo info, JSONObject response) {
                            if (info != null && info.statusCode == 200 && uploadAvatarPath.equals(key)) {
                                List<Integer> wh = ImageUtils.measurePicWidthAndHeight(avatarPhotoPath);
                                if (wh.size() == 2) {
                                    avatarUrl = RequestConstants.FILE_BUCKET + key;
                                    avatarPhotoPath = "";// 把base64Image设为空，以免在上传图片成功但是其它接口错误后，再次提交重复上传
                                    submitUserInfo();
                                }
                            } else {
                                requestFailed();
                            }
                        }
                    }, null);
                } else {
                    requestFailed();
                }
            } else {
                requestFailed();
            }
        }
    }

    private void setTag() {

        mFlowLayout.setMaxSelectCount(1);

        final LayoutInflater mInflater = LayoutInflater.from(InitUserInfoActivity.this);

        mFlowLayout.setAdapter(new TagAdapter<RoleBean>(QueueConstants.roleList) {

            @Override
            public View getView(FlowLayout parent, int position, RoleBean roleBean) {

                TextView textView = (TextView) mInflater.inflate(R.layout.item_role_tag,
                        mFlowLayout, false);

                textView.setText(roleBean.contentRes);

                int textViewMinWidth = SizeUtils.dip2px(TheLApp.context, 30);

                int marginRightWidth = SizeUtils.dip2px(TheLApp.context, 18);

                if (roleBean.contentRes.length() > 1) {

                    int textWidth = (int) getTextWidth(roleBean.contentRes, 12) + textViewMinWidth;

                    FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(textWidth, textViewMinWidth);
                    params.gravity = Gravity.CENTER;
                    params.setMargins(0, marginRightWidth, marginRightWidth, 0);
                    textView.setLayoutParams(params);
                } else {
                    FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(textViewMinWidth, textViewMinWidth);
                    params.gravity = Gravity.CENTER;
                    params.setMargins(0, marginRightWidth, marginRightWidth, 0);
                    textView.setLayoutParams(params);
                }

                return textView;
            }
        });

        mFlowLayout.setOnTagClickListener(new TagFlowLayout.OnTagClickListener() {
            @Override
            public boolean onTagClick(View view, int position, FlowLayout parent, boolean performClick) {
                RoleBean roleBean = QueueConstants.roleList.get(position);

                roleTag = String.valueOf(roleBean.listPoi);

                if (!checkIsEmpty()) {
                    button_login.setEnabled(true);
                } else {
                    button_login.setEnabled(false);
                }

                mFlowLayout.toggle(view, position, parent);
                return true;
            }
        });

    }

    public float getTextWidth(String text, int textSize) {
        TextPaint paint = new TextPaint();
        float scaledDensity = TheLApp.getContext().getResources().getDisplayMetrics().scaledDensity;
        paint.setTextSize(scaledDensity * textSize);
        return paint.measureText(text);
    }

}
