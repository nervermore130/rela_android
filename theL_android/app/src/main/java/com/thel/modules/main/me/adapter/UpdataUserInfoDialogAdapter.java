package com.thel.modules.main.me.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;

import java.util.ArrayList;

public class UpdataUserInfoDialogAdapter extends BaseAdapter {

	private LayoutInflater mInflater;

	private int selectPosition;

	private ArrayList<String> listData = new ArrayList<String>();

	public UpdataUserInfoDialogAdapter(ArrayList<String> listData) {
		this.listData = listData;
		mInflater = (LayoutInflater) TheLApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public UpdataUserInfoDialogAdapter(ArrayList<String> listData, int selectPosition) {
		this.selectPosition = selectPosition;
		this.listData = listData;

		mInflater = (LayoutInflater) TheLApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public void refreshList(int selectPosition) {
		this.selectPosition = selectPosition;
	}

	public void updataList(int selectPosition, ArrayList<String> listData) {
		this.selectPosition = selectPosition;
		this.listData = listData;
	}

	@Override
	public int getCount() {
		return listData.size();
	}

	@Override
	public Object getItem(int position) {
		return listData.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, final ViewGroup parent) {
		convertView = mInflater.inflate(R.layout.updateuserinfo_dialog_listitem, parent, false);
		TextView txt = convertView.findViewById(R.id.txt);

		txt.setText(listData.get(position));

		if (position == selectPosition) {// 如果当前的行就是ListView中选中的一行，就更改显示样式
			convertView.setBackgroundColor(0xffd5e8e9);// 更改整行的背景色
		}

		return convertView;
	}

}
