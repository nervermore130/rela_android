package com.thel.modules.live.bean;

public class AudienceLinkMicRequestBean {

    public String method;

    public float x;

    public float height;

    public float y;

    public String result;

    public float width;

    public String toUserId;

    @Override public String toString() {
        return "AudienceLinkMicRequestBean{" +
                "method='" + method + '\'' +
                ", x=" + x +
                ", height=" + height +
                ", y=" + y +
                ", result='" + result + '\'' +
                ", width=" + width +
                ", toUserId='" + toUserId + '\'' +
                '}';
    }
}
