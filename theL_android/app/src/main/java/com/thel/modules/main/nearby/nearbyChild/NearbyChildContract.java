package com.thel.modules.main.nearby.nearbyChild;

import com.thel.base.BasePresenter;
import com.thel.base.BaseView;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.user.NearUserBean;
import com.thel.bean.user.NearUserListNetBean;

import java.util.List;

/**
 * Created by waiarl on 2017/10/11.
 */

public class NearbyChildContract {
    public interface Presenter extends BasePresenter {

        /**
         * 获取刷新数据
         */
        void getRefreshData();

        /**
         * 加载更多数据
         */
        void getLoadMoreData(int page);

        /**
         * 获取附近日志列表
         */
        void getNearbyMomentData();

        /**
         * 获取附近的小鲜肉
         *
         * @param isRefresh
         */
        void getNearbyPigData(boolean isRefresh);

    }

    interface View extends BaseView<Presenter> {

        /**
         * 界面是否是活跃状态
         *
         * @return
         */
        boolean isActive();

        /**
         * 刷新数据
         */
        void showRefreshData(NearUserListNetBean nearUserListNetBean);

        /**
         * 加载更多
         */
        void showLoadMoreData(NearUserListNetBean nearUserListNetBean);

        /**
         * 加载更多失败
         */
        void loadMoreFailed();

        /**
         * 空数据
         */
        void emptyData(boolean empty);

        /**
         * 请求结束
         */
        void requestFinish();

        /**
         * 显示附近日志九宫格
         *
         * @param momentsBeanList
         */
        void showNearbyMomentData(List<MomentsBean> momentsBeanList);

        /**
         * 附近的小鲜肉数据
         *
         * @param nearUserBeanList
         */
        void showNearbyPigData(List<NearUserBean> nearUserBeanList, boolean isRefresh, boolean hasMoreData);
    }

}
