package com.thel.modules.select_contacts;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.bean.ContactBean;
import com.thel.bean.ContactsListBean;
import com.thel.constants.TheLConstants;
import com.thel.modules.main.me.aboutMe.GiveVipRequestActivity;
import com.thel.modules.main.me.aboutMe.SendCircleRequestActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.utils.DialogUtil;
import com.thel.utils.L;
import com.thel.utils.PinyinUtils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * 选择联系人
 */
public class SelectContactsActivity extends BaseActivity implements TextWatcher {

    /**
     * 已选数量
     */
    @BindView(R.id.selected_num)
    TextView selected_num;

    /**
     * 选择完成
     */
    @BindView(R.id.lin_done)
    LinearLayout lin_done;

    /**
     * 默认页
     */
    @BindView(R.id.bg_friends_default)
    TextView bg_friends_default;

    @BindView(R.id.listview)
    ListView listView;

    @BindView(R.id.recyclerview_head)
    RecyclerView recyclerview_head;

    /**
     * 没有匹配的联系人提示
     */
    @BindView(R.id.no_match_tip)
    TextView no_match_tip;

    @BindView(R.id.ll_search)
    RelativeLayout ll_search;

    @BindView(R.id.edit_search)
    EditText edit_search;

    private CloseActivityReceiver closeReceiver;

    /**
     * 密友功能 *
     */
    // 已经发送过请求的密友id列表
    private String circleFriendIds = null;

    private boolean give_vip = false;//赠送会员intent bundle ，为true的时候为赠送会员跳转过来，相当于requestType不为null

    private int selectLimit = 10;

    private String requestType = null;

    /**
     * 输入关键字过滤后的朋友对象列表
     */
    private ArrayList<ContactBean> filterArr = new ArrayList<ContactBean>();

    /**
     * 选中的朋友对象列表
     */
    private ArrayList<ContactBean> selectedArr = new ArrayList<ContactBean>();

    private int selectedNum = 0;

    private ArrayList<ContactBean> selectedArrTemp = new ArrayList<ContactBean>();

    /**
     * 所有好友的对象列表
     */
    private ArrayList<ContactBean> listPlus = new ArrayList<>();

    private SelectContactsAdapter adapter;

    private SelectUserinfoAdapter selectUserinfoAdapter;

    private ContactBean friend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_select_contacts);

        ButterKnife.bind(this);

        registerReceiver();

        Intent intent = getIntent();
        if (intent != null) {
            selectLimit = intent.getIntExtra("selectLimit",10);
            circleFriendIds = intent.getStringExtra("circleFriendIds");
            if (circleFriendIds != null) {
                selectLimit = 1;
                requestType = intent.getStringExtra("requestType");
                lin_done.setVisibility(View.GONE);
            } else if (give_vip = intent.getBooleanExtra("give_vip", false)) {//新增赠送会员，为true的时候为赠送会员跳转过来
                selectLimit = 1;
                lin_done.setVisibility(View.GONE);
            }
            selectedArr.clear();
            selectedNum = 0;
            if (intent.getSerializableExtra(TheLConstants.BUNDLE_KEY_FRIENDS_LIST) != null) {
                selectedArrTemp.addAll((ArrayList<ContactBean>) intent.getSerializableExtra(TheLConstants.BUNDLE_KEY_FRIENDS_LIST));
                selectedArr.addAll(selectedArrTemp);
                selectedNum = selectedArr.size();
                if (selectedNum > 0) {
                    setDoneText();
                    ll_search.setVisibility(View.VISIBLE);
                }
            }

        }

        filterData();

        //统一listPlus中的对象与selectedArr的对象
        unifyListObject(listPlus, selectedArr, true);

        edit_search.addTextChangedListener(this);

        initList();

        initData();
    }

    private void initList() {
        if (adapter == null) {
            if (requestType != null) {
                adapter = new SelectContactsAdapter(listPlus, true);
                adapter.setIsCanSelected(false);
                selectUserinfoAdapter = new SelectUserinfoAdapter(this, selectedArr);
            } else if (give_vip) {//赠送会员所跳转
                adapter = new SelectContactsAdapter(listPlus, true);
                adapter.setIsCanSelected(false);
            } else {
                adapter = new SelectContactsAdapter(listPlus, false);
                adapter.setIsCanSelected(true);
                if (selectUserinfoAdapter == null) {
                    selectUserinfoAdapter = new SelectUserinfoAdapter(this, selectedArr);
                }

            }
            listView.setAdapter(adapter);
            recyclerview_head.setAdapter(selectUserinfoAdapter);

            //设置布局管理器
            recyclerview_head.setLayoutManager(new LinearLayoutManager(TheLApp.getContext(), LinearLayoutManager.HORIZONTAL, false));
            //如果可以确定每个item的高度是固定的，设置这个选项可以提高性能
            recyclerview_head.setHasFixedSize(true);

        } else {
            adapter.notifyDataSetChanged();
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                friend = (ContactBean) view.getTag(R.id.friend_bean_tag);
                if (requestType != null) {// 密友功能单选
                    Intent intent = new Intent(SelectContactsActivity.this, SendCircleRequestActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("contact", friend);
                    bundle.putString("requestType", requestType);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    finish();
                } else if (give_vip) {//赠送会员所跳转
                    MobclickAgent.onEvent(TheLApp.getContext(), "select_giving_people");
                    Intent intent = new Intent(SelectContactsActivity.this, GiveVipRequestActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(TheLConstants.BUNDLE_KEY_CONTACT, friend);
                    intent.putExtras(bundle);
                    startActivity(intent);
                } else {
                    if (!friend.isSelected && selectedNum == selectLimit) {
                        DialogUtil.showToastShort(SelectContactsActivity.this, String.format(getString(R.string.select_contacts_activity_tip), selectLimit));
                        return;
                    }
                    friend.isSelected = !friend.isSelected;
                    if (friend.isSelected) {
                        selectedNum += 1;
                        selectedArr.add(friend);
                        ll_search.setVisibility(View.VISIBLE);
                        selectUserinfoAdapter.notifyDataSetChanged();

                        recyclerview_head.smoothScrollToPosition(selectUserinfoAdapter.getItemCount());
                    } else {
                        selectedNum -= 1;
                        int index = 0;
                        boolean flag = false;
                        for (ContactBean contactBean : selectedArr) {
                            if (contactBean.userId.equals(friend.userId)) {
                                flag = true;
                                break;
                            }
                            index++;
                        }
                        if (flag) {
                            selectedArr.remove(index);
                            selectUserinfoAdapter.notifyItemRemoved(index);
                        }
                    }
                    if (selectedArr.isEmpty()) {
                        ll_search.setVisibility(View.GONE);
                    } else {
                        ll_search.setVisibility(View.VISIBLE);
                    }
                    adapter.notifyDataSetChanged();
                    setDoneText();
                }
            }
        });
        if (selectUserinfoAdapter != null) {
            selectUserinfoAdapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    //点击删除recyclerview条目
                    clickRemove(position);
                }
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(this.getClass().getName());
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(this.getClass().getName());
        MobclickAgent.onPause(this);
    }

    @Override
    protected void onDestroy() {
        try {
            unregisterReceiver(closeReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void initData() {
        RequestBusiness.getInstance().getContactsList().observeOn(AndroidSchedulers.mainThread())
                .onBackpressureDrop().subscribeOn(Schedulers.io()).subscribe(new InterceptorSubscribe<ContactsListBean>() {
            @Override
            public void onNext(ContactsListBean contactsListBean) {
                super.onNext(contactsListBean);
                if (contactsListBean == null || contactsListBean.data == null || contactsListBean.data.users == null || contactsListBean.data.users.size() == 0) {
                    bg_friends_default.setVisibility(View.VISIBLE);
                } else {
                    listPlus.clear();
                    for (int i = 0; i < contactsListBean.data.users.size(); i++) {
                        ContactBean bean = new ContactBean();
                        bean.avatar = contactsListBean.data.users.get(i).avatar;
                        bean.bgImage = contactsListBean.data.users.get(i).bgImage;
                        bean.nickName = contactsListBean.data.users.get(i).nickName;
                        bean.nickNamePinYin = PinyinUtils.cn2Spell(bean.nickName);
                        bean.userId = String.valueOf(contactsListBean.data.users.get(i).userId);
                        listPlus.add(bean);
                    }
                    filterData();
                    //重新返回到选择好友界面
                    unifyListObject(listPlus, selectedArr, false);
                    bg_friends_default.setVisibility(View.GONE);
                    setDoneText();
                    if (adapter == null) {
                        if (requestType != null) {
                            adapter = new SelectContactsAdapter(listPlus, true);
                        } else if (give_vip) {//赠送会员所跳转
                            adapter = new SelectContactsAdapter(listPlus, true);
                        } else {
                            adapter = new SelectContactsAdapter(listPlus, false);
                        }
                        listView.setAdapter(adapter);
                    } else {
                        adapter.notifyDataSetChanged();
                    }
                }
            }
        });
    }

    private void registerReceiver() {
        closeReceiver = new CloseActivityReceiver();
        registerReceiver(closeReceiver, new IntentFilter(TheLConstants.BROADCAST_ACTION_CLOSE_BEFORE_ACTIVITY));
    }

    private void clickRemove(int position) {
        selectedNum -= 1;

        setDoneText();
        ContactBean bean = selectedArr.get(position);
        selectedArr.remove(position);
        selectUserinfoAdapter.notifyItemRemoved(position);
        bean.isSelected = false;
        adapter.notifyDataSetChanged();
        if (selectedArr.size() == 0) {
            ll_search.setVisibility(View.GONE);
        }
    }

    private void setDoneText() {
        if (selectedNum == 0) {
            selected_num.setVisibility(View.GONE);
            lin_done.setAlpha(0.5f);
        } else {
            selected_num.setVisibility(View.VISIBLE);
            selected_num.setText("(" + selectedNum + "/" + selectLimit + ")");
            lin_done.setAlpha(1f);
        }
    }

    /**
     * 排除不能选择的用户
     */
    private void filterData() {
        if (!TextUtils.isEmpty(circleFriendIds) && requestType != null) {
            try {
                if (listPlus.size() > 0) {
                    List<ContactBean> filterList = new ArrayList<ContactBean>();
                    for (ContactBean contactBean : listPlus) {
                        if (circleFriendIds.contains(contactBean.userId + ",")) {
                            filterList.add(contactBean);
                        }
                    }
                    listPlus.removeAll(filterList);
                }
            } catch (Exception e) {
            }
        }
    }

    /**
     * 统一对象
     */
    private void unifyListObject(ArrayList<ContactBean> listPlus, ArrayList<ContactBean> selectedArr, boolean addSeleteNum) {
        final int sc = selectedArr.size();
        final int lc = listPlus.size();
        ourter:
        for (int i = 0; i < sc; i++) {
            final ContactBean bean = selectedArr.get(i);
            bean.isSelected = true;
            for (int j = 0; j < lc; j++) {
                if (listPlus.get(j).userId.equals(bean.userId)) {
                    bean.nickName = listPlus.get(j).nickName;
                    listPlus.set(j, bean);
                    if (addSeleteNum) {
                        selectedNum += 1;
                    }
                    continue ourter;
                }
            }
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        filterArr.clear();
        if (adapter == null) {
            return;
        }
        if (TextUtils.isEmpty(s.toString().trim())) {
            adapter.refreshAdapter(listPlus);
            no_match_tip.setVisibility(View.INVISIBLE);
        } else {
            for (ContactBean contact : listPlus) {
                if (contact.nickNamePinYin != null && contact.nickNamePinYin.contains(PinyinUtils.cn2Spell(s.toString().trim()))) {
                    filterArr.add(contact);
                }
            }
            adapter.refreshAdapter(filterArr);
            if (filterArr.isEmpty()) {
                no_match_tip.setVisibility(View.VISIBLE);
            } else {
                no_match_tip.setVisibility(View.INVISIBLE);
            }
        }
        adapter.notifyDataSetChanged();
    }

    class CloseActivityReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                boolean closeActivity = intent.getBooleanExtra(TheLConstants.BUNDLE_KEY_CLOSE_BEFORE_ACTIVITY, false);
                if (closeActivity) {
                    SelectContactsActivity.this.finish();
                }
            }
        }
    }

    @OnClick({R.id.tv_back, R.id.lin_done})
    void click(View view) {
        switch (view.getId()) {
            case R.id.tv_back:
                finish();
                break;
            case R.id.lin_done:
                Intent intent = new Intent();
                intent.putExtra(TheLConstants.BUNDLE_KEY_FRIENDS_LIST, selectedArr);
                setResult(TheLConstants.RESULT_CODE_WRITE_MOMENT_SELECT_CONTACT, intent);
                finish();
                break;
        }
    }

    @Override
    public void finish() {
        ViewUtils.hideSoftInput(SelectContactsActivity.this);
        super.finish();
    }
}
