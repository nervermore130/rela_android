package com.thel.modules.main.me.aboutMe;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.constants.BundleConstants;
import com.thel.constants.TheLConstants;
import com.thel.modules.main.home.moments.web.WebViewActivity;
import com.thel.modules.main.me.bean.WalletBean;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.utils.ImageUtils;
import com.thel.utils.SharedPrefUtils;
import com.thel.utils.StringUtils;
import com.thel.utils.ViewUtils;

import org.reactivestreams.Subscription;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * 我的钱包
 * Created by lingwei on 2017/9/23.
 */

public class MyWalletActivity extends BaseActivity implements View.OnClickListener {
    private LinearLayout lin_back;//返回
    private SwipeRefreshLayout swipe_container;
    private TextView txt_today_income;//今日收益
    private TextView txt_total_income;//总共收益
    private TextView txt_withdraw;//提现
    private RelativeLayout rel_income_detail;
    private RelativeLayout rel_withdraw_record;

    private SimpleDraweeView img_avatar1;
    private SimpleDraweeView img_avatar2;
    private SimpleDraweeView img_avatar3;
    private RelativeLayout avatar_area1;
    private RelativeLayout avatar_area2;
    private RelativeLayout avatar_area3;
    private RelativeLayout rel_fans_contribute;//粉丝贡献榜
    private RelativeLayout rel_exchange_dou;
    private WalletBean walletBean;
    private TextView txt_my_balance;
    private RelativeLayout rel_live_rank_list;
    private TextView txt_live_rank_list;
    private TextView txt_this_month_income;
    private TextView txt_this_month_livetime;
    private TextView tv_announcement;
    private TextView notice_tip;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_wallet_activity);
        findViewById();
        setListener();
        processBusiness();

    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void findViewById() {
        lin_back = findViewById(R.id.lin_back);
        swipe_container = findViewById(R.id.swipe_container);
        ViewUtils.initSwipeRefreshLayout(swipe_container);
        txt_today_income = findViewById(R.id.txt_today_income);
        txt_total_income = findViewById(R.id.txt_total_income);
        txt_withdraw = findViewById(R.id.txt_withdraw);
        rel_income_detail = findViewById(R.id.rel_income_detail);
        rel_withdraw_record = findViewById(R.id.rel_withdraw_record);
        txt_this_month_income = findViewById(R.id.txt_this_month_income);
        rel_fans_contribute = findViewById(R.id.rel_fans_contribute);//粉丝贡献榜
        img_avatar1 = findViewById(R.id.img_avatar1);
        img_avatar2 = findViewById(R.id.img_avatar2);
        img_avatar3 = findViewById(R.id.img_avatar3);
        avatar_area1 = findViewById(R.id.avatar_area1);
        avatar_area2 = findViewById(R.id.avatar_area2);
        avatar_area3 = findViewById(R.id.avatar_area3);
        // rel_exchange_dou = (RelativeLayout) findViewById(R.id.rel_exchange_dou);
        txt_my_balance = findViewById(R.id.txt_my_balance);
        txt_this_month_livetime = findViewById(R.id.txt_this_month_livetime);
        rel_live_rank_list = findViewById(R.id.rel_live_rank_list);
        txt_live_rank_list = findViewById(R.id.txt_live_rank_list);
        tv_announcement = findViewById(R.id.tv_announcement);
        notice_tip = findViewById(R.id.notice_tip);
    }

    private void setListener() {
        lin_back.setOnClickListener(this);
        txt_withdraw.setOnClickListener(this);
        rel_income_detail.setOnClickListener(this);
        rel_withdraw_record.setOnClickListener(this);
        rel_fans_contribute.setOnClickListener(this);
        // rel_exchange_dou.setOnClickListener(this);
        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                processBusiness();
            }
        });

        rel_live_rank_list.setOnClickListener(this);
    }

    private void processBusiness() {
        Flowable<WalletBean> flowable = RequestBusiness.getInstance().getWalletData();
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<WalletBean>() {

            @Override
            public void onNext(WalletBean walletBean) {
                super.onNext(walletBean);
                setWalletData(walletBean);
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                requestFinished();
            }

            @Override
            public void onComplete() {
                requestFinished();
            }
        });
    }

    /**
     * 设置粉丝贡献榜前三名头像
     *
     * @param walletBean
     */
    private void setWalletData(WalletBean walletBean) {
        if (walletBean.data != null) {
            this.walletBean = walletBean;
            final int size = walletBean.data.topFans.size();
            for (int i = 0; i < size; i++) {
                if (i == 0) {
                    avatar_area1.setVisibility(View.VISIBLE);
                    img_avatar1.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(walletBean.data.topFans.get(size - 1 - i).avatar, TheLConstants.AVATAR_MIDDLE_SIZE, TheLConstants.AVATAR_MIDDLE_SIZE)));
                } else if (i == 1) {
                    avatar_area2.setVisibility(View.VISIBLE);
                    img_avatar2.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(walletBean.data.topFans.get(size - 1 - i).avatar, TheLConstants.AVATAR_MIDDLE_SIZE, TheLConstants.AVATAR_MIDDLE_SIZE)));
                } else if (i == 2) {
                    avatar_area3.setVisibility(View.VISIBLE);
                    img_avatar3.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(walletBean.data.topFans.get(size - 1 - i).avatar, TheLConstants.AVATAR_MIDDLE_SIZE, TheLConstants.AVATAR_MIDDLE_SIZE)));
                }
            }
            try {
                txt_today_income.setText(walletBean.data.gemIncomingToday + "");
                txt_total_income.setText(walletBean.data.gemIncoming + "");
                txt_my_balance.setText(walletBean.data.gem + "");
                txt_this_month_income.setText(walletBean.data.gemIncomingMonth + "");
                txt_this_month_livetime.setText(walletBean.data.monthLiveTime + "");
            } catch (Exception e) {
                txt_this_month_livetime.setText("");

                txt_my_balance.setText("");
                txt_today_income.setText("");
                txt_total_income.setText("");
                txt_this_month_income.setText("");

            }
            if (walletBean.data.gemIncomingTodayRank > 0) {
                txt_live_rank_list.setVisibility(View.VISIBLE);
                txt_live_rank_list.setText(StringUtils.getString(R.string.rank, walletBean.data.gemIncomingTodayRank));
            } else {
                txt_live_rank_list.setVisibility(View.GONE);
            }
            if (TextUtils.isEmpty(walletBean.data.announcement)) {
                tv_announcement.setVisibility(View.GONE);
                notice_tip.setVisibility(View.GONE);
            } else {

                tv_announcement.setVisibility(View.VISIBLE);
                notice_tip.setVisibility(View.VISIBLE);
                notice_tip.setText(walletBean.data.announcement.replace("\\n", "\n"));
            }
        }
    }

    /**
     * 请求结束关闭dialog
     */
    private void requestFinished() {
        if (swipe_container != null)
            swipe_container.postDelayed(new Runnable() {

                @Override
                public void run() {
                    if (swipe_container != null && swipe_container.isRefreshing())
                        swipe_container.setRefreshing(false);
                }
            }, 1000);
    }

    @Override
    public void onClick(View view) {
        ViewUtils.preventViewMultipleClick(view, 1500);

        Intent intent;

        switch (view.getId()) {
            case R.id.lin_back:// 返回
                finish();
                break;
            case R.id.txt_withdraw:// 提现
                if (walletBean != null) {
                    intent = new Intent(this, WebViewActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(BundleConstants.URL, walletBean.data.withdrawLink);
                    bundle.putBoolean(BundleConstants.NEED_SECURITY_CHECK, false);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
                break;
            case R.id.rel_income_detail:// 收益详情
                intent = new Intent(this, RevenueDetailsActivity.class);
                intent.putExtra("filter", RevenueDetailsActivity.FILTER_TYPE_INCOMING);
                startActivity(intent);
                break;
            case R.id.rel_withdraw_record:// 提现记录
                if (walletBean != null) {
                    intent = new Intent(this, WebViewActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(BundleConstants.URL, walletBean.data.withdrawListLink);
                    bundle.putBoolean(BundleConstants.NEED_SECURITY_CHECK, false);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
                break;
            case R.id.rel_fans_contribute:// 粉丝贡献榜
                if (walletBean != null) {
                    intent = new Intent(this, WebViewActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(BundleConstants.URL, walletBean.data.topFansLink);
                    bundle.putBoolean(BundleConstants.NEED_SECURITY_CHECK, false);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
                break;
           /* case R.id.rel_exchange_dou:// 兑换软妹豆
                if (walletBean != null) {
                    intent = new Intent(this, WebViewActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(BundleConstants.URL, walletBean.data.exchangeLink);
                    bundle.putBoolean(BundleConstants.NEED_SECURITY_CHECK, false);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
                break;*/
            case R.id.rel_live_rank_list://直播榜单
                final String topLink = SharedPrefUtils.getString(SharedPrefUtils.FILE_LIVE, SharedPrefUtils.FILE_TOP_LINK, SharedPrefUtils.FILE_TOP_LINK);
                if (!TextUtils.isEmpty(topLink)) {
                    intent = new Intent(this, WebViewActivity.class);
                    final Bundle bundle = new Bundle();
                    bundle.putString(BundleConstants.URL, topLink);
                    bundle.putBoolean(BundleConstants.NEED_SECURITY_CHECK, false);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
                break;
        }
    }
}
