package com.thel.modules.main.me.timemachine;

import android.os.Bundle;
import androidx.annotation.Nullable;

import com.thel.R;
import com.thel.base.BaseImageViewerActivity;

/**
 * created by LiuYun
 */
public class TimeMachineActivity extends BaseImageViewerActivity {

    @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frame_normal);
        initFragment();
    }

    private void initFragment() {
        TimeMachineFragment mTimeMachineFragment = (TimeMachineFragment) getSupportFragmentManager().findFragmentByTag(TimeMachineFragment.class.getName());

        if (mTimeMachineFragment == null) {
            mTimeMachineFragment = TimeMachineFragment.getInstance();
        }

        if (!mTimeMachineFragment.isAdded()) {
            getSupportFragmentManager().beginTransaction().add(R.id.frame_content, mTimeMachineFragment, TimeMachineFragment.class.getName()).commit();
        }

    }

}
