package com.thel.modules.live.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.constants.TheLConstants;
import com.thel.growingio.GIOShareTrackBean;
import com.thel.growingio.GrowingIoConstant;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.live.adapter.LianGiftCountAdatper;
import com.thel.modules.live.bean.LianGiftCountBean;
import com.thel.modules.live.bean.LiveRoomBean;
import com.thel.modules.live.bean.SoftGiftBean;
import com.thel.modules.live.in.GiftLianSendCallback;
import com.thel.modules.live.in.LiveTextSizeChangedListener;
import com.thel.modules.live.surface.LiveShowContract;
import com.thel.modules.live.view.LiveShowCloseDialog;
import com.thel.modules.live.view.LiveTextSizeDialog;
import com.thel.ui.adapter.ShareGridAdapter;
import com.thel.ui.decoration.ComboItemDivider;
import com.thel.utils.DeviceUtils;
import com.thel.utils.DialogUtil;
import com.thel.utils.DialogUtils;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.ImageUtils;
import com.thel.utils.SizeUtils;
import com.thel.utils.UMShareUtils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by waiarl on 2017/11/3.
 */

public class LiveShowDialogUtils extends DialogUtils {

    private final Handler mMainHandler;

    public LiveShowDialogUtils() {
        mMainHandler = new Handler(Looper.getMainLooper());

    }

    public void closeDialog() {
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
        }
    }

    public boolean isDialogShowing() {
        return mDialog != null && mDialog.isShowing();
    }

    /**
     * 字体更改对话框
     *
     * @param activity
     * @param listener
     */
    public void showTextSizeDialog(Activity activity, LiveTextSizeChangedListener listener) {
        closeDialog();
        mDialog = new LiveTextSizeDialog(activity).initDialog(listener);

    }

    /**
     * 观看直播关闭对话框
     *
     * @param mActivity
     * @param liveRoomBean
     * @param listener
     */
    public void showLiveShowCloseDialog(final Activity mActivity, LiveRoomBean liveRoomBean, View.OnClickListener listener, View.OnClickListener moreLiveListener) {
        closeDialog();
        mDialog = new LiveShowCloseDialog(mActivity).initDialog(liveRoomBean, listener, moreLiveListener);
    }

    /**
     * 直播间连击小礼物（自己输入连击个数）
     */
    public void showLianGiftMsgDialog(final Context context, final SoftGiftBean softGiftBean, final String type, final GiftLianSendCallback callback, final View.OnTouchListener onTouchListener) {
        if (context == null) {
            return;
        }
        final float size = TheLApp.getContext().getResources().getDimension(R.dimen.gift_icon_size);
        final Dialog dialog = new Dialog(context, R.style.CustomDialog);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        final View view = LayoutInflater.from(context).inflate(R.layout.live_show_lian_gift_dialog_view, null);
        dialog.setContentView(view);
        dialog.show();
        final RecyclerView lian_count_recycler = view.findViewById(R.id.lian_count_recycler);
        final ImageView img_gift = view.findViewById(R.id.img_lian_gift);
        final ImageView img_lian_gift_close = view.findViewById(R.id.img_lian_gift_close);
        final EditText edit_lian_gift_input = view.findViewById(R.id.edit_lian_gift_input);
        final TextView combo_tv = view.findViewById(R.id.combo_tv);
        final TextView special_effects_tv = view.findViewById(R.id.special_effects_tv);


        special_effects_tv.setVisibility(View.GONE);


//        final TextView txt_gift_num_tip = view.findViewById(R.id.txt_gift_num_tip);
//        final TextView txt_gift_mean = view.findViewById(R.id.txt_gift_mean);
        final TextView txt_send = view.findViewById(R.id.txt_send);
        txt_send.setSelected(true);
//        txt_gift_num_tip.setVisibility(View.VISIBLE);
//        txt_gift_mean.setVisibility(View.INVISIBLE);
        lian_count_recycler.addItemDecoration(new ComboItemDivider(SizeUtils.dip2px(TheLApp.context, 15)));
        lian_count_recycler.setLayoutManager(new GridLayoutManager(context, 3));
        lian_count_recycler.setHasFixedSize(true);
        List<LianGiftCountBean> lianGiftCountBeans = new ArrayList<>();

        int[] counts = new int[]{10, 100, 520, 999, 1314, 5200};
        String[] titles = TheLApp.getContext().getResources().getStringArray(R.array.combo_gift_title);

        String defaultTile = titles[1];

        combo_tv.setText(defaultTile);

        for (int i = 0; i < counts.length; i++) {
            LianGiftCountBean bean = new LianGiftCountBean();
            bean.title = titles[i];
            bean.count = counts[i];
            if (i == 1) bean.select = true;
            lianGiftCountBeans.add(bean);
        }
        final LianGiftCountAdatper lianGiftCountAdatper = new LianGiftCountAdatper(lian_count_recycler, lianGiftCountBeans);
        lianGiftCountAdatper.setEffectsThreshold(softGiftBean.effectsThreshold);
        lian_count_recycler.setAdapter(lianGiftCountAdatper);
        lianGiftCountAdatper.setOnClickListener(new LianGiftCountAdatper.OnClickListener() {
            @Override
            public void onClick(LianGiftCountBean bean) {
                edit_lian_gift_input.setText("");
                txt_send.setSelected(bean.select);

                if (softGiftBean.effectsThreshold >= 999 && bean.count >= 999) {
                    special_effects_tv.setVisibility(View.VISIBLE);
                } else {
                    special_effects_tv.setVisibility(View.GONE);
                }

                String comboText = bean.title;

                combo_tv.setText(comboText);
            }
        });

        ImageLoaderManager.imageLoader(img_gift, softGiftBean.icon, (int) size, (int) size);
        edit_lian_gift_input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                final int length = s.length();

                final String num = s.toString();
                if (length > 0) {
                    for (int i = 0; i < lianGiftCountAdatper.getItemCount(); i++) {
                        lianGiftCountAdatper.getData().get(i).select = false;
                    }
                    lianGiftCountAdatper.notifyDataSetChanged();
                    txt_send.setSelected(true);
                } else {
                    combo_tv.setText("选择连击数");
                    txt_send.setSelected(false);
                }

                try {
                    if (length > 0) {
                        if (length == 1 && num.equals("0")) {
                            edit_lian_gift_input.setText("");
                            combo_tv.setText("");
                        } else {
                            combo_tv.setText(num + " 连击");
                            if (softGiftBean.effectsThreshold >= 999 && Integer.valueOf(num) >= 999) {
                                special_effects_tv.setVisibility(View.VISIBLE);
                            } else {
                                special_effects_tv.setVisibility(View.GONE);
                            }

                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        edit_lian_gift_input.setOnTouchListener(onTouchListener);
        img_lian_gift_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        txt_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!txt_send.isSelected()) {
                    return;
                }

                dialog.dismiss();
                String combCount;
                int selectIndex = -1;
                for (int i = 0; i < lianGiftCountAdatper.getItemCount(); i++) {
                    if (lianGiftCountAdatper.getItem(i).select) {
                        selectIndex = i;
                        break;
                    }
                }
                if (selectIndex != -1) {
                    combCount = String.valueOf(lianGiftCountAdatper.getItem(selectIndex).count);
                } else {
                    combCount = edit_lian_gift_input.getText().toString();
                }
                final String combst = combCount;
                if (combst.length() > 0) {
                    final int comb = Integer.parseInt(combst);
                    if (callback != null) {
                        callback.lianGift(softGiftBean, type, comb);
                    }
                }
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (context instanceof Activity) {
                    ViewUtils.hideSoftInput((Activity) context);
                }
            }
        });
//        mMainHandler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                if (context != null) {
//                    ViewUtils.showSoftInput(context, edit_lian_gift_input);
//                }
//            }
//        }, 100);

    }

    /**
     * 分享框
     */
    public void showShareDialog(final Activity context, final String title, final String shareTitle, final String shareContent, final String singleTitle, final String url, final String logo, boolean canOpenByBrowser, boolean canCopyUrl, final CallbackManager facebookCallbackMgr, final FacebookCallback facebookCallback, final boolean recordEvent, final LiveShowContract.LiveShareListener liveShareListener, final GIOShareTrackBean trackBean, final DialogInterface.OnDismissListener... onDismissListeners) {
        if (context == null) {
            return;
        }
        mDialog = new Dialog(context, R.style.CustomDialogBottom);
        mDialog.show();

        LayoutInflater flater = LayoutInflater.from(TheLApp.getContext());
        View view = flater.inflate(R.layout.layout_share, null);
        mDialog.setContentView(view);
        GridView gridView = view.findViewById(R.id.gridView);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (onDismissListeners.length > 0)// 只在直播页观众分享的时候记这个事件，为了计算观众主动取消的次数
                    MobclickAgent.onEvent(context, "audience_go_to_share");
                if (mDialog != null && mDialog.isShowing()) {
                    mDialog.dismiss();
                }
                if (liveShareListener != null) {
                    liveShareListener.shareSuccess();
                }
                try {
                    final UMShareListener umShareListener = new UMShareListener() {

                        @Override
                        public void onStart(SHARE_MEDIA share_media) {

                        }

                        @Override
                        public void onResult(SHARE_MEDIA platform) {
                            if (recordEvent)
                                MobclickAgent.onEvent(context, "share_succeeded");

                        }

                        @Override
                        public void onError(SHARE_MEDIA platform, Throwable t) {
                            if (t != null && t.getMessage() != null) {
                                DialogUtil.showToastShort(context, t.getMessage());
                            }
                        }

                        @Override
                        public void onCancel(SHARE_MEDIA platform) {
                            DialogUtil.showToastShort(context, TheLApp.getContext().getString(R.string.my_circle_requests_act_canceled));
                        }
                    };
                    switch (position) {
                        case 1:// 微信朋友圈
                            MobclickAgent.onEvent(context, "start_share");
                            // 微信的分享图片大小要做特别的裁剪，否则有些大图在微信中显示不出
                            // new ShareAction(context).setPlatform(SHARE_MEDIA.WEIXIN_CIRCLE).withMedia(new UMImage(TheLApp.getContext(), ImageUtils.buildNetPictureUrl(logo, TheLConstants.WX_SHARE_IMAGE_SIZE, TheLConstants.WX_SHARE_IMAGE_SIZE))).withTargetUrl(url).withTitle(singleTitle).withText(singleTitle).setCallback(umShareListener).share();
                            UMShareUtils.share(context, SHARE_MEDIA.WEIXIN_CIRCLE, new UMImage(TheLApp.getContext(), ImageUtils.buildNetPictureUrl(logo, TheLConstants.WX_SHARE_IMAGE_SIZE, TheLConstants.WX_SHARE_IMAGE_SIZE)), url, singleTitle, singleTitle, umShareListener, trackBean);
                            break;
                        case 0:// 微信消息
                            MobclickAgent.onEvent(context, "start_share");
                            // 微信的分享图片大小要做特别的裁剪，否则有些大图在微信中显示不出
                            //  new ShareAction(context).setPlatform(SHARE_MEDIA.WEIXIN).withMedia(new UMImage(TheLApp.getContext(), ImageUtils.buildNetPictureUrl(logo, TheLConstants.WX_SHARE_IMAGE_SIZE, TheLConstants.WX_SHARE_IMAGE_SIZE))).withTargetUrl(url).withTitle(shareTitle).withText(shareContent).setCallback(umShareListener).share();
                            UMShareUtils.share(context, SHARE_MEDIA.WEIXIN, new UMImage(TheLApp.getContext(), ImageUtils.buildNetPictureUrl(logo, TheLConstants.WX_SHARE_IMAGE_SIZE, TheLConstants.WX_SHARE_IMAGE_SIZE)), url, shareTitle, shareContent, umShareListener, trackBean);
                            break;
                        case 4:// 微博分享
                            MobclickAgent.onEvent(context, "start_share");
                            //  new ShareAction(context).setPlatform(SHARE_MEDIA.SINA).withMedia(new UMImage(TheLApp.getContext(), logo)).withTargetUrl(url).withTitle(singleTitle).withText(singleTitle).setCallback(umShareListener).share();
                            UMShareUtils.share(context, SHARE_MEDIA.SINA, new UMImage(TheLApp.getContext(), logo), url, singleTitle, singleTitle, umShareListener, trackBean);

                            break;
                        case 2:// QQ消息
                            MobclickAgent.onEvent(context, "start_share");
                            //  new ShareAction(context).setPlatform(SHARE_MEDIA.QQ).withMedia(new UMImage(TheLApp.getContext(), logo)).withTargetUrl(url).withTitle(shareTitle).withText(shareContent).setCallback(umShareListener).share();
                            UMShareUtils.share(context, SHARE_MEDIA.QQ, new UMImage(TheLApp.getContext(), logo), url, shareTitle, shareContent, umShareListener, trackBean);
                            break;
                        case 3:// QQ空间
                            // new ShareAction(context).setPlatform(SHARE_MEDIA.QZONE).withMedia(new UMImage(TheLApp.getContext(), logo)).withTargetUrl(url).withTitle(shareTitle).withText(shareContent).setCallback(umShareListener).share();
                            UMShareUtils.share(context, SHARE_MEDIA.QZONE, new UMImage(TheLApp.getContext(), logo), url, shareTitle, shareContent, umShareListener, trackBean);
                            break;
                        case 5:// Facebook
                            MobclickAgent.onEvent(context, "start_share");
                            try {
                                if (ShareDialog.canShow(ShareLinkContent.class)) {
                                    ShareLinkContent contentS = new ShareLinkContent.Builder().setContentUrl(Uri.parse(url)).setContentTitle(shareTitle).setContentDescription(shareContent).setImageUrl(Uri.parse(logo)).build();
                                    ShareDialog shareDialog = new ShareDialog(context);
                                    if (facebookCallbackMgr != null && facebookCallback != null)
                                        shareDialog.registerCallback(facebookCallbackMgr, facebookCallback);
                                    shareDialog.show(contentS);
                                    if (trackBean != null) {
                                        trackBean.shareBean.shareType = GrowingIoConstant.TYPE_FACEBOOK;
                                        GrowingIOUtil.shareTrack(trackBean);
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            break;
                        case 6:// 复制链接
                            DeviceUtils.copyToClipboard(TheLApp.getContext(), url);
                            DialogUtil.showToastShort(context, context.getString(R.string.info_copied));
                            break;
                        case 7:
                            Intent intent = new Intent();
                            intent.setAction("android.intent.action.VIEW");
                            Uri uri = Uri.parse(url);
                            intent.setData(uri);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            TheLApp.getContext().startActivity(intent);
                            break;
                        default:
                            break;
                    }
                } catch (Exception e) {
                }
            }
        });
        ((TextView) view.findViewById(R.id.title)).setText(title);
        List<Integer> images = new ArrayList<Integer>();
        images.add(R.mipmap.btn_share_wechat);
        images.add(R.mipmap.btn_share_moments);
        images.add(R.mipmap.btn_share_qq);
        images.add(R.mipmap.btn_share_qqzone);
        images.add(R.mipmap.btn_share_weibo);
        images.add(R.mipmap.btn_share_facebook);
        List<String> texts = new ArrayList<String>();
        texts.add(TheLApp.getContext().getString(R.string.share_wx));
        texts.add(TheLApp.getContext().getString(R.string.share_moments));
        texts.add(TheLApp.getContext().getString(R.string.share_qq));
        texts.add(TheLApp.getContext().getString(R.string.share_qzone));
        texts.add(TheLApp.getContext().getString(R.string.share_sina));
        texts.add(TheLApp.getContext().getString(R.string.share_fb));
        if (canCopyUrl) {
            images.add(R.drawable.btn_share_link_s);
            texts.add(TheLApp.getContext().getString(R.string.webview_activity_copy_url));
        }
        if (canOpenByBrowser) {
            images.add(R.drawable.btn_share_browser_s);
            texts.add(TheLApp.getContext().getString(R.string.webview_activity_open_in_browser));
        }
        gridView.setAdapter(new ShareGridAdapter(images, texts, gridView));
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.gravity = Gravity.BOTTOM;
        lp.width = context.getResources().getDisplayMetrics().widthPixels; // 设置宽度
        mDialog.getWindow().setAttributes(lp);
        mDialog.setCanceledOnTouchOutside(true);
        if (onDismissListeners.length > 0)
            mDialog.setOnDismissListener(onDismissListeners[0]);
    }

}
