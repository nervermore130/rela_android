package com.thel.modules.live.liveBigGiftAnimLayout;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import androidx.annotation.Nullable;
import android.util.AttributeSet;

/**
 * Created by waiarl on 2017/10/24.
 */

public class AnimImageView extends androidx.appcompat.widget.AppCompatImageView {
    private final Context mContext;

    private Bitmap bitmap = null;

    public AnimImageView(Context context) {
        this(context, null);
    }

    public AnimImageView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AnimImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }

    public AnimImageView init() {
        return this;
    }

    public AnimImageView setAssetBackgoundResource(String foldPath, String path) {
        /*AssetManager am = getResources().getAssets();
        try {
            final InputStream in = am.open(foldPath + "/" + path);
            bitmap = BitmapFactory.decodeStream(in);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();

        }
        setBackgroundDrawable(new BitmapDrawable(bitmap));*/
        bitmap = LiveBigAnimUtils.getBitmap(mContext, foldPath, path);
        setBackgroundDrawable(new BitmapDrawable(bitmap));
        return this;
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        if (bitmap != null) {
            bitmap.recycle();
            bitmap = null;
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }


    public AnimImageView setDimension(int width, int height) {
        setMeasuredDimension(width, height);
        return this;
    }
}
