package com.thel.modules.welcome;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.flutter.OnActivityResultCallback;
import com.thel.flutter.bridge.RfSBridgeHandlerFactory;
import com.thel.manager.ChatServiceManager;
import com.thel.modules.login.login_register.FacebookLogin;
import com.thel.ui.imageviewer.cropiwa.image.CropIwaResultReceiver;
import com.thel.utils.ExecutorServiceUtils;
import com.thel.utils.L;
import com.thel.utils.LogService;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.UserUtils;

/**
 * @author liuyun
 */
public class WelcomeActivity extends BaseActivity {

    private CropIwaResultReceiver cropResultReceiver;

    private CallbackManager callbackManager = CallbackManager.Factory.create();

    private OnActivityResultCallback onActivityResultCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_frame_normal);

        cropResultReceiver = new CropIwaResultReceiver();
        cropResultReceiver.register(this);
        cropResultReceiver.setListener(RfSBridgeHandlerFactory.getInstance().getListener());

        if (!TextUtils.isEmpty(UserUtils.getMyUserId())) {
            ChatServiceManager.getInstance().startService(TheLApp.context);
        }

        if (ShareFileUtils.getBoolean(ShareFileUtils.CRASH_LOG_UPLOAD, false)) {
            ExecutorServiceUtils.getInstatnce().exec(new LogService());
        }

        getSupportFragmentManager().beginTransaction().add(R.id.frame_content,new WelcomeFragment(),WelcomeFragment.class.getSimpleName()).commit();
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return true;
    }

    @Override
    public void onBackPressed() {
        return;
    }

    public CallbackManager getCallbackManager() {
        return callbackManager;
    }

    @Override
    public void setOnActivityResultCallback(OnActivityResultCallback onActivityResultCallback) {
        this.onActivityResultCallback = onActivityResultCallback;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        L.d("FacebookLogin", " ------onActivityResult------ ");

        if (callbackManager != null) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
        if (onActivityResultCallback != null) {
            onActivityResultCallback.onActivityResult(requestCode, resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    protected void onDestroy() {
        if (cropResultReceiver != null) {
            cropResultReceiver.unregister(this);
        }
        super.onDestroy();
    }

    public void facebookLogin() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();

        if (accessToken != null) {

            boolean isLoggedIn = !accessToken.isExpired();

            if (isLoggedIn) {
                FacebookLogin.getInstance().signIn(WelcomeActivity.this, accessToken, null);
            }

        } else {

            FacebookLogin.getInstance().login(WelcomeActivity.this, callbackManager, null);

        }
    }

}
