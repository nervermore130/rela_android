package com.thel.modules.main.me.bean;

import com.thel.base.BaseDataBean;

import java.util.ArrayList;
import java.util.List;

public class MatchListBean extends BaseDataBean {

    public MatchListDataBean data;

    public static class MatchListDataBean {

        public int countdown;
        public int usedOutToday;//1表示用完了，0表示没有
        public int hasValidImages;//1表示不用显示，0表示显示
        public int notReplyMeCount;
        public boolean haveNextPage;
        public String cursor;
        public String rankId;
        public int reuseMatch; //1表示退出热拉后重进

        @Override
        public String toString() {
            return "MatchListDataBean{" + "countdown=" + countdown + ", matchList=" + matchList + '}';
        }

        public List<MatchBean> matchList = new ArrayList<>();
    }
}
