package com.thel.modules.live.in;

import android.media.MediaPlayer;
import android.view.View;

import com.thel.modules.live.bean.LivePkAssisterBean;
import com.thel.modules.live.bean.LivePkFriendBean;

import java.util.List;

/**
 * Created by waiarl on 2017/11/30.
 * 有关Pk与连麦的接口
 */

public class LivePkContract {
    /**
     * 直播好友列表点击Pk或者连麦接口
     */
    public interface FriendRequestClickListener {
        /**
         * @param friendBean 直播中的好友bean
         * @param type       请求类型，0：pk，1：连麦，参考LiveConstant
         * @param position   列表位置
         */
        void onClickRequest(LivePkFriendBean friendBean, int type, int position);

      /*  *//**
         * 点击好友列表中的头像
         *
         * @param friendBean 直播中的好友bean
         * @param logType       请求类型，0：pk，1：连麦，参考LiveConstant
         * @param position   列表位置
         *//*
        void onClickUser(LivePkFriendBean friendBean, int logType, int position);*/
    }

    /**
     * 直播助攻列表点击事件
     */
    public interface AssistRankClickListener {
        /**
         * 点击某个助攻的人
         *
         * @param bean
         * @param position
         */
        void onAssisterClick(LivePkAssisterBean bean, int position);
    }

    /**
     * 直播pk播放动画监听
     */
    public interface LivePkAeAnimListener {
        int ANIM_START = 1;
        int ANIM_CANCEL = 2;
        int ANIM_END = 3;
        int ANIM_REPEAT = 4;

        /**
         * @param view   播放动画的ae view
         * @param status 播放状态
         */
        void onAnimStatus(View view, int status);
    }

    /**
     * 直播Pk音乐播放状态回调
     */
    public interface LivePkPlaySoundListener {
        int SOUND_START = 1;
        int SOUND_CANCEL = 2;
        int SOUND_END = 3;
        int SOUND_REPEAT = 4;

        /**
         * 直播pk声音播放状态
         *
         * @param status
         */
        void onSoundPlay(MediaPlayer player, int status);
    }
}
