package com.thel.modules.live.bean;

/**
 * Created by waiarl on 2018/1/24.
 * 直播排行榜的排行信息（天排行，周排行，总排行）
 */

public class LiveGiftsRankingInfoBean {
    public int rank;//当前排名
    public float delta;//与上一名差距
    public float incoming = 0f;//收入
    public float defeated = 0f;//打败了多少人，百分比
    public String userId;

    public float getDelta() {
        return (float) (Math.round(defeated * 100)) / 100;
    }
}
