package com.thel.modules.main.me;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.Nullable;

import com.google.android.material.appbar.AppBarLayout;

import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseFragment;
import com.thel.bean.AdBean;
import com.thel.bean.SharePosterBean;
import com.thel.bean.moments.MomentsCheckBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterPushImpl;
import com.thel.flutter.bridge.RfSBridgeHandlerFactory;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.growingio.GrowingIoConstant;
import com.thel.modules.live.GiftFirstChargeBean;
import com.thel.modules.main.home.moments.web.WebViewActivity;
import com.thel.modules.main.me.aboutMe.FavoriteActivity;
import com.thel.modules.main.me.aboutMe.MatchActivity;
import com.thel.modules.main.me.aboutMe.MyLevelActivity;
import com.thel.modules.main.me.aboutMe.MySoftMoneyActivity;
import com.thel.modules.main.me.aboutMe.MyWalletActivity;
import com.thel.modules.main.me.aboutMe.StickerStoreActivity;
import com.thel.modules.main.me.aboutMe.UpdateUserInfoActivity;
import com.thel.modules.main.me.aboutMe.WalkmanActivity;
import com.thel.modules.main.me.aboutMe.WorldActivity;
import com.thel.modules.main.me.aboutMe.RelationshipActivity;
import com.thel.modules.main.me.bean.LogInfoBean;
import com.thel.modules.main.me.bean.MyInfoBean;
import com.thel.modules.main.me.match.eventcollect.collect.LiveLogUtils;
import com.thel.modules.main.me.match.eventcollect.collect.MatchLogUtils;
import com.thel.modules.main.me.match.eventcollect.collect.PayLogUtils;
import com.thel.modules.main.nearby.visit.VisitActivity;
import com.thel.modules.main.settings.SettingActivity;
import com.thel.modules.others.UserInfoWinksActivity;
import com.thel.modules.others.VipConfigActivity;
import com.thel.modules.post.MomentPosterActivity;
import com.thel.modules.qrcode.QRActivity;
import com.thel.modules.select_image.SelectLocalImagesActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.service.DefaultRequestService;
import com.thel.ui.MeMatchGuideLayout;
import com.thel.ui.dialog.ActionSheetDialog;
import com.thel.ui.imageviewer.cropiwa.image.CropIwaResultReceiver;
import com.thel.utils.AppInit;
import com.thel.utils.DateUtils;
import com.thel.utils.DeviceUtils;
import com.thel.utils.DialogUtil;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.GsonUtils;
import com.thel.utils.ImageUtils;
import com.thel.utils.L;
import com.thel.utils.PermissionUtil;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SharedPrefUtils;
import com.thel.utils.ToastUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

import org.reactivestreams.Subscription;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static android.app.Activity.RESULT_OK;
import static com.thel.modules.main.me.aboutMe.UpdateUserInfoActivity.FROM_PAGE_SIGN_NAME;
import static com.thel.utils.DeviceUtils.getFilePathFromUri;

/**
 * 我的界面
 * Created by lingwei on 2017/9/18.
 */

public class MeFragment extends BaseFragment implements MeContract.View, View.OnClickListener, AppBarLayout.OnOffsetChangedListener {

    private static MeFragment instance;
    private SwipeRefreshLayout swipe_container;
    private RelativeLayout rel_title;

    // 上半部分
    private View lin_up;
    private LinearLayout layout_winks;
    private LinearLayout layout_follow;
    private SimpleDraweeView me_background;
    private TextView mask;
    private RelativeLayout avatar_area;
    private SimpleDraweeView img_avatar;
    private ImageView img_verify1;
    private TextView txt_verif1;
    private TextView winkNum;
    private TextView followNum;
    private TextView txt_sign_name;//个性签名
    // 情侣部分
    private RelativeLayout partner_avatars;
    private SimpleDraweeView img_avatar_right;
    private SimpleDraweeView img_avatar_left;
    private TextView txt_nickname;
    private TextView txt_love_days;
    // 匹配tip
    private TextView txt_match_tip;
    private Toolbar mToolbar;
    private AppBarLayout appBarLayout;
    private MeContract.Presenter mPresenter;
    private View rootView;

    private TextView img_mark_tip;
    private LinearLayout layout_fans;
    private TextView me_num_fans;
    private TextView txt_title;
    private TextView img_sticker_new;
    private LinearLayout rel_buy_vip;
    private ImageView iv_me_vip;
    private TextView txt_buy_vip;
    private MyInfoBean myInfoBean;
    private ImageView img_setting_tip;
    private TextView txt_setting_tip;
    // 图片的本地文件路径
    private String imageLocalPath;
    private TextView txt_match_unlock;
    private LinearLayout ll_my_level;
    private TextView txt_level;
    /***新手引导测试view***/
    private RelativeLayout rel_seen_me;
    private View guideRectView;
    private CoordinatorLayout coordinatorLayout;
    private NestedScrollView nestedscrollview;
    private TextView img_see_me_new_point;
    private TextView txt_see_me_tip;
    private RelativeLayout rel_wallet;
    private LinearLayout lin_charge;
    private RelativeLayout rel_add_friend;
    private TextView txt_me_gold;
    private LinearLayout lin_my_settings;
    private RelativeLayout rel_scan;
    private LinearLayout sing_name_container;
    private TextView img_match_vip_new;
    private int recentLikeNum;
    private MeMatchGuideLayout match_me_guide;
    private Handler mHandler = new Handler(Looper.getMainLooper());
    private MomentsCheckBean momentsCheckBean;
    private int likeMeNum = 0;
    private ImageView img_level_new;
    private TextView img_adver_title_point;
    private TextView txt_adver_title;
    private RelativeLayout rel_listening_books;
    private int adverId;
    private CropIwaResultReceiver cropResultReceiver;
    private boolean isChangeAvatar = false;
    private String pageId;
    private String advertTitle;
    private String latitude;
    private String longitude;

    public static MeFragment getInstance() {
        instance = new MeFragment();
        return instance;
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            boolean isShow = ShareFileUtils.getBoolean(ShareFileUtils.MY_MATCH_GUIDE, true);
            if (isShow) {
                swipe_container.setEnabled(false);

                //  coordinatorLayout.onNestedScrollAccepted();
                swipe_container.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                    @Override
                    public boolean onPreDraw() {
                        swipe_container.getViewTreeObserver().removeOnPreDrawListener(this);
                        int width = swipe_container.getWidth();
                        int height = swipe_container.getHeight();

                        nestedscrollview.scrollTo(0, (int) (height / 1.2));
                        if (match_me_guide != null) {
                            mHandler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    match_me_guide.show();

                                }
                            }, 1000);
                        }

                        return false;
                    }
                });

            }

        }
        Log.d("onHiddenChanged", "hidden: " + hidden);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_me, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        findViewById();
        setListener();
        latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
        longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        new MePresenter(this);
        if (getActivity() != null && isAdded()) {
            pageId = Utils.getPageId();
            initUI();
            mPresenter.loadNetData();
            mPresenter.registerReceiver();

            cropResultReceiver = new CropIwaResultReceiver();
            cropResultReceiver.register(getActivity());
            cropResultReceiver.setListener(new CropIwaResultReceiver.Listener() {
                @Override
                public void onCropSuccess(Uri croppedUri) {
                    if (isChangeAvatar) {
                        imageLocalPath = getFilePathFromUri(getActivity(), croppedUri);
                        //上传封面
                        mPresenter.handlePhoto(imageLocalPath);
                        isChangeAvatar = false;
                    }
                }

                @Override
                public void onCropFailed(Throwable e) {
                    if (isChangeAvatar) {
                        ToastUtils.showToastShort(getActivity(), "crop failed");
                        isChangeAvatar = false;
                    }
                }
            });
        }
    }

    private void initUI() {
        myInfoBean = ShareFileUtils.getUserData();

        if (myInfoBean != null) {
            refreshUI(myInfoBean);
        }
    }

    public void refreshMeUi() {
        if (getActivity() != null && isAdded()) {
            mPresenter.loadNetData();
        }
    }

    private void findViewById() {
        rel_title = rootView.findViewById(R.id.rel_title);
        //  img_soft_new = rootView.findViewById(R.id.img_soft_new);
        img_mark_tip = rootView.findViewById(R.id.img_mark_tip);
        swipe_container = rootView.findViewById(R.id.swipe_container);
        ViewUtils.initSwipeRefreshLayout(swipe_container);
        appBarLayout = rootView.findViewById(R.id.appbarlayout);
        mToolbar = rootView.findViewById(R.id.toolbar);
        initToolbar();
        txt_title = rootView.findViewById(R.id.txt_title);
        // 最上部分
        lin_up = rootView.findViewById(R.id.lin_up);
        img_avatar_right = rootView.findViewById(R.id.img_avatar_right);
        img_avatar_left = rootView.findViewById(R.id.img_avatar_left);
        txt_nickname = rootView.findViewById(R.id.txt_nickname);
        txt_love_days = rootView.findViewById(R.id.txt_love_days);
        layout_winks = rootView.findViewById(R.id.layout_winks);
        layout_follow = rootView.findViewById(R.id.layout_follow);
        layout_fans = rootView.findViewById(R.id.layout_fans);
        me_num_fans = rootView.findViewById(R.id.me_num_fans);
        avatar_area = rootView.findViewById(R.id.avatar_area);
        partner_avatars = rootView.findViewById(R.id.partner_avatars);
        img_avatar = rootView.findViewById(R.id.img_avatar);
        me_background = rootView.findViewById(R.id.me_background);
        mask = rootView.findViewById(R.id.mask);
        mask.setVisibility(View.INVISIBLE);
        img_verify1 = rootView.findViewById(R.id.img_verify1);
        txt_verif1 = rootView.findViewById(R.id.txt_verif1);
        winkNum = rootView.findViewById(R.id.me_txt_num_wink);
        followNum = rootView.findViewById(R.id.me_txt_num_followers);
        txt_sign_name = rootView.findViewById(R.id.txt_sign_name);
        txt_match_tip = rootView.findViewById(R.id.txt_match_tip);
        txt_match_unlock = rootView.findViewById(R.id.txt_match_unlock);
        rel_buy_vip = rootView.findViewById(R.id.rel_buy_vip);
        iv_me_vip = rootView.findViewById(R.id.iv_me_vip);
        txt_buy_vip = rootView.findViewById(R.id.txt_buy_vip);
        // moments_msg_count = rootView.findViewById(R.id.moments_msg_count);
        //txt_update_info_tip = rootView.findViewById(R.id.txt_update_info_tip);
        //img_update_info_tip = rootView.findViewById(R.id.img_update_info_tip);
        img_setting_tip = rootView.findViewById(R.id.img_setting_tip);
        txt_setting_tip = rootView.findViewById(R.id.txt_setting_tip);
        // moments_msg_remind = rootView.findViewById(R.id.rel_notifacation);
        img_sticker_new = rootView.findViewById(R.id.img_sticker_new);
        TextView txt_version = rootView.findViewById(R.id.txt_version);
        ll_my_level = rootView.findViewById(R.id.ll_my_level);
        txt_level = rootView.findViewById(R.id.txt_level);
        rootView.findViewById(R.id.lin_rate_us).setOnClickListener(this);
        txt_version.setText("v" + DeviceUtils.getVersionName(getActivity()));
        // nearby_user_view = rootView.findViewById(R.id.nearby_user_view);
        rel_wallet = rootView.findViewById(R.id.rel_wallet);
        rel_scan = rootView.findViewById(R.id.rel_scan);
        sing_name_container = rootView.findViewById(R.id.sing_name_container);
        img_level_new = rootView.findViewById(R.id.img_level_new);
        img_adver_title_point = rootView.findViewById(R.id.img_adver_title_point);
        txt_adver_title = rootView.findViewById(R.id.txt_adver_title);
        rel_listening_books = rootView.findViewById(R.id.rel_listening_books);
        /**4.14.0提示我的等级小红点**/
        if (!ShareFileUtils.getBoolean(SharedPrefUtils.MY_LEVEL_POINT, false)) {
            img_level_new.setVisibility(View.VISIBLE);
        }

       /* if (!SharedPrefUtils.getBoolean(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.NEW_STICKER_STORE, false)) {
            img_sticker_new.setVisibility(View.VISIBLE);
        }*/
        /***新手引导的一个测试view***/
        coordinatorLayout = rootView.findViewById(R.id.coordinatorLayout);
        rel_seen_me = rootView.findViewById(R.id.rel_seen_me);
        img_see_me_new_point = rootView.findViewById(R.id.img_see_me_new);
        // rel_nearby_person = rootView.findViewById(R.id.rel_nearby_person);
        txt_see_me_tip = rootView.findViewById(R.id.txt_see_me_tip);
        guideRectView = rootView.findViewById(R.id.see_me_guide_view);
        nestedscrollview = rootView.findViewById(R.id.nestedscrollview);
        // see_nearby_guide_rect_view = rootView.findViewById(R.id.see_nearby_guide_view);
        lin_charge = rootView.findViewById(R.id.lin_charge);
        rel_add_friend = rootView.findViewById(R.id.rel_add_friend);
        txt_me_gold = rootView.findViewById(R.id.txt_me_gold);
        lin_my_settings = rootView.findViewById(R.id.lin_my_settings);
        rootView.findViewById(R.id.sing_name_container).setOnClickListener(this);
        img_match_vip_new = rootView.findViewById(R.id.img_match_vip_new);
        match_me_guide = rootView.findViewById(R.id.match_me_guide);
        //提示首充优惠
        getGiftIsFirstCharge();

    }

    @SuppressLint("ClickableViewAccessibility")
    private void setListener() {
        rel_title.setOnClickListener(this);
        layout_winks.setOnClickListener(this);
        layout_follow.setOnClickListener(this);
        layout_fans.setOnClickListener(this);
        me_background.setOnClickListener(this);
        img_avatar.setOnClickListener(this);
        rel_buy_vip.setOnClickListener(this);
        ll_my_level.setOnClickListener(this);
        rel_seen_me.setOnClickListener(this);
        rel_wallet.setOnClickListener(this);
        rel_scan.setOnClickListener(this);

        rel_add_friend.setOnClickListener(this);
        lin_my_settings.setOnClickListener(this);
        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.loadNetData();
            }
        });
        if (appBarLayout != null) {//appbarlayout,设置下拉滑动与下拉刷新的冲突
            appBarLayout.addOnOffsetChangedListener(this);
        }
        // rootView.findViewById(R.id.rel_update_info).setOnClickListener(this);
        rootView.findViewById(R.id.rel_homepage).setOnClickListener(this);
        // rootView.findViewById(R.id.rel_circle).setOnClickListener(this);
        rootView.findViewById(R.id.rel_sticker_store).setOnClickListener(this);
        rootView.findViewById(R.id.frame_go_to_mark).setOnClickListener(this);
        rootView.findViewById(R.id.rel_setting).setOnClickListener(this);
        rootView.findViewById(R.id.rel_soft_money).setOnClickListener(this);
        rootView.findViewById(R.id.lin_rate_us).setOnClickListener(this);
        img_avatar_right.setOnClickListener(this);
        img_avatar_left.setOnClickListener(this);
        rootView.findViewById(R.id.rel_world_roaming).setOnClickListener(this);
        rootView.findViewById(R.id.rel_match).setOnClickListener(this);
        rootView.findViewById(R.id.rel_help_feedback).setOnClickListener(this);
        rel_buy_vip.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        v.setAlpha(0.5f);
                        break;
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL:
                        v.setAlpha(1);
                        break;
                }
                return false;
            }
        });
        ll_my_level.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        v.setAlpha(0.5f);
                        break;
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL:
                        v.setAlpha(1);
                        break;
                }
                return false;
            }
        });
        rel_listening_books.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), WalkmanActivity.class);
                startActivity(intent);
                ShareFileUtils.setBoolean(ShareFileUtils.ME_ADS, true);
                img_adver_title_point.setVisibility(View.GONE);
                Intent intent2 = new Intent();
                intent2.setAction(TheLConstants.BROADCAST_CLEAR_RELA_LISTENER_NEW_CHECK_ACTION);
                getActivity().sendBroadcast(intent2);
                ShareFileUtils.setBoolean(ShareFileUtils.ME_ADS, true);
                ShareFileUtils.setInt(ShareFileUtils.ME_ADS_ID, adverId);
                traceMyInfoLog("click_walkman", 1, "", advertTitle);


            }
        });
    }

    @Override
    public void setPresenter(MeContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    public void initToolbar() {
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
    }

    public void getGiftIsFirstCharge() {
        Flowable<GiftFirstChargeBean> beanFlowable = DefaultRequestService.createAllRequestService().getIsFirstCharge();
        beanFlowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<GiftFirstChargeBean>() {
            @Override
            public void onNext(GiftFirstChargeBean data) {
                super.onNext(data);
                showIsFirstCharge(data);

            }
        });

    }

    public void showIsFirstCharge(GiftFirstChargeBean firstChargeBean) {
        if (firstChargeBean != null && firstChargeBean.data != null) {
            //缓存到本地
            TheLConstants.IsFirstCharge = firstChargeBean.data.isFirstCharge;

            if (firstChargeBean.data.isFirstCharge == 1) {
                lin_charge.setVisibility(View.VISIBLE);
                txt_me_gold.setVisibility(View.GONE);

            } else {
                lin_charge.setVisibility(View.GONE);
                //显示我的软妹豆数量
                txt_me_gold.setVisibility(View.VISIBLE);

                if (myInfoBean != null) {
                    if (myInfoBean.gold > 0) {
                        txt_me_gold.setText(TheLApp.context.getString(R.string.soft_moneys, myInfoBean.gold));

                    } else {
                        txt_me_gold.setText(TheLApp.context.getString(R.string.soft_money, myInfoBean.gold));

                    }
                }

            }
        }
    }

    /**
     * refresh与appbarlayout滑动冲突
     *
     * @param appBarLayout
     * @param verticalOffset
     */

    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        final int totalRange = appBarLayout.getTotalScrollRange();
        if (swipe_container != null) {
            swipe_container.setEnabled(verticalOffset >= 0);
        }
        final float start = (0 - totalRange) * 0.7f;
        final float end = (0 - totalRange) * 0.9f;
        if (verticalOffset < start && verticalOffset > end) {
            final float alp = (verticalOffset - end) / (totalRange * 0.2f);
            lin_up.setAlpha(alp);
            rel_buy_vip.setAlpha(alp);
            ll_my_level.setAlpha(alp);
            txt_title.setAlpha(1 - alp);
        } else if (verticalOffset >= start) {
            lin_up.setAlpha(1);
            rel_buy_vip.setAlpha(1);
            ll_my_level.setAlpha(1);
            txt_title.setAlpha(0);
        } else if (verticalOffset <= end) {
            lin_up.setAlpha(0);
            rel_buy_vip.setAlpha(0);
            ll_my_level.setAlpha(0);
            txt_title.setAlpha(1);
        }
    }

    @Override
    public void onResume() {
        if (getActivity() != null && isAdded()) {
            mPresenter.loadNetData();
            getGiftIsFirstCharge();
            loadAdver();
        }
        if (TextUtils.isEmpty(ShareFileUtils.getString(ShareFileUtils.BIND_CELL, ""))) {
            setSettingsErrImgVisible(true);

        } else {
            setSettingsErrImgVisible(false);
        }
        boolean clear_newlike = ShareFileUtils.getBoolean(ShareFileUtils.MATCH_SAW_LIKE_ME, false);
        if (clear_newlike) {
            txt_match_tip.setVisibility(View.GONE);
            img_match_vip_new.setVisibility(View.GONE);
          /*  txt_match_tip.setBackground(null);
            txt_match_tip.setTextColor(ContextCompat.getColor(TheLApp.context, R.color.text_color_gray));
*/
        }
        //红点提醒开关关闭之后 移除消息显示
        if (img_see_me_new_point.getVisibility() == View.VISIBLE || txt_see_me_tip.getVisibility() == View.VISIBLE) {
            if (ShareFileUtils.getInt(ShareFileUtils.MESSAGE_REDPOINT, 1) == 2) {
                clearSeeMeNum();

            }
        }
        super.onResume();

    }

    private void loadAdver() {
        RequestBusiness.getInstance().getAd().onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<AdBean>() {
            @Override
            public void onNext(AdBean data) {
                super.onNext(data);
                String json = GsonUtils.createJsonString(data);
                ShareFileUtils.setString(ShareFileUtils.ADS, json);
                final List<AdBean.Map_list> ad_list = data.data.map_list;
                if (ad_list != null && ad_list.size() > 0) {
                    for (AdBean.Map_list bean : ad_list) {
                        if (bean.advertLocation.equals("fm") && Utils.isChaneseLanguage()) {
                            rel_listening_books.setVisibility(View.VISIBLE);

                            advertTitle = bean.advertTitle;
                            adverId = bean.id;


                            txt_adver_title.setText(advertTitle);
                            /***4.15.0热拉随身听**/

                            if (adverId != 0 && !ShareFileUtils.getBoolean(ShareFileUtils.ME_ADS, false)) { // 服务端的数据返回不为0 并且从未点击过 就展示小红点
                                img_adver_title_point.setVisibility(View.VISIBLE);

                                if (getActivity() != null) {
                                    Intent intent = new Intent();
                                    intent.setAction(TheLConstants.BROADCAST_ACTION_WALKMAN_NEW);
                                    getActivity().sendBroadcast(intent);
                                }
                            } else if (ShareFileUtils.getBoolean(ShareFileUtils.ME_ADS, false) && (ShareFileUtils.getInt(ShareFileUtils.ME_ADS_ID, 1) > 1 && ShareFileUtils.getInt(ShareFileUtils.ME_ADS_ID, 1) != adverId)) {
                                ShareFileUtils.setBoolean(ShareFileUtils.ME_ADS, false);
                                img_adver_title_point.setVisibility(View.VISIBLE);

                                if (getActivity() != null) {
                                    Intent intent = new Intent();
                                    intent.setAction(TheLConstants.BROADCAST_ACTION_WALKMAN_NEW);
                                    getActivity().sendBroadcast(intent);
                                }
                            }


                        }

                    }
                }
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unSubscribe();
    }

    public void traceMyInfoLog(String activity, int redPot, String completion, String text) {
        try {
            LogInfoBean logInfoBean = new LogInfoBean();
            logInfoBean.page = "my";
            logInfoBean.page_id = pageId;
            logInfoBean.activity = activity;

            logInfoBean.lat = latitude;
            logInfoBean.lng = longitude;

            LogInfoBean.LogsDataBean logsDataBean = new LogInfoBean.LogsDataBean();
            if (redPot != 0) {
                logsDataBean.redspot = redPot;

            }
            if (!TextUtils.isEmpty(text)) {
                logsDataBean.text = text;
            }

            if (!TextUtils.isEmpty(completion)) {
                logsDataBean.completion = completion;
            }
            logInfoBean.data = logsDataBean;

            LiveLogUtils.getInstance().addLog(logInfoBean);

        } catch (Exception e) {

        }
    }


    @Override
    public void onClick(View view) {
        ViewUtils.preventViewMultipleClick(view, 1000);
        int viewId = view.getId();
        switch (viewId) {
            case R.id.rel_homepage:
                MobclickAgent.onEvent(TheLApp.getContext(), "my_moment_click");
                String myId = ShareFileUtils.getString(ShareFileUtils.ID, "");
//                Intent intentUserInfo = new Intent();
//                intentUserInfo.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, myId);
//                intentUserInfo.setClass(getActivity(), UserInfoActivity.class);
//                startActivity(intentUserInfo);
                FlutterRouterConfig.Companion.gotoUserInfo(myId);
                traceMyInfoLog("click_homepage", 0, "", "");

                break;
            case R.id.rel_setting:

                startActivity(new Intent(getActivity(), UpdateUserInfoActivity.class));
                if (myInfoBean != null) {
                    traceMyInfoLog("click_profile", 0, myInfoBean.ratio + "", "");

                } else {
                    traceMyInfoLog("click_profile", 0, "0", "");

                }

                break;
            case R.id.rel_wallet: //我的钱包
                startActivity(new Intent(getActivity(), MyWalletActivity.class));
                break;
            case R.id.rel_scan:
                PermissionUtil.requestCameraPermission(getActivity(), new PermissionUtil.PermissionCallback() {
                    @Override
                    public void granted() {
                        startActivity(new Intent(getActivity(), QRActivity.class));
                    }

                    @Override
                    public void denied() {

                    }

                    @Override
                    public void gotoSetting() {

                    }
                });
                traceMyInfoLog("click_scan", 0, "", "");

                break;
            case R.id.lin_rate_us:
                ViewUtils.directToAppStore(TheLApp.getContext());
                try {
                    LogInfoBean logInfoBean = new LogInfoBean();
                    logInfoBean.page = "my";
                    logInfoBean.page_id = pageId;
                    logInfoBean.activity = "click_judge";

                    logInfoBean.lat = latitude;
                    logInfoBean.lng = longitude;

                    LogInfoBean.LogsDataBean logsDataBean = new LogInfoBean.LogsDataBean();

                    logsDataBean.version = DeviceUtils.getVersionCode(TheLApp.getContext()) + "";
                    logInfoBean.data = logsDataBean;

                    LiveLogUtils.getInstance().addLog(logInfoBean);

                } catch (Exception e) {

                }
                break;
            case R.id.img_avatar:
            case R.id.img_avatar_left:
                new ActionSheetDialog(getActivity()).builder().setCancelable(true).setCanceledOnTouchOutside(true)
                        .addSheetItem(getString(R.string.change_avatar), ActionSheetDialog.SheetItemColor.BLACK, new ActionSheetDialog.OnSheetItemClickListener() {
                            @Override
                            public void onClick(int which) {
                                isChangeAvatar = true;
                                Intent intent = new Intent(getActivity(), SelectLocalImagesActivity.class);
                                intent.putExtra("isAvatar", true);
                                startActivityForResult(intent, TheLConstants.BUNDLE_CODE_UPDATE_USER_INFO_ACTIVITY);
                            }
                        }).show();
                break;
            case R.id.img_avatar_right:
//                Intent intent_right = new Intent();
//                intent_right.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, myInfoBean.paternerd.paternerId + "");
//                intent_right.setClass(getActivity(), UserInfoActivity.class);
//                startActivity(intent_right);
                FlutterRouterConfig.Companion.gotoUserInfo(myInfoBean.paternerd.paternerId + "");
                break;
            case R.id.me_background:
                if (getActivity() != null) {
                    new ActionSheetDialog(getActivity()).builder().setCancelable(true).setCanceledOnTouchOutside(true).addSheetItem(getString(R.string.change_bg), ActionSheetDialog.SheetItemColor.BLACK, new ActionSheetDialog.OnSheetItemClickListener() {
                        @Override
                        public void onClick(int which) {
                            startActivityForResult(new Intent(getActivity(), SelectLocalImagesActivity.class), TheLConstants.BUNDLE_CODE_SELECT_BG);
                        }
                    }).show();
                }

                break;
            case R.id.lin_my_settings:// 跳转设置页
                startActivity(new Intent(getActivity(), SettingActivity.class));
                break;
            case R.id.frame_go_to_mark:// 进入收藏界面
                img_mark_tip.setVisibility(View.GONE);
                SharedPrefUtils.setBoolean(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.MARK_ENTRANCE, true);
                startActivity(new Intent(getActivity(), FavoriteActivity.class));
                traceMyInfoLog("click_mark", 0, "", "");

                break;
            case R.id.rel_world_roaming:// 世界漫游
                PermissionUtil.requestLocationPermission(getActivity(), new PermissionUtil.PermissionCallback() {
                    @Override
                    public void granted() {
                        PermissionUtil.requestStoragePermission(getActivity(), new PermissionUtil.PermissionCallback() {
                            @Override
                            public void granted() {
                                startActivity(new Intent(getActivity(), WorldActivity.class));
                            }

                            @Override
                            public void denied() {

                            }

                            @Override
                            public void gotoSetting() {

                            }
                        });
                    }

                    @Override
                    public void denied() {

                    }

                    @Override
                    public void gotoSetting() {

                    }
                });
                traceMyInfoLog("click_world", 0, "", "");

                break;
            case R.id.rel_buy_vip: //跳转到会员设置页
                MobclickAgent.onEvent(TheLApp.getContext(), "premium_click");
                GrowingIOUtil.payTrack(GrowingIOUtil.USER_PAGE_MENBERSHIP_CLICK);

                Intent vipConfigIntent = new Intent(getActivity(), VipConfigActivity.class);
                startActivity(vipConfigIntent);
                traceMyInfoLog("click_vip", 0, "", "");

                break;
            case R.id.layout_winks://人气页面
                Intent intent = new Intent(getActivity(), UserInfoWinksActivity.class);
                intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, ShareFileUtils.getString(ShareFileUtils.ID, ""));
                startActivityForResult(intent, 101);
                traceMyInfoLog("click_popularity", 0, "", "");
                break;
            case R.id.layout_fans:
                ViewUtils.preventViewMultipleClick(view, 2000);
                RelationshipActivity.startActivity(getActivity(), RelationshipActivity.TYPE_FANS);
                traceMyInfoLog("click_fans", 0, "", "");

                break;
            case R.id.layout_follow:
                RelationshipActivity.startActivity(getActivity(), RelationshipActivity.TYPE_FOLLOW);
                traceMyInfoLog("click_follow", 0, "", "");

                break;
           /* case R.id.rel_notifacation:
                unreadMomentsMsg = 0;
                clearMsgRemind();
                MobclickAgent.onEvent(TheLApp.getContext(), "moment_message_click");
                Intent intent3 = new Intent(getActivity(), MomentsMsgsActivity.class);
                startActivityForResult(intent3, 0);
                break;*/
            case R.id.rel_sticker_store:// 表情商店
                SharedPrefUtils.setBoolean(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.NEW_STICKER_STORE, true);
                img_sticker_new.setVisibility(View.INVISIBLE);
                Intent i6 = new Intent();
                if (myInfoBean != null) {
                    i6.putExtra("vipLevel", myInfoBean.level);
                }
                i6.setClass(getActivity(), StickerStoreActivity.class);
                startActivity(i6);
                traceMyInfoLog("click_emoji", 0, "", "");

                break;
            case R.id.rel_soft_money:
                GrowingIOUtil.payTrack(GrowingIOUtil.USER_PAGE_CLICK);
                GrowingIOUtil.payEvar(GrowingIoConstant.ENTRY_PERSONAL_PAGE);
                SharedPrefUtils.setBoolean(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.NEW_SOFT_RECOMMEND, true);
                startActivity(new Intent(getActivity(), MySoftMoneyActivity.class));
                /**
                 * 打点上报
                 * */
                long time = System.currentTimeMillis();
                PayLogUtils.getInstance().reportShortPayLog(GrowingIoConstant.PAGE_MY, pageId, "click_softbean", TheLConstants.IsFirstCharge, "", "");
                break;
            case R.id.rel_help_feedback: //帮助与反馈
                ViewUtils.preventViewMultipleClick(view, 2000);
                MobclickAgent.onEvent(TheLApp.getContext(), "support_click");
                Intent i7 = new Intent(getActivity(), WebViewActivity.class);
                i7.putExtra(WebViewActivity.SITE, "help");
                startActivity(i7);
                traceMyInfoLog("click_help", 0, "", "");

                break;
            case R.id.ll_my_level: //我的等级
                ViewUtils.preventViewMultipleClick(view, 2000);
                Intent intent3 = new Intent(getActivity(), MyLevelActivity.class);
                startActivity(intent3);
                ShareFileUtils.setBoolean(SharedPrefUtils.MY_LEVEL_POINT, true);
                img_level_new.setVisibility(View.GONE);
                traceMyInfoLog("click_rank", 0, "", "");

                break;
            case R.id.rel_seen_me://谁来看过我
                ViewUtils.preventViewMultipleClick(view, 2000);
                gotoVisitActivity();
                clearSeeMeNum();
                traceMyInfoLog("click_visit", seemeNum, "", "");

                break;
            case R.id.rel_match://  匹配
                ViewUtils.preventViewMultipleClick(view, 2000);
                Intent matchIntent = new Intent(getActivity(), MatchActivity.class);

                if (myInfoBean.match != null) {
                    matchIntent.putExtra("sumLikeCount", myInfoBean.match.likeCount);
                    matchIntent.putExtra("notReplyMeCount", myInfoBean.match.notReplyMeCount);
                    matchIntent.putExtra("hasSeenCursor", myInfoBean.cursor);
                    matchIntent.putExtra("ratio", myInfoBean.ratio);
                    ShareFileUtils.setInt(ShareFileUtils.SUM_LIKE_COUNT, myInfoBean.match.likeCount);
                }
                Bundle bundle = new Bundle();
                bundle.putString(ShareFileUtils.MATCH_FROM_PAGE_ID, pageId);
                bundle.putString(ShareFileUtils.MATCH_FROM_PAGE, "my");
                matchIntent.putExtras(bundle);
                startActivity(matchIntent);

                LogInfoBean logInfoBean = new LogInfoBean();
                logInfoBean.page = "my";
                logInfoBean.page_id = pageId;
                logInfoBean.activity = "match";
                logInfoBean.lat = latitude;
                logInfoBean.lng = longitude;
                LogInfoBean.LogsDataBean logsDataBean = new LogInfoBean.LogsDataBean();
                logsDataBean.msg = likeMeNum + "";
                logInfoBean.data = logsDataBean;

                MatchLogUtils.getInstance().addLog(logInfoBean);


                break;
            case R.id.rel_add_friend: //邀请好友
                ViewUtils.preventViewMultipleClick(view, 1000);
                SharedPrefUtils.setBoolean(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.NEW_SHARE_ME, true);
                MobclickAgent.onEvent(TheLApp.getContext(), "invite_friends_click");
                SharePosterBean sharePosterBean = new SharePosterBean();
                String nickName = ShareFileUtils.getString(ShareFileUtils.USER_NAME, "");
                String avatar = ShareFileUtils.getString(ShareFileUtils.AVATAR, "");
                String relaId = ShareFileUtils.getString(ShareFileUtils.USER_THEL_ID, "");
                String bg_url = ShareFileUtils.getString(ShareFileUtils.USER_BACKGROUND, "");
                sharePosterBean.avatar = avatar;
                sharePosterBean.imageUrl = bg_url;
                sharePosterBean.nickname = nickName;
                sharePosterBean.userName = relaId;
                sharePosterBean.from = MomentPosterActivity.FROM_ADDFRIEND;
                MomentPosterActivity.gotoShare(sharePosterBean);
                traceMyInfoLog("click_invite", 0, "", "");

                break;
            case R.id.sing_name_container:
                Intent intent1 = new Intent(getActivity(), UpdateUserInfoActivity.class);
                intent1.putExtra(UpdateUserInfoActivity.FROM_PAGE, FROM_PAGE_SIGN_NAME);
                intent1.putExtra("selfintroduction", myInfoBean.intro);
                startActivity(intent1);
                break;
        }

    }

    private void gotoVisitActivity() {
        if (getActivity() == null) {
            return;
        }
        MobclickAgent.onEvent(TheLApp.getContext(), "who_look_me_click");
        final Intent intent = new Intent(getActivity(), VisitActivity.class);
        startActivity(intent);
    }

    @Override
    public void setDetailErrImgVisible(boolean settingsErrImgVisible) {
       /* if (settingsErrImgVisible) {
            txt_update_info_tip.setVisibility(View.VISIBLE);
            img_update_info_tip.setVisibility(View.VISIBLE);
        } else {
            txt_update_info_tip.setVisibility(View.GONE);
            img_update_info_tip.setVisibility(View.GONE);
        }*/
    }

    public void setSettingsErrImgVisible(boolean detailErrImgVisible) {
        if (detailErrImgVisible) {
           /* img_setting_tip.setVisibility(View.VISIBLE);
            txt_setting_tip.setVisibility(View.VISIBLE);
            txt_setting_tip.setText(TheLApp.context.getString(R.string.phone_not_bound));*/
        } else {
            if (myInfoBean != null) {
                txt_setting_tip.setVisibility(View.VISIBLE);
                img_setting_tip.setVisibility(View.GONE);

                try {
                    if (myInfoBean.ratio != 100) {

                        txt_setting_tip.setText(TheLApp.context.getString(R.string.completed_xx, myInfoBean.ratio) + "%");
                    } else {
                        txt_setting_tip.setVisibility(View.GONE);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    img_setting_tip.setVisibility(View.GONE);
                    txt_setting_tip.setVisibility(View.GONE);
                }

            } else {

                img_setting_tip.setVisibility(View.GONE);
                txt_setting_tip.setVisibility(View.GONE);
            }
        }
    }


    @Override
    public void updateMsgRemind(int unreadMomentsMsg) {
       /* if (unreadMomentsMsg > 0) {
            if (unreadMomentsMsg >= 99) {
                moments_msg_count.setText("99+");
            } else {
                moments_msg_count.setText(unreadMomentsMsg + "");
            }
            moments_msg_count.setVisibility(View.VISIBLE);
        }*/
    }

    @Override
    public void clearMsgRemind() {
       /* if (moments_msg_count == null) {
            return;
        }
        moments_msg_count.setText("");
        moments_msg_count.setVisibility(View.GONE);*/

        //清除底部tab 数字提示

        if (getActivity() != null) {
            Intent intent = new Intent();
            intent.setAction(TheLConstants.BROADCAST_CLEAR_NEW_MOMENT_MSG);
            getActivity().sendBroadcast(intent);
        }
    }

    @Override
    public void clearSeeMeNum() {
        if (getActivity() != null) {
            Intent intent = new Intent();
            intent.setAction(TheLConstants.BROADCAST_CLEAR_NEW_CHECK_ACTION);
            getActivity().sendBroadcast(intent);
            txt_see_me_tip.setVisibility(View.GONE);
            img_see_me_new_point.setVisibility(View.GONE);
        }
    }

    @Override
    public void clearNewLikeMeNum() {
        ShareFileUtils.setInt(ShareFileUtils.NEW_LIKE_NUM, 0);
        txt_match_tip.setVisibility(View.GONE);
        img_match_vip_new.setVisibility(View.GONE);
    }

    @Override
    public void uploadAvatar(String avatar) {
        ShareFileUtils.setString(ShareFileUtils.AVATAR, avatar);
        img_avatar.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE)));
        img_avatar_left.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE)));
    }

    @Override
    public void onRequestFinished() {
        closeLoading();
        if (swipe_container != null && swipe_container.isRefreshing()) {
            swipe_container.setRefreshing(false);
        }
    }

    @Override
    public void requestFailed() {
        closeLoading();
        DialogUtil.showToastShort(getActivity(), TheLApp.getContext().getString(R.string.message_network_error));
    }

    @Override
    public void refreshRatioUI() {
        int userRatio = ShareFileUtils.getUserRatio();
        if (txt_setting_tip != null && userRatio != 0) {
            txt_setting_tip.setVisibility(View.VISIBLE);
            txt_setting_tip.setText(TheLApp.context.getString(R.string.completed_xx, myInfoBean.ratio) + "%");

        } else {
            txt_setting_tip.setVisibility(View.GONE);
        }
    }

    int seemeNum = 0;

    /**
     * 谁来看过我
     *
     * @param checkBean
     */
    @Override
    public void updateNewVisitorUI(MomentsCheckBean checkBean) {

        if (checkBean == null) {
            seemeNum = ShareFileUtils.getInt(ShareFileUtils.NEW_CHECK_NUM, 0);

        } else {
            if (checkBean.notReadViewMeNum != 0 || checkBean.notReadCheckRemind != 0) {
                seemeNum = checkBean.notReadViewMeNum;
            } else if (checkBean.notReadCheckRemind != 0) {
                seemeNum = -1;

            } else if (checkBean.notReadViewMeNum == 0 && checkBean.notReadCheckRemind == 0) {
                seemeNum = 0;
            }
        }
        if (checkBean != null) {
            if (seemeNum > 0) {
                img_see_me_new_point.setVisibility(View.VISIBLE);
                txt_see_me_tip.setVisibility(View.VISIBLE);
                txt_see_me_tip.setText("+" + seemeNum);
            } else if (seemeNum == -1) {
                img_see_me_new_point.setVisibility(View.VISIBLE);
                txt_see_me_tip.setVisibility(View.GONE);
            } else {
                txt_see_me_tip.setVisibility(View.GONE);
                img_see_me_new_point.setVisibility(View.GONE);
            }
        }

        /*
         * 没有会员
         * **/
//更新谁喜欢我

        if (UserUtils.getUserVipLevel() <= 0) {
            if (checkBean.likeMeNum > 0) {

                ShareFileUtils.setBoolean(ShareFileUtils.MATCH_SAW_LIKE_ME, false);
                txt_match_tip.setVisibility(View.VISIBLE);
                img_match_vip_new.setVisibility(View.GONE);
                txt_match_tip.setBackgroundResource(R.drawable.bg_new_likecount_label_shape_pink);
                txt_match_tip.setTextColor(ContextCompat.getColor(TheLApp.context, R.color.white));
                if (checkBean.likeMeNum > 1) {
                    txt_match_tip.setText(TheLApp.getContext().getString(R.string.new_like_me, checkBean.likeMeNum));

                } else {
                    txt_match_tip.setText(TheLApp.getContext().getString(R.string.new_like_me_odd, checkBean.likeMeNum));
                }

            } else {
                txt_match_tip.setVisibility(View.GONE);
                img_match_vip_new.setVisibility(View.GONE);
                if (ShareFileUtils.getBoolean(ShareFileUtils.NEW_MATCH_SETTINGS, true)) {   //匹配设置新版本显示的小红点
                    img_match_vip_new.setVisibility(View.VISIBLE);

                }

            }
        } else {
            /**
             * 有会员显示
             * */
            if (checkBean.likeMeNum > 0) {
                txt_match_tip.setTextColor(ContextCompat.getColor(TheLApp.context, R.color.text_color_gray));
                txt_match_tip.setBackground(null);
                ShareFileUtils.setBoolean(ShareFileUtils.MATCH_SAW_LIKE_ME, false);
                txt_match_tip.setVisibility(View.VISIBLE);

                img_match_vip_new.setVisibility(View.VISIBLE);
                txt_match_tip.setText("+" + checkBean.likeMeNum);
            } else {
                txt_match_tip.setVisibility(View.GONE);
                img_match_vip_new.setVisibility(View.GONE);
                if (ShareFileUtils.getBoolean(ShareFileUtils.NEW_MATCH_SETTINGS, true)) {  //匹配设置新版本显示的小红点
                    img_match_vip_new.setVisibility(View.VISIBLE);

                }
            }

        }

        if (!TextUtils.isEmpty(checkBean.likeMeCursor)) {
            ShareFileUtils.setString(ShareFileUtils.MATCH_LIKE_ME_OLD_CURSOR, checkBean.likeMeCursor);
        }

    }

    @Override
    public void refreshUI(MyInfoBean myInfoBean) {
        try {
            this.myInfoBean = myInfoBean;
            if (myInfoBean != null) {
                ShareFileUtils.saveUserData(myInfoBean);
                if (myInfoBean.paternerd == null) {

                    avatar_area.setVisibility(View.VISIBLE);
                    partner_avatars.setVisibility(View.GONE);
                    sing_name_container.setVisibility(View.VISIBLE);
                    String avatar = ImageUtils.buildNetPictureUrl(myInfoBean.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE);
                    img_avatar.setImageURI(Uri.parse(avatar));
                    avatar_area.setBackgroundResource(R.mipmap.bg_head_frame);
                    txt_title.setText(myInfoBean.nickName);
                    if (myInfoBean.level > 0) {
                        /***
                         *获取今天的时间
                         * 如果小于等于五天就显示会员将过期
                         * **/

                        int haveExpiredTime = haveExpiredTime(myInfoBean.levelExpireTime);
                        if (haveExpiredTime == 0) {
                            String vipcontent = TheLApp.getContext().getString(R.string.vip_will_expired);
                            txt_buy_vip.setText(vipcontent + " >");

                        } else if (haveExpiredTime == -1) {
                            String vipcontent = TheLApp.getContext().getString(R.string.vip_expired);
                            txt_buy_vip.setText(vipcontent + " >");

                        } else {
                            String vipcontent = TheLApp.getContext().getString(R.string.vip_config_act_title);
                            txt_buy_vip.setText(vipcontent + " >");
                        }
                        //   img_vip2.setVisibility(View.VISIBLE);

                        switch (myInfoBean.level) {
                            case 1:
                                //  img_vip2.setImageResource(R.mipmap.icn_vip_1);
                                iv_me_vip.setImageResource(R.mipmap.icn_vip_1);

                                break;
                            case 2:
                                //  img_vip2.setImageResource(R.mipmap.icn_vip_2);
                                iv_me_vip.setImageResource(R.mipmap.icn_vip_2);

                                break;
                            case 3:
                                // img_vip2.setImageResource(R.mipmap.icn_vip_3);
                                iv_me_vip.setImageResource(R.mipmap.icn_vip_3);

                                break;
                            case 4:
                                //  img_vip2.setImageResource(R.mipmap.icn_vip_4);
                                iv_me_vip.setImageResource(R.mipmap.icn_vip_4);

                                break;
                        }
                    } else {
                        iv_me_vip.setImageResource(R.mipmap.icn_vip);
                        /**
                         * 会员到期
                         * **/
                        if (!TextUtils.isEmpty(myInfoBean.levelExpireTime)) {
                            txt_buy_vip.setText(TheLApp.context.getString(R.string.vip_expired));

                        } else {
                            /***
                             * 没有开通过会员
                             * **/
                            String not_vip = TheLApp.getContext().getString(R.string.buy_vip);
                            txt_buy_vip.setText(not_vip + " >");
                        }


                    }
                } else {// 有partner
                    avatar_area.setVisibility(View.GONE);
                    partner_avatars.setVisibility(View.VISIBLE);
                    img_avatar_right.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(myInfoBean.paternerd.paternerAvatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE)));
                    img_avatar_left.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(myInfoBean.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE)));
                    if (myInfoBean.paternerd.paternerNickName != null && myInfoBean.paternerd.days != null) {
                        txt_love_days.setText(String.format(getString(R.string.updatauserinfo_activity_anniversary), myInfoBean.paternerd.paternerNickName, myInfoBean.paternerd.days));
                    }
                    if (myInfoBean.level > 0) {
                        int haveExpiredTime = haveExpiredTime(myInfoBean.levelExpireTime);
                        if (haveExpiredTime == 0) {
                            String vipcontent = TheLApp.getContext().getString(R.string.vip_will_expired);
                            txt_buy_vip.setText(vipcontent + " >");

                        } else if (haveExpiredTime == -1) {
                            String vipcontent = TheLApp.getContext().getString(R.string.vip_expired);
                            txt_buy_vip.setText(vipcontent + " >");

                        } else {
                            String vipcontent = TheLApp.getContext().getString(R.string.vip_config_act_title);
                            txt_buy_vip.setText(vipcontent + " >");
                        }

                        switch (myInfoBean.level) {
                            case 1:
                                iv_me_vip.setImageResource(R.mipmap.icn_vip_1);

                                break;
                            case 2:
                                iv_me_vip.setImageResource(R.mipmap.icn_vip_2);

                                break;
                            case 3:
                                iv_me_vip.setImageResource(R.mipmap.icn_vip_3);

                                break;
                            case 4:
                                iv_me_vip.setImageResource(R.mipmap.icn_vip_4);

                                break;
                        }
                    }

                    //当用户绑定了情侣、并官方认证时，不展示个性签名
                    if (myInfoBean.verifyType >= 1) {
                        sing_name_container.setVisibility(View.GONE);
                    }
                }

                if (!TextUtils.isEmpty(myInfoBean.intro)) {
                    txt_sign_name.setText(myInfoBean.intro);
                } else {
                    txt_sign_name.setText(getString(R.string.edit_intro));
                }

                if (myInfoBean.verifyType >= 1) {
                    img_verify1.setVisibility(View.VISIBLE);
                    txt_verif1.setVisibility(View.VISIBLE);

                    txt_verif1.setText(myInfoBean.verifyIntro);

                    if (myInfoBean.verifyType == 1) {
                        img_verify1.setImageResource(R.mipmap.icn_verify_person);
                    } else {
                        img_verify1.setImageResource(R.mipmap.icn_verify_enterprise);
                    }
                } else {
                    img_verify1.setVisibility(View.GONE);
                    txt_verif1.setVisibility(View.GONE);
                }

                if (!TextUtils.isEmpty(myInfoBean.nickName))
                    txt_nickname.setText(myInfoBean.nickName);

                if (!TextUtils.isEmpty(myInfoBean.bgImage)) {
                    mask.setVisibility(View.VISIBLE);
                }
                me_background.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(myInfoBean.bgImage, AppInit.displayMetrics.widthPixels, TheLApp.context.getResources().getDimension(R.dimen.bg_height))));

                if (!TextUtils.isEmpty(myInfoBean.winkNum)) {
                    winkNum.setText(myInfoBean.winkNum);
                } else {
                    winkNum.setText("0");
                }
                if (!TextUtils.isEmpty(myInfoBean.followNum)) {

                    followNum.setText(myInfoBean.followNum);

                    try {

                        int followUserCount = Integer.valueOf(myInfoBean.followNum);

                        if (followUserCount > 0) {

                            ShareFileUtils.getBoolean(ShareFileUtils.IS_FOLLOW_USER, true);

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    winkNum.setText("0");
                }

                //获取粉丝数
                final int fans = myInfoBean.fansNum;
                if (fans <= 0) {
                    me_num_fans.setText("0");
                } else {
                    me_num_fans.setText(fans + "");
                }

                boolean isNewUser = ShareFileUtils.getBoolean(ShareFileUtils.IS_NEW_USER, false);

                //4.5.0 喜欢数量
                if (!isNewUser) {
                    txt_match_unlock.setVisibility(View.GONE);

                } else {
                    txt_match_unlock.setVisibility(View.VISIBLE);

                }
                /**
                 * 已经去匹配页面看过了喜欢我的列表  取消新喜欢的提示
                 * */


                boolean clear_newlike = ShareFileUtils.getBoolean(ShareFileUtils.MATCH_SAW_LIKE_ME, false);
                if (clear_newlike) {
                    txt_match_tip.setBackground(null);
                    txt_match_tip.setTextColor(ContextCompat.getColor(TheLApp.context, R.color.text_color_gray));

                }
                /**
                 * 非会员用户显示新收到的xx喜欢
                 * */
                if (UserUtils.getUserVipLevel() <= 0) {
                    if (likeMeNum > 0) {
                        ShareFileUtils.setBoolean(ShareFileUtils.MATCH_SAW_LIKE_ME, false);
                        txt_match_tip.setVisibility(View.VISIBLE);
                        img_match_vip_new.setVisibility(View.GONE);
                        txt_match_tip.setBackgroundResource(R.drawable.bg_new_likecount_label_shape_pink);
                        txt_match_tip.setTextColor(ContextCompat.getColor(TheLApp.context, R.color.white));
                        if (likeMeNum > 1) {
                            txt_match_tip.setText(TheLApp.getContext().getString(R.string.new_like_me, likeMeNum));

                        } else {
                            txt_match_tip.setText(TheLApp.getContext().getString(R.string.new_like_me_odd, likeMeNum));
                        }
                    }

                } else {
/**
 * 会员用户红点提示
 * **/
                   /* txt_match_tip.setTextColor(ContextCompat.getColor(TheLApp.context, R.color.text_color_gray));
                    txt_match_tip.setBackground(null);
                    if (recentLikeNum > 0) {
                        txt_match_tip.setVisibility(View.VISIBLE);

                        img_match_vip_new.setVisibility(View.VISIBLE);
                        txt_match_tip.setText("+" + recentLikeNum);
                    }*/

                }

                //4.6.0添加我的等级
                if (myInfoBean.userLevel >= 0) {
                    txt_level.setText(getString(R.string.me_level) + " Lv" + myInfoBean.userLevel + " >");
                } else {
                    txt_level.setText(getString(R.string.me_level) + " Lv0 >");

                }
            }
           /* //附近的人
            if (myInfoBean.nearByUsers != null) {
                nearby_user_view.initData(myInfoBean.nearByUsers);
            }*/
            //有直播权限的时候显示我的钱包，反之不显示。
            if (ShareFileUtils.getInt(TheLConstants.live_permit, 0) == 1) {
                rel_wallet.setVisibility(View.VISIBLE);

            } else {
                rel_wallet.setVisibility(View.GONE);
            }

            // 检查是不是需要绑定账号
            if (TextUtils.isEmpty(ShareFileUtils.getString(ShareFileUtils.BIND_CELL, ""))) {
                setSettingsErrImgVisible(true);
            } else {
                setSettingsErrImgVisible(false);
            }
            //保存用户资料完成度
            if (myInfoBean != null) {
                ShareFileUtils.savaUserRatio(myInfoBean.ratio);
                saveDataToShare();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 当前时间和会员到期时间比较，如果小于或者等于五的话将返回 0 ，小于或者等于零，将返回-1，大于五返回1
     **/
    private int haveExpiredTime(String levelExpireTime) {
        String nowToDay = DateUtils.getNowToDay();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date dt1 = null;
        Date dt2 = null;
        try {
            dt1 = sdf.parse(nowToDay);
            dt2 = sdf.parse(levelExpireTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long betweenDate = (dt2.getTime() - dt1.getTime()) / (1000 * 60 * 60 * 24);

        L.i("MefragmentData", "开始时间 " + dt1);
        L.i("MefragmentData", "结束时间 " + dt2);
        L.i("MefragmentData", "相隔天数 " + betweenDate);

        if (betweenDate >= 1 && betweenDate <= 5) {
            return 0;
        } else if (betweenDate <= 0) {
            return -1;

        }

        return 1;

    }

    public void saveDataToShare() {
        if (myInfoBean != null) {
            ShareFileUtils.setString(ShareFileUtils.EMAIL, myInfoBean.email);
            ShareFileUtils.setString(ShareFileUtils.AVATAR, myInfoBean.avatar);
            ShareFileUtils.setString(ShareFileUtils.USER_THEL_ID, myInfoBean.userName);
            ShareFileUtils.setString(ShareFileUtils.USER_BACKGROUND, myInfoBean.bgImage);
            ShareFileUtils.setString(ShareFileUtils.USER_NAME, myInfoBean.nickName);
            ShareFileUtils.setInt(ShareFileUtils.FRIENDS_FANS_TOTAL, myInfoBean.fansNum);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            mPresenter.loadNetData();
        } else if (resultCode == TheLConstants.RESULT_CODE_TAKE_PHOTO || resultCode == TheLConstants.RESULT_CODE_SELECT_LOCAL_IMAGE) {
            String path = data.getStringExtra(TheLConstants.BUNDLE_KEY_IMAGE_OUTPUT_PATH);
            if (requestCode == TheLConstants.BUNDLE_CODE_SELECT_BG) {// 背景
                if (!TextUtils.isEmpty(path)) {
                    imageLocalPath = ImageUtils.handlePhoto1(path, TheLConstants.F_TAKE_PHOTO_ROOTPATH, ImageUtils.getPicName(), false, TheLConstants.MAX_PIC_WIDTH, TheLConstants.MAX_PIC_HEIGHT, TheLConstants.PIC_QUALITY, 3);
                    mPresenter.uploadImage(imageLocalPath);
                } else {
                    DialogUtil.showToastShort(getActivity(), TheLApp.getContext().getString(R.string.info_rechoise_photo));

                }
            }
        }
    }

    @Override
    public void onDestroy() {
        if (cropResultReceiver != null) {
            cropResultReceiver.unregister(getActivity());
        }
        super.onDestroy();
    }
}
