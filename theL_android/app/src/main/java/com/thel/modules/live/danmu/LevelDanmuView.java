package com.thel.modules.live.danmu;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.live.bean.DanmuBean;
import com.thel.ui.widget.LevelImageView;


public class LevelDanmuView implements CreateDanmu {


    @Override public View createDanmuView(ViewGroup viewGroup, DanmuBean danmuBean) {
        return initLevelDanmu(viewGroup, danmuBean);
    }

    private View initLevelDanmu(ViewGroup viewGroup, DanmuBean danmuBean) {

        int userLevel = danmuBean.userLevel;

        int layoutId;

        if (userLevel >= 16) {
            layoutId = R.layout.layout_low_thirty;
        } else {
            layoutId = R.layout.layout_low_fifteen;
        }

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(layoutId, viewGroup, true);

        ImageView user_avatar_iv = view.findViewById(R.id.user_avatar_iv);

        RelativeLayout avatar_bg_rl = view.findViewById(R.id.avatar_bg_rl);

        LevelImageView level_iv = view.findViewById(R.id.level_iv);

        TextView content_tv = view.findViewById(R.id.content_tv);

        RelativeLayout root_rl = view.findViewById(R.id.root_rl);

        ImageView anim_iv;

        ImageLoaderManager.imageLoaderCircle(user_avatar_iv, R.mipmap.icon_user, danmuBean.avatar);

        String content = danmuBean.nickName + ": " + danmuBean.content;

        content_tv.setText(content);

        if (danmuBean.userLevel < 0 || danmuBean.userLevel > 30) {
            level_iv.setVisibility(View.GONE);
        } else {
            level_iv.setVisibility(View.VISIBLE);
            level_iv.setLevel(danmuBean.userLevel);
        }

        if (danmuBean.userLevel == 30) {
            anim_iv = view.findViewById(R.id.anim_iv);
            anim_iv.setVisibility(View.VISIBLE);
            ImageLoaderManager.imageLoaderGifLoop(anim_iv, "thirty_lv_animation.gif");
        } else if (danmuBean.userLevel >= 26) {
            anim_iv = view.findViewById(R.id.anim_iv);
            anim_iv.setVisibility(View.VISIBLE);
            ImageLoaderManager.imageLoaderGifLoop(anim_iv, "twenty_six_animation.gif");
        }

        if (userLevel == 0) {
            root_rl.setBackgroundResource(R.drawable.shape_level_zero_bg);
            avatar_bg_rl.setBackgroundResource(R.drawable.shape_circle_user_zero_bg);
        } else if (userLevel <= 5) {
            root_rl.setBackgroundResource(R.drawable.shape_level_one_bg);
            avatar_bg_rl.setBackgroundResource(R.drawable.shape_circle_user_one_bg);
        } else if (userLevel <= 10) {
            root_rl.setBackgroundResource(R.drawable.shape_level_six_bg);
            avatar_bg_rl.setBackgroundResource(R.drawable.shape_circle_user_six_bg);
        } else if (userLevel <= 15) {
            root_rl.setBackgroundResource(R.drawable.shape_level_eleven_bg);
            avatar_bg_rl.setBackgroundResource(R.drawable.shape_circle_user_eleven_bg);
        } else if (userLevel <= 20) {
            root_rl.setBackgroundResource(R.drawable.icon_lv_sixteen_bg);
            avatar_bg_rl.setBackgroundResource(R.drawable.shape_circle_user_sixteen_bg);
        } else if (userLevel <= 25) {
            root_rl.setBackgroundResource(R.drawable.icon_lv_twenty_one_bg);
            avatar_bg_rl.setBackgroundResource(R.drawable.shape_circle_user_twenty_one_bg);
        } else if (userLevel <= 29) {
            root_rl.setBackgroundResource(R.drawable.icon_lv_twenty_six_bg);
            avatar_bg_rl.setBackgroundResource(R.drawable.shape_circle_user_twenty_six_bg);
        } else if (userLevel == 30) {
            root_rl.setBackgroundResource(R.drawable.icon_lv_thirty);
            avatar_bg_rl.setBackgroundResource(R.mipmap.ic_colorful_circle);
        } else {
            root_rl.setBackgroundResource(R.drawable.shape_level_zero_bg);
            avatar_bg_rl.setBackgroundResource(R.drawable.shape_circle_user_zero_bg);
        }

        return view;
    }

}
