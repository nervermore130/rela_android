package com.thel.modules.main.discover.view;

import android.content.Context;
import android.content.Intent;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.view.SimpleDraweeView;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.modules.live.bean.LiveRoomBean;
import com.thel.modules.live.surface.watch.LiveWatchActivity;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;
import com.thel.utils.AppInit;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by waiarl on 2017/6/2.
 */

public class LiveNewUserListView extends LinearLayout {
    private final Context mContext;
    private RecyclerView mRecyclerView;
    private ArrayList<LiveRoomBean> list = new ArrayList<>();
    private LiveNewUserAdapter adater;
    private LinearLayoutManager manager;


    public LiveNewUserListView(Context context) {
        this(context, null);
    }

    public LiveNewUserListView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LiveNewUserListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
        setListener();
    }

    private void init() {
        inflate(mContext, R.layout.live_new_user_list_view, this);
        mRecyclerView = findViewById(R.id.recyclerview);
        manager = new LinearLayoutManager(mContext);
        manager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mRecyclerView.setLayoutManager(manager);
        adater = new LiveNewUserAdapter(list);
        mRecyclerView.setAdapter(adater);
        setVisibility(View.GONE);
    }

    public LiveNewUserListView initView(List<LiveRoomBean> list) {
        if (list.size() > 0) {
            setVisibility(View.VISIBLE);
        } else {
            setVisibility(View.GONE);
        }
        this.list.clear();
        this.list.addAll(list);
        adater.notifyDataSetChanged();
        return this;
    }

    private void setListener() {
        adater.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                gotoLiveShowFragmentActivity(position);
            }
        });
    }

    private void gotoLiveShowFragmentActivity(int position) {
        MobclickAgent.onEvent(TheLApp.getContext(), "enter_live_room_from_list");
        Intent intent = new Intent(getContext(), LiveWatchActivity.class);
        intent.putExtra(LiveWatchActivity.FROM_PAGE, LiveWatchActivity.FROM_PAGE_LIVE_ROOM_LIST_FRAGMENT);
        intent.putExtra(TheLConstants.BUNDLE_KEY_LIVEROOMBEAN_LIST, list);
        intent.putExtra(TheLConstants.BUNDLE_KEY_POSITION, position);
        intent.putExtra(TheLConstants.BUNDLE_KEY_CURSOR, 0);
        intent.putExtra(LiveWatchActivity.FROM_PAGE, LiveWatchActivity.FROM_PAGE_NEW_USER);
        getContext().startActivity(intent);
    }

    private class LiveNewUserAdapter extends BaseRecyclerViewAdapter<LiveRoomBean> {

        private final float radius;
        private final double size;
        private final float padding;

        public LiveNewUserAdapter(List<LiveRoomBean> data) {
            super(R.layout.live_new_user_item_view, data);
            radius = TheLApp.getContext().getResources().getDimension(R.dimen.live_new_user_item_radius);
            padding = getResources().getDimension(R.dimen.live_new_user_item_padding);
            size = (AppInit.displayMetrics.widthPixels - 4 * padding) / 2.5;
        }

        @Override
        protected void convert(BaseViewHolder holdView, final LiveRoomBean liveRoomBean) {
            // 头像和昵称
            holdView.setImageViewUrl(R.id.avatar, R.mipmap.icon_user, liveRoomBean.user.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE);
            // 预览图
            holdView.setImageUrl(R.id.preview, liveRoomBean.imageUrl, TheLApp.getContext().getResources().getDimension(R.dimen.moment_pic_thumbnail_big), TheLApp.getContext().getResources().getDimension(R.dimen.moment_pic_thumbnail_big));
            final SimpleDraweeView preview = holdView.getView(R.id.preview);
            preview.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadii(radius, radius, 0, 0).setRoundingMethod(RoundingParams.RoundingMethod.OVERLAY_COLOR).setOverlayColor(ContextCompat.getColor(TheLApp.getContext(), R.color.bg_color)));
            final LayoutParams params = (LayoutParams) preview.getLayoutParams();
            params.width = (int) size;
            params.height = (int) size;

            holdView.setText(R.id.nickname, liveRoomBean.user.nickName);
            holdView.setOnClickListener(R.id.avatar, new OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Intent intent = new Intent();
//                    intent.setClass(TheLApp.getContext(), UserInfoActivity.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, liveRoomBean.user.id + "");
//                    TheLApp.getContext().startActivity(intent);
                    FlutterRouterConfig.Companion.gotoUserInfo(liveRoomBean.user.id+"");
                }
            });
            // 观看人数
            holdView.setText(R.id.audience, TheLApp.getContext().getString(R.string.now_watching, liveRoomBean.liveUsersCount));
            // 直播描述
            holdView.setText(R.id.text, liveRoomBean.text);
           /* // 前三标志
            holdView.setVisibility(R.id.badge, View.GONE);
            if (!TextUtils.isEmpty(liveRoomBean.badge)) {
                holdView.setVisibility(R.id.badge, View.VISIBLE);
                holdView.setImageUrl(R.id.badge, liveRoomBean.badge);
            }*/
            //2.20新增， 主播推荐标签
            if (TextUtils.isEmpty(liveRoomBean.label)) {
                holdView.setVisibility(R.id.txt_label, View.GONE);
                ((TextView) holdView.getView(R.id.text)).setMaxLines(3);
            } else {
                holdView.setVisibility(R.id.txt_label, View.VISIBLE);
                holdView.setText(R.id.txt_label, liveRoomBean.label + "");
                ((TextView) holdView.getView(R.id.text)).setMaxLines(1);
            }
        }
    }
}
