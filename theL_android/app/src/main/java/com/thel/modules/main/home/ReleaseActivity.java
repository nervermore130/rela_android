package com.thel.modules.main.home;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;

import android.text.TextUtils;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.RelativeLayout;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.bean.LivePermitBean;
import com.thel.bean.me.MyCircleActivity;
import com.thel.bean.me.MyCircleFriendListBean;
import com.thel.constants.TheLConstants;
import com.thel.modules.live.Certification.ZhimaCertificationActivity;
import com.thel.modules.main.home.moments.ReleaseLiveMomentActivity;
import com.thel.modules.main.home.moments.ReleaseMomentActivity;
import com.thel.modules.main.home.moments.ReleaseThemeMomentActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.ui.widget.ReleaseMomentViewGroup;
import com.thel.utils.DialogUtil;
import com.thel.utils.PermissionUtil;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.thel.utils.ShareFileUtils.IS_RELEASE_MOMENT;

public class ReleaseActivity extends BaseActivity {

    private String tag;

    @BindView(R.id.root_rl)
    RelativeLayout root_rl;

    @BindView(R.id.release_vp)
    ReleaseMomentViewGroup release_vp;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.O && isTranslucentOrFloating()) {
            fixOrientation();
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_fragment_release);
        ButterKnife.bind(this);

        release_vp.setOnFinishListener(mOnFinishListener);

        release_vp.setOnItemClickListener(mOnItemClickListener);

        root_rl.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override public void onGlobalLayout() {

                if (release_vp != null) {

                    release_vp.in();

                }

                root_rl.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });

    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    @OnClick(R.id.root_rl)
    void onCancel() {
        if (release_vp != null) {
            release_vp.out();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (release_vp != null) {
            release_vp.out();
        }
    }

    private ReleaseMomentViewGroup.OnFinishListener mOnFinishListener = new ReleaseMomentViewGroup.OnFinishListener() {
        @Override
        public void onFinish() {
            fadeOut();
        }

        @Override
        public void onShown() {

        }
    };

    private ReleaseMomentViewGroup.OnItemClickListener mOnItemClickListener = new ReleaseMomentViewGroup.OnItemClickListener() {
        @Override public void onItemClick(int tag, View view) {

            switch (tag) {
                case ReleaseMomentViewGroup.GOTO_ALBUM:
                    gotoAlbum(view);
                    break;
                case ReleaseMomentViewGroup.GOTO_VIDEO:
                    gotoVideo(view);
                    break;
                case ReleaseMomentViewGroup.GOTO_TEXT:
                    gotoText(view);
                    break;
                case ReleaseMomentViewGroup.GOTO_TOPIC:
                    gotoTopic(view);
                    break;
                case ReleaseMomentViewGroup.GOTO_LIVE:
                    gotoLive(view);
                    break;
                case ReleaseMomentViewGroup.GOTO_LOVER:
                    gotoLover(view);
                    break;
                default:
                    break;
            }

        }
    };

    void gotoAlbum(View v) {
        if (UserUtils.isVerifyCell()) {
            ShareFileUtils.setBoolean(Utils.getMyUserId() + "_" + IS_RELEASE_MOMENT, true);

            ViewUtils.preventViewMultipleClick(v, 2000);
            PermissionUtil.requestStoragePermission(ReleaseActivity.this, new PermissionUtil.PermissionCallback() {
                @Override
                public void granted() {
                    Intent intent = new Intent(TheLApp.getContext(), ReleaseMomentActivity.class);
                    intent.putExtra(TheLConstants.BUNDLE_KEY_RELEASE_TYPE, ReleaseMomentActivity.RELEASE_TYPE_PHOTOS);
                    if (!TextUtils.isEmpty(tag)) {
                        intent.putExtra(TheLConstants.BUNDLE_KEY_TOPIC_NAME, tag);
                    }
                    startActivity(intent);
                    finish();
                }

                @Override
                public void denied() {
                    finish();
                }

                @Override
                public void gotoSetting() {
                    finish();
                }
            });
        } else {
            fadeOut();
        }
    }

    void gotoVideo(final View v) {
        if (UserUtils.isVerifyCell()) {
            ShareFileUtils.setBoolean(Utils.getMyUserId() + "_" + IS_RELEASE_MOMENT, true);

            ViewUtils.preventViewMultipleClick(v, 2000);
            Intent intent = new Intent(TheLApp.getContext(), ReleaseMomentActivity.class);
            if (!TextUtils.isEmpty(tag)) {
                intent.putExtra(TheLConstants.BUNDLE_KEY_TOPIC_NAME, tag);
            }
            intent.putExtra(TheLConstants.BUNDLE_KEY_RELEASE_TYPE, ReleaseMomentActivity.RELEASE_TYPE_VIDEO);
            startActivity(intent);
            finish();
        } else {
            fadeOut();
        }
    }

    void gotoText(View v) {
        if (UserUtils.isVerifyCell()) {
            ShareFileUtils.setBoolean(Utils.getMyUserId() + "_" + IS_RELEASE_MOMENT, true);

            ViewUtils.preventViewMultipleClick(v, 2000);
            Intent intent = new Intent(TheLApp.getContext(), ReleaseMomentActivity.class);
            if (!TextUtils.isEmpty(tag)) {
                intent.putExtra(TheLConstants.BUNDLE_KEY_TOPIC_NAME, tag);
            }
            intent.putExtra(TheLConstants.BUNDLE_KEY_RELEASE_TYPE, ReleaseMomentActivity.RELEASE_TYPE_TEXT);
            startActivity(intent);
            finish();
        } else {
            fadeOut();
        }
    }

    void gotoTopic(View v) {
        if (UserUtils.isVerifyCell()) {
            ShareFileUtils.setBoolean(Utils.getMyUserId() + "_" + IS_RELEASE_MOMENT, true);

            ViewUtils.preventViewMultipleClick(v, 2000);
            Intent intent = new Intent(TheLApp.getContext(), ReleaseThemeMomentActivity.class);
            if (!TextUtils.isEmpty(tag)) {
                intent.putExtra(TheLConstants.BUNDLE_KEY_TOPIC_NAME, tag);
            }
            intent.putExtra(TheLConstants.BUNDLE_KEY_RELEASE_TYPE, ReleaseThemeMomentActivity.RELEASE_TYPE_TOPIC);
            startActivity(intent);
            finish();
        } else {
            fadeOut();
        }
    }

    void gotoLive(final View v) {
        if (UserUtils.isVerifyCell()) {
            ShareFileUtils.setBoolean(Utils.getMyUserId() + "_" + IS_RELEASE_MOMENT, true);

            ViewUtils.preventViewMultipleClick(v, 2000);

            if (ShareFileUtils.getInt(TheLConstants.live_permit, 0) == 1) {
                PermissionUtil.requestCameraPermission(ReleaseActivity.this, new PermissionUtil.PermissionCallback() {
                    @Override
                    public void granted() {
                        Intent intent = new Intent(TheLApp.getContext(), ReleaseLiveMomentActivity.class);
                        if (!TextUtils.isEmpty(tag)) {
                            intent.putExtra(TheLConstants.BUNDLE_KEY_TOPIC_NAME, tag);
                        }
                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void denied() {

                    }

                    @Override
                    public void gotoSetting() {

                    }
                });
            } else {
                RequestBusiness.getInstance().getLivePermit().onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<LivePermitBean>() {
                    @Override
                    public void onNext(LivePermitBean livePermitBean) {
                        super.onNext(livePermitBean);
                        if (livePermitBean != null && livePermitBean.data != null) {
                            ShareFileUtils.setInt(TheLConstants.live_permit, livePermitBean.data.perm);
                            if (livePermitBean.data.perm == 1) {
                                Intent intent = new Intent(TheLApp.getContext(), ReleaseLiveMomentActivity.class);
                                if (!TextUtils.isEmpty(tag)) {
                                    intent.putExtra(TheLConstants.BUNDLE_KEY_TOPIC_NAME, tag);
                                }
                                startActivity(intent);
                                finish();
                            } else {
                                DialogUtil.showConfirmDialog(ReleaseActivity.this, "", TheLApp.getContext().getString(R.string.no_live_permition_tip), TheLApp.getContext().getString(R.string.info_continue), TheLApp.getContext().getString(R.string.info_no), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        Intent intent = new Intent(TheLApp.getContext(), ZhimaCertificationActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                });
                            }
                        }
                    }
                });
            }
        } else {
            fadeOut();
        }
    }

    void gotoLover(View v) {
        if (UserUtils.isVerifyCell()) {
            ShareFileUtils.setBoolean(Utils.getMyUserId() + "_" + IS_RELEASE_MOMENT, true);

            ViewUtils.preventViewMultipleClick(v, 2000);

            showLoading();
            RequestBusiness.getInstance().getMyCircleData().onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<MyCircleFriendListBean>() {
                @Override
                public void onNext(MyCircleFriendListBean myCircelBean) {
                    super.onNext(myCircelBean);
                    closeLoading();
                    if (myCircelBean != null && myCircelBean.data != null && myCircelBean.data.paternerd != null && 1 == myCircelBean.data.paternerd.requestStatus) {//有情侣
                        Intent intent = new Intent(TheLApp.getContext(), ReleaseMomentActivity.class);
                        if (!TextUtils.isEmpty(tag)) {
                            intent.putExtra(TheLConstants.BUNDLE_KEY_TOPIC_NAME, tag);
                        }
                        intent.putExtra(TheLConstants.BUNDLE_KEY_RELEASE_TYPE, ReleaseMomentActivity.RELEASE_TYPE_LOVE);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("partner", myCircelBean.data.paternerd);
                        intent.putExtras(bundle);
                        startActivity(intent);
                        finish();
                    } else {
                        DialogUtil.showConfirmDialog(ReleaseActivity.this, "", TheLApp.getContext().getString(R.string.have_no_lover), TheLApp.getContext().getString(R.string.bind_lover), TheLApp.getContext().getString(R.string.info_no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(TheLApp.getContext(), MyCircleActivity.class);
                                startActivity(intent);
                                dialog.cancel();
                                finish();
                            }
                        });
                    }
                }
            });
        } else {
            fadeOut();
        }
    }

    private void fadeOut() {
        root_rl.animate().alpha(0).setDuration(300).setListener(new AnimatorListenerAdapter() {
            @Override public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                root_rl.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            }

            @Override public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                root_rl.setLayerType(View.LAYER_TYPE_NONE, null);
                root_rl.setVisibility(View.GONE);
                ReleaseActivity.this.finish();
            }
        });
    }

    private boolean isTranslucentOrFloating() {
        boolean isTranslucentOrFloating = false;
        try {
            int[] styleableRes = (int[]) Class.forName("com.android.internal.R$styleable").getField("Window").get(null);
            final TypedArray ta = obtainStyledAttributes(styleableRes);
            Method m = ActivityInfo.class.getMethod("isTranslucentOrFloating", TypedArray.class);
            m.setAccessible(true);
            isTranslucentOrFloating = (boolean) m.invoke(null, ta);
            m.setAccessible(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isTranslucentOrFloating;
    }

    private boolean fixOrientation() {
        try {
            Field field = Activity.class.getDeclaredField("mActivityInfo");
            field.setAccessible(true);
            ActivityInfo o = (ActivityInfo) field.get(this);
            o.screenOrientation = -1;
            field.setAccessible(false);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

}
