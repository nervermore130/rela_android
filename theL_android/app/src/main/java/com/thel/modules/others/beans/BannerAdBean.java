package com.thel.modules.others.beans;

import com.thel.utils.JsonUtils;

import org.json.JSONObject;

public class BannerAdBean {

    // 点击广告跳转的链接地址
    public String dumpURL;
    // 广告显示位置（list 表示附近列表的横幅广告，friend表示朋友圈的广告(目前安卓版没用，iOS版用)，main表示开机封面广告）
    public String advertLocation;
    // 广告倒计时时间
    public String time;
    // 广告标题
    public String advertTitle;
    // 广告图片地址（七牛图片地址，根据不同的分辨率参数获取不同尺寸的图片）
    public String advertURL;
    // 广告类型（0表示系统维护广告，1表示普通广告）
    public String type;
    // 跳转类型
    public String dumpType;

    public static final String ADV_DUMP_TYPE_NO = "0";
    public static final String ADV_DUMP_TYPE_WEB = "1";
    public static final String ADV_DUMP_TYPE_LOCAL = "2";
    public static final String ADV_DUMP_TYPE_TAG = "5";
    public static final String ADV_DUMP_TYPE_STICKER = "6";
    public static final String ADV_DUMP_TYPE_STICKER_STORE = "7";
    public static final String ADV_DUMP_TYPE_MOMENT = "8";
    public static final String ADV_DUMP_TYPE_TOPIC = "9";
    public static final String ADV_DUMP_TYPE_LIVE_ROOMS = "10";
    public static final String ADV_DUMP_TYPE_LIVE_ROOM = "11";

    public static final String ADV_LOCATION_COVER = "main";
    public static final String ADV_LOCATION_NEARBY = "list";
    public static final String ADV_LOCATION_MOMENTS = "friend";
    public static final String ADV_LOCATION_STICKER_STORE = "sticker";
    public static final String ADV_LOCATION_NEW_STICKER = "sticker_icon";
    public static final String ADV_LOCATION_WEEKLY = "weekly";
    public static final String ADV_LOCATION_VIP = "vip";
    public static final String ADV_LOCATION_FIND = "find";
    public static final String ADV_LOCATION_LIVE_ROOMS = "live";

    public void fromJson(JSONObject tempobj) {
        dumpURL = JsonUtils.getString(tempobj, "dumpURL", "");
        advertLocation = JsonUtils.getString(tempobj, "advertLocation", "");
        time = JsonUtils.getString(tempobj, "time", "");
        advertTitle = JsonUtils.getString(tempobj, "advertTitle", "");
        advertURL = JsonUtils.getString(tempobj, "advertURL", "");
        type = JsonUtils.getString(tempobj, "type", "");
        dumpType = JsonUtils.getString(tempobj, "dumpType", "");
    }
}