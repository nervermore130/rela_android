package com.thel.modules.main.home.moments;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.postprocessors.IterativeBoxBlurPostProcessor;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UploadManager;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseFragment;
import com.thel.bean.user.UploadTokenBean;
import com.thel.constants.BundleConstants;
import com.thel.constants.TheLConstants;
import com.thel.modules.live.bean.LiveRoomBean;
import com.thel.modules.live.bean.LiveRoomNetBean;
import com.thel.modules.live.surface.show.LiveShowActivity;
import com.thel.modules.live.view.LiveNoticeEditDialog;
import com.thel.modules.main.home.moments.web.WebViewActivity;
import com.thel.modules.preview_image.UserInfoPhotoActivity;
import com.thel.modules.select_image.SelectLocalImagesActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.RequestConstants;
import com.thel.utils.AppInit;
import com.thel.utils.DialogUtil;
import com.thel.utils.ImageUtils;
import com.thel.utils.MD5Utils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.StringUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.ViewUtils;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * 发布声音直播的日志fragment
 * Created by lingwei on 2018/5/4.
 */

public class EditVoiceLiveMomentFragment extends BaseFragment implements View.OnClickListener {
    private TextView txt_live_agreement;
    // 选择的图片地址
    private String bgPicUrl;
    private TextView edit_title;
    private SimpleDraweeView add_live;
    private int isMulti = LiveRoomBean.SINGLE_LIVE;
    // 上传头像图片到七牛后的文件路径
    private String uploadPath = "";
    private String uploadUrl = "";
    private LiveRoomBean liveRoomBean;

    private SimpleDraweeView avatar_bg;
    private TextView start_live;
    private TextView txt_livegift_hint;
    private TextView notice_board;
    private String noticeBroad = "";

    public static EditVoiceLiveMomentFragment getInstance(int type) {
        EditVoiceLiveMomentFragment editVoiceLiveMomentFragment = new EditVoiceLiveMomentFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("type", type);
        editVoiceLiveMomentFragment.setArguments(bundle);
        return editVoiceLiveMomentFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            isMulti = getArguments().getInt("type");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.release_moment_voice_live, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        findviewById(view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void findviewById(View view) {
        txt_live_agreement = view.findViewById(R.id.txt_live_agreement);
        edit_title = view.findViewById(R.id.write_moment_add_text);
        add_live = view.findViewById(R.id.add_live);
        avatar_bg = view.findViewById(R.id.avatar_bg);
        start_live = view.findViewById(R.id.start_live);
        txt_livegift_hint = view.findViewById(R.id.txt_livegift_hint);
        notice_board = view.findViewById(R.id.notice_board);
        notice_board.setOnClickListener(this);
        start_live.setOnClickListener(this);

        setLiveType();
    }

    private void initView() {
        if (getActivity() != null && isAdded()) {

            initViewData();
        }
    }

    private void initViewData() {
        String text = StringUtils.getString(R.string.live_agreement);
        txt_live_agreement.setText(Html.fromHtml(text));
        txt_live_agreement.setOnClickListener(this);
        add_live.setOnClickListener(this);
        avatar_bg.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(UserUtils.getUserAvatar(), AppInit.displayMetrics.widthPixels, AppInit.displayMetrics.heightPixels))).setPostprocessor(new IterativeBoxBlurPostProcessor(10, 10)).build()).setAutoPlayAnimations(true).build());

        String title = ShareFileUtils.getString(ShareFileUtils.RELEASED_LIVE_TITLE, null);

        String image = ShareFileUtils.getString(ShareFileUtils.RELEASED_LIVE_IMAGE, null);

        if (title != null && image != null) {

            edit_title.setText(title);

            uploadUrl = image;
            add_live.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadius(12));
            add_live.setImageURI(uploadUrl);

            start_live.setAlpha(1.0f);
        }
    }

    /**
     * 0single /1 multi
     */
    private void setLiveType() {
        if (isMulti == 1) {
            txt_livegift_hint.setVisibility(View.VISIBLE);
        } else if (isMulti == 0) {
            txt_livegift_hint.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.start_live:
                ViewUtils.preventViewMultipleClick(v, 2000);
                releaseLive();
                break;
            case R.id.notice_board:
                LiveNoticeEditDialog liveNoticeDialog = LiveNoticeEditDialog.newInstence(noticeBroad);
                liveNoticeDialog.setEditNoticeListener(new LiveNoticeEditDialog.EditNoticeListener() {
                    @Override
                    public void notice(String notice) {
                        noticeBroad = notice;
                        if (!TextUtils.isEmpty(notice)) {
                            notice_board.setText(R.string.notice_boarded);
                            notice_board.setTextColor(Color.WHITE);
                        } else {
                            notice_board.setTextColor(getResources().getColor(R.color.rela_color));
                            notice_board.setText(R.string.notice_board);
                        }
                    }
                });
                liveNoticeDialog.show(getChildFragmentManager(), LiveNoticeEditDialog.class.getName());
                break;
            case R.id.txt_live_agreement:
                Intent intentLive = new Intent(getActivity(), WebViewActivity.class);
                intentLive.putExtra(BundleConstants.URL, TheLConstants.LIVE_AGREEMENT_PAGE_URL);
                intentLive.putExtra(BundleConstants.NEED_SECURITY_CHECK, false);

                startActivity(intentLive);
                break;
            case R.id.add_live:
                if (!TextUtils.isEmpty(bgPicUrl)) {
                    Intent i = new Intent(getActivity(), UserInfoPhotoActivity.class);
                    i.putExtra("fromPage", ReleaseMomentActivity.class.getName());
                    ArrayList<String> photoUrls = new ArrayList<String>();
                    photoUrls.add(bgPicUrl);
                    i.putExtra("position", 0);
                    i.putStringArrayListExtra(TheLConstants.BUNDLE_KEY_PHOTOS, photoUrls);
                    startActivityForResult(i, TheLConstants.BUNDLE_CODE_WRITE_MOMENT_ACTIVITY);
                } else {
                    startActivityForResult(new Intent(getActivity(), SelectLocalImagesActivity.class), TheLConstants.BUNDLE_CODE_SELECT_PHOTO);
                }
                break;
        }
    }

    public void releaseLive() {
        // 如果图片已上传成功，则直接发布
        if (!TextUtils.isEmpty(uploadUrl)) {
            startLive();
        } else {
            if (!TextUtils.isEmpty(bgPicUrl)) {
                uploadPath = RequestConstants.UPLOAD_FILE_ROOT_PATH_TIMELINE + ShareFileUtils.getString(ShareFileUtils.ID, "") + "/" + MD5Utils.calculateMD5(new File(bgPicUrl)) + ".jpg";
                showLoading();

                if (!TextUtils.isEmpty(uploadPath)) {
                    RequestBusiness.getInstance().getUploadToken(System.currentTimeMillis() + "", "", uploadPath).onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<UploadTokenBean>() {
                        @Override
                        public void onNext(UploadTokenBean uploadTokenBean) {
                            super.onNext(uploadTokenBean);
                            closeLoading();
                            if (!TextUtils.isEmpty(uploadTokenBean.data.uploadToken)) {
                                UploadManager uploadManager = new UploadManager();
                                uploadManager.put(new File(bgPicUrl), uploadPath, uploadTokenBean.data.uploadToken, new UpCompletionHandler() {
                                    @Override
                                    public void complete(String key, ResponseInfo info, JSONObject response) {
                                        if (info != null && info.statusCode == 200 && uploadPath.equals(key)) {
                                            uploadUrl = RequestConstants.FILE_BUCKET + key;
                                            startLive();
                                        } else {
                                            requestFailed();
                                        }
                                    }
                                }, null);
                            } else {
                                requestFailed();
                            }
                        }
                    });
                }
            } else {
                DialogUtil.showToastShort(getActivity(), TheLApp.context.getString(R.string.uploadimage_activity_dialog_title));
            }
        }
    }

    private void requestFailed() {
        closeLoading();
        DialogUtil.showToastShort(getActivity(), StringUtils.getString(R.string.message_network_error));
    }

    private void startLive() {
        StringBuilder atUsers = new StringBuilder();

        String content;

        if (edit_title != null) {
            content = edit_title.getText().toString().trim();
        } else {
            content = "";
        }

        showLoading();
        RequestBusiness.getInstance().createVoiveLiveRoom(content, uploadUrl, atUsers.toString(), LiveRoomBean.TYPE_VOICE, isMulti, noticeBroad).onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<LiveRoomNetBean>() {
            @Override
            public void onNext(LiveRoomNetBean roomBean) {
                super.onNext(roomBean);
                closeLoading();
                if (roomBean == null)
                    return;
                if (roomBean.data == null) {
                    DialogUtil.showToastShort(getActivity(), roomBean.errdesc);
                    return;
                }
                liveRoomBean = roomBean.data;
                gotoLiveActivity();

                ShareFileUtils.setString(ShareFileUtils.RELEASED_LIVE_TITLE, content);

                ShareFileUtils.setString(ShareFileUtils.RELEASED_LIVE_IMAGE, uploadUrl);
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                closeLoading();
            }
        });
    }

    private void gotoLiveActivity() {
        if (liveRoomBean != null && getActivity() != null) {
            Intent intent = new Intent(getActivity(), LiveShowActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable(TheLConstants.BUNDLE_KEY_LIVE_ROOM, liveRoomBean);
            intent.putExtras(bundle);
            startActivity(intent);
            getActivity().finish();

        }
    }

    public void close() {
        if (!TextUtils.isEmpty(uploadUrl)) {
            DialogUtil.showConfirmDialog(getActivity(), null, getString(R.string.moments_write_moment_discard_tip), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    Activity activity = getActivity();
                    if (activity != null && !activity.isFinishing()) {
                        activity.finish();
                    }
                }
            });
        } else {
            if (isAdded()) {
                getActivity().finish();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == TheLConstants.RESULT_CODE_TAKE_PHOTO) {// 拍摄照片
            String takePhotoPath = data.getStringExtra(TheLConstants.BUNDLE_KEY_IMAGE_OUTPUT_PATH);
            photoSelected(takePhotoPath);
        } else if (resultCode == TheLConstants.RESULT_CODE_WRITE_MOMENT_DELETE_PICTURE) {// 删除图片
            if (data != null) {
                if (data.getIntegerArrayListExtra(TheLConstants.BUNDLE_KEY_INDEX) == null || data.getIntegerArrayListExtra(TheLConstants.BUNDLE_KEY_INDEX).size() == 0) {
                    add_live.getHierarchy().setPlaceholderImage(R.color.transparent);
                    add_live.setController(null);
                    bgPicUrl = "";
                    uploadPath = "";
                    uploadUrl = "";
                    start_live.setAlpha(0.4f);
                }
            }
        } else if (resultCode == TheLConstants.RESULT_CODE_SELECT_LOCAL_IMAGE) {// 选择了图片
            String photoPath = data.getStringExtra(TheLConstants.BUNDLE_KEY_LOCAL_IMAGE_PATH);
            photoSelected(photoPath);
        }
    }

    /**
     * 发布直播 选好图片后刷新界面
     *
     * @param path
     */
    private void photoSelected(String path) {
        uploadUrl = "";
        if (TextUtils.isEmpty(path)) {
            return;
        }
        bgPicUrl = ImageUtils.handlePhoto1(path, TheLConstants.F_TAKE_PHOTO_ROOTPATH, ImageUtils.getPicName(), false, TheLConstants.MAX_PIC_WIDTH, TheLConstants.MAX_PIC_HEIGHT, TheLConstants.PIC_QUALITY, 3);
        if (!TextUtils.isEmpty(bgPicUrl)) {
            add_live.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadius(12));
            add_live.setImageURI(Uri.parse(TheLConstants.FILE_PIC_URL + bgPicUrl));
            start_live.setAlpha(1.0f);
        }
    }

}
