package com.thel.modules.live.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by the L on 2016/5/25.
 * 打赏时请求对象
 * 包括打赏的礼物对象集合 SoftGiftBean
 */
public class SoftEnjoyBean implements Serializable {
    /**
     * 当前余额
     */
    public long gold;

    public String md5;

    /**
     * 礼物列表
     */
    public List<SoftGiftBean> list = new ArrayList<>();

    /**
     * AR礼物列表
     */
    public List<SoftGiftBean> arlist = new ArrayList<>();

    @Override
    public String toString() {
        return "SoftEnjoyBean{" +
                "gold=" + gold +
                ", list=" + list +
                ", arlist=" + arlist +
                '}';
    }
}
