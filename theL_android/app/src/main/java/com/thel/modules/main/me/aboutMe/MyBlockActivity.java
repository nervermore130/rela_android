package com.thel.modules.main.me.aboutMe;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.base.BaseDataBean;
import com.thel.bean.BlackListBean;
import com.thel.bean.BlackListNetBean;
import com.thel.bean.me.MyBlockBean;
import com.thel.bean.user.BlockBean;
import com.thel.bean.user.BlockListBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.flutter.bridge.RfSBridgeHandlerFactory;
import com.thel.manager.ChatServiceManager;
import com.thel.modules.main.me.adapter.MyBlockAdapter;
import com.thel.modules.main.messages.bean.MsgBean;
import com.thel.modules.main.messages.utils.MsgUtils;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.ui.widget.recyclerview.decoration.DefaultItemDivider;
import com.thel.imp.black.BlackUtils;
import com.thel.utils.DialogUtil;
import com.thel.utils.GsonUtils;
import com.thel.utils.SharedPrefUtils;
import com.thel.utils.ViewUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * 我的屏蔽页
 * Created by lingwei on 2017/11/2.
 */

public class MyBlockActivity extends BaseActivity {
    private ArrayList<BlockBean> listPlus = new ArrayList<BlockBean>();

    @BindView(R.id.txt_title)
    TextView txtTitle;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.lin_back)
    LinearLayout linBack;
    @BindView(R.id.rel_title)
    RelativeLayout relTitle;
    @BindView(R.id.block_recyclerview)
    RecyclerView mRecyclerview;
    @BindView(R.id.default_tip)
    TextView defaultTip;
    @BindView(R.id.bg_myblock_default)
    LinearLayout bg_myblock_default;
    private MyBlockAdapter myBlockAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.myblock_activity);
        ButterKnife.bind(this);
        initAdapter();
        processBusiness();
        setLinsener();
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void setLinsener() {
        linBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                MyBlockActivity.this.finish();
            }
        });

        myBlockAdapter.setRemoveFromBlackListener(new MyBlockAdapter.BlackUserLitener() {
            @Override
            public void removeFromBlackList(int pos, final BlockBean bean) {
                Flowable<MyBlockBean> flowable = RequestBusiness.getInstance().removeFromBlackList(bean.userId + "");
                flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<MyBlockBean>() {
                    @Override
                    public void onNext(MyBlockBean myBlockBean) {
                        super.onNext(myBlockBean);

                        if (myBlockBean != null && myBlockBean.data != null && myBlockBean.data.blacklist != null) {

                            String[] userIds = myBlockBean.data.blacklist.split(",");

                            List<Long> idList = new ArrayList<>();

                            for (String id : userIds) {
                                if (!TextUtils.isEmpty(id)) {
                                    long userId = Long.valueOf(id);
                                    idList.add(userId);
                                }
                            }

                            SharedPrefUtils.setString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.BLACK_LIST, GsonUtils.createJsonString(idList));

                            BlackListBean blackListBean = new BlackListBean();

                            blackListBean.blackList = idList;

                            RfSBridgeHandlerFactory.getInstance().getFlutterPushImpl().pushBlackListInfo(blackListBean);


                        }

                        removeBlockData(bean);
                    }

                    @Override
                    public void onError(Throwable t) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
            }

            @Override
            public void goToUserInfo(int pos, BlockBean bean) {
//                Intent intent = new Intent(MyBlockActivity.this, UserInfoActivity.class);
//                intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, bean.userId+"");
//                startActivity(intent);
                FlutterRouterConfig.Companion.gotoUserInfo(bean.userId + "");
            }
        });
    }

    private void removeBlockData(BlockBean blockBean) {
        if (blockBean != null) {
            listPlus.remove(blockBean);
            //从缓存中删除这条数据
            BlackUtils.removeOneBlackUser(blockBean.userId + "");
            // 发送一条取消屏蔽消息
            ChatServiceManager.getInstance().sendMsg(MsgUtils.createMsgBean(MsgBean.MSG_TYPE_REMOVE_FROM_BLACK_LIST, "", 1, blockBean.userId + "", blockBean.userName, blockBean.avatar));
            bg_myblock_default.setVisibility(listPlus.size() == 0 ? View.VISIBLE : View.GONE);
            myBlockAdapter.setNewData(listPlus);
            myBlockAdapter.notifyDataSetChanged();
            DialogUtil.showToastShort(this, getString(R.string.myblock_activity_cancel_success));
        }
    }

    private void initAdapter() {
        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerview.setLayoutManager(manager);
        mRecyclerview.addItemDecoration(new DefaultItemDivider(this, LinearLayoutManager.VERTICAL, R.color.gray, 1, true, true));
        mRecyclerview.setHasFixedSize(true);
        myBlockAdapter = new MyBlockAdapter(listPlus);
        mRecyclerview.setAdapter(myBlockAdapter);

    }

    private void processBusiness() {
        Flowable<BlockListBean> flowable = RequestBusiness.getInstance().getUserBlackList("1000", "1");// 1000只是一个大值，为了拿到用户的所有黑名单同时不修改后台接口
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BlockListBean>() {
            @Override
            public void onNext(BlockListBean blockListBean) {
                super.onNext(blockListBean);
                getUserBlackList(blockListBean);

            }


            @Override
            public void onError(Throwable t) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void getUserBlackList(BlockListBean blockListBean) {
        //获取到用户名黑名单后保存到数据库中
        if (blockListBean.data == null)
            return;
        //将黑名单存储在sp中
        if (blockListBean.data.blackListUsers != null && blockListBean.data.blackListUsers.size() > 0) {
            for (int i = 0; i < blockListBean.data.blackListUsers.size(); i++) {
                BlackUtils.saveOneBlackUser(blockListBean.data.blackListUsers.get(i).userId + "");
            }
        }
        listPlus.clear();
        listPlus.addAll(blockListBean.data.blackListUsers);
        bg_myblock_default.setVisibility(listPlus.size() == 0 ? View.VISIBLE : View.GONE);
        myBlockAdapter.setNewData(listPlus);
        myBlockAdapter.notifyDataSetChanged();
    }


}
