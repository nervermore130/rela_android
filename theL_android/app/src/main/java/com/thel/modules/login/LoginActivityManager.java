package com.thel.modules.login;

import android.app.Activity;

import java.util.Stack;

/**
 * Created by chad
 * Time 17/12/25
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */

public class LoginActivityManager {

    private static LoginActivityManager appManager;

    private Stack<Activity> mActivityMap;

    /**
     * (单一实例)
     */
    public static LoginActivityManager getInstanse() {
        if (appManager == null) {
            synchronized (LoginActivityManager.class) {
                if (appManager == null) {
                    appManager = new LoginActivityManager();
                }
            }
        }
        return appManager;
    }

    /**
     * @param activity (添加Activity到堆栈)
     */
    public void addActivity(Activity activity) {
        if (mActivityMap == null) {
            mActivityMap = new Stack<>();
        }
        if (activity != null) {
            mActivityMap.add(activity);
        }

//        LogUtils.i("MyActivityManager", "actvitynumber:" + mActivityMap.size());
    }

    /**
     * (结束所有Activity)
     */
    public void finishAllActivity() {
        if (mActivityMap != null) {
            for (int i = 0, size = mActivityMap.size(); i < size; i++) {
                if (null != mActivityMap.get(i)) {
                    mActivityMap.get(i).finish();
                }
            }
            mActivityMap.clear();
        }
    }
}
