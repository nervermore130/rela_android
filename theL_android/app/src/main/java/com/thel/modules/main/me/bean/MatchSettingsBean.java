package com.thel.modules.main.me.bean;

import com.thel.base.BaseDataBean;

public class MatchSettingsBean extends BaseDataBean {
    //{"wantAffection":0,"roleName":"4","wantRole":"1,2,3,5"}
    public MatchSettingsDataBean data;

    public static class MatchSettingsDataBean {
        public int wantAffection;   //她的感情状态  0不限1单身 默认是0
        public String roleName;
        public String wantRole;
        public int hideRole;//1隐藏

        @Override
        public String toString() {
            return "MatchSettingsDataBean{" +
                    "wantAffection=" + wantAffection +
                    ", roleName='" + roleName + '\'' +
                    ", wantRole='" + wantRole + '\'' +
                    '}';
        }
    }

}
