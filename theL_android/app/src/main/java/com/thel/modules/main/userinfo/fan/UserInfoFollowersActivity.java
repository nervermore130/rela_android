package com.thel.modules.main.userinfo.fan;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.bean.FollowerBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.modules.main.userinfo.UserInfoTopMainView;
import com.thel.modules.main.userinfo.bean.FollowersListBean;
import com.thel.modules.main.userinfo.bean.FollowersListNetBean;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.service.DefaultRequestService;
import com.thel.ui.decoration.DefaultItemDivider;
import com.thel.ui.widget.DefaultEmptyView;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by waiarl on 2017/11/6.
 */

public class UserInfoFollowersActivity extends BaseActivity {
    private LinearLayout lin_back;
    private TextView txt_title;
    private SwipeRefreshLayout swipe_container;
    private RecyclerView recyclerView;
    private LinearLayout bg_fans_default;
    private TextView tip;
    private String userId;
    private UserInfoFollowersAdapter adapter;
    private LinearLayoutManager manager;
    private List<FollowerBean> list = new ArrayList<>();

    public final int REFRESH_TYPE_ALL = 1;
    public final int REFRESH_TYPE_NEXT_PAGE = 2;
    private int curPage = 1;
    private int pageSize = 20;
    private boolean haveNextPage = false;
    private static long lastClickTime = 0;
    private DefaultEmptyView empty_view;
    public static final String TYPE_FOLLOW = "follow";
    public static final String TYPE_FANS = "fans";
    private String pageType;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userinfo_followers);
        getIntentData();
        findViewById();
        setListener();
        getData();
        getRefreshData();
    }

    private void getData() {
        swipe_container.post(new Runnable() {
            @Override
            public void run() {
                swipe_container.setRefreshing(true);
            }
        });
    }

    private void getRefreshData() {
        curPage = 1;
        getData(REFRESH_TYPE_ALL, curPage);
    }

    private void getIntentData() {
        final Intent intent = getIntent();
        userId = intent.getStringExtra(TheLConstants.BUNDLE_KEY_USER_ID);
        pageType = intent.getStringExtra("pageType");
    }

    private void findViewById() {
        lin_back = this.findViewById(R.id.lin_back);
        txt_title = this.findViewById(R.id.txt_title);
        swipe_container = this.findViewById(R.id.swipe_container);
        ViewUtils.initSwipeRefreshLayout(swipe_container);
        recyclerView = this.findViewById(R.id.listView);
        empty_view = findViewById(R.id.empty_view);
        findViewById(R.id.lin_more).setVisibility(View.GONE);
        initAdapter();
    }

    private void initAdapter() {
        adapter = new UserInfoFollowersAdapter(list);
        manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new DefaultItemDivider(this, LinearLayoutManager.VERTICAL, R.color.gray, 1, true, true, true));

    }

    private void setListener() {
        lin_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                finish();
            }
        });
        adapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                final FollowerBean bean = adapter.getItem(position);
                gotoUserInfoActivity(bean);
            }
        });
        txt_title.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (MotionEvent.ACTION_DOWN == event.getAction()) {
                    if (System.currentTimeMillis() - lastClickTime < 300) {
                        ViewUtils.preventViewMultipleTouch(v, 2000);
                        if (recyclerView != null) {
                            recyclerView.scrollToPosition(0);
                        }
                    }
                    lastClickTime = System.currentTimeMillis();
                }
                return true;
            }
        });
        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getRefreshData();
            }
        });
        adapter.setOnLoadMoreListener(new BaseRecyclerViewAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                recyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        if (haveNextPage) {
                            // 列表滑动到底部加载下一组数据
                            getData(REFRESH_TYPE_NEXT_PAGE, curPage);
                        } else {//为0说明没有更多数据
                            adapter.openLoadMore(0, false);
                            if (list.size() > 0) {
                                View view = getLayoutInflater().inflate(R.layout.load_more_footer_layout, (ViewGroup) recyclerView.getParent(), false);
                                ((TextView) view.findViewById(R.id.text)).setText(getString(R.string.info_no_more));
                                adapter.addFooterView(view);
                            }
                        }
                    }
                });
            }
        });
        adapter.setReloadMoreListener(new BaseRecyclerViewAdapter.ReloadMoreListener() {
            @Override
            public void reloadMore() {
                adapter.removeAllFooterView();
                adapter.openLoadMore(true);
                getData(REFRESH_TYPE_NEXT_PAGE, curPage);
            }
        });
    }

    private void gotoUserInfoActivity(FollowerBean bean) {
        if (bean == null) {
            return;
        }
//        final Intent intent = new Intent(this, UserInfoActivity.class);
//        final Bundle bundle = new Bundle();
//        bundle.putString(TheLConstants.BUNDLE_KEY_USER_ID, bean.userId + "");
//        intent.putExtras(bundle);
//        startActivity(intent);
        FlutterRouterConfig.Companion.gotoUserInfo(bean.userId+"");
    }

    private void getData(final int type, int curPage) {
        if (UserInfoTopMainView.PAGE_TYPE_FANS.equals(pageType)) {
            final Flowable<FollowersListNetBean> flowable = DefaultRequestService.createNearbyRequestService().getUserFollowersList(userId, pageSize + "", curPage + "");
            flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<FollowersListNetBean>() {
                @Override
                public void onNext(FollowersListNetBean data) {
                    super.onNext(data);
                    emptyData(false);
                    if (data != null && data.data != null) {
                        FollowersListBean bean = data.data;
                        haveNextPage = bean.users.size() > 0;
                        bean.filtBlockUsers();
                        if (type == REFRESH_TYPE_ALL) {
                            setRefreshData(bean.users);
                            if (!haveNextPage && data.data.users.size() == 0) {
                                emptyData(true);
                            }
                        } else {
                            setLoadMoreData(bean.users);
                        }
                        if (data.data.fansTotal <= 1) {
                            txt_title.setText(data.data.fansTotal + TheLApp.context.getResources().getString(R.string.friends_activity_fans_odd));
                        } else {
                            txt_title.setText(data.data.fansTotal + TheLApp.context.getResources().getString(R.string.friends_activity_fans_even));
                        }
                    }
                }

                @Override
                public void onError(Throwable t) {
                    super.onError(t);
                    if (type == REFRESH_TYPE_NEXT_PAGE) {
                        loadMoreFailed();
                    }
                }

                @Override
                public void onComplete() {
                    super.onComplete();
                    requestFinish();
                }
            });
        } else {
            Flowable<FollowersListNetBean> flowable = RequestBusiness.getInstance().getUserFollowList(userId, pageSize + "", curPage + "");
            flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<FollowersListNetBean>() {
                @Override
                public void onNext(FollowersListNetBean data) {
                    super.onNext(data);
                    emptyData(false);
                    if (data != null && data.data != null) {
                        FollowersListBean bean = data.data;
                        haveNextPage = bean.users.size() > 0;
                        bean.filtBlockUsers();
                        if (type == REFRESH_TYPE_ALL) {
                            setRefreshData(bean.users);
                            if (!haveNextPage && data.data.users.size() == 0) {
                                emptyData(true);
                            }
                        } else {
                            setLoadMoreData(bean.users);
                        }
                        if (data.data.followersTotal <= 1) {
                            txt_title.setText(data.data.followersTotal + TheLApp.context.getResources().getString(R.string.friends_activity_follow_odd));
                        } else {
                            txt_title.setText(data.data.followersTotal + TheLApp.context.getResources().getString(R.string.friends_activity_follow_even));
                        }
                    }
                }

                @Override
                public void onError(Throwable t) {
                    super.onError(t);
                    if (type == REFRESH_TYPE_NEXT_PAGE) {
                        loadMoreFailed();
                    }
                }

                @Override
                public void onComplete() {
                    super.onComplete();
                    requestFinish();

                }
            });
        }

    }

    private void loadMoreFailed() {
        recyclerView.post(new Runnable() {
            @Override
            public void run() {
                adapter.loadMoreFailed((ViewGroup) recyclerView.getParent());
            }
        });

    }

    private void setLoadMoreData(List<FollowerBean> users) {
        curPage += 1;
        list.addAll(users);
        adapter.notifyDataChangedAfterLoadMore(haveNextPage, list.size());
    }

    private void setRefreshData(List<FollowerBean> users) {
        curPage = 2;
        list.clear();
        list.addAll(users);
        adapter.setNewData(list);
        adapter.openLoadMore(list.size(), haveNextPage);
    }

    private void emptyData(boolean empty) {
        empty_view.setVisibility(empty ? View.VISIBLE : View.GONE);
    }

    private void requestFinish() {
        if (swipe_container != null)
            swipe_container.postDelayed(new Runnable() {

                @Override
                public void run() {
                    if (swipe_container != null && swipe_container.isRefreshing())
                        swipe_container.setRefreshing(false);
                }
            }, 1000);
        closeLoading();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(this.getClass().getName());
        MobclickAgent.onResume(this);
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(this.getClass().getName());
        MobclickAgent.onPause(this);
    }

}
