package com.thel.modules.select_image;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import androidx.viewpager.widget.ViewPager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.constants.TheLConstants;
import com.thel.modules.preview_image.LocalPhotoSimplePagerAdapter;
import com.thel.ui.widget.HackyViewPager;

import java.util.ArrayList;
import java.util.Collection;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import video.com.relavideolibrary.BaseRecyclerAdapter;
import video.com.relavideolibrary.BaseViewHolder;
import video.com.relavideolibrary.Utils.DensityUtils;

import static com.thel.modules.select_image.SelectLocalImagesActivity.RESULT_CODE_PREVIEW;

public class SelectLocalImagesPreviewActivity extends BaseActivity implements ViewPager.OnPageChangeListener {

    @BindView(R.id.txt_title)
    TextView txt_title;

    @BindView(R.id.id_item_select)
    TextView id_item_select;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.view_pager)
    HackyViewPager mViewPager;
    private LocalPhotoSimplePagerAdapter adapter;
    private MyAdapter myAdapter;

    @OnClick(R.id.back)
    void back() {
        finish();
    }

    @OnClick(R.id.complete)
    void complete() {
        ArrayList<ImageBean> mSelectedImgBeanTemp = new ArrayList<>();
        for (ImageBean bean : mSelectedImgBean) {
            if (bean.select) mSelectedImgBeanTemp.add(bean);
        }
        Intent intent = getIntent();
        intent.putParcelableArrayListExtra(TheLConstants.BUNDLE_KEY_PHOTOS, mSelectedImgBeanTemp);
        setResult(RESULT_CODE_PREVIEW, intent);
        finish();
    }

    private ArrayList<ImageBean> mSelectedImgBean = new ArrayList<>();

    private int curSelectPos;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_select_local_images_preview);

        ButterKnife.bind(this);

        mSelectedImgBean = getIntent().getParcelableArrayListExtra(TheLConstants.BUNDLE_KEY_PHOTOS);

        txt_title.setText("1/" + mSelectedImgBean.size());

        adapter = new LocalPhotoSimplePagerAdapter(mSelectedImgBean);

        mViewPager.setAdapter(adapter);

        mViewPager.setOnPageChangeListener(this);

        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        myAdapter = new MyAdapter(R.layout.item_image, recyclerView, mSelectedImgBean);
        recyclerView.setAdapter(myAdapter);

        id_item_select.setBackgroundResource(R.drawable.circle_main_color);
        id_item_select.setText(String.valueOf(1));
        id_item_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    mSelectedImgBean.get(curSelectPos).select = !mSelectedImgBean.get(curSelectPos).select;
                    if (mSelectedImgBean.get(curSelectPos).select) {
                        id_item_select.setBackgroundResource(R.drawable.circle_main_color);
                        id_item_select.setText(String.valueOf(curSelectPos + 1));
                    } else {
                        id_item_select.setBackgroundResource(R.mipmap.release_addpage_check_nor);
                        id_item_select.setText("");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onPageSelected(int position) {
        curSelectPos = position;
        txt_title.setText((position + 1) + "/" + mSelectedImgBean.size());
        myAdapter.notifyDataSetChanged();
        recyclerView.smoothScrollToPosition(position);
        if (mSelectedImgBean.get(position).select) {
            id_item_select.setBackgroundResource(R.drawable.circle_main_color);
            id_item_select.setText(String.valueOf(position + 1));
        } else {
            id_item_select.setBackgroundResource(R.mipmap.release_addpage_check_nor);
            id_item_select.setText("");
        }

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    class MyAdapter extends BaseRecyclerAdapter<ImageBean> {

        public MyAdapter(int layoutId, RecyclerView recyclerView, Collection<ImageBean> list) {
            super(layoutId, recyclerView, list);
        }

        @Override
        public void dataBinding(BaseViewHolder baseViewHolder, ImageBean item, int position) {
            RelativeLayout image_bg = baseViewHolder.getView(R.id.image_bg);
            if (curSelectPos == position) {
                image_bg.setBackgroundResource(R.drawable.shape_relacolor_rec);
            } else {
                image_bg.setBackground(null);
            }
            RequestOptions options = new RequestOptions().transform(new CenterCrop(), new RoundedCorners(DensityUtils.dp2px(4)));
            Glide.with(mContext).load(item.imageName).apply(options).into((ImageView) baseViewHolder.getView(R.id.imageview));
            baseViewHolder.getView(R.id.imageview).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mViewPager.setCurrentItem(position);
                }
            });
        }
    }
}
