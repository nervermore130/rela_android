package com.thel.modules.live.surface.watch;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseFragment;
import com.thel.bean.live.LiveMultiSeatBean;
import com.thel.constants.TheLConstants;
import com.thel.ui.widget.MeetItemViewGroup;
import com.thel.utils.L;
import com.thel.utils.UserUtils;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * 相遇中选择
 * Created by lingwei on 2018/5/13.
 */

public class LiveWatchMeetSelectFragment extends BaseFragment {

    private List<LiveMultiSeatBean> seatBeans;
    private int leftTime;
    private TextView meetion_count_down;
    private MeetItemViewGroup select_grid_view;
    private ImageView close_select_view;
    private TextView confirm;
    private TextView not_change;
    private RelativeLayout root_rl;

    public static LiveWatchMeetSelectFragment newInstance(Bundle bundle) {
        LiveWatchMeetSelectFragment instance = new LiveWatchMeetSelectFragment();
        instance.setArguments(bundle);
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            seatBeans = (List<LiveMultiSeatBean>) getArguments().getSerializable(TheLConstants.BUNDLE_KEY_LIVE_SEAT_ROOM);
            leftTime = getArguments().getInt("leftTime");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_meet_select, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        meetion_count_down = view.findViewById(R.id.meetion_count_down);
        select_grid_view = view.findViewById(R.id.select_grid_view);
        close_select_view = view.findViewById(R.id.close_select_view);
        confirm = view.findViewById(R.id.confirm);
        not_change = view.findViewById(R.id.not_change);
        root_rl = view.findViewById(R.id.root_rl);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        initAdapter();
    }

    private void initView() {
        Observable.interval(0, 1, TimeUnit.SECONDS).take(leftTime)//计时次数
                .observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(new Consumer<Long>() {
            @Override
            public void accept(Long aLong) {

                String time = String.valueOf(leftTime - aLong - 1);

                String str = TheLApp.context.getString(R.string.select_lovely, time);

                int startIndex = str.indexOf(time);

                int endIndex = startIndex + time.length();

                SpannableString spannableString = new SpannableString(str);

                spannableString.setSpan(new AbsoluteSizeSpan(32, true), startIndex, endIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                meetion_count_down.setText(spannableString);

                //  String string = meetion_count_down.getText().toString();

                L.d("interval", aLong + "\n" + (leftTime - aLong));
                if (leftTime - aLong == 1) {
                    confirm.performClick();
                }
            }
        });
        close_select_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().getSupportFragmentManager().beginTransaction().remove(LiveWatchMeetSelectFragment.this).commit();

            }
        });
        root_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirm.setEnabled(false);
                confirm.setTextColor(ContextCompat.getColor(TheLApp.getContext(), R.color.white_55_alpha));
                confirm.setBackgroundResource(R.drawable.shape_white_rec);
                not_change.setVisibility(View.INVISIBLE);
                select_grid_view.meetConfirm();
                for (int i = 0; i < seatBeans.size(); i++) {
                    if (seatBeans.get(i).isSelected) {
                        if (meetSelectListener != null) {
                            meetSelectListener.encounterSb(seatBeans.get(i).seatNum);
                        }
                        break;
                    }
                }
            }
        });
    }

    private void initAdapter() {
        if (seatBeans != null) {
            //初始化多人连麦的座位adapter
            select_grid_view.refreshAllSeats(seatBeans);
            select_grid_view.setOnItemClickListener(new MeetItemViewGroup.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    String myUserId = UserUtils.getMyUserId();
                    String currendUserId = seatBeans.get(position).userId;
                    if (!TextUtils.isEmpty(currendUserId) && !currendUserId.equals(myUserId)) {
                        for (int i = 0; i < seatBeans.size(); i++) {
                            if (i != position) {
                                seatBeans.get(i).isSelected = false;
                            } else {
                                seatBeans.get(position).isSelected = true;
                            }
                        }
                        select_grid_view.refreshAllSeats(seatBeans);

                        confirm.setEnabled(true);
                        confirm.setTextColor(ContextCompat.getColor(TheLApp.getContext(), R.color.white));

                        confirm.setBackgroundResource(R.drawable.shape_pink_rec);
                    }
                }
            });
        }
    }

    private MeetSelectListener meetSelectListener;

    public void setMeetSelectListener(MeetSelectListener meetSelectListener) {
        this.meetSelectListener = meetSelectListener;
    }

    public interface MeetSelectListener {
        void encounterSb(int seatNum);
    }

    public void clear() {

        List<LiveMultiSeatBean> seatBeans = select_grid_view.getData();

        for (LiveMultiSeatBean liveMultiSeatBean : seatBeans) {
            liveMultiSeatBean.isSelected = false;
        }

        select_grid_view.refreshAllSeats(seatBeans);
    }
}
