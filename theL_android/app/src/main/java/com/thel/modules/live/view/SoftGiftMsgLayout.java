package com.thel.modules.live.view;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;

import com.thel.app.TheLApp;
import com.thel.modules.live.bean.GiftMsgShowListBean;
import com.thel.utils.Utils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by the L on 2016/5/27.
 * 送礼物后界面显示,礼物打赏消息显示界面
 */
public class SoftGiftMsgLayout extends ViewGroup {
    private Context mContext;
    private Map<Integer, View> itemViews = new HashMap<>();
    private int MsgMaxCount = 2;//显示的最大消息数
    private int itemHeight = 0;//每个itemheight高度
    private int divideHeight = 0;//每个item之间的高度间隔
    private int baseHeight = 0;//初始高度，第一个item的高度,从底部开始计算，防止向下不够
    private EmptySpaceCallBack callBack;//有空位的时候回调
    private OnGiftItemClickListener giftItemClickListener;//点击某个条目监听

    public static final int PORTAINT = 0;
    public static final int LANDSCAPE = 1;

    public SoftGiftMsgLayout(Context context) {
        this(context, null);
    }

    public SoftGiftMsgLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SoftGiftMsgLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        for (int i = 0; i < getChildCount(); i++) {
            View v = getChildAt(i);
            v.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    private void init() {
        itemHeight = Utils.dip2px(TheLApp.getContext(), 70);//每个item高度为60dp,换算成px
        divideHeight = Utils.dip2px(TheLApp.getContext(), 10);//高度间隔20dp;
        baseHeight = Utils.dip2px((TheLApp.getContext()), 20);
    }


    /**
     * 如果有空位，回调接口
     */
    public interface EmptySpaceCallBack {
        /**
         * 某个消息队列已经显示完毕
         *
         * @param giftMsgShowListBean 显示完毕的消息
         */
        void showFinish(GiftMsgShowListBean giftMsgShowListBean);

        /**
         * 发送礼物消息队列
         */
        void pushGiftMsgList();
    }

    /**
     * 设置回调
     *
     * @param callBack
     */
    public void setCallBackListener(EmptySpaceCallBack callBack) {
        this.callBack = callBack;
    }

    /**
     * 有消息提醒并且有空位，执行回调
     */
    public void remindMsg() {
        if (itemViews.size() < MsgMaxCount) {//如果itemview的数量小于最大能显示的数量，自行回调
            if (callBack != null)
                callBack.pushGiftMsgList();
        }
    }

    /**
     * 有消息来的时候，如果有空位，就显示
     *
     * @param giftMsgListBean
     */

    public void showMsg(GiftMsgShowListBean giftMsgListBean) {
        for (int i = 0; i < MsgMaxCount; i++) {
            SoftGiftMsgItemView view = (SoftGiftMsgItemView) itemViews.get(i);
            if (view == null) {
                addNewItem(i, giftMsgListBean);
                return;
            }

        }
    }

    public void setOrientation(int orientation) {
        if (PORTAINT == orientation) {
            divideHeight = Utils.dip2px(TheLApp.getContext(), 10);//高度间隔20dp;
        } else {
            divideHeight = Utils.dip2px(TheLApp.getContext(), 0);//高度间隔20dp;
        }
    }

    /**
     * 新增SoftGiftSendItemLayou 视图
     *
     * @param giftMsgListBean
     */
    private void addNewItem(final int index, final GiftMsgShowListBean giftMsgListBean) {
        SoftGiftMsgItemView itemView = new SoftGiftMsgItemView(mContext);
        itemView.initView(giftMsgListBean);              //新增VIEW
        itemViews.put(index, itemView);
        addView(itemView);
        itemView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        itemView.setShowFinishListener(new SoftGiftMsgItemView.ShowFinishCallBack() {
            @Override
            public void showFinish(View v, GiftMsgShowListBean listbean) {
                setRemoveAnim(v, index, listbean);//设置消失动画
            }
        });
        itemView.setOnSenderClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (giftItemClickListener != null && giftMsgListBean != null) {
                    String userId = giftMsgListBean.userId;
                    giftItemClickListener.showUserMsg(v, userId);
                }
            }
        });

    }

    /**
     * 设置消失动画
     *
     * @param v        当前itemview
     * @param index    当前itemview所在位置
     * @param listbean 当前itemview所显示的消息队列
     */
    private void setRemoveAnim(final View v, final int index, final GiftMsgShowListBean listbean) {
        ((SoftGiftMsgItemView) v).setOnSenderClickListener(null);
        listbean.setShow(false);//这个消息队列显示为false,并且马上移除（本句可要可不要）
        callBack.showFinish(listbean);//某个消息队列显示完毕

        ObjectAnimator translationY = ObjectAnimator.ofFloat(v, TRANSLATION_Y, -1.5f * v.getMeasuredHeight());
        ObjectAnimator alpha = ObjectAnimator.ofFloat(v, ALPHA, 0);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(200);
        animatorSet.setInterpolator(new DecelerateInterpolator());
        animatorSet.play(translationY).with(alpha);
        animatorSet.start();
        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                removeItemView(v, index);//移除这个itemview


            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    /**
     * 移除某个Item
     *
     * @param v     要移除的item
     * @param index item 所在下标
     */
    private void removeItemView(View v, int index) {
        removeView(v);
        itemViews.remove(index);//显示完毕移除view
        remindMsg();//提醒
        v = null;
    }

    public void setOnGiftItemClickListener(OnGiftItemClickListener listener) {
        this.giftItemClickListener = listener;
    }

    /**
     * 点击某个item监听
     */
    public interface OnGiftItemClickListener {
        /**
         * 显示赠送人的信息
         *
         * @param v      点击的条目
         * @param userId 赠送人id
         */
        void showUserMsg(View v, String userId);
    }

    /**
     * 添加view ,显示动画
     *
     * @param view
     */
    private void setAnimation(final View view) {
        ObjectAnimator oa = ObjectAnimator.ofFloat(view, TRANSLATION_X, -view.getMeasuredWidth(), 0);
        oa.setDuration(500);
        oa.start();
        oa.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                ((SoftGiftMsgItemView) view).showCombo();//出现动画结束后开始显示内部连击动画
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

    }


    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {

        for (int i = 0; i < MsgMaxCount; i++) {
            if (itemViews.get(i) != null) {
                layout(itemViews.get(i), i, b);
            }
        }
    }

    /**
     * 布局
     *
     * @param view itemview
     * @param i    item的index
     * @param b    整个view 最底部 height
     */
    private void layout(View view, int i, int b) {
        final int top = b - (i + 1) * itemHeight - i * divideHeight - baseHeight;
        final int bottom = top + itemHeight;
        if (!((SoftGiftMsgItemView) view).isAdd()) {//如果还没有被添加，就添加
            view.layout(0, top, view.getMeasuredWidth(), bottom);
            ((SoftGiftMsgItemView) view).setAdd(true);
            setAnimation(view);
        } else {
            view.layout(view.getLeft(), top, view.getMeasuredWidth(), bottom);
        }
    }

    public void destoryView(){
        removeAllViews();
    }
}
