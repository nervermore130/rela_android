package com.thel.modules.live.view;

import android.media.MediaPlayer;

/**
 * Created by waiarl on 2017/12/13.
 * pk的一些音乐播放
 */

public class LivePkMediaPlayer extends MediaPlayer {
    private static LivePkMediaPlayer instance;

    public static LivePkMediaPlayer getInstance() {
        if (instance == null) {
            instance = new LivePkMediaPlayer();
            instance.setVolume(0.6f, 0.6f);
        }
        return instance;
    }

    @Override
    public void release() {
        super.release();
        instance = null;
    }
}
