package com.thel.modules.main.me.adapter;

import androidx.core.content.ContextCompat;
import android.view.View;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.constants.TheLConstants;
import com.thel.modules.main.me.bean.FriendsBean;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;

import java.util.List;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class FollowingListAdapter extends BaseRecyclerViewAdapter<FriendsBean> {


    public FollowingListAdapter(List<FriendsBean> data) {
        super(R.layout.following_list_item, data);
    }

    @Override
    protected void convert(final BaseViewHolder helper, final FriendsBean friendsBean) {
        if (friendsBean.verifyType <= 0) {// 非加V用户，显示两行，第一行为昵称和简介，第二行为在线状态、距离、感情状态
            helper.setVisibility(R.id.line2, View.VISIBLE);
            helper.setVisibility(R.id.img_indentify, View.GONE);
            helper.setText(R.id.txt_desc, friendsBean.intro + "");


        } else {// 加V用户，显示一行，显示昵称、认证图标、认证信息
            helper.setVisibility(R.id.line2, View.VISIBLE);
            helper.setVisibility(R.id.img_indentify, View.VISIBLE);
            helper.setText(R.id.txt_desc, friendsBean.verifyIntro + "");

            if (friendsBean.verifyType == 1) {// 个人加V
                helper.setImageResource(R.id.img_indentify, R.mipmap.icn_verify_person);
            } else {// 企业加V
                helper.setImageResource(R.id.img_indentify, R.mipmap.icn_verify_enterprise);

            }
        }
        if (friendsBean.hiding > 0) {
            helper.setText(R.id.txt_distance, "");
            if (friendsBean.secretly > 0) {
                helper.setText(R.id.txt_distance, " (" + TheLApp.context.getString(R.string.vip_config_act_secretly_follow_title) + ")");

            }
        } else {
            helper.setText(R.id.txt_distance, friendsBean.distance);
            if (friendsBean.secretly > 0) {
                helper.setText(R.id.txt_distance, friendsBean.distance + " (" + TheLApp.context.getString(R.string.vip_config_act_secretly_follow_title) + ")");

            }
        }

        if (friendsBean.level > 0) {
            helper.setVisibility(R.id.img_vip, VISIBLE);
            switch (friendsBean.level) {
                case 1:
                    helper.setImageResource(R.id.img_vip, R.mipmap.icn_vip_1);
                    break;
                case 2:
                    helper.setImageResource(R.id.img_vip, R.mipmap.icn_vip_2);

                    break;
                case 3:
                    helper.setImageResource(R.id.img_vip, R.mipmap.icn_vip_3);

                    break;
                case 4:
                    helper.setImageResource(R.id.img_vip, R.mipmap.icn_vip_4);

                    break;
            }
        } else {
            helper.setVisibility(R.id.img_vip, GONE);

        }

        // 角色设定 0=unknow,1=t,2=p,3=h,5=bi
        if (friendsBean.roleName.equals("0")) {
            helper.setImageResource(R.id.img_role, R.mipmap.icn_role_unknow);
        } else if (friendsBean.roleName.equals("1")) {
            helper.setImageResource(R.id.img_role, R.mipmap.icn_role_t);
        } else if (friendsBean.roleName.equals("2")) {
            helper.setImageResource(R.id.img_role, R.mipmap.icn_role_p);
        } else if (friendsBean.roleName.equals("3")) {
            helper.setImageResource(R.id.img_role, R.mipmap.icn_role_h);
        } else if (friendsBean.roleName.equals("5")) {
            helper.setImageResource(R.id.img_role, R.mipmap.icn_role_bi);
        } else if (friendsBean.roleName.equals("6")) {
            helper.setImageResource(R.id.img_role, R.mipmap.icn_role_s);
        } else {
            helper.setImageResource(R.id.img_role, R.mipmap.icn_role_unknow);
        }
        // 头像
        helper.setImageUrl(R.id.img_thumb, friendsBean.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE);

        // 用户名
        helper.setText(R.id.txt_name, friendsBean.nickName);


        if (mOnItemClickListener != null) {
            helper.setOnClickListener(R.id.txt_follow, new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    int position = helper.getLayoutPosition();
                    mOnItemClickListener.setOnItemClickListener(v, position, friendsBean);

                }

            });
        }
        if (friendsBean.followStatus == 1) {
            helper.setText(R.id.txt_follow, TheLApp.context.getString(R.string.userinfo_activity_followed));
            helper.setTextColor(R.id.txt_follow, ContextCompat.getColor(TheLApp.context, R.color.tab_normal));
            helper.setBackgroundRes(R.id.txt_follow, R.drawable.bg_unfollow_rectangle_buttom_blue);
        } else {
            helper.setText(R.id.txt_follow, TheLApp.context.getString(R.string.userinfo_activity_follow));
            helper.setTextColor(R.id.txt_follow, ContextCompat.getColor(TheLApp.context, R.color.white));
            helper.setBackgroundRes(R.id.txt_follow, R.drawable.bg_follow_rectangle_buttom_blue);
        }

    }

    private OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(OnItemClickListener mOnItemClickListener) {
        this.mOnItemClickListener = mOnItemClickListener;
    }

    public interface OnItemClickListener {
        void setOnItemClickListener(View view, int position, FriendsBean item);
    }
}
