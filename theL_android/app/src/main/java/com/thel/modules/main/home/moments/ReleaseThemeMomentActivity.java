package com.thel.modules.main.home.moments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.thel.R;
import com.thel.TagBean;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.bean.ContactBean;
import com.thel.bean.MentionedUserBean;
import com.thel.bean.ReleaseMomentListBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.theme.ThemeClassBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.modules.label.LabelActivity;
import com.thel.modules.select_contacts.SelectContactsActivity;
import com.thel.modules.select_contacts.SelectUserinfoAdapter;
import com.thel.modules.select_image.SelectLocalImagesActivity;
import com.thel.modules.theme.ThemeClassifyActivity;
import com.thel.service.ReleaseMomentsService;
import com.thel.ui.widget.recyclerview.decoration.DefaultItemDivider;
import com.thel.ui.widget.tourguide.FrameLayoutWithHole;
import com.thel.ui.widget.tourguide.MyTourGuide;
import com.thel.utils.AppInit;
import com.thel.utils.DialogUtil;
import com.thel.utils.GsonUtils;
import com.thel.utils.ImageUtils;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SharedPrefUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.UMShareAPI;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.functions.Consumer;
import tourguide.tourguide.Overlay;
import tourguide.tourguide.Pointer;
import tourguide.tourguide.ToolTip;

import static com.thel.modules.main.home.moments.ReleaseMomentActivity.RELEASE_TYPE_PHOTOS;


/**
 * 发布话题日志页面
 */
public class ReleaseThemeMomentActivity extends BaseActivity implements View.OnClickListener {

    public static final String RELEASE_TYPE_TOPIC = "topic";

    @BindView(R.id.lin_back)
    LinearLayout lin_back;

    @BindView(R.id.title)
    TextView title;

    @BindView(R.id.img_done)
    ImageView img_done;

    @BindView(R.id.container)
    LinearLayout container;

    @BindView(R.id.container1)
    LinearLayout container1;

    private ImageView shareToLogo;

    private RelativeLayout mention_content, shareToContent, rel_select_theme, add_topic, rel_top;

    private TextView select_theme, txt_limit, shareToType;

    private RecyclerView portrait;

    private EditText edit_theme;

    private SimpleDraweeView img_bg;

    private FrameLayout frame_guide;

    private Integer[] shareIconArr;

    private int shareToPostion = 0;

    private List<String> shareToTipsList, shareToList;

    private List<PicBean> selectedPics = new ArrayList<>();

    // 判断是不是已经选择了一张以上图片，因为点击按钮也是放在recyclerview中的，所以当数据源的count > 1时，才表示已经选择了图片
    private final int HAVE_SELECTED_PIC_COUNT = 1;

    private final int SELECT_PIC_LIMIT = 6;

    private String releaseType = "", themeStr, takePhotoPath;

    private ArrayList<ContactBean> mentionList = new ArrayList<>();

    private SelectUserinfoAdapter selectUserinfoAdapter;

    private MyTourGuide mTourGuideHandler;

    // 防止多次点击选择图片
    private boolean isSelectingPhoto = false;

    private ThemeClassBean themeClassBean;

    // 选择的图片地址
    private String bgPicUrl;

    private TextWatcher editThemeTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            int count = 140 - Utils.charCount(s.toString());
            if (count <= 20) {
                txt_limit.setVisibility(View.VISIBLE);
                if (count >= 0)
                    txt_limit.setTextColor(getResources().getColor(R.color.white));
                else if (count < 0)
                    txt_limit.setTextColor(ContextCompat.getColor(ReleaseThemeMomentActivity.this, R.color.tag_color));
                txt_limit.setText(count / 2 + "");
            } else {
                txt_limit.setVisibility(View.INVISIBLE);
            }
            themeStr = s.toString().trim();
            if (!TextUtils.isEmpty(themeStr) && count >= 0 && !Utils.isChaneseLanguage()) {
                setDoneButtonEnabled(true);
            } else if (!TextUtils.isEmpty(themeStr) && count >= 0 && themeClassBean != null && !TextUtils.isEmpty(themeClassBean.type) && Utils.isChaneseLanguage()) {
                setDoneButtonEnabled(true);

            } else {
                setDoneButtonEnabled(false);
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //VmPolicy方式处理FileUriExposedException
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
        }

        setContentView(R.layout.activity_release_theme_moment);

        ButterKnife.bind(this);

        releaseType = getIntent().getStringExtra(TheLConstants.BUNDLE_KEY_RELEASE_TYPE);

        dynamicInflateView();

        initEvent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(this.getClass().getName());
        MobclickAgent.onResume(this);
        isSelectingPhoto = false;
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(this.getClass().getName());
        MobclickAgent.onPause(this);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            close();
        }
        return true;
    }

    @SuppressLint("NewApi")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);

        if (resultCode == TheLConstants.RESULT_CODE_TAKE_PHOTO) {// 拍摄照片
            takePhotoPath = data.getStringExtra(TheLConstants.BUNDLE_KEY_IMAGE_OUTPUT_PATH);
            if (releaseType.equals(RELEASE_TYPE_TOPIC)) {
                if (!TextUtils.isEmpty(takePhotoPath)) {
                    bgPicUrl = ImageUtils.handlePhoto1(takePhotoPath, TheLConstants.F_TAKE_PHOTO_ROOTPATH, ImageUtils.getPicName(), false, TheLConstants.MAX_PIC_WIDTH, TheLConstants.MAX_PIC_HEIGHT, TheLConstants.PIC_QUALITY, 3);

                    img_bg.setImageURI(Uri.parse(TheLConstants.FILE_PIC_URL + bgPicUrl));
                }
            }
        } else if (resultCode == TheLConstants.RESULT_CODE_WRITE_MOMENT_SELECT_CONTACT) {// 选择@联系人
            if (data.getSerializableExtra(TheLConstants.BUNDLE_KEY_FRIENDS_LIST) != null) {
                mentionList.clear();
                mentionList.addAll((ArrayList<ContactBean>) data.getSerializableExtra(TheLConstants.BUNDLE_KEY_FRIENDS_LIST));
                Collections.reverse(mentionList);
                refreshMentionList();
            }
        } else if (resultCode == TheLConstants.RESULT_CODE_SELECT_LOCAL_IMAGE) {// 选择了图片
            String photoPath = data.getStringExtra(TheLConstants.BUNDLE_KEY_LOCAL_IMAGE_PATH);

            if (!TextUtils.isEmpty(photoPath)) {
                if (releaseType.equals(RELEASE_TYPE_TOPIC)) {
                    bgPicUrl = ImageUtils.handlePhoto1(photoPath, TheLConstants.F_TAKE_PHOTO_ROOTPATH, ImageUtils.getPicName(), false, TheLConstants.MAX_PIC_WIDTH, TheLConstants.MAX_PIC_HEIGHT, TheLConstants.PIC_QUALITY, 3);

                    img_bg.setImageURI(Uri.parse(TheLConstants.FILE_PIC_URL + bgPicUrl));
                }
            } else {
                Toast.makeText(this, getString(R.string.info_rechoise_photo), Toast.LENGTH_SHORT).show();
            }
        } else if (resultCode == TheLConstants.RESULT_CODE_SELECT_TYPE && Utils.isChaneseLanguage()) { //话题选择分类
            setDoneButtonEnabled(false);

            if (data.getSerializableExtra(TheLConstants.BUNDLE_KEY_THEME_LIST) != null) {
                themeClassBean = (ThemeClassBean) data.getSerializableExtra(TheLConstants.BUNDLE_KEY_THEME_LIST);
                if (!TextUtils.isEmpty(themeClassBean.text) && Utils.isChaneseLanguage()) {
                    select_theme.setText(themeClassBean.text + " >");
                    if (!TextUtils.isEmpty(themeStr)) {
                        setDoneButtonEnabled(true);
                    }
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        ViewUtils.preventViewMultipleClick(v, 1000);

        switch (v.getId()) {
            case R.id.rel_select_theme:
                startActivityForResult(new Intent(this, ThemeClassifyActivity.class), TheLConstants.BUNDLE_CODE_SELECT_THEME);
                break;
            case R.id.img_bg:
                if (!isSelectingPhoto) {
                    isSelectingPhoto = true;
                    if (mTourGuideHandler != null) {
                        mTourGuideHandler.cleanUp();
                        SharedPrefUtils.setBoolean(SharedPrefUtils.FILE_GUIDES, SharedPrefUtils.GUIDES_CHANGE_TOPIC_MOMENT_BG, true);
                    }
                    new RxPermissions(this).request(Manifest.permission.READ_EXTERNAL_STORAGE).subscribe(new Consumer<Boolean>() {
                        @Override
                        public void accept(Boolean aBoolean) {
                            startActivityForResult(new Intent(ReleaseThemeMomentActivity.this, SelectLocalImagesActivity.class), TheLConstants.BUNDLE_CODE_SELECT_PHOTO);
                        }
                    });
                }
                break;
            case R.id.img_done:
                if (!TextUtils.isEmpty(themeStr) && themeClassBean != null && !TextUtils.isEmpty(themeClassBean.type)) {
                    releaseMoment();
                }
                break;
            case R.id.lin_back:
                close();
                break;
            case R.id.mention_content:
                Intent intent = new Intent(ReleaseThemeMomentActivity.this, SelectContactsActivity.class);
                intent.putExtra(TheLConstants.BUNDLE_KEY_FRIENDS_LIST, mentionList);
                startActivityForResult(intent, TheLConstants.BUNDLE_CODE_WRITE_MOMENT_ACTIVITY);
                break;
            case R.id.share_to_content:
                share();
                break;
            case R.id.add_topic:
//                startActivityForResult(new Intent(ReleaseThemeMomentActivity.this, LabelActivity.class), TheLConstants.BUNDLE_CODE_WRITE_MOMENT_ACTIVITY);
                FlutterRouterConfig.Companion.gotoTagList(new ArrayList<>());
                break;
        }
    }

    private void refreshMentionList() {
        if (selectUserinfoAdapter == null) {
            selectUserinfoAdapter = new SelectUserinfoAdapter(this, mentionList);
            portrait.addItemDecoration(new DefaultItemDivider(TheLApp.getContext(), LinearLayoutManager.HORIZONTAL, R.color.transparent, 10, true, true));

            //设置布局管理器
            portrait.setLayoutManager(new LinearLayoutManager(TheLApp.getContext(), LinearLayoutManager.HORIZONTAL, false));
            //如果可以确定每个item的高度是固定的，设置这个选项可以提高性能
            portrait.setHasFixedSize(true);
            portrait.setAdapter(selectUserinfoAdapter);
        } else
            selectUserinfoAdapter.notifyDataSetChanged();
    }

    private void initEvent() {

        if (!TextUtils.isEmpty(releaseType)) {
            switch (releaseType) {
                case RELEASE_TYPE_PHOTOS:
                    Intent intent = new Intent(ReleaseThemeMomentActivity.this, SelectLocalImagesActivity.class);
                    intent.putExtra(TheLConstants.BUNDLE_KEY_SELECT_AMOUNT, SELECT_PIC_LIMIT);
                    intent.putExtra("direct_release", true);// 控制如果不选择照片或音乐，则直接关闭发布日志页面
                    startActivityForResult(intent, TheLConstants.BUNDLE_CODE_WRITE_MOMENT_ACTIVITY);
                    break;
            }
        }

        MomentsBean moment = (MomentsBean) getIntent().getSerializableExtra(TheLConstants.BUNDLE_KEY_MOMENT_BEAN);

        if (moment != null) {
            edit_theme.setText(moment.momentsText);
            bgPicUrl = moment.imageUrl;
            img_bg.setImageURI(Uri.parse(TheLConstants.FILE_PIC_URL + bgPicUrl));
            themeClassBean = new ThemeClassBean();
            themeClassBean.type = moment.themeClass;
            String text = getThemeName(themeClassBean.type) + " >";
            select_theme.setText(text);
            setDoneButtonEnabled(true);
        }
    }

    private String getThemeName(String type) {
        // 获取所有键值对对象的集合
        Set<Map.Entry<Integer, ThemeClassBean>> set = ThemeClassifyActivity.themeClassBeanMap.entrySet();
        // 遍历键值对对象的集合，得到每一个键值对对象
        for (Map.Entry<Integer, ThemeClassBean> themeClassBeanEntry : set) {
            // 根据键值对对象获取键和值

            ThemeClassBean themeClassBean = themeClassBeanEntry.getValue();
            if (themeClassBean.type.equals(type)) {
                return themeClassBean.text;
            }

        }
        return null;
    }

    /**
     * 根据releaseType动态加载布局
     */
    private void dynamicInflateView() {

        if (!TextUtils.isEmpty(releaseType)) {
            View release_moment_option = View.inflate(this, R.layout.release_moment_option, null);
            add_topic = release_moment_option.findViewById(R.id.add_topic);
            add_topic.setOnClickListener(this);
            shareToContent = release_moment_option.findViewById(R.id.share_to_content);
            shareToContent.setOnClickListener(this);
            shareToType = release_moment_option.findViewById(R.id.share_to_txt);
            mention_content = release_moment_option.findViewById(R.id.mention_content);
            mention_content.setOnClickListener(this);
            portrait = release_moment_option.findViewById(R.id.portrait);
            shareToLogo = release_moment_option.findViewById(R.id.share_to_logo);

            switch (releaseType) {
                case RELEASE_TYPE_TOPIC:
                    View release_moment_topic = View.inflate(this, R.layout.release_moment_topic, null);
                    img_bg = release_moment_topic.findViewById(R.id.img_bg);
                    img_bg.setOnClickListener(this);
                    frame_guide = release_moment_topic.findViewById(R.id.frame_guide);
                    txt_limit = release_moment_topic.findViewById(R.id.txt_limit);
                    edit_theme = release_moment_topic.findViewById(R.id.edit_theme);
                    rel_select_theme = release_moment_topic.findViewById(R.id.rel_select_theme);
                    rel_select_theme.setOnClickListener(this);
                    select_theme = release_moment_topic.findViewById(R.id.select_theme);
                    rel_top = release_moment_topic.findViewById(R.id.rel_top);
                    rel_top.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, AppInit.displayMetrics.widthPixels));

                    add_topic.setVisibility(View.GONE);
                    title.setText(getString(R.string.info_topic));
                    edit_theme.addTextChangedListener(editThemeTextWatcher);

                    if (!Utils.isChaneseLanguage()) {
                        rel_select_theme.setVisibility(View.GONE);
                    }

                    if (!SharedPrefUtils.getBoolean(SharedPrefUtils.FILE_GUIDES, SharedPrefUtils.GUIDES_CHANGE_TOPIC_MOMENT_BG, false))
                        mTourGuideHandler = MyTourGuide.init(this, new MyTourGuide.GlobalLayoutCallback() {
                            @Override
                            public void onGlobalLayout() {
                                final FrameLayoutWithHole overlay1 = mTourGuideHandler.getOverlay();
                                if (overlay1 != null) {
                                    overlay1.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            if (mTourGuideHandler != null) {
                                                mTourGuideHandler.cleanUp();
                                            }
                                        }
                                    });
                                }
                            }
                        }).with(MyTourGuide.Technique.Click).setPointer(new Pointer()).setToolTip(new ToolTip().setBackgroundColor(getResources().getColor(R.color.tab_normal)).setTitle(getString(R.string.info_note)).setDescription(getString(R.string.write_love_moment_act_tip))).setOverlay(new Overlay()).playOn(frame_guide);

                    container.addView(release_moment_topic);
                    container.addView(release_moment_option);
                    break;
            }
        }

        shareToType.setText(getResources().getStringArray(R.array.moments_share_to)[0]);
        lin_back.setOnClickListener(this);
    }

    private void close() {
        if (selectedPics.size() > HAVE_SELECTED_PIC_COUNT) {
            DialogUtil.showConfirmDialog(ReleaseThemeMomentActivity.this, null, getString(R.string.moments_write_moment_discard_tip), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    finish();
                }
            });
        } else {
            finish();
        }
    }

    private void setDoneButtonEnabled(boolean enabled) {
        if (enabled) {
            img_done.setImageResource(R.mipmap.btn_done);
            img_done.setOnClickListener(this);
        } else {
            img_done.setImageResource(R.mipmap.btn_done_disable);
            img_done.setOnClickListener(null);
        }
    }

    private void share() {
        if (shareIconArr == null)
            shareIconArr = new Integer[]{R.mipmap.icn_feed_share_all,
                    R.mipmap.icn_feed_share_friends,
                    R.mipmap.icn_feed_share_private,
                    R.mipmap.icn_feed_share_anonymous};

        if (shareToList == null) {
            String[] shareToArr = this.getResources().getStringArray(R.array.moments_share_to);
            shareToList = Arrays.asList(shareToArr);
        }

        if (shareToTipsList == null) {
            String[] shareToTipsArr = this.getResources().getStringArray(R.array.moments_share_to_tips);
            shareToTipsList = Arrays.asList(shareToTipsArr);
        }

        DialogUtil.getInstance().showSingleChoiseDialog(this, "", shareToList, shareIconArr, shareToTipsList, shareToPostion, new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                DialogUtil.getInstance().closeDialog();
                shareToPostion = (Integer) view.getTag();
                shareToType.setText(shareToList.get(shareToPostion));
                shareToLogo.setImageResource(shareIconArr[shareToPostion]);
                if (shareToPostion == 2 && !releaseType.equals(RELEASE_TYPE_TOPIC)) {//当是自己,并且不是发布话题的时候，是不能提及的
                    mentionList.clear();
                    refreshMentionList();
                    mention_content.setVisibility(View.GONE);
                } else {
                    mention_content.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void releaseMoment() {
        MomentsBean sendMoment = new MomentsBean();
        sendMoment.userId = Integer.valueOf(ShareFileUtils.getString(ShareFileUtils.ID, "0"));
        sendMoment.nickname = ShareFileUtils.getString(ShareFileUtils.USER_NAME, "");
        sendMoment.releaseTime = System.currentTimeMillis();
        sendMoment.avatar = ShareFileUtils.getString(ShareFileUtils.AVATAR, "");
        sendMoment.momentsId = System.currentTimeMillis() + MomentsBean.SEND_MOMENT_FLAG;// 预发送的日志id标识
        if (releaseType.equals(RELEASE_TYPE_TOPIC)) {
            sendMoment.momentsText = edit_theme.getText().toString().trim();
            //        sendMoment.themeClass = choosedTag;
            if (Utils.isChaneseLanguage() && themeClassBean != null && !TextUtils.isEmpty(themeClassBean.type)) {
                sendMoment.themeClass = themeClassBean.type;
            }
            sendMoment.momentsType = MomentsBean.MOMENT_TYPE_THEME;
            // 封装图片日志
            sendMoment.imageUrl = bgPicUrl;
        }

        sendMoment.shareTo = shareToPostion == 3 ? 1 : shareToPostion + 1;
        sendMoment.secret = shareToPostion == 3 ? MomentsBean.IS_SECRET : MomentsBean.IS_NOT_SECRET;

        // 封装提及用户
        if (!mentionList.isEmpty()) {
            Collections.reverse(mentionList);
            for (ContactBean contact : mentionList) {
                MentionedUserBean atUser = new MentionedUserBean();
                atUser.userId = Integer.valueOf(contact.userId);
                atUser.nickname = contact.nickName;
                atUser.momentsId = sendMoment.releaseTime + "";
                atUser.avatar = contact.avatar;
                sendMoment.atUserList.add(atUser);
            }
        }

        //将发布内容以json的形式保存本地
        String releaseContent = ShareFileUtils.getString(TheLConstants.RELEASE_CONTENT_KEY, "");
        String toJson;
        int releaseIndex;
        if (TextUtils.isEmpty(releaseContent)) {//之前没有未发布内容
            List<MomentsBean> list = new ArrayList<>();
            list.add(sendMoment);
            ReleaseMomentListBean releaseTagListBean = new ReleaseMomentListBean();
            releaseTagListBean.list = list;
            toJson = GsonUtils.createJsonString(releaseTagListBean);
            releaseIndex = 0;
        } else {//有未发布内容
            ReleaseMomentListBean releaseTagListBean = GsonUtils.getObject(releaseContent, ReleaseMomentListBean.class);
            releaseTagListBean.list.add(sendMoment);
            toJson = GsonUtils.createJsonString(releaseTagListBean);
            releaseIndex = releaseTagListBean.list.size() - 1;
        }
        ShareFileUtils.setString(TheLConstants.RELEASE_CONTENT_KEY, toJson);
        L.d("release", toJson);
        Intent intent = new Intent(this, ReleaseMomentsService.class);
        // 要发的日志标识，用发布时间作为标识
        intent.putExtra(ReleaseMomentsService.MOMENT_INDEX, releaseIndex);
        TheLApp.getContext().startService(intent);
        finish();
    }
}
