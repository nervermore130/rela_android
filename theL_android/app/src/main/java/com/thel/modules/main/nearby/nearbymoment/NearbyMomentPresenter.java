package com.thel.modules.main.nearby.nearbymoment;

import com.thel.bean.moments.MomentsListBean;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.service.DefaultRequestService;
import com.thel.ui.adapter.MomentsAdapter;
import com.thel.utils.L;

import org.reactivestreams.Subscription;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by waiarl on 2017/10/12.
 */

public class NearbyMomentPresenter implements NearbyMomentContract.Presenter {
    private final NearbyMomentContract.View view;
    private final int PAGE_SIZE = 20;
    private int curPage = 1;
    // 刷新类型，全部刷新（即下拉刷新）为1，还是加载下一页为2
    public int refreshType = 0;
    public final int REFRESH_TYPE_ALL = 1;
    public final int REFRESH_TYPE_NEXT_PAGE = 2;

    public NearbyMomentPresenter(NearbyMomentContract.View view) {
        this.view = view;
        view.setPresenter(this);
    }

    @Override
    public void getRefreshNearbyMomentList() {
        curPage = 1;
        refreshType = REFRESH_TYPE_ALL;
        getData(refreshType, curPage);
    }

    @Override
    public void getMoreNearbyMomentList(int curPage) {
        this.curPage = curPage;
        refreshType = REFRESH_TYPE_NEXT_PAGE;
        getData(refreshType, curPage);
    }

    public void getData(final int refreshType, int curPage) {
        L.d("Nearby", " getData : " + curPage);
        final Flowable<MomentsListBean> flowable = DefaultRequestService.createNearbyRequestService().getNearbyMomentsList(curPage + "");
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<MomentsListBean>() {

            @Override
            public void onNext(MomentsListBean momentsListBean) {
                super.onNext(momentsListBean);
                if (!hasErrorCode && momentsListBean != null && momentsListBean.momentsList != null) {
//                    NearbyUtils.filtNearbyMoment(momentsListBean);//过滤黑名单
                    momentsListBean.filterBlockMoments();
                    MomentsAdapter.filterUserMoments(momentsListBean.momentsList);
                    MomentsAdapter.filterBlack(momentsListBean.momentsList);
                    MomentsAdapter.filterAdMoment(momentsListBean.momentsList);
                    if (REFRESH_TYPE_ALL == refreshType) {
                        view.showRefreshNearbyMomentList(momentsListBean);
                        if (momentsListBean.momentsList.size() == 0 && !momentsListBean.haveNextPage) {
                            view.emptyData();
                        }
                    } else {
                        view.showMoreNearbyMomentList(momentsListBean);
                    }
                } else if (hasErrorCode && REFRESH_TYPE_NEXT_PAGE == refreshType) {
                    view.loadMoreFailed();
                } else if (hasErrorCode && REFRESH_TYPE_ALL == refreshType) {
                    view.emptyData();
                }
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                if (REFRESH_TYPE_ALL == refreshType) {
                    view.emptyData();
                } else {
                    view.loadMoreFailed();
                }
            }

            @Override
            public void onComplete() {
                view.requestFinished();
            }
        });
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {

    }
}
