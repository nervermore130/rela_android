package com.thel.modules.live.view.sound;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thel.base.BaseDialogFragment;
import com.thel.modules.live.in.LiveBaseView;
import com.thel.utils.AppInit;
import com.thel.utils.SizeUtils;

/**
 * Created by waiarl on 2018/1/23.
 */

public class LiveSoundChoiceDialogView extends BaseDialogFragment implements LiveBaseView<LiveSoundChoiceDialogView> {


    private LiveSoundChoiceView view;
    private static LiveSoundChoiceDialogView instance;

    public static LiveSoundChoiceDialogView getInstance(Bundle bundle) {
        instance = new LiveSoundChoiceDialogView();
        instance.setArguments(bundle);
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = new LiveSoundChoiceView(getContext());
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
        setListener();
    }

    private void init() {

    }

    private void setListener() {
        view.setLiveSoundCloseDialogListener(new LiveSoundChoiceView.LiveSoundAppCloseListener() {
            @Override
            public void closeDialog() {
                dismiss();
            }
        });
    }

    public LiveSoundChoiceDialogView setLiveSoundAppChoiceListener(LiveSoundChoiceView.LiveSoundAppChoiceListener liveSoundAppChoiceListener) {
        if (view != null) {
            view.setLiveSoundAppChoiceListener(liveSoundAppChoiceListener);
        }
        return this;
    }

    @Override
    public void onResume() {
        super.onResume();
        int width = AppInit.displayMetrics.widthPixels;
        int height = SizeUtils.dip2px(getContext(), 130);
        getDialog().getWindow().setLayout(width, height);
    }

    @Override
    public LiveSoundChoiceDialogView show() {
        return null;
    }

    @Override
    public LiveSoundChoiceDialogView hide() {
        return null;
    }

    @Override
    public LiveSoundChoiceDialogView destroyView() {
        return null;
    }

    @Override
    public boolean isAnimating() {
        return false;
    }

    @Override
    public void setAnimating(boolean isAnimating) {

    }

    @Override
    public void showShade(boolean show) {

    }
}
