package com.thel.modules.welcome

import android.app.Activity
import android.content.Intent
import android.content.res.Configuration
import android.os.Handler
import android.os.Message
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.*
import com.thel.BuildConfig
import com.thel.R
import com.thel.app.TheLApp
import com.thel.base.BaseUIFragment
import com.thel.constants.TheLConstants
import com.thel.growingio.GrowingIoConstant
import com.thel.manager.ChatServiceManager
import com.thel.modules.main.MainActivity
import com.thel.network.service.DefaultRequestService
import com.thel.utils.*
import com.umeng.analytics.MobclickAgent
import kotlinx.android.synthetic.main.activity_welcome.*
import me.rela.rf_s_bridge.RfSBridgePlugin
import java.lang.ref.WeakReference
import java.util.*

class WelcomeFragment : BaseUIFragment() {

    private var waitTime: Long = 2500

    private var mIntentHandler: IntentHandler? = null

    override fun setLayout(): Int = R.layout.activity_welcome

    companion object {

        const val MSG_START_HOME = 0
        const val MSG_START_ME = 1
    }

    override fun initView() {

        initLanguage()

        findViewById()

        initSDKConfig()

        mIntentHandler = activity?.let { IntentHandler(it) }

        val isFirstShowPushDialog = ShareFileUtils.getBoolean(ShareFileUtils.IS_FIRST_SHOW_PUSH_DIALOG, true)

        if (isFirstShowPushDialog) {
            showPushDialog()
            ShareFileUtils.setBoolean(ShareFileUtils.IS_FIRST_SHOW_PUSH_DIALOG, false)
        } else {
            if (!BuildConfig.DEBUG) {
                appStart()
            }
        }

        if (!TextUtils.isEmpty(UserUtils.getMyUserId())) {
            ChatServiceManager.getInstance().startService(TheLApp.context)
        }

        if (ShareFileUtils.getBoolean(ShareFileUtils.CRASH_LOG_UPLOAD, false))
            ExecutorServiceUtils.getInstatnce().exec(LogService())

    }


    private inner class IntentHandler(activity: Activity) : Handler() {

        private val weakReference: WeakReference<Activity> = WeakReference(activity)

        override fun handleMessage(msg: Message) {

            Log.d("FlutterViewProvider", " ------------handleMessage-----------")

            val activity = weakReference.get()
            if (activity == null) {
                Log.d("WelcomeActivity", "WelcomeActivity destory")
                return
            }
            var page = 1//默认跳到1
            var notification = ""
            when (msg.what) {
                MSG_START_HOME -> {
                    page = 1
                    notification = msg.obj as String
                }
                MSG_START_ME -> page = 5
            }

            L.d("WelcomeActivity", " UserUtils.getUserKey() : " + UserUtils.getUserKey())

            L.d("WelcomeActivity", " UserUtils.getMyUserId() : " + UserUtils.getMyUserId())

            val intent: Intent
            if (!TextUtils.isEmpty(UserUtils.getUserKey()) && !TextUtils.isEmpty(UserUtils.getMyUserId())) {
                intent = Intent(activity, MainActivity::class.java)
                if (!TextUtils.isEmpty(notification) && notification == "notification") {
                    intent.putExtra("fromPage", "notification")
                }
                intent.putExtra(TheLConstants.BUNDLE_KEY_WHERE_TO_GO, page.toString() + "")
                activity.startActivity(intent)
                activity.finish()

            } else {

                MobclickAgent.onEvent(activity, "enter_default_page")// 注册转化率统计第一步:刚打开软件
                RfSBridgePlugin.getInstance().activeRpc("EventLogout", "", null)
                intent = Intent(activity, MainActivity::class.java)
                activity.startActivity(intent)
            }

        }

    }

    private fun findViewById() {
        val topMargin = (AppInit.displayMetrics.heightPixels / 4.6).toInt()
        val w = AppInit.displayMetrics.widthPixels / 3
        val h = w * 1470 / 3072
        val param = img_logo?.layoutParams as RelativeLayout.LayoutParams
        param.width = w
        param.height = h
        param.topMargin = topMargin

        proxy.setText(ShareFileUtils.getString(ShareFileUtils.FLUTTER_PROXY, ""))

        proxy_open.setChecked(ShareFileUtils.getBoolean(ShareFileUtils.FLUTTER_PROXY_OPEN, false))
        proxy_open.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked -> ShareFileUtils.setBoolean(ShareFileUtils.FLUTTER_PROXY_OPEN, isChecked) })

        on_line_btn.setOnClickListener(View.OnClickListener { v ->
            ViewUtils.preventViewMultipleClick(v, 3000)
            ShareFileUtils.setString(ShareFileUtils.HTTP_URL, UrlConstants.BASE_URL)
            ShareFileUtils.setString(ShareFileUtils.HTTP_HOST, UrlConstants.HTTP_HOST)
            ShareFileUtils.setString(ShareFileUtils.IM_HOST, UrlConstants.IM_HOST)
            ShareFileUtils.setString(ShareFileUtils.FLUTTER_PROXY, proxy.getText().toString().trim { it <= ' ' })
            DefaultRequestService.reBuild()
            appStart()
        })

        text_btn.setOnClickListener(View.OnClickListener { v ->
            ViewUtils.preventViewMultipleClick(v, 3000)
            ShareFileUtils.setString(ShareFileUtils.HTTP_URL, UrlConstants.BASE_TEST_URL)
            ShareFileUtils.setString(ShareFileUtils.HTTP_HOST, UrlConstants.HTTP_TEST_HOST)
            ShareFileUtils.setString(ShareFileUtils.IM_HOST, UrlConstants.IM_TEST_HOST)
            ShareFileUtils.setString(ShareFileUtils.FLUTTER_PROXY, proxy.getText().toString().trim { it <= ' ' })
            DefaultRequestService.reBuild()
            appStart()
        })


        go_btn.setOnClickListener(View.OnClickListener { v ->
            ViewUtils.preventViewMultipleClick(v, 3000)
            ShareFileUtils.setString(ShareFileUtils.HTTP_URL, UrlConstants.BASE_PRE_URL)
            ShareFileUtils.setString(ShareFileUtils.HTTP_HOST, UrlConstants.HTTP_PRE_HOST)
            ShareFileUtils.setString(ShareFileUtils.IM_HOST, UrlConstants.IM_PRE_HOST)
            ShareFileUtils.setString(ShareFileUtils.FLUTTER_PROXY, proxy.getText().toString().trim { it <= ' ' })
            DefaultRequestService.reBuild()
            appStart()
        })

        if (!BuildConfig.DEBUG || L.IS_USE_BAIDU_TEST) {
            test_ll.setVisibility(View.GONE)
            waitTime = 1000
        } else {
            test_ll.setVisibility(View.VISIBLE)
        }


    }

    private fun showPushDialog() {
        activity?.let { activity ->
            PermissionUtil.requestPushPermission(activity, object : PermissionUtil.PermissionCallback {
                override fun granted() {
                    PermissionUtil.requestLocationPermission(activity, object : PermissionUtil.PermissionCallback {
                        override fun granted() {
                            LocationUtils.setUpLocation()
                            if (!BuildConfig.DEBUG)
                                appStart()
                        }

                        override fun denied() {
                            if (!BuildConfig.DEBUG)
                                appStart()
                            GrowingIOUtil.tracePush(GrowingIoConstant.CLOSE_LOCATION)
                        }

                        override fun gotoSetting() {
                            activity.finish()
                        }
                    })
                }

                override fun denied() {
                    PermissionUtil.requestLocationPermission(activity, object : PermissionUtil.PermissionCallback {
                        override fun granted() {
                            LocationUtils.setUpLocation()
                            if (!BuildConfig.DEBUG)
                                appStart()
                        }

                        override fun denied() {
                            if (!BuildConfig.DEBUG)
                                appStart()
                            GrowingIOUtil.tracePush(GrowingIoConstant.CLOSE_LOCATION)

                        }

                        override fun gotoSetting() {
                            activity.finish()
                        }
                    })
                    GrowingIOUtil.tracePush(GrowingIoConstant.CLOSE_PUSH)
                }

                override fun gotoSetting() {
                    activity.finish()
                }
            })
        }
    }

    private fun initSDKConfig() {
        // 友盟统计
        MobclickAgent.openActivityDurationTrack(false)
        // 关闭友盟错误统计
        MobclickAgent.setCatchUncaughtExceptions(false)
        MobclickAgent.onEvent(TheLApp.context, "open_app")
    }

    private fun appStart() {

        val i = activity?.intent
        var app = ""
        var fromPage = ""
        if (null != i) {
            if (i.hasExtra("startApp")) {
                app = i.getStringExtra("startApp")
            }
            if (i.hasExtra("fromPage")) {
                fromPage = i.getStringExtra("fromPage")
            }
        }
        if (!TextUtils.isEmpty(app) && app == "FinishActivity") {
            activity?.finish()
        } else {
            mIntentHandler?.sendMessageDelayed(mIntentHandler!!.obtainMessage(MSG_START_HOME, fromPage), waitTime)
        }
    }

    /**
     * 初始化语言
     */
    private fun initLanguage() {
        try {
            val mLanguageKey = SharedPrefUtils.getString(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.LANGUAGE_SETTING, "")//获取缓存语言，userid+_+key
            if (TextUtils.isEmpty(mLanguageKey)) {
                return
            }
            val conf = Configuration(resources.configuration)
            val dm = resources.displayMetrics
            if (mLanguageKey == TheLConstants.LANGUAGE_ZH_CN) {//简体中文
                conf.locale = Locale.SIMPLIFIED_CHINESE
            } else if (mLanguageKey == TheLConstants.LANGUAGE_ZH_TW) {//繁体中文 (台湾)
                conf.locale = Locale.TAIWAN
            } else if (mLanguageKey == TheLConstants.LANGUAGE_ZH_HK) {//繁体中文（香港）
                conf.locale = Locale.TRADITIONAL_CHINESE
            } else if (mLanguageKey == TheLConstants.LANGUAGE_EN_US) {//英文（美）
                conf.locale = Locale.US
            } else if (mLanguageKey == TheLConstants.LANGUAGE_FR_RFR) {//法语（法国）
                conf.locale = Locale.FRANCE
            } else if (mLanguageKey == TheLConstants.LANGUAGE_TH_RTH) {//泰语
                conf.locale = Locale("th", "TH")
            } else if (mLanguageKey == TheLConstants.LANGUAGE_ES) {//西班牙语
                conf.locale = Locale("es")
            } else if (mLanguageKey == TheLConstants.LANGUAGE_JA_JP) {//4.1.4 新增日语
                conf.locale = Locale.JAPAN
            }
            resources.updateConfiguration(conf, dm)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

}