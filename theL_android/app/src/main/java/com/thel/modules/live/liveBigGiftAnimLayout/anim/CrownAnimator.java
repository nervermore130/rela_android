package com.thel.modules.live.liveBigGiftAnimLayout.anim;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.BounceInterpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.thel.R;
import com.thel.utils.Utils;

/**
 * 皇冠的动画
 * created by Setsail on 2016.6.3
 */
public class CrownAnimator {
    private final String foldPath;
    private final String crownRes;
    private final String shineRes;
    private Handler mHandler;
    private int duration;
    private Context mContext;
    private int crownSize;
    private int lightSize;
    private int lightMargin;
    private int mHeight;

    public CrownAnimator(Context context, int d) {
        mContext = context;
        crownSize = Utils.dip2px(context, 150);
        lightSize = Utils.dip2px(context, 350);
        lightMargin = Utils.dip2px(context, 50);
        duration = d;
        mHandler = new Handler(Looper.getMainLooper());
        foldPath = "anim/crown";
        crownRes = "icn_animate_crown";
        shineRes = "icn_animate_crown_shine";
    }

    public int start(final ViewGroup parent) {
        mHeight = parent.getMeasuredHeight();
        final RelativeLayout backgound = new RelativeLayout(mContext);
        parent.addView(backgound, RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        showCrown(backgound);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                backgound.clearAnimation();
                parent.removeView(backgound);
            }
        }, duration);
        return duration;
    }

    private void showCrown(final ViewGroup parent) {
        final LinearLayout lin_light = new LinearLayout(mContext);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.addRule(RelativeLayout.CENTER_HORIZONTAL);
        lp.topMargin = (mHeight - lightSize) / 2;
        lin_light.setLayoutParams(lp);
        final ImageView light = new ImageView(mContext);
        LinearLayout.LayoutParams layoutParamsLight = new LinearLayout.LayoutParams(lightSize, lightSize);
        layoutParamsLight.topMargin = lightMargin / 2;
        light.setLayoutParams(layoutParamsLight);
        light.setImageResource(R.mipmap.icn_animate_crown_shine);
        // LiveBigAnimUtils.setAssetImage(light, foldPath, shineRes);
        lin_light.addView(light);
        parent.addView(lin_light);
        showLight(light);

        final ImageView child = new ImageView(mContext);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(crownSize, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        child.setLayoutParams(layoutParams);
        child.setImageResource(R.mipmap.icn_animate_crown);
        //LiveBigAnimUtils.setAssetImage(child, foldPath, crownRes);
        child.setAdjustViewBounds(true);
        parent.addView(child);

        ScaleAnimation scaleAnimation = new ScaleAnimation(0f, 1f, 0f, 1f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        scaleAnimation.setDuration(1200);
        scaleAnimation.setInterpolator(new MyBounceInterpolator());
        scaleAnimation.setFillAfter(true);
        child.startAnimation(scaleAnimation);

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                child.clearAnimation();
                parent.removeView(child);
                light.clearAnimation();
                parent.removeView(lin_light);
            }
        }, duration);
    }

    private void showLight(final ImageView light) {
        RotateAnimation rotateAnimation = new RotateAnimation(0, 359, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotateAnimation.setInterpolator(new LinearInterpolator());
        rotateAnimation.setDuration(2000);
        rotateAnimation.setRepeatCount(Animation.INFINITE);
        light.startAnimation(rotateAnimation);
    }

    private class MyBounceInterpolator extends BounceInterpolator {
        public MyBounceInterpolator() {
        }

        @SuppressWarnings({"UnusedDeclaration"})
        public MyBounceInterpolator(Context context, AttributeSet attrs) {
        }

        private float bounce(float t) {
            return t * t * 8.0f;
        }

        private float finalSize;

        public float getInterpolation(float t) {
            t *= 1.1226f;
            if (t < 0.3535f)
                return bounce(t);
            else if (t < 0.7408f) {
                finalSize = bounce(t - 0.54719f) + 0.8f;
                return finalSize;
            } else
                return finalSize;
        }

    }
}

