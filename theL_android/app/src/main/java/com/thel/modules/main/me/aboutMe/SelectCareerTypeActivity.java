package com.thel.modules.main.me.aboutMe;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.constants.TheLConstants;
import com.thel.modules.main.me.adapter.SelectCareerTypeAdapter;

/**
 * 选择职业类型
 * Created by lingwei on 2017/10/13.
 */

public class SelectCareerTypeActivity extends BaseActivity {

    private LinearLayout lin_done;
    private ImageView img_done;

    // 大类列表
    private ListView list_father;
    // 小类列表
    private ListView list_child;
    private SelectCareerTypeAdapter adapterFather;
    private SelectCareerTypeAdapter adapterChild;
    private String[] arr_father;
    private String[] arr_tech;
    private String[] arr_culture;
    private String[] arr_entertainment;
    private String[] arr_economy;
    private String[] arr_manufacturing;
    private String[] arr_transport;
    private String[] arr_service;
    private String[] arr_utilities;
    private String[] arr_student;
    private String[] arr_other;
    private String[] arr_do_not_show;
    private int selectedFather = -1;
    private int selectedChild = -1;
    private String[] curChild;
    private LinearLayout lin_back;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_career_type_activity);
        findViewById();
        setDoneButtonEnabled(false);
        initData();
        adapterFather = new SelectCareerTypeAdapter(arr_father, 1);
        adapterChild = new SelectCareerTypeAdapter(new String[]{}, 2);
        list_father.setAdapter(adapterFather);
        list_child.setAdapter(adapterChild);
        setLinsenter();
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void setLinsenter() {
        lin_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("selectedPos", selectedFather + "," + selectedChild);
                intent.putExtra("selectedCareer", curChild[selectedChild]);
                setResult(TheLConstants.RESULT_CODE_SELECT_CAREER_TYPE, intent);
                finish();
            }
        });
        list_father.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                if (position == selectedFather) {
                    return;
                }
                switch (position) {
                    case 0:
                        adapterChild.refreshAdapter(arr_tech);
                        curChild = arr_tech;
                        break;
                    case 1:
                        adapterChild.refreshAdapter(arr_culture);
                        curChild = arr_culture;
                        break;
                    case 2:
                        adapterChild.refreshAdapter(arr_entertainment);
                        curChild = arr_entertainment;
                        break;
                    case 3:
                        adapterChild.refreshAdapter(arr_economy);
                        curChild = arr_economy;
                        break;
                    case 4:
                        adapterChild.refreshAdapter(arr_manufacturing);
                        curChild = arr_manufacturing;
                        break;
                    case 5:
                        adapterChild.refreshAdapter(arr_transport);
                        curChild = arr_transport;
                        break;
                    case 6:
                        adapterChild.refreshAdapter(arr_service);
                        curChild = arr_service;
                        break;
                    case 7:
                        adapterChild.refreshAdapter(arr_utilities);
                        curChild = arr_utilities;
                        break;
                    case 8:
                        adapterChild.refreshAdapter(arr_student);
                        curChild = arr_student;
                        break;
                    case 9:
                        adapterChild.refreshAdapter(arr_other);
                        curChild = arr_other;
                        break;
                    case 10:
                        adapterChild.refreshAdapter(arr_do_not_show);
                        curChild = arr_do_not_show;
                        break;
                    default:
                        break;
                }
                adapterFather.refreshSelectedPos(position);
                adapterChild.refreshSelectedPos(-1);
                selectedFather = position;
                selectedChild = -1;
                setDoneButtonEnabled(false);
            }
        });
        list_child.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                if (position == selectedChild) {
                    return;
                }
                adapterChild.refreshSelectedPos(position);
                selectedChild = position;
                setDoneButtonEnabled(true);
            }
        });
        lin_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void findViewById() {
        list_father = findViewById(R.id.list_father);
        list_child = findViewById(R.id.list_child);
        lin_done = findViewById(R.id.lin_done);
        img_done = lin_done.findViewById(R.id.img_done);
        lin_back = findViewById(R.id.lin_back);
    }

    private void initData() {
        arr_father = this.getResources().getStringArray(R.array.career_type_father);
        arr_tech = this.getResources().getStringArray(R.array.career_type_tech);
        arr_culture = this.getResources().getStringArray(R.array.career_type_culture);
        arr_entertainment = this.getResources().getStringArray(R.array.career_type_entertainment);
        arr_economy = this.getResources().getStringArray(R.array.career_type_economy);
        arr_manufacturing = this.getResources().getStringArray(R.array.career_type_manufacturing);
        arr_transport = this.getResources().getStringArray(R.array.career_type_transport);
        arr_service = this.getResources().getStringArray(R.array.career_type_service);
        arr_utilities = this.getResources().getStringArray(R.array.career_type_utilities);
        arr_student = this.getResources().getStringArray(R.array.career_type_student);
        arr_other = new String[]{TheLApp.getContext().getString(R.string.info_other)};
        arr_do_not_show = new String[]{TheLApp.getContext().getString(R.string.info_do_not_show)};
    }

    private void setDoneButtonEnabled(boolean enabled) {
        if (enabled) {
            img_done.setImageResource(R.mipmap.btn_done);
            lin_done.setEnabled(true);
        } else {
            img_done.setImageResource(R.mipmap.btn_done_disable);
            lin_done.setEnabled(false);
        }
    }
}
