package com.thel.modules.main.me.aboutMe;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.appevents.AppEventsLogger;
import com.meituan.android.walle.WalleChannelReader;
import com.rela.pay.OnPayStatusListener;
import com.rela.pay.PayConstants;
import com.rela.pay.PayProxyImpl;
import com.thel.BuildConfig;
import com.thel.R;
import com.thel.android.pay.IabHelper;
import com.thel.android.pay.IabResult;
import com.thel.android.pay.Inventory;
import com.thel.android.pay.Purchase;
import com.thel.android.pay.SkuDetails;
import com.thel.android.pay.SyncOrdersService;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.base.IntentFilterActivity;
import com.thel.bean.GoogleIapNotifyResultBean;
import com.thel.bean.PayOrderBean;
import com.thel.constants.BundleConstants;
import com.thel.constants.TheLConstants;
import com.thel.db.MomentsDataBaseAdapter;
import com.thel.growingio.GIoGiftTrackBean;
import com.thel.growingio.GrowingIoConstant;
import com.thel.imp.vip.VipUtils;
import com.thel.modules.main.home.moments.web.WebViewActivity;
import com.thel.modules.main.me.adapter.BuyVipListAdapter;
import com.thel.modules.main.me.bean.LogInfoBean;
import com.thel.modules.main.me.bean.VipBean;
import com.thel.modules.main.me.bean.VipListBean;
import com.thel.modules.main.me.match.eventcollect.collect.LiveLogUtils;
import com.thel.modules.main.me.match.eventcollect.collect.MatchLogUtils;
import com.thel.modules.main.me.match.eventcollect.collect.PayLogUtils;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.RequestConstants;
import com.thel.ui.decoration.DefaultItemDivider;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.utils.DialogUtil;
import com.thel.utils.FireBaseUtils;
import com.thel.utils.GooglePayUtils;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.GsonUtils;
import com.thel.utils.JsonUtils;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * 购买会员页
 * Created by lingwei on 2017/9/28.
 */

public class BuyVipActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.listView)
    RecyclerView listView;

    @BindView(R.id.lin_back)
    LinearLayout lin_back;

    @BindView(R.id.txt_reload)
    TextView txt_reload;

    @BindView(R.id.txt_pay_qa)
    TextView txt_pay_qa;

    @BindView(R.id.lin_reload)
    LinearLayout lin_reload;

    @BindView(R.id.img_reload)
    ImageView img_reload;

    private PayResultReceiver payResultReceiver;

    private boolean hasBoughtVip = false;

    private boolean hasLoadedData = false;

    private boolean iabSetuped = false;

    private Integer pos = -1;

    private double normalPrice = 1;//正常价格

    private String price_currency_code = "";//货币参数

    private ArrayList<VipBean> listPlus = new ArrayList<>();

    private IabHelper mHelper;

    private BuyVipListAdapter adapter;
    public VipListBean mVipListBean;
    public String selectPrice;

    private String from_page_id;
    private String from_page;
    private String fromPage;  //表情商店 表情详情
    private String fromPageId;

    //购买会员结果
    public void traceBuyVipData(String page, String activity, String pay_type, String toast) {
        String oder_id = ShareFileUtils.getString(ShareFileUtils.BUY_VIP_ODER_ID, "");
        LogInfoBean logInfoBean = new LogInfoBean();
        logInfoBean.page = page;
        logInfoBean.page_id = pageId;
        logInfoBean.activity = activity;
        logInfoBean.from_page = from_page;
        logInfoBean.from_page_id = from_page_id;
        logInfoBean.lat = latitude;
        logInfoBean.lng = longitude;

        LogInfoBean.LogsDataBean logsDataBean = new LogInfoBean.LogsDataBean();
        logsDataBean.amount = selectPrice;
        logsDataBean.pay_type = pay_type;
        logsDataBean.order_id = oder_id;
        logsDataBean.toast = toast;
        logInfoBean.data = logsDataBean;

        MatchLogUtils.getInstance().addLog(logInfoBean);


    }

    //发送请求购买会员
    public void traceRequestBuyVip(String page, String activity) {

        LogInfoBean logInfoBean = new LogInfoBean();
        logInfoBean.page = page;
        logInfoBean.page_id = pageId;
        logInfoBean.activity = activity;
        logInfoBean.from_page = from_page;
        logInfoBean.from_page_id = from_page_id;
        logInfoBean.lat = latitude;
        logInfoBean.lng = longitude;

        MatchLogUtils.getInstance().addLog(logInfoBean);


    }

    private String latitude;
    private String longitude;
    private String pageId;

    public void huaweiChannelTest(View view) {
        getVipList("huawei");
    }

    public void vivoChannelTest(View view) {
        getVipList("vivo");
    }

    private class PayConsumer extends InterceptorSubscribe<PayOrderBean> {

        private String payType;

        public PayConsumer(String payType) {
            this.payType = payType;
        }

        @Override
        public void onNext(PayOrderBean result) {
            super.onNext(result);
            closeLoading();
            MobclickAgent.onEvent(BuyVipActivity.this, "create_order_succeed");
            if (PayConstants.PAY_TYPE_ALIPAY.equals(payType)) {
                MobclickAgent.onEvent(BuyVipActivity.this, "start_alipay");

                final GIoGiftTrackBean payTrackBean = new GIoGiftTrackBean(GrowingIoConstant.MEMBERSHIP_PURCHASE, result.data.outTradeNo, result.data.subject, PayConstants.PAY_TYPE_ALIPAY);
                GrowingIOUtil.payGiftTrack(payTrackBean);

            } else if (PayConstants.PAY_TYPE_WXPAY.equals(payType)) {
                MobclickAgent.onEvent(BuyVipActivity.this, "start_wx_pay");


                final GIoGiftTrackBean payTrackBean = new GIoGiftTrackBean(GrowingIoConstant.MEMBERSHIP_PURCHASE, result.data.outTradeNo, result.data.subject, PayConstants.PAY_TYPE_WXPAY);
                GrowingIOUtil.payGiftTrack(payTrackBean);

            } else if (GooglePayUtils.GOOGLEPAY.equals(payType)) {
                if (iabSetuped && mHelper != null) {
                    final String sku = result.data.iapId;
                    buyItem(sku, result.data.outTradeNo, result.data.totalFee);
                    final GIoGiftTrackBean payTrackBean = new GIoGiftTrackBean(GrowingIoConstant.MEMBERSHIP_PURCHASE, result.data.outTradeNo, result.data.subject, GooglePayUtils.GOOGLEPAY);
                    GrowingIOUtil.payGiftTrack(payTrackBean);

                }
            }
            ShareFileUtils.setString(ShareFileUtils.BUY_VIP_ODER_ID, result.data.outTradeNo);

        }
    }

    private class IapNotifyConsumer extends InterceptorSubscribe<GoogleIapNotifyResultBean> {

        private String outTradeNo;

        public IapNotifyConsumer(String outTradeNo) {
            this.outTradeNo = outTradeNo;
        }

        @Override
        public void onNext(GoogleIapNotifyResultBean data) {
            super.onNext(data);
            if (GooglePayUtils.getInstance().isNotified(data, true)) {
                final Purchase purchase = MomentsDataBaseAdapter.getInstance(TheLApp.getContext(), ShareFileUtils.getString(ShareFileUtils.ID, "")).getInAppOrder(outTradeNo);
                mHelper.consumeAsync(purchase, new IabHelper.OnConsumeFinishedListener() {
                    @Override
                    public void onConsumeFinished(Purchase purchase, IabResult result) {

                    }
                });
                MomentsDataBaseAdapter.getInstance(TheLApp.getContext(), ShareFileUtils.getString(ShareFileUtils.ID, "")).deleteInAppOrder(outTradeNo);
                closeLoading();

                L.d("showWhoSeenMeData", " onNext hasBoughtVip : " + hasBoughtVip);

                hasBoughtVip = true;
                DialogUtil.showToastShort(TheLApp.getContext(), getThelString(R.string.buy_vip_succeed));
                finish();
                traceBuyVipData("vip.buy", "success", "googleplay", "");

            } else {
                closeLoading();
                notifyFailed(outTradeNo);
                traceBuyVipData("vip.buy", "fail", "googleplay", getThelString(R.string.synchronize_order_failed));

            }

        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.buy_vip_activity);

        ButterKnife.bind(this);

        getIntentData();
        registerReceiver();

        initView();
        initData();
        pageId = Utils.getPageId();
        //开发版本测试专用
        if (BuildConfig.DEBUG && !L.IS_USE_BAIDU_TEST) {
            findViewById(R.id.channelTest).setVisibility(View.VISIBLE);
        }
        traceBuyVipLog();

        ///5.0.4上报埋点需求
    }

    private void getIntentData() {
        Intent intent = getIntent();
        fromPage = intent.getStringExtra("fromPage");
        fromPageId = intent.getStringExtra("pageId");
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            from_page_id = bundle.getString(ShareFileUtils.MATCH_FROM_PAGE_ID, "");
            from_page = bundle.getString(ShareFileUtils.MATCH_FROM_PAGE, "");

        }

        boolean isVip = intent.getBooleanExtra("isVip", false);
        if (isVip) {
            ((TextView) findViewById(R.id.txt_title)).setText(getThelString(R.string.vip_config_act_title));

        } else {
            ((TextView) findViewById(R.id.txt_title)).setText(getThelString(R.string.buy_vip));

        }
    }

    private void initView() {
        findViewById(R.id.img_more).setVisibility(View.GONE);

        lin_back.setOnClickListener(this);
        txt_pay_qa.setOnClickListener(this);
        img_reload.setOnClickListener(this);
        txt_pay_qa.setText(R.string.pay_qa1);
        listView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        listView.addItemDecoration(new DefaultItemDivider(this, RecyclerView.VERTICAL, R.color.gray, 1, false, true));

        FireBaseUtils.uploadGoogle(TheLConstants.FireBaseConstant.VIEW_ITEM_PURCHASE_DETAILS,this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(this.getClass().getName());
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(this.getClass().getName());
        MobclickAgent.onPause(this);
    }

    private void initData() {
        processBusiness();
        latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
        longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");
    }

    // 注册广播接收器
    private void registerReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(TheLConstants.BROADCAST_PAY);
        payResultReceiver = new PayResultReceiver();
        registerReceiver(payResultReceiver, intentFilter);
    }

    protected void processBusiness() {
        showLoading();
        /**
         * 4.10.0做abtest用来区分是vivo的新充值用户就让它显示新充值的页面
         *
         * **/
        String channel = WalleChannelReader.getChannel(getApplication());
        if (TextUtils.isEmpty(channel) || (!TextUtils.isEmpty(channel) && !channel.equals("vivo") && !channel.equals("huawei"))) {
            channel = "";
        }
        getVipList(channel);
    }

    private void getVipList(String channel) {
        RequestBusiness.getInstance().getBuyVipList(channel).observeOn(AndroidSchedulers.mainThread()).onBackpressureDrop().subscribeOn(Schedulers.io()).subscribe(new InterceptorSubscribe<VipListBean>() {
            @Override
            public void onNext(VipListBean result) {
                super.onNext(result);
                hasLoadedData = true;
                listPlus.clear();
                listPlus.addAll(result.data.list);
                if (RequestConstants.APPLICATION_ID_GLOBAL.equals(BuildConfig.APPLICATION_ID)) {
                    final List<String> skuIds = new ArrayList<>();
                    for (VipBean vipBean : listPlus) {
                        vipBean.isPendingPrice = true;
                        skuIds.add(vipBean.iapId);
                    }
                    refreshUi(result.data.channel);
                    setupIap(skuIds);
                } else
                    requestFinished(result.data.channel);
            }
        });
    }

    private void requestFinished(String channel) {
        closeLoading();
        refreshUi(channel);
    }

    private void setupIap(final List<String> skuIds) {
        if (mHelper == null) {
            mHelper = new IabHelper(TheLApp.getContext(), GooglePayUtils.KEY);
            mHelper.enableDebugLogging(true);
        }
        if (!iabSetuped)
            mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
                @Override
                public void onIabSetupFinished(IabResult result) {
                    iabSetuped = result.isSuccess();
                    if (iabSetuped && mHelper != null) {
                        queryItems(skuIds);
                    } else {
                        loadFailed(result.getResponse() + "");
                        DialogUtil.showToastShort(BuyVipActivity.this, getString(R.string.google_play_connect_error));
                    }
                }
            });
        else
            queryItems(skuIds);
    }

    private void getNormalPrice() {
        if (normalPrice == 1) {
            for (VipBean bean : listPlus) {
                if (bean.months == 1) {
                    normalPrice = Double.valueOf(bean.price);
                    return;
                }
            }
        }
    }

    private void refreshUi(String channel) {
        if (hasLoadedData) {
            getNormalPrice();
            lin_reload.setVisibility(View.GONE);
                adapter = new BuyVipListAdapter(listPlus, normalPrice);
                adapter.setChannel(channel);
                listView.setAdapter(adapter);
                adapter.setOnRecyclerViewItemChildClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemChildClickListener() {
                    @Override
                    public void onItemChildClick(BaseRecyclerViewAdapter adapter, View view, int position) {
                        switch (view.getId()) {
                            case R.id.rl_operation:
                                pos = (int) view.getTag() - adapter.getHeaderLayoutCount();
                                if (pos >= 0) {
                                    operate(pos);
                                }
                                break;
                        }
                    }
                });

        } else {
            loadFailed("");
        }
    }

    private void operate(final int pos) {
        if (RequestConstants.APPLICATION_ID_LOCAL.equals(BuildConfig.APPLICATION_ID))
            DialogUtil.getInstance().showSelectionDialogWithIcon(this, getString(R.string.pay_options), new String[]{getString(R.string.pay_alipay), getString(R.string.pay_wx)}, new int[]{R.mipmap.icon_zhifubao, R.mipmap.icon_wechat}, new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                    DialogUtil.getInstance().closeDialog();
                    MobclickAgent.onEvent(TheLApp.getContext(), "click_price_button");

                    traceRequestBuyVip("vip.buy", listPlus.get(pos).iapId);
                    selectPrice = listPlus.get(pos).price;
                    switch (position) {
                        case 0:// 支付宝
                            showLoadingNoBack();
                            MobclickAgent.onEvent(BuyVipActivity.this, "start_alipay");

                            RequestBusiness.getInstance().createStickerPackOrder(TheLConstants.PRODUCT_TYPE_VIP, listPlus.get(pos).id + "", PayConstants.PAY_TYPE_ALIPAY, PayConstants.ALIPAY_SOURCE).observeOn(AndroidSchedulers.mainThread()).onBackpressureDrop().subscribeOn(Schedulers.io()).subscribe(new InterceptorSubscribe<PayOrderBean>() {
                                @Override
                                public void onNext(PayOrderBean data) {
                                    super.onNext(data);

                                    final GIoGiftTrackBean payTrackBean = new GIoGiftTrackBean(GrowingIoConstant.MEMBERSHIP_PURCHASE, data.data.outTradeNo, data.data.subject, PayConstants.PAY_TYPE_ALIPAY);
                                    GrowingIOUtil.payGiftTrack(payTrackBean);

                                    PayProxyImpl.getInstance().pay(PayConstants.PAY_TYPE_ALIPAY, BuyVipActivity.this, GsonUtils.createJsonString(data.data), new OnPayStatusListener() {
                                        @Override
                                        public void onPayStatus(int payStatus, String errorInfo) {

                                            if (payStatus == 1) {
                                                MobclickAgent.onEvent(BuyVipActivity.this, "vip_pay_succeed");// 购买vip支付成功事件统计
                                                hasBoughtVip = true;
                                                DialogUtil.showToastShort(TheLApp.getContext(), getThelString(R.string.buy_vip_succeed));
                                                finish();
                                                traceBuyVipData("vip.buy", "success", "alipay", "");

                                            } else {
                                                if (TextUtils.equals(errorInfo, "8000")) {

                                                } else {
                                                    // 其他值就可以判断为支付失败，包括用户主动取消支付，或者系统返回的错误
                                                    if (TextUtils.equals(errorInfo, "6001")) {
                                                        traceBuyVipData("vip.buy", "fail", "alipay", getThelString(R.string.pay_canceled));
                                                    } else {
                                                        traceBuyVipData("vip.buy", "fail", "alipay", getThelString(R.string.pay_failed));
                                                    }
                                                    closeLoading();
                                                }
                                            }
                                        }

                                    });

                                }
                            });

                            break;
                        case 1: // 微信支付
                            showLoadingNoBack();
                            MobclickAgent.onEvent(BuyVipActivity.this, "start_wx_pay");

                            RequestBusiness.getInstance().createStickerPackOrder(TheLConstants.PRODUCT_TYPE_VIP, listPlus.get(pos).id + "", PayConstants.PAY_TYPE_WXPAY, PayConstants.WXPAY_SOURCE).observeOn(AndroidSchedulers.mainThread()).onBackpressureDrop().subscribeOn(Schedulers.io()).subscribe(new InterceptorSubscribe<PayOrderBean>() {
                                @Override
                                public void onNext(PayOrderBean data) {
                                    super.onNext(data);

                                    final GIoGiftTrackBean payTrackBean = new GIoGiftTrackBean(GrowingIoConstant.MEMBERSHIP_PURCHASE, data.data.outTradeNo, data.data.subject, PayConstants.PAY_TYPE_WXPAY);
                                    GrowingIOUtil.payGiftTrack(payTrackBean);

                                    PayProxyImpl.getInstance().pay(PayConstants.PAY_TYPE_WXPAY, BuyVipActivity.this, GsonUtils.createJsonString(data.data), new OnPayStatusListener() {
                                        @Override
                                        public void onPayStatus(int payStatus, String errorInfo) {

                                            if (payStatus == 1) {
                                                MobclickAgent.onEvent(BuyVipActivity.this, "vip_pay_succeed");// 购买vip支付成功事件统计

                                                hasBoughtVip = true;
                                                DialogUtil.showToastShort(TheLApp.getContext(), getString(R.string.buy_vip_succeed));
                                                finish();
                                                traceBuyVipData("vip.buy", "success", "wxpay", "");

                                            } else {
                                                ViewUtils.hideSoftInput(BuyVipActivity.this);
                                                traceBuyVipData("vip.buy", "fail", "wxpay", "");
                                                closeLoading();
                                            }

                                        }
                                    });

                                }
                            });

                            break;
                        default:
                            break;
                    }
                }
            }, false, null);
        else {// 国际版，用google wallet
            traceRequestBuyVip("vip.buy", listPlus.get(pos).iapId);
            selectPrice = listPlus.get(pos).price;

            showLoadingNoBack();
            RequestBusiness.getInstance().createStickerPackOrder(TheLConstants.PRODUCT_TYPE_VIP, listPlus.get(pos).id + "", GooglePayUtils.GOOGLEPAY, GooglePayUtils.GOOGLEPAY_SOURCE).onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new PayConsumer(GooglePayUtils.GOOGLEPAY));
        }

    }

    private void queryItems(final List<String> skuIds) {
        mHelper.queryInventoryAsync(true, skuIds, new IabHelper.QueryInventoryFinishedListener() {
            @Override
            public void onQueryInventoryFinished(IabResult result, Inventory inv) {
                if (result.isSuccess()) {
                    closeLoading();
                    for (VipBean vipBean : listPlus) {
                        SkuDetails skuDetails = inv.getSkuDetails(vipBean.iapId);
                        if (skuDetails != null) {
                            vipBean.isPendingPrice = false;
                            vipBean.googlePrice = skuDetails.getPrice();
                            try {
                                if (TextUtils.isEmpty(price_currency_code)) {//获取货币参数
                                    price_currency_code = JsonUtils.getString(new JSONObject(skuDetails.getJson()), "price_currency_code", "");
                                }
                                // google返回的price_amount_micros是*1000000，我们在这里只/10000，因为后面会再/100（为了跟国内版逻辑统一）
                                vipBean.price = JsonUtils.getLong(new JSONObject(skuDetails.getJson()), "price_amount_micros", 0) / 10000d + "";
                                if (vipBean.months == 1) {
                                    normalPrice = Double.valueOf(vipBean.price);
                                }
                            } catch (JSONException e) {
                                loadFailed("");
                                return;
                            }
                        }
                    }
                    refreshUi("");
                } else {
                    loadFailed(result.getResponse() + "");
                }
            }
        });
    }

    private void loadFailed(String googleError) {
        closeLoading();
        if (!TextUtils.isEmpty(googleError))
            DialogUtil.showToastShort(BuyVipActivity.this, getThelString(R.string.google_play_connect_error) + "(" + googleError + ")");
        try {
            lin_reload.setVisibility(View.VISIBLE);
            img_reload.clearAnimation();
            txt_reload.setText(getString(R.string.info_reload));
            if (adapter != null) {
                listPlus.clear();
                adapter.notifyDataSetChanged();
            }
        } catch (Exception e) {
        }
    }

    private void buyItem(final String sku, final String outTradeNo, final double totalFee) {

        mHelper.handleActivityResult(TheLConstants.REQUEST_CODE_ANDROID_PAY, -1, null);

        mHelper.launchPurchaseFlow(BuyVipActivity.this, sku, TheLConstants.REQUEST_CODE_ANDROID_PAY, new IabHelper.OnIabPurchaseFinishedListener() {
            @Override
            public void onIabPurchaseFinished(IabResult result, Purchase info) {
                showLoadingNoBack();
                if (result.isSuccess()) {
                    onPaid(info);
                    AppEventsLogger logger = AppEventsLogger.newLogger(BuyVipActivity.this);
                    if (logger != null && !TextUtils.isEmpty(price_currency_code)) {//facebook记录购买成功
                        logger.logPurchase(BigDecimal.valueOf(totalFee), Currency.getInstance(price_currency_code));
                    }
                } else {
                    if (IabHelper.IABHELPER_USER_CANCELLED == result.getResponse()) {
                        closeLoading();
                        DialogUtil.showToastShort(BuyVipActivity.this, getThelString(R.string.pay_canceled));
                    } else if (IabHelper.BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED == result.getResponse()) { // 已购买过该商品，但是没有消耗掉，此时需要查询本地是否有未同步的订单
                        List<String> skus = new ArrayList<>();
                        skus.add(sku);
                        mHelper.queryInventoryAsync(true, skus, new IabHelper.QueryInventoryFinishedListener() {
                            @Override
                            public void onQueryInventoryFinished(IabResult result, Inventory inv) {
                                if (result.isSuccess())
                                    mHelper.consumeAsync(inv.getPurchase(sku), new IabHelper.OnConsumeFinishedListener() {
                                        @Override
                                        public void onConsumeFinished(Purchase purchase, IabResult result) {
                                            closeLoading();
                                            if (result.isSuccess()) {
                                                buyItem(sku, outTradeNo, totalFee);
                                            } else {
                                                DialogUtil.showToastShort(BuyVipActivity.this, getThelString(R.string.google_play_connect_error));
                                            }
                                        }
                                    });
                                else {
                                    closeLoading();
                                    DialogUtil.showToastShort(BuyVipActivity.this, getThelString(R.string.google_play_connect_error) + "(" + result.getResponse() + ")");
                                }
                            }
                        });
                    } else {
                        closeLoading();
                        DialogUtil.showToastShort(BuyVipActivity.this, getThelString(R.string.pay_failed) + "(" + result.getResponse() + ")");
                    }
                }
            }
        }, outTradeNo);
    }

    private void onPaid(Purchase purchase) {
        MomentsDataBaseAdapter.getInstance(TheLApp.getContext(), ShareFileUtils.getString(ShareFileUtils.ID, "")).saveInAppOrder(purchase);
        RequestBusiness.getInstance().iapNotify(purchase.getDeveloperPayload(), purchase.getOriginalJson(), purchase.getSignature()).observeOn(AndroidSchedulers.mainThread()).onBackpressureDrop().subscribeOn(Schedulers.io()).subscribe(new IapNotifyConsumer(purchase.getDeveloperPayload()));
    }

    private void notifyFailed(final String outTradeNo) {
        DialogUtil.showConfirmDialog(this, "", getString(R.string.synchronize_order_failed), getString(R.string.retry), getString(R.string.info_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Purchase pur = MomentsDataBaseAdapter.getInstance(TheLApp.getContext(), ShareFileUtils.getString(ShareFileUtils.ID, "")).getInAppOrder(outTradeNo);
                if (pur != null) {
                    showLoadingNoBack();
                    RequestBusiness.getInstance().iapNotify(pur.getDeveloperPayload(), pur.getOriginalJson(), pur.getSignature()).observeOn(AndroidSchedulers.mainThread()).onBackpressureDrop().subscribeOn(Schedulers.io()).subscribe(new IapNotifyConsumer(pur.getDeveloperPayload()));
                } else {
                    startSyncOrders();
                }
            }
        }, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                startSyncOrders();
            }
        });
    }

    private void startSyncOrders() {
        SyncOrdersService.startSyncService(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_reload:
                Animation anim = AnimationUtils.loadAnimation(TheLApp.getContext(), R.anim.rotate);
                LinearInterpolator lir = new LinearInterpolator();
                anim.setInterpolator(lir);
                v.setLayerType(View.LAYER_TYPE_HARDWARE, null);
                v.startAnimation(anim);
                txt_reload.setText(getString(R.string.info_reloading));
                processBusiness();
                break;
            case R.id.txt_pay_qa:
                ViewUtils.preventViewMultipleClick(v, 1000);
                Intent intent = new Intent(BuyVipActivity.this, WebViewActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(BundleConstants.URL, TheLConstants.BUY_SOFT_MONEY_HELP_PAGE_URL);
                bundle.putBoolean(WebViewActivity.NEED_SECURITY_CHECK, false);
                intent.putExtras(bundle);
                startActivity(intent);
                traceQuestionBuyVip("vip.buy", "click_question");
                break;
            case R.id.lin_back:
                BuyVipActivity.this.finish();
                break;
        }
    }

    //购买遇到问题点击
    public void traceQuestionBuyVip(String page, String activity) {

        LogInfoBean logInfoBean = new LogInfoBean();
        logInfoBean.page = page;
        logInfoBean.page_id = pageId;
        logInfoBean.activity = activity;
        logInfoBean.from_page = from_page;
        logInfoBean.from_page_id = from_page_id;
        logInfoBean.lat = latitude;
        logInfoBean.lng = longitude;
        LogInfoBean.LogsDataBean logsDataBean = logInfoBean.data;
        if (logsDataBean == null) {
            logsDataBean = new LogInfoBean.LogsDataBean();
        }
        logsDataBean.time = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS").format(System.currentTimeMillis()) + "";

        MatchLogUtils.getInstance().addLog(logInfoBean);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mHelper != null && mHelper.handleActivityResult(requestCode, resultCode, data)) {
            return;
        } else
            super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void finish() {

        L.d("showWhoSeenMeData", " hasBoughtVip : " + hasBoughtVip);

        if (hasBoughtVip) {
            setResult(RESULT_OK);
            VipUtils.hasBoughtVip();
        }
        super.finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(payResultReceiver);
        } catch (IllegalArgumentException e) {
        }

        if (mHelper != null)
            mHelper.dispose();
        mHelper = null;
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    // 微信支付成功接收器
    private class PayResultReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                String result = intent.getStringExtra("result");
                if (TextUtils.equals(result, "succeed")) {
                    MobclickAgent.onEvent(BuyVipActivity.this, "vip_pay_succeed");// 购买vip支付成功事件统计

                    L.d("showWhoSeenMeData", " onReceive hasBoughtVip : " + hasBoughtVip);

                    hasBoughtVip = true;
                    DialogUtil.showToastShort(TheLApp.getContext(), getString(R.string.buy_vip_succeed));
                    finish();
                    traceBuyVipData("vip.buy", "success", "wxpay", "");

                } else if (TextUtils.equals(result, "fail")) {
                    ViewUtils.hideSoftInput(BuyVipActivity.this);
                    traceBuyVipData("vip.buy", "fail", "wxpay", "");

                }
            }
        }
    }

    public void traceBuyVipLog() {
        try {
            if (!TextUtils.isEmpty(fromPage) && !TextUtils.isEmpty(fromPageId)) {
                LogInfoBean logInfoBean = new LogInfoBean();
                logInfoBean.page = "vip.buy";
                logInfoBean.page_id = pageId;
                logInfoBean.from_page = fromPage;
                logInfoBean.from_page_id = fromPageId;
                MatchLogUtils.getInstance().addLog(logInfoBean);

            }

        } catch (Exception e) {

        }
    }
}
