package com.thel.modules.live.bean;

import com.thel.base.BaseDataBean;

import java.util.ArrayList;
import java.util.List;

public class LiveTopFansTodayBean extends BaseDataBean {

    public TopDataBean data;

    public class TopDataBean {
        public List<LiveTopFansBean> topFans = new ArrayList<>();

    }
}
