package com.thel.modules.main.userinfo.moment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.thel.bean.moments.MomentsListBean;
import com.thel.constants.TheLConstants;
import com.thel.modules.main.home.follow.FollowPresenter;
import com.thel.modules.main.userinfo.UserInfoContract;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestConstants;
import com.thel.network.service.DefaultRequestService;
import com.thel.utils.L;
import com.thel.utils.Utils;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by waiarl on 2018/4/17.
 */

public class UserInfoMomentPresenter implements UserInfoContract.MomentPresenter {

    private final UserInfoContract.MomentView view;

    public final int pageSize = 10; // 每页请求条数

    private MomentsReleaseStatusReceiver mMomentsReleaseStatusReceiver;

    public UserInfoMomentPresenter(UserInfoContract.MomentView view) {
        this.view = view;
        mMomentsReleaseStatusReceiver = new MomentsReleaseStatusReceiver();
        view.setPresenter(this);
    }

    @Override
    public void getRefreshMomentListData(String userId, int type) {
        getMomentData(userId, 1, type);
    }

    @Override
    public void getLoadMoreListData(String userId, int cursor, int type) {
        getMomentData(userId, cursor, type);
    }

    private void getMomentData(String userId, final int cursor, final int type) {
        if (!Utils.isLogged()) {
            return;
        }
        final String mainType = type == UserInfoContract.MomentView.TYPE_MOMENT ? "" : RequestConstants.I_VIDEO;
        final Flowable<MomentsListBean> flowable = DefaultRequestService.createNearbyRequestService().getUserInfoMomentsList(userId, pageSize + "", cursor + "", mainType);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<MomentsListBean>() {
            @Override
            public void onNext(MomentsListBean data) {
                super.onNext(data);

                if (!hasErrorCode && data != null) {
                    if (cursor == 1) {
                        view.showRefreshListData(data);
                    } else {
                        view.showLoadMoreListData(data);
                    }
                    if (cursor < 2) {
                        view.refreshMomentNum(data.momentsTotalNum, type);
                    }
                } else if (cursor > 1) {
                    view.loadMoreFailed();
                }
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                if (cursor > 1) {
                    view.loadMoreFailed();
                }
            }

            @Override
            public void onComplete() {
                super.onComplete();
                view.requestFinish();
            }
        });
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {

    }

    @Override
    public void register(Context context) {
        IntentFilter intentFilter = new IntentFilter();

        intentFilter.addAction(TheLConstants.BROADCAST_RELEASE_MOMENT_SUCCEED);
        intentFilter.addAction(TheLConstants.BROADCAST_RELEASE_MOMENT_FAIL);
        intentFilter.addAction(TheLConstants.BROADCAST_UPDATE_UNREAD_MSG_COUNT);
        if (mMomentsReleaseStatusReceiver != null && context != null) {
            context.registerReceiver(mMomentsReleaseStatusReceiver, intentFilter);
        }
    }


    @Override
    public void unRegister(Context context) {
        if (mMomentsReleaseStatusReceiver != null && context != null) {
            context.unregisterReceiver(mMomentsReleaseStatusReceiver);
        }
    }

    private class MomentsReleaseStatusReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {

                String action = intent.getAction();

                L.d("UserInfoMomentPresenter", " action : " + action);

                if (action != null) {
                    switch (action) {
                        case TheLConstants.BROADCAST_RELEASE_MOMENT_SUCCEED:
                            view.refreshData();
                            break;

                        default:
                            break;
                    }
                }
            }
        }
    }
}
