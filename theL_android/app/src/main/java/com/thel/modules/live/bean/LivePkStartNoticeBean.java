package com.thel.modules.live.bean;

import java.io.Serializable;

/**
 * Created by waiarl on 2017/12/4.
 */

public class LivePkStartNoticeBean extends BaseLivePkBean implements Serializable {
    public long leftTime;//离Pk结束的剩余时间，单位：秒
    public String userId;//对方ID
    public String nickName;//对方昵称
    public String avatar;//对方头像
    public float x;//x
    public float y;//y
    public float height;
    public float width;
}
