package com.thel.modules.main.home.moments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.thel.R;
import com.thel.base.BaseActivity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import video.com.relavideolibrary.BaseRecyclerAdapter;
import video.com.relavideolibrary.BaseViewHolder;

public class MomentPermissionActivity extends BaseActivity {

    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;

    @OnClick(R.id.tv_back)
    void back() {
        finish();
    }

    int selectPosition;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        selectPosition = getIntent().getIntExtra("position", 0) - 1;
        setContentView(R.layout.activity_moment_permission);
        ButterKnife.bind(this);

        String[] shareToArr = this.getResources().getStringArray(R.array.moments_share_to);
        String[] shareToTipsArr = this.getResources().getStringArray(R.array.moments_share_to_tips);
        List<PermissionBean> list = new ArrayList<>();
        for (int i = 0; i < shareToArr.length; i++) {
            PermissionBean bean = new PermissionBean();
            bean.shareToPermission = shareToArr[i];
            bean.ShareToTips = shareToTipsArr[i];
            bean.select = selectPosition == i;
            list.add(bean);
        }
        recycler_view.setHasFixedSize(true);
        recycler_view.setLayoutManager(new LinearLayoutManager(this));
        recycler_view.setAdapter(new MyAdapter(R.layout.moment_permission_item, recycler_view, list));
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    class PermissionBean {
        String shareToPermission;
        String ShareToTips;
        boolean select;
    }

    class MyAdapter extends BaseRecyclerAdapter<PermissionBean> {
        public MyAdapter(int layoutId, RecyclerView recyclerView, Collection<PermissionBean> list) {
            super(layoutId, recyclerView, list);
        }

        @Override
        public void dataBinding(BaseViewHolder baseViewHolder, PermissionBean permissionBean, int position) {
            baseViewHolder.setText(R.id.permission_text, permissionBean.shareToPermission);
            baseViewHolder.setText(R.id.tips, permissionBean.ShareToTips);
            ImageView imageView = baseViewHolder.getView(R.id.select);
            imageView.setVisibility(permissionBean.select ? View.VISIBLE : View.INVISIBLE);
            baseViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = MomentPermissionActivity.this.getIntent();
                    intent.putExtra("position", position + 1);
                    MomentPermissionActivity.this.setResult(Activity.RESULT_OK, intent);
                    MomentPermissionActivity.this.finish();
                    selectPosition = position;
//                    for (int i = 0; i < mData.size(); i++) {
//                        mData.get(position).select = (position == i);
//                        notifyDataSetChanged();
//                    }
                }
            });
        }
    }
}
