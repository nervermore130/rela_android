package com.thel.modules.live.bean;

import android.text.TextUtils;

import java.util.List;

/**
 * @author liuyun
 * @date 2017/12/5
 */

public class LiveRoomMsgConnectMicBean {
    public String method;
    public String userId;
    public String nickName;
    public String avatar;
    public String linkMicChannel;
    public String fromUserId;
    public String fromNickname;
    public String fromAvatar;
    public String toUserId;
    public String result;
    public String nickname;

    public AgoraBean agora;

    public List<LinkMicAudienceBean> list;

    public static class LinkMicAudienceBean {

        public int status;

        public String userId;

        public String nickname;

        public String avatar;

    }

    public String getNickName() {

        if (!TextUtils.isEmpty(nickName)) {
            return nickName;
        }

        if (!TextUtils.isEmpty(nickname)) {
            return nickname;
        }

        return "";

    }

    @Override public String toString() {
        return "LiveRoomMsgConnectMicBean{" +
                "method='" + method + '\'' +
                ", userId='" + userId + '\'' +
                ", nickName='" + nickName + '\'' +
                ", avatar='" + avatar + '\'' +
                ", linkMicChannel='" + linkMicChannel + '\'' +
                ", fromUserId='" + fromUserId + '\'' +
                ", fromNickname='" + fromNickname + '\'' +
                ", fromAvatar='" + fromAvatar + '\'' +
                ", toUserId='" + toUserId + '\'' +
                ", result='" + result + '\'' +
                ", nickname='" + nickname + '\'' +
                ", agora=" + agora +
                ", list=" + list +
                '}';
    }
}
