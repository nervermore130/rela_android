package com.thel.modules.main.home.moments.comment;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.base.BaseDataBean;
import com.thel.bean.ReleasedCommentReplyBean;
import com.thel.bean.ReleasedCommentReplyBeanNew;
import com.thel.bean.StickerBean;
import com.thel.bean.comment.CommentBean;
import com.thel.bean.comment.CommentReplyListBean;
import com.thel.bean.comment.CommentReplyListBeanNew;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.imp.sticker.StickerContract;
import com.thel.imp.sticker.StickerUtils;
import com.thel.modules.login.login_register.LoginRegisterActivity;
import com.thel.modules.main.me.aboutMe.UpdateUserInfoActivity;
import com.thel.modules.main.me.bean.MyInfoBean;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestConstants;
import com.thel.network.service.DefaultRequestService;
import com.thel.ui.adapter.MomentReplyRecyclerViewAdapter;
import com.thel.ui.widget.emoji.PreviewGridView;
import com.thel.ui.widget.emoji.SendEmojiLayout;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.decoration.DefaultItemDivider;
import com.thel.utils.BusinessUtils;
import com.thel.utils.DeviceUtils;
import com.thel.utils.DialogUtil;
import com.thel.utils.EmojiUtils;
import com.thel.utils.JsonUtils;
import com.thel.utils.MD5Utils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SharedPrefUtils;
import com.thel.utils.SizeUtils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.thel.R.id.img_thumb;

/**
 * moment评论的回复列表页面
 *
 * @author Setsail
 */
public class MomentCommentReplyActivity extends BaseActivity implements OnClickListener, PreviewGridView.MyOnItemClickListener, OnItemClickListener, BaseRecyclerViewAdapter.RequestLoadMoreListener, BaseRecyclerViewAdapter.ReloadMoreListener, StickerContract.StickerChangedListener {

    private LinearLayout lin_back;
    private TextView title_txt;
    private SwipeRefreshLayout swipe_container;
    private RecyclerView listView;
    private EditText input;
    private TextView textLength;
    private ImageView submit;

    private RelativeLayout rel_more_bottom;
    private SendEmojiLayout sendEmojiLayout;// 表情键盘
    private ImageView img_emoji;
    private int defaultSoftInputHeight = SizeUtils.dip2px(TheLApp.getContext(), 200);

    private LinearLayout default_page;

    private Handler mHandler = new Handler(Looper.getMainLooper());

    private String commentId;
    private CommentBean parentComment;
    /**
     * 该父评论下的子评论总数
     */
    private int commentCount;
    /**
     * 该父评论所属的日志的发布者id
     */
    private long momentUserId;

    private String momentId;

    private ArrayList<CommentBean> commentList = new ArrayList<>();
    private MomentReplyRecyclerViewAdapter listAdapter;

    private int limit = 50;
    private int cursor = 0; // 当前的数据库游标

    private boolean haveNextPage = false;

    private static long lastClickTime = 0;

    // 要回复的评论
    private CommentBean toComment = null;
    // 要操作的评论
    private CommentBean operateComment = null;

    private String myComment;
    private String commentType;

    private TextWatcher textWatcher;

    private LinearLayout lin_update_user_info;
    private CommentReplyListBean commentReplyListBean;

    public static final String FROM_MOMENT = "from_mement";//来自日志
    public static final String FROM_NOTICE = "from_notice";//来自通知
    private String intent_from;
    private StickerUtils stickerUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_moment_comment);

        findViewById();

        needRefreshStickers = false;

        sendEmojiLayout.initViews(findViewById(R.id.rel_preview));

        Intent intent = getIntent();

        commentId = intent.getStringExtra(TheLConstants.BUNDLE_KEY_COMMENT_ID);
        intent_from = intent.getStringExtra(TheLConstants.BUNDLE_KEY_INTENT_FROM);
        setListener();
        initAdapter();
        processBusiness();
        stickerUtils = new StickerUtils();
        stickerUtils.registerReceiver(this);
    }

    private void initAdapter() {
        listAdapter = new MomentReplyRecyclerViewAdapter(this, commentList, this, MomentReplyRecyclerViewAdapter.PAGE_MOMENT_COMMENT_REPLY);
        listAdapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {

            @Override
            public void onItemClick(View view, final int position) {
                if (position == 0) {
                    resetInput();
                } else {
                    toComment = commentList.get(position);
                    reply(toComment.nickname);
                }
            }
        });

        // 列表长按
        listAdapter.setOnRecyclerViewItemLongClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemLongClickListener() {
            @Override
            public boolean onItemLongClick(View view, final int position) {
                operateComment = commentList.get(position);
                if ((operateComment.userId + "").equals(ShareFileUtils.getString(ShareFileUtils.ID, ""))) {// 我自己的评论
                    DialogUtil.getInstance().showSelectionDialog(MomentCommentReplyActivity.this, new String[]{
                            getString(R.string.info_reply),
                            getString(R.string.info_copy),
                            getString(R.string.info_delete)}, new OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int p, long id) {
                            DialogUtil.getInstance().closeDialog();
                            switch (p) {
                                case 0:// 回复
                                    toComment = operateComment;
                                    reply(operateComment.nickname);
                                    break;
                                case 1:// 复制
                                    DeviceUtils.copyToClipboard(MomentCommentReplyActivity.this, operateComment.commentText);
                                    break;
                                case 2:// 删除
                                    deleteComment(position);
                                    break;

                                default:
                                    break;
                            }
                        }
                    }, true, 1, null);
                } else {
                    if (ShareFileUtils.getString(ShareFileUtils.ID, "").equals(momentUserId + "")) {// 我自己的日志，可以删除别人的评论
                        DialogUtil.getInstance().showSelectionDialog(MomentCommentReplyActivity.this, new String[]{
                                getString(R.string.info_delete),
                                getString(R.string.info_reply),
                                getString(R.string.info_copy),
                                getString(R.string.info_report)}, new OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int p, long id) {
                                switch (p) {
                                    case 0:// 回复
                                        deleteComment(position);


                                        break;
                                    case 1:// 复制
                                        toComment = operateComment;
                                        reply(operateComment.nickname);
                                        break;
                                    case 2:// 举报
                                        DeviceUtils.copyToClipboard(MomentCommentReplyActivity.this, operateComment.commentText);

                                        break;
                                    case 3:// 删除
                                        report();

                                        break;

                                    default:
                                        break;
                                }
                                DialogUtil.getInstance().closeDialog();
                            }
                        }, true, 1, null);
                    } else {
                        DialogUtil.getInstance().showSelectionDialog(MomentCommentReplyActivity.this, new String[]{
                                getString(R.string.info_report),
                                getString(R.string.info_reply),
                                getString(R.string.info_copy)}, new OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                switch (position) {
                                    case 0:// 回复
                                        report();

                                        break;
                                    case 1:// 复制
                                        toComment = operateComment;
                                        reply(operateComment.nickname);
                                        break;
                                    case 2:// 举报
                                        DeviceUtils.copyToClipboard(MomentCommentReplyActivity.this, operateComment.commentText);

                                        break;

                                    default:
                                        break;
                                }
                                DialogUtil.getInstance().closeDialog();
                            }
                        }, true, 1, null);
                    }
                }

                return false;
            }

        });
        listAdapter.setOnLoadMoreListener(this);
        listAdapter.setReloadMoreListener(this);
        listView.setAdapter(listAdapter);
    }

    private void jumpToUserInfo(int userId) {
//        Intent intent = new Intent();
//        intent.setClass(this, UserInfoActivity.class);
//        intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, userId + "");
//        startActivity(intent);
        FlutterRouterConfig.Companion.gotoUserInfo(userId+"");
    }

    private void findViewById() {
        default_page = this.findViewById(R.id.default_page);
        ((ImageView) (default_page.findViewById(R.id.default_image))).setImageResource(R.mipmap.stay_filters_default);
        ((TextView) (default_page.findViewById(R.id.default_text))).setText(getString(R.string.moment_comments_error));
        lin_back = this.findViewById(R.id.lin_back);
        lin_back.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                ViewUtils.preventViewMultipleClick(arg0, 2000);

                close();

            }
        });
        findViewById(R.id.img_share).setVisibility(View.INVISIBLE);
        title_txt = this.findViewById(R.id.title_txt);
        swipe_container = this.findViewById(R.id.swipe_container);
        ViewUtils.initSwipeRefreshLayout(swipe_container);
        listView = this.findViewById(R.id.recyclerview);
        //如果可以确定每个item的高度是固定的，设置这个选项可以提高性能
        listView.setHasFixedSize(true);
        listView.addItemDecoration(new DefaultItemDivider(TheLApp.getContext(), LinearLayoutManager.VERTICAL, R.color.gray, 1, true, false));
        listView.setLayoutManager(new LinearLayoutManager(TheLApp.getContext()));
        input = this.findViewById(R.id.input);
        input.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                View rootview = MomentCommentReplyActivity.this.getWindow().getDecorView(); // this = activity
                rootview.getWindowVisibleDisplayFrame(r);

                int screenHeight = rootview.getHeight();
                int heightDifference = screenHeight - r.bottom;
                if (heightDifference > 0) {
                    int softHeight = ShareFileUtils.getInt(ShareFileUtils.SOFT_KEYBOARD_HEIGHT, 0);
                    if (softHeight != heightDifference) {// 更新软键盘的高度
                        softHeight = heightDifference;
                        ShareFileUtils.setInt(ShareFileUtils.SOFT_KEYBOARD_HEIGHT, softHeight);
                    }
                    if (softHeight >= defaultSoftInputHeight) {
                        rel_more_bottom.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, softHeight));
                    } else {
                        rel_more_bottom.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, defaultSoftInputHeight));
                    }
                    // 调整表情键盘的上间距
                    sendEmojiLayout.refreshPaddingTop();

                    hideBottomMore();
                }

            }
        });
        textLength = this.findViewById(R.id.write_comment_text_length);
        submit = this.findViewById(R.id.send);

        rel_more_bottom = this.findViewById(R.id.rel_more_bottom);
        img_emoji = this.findViewById(R.id.img_emoji);
        sendEmojiLayout = this.findViewById(R.id.sendEmojiLayout);
        int softHeight = ShareFileUtils.getInt(ShareFileUtils.SOFT_KEYBOARD_HEIGHT, 0);
        if (softHeight >= defaultSoftInputHeight) {
            rel_more_bottom.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, softHeight));
        } else {
            rel_more_bottom.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, defaultSoftInputHeight));
        }

        lin_update_user_info = findViewById(R.id.lin_update_user_info);
        lin_update_user_info.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                Intent intent = new Intent(MomentCommentReplyActivity.this, UpdateUserInfoActivity.class);
                Bundle bundle = new Bundle();
                MyInfoBean userBean = ShareFileUtils.getUserData();
                bundle.putSerializable("my_info", userBean);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

    }

    private void hideBottomMore() {
        if (rel_more_bottom.getVisibility() == View.VISIBLE) {
            rel_more_bottom.setVisibility(View.GONE);
        }
    }

    public static boolean needRefreshStickers = false;

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(this.getClass().getName());
        MobclickAgent.onResume(this);

        if (needRefreshStickers && sendEmojiLayout != null) {
            needRefreshStickers = false;
            sendEmojiLayout.refreshUI();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(this.getClass().getName());
        MobclickAgent.onPause(this);
    }

    protected void processBusiness() {
        swipe_container.post(new Runnable() {
            @Override
            public void run() {
                swipe_container.setRefreshing(true);
            }
        });
        loadAllData();
    }

    /**
     * 没有绑定手机号码不能发布日志，直接跳到绑定手机页面
     */
    private boolean isBindCell() {
        String cell = ShareFileUtils.getString(ShareFileUtils.BIND_CELL, "");
        if (TextUtils.isEmpty(cell) || "null".equals(cell)) {
            //打开注册页面
            LoginRegisterActivity.startActivity(TheLApp.getContext(), LoginRegisterActivity.BUNDLE,false);//绑定手机
            return false;
        }
        return true;
    }

    protected void setListener() {

        listView.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                dismissKeyboard();
                hideBottomMore();
                return false;
            }
        });

        // 发布评论
        submit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                if (isBindCell()) {
                    BusinessUtils.playSound(R.raw.sound_send);
                    submitComment();
                }
            }
        });

        img_emoji.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissKeyboard();
                mHandler.postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        if (rel_more_bottom.getVisibility() == View.VISIBLE) {
                            if (sendEmojiLayout.getVisibility() == View.VISIBLE) {// 底部区域已弹出，则调出键盘
                                showKeyboard();
                            } else {
                                sendEmojiLayout.setVisibility(View.VISIBLE);
                            }
                        } else {
                            rel_more_bottom.setVisibility(View.VISIBLE);
                            sendEmojiLayout.setVisibility(View.VISIBLE);
                        }
                    }
                }, 100);
            }
        });

        title_txt.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (MotionEvent.ACTION_DOWN == event.getAction()) {
                    if (System.currentTimeMillis() - lastClickTime < 300) {
                        ViewUtils.preventViewMultipleTouch(v, 2000);
                        if (listView != null) {
                            listView.scrollToPosition(0);
                        }
                    }
                    lastClickTime = System.currentTimeMillis();
                }
                return true;
            }
        });

        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                loadAllData();
            }
        });

        textWatcher = new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                textLength.setText((500 - s.length()) + "");
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };

        input.addTextChangedListener(textWatcher);

    }

    /**
     * 关闭键盘
     */
    private void dismissKeyboard() {
        // 取消键盘
        ViewUtils.hideSoftInput(this, input);
    }

    /**
     * 开启键盘
     */
    private void showKeyboard() {
        // 开启键盘
        ViewUtils.showSoftInput(this, input);
    }

    private void loadCommentsData(int cursor) {

        Flowable<CommentReplyListBeanNew> flowable = DefaultRequestService.createAllRequestService().getCommentReplyList(commentId, String.valueOf(cursor), String.valueOf(limit));

        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<CommentReplyListBeanNew>() {
            @Override
            public void onNext(CommentReplyListBeanNew commentReplyListBean) {
                super.onNext(commentReplyListBean);
                if (!releasedComments.isEmpty() && commentList.size() > limit) {// 如果刚发布过评论，则需要删掉发过的所有评论
                    for (CommentBean comment : releasedComments) {
                        commentList.remove(comment);
                    }
                    releasedComments.clear();
                }
                parentComment = commentReplyListBean.data.comment;
                momentUserId = commentReplyListBean.data.userId;
                momentId = commentReplyListBean.data.momentId;
                updateTitle(parentComment.commentNum);
                refreshComments(commentReplyListBean.data);
                requestFinished();
            }
        });
    }

    private void loadAllData() {
        cursor = 0;
        loadCommentsData(cursor);
    }

    private CommentBean lastReleasedComment;
    private List<CommentBean> releasedComments = new ArrayList<CommentBean>();

    private void afterReleaseComment(ReleasedCommentReplyBean releasedComment) {
        // 增加评论数
        updateTitle(commentCount + 1);
        lastReleasedComment = new CommentBean();
        lastReleasedComment.momentsId = releasedComment.momentId + "";
        lastReleasedComment.commentId = releasedComment.id + "";
        lastReleasedComment.userId = Integer.valueOf(ShareFileUtils.getString(ShareFileUtils.ID, ""));
        lastReleasedComment.nickname = ShareFileUtils.getString(ShareFileUtils.USER_NAME, "");
        lastReleasedComment.avatar = ShareFileUtils.getString(ShareFileUtils.AVATAR, "");
        lastReleasedComment.commentTime = "0_0";
        lastReleasedComment.commentText = myComment;
        lastReleasedComment.commentType = commentType;
        if (toComment != null) {
            lastReleasedComment.toUserId = toComment.userId;
            lastReleasedComment.toNickname = toComment.nickname;
            lastReleasedComment.toAvatar = toComment.toAvatar;
            lastReleasedComment.sayType = CommentBean.SAY_TYPE_TO;
        } else {
            lastReleasedComment.sayType = CommentBean.SAY_TYPE_SAY;
        }
        commentList.add(lastReleasedComment);
        releasedComments.add(lastReleasedComment);
        listAdapter.notifyDataSetChanged();
        listView.scrollToPosition(commentList.size() - 1);
    }

    private void submitComment() {

        myComment = input.getText().toString().replace("\n", " ").trim();

        commentType = CommentBean.COMMENT_TYPE_TEXT;

        if (TextUtils.isEmpty(myComment)) {
            DialogUtil.showToastShort(this, getString(R.string.moment_comments_empty_tip));
            return;
        }
        showLoadingNoBack();

        replyComment(CommentBean.COMMENT_TYPE_TEXT);
    }


    private void replyComment(String type) {
        Map<String, String> map = new HashMap<>();
        map.put(RequestConstants.I_COMMENT_ID, commentId);
        map.put(RequestConstants.I_TEXT, myComment);
        map.put(RequestConstants.I_TYPE, type);
        if (toComment == null) {// say类型的评论
            map.put(RequestConstants.I_TO_USER_ID, "");
        } else {// to类型的评论
            map.put(RequestConstants.I_TO_USER_ID, String.valueOf(toComment.userId));
        }

        Flowable<ReleasedCommentReplyBeanNew> flowable = DefaultRequestService.createAllRequestService().replyComment(MD5Utils.generateSignatureForMap(map));
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<ReleasedCommentReplyBeanNew>() {
            @Override
            public void onNext(ReleasedCommentReplyBeanNew data) {
                super.onNext(data);
                input.setText("");
                afterReleaseComment(data.data);
                resetInput();
                requestFinished();
            }
        });
    }

    private void resetInput() {
        input.setHint(getString(R.string.moments_comment_input_hint));
        toComment = null;
    }

    private void reply(String nickname) {
        input.setHint(getString(R.string.moments_comment_input_reply) + " " + nickname);
        ViewUtils.showSoftInput(this, input);
    }

    /**
     * 删除评论
     *
     * @param position 如果position == 0，则表示删除父评论，此时
     */
    private void deleteComment(final int position) {
        DialogUtil.showConfirmDialog(this, null, position == 0 ? getString(R.string.delete_parent_comment_confirm) : getString(R.string.moments_delete_comment_confirm), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (operateComment != null && !TextUtils.isEmpty(operateComment.commentId)) {
                    showLoadingNoBack();
                    Map<String, String> map = new HashMap<>();
                    map.put(RequestConstants.I_ID, operateComment.commentId);
                    if (position == 0) {

                        Flowable<BaseDataBean> flowable = DefaultRequestService.createAllRequestService().deleteComment(MD5Utils.generateSignatureForMap(map));
                        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
                            @Override
                            public void onNext(BaseDataBean data) {
                                super.onNext(data);
                                requestFinished();
                                finish();
                            }
                        });

                    } else {

                        Flowable<BaseDataBean> flowable = DefaultRequestService.createAllRequestService().deleteCommentReply(MD5Utils.generateSignatureForMap(map));
                        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
                            @Override
                            public void onNext(BaseDataBean data) {
                                super.onNext(data);
                                commentList.remove(operateComment);
                                listAdapter.notifyDataSetChanged();
                                updateTitle(commentCount - 1);
                                requestFinished();
                            }
                        });
                    }
                }
            }
        });
    }

    private void updateTitle(int count) {
        if (count < 0)
            return;
        commentCount = count;
        if (count <= 1)
            title_txt.setText(getString(R.string.comment_count_odd, count));
        else
            title_txt.setText(getString(R.string.comment_count_even, count));
    }

    public void refreshComments(CommentReplyListBean result) {
        if (cursor == 0) {
            commentList.clear();
            commentList.add(result.comment);// 第一项是父评论
        }
        haveNextPage = result.haveNextPage;
        commentList.addAll(result.commentList);
        if (cursor == 0) {
            listAdapter.removeAllFooterView();
            listAdapter.openLoadMore(commentList.size(), true);
            listAdapter.setNewData(commentList);
        } else {
            listAdapter.notifyDataChangedAfterLoadMore(true, commentList.size());
        }
        cursor = result.cursor;
    }

    private void saveHistorySticker(StickerBean stickerBean) {
        try {
            JSONObject object = stickerBean.toJSON();
            JSONArray jsonArray = new JSONArray(SharedPrefUtils.getString(SharedPrefUtils.FILE_STICKERS, SharedPrefUtils.FILE_STICKERS, "[]"));
            for (int i = 0; i < jsonArray.length(); i++) {
                if (jsonArray.getJSONObject(i).getLong("id") == object.getLong("id")) {
                    JsonUtils.removeIndexOfJSONArray(jsonArray, i);
                    break;
                }
            }
            JSONArray tempArr = new JSONArray();
            tempArr.put(0, object);
            for (int i = 0; i < jsonArray.length(); i++) {
                tempArr.put(i + 1, jsonArray.get(i));
                if (i == 6) {// 最多8个历史记录（1页）
                    break;
                }
            }
            SharedPrefUtils.setString(SharedPrefUtils.FILE_STICKERS, SharedPrefUtils.FILE_STICKERS, tempArr.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void requestFinished() {

        mHandler.postDelayed(new Runnable() {

            @Override
            public void run() {
                if (listView != null) {
                    if (swipe_container != null && swipe_container.isRefreshing())
                        swipe_container.setRefreshing(false);
                }
            }
        }, 1000);
        closeLoading();
    }

    private void requestFaild() {
        listView.post(new Runnable() {
            @Override
            public void run() {
                listAdapter.loadMoreFailed((ViewGroup) listView.getParent());
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (rel_more_bottom.getVisibility() == View.VISIBLE) {
                hideBottomMore();
            } else {
                close();
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void close() {
        if (!TextUtils.isEmpty(input.getText().toString().trim())) {
            DialogUtil.showConfirmDialog(MomentCommentReplyActivity.this, null, getString(R.string.moments_release_moment_discard_tip), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    finish();
                }
            });
        } else {
            finish();
        }
    }

    @Override
    public void finish() {
        ViewUtils.hideSoftInput(this, input);
        super.finish();
    }

    @Override
    public void onItemClick(View view) {
        showLoadingNoBack();
        StickerBean stickerBean = (StickerBean) view.findViewById(R.id.img).getTag();
        saveHistorySticker(stickerBean);
        myComment = stickerBean.buildStickerMsgText();
        commentType = CommentBean.COMMENT_TYPE_STICKER;
        replyComment(commentType);
    }


    /**
     * 静态表情点击事件
     *
     * @param parent
     * @param view
     * @param position
     * @param id
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        StickerBean emoji = (StickerBean) view.findViewById(R.id.img).getTag();
        if (emoji.isDeleteBtn) {
            int selection = input.getSelectionStart();
            String text = input.getText().toString();
            if (selection > 0) {
                String text2 = text.substring(0, selection);
                if (text2.endsWith("]")) {
                    int start = text2.lastIndexOf("[");
                    int end = selection;
                    input.getText().delete(start, end);
                    return;
                }
                input.getText().delete(selection - 1, selection);
            }
        }
        if (!TextUtils.isEmpty(emoji.label)) {
            SpannableString spannableString = EmojiUtils.getInstace(EmojiUtils.DEFAULT_SIZE).addEmoji(this, (int) emoji.id, emoji.label);
            int index = input.getSelectionStart();//获取光标所在位置
            Editable editable = input.getEditableText();//获取EditText的文字
            if (index < 0 || index >= input.length()) {
                editable.append(spannableString);
            } else {
                editable.insert(index, spannableString);//光标所在位置插入文字
            }
        }
    }

    @Override
    public void reloadMore() {
        listAdapter.removeAllFooterView();
        listAdapter.openLoadMore(true);
        loadCommentsData(cursor);
    }

    @Override
    public void onLoadMoreRequested() {
        listView.post(new Runnable() {
            @Override
            public void run() {
                if (haveNextPage) {
                    // 列表滑动到底部加载下一组数据
                    loadCommentsData(cursor);
                } else {
                    listAdapter.openLoadMore(commentList.size(), false);
                    View view = getLayoutInflater().inflate(R.layout.load_more_footer_layout, (ViewGroup) listView.getParent(), false);
                    ((TextView) view.findViewById(R.id.text)).setText(TheLApp.getContext().getString(R.string.info_no_more));
                    listAdapter.addFooterView(view);
                }
            }
        });
    }

    @Override
    public void onClick(final View v) {
        int viewId = v.getId();
        switch (viewId) {
            case img_thumb:
                ViewUtils.preventViewMultipleClick(v, 1000);
                jumpToUserInfo((Integer) v.getTag());
                break;
            case R.id.txt_look_original_moment:
                ViewUtils.preventViewMultipleClick(v, 1000);
                jumpToOriginalMoment();
                break;
        }
    }

    private void jumpToOriginalMoment() {
//        final Intent intent = new Intent(this, MomentCommentActivity.class);
//        intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, momentId);
//        startActivity(intent);
        FlutterRouterConfig.Companion.gotoMomentDetails(momentId);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if (textWatcher != null && input != null)
                input.removeTextChangedListener(textWatcher);
        } catch (Exception e) {
        }
        stickerUtils.unRegisterReceiver(this);
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    /**
     * 举报评论
     */
    private void report() {
        DialogUtil.showConfirmDialog(this, null, getString(R.string.moments_report_comment_confirm), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (operateComment != null && !TextUtils.isEmpty(operateComment.commentId)) {
                    showLoading();

                    Map<String, String> map = new HashMap<>();
                    map.put(RequestConstants.I_ID, operateComment.commentId);

                    Flowable<BaseDataBean> flowable = DefaultRequestService.createAllRequestService().reportComment(MD5Utils.generateSignatureForMap(map));

                    flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
                        @Override
                        public void onNext(BaseDataBean data) {
                            super.onNext(data);
                            requestFinished();
                            DialogUtil.showToastShort(MomentCommentReplyActivity.this, getString(R.string.userinfo_activity_report_success));
                        }
                    });

                }
            }
        });
    }

    @Override
    public void onStickerChanged() {
        needRefreshStickers = true;
    }
}