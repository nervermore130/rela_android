package com.thel.modules.main.me.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;

public class SelectCareerTypeAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private String[] data;
    private int selectedPos = -1;

    // 1 2
    private int type;

    public SelectCareerTypeAdapter(String[] data, int type) {
        mInflater = (LayoutInflater) TheLApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.type = type;
        refreshAdapter(data);
    }

    public void refreshAdapter(String[] data) {
        this.data = data;
    }

    public void refreshSelectedPos(int pos) {
        selectedPos = pos;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return data.length;
    }

    @Override
    public Object getItem(int position) {
        return data[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        HoldView holdView = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.select_career_listitem, parent, false);
            holdView = new HoldView();
            holdView.text = convertView.findViewById(R.id.text);
            convertView.setTag(holdView); // 把holdview缓存下来
        } else {
            holdView = (HoldView) convertView.getTag();
        }

        if (position == selectedPos) {
            holdView.text.setBackgroundColor(TheLApp.getContext().getResources().getColor(R.color.gray));
        } else {
            if (type == 1) {
                holdView.text.setBackgroundColor(TheLApp.getContext().getResources().getColor(R.color.white));
            } else {
                holdView.text.setBackgroundColor(TheLApp.getContext().getResources().getColor(R.color.light_gray));
            }
        }
        holdView.text.setText(data[position]);
        return convertView;
    }

    class HoldView {
        TextView text;
    }
}
