package com.thel.modules.main.me.match;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.constants.TheLConstants;
import com.thel.imp.black.BlackUtils;
import com.thel.modules.main.me.aboutMe.BuyVipActivity;
import com.thel.modules.main.me.aboutMe.UpdateUserInfoActivity;
import com.thel.modules.main.me.bean.LogInfoBean;
import com.thel.modules.main.me.bean.MatchBean;
import com.thel.modules.main.me.bean.MatchListBean;
import com.thel.modules.main.me.match.eventcollect.collect.MatchLogUtils;
import com.thel.modules.main.settings.SettingsActivity;
import com.thel.modules.others.MatchVipDetailViewDialog;
import com.thel.modules.others.VipConfigActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.ui.widget.DefaultEmptyView;
import com.thel.ui.widget.MySwipeRefreshLayout;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;

import java.util.ArrayList;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class WhoLikesMeActivity extends BaseActivity {
    /**
     * 下一页数据的游标
     */
    private String cursor = "";
    /**
     * 单页最大的数据量
     */
    private int limit = 30;
    private MySwipeRefreshLayout swipe_container;
    protected RecyclerView mRecyclerView;
    protected LikeMeListRecyclerViewAdapter mAdapter;
    private GridLayoutManager manager;

    private final int SPAN_COUNT = 3;
    protected ArrayList<MatchBean> list = new ArrayList<>();
    private boolean isFirstCreate = true;
    protected boolean haveNextPage;
    protected int curPage = 1;
    protected String tag;
    private long lastRefreshTime = 0;//上次刷新时间
    private BlackUtils blackUtils;
    protected DefaultEmptyView empty_view;
    private boolean isAutoRefresh = false;
    private int notReplyMeCount;
    private TextView tv_waiting_number;
    private RelativeLayout rl_no_vip;
    private LinearLayout ll_waiting_response;
    private TextView tx_like_count;
    private TextView tv_heart_num;
    private int sumLikeCount;
    private RelativeLayout rl_default_like_view;
    private RelativeLayout rel_bottom;
    private TextView complete_userinfo;
    private TextView updateInfoText;
    private RelativeLayout rl_goto_recharge;
    private LinearLayout ll_look_all_cip_privilege;
    private TextView txt_default_empty;
    private LinearLayout lin_back;
    private LinearLayout ll_push;

    public static final int REFRESH_TYPE_ALL = 0;
    public static final int REFRESH_TYPE_NEXT_PAGE = 1;
    private int refreshType = REFRESH_TYPE_ALL;
    private ImageView img_off_or_on;
    private int recentLikeMeCount;
    private RefreshLikeMeListBrocast refreshLikeMeListBrocast;
    private RelativeLayout rl_who_like_me_dialog;
    private RelativeLayout rl_recall_dialog;
    private RelativeLayout rl_more_be_see;
    private String pageId;
    private String newCursor;
    private String latitude;
    private String longitude;
    private MatchListBean matchListBean;
    private String from_page_id;
    private String from_page;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.who_likes_me_activity);
        initIntent();
        initView();
        initData();
        setLisener();
        registerReceiver();
    }

    private void initIntent() {
        Intent intent = getIntent();
        sumLikeCount = intent.getIntExtra("sumLikeCount", 0);
        notReplyMeCount = ShareFileUtils.getInt(ShareFileUtils.NOT_REPLY_MECOUNT, 0);

        recentLikeMeCount = ShareFileUtils.getInt(ShareFileUtils.NEW_LIKE_NUM, 0);
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            from_page_id = bundle.getString(ShareFileUtils.MATCH_FROM_PAGE_ID, "");
            from_page = bundle.getString(ShareFileUtils.MATCH_FROM_PAGE, "");

        }

        pageId = Utils.getPageId();
    }

    public void registerReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(TheLConstants.BROADCAST_MATCH_LIKE_ME_LIST);
        refreshLikeMeListBrocast = new RefreshLikeMeListBrocast();
        registerReceiver(refreshLikeMeListBrocast, intentFilter);

    }

    //个人资料完成度的广播接收器
    private class RefreshLikeMeListBrocast extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                if (TheLConstants.BROADCAST_MATCH_LIKE_ME_LIST.equals(intent.getAction())) {// 匹配成功后刷新列表
                    initData();
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        int opened = ShareFileUtils.getInt(ShareFileUtils.likeMe, 1);
        if (opened == 1 && img_off_or_on != null) {
            img_off_or_on.setImageResource(R.mipmap.lookme_icon_push_on);
        } else {
            img_off_or_on.setImageResource(R.mipmap.lookme_icon_push_off);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            unregisterReceiver(refreshLikeMeListBrocast);

        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void setLisener() {

        ll_look_all_cip_privilege.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToGetAllPrivilege();
                String latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
                String longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");
                LogInfoBean logInfoBean = new LogInfoBean();
                logInfoBean.page = "match.who";
                logInfoBean.page_id = pageId;
                logInfoBean.activity = "info";
                logInfoBean.from_page = from_page;
                logInfoBean.from_page_id = from_page_id;
                logInfoBean.lat = latitude;
                logInfoBean.lng = longitude;
                MatchLogUtils.getInstance().addLog(logInfoBean);


            }
        });
        ll_push.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(WhoLikesMeActivity.this, SettingsActivity.class);
                intent.putExtra(SettingsActivity.PAGE_TYPE_TAG, SettingsActivity.PAGE_TYPE_PUSH);
                startActivityForResult(intent, TheLConstants.BUNDLE_CODE_SETTINGS_ACTIVITY);

                String latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
                String longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");

                LogInfoBean logInfoBean = new LogInfoBean();
                logInfoBean.page = "match.who";
                logInfoBean.page_id = pageId;
                logInfoBean.activity = "notice";
                logInfoBean.from_page = from_page;
                logInfoBean.from_page_id = from_page_id;
                logInfoBean.lat = latitude;
                logInfoBean.lng = longitude;
                MatchLogUtils.getInstance().addLog(logInfoBean);
            }
        });
        rl_goto_recharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoBuyVip();
            }
        });
        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadNewLikeMeData("", REFRESH_TYPE_ALL);
            }
        });
        mAdapter.setOnLoadMoreListener(new BaseRecyclerViewAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                mRecyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        if (haveNextPage) {
                            // 列表滑动到底部加载下一组数据
                            loadNewLikeMeData(cursor, REFRESH_TYPE_NEXT_PAGE);
                        } else {
                            mAdapter.openLoadMore(0, false);
                            if (list.size() > 0) {
                                View view = getLayoutInflater().inflate(R.layout.load_more_footer_layout, (ViewGroup) mRecyclerView.getParent(), false);
                                ((TextView) view.findViewById(R.id.text)).setText(getString(R.string.info_no_more));
                                mAdapter.addFooterView(view);
                            }
                        }
                    }
                });
            }
        });
        mAdapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                gotoUserInfoActivity(position);

            }
        });
        lin_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        //没有收到过喜欢，跳转完善资料页
        complete_userinfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(WhoLikesMeActivity.this, UpdateUserInfoActivity.class);
                startActivity(intent);
            }
        });
        //查看谁喜欢我
        rl_who_like_me_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MatchVipDetailViewDialog dialog = new MatchVipDetailViewDialog(WhoLikesMeActivity.this);
                dialog.setSeeingType(MatchVipDetailViewDialog.VIP_WHO_LIKE_ME, pageId, "match");
                dialog.show();

                String latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
                String longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");

                LogInfoBean logInfoBean = new LogInfoBean();
                logInfoBean.page = "match.who";
                logInfoBean.page_id = pageId;
                logInfoBean.activity = "view";
                logInfoBean.from_page = from_page;
                logInfoBean.from_page_id = from_page_id;
                logInfoBean.lat = latitude;
                logInfoBean.lng = longitude;
                MatchLogUtils.getInstance().addLog(logInfoBean);
            }
        });
        //后悔
        rl_recall_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MatchVipDetailViewDialog dialog = new MatchVipDetailViewDialog(WhoLikesMeActivity.this);
                dialog.setSeeingType(MatchVipDetailViewDialog.VIP_RECALL, pageId, "match");
                dialog.show();

                String latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
                String longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");

                LogInfoBean logInfoBean = new LogInfoBean();
                logInfoBean.page = "match.who";
                logInfoBean.page_id = pageId;
                logInfoBean.activity = "view";
                logInfoBean.from_page = from_page;
                logInfoBean.from_page_id = from_page_id;
                logInfoBean.lat = latitude;
                logInfoBean.lng = longitude;
                MatchLogUtils.getInstance().addLog(logInfoBean);
            }
        });
        //更多曝光
        rl_more_be_see.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MatchVipDetailViewDialog dialog = new MatchVipDetailViewDialog(WhoLikesMeActivity.this);
                dialog.setSeeingType(MatchVipDetailViewDialog.VIP_EXPOSURE, pageId, "match");
                dialog.show();

                String latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
                String longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");

                LogInfoBean logInfoBean = new LogInfoBean();
                logInfoBean.page = "match.who";
                logInfoBean.page_id = pageId;
                logInfoBean.activity = "view";
                logInfoBean.from_page = from_page;
                logInfoBean.from_page_id = from_page_id;
                logInfoBean.lat = latitude;
                logInfoBean.lng = longitude;
                MatchLogUtils.getInstance().addLog(logInfoBean);

            }
        });

    }

    /**
     * 查看会员全部特权
     **/
    private void goToGetAllPrivilege() {

        Intent intent = new Intent(this, VipConfigActivity.class);
        intent.putExtra("showType", VipConfigActivity.SHOW_TYPE_LIKE);
        Bundle bundle = new Bundle();
        bundle.putString(ShareFileUtils.MATCH_FROM_PAGE_ID, pageId);
        bundle.putString(ShareFileUtils.MATCH_FROM_PAGE, "match.who");
        intent.putExtras(bundle);
        startActivity(intent);

    }

    /**
     * 去购买会员
     */

    private void gotoBuyVip() {

        Intent intent = new Intent(this, BuyVipActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(ShareFileUtils.MATCH_FROM_PAGE_ID, pageId);
        bundle.putString(ShareFileUtils.MATCH_FROM_PAGE, "match.who");
        intent.putExtras(bundle);

        startActivityForResult(intent, TheLConstants.REQUEST_CODE_BUY_VIP);
        String latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
        String longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");

        LogInfoBean logInfoBean = new LogInfoBean();
        logInfoBean.page = "match.who";
        logInfoBean.page_id = pageId;
        logInfoBean.activity = "buy";
        logInfoBean.from_page = from_page;
        logInfoBean.from_page_id = from_page_id;
        logInfoBean.lat = latitude;
        logInfoBean.lng = longitude;
        MatchLogUtils.getInstance().addLog(logInfoBean);
    }

    private void gotoUserInfoActivity(int position) {
        MatchBean matchBean = mAdapter.getData().get(position);
        Intent intent = new Intent(this, DetailedMatchUserinfoActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("MatchBean", matchBean);
        bundle.putString("from", "WhoLikesMeActivity");
        bundle.putString(ShareFileUtils.MATCH_FROM_PAGE_ID, pageId);
        bundle.putString(ShareFileUtils.MATCH_FROM_PAGE, "match.who");
        intent.putExtras(bundle);
        startActivity(intent);
        traceLikeMeData("match.who", "click");

    }

    private void traceLikeMeData(String page, String activity) {

        LogInfoBean logInfoBean = new LogInfoBean();
        logInfoBean.page = page;
        logInfoBean.page_id = pageId;
        ShareFileUtils.setString(ShareFileUtils.MATCH_FROM_PAGE_ID, pageId);

        logInfoBean.activity = activity;
        logInfoBean.from_page = from_page;
        logInfoBean.from_page_id = from_page_id;
        logInfoBean.lat = latitude;
        logInfoBean.lng = longitude;

        MatchLogUtils.getInstance().addLog(logInfoBean);
    }

    private void initView() {
        lin_back = findViewById(R.id.lin_back);
        ll_push = findViewById(R.id.ll_push);
        img_off_or_on = findViewById(R.id.img_off_or_on);
        /**
         * 是会员 并且有未回应的人
         * **/
        swipe_container = findViewById(R.id.swipe_container);
        ViewUtils.initSwipeRefreshLayout(swipe_container);
        mRecyclerView = findViewById(R.id.recyclerview);
        manager = new GridLayoutManager(this, SPAN_COUNT);
        mRecyclerView.setLayoutManager(manager);
        mAdapter = new LikeMeListRecyclerViewAdapter(list);
        mRecyclerView.setAdapter(mAdapter);
        tv_waiting_number = findViewById(R.id.tv_waiting_number);
        /**
         * 没有会员
         * **/
        rl_no_vip = findViewById(R.id.rl_no_vip);
        ll_waiting_response = findViewById(R.id.ll_waiting_response);
        tx_like_count = findViewById(R.id.tx_like_count);
        // tv_heart_num = findViewById(R.id.tv_heart_num);
        rl_goto_recharge = findViewById(R.id.rl_goto_recharge);
        ll_look_all_cip_privilege = findViewById(R.id.ll_look_all_cip_privilege);
        rl_who_like_me_dialog = findViewById(R.id.rl_who_like_me_dialog);
        rl_recall_dialog = findViewById(R.id.rl_recall_dialog);
        rl_more_be_see = findViewById(R.id.rl_more_be_See);

        /**
         * 默认空的页面
         * **/
        rl_default_like_view = findViewById(R.id.rl_default_like_view);
        rel_bottom = findViewById(R.id.rel_bottom);
        complete_userinfo = findViewById(R.id.complete_userinfo);
        updateInfoText = findViewById(R.id.txt_default_tip);
        txt_default_empty = findViewById(R.id.txt_default_empty);

        newCursor = ShareFileUtils.getString(ShareFileUtils.MATCH_NEW_LIKE_ME_COUNT, "");

        int opened = ShareFileUtils.getInt(ShareFileUtils.likeMe, 1);
        if (opened == 1) {
            img_off_or_on.setImageResource(R.mipmap.lookme_icon_push_on);
        } else {
            img_off_or_on.setImageResource(R.mipmap.lookme_icon_push_off);
        }

        /**
         * 新收到了喜欢
         * */

        /****
         * 判断是否是会员页面
         *
         * */


    }

    private void initData() {
        sumLikeCount = ShareFileUtils.getInt(ShareFileUtils.SUM_LIKE_COUNT, 0);
        latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
        longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");
        if (UserUtils.getUserVipLevel() == 0) {      //1.没有会员的话就显示 立即开通会员
            swipe_container.setVisibility(View.GONE);
            ll_waiting_response.setVisibility(View.GONE);

        } else {
            if (null != swipe_container) {
                swipe_container.post(new Runnable() {
                    @Override
                    public void run() {
                        swipe_container.setRefreshing(true);
                    }
                });
            }
        }

        loadNewLikeMeData("", REFRESH_TYPE_ALL);
    }

    @Override
    public void finish() {
        super.finish();
        String oldCursor = ShareFileUtils.getString(ShareFileUtils.MATCH_LIKE_ME_OLD_CURSOR, "");
        ShareFileUtils.setString(ShareFileUtils.MATCH_NEW_LIKE_ME_COUNT, oldCursor);

    }

    /**
     * 查看喜欢我的人的数据
     *
     * @param cursor
     * @param type
     */
    public void loadNewLikeMeData(String cursor, int type) {
        refreshType = type;
        this.cursor = cursor;

        Flowable<MatchListBean> flowable = RequestBusiness.getInstance().getLikeMeMatchList(cursor, limit, newCursor);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<MatchListBean>() {
            @Override
            public void onNext(MatchListBean matchListBean) {
                super.onNext(matchListBean);
                if (TextUtils.isEmpty(matchListBean.errcode)) {
                    getLikeMeListBean(matchListBean);

                } else {


                }
            }

            @Override
            public void onError(Throwable t) {
                L.i("whoLikeMeActivityPrint", "result3" + t.getMessage());
                //  rl_default_like_view.setVisibility(View.VISIBLE);

            }

            @Override
            public void onComplete() {
                requestFinish();
            }
        });
    }

    private void getLikeMeListBean(MatchListBean matchListBean) {
        closeLoading();
        if (matchListBean != null && matchListBean.data == null) {
            return;
        }
        if (refreshType == REFRESH_TYPE_ALL) {
            list.clear();

        }
        list.addAll(matchListBean.data.matchList);

        if (refreshType == REFRESH_TYPE_ALL) {
            mAdapter.removeAllFooterView();
            mAdapter.openLoadMore(list.size(), true);
            mAdapter.setNewData(list);


        } else {
            mAdapter.notifyDataChangedAfterLoadMore(true, list.size());
        }
        this.matchListBean = matchListBean;
        cursor = matchListBean.data.cursor;
        haveNextPage = matchListBean.data.haveNextPage;
        notReplyMeCount = matchListBean.data.notReplyMeCount;

        if (UserUtils.getUserVipLevel() == 0) {      //1.没有会员的话就显示 立即开通会员

            if (notReplyMeCount > 0) {        //并且还有等待回应的喜欢的人
                rl_no_vip.setVisibility(View.VISIBLE);
                rel_bottom.setVisibility(View.VISIBLE);
                tx_like_count.setText(TheLApp.context.getString(R.string.new_like_count, notReplyMeCount));

            } else if (notReplyMeCount == 0 && sumLikeCount > 0) {
                /**
                 * 总喜欢自己的人数大于0 显示尚无回应的喜欢
                 * */

                rl_no_vip.setVisibility(View.VISIBLE);
                rel_bottom.setVisibility(View.VISIBLE);
                tx_like_count.setText(TheLApp.context.getString(R.string.new_like_count, 0));

            } else if (notReplyMeCount == 0 && sumLikeCount == 0) {
                /**
                 * 网络不佳 /还没有收到过喜欢、提示去完善资料页
                 *
                 * **/
                showEmptyView();
            }

        } else {
            traceLikeMeDataList("match.who", "exposure");

            if (notReplyMeCount == 0 && sumLikeCount > 0) {
                /**
                 * 总喜欢自己的人数大于0 显示尚无回应的喜欢
                 * */

                showDefaultNotReplyLike();


            } else if (notReplyMeCount == 0 && sumLikeCount == 0) {
                /**
                 * 网络不佳 /还没有收到过喜欢、提示去完善资料页
                 *
                 * **/
                showEmptyView();
            }

        }
        tv_waiting_number.setText(TheLApp.context.getString(R.string.new_like_count, notReplyMeCount));
        if (TextUtils.isEmpty(cursor) && notReplyMeCount == 0) {
            showDefaultNotReplyLike();
        }

    }

    //谁喜欢我曝光数据
    public void traceLikeMeDataList(String page, String activity) {

        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                LogInfoBean logInfoBean = new LogInfoBean();
                logInfoBean.page = page;
                logInfoBean.page_id = pageId;
                ShareFileUtils.setString(ShareFileUtils.MATCH_FROM_PAGE_ID, pageId);

                logInfoBean.activity = activity;
                logInfoBean.from_page = from_page;
                logInfoBean.from_page_id = from_page_id;
                logInfoBean.lat = latitude;
                logInfoBean.lng = longitude;
                LogInfoBean.LogsDataBean logsDataBean = new LogInfoBean.LogsDataBean();

                logsDataBean.receiver_id = list.get(i).userId + "";
                if (matchListBean != null && matchListBean.data != null && !TextUtils.isEmpty(matchListBean.data.rankId)) {
                    logsDataBean.rank_id = matchListBean.data.rankId;

                }
                logInfoBean.data = logsDataBean;
                MatchLogUtils.getInstance().addLog(logInfoBean);

            }
        }

    }

    private void showDefaultNotReplyLike() {
        rl_default_like_view.setVisibility(View.VISIBLE);
        rel_bottom.setVisibility(View.GONE);
        txt_default_empty.setText(TheLApp.context.getString(R.string.no_response));
        complete_userinfo.setVisibility(View.GONE);
        updateInfoText.setVisibility(View.GONE);
    }

    private void showEmptyView() {
        int userRatio = ShareFileUtils.getUserRatio();

        rl_default_like_view.setVisibility(View.VISIBLE);

        rel_bottom.setVisibility(View.GONE);
        if (userRatio < 80) {
            complete_userinfo.setVisibility(View.VISIBLE);
            updateInfoText.setVisibility(View.VISIBLE);
        }
    }

    public void requestFinish() {
        if (swipe_container != null)
            swipe_container.postDelayed(new Runnable() {

                @Override
                public void run() {
                    if (swipe_container != null && swipe_container.isRefreshing())
                        swipe_container.setRefreshing(false);
                }
            }, 1000);
    }

}


