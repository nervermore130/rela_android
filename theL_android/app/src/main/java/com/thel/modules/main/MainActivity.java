package com.thel.modules.main;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.google.gson.reflect.TypeToken;
import com.networkbench.agent.impl.NBSAppAgent;
import com.tencent.bugly.crashreport.CrashReport;
import com.thel.BuildConfig;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.base.BaseDataBean;
import com.thel.bean.MusicListBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bean.RouteBean;
import com.thel.flutter.bridge.RfSBridgeHandlerFactory;
import com.thel.imp.black.BlackUtils;
import com.thel.manager.ActivityManager;
import com.thel.manager.ChatServiceManager;
import com.thel.modules.live.bean.SoftEnjoyNetBean;
import com.thel.modules.live.stream.RelaStickDownload;
import com.thel.modules.login.login_register.FacebookLogin;
import com.thel.modules.main.me.aboutMe.MatchActivity;
import com.thel.modules.welcome.SplashActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.service.DefaultRequestService;
import com.thel.ui.imageviewer.cropiwa.image.CropIwaResultReceiver;
import com.thel.utils.ActivityUtils;
import com.thel.utils.AppInit;
import com.thel.utils.BadgeUtil;
import com.thel.utils.BusinessUtils;
import com.thel.utils.DeviceUtils;
import com.thel.utils.DialogUtil;
import com.thel.utils.EmojiUtils;
import com.thel.utils.FireBaseUtils;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;
import com.thel.utils.NotchUtils;
import com.thel.utils.RelaTimer;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SizeUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;
import com.thel.utils.VideoUtils;
import com.thel.utils.location.LocationListener;
import com.thel.utils.location.LocationManager;
import com.umeng.analytics.MobclickAgent;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import butterknife.ButterKnife;
import io.flutter.embedding.android.FlutterView;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import me.rela.rf_s_bridge.new_router.NewUniversalRouter;
import me.rela.rf_s_bridge.new_router.containers.NewFlutterFragment;
import me.rela.rf_s_bridge.new_router.interfaces.IRouteListener;
import me.rela.rf_s_bridge.new_router.interfaces.OnRouteBackListener;
import video.com.relavideolibrary.RelaVideoSDK;
import video.com.relavideolibrary.interfaces.FilterDataCallback;
import video.com.relavideolibrary.interfaces.MusicCategoryCallback;
import video.com.relavideolibrary.interfaces.MusicListCallback;
import video.com.relavideolibrary.interfaces.MusicListSyncDataCallback;
import video.com.relavideolibrary.model.FilterBean;
import video.com.relavideolibrary.model.MusicCategoryBean;

/**
 * @author liuyun
 * @date 2017/3/5
 */

public class MainActivity extends BaseActivity implements FilterDataCallback, MusicCategoryCallback, MusicListCallback {

    private MainFragment mainFragment;

    private FrameLayout native_container_fl;

    public static WeakReference<MainActivity> sRef;

    private CropIwaResultReceiver cropResultReceiver;

    private CallbackManager callbackManager = CallbackManager.Factory.create();

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (callbackManager != null) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }

        RfSBridgeHandlerFactory.getInstance().onActivityResult(requestCode, resultCode, data);

        super.onActivityResult(requestCode, resultCode, data);
    }

    private Fragment getNativeFragment() {

        mainFragment = getMainFragment();

        if (mainFragment == null) {
            mainFragment = MainFragment.getInstance();
        }

        if (mainFragment.isAdded()) {
            return null;
        }

        return mainFragment;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        L.d("MainActivity", "---------onCreate--------" + savedInstanceState);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main_new);

        native_container_fl = findView(R.id.native_container_fl);

        NewFlutterFragment flutterFragment = new NewFlutterFragment.NewEngineFragmentBuilder().renderMode(FlutterView.RenderMode.texture).transparencyMode(FlutterView.TransparencyMode.opaque).build();

        getSupportFragmentManager().beginTransaction().add(R.id.flutter_container_fl, flutterFragment, "main_NewFlutterFragment").commit();

        getSupportFragmentManager().beginTransaction().add(R.id.native_container_fl, getNativeFragment(), "main_MainFragment").commit();

        sRef = new WeakReference<>(this);

        ButterKnife.bind(this);

        cropResultReceiver = new CropIwaResultReceiver();
        cropResultReceiver.register(this);
        cropResultReceiver.setListener(RfSBridgeHandlerFactory.getInstance().getListener());

        if (!TextUtils.isEmpty(UserUtils.getUserKey()) && !TextUtils.isEmpty(UserUtils.getMyUserId())) {
            init();
        }

        boolean isNewUser = ShareFileUtils.getBoolean(ShareFileUtils.IS_NEW_USER, false);
        if (isNewUser) {
            FireBaseUtils.uploadGoogle(TheLConstants.FireBaseConstant.SIGN_UP, this);

        } else {
            FireBaseUtils.uploadGoogle(TheLConstants.FireBaseConstant.Login, this);

        }
    }

    public void init() {

        ChatServiceManager.getInstance().startService(TheLApp.context);

        GrowingIOUtil.uploadGrowingCS1(ShareFileUtils.getString(ShareFileUtils.ID, ""));

        new RelaVideoSDK().addFilter(this).addMusicCategory(this).addMusicList(this);

        Utils.checkUpdate(TheLApp.getContext());

        BusinessUtils.getInitData();

        BlackUtils.getNetBlackList();

        getLiveGiftList();

        setNBSCrashMessage();

        VideoUtils.submitVideoReport();

        loadEmoji();

        AppInit.getInstance().initMainActivity();

        pushUmengToken();

        RelaTimer.getInstance().init();

        //下载贴纸
        try {
            new RelaStickDownload().download();
            CrashReport.setUserId(UserUtils.getMyUserId());
        } catch (Exception e) {
            e.printStackTrace();
        }

//        Crashlytics.setUserIdentifier(UserUtils.getMyUserId());

        NewUniversalRouter.sharedInstance.setOnRouteBackListener(new OnRouteBackListener() {
            @Override
            public void onRouteBack() {
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        DialogUtil.showConfirmDialog(MainActivity.this, "", getString(R.string.info_exit), new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialogInterface, int arg1) {
                                dialogInterface.dismiss();
                                MainActivity.this.moveTaskToBack(true);
                            }
                        });
                    }
                });

            }

            @Override
            public void popNative(String count) {

                L.d("MainActivity", " activity stack finish count : " + count);

                if (!TextUtils.isEmpty(count)) {
                    try {
                        int popCount = Integer.valueOf(count);
                        int activityCount = ActivityManager.getInstance().getStackActivityCount();
                        StringBuilder stringBuilder = new StringBuilder();
                        for (int i = activityCount - 1; i > activityCount - popCount - 1; i--) {
                            Activity activity = ActivityManager.getInstance().getActivitys().get(i);
                            ActivityManager.getInstance().removeActivity(activity);
                            stringBuilder.append(activity.getClass().getSimpleName());
                            stringBuilder.append(",");
                            activity.finish();
                        }
                        L.d("MainActivity", " activity stack : " + stringBuilder.toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void containerSwitchToNative(@NonNull String uri) {
                switch (uri) {
                    case "0":
                    case "2":
                    case "4":
                        native_container_fl.setVisibility(View.GONE);
                        break;
                    case "1":
                        native_container_fl.setVisibility(View.VISIBLE);
                        if (mainFragment != null) {
                            mainFragment.showLivePage();
                        }
                        break;
                    case "3":
                        native_container_fl.setVisibility(View.VISIBLE);
                        if (mainFragment != null) {
                            mainFragment.showMessagePage();
                        }
                        break;
                    default:
                        break;
                }

            }

        });
    }

    private void getLocation() {
        new LocationManager(new LocationListener() {
            @Override
            public void onLocation(double lat, double lng) {

                ShareFileUtils.setString(ShareFileUtils.LATITUDE, lat + "");

                ShareFileUtils.setString(ShareFileUtils.LONGITUDE, lng + "");

            }

            @Override
            public void onFailed() {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getLocation();
        BadgeUtil.clearBadgeCount(TheLApp.context);

        String pushMsg = ShareFileUtils.getString(ShareFileUtils.PUSH_MSG, "");

        L.d("MainActivity", " pushMsg : " + pushMsg);

        if (!TextUtils.isEmpty(pushMsg)) {

            Flowable.timer(1, TimeUnit.SECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Long>() {
                @Override
                public void accept(Long aLong) throws Exception {

                    Intent intent = new Intent(TheLApp.context, SplashActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(SplashActivity.PUSH_MSG, pushMsg);
                    TheLApp.context.startActivity(intent);

//            PushUtils.push(this, pushMsg);

                    ShareFileUtils.setString(ShareFileUtils.PUSH_MSG, "");
                }
            });

        }

    }

    private void loadEmoji() {

        Flowable.just("").onBackpressureDrop().subscribeOn(Schedulers.io()).subscribe(new Consumer<String>() {
            @Override
            public void accept(String s) {
                EmojiUtils.getInstace(EmojiUtils.DEFAULT_SIZE).getFileText(TheLApp.context);
            }
        });
    }

    private MainFragment getMainFragment() {
        return (MainFragment) getSupportFragmentManager().findFragmentByTag(MainFragment.class.getName());
    }

    private void setNBSCrashMessage() {
        NBSAppAgent.setUserCrashMessage("用户ID:getUserId", Utils.getMyUserId());
        NBSAppAgent.setUserCrashMessage("设备型号：deviceModel:", DeviceUtils.getModel());
        NBSAppAgent.setUserCrashMessage("SDK版本：getSdkVersion:", DeviceUtils.getSdkVersion() + "");
        NBSAppAgent.setUserCrashMessage("系统版本：getReleaseVersion:", DeviceUtils.getReleaseVersion() + "");
        NBSAppAgent.setUserCrashMessage("设备IMEI：getDeviceId:", ShareFileUtils.getString(ShareFileUtils.DEVICE_ID, ""));
        NBSAppAgent.setUserCrashMessage("语言：getLanguage:", DeviceUtils.getLanguage() + "");
        NBSAppAgent.setUserCrashMessage("栈顶activity:getTopActivity", ActivityUtils.getTopActivity(this) + "");
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        L.d("MainActivity", " --------onNewIntent-------- ");

        setIntent(intent);

        showTab(intent);
    }

    private void showTab(Intent intent) {
        if (intent != null) {

            L.d("MainActivity", " intent : " + intent.toString());

            String tag = intent.getStringExtra(TheLConstants.BUNDLE_KEY_WHERE_TO_GO);

            L.d("MainActivity", " tag : " + tag);

            if (null != tag && tag.length() > 1) {

                if (tag.equals(TheLConstants.MainFragmentPageConstants.FRAGMENT_ME)) {
                    MainFragment newMainFragment = MainFragment.getInstance();
                    if (mainFragment != null) {
                        getSupportFragmentManager().beginTransaction().remove(mainFragment).add(R.id.frame_content, newMainFragment, MainFragment.class.getName()).commit();
                    } else {
                        getSupportFragmentManager().beginTransaction().add(R.id.frame_content, newMainFragment, MainFragment.class.getName()).commit();
                    }
                }

                if (tag.equals(TheLConstants.MainFragmentPageConstants.FRAGMENT_NEARBY)) {

                    if (mainFragment != null && mainFragment.isAdded()) {
                        mainFragment.showNearBy();
                    }

                }

                if (tag.equals(TheLConstants.MainFragmentPageConstants.FRAGMENT_DISCOVER)) {

                    if (mainFragment != null && mainFragment.isAdded()) {
                        mainFragment.showDiscoverVoice();
                    }

                }

                if (mainFragment != null) {
                    if (tag.equals(TheLConstants.MainFragmentPageConstants.FRAGMENT_HOME)) {

                        mainFragment.initNativeFragments();
                        mainFragment.showOrHideFragment(tag);

                        String childPage = intent.getStringExtra(TheLConstants.BUNDLE_KEY_CHILD_PAGE);

                        if (childPage != null && childPage.equals(TheLConstants.MainFragmentPageConstants.FRAGMENT_THEME)) {
                            mainFragment.showThemeFragment();
                        } else {
                            mainFragment.showMoments();
                        }

                    }
                }

                if ("Match".equals(tag)) {// 配对
                    MobclickAgent.onEvent(TheLApp.getContext(), "match_wait_end");// 匹配等待结束
                    startActivity(new Intent(this, MatchActivity.class));
                } else if (TheLConstants.MainFragmentPageConstants.FRAGMENT_NEARBY.equals(tag)) {
                    gotoNearbyPage(TheLConstants.MainFragmentPageConstants.FRAGMENT_NEARBY);
                } else if (TheLConstants.MainFragmentPageConstants.FRAGMENT_DISCOVER.equals(tag)) {
                    gotoNearbyPage(TheLConstants.MainFragmentPageConstants.FRAGMENT_DISCOVER);
                }


            }
        }
    }

    private void gotoNearbyPage(String gotoTab) {
        try {

            if (mainFragment != null) {
                mainFragment.setTabBtnBg(gotoTab);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
    //是否为图片预览返回
//        boolean b = getImageViewer().onKeyDown(keyCode, event);
//        if (b) {
//            return b;
//        }
//        if (keyCode == KeyEvent.KEYCODE_BACK) {
//            DialogUtil.showConfirmDialog(this, "", getString(R.string.info_exit), new DialogInterface.OnClickListener() {
//
//                @Override
//                public void onClick(DialogInterface dialogInterface, int arg1) {
//                    dialogInterface.dismiss();
//                    MainActivity.this.moveTaskToBack(true);
//                }
//            });
//            return true;
//        }
//        return false;
//    }

    @Override
    public List<FilterBean> getFilterData() {

        int[] filterResId = {-1, R.raw.a1_chenguang, R.raw.a2_haiyang, R.raw.a3_qingcao, R.raw.b1_heibai, R.raw.b2_mingliang, R.raw.b3_chenmo, R.raw.f1_weifeng, R.raw.f2_lieri, R.raw.f3_nihong, R.raw.f4_anyong, R.raw.f5_huanghun, R.raw.f6_shiguang, R.raw.f7_qingcao, R.raw.y1_zaocan, R.raw.y2_yanmai, R.raw.s1_xuanlan, R.raw.s2_yexing};

        String[] filterName = {getString(video.com.relavideolibrary.R.string.original_film), "A1", "A2", "A3", "B1", "B2", "B3", "F1", "F2", "F3", "F4", "F5", "F6", "F7", "Y1", "Y2", "S1", "S2"};

        ArrayList<FilterBean> list = new ArrayList<>();

        for (int i = 0; i < filterResId.length; i++) {
            FilterBean filterBean = new FilterBean();
            filterBean.filterName = filterName[i];
            filterBean.filterId = filterResId[i];
            list.add(filterBean);
        }

        return list;
    }

    @Override
    public List<MusicCategoryBean> getMusicCategoryData() {
        List<MusicCategoryBean> categoryBeanList = new ArrayList<>();

        int[] categoryCodeArr = {video.com.relavideolibrary.Utils.Constant.NEWEST_MUSIC_CATEGORY, video.com.relavideolibrary.Utils.Constant.HOT_MUSIC_CATEGORY, 0, 1, 2, 3, 4, 5, 6};
        String[] categoryNameArr = getResources().getStringArray(R.array.music_category);
        int[] selectImageId = {R.mipmap.icn_new_selected, R.mipmap.icn_hot_selected, R.mipmap.icn_love_press, R.mipmap.icn_quiet_press, R.mipmap.icn_calssical_press, R.mipmap.icn_melancholy_press, R.mipmap.icn_exciting_press, R.mipmap.icn_cheerful_press, R.mipmap.icn_movie_press};
        int[] unSelectImageId = {R.mipmap.ic_new, R.mipmap.ic_hot, R.mipmap.icn_love_normal, R.mipmap.icn_quiet_normal, R.mipmap.ic_classical, R.mipmap.ic_sad, R.mipmap.ic_exciting, R.mipmap.ic_happy, R.mipmap.ic_movie};
        for (int i = 0; i < categoryNameArr.length; i++) {
            MusicCategoryBean musicCategoryBean = new MusicCategoryBean();
            musicCategoryBean.categoryName = categoryNameArr[i];
            musicCategoryBean.selectImage = selectImageId[i];
            musicCategoryBean.unSelectImage = unSelectImageId[i];
            musicCategoryBean.categoryCode = categoryCodeArr[i];
            categoryBeanList.add(musicCategoryBean);
        }

        return categoryBeanList;
    }

    @Override
    public void getMusicList(int category,
                             final MusicListSyncDataCallback musicListSyncDataCallback) {

        if (category == video.com.relavideolibrary.Utils.Constant.NEWEST_MUSIC_CATEGORY) {
            RequestBusiness.getInstance().getNewest(1).onBackpressureDrop().observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(new InterceptorSubscribe<MusicListBean>() {
                @Override
                public void onNext(MusicListBean data) {
                    super.onNext(data);
                    if (data != null && data.data != null) {
                        if (musicListSyncDataCallback != null) {
                            musicListSyncDataCallback.onSuccess(data.data);
                        }
                    }
                }
            });
        } else if (category == video.com.relavideolibrary.Utils.Constant.HOT_MUSIC_CATEGORY) {
            RequestBusiness.getInstance().getHot(1).onBackpressureDrop().observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(new InterceptorSubscribe<MusicListBean>() {
                @Override
                public void onNext(MusicListBean data) {
                    super.onNext(data);
                    if (data != null && data.data != null) {
                        if (musicListSyncDataCallback != null) {
                            musicListSyncDataCallback.onSuccess(data.data);
                        }
                    }
                }
            });
        } else {
            RequestBusiness.getInstance().getByCategory(category, 1).onBackpressureDrop().observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(new InterceptorSubscribe<MusicListBean>() {
                @Override
                public void onNext(MusicListBean data) {
                    super.onNext(data);
                    if (data != null && data.data != null) {
                        if (musicListSyncDataCallback != null) {
                            musicListSyncDataCallback.onSuccess(data.data);
                        }
                    }
                }
            });
        }
    }

    public void getLiveGiftList() {

        String etag = ShareFileUtils.getString(ShareFileUtils.GIFT_ETAG, "");
        etag = "";
        final Flowable<SoftEnjoyNetBean> flowable = DefaultRequestService.createLiveRequestService().getLiveGiftList(etag, BuildConfig.APPLICATION_ID);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<SoftEnjoyNetBean>() {
            @Override
            public void onNext(SoftEnjoyNetBean data) {

                if (data != null && data.data != null) {

                    L.d("LiveShowPresenter", " data.data : " + data.data.list.toString());

                    ShareFileUtils.setString(ShareFileUtils.GIFT_ETAG, data.data.md5);

                    if (data.data.list != null) {
                        if (data.data.list.size() > 0) {
                            String json = GsonUtils.createJsonString(data);
                            ShareFileUtils.setString(ShareFileUtils.GIFT_LIST, json);
                        }
                    }

                }
            }
        });

    }

    public CallbackManager getCallbackManager() {
        return callbackManager;
    }

    @Override
    protected void onDestroy() {

        L.d("MainActivity", "---------onDestroy--------");

        if (cropResultReceiver != null) {
            cropResultReceiver.unregister(this);
        }

        super.onDestroy();
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return true;
    }

    private void pushUmengToken() {

        String um_token = ShareFileUtils.getString(ShareFileUtils.UMENG_TOKEN, "");

        if (!TextUtils.isEmpty(um_token) && !TextUtils.isEmpty(UserUtils.getUserKey()) && !TextUtils.isEmpty(UserUtils.getMyUserId())) {

            L.d("RelaApplication", " um_token : " + um_token);

            RequestBusiness.getInstance().uploadDeviceToken(um_token, um_token).onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
                @Override
                public void onNext(BaseDataBean data) {
                    super.onNext(data);

                    L.d("RelaApplication", " pushUmengToken onNext : ");

                }
            });
        }

    }

    public void facebookLogin() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();

        if (accessToken != null) {

            boolean isLoggedIn = !accessToken.isExpired();

            if (isLoggedIn) {
                FacebookLogin.getInstance().signIn(MainActivity.this, accessToken, null);
            }

        } else {

            FacebookLogin.getInstance().login(MainActivity.this, callbackManager, null);

        }
    }

    @Override
    public void onBackPressed() {
        NewUniversalRouter.sharedInstance.getFlutterRouteStack(new IRouteListener() {
            @Override
            public void onRouteCount(String route) {

                if (!TextUtils.isEmpty(route)) {
                    List<RouteBean> routeBeans = GsonUtils.getGson().fromJson(route, new TypeToken<List<RouteBean>>() {
                    }.getType());
                    if (routeBeans.size() > 1) {
                        RouteBean routeBean = routeBeans.get(routeBeans.size() - 1);
                        if (routeBean.getRoute().contains("dialog")) {
                            NewUniversalRouter.sharedInstance.nativeRequestFlutterPop();
                        } else {
                            NewUniversalRouter.sharedInstance.nativePop();
                        }
                    } else {
                        if (NewUniversalRouter.sharedInstance.mOnRouteBackListener != null) {
                            NewUniversalRouter.sharedInstance.mOnRouteBackListener.onRouteBack();
                        }
                    }
                }

            }
        });
    }
}

