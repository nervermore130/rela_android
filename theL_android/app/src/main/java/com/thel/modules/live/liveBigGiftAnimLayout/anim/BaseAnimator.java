package com.thel.modules.live.liveBigGiftAnimLayout.anim;

public class BaseAnimator {

    public OnAnimEndListener mOnAnimEndListener;

    public void setOnAnimEndListener(OnAnimEndListener mOnAnimEndListener) {
        this.mOnAnimEndListener = mOnAnimEndListener;
    }

    public interface OnAnimEndListener {

        void onAnimEnd();

    }
}
