package com.thel.modules.main.messages.span;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import androidx.core.content.ContextCompat;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

import com.google.gson.reflect.TypeToken;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.constants.TheLConstants;
import com.thel.imp.black.BlackUtils;
import com.thel.modules.main.home.moments.ReportActivity;
import com.thel.modules.main.userinfo.BlockNetBean;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.api.nearbyapi.NearbyApiBusiness;
import com.thel.utils.DialogUtil;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;
import com.thel.utils.SharedPrefUtils;
import com.thel.utils.UserUtils;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class StrangerMsgClickSpan extends ClickableSpan {
    public static final int TYPE_BLOCK = 0;//屏蔽
    public static final int TYPE_REPORT = 1;//巨擘
    private final Context activity;
    private int type;

    private String userId;

    public StrangerMsgClickSpan(Context activity, String userId, int type) {
        super();
        this.userId = userId;
        this.type = type;
        this.activity = activity;

        L.d("StrangerMsgClickSpan", " userId : " + userId);

        L.d("StrangerMsgClickSpan", "  UserUtils.getMyUserId : " + UserUtils.getMyUserId());

    }

    @Override
    public void updateDrawState(TextPaint ds) {
        ds.setColor(ContextCompat.getColor(TheLApp.getContext(), R.color.text_color_green));
        ds.setUnderlineText(true); // 去掉下划线
    }

    @Override
    public void onClick(View widget) {
        if (TYPE_BLOCK == type) {//
            if (!isBlockUser()) {
                DialogUtil.showConfirmDialog((Activity) activity, "", TheLApp.getContext().getString(R.string.userinfo_activity_dialog_block), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        blockUser();
                    }
                });
            } else {
                DialogUtil.showAlert(activity, "", TheLApp.context.getResources().getString(R.string.info_repeat_report_user), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
            }

        } else {
            //对于已经举报过的用户直接弹窗提示
            if (isAlreadyReport()) {
                showAlreadyReportDialog();
            } else {
                reportUser();
            }
        }
    }

    private boolean isAlreadyReport() {
        final String[] reportUserList = SharedPrefUtils.getString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.REPORT_USER_LIST, "").split(",");
        if (reportUserList.length > 0) {
            for (String id : reportUserList) {
                if (id.equals(userId)) {
                    return true;
                }
            }
        }
        return false;
    }

    private void showAlreadyReportDialog() {
        DialogUtil.showConfirmDialog((Activity) activity,"",TheLApp.context.getString(R.string.info_repeat_report_user), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
    }

    private void reportUser() {
        Intent intent = new Intent(activity, ReportActivity.class);
        intent.putExtra(TheLConstants.BUNDLE_KEY_REPORT_TYPE, ReportActivity.REPORT_TYPE_REPORT_USER);
        intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, userId);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
    }

    private void blockUser() {
        final Flowable<BlockNetBean> flowable = NearbyApiBusiness.pullUserIntoBlackList(userId);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BlockNetBean>() {
            @Override
            public void onNext(BlockNetBean data) {
                super.onNext(data);

                if (data != null && data.data != null) {
                    BlackUtils.saveOneBlackUser(userId);
                }
            }

        });
    }

    private boolean isBlockUser() {
        String blackList = SharedPrefUtils.getString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.BLACK_LIST, "[]");

        if (blackList != null) {

            Type typeOfDest = new TypeToken<List<Double>>() {
            }.getType();

            List<Double> userList = GsonUtils.getObjects(blackList, typeOfDest);
            if (userList != null && userList.size() > 0) {
                for (Double userId : userList) {

                    BigDecimal bigDecimal = new BigDecimal(userId);

                    String blockUserId = bigDecimal.toPlainString();

                    if (blockUserId.equals(this.userId)) {
                        return true;
                    }
                }
                return false;
            } else {
                return false;
            }
        }
        return false;
    }
}
