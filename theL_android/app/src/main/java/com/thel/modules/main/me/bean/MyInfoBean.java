package com.thel.modules.main.me.bean;

import com.thel.base.BaseDataBean;
import com.thel.bean.user.NearUserBean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 用户自己的信息
 *
 * @author lingwei
 */
public class MyInfoBean extends BaseDataBean implements Serializable {

    /**
     * id : 100761554
     * winkNum : 630
     * isWinked : 0
     * avatar : http://static.thel.co/app/avatar/100761554/f85ed6efb131c8b4050a6ae226217fc3.jpg
     * nickName : bittoy
     * email : bittoy@qq.com
     * age : 31
     * level : 4
     * hiding : 0
     * height : 201
     * weight : 61
     * horoscope : 6
     * ethnicity : 0
     * livecity : 22
     * travelcity : 33
     * interests :
     * intro : des
     * location : 4
     * affection : 0
     * roleName : 0
     * purpose : 4
     * occupation : 1
     * movie : 5
     * music : ic
     * books : bk
     * food : od
     * others : others
     * discriptions :
     * voiceMessage :
     * recorderDate :
     * recorderTimes : 0
     * messageUser : acountuser100761554
     * messagePassword : db41e78df93dd4560ed73bd0b38502d2
     * messageHost : theltest.com
     * userName : 29416589
     * wantRole : 1,3
     * professionalTypes : 0,4
     * outLevel : 0
     * birthday : 1986-07-22
     * wantAgeBegin : 0
     * wantAgeEnd : 100
     * fansNum : 18
     * followNum : 34
     * friendNum : 6
     * bgImage : http://static.thel.co/app/bgimage/100761554/b2ab72653d22132d491a376a0eab5960.jpg
     * locked : 1
     * sinaUid : 1
     * sinaToken : 1
     * sinaTokenSecret : 1
     * verifyType : 1
     * workField :
     * verifyIntro : 1
     * badges : []
     * winkPush : 1
     * followerPush : 0
     * keyPush : 0
     * messagePush : 1
     * commentTextPush : 1
     * commentUserPush : 1
     * commentWinkPush : 1
     * livePush : 1
     */
    public int id;
    public String winkNum;
    public String isWinked;
    public String avatar;
    public String nickName;
    public String email;
    public String age;
    public int level;
    public int hiding;
    public String height;
    public String weight;
    public String horoscope;
    public String ethnicity;
    public String livecity;
    //修改昵称之后返回的天数 null则为可以修改
    public String nicknameModifyDayLeft;

    @Override
    public String toString() {
        return "MyInfoDataBean{" + "id=" + id + ", winkNum='" + winkNum + '\'' + ", isWinked='" + isWinked + '\'' + ", avatar='" + avatar + '\'' + ", nickName='" + nickName + '\'' + ", email='" + email + '\'' + ", age='" + age + '\'' + ", level=" + level + ", hiding=" + hiding + ", height='" + height + '\'' + ", weight='" + weight + '\'' + ", horoscope='" + horoscope + '\'' + ", ethnicity='" + ethnicity + '\'' + ", livecity='" + livecity + '\'' + ", travelcity='" + travelcity + '\'' + ", interests='" + interests + '\'' + ", intro='" + intro + '\'' + ", location='" + location + '\'' + ", affection='" + affection + '\'' + ", roleName='" + roleName + '\'' + ", purpose='" + purpose + '\'' + ", occupation='" + occupation + '\'' + ", movie='" + movie + '\'' + ", music='" + music + '\'' + ", books='" + books + '\'' + ", food='" + food + '\'' + ", others='" + others + '\'' + ", discriptions='" + discriptions + '\'' + ", voiceMessage='" + voiceMessage + '\'' + ", recorderDate='" + recorderDate + '\'' + ", recorderTimes='" + recorderTimes + '\'' + ", messageUser='" + messageUser + '\'' + ", messagePassword='" + messagePassword + '\'' + ", messageHost='" + messageHost + '\'' + ", userName='" + userName + '\'' + ", wantRole='" + wantRole + '\'' + ", professionalTypes='" + professionalTypes + '\'' + ", outLevel='" + outLevel + '\'' + ", birthday='" + birthday + '\'' + ", wantAgeBegin='" + wantAgeBegin + '\'' + ", wantAgeEnd='" + wantAgeEnd + '\'' + ", fansNum=" + fansNum + ", followNum='" + followNum + '\'' + ", friendNum='" + friendNum + '\'' + ", bgImage='" + bgImage + '\'' + ", locked='" + locked + '\'' + ", sinaUid='" + sinaUid + '\'' + ", sinaToken='" + sinaToken + '\'' + ", sinaTokenSecret='" + sinaTokenSecret + '\'' + ", verifyType=" + verifyType + ", workField='" + workField + '\'' + ", verifyIntro='" + verifyIntro + '\'' + ", winkPush=" + winkPush + ", followerPush=" + followerPush + ", keyPush=" + keyPush + ", messagePush=" + messagePush + ", commentTextPush=" + commentTextPush + ", commentUserPush=" + commentUserPush + ", commentWinkPush=" + commentWinkPush + ", livePush=" + livePush + ", badges=" + badges + ", paternerd=" + paternerd + '}';
    }

    public String travelcity;
    public String interests;
    public String intro;
    public String location;
    /**
     * 感情状态  -1表示未设置 0 不想透露 1 单身 2 约会中 3 稳定关系 4 已婚 5开放关系 6求交往 7 等一个人
     */
    public String affection;
    public String roleName;
    public String purpose;
    public String occupation;
    public String movie;
    public String music;
    public String books;
    public String food;
    public String others;
    public String discriptions;
    public String voiceMessage;
    public String recorderDate;
    public String recorderTimes;
    public String messageUser;
    public String messagePassword;
    public String messageHost;
    public String userName;
    public String wantRole;
    public String professionalTypes;
    public String outLevel;
    public String birthday;
    public String wantAgeBegin;
    public String wantAgeEnd;
    public int fansNum;
    public String followNum;
    public String friendNum;
    public String bgImage;
    public String locked;
    public String sinaUid;
    public String sinaToken;
    public String sinaTokenSecret;
    public int verifyType;
    public String workField;
    public String verifyIntro;
    public int winkPush;
    public int followerPush;
    public int keyPush;
    public int messagePush;
    public int commentTextPush;
    public int commentUserPush;
    public int commentWinkPush;
    public int livePush;
    //用户等级
    public int userLevel;
    //会员到期时间
    public String levelExpireTime;
    //资料完成度
    public int ratio;
    ///4.5.0 匹配
    public MatchNetBean match;
    //4.10.0 软妹豆金额
    public int gold;

    public String cursor;

    public String picUrl;

    //第一次注册的用户返回 0：不可以修改 1：可以修改
    public int userNameRevisable;
    public List<?> badges;
    public ArrayList<NearUserBean> nearByUsers;
    // 是否有伴侣
    public MyCirclePartnerBean paternerd = null;

    public static class MatchNetBean {
        // "likeCount":23   //喜欢数量 4.5版本新增 可以不再调用/interface/stay/match/likeMeCount接口
        public int likeCount;
        //最新喜欢的人
        public int recentLikeMeCount;
        //未回应的喜欢的人
        public int notReplyMeCount;
    }

}
