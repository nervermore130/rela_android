package com.thel.modules.live_classification;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.thel.R;
import com.thel.bean.LiveClassificationBean;
import com.thel.constants.TheLConstants;
import com.thel.manager.ImageLoaderManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chad
 * Time 17/12/4
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */

public class LiveClassificationAdapter extends RecyclerView.Adapter<LiveClassificationAdapter.LiveViewHolder> {

    private List<LiveClassificationBean.Data> mData;

    private Context mContext;

    public LiveClassificationAdapter() {
        mData = new ArrayList<>();
    }

    @Override
    public LiveViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_live_calssification_list, parent, false);
        return new LiveViewHolder(view);
    }

    @Override
    public void onBindViewHolder(LiveViewHolder holder, int position) {

        final LiveClassificationBean.Data item = mData.get(position);

        holder.title.setText(item.name);
        ImageLoaderManager.imageLoader(holder.icon, item.selectedIcon);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                bundle.putSerializable(TheLConstants.BUNDLE_KEY_BY_CLASSFY, item);
                intent.putExtras(bundle);
                ((Activity) mContext).setResult(TheLConstants.RESULT_CODE_SELECT_TYPE_CLASSFY, intent);
                ((Activity) mContext).finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setData(@NonNull List<LiveClassificationBean.Data> list) {
        this.mData = list;
        notifyDataSetChanged();
    }

    public class LiveViewHolder extends RecyclerView.ViewHolder {

        public ImageView icon;

        public TextView title;

        public LiveViewHolder(View itemView) {
            super(itemView);
            icon = itemView.findViewById(R.id.icon);
            title = itemView.findViewById(R.id.title);
        }
    }
}
