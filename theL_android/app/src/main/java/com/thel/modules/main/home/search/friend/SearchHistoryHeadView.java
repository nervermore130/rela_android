package com.thel.modules.main.home.search.friend;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.SearchBean;
import com.thel.bean.SearchListBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.imp.follow.FollowStatusChangedImpl;
import com.thel.modules.main.home.search.SearchRecommendAdapter;
import com.thel.modules.main.home.search.SearchRecommendBean;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.service.DefaultRequestService;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.utils.FireBaseUtils;
import com.thel.utils.GsonUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.ViewUtils;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.thel.imp.follow.FollowStatusChangedListener.ACTION_TYPE_CANCEL_FOLLOW;
import static com.thel.imp.follow.FollowStatusChangedListener.ACTION_TYPE_FOLLOW;
import static com.thel.modules.main.home.search.friend.SearchUserAdapter.SEARCH_HISTORY;

/**
 * Created by chad
 * Time 18/8/15
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */
public class SearchHistoryHeadView extends LinearLayout implements View.OnClickListener {

    private Context context;

    private RecyclerView recyclerview_history;

    private RecyclerView recommend_user_recycler;

    private LinearLayout recommend_user_container;

    private SearchUserAdapter adapter;

    public SearchRecommendAdapter recommendUseAdapter;

    // 推荐用户
    private List<SearchRecommendBean.UserList> recommendList = new ArrayList<>();

    private ArrayList<SearchBean> listPlus = new ArrayList<SearchBean>();

    private SearchHeadListener searchHeadListener;

    private boolean isShowAllData = false;

    private boolean hasMoreData = false;

    private LinearLayout search_container;
    private TextView all_search_history;
    private TextView clear_search_history;
    private TextView change_recommend;
    private TextView more_recommend;

    private int cursor = 1;

    public SearchHistoryHeadView(Context context) {
        this(context, null, 0);
    }

    public SearchHistoryHeadView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SearchHistoryHeadView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        initView();
        initHistoryRecycler();
        initRecommendRecycler();
        refreshUi();
    }

    private void initRecommendRecycler() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(TheLApp.getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recommend_user_recycler.setLayoutManager(linearLayoutManager);
        //如果可以确定每个item的高度是固定的，设置这个选项可以提高性能
        recommend_user_recycler.setHasFixedSize(true);
        recommend_user_recycler.setItemAnimator(null);
        recommendUseAdapter = new SearchRecommendAdapter(recommendList);
        recommend_user_recycler.setAdapter(recommendUseAdapter);

        if (recommendUseAdapter.getData() == null || recommendUseAdapter.getData().size() == 0) {
            recommend_user_container.setVisibility(GONE);
        } else {
            recommend_user_container.setVisibility(VISIBLE);
        }

        recommendUseAdapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                final SearchRecommendBean.UserList bean = recommendUseAdapter.getData().get(position);

//                Intent intent = new Intent();
//                intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, bean.userId + "");
//                intent.putExtra("fromPage", SearchUserFragment.class.getName());
//                intent.setClass(context, UserInfoActivity.class);
//                context.startActivity(intent);
                FlutterRouterConfig.Companion.gotoUserInfo(bean.userId + "");
            }
        });
        recommendUseAdapter.setOnRecyclerViewItemChildClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseRecyclerViewAdapter adapter, View view, int position) {
                switch (view.getId()) {
                    case R.id.txt_follow:
                        final int pos = (int) view.getTag() - adapter.getHeaderLayoutCount();
                        final SearchRecommendBean.UserList bean = recommendUseAdapter.getItem(pos);
                        //关注/取消关注某个用户
                        String type = bean.isFollow ? ACTION_TYPE_CANCEL_FOLLOW : ACTION_TYPE_FOLLOW;
                        if (type.equals(ACTION_TYPE_FOLLOW)) {
                            FireBaseUtils.uploadGoogle(TheLConstants.FireBaseConstant.ATTENTION, context);
                        }
                        FollowStatusChangedImpl.followUser(bean.userId + "", type, bean.userName, bean.avatar);
                        bean.isFollow = !bean.isFollow;
                        recommendUseAdapter.notifyDataSetChanged();
                        break;
                }

            }
        });
    }

    private void initHistoryRecycler() {
        LinearLayoutManager manager = new LinearLayoutManager(context);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerview_history.setLayoutManager(manager);
        recyclerview_history.setHasFixedSize(true);
        recyclerview_history.setItemAnimator(null);

        adapter = new SearchUserAdapter(R.layout.search_user_item, listPlus, SEARCH_HISTORY);
        recyclerview_history.setAdapter(adapter);

        adapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (searchHeadListener != null) {
                    //跳转到用户详情
                    searchHeadListener.historyItemClick(position, adapter.getItem(position));
                }
            }
        });

        adapter.setDeleteItemListener(new SearchUserAdapter.DeleteItemListener() {
            @Override
            public void onClick(int position) {
                String json = ShareFileUtils.getString(TheLConstants.SEARCH_HISTORY_JSON, "");
                SearchListBean searchListBean = GsonUtils.getObject(json, SearchListBean.class);
                if (searchListBean == null) return;
                searchListBean.list.remove(position);
                String toJson = GsonUtils.createJsonString(searchListBean);
                ShareFileUtils.setString(TheLConstants.SEARCH_HISTORY_JSON, toJson);

                if (isShowAllData) {
                    listPlus.remove(position);
                } else {
                    listPlus.clear();
                    if (searchListBean.list.size() <= 3) {
                        hasMoreData = false;
                        listPlus.addAll(searchListBean.list);
                    } else {
                        for (int i = 0; i < searchListBean.list.size(); i++) {
                            if (i > 2) {
                                hasMoreData = true;
                                break;
                            }
                            listPlus.add(searchListBean.list.get(i));
                        }
                    }
                    refreshUi();
                }
                SearchHistoryHeadView.this.getParent().requestLayout();
                adapter.notifyDataSetChanged();
            }
        });
    }

    private void initView() {
        inflate(context, R.layout.search_user_header, this);
        recyclerview_history = findViewById(R.id.search_history_recycler);
        recommend_user_recycler = findViewById(R.id.recommend_user_recycler);
        search_container = findViewById(R.id.search_container);
        all_search_history = findViewById(R.id.all_search_history);
        recommend_user_container = findViewById(R.id.recommend_user_container);
        all_search_history.setOnClickListener(this);
        clear_search_history = findViewById(R.id.clear_search_history);
        clear_search_history.setOnClickListener(this);
        change_recommend = findViewById(R.id.change_recommend);
        change_recommend.setOnClickListener(this);
        more_recommend = findViewById(R.id.more_recommend);
        more_recommend.setOnClickListener(this);
    }

    private void refreshUi() {
        if (listPlus.size() == 0) {
            search_container.setVisibility(View.GONE);
        } else if (listPlus.size() < 3) {
            search_container.setVisibility(View.VISIBLE);
            all_search_history.setVisibility(View.GONE);
        } else {
            search_container.setVisibility(View.VISIBLE);
            if (isShowAllData || !hasMoreData) {
                all_search_history.setVisibility(View.GONE);
            } else {
                all_search_history.setVisibility(View.VISIBLE);
            }
        }
    }

    public void refreshData() {
        isShowAllData = false;
        listPlus.clear();
        String string = ShareFileUtils.getString(TheLConstants.SEARCH_HISTORY_JSON, "");
        SearchListBean searchListBean = GsonUtils.getObject(string, SearchListBean.class);
        if (searchListBean != null) {
            if (searchListBean.list.size() <= 3) {
                hasMoreData = false;
                listPlus.addAll(searchListBean.list);
            } else {
                for (int i = 0; i < searchListBean.list.size(); i++) {
                    if (i > 2) {
                        hasMoreData = true;
                        break;
                    }
                    listPlus.add(searchListBean.list.get(i));
                }
            }
        }
        refreshUi();
        adapter.notifyDataSetChanged();
        cursor = 1;
        refreshRecommendData();
    }

    private void refreshRecommendData() {
        DefaultRequestService.createUserRequestService().getRecommendUserRandom(cursor + "", "10").onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new InterceptorSubscribe<SearchRecommendBean>() {
                    @Override
                    public void onNext(SearchRecommendBean data) {
                        super.onNext(data);
                        if (data != null && data.data != null) {
                            cursor = data.data.cursor;
                            if (data.data.list != null) {
                                recommendList = data.data.list;
                                recommendUseAdapter.setNewData(recommendList);
                                if (recommendUseAdapter.getData() == null || recommendUseAdapter.getData().size() == 0) {
                                    recommend_user_container.setVisibility(GONE);
                                } else {
                                    recommend_user_container.setVisibility(VISIBLE);
                                }
                                SearchHistoryHeadView.this.getParent().requestLayout();
                            }
                        }

                    }
                });
    }

    public void setSearchHeadListener(SearchHeadListener searchHistoryListener) {
        this.searchHeadListener = searchHistoryListener;
    }

    @Override
    public void onClick(View v) {
        ViewUtils.preventViewMultipleClick(v, 1000);
        switch (v.getId()) {
            case R.id.all_search_history:
                hasMoreData = false;
                isShowAllData = true;
                listPlus.clear();
                String string = ShareFileUtils.getString(TheLConstants.SEARCH_HISTORY_JSON, "");
                SearchListBean searchListBean = GsonUtils.getObject(string, SearchListBean.class);
                if (searchListBean != null)
                    listPlus.addAll(searchListBean.list);
                adapter.notifyDataSetChanged();
                refreshUi();
                SearchHistoryHeadView.this.getParent().requestLayout();
                break;
            case R.id.clear_search_history:
                ShareFileUtils.setString(TheLConstants.SEARCH_HISTORY_JSON, "");
                listPlus.clear();
                search_container.setVisibility(View.GONE);
                SearchHistoryHeadView.this.getParent().requestLayout();
                break;
            case R.id.change_recommend:
                refreshRecommendData();
                break;
            case R.id.more_recommend:
//                ToastUtils.showToastShort(context, "开发中...");
                break;
        }
    }

    public interface SearchHeadListener {
        void historyItemClick(int position, SearchBean bean);
    }

}
