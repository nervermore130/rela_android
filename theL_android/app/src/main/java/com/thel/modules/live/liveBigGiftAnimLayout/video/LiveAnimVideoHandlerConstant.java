package com.thel.modules.live.liveBigGiftAnimLayout.video;

/**
 * Created by waiarl on 2018/1/12.
 */

public interface LiveAnimVideoHandlerConstant {
    String TAG = "LiveBigAnimVideoView";

    int MSG_SURFACE_AVAILABLE = 0;
    int MSG_SURFACE_CHANGED = 1;
    int MSG_SURFACE_DESTROYED = 2;
    int MSG_SHUTDOWN = 3;
    int MSG_FRAME_AVAILABLE = 4;
    int MSG_RELEASE_PLAYER = 8;
    int MSG_REDRAW = 9;
    int MSG_SWITCH_FILTER = 10;
    int MSG_RESEUME_START_PLAY = 12;
    int MSG_PAUSE_STOP_PLAY = 13;
    int MSG_RELOAD_VIDEO = 14;
    int MSG_RESTART_VIDEO = 15;
    int MSG_VIDEO_PLAYER_PREPARED = 16;
    int MSG_VIDEO_PLAYER_SIZE_CHANGED = 17;
    int MSG_INIT_PLAYER = 18;
    int MSG_PLAY_VIDEO = 19;
}
