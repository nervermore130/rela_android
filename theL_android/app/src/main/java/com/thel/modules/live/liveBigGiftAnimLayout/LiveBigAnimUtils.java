package com.thel.modules.live.liveBigGiftAnimLayout;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import androidx.core.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;

import com.thel.app.TheLApp;
import com.thel.manager.ImageLoaderManager;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by waiarl on 2017/10/24.
 */

public class LiveBigAnimUtils {

    public static void setAssetImage(ImageView imageView, String foldPath, String path) {
        if (imageView == null || imageView.getContext() == null) {
            return;
        }
        ImageLoaderManager.imageLoaderAssents(imageView, foldPath, path + ".png");
        // imageView.setImageBitmap(getBitmap(null,foldPath,path));
    }

    public static void setFrameAnim(Context context, View view, int[] res, int freq) {
        AnimationDrawable anim = new AnimationDrawable();
        final int count = res.length;
        for (int i = 0; i < count; i++) {
            anim.addFrame(ContextCompat.getDrawable(context, res[i]), freq);
        }
        anim.setOneShot(false);
        view.setBackgroundDrawable(anim);
        anim.start();
    }

    public static void setFrameAnim2(Context context, ImageView view, int[] res, int freq) {
        AnimationDrawable anim = new AnimationDrawable();
        final int count = res.length;
        for (int i = 0; i < count; i++) {
            anim.addFrame(ContextCompat.getDrawable(context, res[i]), freq);
        }
        anim.setOneShot(false);
        view.setImageDrawable(anim);
        anim.start();
    }

    public static void setFrameAnim(Context context, View view, String foldPath, String[] res, int freq) {
        AnimationDrawable anim = new AnimationDrawable();
        final int count = res.length;
        for (int i = 0; i < count; i++) {
            anim.addFrame(getDrawable(context, foldPath, res[i]), freq);
        }
        anim.setOneShot(false);
        view.setBackgroundDrawable(anim);
        anim.start();
    }

    public static void setFrameAnim2(Context context, ImageView view, String foldPath, String[] res, int freq) {
        AnimationDrawable anim = new AnimationDrawable();
        final int count = res.length;
        for (int i = 0; i < count; i++) {
            anim.addFrame(getDrawable(context, foldPath, res[i]), freq);
        }
        anim.setOneShot(false);
        view.setImageDrawable(anim);
        anim.start();
    }

    public static Drawable getDrawable(Context context, String foldPath, String path) {
        /*Bitmap bitmap = null;
        final Context con = context == null ? TheLApp.getContext() : context;
        AssetManager am = con.getResources().getAssets();
        InputStream in=null;
        try {
            in = am.open(foldPath + "/" + path + ".png");
            bitmap = BitmapFactory.decodeStream(in);
        } catch (IOException e) {
            e.printStackTrace();

        }finally {
            if(in!=null){
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }*/
        return new BitmapDrawable(getBitmap(context, foldPath, path));
    }

    public static Bitmap getBitmap(Context context, String foldPath, String path) {
        Bitmap bitmap = null;
        final Context con = context == null ? TheLApp.getContext() : context;
        AssetManager am = con.getResources().getAssets();
        InputStream in = null;
        try {
            in = am.open(foldPath + "/" + path + ".png");
            bitmap = BitmapFactory.decodeStream(in);
        } catch (IOException e) {
            e.printStackTrace();

        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return bitmap;
    }

    public static void setAssetBackground(View view, String foldPath, String path) {
        view.setBackgroundDrawable(getDrawable(null, foldPath, path));
    }

    public static void setAssetBackground(AnimImageView view, String foldPath, String path) {
        view.setAssetBackgoundResource(foldPath, path + ".png");
    }
}
