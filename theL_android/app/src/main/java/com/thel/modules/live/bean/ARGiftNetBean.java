package com.thel.modules.live.bean;

import com.thel.base.BaseDataBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chad
 * Time 19/1/22
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */
public class ARGiftNetBean extends BaseDataBean {
    public Data data;

    public class Data {

        public List<ARGiftBean> list = new ArrayList<>();

        public String arGiftMasterKey;
    }

    public class ARGiftBean {
        public int id;

        public String title;

        public int arGiftId;

        public String arGiftReleaseTime;

        public String arResource;
    }
}
