package com.thel.modules.live.surface;

import android.app.Activity;

import com.thel.base.BasePresenter;
import com.thel.base.BaseView;
import com.thel.bean.LiveAdInfoBean;
import com.thel.modules.live.bean.LiveTopFansTodayBean;
import com.thel.modules.live.bean.LiveUserCardBean;
import com.thel.modules.live.bean.SoftEnjoyBean;
import com.thel.modules.live.bean.SoftGiftBean;

/**
 * Created by waiarl on 2017/10/27.
 */

public class LiveShowContract {
    public interface Presenter extends BasePresenter {
        /**
         * 获取名片
         *
         * @param userId
         */
        void getLiveUserCard(String userId);

        /**
         * 获取礼物列表
         */
        void getLiveGiftList();

        void unBindView();

        void reBindView(View view);

        void getLiveAdInfo(String url);

        void getSingleGiftDetail(int id);

        void getLiveAnchorUserCard(String userId);

        /**
         * 获取主播前三名粉丝信息
         */
        void getAnchorTopFans(String userId, int arriveToTopfans);

        void registerReceiver(Activity activity);

        void unRegister(Activity activity);

    }

    public interface View extends BaseView<Presenter> {
        void showLiveUserCard(LiveUserCardBean liveUserCardBean);

        void showLiveGiftList(SoftEnjoyBean softEnjoyBean);

        void showAdView(LiveAdInfoBean liveAdInfoBean);

        void addOfflineGift(SoftGiftBean softGiftBean);

        void showLiveAnchorUserCard(LiveUserCardBean liveUserCardBean);

        void refreshTopFans(LiveTopFansTodayBean topFansBean, int arriveToTopfans);

        void sendShareMsg();

        void sendRecommendMsg();
    }

    /**
     * 直播分享成功回调
     */
    public interface LiveShareListener {
        void shareSuccess();
    }
}
