package com.thel.modules.main.me;

import com.thel.base.BasePresenter;
import com.thel.base.BaseView;
import com.thel.bean.moments.MomentsCheckBean;
import com.thel.modules.main.me.bean.MyInfoBean;

/**
 * Created by lingwei on 2017/9/18.
 */

public class MeContract {
    interface View extends BaseView<MeContract.Presenter> {
        void refreshUI(MyInfoBean myInfoBean);

        // void getLikeMeCount(LikeCountBean likeCountBean);

        //详细资料
        void setDetailErrImgVisible(boolean settingsErrImgVisible);

        //设置有消息数字提醒
        void updateMsgRemind(int msgNum);

        //
        //清除消息提醒
        void clearMsgRemind();

        void uploadAvatar(String avatar);

        void onRequestFinished();

        void requestFailed();

        void refreshRatioUI();

        void updateNewVisitorUI(MomentsCheckBean num);

        void clearSeeMeNum();

        void clearNewLikeMeNum();

    }

    interface Presenter extends BasePresenter {
        void loadNetData();

        void registerReceiver();

        void handlePhoto(String imagePath);

        void uploadImage(String imageLocalPath);

    }
}
