package com.thel.modules.live.view;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.constants.TheLConstants;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.live.bean.LiveRoomMsgBean;
import com.thel.modules.live.in.LiveBaseView;
import com.thel.ui.widget.UserLevelImageView;
import com.thel.utils.L;
import com.thel.utils.SimpleDraweeViewUtils;
import com.thel.utils.SizeUtils;
import com.thel.utils.StringUtils;

/**
 * 付费用户进入直播
 * Created by waiarl on 2018/1/25.
 */

public class LiveLevelEnterView extends RelativeLayout implements LiveBaseView<LiveLevelEnterView> {
    private static final String TAG = "LiveLevelEnterView";
    private final Context mContext;
    private RelativeLayout rel_bg;
    private ImageView img_avatar;
    private UserLevelImageView img_level;
    private ImageView img_vip;
    private TextView txt_enter;
    private SimpleDraweeView img_gif;

    private LiveRoomMsgBean liveRoomMsgBean;

    private static int[] colors6 = {getColor(R.color.live_level_enter_bg_6),
            getColor(R.color.live_level_enter_bg_6)};
    private static int[] colors11 = {getColor(R.color.live_level_enter_bg_11),
            getColor(R.color.live_level_enter_bg_11)};
    private static int[] colors16 = {getColor(R.color.live_level_enter_bg_16),
            getColor(R.color.live_level_enter_bg_16)};
    private static int[] colors21 = {getColor(R.color.live_level_enter_bg_21),
            getColor(R.color.live_level_enter_bg_21)};
    private static int[] colors26 = {getColor(R.color.live_level_enter_bg_26_1),
            getColor(R.color.live_level_enter_bg_26_2)};
    private static int[] colors30 = {getColor(R.color.transparent),
            getColor(R.color.transparent),
    };
    //private String[] gifRes = {"enter/live_level_26.gif", "enter/live_level_26.gif"};
    private String[] gifRes = {"enter/level_enter_29.webp", "enter/level_enter_30.webp"};
    private String gif30Bg = "mipmap-xxhdpi/live_level_30_bg.png";
    private ImageView img_bg_30_1;

    public LiveLevelEnterView(Context context) {
        this(context, null);
    }

    public LiveLevelEnterView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LiveLevelEnterView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
        initViewData();
        setListener();
    }

    private void init() {
        inflate(mContext, R.layout.live_level_enter_view, this);
        rel_bg = findViewById(R.id.rel_bg);
        img_avatar = findViewById(R.id.img_avatar);
        img_level = findViewById(R.id.img_level);
        img_vip = findViewById(R.id.img_vip);
        txt_enter = findViewById(R.id.txt_enter);
        img_gif = findViewById(R.id.img_gif);
        img_bg_30_1 = findViewById(R.id.img_bg_30_1);
    }

    private void initViewData() {
        img_bg_30_1.setVisibility(View.GONE);
    }

    private void setListener() {

    }

    public LiveLevelEnterView initView(LiveRoomMsgBean liveRoomMsgBean) {
        this.liveRoomMsgBean = liveRoomMsgBean;
        if (liveRoomMsgBean == null) {
            return this;
        }
        setLinBg();
        setAvatar();
        setLevel();
        setVip();
        setContent();
        setGif();
        set30bgGif();
        return this;
    }


    private void setLinBg() {
        if (liveRoomMsgBean.upgradeUserLevel >= 11) {
            rel_bg.setBackgroundColor(TheLApp.context.getResources().getColor(R.color.transparent));

            return;
        }
        final int level = liveRoomMsgBean.userLevel;
        int[] colors = new int[]{getColor(R.color.live_level_enter_bg_6)};
        if (level >= 30) {
            colors = colors30;
        } else if (level >= 26) {
            colors = colors26;
        } else if (level >= 21) {
            colors = colors21;
        } else if (level >= 16) {
            colors = colors16;
        } else if (level >= 11) {
            colors = colors11;
        } else if (level >= 6) {
            colors = colors6;
        }
        final GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, colors);
        gradientDrawable.setCornerRadius(TheLApp.getContext().getResources().getDimension(R.dimen.live_level_enter_bg_corner_size));
        gradientDrawable.setAlpha((int) (255 * 0.8f));
        rel_bg.setBackground(gradientDrawable);
    }


    private void setAvatar() {
        String avatar = TextUtils.isEmpty(liveRoomMsgBean.upgradeAvatar) ? liveRoomMsgBean.avatar : liveRoomMsgBean.upgradeAvatar;
        ImageLoaderManager.imageLoaderDefaultCircle(img_avatar, R.mipmap.icon_user, avatar, TheLConstants.AVATAR_SMALL_SIZE, TheLConstants.AVATAR_SMALL_SIZE);
    }

    private void setLevel() {

        final int level = liveRoomMsgBean.upgradeUserLevel >= 11 ? liveRoomMsgBean.upgradeUserLevel : liveRoomMsgBean.userLevel;

        if (liveRoomMsgBean.iconSwitch == 1) {
            img_level.setVisibility(VISIBLE);
            img_level.initView(level);
        } else {
            img_level.setVisibility(GONE);
        }

       /* int res = TheLConstants.USER_LEVEL_RES[0];
        if (level >= 0 && level < TheLConstants.USER_LEVEL_RES.length) {
            res = TheLConstants.USER_LEVEL_RES[level];
        }*/
       /* img_level.setImageResource(res);
        final LinearLayout.LayoutParams param = (LinearLayout.LayoutParams) img_level.getLayoutParams();
        final int height = param.height;
        final int width = LiveUtils.getLevelImageWidth(height, level);
        param.width = width;*/
    }

    private void setVip() {
        final int vip = liveRoomMsgBean.vip;
        img_vip.setVisibility(View.GONE);
        if (vip > 0 && vip < TheLConstants.VIP_LEVEL_RES.length) {
            img_vip.setVisibility(View.VISIBLE);
            img_vip.setImageResource(TheLConstants.VIP_LEVEL_RES[vip]);
        }
    }

    private void setContent() {

        String content;

        if (liveRoomMsgBean.upgradeUserLevel >= 11) {
            content = StringUtils.getString(R.string.promoted_to_level, liveRoomMsgBean.upgradeNickName, liveRoomMsgBean.upgradeUserLevel);
        } else {
            content = StringUtils.getString(R.string.live_enter_live, liveRoomMsgBean.nickName);
        }

        txt_enter.setText(content);
    }

    private void setGif() {

        if (liveRoomMsgBean.upgradeUserLevel >= 11) {
            img_gif.setVisibility(View.VISIBLE);
            img_gif.setLayoutParams(new RelativeLayout.LayoutParams(SizeUtils.dip2px(getContext(), 250), SizeUtils.dip2px(getContext(), 60)));
            String path = TheLConstants.ASSET_PIC_URL + "/enter/level_up_anima.webp";
            DraweeController controller = Fresco.newDraweeControllerBuilder().setUri(path).setAutoPlayAnimations(true).build();
            img_gif.setController(controller);
        } else {
            if (liveRoomMsgBean.userLevel < 26) {
                img_gif.setVisibility(View.GONE);
                return;
            }
            img_gif.setVisibility(View.VISIBLE);
            String res = gifRes[0];
            if (liveRoomMsgBean.userLevel >= 30) {
                res = gifRes[1];
            }

            if (!TextUtils.isEmpty(liveRoomMsgBean.gifImage)) {
                img_gif.setLayoutParams(new RelativeLayout.LayoutParams(SizeUtils.dip2px(getContext(), 250), SizeUtils.dip2px(getContext(), 30)));
                DraweeController controller = Fresco.newDraweeControllerBuilder().setUri(liveRoomMsgBean.gifImage).setAutoPlayAnimations(true).build();
                img_gif.setController(controller);
            } else {
                String path = TheLConstants.ASSET_PIC_URL + "/" + res;
                img_gif.setLayoutParams(new RelativeLayout.LayoutParams(SizeUtils.dip2px(getContext(), 278), SizeUtils.dip2px(getContext(), 30)));
                SimpleDraweeViewUtils.setImageUri(img_gif, Uri.parse(path));
            }
        }

    }


    private static int getColor(int res) {
        return ContextCompat.getColor(TheLApp.getContext(), res);
    }

    public long getDuration() {
        long duration = 1000;
        if (liveRoomMsgBean != null) {
            int level = liveRoomMsgBean.userLevel;
            if (level >= 30) {
                duration = 3000;
            } else if (level >= 26) {
                duration = 2500;
            } else if (level >= 21) {
                duration = 2000;
            } else if (level >= 16) {
                duration = 1000;
            }
        }
        return duration;
    }


    private void set30bgGif() {

        L.d(TAG, " liveRoomMsgBean.upgradeUserLevel : " + liveRoomMsgBean.upgradeUserLevel);

        if (liveRoomMsgBean.upgradeUserLevel >= 11) {

            img_bg_30_1.setVisibility(View.VISIBLE);
            img_bg_30_1.getLayoutParams().width = SizeUtils.dip2px(getContext(), 250);
            img_bg_30_1.getLayoutParams().height = SizeUtils.dip2px(getContext(), 30);

            ImageLoaderManager.imageLoader(img_bg_30_1, R.mipmap.level_up_bg);

        } else {

            final int level = liveRoomMsgBean.userLevel;
            if (level < 30) {
                return;
            }

            L.d(TAG, " liveRoomMsgBean.baseMapImage : " + liveRoomMsgBean.baseMapImage);

            img_bg_30_1.setVisibility(View.VISIBLE);

            if (!TextUtils.isEmpty(liveRoomMsgBean.baseMapImage)) {
                img_bg_30_1.getLayoutParams().width = SizeUtils.dip2px(getContext(), 250);
                img_bg_30_1.getLayoutParams().height = SizeUtils.dip2px(getContext(), 30);
                ImageLoaderManager.imageLoader(img_bg_30_1, R.mipmap.live_level_30_bg, liveRoomMsgBean.baseMapImage);
            } else {
                final int width = (int) TheLApp.getContext().getResources().getDimension(R.dimen.live_level_enter_bg_30_gif_width);
                rel_bg.getLayoutParams().width = width;
                ImageLoaderManager.imageLoader(img_bg_30_1, R.mipmap.live_level_30_bg);
            }
        }

    }

    /*******************************************一下为接口继承方法******************************************************/
    @Override
    public LiveLevelEnterView show() {
        return null;
    }

    @Override
    public LiveLevelEnterView hide() {
        return null;
    }

    @Override
    public LiveLevelEnterView destroyView() {
        return null;
    }

    @Override
    public boolean isAnimating() {
        return false;
    }

    @Override
    public void setAnimating(boolean isAnimating) {

    }

    @Override
    public void showShade(boolean show) {

    }
}
