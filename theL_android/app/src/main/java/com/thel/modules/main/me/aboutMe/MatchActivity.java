package com.thel.modules.main.me.aboutMe;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterPushImpl;
import com.thel.flutter.bridge.RfSBridgeHandlerFactory;
import com.thel.modules.main.me.bean.LogInfoBean;
import com.thel.modules.main.me.bean.MatchBean;
import com.thel.modules.main.me.bean.MatchListBean;
import com.thel.modules.main.me.match.DetailedMatchUserinfoActivity;
import com.thel.modules.main.me.match.MatchSettingsActivity;
import com.thel.modules.main.me.match.WhoLikesMeActivity;
import com.thel.modules.main.me.match.eventcollect.collect.MatchLogUtils;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.ui.slidecard.RevertCardSlidePanel;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.ToastUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MatchActivity extends BaseActivity implements RevertCardSlidePanel.FlingCardListener {

    @BindView(R.id.match_loading_container)
    LinearLayout match_loading_container;

    @BindView(R.id.match_empty_layout)
    LinearLayout match_empty_layout;

    @BindView(R.id.match_loading_anim)
    LottieAnimationView match_loading_anim;

    @BindView(R.id.fling_card_view)
    RevertCardSlidePanel fling_card_view;

    @BindView(R.id.empty_view_img)
    ImageView empty_view_img;

    @BindView(R.id.empty_view_txt)
    TextView empty_view_txt;

    @BindView(R.id.empty_view_btn)
    TextView empty_view_btn;

    @BindView(R.id.like_vip_tip)
    TextView like_vip_tip;

    @BindView(R.id.match_settings_tip)
    View match_settings_tip;

    @BindView(R.id.like_tip)
    View like_tip;
    private long time;
    private String userid;
    private String pageId;
    private boolean match_settings_new;
    private String from_page_id;
    private String from_page;

    @OnClick(R.id.back)
    void back() {
        finish();
    }

    @OnClick(R.id.iv_match_me)
    void goToMyInfoActivity() {

        Intent intent = new Intent(this, MatchSettingsActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(ShareFileUtils.MATCH_FROM_PAGE_ID, pageId);
        bundle.putString(ShareFileUtils.MATCH_FROM_PAGE, "match");
        intent.putExtras(bundle);
        startActivity(intent);
        ShareFileUtils.setBoolean(ShareFileUtils.NEW_MATCH_SETTINGS, false);
        match_settings_tip.setVisibility(View.GONE);
        traceMatchData("match", "myinfo");
    }

    @OnClick(R.id.iv_like_list)
    void goToLikeMeListActivity() {

        Intent intent2 = new Intent();
        intent2.setAction(TheLConstants.BROADCAST_CLEAR_LIKE_ME_NEW_CHECK_ACTION);
        sendBroadcast(intent2);

        ShareFileUtils.setBoolean(ShareFileUtils.MATCH_SAW_LIKE_ME, true);

        like_vip_tip.setVisibility(View.GONE);
        like_tip.setVisibility(View.GONE);
        Intent intent = new Intent(this, WhoLikesMeActivity.class);
        intent.putExtra("sumLikeCount", sumLikeCount);
        intent.putExtra("notReplyMeCount", notReplyMeCount);
        intent.putExtra("hasSeenCursor", hasSeenCursor);
        Bundle bundle = new Bundle();
        bundle.putString(ShareFileUtils.MATCH_FROM_PAGE_ID, pageId);
        bundle.putString(ShareFileUtils.MATCH_FROM_PAGE, "match");
        intent.putExtras(bundle);
        startActivity(intent);

        String latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
        String longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");
        LogInfoBean logInfoBean = new LogInfoBean();

        logInfoBean.page = "match";
        logInfoBean.page_id = pageId;
        logInfoBean.activity = "who";
        logInfoBean.from_page = from_page;
        logInfoBean.from_page_id = from_page_id;
        logInfoBean.lat = latitude;
        logInfoBean.lng = longitude;

        LogInfoBean.LogsDataBean logsDataBean = new LogInfoBean.LogsDataBean();
        if (recentLikeMeCount > 0) {
            logsDataBean.msg = recentLikeMeCount + "";

        }
        logInfoBean.data = logsDataBean;

        MatchLogUtils.getInstance().addLog(logInfoBean);

        FlutterPushImpl.Companion.getMomentsCheckBean().likeMeNum = 0;

        RfSBridgeHandlerFactory.getInstance().getFlutterPushImpl().pushNotRead();
    }

    private void traceMatchData(String page, String activity) {
        String latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
        String longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");

        LogInfoBean logInfoBean4 = new LogInfoBean();
        logInfoBean4.page = page;
        logInfoBean4.page_id = pageId;
        logInfoBean4.activity = activity;
        logInfoBean4.from_page = from_page;
        logInfoBean4.from_page_id = from_page_id;
        logInfoBean4.lat = latitude;
        logInfoBean4.lng = longitude;

        MatchLogUtils.getInstance().addLog(logInfoBean4);

    }

    private List<MatchBean> preData = new ArrayList<>();//预加载数据

    private boolean firstLoadData = true;//第一次加载匹配数据

    private int ratio;//个人资料完善度

    private int sumLikeCount;
    private int notReplyMeCount;
    private String hasSeenCursor;
    private int recentLikeMeCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match);

        ButterKnife.bind(this);

        fling_card_view.setFlingCardListener(this);
        recentLikeMeCount = ShareFileUtils.getInt(ShareFileUtils.NEW_LIKE_NUM, 0);

        ratio = getIntent().getIntExtra("ratio", 0);
        sumLikeCount = getIntent().getIntExtra("sumLikeCount", 0);
        hasSeenCursor = getIntent().getStringExtra("hasSeenCursor");
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            from_page_id = bundle.getString(ShareFileUtils.MATCH_FROM_PAGE_ID, "");
            from_page = bundle.getString(ShareFileUtils.MATCH_FROM_PAGE, "");

        }

        userid = ShareFileUtils.getString(ShareFileUtils.ID, "");
        notReplyMeCount = ShareFileUtils.getInt(ShareFileUtils.NOT_REPLY_MECOUNT, 0);
        time = System.currentTimeMillis();
        pageId = Utils.getPageId();
        match_settings_new = ShareFileUtils.getBoolean(ShareFileUtils.NEW_MATCH_SETTINGS, true);
        if (recentLikeMeCount > 0) {
            like_vip_tip.setVisibility(View.VISIBLE);
            String countStr = String.valueOf(recentLikeMeCount);
            if (recentLikeMeCount > 99) {
                countStr = "99+";
            }
            like_vip_tip.setText(countStr);

        }
        if (match_settings_new) {
            match_settings_tip.setVisibility(View.VISIBLE);
        }

        loadMatchList();

    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void loadMatchList() {

        Flowable<MatchListBean> flowable = RequestBusiness.getInstance().getMatchList();
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<MatchListBean>() {
            @Override
            public void onNext(MatchListBean matchListBean) {
                super.onNext(matchListBean);
                closeMatchLoading();
                if (matchListBean != null && matchListBean.data != null) {
                    if (firstLoadData) {
                        if (matchListBean.data.hasValidImages == 0) {
                            setEmptyLayout(4);
                            return;
                        } else if (matchListBean.data.usedOutToday == 1) {
                            setEmptyLayout(0);
                            return;
                        }
                    }
                    if (matchListBean.data.matchList != null && matchListBean.data.matchList.size() > 0) {
                        fling_card_view.setData(matchListBean, matchListBean.data.matchList, matchListBean.data.usedOutToday == 1, from_page, from_page_id);
                    } else {
                        if (firstLoadData) {//第一次加载数据切数据为空
                            setEmptyLayout(1);
                        }
                    }
                    if (matchListBean.data.reuseMatch == 1) {
                        ToastUtils.showToastShort(MatchActivity.this, getString(R.string.rejoin_match));
                    }
                } else {
                    if (firstLoadData) {//第一次加载数据切数据为空
                        setEmptyLayout(1);
                    }
                }

                firstLoadData = false;
            }

            @Override
            public void onError(Throwable t) {
                closeMatchLoading();
                setEmptyLayout(3);
            }
        });
    }

    private void closeMatchLoading() {
        match_loading_anim.cancelAnimation();
        match_loading_container.setVisibility(View.GONE);
    }

    private void retryLoad() {
        match_loading_container.setVisibility(View.VISIBLE);
        match_loading_anim.playAnimation();
        firstLoadData = true;
        loadMatchList();
    }

    private void setEmptyLayout(int status) {
        switch (status) {
            case 0://速配次数用完
                fling_card_view.setVisibility(View.GONE);
                match_empty_layout.setVisibility(View.VISIBLE);
                empty_view_img.setImageResource(R.mipmap.bg_default_seeu);
                empty_view_txt.setText(R.string.match_empty_tip1);
                empty_view_btn.setVisibility(View.VISIBLE);
                empty_view_btn.setText(R.string.match_empty_tip2);
                empty_view_btn.setOnClickListener(v -> {
                    MatchActivity.this.finish();
                });
                break;
            case 1://未匹配到对象
                fling_card_view.setVisibility(View.GONE);
                match_empty_layout.setVisibility(View.VISIBLE);
                empty_view_img.setImageResource(R.mipmap.bg_default_like);
                empty_view_txt.setText(R.string.match_empty_tip3);
                empty_view_btn.setVisibility(View.VISIBLE);
                empty_view_btn.setText(R.string.retry);
                empty_view_btn.setOnClickListener(v -> {
                    retryLoad();
                });
                break;
            case 2://网络不佳
                fling_card_view.setVisibility(View.GONE);
                match_empty_layout.setVisibility(View.VISIBLE);
                empty_view_img.setImageResource(R.mipmap.bg_default_poornetwork);
                empty_view_txt.setText(R.string.match_empty_tip4);
                empty_view_btn.setVisibility(View.VISIBLE);
                empty_view_btn.setText(R.string.match_empty_tip5);
                break;
            case 3://服务器出错
                fling_card_view.setVisibility(View.GONE);
                match_empty_layout.setVisibility(View.VISIBLE);
                empty_view_img.setImageResource(R.mipmap.bg_default_server);
                empty_view_txt.setText(R.string.match_empty_tip6);
                empty_view_btn.setVisibility(View.GONE);
                break;
            case 4://添加个人照片
                fling_card_view.setVisibility(View.GONE);
                match_empty_layout.setVisibility(View.VISIBLE);
                empty_view_img.setImageResource(R.mipmap.bg_default_photo);
                empty_view_txt.setText(R.string.match_empty_tip7);
                empty_view_btn.setVisibility(View.VISIBLE);
                empty_view_btn.setText(R.string.match_empty_tip8);
                empty_view_btn.setOnClickListener(v -> {
                    startActivityForResult(new Intent(MatchActivity.this, UpdateUserInfoActivity.class), 999);
                });
                break;
            default://默认隐藏空页面
                fling_card_view.setVisibility(View.VISIBLE);
                match_empty_layout.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void loadMore() {
        loadMatchList();
    }

    @Override
    public void usedOutToday() {
        setEmptyLayout(0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 999) {
            retryLoad();
        } else {
            if (fling_card_view != null)
                fling_card_view.onActivityResult(requestCode, resultCode, data);
        }
    }
}
