package com.thel.modules.live.liveBigGiftAnimLayout.anim;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.Point;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.thel.R;
import com.thel.modules.live.liveBigGiftAnimLayout.LiveBigAnimUtils;
import com.thel.utils.Utils;

import java.util.Random;


/**
 * 圣诞老人动画
 * Created by the L on 2016/12/7.
 */

public class ChristmasAnimator {
    private final Context mContext;
    private final int mDuration;
    private final Handler mHandler;
    private final Random mRandom;
    private final int bodyDropDuration;
    //    private final int[] snowRes;
    //    private final int[] sweetRes;
    //    private final int[] bodyRes;
    private final String foldPath;
    private final String[] snowRes;
    private final String[] sweetRes;
    private final String[] bodyRes;
    private final String[] handRes;
    private final int minSnowCount;
    private final int differSnowCount;
    private final int rate;
    private final int minSnowWidth;
    private final int differSnowWidth;
    private final int minSnowBallWidth;
    private final int differSnowBallWidth;
    private final int minDropDuration;
    private final int differDropDuration;
    private final int minRotateCycle;
    private final int differRotateCycle;
    private final int sweetRate;
    private final int bodyRate;
    private final int rotateRate;
    private final float[] leftRotateDegree;
    private final float[] rightRotateDegree;
    private final int outDuration;
    private boolean isPlaying;
    private int mWidth;
    private int mHeight;
    private int sweepDropDuration;

    public ChristmasAnimator(Context context, int duration) {
        mContext = context;
        mDuration = duration;
        mHandler = new Handler(Looper.getMainLooper());
        mRandom = new Random();
        bodyDropDuration = 800;//身体下落时间
        sweepDropDuration = 500;//糖果下落时间
      /*  snowRes = new int[]{R.drawable.snowman_snow1, R.drawable.snowman_snowball, R.drawable.snowman_snow2};//雪花雪球资源
        sweetRes = new int[]{R.drawable.christmas_sweet_1, R.drawable.christmas_sweet_2, R.drawable.christmas_sweet_3, R.drawable.christmas_sweet_4};
        bodyRes = new int[]{R.drawable.christmas_body_1, R.drawable.christmas_body_2};*/
        foldPath = "anim/christ";
        snowRes = new String[]{"snowman_snow1", "snowman_snowball", "snowman_snow2"};//雪花雪球资源
        sweetRes = new String[]{"christmas_sweet_1", "christmas_sweet_2", "christmas_sweet_3", "christmas_sweet_4"};
        bodyRes = new String[]{"christmas_body_1", "christmas_body_2"};
        handRes = new String[]{"christmas_left", "christmas_right"};
        minSnowCount = 3;//雪球最小数
        differSnowCount = 5;//雪球变化区间
        rate = 400;//雪花每波间隔
        minSnowWidth = Utils.dip2px(mContext, 10);//最小雪花大小
        differSnowWidth = Utils.dip2px(mContext, 10);//雪球宽度变化区间
        minSnowBallWidth = Utils.dip2px(mContext, 5);//最小雪球宽度
        differSnowBallWidth = Utils.dip2px(mContext, 5);//雪球宽度变化区间
        minDropDuration = 3000;//最小掉落时间
        differDropDuration = 2000;//掉落时间变化区间
        minRotateCycle = 2000;//雪转动最小周期
        differRotateCycle = 3000;//雪转动周期变化区间
        sweetRate = 200;//糖果动画间隔
        bodyRate = 300;//身体动画间隔
        rotateRate = 800;//hug动画时间
        leftRotateDegree = new float[]{0, -55};//左手 hug 的度数
        rightRotateDegree = new float[]{0, 50};//右手hug 的度数
        outDuration = 1000;//最后动画淡出时间
    }

    public int start(final ViewGroup parent) {
        isPlaying = true;
        mWidth = parent.getMeasuredWidth();
        mHeight = parent.getMeasuredHeight();
        if (mWidth == 0 || mHeight == 0) {
            return 0;
        }
        final RelativeLayout background = new RelativeLayout(mContext);
        background.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
        parent.addView(background);
        setBackground(background);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                isPlaying = false;
                background.clearAnimation();
                parent.removeView(background);
            }
        }, mDuration);
        return mDuration;
    }

    private void setBackground(ViewGroup background) {
        addBody(background);
        addSnow(background);
    }

    /**
     * 添加圣诞老人
     */
    private void addBody(final ViewGroup background) {
        final RelativeLayout rel_body = (RelativeLayout) RelativeLayout.inflate(mContext, R.layout.live_big_anim_christmas_body_view, null);
        initRelBody(rel_body);
        background.addView(rel_body);
        final RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams) rel_body.getLayoutParams();
        param.width = RelativeLayout.LayoutParams.MATCH_PARENT;
        param.height = RelativeLayout.LayoutParams.WRAP_CONTENT;
        param.addRule(RelativeLayout.CENTER_VERTICAL);
        rel_body.measure(0, 0);
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(rel_body, "translationX", mWidth, 0);
        objectAnimator.setDuration(bodyDropDuration);
        objectAnimator.start();
        objectAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                hug(background, rel_body);
                addSweet(background, rel_body);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    private void initRelBody(RelativeLayout rel_body) {
        final ImageView img_body = rel_body.findViewById(R.id.img_body);
        final ImageView img_left = rel_body.findViewById(R.id.img_hand_left);
        final ImageView img_right = rel_body.findViewById(R.id.img_hand_right);
        LiveBigAnimUtils.setAssetImage(img_left, foldPath, handRes[0]);
        LiveBigAnimUtils.setAssetImage(img_right, foldPath, handRes[1]);
        LiveBigAnimUtils.setAssetBackground(img_body, foldPath, bodyRes[0]);
    }

    /**
     * hug
     */
    private void hug(ViewGroup background, RelativeLayout rel_body) {
        final ImageView img_body = rel_body.findViewById(R.id.img_body);
        final ImageView img_left = rel_body.findViewById(R.id.img_hand_left);
        final ImageView img_right = rel_body.findViewById(R.id.img_hand_right);
        // LiveBigGiftAnimLayout.setFrameAnim(mContext, img_body, bodyRes, bodyRate);
        LiveBigAnimUtils.setFrameAnim2(mContext, img_body, foldPath, bodyRes, bodyRate);
        img_left.setPivotX(img_left.getMeasuredWidth() * 25 / 196);
        img_left.setPivotY(img_left.getMeasuredHeight() / 10);
        img_right.setPivotX(img_right.getMeasuredWidth() * 137 / 154);
        img_right.setPivotY(img_right.getMeasuredWidth() / 10);
        rockView(img_left, leftRotateDegree, rotateRate);
        rockView(img_right, rightRotateDegree, rotateRate);
    }

    /**
     * 摇晃hug  view
     */
    private void rockView(ImageView view, float[] rotateDegree, int rotateRate) {
        ObjectAnimator oa = ObjectAnimator.ofFloat(view, "rotation", rotateDegree);
        oa.setDuration(rotateRate);
        oa.setRepeatCount(ObjectAnimator.INFINITE);
        oa.setRepeatMode(ObjectAnimator.REVERSE);
        oa.setInterpolator(new LinearInterpolator());
        oa.start();
    }

    /**
     * 添加糖果吊坠
     */
    private void addSweet(ViewGroup background, final RelativeLayout rel_body) {
        final ImageView img_sweet = new ImageView(mContext);
        //img_sweet.setBackgroundResource(R.drawable.christmas_sweet_1);
        LiveBigAnimUtils.setAssetBackground(img_sweet, foldPath, sweetRes[0]);
        background.addView(img_sweet);
        ViewGroup.LayoutParams param = img_sweet.getLayoutParams();
        final int w = Math.min(mWidth, mHeight);
        final int h = w * 511 / 735;
        param.width = w;
        param.height = h;
        if (param instanceof RelativeLayout.LayoutParams) {
            ((RelativeLayout.LayoutParams) param).addRule(RelativeLayout.CENTER_HORIZONTAL);
        }
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(img_sweet, "translationY", -h, 0);
        objectAnimator.setDuration(sweepDropDuration);
        objectAnimator.start();
        objectAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                // LiveBigGiftAnimLayout.setFrameAnim(mContext, img_sweet, sweetRes, sweetRate);
                LiveBigAnimUtils.setFrameAnim(mContext, img_sweet, foldPath, sweetRes, sweetRate);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                img_sweet.animate().alpha(0).setDuration(outDuration);
                rel_body.animate().translationXBy(-mWidth).setDuration(outDuration);
            }
        }, mDuration - bodyDropDuration - outDuration - 200);//多200防止误差
    }

    /**
     * 下雪
     */
    private void addSnow(final ViewGroup background) {
        background.post(new Runnable() {
            @Override
            public void run() {
                final int snowCount = mRandom.nextInt(differSnowCount) + minSnowCount;
                for (int i = 0; i < snowCount; i++) {
                    dropOneSnow(background);
                }
                if (isPlaying) {
                    background.postDelayed(this, rate);
                }
            }
        });
    }

    /**
     * 单个雪
     */
    private void dropOneSnow(final ViewGroup background) {
        if (!isPlaying) {
            return;
        }
        final ImageView img_snow = new ImageView(mContext);
        background.addView(img_snow);
        final int resIndex = mRandom.nextInt(snowRes.length);
        /*final int res = snowRes[resIndex];
        int w = minSnowWidth + mRandom.nextInt(differSnowWidth);
        if (res == R.drawable.snowman_snowball) {//如果是雪球，换雪球随机大小
            w = minSnowBallWidth + mRandom.nextInt(differSnowBallWidth);
        }
        int h = getImageHeight(res, w);
        img_snow.setImageResource(snowRes[resIndex]);*/
        final String res = snowRes[resIndex];
        int w = minSnowWidth + mRandom.nextInt(differSnowWidth);
        if (res.equals(snowRes[1])) {//如果是雪球，换雪球随机大小
            w = minSnowBallWidth + mRandom.nextInt(differSnowBallWidth);
        }
        int h = getImageHeight(resIndex, w);
        // img_snow.setImageResource(snowRes[resIndex]);
        LiveBigAnimUtils.setAssetImage(img_snow, foldPath, snowRes[resIndex]);
        img_snow.setLayoutParams(new RelativeLayout.LayoutParams(w, h));
        Point startPoint = new Point(mRandom.nextInt(mWidth), 0 - h);
        Point endPoint = new Point(startPoint.x, mHeight);
        Path path = new Path();
        path.moveTo(startPoint.x, startPoint.y);
        path.lineTo(endPoint.x, endPoint.y);
        int cycle = 0;
        if (resIndex == 0 || resIndex == 2) {//如果是雪花，则旋转
            cycle = minRotateCycle + mRandom.nextInt(differRotateCycle);
        }
        final int dropDuration = minDropDuration + mRandom.nextInt(differDropDuration);
        FloatAnimation anim = new FloatAnimation(path, background, img_snow, dropDuration, cycle);
        anim.setDuration(dropDuration);
        anim.setInterpolator(new LinearInterpolator());
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                background.post(new Runnable() {
                    @Override
                    public void run() {
                        background.removeView(img_snow);
                    }
                });
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        img_snow.startAnimation(anim);
    }

    class FloatAnimation extends Animation {
        private final int mCycle;
        private final int mDropDuration;
        private PathMeasure mPm;
        private View mView;
        private float mDistance;

        public FloatAnimation(Path path, View parent, View child, int dropDuration, int cycle) {
            mPm = new PathMeasure(path, false);
            mDistance = mPm.getLength();
            mView = child;
            parent.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            mCycle = cycle;
            mDropDuration = dropDuration;
        }

        @Override
        protected void applyTransformation(float factor, Transformation transformation) {
            Matrix matrix = transformation.getMatrix();
            mPm.getMatrix(mDistance * factor, matrix, PathMeasure.POSITION_MATRIX_FLAG);
            if (mCycle > 0) {
                mView.setRotation(360 * mDropDuration * factor / mCycle);
            }
            mView.setScaleX(1);
            mView.setScaleY(1);
            transformation.setAlpha(1);
        }
    }

    /**
     * 根据不同的资源使用不同比例获取高度
     */
    private int getImageHeight(int resId, int img_width) {
        int img_height;
        switch (resId) {
            case 0:
                img_height = img_width * 575 / 580;
                break;
            case 1:
                img_height = img_width;
                break;
            case 2:
                img_height = img_width * 340 / 296;
                break;
            default:
                img_height = img_width;
                break;
        }
        return img_height;
    }
}
