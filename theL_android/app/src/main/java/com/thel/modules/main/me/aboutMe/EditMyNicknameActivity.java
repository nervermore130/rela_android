package com.thel.modules.main.me.aboutMe;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.base.BaseDataBean;
import com.thel.constants.TheLConstants;
import com.thel.modules.main.me.bean.LogInfoBean;
import com.thel.modules.main.me.match.eventcollect.collect.MatchLogUtils;
import com.thel.modules.others.VipConfigActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.utils.DialogUtil;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * 修改昵称 区分会员和非会员
 * Created by lingwei on 2018/7/17.
 */

public class EditMyNicknameActivity extends BaseActivity {

    private EditText edit_nickname;
    private ImageView lin_back;
    private TextView img_more;
    private TextView txt_num;
    private TextView txt_title;
    private Handler mHandler;
    private String nickname;
    private String currentNickname;
    private String requestFrom;
    private String oldIntroContent;
    private String oldRelaIdContent;
    private TextView tv_limit_days;
    private TextView txt_nickname_num;
    private TextView recharge_vip;
    private ImageView my_img_vip;
    private String nicknameModifyDayLeft;
    private String pageId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_nickname_acitivity_layout);
        mHandler = new Handler(Looper.getMainLooper());
        findViewById();
        getIntentData();
        setListener();
        pageId = Utils.getPageId();

    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void findViewById() {
        lin_back = findViewById(R.id.lin_back);
        txt_title = findViewById(R.id.txt_title);
        img_more = findViewById(R.id.img_more);
        edit_nickname = findViewById(R.id.edit_nickname);
        txt_num = findViewById(R.id.txt_num);
        tv_limit_days = findViewById(R.id.tv_limit_days);
        txt_nickname_num = findViewById(R.id.txt_nickname_num);
        recharge_vip = findViewById(R.id.recharge_vip);
        my_img_vip = findViewById(R.id.my_img_vip);
    }

    private void setListener() {
        recharge_vip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoBuyVip();
            }
        });
        lin_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                ViewUtils.hideSoftInput(EditMyNicknameActivity.this);

            }
        });
        img_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewUtils.hideSoftInput(EditMyNicknameActivity.this, img_more);

                if (requestFrom.equals("nickName")) {
                    saveNickName();
                }
            }
        });
        edit_nickname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                txt_nickname_num.setText((12 - s.length()) + "/12");
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });


    }

    /**
     * 购买会员
     */
    private void gotoBuyVip() {
        Intent intent = new Intent(this, VipConfigActivity.class);
        intent.putExtra("fromPage", "nickname");
        intent.putExtra("fromPageId", pageId);
        startActivity(intent);
        traceEditNameLog("nickname.buy");
    }


    private void saveNickName() {

        nickname = edit_nickname.getText().toString().trim();
        if (nickname.equals(currentNickname)) {
            finish();
            return;
        }
        if (TextUtils.isEmpty(nickname)) {
            DialogUtil.showToastShort(this, getString(R.string.register_activity_nick_name_is_empty));
            return;
        }
        Flowable<BaseDataBean> flowable = RequestBusiness.getInstance().getNickNameTimeVerify(nickname);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
            @Override
            public void onNext(BaseDataBean data) {
                super.onNext(data);
                saveSuccess();
            }
        });

    }

    private void saveSuccess() {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                final Intent intent = new Intent();
                final Bundle bundle = new Bundle();
                bundle.putString(TheLConstants.BUNDLE_KEY_NICKNAME, nickname);
                intent.putExtras(bundle);
                setResult(TheLConstants.RESULT_CODE_EDIT_NICKNAME, intent);
                finish();
            }
        }, 2000);
    }


    private void getIntentData() {
        final Intent intent = getIntent();
        requestFrom = intent.getStringExtra("requestFrom");
        nicknameModifyDayLeft = intent.getStringExtra("day");
        if (requestFrom.equals("nickName")) {
            txt_title.setText(TheLApp.context.getString(R.string.register_activity_nick_name));
            edit_nickname.setVisibility(View.VISIBLE);
            tv_limit_days.setVisibility(View.VISIBLE);
            currentNickname = intent.getStringExtra(TheLConstants.BUNDLE_KEY_NICKNAME);
            if (edit_nickname != null) {
                edit_nickname.setText(currentNickname);
            }
            if (currentNickname != null && txt_nickname_num != null) {
                txt_nickname_num.setText((12 - currentNickname.length()) + "/12");
            }
        }
        /***
         * 1.如果没有开通会员，没有修改过名称，要显示的提示 , 开通会员，每30天仅限修改1次昵称，开通会员每10天即可修改1次。
         * 2.如果没有开通会员，已经修改了昵称，要显示的提示，开通会员可立即修改 >，每30天仅限修改1次昵称，你距离下次修改还剩X天。
         * 3.已经开通了会员，没有修改过昵称，显示： 自己的会员图标，会员特权：每10天可修改1次昵称。
         * 4.已经开通了会员，已经修改过了昵称，显示：自己的名称不可点击， 自己的会员图标，会员特权：每10天可修改1次昵称，你距离下次修改还剩X天。
         * */

        switch (UserUtils.getUserVipLevel()) {
            case 1:
                my_img_vip.setImageResource(R.mipmap.icn_vip_1);

                break;
            case 2:
                my_img_vip.setImageResource(R.mipmap.icn_vip_2);

                break;
            case 3:
                my_img_vip.setImageResource(R.mipmap.icn_vip_3);

                break;
            case 4:
                my_img_vip.setImageResource(R.mipmap.icn_vip_4);

                break;
        }
        int modifyDay = Integer.parseInt(nicknameModifyDayLeft);
        if (UserUtils.getUserVipLevel() == 0 && modifyDay == 0) {  //一步骤
            my_img_vip.setVisibility(View.GONE);
            tv_limit_days.setText(TheLApp.context.getString(R.string.vip_nickname_change_remind));
            recharge_vip.setText(TheLApp.context.getString(R.string.buy_vip) + " >");

        } else if (UserUtils.getUserVipLevel() == 0 && modifyDay > 0) {  //二步骤
            my_img_vip.setVisibility(View.GONE);
            String text = TheLApp.context.getString(R.string.remind_modify_time, modifyDay + "");
            recharge_vip.setText(TheLApp.context.getString(R.string.right_now_modify));
            tv_limit_days.setText(text);

            img_more.setVisibility(View.GONE);
            edit_nickname.setEnabled(false);
            edit_nickname.setTextColor(ContextCompat.getColor(this, R.color.pic_placeholder_color));
            txt_nickname_num.setVisibility(View.GONE);


        } else if (UserUtils.getUserVipLevel() > 0 && modifyDay == 0) { //步骤三
            my_img_vip.setVisibility(View.VISIBLE);
            tv_limit_days.setText(TheLApp.context.getString(R.string.vip_privilege));
            recharge_vip.setVisibility(View.GONE);

        } else if (UserUtils.getUserVipLevel() > 0 && modifyDay > 0) {//步骤四
            my_img_vip.setVisibility(View.VISIBLE);
            tv_limit_days.setText(TheLApp.context.getString(R.string.vip_privilege));
            recharge_vip.setVisibility(View.GONE);
            String day = TheLApp.context.getString(R.string.vip_remind_modify_time, modifyDay);
            tv_limit_days.setText(day);

            img_more.setVisibility(View.GONE);
            edit_nickname.setEnabled(false);
            edit_nickname.setTextColor(ContextCompat.getColor(this, R.color.pic_placeholder_color));
            txt_nickname_num.setVisibility(View.GONE);

        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            DialogUtil.showConfirmDialog(EditMyNicknameActivity.this, "", getString(R.string.updatauserinfo_activity_quit_confirm), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    EditMyNicknameActivity.this.finish();
                }
            });
        }
        return super.onKeyDown(keyCode, event);
    }

    public void traceEditNameLog(String activity) {
        try {

            LogInfoBean logInfoBean = new LogInfoBean();
            logInfoBean.page = "nickname";
            logInfoBean.page_id = pageId;
            logInfoBean.activity = activity;

            MatchLogUtils.getInstance().addLog(logInfoBean);

        } catch (Exception e) {

        }
    }
}
