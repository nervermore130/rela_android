package com.thel.modules.live.in;

import android.app.Activity;
import android.view.SurfaceView;

/**
 * Created by waiarl on 2017/5/11.
 */

public interface StreamerIn {

    enum streamState {
        start, pause, stop
    }

    enum msgOther {
        one, two, three, four, five, six, seven, eight, nine, ten
    }

    void initStreamer( Activity context, String streamingUrl);

    /**
     * 初始化
     */
    void init();


    void initStreamConfig();

    /**
     * 设置静音
     */
    void setMulte(boolean multe);

    /**
     * 开始
     */
    void start();

    /**
     * 暂停
     */
    void pause();

    /**
     * 重启
     */
    void resume();

    void destroy();

    void setOnInfoListener(StreamListener listener);

    void setOnErrorListener(StreamListener listener);

    /**
     * 开启预览
     */
    void changePreviewState(boolean start);

    /**
     * stream 状态
     *
     * @param state
     * @return
     */
    boolean StreamState(streamState state);

    void switchCamera();

    /**
     * 设置美颜
     *
     * @param enable
     */
    void switchBeauty(boolean enable);

    Object getStream();

    void doOthers(msgOther msg);

    void startCameraPreviewWithPermCheck();

}
