package com.thel.modules.main.me.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.constants.TheLConstants;
import com.thel.imp.follow.FollowStatusChangedImpl;
import com.thel.modules.main.me.AwaitLiveActivity;
import com.thel.modules.main.me.aboutMe.FollowingAndFansFragment;
import com.thel.modules.main.me.aboutMe.SecretFollowActivity;
import com.thel.modules.main.me.bean.FriendsBean;
import com.thel.modules.main.me.match.eventcollect.collect.SocialLogUtils;
import com.thel.modules.main.messages.ChatActivity;
import com.thel.ui.widget.popupwindow.UnFollowPopupWindow;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;
import com.thel.utils.FireBaseUtils;
import com.thel.utils.UserUtils;

import java.util.List;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class FriendsListAdapter extends BaseRecyclerViewAdapter<FriendsBean> {

    private String pageid;
    private String from_page_id;
    private String from_page;
    private String listType = "";
    private Context mContext;

    public FriendsListAdapter(List<FriendsBean> data, String listType, Context context) {
        super(R.layout.common_friend_list_item, data);
        this.listType = listType;
        this.mContext = context;
    }

    public FriendsListAdapter(List<FriendsBean> data, String listType, Context context, String from_page, String from_page_id, String page_id) {
        super(R.layout.common_friend_list_item, data);
        this.listType = listType;
        this.mContext = context;
        this.from_page = from_page;
        this.from_page_id = from_page_id;
        this.pageid = page_id;
    }

    @Override
    protected void convert(final BaseViewHolder helper, final FriendsBean friendsBean) {
        if (friendsBean.userId.equals(UserUtils.getMyUserId())) {
            helper.setVisibility(R.id.ll_statue_view, GONE);
        } else {
            helper.setVisibility(R.id.ll_statue_view, VISIBLE);
        }
        if (friendsBean.verifyType <= 0) {// 非加V用户，显示两行，第一行为昵称和简介，第二行为在线状态、距离、感情状态
            helper.setVisibility(R.id.line2, View.VISIBLE);
            helper.setVisibility(R.id.img_indentify, View.GONE);
            helper.setText(R.id.txt_desc, friendsBean.intro + "");


        } else {// 加V用户，显示一行，显示昵称、认证图标、认证信息
            helper.setVisibility(R.id.line2, View.VISIBLE);
            helper.setVisibility(R.id.img_indentify, View.VISIBLE);
            helper.setText(R.id.txt_desc, friendsBean.verifyIntro + "");

            if (friendsBean.verifyType == 1) {// 个人加V
                helper.setImageResource(R.id.img_indentify, R.mipmap.icn_verify_person);
            } else {// 企业加V
                helper.setImageResource(R.id.img_indentify, R.mipmap.icn_verify_enterprise);

            }
        }
        if (friendsBean.hiding > 0) {
            helper.setText(R.id.txt_distance, "");
            if (friendsBean.secretly > 0) {
                helper.setText(R.id.txt_distance, " (" + TheLApp.context.getString(R.string.vip_config_act_secretly_follow_title) + ")");

            }
        } else {
            helper.setText(R.id.txt_distance, friendsBean.distance);
            if (friendsBean.secretly > 0) {
                helper.setText(R.id.txt_distance, friendsBean.distance + " (" + TheLApp.context.getString(R.string.vip_config_act_secretly_follow_title) + ")");

            }
        }

        if (friendsBean.level > 0) {
            helper.setVisibility(R.id.img_vip, VISIBLE);
            switch (friendsBean.level) {
                case 1:
                    helper.setImageResource(R.id.img_vip, R.mipmap.icn_vip_1);
                    break;
                case 2:
                    helper.setImageResource(R.id.img_vip, R.mipmap.icn_vip_2);

                    break;
                case 3:
                    helper.setImageResource(R.id.img_vip, R.mipmap.icn_vip_3);

                    break;
                case 4:
                    helper.setImageResource(R.id.img_vip, R.mipmap.icn_vip_4);

                    break;
            }
        } else {
            helper.setVisibility(R.id.img_vip, GONE);

        }

        // 角色设定 0=unknow,1=t,2=p,3=h,5=bi
        if (friendsBean.roleName.equals("0")) {
            helper.setImageResource(R.id.img_role, R.mipmap.icn_role_unknow);
        } else if (friendsBean.roleName.equals("1")) {
            helper.setImageResource(R.id.img_role, R.mipmap.icn_role_t);
        } else if (friendsBean.roleName.equals("2")) {
            helper.setImageResource(R.id.img_role, R.mipmap.icn_role_p);
        } else if (friendsBean.roleName.equals("3")) {
            helper.setImageResource(R.id.img_role, R.mipmap.icn_role_h);
        } else if (friendsBean.roleName.equals("5")) {
            helper.setImageResource(R.id.img_role, R.mipmap.icn_role_bi);
        } else if (friendsBean.roleName.equals("6")) {
            helper.setImageResource(R.id.img_role, R.mipmap.icn_role_s);
        } else {
            helper.setImageResource(R.id.img_role, R.mipmap.icn_role_unknow);
        }
        // 头像
        helper.setImageUrl(R.id.img_thumb, friendsBean.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE);

        // 用户名
        helper.setText(R.id.txt_name, friendsBean.nickName);

        TextView followView = helper.getView(R.id.statue_view);
        ImageView image_remove = helper.getView(R.id.image_remove);
        String text = "";
        if (listType.equals(SecretFollowActivity.TYPE_SECRET)) {
            text = TheLApp.getContext().getString(R.string.secretly_followed);
        } else {
            if (listType.equals(FollowingAndFansFragment.TYPE_FOLLOW)) {
                if (friendsBean.followStatus == 3) {
                    text = TheLApp.getContext().getString(R.string.interrelated);
                } else {
                    text = TheLApp.getContext().getString(R.string.userinfo_activity_followed);
                }
                followView.setBackgroundResource(R.drawable.follow_unselector);
                followView.setTextColor(TheLApp.getContext().getResources().getColor(R.color.text_unselector));
            } else if (listType.equals(FollowingAndFansFragment.TYPE_FRIEND)) {
                text = TheLApp.getContext().getString(R.string.userinfo_activity_left_chat);
                followView.setBackgroundResource(R.drawable.follow_selector);
                followView.setTextColor(TheLApp.getContext().getResources().getColor(R.color.text_selector));
            } else if (listType.equals(FollowingAndFansFragment.TYPE_FANS)) {
                image_remove.setVisibility(VISIBLE);

                if (friendsBean.followStatus == 3) {
                    text = TheLApp.getContext().getString(R.string.interrelated);
                    followView.setBackgroundResource(R.drawable.follow_unselector);
                    followView.setTextColor(TheLApp.getContext().getResources().getColor(R.color.text_unselector));
                } else {
                    text = TheLApp.getContext().getString(R.string.repowder);
                    followView.setBackgroundResource(R.drawable.follow_selector);
                    followView.setTextColor(TheLApp.getContext().getResources().getColor(R.color.text_selector));
                }
            } else {
                switch (friendsBean.followStatus) {
                    case 0:
                        text = TheLApp.getContext().getString(R.string.userinfo_activity_follow);
                        followView.setTextColor(TheLApp.getContext().getResources().getColor(R.color.text_selector));
                        followView.setBackgroundResource(R.drawable.follow_selector);

                        break;
                    case 1:
                        text = TheLApp.getContext().getString(R.string.userinfo_activity_followed);
                        followView.setBackgroundResource(R.drawable.follow_unselector);
                        followView.setTextColor(TheLApp.getContext().getResources().getColor(R.color.text_unselector));
                        break;
                    case 2:
                        text = TheLApp.getContext().getString(R.string.repowder);
                        followView.setBackgroundResource(R.drawable.follow_selector);
                        followView.setTextColor(TheLApp.getContext().getResources().getColor(R.color.text_selector));

                        break;
                    case 3:
                        text = TheLApp.getContext().getString(R.string.interrelated);
                        followView.setBackgroundResource(R.drawable.follow_unselector);
                        followView.setTextColor(TheLApp.getContext().getResources().getColor(R.color.text_unselector));
                        break;
                }
            }
        }
        followView.setText(text);
        image_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = helper.getLayoutPosition();
                mOnItemLongClickListener.onItemLongClick(v, position, friendsBean);

            }
        });

        followView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listType.equals(FollowingAndFansFragment.TYPE_FOLLOW) || listType.equals(SecretFollowActivity.TYPE_SECRET)) {
                    UnFollowPopupWindow mUnFollowPopupWindow = new UnFollowPopupWindow(mContext, friendsBean.nickName);
                    mUnFollowPopupWindow.setOnUnFollowListener(new UnFollowPopupWindow.OnUnFollowListener() {
                        @Override
                        public void onUnFollow() {
                            try {
                                mData.remove(helper.getAdapterPosition() - getHeaderLayoutCount());
                                notifyItemRemoved(helper.getAdapterPosition());
                                FollowStatusChangedImpl.followUser(friendsBean.userId, FollowStatusChangedImpl.ACTION_TYPE_CANCEL_FOLLOW, friendsBean.nickName, friendsBean.avatar);
                                SocialLogUtils.getInstance().traceSocialData("my.follow", pageid, "unfollow", from_page, from_page_id);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    mUnFollowPopupWindow.showAtLocation(v, Gravity.BOTTOM, 0, 0);
                } else if (listType.equals(FollowingAndFansFragment.TYPE_FRIEND)) {
                    Intent intent = new Intent(mContext, ChatActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("toUserId", friendsBean.userId);
                    bundle.putString("toName", friendsBean.nickName);
                    bundle.putString("toAvatar", friendsBean.avatar);
                    intent.putExtras(bundle);
                    mContext.startActivity(intent);
                    SocialLogUtils.getInstance().traceSocialData("my.friend", pageid, "click_message", from_page, from_page_id);
                } else if (listType.equals(FollowingAndFansFragment.TYPE_FANS)) {
                    if (friendsBean.followStatus == 3) {
                        UnFollowPopupWindow mUnFollowPopupWindow = new UnFollowPopupWindow(mContext, friendsBean.nickName);
                        mUnFollowPopupWindow.setOnUnFollowListener(new UnFollowPopupWindow.OnUnFollowListener() {
                            @Override
                            public void onUnFollow() {
                                friendsBean.followStatus = 2;
                                notifyItemChanged(helper.getAdapterPosition());
                                FollowStatusChangedImpl.followUser(friendsBean.userId, FollowStatusChangedImpl.ACTION_TYPE_CANCEL_FOLLOW, friendsBean.nickName, friendsBean.avatar);
                            }
                        });
                        SocialLogUtils.getInstance().traceSocialData("my.fans", pageid, "unfollow", from_page, from_page_id);

                        mUnFollowPopupWindow.showAtLocation(v, Gravity.BOTTOM, 0, 0);
                    } else {
                        friendsBean.followStatus = 3;
                        notifyItemChanged(helper.getAdapterPosition());
                        FollowStatusChangedImpl.followUser(friendsBean.userId, FollowStatusChangedImpl.ACTION_TYPE_FOLLOW, friendsBean.nickName, friendsBean.avatar);
                        FireBaseUtils.uploadGoogle(TheLConstants.FireBaseConstant.ATTENTION, TheLApp.context);
                        SocialLogUtils.getInstance().traceSocialData("my.fans", pageid, "follow", from_page, from_page_id);

                    }
                } else if (listType.equals(AwaitLiveActivity.TYPE_AWAIt_LIVE)) {
                    switch (friendsBean.followStatus) {
                        case 0:
                            FollowStatusChangedImpl.followUser(friendsBean.userId, FollowStatusChangedImpl.ACTION_TYPE_FOLLOW, friendsBean.nickName, friendsBean.avatar);
                            FireBaseUtils.uploadGoogle(TheLConstants.FireBaseConstant.ATTENTION, TheLApp.context);

                            friendsBean.followStatus = 1;
                            notifyDataSetChanged();
                            break;
                        case 2:
                            FollowStatusChangedImpl.followUser(friendsBean.userId, FollowStatusChangedImpl.ACTION_TYPE_FOLLOW, friendsBean.nickName, friendsBean.avatar);
                            FireBaseUtils.uploadGoogle(TheLConstants.FireBaseConstant.ATTENTION, TheLApp.context);

                            friendsBean.followStatus = 3;
                            notifyDataSetChanged();
                            break;
                        case 1:
                            UnFollowPopupWindow mUnFollowPopupWindow = new UnFollowPopupWindow(mContext, friendsBean.nickName);
                            mUnFollowPopupWindow.setOnUnFollowListener(new UnFollowPopupWindow.OnUnFollowListener() {
                                @Override
                                public void onUnFollow() {
                                    FollowStatusChangedImpl.followUser(friendsBean.userId, FollowStatusChangedImpl.ACTION_TYPE_CANCEL_FOLLOW, friendsBean.nickName, friendsBean.avatar);
                                    friendsBean.followStatus = 0;
                                    notifyDataSetChanged();
                                }
                            });
                            mUnFollowPopupWindow.showAtLocation(v, Gravity.BOTTOM, 0, 0);
                            break;
                        case 3:
                            UnFollowPopupWindow mUnFollowPopupWindow1 = new UnFollowPopupWindow(mContext, friendsBean.nickName);
                            mUnFollowPopupWindow1.setOnUnFollowListener(new UnFollowPopupWindow.OnUnFollowListener() {
                                @Override
                                public void onUnFollow() {
                                    FollowStatusChangedImpl.followUser(friendsBean.userId, FollowStatusChangedImpl.ACTION_TYPE_CANCEL_FOLLOW, friendsBean.nickName, friendsBean.avatar);
                                    friendsBean.followStatus = 2;
                                    notifyDataSetChanged();
                                }
                            });
                            mUnFollowPopupWindow1.showAtLocation(v, Gravity.BOTTOM, 0, 0);
                            break;
                    }
                } else {
                    switch (friendsBean.followStatus) {
                        case 0:
                        case 2:
                            mData.remove(helper.getAdapterPosition());
                            notifyItemRemoved(helper.getAdapterPosition());
                            FollowStatusChangedImpl.followUser(friendsBean.userId, FollowStatusChangedImpl.ACTION_TYPE_FOLLOW, friendsBean.nickName, friendsBean.avatar);
                            FireBaseUtils.uploadGoogle(TheLConstants.FireBaseConstant.ATTENTION, TheLApp.context);
                            SocialLogUtils.getInstance().traceSocialData("my.follow", pageid, "follow", from_page, from_page_id);

                            break;
                        case 1:
                        case 3:
                            UnFollowPopupWindow mUnFollowPopupWindow = new UnFollowPopupWindow(mContext, friendsBean.nickName);
                            mUnFollowPopupWindow.setOnUnFollowListener(new UnFollowPopupWindow.OnUnFollowListener() {
                                @Override
                                public void onUnFollow() {
                                    mData.remove(helper.getAdapterPosition() - getHeaderLayoutCount());
                                    notifyItemRemoved(helper.getAdapterPosition());
                                    FollowStatusChangedImpl.followUser(friendsBean.userId, FollowStatusChangedImpl.ACTION_TYPE_CANCEL_FOLLOW, friendsBean.nickName, friendsBean.avatar);
                                }
                            });
                            mUnFollowPopupWindow.showAtLocation(v, Gravity.BOTTOM, 0, 0);
                            break;
                    }
                }
            }
        });


        /*if (friendsBean.secretly > 0) {//如果是悄悄关注

            helper.setVisibility(R.id.txt_secretly_follow, View.VISIBLE);
            helper.setText(R.id.txt_secretly_follow, "(" + TheLApp.getContext().getString(R.string.secretly_follow) + ")");
            helper.setVisibility(R.id.txt_desc, View.GONE);
        } else {
            helper.setVisibility(R.id.txt_secretly_follow, View.GONE);
            helper.setVisibility(R.id.txt_desc, View.VISIBLE);

        }*/
        if (mOnItemLongClickListener != null) {
            helper.setOnLongClickListener(R.id.item, new View.OnLongClickListener() {

                @Override
                public boolean onLongClick(View v) {
                    int position = helper.getLayoutPosition();
                    mOnItemLongClickListener.onItemLongClick(v, position, friendsBean);

                    return true;
                }
            });
        }
    }

    private OnItemLongClickListener mOnItemLongClickListener;

    public void setOnItemLongClickListener(OnItemLongClickListener mOnItemLongClickListener) {
        this.mOnItemLongClickListener = mOnItemLongClickListener;
    }

    public interface OnItemLongClickListener {
        void onItemLongClick(View view, int position, FriendsBean item);
    }
}
