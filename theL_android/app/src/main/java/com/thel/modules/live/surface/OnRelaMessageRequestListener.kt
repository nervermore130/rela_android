package com.thel.modules.live.surface

import com.thel.R
import com.thel.app.TheLApp
import com.thel.bean.LiveInfoLogBean
import com.thel.chat.live.interfaces.OnMessageRequestListener
import com.thel.modules.live.bean.LiveRoomMsgBean
import com.thel.modules.live.utils.LiveUtils
import com.thel.utils.DialogUtil
import com.thel.utils.ToastUtils
import org.json.JSONObject
import java.lang.Exception

abstract class OnRelaMessageRequestListener : OnMessageRequestListener {
    override fun onError(error: Throwable?) {
        error?.let {
            try {
                LiveInfoLogBean.getInstance().liveMsgLogAnalytics.log = it.message
                LiveInfoLogBean.getInstance().liveMsgLogAnalytics.logType = "tencent_error_log"
                LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().liveMsgLogAnalytics)
            }catch (e : Exception){
                e.printStackTrace()
            }

        }

        onMessageError(error)
    }

    override fun onSuccess(code: String, payload: String) {

        when (code) {
            LiveRoomMsgBean.TYPE_ERRORALERT -> {

                val jsonObject = JSONObject(payload)

                if (jsonObject.has("errdesc")) {

                    val errdesc = jsonObject.getString("errdesc")

                    ToastUtils.showToastShort(TheLApp.context, errdesc)
                }

            }

            LiveRoomMsgBean.TYPE_BANED -> DialogUtil.showToastShort(TheLApp.context, TheLApp.context.getString(R.string.live_you_have_been_baned))
            else -> onMessageSuccess(code, payload)
        }
    }

    abstract fun onMessageError(error: Throwable?)

    abstract fun onMessageSuccess(code: String, payload: String)

}