package com.thel.modules.main.messages.utils;

import android.util.Log;

import com.thel.bean.FriendListBean;
import com.thel.imp.friend.FriendListContract;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestConstants;
import com.thel.network.service.DefaultRequestService;
import com.thel.utils.L;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by chad
 * Time 18/3/23
 * Email: wuxianchuang@foxmail.com
 * Description: TODO 缓存好友列表,用于聊天模块陌生人判断
 * TODO 去掉FriendUtils
 */

public class FriendRelationManager {

    private static final String TAG = "FriendRelationManager";

    private FriendRelationManager() {

    }

    private static FriendRelationManager instance;

    public synchronized static FriendRelationManager getInstance() {
        if (instance == null) instance = new FriendRelationManager();
        return instance;
    }


    /**
     * 从网络中获取自己的好友列表
     */
    public void getNetRelationFriendList(final FriendListContract.FriendListRefreshCallBack callBack) {
        final Flowable<FriendListBean> flowable = DefaultRequestService.createCommonRequestService().getNetFriendList(RequestConstants.RELATION_TYPE_FRIEND);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<FriendListBean>() {

            @Override
            public void onNext(FriendListBean friendListBean) {

                super.onNext(friendListBean);

                L.d(TAG, " getNetRelationFriendList friendListBean : " + friendListBean);

                if (friendListBean != null && friendListBean.data != null && friendListBean.data.list != null) {


                    if (callBack != null) {//注意，此时在子线程
                        callBack.onFriendListRefresh(friendListBean.data.list);
                    }

                }


            }
        });
    }
}
