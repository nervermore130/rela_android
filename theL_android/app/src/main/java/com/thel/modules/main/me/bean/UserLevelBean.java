package com.thel.modules.main.me.bean;

import com.thel.base.BaseDataBean;

/**
 * 用户等级bean
 * Created by lingwei on 2018/1/17.
 */

public class UserLevelBean extends BaseDataBean {

    /**
     * data : {"userId":"103351770","level":0,"exp":0,"diamond":0,"watchTime":0,"shareCount":0,"currentExpPercent":0}
     */

    public UserLevelDataBean data;

    public static class UserLevelDataBean {
        @Override
        public String toString() {
            return "UserLevelDataBean{" + "userId='" + userId + '\'' + ", level=" + level + ", exp=" + exp + ", diamond=" + diamond + ", watchTime=" + watchTime + ", shareCount=" + shareCount + ", currentExpPercent=" + currentExpPercent + '}';
        }

        /**
         * userId : 103351770
         * level : 0
         * exp : 0
         * diamond : 0
         * watchTime : 0
         * shareCount : 0
         * currentExpPercent : 0
         */

        public int userId;
        public int level = 0;
        public int exp;
        public int diamond;
        public float watchTime;
        public int shareCount;
        public int entrySwitch; //1默认是关闭 0是打开
        public int levelIconSwitch;//默认是打开 1  //0是关闭
        public float currentExpPercent;
        public int levelUpRequire;// 升下一级所需经验
        public int nextLevelHasGot;// 当前已获得经验
        public int isCloaking; //榜单隐身
    }
}
