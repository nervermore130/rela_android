package com.thel.modules.main.discover.view;

import android.content.Context;
import android.graphics.Typeface;
import androidx.annotation.Nullable;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.live.bean.LiveClassifyBean;
import com.thel.utils.L;

/**
 * 直播分类可滚动的顶部头布局
 * Created by lingwei on 2017/11/19.
 */

public class LiveClassifyTabTitleView extends LinearLayout {
    private Context mContext;
    private ImageView img;
    private TextView txt_title;
    private LiveClassifyBean bean;
    private boolean mSelect = false;
    private TextPaint textPaint;

    public LiveClassifyTabTitleView(Context context) {
        this(context, null);
    }

    public LiveClassifyTabTitleView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LiveClassifyTabTitleView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.live_classfy_tab_title_view, this);
        img = findViewById(R.id.img);
        txt_title = findViewById(R.id.txt_title);
        textPaint = txt_title.getPaint();
    }

    public LiveClassifyTabTitleView initView(LiveClassifyBean bean) {
        this.bean = bean;
        if (bean == null) {
            return this;
        }

        L.d("LiveClassifyTabTitleView", " bean.nativeUnSelectedIcon : " + bean.nativeUnSelectedIcon);

        if (bean.nativeUnSelectedIcon != 0) {
            img.setImageResource(bean.nativeUnSelectedIcon);
        } else if (!TextUtils.isEmpty(bean.icon)) {
            //            ImageLoaderManager.imageLoaderWithSimpleTarget(img, bean.icon, TheLConstants.AVATAR_SMALL_SIZE, TheLConstants.AVATAR_SMALL_SIZE);
            ImageLoaderManager.imageLoader(img, bean.icon);

        }
        txt_title.setText(bean.name);
        return this;
    }

    public LiveClassifyTabTitleView setSelect(boolean isSelect) {
        if (isSelect) {
            txt_title.setTypeface(Typeface.DEFAULT_BOLD);
            if (bean.nativeIcon != 0) {
                img.setImageResource(bean.nativeIcon);
            } else if (!TextUtils.isEmpty(bean.selectedIcon)) {
                ImageLoaderManager.imageLoader(img, bean.selectedIcon);
                //                ImageLoaderManager.imageLoaderWithSimpleTarget(img, bean.selectedIcon, TheLConstants.AVATAR_SMALL_SIZE, TheLConstants.AVATAR_SMALL_SIZE);
            }
        } else {
            txt_title.setTypeface(Typeface.DEFAULT);

            if (bean.nativeUnSelectedIcon != 0) {
                img.setImageResource(bean.nativeUnSelectedIcon);
            } else if (!TextUtils.isEmpty(bean.icon)) {
                ImageLoaderManager.imageLoader(img, bean.icon);
                //                ImageLoaderManager.imageLoaderWithSimpleTarget(img, bean.icon, TheLConstants.AVATAR_SMALL_SIZE, TheLConstants.AVATAR_SMALL_SIZE);

            }
        }
        return this;
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);
        if (mSelect != selected) {
            mSelect = selected;
            setSelect(selected);
        }
    }
}
