package com.thel.modules.main.me.bean;

import android.content.ContentValues;
import android.database.Cursor;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.user.UserInfoLiveBean;
import com.thel.modules.main.messages.db.DataBaseAdapter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

public class FriendsBean implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final int FRIEND_TYPE_FRIENDS = 0;
    public static final int FRIEND_TYPE_FOLLOW = 1;
    public static final int FRIEND_TYPE_FANS = 2;
    /**
     * 用户id
     */
    public String userId;


    /**
     * 是否关注这个用户,1:已关注，0未关注
     */
    public int isFollow = 1;

    /**
     * 0 互相未关注；
     * 1 我关注对方；
     * 2 对方关注我；
     * 3 互相关注；
     */
    public int followStatus = 1;
    @Override
    public String toString() {
        return "FriendsDataBean{" + "userId='" + userId + '\'' + ", online='" + online + '\'' + ", nickName='" + nickName + '\'' + ", userNamePinYin='" + userNamePinYin + '\'' + ", roleName='" + roleName + '\'' + ", avatar='" + avatar + '\'' + ", distance='" + distance + '\'' + ", intro='" + intro + '\'' + ", unit='" + unit + '\'' + ", loginTime='" + loginTime + '\'' + ", friendType=" + friendType + ", verifyType=" + verifyType + ", verifyIntro='" + verifyIntro + '\'' + ", age=" + age + ", hiding=" + hiding + ", level=" + level + ", affection=" + affection + ", secretly=" + secretly + '}';
    }

    /**
     * 是否在线( 0 不在,1 在,2忙)
     */
    public String online;

    /**
     * 用户名
     */
    public String nickName;

    /**
     * 用户名(拼音，用于排序)
     */
    public String userNamePinYin;

    /**
     * 角色
     */
    public String roleName;

    /**
     * 头像
     */
    public String avatar;

    /**
     * 距离
     */
    public String distance;

    public String intro;
    // days 和 hours,和loginTime组合使用，标识几天前登陆或几小时前登陆
    public String unit;
    public String loginTime;

    public int friendType;

    /**
     * 认证状态，是否是加V用户 0:否 1:个人V 2:企业V
     */
    public int verifyType;
    /**
     * 认证说明
     */
    public String verifyIntro;
    /**
     * 年龄
     */
    public int age;
    /**
     * 是否开启了隐身
     */
    public int hiding;
    /**
     * 会员等级
     */
    public int level;
    /**
     * 感情状态
     */
    public int affection;
    /**
     * 悄悄关注（0:未悄悄关注，1：悄悄关注）
     */
    public int secretly = 0;

    public UserInfoLiveBean liveInfo;

    public boolean awaitStatus;


    public String getLoginTimeShow() {
        StringBuilder str = new StringBuilder();
        if (level == 0 || hiding == 0) {// 如果是会员并且开了隐身功能，则不显示距离
            str.append(distance);
            str.append(", ");
        }
        str.append(age);
        str.append(TheLApp.getContext().getString(R.string.updatauserinfo_activity_age_unit));
        ArrayList<String> relationship_list = new ArrayList<>(Arrays.asList(TheLApp.getContext().getResources().getStringArray(R.array.userinfo_relationship))); // 感情状态
        str.append(", ");
        try {
            str.append(relationship_list.get(affection));
        } catch (Exception e) {
            str.append(relationship_list.get(0));
        }

        return str.toString();
    }

    public void fromCursor(Cursor cursor) {
        userId = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.F_USERID));
        online = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.F_ONLINE));
        nickName = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.F_USERNAME));
        userNamePinYin = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.F_USERNAME_PINYIN));
        avatar = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.F_AVATAR));
        roleName = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.F_ROLE_NAME));
        distance = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.F_DISTANCE));
        intro = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.F_USER_INTRO));
        unit = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.F_UNIT));
        loginTime = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.F_LOGIN_TIME));
        friendType = cursor.getInt(cursor.getColumnIndex(DataBaseAdapter.F_FRIEND_TYPE));
        verifyType = cursor.getInt(cursor.getColumnIndex(DataBaseAdapter.F_VERIFY_TYPE));
        verifyIntro = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.F_VERIFY_INTRO));
        age = cursor.getInt(cursor.getColumnIndex(DataBaseAdapter.F_AGE));
        level = cursor.getInt(cursor.getColumnIndex(DataBaseAdapter.F_LEVEL));
        affection = cursor.getInt(cursor.getColumnIndex(DataBaseAdapter.F_AFFECTION));
        hiding = cursor.getInt(cursor.getColumnIndex(DataBaseAdapter.F_HIDING));
    }

    public ContentValues getContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DataBaseAdapter.F_USERID, userId);
        contentValues.put(DataBaseAdapter.F_ONLINE, online);
        contentValues.put(DataBaseAdapter.F_USERNAME, nickName);
        contentValues.put(DataBaseAdapter.F_USERNAME_PINYIN, userNamePinYin);
        contentValues.put(DataBaseAdapter.F_AVATAR, avatar);
        contentValues.put(DataBaseAdapter.F_ROLE_NAME, roleName);
        contentValues.put(DataBaseAdapter.F_DISTANCE, distance);
        contentValues.put(DataBaseAdapter.F_USER_INTRO, intro);
        contentValues.put(DataBaseAdapter.F_UNIT, unit);
        contentValues.put(DataBaseAdapter.F_LOGIN_TIME, loginTime);
        contentValues.put(DataBaseAdapter.F_FRIEND_TYPE, friendType);
        contentValues.put(DataBaseAdapter.F_VERIFY_TYPE, verifyType);
        contentValues.put(DataBaseAdapter.F_VERIFY_INTRO, verifyIntro);
        contentValues.put(DataBaseAdapter.F_AGE, age);
        contentValues.put(DataBaseAdapter.F_HIDING, hiding);
        contentValues.put(DataBaseAdapter.F_LEVEL, level);
        contentValues.put(DataBaseAdapter.F_AFFECTION, affection);
        return contentValues;


    }

}
