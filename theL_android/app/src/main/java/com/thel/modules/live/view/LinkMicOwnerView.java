package com.thel.modules.live.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.utils.ScreenUtils;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by liuyun on 2017/12/18.
 */

public class LinkMicOwnerView extends RelativeLayout {

    private String toUserId;

    public LinkMicOwnerView(Context context) {
        super(context);
        init(context);
    }

    public LinkMicOwnerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public LinkMicOwnerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @Override protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int screenWidth = ScreenUtils.getScreenWidth(TheLApp.context) - 3;

        int screenHeight = ScreenUtils.getScreenHeight(TheLApp.context);

        int width = (int) (screenWidth * 0.5);

        int height = (int) (screenHeight * 0.375);

        widthMeasureSpec = MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY);

        heightMeasureSpec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    private void init(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_link_mic_owner_view, this, true);
        ButterKnife.bind(this, view);
    }

    public void show(String toUserId) {

        this.setVisibility(View.VISIBLE);

        this.toUserId = toUserId;

    }

    public void hide() {
        setVisibility(GONE);
    }

    private LinkMicInfoView.OnLinkMicHangupClickListener mOnLinkMicClickListener;

    public void setOnLinkMicClickListener(LinkMicInfoView.OnLinkMicHangupClickListener mOnLinkMicClickListener) {
        this.mOnLinkMicClickListener = mOnLinkMicClickListener;
    }

    @OnClick(R.id.hang_up_tv) void onHangupClick() {
        if (mOnLinkMicClickListener != null) {
            mOnLinkMicClickListener.onHangup(toUserId);
        }
    }

}
