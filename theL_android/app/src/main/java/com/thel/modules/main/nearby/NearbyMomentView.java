package com.thel.modules.main.nearby;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.view.SimpleDraweeView;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.moments.MomentsBean;
import com.thel.constants.MomentTypeConstants;
import com.thel.constants.TheLConstants;
import com.thel.modules.main.nearby.nearbymoment.NearbyMomentActivity;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;
import com.thel.utils.AppInit;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * Created by waiarl on 2017/4/21.
 */

public class NearbyMomentView extends LinearLayout {
    private final Context mContext;
    private RecyclerView recyclerView;
    private StaggeredGridLayoutManager manager;
    //private List<MomentsBean> list = new ArrayList<>();
    private List<MomentsBean> list = new ArrayList<>();
    private List<MomentsBean> momentsList = new ArrayList<>();
    private NearbyMomentViewAdapter adapter;
    private final int NINE_PALACES_COUNT = 9;//附近九宫格最多


    public NearbyMomentView(Context context) {
        this(context, null);
    }

    public NearbyMomentView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NearbyMomentView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
        setListener();
    }

    private void init() {
        inflate(mContext, R.layout.nearby_moment_view, this);
        recyclerView = findViewById(R.id.recyclerview);
        manager = new StaggeredGridLayoutManager(3, VERTICAL);
        recyclerView.setLayoutManager(manager);
        recyclerView.setHasFixedSize(true);
        adapter = new NearbyMomentViewAdapter(momentsList);
        recyclerView.setAdapter(adapter);
    }

    //    public NearbyMomentView initView(List<MomentsBean> beanList) {
    //        if (beanList == null) {
    //            return this;
    //        }
    //        list.clear();
    //        list.addAll(beanList);
    //        adapter.setNewData(list);
    //        return this;
    //    }
    public NearbyMomentView initView(List<MomentsBean> beanList) {
        if (beanList == null) {
            return this;
        }
        list.clear();
        list.addAll(beanList);
        momentsList.clear();
        final int size = list.size();
        if (size < NINE_PALACES_COUNT) {
            momentsList.addAll(list);
        } else {
            for (int i = 0; i < NINE_PALACES_COUNT; i++) {
                momentsList.add(list.get(i));
            }
        }
        adapter.setNewData(momentsList);
        return this;
    }

    public List<MomentsBean> getAdapterData() {
        return adapter.getData();
    }


    /**
     * 过滤新添加的黑名单
     *
     * @param userId
     */
    public void filterNewBlack(String userId) {
        if (TextUtils.isEmpty(userId) || userId.equals("0")) {
            return;
        }
        int index = 0;
        final Iterator<MomentsBean> iter = adapter.getData().iterator();
        while (iter.hasNext()) {//adapter中移除
            final MomentsBean momentsBean = iter.next();
            if (userId.equals(momentsBean.userId + "")) {
                iter.remove();
                adapter.notifyItemRemoved(index);
                index -= 1;
            }
            index += 1;
        }
        final Iterator<MomentsBean> iter1 = list.iterator();
        while (iter1.hasNext()) {//原列表移除
            final MomentsBean momentsBean = iter1.next();
            if (userId.equals(momentsBean.userId + "")) {
                iter1.remove();
            }
        }
        final int listSize = list.size();
        final int aSize = adapter.getData().size();
        for (int i = aSize; i < NINE_PALACES_COUNT && i < listSize; i++) {
            adapter.getData().add(list.get(i));
        }
        adapter.notifyDataSetChanged();
    }

    /**
     * 屏蔽单个日志
     *
     * @param momentId
     */
    public void filterNewMoment(String momentId) {
        if (TextUtils.isEmpty(momentId)) {
            return;
        }
        int index = 0;
        final Iterator<MomentsBean> iter = adapter.getData().iterator();
        while (iter.hasNext()) {//adapter中移除
            final MomentsBean momentsBean = iter.next();
            if (momentId.equals(momentsBean.momentsId)) {
                iter.remove();
                adapter.notifyItemRemoved(index);
                index -= 1;
            }
            index += 1;
        }
        final Iterator<MomentsBean> iter1 = list.iterator();
        while (iter1.hasNext()) {//原列表移除
            final MomentsBean momentsBean = iter1.next();
            if (momentId.equals(momentsBean.momentsId)) {
                iter1.remove();
            }
        }
        final int listSize = list.size();
        final int aSize = adapter.getData().size();
        for (int i = aSize; i < NINE_PALACES_COUNT && i < listSize; i++) {
            adapter.getData().add(list.get(i));
        }
        adapter.notifyDataSetChanged();

    }

    private void setListener() {
        adapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                gotoMoment(position);
            }
        });
    }

    private void gotoMoment(int position) {
        final MomentsBean bean = adapter.getItem(position);
        //根据momentId来决定

        //        if (position == 0) {
        gotoNearbyMomentActivity(bean.momentsId);
        //        } else {
        //           gotoMomentCommentActivity(bean);
        //        }
    }

    private void gotoNearbyMomentActivity(String momentsId) {
        if (mContext == null) {
            return;
        }
        final Intent intent = new Intent(mContext, NearbyMomentActivity.class);
        final Bundle bundle = new Bundle();
        bundle.putString(TheLConstants.BUNDLE_KEY_MOMENT_ID, momentsId);
        intent.putExtras(bundle);
        mContext.startActivity(intent);
    }

    private void gotoMomentCommentActivity(MomentsBean bean) {
       /* if (bean == null) {
            return;
        }
        Intent intent;
        if (MomentsBean.MOMENT_TYPE_THEME.equals(bean.momentsType))
            intent = new Intent(mContext, ThemeDetailActivity.class);
        else
            intent = new Intent(mContext, MomentCommentActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(TheLConstants.BUNDLE_KEY_MOMENT_ID, bean.momentsId);
        intent.putExtras(bundle);
        mContext.startActivity(intent);*/

    }

    class NearbyMomentViewAdapter extends BaseRecyclerViewAdapter<MomentsBean> {
        private float photoWidth; // 图片宽度
        private float photoHeigth; // 图片高度
        private float radius; // 图片圆角半径

        public NearbyMomentViewAdapter(List<MomentsBean> data) {
            super(R.layout.nearby_moment_item, data);
            photoWidth = (AppInit.displayMetrics.widthPixels - TheLApp.getContext().getResources().getDimension(R.dimen.nearby_user_view_padding_2x) * 4) / 3;
            photoHeigth = photoWidth;
            radius = TheLApp.getContext().getResources().getDimension(R.dimen.nearby_user_view_radius);
        }

        private void setDefaultVisibleState(BaseViewHolder baseViewHolder) {
            baseViewHolder.setVisible(R.id.txt_distance, true);
            baseViewHolder.setVisible(R.id.txt_nickname, false);
            baseViewHolder.setVisible(R.id.mask, false);
            hideTxtMoment(baseViewHolder);
            SimpleDraweeView img = baseViewHolder.getView(R.id.img);
            img.setVisibility(View.VISIBLE);
        }


        @Override
        protected void convert(BaseViewHolder baseViewHolder, MomentsBean momentsBean) {
            final int headConnt = getHeaderLayoutCount();
            final int pos = baseViewHolder.getLayoutPosition() - headConnt;
            setDefaultVisibleState(baseViewHolder);
            TextView txt_distance = baseViewHolder.convertView.findViewById(R.id.txt_distance);
            txt_distance.getPaint().setFakeBoldText(true);
            try {
                if (momentsBean.distance != null && (momentsBean.distance.contains(" Km") || momentsBean.distance.contains(" km"))) {
                    float dis = -1f;
                    if (momentsBean.distance.contains(" Km")) {
                        dis = Float.valueOf(momentsBean.distance.substring(0, momentsBean.distance.indexOf(" Km")));
                    } else if (momentsBean.distance.contains(" km")) {
                        dis = Float.valueOf(momentsBean.distance.substring(0, momentsBean.distance.indexOf(" km")));
                    }
                    if (dis <= 5) {
                        if (dis == -1f) {
                            baseViewHolder.setText(R.id.txt_distance, momentsBean.distance);
                        } else {
                            baseViewHolder.setText(R.id.txt_distance, momentsBean.distance);
                        }
                    } else {
                        baseViewHolder.setText(R.id.txt_distance, momentsBean.distance);
                    }
                } else {
                    baseViewHolder.setText(R.id.txt_distance, momentsBean.distance);
                }
            } catch (Exception e) {
                baseViewHolder.setText(R.id.txt_distance, momentsBean.distance);
            }

            TextView txt_nickname = baseViewHolder.convertView.findViewById(R.id.txt_nickname);
            txt_nickname.getPaint().setFakeBoldText(true);
            baseViewHolder.setText(R.id.txt_nickname, momentsBean.nickname);

            FrameLayout img_with_mask = baseViewHolder.getView(R.id.img_with_mask);
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams((int) photoWidth, (int) photoHeigth);
            img_with_mask.setLayoutParams(lp);

            final SimpleDraweeView img = baseViewHolder.getView(R.id.img);
            img.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadius(radius));
            final String[] urls = momentsBean.imageUrl.split(",");
            boolean isUrlExist = false;
            if (!TextUtils.isEmpty(urls[0])) {
                isUrlExist = true;
                baseViewHolder.setImageUrl(R.id.img, urls[0], photoWidth, photoHeigth);
                hideTxtMoment(baseViewHolder);
            } else {
                isUrlExist = false;
                baseViewHolder.setImageResource(R.id.img, R.drawable.bg_ragius_shade_5_shape);
                showTxtMoment(baseViewHolder, momentsBean);
            }

            switch (momentsBean.momentsType) {
                case MomentTypeConstants.MOMENT_TYPE_TEXT://纯文本
                    if (!isUrlExist)
                        setShowInfo(baseViewHolder, momentsBean, img);
                    break;
                case MomentTypeConstants.MOMENT_TYPE_IMAGE://图片日志
                    break;
                case MomentTypeConstants.MOMENT_TYPE_TEXT_IMAGE://带文本的图片日志
                    break;
                case MomentTypeConstants.MOMENT_TYPE_VOICE://纯音乐日志
                    if (!TextUtils.isEmpty(momentsBean.albumLogo444))
                        baseViewHolder.setImageUrl(R.id.img, momentsBean.albumLogo444, photoWidth, photoHeigth);
                    break;
                case MomentTypeConstants.MOMENT_TYPE_TEXT_VOICE://带文字的音乐日志
                    if (!TextUtils.isEmpty(momentsBean.albumLogo444))
                        baseViewHolder.setImageUrl(R.id.img, momentsBean.albumLogo444, photoWidth, photoHeigth);
                    hideTxtMoment(baseViewHolder);
                    break;
                case MomentTypeConstants.MOMENT_TYPE_THEME://话题日志
                    if (isUrlExist)
                        hideTxtMoment(baseViewHolder);
                    break;
                case MomentTypeConstants.MOMENT_TYPE_THEME_REPLY:// 参与话题
                    if (!isUrlExist)
                        setShowInfo(baseViewHolder, momentsBean, img);
                    break;
                case MomentTypeConstants.MOMENT_TYPE_VIDEO:// 视频
                    if (isUrlExist)
                        hideTxtMoment(baseViewHolder);
                    break;
                case MomentTypeConstants.MOMENT_TYPE_LIVE:// 直播
                    if (isUrlExist)
                        hideTxtMoment(baseViewHolder);
                    break;
                case MomentTypeConstants.MOMENT_TYPE_USER_CARD:// 推荐用户卡片
                    if (!isUrlExist)
                        setShowInfo(baseViewHolder, momentsBean, img);

                    break;
                case MomentTypeConstants.MOMENT_TYPE_RECOMMEND:// 推荐日志（转发）
                    if (!isUrlExist)
                        setShowInfo(baseViewHolder, momentsBean, img);
                    break;
                case MomentTypeConstants.MOMENT_TYPE_WEB://网页日志
                    if (!isUrlExist)
                        setShowInfo(baseViewHolder, momentsBean, img);
                    break;
                case MomentTypeConstants.MOMENT_TYPE_LIVE_USER://直播用户
                    if (isUrlExist)
                        hideTxtMoment(baseViewHolder);
                    break;
                default:
                    break;
            }

            baseViewHolder.getView(R.id.lin_shade).setLayoutParams(lp);
            View rel_info = baseViewHolder.getView(R.id.rel_info);
            RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams) rel_info.getLayoutParams();
            param.width = (int) photoWidth;
        }
    }


    /*显示头像(如果没有头像显示默认的头像)*/
    private void setShowInfo(BaseViewHolder baseViewHolder, MomentsBean momentsBean, SimpleDraweeView img) {
        if (!TextUtils.isEmpty(momentsBean.avatar)) {
            baseViewHolder.setImageUrl(R.id.img, momentsBean.avatar);
        } else {
            baseViewHolder.setImageResource(R.id.img, R.drawable.default_avatar_radius_shade_5_shape);
        }
    }

    private void showTxtMoment(BaseViewHolder baseViewHolder, MomentsBean momentsBean) {
        baseViewHolder.setVisible(R.id.mask, true);
        TextView various_txt_moment = baseViewHolder.convertView.findViewById(R.id.various_txt_moment);
        various_txt_moment.getPaint().setFakeBoldText(true);
        baseViewHolder.setText(R.id.various_txt_moment, momentsBean.momentsText);
        baseViewHolder.setVisible(R.id.various_txt_moment, true);
    }

    private void hideTxtMoment(BaseViewHolder baseViewHolder) {
        baseViewHolder.setVisible(R.id.various_txt_moment, false);
    }
}
