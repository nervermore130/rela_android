package com.thel.modules.live.liveBigGiftAnimLayout.video;

import android.media.MediaPlayer;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.thel.R;
import com.thel.base.BaseFragment;

import java.io.IOException;

import baidu.measure.TextureRenderView;

public class LiveVideoAnimSurFragment extends BaseFragment {

    public static final String TAG = "EditActivity";
    private static LiveVideoAnimSurFragment instance;

    private TextureRenderView textureView;


    private int currentFilterId = -1;
    //是否正在编辑中
    private boolean isEdit = false;
    private MediaPlayer mMediaPlayer;
    private GiftFilterSurfaceView giftFilterSurfaceView;
    public static LiveVideoAnimSurFragment getInstance() {
        instance = new LiveVideoAnimSurFragment();
        return instance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_live_anim_video_view, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initMediaPlayer("");
        initVideoView(view);
    }


    private void initVideoView(View rootView) {
        FrameLayout video_container = rootView.findViewById(R.id.video_container);
        giftFilterSurfaceView = new GiftFilterSurfaceView(getContext());

        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(900, 1600);
        layoutParams.gravity = Gravity.CENTER;

        video_container.addView(giftFilterSurfaceView, layoutParams);

//        giftFilterSurfaceView.setMediaPlayer(mMediaPlayer);

        giftFilterSurfaceView.setSourceSize(900, 1600);
    }

    private void initMediaPlayer(String url) {
        url = "http://pro.thel.co/gift/video/1514199928405jendak.mp4";
        mMediaPlayer = new MediaPlayer();
        try {
            mMediaPlayer.setDataSource(url);
            mMediaPlayer.setLooping(true);
            mMediaPlayer.prepareAsync();
            mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mMediaPlayer.start();
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
