package com.thel.modules.live.stream;

import com.thel.modules.live.bean.SoftEnjoyNetBean;
import com.thel.modules.live.bean.SoftGiftBean;
import com.thel.utils.GsonUtils;
import com.thel.utils.ShareFileUtils;

import org.lasque.tusdk.core.secret.TuSDKOnlineStickerDownloader;
import org.lasque.tusdk.core.type.DownloadTaskStatus;
import org.lasque.tusdk.modules.view.widget.sticker.StickerGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chad
 * Time 19/1/11
 * Email: wuxianchuang@foxmail.com
 * Description: TODO 提前下载贴纸
 */
public class StickerPreDownload {

    private int preDownCount;

    private List<SoftGiftBean> preStickGroupList;

    /**
     * 获取贴纸下载器
     */
    private TuSDKOnlineStickerDownloader mDownLoader;

    public StickerPreDownload() {
        preStickGroupList = new ArrayList<>();
        if (mDownLoader == null) {
            mDownLoader = new TuSDKOnlineStickerDownloader();
            mDownLoader.setDelegate(new TuSDKOnlineStickerDownloader.TuSDKOnlineStickerDownloaderDelegate() {

                @Override
                public void onDownloadProgressChanged(long stickerGroupId, float progress, DownloadTaskStatus
                        status) {
                    if (status == DownloadTaskStatus.StatusDowned && preDownCount > 1) {
                        preDownCount--;
                        downloadStickerGroup(preStickGroupList.get(preDownCount - 1));
                    }
                    float allSize = preStickGroupList.size();
                    float hasDownSize = preStickGroupList.size() - preDownCount;
                    int oldProgress = (int) (hasDownSize / allSize * 100);
                    int curentProgress = (int) (progress * 100 / allSize);
                    int finalProgress = curentProgress + oldProgress;
                    if (finalProgress >= 98) finalProgress = 100;
                    if (downloadListener != null) {
                        downloadListener.progress(finalProgress);
                    }
                }
            });
        }
    }

    /**
     * @param downloadListener
     * @return 需要下载贴纸返回false
     */
    public boolean startPreDownload(DownloadListener downloadListener) {
        this.downloadListener = downloadListener;
        List<SoftGiftBean> rawStickGroupList = getStickList();
        if (rawStickGroupList == null || rawStickGroupList.size() == 0) return false;
        for (int i = 0; i < rawStickGroupList.size(); i++) {
            SoftGiftBean model = rawStickGroupList.get(i);
            if (!isDownloaded(model)) {
                preStickGroupList.add(model);
            }
        }

        if (preStickGroupList.size() > 0) {
            preDownCount = preStickGroupList.size();
            downloadStickerGroup(preStickGroupList.get(preDownCount - 1));
            return true;
        }
        return false;
    }

    private List<SoftGiftBean> getStickList() {
        String json = ShareFileUtils.getString(ShareFileUtils.GIFT_LIST, "{}");
        SoftEnjoyNetBean softEnjoyNetBean = GsonUtils.getObject(json, SoftEnjoyNetBean.class);
        if (softEnjoyNetBean != null && softEnjoyNetBean.data != null && softEnjoyNetBean.data.arlist != null) {
            return softEnjoyNetBean.data.arlist;
        }
        return null;
    }

    /**
     * 下载指定贴纸，如果贴纸正在下载中则不做任何操作
     *
     * @param stickerGroup 贴纸对象
     */
    private void downloadStickerGroup(SoftGiftBean stickerGroup) {
        if (stickerGroup == null || mDownLoader.isDownloading(stickerGroup.arGiftId))
            return;

        StickerGroup stickerGroup1 = new StickerGroup();
        stickerGroup1.groupId = stickerGroup.arGiftId;
        mDownLoader.downloadStickerGroup(stickerGroup1);

    }

    /**
     * 判断贴纸是否已被下载到本地
     *
     * @param stickerGroup
     * @return
     */
    private boolean isDownloaded(SoftGiftBean stickerGroup) {
        return mDownLoader.isDownloaded(stickerGroup.arGiftId);
    }

    /**
     * 贴纸是否正在下载
     *
     * @return
     */
    private boolean isDownlowding(SoftGiftBean stickerGroup) {
        return mDownLoader.containsTask(stickerGroup.arGiftId);
    }

    private DownloadListener downloadListener;

    public interface DownloadListener {
        void progress(int progress);
    }
}
