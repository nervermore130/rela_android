package com.thel.modules.live.liverank;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thel.base.BaseDialogFragment;
import com.thel.constants.TheLConstants;
import com.thel.modules.live.in.LiveBaseView;

/**
 * Created by waiarl on 2018/2/1.
 */

public class LiveGiftRankDialogView extends BaseDialogFragment implements LiveBaseView<LiveGiftRankDialogView> {

    private LiveGiftRankingView view;
    private String userId;

    public static LiveGiftRankDialogView getInstance(Bundle bundle) {
        LiveGiftRankDialogView instance = new LiveGiftRankDialogView();
        instance.setArguments(bundle);
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Bundle bundle = getArguments();
        userId = bundle.getString(TheLConstants.BUNDLE_KEY_USER_ID);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = new LiveGiftRankingView(getContext());
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
        setListener();
    }

    private void init() {
        view.initView(getChildFragmentManager(), userId);
    }

    private void setListener() {
        view.setCloseLayoutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    /*******************************************一下为接口继承方法******************************************************/
    @Override
    public LiveGiftRankDialogView show() {
        return null;
    }

    @Override
    public LiveGiftRankDialogView hide() {
        return null;
    }

    @Override
    public LiveGiftRankDialogView destroyView() {
        return null;
    }

    @Override
    public boolean isAnimating() {
        return false;
    }

    @Override
    public void setAnimating(boolean isAnimating) {

    }

    @Override
    public void showShade(boolean show) {

    }
}
