package com.thel.modules.main.home.community;

import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.widget.LinearLayout;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.theme.ThemeBean;
import com.thel.constants.TheLConstants;
import com.thel.modules.main.me.bean.LogInfoBean;
import com.thel.modules.main.me.match.eventcollect.collect.LiveLogUtils;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;
import com.thel.utils.ShareFileUtils;

import java.util.List;

/**
 * Created by chad
 * Time 17/10/18
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */

public class HotThemesAdapter extends BaseRecyclerViewAdapter<ThemeBean> {

    private final String page;
    private final String pageId;
    private final String fromPage;
    private final String fromPageId;
    private final String sort;
    private final String rank_id;

    public HotThemesAdapter(List<ThemeBean> data, String page, String PageId, String fromPage, String fromPageId, String sort, String rankid) {
        super(R.layout.hot_themes_listitem, data);
        this.page = page;
        this.pageId = PageId;
        this.fromPage = fromPage;
        this.fromPageId = fromPageId;
        this.sort = sort;
        this.rank_id = rankid;
    }

    @Override
    protected void convert(BaseViewHolder helper, ThemeBean item) {
        helper.setText(R.id.txt_theme_title, "");
        if (!TextUtils.isEmpty(item.text)) {
            helper.setText(R.id.txt_theme_title, item.text.replace("\n", " "));
        }
        helper.setText(R.id.txt_theme_desc, item.buildDesc());
        helper.setVisible(R.id.lin_participate_theme, true);
        //  helper.setVisible(R.id.txt_hot_theme_title, helper.getLayoutPosition() == getHeaderLayoutCount() ? true : false);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) helper.getView(R.id.img).getLayoutParams();
        helper.setBackgroundColor(R.id.lin_participate_theme, ContextCompat.getColor(TheLApp.getContext(), R.color.transparent));
        params.width = (int) TheLApp.getContext().getResources().getDimension(R.dimen.discover_page_theme_width);
        params.height = (int) TheLApp.getContext().getResources().getDimension(R.dimen.discover_page_theme_width);
        helper.setImageUrl(R.id.img, item.image, TheLConstants.ICON_MIDDLE_SIZE, TheLConstants.ICON_MIDDLE_SIZE);
        traceThemeLog("exposure", helper.getAdapterPosition(), item.id);
    }

    public void traceThemeLog(String activity, int position, String momentsId) {
        String latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
        String longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");
        try {
            LogInfoBean logInfoBean = new LogInfoBean();
            logInfoBean.page = page;
            logInfoBean.page_id = pageId;
            logInfoBean.activity = activity;
            logInfoBean.from_page = fromPage;
            logInfoBean.from_page_id = fromPageId;
            logInfoBean.lat = latitude;
            logInfoBean.lng = longitude;

            LogInfoBean.LogsDataBean logsDataBean = new LogInfoBean.LogsDataBean();
            logsDataBean.rank_id = rank_id;
            logsDataBean.index = position;
            logsDataBean.moment_id = momentsId;
            logsDataBean.sort = sort;
            logInfoBean.data = logsDataBean;

            LiveLogUtils.getInstance().addLog(logInfoBean);

        } catch (Exception e) {

        }
    }
}
