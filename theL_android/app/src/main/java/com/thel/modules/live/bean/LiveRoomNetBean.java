package com.thel.modules.live.bean;

import com.thel.base.BaseDataBean;

/**
 * Created by waiarl on 2017/11/2.
 */

public class LiveRoomNetBean extends BaseDataBean {
    public LiveRoomBean data;

    @Override
    public String toString() {
        return "LiveRoomNetBean{" +
                "data=" + data +
                '}';
    }
}
