package com.thel.modules.live.liveBigGiftAnimLayout.video;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.AttributeSet;
import android.view.TextureView;

import com.thel.utils.L;

import baidu.measure.IRenderView;
import baidu.measure.TextureRenderView;

/**
 * Created by waiarl on 2018/1/11.
 */

public class LiveBigAnimVideoView extends TextureRenderView implements TextureView.SurfaceTextureListener, LiveAnimVideoHandlerConstant {
    private final Context mContext;
    private boolean destory = false;
    private LiveAnimVideoThread mRenderThread;
    private Handler mUIHandler;


    public LiveBigAnimVideoView(Context context) {
        this(context, null);
    }

    public LiveBigAnimVideoView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LiveBigAnimVideoView(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public LiveBigAnimVideoView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mContext = context;
        init();

    }

    private void init() {
       // setBackgroundColor(Color.RED);
        setId(video.com.relavideolibrary.R.id.edit_video_id);
        setAspectRatio(IRenderView.AR_MATCH_PARENT);

        createUiHandler();
        setSurfaceTextureListener(this);
        createRenderThread();
    }

    private void createUiHandler() {
        mUIHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (destory) {
                    return;
                }
                switch (msg.what) {
                    case MSG_VIDEO_PLAYER_PREPARED:
                        setVideoSize(msg.arg1, msg.arg2);
                        break;
                    case MSG_VIDEO_PLAYER_SIZE_CHANGED:
                        setVideoSize(msg.arg1, msg.arg2);
                        break;
                }
            }
        };
    }

    private void createRenderThread() {
        mRenderThread = new LiveAnimVideoThread(mUIHandler);
        mRenderThread.setName("TexFromCam Render");
        mRenderThread.start();
        mRenderThread.waitUntilReady();
    }


    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        L.i(TAG, "onSurfaceTextureAvailable surfaceTexture=" + surface.hashCode() + ";width=" + width + ";height=" + height);
        L.i(TAG,"mWidth="+getWidth()+",mHeight="+getHeight());
        if (mRenderThread != null) {
            // Normal case -- render thread is running, tell it about the new surface.
            LiveAnimVideoHandler rh = mRenderThread.getHandler();
            rh.sendSurfaceAvailable(surface, width, height);
        } else {
            L.i(TAG, "render thread not running");
        }
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
        L.d(TAG, "onSurfaceTextureSizeChanged surfaceTexture=" + surface.hashCode() + ";width=" + width + ";height=" + height);
        if (mRenderThread != null) {
            // Normal case -- render thread is running, tell it about the new surface.
            LiveAnimVideoHandler rh = mRenderThread.getHandler();
            rh.sendSurfaceChanged(surface, width, height);
        } else {
            L.i(TAG, "render thread not running");
        }
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        L.d(TAG, "onSurfaceTextureDestroyed surfaceTexture=" + surface.hashCode());
        if (mRenderThread != null) {
            LiveAnimVideoHandler rh = mRenderThread.getHandler();
            rh.sendSurfaceDestroyed(surface);
        }
        return true;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {

    }


    public void playAnim(String url) {
        // url = "/sdcard/thel/download/111.mp4";
        if(mRenderThread.getHandler()!=null) {
            mRenderThread.getHandler().sendPlayVideo(url);
        }
    }

    public void stopPlay() {
        if(mRenderThread.getHandler()!=null){
            mRenderThread.getHandler().sendPause();
        }
    }

    public void destoryView() {
        destory = true;
    }


}
