package com.thel.modules.live.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.thel.R;
import com.thel.bean.LiveNoticeCheckBean;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestConstants;
import com.thel.network.service.DefaultRequestService;
import com.thel.utils.AppInit;
import com.thel.utils.DialogUtil;
import com.thel.utils.MD5Utils;

import java.util.HashMap;
import java.util.Map;

import androidx.fragment.app.DialogFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import video.com.relavideolibrary.Utils.DensityUtils;

/**
 * Created by chad
 * Time 18/8/27
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */
public class LiveNoticeEditDialog extends DialogFragment implements TextWatcher {

    @OnClick(R.id.close)
    void close() {
        this.dismiss();
    }

    @BindView(R.id.edit_notice)
    EditText edit_notice;

    @BindView(R.id.last_count)
    TextView last_count;

    @OnClick(R.id.sure)
    void sure() {
        final String content = edit_notice.getText().toString();
        if (!TextUtils.isEmpty(content)) {
            Map<String, String> data = new HashMap<>();
            data.put(RequestConstants.I_ANNOUNCEMENT, content);
            DefaultRequestService.createAllRequestService().checkAnnounement(MD5Utils.generateSignatureForMap(data))
                    .onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<LiveNoticeCheckBean>() {
                @Override
                public void onNext(LiveNoticeCheckBean data) {
                    super.onNext(data);
                    if (data != null && data.data != null) {
                        if (data.data.valid) {
                            LiveNoticeEditDialog.this.dismiss();
                            if (editNoticeListener != null)
                                editNoticeListener.notice(content);
                        } else {
                            DialogUtil.showToastShortGravityCenter(getContext(), getString(R.string.live_notice_check_error));
                        }
                    }
                }
            });
        } else {
            if (editNoticeListener != null)
                editNoticeListener.notice(content);
            this.dismiss();
        }
    }

    private String defaultNotice = "";

    public static LiveNoticeEditDialog newInstence(String defaultNotice) {
        LiveNoticeEditDialog noticeDialog = new LiveNoticeEditDialog();
        Bundle bundle = new Bundle();
        bundle.putString("notice", defaultNotice);
        noticeDialog.setArguments(bundle);
        return noticeDialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            defaultNotice = getArguments().getString("notice");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.live_notice_layout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        edit_notice.setText(defaultNotice);
        last_count.setText(String.valueOf(200 - defaultNotice.length()));
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), R.style.DialogFadeAnim);

        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER); //可设置dialog的位置
        window.getDecorView().setPadding(0, 0, 0, 0); //消除边距
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.width = AppInit.displayMetrics.widthPixels - DensityUtils.dp2px(50);   //设置宽度充满屏幕
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        return dialog;

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        edit_notice.addTextChangedListener(this);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        last_count.setText(String.valueOf(200 - s.length()));
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public void onDestroy() {
        if (getActivity() != null) {
            //拿到InputMethodManager
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            //如果window上view获取焦点 && view不为空
            if (imm != null && imm.isActive() && getActivity().getCurrentFocus() != null) {
                //拿到view的token 不为空
                if (getActivity().getCurrentFocus().getWindowToken() != null) {
                    //表示软键盘窗口总是隐藏，除非开始时以SHOW_FORCED显示。
                    imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                }
            }
        }
        super.onDestroy();
    }

    private EditNoticeListener editNoticeListener;

    public void setEditNoticeListener(EditNoticeListener editNoticeListener) {
        this.editNoticeListener = editNoticeListener;
    }

    public interface EditNoticeListener {
        void notice(String notice);
    }
}
