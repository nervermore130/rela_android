package com.thel.modules.main.discover.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.widget.RelativeLayout;
import android.widget.Scroller;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.constants.TheLConstants;
import com.thel.modules.live.bean.LivePopularityNetBean;
import com.thel.modules.main.home.moments.web.WebViewActivity;
import com.thel.utils.GrowingIOUtil;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * 直播列表界面主播榜轮播
 * Created by lingwei on 2017/11/19.
 */

public class LiveScrollViewpagerView extends RelativeLayout {
    private final Context mContext;
    private ViewPager viewPager;
    private List<View> views = new ArrayList<>();
    private ViewpagerAdapter adapter;
    private int scrollDuration = 1000;
    private final int SCROLL = 1;
    private final int STOP = 2;
    private boolean canScroll = true;
    private Timer timer;
    private MyTimerTask task;
    private String topLink = TheLConstants.LIVE_RANKING_LIST_URL;

    private boolean isScrolling = false;
    private LivePopularityNetBean bean;
    private PopularityItemClickLitener mPopularityItemClickLitener;

    public LiveScrollViewpagerView(Context context) {
        this(context, null);
    }

    public LiveScrollViewpagerView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LiveScrollViewpagerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }

    private void init() {
        viewPager = new ViewPager(mContext);
        addView(viewPager, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        setSliderTransformDuration(scrollDuration);
        timer = new Timer();
        //   getViewList();
        adapter = new ViewpagerAdapter(views);
        viewPager.setAdapter(adapter);
        if (viewPager.getAdapter().getCount() > 1) {
            setAutoScroll();
        }
        setListener();

    }

    private void setListener() {
        viewPager.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        Intent intent = new Intent(mContext, WebViewActivity.class);
                        Bundle bundle = new Bundle();
                        if (bean != null && bean.data != null) {
                            bundle.putString(WebViewActivity.URL, bean.data.topLink);

                        } else {
                            bundle.putString(WebViewActivity.URL, topLink);

                        }
                        bundle.putBoolean(WebViewActivity.NEED_SECURITY_CHECK, false);
                        intent.putExtras(bundle);
                        GrowingIOUtil.goToWeb("GotoWebviewPage");
                        mContext.startActivity(intent);
                        break;
                }
                return false;
            }
        });

    }

    private void setAutoScroll() {
        if (viewPager.getAdapter().getCount() < 2) {
            canScroll = false;
            isScrolling = false;
            return;
        }
        canScroll = true;
        if (!isScrolling) {
            isScrolling = true;
            task = new MyTimerTask();
            timer = new Timer();
            timer.schedule(task, 1000, 3000);
        }

    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case SCROLL:
                    scrollToNext();
                    break;
                case STOP://显示正常内容
                    stop();
                    break;
            }
        }
    };

    private void stop() {
        canScroll = false;
        isScrolling = false;
        if (timer != null)
            timer.cancel();
    }

    @Override
    protected void onWindowVisibilityChanged(int visibility) {
        super.onWindowVisibilityChanged(visibility);
        if (canScroll && viewPager != null && viewPager.getAdapter().getCount() > 1) { // 避免手动停止轮播后，页面跳转回来又开始轮播的情况
            if (visibility == VISIBLE) {
                setAutoScroll();
            } else {
                stop();
            }
        }
    }

    private void scrollToNext() {
        if (viewPager.getAdapter().getCount() < 2) {
            canScroll = false;
            isScrolling = false;
            return;
        }
        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true);
    }

    class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            if (canScroll) {
                handler.sendEmptyMessage(SCROLL);
            } else {
                handler.sendEmptyMessage(STOP);
            }
        }
    }

    public void initView(LivePopularityNetBean bean) {
        this.bean = bean;
        handler.removeCallbacksAndMessages(null);
        isScrolling = false;
        canScroll = false;
        stop();
        views.clear();
        adapter.notifyDataSetChanged();

        final LivePopularityView v1 = new LivePopularityView(mContext);
        final LivePopularityView v2 = new LivePopularityView(mContext);
        final LivePopularityView v3 = new LivePopularityView(mContext);
        final LivePopularityView v4 = new LivePopularityView(mContext);

        if (bean.data == null) {
            return;
        }
        v1.initView(bean.data.anchorToday, TheLApp.getContext().getString(R.string.anchor_today));
        v2.initView(bean.data.anchorWeek, TheLApp.getContext().getString(R.string.anchor_Week));

        v3.initView(bean.data.guardToday, TheLApp.getContext().getString(R.string.guard_today));
        v4.initView(bean.data.guardWeek, TheLApp.getContext().getString(R.string.guard_week));

        views.add(v1);
        views.add(v2);
        views.add(v3);
        views.add(v4);

        adapter.notifyDataSetChanged();
        setAutoScroll();
    }

    public void setSliderTransformDuration(int duration) {
        try {
            Field mScroller = ViewPager.class.getDeclaredField("mScroller");
            mScroller.setAccessible(true);
            FixedSpeedScroller scroller = new FixedSpeedScroller(viewPager.getContext(), null, duration);
            mScroller.set(viewPager, scroller);
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    class ViewpagerAdapter extends PagerAdapter {

        private final List<View> pagerViews;

        public ViewpagerAdapter(List<View> pagerViews) {
            this.pagerViews = pagerViews;
            Log.i("viewpageradapter", " " + pagerViews.size() + "");
        }

        @Override
        public int getCount() {
            if (pagerViews.size() == 1)
                return 1;
            else if (pagerViews.size() == 0) {
                return 0;
            } else
                return Integer.MAX_VALUE;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            Log.i("adapter", "isViewFromObject," + (view == object));
            return view == object;
        }

        @Override
        public int getItemPosition(Object object) {
            Log.i("adapter", "getItemPosition");
            return super.getItemPosition(object);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            Log.i("adapter", "destroyItem," + position);
            //  container.removeView(pagerViews.get(position));
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Log.i("adapter", "instantiateItem," + position);
            if (pagerViews.size() > 0) {
                //position % view.size()是指虚拟的position会在[0，view.size()）之间循环
                View view = pagerViews.get(position % pagerViews.size());
                if (container.equals(view.getParent())) {
                    container.removeView(view);
                }
                container.addView(view);
                return view;
            }
            return null;
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            super.setPrimaryItem(container, position, object);
            Log.i("refresh", "setPrimaryItem," + position);
        }

    }

    public class FixedSpeedScroller extends Scroller {

        private int mDuration = 1000;

        public FixedSpeedScroller(Context context) {
            super(context);
        }

        public FixedSpeedScroller(Context context, Interpolator interpolator) {
            super(context, interpolator);
        }

        public FixedSpeedScroller(Context context, Interpolator interpolator, int duration) {
            this(context, interpolator);
            mDuration = duration;
        }

        @Override
        public void startScroll(int startX, int startY, int dx, int dy, int duration) {
            // Ignore received duration, use fixed one instead
            super.startScroll(startX, startY, dx, dy, mDuration);
        }

        @Override
        public void startScroll(int startX, int startY, int dx, int dy) {
            // Ignore received duration, use fixed one instead
            super.startScroll(startX, startY, dx, dy, mDuration);
        }
    }

    public interface PopularityItemClickLitener {
        void onItemClick(int position);
    }

    public void setPopularityTopLinkOnClick(PopularityItemClickLitener popularityItemClickLitener) {
        this.mPopularityItemClickLitener = popularityItemClickLitener;
    }
}
