package com.thel.modules.main.home.moments;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.core.content.ContextCompat;
import android.text.Editable;
import android.text.Spannable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.bean.ContactBean;
import com.thel.bean.ReleasedCommentBeanV2New;
import com.thel.bean.comment.CommentBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.recommend.RecommendWebBean;
import com.thel.constants.TheLConstants;
import com.thel.modules.live.bean.LiveRoomBean;
import com.thel.modules.main.messages.ChatActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestConstants;
import com.thel.network.service.DefaultRequestService;
import com.thel.utils.DialogUtil;
import com.thel.utils.GsonUtils;
import com.thel.utils.ImageUtils;
import com.thel.utils.L;
import com.thel.utils.MD5Utils;
import com.thel.utils.ViewUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.thel.R.id.other_portrait;
import static com.thel.modules.main.home.moments.WriteRecommendMomentActivity.FROM_PAGE_MOMENT;

/**
 * Created by lingwei on 2017/12/8
 * 推荐日志后准备跳转聊天编辑的类
 */

public class WriteRecommendMomentToChatActivity extends BaseActivity {
    private View img_back;
    private View txt_title;
    private TextView img_done;
    private View rel_pic;
    private SimpleDraweeView img_pic;
    private ImageView img_play;
    private TextView txt_content;
    private View lin_music_des;
    private TextView txt_music_name;
    private TextView txt_music_album;
    private TextView txt_music_artist;
    private EditText edit_content;

    private MomentsBean oldMomentBean;


    private Handler mHandler = new Handler(Looper.getMainLooper());

    private int shareToPostion = 0;

    private List<String> shareToList = new ArrayList<>();
    private List<String> shareToTipsList = new ArrayList<>();
    private ArrayList<ContactBean> mentionList = new ArrayList<>();
    private DialogUtil dialogUtils;

    private String choosedTag = "";
    private TextWatcher textWatcher;


    public static final int FROM_PAGE_WRITE_MOMENT = 0;//推荐日志
    public static final int FROM_PAGE_WRITE_WEBVIEW = 1;//推荐网页
    public static final int FROM_PAGE_WRITE_LIVEUSER = 3;//推荐主播
    public static final int FROM_PAGE_WRITE_CHAT = 4; //从推荐聊天页面过来 4.3.0新增
    public static final int FROM_PAGE_LIVE_USER = 2;//推荐主播

    private int from_page = FROM_PAGE_WRITE_MOMENT;
    private RecommendWebBean recommendWebBean;
    private LiveRoomBean liveuserbean;
    private String toAvatar;
    private String toName;
    private SimpleDraweeView to_portrait;
    private TextView txt_to_name;
    private String toUserId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout();
        getIntentData();
        findViewById();
        initData();
        initView();
        setListener();
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void initData() {
        dialogUtils = DialogUtil.getInstance();
        if (!TextUtils.isEmpty(toAvatar)) {
            to_portrait.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(toAvatar, TheLConstants.AVATAR_SMALL_SIZE, TheLConstants.AVATAR_SMALL_SIZE)));
        }
        if (!TextUtils.isEmpty(toName)) {
            txt_to_name.setText(toName);
        }

    }

    protected void setLayout() {
        setContentView(R.layout.activity_write_recommend_moment_chat_layout);
    }

    protected void findViewById() {
        img_back = findViewById(R.id.img_back);
        txt_title = findViewById(R.id.txt_title);
        img_done = findViewById(R.id.img_done);
        rel_pic = findViewById(R.id.rel_pic);
        img_pic = findViewById(R.id.img_pic);
        img_play = findViewById(R.id.img_play);
        txt_content = findViewById(R.id.txt_content);
        lin_music_des = findViewById(R.id.lin_music_des);
        txt_music_name = findViewById(R.id.txt_music_name);
        txt_music_album = findViewById(R.id.txt_music_album);
        txt_music_artist = findViewById(R.id.txt_music_artist);
        edit_content = findViewById(R.id.edit_content);
        to_portrait = findViewById(other_portrait);
        txt_to_name = findViewById(R.id.txt_other_name);

    }

    private void getIntentData() {
        final Intent intent = getIntent();
        if (intent == null) {
            return;
        }
        from_page = intent.getIntExtra(TheLConstants.BUNDLE_KEY_INTENT_FROM, FROM_PAGE_MOMENT);
        toAvatar = intent.getStringExtra("toAvatar");
        toName = intent.getStringExtra("toName");
        if (FROM_PAGE_WRITE_MOMENT == from_page) {
            oldMomentBean = (MomentsBean) intent.getSerializableExtra(TheLConstants.BUNDLE_KEY_MOMENT_BEAN);
            toUserId = intent.getStringExtra("toUserId");
            toAvatar = intent.getStringExtra("toAvatar");
            toName = intent.getStringExtra("toName");
        } else if (FROM_PAGE_WRITE_LIVEUSER == from_page) {
            liveuserbean = (LiveRoomBean) intent.getSerializableExtra(TheLConstants.BUNDLE_KEY_LIVEUSER_BEAN);
            toAvatar = intent.getStringExtra("toAvatar");
            toName = intent.getStringExtra("toName");
            toUserId = intent.getStringExtra("toUserId");
        } else if (FROM_PAGE_WRITE_WEBVIEW == from_page) {
            recommendWebBean = (RecommendWebBean) intent.getSerializableExtra(TheLConstants.BUNDLE_KEY_RECOMMEND_WEB_BEAN);
            toAvatar = intent.getStringExtra("toAvatar");
            toName = intent.getStringExtra("toName");
            toUserId = intent.getStringExtra("toUserId");

        }
    }

    protected void setListener() {

      //  setDoneButtonEnabled(true);

        textWatcher = new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String str = s.toString().trim();
                if (str.length() >= getResources().getInteger(R.integer.moment_word_count_limit)) {
                    DialogUtil.showToastShort(WriteRecommendMomentToChatActivity.this, getString(R.string.updatauserinfo_activity_intro_hint2, getResources().getInteger(R.integer.moment_word_count_limit)));
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String str = s.toString();
                s.setSpan(new ForegroundColorSpan(Color.BLACK), 0, str.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                if (!TextUtils.isEmpty(choosedTag) && s.toString().contains(choosedTag)) {
                    s.setSpan(new ForegroundColorSpan(ContextCompat.getColor(TheLApp.getContext(), R.color.tag_color)), str.indexOf(choosedTag), str.indexOf(choosedTag) + choosedTag.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                }
//                if (str.length() >= getResources().getInteger(R.integer.moment_word_count_limit) || str.trim().length() == 0) {
//                    setDoneButtonEnabled(false);
//                } else {
//                    setDoneButtonEnabled(true);
//                }
            }
        };
        edit_content.addTextChangedListener(textWatcher);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                ViewUtils.hideSoftInput(WriteRecommendMomentToChatActivity.this, edit_content);

                close();
            }
        });
        img_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                releaseChat();
            }
        });

    }

    /**
     * 发送到聊天
     */
    private void releaseChat() {

        L.d("ToChat", " from_page : " + from_page);

        if (FROM_PAGE_WRITE_MOMENT == from_page) {
            recommendMoment();
        } else if (FROM_PAGE_WRITE_WEBVIEW == from_page) {
            recommendWeb();//推荐网页
        } else if (FROM_PAGE_WRITE_LIVEUSER == from_page) {
            recommendLiveUser();
        }
    }

    private void recommendLiveUser() {
        final Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra("fromPage", "SendCardActivity_recommend");
        Bundle bundle = new Bundle();
        intent.putExtra("toUserId", toUserId);
        intent.putExtra("toAvatar", toAvatar);
        intent.putExtra("toName", toName);
        intent.putExtra(TheLConstants.BUNDLE_KEY_INTENT_FROM, ChatActivity.FROM_PAGE_LIVE_USER);
        bundle.putSerializable(TheLConstants.BUNDLE_KEY_LIVE_ROOM, liveuserbean);
        intent.putExtra("recommendText", edit_content.getText().toString().trim());
        intent.putExtras(bundle);
        startActivity(intent);
        setResult(TheLConstants.RESULT_CODE_RECOMMEND_TO_CHAT_SUCCESS);
        finish();
    }

    private void recommendWeb() {
        final Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra("fromPage", "SendCardActivity_recommend");
        Bundle bundle = new Bundle();
        intent.putExtra("toUserId", toUserId);
        intent.putExtra("toAvatar", toAvatar);
        intent.putExtra("toName", toName);
        intent.putExtra(TheLConstants.BUNDLE_KEY_INTENT_FROM, ChatActivity.FROM_PAGE_WEBVIEW);
        bundle.putSerializable(TheLConstants.BUNDLE_KEY_RECOMMEND_WEB_BEAN, recommendWebBean);
        intent.putExtra("recommendText", edit_content.getText().toString().trim());
        intent.putExtras(bundle);
        setResult(TheLConstants.RESULT_CODE_RECOMMEND_TO_CHAT_SUCCESS);
        startActivity(intent);
        finish();
    }

    private void recommendMoment() {
        String releaseText = edit_content.getText().toString().trim();

        L.d("ToChat", " oldMomentBean : " + GsonUtils.createJsonString(oldMomentBean));

        final Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra("fromPage", "SendCardActivity_recommend");
        Bundle bundle = new Bundle();
        intent.putExtra("toUserId", toUserId);
        intent.putExtra("toAvatar", toAvatar);
        intent.putExtra("toName", toName);
        intent.putExtra("recommend", 2);
        intent.putExtra("recommendText", releaseText);
        bundle.putSerializable(TheLConstants.BUNDLE_KEY_MOMENT_BEAN, oldMomentBean);
        intent.putExtras(bundle);
        setResult(TheLConstants.RESULT_CODE_RECOMMEND_TO_CHAT_SUCCESS);
        startActivity(intent);
        finish();

        //4.7.3推荐日志时，将推荐文字作为日志的一条评论
        Intent intentCast = new Intent();
        intentCast.setAction(TheLConstants.BROADCAST_AUTO_RELEASE_NEW_RECOMMEND_COMMENT_ACTION);
        intentCast.putExtra(RequestConstants.I_TEXT, releaseText);
        intentCast.putExtra(RequestConstants.I_TYPE, CommentBean.COMMENT_TYPE_TEXT);
        sendBroadcast(intentCast);
        releaseComment(releaseText);
    }

    private void releaseComment(String releaseText) {
        Map<String, String> map = new HashMap<>();
        map.put(RequestConstants.I_TEXT, releaseText);
        map.put(RequestConstants.I_TYPE, CommentBean.COMMENT_TYPE_TEXT);
        map.put(RequestConstants.I_TO_USER_ID, "");
        map.put(RequestConstants.I_MOMENT_ID, oldMomentBean.momentsId);

        Flowable<ReleasedCommentBeanV2New> flowable = DefaultRequestService.createMomentRequestService().replyMoment(MD5Utils.generateSignatureForMap(map));
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<ReleasedCommentBeanV2New>() {
            @Override
            public void onNext(ReleasedCommentBeanV2New data) {
                super.onNext(data);
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
            }
        });
    }

    private void close() {
        if (!TextUtils.isEmpty(edit_content.getText().toString().trim())) {
            DialogUtil.showConfirmDialog(WriteRecommendMomentToChatActivity.this, null, getString(R.string.moments_write_moment_discard_tip), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    finish();
                }
            });
        } else {
            finish();
        }
    }


   /* private void setDoneButtonEnabled(boolean enabled) {
        img_done.setEnabled(enabled);
        img_done.setImageResource(enabled ? R.mipmap.btn_done : R.mipmap.btn_done_disable);
    }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }

    /***********************************************************************************************************/
    //
    private void initView() {
        rel_pic.setVisibility(View.GONE);
        img_play.setVisibility(View.GONE);
        lin_music_des.setVisibility(View.GONE);
        if (FROM_PAGE_WRITE_MOMENT == from_page) {
            initMomentBeanView();
        } else if (FROM_PAGE_WRITE_LIVEUSER == from_page) {
            initLiveUserView();
        } else if (FROM_PAGE_WRITE_WEBVIEW == from_page) {
            initRecommendWebBeanView();
        }
    }

    private void initLiveUserView() {
        setLiveUser(liveuserbean);
    }

    /**
     * 推荐网页
     */
    private void initRecommendWebBeanView() {
        if (recommendWebBean == null) {
            return;
        }
        rel_pic.setVisibility(View.VISIBLE);
        if (!TextUtils.isEmpty(recommendWebBean.imageUrl)) {
            setImage(recommendWebBean.imageUrl, img_pic);
        } else {
            img_pic.setImageResource(R.drawable.ic_launcher);
        }
        if (!TextUtils.isEmpty(recommendWebBean.title)) {
            txt_content.setText(recommendWebBean.title);
        } else {
            txt_content.setText(recommendWebBean.url);
        }


    }

    /**
     * 推荐日志
     */
    private void initMomentBeanView() {

        L.d("ToChat", " oldMomentBean : " + oldMomentBean);

        if (oldMomentBean == null) {
            return;
        }

        L.d("ToChat", " oldMomentBean.momentsType : " + oldMomentBean.momentsType);

        if (MomentsBean.MOMENT_TYPE_TEXT.equals(oldMomentBean.momentsType) || MomentsBean.MOMENT_TYPE_RECOMMEND.equals(oldMomentBean.momentsType)) {//纯文本（推荐日志也是纯文本日志）
            txt_content.setText(oldMomentBean.momentsText);
        } else if (MomentsBean.MOMENT_TYPE_IMAGE.equals(oldMomentBean.momentsType)) {//图片日志
            setPicView(false);
        } else if (MomentsBean.MOMENT_TYPE_TEXT_IMAGE.equals(oldMomentBean.momentsType)) {//带文本的图片日志
            setPicView(true);
        } else if (MomentsBean.MOMENT_TYPE_LIVE.equals(oldMomentBean.momentsType) || MomentsBean.MOMENT_TYPE_VOICE_LIVE.equals(oldMomentBean.momentsType)) {//直播日志
            setPicView(true);
        } else if (MomentsBean.MOMENT_TYPE_THEME.equals(oldMomentBean.momentsType)) {//话题日志
            setThemeView();
        } else if (MomentsBean.MOMENT_TYPE_THEME_REPLY.equals(oldMomentBean.momentsType)) {//话题回复日志
            if (TextUtils.isEmpty(getImageUrl(oldMomentBean))) {//如果不带图片
                txt_content.setText(oldMomentBean.momentsText);
            } else {
                setPicView(!TextUtils.isEmpty(oldMomentBean.momentsText));
            }
        } else if (MomentsBean.MOMENT_TYPE_VIDEO.equals(oldMomentBean.momentsType)) {//视频日志
            L.d("ToChat", " oldMomentBean.momentsText : " + oldMomentBean.momentsText);
            setPicView(!TextUtils.isEmpty(oldMomentBean.momentsText));
        } else if (MomentsBean.MOMENT_TYPE_USER_CARD.equals(oldMomentBean.momentsType)) {//推荐用户名片
            txt_content.setText(oldMomentBean.momentsText);
            if (TextUtils.isEmpty(oldMomentBean.imageUrl)) {//如果被推荐名片没有背景
                img_pic.setImageResource(R.color.tab_normal);
            } else {
                setImage(oldMomentBean.imageUrl, img_pic);
            }
        } else if (MomentsBean.MOMENT_TYPE_VOICE.equals(oldMomentBean.momentsType)) {//纯音乐日志
            setMusicView();
        } else if (MomentsBean.MOMENT_TYPE_TEXT_VOICE.equals(oldMomentBean.momentsType)) {//带文字的音乐日志
            // setPicView(true);
            setMusicView(true);
            //img_play.setVisibility(View.VISIBLE);
        } else if (MomentsBean.MOMENT_TYPE_WEB.equals(oldMomentBean.momentsType)) {//如果是网页日志
            try {
                final JSONObject obj = new JSONObject(oldMomentBean.momentsText);
                RecommendWebBean bean = new RecommendWebBean();
                bean.fromJson(obj);
                txt_content.setText(bean.momentsText);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else if (MomentsBean.MOMENT_TYPE_USER_CARD.equals(oldMomentBean.momentsType)) {//如果是推荐主播
            txt_content.setText(oldMomentBean.momentsText);
        } else {//默认当普通文本类型
            txt_content.setText(oldMomentBean.momentsText);
        }
    }

    private void setLiveUser(LiveRoomBean liveuserbean) {
        rel_pic.setVisibility(View.VISIBLE);
        if (liveuserbean != null && liveuserbean.user != null) {
            String avatar = liveuserbean.user.avatar;
            if (!TextUtils.isEmpty(avatar)) {
                img_pic.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(avatar, TheLConstants.ICON_MIDDLE_SIZE, TheLConstants.ICON_MIDDLE_SIZE))).build()).setAutoPlayAnimations(true).build());
            }
            txt_content.setText(liveuserbean.user.nickName + " " + getString(R.string.is_on_live) + " " + liveuserbean.text);
        }
    }

    /**
     * 音乐
     */
    private void setMusicView() {
        //setPicView(false);
        setMusicView(false);
        img_play.setVisibility(View.VISIBLE);
        lin_music_des.setVisibility(View.VISIBLE);
        txt_music_name.setText(oldMomentBean.songName);
        txt_music_album.setText(TheLApp.getContext().getString(R.string.moments_album) + oldMomentBean.albumName);
        txt_music_artist.setText(TheLApp.getContext().getString(R.string.moments_artist) + oldMomentBean.artistName);
    }

    /**
     * 话题
     */
    private void setThemeView() {
        rel_pic.setVisibility(View.VISIBLE);
        txt_content.setText(oldMomentBean.momentsText);
        if (TextUtils.isEmpty(oldMomentBean.imageUrl)) {//没背景图片的话题
            setImage(TheLConstants.RES_PIC_URL + R.mipmap.bg_topic_default, img_pic);
        } else {
            setImage(oldMomentBean.imageUrl, img_pic);
        }
    }

    /**
     * 带图片的日志
     */
    private void setPicView(boolean hasText) {
        rel_pic.setVisibility(View.VISIBLE);
        setImage(getImageUrl(oldMomentBean), img_pic);
        if (hasText) {
            txt_content.setText(oldMomentBean.momentsText);
        } else {
            txt_content.setVisibility(View.GONE);
        }
    }

    /**
     * 带音乐图片的日志
     */
    private void setMusicView(boolean hasText) {
        rel_pic.setVisibility(View.VISIBLE);
        //  setImage(getImageUrl(oldMomentBean), img_pic);
        setMusicImage(getMusicImageUrl(oldMomentBean), img_pic);
        if (hasText) {
            txt_content.setText(oldMomentBean.momentsText);
        } else {
            txt_content.setVisibility(View.GONE);
        }
    }

    /**
     * 设置图片
     */
    private void setImage(String imageUrl, SimpleDraweeView img_pic) {
        if (!TextUtils.isEmpty(imageUrl)) {
            img_pic.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(imageUrl, TheLConstants.ICON_MIDDLE_SIZE, TheLConstants.ICON_MIDDLE_SIZE))).build()).setAutoPlayAnimations(true).build());
        }
    }

    /***
     * 設置音樂圖片
     */
    private void setMusicImage(String imageUrl, SimpleDraweeView img_pic) {
        if (!TextUtils.isEmpty(imageUrl)) {
            img_pic.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(imageUrl, TheLConstants.ICON_MIDDLE_SIZE, TheLConstants.ICON_MIDDLE_SIZE))).build()).setAutoPlayAnimations(true).build());
        }
    }

    /**
     * 获取第一张缩略图
     */
    private String getImageUrl(MomentsBean bean) {
        String[] picUrls = bean.thumbnailUrl.split(",");
        if (picUrls.length > 0) {
            return picUrls[0];
        }
        return "";
    }

    /***
     * 获取音乐日志缩略图
     */
    private String getMusicImageUrl(MomentsBean bean) {
        String picUril = bean.albumLogo444;
        if (!TextUtils.isEmpty(picUril)) {
            return picUril;
        }
        return "";
    }

    @Override
    public void onBackPressed() {
        close();
        super.onBackPressed();
    }

}
