package com.thel.modules.login.opt;

import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ksyun.media.player.IMediaPlayer;
import com.ksyun.media.player.KSYMediaPlayer;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.manager.ChatServiceManager;
import com.thel.modules.login.LoginActivityManager;
import com.thel.modules.login.login_register.FacebookFragment;
import com.thel.modules.login.login_register.LoginRegisterActivity;
import com.thel.modules.login.login_register.WechatFragment;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.Utils;
import com.umeng.analytics.MobclickAgent;

import java.io.IOException;

public class LoginOptActivity extends BaseActivity{

    private LinearLayout lin_phone_register;
    private TextView txt_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LoginActivityManager.getInstanse().addActivity(this);

        setContentView(R.layout.activity_login_opt);

        getWindow().setFormat(PixelFormat.TRANSLUCENT);//解决surfaceveiw第一次进入的时候会有闪屏现象

        ChatServiceManager.getInstance().stopService(TheLApp.context);

        findViewById();

        setListener();

        addWXorFBRegistFragment();

    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void findViewById() {
        lin_phone_register = findViewById(R.id.lin_phone_register);
        txt_login = findViewById(R.id.txt_login);
        txt_login.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);
    }

    private void addWXorFBRegistFragment() {
        if (ShareFileUtils.isGlobalVersion()) {
            findViewById(R.id.fb_frame).setVisibility(View.VISIBLE);
            getSupportFragmentManager().beginTransaction().add(R.id.fb_frame, FacebookFragment.newInstance(1)).commit();
        } else {
            findViewById(R.id.wx_frame).setVisibility(View.VISIBLE);
            getSupportFragmentManager().beginTransaction().add(R.id.wx_frame, WechatFragment.newInstance(1)).commit();
        }
    }

    private void setListener() {
        lin_phone_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoPhoneRegister();
            }
        });
        txt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoLogin();
            }
        });
    }

    private void gotoLogin() {
        LoginRegisterActivity.startActivity(this, LoginRegisterActivity.LOGIN,false);
    }

    private void gotoPhoneRegister() {
        // 手机注册转化率统计：点击手机登陆
        MobclickAgent.onEvent(this, "mobile_login");
        LoginRegisterActivity.startActivity(this, LoginRegisterActivity.REGISTER,false);
    }
}
