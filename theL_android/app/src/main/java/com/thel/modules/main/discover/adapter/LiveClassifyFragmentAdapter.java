package com.thel.modules.main.discover.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import java.util.List;

/**
 * 直播分类
 * Created by lingwei on 2017/11/19.
 */

public class LiveClassifyFragmentAdapter extends FragmentStatePagerAdapter {
    private List<Fragment> fragmentList;
    private int currentPosition = 0;

    public LiveClassifyFragmentAdapter(List<Fragment> fragmentList, FragmentManager fm) {
        super(fm);
        this.fragmentList = fragmentList;
    }


    public void refreshFragmentList(List<Fragment> list) {
        try {
            if (fragmentList != null) {
                fragmentList.addAll(list);
                notifyDataSetChanged();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    public int getItemPosition(Object object) {
        Object tag = ((Fragment) object).getTag();
        if (tag != null) {
            for (int i = 0; i < fragmentList.size(); i++) {
                if (tag.equals(fragmentList.get(i))) {
                    return i;
                }
            }
        }

        return POSITION_NONE;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        if (position == currentPosition) {
            return;
        }
        super.destroyItem(container, position, object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        return super.instantiateItem(container, position);
    }

    public void setCurrentPosition(int position) {
        this.currentPosition = position;
    }
}

