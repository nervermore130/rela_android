package com.thel.modules.live.utils;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.ViewCompat;
import androidx.core.view.ViewPropertyAnimatorListener;
import androidx.interpolator.view.animation.FastOutSlowInInterpolator;

import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.LottieComposition;
import com.airbnb.lottie.OnCompositionLoadedListener;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.analytics.AnalyticsBean;
import com.thel.constants.TheLConstants;
import com.thel.data.local.FileHelper;
import com.thel.modules.live.LiveConstant;
import com.thel.modules.live.bean.LivePkAeAnimBean;
import com.thel.modules.live.bean.LivePkFriendBean;
import com.thel.modules.live.bean.LivePkInitBean;
import com.thel.modules.live.bean.LivePkRequestNoticeBean;
import com.thel.modules.live.bean.LivePkStartNoticeBean;
import com.thel.modules.live.bean.LiveRoomMsgBean;
import com.thel.modules.live.bean.SoftEnjoyBean;
import com.thel.modules.live.bean.SoftGiftBean;
import com.thel.modules.live.in.LiveBaseView;
import com.thel.modules.live.in.LivePkContract;
import com.thel.modules.live.view.LivePKBloodView;
import com.thel.modules.live.view.LivePkMediaPlayer;
import com.thel.utils.AppInit;
import com.thel.utils.DateUtils;
import com.thel.utils.DeviceUtils;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;
import com.thel.utils.Utils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created by waiarl on 2017/10/20.
 */

public class LiveUtils {
    private static final Interpolator INTERPOLATOR = new FastOutSlowInInterpolator();
    //固定Pk推流的高度为整个屏幕高度的0.375倍
    private static final float PK_STREAM_HEIGHT_D = 0.375f;
    //根据屏幕750的宽，所设置的AE动画的缩放比例以及所有类似图片的比例
    private static final float PK_AE_SCALE = 43f;
    private static final float PK_ICON_SCALE = 43f;
    private static final float PK_STREAM_HEIGHT_M = 0.175F;
    private static final float PK_AE_COMMON_WIDTH = 750;//PK ae设置的宽度标准为750
    private static final float PK_AE_COMMON_HEIGHT = 1334;//pk ae设置的高度标准为1334
    private static final float PK_AE_BOOTOM_MARGIN = 500;//PK ae的高度距离底部标准为500
    private static final float PK_AE_BOTTOM_OFFSET = 100;//PK ae距离动画底部距离推流底部的向下便宜比例
    private static final float PK_ICON_AE_BOTTOM_OFFSET = 3.2f;//PK icon距离ae底部的偏移值


    public static void appearFollow(final ImageView lin_follow) {
        lin_follow.setAlpha(1f);
        final ViewGroup.LayoutParams layoutParams = lin_follow.getLayoutParams();
        final int delta = layoutParams.width;
        ObjectAnimator transAnim = ObjectAnimator.ofFloat(lin_follow, "translationX", 0, delta);//向右移动
        transAnim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                layoutParams.width = 0;
                lin_follow.setLayoutParams(layoutParams);
                lin_follow.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                layoutParams.width = delta;
                lin_follow.setLayoutParams(layoutParams);
                lin_follow.setVisibility(View.VISIBLE);
                lin_follow.setTranslationX(lin_follow.getTranslationX() - delta);
            }

            @Override
            public void onAnimationCancel(Animator animator) {
                layoutParams.width = delta;
                lin_follow.setLayoutParams(layoutParams);
                lin_follow.setVisibility(View.VISIBLE);
                lin_follow.setTranslationX(lin_follow.getTranslationX() - delta);
            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        transAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float currentValue = (float) animation.getAnimatedValue();
                layoutParams.width = Math.abs((int) currentValue);
                lin_follow.setLayoutParams(layoutParams);
                lin_follow.setVisibility(View.INVISIBLE);
            }
        });
        transAnim.setDuration(200);
        transAnim.start();
    }

    public static void appearTextFollow(final TextView lin_follow) {
        lin_follow.setAlpha(1f);
        final ViewGroup.LayoutParams layoutParams = lin_follow.getLayoutParams();
        final int delta = layoutParams.width;
        ObjectAnimator transAnim = ObjectAnimator.ofFloat(lin_follow, "translationX", 0, delta);//向右移动
        transAnim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                layoutParams.width = 0;
                lin_follow.setLayoutParams(layoutParams);
                lin_follow.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                layoutParams.width = delta;
                lin_follow.setLayoutParams(layoutParams);
                lin_follow.setVisibility(View.VISIBLE);
                lin_follow.setTranslationX(lin_follow.getTranslationX() - delta);

            }

            @Override
            public void onAnimationCancel(Animator animator) {
                layoutParams.width = delta;
                lin_follow.setLayoutParams(layoutParams);
                lin_follow.setVisibility(View.VISIBLE);
                lin_follow.setTranslationX(lin_follow.getTranslationX() - delta);
            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        transAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float currentValue = (float) animation.getAnimatedValue();
                layoutParams.width = Math.abs((int) currentValue);
                lin_follow.setLayoutParams(layoutParams);
                lin_follow.setVisibility(View.INVISIBLE);
            }
        });
        transAnim.setDuration(200);
        transAnim.start();
    }

    public static void disappearFollow(final ImageView lin_follow) {
        lin_follow.animate().alpha(0).setDuration(1500).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                lin_follow.setVisibility(View.INVISIBLE);
                animateFollow(lin_follow);

            }

            @Override
            public void onAnimationCancel(Animator animation) {
                lin_follow.setVisibility(View.INVISIBLE);
                animateFollow(lin_follow);
            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    public static void disappearTextFollow(final TextView lin_follow) {
        lin_follow.animate().alpha(0).setDuration(1500).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                lin_follow.setVisibility(View.INVISIBLE);
                animateTextFollow(lin_follow);

            }

            @Override
            public void onAnimationCancel(Animator animation) {
                lin_follow.setVisibility(View.INVISIBLE);
                animateTextFollow(lin_follow);
            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    private static void animateTextFollow(final TextView lin_follow) {
        final ViewGroup.LayoutParams layoutParams = lin_follow.getLayoutParams();
        final int delta = layoutParams.width;
        ObjectAnimator transAnim = ObjectAnimator.ofFloat(lin_follow, "translationX", delta, 0);//向左移动
        transAnim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                layoutParams.width = delta;
                lin_follow.setLayoutParams(layoutParams);
                lin_follow.setVisibility(View.GONE);

            }

            @Override
            public void onAnimationCancel(Animator animator) {
                layoutParams.width = delta;
                lin_follow.setLayoutParams(layoutParams);
                lin_follow.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        transAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float currentValue = (float) animation.getAnimatedValue();
                layoutParams.width = Math.abs((int) currentValue);
                lin_follow.setLayoutParams(layoutParams);
            }
        });
        transAnim.setDuration(200);
        transAnim.start();

    }

    private static void animateFollow(final ImageView lin_follow) {
        final ViewGroup.LayoutParams layoutParams = lin_follow.getLayoutParams();
        final int delta = layoutParams.width;
        ObjectAnimator transAnim = ObjectAnimator.ofFloat(lin_follow, "translationX", delta, 0);//向左移动
        transAnim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                layoutParams.width = delta;
                lin_follow.setLayoutParams(layoutParams);
                lin_follow.setVisibility(View.GONE);

            }

            @Override
            public void onAnimationCancel(Animator animator) {
                layoutParams.width = delta;
                lin_follow.setLayoutParams(layoutParams);
                lin_follow.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        transAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float currentValue = (float) animation.getAnimatedValue();
                layoutParams.width = Math.abs((int) currentValue);
                lin_follow.setLayoutParams(layoutParams);
            }
        });
        transAnim.setDuration(200);
        transAnim.start();

    }

    /**
     * 直播间的一些自定义view进入
     *
     * @param liveBaseView
     */

    public static void animInLiveShowView(final LiveBaseView liveBaseView) {
        L.i("LiveBaseView", "show");
        if (liveBaseView == null || !(liveBaseView instanceof View)) {
            return;
        }
        if (liveBaseView.isAnimating()) {
            return;
        }
       /* if(rt) {
            ((View) liveBaseView).setVisibility(View.VISIBLE);
            return;
        }*/

        View view = ((View) liveBaseView);

        ((View) liveBaseView).setVisibility(View.VISIBLE);
        liveBaseView.showShade(false);
        final int mh = ((View) liveBaseView).getMeasuredHeight();
        if (mh == 0) {//第一次进入的时候，得到的高度会为0，所以需要测量
            ((View) liveBaseView).measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        }
        final int top = ((View) liveBaseView).getTop();
        final int h = ((View) liveBaseView).getMeasuredHeight();
        final int m = getMarginBottom((View) liveBaseView);
        if (top != 0 || mh == 0) {
            final int height = (h + m);
            ((View) liveBaseView).setTranslationY(height);
        }
        ViewCompat.animate((View) liveBaseView).translationY(0).setInterpolator(INTERPOLATOR).withLayer().setListener(new ViewPropertyAnimatorListener() {
            @Override
            public void onAnimationStart(View view) {
                liveBaseView.setAnimating(true);
            }

            @Override
            public void onAnimationEnd(View view) {
                liveBaseView.setAnimating(false);
                ((View) liveBaseView).setVisibility(View.VISIBLE);
                liveBaseView.showShade(true);
            }

            @Override
            public void onAnimationCancel(View view) {
                liveBaseView.setAnimating(false);
                ((View) liveBaseView).setVisibility(View.VISIBLE);
                liveBaseView.showShade(true);

            }
        });


    }

    /**
     * 直播间的一些自定义View 淡出
     *
     * @param liveBaseView
     * @param destory
     */
    public static void animOutLiveShowView(final LiveBaseView liveBaseView, final boolean destory) {
        L.i("LiveBaseView", "gone");

        if (liveBaseView == null || !(liveBaseView instanceof View)) {
            return;
        }
       /* if(rt) {
            ((View) liveBaseView).setVisibility(View.GONE);
            return;
        }*/
        if (liveBaseView.isAnimating()) {
            return;
        }
        liveBaseView.showShade(false);
        ViewCompat.animate((View) liveBaseView).translationY(((View) liveBaseView).getHeight() + getMarginBottom((View) liveBaseView)).setInterpolator(INTERPOLATOR).withLayer().setListener(new ViewPropertyAnimatorListener() {
            @Override
            public void onAnimationStart(View view) {
                liveBaseView.setAnimating(true);

            }

            @Override
            public void onAnimationEnd(View view) {
                liveBaseView.setAnimating(false);
                liveBaseView.showShade(false);
                ((View) liveBaseView).setVisibility(View.GONE);
                if (destory) {
                    liveBaseView.destroyView();
                }
            }

            @Override
            public void onAnimationCancel(View view) {
                liveBaseView.setAnimating(false);
                liveBaseView.showShade(false);
                ((View) liveBaseView).setVisibility(View.GONE);
                if (destory) {
                    liveBaseView.destroyView();
                }
            }
        });
    }

    static int getMarginBottom(View view) {
        int marginBottom = 0;
        final ViewGroup.LayoutParams param = view.getLayoutParams();
        if (param instanceof ViewGroup.MarginLayoutParams) {
            marginBottom = ((ViewGroup.MarginLayoutParams) param).bottomMargin;
        }
        return marginBottom;
    }

    /**
     * 直播UI空间淡入
     *
     * @param liveBaseView
     * @param UIView
     */
    public static void animInLiveShowUIView(final LiveBaseView liveBaseView, View UIView) {
        if (liveBaseView == null || UIView == null) {
            return;
        }
        if (UIView.getVisibility() == View.VISIBLE || liveBaseView.isAnimating()) {
            return;
        }
        UIView.setVisibility(View.VISIBLE);
        ViewCompat.animate(UIView).translationX(0).setInterpolator(INTERPOLATOR).withLayer().setListener(new ViewPropertyAnimatorListener() {
            @Override
            public void onAnimationStart(View view) {
                liveBaseView.setAnimating(true);
            }

            @Override
            public void onAnimationEnd(View view) {
                liveBaseView.setAnimating(false);
            }

            @Override
            public void onAnimationCancel(View view) {
                liveBaseView.setAnimating(false);
            }
        });
    }

    /**
     * 直播UI控件淡出
     *
     * @param liveBaseView
     * @param UIView
     */
    public static void animOutLiveShowUIView(final LiveBaseView liveBaseView, final View UIView) {
        if (liveBaseView == null || UIView == null) {
            return;
        }
        if (UIView.getVisibility() != View.VISIBLE || liveBaseView.isAnimating()) {
            return;
        }
        ViewCompat.animate(UIView).translationX(UIView.getMeasuredWidth()).setInterpolator(INTERPOLATOR).withLayer().setListener(new ViewPropertyAnimatorListener() {
            @Override
            public void onAnimationStart(View view) {
                liveBaseView.setAnimating(true);
            }

            @Override
            public void onAnimationEnd(View view) {
                liveBaseView.setAnimating(false);
                UIView.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(View view) {
                liveBaseView.setAnimating(false);
                UIView.setVisibility(View.GONE);
            }
        });
    }

    public static void playScrollAnim(View img_scroll, final View lin_scroll) {
        if (img_scroll == null || lin_scroll == null) {
            return;
        }
        final float dis = Utils.dip2px(TheLApp.getContext(), -100);
        ObjectAnimator anim = ObjectAnimator.ofFloat(img_scroll, View.TRANSLATION_Y, dis, 0, dis, 0, dis, 0);
        anim.setDuration(5000);
        anim.setInterpolator(new LinearInterpolator());
        anim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                lin_scroll.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        anim.start();
    }


    /**
     * 播放ae动画
     *
     * @param live_pk_ae_view
     * @param bean
     * @param listener
     */
    public static void playPkAeAnim(final LottieAnimationView live_pk_ae_view, LivePkAeAnimBean bean, final LivePkContract.LivePkAeAnimListener listener) {
        if (live_pk_ae_view == null || bean == null) {
            return;
        }
        final String path = LiveConstant.LIVE_PK_AE_PATH + bean.ae_anim_path;
        live_pk_ae_view.cancelAnimation();
        live_pk_ae_view.loop(false);
        live_pk_ae_view.setProgress(0);
        LottieComposition.Factory.fromAssetFileName(TheLApp.getContext(), path, new OnCompositionLoadedListener() {
            @Override
            public void onCompositionLoaded(@Nullable LottieComposition composition) {
                live_pk_ae_view.setVisibility(View.VISIBLE);
                live_pk_ae_view.setComposition(composition);
                live_pk_ae_view.addAnimatorListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        if (listener != null) {
                            listener.onAnimStatus(live_pk_ae_view, LivePkContract.LivePkAeAnimListener.ANIM_START);
                        }
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        live_pk_ae_view.setVisibility(View.GONE);
                        if (listener != null) {
                            listener.onAnimStatus(live_pk_ae_view, LivePkContract.LivePkAeAnimListener.ANIM_END);
                        }
                        live_pk_ae_view.removeAnimatorListener(this);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        live_pk_ae_view.setVisibility(View.GONE);
                        if (listener != null) {
                            listener.onAnimStatus(live_pk_ae_view, LivePkContract.LivePkAeAnimListener.ANIM_CANCEL);
                        }
                        live_pk_ae_view.removeAnimatorListener(this);
                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {
                        if (listener != null) {
                            listener.onAnimStatus(live_pk_ae_view, LivePkContract.LivePkAeAnimListener.ANIM_REPEAT);
                        }
                    }
                });

                live_pk_ae_view.playAnimation();
            }
        });
    }

    /**
     * 从直播PK请求bean里面获取直播livePkFriendBean
     *
     * @param noticeBean
     * @return
     */
    public static LivePkFriendBean getLivePkFriendBean(LivePkRequestNoticeBean noticeBean) {
        final LivePkFriendBean bean = new LivePkFriendBean();
        if (noticeBean == null) {
            return bean;
        }
        bean.id = noticeBean.userId;
        bean.avatar = noticeBean.avatar;
        bean.nickName = noticeBean.nickName;
        bean.liveStatus = LivePkFriendBean.LIVE_STATUS_PK;
        return bean;
    }

    /**
     * 播放Pk中的一些Ae动画播放时的音频文件
     *
     * @param bean
     * @param listener
     */
    public static void playLivePkSound(LivePkAeAnimBean bean, final LivePkContract.LivePkPlaySoundListener listener) {
        if (bean == null) {
            return;
        }
        final String path = LiveConstant.LIVE_PK_SOUND_PATH + bean.sound_path;
        playSound(path, listener);
    }

    /**
     * 播放声音Assets文件
     *
     * @param assetPath
     * @param listener
     */
    public static void playSound(String assetPath, final LivePkContract.LivePkPlaySoundListener listener) {
        if (TextUtils.isEmpty(assetPath)) {
            return;
        }
        try {
            final AssetFileDescriptor fileDescriptor = TheLApp.getContext().getAssets().openFd(assetPath);
            final LivePkMediaPlayer player = LivePkMediaPlayer.getInstance();
            try {
                if (player.isPlaying()) {
                    player.pause();
                    player.stop();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            player.reset();
            player.setVolume(0.7f, 0.7f);
            player.setDataSource(fileDescriptor.getFileDescriptor(), fileDescriptor.getStartOffset(), fileDescriptor.getLength());
            player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    if (listener != null) {
                        listener.onSoundPlay(player, LivePkContract.LivePkPlaySoundListener.SOUND_END);
                    }
                }
            });
            player.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    player.release();
                    return false;
                }
            });
            player.prepare();
            player.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static float[] getPkParams(int screenWidth, int screenHeight) {
        final float[] paramArr = new float[4];

        final float mHeight = screenHeight;
        //一下为设置
        final float pkDivider = TheLApp.getContext().getResources().getDimension(R.dimen.live_pk_stream_divider);
        final float width = (screenWidth - pkDivider) / 2;
        final float height = mHeight * PK_STREAM_HEIGHT_D;

        final float x = width + pkDivider;

        final float live_top_height = TheLApp.getContext().getResources().getDimension(R.dimen.live_show_capture_top_height);
        final float blood_height = TheLApp.getContext().getResources().getDimension(R.dimen.live_pk_blood_view_height);
        final float live_stream_top_margin = TheLApp.getContext().getResources().getDimension(R.dimen.live_pk_stream_top_margin);
        final float y = live_top_height + blood_height + live_stream_top_margin;

        paramArr[0] = x;
        paramArr[1] = y;
        paramArr[2] = width;
        paramArr[3] = height;
        return paramArr;
    }

    public static float[] getPkUpLoadParams() {
        final float[] param = new float[4];
        final float[] paramaArr = getPkParams(AppInit.displayMetrics.widthPixels, AppInit.displayMetrics.heightPixels);
        final float mWidth = AppInit.displayMetrics.widthPixels;
        final float mHeight = AppInit.displayMetrics.heightPixels;
        if (mWidth == 0 || mHeight == 0) {
            return param;
        }
        param[0] = paramaArr[0] / mWidth;
        param[1] = paramaArr[1] / mHeight;
        param[2] = paramaArr[2] / mWidth;
        param[3] = PK_STREAM_HEIGHT_D;
        return param;
    }

    /**
     * 设置结束Pk Textview的params
     *
     * @param txt_finish_pk
     */
    public static void setTxtFinishPkParam(TextView txt_finish_pk) {
        if (txt_finish_pk == null || !(txt_finish_pk.getLayoutParams() instanceof RelativeLayout.LayoutParams)) {
            return;
        }
        final RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams) txt_finish_pk.getLayoutParams();
        final int mHeight = AppInit.displayMetrics.heightPixels;

        final float[] paramArr = LiveUtils.getPkUpLoadParams();
        final float rh = mHeight * (1 - paramArr[1] - paramArr[3]);
        final float bottomMargin = rh + Utils.dip2px(TheLApp.getContext(), 5);//待微调
        param.bottomMargin = (int) bottomMargin;
    }

    public static LivePkFriendBean getLivePkFriendBean(LivePkInitBean livePkInitBean) {
        if (livePkInitBean == null) {
            return new LivePkFriendBean();
        }
        final LivePkFriendBean friendBean = new LivePkFriendBean();
        friendBean.liveStatus = LivePkFriendBean.LIVE_STATUS_PK;
        friendBean.id = livePkInitBean.userId;
        friendBean.nickName = livePkInitBean.nickName;
        friendBean.avatar = livePkInitBean.avatar;
        return friendBean;
    }

    /**
     * 设置观看端血条所在位置
     *
     * @param livePkBloodView
     * @param pkStreamParams
     */
    public static void setPkShowBloodViewParams(LivePKBloodView livePkBloodView, float[] pkStreamParams) {
        if (livePkBloodView == null || !(livePkBloodView.getLayoutParams() instanceof RelativeLayout.LayoutParams) || pkStreamParams == null || pkStreamParams.length < 4) {
            return;
        }
        final RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams) livePkBloodView.getLayoutParams();
        final float bloodViewHeight = TheLApp.getContext().getResources().getDimension(R.dimen.live_pk_blood_view_height);
        final float bottomMargin = TheLApp.getContext().getResources().getDimension(R.dimen.live_pk_stream_top_margin);
        final float mHeight = AppInit.displayMetrics.heightPixels - DeviceUtils.getNavigationBarHeight(TheLApp.getContext());
        final float top = mHeight * pkStreamParams[1];
        final int topMargin = (int) (top - bloodViewHeight - bottomMargin);
        param.topMargin = topMargin;
    }

    /**
     * 获取观看端相应的x,y,width,height参数
     *
     * @param x
     * @param y
     * @param width
     * @param height
     * @return
     */
    public static float[] getLiveShowPkParam(float x, float y, float width, float height) {
        final float[] param = new float[]{x, y, width, height};

       /* final float mWidth = AppInit.displayMetrics.widthPixels;
        final float mHeight = AppInit.displayMetrics.heightPixels;
        if (mHeight == 0 || mWidth == 0) {
            return param;
        }
        final float mh = mHeight * PK_STREAM_HEIGHT_D;//本地流应该又的高度
        float hd = height / mh;//实际推流高度与本地推流高度比值
        if (hd == 0) {
            hd = 1;
        }
        param[1] = y / hd;
        param[3] = height / hd;


        float wd = (width + x) / mWidth;
        if (wd == 0) {
            wd = 1;
        }
        param[0] = x / wd;
        param[2] = width / wd;*/

        return param;
    }

    /**
     * 根据不同的ae动画设置ae控件的layoutparams
     *
     * @param live_pk_ae_view
     * @param bean
     */
    public static void setPkAeViewLayoutParam(LottieAnimationView live_pk_ae_view, LivePkAeAnimBean bean, float[] paramArr) {
        if (live_pk_ae_view == null || bean == null || !(live_pk_ae_view.getLayoutParams() instanceof RelativeLayout.LayoutParams) || paramArr == null || paramArr.length < 4) {
            return;
        }
        final RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams) live_pk_ae_view.getLayoutParams();
        final int mWidth = AppInit.displayMetrics.widthPixels;
        final int mHeight = AppInit.displayMetrics.heightPixels;
        final float dx = mWidth / PK_AE_COMMON_WIDTH;//ae动画是按照750宽设置的，
        final int width = (int) (bean.width * dx);
        final int height = (int) (bean.height * dx);

       /* final float y = paramArr[1];//推流顶端
        final float h = paramArr[3];//推流高度
        final float rh = mHeight - (y + h);//推流底端距离底部高度
        final float crh = PK_AE_COMMON_HEIGHT * (1 - (PK_STREAM_HEIGHT_M + PK_STREAM_HEIGHT_D));//1334标准的情况下推流距离底部高度
        float dh = crh / PK_AE_BOOTOM_MARGIN;
        if (dh == 0) {
            dh = 1;
        }
        final float bottomMargin = rh / dh;//d的具体数据带微调*/
        final float bottom = mHeight * (1 - (paramArr[1] + paramArr[3]));//推流底端距离底部高度
        final float bottomMargin = bottom - PK_AE_BOTTOM_OFFSET * dx;
        param.width = width;
        param.height = height;
        param.bottomMargin = (int) bottomMargin;
    }

    /**
     * 设置直播的 PK图标
     *
     * @param img_live_pk
     * @param paramArr
     */
    public static void setLivePkIconViewParam(ImageView img_live_pk, float[] paramArr) {
        if (img_live_pk == null || paramArr == null || paramArr.length < 4 || !(img_live_pk.getLayoutParams() instanceof RelativeLayout.LayoutParams)) {
            return;
        }
        img_live_pk.setVisibility(View.VISIBLE);
        final RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams) img_live_pk.getLayoutParams();

        final int mWidth = AppInit.displayMetrics.widthPixels;
        final int mHeight = AppInit.displayMetrics.heightPixels;
        final float dx = mWidth / 750f;//ae动画是按照750宽设置的，所以都要设置相应的比例
        final float rW = TheLApp.getContext().getResources().getDimension(R.dimen.live_pk_icon_width);
        final float rH = TheLApp.getContext().getResources().getDimension(R.dimen.live_pk_icon_height);

        final int width = (int) (rW * dx);
        final int height = (int) (rH * dx);//icon图片宽高比例为162*116;b'h
       /* final float y = paramArr[1];//推流顶端
        final float h = paramArr[3];//推流高度
        final float rh = mHeight - (y + h);//推流底端距离底部高度
        final float crh = PK_AE_COMMON_HEIGHT * (1 - (PK_STREAM_HEIGHT_M + PK_STREAM_HEIGHT_D));//1334标准的情况下推流距离底部高度
        float dh = crh / PK_AE_BOOTOM_MARGIN;
        if (dh == 0) {
            dh = 1;
        }
        final float d = -5;
        final float bottomMargin = rh / dh + d * dx;//d的具体数据带微调*/
        final float bottom = mHeight * (1 - (paramArr[1] + paramArr[3]));//推流底端距离底部高度
        final float bottomMargin = bottom - PK_AE_BOTTOM_OFFSET * dx - PK_ICON_AE_BOTTOM_OFFSET * dx;
        param.width = width;
        param.height = height;
        param.bottomMargin = (int) bottomMargin;
    }

    /**
     * 主播端失败view设置
     *
     * @param livePkFailedView
     * @param paramArr
     */
    public static void setLivePkFailedViewParam(View livePkFailedView, float[] paramArr) {
        if (livePkFailedView == null || paramArr == null || paramArr.length < 4 || !(livePkFailedView.getLayoutParams() instanceof RelativeLayout.LayoutParams)) {
            return;
        }
        final RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams) livePkFailedView.getLayoutParams();
       /* param.width = (int) paramArr[2];
        param.height = (int) paramArr[3];
        param.topMargin = (int) paramArr[1];
        param.leftMargin = (int) paramArr[0];*/
        final float mWidth = AppInit.displayMetrics.widthPixels;
        final float mHeight = AppInit.displayMetrics.heightPixels;
        param.width = (int) (mWidth * paramArr[2]);
        param.height = (int) (mHeight * paramArr[3]);
        param.leftMargin = (int) (mWidth * paramArr[0]);
        param.topMargin = (int) (mHeight * paramArr[1]);
        livePkFailedView.setLayoutParams(param);

    }

    /**
     * 观看端失败view设置（总高度要加上虚拟按键的高度）
     *
     * @param livePkFailedView
     * @param paramArr
     */
    public static void setLiveShowPkFailedViewParam(View livePkFailedView, float[] paramArr) {
        if (livePkFailedView == null || paramArr == null || paramArr.length < 4 || !(livePkFailedView.getLayoutParams() instanceof RelativeLayout.LayoutParams)) {
            return;
        }
        final RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams) livePkFailedView.getLayoutParams();
       /* param.width = (int) paramArr[2];
        param.height = (int) paramArr[3];
        param.topMargin = (int) paramArr[1];
        param.leftMargin = (int) paramArr[0];*/
        final float mWidth = AppInit.displayMetrics.widthPixels;
        final float mHeight = AppInit.displayMetrics.heightPixels;
        param.width = (int) (mWidth * paramArr[2]);
        param.height = (int) (mHeight * paramArr[3]);
        param.leftMargin = (int) (mWidth * paramArr[0]);
        param.topMargin = (int) ((mHeight - DeviceUtils.getNavigationBarHeight(TheLApp.getContext())) * paramArr[1]);
        livePkFailedView.setLayoutParams(param);

    }

    /**
     * 观看端根据Pk开始消息获取对方Pk的数据
     *
     * @param pkStartNoticeBean
     * @return
     */
    public static LivePkFriendBean getLivePkFriendBean(LivePkStartNoticeBean pkStartNoticeBean) {
        if (pkStartNoticeBean == null) {
            return new LivePkFriendBean();
        }
        final LivePkFriendBean friendBean = new LivePkFriendBean();
        friendBean.liveStatus = LivePkFriendBean.LIVE_STATUS_PK;
        friendBean.id = pkStartNoticeBean.userId;
        friendBean.nickName = pkStartNoticeBean.nickName;
        friendBean.avatar = pkStartNoticeBean.avatar;
        return friendBean;
    }

    public static void playPkLastMinuteAnimView(final ImageView iconView, final LottieAnimationView live_pk_ae_view, LivePkAeAnimBean bean, final LivePkContract.LivePkAeAnimListener listener) {
        if (live_pk_ae_view == null || bean == null || iconView == null) {
            return;
        }
        final String path = LiveConstant.LIVE_PK_AE_PATH + bean.ae_anim_path;
        live_pk_ae_view.cancelAnimation();
        live_pk_ae_view.loop(false);
        live_pk_ae_view.setProgress(0);
        LottieComposition.Factory.fromAssetFileName(TheLApp.getContext(), path, new OnCompositionLoadedListener() {
            @Override
            public void onCompositionLoaded(@Nullable LottieComposition composition) {
                live_pk_ae_view.setComposition(composition);
                live_pk_ae_view.addAnimatorListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        live_pk_ae_view.setVisibility(View.VISIBLE);
                        if (listener != null) {
                            listener.onAnimStatus(live_pk_ae_view, LivePkContract.LivePkAeAnimListener.ANIM_START);
                        }
                        iconView.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        live_pk_ae_view.setVisibility(View.GONE);
                        if (listener != null) {
                            listener.onAnimStatus(live_pk_ae_view, LivePkContract.LivePkAeAnimListener.ANIM_END);
                        }
                        live_pk_ae_view.removeAnimatorListener(this);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        live_pk_ae_view.setVisibility(View.GONE);
                        if (listener != null) {
                            listener.onAnimStatus(live_pk_ae_view, LivePkContract.LivePkAeAnimListener.ANIM_CANCEL);
                        }
                        live_pk_ae_view.removeAnimatorListener(this);
                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {
                        if (listener != null) {
                            listener.onAnimStatus(live_pk_ae_view, LivePkContract.LivePkAeAnimListener.ANIM_REPEAT);
                        }
                    }
                });
                live_pk_ae_view.loop(true);
                live_pk_ae_view.playAnimation();
            }
        });

    }

    /**
     * 获取直播Log的filePath位置
     *
     * @return
     */
    public static String getLivePkLogFilePath() {
        final String dir = FileHelper.getInstance().getLiveLogPkDir();
        final String fileName = TheLConstants.LIVE_PK_LOG_PATH_NAME;
        return dir + "/" + fileName;
    }

    public static String getLivePkErrorLogStr(int event, Object[] data) {
        final String time = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS").format(System.currentTimeMillis()) + "";
        final String typeStr = ",livePkOrConnectMic,joinChannelError";
        final String ev = ",event=" + event;
        final int length = data.length;
        StringBuilder sb = new StringBuilder(time + typeStr + ev);
        for (int i = 0; i < length; i++) {
            sb.append(",").append("data[" + i + "]").append(":").append(data[i]);
        }
        return sb.toString();
    }

    public static void pushLivePointLog(AnalyticsBean analyticsBean) {

        sequenceSender(analyticsBean);

    }

    public static void sequenceSender(AnalyticsBean analyticsBean) {
        QiniuLogger.main(analyticsBean);
    }

    private static String encodeHeadInfo(String headInfo) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0, length = headInfo.length(); i < length; i++) {
            char c = headInfo.charAt(i);
            if (c <= '\u001f' || c >= '\u007f') {
                stringBuffer.append(String.format("\\u%04x", (int) c));
            } else {
                stringBuffer.append(c);
            }
        }
        return stringBuffer.toString();
    }

    /**
     * 关注了主播 消息
     *
     * @param payload
     * @return
     */
    public static LiveRoomMsgBean getFollowMsgBean(String payload) {
        if (TextUtils.isEmpty(payload)) {
            return null;
        }
        final LiveRoomMsgBean liveRoomMsgBean = GsonUtils.getObject(payload, LiveRoomMsgBean.class);
        liveRoomMsgBean.content = TheLApp.getContext().getString(R.string.followed_the_broadcaster);
        liveRoomMsgBean.type = LiveRoomMsgBean.TYPE_FOLLOW;

        return liveRoomMsgBean;
    }

    /**
     * 推荐了主播 消息
     *
     * @param payload
     * @return
     */
    public static LiveRoomMsgBean getRecommendMsgBean(String payload) {
        if (TextUtils.isEmpty(payload)) {
            return null;
        }
        final LiveRoomMsgBean liveRoomMsgBean = GsonUtils.getObject(payload, LiveRoomMsgBean.class);
        liveRoomMsgBean.content = TheLApp.getContext().getString(R.string.recommended_the_broadcaster);
        liveRoomMsgBean.type = LiveRoomMsgBean.TYPE_RECOMM;

        return liveRoomMsgBean;
    }

    /**
     * 分享了主播 消息
     *
     * @param payload
     * @return
     */
    public static LiveRoomMsgBean getSharetToMsgBean(String payload) {
        if (TextUtils.isEmpty(payload)) {
            return null;
        }
        final LiveRoomMsgBean liveRoomMsgBean = GsonUtils.getObject(payload, LiveRoomMsgBean.class);
        liveRoomMsgBean.content = TheLApp.getContext().getString(R.string.shared_the_live);
        liveRoomMsgBean.type = LiveRoomMsgBean.TYPE_SHARETO;
        return liveRoomMsgBean;
    }

    public static LiveRoomMsgBean getSendGiftMsgBean(String payload, SoftEnjoyBean softEnjoyBean) {

        if (TextUtils.isEmpty(payload) || softEnjoyBean == null || softEnjoyBean.list == null || softEnjoyBean.list.isEmpty()) {
            return null;
        }
        final LiveRoomMsgBean liveRoomMsgBean = GsonUtils.getObject(payload, LiveRoomMsgBean.class);
        final SoftGiftBean giftBean = getSoftGiftBeanFromList(liveRoomMsgBean.id, softEnjoyBean);
        if (giftBean == null) {
            return null;
        }
        liveRoomMsgBean.content = TheLApp.getContext().getString(R.string.sent_xx, liveRoomMsgBean.combo + "", giftBean.title);
        liveRoomMsgBean.giftIcon = giftBean.icon;
        liveRoomMsgBean.type = LiveRoomMsgBean.TYPE_GIFTCOMBO;
        liveRoomMsgBean.giftTitle = giftBean.title;

        return liveRoomMsgBean;
    }

    private static SoftGiftBean getSoftGiftBeanFromList(int id, SoftEnjoyBean softEnjoyBean) {
        if (softEnjoyBean == null || softEnjoyBean.list == null || softEnjoyBean.arlist == null) {
            return null;
        }

        L.d("LiveUtils", " getSoftGiftBeanFromList id : " + id);

        for (SoftGiftBean bean : softEnjoyBean.list) {

            L.d("LiveUtils", " getSoftGiftBeanFromList bean.id : " + bean.id);

            if (bean.id == id) {

                return bean;
            }
        }

        for (SoftGiftBean bean : softEnjoyBean.arlist) {

            L.d("LiveUtils", " getSoftGiftBeanFromList arlist bean.id : " + bean.id);

            if (bean.id == id) {

                return bean;
            }
        }
        return null;
    }

    /**
     * 根据icon高度和用户等级计算 icon的宽度
     *
     * @param height
     * @param level
     * @return
     */
    public static int getLevelImageWidth(int height, int level) {
        float width = height * 152f / 72f;
        if (level >= 30) {
            width = height * 264 / 72;
        } else if (level >= 26) {
            width = height * 208 / 72;
        }
        return (int) width;
    }

    public static SoftGiftBean getGiftBeanById(int id, @NonNull SoftEnjoyBean softEnjoyBean) {

        L.d("LiveUtils", " softEnjoyBean.list : " + softEnjoyBean.list);

        if (softEnjoyBean.list != null) {
            for (SoftGiftBean giftBean : softEnjoyBean.list) {
                if (giftBean.id == id) {
                    return giftBean;
                }
            }
        }

        return null;
    }

    public static String getLiveTime() {
        return DateUtils.getNowFormat() + " " + getCurrentTimeZone().replace("GMT", "").replace(":", "");
    }

    public static String getCurrentTimeZone() {

        Calendar cal = Calendar.getInstance();

        TimeZone timeZone = cal.getTimeZone();

//        String timeZoneId = Calendar.getInstance().getTimeZone().getID();

        return timeZone.getDisplayName(false, TimeZone.SHORT);
    }
}
