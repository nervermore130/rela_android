package com.thel.modules.main.home.moments.comment;

import com.thel.base.BasePresenter;
import com.thel.base.BaseView;
import com.thel.bean.comment.CommentBean;
import com.thel.bean.comment.MomentCommentBean;

import java.util.List;

/**
 * Created by liuyun on 2017/10/23.
 */

public class MomentCommentContract {

    interface View extends BaseView<MomentCommentContract.Presenter> {

        boolean isActive();

        void updateCommentUI(List<MomentCommentBean.MomentCommentLiseBean> commentList);
    }

    interface Presenter extends BasePresenter {

        void loadCommentList(String id, String limit, String cursor);

    }

}
