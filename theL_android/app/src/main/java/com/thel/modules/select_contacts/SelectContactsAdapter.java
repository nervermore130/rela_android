package com.thel.modules.select_contacts;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.ContactBean;
import com.thel.constants.TheLConstants;
import com.thel.utils.ImageUtils;

import java.util.ArrayList;

/**
 * Created by chad
 * Time 17/10/12
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */

public class SelectContactsAdapter extends BaseAdapter {

    private ArrayList<ContactBean> friendsList = new ArrayList<ContactBean>();

    private LayoutInflater mInflater;

    private boolean isSingleSelect;

    private boolean isCanSelected = false;

    public SelectContactsAdapter(ArrayList<ContactBean> searchlist, boolean singleSelect) {
        mInflater = (LayoutInflater) TheLApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        isSingleSelect = singleSelect;
        refreshAdapter(searchlist);
    }

    public void refreshAdapter(ArrayList<ContactBean> searchlist) {
        this.friendsList = searchlist;
    }

    public void setIsCanSelected(boolean isCanSelected) {
        this.isCanSelected = isCanSelected;
    }

    @Override
    public int getCount() {
        return friendsList.size();
    }

    @Override
    public Object getItem(int position) {
        return friendsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        HoldView holdView = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.select_contact_listitem, parent, false);
            holdView = new HoldView();
            holdView.portrait = convertView.findViewById(R.id.portrait);
            holdView.nickname = convertView.findViewById(R.id.nickname);
            holdView.select_btn_press = convertView.findViewById(R.id.btn_press);
            holdView.rel_portrait = convertView.findViewById(R.id.rel_portrait);
            convertView.setTag(holdView); // 把holdview缓存下来
        } else {
            holdView = (HoldView) convertView.getTag();
        }

        ContactBean searchBean = friendsList.get(position);

        if (isCanSelected) {
            if (isSingleSelect) {
                holdView.rel_portrait.setSelected(!searchBean.isSelected);
                holdView.select_btn_press.setVisibility(!searchBean.isSelected ? View.VISIBLE : View.GONE);
            } else {
                holdView.rel_portrait.setSelected(searchBean.isSelected);
                holdView.select_btn_press.setVisibility(searchBean.isSelected ? View.VISIBLE : View.GONE);
            }
        }else{
            holdView.rel_portrait.setSelected(false);
            holdView.select_btn_press.setVisibility(View.GONE);
        }

        holdView.portrait.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(searchBean.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE)));
        holdView.nickname.setText(searchBean.nickName);
        convertView.setTag(R.id.friend_bean_tag, searchBean);
        return convertView;
    }

    class HoldView {
        SimpleDraweeView portrait;
        TextView nickname;
        ImageView select_btn_press;
        View rel_portrait;
    }

}
