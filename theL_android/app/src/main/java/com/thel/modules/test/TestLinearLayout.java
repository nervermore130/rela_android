package com.thel.modules.test;

import android.content.Context;
import android.graphics.Canvas;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.thel.utils.L;

public class TestLinearLayout extends LinearLayout {
    public TestLinearLayout(Context context) {
        super(context);
    }

    public TestLinearLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public TestLinearLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        L.d("TestRelativeLayout", "------------LinearLayout onDraw----------");

    }

    @Override public void draw(Canvas canvas) {
        super.draw(canvas);

        L.d("TestRelativeLayout", "------------LinearLayout draw----------");

    }

}
