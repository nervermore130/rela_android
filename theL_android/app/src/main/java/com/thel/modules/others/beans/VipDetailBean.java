package com.thel.modules.others.beans;

import java.io.Serializable;

public class VipDetailBean implements Serializable {
    public int type;
    public String title;
    public int res_Index;
    public String desc;

    public VipDetailBean() {

    }

    public VipDetailBean(int type, String title, int res_Index, String desc) {
        this.type = type;
        this.title = title;
        this.res_Index = res_Index;
        this.desc = desc;
    }

}
