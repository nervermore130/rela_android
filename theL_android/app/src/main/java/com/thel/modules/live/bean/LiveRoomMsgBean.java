package com.thel.modules.live.bean;

import com.thel.base.BaseDataBean;

import java.io.Serializable;

public class LiveRoomMsgBean extends BaseDataBean implements Serializable {

    public String userId;
    public String nickName;
    public String avatar;
    public long time;
    public String type;
    public String content;
    public int vip;
    public int id;//礼物id
    public int combo;//连击
    public String giftIcon;//赠送的礼物的icon(本地缓冲获取，不是网络上给的)
    public int userLevel = 0;//用户等级
    public String toUserId;
    public String toNickname;
    public String giftTitle;
    public int iconSwitch = 1;//1显示用户等级 0不显示用户等级
    public int rank;//日榜排行 1 2 3

    //35级加入直播间，动态ui的数据
    public String joinTextColor;
    public String baseMapImage;
    public String gifImage;
    public String soundEffect;

    //35级以上用户的聊天背景
    public String gradualChangeColor1;
    public String gradualChangeColor2;
    public String backgroundTextColor;

    public int level;

    //11级以上用户升级的通知的数据
    public String upgradeUserId;
    public String upgradeAvatar;
    public String upgradeNickName;
    public int upgradeLevel;
    public int upgradeUserLevel;

    public static final String TYPE_JOIN = "join";
    public static final String TYPE_LEAVE = "leave";
    public static final String TYPE_BANED = "baned";
    public static final String TYPE_CONNECTING = "connecting";
    public static final String TYPE_CONNECT_FAILED = "connect_failed";
    public static final String TYPE_CONNECT_INTERRUPTED = "connect_interrupted";
    public static final String TYPE_CONNECT_SUCCEED = "connect_succeed";
    public static final String TYPE_EMOJI = "emoji";
    public static final String TYPE_MSG = "msg";
    public static final String TYPE_SYS_MSG = "sysmsg";
    public static final String TYPE_MIC_SORT = "mic_sort";
    public static final String TYPE_MIC_SORT_TIPS = "hostmsg";
    public static final String TYPE_UPLOADER_STATUS = "uploaderStatus";
    public static final String TYPE_NOTICE = "notice";
    public static final String TYPE_GIFT_MSG = "giftmsg";//赠送礼物消息
    public static final String TYPE_DANMU_MSG = "barragemsg";//弹幕信息
    public static final String TYPE_VISIT = "visit";//新用户加入
    public static final String TYPE_VIP_VISIT = "vipvisit";//Vip加入
    public static final String RESPONSE_TYPE_TOO_FAST = "too_fast";
    public static final String NETWORK_WARNING_NOTIFY = "notify";//网警提示
    public static final String TYPE_CONNECT_MIC = "linkmic";
    public static final String TYPE_UPDATE = "update";
    public static final String TYPE_CLOSE = "close";
    public static final String TYPE_ERRORALERT = "errorAlert";


    /**
     * 直播间内升级通知
     */
    public static final String TYPE_UPGRADE_EFFECTS = "upgrade_effects";
    /**
     * 所有直播间升级通知
     */
    public static final String TYPE_ALL_UPGRADE_EFFECTS = "all_upgrade_effects";

    /**
     * 免费弹幕
     */
    public static final String TYPE_FREE_BARRAGE = "free_barrage";

    public static final String TYPE_LIVE_PK = "pk";//直播Pk中返回的一些消息
    public static final String TYPE_LIVE_PK_RESPONSE_CODE_NOT_LIVING = "not_living";//主播不在直播
    public static final String TYPE_LIVE_PK_RESPONSE_CODE_IN_LINKMIC = "in_linkmic";//主播在连麦中
    public static final String TYPE_LIVE_PK_RESPONSE_CODE_IN_PK = "in_pk";//主播在Pk中
    public static final String TYPE_LIVE_PK_RESPONSE_CODE_LOW_CLI_VER = "low_cli_ver";//过低的客户端版本
    public static final String TYPE_LIVE_PK_RESPONSE_CODE_500 = "500";//服务端错误
    public static final String TYPE_LIVE_PK_RESPONSE_CODE_REJECT = "pk_reject";//上次发起PK被该主播拒绝，需等待10分钟才能对该主播再次发起PK

    public static final String PK_NOTICE_METHOD_REQUEST = "request";//请求
    public static final String PK_NOTICE_METHOD_RESPONSE = "response";//pk相应通知PK发起方
    public static final String PK_NOTICE_METHOD_START = "start";//服务器发送PK开始通知给双方直播室内的所有人
    public static final String PK_NOTICE_METHOD_CANCEL = "cancel";//服务器发送取消PK通知给接收方收
    public static final String PK_NOTICE_METHOD_HANGUP = "hangup";//服务器发送挂断通知给被挂断方收
    public static final String PK_NOTICE_METHOD_TIMEOUT = "timeout";//超时
    public static final String PK_NOTICE_METHOD_SUMMARY = "summary";//进入总结阶段 服务器发送 总结剩余时间 和 双方助攻前三名 给双方直播室内的所有人
    public static final String PK_NOTICE_METHOD_STOP = "stop";//PK结束时 服务器发送 PK结束 和 双方助攻前三名 通知给双方直播室内的所有人
    public static final String PK_NOTICE_METHOD_PKGEM = "pkgem";//主播收到礼物时 服务器发送主播软妹币增量通知给双方直播室内的所有人

    /***4.4.0新增广播消息类型code***/
    public static final String TYPE_FOLLOW = "follow";//关注
    public static final String TYPE_RECOMM = "recomm";//推荐
    public static final String TYPE_SHARETO = "shareto";//分享
    public static final String TYPE_GIFTCOMBO = "giftcombo";//连击
    public static final String TYPE_TOPGIFT = "topgift";//最贵礼物

    /**
     * 4.8.0多人连麦
     */
    public static final String TYPE_AUDIO_BROADCAST_CODE = "audiobroadcast";//多人连麦广播Code
    public static final String TYPE_METHOD_ONSEAT = "onSeat";//上麦
    public static final String TYPE_METHOD_OFFSEAT = "offSeat"; //下麦
    public static final String TYPE_METHOD_MUTE = "mute";//闭麦
    public static final String TYPE_METHOD_GUEST_GIFT = "guestGift";//用户给嘉宾送礼物
    public static final String TYPE_METHOD_START_ENCOUNTER = "startEncounter";//开始相遇倒计时
    public static final String TYPE_METHOD_END_ENCOUNTER = "endEncounter";//结束相遇
    public static final String TYPE_METHOD_ENCOUNTERSB = "encounterSb";//点击配对某人
    public static final String TYPE_METHOD_UPLOAD_VOLUMN = "audioVolumn";//说话声音音量提示
    /**
     * 4.8.2排麦
     */
    public static final String TYPE_METHOD_MIC_ON = "waitMic_on";//排麦开启
    public static final String TYPE_METHOD_MIC_OFF = "waitMic_off";//关闭排麦
    public static final String TYPE_METHOD_MIC_ADD = "waitMic_add";//排麦列表增加
    public static final String TYPE_METHOD_MIC_DEL = "waitMic_del";//排麦列表减少
    public static final String TYPE_METHOD_MIC_REQ = "waitMic_req";//请求上麦
    public static final String TYPE_METHOD_MIC_CANCEL = "waitMic_cancel";//取消上麦
    public static final String TYPE_METHOD_MIC_AGREE = "waitMic_agree";//同意上麦
    public static final String TYPE_METHOD_MIC_LIST = "waitMicList";//获取排麦列表
    //micStatus
    public static final String TYPE_MIC_STATUS_ON = "on";//开麦
    public static final String TYPE_MIC_STATUS_OFF = "off";//闭麦
    public static final String TYPE_MIC_STATUS_CLOSE = "close";//主播禁麦

    public static final String LIVE_MSG_RESPONSE_CODE_OK = "OK";

    public static final String MSG_SEND_CODE_TYPE_PK = "pk";

    public static final String LINK_MIC_RESULT_YES = "yes";

    public static final String METHOD_LINK_MIC_GUEST_RESPONSE = "guestResponse";

    public static final String TYPE_TOP_TODAY = "top_today";

    /**
     * 系统送豆
     */
    public static final String FREE_GOLD = "free_gold";

    /**
     * 连击礼物大于999会收到一条礼物视频
     */
    public static final String CODE_SPECIAL_GIFT = "special_gift";

    @Override public String toString() {
        return "LiveRoomMsgBean{" +
                "userId='" + userId + '\'' +
                ", nickName='" + nickName + '\'' +
                ", avatar='" + avatar + '\'' +
                ", time=" + time +
                ", logType='" + type + '\'' +
                ", content='" + content + '\'' +
                ", vip=" + vip +
                ", id=" + id +
                ", combo=" + combo +
                ", giftIcon='" + giftIcon + '\'' +
                ", userLevel=" + userLevel +
                ", toUserId='" + toUserId + '\'' +
                ", toNickname='" + toNickname + '\'' +
                ", giftTitle='" + giftTitle + '\'' +
                '}';
    }
}
