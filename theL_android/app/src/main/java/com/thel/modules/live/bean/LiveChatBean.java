package com.thel.modules.live.bean;

import com.thel.base.BaseDataBean;

import java.io.Serializable;
import java.util.List;

/**
 * @author Setsail
 */
public class LiveChatBean extends BaseDataBean implements Serializable {

    public String host;

    public int port;

    public String channel;

    public String token;

    public List<HostBean> hosts;

    public String userSig;

    public int liveChatSwitch;

    public class HostBean implements Serializable {
        public String host;
        public int port;
    }

    @Override
    public String toString() {
        return "LiveChatBean{" +
                "host='" + host + '\'' +
                ", port=" + port +
                ", channel='" + channel + '\'' +
                ", token='" + token + '\'' +
                '}';
    }
}
