package com.thel.modules.main.video_discover.autoplay;

import android.view.View;
import android.widget.FrameLayout;

import com.thel.bean.video.VideoBean;

/**
 * Created by waiarl on 2018/3/20.
 */

public interface VideoAutoPlayImp<T extends View> {
    FrameLayout getContainer();

    int getVisibleHeightPercent();

    int getPosition();

    T bindAutoPlayVideoCtrl(AutoPlayVideoCtrl recyclerViewStateBean);

    T getView();

    RecyclerViewStateImp.RecyclerViewScrollStateChangedListener getRecyclerViewScrollStateChangedListener();

    T stopPlay();

    T showVideoLoading();

    T cancelVideoLoading();

    String getVideoUrl();

    String getMomentId();

    VideoBean getVideoBean();
}
