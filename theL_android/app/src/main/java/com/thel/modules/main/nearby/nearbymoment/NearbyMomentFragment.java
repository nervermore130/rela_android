package com.thel.modules.main.nearby.nearbymoment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.bumptech.glide.Glide;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseAdapter;
import com.thel.bean.MusicInfoBean;
import com.thel.bean.gift.WinkCommentBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.moments.MomentsListBean;
import com.thel.constants.BundleConstants;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.growingio.GrowingIoConstant;
import com.thel.imp.BaseFunctionFragment;
import com.thel.imp.TittleClickListener;
import com.thel.manager.ListVideoVisibilityManager;
import com.thel.modules.main.home.moments.comment.MomentCommentActivity;
import com.thel.modules.main.home.moments.theme.ThemeDetailActivity;
import com.thel.modules.main.home.moments.web.WebViewActivity;
import com.thel.modules.main.me.bean.LogInfoBean;
import com.thel.modules.main.me.match.eventcollect.collect.LiveLogUtils;
import com.thel.modules.others.VipConfigActivity;
import com.thel.ui.adapter.MomentsAdapter;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.decoration.DefaultItemDivider;
import com.thel.utils.DialogUtil;
import com.thel.utils.L;
import com.thel.utils.LocationUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by waiarl on 2017/10/12.
 */

public class NearbyMomentFragment extends BaseFunctionFragment implements NearbyMomentContract.View, TittleClickListener {
    private final String TAG = NearbyMomentFragment.class.getSimpleName();
    private String scrollMomentId;
    private SwipeRefreshLayout swipe_container;
    private RecyclerView recyclerView;
    private LinearLayoutManager manager;
    private MomentsAdapter adapter;
    private List<MomentsBean> list = new ArrayList<>();
    private NearbyMomentContract.Presenter presenter;
    private boolean haveNextPage = true;
    private int curPage = 1;
    private MomentReleaseReceiver receiver;
    private Location lastLocation;

    private static boolean scrollToPosition = true;
    private String pageId;
    private String from_page_id;
    private String from_page;
    private String latitude;
    private String longitude;

    public static NearbyMomentFragment getInstance(Bundle bundle) {
        NearbyMomentFragment instance = new NearbyMomentFragment();
        instance.setArguments(bundle);
        scrollToPosition = true;
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Bundle bundle = getArguments();
        scrollMomentId = bundle.getString(TheLConstants.BUNDLE_KEY_MOMENT_ID);

        from_page = ShareFileUtils.getString(ShareFileUtils.rootSwitchPage, "");
        from_page_id = ShareFileUtils.getString(ShareFileUtils.rootSwitchPageId, "");

        pageId = Utils.getPageId();
        latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
        longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_nearby_moment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        findViewById(view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setListener();
        new NearbyMomentPresenter(this);
        initData();
        registerReceiver();
        adapter.registerNewCommentNotify(getActivity());
    }

    private void registerReceiver() {
        receiver = new MomentReleaseReceiver();
        getContext().registerReceiver(receiver, new IntentFilter(TheLConstants.BROADCAST_FAILED_MOMENTS_CHECK_ACTION));
    }

    private void initData() {
        if (null != swipe_container) {
            swipe_container.post(new Runnable() {
                @Override
                public void run() {
                    swipe_container.setRefreshing(true);
                }
            });
        }
        refreshData();
    }

    private void refreshData() {
        curPage = 1;
        presenter.getRefreshNearbyMomentList();
        if (lastLocation == null) {
            requestGaodeMapClient();
        }
    }

    private void requestGaodeMapClient() {
        L.i(TAG, "aMapLocation getErrorCode111111");

        final AMapLocationClient locationClient = new AMapLocationClient(TheLApp.getContext());
        locationClient.setLocationListener(new AMapLocationListener() {
            @Override
            public void onLocationChanged(AMapLocation aMapLocation) {

                if (aMapLocation != null && aMapLocation.getErrorCode() == 0) {
                    L.i(TAG, "aMapLocation getLatitude" + aMapLocation.getLatitude() + "aMapLocation.getLongitude() " + aMapLocation.getLongitude());

                    LocationUtils.saveLocation(aMapLocation.getLatitude(), aMapLocation.getLongitude(), aMapLocation.getCity());
                } else if (aMapLocation != null && aMapLocation.getErrorCode() == 12) { //定位权限没有打开
                    LocationUtils.saveLocation(0.0, 0.0, "");

                } else {
                    L.i(TAG, "aMapLocation getErrorCode" + aMapLocation.getErrorCode());

                }

                locationClient.stopLocation();
                locationClient.onDestroy();
            }
        });
        final AMapLocationClientOption mLocationOption = new AMapLocationClientOption();
        mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Battery_Saving);
        locationClient.setLocationOption(mLocationOption);
        locationClient.startLocation();
    }


    private void findViewById(View view) {
        swipe_container = view.findViewById(R.id.swipe_container);
        ViewUtils.initSwipeRefreshLayout(swipe_container);
        recyclerView = view.findViewById(R.id.recyclerview);
        manager = new LinearLayoutManager(getActivity());
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(manager);
        recyclerView.addItemDecoration(new DefaultItemDivider(TheLApp.context, LinearLayoutManager.VERTICAL, R.color.bg_color, (int) TheLApp.getContext().getResources().getDimension(R.dimen.wide_divider_height), true, false));

        adapter = new MomentsAdapter(R.layout.item_moments, null, GrowingIoConstant.ENTRY_NEARBY_PAGE, "aroundmoments", pageId, from_page, from_page_id);
        recyclerView.setAdapter(adapter);
    }

    private void setListener() {
        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshData();
            }
        });
        adapter.setOnItemClickListener(new BaseAdapter.OnItemClickListener<MomentsBean>() {
            @Override
            public void onItemClick(View view, MomentsBean item, int position) {

                Intent intent;
                Bundle bundle = new Bundle();
                if (MomentsBean.MOMENT_TYPE_THEME.equals(item.momentsType)) {
//                    intent = new Intent(getContext(), ThemeDetailActivity.class);
//                    bundle.putSerializable(TheLConstants.BUNDLE_KEY_MOMENT_BEAN, item);
//                    intent.putExtras(bundle);
//                    getActivity().startActivityForResult(intent, TheLConstants.BUNDLE_CODE_MOMENTS_ACTIVITY);
                    FlutterRouterConfig.Companion.gotoThemeDetails(item.momentsId);
                } else if (MomentsBean.MOMENT_TYPE_AD.equals(item.momentsType)) {
                    if (!TextUtils.isEmpty(item.appSchemeUrl)) {
                        intent = new Intent(getContext(), VipConfigActivity.class);
                    } else {
                        bundle.putString(BundleConstants.URL, item.adUrl);
                        intent = new Intent(getContext(), WebViewActivity.class);
                    }
                    bundle.putSerializable(TheLConstants.BUNDLE_KEY_MOMENT_BEAN, item);
                    intent.putExtras(bundle);
                    getActivity().startActivityForResult(intent, TheLConstants.BUNDLE_CODE_MOMENTS_ACTIVITY);
                } else {
//                    intent = new Intent(getContext(), MomentCommentActivity.class);
                    FlutterRouterConfig.Companion.gotoMomentDetails(item.momentsId);
                }
            }
        });
        adapter.setOnLoadMoreListener(new BaseRecyclerViewAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                recyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        if (haveNextPage) {
                            L.d("Nearby", " curPage : " + curPage);
                            presenter.getMoreNearbyMomentList(curPage);
                        } else {//为0说明没有更多数据
                            adapter.openLoadMore(0, false);
                            if (list.size() > 0) {
                                View view = getActivity().getLayoutInflater().inflate(R.layout.load_more_footer_layout, (ViewGroup) recyclerView.getParent(), false);
                                ((TextView) view.findViewById(R.id.text)).setText(getString(R.string.info_no_more));
                                adapter.addFooterView(view);
                            }
                        }
                    }
                });
            }
        });
        adapter.setReloadMoreListener(new BaseRecyclerViewAdapter.ReloadMoreListener() {
            @Override
            public void reloadMore() {
                recyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        adapter.removeAllFooterView();
                        adapter.openLoadMore(true);
                        presenter.getMoreNearbyMomentList(curPage);
                    }
                });
            }
        });
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {

                    if (adapter != null && adapter.getData() != null) {

                        ListVideoVisibilityManager.getInstance().calculatorItemPercent(manager, newState, adapter, TheLConstants.EntryConstants.ENTRY_NEARBYMOMENTS);

                    }

                    try {
                        if (getContext() != null) {
                            Glide.with(getContext()).resumeRequests();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {

                    try {
                        if (getContext() != null) {
                            Glide.with(getContext()).pauseRequests();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

            }
        });
    }

    @Override
    public void setPresenter(NearbyMomentContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showRefreshNearbyMomentList(MomentsListBean momentsBean) {
        curPage = 2;
        haveNextPage = momentsBean.haveNextPage;
        list.clear();
        list.addAll(momentsBean.momentsList);
        MomentsAdapter.filterUserMoments(list);
        MomentsAdapter.filterBlack(list);
        MomentsAdapter.filterAdMoment(list);
        MomentsAdapter.filterOneMoments(list);
        if (adapter.getData() != null && adapter.getData().size() > 0) {
            MomentsAdapter.filterEquallyMoment(adapter.getData(), list);
        }
        adapter.setNewData(list);
        adapter.openLoadMore(list.size(), true);
        if (scrollToPosition) {
            scrollToMomentById(scrollMomentId);
            scrollToPosition = false;
        }
    }

    /**
     * 跳到指定momentid
     *
     * @param momentId
     */
    private void scrollToMomentById(String momentId) {
        if (TextUtils.isEmpty(momentId)) {
            return;
        }
        final int headCount = adapter.getHeaderLayoutCount();
        final int size = list.size();
        for (int i = 0; i < size; i++) {
            if (momentId.equals(list.get(i).momentsId)) {
                final int pos = i;
                recyclerView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        manager.scrollToPositionWithOffset(pos + headCount, 0);
                    }
                }, 100);
                return;
            }
        }
    }

    @Override
    public void showMoreNearbyMomentList(MomentsListBean momentsBean) {
        curPage += 1;
        haveNextPage = momentsBean.haveNextPage;
        list.addAll(momentsBean.momentsList);
        adapter.notifyDataChangedAfterLoadMore(true, list.size());
    }

    @Override
    public void emptyData() {

    }

    @Override
    public void loadMoreFailed() {
        recyclerView.post(new Runnable() {
            @Override
            public void run() {
                adapter.loadMoreFailed((ViewGroup) recyclerView.getParent());

            }
        });
    }

    @Override
    public void requestFinished() {
        if (swipe_container != null) {
            swipe_container.postDelayed(new Runnable() {

                @Override
                public void run() {
                    if (swipe_container != null && swipe_container.isRefreshing()) {
                        swipe_container.setRefreshing(false);
                    }
                }
            }, 1000);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMusicInfo(MusicInfoBean musicInfoBean) {

        L.d(TAG, "--------onMusicInfo-------");

        List<MomentsBean> data = adapter.getData();

        for (MomentsBean momentsBean : data) {
            if (momentsBean.momentsId.equals(musicInfoBean.momentId)) {
                momentsBean.isMusicPlaying = !momentsBean.isMusicPlaying;
            } else {
                momentsBean.isMusicPlaying = false;
            }
        }

        adapter.notifyDataSetChanged();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onDestroy() {
        try {
            getContext().unregisterReceiver(receiver);
        } catch (Exception e) {
            e.printStackTrace();
        }

        adapter.unregisterNewCommentNotify(getActivity());
        super.onDestroy();
    }

    @Override
    public void onTitleClick() {
        if (isVisible() && swipe_container != null && !swipe_container.isRefreshing() && recyclerView != null) {
            recyclerView.smoothScrollToPosition(0);
        }
    }

    @Override
    public void onMomentReport(String momentId) {
        super.onMomentReport(momentId);
        onBlackOneMoment(momentId);
    }

    @Override
    public void onMomentDelete(String momentId) {
        super.onMomentDelete(momentId);
        onBlackOneMoment(momentId);
    }

    /**
     * 屏蔽他的日志，刷新界面
     */
    @Override
    public void onBlackherMoment(String userId) {

        DialogUtil.showToastShort(TheLApp.getContext(), getString(R.string.myblock_activity_block_success));
        if (adapter != null) {
            List<MomentsBean> list = adapter.getData();
            if (list != null) {
                ListIterator<MomentsBean> iterable = list.listIterator();
                while (iterable.hasNext()) {
                    MomentsBean momentsBean = iterable.next();
                    if (String.valueOf(momentsBean.userId).equals(userId)) {
                        iterable.remove();
                    }
                }
            }
            adapter.notifyDataSetChanged();
        }
        closeLoading();
    }

    @Override
    public void onBlackOneMoment(String momentId) {

        if (adapter != null) {
            List<MomentsBean> list = adapter.getData();
            if (list != null) {
                ListIterator<MomentsBean> iterable = list.listIterator();
                while (iterable.hasNext()) {
                    MomentsBean momentsBean = iterable.next();
                    if (String.valueOf(momentsBean.momentsId).equals(momentId)) {
                        iterable.remove();
                    }
                }
            }
            adapter.notifyDataSetChanged();
        }
        closeLoading();
    }

    @Override
    public void onLikeStatusChanged(String momentId, boolean like, String myUserId, WinkCommentBean myWinkUserBean) {
        super.onLikeStatusChanged(momentId, like, myUserId, myWinkUserBean);
    }

    @Override
    public void onFollowStatusChanged(int followStatus, String userId, String nickName, String avatar) {
        super.onFollowStatusChanged(followStatus, userId, nickName, avatar);
        final int size = adapter.getData().size();
        final int headerCount = adapter.getHeaderLayoutCount();
        for (int i = 0; i < size; i++) {
            MomentsBean bean = adapter.getData().get(i);
            if (userId.equals(bean.userId)) {
                bean.followerStatus = followStatus;
                adapter.notifyItemChanged(headerCount + i);
                return;
            }
        }
        if (!TextUtils.isEmpty(userId)) {
            if (followStatus == 1) {
                traceNearbyMomentLog("follow", userId);

            } else {
                traceNearbyMomentLog("unfollow", userId);

            }
        }
    }

    @Override
    public void onBlackUsersChanged(String userId, boolean isAdd) {
        super.onBlackUsersChanged(userId, isAdd);
        if (isAdd) {
            onBlackherMoment(userId);
        }
    }

    class MomentReleaseReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (presenter != null) {
                refreshData();
            }
        }
    }

    public void traceNearbyMomentLog(String activity, String userid) {
        try {

            LogInfoBean logInfoBean = new LogInfoBean();
            logInfoBean.page = "aroundmoments";
            logInfoBean.page_id = pageId;
            logInfoBean.activity = activity;
            logInfoBean.from_page_id = from_page_id;
            logInfoBean.from_page = from_page;
            logInfoBean.lat = latitude;
            logInfoBean.lng = longitude;

            LogInfoBean.LogsDataBean logsDataBean = new LogInfoBean.LogsDataBean();

            if (!TextUtils.isEmpty(userid)) {
                logsDataBean.user_id = userid;
            }
            logInfoBean.data = logsDataBean;

            LiveLogUtils.getInstance().addLog(logInfoBean);

        } catch (Exception e) {

        }
    }
}
