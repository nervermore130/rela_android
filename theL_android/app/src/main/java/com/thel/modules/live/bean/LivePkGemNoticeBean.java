package com.thel.modules.live.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by waiarl on 2017/12/4.
 * 主播收到的礼物bean
 */

public class LivePkGemNoticeBean extends BaseLivePkBean implements Serializable {
    public String userId;//主播id
    public float pkGem;//类型: fload64 主播的软妹币增量

    public List<LivePkAssisterBean> assisters=new ArrayList<>();
}
