package com.thel.modules.live.stream;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.media.AudioFormat;
import android.opengl.GLSurfaceView;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;

import com.qiniu.pili.droid.streaming.AudioSourceCallback;
import com.qiniu.pili.droid.streaming.CameraStreamingSetting;
import com.qiniu.pili.droid.streaming.MediaStreamingManager;
import com.qiniu.pili.droid.streaming.MicrophoneStreamingSetting;
import com.qiniu.pili.droid.streaming.StreamingPreviewCallback;
import com.qiniu.pili.droid.streaming.StreamingProfile;
import com.qiniu.pili.droid.streaming.StreamingState;
import com.qiniu.pili.droid.streaming.StreamingStateChangedListener;
import com.qiniu.pili.droid.streaming.SurfaceTextureCallback;
import com.thel.R;
import com.thel.tusdk.plain.CameraConfig;
import com.thel.tusdk.plain.EncodingConfig;
import com.thel.tusdk.plain.TuConfig;

import org.lasque.tusdk.api.engine.TuSdkFilterEngine;
import org.lasque.tusdk.api.engine.TuSdkFilterEngineImpl;
import org.lasque.tusdk.api.video.preproc.filter.TuSDKVideoProcesser;
import org.lasque.tusdk.core.api.engine.TuSdkEngineProcessor;
import org.lasque.tusdk.core.seles.SelesEGLContextFactory;
import org.lasque.tusdk.core.seles.SelesParameters;
import org.lasque.tusdk.core.seles.tusdk.FilterWrap;
import org.lasque.tusdk.core.struct.TuSdkSize;
import org.lasque.tusdk.core.type.ColorFormatType;
import org.lasque.tusdk.core.utils.JVMUtils;
import org.lasque.tusdk.core.utils.TLog;
import org.lasque.tusdk.core.utils.ThreadHelper;
import org.lasque.tusdk.core.utils.hardware.CameraConfigs;
import org.lasque.tusdk.core.utils.hardware.InterfaceOrientation;
import org.lasque.tusdk.core.utils.image.ImageOrientation;
import org.lasque.tusdk.video.editor.TuSdkMediaEffectData;
import org.lasque.tusdk.video.editor.TuSdkMediaFilterEffectData;
import org.lasque.tusdk.video.editor.TuSdkMediaPlasticFaceEffect;
import org.lasque.tusdk.video.editor.TuSdkMediaSkinFaceEffect;

import java.net.URISyntaxException;
import java.nio.IntBuffer;
import java.util.HashMap;
import java.util.List;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;

import static org.lasque.tusdk.video.editor.TuSdkMediaEffectData.TuSdkMediaEffectDataType.TuSdkMediaEffectDataTypeFilter;
import static org.lasque.tusdk.video.editor.TuSdkMediaEffectData.TuSdkMediaEffectDataType.TuSdkMediaEffectDataTypePlasticFace;
import static org.lasque.tusdk.video.editor.TuSdkMediaEffectData.TuSdkMediaEffectDataType.TuSdkMediaEffectDataTypeSkinFace;
import static org.lasque.tusdk.video.editor.TuSdkMediaEffectData.TuSdkMediaEffectDataType.TuSdkMediaEffectDataTypeSticker;

/**
 * Created by chad
 * Time 19/1/15
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */
abstract public class TuSdkPreviewManager implements SurfaceTextureCallback, StreamingPreviewCallback, StreamingStateChangedListener {

    public static final String TAG = "TuSdkManager";

    protected CameraStreamingSetting mCameraStreamingSetting;

    protected CameraConfig mCameraConfig;

    protected EncodingConfig mEncodingConfig = new EncodingConfig();

    protected StreamingProfile mProfile = new StreamingProfile();

    protected int mCurrentCamFacingIndex;

    protected MediaStreamingManager mMediaStreamingManager;

    protected GLSurfaceView mGLSurfaceView;

    protected EGLContext mEGLCurrentContext;

    protected boolean mNoFace = false;

    protected boolean mIsReady;

    public TuSdkPreviewManager(CameraConfig mCameraConfig, GLSurfaceView glSurfaceView) {

        this.mGLSurfaceView = glSurfaceView;

        this.mCameraConfig = mCameraConfig;

        mCameraStreamingSetting = buildCameraStreamingSetting();

        mCurrentCamFacingIndex = mCameraConfig.mFrontFacing ? 1 : 0;

        HandlerThread handlerThread = new HandlerThread("TUTU");
        handlerThread.start();
        mTuHandler = new Handler(handlerThread.getLooper()) {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == MESSAGE_REPLAY_MEDIA_EFFECT) {
                    try {
                        replayMediaEffect();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        };

    }

    private CameraStreamingSetting buildCameraStreamingSetting() {

        // 使用自定义美颜
        mCameraConfig.mIsCustomFaceBeauty = true;

        CameraStreamingSetting cameraStreamingSetting = new CameraStreamingSetting();
        cameraStreamingSetting.setCameraId(mCameraConfig.mFrontFacing ? Camera.CameraInfo.CAMERA_FACING_FRONT : Camera.CameraInfo.CAMERA_FACING_BACK)
                .setCameraPrvSizeLevel(mCameraConfig.mSizeLevel)
                .setCameraPrvSizeRatio(mCameraConfig.mSizeRatio)
                .setFocusMode(mCameraConfig.mFocusMode)
                .setContinuousFocusModeEnabled(mCameraConfig.mContinuousAutoFocus)
                .setFrontCameraPreviewMirror(mCameraConfig.mPreviewMirror)
                .setFrontCameraMirror(mCameraConfig.mEncodingMirror).setRecordingHint(false)
                .setResetTouchFocusDelayInMs(3000)
                .setBuiltInFaceBeautyEnabled(!mCameraConfig.mIsCustomFaceBeauty)
                .setFaceBeautySetting(new CameraStreamingSetting.FaceBeautySetting(1.0f, 1.0f, 0.8f));

        if (mCameraConfig.mIsFaceBeautyEnabled) {
            cameraStreamingSetting.setVideoFilter(CameraStreamingSetting.VIDEO_FILTER_TYPE.VIDEO_FILTER_BEAUTY);
        } else {
            cameraStreamingSetting.setVideoFilter(CameraStreamingSetting.VIDEO_FILTER_TYPE.VIDEO_FILTER_NONE);
        }
        return cameraStreamingSetting;
    }

    public void initStreamingManager() {
        mGLSurfaceView.setEGLContextFactory(new TuSDKEGLContextFactory());
        mMediaStreamingManager = new MediaStreamingManager(mGLSurfaceView.getContext(), mGLSurfaceView, mEncodingConfig.mCodecType);

        StreamingProfile.AudioProfile aProfile = null;
        StreamingProfile.VideoProfile vProfile = null;

        if (!mEncodingConfig.mIsAudioOnly) {
            // video quality
            if (mEncodingConfig.mIsVideoQualityPreset) {
                mProfile.setVideoQuality(mEncodingConfig.mVideoQualityPreset);
            } else {
                vProfile = new StreamingProfile.VideoProfile(
                        mEncodingConfig.mVideoQualityCustomFPS,
                        mEncodingConfig.mVideoQualityCustomBitrate * 1024,
                        mEncodingConfig.mVideoQualityCustomMaxKeyFrameInterval,
                        mEncodingConfig.mVideoQualityCustomProfile
                );
            }

            // video size
            if (mEncodingConfig.mIsVideoSizePreset) {
                mProfile.setEncodingSizeLevel(mEncodingConfig.mVideoSizePreset);
            } else {
                mProfile.setPreferredVideoEncodingSize(mEncodingConfig.mVideoSizeCustomWidth, mEncodingConfig.mVideoSizeCustomHeight);
            }

            // video misc
            mProfile.setEncodingOrientation(mEncodingConfig.mVideoOrientationPortrait ? StreamingProfile.ENCODING_ORIENTATION.PORT : StreamingProfile.ENCODING_ORIENTATION.LAND);
            mProfile.setEncoderRCMode(mEncodingConfig.mVideoRateControlQuality ? StreamingProfile.EncoderRCModes.QUALITY_PRIORITY : StreamingProfile.EncoderRCModes.BITRATE_PRIORITY);
            mProfile.setBitrateAdjustMode(mEncodingConfig.mBitrateAdjustMode);
            mProfile.setFpsControllerEnable(mEncodingConfig.mVideoFPSControl);
            mProfile.setYuvFilterMode(mEncodingConfig.mYuvFilterMode);
            if (mEncodingConfig.mBitrateAdjustMode == StreamingProfile.BitrateAdjustMode.Auto) {
                mProfile.setVideoAdaptiveBitrateRange(mEncodingConfig.mAdaptiveBitrateMin * 1024, mEncodingConfig.mAdaptiveBitrateMax * 1024);
            }
        }

        if (mEncodingConfig.mIsPictureStreamingEnabled) {
            if (mEncodingConfig.mPictureStreamingFilePath == null) {
                mProfile.setPictureStreamingResourceId(R.drawable.pause_publish);
            } else {
                mProfile.setPictureStreamingFilePath(mEncodingConfig.mPictureStreamingFilePath);
            }
        }

        // audio quality
        if (mEncodingConfig.mIsAudioQualityPreset) {
            mProfile.setAudioQuality(mEncodingConfig.mAudioQualityPreset);
        } else {
            aProfile = new StreamingProfile.AudioProfile(
                    mEncodingConfig.mAudioQualityCustomSampleRate,
                    mEncodingConfig.mAudioQualityCustomBitrate * 1024
            );
        }

        // custom
        if (aProfile != null || vProfile != null) {
            StreamingProfile.AVProfile avProfile = new StreamingProfile.AVProfile(vProfile, aProfile);
            mProfile.setAVProfile(avProfile);
        }

        try {
            String pubUrl = setPubUrl();
            if (!TextUtils.isEmpty(pubUrl)) {
                mProfile.setPublishUrl(pubUrl);
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        MicrophoneStreamingSetting microphoneStreamingSetting = null;
        if (mEncodingConfig.mAudioStereo) {
            /**
             * Notice !!! {@link AudioFormat#CHANNEL_IN_STEREO} is NOT guaranteed to work on all devices.
             */
            microphoneStreamingSetting = new MicrophoneStreamingSetting();
            microphoneStreamingSetting.setChannelConfig(AudioFormat.CHANNEL_IN_STEREO);
        }
        mMediaStreamingManager.prepare(mCameraStreamingSetting, microphoneStreamingSetting, null, mProfile);

        if (mCameraConfig.mIsCustomFaceBeauty) {
            mMediaStreamingManager.setSurfaceTextureCallback(this);
        }
        mMediaStreamingManager.setStreamingStateListener(this);

        // 开启预览回调
        mMediaStreamingManager.setStreamingPreviewCallback(this);

        if (audioSourceCallback != null) {
            mMediaStreamingManager.setAudioSourceCallback(audioSourceCallback);
        }
    }

    public void onResume() {
        if (mEncodingConfig.mIsPictureStreamingEnabled && mIsReady) {
            mMediaStreamingManager.togglePictureStreaming();
        } else {
            mMediaStreamingManager.resume();
        }
    }

    public void onPause() {
        if (mEncodingConfig.mIsPictureStreamingEnabled) {
            mMediaStreamingManager.togglePictureStreaming();
        } else {
            mIsReady = false;
            mMediaStreamingManager.pause();
        }
    }

    public void onDestroy() {
        mMediaStreamingManager.destroy();
        destroyFilterEngine();
        mEGLCurrentContext = null;
        mTuHandler.getLooper().quit();
    }

    public void stopMediaStreaming() {
        if (mMediaStreamingManager != null) {
            mMediaStreamingManager.pause();
            mMediaStreamingManager.stopStreaming();
            mMediaStreamingManager.setStreamingPreviewCallback(null);
        }
    }

    /// ========================= TuSDK 相关 ========================= ///

    public static final int MESSAGE_REPLAY_MEDIA_EFFECT = 0x66;
    protected Handler mTuHandler;
    private List<TuSdkMediaEffectData> allMediaEffectData = null;

    // TuSDK Filter Engine
    protected TuSdkFilterEngine mFilterEngine;

    //是否输出处理后的数据  在切换的时候为false
    protected boolean mEnableProcess = true;

    private FilterWrap mFilterWrap;

    private boolean mCurrentUseSkinNatural = false;

    private float mCurrentFilterPrecentValue = 0;

    /**
     * 准备滤镜引擎
     */
    private void prepareFilterEngine() {

        if (mFilterEngine != null) {
            destroyFilterEngine();
        }

        mFilterEngine = new TuSdkFilterEngineImpl(true, true);

        //设置特效委托
        mFilterEngine.setMediaEffectDelegate(mMediaEffectDelegate);

        //人脸状态委托
        mFilterEngine.setFaceDetectionDelegate(mFaceDetectionDelegate);

        mFilterEngine.setFilterChangedListener(new TuSdkFilterEngine.TuSdkFilterEngineListener() {
            @Override
            public void onPictureDataCompleted(IntBuffer intBuffer, TuSdkSize tuSdkSize) {

            }

            @Override
            public void onPreviewScreenShot(Bitmap bitmap) {

            }

            @Override
            public void onFilterChanged(FilterWrap filterWrap) {
                mFilterWrap = filterWrap;
            }
        });

        // 设置摄像头方向
        CameraConfigs.CameraFacing facing = (mCurrentCamFacingIndex == CameraStreamingSetting.CAMERA_FACING_ID.CAMERA_FACING_FRONT.ordinal()) ? CameraConfigs.CameraFacing.Front : CameraConfigs.CameraFacing.Back;
        mFilterEngine.setCameraFacing(facing);

        //根据屏幕方向设置滤镜方向
        if (((Activity) mGLSurfaceView.getContext()).getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED
                || ((Activity) mGLSurfaceView.getContext()).getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
            mFilterEngine.setInterfaceOrientation(InterfaceOrientation.Portrait);
        } else {
            mFilterEngine.setInterfaceOrientation(InterfaceOrientation.LandscapeRight);
        }
        updateFilterEngineCameraDisplayOrientation(facing);

        // 设置是否开启动态贴纸功能
        mFilterEngine.setEnableLiveSticker(true);

        mFilterEngine.setEnableOutputYUVData(true);
        mFilterEngine.setYuvOutputImageFormat(ColorFormatType.NV21);
        mFilterEngine.setYuvOutputOrienation(getSnatchFrameOrienation());

        mFilterEngine.onSurfaceCreated();

        if (allMediaEffectData != null)
            return;

        ThreadHelper.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mFilterEngine != null) {
                    // 移除不可叠加的特效
                    mFilterEngine.removeMediaEffectsWithType(TuSdkMediaEffectDataTypeFilter);
                    mFilterEngine.removeMediaEffectsWithType(TuSdkMediaEffectData.TuSdkMediaEffectDataType.TuSdkMediaEffectDataTypeComic);
                    TuSdkMediaFilterEffectData effectData = new TuSdkMediaFilterEffectData(TuConfig.CAMERA_FILTER_CODES[0]);
                    if (effectData.getFilterArg("mixied") != null) {
                        mCurrentFilterPrecentValue = effectData.getFilterArg("mixied").getPrecentValue();
                    }
                    mFilterEngine.addMediaEffectData(effectData);

                    switchConfigSkin(mCurrentUseSkinNatural);

                    switchBeautyPlasticConfig();
                }
            }
        }, 500);
    }

    private TuSDKVideoProcesser.TuSDKVideoProcessorMediaEffectDelegate mMediaEffectDelegate = new TuSDKVideoProcesser.TuSDKVideoProcessorMediaEffectDelegate() {
        @Override
        public void didApplyingMediaEffect(TuSdkMediaEffectData tuSdkMediaEffectData) {
            allMediaEffectData = mFilterEngine.getAllMediaEffectData();
        }

        @Override
        public void didRemoveMediaEffect(List<TuSdkMediaEffectData> list) {
            allMediaEffectData = mFilterEngine.getAllMediaEffectData();
        }
    };

    private TuSdkEngineProcessor.TuSdkVideoProcesserFaceDetectionDelegate mFaceDetectionDelegate = new TuSdkEngineProcessor.TuSdkVideoProcesserFaceDetectionDelegate() {
        @Override
        public void onFaceDetectionResult(TuSdkEngineProcessor.TuSdkVideoProcesserFaceDetectionResultType tuSdkVideoProcesserFaceDetectionResultType, int faceCount) {
            mNoFace = tuSdkVideoProcesserFaceDetectionResultType == TuSdkEngineProcessor.TuSdkVideoProcesserFaceDetectionResultType.TuSDKVideoProcesserFaceDetectionResultTypeNoFaceDetected;
            List<TuSdkMediaEffectData> filterEffects = mFilterEngine.mediaEffectsWithType(TuSdkMediaEffectDataTypeSticker);
            if (filterEffects != null && filterEffects.size() > 0) {
                if (mOnTuSdkListener != null) mOnTuSdkListener.onFaceDetection(!mNoFace);
            }
        }
    };

    private void replayMediaEffect() {
        if (allMediaEffectData != null) {
            for (TuSdkMediaEffectData data : allMediaEffectData) {
                if (!mEnableProcess) break;
                if (data.getMediaEffectType() == TuSdkMediaEffectDataTypePlasticFace) {
                    TuSdkMediaPlasticFaceEffect plasticFaceEffect = (TuSdkMediaPlasticFaceEffect) data.clone();
                    List<SelesParameters.FilterArg> filterArgs = data.getFilterArgs();
                    mFilterEngine.addMediaEffectData(plasticFaceEffect);
                    for (SelesParameters.FilterArg arg : plasticFaceEffect.getFilterArgs()) {
                        if (arg.equalsKey("eyeSize")) {// 大眼
                            arg.setMaxValueFactor(0.75f);
                        }
                        if (arg.equalsKey("chinSize")) {// 瘦脸
                            arg.setMaxValueFactor(0.75f);
                        }
                        if (arg.equalsKey("noseSize")) {// 瘦鼻
                            arg.setMaxValueFactor(0.6f);
                        }

                    }
                    for (SelesParameters.FilterArg arg : filterArgs) {
                        SelesParameters.FilterArg filterArg = plasticFaceEffect.getFilterArg(arg.getKey());
                        filterArg.setPrecentValue(arg.getPrecentValue());
                    }
                    plasticFaceEffect.submitParameters();
                } else if (data.getMediaEffectType() == TuSdkMediaEffectDataTypeFilter) {
                    mFilterEngine.removeMediaEffectsWithType(TuSdkMediaEffectData.TuSdkMediaEffectDataType.TuSdkMediaEffectDataTypeFilter);
                    TuSdkMediaFilterEffectData dataClone = (TuSdkMediaFilterEffectData) data.clone();
                    TuSdkMediaFilterEffectData filterEffectData = new TuSdkMediaFilterEffectData(dataClone.getFilterCode());
                    SelesParameters.FilterArg mixied = dataClone.getFilterArg("mixied");
                    if (mixied != null) {
                        filterEffectData.getFilterArg("mixied").setPrecentValue(mCurrentFilterPrecentValue);
                    }
                    mFilterEngine.addMediaEffectData(filterEffectData);
                } else if (data.getMediaEffectType() == TuSdkMediaEffectDataTypeSkinFace) {

                    TuSdkMediaSkinFaceEffect skinFaceEffect = new TuSdkMediaSkinFaceEffect(mCurrentUseSkinNatural);

                    // 美白
                    SelesParameters.FilterArg whiteningArgs = skinFaceEffect.getFilterArg("whitening");
                    whiteningArgs.setMaxValueFactor(0.35f);//设置最大值限制
                    // 磨皮
                    SelesParameters.FilterArg smoothingArgs = skinFaceEffect.getFilterArg("smoothing");
                    smoothingArgs.setMaxValueFactor(0.66f);//设置最大值限制
                    // 紅潤
                    SelesParameters.FilterArg ruddyArgs = skinFaceEffect.getFilterArg("ruddy");
                    ruddyArgs.setMaxValueFactor(0.3f);//设置最大值限制

                    mFilterEngine.addMediaEffectData(skinFaceEffect);

                    // 获取key值并显示到调节栏
                    for (SelesParameters.FilterArg filterArg : data.getFilterArgs()) {
                        SelesParameters.FilterArg arg = skinFaceEffect.getFilterArg(filterArg.getKey());
                        arg.setPrecentValue(filterArg.getPrecentValue());
                    }

                    skinFaceEffect.submitParameters();
                } else {
                    mFilterEngine.addMediaEffectData(data.clone());
                }
            }
        }
    }

    /**
     * 销毁 TuSDKFilterEngine
     */
    private void destroyFilterEngine() {
        mEnableProcess = false;
        mEGLCurrentContext = null;
        if (mFilterWrap != null) {
            mFilterWrap.getFilter().destroy();
            mFilterWrap.destroy();
        }
        if (mFilterEngine != null) {
            mFilterEngine.release();
            mFilterEngine = null;
            //强制GC 解决在一部分低端魔改机型上垃圾回收机制的缺陷
            JVMUtils.runGC();
        }
    }

    /**
     * 微整形
     */
    private void switchBeautyPlasticConfig() {

        /** 微整形默认值  Float 为进度值 */
        HashMap<String, Float> mDefaultBeautyPercentParams = new HashMap<String, Float>() {
            {
                put("eyeSize", 0.3f);//大眼
                put("chinSize", 0.3f);//瘦脸
                put("noseSize", 0.2f);//瘦鼻
                put("mouthWidth", 0.5f);//嘴形
                put("archEyebrow", 0.5f);//眉型
                put("jawSize", 0.5f);//下巴
                put("eyeAngle", 0.5f);//眼角
                put("eyeDis", 0.5f);//眼距
            }
        };

        // 添加一个默认微整形特效
        TuSdkMediaPlasticFaceEffect plasticFaceEffect = new TuSdkMediaPlasticFaceEffect();
        mFilterEngine.addMediaEffectData(plasticFaceEffect);
        for (SelesParameters.FilterArg arg : plasticFaceEffect.getFilterArgs()) {
            if (arg.equalsKey("eyeSize")) {// 大眼
                arg.setMaxValueFactor(0.75f);
            }
            if (arg.equalsKey("chinSize")) {// 瘦脸
                arg.setMaxValueFactor(0.75f);
            }
            if (arg.equalsKey("noseSize")) {// 瘦鼻
                arg.setMaxValueFactor(0.6f);
            }

        }
        for (String key : mDefaultBeautyPercentParams.keySet()) {
            TLog.e("key -- %s", mDefaultBeautyPercentParams.get(key));
            submitPlasticFaceParamter(key, mDefaultBeautyPercentParams.get(key));
        }
    }

    /**
     * 应用整形值
     *
     * @param key
     * @param progress
     */
    private void submitPlasticFaceParamter(String key, float progress) {
        List<TuSdkMediaEffectData> filterEffects = mFilterEngine.mediaEffectsWithType(TuSdkMediaEffectDataTypePlasticFace);

        if (filterEffects.size() == 0) return;

        // 只能添加一个滤镜特效
        TuSdkMediaPlasticFaceEffect filterEffect = (TuSdkMediaPlasticFaceEffect) filterEffects.get(0);
        filterEffect.submitParameter(key, progress);
    }

    /**
     * 美颜
     *
     * @param useSkinNatural true 自然(精准)美颜 false 极致美颜
     */
    private void switchConfigSkin(boolean useSkinNatural) {
        if (mFilterEngine == null) return;

        mCurrentUseSkinNatural = useSkinNatural;

        TuSdkMediaSkinFaceEffect skinFaceEffect = new TuSdkMediaSkinFaceEffect(useSkinNatural);

        // 美白
        SelesParameters.FilterArg whiteningArgs = skinFaceEffect.getFilterArg("whitening");
        whiteningArgs.setMaxValueFactor(0.35f);//设置最大值限制
        // 磨皮
        SelesParameters.FilterArg smoothingArgs = skinFaceEffect.getFilterArg("smoothing");
        smoothingArgs.setMaxValueFactor(0.66f);//设置最大值限制

        // 紅潤
        SelesParameters.FilterArg ruddyArgs = skinFaceEffect.getFilterArg("ruddy");
        ruddyArgs.setMaxValueFactor(0.3f);//设置最大值限制

        whiteningArgs.setPrecentValue(0.3f);//设置默认显示

        smoothingArgs.setPrecentValue(0.3f);//设置默认显示
        mFilterEngine.addMediaEffectData(skinFaceEffect);
    }

    private void updateFilterEngineCameraDisplayOrientation(CameraConfigs.CameraFacing facing) {

        switch (mCameraStreamingSetting.getCameraDisplayOrientation()) {
            case 90:
                mFilterEngine.setInputImageOrientation(facing == CameraConfigs.CameraFacing.Back ? ImageOrientation.Right : ImageOrientation.LeftMirrored);
                mFilterEngine.setOutputImageOrientation(facing == CameraConfigs.CameraFacing.Back ? ImageOrientation.Left : ImageOrientation.LeftMirrored);
                break;
            case 270:
                mFilterEngine.setInputImageOrientation(facing == CameraConfigs.CameraFacing.Back ? ImageOrientation.Up : ImageOrientation.RightMirrored);
                mFilterEngine.setOutputImageOrientation(facing == CameraConfigs.CameraFacing.Back ? ImageOrientation.Up : ImageOrientation.RightMirrored);
                break;
            default:
                mFilterEngine.setInputImageOrientation(ImageOrientation.Up);
                mFilterEngine.setOutputImageOrientation(ImageOrientation.Up);
                break;
        }
    }

    /**
     * 获取抓取帧的输出方向
     *
     * @return ImageOrientation
     */
    private ImageOrientation getSnatchFrameOrienation() {
        if (mFilterEngine == null) return ImageOrientation.LeftMirrored;
        if (mFilterEngine.getCameraFacing() == CameraConfigs.CameraFacing.Front) {
            if (android.os.Build.MODEL.contains("Nexus")) {
                return ImageOrientation.RightMirrored;
            } else {
                return ImageOrientation.LeftMirrored;
            }
        } else
            return ImageOrientation.RightMirrored;
    }

    /**
     * 切换相机方向
     */
    public boolean switchCameraOrienation() {
        boolean camFaceFront = false;
        mCurrentCamFacingIndex = (mCurrentCamFacingIndex + 1) % CameraStreamingSetting.getNumberOfCameras();
        CameraStreamingSetting.CAMERA_FACING_ID facingId;
        if (mCurrentCamFacingIndex == CameraStreamingSetting.CAMERA_FACING_ID.CAMERA_FACING_BACK.ordinal()) {
            facingId = CameraStreamingSetting.CAMERA_FACING_ID.CAMERA_FACING_BACK;
        } else if (mCurrentCamFacingIndex == CameraStreamingSetting.CAMERA_FACING_ID.CAMERA_FACING_FRONT.ordinal()) {
            facingId = CameraStreamingSetting.CAMERA_FACING_ID.CAMERA_FACING_FRONT;
            camFaceFront = true;
        } else {
            facingId = CameraStreamingSetting.CAMERA_FACING_ID.CAMERA_FACING_3RD;
        }
        Log.i(TAG, "switchCamera:" + facingId);
        mMediaStreamingManager.switchCamera(facingId);
        mEnableProcess = false;
        return camFaceFront;
    }

    /**
     * 目前仅调整 eyeSize eyeSize eyeSize eyeSize,其他参数都用默认值
     *
     * @param key
     * @param progress
     */
    public void adjustBeautyParams(String key, float progress) {
        List<TuSdkMediaEffectData> filterEffects = null;
        if (key.equals("chinSize") || key.equals("eyeSize")) {
            filterEffects = mFilterEngine.mediaEffectsWithType(TuSdkMediaEffectDataTypePlasticFace);
        } else if (key.equals("whitening") || key.equals("smoothing")) {
            progress = progress * 0.7f;
            filterEffects = mFilterEngine.mediaEffectsWithType(TuSdkMediaEffectDataTypeSkinFace);
        } else {
            Log.d(TAG, "adjustBeautyParams error");
        }

        if (filterEffects == null || filterEffects.size() == 0) return;

        // 只能添加一个滤镜特效
        TuSdkMediaEffectData filterEffect = filterEffects.get(0);
        filterEffect.submitParameter(key, progress);
    }

    @Override
    public void onSurfaceCreated() {
        Log.i(TAG, "onSurfaceCreated");
        /**
         * only used in custom beauty algorithm case
         */
        // 准备 TuSDKFilterEngine
        prepareFilterEngine();
        if (mOnTuSdkListener != null) {
            mOnTuSdkListener.onSurfaceCreated();
        }
    }

    @Override
    public void onSurfaceChanged(int width, int height) {
        Log.i(TAG, "onSurfaceChanged width:" + width + ",height:" + height);
        /**
         * only used in custom beauty algorithm case
         */
        if (mFilterEngine != null)
            mFilterEngine.onSurfaceChanged(width, height);
    }

    @Override
    public void onSurfaceDestroyed() {
        Log.i(TAG, "onSurfaceDestroyed");
        /**
         * only used in custom beauty algorithm case
         */
        destroyFilterEngine();
    }

    @Override
    public int onDrawFrame(int texId, int texWidth, int texHeight, float[] transformMatrix) {
        /**
         * When using custom beauty algorithm, you should return a new texId from the SurfaceTexture.
         * newTexId should not equal with texId, Otherwise, there is no filter effect.
         */
        int textureId;
        try {
            if (mFilterEngine == null || !mEnableProcess) {
                textureId = texId;
            } else {
                textureId = mFilterEngine.processFrame(texId, texWidth, texHeight, System.nanoTime()
                );
            }
        } catch (Exception e) {
            e.printStackTrace();
            textureId = texId;
        }

        if (mOnTuSdkListener != null) {
            mOnTuSdkListener.onFrameAvailable(mEGLCurrentContext, textureId, texWidth, texHeight, transformMatrix);
        }
        return textureId;
    }

    @Override
    public boolean onPreviewFrame(byte[] bytes, int i, int i1, int i2, int i3, long l) {
        /**
         * When using custom beauty algorithm in sw encode mode, you should change the bytes array's values here
         * eg: byte[] beauties = readPixelsFromGPU();
         * System.arraycopy(beauties, 0, bytes, 0, bytes.length);
         *
         */
        if (mFilterEngine != null && mEnableProcess)
            mFilterEngine.snatchFrame(bytes);
        return true;
    }

    @Override
    public void onStateChanged(StreamingState streamingState, Object o) {
        switch (streamingState) {
            case READY:
                mIsReady = true;
                mEnableProcess = true;
                mTuHandler.removeCallbacksAndMessages(null);
                mTuHandler.sendEmptyMessageDelayed(MESSAGE_REPLAY_MEDIA_EFFECT, 500);
                break;
            case CAMERA_SWITCHED:
                mEnableProcess = true;
                break;
        }
    }

    protected OnTuSdkListener mOnTuSdkListener;

    public void setOnTuSdkListener(OnTuSdkListener mOnTuSdkListener) {
        this.mOnTuSdkListener = mOnTuSdkListener;
    }

    protected AudioSourceCallback audioSourceCallback;

    public void setAudioSourceCallback(AudioSourceCallback audioSourceCallback) {
        this.audioSourceCallback = audioSourceCallback;
    }

    public interface OnTuSdkListener {

        void onSurfaceCreated();

        void onFrameAvailable(EGLContext eglContext, int textureId, int width, int height, float[] transformMatrix);

        void onFaceDetection(boolean visible);
    }

//    private class TuSDKEGLContextFactory implements GLSurfaceView.EGLContextFactory {
//        private int EGL_CONTEXT_CLIENT_VERSION = 0x3098;
//
//        private TuSdkPreviewManager mTuSdkStreamManager;
//
//        public TuSDKEGLContextFactory(TuSdkPreviewManager tuSdkStreamManager) {
//            this.mTuSdkStreamManager = tuSdkStreamManager;
//        }
//
//        public EGLContext createContext(EGL10 egl, EGLDisplay display, EGLConfig eglConfig) {
//            checkEglError("before createContext", egl);
//            int[] attrib_list = {EGL_CONTEXT_CLIENT_VERSION, 2, EGL10.EGL_NONE};
//
//            EGLContext ctx;
//
//            if (mTuSdkStreamManager.mEGLCurrentContext == null) {
//                mTuSdkStreamManager.mEGLCurrentContext = egl.eglCreateContext(display, eglConfig,
//                        EGL10.EGL_NO_CONTEXT, attrib_list);
//                ctx = mTuSdkStreamManager.mEGLCurrentContext;
//            } else {
//                ctx = mTuSdkStreamManager.mEGLCurrentContext;
//            }
//            checkEglError("after createContext", egl);
//            return ctx;
//        }
//
//        public void destroyContext(EGL10 egl, EGLDisplay display, EGLContext context) {
//            if (mTuSdkStreamManager.mEGLCurrentContext == null) {
//                egl.eglDestroyContext(display, context);
//            }
//        }
//
//        private void checkEglError(String prompt, EGL10 egl) {
//            int error;
//            while ((error = egl.eglGetError()) != EGL10.EGL_SUCCESS) {
//                Log.d(TAG, String.format(Locale.US, "%s: EGL error: 0x%x", prompt, error));
//            }
//        }
//    }

    private class TuSDKEGLContextFactory extends SelesEGLContextFactory {

        public TuSDKEGLContextFactory() {
            super(2);
        }

        @Override
        public EGLContext createContext(EGL10 egl10, EGLDisplay eglDisplay, EGLConfig eglConfig) {
            if (TuSdkPreviewManager.this.mEGLCurrentContext == null) {
                TuSdkPreviewManager.this.mEGLCurrentContext = super.createContext(egl10, eglDisplay, eglConfig);
            }
            return TuSdkPreviewManager.this.mEGLCurrentContext;
        }

        @Override
        public void destroyContext(EGL10 egl, EGLDisplay display, javax.microedition.khronos.egl.EGLContext context) {
            egl.eglMakeCurrent(display, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, context);
            destroyFilterEngine();
            if (TuSdkPreviewManager.this.mEGLCurrentContext == null) {
                super.destroyContext(egl, display, context);
            }
        }
    }

    /**
     * 设置推流地址
     *
     * @return
     */
    public abstract String setPubUrl();
}
