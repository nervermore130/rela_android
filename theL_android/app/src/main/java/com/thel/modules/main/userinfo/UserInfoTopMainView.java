package com.thel.modules.main.userinfo;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.StringRes;
import androidx.core.content.ContextCompat;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseDataBean;
import com.thel.bean.user.UserInfoBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.imp.follow.FollowStatusChangedImpl;
import com.thel.imp.follow.FollowStatusChangedListener;
import com.thel.imp.picupload.PicUploadContract;
import com.thel.imp.picupload.PicUploadUtils;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.main.me.aboutMe.UpdateUserInfoActivity;
import com.thel.modules.main.me.bean.RoleBean;
import com.thel.modules.main.userinfo.fan.UserInfoFollowersActivity;
import com.thel.modules.others.UserInfoWinksActivity;
import com.thel.modules.select_image.SelectLocalImagesActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.RequestConstants;
import com.thel.ui.dialog.ActionSheetDialog;
import com.thel.utils.AppInit;
import com.thel.utils.DateUtils;
import com.thel.utils.DialogUtil;
import com.thel.utils.ImageUtils;
import com.thel.utils.L;
import com.thel.utils.MD5Utils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.thel.modules.main.me.aboutMe.UpdateUserInfoActivity.FROM_PAGE_SIGN_NAME;

/**
 * Created by waiarl on 2017/10/17.
 */

public class UserInfoTopMainView extends RelativeLayout {
    private final Context context;
    private RelativeLayout rel_top;
    private View lin_left_infos;
    private ImageView me_background;
    private RelativeLayout rel_avatar;
    private ImageView img_avatar;
    private RelativeLayout rel_partner;
    private ImageView img_avatar_right;
    private ImageView img_avatar_left;
    private TextView me_nickname;
    private TextView txt_love_days;
    private TextView me_txt_num_wink;
    private TextView me_txt_num_fans;
    private TextView txt_infos;
    private LinearLayout lin_profile;
    private LinearLayout sing_name_container;
    private ImageView img_base_info;
    private LinearLayout lin_follow;
    private ImageView img_follow;
    private TextView txt_follow;
    private TextView txt_sign_name;

    private UserInfoBean userInfoBean;
    private String tAge;
    private String tHoroscope;
    private String tAffection;
    private String userId;
    private boolean isMyself = false;
    private int followStatus;
    private UserInfoContract.Presenter presenter;
    private int fansNum = 0;
    private LinearLayout lin_auth;
    private TextView txt_auth;
    private LinearLayout lin_revise_info;
    private ImageView img_vip;
    private ImageView img_auth;
    private ImageView iv_sign_name;
    private TextView txt_distance;
    private String imageLocalPath;
    private SparseArray<RoleBean> relationshipMap = new SparseArray<RoleBean>();
    public static String PAGE_TYPE_FOLLOW = "follow";
    public static String PAGE_TYPE_FANS = "fans";

    public enum ValueName {
        TAGE, THOROSCOPE, TAFFECTION
    }

    public UserInfoTopMainView(Context context) {
        this(context, null);
    }

    public UserInfoTopMainView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public UserInfoTopMainView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        init();
        initViewData();
        initData();
        setListener();
    }

    private void initData() {
        initLocalData();
        initRelationshipMap();

    }

    private void initRelationshipMap() {
        relationshipMap.put(1, new RoleBean(0, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[0]));
        relationshipMap.put(6, new RoleBean(1, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[1]));
        relationshipMap.put(7, new RoleBean(2, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[2]));
        relationshipMap.put(2, new RoleBean(3, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[3]));
        relationshipMap.put(3, new RoleBean(4, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[4]));
        relationshipMap.put(4, new RoleBean(5, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[5]));
        relationshipMap.put(5, new RoleBean(6, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[6]));
        relationshipMap.put(0, new RoleBean(7, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[7]));


    }

    public void init() {
        inflate(context, R.layout.userinfo_main_top_view, this);
        rel_top = findViewById(R.id.rel_top);
        me_background = findViewById(R.id.me_background);
        lin_left_infos = findViewById(R.id.lin_left_infos);
        rel_avatar = findViewById(R.id.rel_avatar);
        img_avatar = findViewById(R.id.img_avatar);
        rel_partner = findViewById(R.id.rel_partner);
        img_avatar_right = findViewById(R.id.img_avatar_right);
        img_avatar_left = findViewById(R.id.img_avatar_left);
        me_nickname = findViewById(R.id.me_nickname);
        txt_love_days = findViewById(R.id.txt_love_days);
        me_txt_num_wink = findViewById(R.id.me_txt_num_wink);
        me_txt_num_fans = findViewById(R.id.me_txt_num_fans);
        txt_infos = findViewById(R.id.txt_infos);
        lin_profile = findViewById(R.id.lin_profile);
        sing_name_container = findViewById(R.id.sing_name_container);
        img_base_info = findViewById(R.id.img_base_info);
        lin_follow = findViewById(R.id.lin_follow);
        img_follow = findViewById(R.id.img_follow);
        txt_follow = findViewById(R.id.txt_follow);
        txt_sign_name = findViewById(R.id.txt_sign_name);
        iv_sign_name = findViewById(R.id.iv_sign_name);
        /***4.5.0 认证信息***/
        lin_auth = findViewById(R.id.lin_auth);
        txt_auth = findViewById(R.id.txt_auth);
        /***4.5.0修改资料***/
        lin_revise_info = findViewById(R.id.lin_revise_info);
        img_vip = findViewById(R.id.img_vip);
        img_auth = findViewById(R.id.img_auth);
        txt_distance = findViewById(R.id.txt_distance);
    }

    private void initViewData() {
        lin_auth.setVisibility(View.GONE);
        lin_revise_info.setVisibility(View.GONE);
    }

    public void setPresenter(UserInfoContract.Presenter presenter) {
        this.presenter = presenter;
    }

    public UserInfoTopMainView initView(String userId) {
        if (TextUtils.isEmpty(userId)) {
            return this;
        }
        this.userId = userId;
        isMyself = userId.equals(ShareFileUtils.getString(ShareFileUtils.ID, ""));
        setMySelfView();
        return this;
    }

    private void setMySelfView() {
        if (isMyself) {
            lin_follow.setVisibility(GONE);
            lin_revise_info.setVisibility(View.VISIBLE);
        } else {
            lin_follow.setVisibility(VISIBLE);
            lin_revise_info.setVisibility(View.GONE);

        }
    }

    public UserInfoTopMainView initView(final UserInfoBean userInfoBean) {
        if (userInfoBean == null) {
            return this;
        }
        this.userInfoBean = userInfoBean;
        followStatus = userInfoBean.followStatus;
        fansNum = userInfoBean.followerNum;

        ImageLoaderManager.imageLoaderDefault(me_background, R.color.tab_normal, userInfoBean.bgImage, AppInit.displayMetrics.widthPixels, AppInit.displayMetrics.heightPixels);
        //用户昵称
        me_nickname.setText(userInfoBean.nickName);
        setVipImage(userInfoBean, img_vip);
        //挤眼
        setWinkNums(userInfoBean.winkNum);
        //粉丝
        //显示粉丝
        setFansNum();

        final StringBuilder sb = new StringBuilder();
        //年龄
        tAge = TextUtils.isEmpty(userInfoBean.age) ? "" : userInfoBean.age + getString(R.string.updatauserinfo_activity_age_unit);

        if (tAge.length() > 0) {
            sb.append(tAge).append(", ");
        } else {
            if (!TextUtils.isEmpty(userInfoBean.birthday)) {
                sb.append("18岁").append(", ");
            }

        }
        //星座
        tHoroscope = !TextUtils.isEmpty(userInfoBean.birthday) ? DateUtils.date2Constellation(userInfoBean.birthday) : "";

        if (tHoroscope.length() > 0) {
            sb.append(tHoroscope).append(", ");
        }
        //身高体重
        String tHandW = getHandW(userInfoBean.height, userInfoBean.weight);
        if (tHandW.length() > 0) {
            sb.append(tHandW).append(", ");
        }

        //感情状态
        tAffection = getRelationShip(userInfoBean);


        if (tAffection.length() > 0) {
            sb.append(tAffection).append(", ");
        }

        String text = sb.toString();

        if (text.endsWith(", ")) {
            text = text.substring(0, text.length() - 2);
        }

        if (text.length() == 0) {
            txt_infos.setVisibility(INVISIBLE);
        } else {
            txt_infos.setVisibility(VISIBLE);
        }

        L.d("UserInfoTopMainView", " text : " + text);

        txt_infos.setText(text);

        //关注按钮
        if (!isMyself) {//是自己不显示
            refreshFollowBtn(userInfoBean.followStatus);
        }
        //距离

        txt_distance.setText(userInfoBean.distance + "");

        //认证
        setAuthImage(userInfoBean, img_auth);
        txt_auth.setText(userInfoBean.verifyIntro);
        //距离与认证只显示一个
        if (userInfoBean.verifyType > 0) {
            lin_auth.setVisibility(View.VISIBLE);
            txt_distance.setVisibility(View.GONE);
        } else {
            lin_auth.setVisibility(View.GONE);
            if (!TextUtils.isEmpty(userInfoBean.distance)) {
                txt_distance.setVisibility(View.VISIBLE);

            } else {
                txt_distance.setVisibility(View.GONE);

            }
        }

        //头像
        rel_partner.setVisibility(View.GONE);
        rel_avatar.setVisibility(View.GONE);
        if (userInfoBean.paternerd == null) {//如果没有伴侣
            rel_avatar.setVisibility(View.VISIBLE);//单人头像可见
            ImageLoaderManager.imageLoaderDefaultCircle(img_avatar, R.mipmap.icon_user, userInfoBean.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE);

            txt_love_days.setVisibility(View.GONE);
        } else {//有伴侣，显示恋爱天数
            rel_partner.setVisibility(View.VISIBLE);//情侣头像可见
            ImageLoaderManager.imageLoaderDefaultCircle(img_avatar_right, R.mipmap.icon_user, userInfoBean.paternerd.paternerAvatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE);
            ImageLoaderManager.imageLoaderDefaultCircle(img_avatar_left, R.mipmap.icon_user, userInfoBean.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE);

            txt_love_days.setVisibility(View.VISIBLE);
            txt_love_days.setText(String.format(getString(R.string.updatauserinfo_activity_anniversary), userInfoBean.paternerd.paternerNickName, userInfoBean.paternerd.days));
        }
        if (!TextUtils.isEmpty(userInfoBean.intro)) {
            txt_sign_name.setText(userInfoBean.intro);
        } else {
            if (UserUtils.getMyUserId().equals(userInfoBean.id)) {
                txt_sign_name.setText(getString(R.string.edit_intro));
            } else {
                sing_name_container.setVisibility(GONE);
            }
        }
        if (UserUtils.getMyUserId().equals(userInfoBean.id)) {
            iv_sign_name.setVisibility(VISIBLE);
            sing_name_container.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, UpdateUserInfoActivity.class);
                    intent.putExtra(UpdateUserInfoActivity.FROM_PAGE, FROM_PAGE_SIGN_NAME);
                    intent.putExtra("selfintroduction", userInfoBean.intro);

                    context.startActivity(intent);
                }
            });
        } else {
            iv_sign_name.setVisibility(INVISIBLE);
        }

        return this;
    }

    /**
     * 设置会员
     *
     * @param userInfoBean
     * @param img_vip
     */
    private void setVipImage(UserInfoBean userInfoBean, ImageView img_vip) {
        int level = userInfoBean.level;
        if (level >= TheLConstants.VIP_LEVEL_RES.length) {
            level = TheLConstants.USER_LEVEL_RES.length - 1;
        }
        if (level < 1) {
            img_vip.setVisibility(View.GONE);
        } else {
            img_vip.setVisibility(View.VISIBLE);
            img_vip.setImageResource(TheLConstants.VIP_LEVEL_RES[level]);
        }
    }

    /**
     * 设置认证
     *
     * @param userInfoBean
     * @param img_auth
     */
    private void setAuthImage(UserInfoBean userInfoBean, ImageView img_auth) {
        if (userInfoBean.verifyType >= 1) {// 加V用户，显示认证信息
            if (userInfoBean.verifyType == 1) {
                img_auth.setImageResource(R.mipmap.icn_verify_person);
            } else {
                img_auth.setImageResource(R.mipmap.icn_verify_enterprise);
            }
        }
    }


    /**
     * 设置粉丝数
     */
    private void setFansNum() {
        if (fansNum > 1) {
            me_txt_num_fans.setText(getSpanText(getString(R.string.userinfo_activity_fans_even, fansNum), fansNum + ""));
        } else {
            me_txt_num_fans.setText(getSpanText(getString(R.string.userinfo_activity_fans_old, fansNum), fansNum + ""));
        }
    }

    /**
     * 设置关注人数
     **/

  /*  private void setFollowNum() {
        if (followedNum > 1) {
            me_txt_num_follow.setText(getSpanText(getString(R.string.followed_fum_even, followedNum), followedNum + ""));
        } else {
            me_txt_num_follow.setText(getSpanText(getString(R.string.followed_fum_old, followedNum), followedNum + ""));
        }
    }
*/
    protected String getString(@StringRes int res) {
        return context.getString(res);
    }

    protected String getString(@StringRes int resId, Object... formatArgs) {
        return context.getString(resId, formatArgs);
    }

    private ImageView drawIconImageView(int size) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(size, size);
        params.setMargins(Utils.dip2px(TheLApp.getContext(), 5), 0, 0, 0);
        ImageView imageView = new ImageView(TheLApp.getContext());
        imageView.setLayoutParams(params);
        return imageView;
    }

    public void setWinkNums(final String winkNumStr) {
        me_txt_num_wink.setText(getSpanText(getString(R.string.userinfo_activity_wink_even, winkNumStr + ""), winkNumStr + ""));
    }

    private SpannableString getSpanText(String txt, String target) {
        final int start = txt.indexOf(target);
        final int end = start + target.length();
        final SpannableString ss = new SpannableString(txt);
        ss.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.white)), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return ss;
    }

    //星座
    private String getHoroscope(String horoscope) {
        int horoscope_index = 0;
        try {
            horoscope_index = Integer.parseInt(horoscope);
        } catch (Exception e) {
            horoscope_index = 0;
        }
        if (horoscope_index < 0 || horoscope_index >= constellation_list.size()) {
            horoscope_index = 0;
        }
        return " , " + constellation_list.get(horoscope_index);
    }

    //身高体重
    private String getHandW(String height, String weight) {
        int heightUnits = ShareFileUtils.getInt(ShareFileUtils.HEIGHT_UNITS, 0); // 身高单位(0=cm, 1=Inches)
        int weightUnits = ShareFileUtils.getInt(ShareFileUtils.WEIGHT_UNITS, 0); // 体重单位(0=kg, 1=Lbs)
        try {
            String h;
            String w;
            if (heightUnits == 0) {
                h = Float.valueOf(height).intValue() + " cm";
            } else {
                h = Utils.cmToInches(height) + " Inches";
            }
            if (weightUnits == 0) {
                w = Float.valueOf(weight).intValue() + " kg";
            } else {
                w = Utils.kgToLbs(weight) + " Lbs";
            }
            return h + "/" + w;
        } catch (Exception e) {
        }
        return "";
    }

    //感情状态
    private String getRelationShip(UserInfoBean userinfoBean) {
        try {

            int index = Integer.parseInt(userinfoBean.affection);
            RoleBean relationshipBean = relationshipMap.get(index);
            String affection = relationshipBean.contentRes;

            return affection;

        } catch (Exception e) {
            return "";

        }
    }

    // 数据
    private ArrayList<String> constellation_list = new ArrayList<String>(); // 星座
    private ArrayList<String> relationship_list = new ArrayList<String>(); // 感情状态

    private void initLocalData() {
        String[] constellation_Array = context.getResources().getStringArray(R.array.filter_constellation);
        String[] relationship_Array = context.getResources().getStringArray(R.array.userinfo_relationship);
        constellation_list = new ArrayList<String>(Arrays.asList(constellation_Array));
        relationship_list = new ArrayList<String>(Arrays.asList(relationship_Array));
    }

    /**
     * 刷新关注按钮
     *
     * @param status 关注状态
     */
    public void refreshFollowBtn(int status) {

        if (status == 0) {
            lin_follow.setBackgroundResource(R.drawable.selector_edit_info_bg);
            img_follow.setImageResource(R.mipmap.btn_userinfo_follow_status0);
            lin_follow.setAlpha(1f);
            if (userInfoBean.secretly > 0) {//如果已经悄悄关注，后加显示已悄悄关注
                lin_follow.setBackgroundResource(R.drawable.shape_user_info_follow_bg);
                txt_follow.setText(TheLApp.getContext().getString(R.string.secretly_followed));
                lin_follow.setAlpha(0.6f);
            } else {
                txt_follow.setText(TheLApp.getContext().getString(R.string.userinfo_activity_follow));
            }
        } else if (status == 1) {

            lin_follow.setBackgroundResource(R.drawable.shape_user_info_follow_bg);
            txt_follow.setText(TheLApp.getContext().getString(R.string.userinfo_activity_followed));
            img_follow.setImageResource(R.mipmap.btn_userinfo_follow_status1);
            lin_follow.setAlpha(0.6f);
        } else if (status == 2) {

            lin_follow.setBackgroundResource(R.drawable.selector_edit_info_bg);
            img_follow.setImageResource(R.mipmap.btn_userinfo_follow_status2);
            lin_follow.setAlpha(1f);
            if (userInfoBean.secretly > 0) {//如果已经悄悄关注，后加显示已悄悄关注
                lin_follow.setBackgroundResource(R.drawable.shape_user_info_follow_bg);
                txt_follow.setText(TheLApp.getContext().getString(R.string.secretly_followed));
                lin_follow.setAlpha(0.6f);
            } else {
                txt_follow.setText(TheLApp.getContext().getString(R.string.repowder));
            }
        } else if (status == 3) {

            lin_follow.setBackgroundResource(R.drawable.shape_user_info_follow_bg);

            txt_follow.setText(TheLApp.getContext().getString(R.string.interrelated));
            img_follow.setImageResource(R.mipmap.btn_userinfo_follow_status3);
            lin_follow.setAlpha(0.6f);
        }
    }

    /**
     * 设置关注按钮可点击
     *
     * @param enable
     */
    public void setFollowEnable(boolean enable) {
        lin_follow.setEnabled(enable);
    }

    public void setInfoViewAlpha(float alp) {
        lin_left_infos.setAlpha(alp);
    }


    public void isShowFollowBtn(int followStatus, String userId) {
        if (!userId.equals(UserUtils.getMyUserId()) && (followStatus == 0 || followStatus == 2)) {
            lin_follow.setVisibility(VISIBLE);
        } else {
            lin_follow.setVisibility(GONE);
        }
    }

    public String getValue(ValueName valueName) {
        String value = "";
        switch (valueName) {
            case TAGE:
                value = tAge;
                break;
            case TAFFECTION:
                value = tAffection;
                break;
            case THOROSCOPE:
                value = tHoroscope;
                break;
        }
        return value;
    }

    private void setListener() {
        lin_follow.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                follow(userId);
            }
        });
        lin_follow.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        v.setAlpha(0.3f);
                        break;
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL:
                        if ((TheLApp.getContext().getString(R.string.userinfo_activity_followed).equals(txt_follow.getText()) || (TheLApp.getContext().getString(R.string.userinfo_activity_followed).equals(txt_follow.getText())))) {
                            v.setAlpha(0.5f);

                        } else {
                            v.setAlpha(1f);

                        }
                        // refreshFollowBtn(followStatus);

                        break;
                }
                return false;
            }
        });
        img_avatar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                showAvatar();
            }
        });
        img_avatar_left.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                showAvatar();
            }
        });
        img_avatar_right.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoPartnerUserInfoActivity();
            }
        });
        me_txt_num_fans.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                gotoUserInfoFollowerActivity(PAGE_TYPE_FANS);
            }
        });
     /*   me_txt_num_follow.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                gotoUserInfoFollowerActivity(PAGE_TYPE_FOLLOW);

            }
        });*/
        me_txt_num_wink.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                gotoUserInfoWinkActivity();
            }
        });
        me_background.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userInfoBean != null) {
                    if (!Utils.isMyself(userInfoBean.id + "")) {
                        DialogUtil.getInstance().showSelectionDialog((Activity) getContext(), new String[]{getString(R.string.report_cover), getString(R.string.info_no)}, new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                DialogUtil.getInstance().closeDialog();
                                switch (position) {
                                    case 0:// 举报
                                        showConfirmReportBgDialog(userInfoBean.id, "bgimg", 0);
                                        //   presenter.postReportImageOrUser(userInfoBean.id, "bgimg", 0);
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }, true, 1, null);
                    } else {
                        new ActionSheetDialog(getContext()).builder().setCancelable(true).setCanceledOnTouchOutside(true)
                                .addSheetItem(getString(R.string.change_bg), ActionSheetDialog.SheetItemColor.BLACK, new ActionSheetDialog.OnSheetItemClickListener() {
                                    @Override
                                    public void onClick(int which) {
                                        ((Activity) getContext()).startActivityForResult(new Intent(getContext(), SelectLocalImagesActivity.class), TheLConstants.BUNDLE_CODE_SELECT_BG);
                                    }
                                })
                                .show();
                    }
                }
            }
        });

    }

    private void showConfirmReportBgDialog(String userId, String bgimg, int type) {
        DialogUtil.showConfirmDialog(context, "", getString(R.string.ensure_roport_user_bg), getString(R.string.info_ok), getString(R.string.info_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                presenter.postReportImageOrUser(userInfoBean.id, "bgimg", 0);
                dialog.dismiss();
            }
        });
    }

    /**
     * 跳转到粉丝或者关注详情
     */
    private void gotoUserInfoFollowerActivity(String pageType) {
        Intent intent2 = new Intent(context, UserInfoFollowersActivity.class);
        intent2.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, userId);
        intent2.putExtra("pageType", pageType);
        context.startActivity(intent2);
    }

    /**
     * 跳转到挤眼详情
     */
    private void gotoUserInfoWinkActivity() {
        Intent intent1 = new Intent(context, UserInfoWinksActivity.class);
        intent1.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, userId);
        context.startActivity(intent1);
    }

    /**
     * 跳转到个人主页
     */
    private void gotoPartnerUserInfoActivity() {
        if (userInfoBean.paternerd != null) {
//            final Intent intent = new Intent(context, UserInfoActivity.class);
//            final Bundle bundle = new Bundle();
//            bundle.putString(TheLConstants.BUNDLE_KEY_USER_ID, userInfoBean.paternerd.paternerId + "");
//            intent.putExtras(bundle);
//            context.startActivity(intent);
            FlutterRouterConfig.Companion.gotoUserInfo(userInfoBean.paternerd.paternerId+"");
        }

    }

    /**
     * 显示个人头像大图
     */
    private void showAvatar() {
        if (userInfoBean != null) {
            UserInfoUtils.showVerifyInfoDialog(context, userInfoBean, presenter);
        }
    }

    private void follow(final String userId) {
        if (TextUtils.isEmpty(userId)) {
            return;
        }
        if (userInfoBean == null) {
            return;
        }
        String actionType = "0";
        if ((followStatus == 0 || followStatus == 2) && userInfoBean.secretly > 0) {//如果是悄悄关注
            UserInfoUtils.secretlyFollow(context, false, userId, presenter);//取消悄悄关注
        } else if (followStatus == 1 || followStatus == 3) {
            actionType = FollowStatusChangedListener.ACTION_TYPE_CANCEL_FOLLOW;// 取消关注
            final String finalActionType = actionType;
            DialogUtil.showConfirmDialog(context, null, getString(R.string.userinfo_activity_unfollow_confirm), getString(R.string.userinfo_activity_unfollow_operation), getString(R.string.info_no), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    arg0.dismiss();
                    presenter.showLoading();
                    //    requestBussiness.followUser(new DefaultNetworkHelper(UserInfoActivity.this), userId, actionType);
                    FollowStatusChangedImpl.followUserWithNoDialog(userId, finalActionType, userInfoBean.nickName, userInfoBean.avatar);
                    //todo
                }
            });
        } else {
            actionType = FollowStatusChangedListener.ACTION_TYPE_FOLLOW;
            presenter.showLoading();
            //todo
            //   requestBussiness.followUser(new DefaultNetworkHelper(UserInfoActivity.this), userId, actionType);
            FollowStatusChangedImpl.followUserWithNoDialog(userId, actionType, userInfoBean.nickName, userInfoBean.avatar);

        }
    }

    public void setLinProfileClickListener(OnClickListener listener) {
        lin_profile.setOnClickListener(listener);
    }

    public void setReviseInfoListener(View.OnClickListener listener) {
        lin_revise_info.setOnClickListener(listener);
        lin_revise_info.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        v.setAlpha(0.5f);
                        break;
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL:
                        v.setAlpha(1);
                        // refreshFollowBtn(followStatus);

                        break;
                }

                return false;
            }
        });
    }

    /**
     * 关注状态改变
     *
     * @param status
     * @param userId
     * @param nickName
     * @param avatar
     */
    public void followStatusChanged(int status, String userId, String nickName, String avatar) {
       /* if (isUserAbandoned) {
            closeLoading();
            finish();
            return;
        }*/
        if (TextUtils.isEmpty(this.userId)) {
            return;
        }
        if (!this.userId.equals(userId)) {//不是本人，就取消
            return;
        }
        if (userInfoBean == null) {
            return;
        }
        // 改变是否关注的状态
        if (status == FollowStatusChangedListener.FOLLOW_STATUS_FOLLOW) {
            if (followStatus == 0) {
                followStatus = 1;
            } else if (followStatus == 2) {
                followStatus = 3;
            }
        } else if (status == FollowStatusChangedListener.FOLLOW_STATUS_NO) {
            if (followStatus == 1) {
                followStatus = 0;
            } else if (followStatus == 3) {
                followStatus = 2;
            }
        }
        if (status == 0) {
            fansNum = fansNum - 1;
        } else {
            fansNum = fansNum + 1;
        }
        userInfoBean.followStatus = followStatus;
        refreshFollowBtn(followStatus);
        setFansNum();
        if (userInfoBean.secretly > 0) {//如果已经悄悄关注，后加显示已悄悄关注
            lin_follow.setBackgroundResource(R.drawable.shape_user_info_follow_bg);
            img_follow.setImageResource(R.mipmap.btn_userinfo_follow_status1);
            txt_follow.setText(TheLApp.getContext().getString(R.string.secretly_followed));
            lin_follow.setAlpha(0.6f);
        }
    }

    public void setMeBackgroundClickListener(OnClickListener listener) {
        me_background.setOnClickListener(listener);
    }

    public int refreshFollowValue() {
        return followStatus;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == TheLConstants.RESULT_CODE_TAKE_PHOTO) {
            String path = data.getStringExtra(TheLConstants.BUNDLE_KEY_IMAGE_OUTPUT_PATH);
            // 背景
            if (!TextUtils.isEmpty(path)) {
                imageLocalPath = ImageUtils.handlePhoto1(path, TheLConstants.F_TAKE_PHOTO_ROOTPATH, path.replace(TheLConstants.F_TAKE_PHOTO_ROOTPATH, ""), false, TheLConstants.MAX_PIC_WIDTH, TheLConstants.MAX_PIC_HEIGHT, TheLConstants.PIC_QUALITY, 3);
                uploadImage();
            } else {
                DialogUtil.showToastShort(context, TheLApp.getContext().getString(R.string.info_rechoise_photo));

            }
        } else if (resultCode == TheLConstants.RESULT_CODE_SELECT_LOCAL_IMAGE) {
            String path = data.getStringExtra(TheLConstants.BUNDLE_KEY_LOCAL_IMAGE_PATH);
            if (!TextUtils.isEmpty(path)) {
                {// 选择背景
                    imageLocalPath = ImageUtils.handlePhoto1(path, TheLConstants.F_TAKE_PHOTO_ROOTPATH, ImageUtils.getPicName(), false, TheLConstants.MAX_PIC_WIDTH, TheLConstants.MAX_PIC_HEIGHT, TheLConstants.PIC_QUALITY, 3);
                    uploadImage();
                }
            } else {
                DialogUtil.showToastShort(context, TheLApp.getContext().getString(R.string.info_rechoise_photo));
            }
        }
    }

    private void uploadImage() {
        try {
            if (TextUtils.isEmpty(imageLocalPath)) {
                return;
            }
            String uploadBgPath = RequestConstants.UPLOAD_FILE_ROOT_PATH_BG_IMG + ShareFileUtils.getString(ShareFileUtils.ID, "") + "/" + MD5Utils.calculateMD5(new File(imageLocalPath)) + ".jpg";
            PicUploadUtils.uploadPic(imageLocalPath, uploadBgPath, new PicUploadContract.PicUploadCallback() {
                @Override
                public void uploadFailed(int index) {
                }

                @Override
                public void uploadComplete(String imageUrls) {
                    L.i("getUpLoadImage", "imageUrls=" + imageUrls + "");
                    uploadBgImage(imageUrls);

                }

                @Override
                public void uploadVideoComplete(String videoUrl, String videoWebp) {

                }
            }, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void uploadBgImage(final String bgimage) {
        Flowable<BaseDataBean> flowable = RequestBusiness.getInstance().uploadBgImage(bgimage);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
            @Override
            public void onNext(BaseDataBean baseDataBean) {
                super.onNext(baseDataBean);

                if (getContext() != null && me_background != null) {

                    try {

                        ImageLoaderManager.imageLoaderDefault(me_background, R.color.tab_normal, bgimage);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onError(Throwable t) {
                L.d(" onError : " + t.getMessage());
            }

            @Override
            public void onComplete() {
            }
        });
    }
}
