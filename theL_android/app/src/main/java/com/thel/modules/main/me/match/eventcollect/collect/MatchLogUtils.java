package com.thel.modules.main.me.match.eventcollect.collect;

import com.thel.modules.main.me.bean.LogInfoBean;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.service.DefaultRequestService;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;
import com.thel.utils.MD5Utils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.UserUtils;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.FormBody;

public class MatchLogUtils {

    private List<LogInfoBean> logs = new ArrayList<>();
    private LogInfoBean logInfoBean = new LogInfoBean();
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

    private MatchLogUtils() {

    }

    private static MatchLogUtils instance = new MatchLogUtils();

    public static MatchLogUtils getInstance() {
        return instance;
    }

    public void addLog(LogInfoBean log) {

        logs.add(log);
        if (logs.size() > 0) {
            send();
            getLog();
            logs.clear();
        }


    }

    private void send() {

        String log = GsonUtils.createJsonString(logs);
        send(log);
    }

    public void send(String json) {
        L.d("MatchLogUtils", json);
        final FormBody formBody = new FormBody.Builder().add("logs", json).build();

        Flowable<String> flowable = DefaultRequestService.
                createTestReportRequestService().
                postMatchLogs(formBody);

        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<>());
    }

    public void send(Map<String, String> tracingMap) {

        try {
            HashMap<String, String> m = new HashMap<>(tracingMap);

            // 注入通用信息
            String latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
            String longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");
            String ua = MD5Utils.getUserAgent();

            m.put("lat", latitude);
            m.put("lng", longitude);
            m.put("ua", ua);
            m.put("time", dateFormat.format(new Date()));
            m.put("user_id", UserUtils.getMyUserId());

            String log = GsonUtils.createJsonString(m);
            send(log);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public LogInfoBean getLog() {
        if (logs.size() > 0) {
            logInfoBean = logs.get(logs.size() - 1);

        }
        return logInfoBean;
    }

}
