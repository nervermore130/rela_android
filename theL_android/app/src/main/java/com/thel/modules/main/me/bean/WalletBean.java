package com.thel.modules.main.me.bean;

import com.thel.base.BaseDataBean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by the L on 2016/5/24.
 * 我的钱包
 */
public class WalletBean extends BaseDataBean implements Serializable {
    /**
     * data : {"gold":0,"gem":0,"gemIncoming":0,"gemIncomingToday":0,"topFans":[],"topFansLink":"https://dev.rela.me/act/anchorlist/user.html","withdrawLink":"https://dev.rela.me/withdraw/tutorial.html","withdrawListLink":"https://dev.rela.me/act/withdraw/list.html","exchangeLink":"https://dev.rela.me/act/payproject/coin.html"}
     */

    public WalletDataBean data;

    public static class WalletDataBean {
        @Override
        public String toString() {
            return "WalletDataBean{" + "gold=" + gold + ", gem=" + gem + ", gemIncoming=" + gemIncoming + ", gemIncomingToday=" + gemIncomingToday + ", topFansLink='" + topFansLink + '\'' + ", withdrawLink='" + withdrawLink + '\'' + ", withdrawListLink='" + withdrawListLink + '\'' + ", exchangeLink='" + exchangeLink + '\'' + ", topFans=" + topFans + '}';
        }

        /**
         * gold : 0
         * gem : 0
         * gemIncoming : 0
         * gemIncomingToday : 0
         * topFans : []
         * topFansLink : https://dev.rela.me/act/anchorlist/user.html
         * withdrawLink : https://dev.rela.me/withdraw/tutorial.html
         * withdrawListLink : https://dev.rela.me/act/withdraw/list.html
         * exchangeLink : https://dev.rela.me/act/payproject/coin.html
         */

        public double gold;
        public double gem;
        public double gemIncoming;
        public double gemIncomingToday;
        public double gemIncomingMonth; //月直播收益
        public double monthLiveTime; //当月直播时长
        public String topFansLink;
        public String withdrawLink;
        public String withdrawListLink;
        public String exchangeLink;
        public List<TopFansUserBean> topFans;
        public String announcement; //公告
        public int gemIncomingTodayRank = -1;// 主播今日收入排行
    }

    /* *//**
     * 用户总的软妹币
     *//*
    public double gold;
    *//**
     * 总收益
     *//*
    public double gem;
    *//**
     * 今日收益
     *//*
    public double gemIncomingToday;
    *//**
     * 提现链接
     *//*
    public String withdrawLink;
    *//**
     * 提现记录链接
     *//*
    public String withdrawListLink;
    *//**
     * 粉丝贡献榜前三名头像
     *//*
    public List<SimpleUserBean> topFans = new ArrayList<>();
    *//**
     * 粉丝贡献榜前三名链接
     *//*
    public String topFansLink;
    *//**
     * 兑换软妹豆链接
     *//*
    public String exchangeLink;
*/

}
