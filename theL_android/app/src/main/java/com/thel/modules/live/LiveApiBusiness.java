package com.thel.modules.live;

import android.net.Uri;

import com.thel.base.BaseDataBean;
import com.thel.bean.LiveInfoLogBean;
import com.thel.network.api.LiveApi;
import com.thel.network.service.DefaultRequestService;
import com.thel.utils.BusinessUtils;
import com.thel.utils.L;
import com.thel.utils.MD5Utils;
import com.thel.utils.UrlConstants;
import com.thel.utils.Utils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import io.reactivex.Flowable;

/**
 * Created by waiarl on 2017/12/21.
 */

public class LiveApiBusiness {
    /**
     * 上传错误日志
     *
     * @param log
     * @return
     */
    public static Flowable<BaseDataBean> uploadPkErrorLog(String log) {
        final Map<String, String> data = new HashMap<>();
        data.put("log", log + "");
        data.put("userId", Utils.getMyUserId() + "");
        return DefaultRequestService.createUrlRetrofit(UrlConstants.UPLOAD_LOG_URL).create(LiveApi.class).uploadPkErrorLog(MD5Utils.generateSignatureForMap(data));
    }

}
