package com.thel.modules.live.livepkfriend;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

import androidx.core.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ImageSpan;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.constants.TheLConstants;
import com.thel.modules.live.LiveConstant;
import com.thel.modules.live.bean.LivePkFriendBean;
import com.thel.modules.live.bean.LivePkRefuseBean;
import com.thel.modules.live.in.LivePkContract;
import com.thel.modules.live.surface.show.LiveShowBaseViewFragment;
import com.thel.modules.live.utils.LinkMicOrPKRefuseUtils;
import com.thel.modules.live.utils.LiveUtils;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;
import com.thel.utils.L;
import com.thel.utils.ToastUtils;
import com.thel.utils.Utils;

import java.util.List;

/**
 * Created by waiarl on 2017/11/29.
 * 连麦或PK
 */

public class LiveLinkMicOrPKListAdapter extends BaseRecyclerViewAdapter<LivePkFriendBean> {

    private static final String TAG = "LiveLinkMicOrPKListAdapter";

    private final int levelIconSize;
    private int vipIconSize;

    private int type;

    private int indicatorIndex = -1;//主动连麦和日榜分割线position,不显示则为－1

    private int linkMicStatus = LiveShowBaseViewFragment.LINK_MIC_STATUS_NONE;

    private LivePkContract.FriendRequestClickListener friendRequestClickListener;

    public LiveLinkMicOrPKListAdapter(List<LivePkFriendBean> data, int type) {
        super(R.layout.adapter_live_pk_friendlist_item, data);
        this.type = type;

        vipIconSize = Utils.dip2px(TheLApp.getContext(), 17);
        levelIconSize = (int) TheLApp.getContext().getResources().getDimension(R.dimen.live_chat_level_icon_height);
    }

    public void setIndicatorIndex(int index) {
        this.indicatorIndex = index;
    }

    public void setLinkMicStatus(int linkMicStatus) {
        this.linkMicStatus = linkMicStatus;
        notifyDataSetChanged();
    }

    @SuppressLint("StringFormatMatches")
    @Override
    protected void convert(BaseViewHolder helper, LivePkFriendBean item) {

        setListener(helper, item);

        helper.setVisibility(R.id.link_mic_icon, View.GONE);

        helper.setCircleImageViewUrl(R.id.img_avatar, R.mipmap.icon_user, item.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE);

        helper.setVisibility(R.id.indicator, helper.getAdapterPosition() == indicatorIndex ? View.VISIBLE : View.GONE);

        final TextView txt_status = helper.getView(R.id.txt_status);

        LinearLayout status_container = helper.getView(R.id.status_container);

        int statusColor = getColor(R.color.live_pk_friendlist_disable_color);//默认灰色颜色
        Drawable drawable = ContextCompat.getDrawable(TheLApp.getContext(), R.color.transparent);//默认透明背景
        if (linkMicStatus != LiveShowBaseViewFragment.LINK_MIC_STATUS_NONE) {
            statusColor = getColor(R.color.live_pk_friendlist_disable_color);
            drawable = mContext.getResources().getDrawable(R.drawable.bg_border_gray_liveroom_label_shape);
        }

        if (type == LiveConstant.TYPE_PK) {
            helper.setText(R.id.txt_nickname, item.nickName + "");
            helper.setVisibility(R.id.txt_content, View.GONE);

            switch (item.liveStatus) {

                case LivePkFriendBean.LIVE_STATUS_NO://无状态
                    txt_status.setText(TheLApp.getContext().getString(R.string.pk));

                    if (linkMicStatus == LiveShowBaseViewFragment.LINK_MIC_STATUS_NONE) {
                        if (!LinkMicOrPKRefuseUtils.isPKRefuse(item.id)) {//如果不是Pk被拒绝用户，
                            statusColor = getColor(R.color.live_pk_friendlist_enable_color);
                            drawable = mContext.getResources().getDrawable(R.drawable.bg_border_green_liveroom_label_shape);
                        } else {
                            statusColor = getColor(R.color.live_pk_friendlist_disable_color);
                            drawable = mContext.getResources().getDrawable(R.drawable.bg_border_gray_liveroom_label_shape);
                        }
                    }

                    break;
                case LivePkFriendBean.LIVE_STATUS_LINK://正在连麦
                    txt_status.setText(TheLApp.getContext().getString(R.string.connecting_the_microphone));
                    break;
                case LivePkFriendBean.LIVE_STATUS_PK://正在pk
                    txt_status.setText(TheLApp.getContext().getString(R.string.pk_now));
                    break;
            }
        } else if (type == LiveConstant.TYPE_AUDIENCE_LINK_MIC) {//观众连麦
            if (!TextUtils.isEmpty(item.nickName)) {
                helper.setText(R.id.txt_nickname, buildNickname(item.nickName, item.vip, item.userLevel, item.iconSwitch));
            } else {
                helper.setText(R.id.txt_nickname, buildNickname(item.nickname, item.vip, item.userLevel, item.iconSwitch));
            }
            helper.setVisibility(R.id.txt_content, View.VISIBLE);
            if (item.isGuard) {
                helper.setText(R.id.txt_content, String.format(mContext.getString(R.string.gifted_credits), item.gold));

                txt_status.setText(TheLApp.getContext().getString(R.string.connect_mic));
                if (linkMicStatus == LiveShowBaseViewFragment.LINK_MIC_STATUS_NONE) {
                    statusColor = getColor(R.color.live_pk_friendlist_enable_color);
                    drawable = mContext.getResources().getDrawable(R.drawable.bg_border_green_liveroom_label_shape);
                }
            } else {
                helper.setText(R.id.txt_content, mContext.getString(R.string.application_mic));
                helper.setVisibility(R.id.link_mic_icon, View.VISIBLE);
                txt_status.setText(TheLApp.getContext().getString(R.string.accept));

                if (linkMicStatus == LiveShowBaseViewFragment.LINK_MIC_STATUS_NONE) {
                    statusColor = getColor(R.color.white);
                    drawable = mContext.getResources().getDrawable(R.drawable.bg_border_green_liveroom_label_shape2);
                } else {
                    statusColor = getColor(R.color.white);
                    drawable = mContext.getResources().getDrawable(R.drawable.bg_border_gray_liveroom_label_shape2);
                }
            }

        } else if (type == LiveConstant.TYPE_FRIEND_LINK_MIC) {//好友连麦
            if (!TextUtils.isEmpty(item.nickName)) {
                helper.setText(R.id.txt_nickname, buildNickname(item.nickName, item.vip, item.userLevel, item.iconSwitch));
            } else {
                helper.setText(R.id.txt_nickname, buildNickname(item.nickname, item.vip, item.userLevel, item.iconSwitch));
            }
            helper.setVisibility(R.id.txt_content, View.GONE);

            switch (item.liveStatus) {

                case LivePkFriendBean.LIVE_STATUS_NO://无状态
                    txt_status.setText(TheLApp.getContext().getString(R.string.connect_mic));

                    if (linkMicStatus == LiveShowBaseViewFragment.LINK_MIC_STATUS_NONE) {
                        statusColor = getColor(R.color.live_pk_friendlist_enable_color);
                        drawable = mContext.getResources().getDrawable(R.drawable.bg_border_green_liveroom_label_shape);
                    }
                    break;
                case LivePkFriendBean.LIVE_STATUS_LINK://正在连麦
                    txt_status.setText(TheLApp.getContext().getString(R.string.connecting_the_microphone));
                    break;
                case LivePkFriendBean.LIVE_STATUS_PK://正在pk
                    txt_status.setText(TheLApp.getContext().getString(R.string.pk_now));
                    break;
            }
        }

        status_container.setBackground(drawable);
        txt_status.setTextColor(statusColor);
    }

    private int getColor(int colorId) {
        return ContextCompat.getColor(TheLApp.getContext(), colorId);
    }

    private void setListener(BaseViewHolder helper, final LivePkFriendBean item) {

        final int position = helper.getLayoutPosition() - getHeaderLayoutCount();

        final TextView txt_status = helper.getView(R.id.txt_status);

        if (item.liveStatus == LivePkFriendBean.LIVE_STATUS_NO) {

            txt_status.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (linkMicStatus != LiveShowBaseViewFragment.LINK_MIC_STATUS_NONE) {//主播正在连麦中
                        ToastUtils.showCenterToastShort(mContext, mContext.getString(R.string.start_a_new_one));
                        return;
                    }

                    if (type == LiveConstant.TYPE_PK) {
                        if (LinkMicOrPKRefuseUtils.isPKRefuse(item.id)) {

                            LivePkRefuseBean livePkRefuseBean = LinkMicOrPKRefuseUtils.getPkRefuseInfo(item.id);

                            showRefuseTime(livePkRefuseBean);

                            return;

                        }

                    } else if (type == LiveConstant.TYPE_FRIEND_LINK_MIC) {
                        if (LinkMicOrPKRefuseUtils.isLinkMicRefuse(item.id)) {

                            LivePkRefuseBean livePkRefuseBean = LinkMicOrPKRefuseUtils.getLinkMicRefuseInfo(item.id);

                            showRefuseTime(livePkRefuseBean);

                            return;
                        }

                    }

                    if (friendRequestClickListener != null) {

                        if (item.isGuard) {
                            item.id = item.userId;
                            friendRequestClickListener.onClickRequest(item, LiveConstant.TYPE_GUARD_AUDIENCE_LINK_MIC, position);
                        } else {
                            friendRequestClickListener.onClickRequest(item, type, position);
                        }

                    }
                }
            });
        }
    }

    /**
     * 点击连麦或者Pk
     *
     * @param listener
     */
    public void setOnFriendRequestClickListener(LivePkContract.FriendRequestClickListener listener) {
        this.friendRequestClickListener = listener;
    }

    private void showRefuseTime(LivePkRefuseBean livePkRefuseBean) {

        L.d(TAG, " livePkRefuseBean : " + livePkRefuseBean);

        if (livePkRefuseBean != null) {

            long deltaTime = 10 * 60 * 1000 - (System.currentTimeMillis() - livePkRefuseBean.refuseTime);

            L.d(TAG, " deltaTime : " + deltaTime);

            String content = TheLApp.context.getResources().getString(R.string.invite_after_xx, Utils.formatTime(deltaTime / 1000));

            ToastUtils.showToastShort(TheLApp.context, content);

        }
    }

    /**
     * 显示内容 SP
     *
     * @return
     */
    private SpannableString buildNickname(String nickName, int vip, int userLevel, int iconSwitch) {
        SpannableString sp = null;
        String userLevelStr1;
        String userLevelStr2 = "";
        String vipStr1;
        String vipStr2 = "";
        int start1 = 0, end1 = 0, start2 = 0, end2 = 0;

        if (userLevel >= 0 && iconSwitch == 1) {
            userLevelStr1 = userLevel + "";
            userLevelStr2 = userLevelStr1 + " ";
            end1 = start1 + userLevelStr1.length();
        }

        if (vip > 0) {
            vipStr1 = vip + "";
            vipStr2 = vipStr1 + " ";
            start2 = start1 + iconSwitch == 1 ? userLevelStr2.length() : 0;
            end2 = start2 + vipStr1.length();
        }

        sp = new SpannableString((iconSwitch == 1 ? userLevelStr2 : "") + vipStr2 + nickName);

        //会员
        final Bitmap vipBitmap = getVipBitmap(vip);
        final MyIm myVipLevelIm = new MyIm(TheLApp.getContext(), vipBitmap);//为调整文字与会员图片保持居中
        sp.setSpan(myVipLevelIm, start2, end2, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);

        if (iconSwitch == 1) {
            //用户等级
            final Bitmap userLevelBitmap = getUserLevelBitmap(userLevel);
            final MyIm myUserLevelIm = new MyIm(TheLApp.getContext(), userLevelBitmap);
            sp.setSpan(myUserLevelIm, start1, end1, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        }

        return sp;
    }

    /**
     * 获取用户等级bitmap
     *
     * @param userLevel
     * @return
     */
    private Bitmap getUserLevelBitmap(int userLevel) {
        Bitmap bitmap;
        if (userLevel >= TheLConstants.USER_LEVEL_RES.length) {
            userLevel = TheLConstants.USER_LEVEL_RES.length - 1;
        }
        if (userLevel < TheLConstants.USER_LEVEL_RES.length && userLevel >= 0) {
            bitmap = BitmapFactory.decodeResource(TheLApp.getContext().getResources(), TheLConstants.USER_LEVEL_RES[userLevel]);
        } else {
            bitmap = BitmapFactory.decodeResource(TheLApp.getContext().getResources(), TheLConstants.USER_LEVEL_RES[0]);

        }
        final int height = levelIconSize;
        final float width = LiveUtils.getLevelImageWidth(height, userLevel);

        if (bitmap != null) {
            bitmap = Bitmap.createScaledBitmap(bitmap, (int) width, height, true);
        }
        return bitmap;
    }

    /**
     * 根据vip等级获取相应的vip
     *
     * @param vip
     * @return
     */
    private Bitmap getVipBitmap(int vip) {
        Bitmap bitmap;
        if (vip >= TheLConstants.VIP_LEVEL_RES.length) {
            vip = TheLConstants.VIP_LEVEL_RES.length - 1;
        }
        if (vip < TheLConstants.VIP_LEVEL_RES.length && vip > 0) {
            bitmap = BitmapFactory.decodeResource(TheLApp.getContext().getResources(), TheLConstants.VIP_LEVEL_RES[vip]);
        } else {
            bitmap = BitmapFactory.decodeResource(TheLApp.getContext().getResources(), TheLConstants.VIP_LEVEL_RES[1]);
        }
        if (bitmap != null) {
            bitmap = Bitmap.createScaledBitmap(bitmap, vipIconSize, vipIconSize, true);
        }
        return bitmap;
    }

    private Bitmap getMicBitmap() {
        Bitmap bitmap;
        bitmap = BitmapFactory.decodeResource(TheLApp.getContext().getResources(), R.mipmap.btn_link_mic);
        int height = Utils.dip2px(TheLApp.getContext(), 18);
        if (bitmap != null) {
            bitmap = Bitmap.createScaledBitmap(bitmap, (int) (height * 1.2f), height, true);
        }
        return bitmap;
    }

    public class MyIm extends ImageSpan {

        public MyIm(Context arg0, Bitmap arg1) {
            super(arg0, arg1);
        }

        public int getSize(Paint paint, CharSequence text, int start, int end, Paint.FontMetricsInt fm) {
            Drawable d = getDrawable();
            Rect rect = d.getBounds();
            if (fm != null) {
                Paint.FontMetricsInt fmPaint = paint.getFontMetricsInt();
                int fontHeight = fmPaint.bottom - fmPaint.top;
                int drHeight = rect.bottom - rect.top;

                int top = drHeight / 2 - fontHeight / 4;
                int bottom = drHeight / 2 + fontHeight / 4;

                fm.ascent = -bottom;
                fm.top = -bottom;
                fm.bottom = top;
                fm.descent = top;
            }
            return rect.right;
        }

        @Override
        public void draw(Canvas canvas, CharSequence text, int start, int end, float x, int top, int y, int bottom, Paint paint) {
            Drawable b = getDrawable();
            canvas.save();
            int transY = 0;
            transY = ((bottom - top) - b.getBounds().bottom) / 2 + top;
            canvas.translate(x, transY);
            b.draw(canvas);
            canvas.restore();
        }
    }
}
