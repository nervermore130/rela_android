package com.thel.modules.main.home.moments.web;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.base.BaseFragment;
import com.thel.bean.ResultBean;
import com.thel.bean.recommend.RecommendWebBean;
import com.thel.constants.TheLConstants;
import com.thel.constants.TheLConstantsExt;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.modules.live.surface.watch.LiveWatchActivity;
import com.thel.modules.main.home.moments.ReleaseMomentActivity;
import com.thel.modules.main.home.moments.ReportActivity;
import com.thel.modules.main.home.moments.SendCardActivity;
import com.thel.modules.main.home.moments.comment.MomentCommentActivity;
import com.thel.modules.main.home.moments.theme.ThemeDetailActivity;
import com.thel.modules.main.home.tag.TagDetailActivity;
import com.thel.modules.main.me.aboutMe.BuyVipActivity;
import com.thel.modules.main.me.aboutMe.StickerPackDetailActivity;
import com.thel.modules.main.me.aboutMe.StickerStoreActivity;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.modules.preview_video.VideoActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.ui.adapter.ShareGridAdapter;
import com.thel.utils.Base64;
import com.thel.utils.BusinessUtils;
import com.thel.utils.DeviceUtils;
import com.thel.utils.DialogUtil;
import com.thel.utils.ImageUtils;
import com.thel.utils.JsonUtils;
import com.thel.utils.L;
import com.thel.utils.MD5Utils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.UMShareUtils;
import com.thel.utils.UrlConstants;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.thel.constants.BundleConstants.NEED_SECURITY_CHECK;

/**
 * Created by liuyun on 2017/10/27.
 */

public class WebFragment extends BaseFragment {

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.webView)
    WebView webView;

    @BindView(R.id.rel_title)
    RelativeLayout rel_title;

    @BindView(R.id.txt_title)
    TextView txt_title;

    @BindView(R.id.rel_bottom)
    LinearLayout rel_bottom;

    @BindView(R.id.lin_back)
    LinearLayout lin_back;

    @BindView(R.id.lin_more)
    LinearLayout lin_more;

    @BindView(R.id.img_share)
    ImageView img_share;

    @BindView(R.id.bottom_back)
    ImageView bottom_back;

    @BindView(R.id.bottom_next)
    ImageView bottom_next;

    @BindView(R.id.bottom_refresh)
    ImageView bottom_refresh;

    @BindView(R.id.bottom_cancel)
    View bottom_cancel;

    @BindView(R.id.bottom_copy)
    ImageView bottom_copy;

    @BindView(R.id.gridView)
    GridView gridView;

    @BindView(R.id.bottom_title)
    TextView bottom_title;

    @BindView(R.id.rel_dialog)
    RelativeLayout rel_dialog;

    @BindView(R.id.img_recommend)
    View img_recommend;

    @BindView(R.id.bottom_shade)
    View bottom_shade;
    @BindView(R.id.lin_previous)
    LinearLayout lin_previous;

    private String adUrl;
    private String uid;
    private String site = "";
    private String dumpUrl;
    private String shareLogo;
    private String shareTitle;
    private String shareContent;

    private CallbackManager callbackManager;
    private UMShareListener umShareListener;


    private String security_check = "https://security.rela.me/safe_redirect?url=";

    // 1：关注 2：取消关注
    private String followOrUnfollow;

    private String getMyInfoCallback;
    private String followCallback;
    private String unfollowCallback;
    private String shareSucceedCallback = "relaClientShareSuccess";

    private static final int SHOW_DIALOG = 0;
    private static final int WRITE_VIDEO_MOMENT = 1;
    private static final int SHOW_IMAGE_SHARE_DIALOG = 2;

    public static final String SHARE_TYPE_WEB = "web";//分享网页
    public static final String SHARE_TYPE_IMAGE = "image";//分享图片

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */

    private void getDataFromPrepage() {

        Bundle bundle = getArguments();
        site = bundle.getString("site");
        uid = bundle.getString("uid");

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_web, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
        setListener();
    }

    private void init() {
        /** 分享初始化 **/
        callbackManager = CallbackManager.Factory.create();
        umShareListener = new UMShareListener() {

            @Override
            public void onStart(SHARE_MEDIA share_media) {

            }

            @Override
            public void onResult(SHARE_MEDIA platform) {
                shareResult(1);
            }

            @Override
            public void onError(SHARE_MEDIA platform, Throwable t) {
                shareResult(0);
            }

            @Override
            public void onCancel(SHARE_MEDIA platform) {
                shareResult(0);
            }
        };
        /** 分享初始化 **/

        setListener();
        getDataFromPrepage();

        registerReceiver();

        WebChromeClient webChromeClient = new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                if (newProgress == 100) {
                    progressBar.setVisibility(View.GONE);
                    if ("ad".equals(site) || "thelSite".equals(site))
                        img_share.setVisibility(View.VISIBLE);
                } else {
                    img_share.setVisibility(View.GONE);
                    if (View.GONE == progressBar.getVisibility()) {
                        progressBar.setVisibility(View.VISIBLE);
                    }
                    progressBar.setProgress(newProgress);
                }
                super.onProgressChanged(view, newProgress);
            }

            @Override
            public void onShowCustomView(View view, WebChromeClient.CustomViewCallback callback) {
                if (myCallback != null) {
                    myCallback.onCustomViewHidden();
                    myCallback = null;
                    return;
                }

                long id = Thread.currentThread().getId();

                ViewGroup parent = (ViewGroup) webView.getParent();
                String s = parent.getClass().getName();
                parent.removeView(rel_title);
                parent.removeView(webView);
                parent.removeView(rel_bottom);
                parent.addView(view);
                myView = view;
                myCallback = callback;
                //                chromeClient = this;
            }

            private View myView = null;
            private CustomViewCallback myCallback = null;


            public void onHideCustomView() {

                long id = Thread.currentThread().getId();

                if (myView != null) {

                    if (myCallback != null) {
                        myCallback.onCustomViewHidden();
                        myCallback = null;
                    }

                    ViewGroup parent = (ViewGroup) myView.getParent();
                    parent.removeView(myView);
                    if (null != rel_title.getParent())     // fabric  #10541
                        ((ViewGroup) rel_title.getParent()).removeView(rel_title);
                    if (null != webView.getParent())
                        ((ViewGroup) webView.getParent()).removeView(webView);
                    if (null != rel_bottom.getParent())
                        ((ViewGroup) rel_bottom.getParent()).removeView(rel_bottom);
                    parent.addView(rel_title);
                    parent.addView(webView);
                    parent.addView(rel_bottom);
                    myView = null;
                }
            }
        };

        webView.getSettings().setUserAgentString(webView.getSettings().getUserAgentString() + " theL/" + DeviceUtils.getVersionName(getContext()));
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.getSettings().setAppCacheEnabled(false);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setSavePassword(false);
        webView.addJavascriptInterface(new InJavaScriptLocalObj(), "local_obj");
        webView.addJavascriptInterface(new InJavaScriptRecomendWeb(), "getDefaultImage");
        webView.removeJavascriptInterface("searchBoxJavaBridge_");// 修复漏洞
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            WebView.setWebContentsDebuggingEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);

        webView.setWebChromeClient(webChromeClient);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                L.d("refresh", "should,url=" + url);
                if (TextUtils.isEmpty(url))
                    return true;
                if (url.contains("xiami://song/") || url.contains("www.xiami.com")) {
                    try {
                        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        startActivity(i);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else if (url.contains(UrlConstants.WebURLConstants.URL_GETINFO)) {// 获取我的信息
                    if (filterUrl(webView.getUrl())) {
                        try {
                            Uri uri = Uri.parse(url);
                            getMyInfoCallback = uri.getQueryParameter("callback");
                            webView.loadUrl("javascript:" + getMyInfoCallback + "('" + buildCallbackParam(ShareFileUtils.getString(ShareFileUtils.ID, ""), ShareFileUtils.getString(ShareFileUtils.USER_NAME, ""), ShareFileUtils.getString(ShareFileUtils.AVATAR, ""), BusinessUtils.generateJsKey(), DeviceUtils.getLanguage()) + "')");
                        } catch (Exception e) {
                        }
                    }
                } else if (url.contains(UrlConstants.WebURLConstants.URL_FOLLOW)) {// 关注某人
                    if (filterUrl(webView.getUrl())) {
                        try {
                            Uri uri = Uri.parse(url);
                            String userId = uri.getQueryParameter("userId");
                            String nickName = uri.getQueryParameter("nickName");
                            String avatar = uri.getQueryParameter("avatar");
                            followCallback = uri.getQueryParameter("callback");
                            if (!TextUtils.isEmpty(userId)) {
                                ((BaseActivity) getActivity()).showLoading();
                                followOrUnfollow = "1";
                                followUser(userId, followOrUnfollow);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else if (url.contains(UrlConstants.WebURLConstants.URL_UNFOLLOW)) {// 取消关注某人
                    if (filterUrl(webView.getUrl())) {
                        try {
                            Uri uri = Uri.parse(url);
                            unfollowCallback = uri.getQueryParameter("callback");
                            if (!TextUtils.isEmpty(uri.getQueryParameter("userId"))) {
                                ((BaseActivity) getActivity()).showLoading();
                                followOrUnfollow = "0";
                                followUser(uri.getQueryParameter("userId"), followOrUnfollow);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else if (url.contains(UrlConstants.WebURLConstants.URL_USER)) {// 跳转到用户详情页
                    if (filterUrl(webView.getUrl())) {
                        try {
                            Uri uri = Uri.parse(url);
                            if (!TextUtils.isEmpty(uri.getQueryParameter("userId"))) {
//                                Intent intent = new Intent();
//                                intent.setClass(getContext(), UserInfoActivity.class);
//                                intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, uri.getQueryParameter(TheLConstants.BUNDLE_KEY_USER_ID));
//                                startActivity(intent);
                                FlutterRouterConfig.Companion.gotoUserInfo(uri.getQueryParameter(TheLConstants.BUNDLE_KEY_USER_ID));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else if (url.contains(UrlConstants.WebURLConstants.URL_MOMENT)) {// 跳转到日志详情页
                    if (filterUrl(webView.getUrl())) {
                        try {
                            Uri uri = Uri.parse(url);
                            if (!TextUtils.isEmpty(uri.getQueryParameter("momentId"))) {
//                                Intent intent = new Intent();
//                                intent.setClass(getContext(), MomentCommentActivity.class);
//                                intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, uri.getQueryParameter("momentId"));
//                                startActivity(intent);
                                FlutterRouterConfig.Companion.gotoMomentDetails(uri.getQueryParameter("momentId"));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else if (url.contains(UrlConstants.WebURLConstants.URL_THEME)) {// 跳转到话题详情页
                    if (filterUrl(webView.getUrl())) {
                        try {
                            Uri uri = Uri.parse(url);
                            if (!TextUtils.isEmpty(uri.getQueryParameter("momentId"))) {
//                                Intent intent = new Intent();
//                                intent.setClass(getContext(), ThemeDetailActivity.class);
//                                intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, uri.getQueryParameter("momentId"));
//                                startActivity(intent);
                                FlutterRouterConfig.Companion.gotoThemeDetails(uri.getQueryParameter("momentId"));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else if (url.contains(UrlConstants.WebURLConstants.URL_RELEASE_THEME)) {
                    if (filterUrl(webView.getUrl())) {
                        try {
                            Uri uri = Uri.parse(url);
                            String tagName = uri.getQueryParameter("tagName");
                            Intent intent = new Intent();
                            intent.setClass(getContext(), ReleaseMomentActivity.class);
                            if (!TextUtils.isEmpty(tagName)) {
                                intent.putExtra(TheLConstants.BUNDLE_KEY_TOPIC_NAME, tagName);
                            }
                            startActivity(intent);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else if (url.contains(UrlConstants.WebURLConstants.URL_TAG)) {// 跳转到标签详情页
                    if (filterUrl(webView.getUrl())) {
                        try {
                            Uri uri = Uri.parse(url);
                            if (!TextUtils.isEmpty(uri.getQueryParameter("tagName"))) {
//                                Intent intent = new Intent(getContext(), TagDetailActivity.class);
//                                intent.putExtra(TheLConstants.BUNDLE_KEY_TOPIC_NAME, uri.getQueryParameter("tagName"));
//                                startActivity(intent);
                                FlutterRouterConfig.Companion.gotoTagDetails(-1,uri.getQueryParameter("tagName"));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else if (url.contains(UrlConstants.WebURLConstants.URL_STICKER_PACK)) {// 跳转到表情包详情页
                    if (filterUrl(webView.getUrl())) {
                        try {
                            Uri uri = Uri.parse(url);
                            if (!TextUtils.isEmpty(uri.getQueryParameter("emojiId"))) {
                                Intent intent = new Intent(getContext(), StickerPackDetailActivity.class);
                                intent.putExtra("id", Long.valueOf(uri.getQueryParameter("emojiId")));
                                startActivity(intent);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else if (url.contains(UrlConstants.WebURLConstants.URL_STICKER_STORE)) {// 跳转到表情商店
                    if (filterUrl(webView.getUrl())) {
                        Intent intent = new Intent(getContext(), StickerStoreActivity.class);
                        startActivity(intent);
                    }
                } else if (url.contains(UrlConstants.WebURLConstants.URL_VIDEO)) {// 播放视频
                    if (filterUrl(webView.getUrl())) {
                        try {
                            Uri uri = Uri.parse(url);
                            if (!TextUtils.isEmpty(uri.getQueryParameter("url"))) {
                                Intent intent = new Intent(getActivity(), VideoActivity.class);
                                intent.putExtra("url", uri.getQueryParameter("url"));
                                intent.putExtra("Referer", webView.getUrl());
                                startActivity(intent);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else if (url.contains(UrlConstants.WebURLConstants.URL_FEEDBACK)) {// 意见反馈
                    if (filterUrl(webView.getUrl())) {
                        MobclickAgent.onEvent(TheLApp.getContext(), "feedbacks_click");
                        Intent intent = new Intent(getActivity(), ReportActivity.class);
                        intent.putExtra(TheLConstants.BUNDLE_KEY_REPORT_TYPE, ReportActivity.REPORT_TYPE_BUG_FEEDBACK);
                        startActivity(intent);
                    }
                } else if (url.contains(UrlConstants.WebURLConstants.URL_REQUEST)) {// 调用服务端的接口，然后将返回结果返回给网页
                    if (filterUrl(webView.getUrl())) {
                        try {
                            Uri uri = Uri.parse(url);
                            final String callbackFunc = uri.getQueryParameter("callbackFunc");
                            String path = uri.getQueryParameter("path");
                            if (!path.startsWith("/"))
                                path = "/" + path;
                            String method = uri.getQueryParameter("method");
                            JSONObject params = new JSONObject(uri.getQueryParameter("params"));
                            Iterator<String> iterator = params.keys();
                            Map<String, String> data = MD5Utils.getDefaultHashMap();
                            data.put("isWeb", 1 + "");
                            while (iterator.hasNext()) {
                                String key = iterator.next();
                                data.put(key, params.getString(key));
                            }

                            L.d("WebFragment", " path : " + path);


                            //                            RequestCoreBean requestBean = RequestUtils.getRequestCoreBean(data, path, null);
                            //                            requestBean.parser = new DefaultParser();
                            //                            OneRequestNetworkHelper defaultNetworkHelper = new OneRequestNetworkHelper(new UIDataListener() {
                            //                                @Override
                            //                                public void onDataChanged(RequestCoreBean rcb) {
                            //                                    if (RequestConstants.RESPONSE_SUCCESS == rcb.responseValue && null != rcb.responseDataObj)
                            //                                        webView.loadUrl("javascript:" + callbackFunc + "('" + buildCommonCallbackParam(null, rcb.responseDataStr) + "')");
                            //                                    else
                            //                                        DialogUtil.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.info_wrong));
                            //                                }
                            //
                            //                                @Override
                            //                                public void onErrorHappened(VolleyError error, RequestCoreBean rcb) {
                            //                                    if (error.networkResponse != null) {
                            //                                        webView.loadUrl("javascript:" + callbackFunc + "('" + buildCommonCallbackParam(error, "") + "')");
                            //                                    } else {
                            //                                        if (error.getMessage() != null)
                            //                                            Log.e("httpError", error.getMessage());
                            //                                    }
                            //                                }
                            //                            });
                            //                            if ("get".equalsIgnoreCase(method)) {
                            //                                defaultNetworkHelper.sendGETRequest(requestBean);
                            //                            } else if ("post".equalsIgnoreCase(method)) {
                            //                                defaultNetworkHelper.sendPOSTRequest(requestBean);
                            //                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else if (url.contains(UrlConstants.WebURLConstants.URL_ALERT)) {// 网页调用弹窗
                    if (filterUrl(webView.getUrl())) {
                        try {
                            Uri uri = Uri.parse(url);
                            final String callbackFunc = uri.getQueryParameter("callbackFunc");
                            String content = uri.getQueryParameter("content");
                            String title = uri.getQueryParameter("title");
                            JSONArray params = new JSONArray(uri.getQueryParameter("arr"));
                            HashMap<String, DialogInterface.OnClickListener> buttons = new HashMap<>();
                            for (int i = 0; i < params.length(); i++) {
                                final int index = i;
                                buttons.put(params.getString(i), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        webView.loadUrl("javascript:" + callbackFunc + "('" + index + "')");
                                    }
                                });
                            }
                            DialogUtil.showMultiButtonAlert(getContext(), title, content, buttons);
                        } catch (Exception e) {
                        }
                    }
                } else if (url.contains(UrlConstants.WebURLConstants.URL_LIVE_ROOM)) {// 跳转直播房间
                    if (filterUrl(webView.getUrl())) {
                        try {
                            Uri uri = Uri.parse(url);
                            String liveId = uri.getQueryParameter("liveId");
                            String userId = uri.getQueryParameter("userId");
                            Intent intent = new Intent();
                            intent.setClass(getContext(), LiveWatchActivity.class);
                            if (!TextUtils.isEmpty(liveId)) {
                                intent.putExtra(TheLConstants.BUNDLE_KEY_ID, liveId);
                                startActivity(intent);
                            } else if (!TextUtils.isEmpty(userId)) {
                                intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, userId);
                                startActivity(intent);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else if (url.contains(UrlConstants.WebURLConstants.URL_SHARE)) {// 分享页面
                    if (filterUrl(webView.getUrl())) {
                        try {
                            Uri uri = Uri.parse(url);
                            String title = uri.getQueryParameter("title");
                            String desc = uri.getQueryParameter("desc");
                            String jumpUrl = uri.getQueryParameter("url");
                            String logo = uri.getQueryParameter("imgUrl");
                            String type = uri.getQueryParameter("type");
                            if (SHARE_TYPE_IMAGE.equals(type)) {//4.0.0 社会化分享
                                Message msg = Message.obtain();
                                msg.what = SHOW_IMAGE_SHARE_DIALOG;
                                msg.obj = new DialogBean(title, desc, jumpUrl, logo);
                                mHandler.sendMessage(msg);
                            } else {
                                Message msg = Message.obtain();
                                msg.what = SHOW_DIALOG;
                                msg.obj = new DialogBean(title, desc, jumpUrl, logo);
                                mHandler.sendMessage(msg);
                            }
                        } catch (Exception e) {
                        }
                    } else {
                        try {
                            Uri uri = Uri.parse(url);
                            String title = uri.getQueryParameter("title");
                            String desc = uri.getQueryParameter("desc");
                            String jumpUrl = uri.getQueryParameter("url");
                            String logo = uri.getQueryParameter("imgUrl");
                            String type = uri.getQueryParameter("type");
                            final DialogBean bean = new DialogBean(title, desc, jumpUrl, logo);
                            if (SHARE_TYPE_IMAGE.equals(type)) {//4.0.0 社会化分享
                                Message msg = Message.obtain();
                                msg.what = SHOW_IMAGE_SHARE_DIALOG;
                                msg.obj = new DialogBean(title, desc, jumpUrl, logo);
                                mHandler.sendMessage(msg);
                            }
                        } catch (Exception e) {
                        }
                    }
                } else if (url.contains(UrlConstants.WebURLConstants.URL_WRITE_VIDEO)) {
                    if (filterUrl(webView.getUrl())) {
                        mHandler.sendEmptyMessage(WRITE_VIDEO_MOMENT);
                    }
                } else if (url.contains(UrlConstants.WebURLConstants.URL_BUY_VIP)) {
                    if (filterUrl(webView.getUrl())) {
                        Intent intent = new Intent(getContext(), BuyVipActivity.class);
                        startActivity(intent);
                    }
                } else {
                    if (url.matches(TheLConstants.URL_PATTERN_STR))
                        webView.loadUrl(url);
                }

                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                String title = view.getTitle();
                if (title == null) {
                    title = "";
                }
                //                if (!"ad".equals(site))
                txt_title.setText(title);
                dumpUrl = url;
                refreshBtns();
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
            }
        });

        if (site != null) {
            if (site.equals("sina")) {
                webView.loadUrl(TheLConstants.THEL_SINA_SITE);
                txt_title.setText(TheLApp.getContext().getString(R.string.webview_activity_sina));
            } else if (site.equals("instagram")) {
                webView.loadUrl(TheLConstants.THEL_INS_SITE);
                txt_title.setText("Instagram");
            } else if (site.equals("fb")) {
                webView.loadUrl(TheLConstants.THEL_FB_SITE);
                txt_title.setText("Facebook");
            } else if (site.equals("help")) {
                webView.loadUrl(TheLConstants.THEL_HELP_SITE);
            } else if (site.equals("douban_cooperator")) {
                webView.loadUrl(TheLConstants.THEL_DOUBAN_COOPERATOR);
                txt_title.setText(TheLApp.getContext().getString(R.string.webview_activity_cooperater));
            } else if (site.equals("sina_user")) {
                webView.loadUrl("http://m.weibo.cn/u/" + uid);
                txt_title.setText(TheLApp.getContext().getString(R.string.webview_activity_sina));
            } else if (site.equals("ad") || site.equals("thelSite")) {
                lin_more.setVisibility(View.INVISIBLE);
                dumpUrl = getArguments().getString("url");
                webView.loadUrl(dumpUrl);
                txt_title.setText(getArguments().getString("title"));
                adUrl = dumpUrl;
            } else {
                loadOtherUrl();
            }
        } else {
            loadOtherUrl();
        }
    }

    private void setListener() {
        lin_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                getActivity().finish();
            }
        });
        lin_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);

                if (webView.canGoBack()) {// 如果网页可以后退则后退
                    webView.goBack();
                } else {
                    getActivity().finish();
                }
            }
        });
        bottom_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (webView.canGoBack()) {// 如果网页可以后退则后退
                    webView.goBack();
                }
            }
        });

        bottom_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (webView.canGoForward()) {// 如果网页可以前进则前进
                    webView.goForward();
                }
            }
        });

        bottom_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                webView.reload();
                rel_dialog.setVisibility(View.GONE);
            }
        });

        bottom_copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(webView.getUrl())) {
                    DeviceUtils.copyToClipboard(getActivity(), webView.getUrl());
                    DialogUtil.showToastShort(getActivity(), getString(R.string.info_copied));
                }
                rel_dialog.setVisibility(View.GONE);
            }
        });

        bottom_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rel_dialog.setVisibility(View.GONE);
            }
        });
        bottom_shade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rel_dialog.setVisibility(View.GONE);
            }
        });
        rel_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        lin_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogUtil.getInstance().showSelectionDialog(getActivity(), new String[]{getString(R.string.webview_activity_open_in_browser), getString(R.string.webview_activity_copy_url)}, new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> adapter, View view, int position, long arg3) {
                        ViewUtils.preventViewMultipleClick(view, 2000);
                        DialogUtil.getInstance().closeDialog();
                        switch (position) {
                            case 0:
                                if (!TextUtils.isEmpty(webView.getUrl())) {
                                    Intent intent = new Intent();
                                    intent.setAction("android.intent.action.VIEW");
                                    Uri url = Uri.parse(webView.getUrl());
                                    intent.setData(url);
                                    startActivity(intent);
                                }
                                break;
                            case 1:
                                if (!TextUtils.isEmpty(webView.getUrl())) {
                                    DeviceUtils.copyToClipboard(getActivity(), webView.getUrl());
                                    DialogUtil.showToastShort(getActivity(), getString(R.string.info_copied));
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }, false, 2, null);
            }
        });

        img_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(dumpUrl)) {
                    webView.loadUrl("javascript:window.local_obj.showSource(document.getElementsByTagName('meta')[0].content);");
                }
            }
        });
        img_recommend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recommendWeb();
            }
        });
    }

    private void shareResult(final int succeed) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("code", succeed + "");
                    if (succeed == 1) {
                        jsonObject.put("msg", "分享成功");
                    } else {
                        jsonObject.put("msg", "分享失败");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                webView.loadUrl("javascript:" + shareSucceedCallback + "('" + jsonObject.toString() + "')");
            }
        });
    }

    private String pst = "^https?://([^/\\?]*)/?.*";
    private Pattern pattern = Pattern.compile(pst, Pattern.CASE_INSENSITIVE);
    private final String HOST_1 = ".rela.me";
    private final String HOST_2 = ".thel.co";
    private final String HOST_3 = "rela.me";
    private final String HOST_4 = "thel.co";

    /**
     * 如果是thel的页面，则返回true，否则false
     *
     * @param url
     * @return
     */
    private boolean filterUrl(String url) {
        try {
            final String host = Uri.parse(url).getHost();
            return host.endsWith(HOST_1) || host.endsWith(HOST_2) || host.equals(HOST_3) || host.equals(HOST_4);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private String buildCallbackParam(String userId, String nickName, String avatar, String key, String language) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("code", "1");
            JSONObject user = new JSONObject();
            user.put("userId", userId);
            if (!TextUtils.isEmpty(nickName)) {
                user.put("nickName", nickName);
            }
            if (!TextUtils.isEmpty(avatar)) {
                user.put("avatar", avatar);
            }
            if (!TextUtils.isEmpty(key)) {
                user.put("jskey", key);
            }
            if (!TextUtils.isEmpty(language)) {
                user.put("language", language);
            }
            jsonObject.put("data", user);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString().replaceAll("\\\\", "\\\\\\\\").replaceAll("\n", "").replaceAll("'", "\\\\\'");
    }

    //    private String buildCommonCallbackParam(VolleyError error, String result) {
    //        JSONObject resultObj = new JSONObject();
    //        if (error != null) {
    //            try {
    //                resultObj.put("statusCode", error.networkResponse.statusCode);
    //                if (error.getMessage() != null)
    //                    resultObj.put("message", error.getMessage());
    //                else
    //                    resultObj.put("message", "");
    //                resultObj.put("data", "");
    //            } catch (Exception e) {
    //                e.printStackTrace();
    //            }
    //        } else {
    //            try {
    //                resultObj.put("statusCode", HttpStatus.SC_OK);
    //                resultObj.put("message", "");
    //                resultObj.put("data", result);
    //            } catch (Exception e) {
    //                e.printStackTrace();
    //            }
    //        }
    //        return resultObj.toString().replaceAll("\\\\", "\\\\\\\\").replaceAll("\n", "").replaceAll("'", "\\\\\'");
    //    }

    private void refreshBtns() {
        if (webView.canGoBack()) {// 刷新返回按钮
            bottom_back.setAlpha(1f);
        } else {
            bottom_back.setAlpha(0.3f);
        }
        if (webView.canGoForward()) {// 刷新前进按钮
            bottom_next.setAlpha(1f);
        } else {
            bottom_next.setAlpha(0.3f);
        }
    }

    private WebViewReceiver webViewReceiver;

    // 注册广播接收器
    private void registerReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(TheLConstants.BROADCAST_WX_SHARE);
        webViewReceiver = new WebViewReceiver();
        getContext().registerReceiver(webViewReceiver, intentFilter);
    }

    private class WebViewReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent i) {
            if (i != null) {
                if (TheLConstants.BROADCAST_WX_SHARE.equals(i.getAction())) {// 微信、微博分享成功消息
                    if ("succeed".equals(i.getStringExtra("result"))) {
                        shareResult(1);
                    } else {
                        shareResult(0);
                    }
                }
            }

        }
    }

    private class InJavaScriptLocalObj {
        @JavascriptInterface
        public void showSource(String html) {
            if (!TextUtils.isEmpty(html)) {
                try {
                    JSONObject jsonObject = new JSONObject(html);
                    shareLogo = JsonUtils.getString(jsonObject, "img", "");
                    String language = DeviceUtils.getLanguage();
                    if ("zh_CN".equals(language)) {
                        shareTitle = JsonUtils.getString(jsonObject, "title_zh_CN", "");
                        shareContent = JsonUtils.getString(jsonObject, "content_zh_CN", "");
                    } else if ("zh_TW".equals(language)) {
                        shareTitle = JsonUtils.getString(jsonObject, "title_zh_TW", "");
                        shareContent = JsonUtils.getString(jsonObject, "content_zh_TW", "");
                    } else {
                        shareTitle = JsonUtils.getString(jsonObject, "title_en_US", "");
                        shareContent = JsonUtils.getString(jsonObject, "content_en_US", "");
                    }
                    String url = JsonUtils.getString(jsonObject, "url", "");
                    if (!TextUtils.isEmpty(url))
                        dumpUrl = url;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            Message msg = Message.obtain();
            msg.what = SHOW_DIALOG;
            msg.obj = new DialogBean(shareTitle, shareContent, dumpUrl, shareLogo);
            mHandler.sendMessage(msg);
        }
    }

    private class InJavaScriptRecomendWeb {
        @JavascriptInterface
        public void recommendImage(String image) {
        }

    }

    private void loadOtherUrl() {
        String dumpUrl = getArguments().getString("url");
        if (TextUtils.isEmpty(dumpUrl))
            return;
        if (dumpUrl.startsWith("www.")) {
            dumpUrl = "http://" + dumpUrl;
        }
        try {
            if (getArguments().getBoolean(NEED_SECURITY_CHECK, true))
                dumpUrl = security_check + URLEncoder.encode(dumpUrl, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        webView.loadUrl(dumpUrl);
        if (!TextUtils.isEmpty(getArguments().getString("title")))
            txt_title.setText(getArguments().getString("title"));
    }

    private class DialogBean {
        public String title;
        public String content;
        public String url;
        public String logo;

        public DialogBean(String title, String content, String url, String logo) {
            this.content = content;
            this.logo = logo;
            this.title = title;
            this.url = url;
        }

        @Override
        public String toString() {
            return "DialogBean{" + "title='" + title + '\'' + ", content='" + content + '\'' + ", url='" + url + '\'' + ", logo='" + logo + '\'' + '}';
        }
    }

    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case SHOW_DIALOG:
                    DialogBean bean = (DialogBean) msg.obj;
                    showDialog(bean);
                    break;
                case WRITE_VIDEO_MOMENT:
                    gotoWriteVideoMoment();
                    break;
                case SHOW_IMAGE_SHARE_DIALOG:
                    DialogBean bean1 = (DialogBean) msg.obj;
                    showShareImageDialog(bean1);
                    break;
            }
        }
    };

    private void gotoWriteVideoMoment() {
        Intent intent = new Intent(TheLApp.getContext(), ReleaseMomentActivity.class);
        intent.putExtra(TheLConstants.BUNDLE_KEY_RELEASE_TYPE, ReleaseMomentActivity.RELEASE_TYPE_VIDEO);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        TheLApp.getContext().startActivity(intent);
    }

    private String getnewurl(String url, String addurl) {
        String newurl = url;
        if (url.contentEquals("?")) {
            newurl = url + "&rela_webview_share=" + addurl;
        } else {
            newurl = url + "?rela_webview_share=" + addurl;
        }
        return newurl;
    }

    /**
     * 4.0.0, 图片社会化分享
     *
     * @param dialogBean
     */
    private void showShareImageDialog(final DialogBean dialogBean) {
        if (TextUtils.isEmpty(dialogBean.logo)) {
            return;
        }
        byte[] d = null;
        try {
            d = Base64.decode(dialogBean.logo);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        if (null == d) {
            return;
        }
        final byte[] data = d;
        rel_dialog.setVisibility(View.VISIBLE);
        final FacebookCallback facebookCallback = new FacebookCallback() {
            @Override
            public void onSuccess(Object o) {
                shareResult(1);
            }

            @Override
            public void onCancel() {
                shareResult(0);
            }

            @Override
            public void onError(FacebookException error) {
                shareResult(0);
            }
        };
        String title = getString(R.string.share_title_event);
        final String shareTitle = TextUtils.isEmpty(dialogBean.title) ? "Rela热拉" : dialogBean.title;
        final String shareContent = TextUtils.isEmpty(dialogBean.content) ? getString(R.string.share_content_event) : dialogBean.content;
        final String singleTitle = TextUtils.isEmpty(dialogBean.title) ? getString(R.string.share_content_event) : dialogBean.title;
        final String url = dialogBean.url;
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    switch (position) {
                        case 0:// 微信朋友圈
                            MobclickAgent.onEvent(getActivity(), "start_share");
                            String newurl = getnewurl(url, "wx_timeline");
                            //new ShareAction(getActivity()).setPlatform(SHARE_MEDIA.WEIXIN_CIRCLE).withMedia(new UMImage(TheLApp.getContext(), logo)).withTargetUrl(newurl).withTitle(singleTitle).withText(singleTitle).setCallback(umShareListener).share();
                            UMShareUtils.share(getActivity(), SHARE_MEDIA.WEIXIN_CIRCLE, new UMImage(TheLApp.getContext(), data), umShareListener);

                            break;
                        case 1:// 微信消息
                            newurl = getnewurl(url, "wx_appmsg");
                            MobclickAgent.onEvent(getActivity(), "start_share");
                            //new ShareAction(getActivity()).setPlatform(SHARE_MEDIA.WEIXIN).withMedia(new UMImage(TheLApp.getContext(), logo)).withTargetUrl(newurl).withTitle(shareTitle).withText(shareContent).setCallback(umShareListener).share();
                            UMShareUtils.share(getActivity(), SHARE_MEDIA.WEIXIN, new UMImage(TheLApp.getContext(), data), umShareListener);
                            break;
                        case 2:// 微博分享
                            newurl = getnewurl(url, "weibo");

                            MobclickAgent.onEvent(getActivity(), "start_share");
                            //   new ShareAction(getActivity()).setPlatform(SHARE_MEDIA.SINA).withMedia(new UMImage(TheLApp.getContext(), logo)).withTargetUrl(newurl).withTitle(singleTitle).withText(singleTitle).setCallback(umShareListener).share();
                            UMShareUtils.share(getActivity(), SHARE_MEDIA.SINA, new UMImage(TheLApp.getContext(), data), umShareListener);
                            break;
                        case 3:// QQ空间
                            newurl = getnewurl(url, "qq_zone qq");

                            //   new ShareAction(getActivity()).setPlatform(SHARE_MEDIA.QZONE).withMedia(new UMImage(TheLApp.getContext(), logo)).withTargetUrl(newurl).withTitle(shareTitle).withText(shareContent).setCallback(umShareListener).share();
                            UMShareUtils.share(getActivity(), SHARE_MEDIA.QZONE, new UMImage(TheLApp.getContext(), data), umShareListener);
                            break;
                        case 4:// Facebook
                            newurl = getnewurl(url, "facebook");

                            MobclickAgent.onEvent(getActivity(), "start_share");
                            if (ShareDialog.canShow(SharePhotoContent.class)) {
                               /* ShareLinkContent contentS = new ShareLinkContent.Builder().setContentUrl(Uri.parse(newurl)).setContentTitle(shareTitle).setContentDescription(shareContent).setImageUrl(Uri.parse(logo)).build();
                                ShareDialog shareDialog = new ShareDialog(getActivity());
                                if (callbackManager != null && facebookCallback != null)
                                    shareDialog.registerCallback(callbackManager, facebookCallback);
                                shareDialog.show(contentS)*/
                                /**4.0.0 facebook分享图片**/
                                SharePhoto photo = new SharePhoto.Builder().setBitmap(Utils.decodeBitmap(data)).build();
                                SharePhotoContent content = new SharePhotoContent.Builder().addPhoto(photo).build();
                            }
                            break;
                        case 5:// QQ消息
                            newurl = getnewurl(url, "qq_appmsg");

                            MobclickAgent.onEvent(getActivity(), "start_share");
                            //   new ShareAction(getActivity()).setPlatform(SHARE_MEDIA.QQ).withMedia(new UMImage(TheLApp.getContext(), logo)).withTargetUrl(newurl).withTitle(shareTitle).withText(shareContent).setCallback(umShareListener).share();
                            UMShareUtils.share(getActivity(), SHARE_MEDIA.QQ, new UMImage(TheLApp.getContext(), data), umShareListener);
                            break;
                        case 6:
                            downloadPic(dialogBean.logo);
                            break;

                        default:
                            break;
                    }
                    rel_dialog.setVisibility(View.GONE);
                } catch (Exception e) {
                }
            }

        });
        bottom_title.setText(title);
        List<Integer> images = new ArrayList<>();
        images.add(R.drawable.btn_share_moments_s);
        images.add(R.drawable.btn_share_wx_s);
        images.add(R.drawable.btn_share_sina_s);
        images.add(R.drawable.btn_share_qzone_s);
        images.add(R.drawable.btn_share_fb_s);
        images.add(R.drawable.btn_share_qq_s);
        List<String> texts = new ArrayList<String>();
        texts.add(TheLApp.getContext().getString(R.string.share_moments));
        texts.add(TheLApp.getContext().getString(R.string.share_wx));
        texts.add(TheLApp.getContext().getString(R.string.share_sina));
        texts.add(TheLApp.getContext().getString(R.string.share_qzone));
        texts.add(TheLApp.getContext().getString(R.string.share_fb));
        texts.add(TheLApp.getContext().getString(R.string.share_qq));
        /** 4.0.0 下载图片**/
        texts.add(TheLApp.getContext().getString(R.string.download_pic));
        images.add(R.mipmap.btn_sharesocial_download);

        gridView.setAdapter(new ShareGridAdapter(images, texts, gridView));
    }

    private void showDialog(DialogBean dialogBean) {
        rel_dialog.setVisibility(View.VISIBLE);
        final FacebookCallback facebookCallback = new FacebookCallback() {
            @Override
            public void onSuccess(Object o) {
                shareResult(1);
            }

            @Override
            public void onCancel() {
                shareResult(0);
            }

            @Override
            public void onError(FacebookException error) {
                shareResult(0);
            }
        };
        String title = getString(R.string.share_title_event);
        final String shareTitle = TextUtils.isEmpty(dialogBean.title) ? "Rela热拉" : dialogBean.title;
        final String shareContent = TextUtils.isEmpty(dialogBean.content) ? getString(R.string.share_content_event) : dialogBean.content;
        final String singleTitle = TextUtils.isEmpty(dialogBean.title) ? getString(R.string.share_content_event) : dialogBean.title;
        final String url = dialogBean.url;
        final String logo = TextUtils.isEmpty(dialogBean.logo) ? TheLConstantsExt.DEFAULT_SHARE_LOGO_URL : dialogBean.logo;
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    switch (position) {
                        case 0:// 微信朋友圈
                            MobclickAgent.onEvent(getActivity(), "start_share");
                            String newurl = getnewurl(url, "wx_timeline");
                            //new ShareAction(getActivity()).setPlatform(SHARE_MEDIA.WEIXIN_CIRCLE).withMedia(new UMImage(TheLApp.getContext(), logo)).withTargetUrl(newurl).withTitle(singleTitle).withText(singleTitle).setCallback(umShareListener).share();
                            UMShareUtils.share(getActivity(), SHARE_MEDIA.WEIXIN_CIRCLE, new UMImage(TheLApp.getContext(), ImageUtils.buildNetPictureUrl(logo, TheLConstants.WX_SHARE_IMAGE_SIZE, TheLConstants.WX_SHARE_IMAGE_SIZE)), newurl, singleTitle, singleTitle, umShareListener);

                            break;
                        case 1:// 微信消息
                            newurl = getnewurl(url, "wx_appmsg");
                            MobclickAgent.onEvent(getActivity(), "start_share");
                            //new ShareAction(getActivity()).setPlatform(SHARE_MEDIA.WEIXIN).withMedia(new UMImage(TheLApp.getContext(), logo)).withTargetUrl(newurl).withTitle(shareTitle).withText(shareContent).setCallback(umShareListener).share();
                            UMShareUtils.share(getActivity(), SHARE_MEDIA.WEIXIN, new UMImage(TheLApp.getContext(), ImageUtils.buildNetPictureUrl(logo, TheLConstants.WX_SHARE_IMAGE_SIZE, TheLConstants.WX_SHARE_IMAGE_SIZE)), newurl, shareTitle, shareContent, umShareListener);
                            break;
                        case 2:// 微博分享
                            newurl = getnewurl(url, "weibo");

                            MobclickAgent.onEvent(getActivity(), "start_share");
                            //   new ShareAction(getActivity()).setPlatform(SHARE_MEDIA.SINA).withMedia(new UMImage(TheLApp.getContext(), logo)).withTargetUrl(newurl).withTitle(singleTitle).withText(singleTitle).setCallback(umShareListener).share();
                            UMShareUtils.share(getActivity(), SHARE_MEDIA.SINA, new UMImage(TheLApp.getContext(), logo), newurl, singleTitle, singleTitle, umShareListener);
                            break;
                        case 3:// QQ空间
                            newurl = getnewurl(url, "qq_zone qq");

                            //   new ShareAction(getActivity()).setPlatform(SHARE_MEDIA.QZONE).withMedia(new UMImage(TheLApp.getContext(), logo)).withTargetUrl(newurl).withTitle(shareTitle).withText(shareContent).setCallback(umShareListener).share();
                            UMShareUtils.share(getActivity(), SHARE_MEDIA.QZONE, new UMImage(TheLApp.getContext(), logo), newurl, shareTitle, shareContent, umShareListener);
                            break;
                        case 4:// Facebook
                            newurl = getnewurl(url, "facebook");

                            MobclickAgent.onEvent(getActivity(), "start_share");
                            if (ShareDialog.canShow(ShareLinkContent.class)) {
                                ShareLinkContent contentS = new ShareLinkContent.Builder().setContentUrl(Uri.parse(newurl)).setContentTitle(shareTitle).setContentDescription(shareContent).setImageUrl(Uri.parse(logo)).build();
                                ShareDialog shareDialog = new ShareDialog(getActivity());
                                if (callbackManager != null && facebookCallback != null)
                                    shareDialog.registerCallback(callbackManager, facebookCallback);
                                shareDialog.show(contentS);
                            }
                            break;
                        case 5:// QQ消息
                            newurl = getnewurl(url, "qq_appmsg");

                            MobclickAgent.onEvent(getActivity(), "start_share");
                            //   new ShareAction(getActivity()).setPlatform(SHARE_MEDIA.QQ).withMedia(new UMImage(TheLApp.getContext(), logo)).withTargetUrl(newurl).withTitle(shareTitle).withText(shareContent).setCallback(umShareListener).share();
                            UMShareUtils.share(getActivity(), SHARE_MEDIA.QQ, new UMImage(TheLApp.getContext(), logo), newurl, shareTitle, shareContent, umShareListener);
                            break;
                        case 6:
                            Intent intent = new Intent();
                            intent.setAction("android.intent.action.VIEW");
                            Uri uri = Uri.parse(url);
                            intent.setData(uri);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            TheLApp.getContext().startActivity(intent);
                            break;
                        default:
                            break;
                    }
                    rel_dialog.setVisibility(View.GONE);
                } catch (Exception e) {
                }
            }

        });
        bottom_title.setText(title);
        List<Integer> images = new ArrayList<Integer>();
        images.add(R.drawable.btn_share_moments_s);
        images.add(R.drawable.btn_share_wx_s);
        images.add(R.drawable.btn_share_sina_s);
        images.add(R.drawable.btn_share_qzone_s);
        images.add(R.drawable.btn_share_fb_s);
        images.add(R.drawable.btn_share_qq_s);
        List<String> texts = new ArrayList<String>();
        texts.add(TheLApp.getContext().getString(R.string.share_moments));
        texts.add(TheLApp.getContext().getString(R.string.share_wx));
        texts.add(TheLApp.getContext().getString(R.string.share_sina));
        texts.add(TheLApp.getContext().getString(R.string.share_qzone));
        texts.add(TheLApp.getContext().getString(R.string.share_fb));
        texts.add(TheLApp.getContext().getString(R.string.share_qq));
        images.add(R.drawable.btn_share_browser_s);
        texts.add(TheLApp.getContext().getString(R.string.webview_activity_open_in_browser));
        //4.0.0 下载图片
        // images.add(R.drawable.btn_sharesocial_download);
        //texts.add(TheLApp.getContext().getString(R.string.download_pic));
        gridView.setAdapter(new ShareGridAdapter(images, texts, gridView));
    }

    /**
     * 4.0.0，下载图片
     *
     * @param logo
     */
    private void downloadPic(String logo) {
        Utils.savePicFromBase64(logo);
    }

    /**
     * 推荐网页
     */
    private void recommendWeb() {
        inSertGetImageJs();//注入js
        getJsImage();
    }

    /**
     * 获取图片
     */
    private void inSertGetImageJs() {
        webView.loadUrl("javascript:" + recommendWebJs2);//注入js

    }

    private String getUrlByValue(String value) {
        if (value.startsWith("\"")) {
            value = value.substring(1);
        }
        if (value.endsWith("\"")) {
            value = value.substring(0, value.length() - 1);

        }
        return value;
    }

    /**
     *
     */
    private void getJsImage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            webView.evaluateJavascript("getDefaultImg(300, 300)", new ValueCallback<String>() {
                @Override
                public void onReceiveValue(String value) {
                    L.d("refresh", "getJsImage:value=" + value);
                    String imageUrl = getUrlByValue(value);
                    L.d("refresh", "getJsImage:value=" + imageUrl);
                    String title = webView.getTitle();
                    RecommendWebBean bean = new RecommendWebBean();
                    bean.imageUrl = imageUrl;
                    bean.title = title;
                    bean.url = webView.getUrl();
                    final Intent intent = new Intent(getContext(), SendCardActivity.class);
                    intent.putExtra(TheLConstants.BUNDLE_KEY_INTENT_FROM, SendCardActivity.FROM_CECOMMEND_WEBVIEW);
                    intent.putExtra(TheLConstants.BUNDLE_KEY_RECOMMEND_WEB_BEAN, bean);
                    startActivity(intent);
                }
            });
        }
    }

    private String recommendWebJs = "function getDefaultImg(width, height) { \n" + "var reg = /(http[s]?|ftp):\\/\\/[^\\/.]+?..+\\w$/g \n" + "var imgs = document.getElementsByTagName('img'); \n" + "for (var i = 0; i < imgs.length; i++) { \n" + "var img = imgs[i]; \n" + "var w = img.width; \n" + "var h = img.height; \n" + "if (w >= width && h >= height) { \n" + "var src = img.src; \n" + "if (reg.test(src)) return src; \n" + "} \n" + "} \n" + "return ''; \n" + "}";


    private String recommendWebJs2 = "function getDefaultImg(b,k){" + "var c=/(http[s]?|ftp):\\/\\/[^\\/\\.]+?\\..+\\w$/g;" + "var g=document.getElementsByTagName(\"img\");" + "for(var e=0;e<g.length;e++)" + "{var d=g[e];var j=d.width;var f=d.height;" + "if(j>=b&&f>=k)" + "{var a=d.src;if(c.test(a)){return a}}}" + "return\"\"};";

    private void followUser(String userId, String type) {
        RequestBusiness.getInstance().followUser(userId, type).onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<ResultBean>());
    }


}
