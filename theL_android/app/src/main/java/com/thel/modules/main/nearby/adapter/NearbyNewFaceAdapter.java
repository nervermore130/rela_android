package com.thel.modules.main.nearby.adapter;

import android.content.Intent;
import android.net.Uri;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.postprocessors.IterativeBoxBlurPostProcessor;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.user.NearUserBean;
import com.thel.constants.TheLConstants;
import com.thel.modules.live.surface.watch.LiveWatchActivity;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;
import com.thel.utils.BusinessUtils;
import com.thel.utils.ImageUtils;
import com.umeng.analytics.MobclickAgent;

import java.util.List;

import video.com.relavideolibrary.Utils.DensityUtils;

/**
 * Created by chad
 * Time 18/11/19
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */
public class NearbyNewFaceAdapter extends BaseRecyclerViewAdapter<NearUserBean> {
    //是否显示距离
    private final boolean hideNearbyDistance;

    private float radius; // 图片高度
    private float photoWidth; // 图片宽度
    private float photoHeigth; // 图片高度

    public NearbyNewFaceAdapter(List<NearUserBean> list, boolean hideNearbyDistance) {
        super(R.layout.adapter_nearby_new_face_recyclerview, list);
        this.hideNearbyDistance = hideNearbyDistance;

        photoWidth = DensityUtils.dp2px(75);
        photoHeigth = DensityUtils.dp2px(133);

        radius = TheLApp.getContext().getResources().getDimension(R.dimen.nearby_user_view_radius);
    }

    @Override
    protected void convert(BaseViewHolder holdView, final NearUserBean userBean) {
        holdView.setVisibility(R.id.ll_distance, View.GONE);
        holdView.setVisibility(R.id.rl_vertify, View.GONE);
        holdView.setVisibility(R.id.ll_live_intro, View.GONE);
        holdView.setVisibility(R.id.lin_live, View.GONE);
        holdView.setVisibility(R.id.img_vip, View.GONE);
        holdView.setVisibility(R.id.btn_more_face, View.GONE);
        holdView.setVisibility(R.id.rel_info, View.VISIBLE);

        if (userBean.verifyType <= 0) {//非加V用户
            holdView.setVisibility(R.id.ll_distance, View.VISIBLE);
            setDistance(holdView, userBean);
        } else {//加V
            holdView.setVisibility(R.id.rl_vertify, View.VISIBLE);
            setVerify(holdView, userBean);
        }

        if (userBean.level > 0) {// vip用户
            holdView.setVisibility(R.id.img_vip, View.VISIBLE);
            if (userBean.level < TheLConstants.VIP_LEVEL_RES.length) {
                holdView.setImageUrl(R.id.img_vip, TheLConstants.RES_PIC_URL + TheLConstants.VIP_LEVEL_RES[userBean.level]);
            }
        }
        TextView nickname = holdView.convertView.findViewById(R.id.txt_nickname);
        nickname.getPaint().setFakeBoldText(true);
        holdView.setText(R.id.txt_nickname, userBean.nickname);

        final SimpleDraweeView imgThumb = holdView.getView(R.id.img_thumb);

        processOnLiveArea(holdView, userBean);

        if (holdView.getAdapterPosition() == 9) {
            holdView.setVisibility(R.id.btn_more_face, View.VISIBLE);
            holdView.setVisibility(R.id.rel_info, View.GONE);
            holdView.setVisibility(R.id.lin_live, View.GONE);
            if (!TextUtils.isEmpty(userBean.picUrl)) {//如果有url，设置背景色，没有，设置默认热拉色
                imgThumb.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(userBean.picUrl, photoWidth, photoHeigth))).setPostprocessor(new IterativeBoxBlurPostProcessor(150, 10)).build()).setAutoPlayAnimations(true).build());
            } else {
                imgThumb.setImageResource(R.color.tab_normal);
            }
        } else {
            imgThumb.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(userBean.picUrl, photoWidth, photoHeigth))).build()).setAutoPlayAnimations(true).build());
        }
        imgThumb.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadius(radius).setRoundingMethod(RoundingParams.RoundingMethod.OVERLAY_COLOR).setOverlayColor(ContextCompat.getColor(TheLApp.getContext(), R.color.white)));
    }

    private void processOnLiveArea(BaseViewHolder holdView, final NearUserBean userBean) {
        if (userBean.live > 0) {
            holdView.setVisibility(R.id.lin_live, View.VISIBLE);
            holdView.setVisibility(R.id.ll_live_intro, View.VISIBLE);
            holdView.setText(R.id.txt_live_intro, userBean.liveText);
        }
        if (NearUserBean.LIVE_TYPE_VOICE.equals(userBean.liveType)) {//声音直播
            holdView.setImageResource(R.id.img_on_live, R.mipmap.icon_voice_live);
        } else {
            holdView.setImageResource(R.id.img_on_live, R.mipmap.icon_camera_live);
        }
        holdView.setBackgroundRes(R.id.rel_info, userBean.live == 0 ? R.color.transparent : R.drawable.bg_nearby_live_bg);

        holdView.setText(R.id.txt_on_live_looker_num, userBean.liveLooker + "");
        holdView.setOnClickListener(R.id.lin_live, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (BusinessUtils.canIntoLiveRoom(TheLApp.getContext(), userBean.userId + "")) {
                    MobclickAgent.onEvent(TheLApp.getContext(), "enter_live_room_from_nearby");
                    Intent intent = new Intent(TheLApp.getContext(), LiveWatchActivity.class);
                    intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, userBean.userId + "");
                    intent.putExtra(LiveWatchActivity.FROM_PAGE, LiveWatchActivity.FROM_PAGE_MOMENT);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    TheLApp.getContext().startActivity(intent);
                }
            }
        });

    }


    /***距离***/
    private void setDistance(BaseViewHolder holdView, NearUserBean userBean) {
        TextView txt_distance = holdView.convertView.findViewById(R.id.txt_distance);
        txt_distance.getPaint().setFakeBoldText(true);
        if (hideNearbyDistance) {// 五公里内的都显示为“附近”
            try {
                if (userBean.distance != null && (userBean.distance.contains(" Km") || userBean.distance.contains(" km"))) {
                    float dis = -1f;
                    if (userBean.distance.contains(" Km")) {
                        dis = Float.valueOf(userBean.distance.substring(0, userBean.distance.indexOf(" Km")));
                    } else if (userBean.distance.contains(" km")) {
                        dis = Float.valueOf(userBean.distance.substring(0, userBean.distance.indexOf(" km")));
                    }
                    if (dis <= 5) {
                        if (dis == -1f) {
                            holdView.setText(R.id.txt_distance, userBean.distance);
                        } else {
                            holdView.setText(R.id.txt_distance, TheLApp.getContext().getString(R.string.nearby_activity_nearby));
                        }
                    } else {
                        holdView.setText(R.id.txt_distance, userBean.distance);
                    }
                } else {
                    holdView.setText(R.id.txt_distance, TheLApp.getContext().getString(R.string.nearby_activity_nearby));
                }
            } catch (Exception e) {
                holdView.setText(R.id.txt_distance, userBean.distance);
            }
        } else {
            holdView.setText(R.id.txt_distance, userBean.distance);
        }
        final int position = holdView.getLayoutPosition() - getHeaderLayoutCount();
    }


    /***显示简介***/
    private void setVerify(BaseViewHolder holdView, NearUserBean userBean) {
        holdView.setText(R.id.txt_verify, userBean.verifyIntro);
        if (userBean.verifyType == 1) {// 个人加V
            //  holdView.setBackgroundRes(R.id.txt_verify, R.drawable.bg_textview_identification_yellow);
            holdView.setImageResource(R.id.iv_vertify, R.mipmap.icn_verify_person);
        } else if (userBean.verifyType == 2) {// 企业加V
            holdView.setImageResource(R.id.iv_vertify, R.mipmap.icn_verify_enterprise);
        } else {//默认个人加V
            holdView.setImageResource(R.id.iv_vertify, R.mipmap.icn_verify_person);

        }
    }
}
