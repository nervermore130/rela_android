package com.thel.modules.main.discover;

import com.thel.base.BasePresenter;
import com.thel.base.BaseView;
import com.thel.bean.AdBean;
import com.thel.modules.live.bean.LiveClassifyBean;
import com.thel.modules.live.bean.LivePopularityNetBean;

import java.util.List;

/**
 * Created by lingwei on 2017/11/19.
 */

public interface LiveRoomsListContract2 {
    interface Presenter extends BasePresenter {
        void getRefreshLiveClassfyData();

        void getRefreshTopLinkData();

    }

    interface View extends BaseView<Presenter> {
        void showRefreshLiveClassifyData(List<LiveClassifyBean> listData);

        void requestFinish();

        void showRefreshTopLinkData(LivePopularityNetBean bean);

    }
}
