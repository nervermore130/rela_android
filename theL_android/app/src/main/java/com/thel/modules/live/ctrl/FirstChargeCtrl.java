package com.thel.modules.live.ctrl;

import com.thel.constants.TheLConstants;
import com.thel.modules.live.GiftFirstChargeBean;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.service.DefaultRequestService;
import com.thel.utils.L;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/*
 * 首充,解决多个地方同时有自己的获取首充逻辑,根据之前的逻辑，均未对失败逻辑做处理，故本控制器也咱不处理失败
 *
 * add by wangjun 2019.11.12
 * */
public class FirstChargeCtrl {

    public static boolean sIsLiveRoomFirstChargeAlreadyShow = false;//全局记录 '应用每次重启后展示 1 次'

    private static FirstChargeCtrl mInstance;

    public static FirstChargeCtrl get() {
        if (mInstance == null) {
            synchronized (FirstChargeCtrl.class) {
                if (mInstance == null) {
                    mInstance = new FirstChargeCtrl();
                }
            }
        }
        return mInstance;
    }

    private final String TAG = this.getClass().getSimpleName();
    //    private Context context;
    private volatile boolean fetching = false;
    private List<FirstChargeCallBack> callBackList = new CopyOnWriteArrayList<>();
    private boolean isAlreadyShow = false;

//    private boolean isFirstCharge;

    private FirstChargeCtrl() {
//        this.context = context;
    }

    public void register(FirstChargeCallBack callBack) {
        if (TheLConstants.IsFirstCharge == 0) {//如果已知不是首充
            callBack.getCompleted(false);
        } else {
            synchronized (this) {
                callBackList.add(callBack);
                if (!fetching) {//如果数据在获取中
                    getGiftIsFirstCharge();
                }
            }
        }
    }

    public void unregister(FirstChargeCallBack callBack) {
        callBackList.remove(callBack);
    }

    public void updateWhenPaymentSuccessful() {
        L.d(TAG, "updateWhenPaymentSuccessful ");
        if (TheLConstants.IsFirstCharge != 0) {//
            TheLConstants.IsFirstCharge = 0;
            dispatchListeners(false);
        }
    }

    public synchronized void destroy() {
        callBackList.clear();
        fetching = false;
    }

    private synchronized void handleHttpResponseSuccessful(GiftFirstChargeBean firstChargeBean) {
        L.d(TAG, "handleHttpResponseSuccessful " + firstChargeBean);
        if (firstChargeBean != null && firstChargeBean.data != null) {
            //缓存到本地
            TheLConstants.IsFirstCharge = firstChargeBean.data.isFirstCharge;
            dispatchListeners(isSuccessful(TheLConstants.IsFirstCharge));
        }
        fetching = false;
    }

    private void dispatchListeners(boolean isFirstCharge) {
        for (FirstChargeCallBack callBack : callBackList) {
            callBack.getCompleted(isFirstCharge);
        }
    }

    private synchronized void handleHttpResponseError(Throwable t) {
        L.d(TAG, "handleHttpResponseError " + t);
        fetching = false;
    }

    private boolean isSuccessful(int code) {
        return code == 1;
    }

    private void getGiftIsFirstCharge() {
        fetching = true;
        Flowable<GiftFirstChargeBean> beanFlowable = DefaultRequestService.createAllRequestService().getIsFirstCharge();
        beanFlowable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<GiftFirstChargeBean>() {
            @Override
            public void onNext(GiftFirstChargeBean data) {
                super.onNext(data);
                handleHttpResponseSuccessful(data);
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                handleHttpResponseError(t);
            }
        });

    }

    public interface FirstChargeCallBack {
        void getCompleted(boolean isFirstCharge);
//        void onError(boolean isFirstCharge);
    }
}
