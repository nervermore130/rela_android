package com.thel.modules.main.userinfo;

import android.content.Context;

import com.thel.base.BasePresenter;
import com.thel.base.BaseView;
import com.thel.bean.HiddenLoveBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.moments.MomentsListBean;
import com.thel.bean.user.BlockBean;
import com.thel.bean.user.UserInfoBean;

import java.util.List;

/**
 * Created by waiarl on 2017/10/18.
 */

public class UserInfoContract {
    interface Presenter extends BasePresenter {
        /**
         * 获取个人信息
         *
         * @param userId
         */
        void getUserInfoData(String userId);

        /**
         * 刷新个人日志列表
         *
         * @param userId
         */
        void getRefreshUserInfoMomentListData(String userId);

        /**
         * 加载更多日志
         *
         * @param userId
         * @param cursor
         */
        void getLoadMoreUserInfoMomentListData(String userId, int cursor);

        /**
         * 屏蔽她
         *
         * @param userId
         */
        void blackHer(String userId);

        void showLoading();

        /**
         * 暗恋
         *
         * @param userId
         */
        void sendHiddenLove(String userId);


        /**
         * 悄悄关注
         *
         * @param userId
         * @param actionType
         */
        void secretlyFollow(String userId, int actionType);

        /**
         * 举报用户头像
         *
         * @param id
         * @param avatar
         * @param type
         */
        void postReportImageOrUser(String id, String avatar, int type);
    }

    interface View extends BaseView<Presenter> {

        void showUserInfoCommonViewData(UserInfoBean userInfoBean);

        /**
         * 显示个人信息顶部
         *
         * @param userInfoBean
         */
        void showUserInfoMainTopViewData(UserInfoBean userInfoBean);


        /**
         * 个人信息（右侧个人详情）
         *
         * @param userInfoBean
         */
        void showUserInfoRightViewData(UserInfoBean userInfoBean);

        /**
         * 日志列表header，包括直播信息，个人相册，以及密友
         *
         * @param userInfoBean
         */
        void showUserInfoMomentHeaderView(UserInfoBean userInfoBean);

        /**
         * 个人日志列表
         *
         * @param momentsBeanList
         */
        void showRefreshMomentListData(List<MomentsBean> momentsBeanList);

        /**
         * 更多个人日志
         *
         * @param momentsBeanList
         */
        void showLoadMoreMomentListData(List<MomentsBean> momentsBeanList);

        /**
         * 加载更多个人日志列表失败
         */
        void loadMoreFailed();

        /**
         * 是否被屏蔽或者屏蔽了她
         */
        void isBlockMe();

        /**
         * 个人日志列表为空
         */
        void emptyMoment();

        /**
         * 请求结束
         */
        void requestFinish();

        /**
         * 非法用户
         */
        void illegalUser();

        void showLoadingDialog();

        void showSecretlyFollow(String userId, int actionType);

        /**
         * 没有更多数据
         *
         * @param haveNextPage
         */
        boolean haveNextPage(boolean haveNextPage);

        void refreshMomentNum(int momentsTotalNum);

        void pullUserIntoBlock(BlockBean data);

        void showHiddenLove(HiddenLoveBean hiddenLoveBean);

        /**
         * 不存在用户
         */
        void notExistUser();

        /**
         * 此用户已注销
         */
        void unregisterUser();

        /**
         * 已经被我屏蔽
         */
        void isMyBlack();
    }

    public interface MomentPresenter extends BasePresenter {
        /**
         * 刷新个人日志列表
         *
         * @param userId
         */
        void getRefreshMomentListData(String userId, int type);

        /**
         * 加载更多日志
         *
         * @param userId
         * @param cursor
         */
        void getLoadMoreListData(String userId, int cursor, int type);

        void register(Context context);

        void unRegister(Context context);

    }

    public interface MomentView extends BaseView<MomentPresenter> {
        int TYPE_MOMENT = 0;
        int TYPE_VIDEO = 1;

        void showRefreshListData(MomentsListBean momentsListBean);

        void showLoadMoreListData(MomentsListBean momentsListBean);

        void emptyData(boolean empty);

        void refreshMomentNum(int momentsTotalNum, int type);

        void loadMoreFailed();

        void requestFinish();

        void refreshData();

    }

}
