package com.thel.modules.main.messages.db;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import android.util.Log;

import com.thel.app.TheLApp;
import com.thel.constants.TheLConstants;
import com.thel.db.DBUtils;
import com.thel.imp.follow.FollowStatusChangedImpl;
import com.thel.imp.friend.FriendListContract;
import com.thel.imp.wink.WinkUtils;
import com.thel.modules.main.messages.bean.MsgBean;
import com.thel.modules.main.messages.bean.MsgListBean;
import com.thel.modules.main.messages.bean.MsgWithUnreadBean;
import com.thel.modules.main.messages.config.XMPPConstants;
import com.thel.modules.main.messages.utils.MessagesUtils;
import com.thel.modules.main.messages.utils.MsgUtils;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MessageDataBaseAdapter {

    private static final String TAG = MessageDataBaseAdapter.class.getSimpleName();

    /**
     * 数据库对象
     */
    private SQLiteDatabase db;
    private static String databaseName;
    /**
     * 数据库适配器对象
     */
    private static MessageDataBaseAdapter dbAdapter = null;
    private MessageDataBaseHelper dbHelper = null;
    /**
     * 数据库版本号
     */
    private static final int DB_VERSION = 4;
    //4.0.0 以后该为5，要迁移聊天数据，并且创建新表
    //private static final int DB_VERSION = 5;

    /**
     * 系统表sqlite_sequence
     */
    public static final String TB_NAME_SQLITE_SEQUENCE = "sqlite_sequence";

    /**
     * 系统消息表
     */
    public static final String TB_NAME_SYSTEM_MSG = "table_0000";
    public static final String SYSTEM_MSG = "0000";

    /**
     * 待发送的消息表
     */
    public static final String TB_NAME_SENDING_MSGS = "table_sending_msgs";
    public static final String SENDING_MSGS = "sending_msgs";

    /**
     * 消息列表，存储对话记录（最新的一条消息、未读消息数）
     */
    public static final String TB_NAME_MSG_LIST = "table_msg_list";
    public static final String MSG_LIST = "msg_list";

    /**
     * 陌生人消息表
     */

    public static final String TB_NAME_STRANGER_MSG_LIST = "table_stranger_msg_list_v1";
    public static final String TB_NAME_STRANGER_MSG_LIST_T = "stranger_msg_list_v1";
    public static final String STRANGER_MSG_USER_ID = "stranger_msg_userid";//陌生人消息id

    // 原先关注、取消关注、挤眼消息全都存在系统消息表中，2.18.0版本开始将他们分开存了
    /**
     * 关注和取消关注消息表
     */
    public static final String FOLLOW_TABLENAME = "follow";
    /**
     * 挤眼消息表
     */
    public static final String WINK_TABLENAME = "wink";

    public static final String SYSTEM_TABLENAME = "0000";

    /**
     * 密友和伴侣请求对话的userId
     */
    public static final String CIRCLE_REQUEST_CHAT_USER_ID = "circleRequest";

    /*----------------------------------------字段--------------------------------------------*/

    public final String F_ID = "_id";
    public static final String F_PACKETID = "packetId";     // 消息的ID
    public static final String F_MSG_DIRECTION = "msgDirection"; // 消息的方向
    public static final String F_MSG_STATUS = "msgStatus"; // 消息状态(发送成功,失败等)
    public static final String F_MSG_TYPE = "msgType";
    public static final String F_MSG_TIME = "msgTime";
    public static final String F_MSG_TEXT = "msgText";
    public static final String F_USERID = "userId"; // 对方的id
    public static final String F_USERNAME = "userName"; // 对方的name
    public static final String F_AVATAR = "avatar"; // 对方的头像地址
    public static final String F_RECEIVEDID = "receivedId"; // 我自己的id
    public static final String F_MSG_LNG = "msgLng";
    public static final String F_MSG_LAT = "msgLat";
    public static final String F_MSG_MAPURL = "msgMapUrl";
    public static final String F_MSG_IMAGE = "msgImage";
    public static final String F_MSG_HEIGHT = "msgHeight";
    public static final String F_MSG_WIDTH = "msgWidth";
    public static final String F_VOICETIME = "voiceTime"; // 录音时长
    public static final String F_FILEPATH = "filePath";
    public static final String F_FROMUSERID = "fromUserId";
    public static final String F_FROMNICKNAME = "fromNickname";
    public static final String F_FROMAVATAR = "fromAvatar";
    public static final String F_FROMMESSAGEUSER = "fromMessageUser";
    public static final String F_TOUSERID = "toUserId";
    public static final String F_TONICKNAME = "toNickname";
    public static final String F_TOAVATAR = "toAvatar";
    public static final String F_TOMESSAGEUSER = "toMessageUser";
    public static final String F_HADREAD = "hadread";
    public static final String F_HADPLAYED = "hadplayed";
    public static final String F_MSG_RESERVED_ONE = "msgReservedOne"; // 是否是系统消息(wink,follow)
    public static final String F_MSG_RESERVED_TWO = "msgReservedTwo"; // 保留字段
    public static final String F_MSG_RESERVED_THREE = "msgReservedThree";
    public static final String F_MSG_SEND_TIMES = "sendTimes";
    public static final String F_MSG_UNREAD_COUNT = "unreadCount";
    public static final String F_MSG_STICKY_TOP = "stickyTop";// 置顶时间

    public synchronized static MessageDataBaseAdapter getInstance(Context context, String myUserId) {
        if (null == dbAdapter) {
            dbAdapter = new MessageDataBaseAdapter();
        }
        databaseName = "the_l_" + myUserId + ".db";
        return dbAdapter.openDataBase();
    }

    private MessageDataBaseAdapter openDataBase() {
        if (dbHelper == null) {
            dbHelper = new MessageDataBaseHelper(TheLApp.context);
        }
        try {
            db = dbHelper.getWritableDatabase();
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
        return dbAdapter;
    }

    public SQLiteDatabase getDb() {
        return db;
    }

    public void closeDataBase() {
        // if (null != db && db.isOpen()) {
        // db.close();
        // }
    }

    public void closeDB() {
        if (null != db && db.isOpen()) {
            db.close();
        }
        dbAdapter = null;
        dbHelper = null;
    }

    /**
     * 判断表是否存在
     *
     * @param tabName
     * @return
     */
    public boolean isTableExist(String tabName) {
        boolean result = false;
        tabName = "table_" + tabName;
        Cursor cursor = null;
        try {
            String sql = "select count(*) as c from sqlite_master where type ='table' and name ='" + tabName.trim() + "' ";
            cursor = db.rawQuery(sql, null);
            if (cursor.moveToNext()) {
                int count = cursor.getInt(0);
                if (count > 0) {
                    result = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != cursor)
                cursor.close();
        }
        return result;
    }

    /**
     * 建表
     *
     * @param tableName
     */
    private void createTable(String tableName) {
//        tableName = "table_" + tableName;
//        String sql = "create table " + tableName + "("
//                + F_ID + " integer primary key AUTOINCREMENT,"
//                + F_PACKETID + " text UNIQUE,"
//                + F_MSG_DIRECTION + " text,"
//                + F_MSG_STATUS + " text,"
//                + F_MSG_TYPE + " Long,"
//                + F_MSG_TIME + " text,"
//                + F_MSG_TEXT + " text,"
//                + F_AVATAR + " text,"
//                + F_USERID + " text,"
//                + F_USERNAME + " text,"
//                + F_RECEIVEDID + " text,"
//                + F_MSG_LNG + " text,"
//                + F_MSG_LAT + " text,"
//                + F_MSG_MAPURL + " text,"
//                + F_MSG_IMAGE + " text,"
//                + F_MSG_HEIGHT + " text,"
//                + F_MSG_WIDTH + " text,"
//                + F_VOICETIME + " text,"
//                + F_FILEPATH + " text,"
//                + F_MSG_RESERVED_ONE + " text,"
//                + F_MSG_RESERVED_TWO + " text,"
//                + F_MSG_RESERVED_THREE + " text,"
//                + F_FROMUSERID + " text,"
//                + F_FROMNICKNAME + " text,"
//                + F_FROMAVATAR + " text,"
//                + F_FROMMESSAGEUSER + " text,"
//                + F_TOUSERID + " text,"
//                + F_TONICKNAME + " text,"
//                + F_TOAVATAR + " text,"
//                + F_TOMESSAGEUSER + " text,"
//                + F_HADREAD + " integer,"
//                + F_HADPLAYED + " integer" + ")";
//
//        db.execSQL(sql);
    }

    /**
     * 创建消息列表
     */
    private void createMsgListTable() {
//        String sql = "create table "
//                + TB_NAME_MSG_LIST + "("
//                + F_ID + " integer primary key AUTOINCREMENT,"
//                + F_USERID + " text UNIQUE,"
//                + F_MSG_UNREAD_COUNT + " integer,"
//                + F_MSG_STICKY_TOP + " integer,"
//                + F_PACKETID + " text,"
//                + F_MSG_DIRECTION + " text,"
//                + F_MSG_STATUS + " text,"
//                + F_MSG_TYPE + " Long,"
//                + F_MSG_TIME + " text,"
//                + F_MSG_TEXT + " text,"
//                + F_AVATAR + " text,"
//                + F_USERNAME + " text,"
//                + F_RECEIVEDID + " text,"
//                + F_MSG_LNG + " text,"
//                + F_MSG_LAT + " text,"
//                + F_MSG_MAPURL + " text,"
//                + F_MSG_IMAGE + " text,"
//                + F_MSG_HEIGHT + " text,"
//                + F_MSG_WIDTH + " text,"
//                + F_VOICETIME + " text,"
//                + F_FILEPATH + " text,"
//                + F_MSG_RESERVED_ONE + " text,"
//                + F_MSG_RESERVED_TWO + " text,"
//                + F_MSG_RESERVED_THREE + " text,"
//                + F_FROMUSERID + " text,"
//                + F_FROMNICKNAME + " text,"
//                + F_FROMAVATAR + " text,"
//                + F_FROMMESSAGEUSER + " text,"
//                + F_TOUSERID + " text,"
//                + F_TONICKNAME + " text,"
//                + F_TOAVATAR + " text,"
//                + F_TOMESSAGEUSER + " text,"
//                + F_HADREAD + " integer,"
//                + F_HADPLAYED + " integer" + ")";
//        try {
//            db.execSQL(sql);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    /**
     * 创建消息列表
     */
    private void createStrangerMsgListTable() {
//        String sql = "create table "
//                + TB_NAME_STRANGER_MSG_LIST + "("
//                + F_ID + " integer primary key AUTOINCREMENT,"
//                + F_USERID + " text UNIQUE,"
//                + F_MSG_UNREAD_COUNT + " integer,"
//                + F_MSG_STICKY_TOP + " integer,"
//                + F_PACKETID + " text,"
//                + F_MSG_DIRECTION + " text,"
//                + F_MSG_STATUS + " text,"
//                + F_MSG_TYPE + " Long,"
//                + F_MSG_TIME + " text,"
//                + F_MSG_TEXT + " text,"
//                + F_AVATAR + " text,"
//                + F_USERNAME + " text,"
//                + F_RECEIVEDID + " text,"
//                + F_MSG_LNG + " text,"
//                + F_MSG_LAT + " text,"
//                + F_MSG_MAPURL + " text,"
//                + F_MSG_IMAGE + " text,"
//                + F_MSG_HEIGHT + " text,"
//                + F_MSG_WIDTH + " text,"
//                + F_VOICETIME + " text,"
//                + F_FILEPATH + " text,"
//                + F_MSG_RESERVED_ONE + " text,"
//                + F_MSG_RESERVED_TWO + " text,"
//                + F_MSG_RESERVED_THREE + " text,"
//                + F_FROMUSERID + " text,"
//                + F_FROMNICKNAME + " text,"
//                + F_FROMAVATAR + " text,"
//                + F_FROMMESSAGEUSER + " text,"
//                + F_TOUSERID + " text,"
//                + F_TONICKNAME + " text,"
//                + F_TOAVATAR + " text,"
//                + F_TOMESSAGEUSER + " text,"
//                + F_HADREAD + " integer,"
//                + F_HADPLAYED + " integer" + ")";
//        try {
//            db.execSQL(sql);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    /**
     * 创建待发送表
     *
     * @param tableName
     */
    private void createSendingTable(String tableName) {
//        tableName = "table_" + tableName;
//        String sql = "create table " + tableName + "("
//                + F_ID + " integer primary key AUTOINCREMENT,"
//                + F_PACKETID + " text UNIQUE,"
//                + F_MSG_SEND_TIMES + " integer,"
//                + F_MSG_DIRECTION + " text,"
//                + F_MSG_STATUS + " text,"
//                + F_MSG_TYPE + " Long,"
//                + F_MSG_TIME + " text,"
//                + F_MSG_TEXT + " text,"
//                + F_AVATAR + " text,"
//                + F_USERID + " text,"
//                + F_USERNAME + " text,"
//                + F_RECEIVEDID + " text,"
//                + F_MSG_LNG + " text,"
//                + F_MSG_LAT + " text,"
//                + F_MSG_MAPURL + " text,"
//                + F_MSG_IMAGE + " text,"
//                + F_MSG_HEIGHT + " text,"
//                + F_MSG_WIDTH + " text,"
//                + F_VOICETIME + " text,"
//                + F_FILEPATH + " text,"
//                + F_MSG_RESERVED_ONE + " text,"
//                + F_MSG_RESERVED_TWO + " text,"
//                + F_MSG_RESERVED_THREE + " text,"
//                + F_FROMUSERID + " text,"
//                + F_FROMNICKNAME + " text,"
//                + F_FROMAVATAR + " text,"
//                + F_FROMMESSAGEUSER + " text,"
//                + F_TOUSERID + " text,"
//                + F_TONICKNAME + " text,"
//                + F_TOAVATAR + " text,"
//                + F_TOMESSAGEUSER + " text,"
//                + F_HADREAD + " integer,"
//                + F_HADPLAYED + " integer" + ")";
//
//        db.execSQL(sql);
    }

    /**
     * 删除消息表
     *
     * @para tableName
     */
    public void dropTable(String tableName) {
        try {
            db.execSQL("drop table if exists table_" + tableName);
            deleteChatByUserId(tableName);
            deleteStrangerChatByUserId(tableName);//如果在陌生人里面，也删除
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*------------------------------系统消息表的操作-----------------------------------*/

    /**
     * 删除系统消息记录（只删除收到的系统消息，不删除发送的系统消息）
     *
     * @return
     */
    public boolean deleteSystemMessage(int systemType) {
        if (systemType == MsgBean.IS_SYSTEM_MSG) {// 系统表
            int delCount = 0;
            try {
                delCount = db.delete(TB_NAME_SYSTEM_MSG, F_MSG_DIRECTION + " = ?", new String[]{
                        MsgBean.MSG_DIRECTION_TYPE_INCOME});
                deleteChatByUserId(SYSTEM_MSG);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                closeDataBase();
            }
            return delCount >= 1;
        } else if (systemType == MsgBean.IS_WINK_MSG) {// 挤眼表
            dropTable(MessageDataBaseAdapter.WINK_TABLENAME);
            return true;
        } else if (systemType == MsgBean.IS_FOLLOW_MSG) {// 关注表
            dropTable(MessageDataBaseAdapter.FOLLOW_TABLENAME);
            return true;
        }
        return false;
    }

    /*------------------------------系统消息表的操作-----------------------------------*/

    /*-------------------------------------对表的各项操作---------------------------------------*/

    /**
     * 插入或更新一条新的对话
     *
     * @param msgBean
     * @param isRequest 是不是密友请求，密友请求要特殊处理
     * @param count     如果是密友请求的话，此属性生效，作为未读消息数
     */
    public void insertNewChat(MsgBean msgBean, boolean isRequest, int count) {
//        L.d("insertNewChat", " msgBean : " + msgBean);
//        // 判断表是否存在
//        if (!isTableExist("msg_list")) {
//            createMsgListTable(); // 建表
//        }
//        if (TextUtils.isEmpty(msgBean.packetId) && !isRequest) {// 当普通消息（包括系统消息）的packetId为空时，说明消息已经被删光了，则删除对话记录。但密友请求消息的packetId是空的
//            deleteChatByUserId(msgBean.userId);
//            return;
//        }
//        Cursor cursor = null;
//        try {
//            cursor = db.rawQuery("select * from " + TB_NAME_MSG_LIST + " where " + F_USERID + " = ?", new String[]{
//                    msgBean.userId});
//            MsgWithUnreadBean msgListItemBean = new MsgWithUnreadBean();
//            msgListItemBean.msgBean = msgBean;
//            msgListItemBean.stickyTop = getChatStickyTopTime(msgBean.userId);
//            if (isRequest)
//                msgListItemBean.unReadCount = count;
//            else
//                msgListItemBean.unReadCount = getCountUnread(msgBean.userId);
//            if (cursor.moveToNext()) {// 已存在对话，则刷新
//                long isSuccess = db.update(TB_NAME_MSG_LIST, msgListItemBean.getContentValues(), F_USERID + " = ?", new String[]{
//                        msgBean.userId});
//                L.d("insertNewChat", " isSuccess : " + isSuccess);
//            } else {// 不存在对话，则新建一条
//                db.insert(TB_NAME_MSG_LIST, null, msgListItemBean.getContentValues());
//            }
////            cursor.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            closeDataBase();
//            if (null != cursor)
//                cursor.close();
//        }
    }

    /**
     * 获取某个对话的未读消息数
     *
     * @param userId
     * @return
     */
    public int getChatUnreadCount(String userId) {
        Cursor cursor = null;
        int count = 0;
        String sql = "select " + F_MSG_UNREAD_COUNT + " from " + TB_NAME_MSG_LIST + " where " + F_USERID + " = '" + userId + "'";
        try {
            cursor = db.rawQuery(sql, null);
            if (cursor.moveToNext()) {
                count = cursor.getInt(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != cursor)
                cursor.close();
        }
        return count;
    }

    /**
     * 获取某个对话最新一条消息的时间
     *
     * @param userId
     * @return
     */
    public MsgBean getChatLastMsg(String userId) {
        Cursor cursor = null;
        MsgBean msgBean = null;
        String sql = "select * from " + TB_NAME_MSG_LIST + " where " + F_USERID + " = '" + userId + "'";
        try {
            cursor = db.rawQuery(sql, null);
            if (cursor.moveToNext()) {
                msgBean = new MsgBean();
                msgBean.fromCursor(cursor, false);
                L.d("MessageData", " msgBean : " + msgBean);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != cursor)
                cursor.close();
        }
        return msgBean;
    }

    /**
     * 删除消息列表中的一个对话
     *
     * @param userId
     */
    public void deleteChatByUserId(String userId) {
        try {
            db.delete(TB_NAME_MSG_LIST, F_USERID + " = ?", new String[]{userId});
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDataBase();
        }
    }

    /**
     * 获取对话的指定时间（-1表示未置顶）
     *
     * @param userId
     * @return
     */
    public long getChatStickyTopTime(String userId) {
        Cursor cursor = null;
        long stickyTopTime = -1;
        String sql = "select " + F_MSG_STICKY_TOP + " from " + TB_NAME_MSG_LIST + " where " + F_USERID + " = '" + userId + "'";
        try {
            cursor = db.rawQuery(sql, null);
            if (cursor.moveToNext()) {
                stickyTopTime = cursor.getInt(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != cursor)
                cursor.close();
        }
        return stickyTopTime;
    }

    /**
     * 更新对话的置顶时间
     *
     * @param userId
     * @param time
     */
    public void updateChatStickyTopTime(String userId, long time) {
        try {
            db.execSQL("UPDATE " + TB_NAME_MSG_LIST + " SET " + F_MSG_STICKY_TOP + " = " + time + " WHERE " + F_USERID + " = '" + userId + "'");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDataBase();
        }
    }

    /**
     * 刷新对话的未读消息数
     *
     * @param userId
     * @param count
     */
    public void updateChatUnreadCount(String userId, int count) {
        try {
            db.execSQL("UPDATE " + TB_NAME_MSG_LIST + " SET " + F_MSG_UNREAD_COUNT + " = " + count + " WHERE " + F_USERID + " = '" + userId + "'");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDataBase();
        }
    }

    /**
     * 刷新对话信息
     *
     * @param userId
     */
    private void updateChatByUserId(String userId) {
        insertNewChat(getLastMsg("table_" + userId), false, 0);// 刷新该对话在消息列表数据
    }


    public boolean transferDataFor2180() {
        if (db == null)
            return false;

        // 判断表是否存在
        if (!isTableExist(SYSTEM_MSG)) {
            return true;
        }
        Cursor cursor = null;
        final ArrayList<MsgBean> winkMsgs = new ArrayList<>();
        final ArrayList<MsgBean> followMsgs = new ArrayList<>();
        try {
            cursor = db.rawQuery("select * from table_" + SYSTEM_MSG + " where " + F_MSG_DIRECTION + " = '" + MsgBean.MSG_DIRECTION_TYPE_INCOME + "'", null);
            while (cursor.moveToNext()) {
                final MsgBean tempbean = new MsgBean();
                tempbean.fromCursor(cursor, false);
                if (MsgBean.MSG_TYPE_WINK.equals(tempbean.msgType)) {
                    tempbean.isSystem = MsgBean.IS_WINK_MSG;
                    winkMsgs.add(tempbean);
                } else if (MsgBean.MSG_TYPE_FOLLOW.equals(tempbean.msgType) || MsgBean.MSG_TYPE_UNFOLLOW.equals(tempbean.msgType)) {
                    tempbean.isSystem = MsgBean.IS_FOLLOW_MSG;
                    followMsgs.add(tempbean);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != cursor) {
                cursor.close();
            }
            closeDataBase();
        }
        final Map<String, ArrayList<MsgBean>> msgsMap = new HashMap<>();
        msgsMap.put(WINK_TABLENAME, winkMsgs);
        msgsMap.put(FOLLOW_TABLENAME, followMsgs);

        try {
            db.beginTransaction();
            // 处理消息，这些消息会显示在消息列表中
            for (Map.Entry<String, ArrayList<MsgBean>> entry : msgsMap.entrySet()) {// 处理与一个用户的所有消息
                String tableName = entry.getKey();
                // 判断表是否存在
                if (!isTableExist(tableName)) {
                    createTable(tableName); // 建表
                }
                for (MsgBean msgBean : entry.getValue()) {// 逐条保存与这个用户的每条消息
                    try {
                        db.execSQL(buildMsgInsertSQL(msgBean), buildMsgInsertParams(msgBean));
                    } catch (SQLException e) {
                        e.printStackTrace();
                        continue;
                    }
                }
                updateChatByUserId(tableName);
            }
            // 删除系统消息中的数据
            db.delete(TB_NAME_SYSTEM_MSG, F_MSG_DIRECTION + " = ?", new String[]{MsgBean.MSG_DIRECTION_TYPE_INCOME});
            db.delete(TB_NAME_MSG_LIST, F_USERID + " = ?", new String[]{SYSTEM_MSG});
            db.setTransactionSuccessful();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
        return true;
    }

    /**
     * 获取对话列表
     *
     * @return
     */
    public MsgListBean getChatList() {
        if (!isTableExist(MSG_LIST)) {
            createMsgListTable(); // 建表
        }

        MsgListBean msgListBean = new MsgListBean();
        Cursor cursor = null;
        try {
            cursor = db.rawQuery("select * from " + TB_NAME_MSG_LIST + " order by " + F_MSG_STICKY_TOP + " desc, " + F_MSG_TIME + " desc", null);

            L.d("continue", " cursor count : " + cursor.getCount());

            if (cursor != null && cursor.moveToFirst()) {
                do {
                    MsgWithUnreadBean tempbean = new MsgWithUnreadBean();
                    tempbean.fromCursor(cursor);
                    L.d("continue", "tempbean :" + tempbean);
                    // 如果密友请求对话的msgText不是show，则不显示入口
                    L.d("continue", "1");
                    if (MsgBean.MSG_TYPE_REQUEST.equals(tempbean.msgBean.msgType) && !MsgBean.SHOW_CIRCLE_REQUEST_CHAT.equals(tempbean.msgBean.msgText)) {
                        L.d("continue", "2");
                        continue;
                    }
                    msgListBean.msgWithUnreadBeans.add(tempbean);
                    msgListBean.unreadTotal += tempbean.unReadCount;
                } while (cursor.moveToNext());
            }
//            while (cursor.moveToNext()) {
//                MsgWithUnreadBean tempbean = new MsgWithUnreadBean();
//                tempbean.fromCursor(cursor);
//                // 如果密友请求对话的msgText不是show，则不显示入口
//                if (MsgBean.MSG_TYPE_REQUEST.equals(tempbean.msgBean.msgType) && !MsgBean.SHOW_CIRCLE_REQUEST_CHAT.equals(tempbean.msgBean.msgText))
//                    continue;
//                msgListBean.msgWithUnreadBeans.add(tempbean);
//                msgListBean.unreadTotal += tempbean.unReadCount;
//            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != cursor) {
                cursor.close();
            }
            closeDataBase();
        }
        return msgListBean;
    }

    /**
     * 读取表中未读消息数
     *
     * @param tabName
     * @return
     */
    public int getCountUnread(String tabName) {
        if (!isTableExist(tabName)) {
            createTable(tabName);
        }
        Cursor cursor = null;
        int count = 0;
        String sql = "select count(*) as c from table_" + tabName + " where hadread = '0'";
        try {
            cursor = db.rawQuery(sql, null);
            if (cursor.moveToNext()) {
                count = cursor.getInt(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != cursor)
                cursor.close();
        }
        return count;
    }

    /**
     * 删除一条消息记录(根据记录id)
     *
     * @param tableName
     * @param id
     * @return
     */
    public boolean deleteMessage(String tableName, int id) {
        int delCount = 0;
        try {
            delCount = db.delete("table_" + tableName, F_ID + "=" + id, null);
            updateChatByUserId(tableName);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDataBase();
        }
        return delCount >= 1;
    }

    /**
     * 获取表中最后一条消息，如果消息已经为空了，则返回空的MsgBean实例，packetId为空
     *
     * @param tableName
     * @return
     */
    public MsgBean getLastMsg(String tableName) {
        if (TextUtils.isEmpty(tableName)) {
            return null;
        }
        Cursor cursor = null;
        MsgBean tempbean = null;
        try {
            // 如果是系统消息表，则要过滤掉发出去的消息
            if (tableName.equals(TB_NAME_SYSTEM_MSG)) {
//                cursor = db.rawQuery("select * from " + tableName + " where " + F_MSG_DIRECTION + " = 1 order by msgTime desc limit 1", null);
                cursor = db.rawQuery("select * from " + tableName + " where " + F_MSG_DIRECTION + " = 1 order by _id desc limit 1", null);
            } else {
//                cursor = db.rawQuery("select * from " + tableName + " order by msgTime desc limit 1", null);
                cursor = db.rawQuery("select * from " + tableName + " order by _id desc limit 1", null);
            }
            tempbean = new MsgBean();
            while (cursor.moveToNext()) {
                tempbean.fromCursor(cursor, false);
            }
            tempbean.userId = tableName.replace("table_", "");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != cursor)
                cursor.close();
        }
        return tempbean;
    }

    /**
     * 获取多条消息记录(分页)
     *
     * @param tableName
     * @return
     */
    public ArrayList<MsgBean> getPeopleMessagesPagination(String tableName, int startIndex, int size) {
        L.d("refresh", "tableExist:" + isTableExist(tableName) + ",tableName=" + tableName + ",name=table_" + tableName);
        if (!isTableExist(tableName)) {
            createTable(tableName); // 建表
        }

        tableName = "table_" + tableName;

        ArrayList<MsgBean> arrayList = new ArrayList<MsgBean>();
        Cursor cursor = null;
        try {
            if (tableName.equals(TB_NAME_SYSTEM_MSG)) {
//                cursor = db.rawQuery("select * from (select * from " + tableName + " where msgDirection = 1 order by msgTime desc limit " + size + " offset " + startIndex + ") order by msgTime", null);
                cursor = db.rawQuery("select * from (select * from " + tableName + " where msgDirection = 1 order by _id desc limit " + size + " offset " + startIndex + ") order by _id", null);
            } else {
//                cursor = db.rawQuery("select * from (select * from " + tableName + " order by msgTime desc limit " + size + " offset " + startIndex + ") order by msgTime", null);
                cursor = db.rawQuery("select * from (select * from " + tableName + " order by _id desc limit " + size + " offset " + startIndex + ") order by _id", null);
            }

            if (cursor != null && cursor.moveToFirst()) {
                do {
                    MsgBean tempbean = new MsgBean();
                    tempbean.fromCursor(cursor, false);
                    L.d("MessageDataBase", " tempbean time : " + tempbean.msgTime);
                    L.d("MessageDataBase", " tempbean id : " + tempbean.id);
                    arrayList.add(tempbean);
                } while (cursor.moveToNext());
            }

//            while (cursor.moveToNext()) {
//                MsgBean tempbean = new MsgBean();
//                tempbean.fromCursor(cursor, false);
//                arrayList.add(tempbean);
//            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != cursor) {
                cursor.close();
            }
            closeDataBase();
        }
        L.subLog("MessagesPagination", arrayList.toString());
        return arrayList;
    }

    public List<MsgBean> getMsgList(String tableName) {

        L.d("MessageDataBase", " tableName : " + tableName);

        if (!isTableExist(tableName)) {
            return null;
        }

        List<MsgBean> list = new ArrayList<>();

        Cursor cursor = db.rawQuery("select * from " + "table_" + tableName, null);

        if (cursor.moveToFirst()) {

            L.d("MessageDataBase", " tempBean cursor.getCount() : " + cursor.getCount());

            do {
                MsgBean tempBean = new MsgBean();
                tempBean.fromCursor(cursor, false);
                L.d("MessageDataBase", " tempBean time : " + tempBean.msgTime);
                L.d("MessageDataBase", " tempBean id : " + tempBean.id);
                list.add(tempBean);
            } while (cursor.moveToNext());
        }

        return list;
    }

    /**
     * 获取一个对话的所有图片url
     *
     * @param tableName
     * @return
     */
    public ArrayList<String> getAllPhotosInChat(String tableName) {
        if (!isTableExist(tableName)) {
            createTable(tableName); // 建表
        }

        tableName = "table_" + tableName;

        ArrayList<String> arrayList = new ArrayList<>();
        Cursor cursor = null;
        try {
            cursor = db.rawQuery("SELECT " + F_FILEPATH + " FROM " + tableName + " WHERE " + F_MSG_TYPE + " = ? ORDER BY " + F_MSG_TIME + " ASC", new String[]{
                    MsgBean.MSG_TYPE_IMAGE});
            while (cursor.moveToNext()) {
                arrayList.add(cursor.getString(cursor.getColumnIndex(F_FILEPATH)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != cursor) {
                cursor.close();
            }
            closeDataBase();
        }
        return arrayList;
    }

    public ArrayList<MsgBean> getFailedSystemMsgs() {
        String tableName = "table_" + SYSTEM_MSG;
        // 判断表是否存在
        if (!isTableExist(SYSTEM_MSG)) {
            createTable(SYSTEM_MSG); // 建表
        }
        ArrayList<MsgBean> arrayList = new ArrayList<MsgBean>();
        Cursor cursor = null;
        try {
            String myUserId = ShareFileUtils.getString(ShareFileUtils.ID, "");
            cursor = db.rawQuery("select * from " + tableName + " where " + F_MSG_DIRECTION + " = '" + MsgBean.MSG_DIRECTION_TYPE_OUTGOING + "' and " + F_MSG_STATUS + " = '" + XMPPConstants.MSGSTATUS_FAIL + "' and " + F_FROMUSERID + " = '" + myUserId + "'", null);
            while (cursor.moveToNext()) {
                MsgBean tempbean = new MsgBean();
                tempbean.fromCursor(cursor, false);
                arrayList.add(tempbean);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != cursor) {
                cursor.close();
            }
            closeDataBase();
        }
        return arrayList;
    }

    /**
     * 保存一条消息记录
     *
     * @param msgBean
     * @param tableName
     */
    public MsgBean saveMessage(MsgBean msgBean, String tableName) {
        // 判断表是否存在
        if (!isTableExist(tableName)) {
            createTable(tableName); // 建表
        }
        int id = -1;
        try {
            MsgBean tempMsg = getMsgByPacketId(msgBean.packetId, tableName);
            if (tempMsg != null) {
                db.update("table_" + tableName, msgBean.getContentValues(false), F_PACKETID + " = ?", new String[]{
                        msgBean.packetId});
                id = tempMsg.id;
            } else {
                id = (int) db.insert("table_" + tableName, null, msgBean.getContentValues(false));
            }
            if (!SYSTEM_MSG.equals(tableName)) {// 发出的系统消息不显示在消息列表中，所以不需要刷新
                if (MessagesUtils.isRelationFriend(tableName)) {
                    updateChatByUserId(tableName);
                    deleteStrangerChatByUserId(tableName);
                } else {
                    updateChatStrangerByUserId(tableName, msgBean);
                    deleteChatByUserId(tableName);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDataBase();
        }
        msgBean.id = id;
        return msgBean;
    }

    /**
     * 保存一条消息记录
     *
     * @param msgBean
     * @param tableName
     */
    public MsgBean saveWarningMessage(MsgBean msgBean, String tableName) {
        // 判断表是否存在
        if (!isTableExist(tableName)) {//没这个表就返回
            createTable(tableName); // 建表
        }
        int id = -1;
        try {
            MsgBean tempMsg = getMsgByPacketId(msgBean.packetId, tableName);
            if (tempMsg != null) {
                db.update("table_" + tableName, msgBean.getContentValues(false), F_PACKETID + " = ?", new String[]{
                        msgBean.packetId});
                id = tempMsg.id;
            } else {
                id = (int) db.insert("table_" + tableName, null, msgBean.getContentValues(false));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDataBase();
        }
        msgBean.id = id;
        return msgBean;
    }

    /**
     * 保存一条待发送消息
     *
     * @param msgBean
     * @return
     */
    public void saveSendingMessage(MsgBean msgBean) {
        // 判断表是否存在
        if (!isTableExist(TB_NAME_SENDING_MSGS)) {
            createSendingTable(TB_NAME_SENDING_MSGS); // 建表
        }
        try {
            db.insert("table_" + TB_NAME_SENDING_MSGS, null, msgBean.getContentValues(true));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDataBase();
        }
    }

    /**
     * 发送成功，移除待发送消息
     *
     * @param packetId
     */
    public void removeSendingMsg(String packetId) {
        try {
            db.delete("table_" + TB_NAME_SENDING_MSGS, F_PACKETID + " = '" + packetId + "'", null);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDataBase();
        }
    }

    /**
     * 更新发送次数
     *
     * @param packetId
     */
    public void updateSendingMsgSendTimes(String packetId, int times) {
        try {
            db.execSQL("UPDATE table_" + TB_NAME_SENDING_MSGS + " SET " + F_MSG_SEND_TIMES + " = " + times + " WHERE " + F_PACKETID + " = '" + packetId + "'");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDataBase();
        }
    }

    /**
     * 读取第一条待发送的消息
     *
     * @return
     */
    public MsgBean getFirstSendingMsg() {
        // 判断表是否存在
        if (!isTableExist(TB_NAME_SENDING_MSGS)) {
            createSendingTable(TB_NAME_SENDING_MSGS); // 建表
        }

        MsgBean msgBean = null;
        Cursor cursor = null;
        try {
            cursor = db.rawQuery("select * from table_" + TB_NAME_SENDING_MSGS + " limit 1", null);
            while (cursor.moveToNext()) {
                msgBean = new MsgBean();
                msgBean.fromCursor(cursor, true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != cursor) {
                cursor.close();
            }
            closeDataBase();
        }
        return msgBean;
    }

    public String buildMsgInsertSQL(MsgBean msgBean) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("INSERT OR IGNORE INTO table_");
        switch (msgBean.isSystem) {
            case MsgBean.IS_NORMAL_MSG:
                stringBuilder.append(msgBean.fromUserId);
                break;
            case MsgBean.IS_SYSTEM_MSG:
                stringBuilder.append(MessageDataBaseAdapter.SYSTEM_MSG);
                break;
            case MsgBean.IS_WINK_MSG:
                stringBuilder.append(msgBean.fromUserId);
                break;
            case MsgBean.IS_FOLLOW_MSG:
                stringBuilder.append(MessageDataBaseAdapter.FOLLOW_TABLENAME);
                break;
        }

        stringBuilder.append(" VALUES(");
        stringBuilder.append("null,");
        stringBuilder.append("?").append(",");
        stringBuilder.append("?").append(",");
        stringBuilder.append("?").append(",");
        stringBuilder.append("?").append(",");
        stringBuilder.append("?").append(",");
        stringBuilder.append("?").append(",");
        stringBuilder.append("?").append(",");
        stringBuilder.append("?").append(",");
        stringBuilder.append("?").append(",");
        stringBuilder.append("?").append(",");
        stringBuilder.append("?").append(",");
        stringBuilder.append("?").append(",");
        stringBuilder.append("?").append(",");
        stringBuilder.append("?").append(",");
        stringBuilder.append("?").append(",");
        stringBuilder.append("?").append(",");
        stringBuilder.append("?").append(",");
        stringBuilder.append("?").append(",");
        stringBuilder.append("?").append(",");
        stringBuilder.append("'',");
        stringBuilder.append("'',");
        stringBuilder.append("?").append(",");
        stringBuilder.append("?").append(",");
        stringBuilder.append("?").append(",");
        stringBuilder.append("?").append(",");
        stringBuilder.append("?").append(",");
        stringBuilder.append("?").append(",");
        stringBuilder.append("?").append(",");
        stringBuilder.append("?").append(",");
        stringBuilder.append("?").append(",");
        stringBuilder.append("?").append(")");
        return stringBuilder.toString();
    }

    public String[] buildMsgInsertParams(MsgBean msgBean) {
        String[] params = new String[]{

                TextUtils.isEmpty(msgBean.packetId) ? "" : msgBean.packetId,

                TextUtils.isEmpty(msgBean.msgDirection) ? "" : msgBean.msgDirection,

                TextUtils.isEmpty(msgBean.msgStatus) ? "" : msgBean.msgStatus,

                TextUtils.isEmpty(msgBean.msgType) ? "" : msgBean.msgType,

                msgBean.msgTime == null ? "" : msgBean.msgTime + "",

                TextUtils.isEmpty(msgBean.msgText) ? "" : msgBean.msgText,

                TextUtils.isEmpty(msgBean.avatar) ? "" : msgBean.avatar,

                TextUtils.isEmpty(msgBean.userId) ? "" : msgBean.userId,

                TextUtils.isEmpty(msgBean.userName) ? "" : msgBean.userName,

                TextUtils.isEmpty(msgBean.receivedId) ? "" : msgBean.receivedId,

                TextUtils.isEmpty(msgBean.msgLng) ? "" : msgBean.msgLng,

                TextUtils.isEmpty(msgBean.msgLat) ? "" : msgBean.msgLat,

                TextUtils.isEmpty(msgBean.msgMapUrl) ? "" : msgBean.msgMapUrl,

                TextUtils.isEmpty(msgBean.msgImage) ? "" : msgBean.msgImage,

                TextUtils.isEmpty(msgBean.msgHeight) ? "" : msgBean.msgHeight,

                TextUtils.isEmpty(msgBean.msgWidth) ? "" : msgBean.msgWidth,

                TextUtils.isEmpty(msgBean.voiceTime) ? "" : msgBean.voiceTime,

                TextUtils.isEmpty(msgBean.filePath) ? "" : msgBean.filePath,

                msgBean.isSystem + "",

                TextUtils.isEmpty(msgBean.fromUserId) ? "" : msgBean.fromUserId,

                TextUtils.isEmpty(msgBean.fromNickname) ? "" : msgBean.fromNickname,

                TextUtils.isEmpty(msgBean.fromAvatar) ? "" : msgBean.fromAvatar,

                TextUtils.isEmpty(msgBean.fromMessageUser) ? "''" : msgBean.fromMessageUser,

                TextUtils.isEmpty(msgBean.toUserId) ? "" : msgBean.toUserId,

                TextUtils.isEmpty(msgBean.toUserNickname) ? "" : msgBean.toUserNickname,

                TextUtils.isEmpty(msgBean.toAvatar) ? "" : msgBean.toAvatar,

                TextUtils.isEmpty(msgBean.toMessageUser) ? "" : msgBean.toMessageUser,

                msgBean.hadRead + "",

                msgBean.hadPlayed + "",

        };
        return params;
    }

    /**
     * 获取指定的消息
     *
     * @param packetId
     * @param tableName
     * @return
     */
    public MsgBean getMsgByPacketId(String packetId, String tableName) {
        Cursor cursor = null;
        MsgBean tempbean = null;
        if (TextUtils.isEmpty(packetId)) {
            return tempbean;
        }
        try {
            cursor = db.rawQuery("select * from table_" + tableName + " where " + F_PACKETID + " = ?", new String[]{
                    packetId});
            while (cursor.moveToNext()) {
                tempbean = new MsgBean();
                tempbean.fromCursor(cursor, false);
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return tempbean;
    }

    /**
     * 更新表中单条未读消息为已读且已发回执(或已读但未发回执)
     *
     * @param tabName
     */
    public void updateUnreadMsg(String tabName, String packetId, boolean hasSentReceipt) {
        String tableName = "table_" + tabName;
        try {
            if (hasSentReceipt) {
                db.execSQL("UPDATE " + tableName + " SET " + F_HADREAD + " = " + "'" + MsgBean.HAD_READ_AND_SENT_RECEIPT + "' WHERE " + F_PACKETID + " = '" + packetId + "'");
            } else {
                db.execSQL("UPDATE " + tableName + " SET " + F_HADREAD + " = " + "'" + MsgBean.HAD_READ + "' WHERE " + F_PACKETID + " = '" + packetId + "'");
                db.execSQL("UPDATE " + TB_NAME_MSG_LIST + " SET " + F_MSG_UNREAD_COUNT + " = " + getCountUnread(tabName) + " WHERE " + F_USERID + " = '" + tabName + "'");// 刷新消息列表中的未读数
                if (isTableExist(TB_NAME_STRANGER_MSG_LIST_T)) {
                    db.execSQL("UPDATE " + TB_NAME_STRANGER_MSG_LIST + " SET " + F_MSG_UNREAD_COUNT + " = " + getCountUnread(tabName) + " WHERE " + F_USERID + " = '" + tabName + "'");// 刷新陌生人消息列表中的未读数
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDataBase();
        }
    }

    /**
     * 更新表中所有未读消息为已读
     *
     * @param tabName
     */
    public void updataUnreadMsg(String tabName) {
        try {
            db.execSQL("UPDATE table_" + tabName + " SET " + F_HADREAD + " = '1' WHERE " + F_HADREAD + " = '0'");
            db.execSQL("UPDATE " + TB_NAME_MSG_LIST + " SET " + F_MSG_UNREAD_COUNT + " = 0 WHERE " + F_USERID + " = '" + tabName + "'");// 把消息列表中的未读数置为0
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDataBase();
        }
    }

    /**
     * 更新表中一条未听语音为已听
     *
     * @param tabName
     * @param id
     */
    public void updataUnListenedMsg(String tabName, int id) {
        tabName = "table_" + tabName;
        try {
            db.execSQL("UPDATE " + tabName + " SET " + F_HADPLAYED + " = '1' WHERE " + F_ID + " = '" + id + "'");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDataBase();
        }
    }

    /**
     * 刷新为语音下载失败
     *
     * @param voiceMsg
     */
    public void updateVoiceMsgToFailed(MsgBean voiceMsg) {
        String tableName = "table_" + voiceMsg.fromUserId;
        try {
            db.execSQL("UPDATE " + tableName + " SET " + F_HADPLAYED + " = '2' WHERE " + F_PACKETID + " = '" + voiceMsg.packetId + "'");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDataBase();
        }
    }

    /**
     * 刷新语音路径
     *
     * @param voiceMsg
     */
    public void updateVoiceMsgPath(MsgBean voiceMsg) {
        String tableName = "table_" + voiceMsg.fromUserId;
        try {
            db.execSQL("UPDATE " + tableName + " SET " + F_FILEPATH + " = '" + voiceMsg.filePath + "' WHERE " + F_PACKETID + " = '" + voiceMsg.packetId + "'");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDataBase();
        }
    }

    /**
     * 刷新语音路径
     *
     * @param msg
     * @param tableName
     */
    public void updateMsg(MsgBean msg, String tableName) {
        try {
            db.update("table_" + tableName, msg.getContentValues(false), F_PACKETID + " = ?", new String[]{
                    msg.packetId});
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDataBase();
        }
    }

    /**
     * 更新表中一条消息状态为发送失败
     *
     * @param tabName
     * @param packetId
     */
    public void updateMsgToFailed(String tabName, String packetId) {
        tabName = "table_" + tabName;
        try {
            db.execSQL("UPDATE " + tabName + " SET " + F_MSG_STATUS + " = '" + MsgBean.MSG_STATUS_FAILED + "' WHERE " + F_PACKETID + " = '" + packetId + "'");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDataBase();
        }
    }

    /**
     * 更新表中一条消息状态为发送中
     *
     * @param tabName
     * @param packetId
     */
    public void updateMsgToSending(String tabName, String packetId) {
        try {
            db.execSQL("UPDATE table_" + tabName + " SET " + F_MSG_STATUS + " = '" + MsgBean.MSG_STATUS_SENDING + "' WHERE " + F_PACKETID + " = '" + packetId + "'");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDataBase();
        }
    }

    /**
     * 更新表中一条消息状态为发送成功
     *
     * @param tabName
     * @param packetId
     */
    public void updateMsgToSucceed(String tabName, String packetId) {
        tabName = "table_" + tabName;
        Cursor cursor = null;
        try {
            // 有可能在调这个方法的时候，该消息已经收到received回执了，所以，先判断一下是不是已经received了
            cursor = db.rawQuery("SELECT " + F_MSG_STATUS + " FROM " + tabName + " WHERE " + F_PACKETID + " = ?", new String[]{
                    packetId});
            while (cursor.moveToNext()) {
                final String status = cursor.getString(0);
                if (MsgBean.MSG_STATUS_RECEIVED.equals(status))
                    return;
            }
            db.execSQL("UPDATE " + tabName + " SET " + F_MSG_STATUS + " = '" + MsgBean.MSG_STATUS_SUCCEEDED + "' WHERE " + F_PACKETID + " = '" + packetId + "'");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != cursor)
                cursor.close();
            closeDataBase();
        }
    }

    /**
     * 更新表中一条消息状态为对方已接受
     *
     * @param tabName
     * @param packetId
     */
    public void updateMsgToReceived(String tabName, String packetId) {
        tabName = "table_" + tabName;
        try {
            db.execSQL("UPDATE " + tabName + " SET " + F_MSG_STATUS + " = '" + MsgBean.MSG_STATUS_RECEIVED + "' WHERE " + F_PACKETID + " = '" + packetId + "'");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDataBase();
        }
    }

    private class MessageDataBaseHelper extends SQLiteOpenHelper {
        public MessageDataBaseHelper(Context context) {
            super(context, databaseName, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }
    /********************************4.0.0陌生人消息分类表*****************************************************/
    /**
     * 迁移消息,4.0.0
     */
    public boolean transferDataFor4000() {
        if (db == null) {
            return false;
        }
        if (!isTableExist(TB_NAME_STRANGER_MSG_LIST_T)) {
            createStrangerMsgListTable();
        }
        final MsgListBean msgListBean = getChatList();
        final int length = msgListBean.msgWithUnreadBeans.size();
        for (MsgWithUnreadBean bean : msgListBean.msgWithUnreadBeans) {
            if (bean.msgBean == null || TextUtils.isEmpty(bean.msgBean.userId)) {
                continue;
            }
            if (WINK_TABLENAME.equals(bean.msgBean.userId)) {//挤眼消息删除
                //从未读消息里面删除
                deleteChatByUserId(bean.msgBean.userId);
            } else {
                if (!MessagesUtils.isRelationFriend(bean.msgBean.userId)) {//如果不是好友
                    //移动到陌生人对话里面
                    insertNewStrangerChat(bean.msgBean, false, bean.unReadCount);
                    //从未读消息里面删除
                    deleteChatByUserId(bean.msgBean.userId);
                    //最好方式为clone
                    /**最好的方式为clone，更新为陌生人消息**/
                    //  bean.msgBean.userId = STRANGER_MSG_USER_ID;//更新为陌生人消息
                    changeMsgToStrangerType(bean.msgBean);
                    insertNewChat(bean.msgBean, false, 0);
                }
            }
        }
        return true;
    }

    /**
     * 插入或更新一条新的对话
     *
     * @param msgBean
     * @param isRequest 是不是密友请求，密友请求要特殊处理
     * @param count     如果是密友请求的话，此属性生效，作为未读消息数
     */
    public void insertNewStrangerChat(MsgBean msgBean, boolean isRequest, int count) {
        // 判断表是否存在
        if (!isTableExist(TB_NAME_STRANGER_MSG_LIST_T)) {
            createStrangerMsgListTable(); // 建表
        }
        if (TextUtils.isEmpty(msgBean.packetId) && !isRequest) {// 当普通消息（包括系统消息）的packetId为空时，说明消息已经被删光了，则删除对话记录。但密友请求消息的packetId是空的
            deleteStrangerChatByUserId(msgBean.userId);
            return;
        }
        Cursor cursor = null;
        try {
            cursor = db.rawQuery("select * from " + TB_NAME_STRANGER_MSG_LIST + " where " + F_USERID + " = ?", new String[]{
                    msgBean.userId});
            MsgWithUnreadBean msgListItemBean = new MsgWithUnreadBean();
            msgListItemBean.msgBean = msgBean;
            if (isRequest)
                msgListItemBean.unReadCount = count;
            else
                msgListItemBean.unReadCount = getCountUnread(msgBean.userId);
            if (cursor.moveToNext()) {// 已存在对话，则刷新
                db.update(TB_NAME_STRANGER_MSG_LIST, msgListItemBean.getContentValues(), F_USERID + " = ?", new String[]{
                        msgBean.userId});
            } else {// 不存在对话，则新建一条
                db.insert(TB_NAME_STRANGER_MSG_LIST, null, msgListItemBean.getContentValues());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDataBase();
            if (null != cursor)
                cursor.close();
        }
    }

    /**
     * 获取某个对话的未读消息数
     *
     * @param userId
     * @return
     */
    public int getChatStrangerUnreadCount(String userId) {
        Cursor cursor = null;
        int count = 0;
        String sql = "select " + F_MSG_UNREAD_COUNT + " from " + TB_NAME_STRANGER_MSG_LIST + " where " + F_USERID + " = '" + userId + "'";
        try {
            cursor = db.rawQuery(sql, null);
            if (cursor.moveToNext()) {
                count = cursor.getInt(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != cursor)
                cursor.close();
        }
        return count;
    }

    /**
     * 获取某个对话最新一条消息的时间
     *
     * @param userId
     * @return
     */
    public MsgBean getChatStrangerLastMsg(String userId) {
        Cursor cursor = null;
        MsgBean msgBean = null;
        String sql = "select * from " + TB_NAME_STRANGER_MSG_LIST + " where " + F_USERID + " = '" + userId + "'";
        try {
            cursor = db.rawQuery(sql, null);
            if (cursor.moveToNext()) {
                msgBean = new MsgBean();
                msgBean.fromCursor(cursor, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != cursor)
                cursor.close();
        }
        return msgBean;
    }

    /**
     * 删除消息列表中的一个对话
     *
     * @param userId
     */
    public void deleteStrangerChatByUserId(String userId) {
        try {
            db.delete(TB_NAME_STRANGER_MSG_LIST, F_USERID + " = ?", new String[]{userId});
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDataBase();
        }
    }

    /**
     * 获取对话的指定时间（-1表示未置顶）
     *
     * @param userId
     * @return
     */
    public long getChatStrangerStickyTopTime(String userId) {
        Cursor cursor = null;
        long stickyTopTime = -1;
        String sql = "select " + F_MSG_STICKY_TOP + " from " + TB_NAME_STRANGER_MSG_LIST + " where " + F_USERID + " = '" + userId + "'";
        try {
            cursor = db.rawQuery(sql, null);
            if (cursor.moveToNext()) {
                stickyTopTime = cursor.getInt(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != cursor)
                cursor.close();
        }
        return stickyTopTime;
    }

    /**
     * 更新陌生人消息
     *
     * @param userId
     * @param msgBean
     */
    public void updateChatStrangerByUserId(String userId, MsgBean msgBean) {
        insertNewStrangerChat(getLastMsg("table_" + userId), false, 0);// 刷新该对话在消息列表数据
        //同时刷新陌生人消息数据
        final MsgBean msgBean1 = msgBean.clone();
        if (msgBean1 != null) {
            changeMsgToStrangerType(msgBean1);
            // msgBean.userId = STRANGER_MSG_USER_ID;
            insertNewChat(msgBean1, false, 0);
        } else {
            changeMsgToStrangerType(msgBean);
            // msgBean.userId = STRANGER_MSG_USER_ID;
            insertNewChat(msgBean, false, 0);
        }
    }

    /**
     * 把消息转换为陌生人消息
     *
     * @param msgBean
     */
    private void changeMsgToStrangerType(MsgBean msgBean) {
        msgBean.userId = STRANGER_MSG_USER_ID;
        msgBean.isSystem = MsgBean.IS_STRANGER_MSG;
    }


    /**
     * 把聊天对象放到朋友列表
     *
     * @param userId
     */
    public void putUserMsgIntoFriend(String userId) {//把表放入到好友里面
        if (TextUtils.isEmpty(userId)) {
            return;
        }
        if (!isTableExist(userId)) {//如果没有这个聊天记录表，就返回
            return;
        }
        Cursor cursor = null;
        String sql = "select * from " + TB_NAME_STRANGER_MSG_LIST + " where " + F_USERID + " = '" + userId + "'";

        try {
            cursor = db.rawQuery(sql, null);
            MsgBean bean = null;
            if (cursor.moveToNext()) {//如果已经存在对话
                bean = new MsgBean();
                bean.fromCursor(cursor, false);
                final int count = cursor.getInt(cursor.getColumnIndex(F_MSG_UNREAD_COUNT));
                insertNewChat(bean, false, count);//插入到好友消息列表
                deleteStrangerChatByUserId(userId);//从陌生人里面删除

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    /**
     * 把聊天对象放到陌生人列表
     *
     * @param userId
     */
    public void putUserMsgIntoStranger(String userId) {//把表放入到陌生人里面
        if (TextUtils.isEmpty(userId)) {
            return;
        }
        if (!isTableExist(userId)) {//如果没有这个聊天记录表，就返回
            return;
        }
        if (MessagesUtils.isRelationFriend(userId)) {//如果依旧是好友，就返回（防止误操作）
            return;
        }
        Cursor cursor = null;
        String sql = "select * from " + TB_NAME_MSG_LIST + " where " + F_USERID + " = '" + userId + "'";

        try {
            cursor = db.rawQuery(sql, null);
            MsgBean bean = null;
            if (cursor.moveToNext()) {//如果已经存在对话
                bean = new MsgBean();
                bean.fromCursor(cursor, false);
                final int count = cursor.getInt(cursor.getColumnIndex(F_MSG_UNREAD_COUNT));
                insertNewStrangerChat(bean, false, count);//插入到陌生人消息里面
                deleteChatByUserId(userId);//从好友消息里面删除
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    /**
     * 失误导致，只test用一次
     */
    public void changeStrangeMsgType() {
        MsgListBean listBean = getChatList();
        for (MsgWithUnreadBean msgWithUnreadBean : listBean.msgWithUnreadBeans) {
            if (STRANGER_MSG_USER_ID.equals(msgWithUnreadBean.msgBean.userId)) {

                changeMsgToStrangerType(msgWithUnreadBean.msgBean);
                // msgBean.userId = STRANGER_MSG_USER_ID;
                insertNewChat(msgWithUnreadBean.msgBean, false, 0);
            }
        }
    }

    /**
     * 获取陌生人对话列表
     *
     * @return
     */
    public MsgListBean getStrangerChatList() {
        if (!isTableExist(TB_NAME_STRANGER_MSG_LIST_T)) {
            createMsgListTable(); // 建表
        }

        MsgListBean msgListBean = new MsgListBean();
        Cursor cursor = null;
        try {
            cursor = db.rawQuery("select * from " + TB_NAME_STRANGER_MSG_LIST + " order by " + F_MSG_STICKY_TOP + " desc, " + F_MSG_TIME + " desc", null);
            while (cursor.moveToNext()) {
                MsgWithUnreadBean tempbean = new MsgWithUnreadBean();
                tempbean.fromCursor(cursor);
                // 如果密友请求对话的msgText不是show，则不显示入口
                if (MsgBean.MSG_TYPE_REQUEST.equals(tempbean.msgBean.msgType) && !MsgBean.SHOW_CIRCLE_REQUEST_CHAT.equals(tempbean.msgBean.msgText))
                    continue;
                msgListBean.msgWithUnreadBeans.add(tempbean);
                msgListBean.unreadTotal += tempbean.unReadCount;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != cursor) {
                cursor.close();
            }
            closeDataBase();
        }
        return msgListBean;
    }

    /**
     * 刷新好友列表，把所有的陌生人过滤一遍，发现是好友的放到好友里面
     *
     * @param listTableChangedListener
     */
    public void refreshMsgList(FriendListContract.FriendListTableChangedListener listTableChangedListener) {
        final MsgListBean msgListBean = getStrangerChatList();
        L.d("refreshMsgList", "msgListBean :" + msgListBean);
        if (msgListBean == null || msgListBean.msgWithUnreadBeans == null) {
            return;
        }
        final List<MsgWithUnreadBean> list = msgListBean.msgWithUnreadBeans;
        boolean haveChanged = false;
        for (MsgWithUnreadBean msgWithUnreadBean : list) {
            final String userId = msgWithUnreadBean.msgBean.userId;
            if (MessagesUtils.isRelationFriend(userId)) {
                L.d("refreshMsgList", "msgWithUnreadBean :" + msgWithUnreadBean);
                putUserMsgIntoFriend(userId);
                haveChanged = true;
            }
        }

        if (haveChanged) {
            FollowStatusChangedImpl.sendRelationChangedBroadCast();
            if (listTableChangedListener != null) {
                listTableChangedListener.onFriendListTableChanged();
            }
        }
    }

    /**
     * 判断好友是否变成陌生人
     */
    public void refreshMsgList() {
        final MsgListBean msgListBean = getChatList();
        if (msgListBean == null || msgListBean.msgWithUnreadBeans == null) {
            return;
        }
        final List<MsgWithUnreadBean> list = msgListBean.msgWithUnreadBeans;
        for (MsgWithUnreadBean msgWithUnreadBean : list) {
            final String userId = msgWithUnreadBean.msgBean.userId;
            if (!MessagesUtils.isRelationFriend(userId)) {
                putUserMsgIntoStranger(userId);
            }
        }

    }


    /**
     * 获取陌生人列表信息的行数
     *
     * @return
     */
    public int getStrangTableCount() {
        int count = 0;
        if (!isTableExist(TB_NAME_STRANGER_MSG_LIST_T)) {
            return 0;
        }
        Cursor cursor = null;
        try {
            cursor = db.rawQuery("select count(*) from " + TB_NAME_STRANGER_MSG_LIST, null);
            if (cursor.moveToNext()) {
                count = (int) cursor.getLong(0);
                L.d("refresh", "strangerCount:count=" + count + ",columnCount=" + cursor.getColumnCount());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != cursor) {
                cursor.close();
            }
            closeDataBase();
        }
        return count;
    }

    /**
     * 获取陌生人未读消息总数
     *
     * @return
     */
    public int getStrangerUnReadMsgCount() {
        int count = 0;
        if (!isTableExist(TB_NAME_STRANGER_MSG_LIST_T)) {
            return 0;
        }
        Cursor cursor = null;
        try {
            cursor = db.rawQuery("select sum(" + F_MSG_UNREAD_COUNT + ") from " + TB_NAME_STRANGER_MSG_LIST, null);
            while (cursor.moveToNext()) {
                count = cursor.getInt(0);
                L.d("refresh", "strangerUnReadMsgCount=" + count);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != cursor) {
                cursor.close();
            }
            closeDataBase();
        }
        return count;
    }

    /**
     * 陌生人消息里面是否有最新可以回挤的消息
     *
     * @return
     */
    public boolean haveStrangerWink() {
        if (!isTableExist(TB_NAME_STRANGER_MSG_LIST_T)) {
            return false;
        }
        Cursor cursor = null;
        try {
            final String sql = "select " + F_USERID + " from " + TB_NAME_STRANGER_MSG_LIST + " where " + F_MSG_TYPE + " = '" + MsgBean.MSG_TYPE_WINK + "' and " + F_MSG_DIRECTION + " = '" + MsgBean.MSG_DIRECTION_TYPE_INCOME + "'";
            cursor = db.rawQuery(sql, null);
            while (cursor.moveToNext()) {
                final String userId = cursor.getString(0);
                if (!WinkUtils.isTodayWinkedUser(userId)) {
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != cursor) {
                cursor.close();
            }
            closeDataBase();
        }
        return false;
    }

    /**
     * 获取陌生人未读消息总数
     *
     * @return
     */
    public int getStragnerUnReadMsgCount() {
        int count = 0;
        if (!isTableExist(TB_NAME_STRANGER_MSG_LIST_T)) {
            return 0;
        }
        Cursor cursor = null;
        try {
            cursor = db.rawQuery("select sum(" + F_MSG_UNREAD_COUNT + ") from " + TB_NAME_STRANGER_MSG_LIST, null);
            while (cursor.moveToNext()) {
                count = cursor.getInt(0);
                L.d("refresh", "strangerUnReadMsgCount=" + count);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != cursor) {
                cursor.close();
            }
            closeDataBase();
        }
        return count;
    }
}
