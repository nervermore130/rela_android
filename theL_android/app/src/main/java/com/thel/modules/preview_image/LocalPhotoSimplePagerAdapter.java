package com.thel.modules.preview_image;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import android.media.MediaPlayer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.VideoView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.select_image.ImageBean;

import java.io.File;
import java.util.ArrayList;

public class LocalPhotoSimplePagerAdapter extends PagerAdapter {

    private String dirPath;
    private ArrayList<ImageBean> picUrls = new ArrayList<>();

    private LayoutInflater mInflater;

    private int mCurrentPos;

    public LocalPhotoSimplePagerAdapter(String dirPath, ArrayList<ImageBean> picUrls) {
        mInflater = (LayoutInflater) TheLApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.dirPath = dirPath;
        this.picUrls = picUrls;
    }

    public LocalPhotoSimplePagerAdapter(ArrayList<ImageBean> picUrls) {
        mInflater = (LayoutInflater) TheLApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.dirPath = null;
        this.picUrls = picUrls;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return picUrls.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void setPrimaryItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        super.setPrimaryItem(container, position, object);
        mCurrentPos = position;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View convertView;
        convertView = mInflater.inflate(R.layout.local_photo_pager_item, container, false);
        uk.co.senab.photoview.PhotoView imgProduct = convertView.findViewById(R.id.img_thumb);
        VideoView video_preview = convertView.findViewById(R.id.video_preview);
        String picUrl = picUrls.get(position).imageName;
        if (picUrl.endsWith(".mp4")) {
            video_preview.setVisibility(View.VISIBLE);
            imgProduct.setVisibility(View.GONE);
            video_preview.setVideoPath(picUrl);
            video_preview.start();
            video_preview.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
                    mediaPlayer.setVolume(0f,0f);//静音
                    mediaPlayer.start();
                    mediaPlayer.setLooping(true);
                }
            });
        } else {
            video_preview.setVisibility(View.GONE);
            imgProduct.setVisibility(View.VISIBLE);
            if (dirPath != null) {
                ImageLoaderManager.imageLoader(imgProduct, dirPath + File.separator + picUrl);
            } else {
                ImageLoaderManager.imageLoader(imgProduct, picUrl);

            }
        }

        container.addView(convertView, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);

        return convertView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

}
