package com.thel.modules.login.email;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.base.BaseDataBean;
import com.thel.network.LoginInterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


public class EmailForgetActivity extends BaseActivity {

    @BindView(R.id.img_back)
    ImageView back;

    @OnClick(R.id.img_back)
    void back() {
        finish();
    }

    @BindView(R.id.txt_title)
    TextView title;

    @BindView(R.id.forgetPW_email_et)
    EditText et_email;

    @OnClick(R.id.txt_sendemail)
    void send(View view) {
        ViewUtils.preventViewMultipleClick(view, 2000);
        showLoadingNoBack();
        sendFindPasswordEmail();
    }

    private static long timeLimit = 60 * 1000 * 30;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_forget);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_MASK_ADJUST | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ButterKnife.bind(this);

        initToolbar();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(this.getClass().getName());
        MobclickAgent.onResume(this);
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(this.getClass().getName());
        MobclickAgent.onPause(this);
    }

    private void initToolbar() {
        back.setImageResource(R.mipmap.title_back);
        title.setText(getString(R.string.login_activity_forget_password));
    }

    private void sendFindPasswordEmail() {
        final String email = et_email.getText().toString().trim();

        if (!isEmail(email)) {
            Toast.makeText(this, getString(R.string.register_activity_wrong_email), Toast.LENGTH_SHORT).show();
            closeLoading();
            return;
        }

        // 判断30分钟内此email是否已经执行过找密码的操作
        long time = ShareFileUtils.getLong(email, 0);

        if (System.currentTimeMillis() - time < timeLimit) {
            Toast.makeText(this, getString(R.string.forgetpassword_activity_tip), Toast.LENGTH_SHORT).show();
            closeLoading();
            return;
        }

        RequestBusiness.getInstance().findBackPassword(email)
                .onBackpressureDrop().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new LoginInterceptorSubscribe<BaseDataBean>() {
                    @Override
                    public void onNext(BaseDataBean data) {
                        super.onNext(data);
                        // 将此邮箱最近一次执行找回密码操作的时间记录下来
                        ShareFileUtils.setLong(email, System.currentTimeMillis());
                        closeLoading();
                        Toast.makeText(EmailForgetActivity.this, getString(R.string.forgetpassword_activity_success), Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private boolean isEmail(String str) {
        return !TextUtils.isEmpty(str)
                && str.matches("^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$");
    }
}
