package com.thel.modules.main.me.adapter;

import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseDataBean;
import com.thel.bean.user.NearUserBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.BridgeUtils;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.flutter.bridge.RfSBridgeHandlerFactory;
import com.thel.growingio.GrowingIoConstant;
import com.thel.modules.live.surface.watch.LiveWatchActivity;
import com.thel.modules.main.me.bean.FriendsBean;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.service.DefaultRequestService;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;
import com.thel.utils.BusinessUtils;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.MD5Utils;
import com.umeng.analytics.MobclickAgent;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class HostFollowListAdapter extends BaseRecyclerViewAdapter<FriendsBean> {


    public HostFollowListAdapter(List<FriendsBean> data) {
        super(R.layout.follow_anchors_list_item, data);
    }

    @Override
    protected void convert(final BaseViewHolder helper, final FriendsBean friendsBean) {

        if (friendsBean.level > 0) {
            helper.setVisibility(R.id.img_vip, VISIBLE);
            switch (friendsBean.level) {
                case 1:
                    helper.setImageResource(R.id.img_vip, R.mipmap.icn_vip_1);
                    break;
                case 2:
                    helper.setImageResource(R.id.img_vip, R.mipmap.icn_vip_2);

                    break;
                case 3:
                    helper.setImageResource(R.id.img_vip, R.mipmap.icn_vip_3);

                    break;
                case 4:
                    helper.setImageResource(R.id.img_vip, R.mipmap.icn_vip_4);

                    break;
            }
        } else {
            helper.setVisibility(R.id.img_vip, GONE);

        }
        // 头像
        helper.setImageUrl(R.id.img_thumb, friendsBean.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE);

        // 用户名
        helper.setText(R.id.txt_name, friendsBean.nickName);
        TextView followView = helper.getView(R.id.statue_view);
        String text = "";

        if (friendsBean.liveInfo != null && friendsBean.liveInfo.live > 0) {
            helper.setVisibility(R.id.lin_live, View.VISIBLE);
            text = TheLApp.getContext().getString(R.string.watch_live);
            followView.setBackgroundResource(R.drawable.follow_selector);
            followView.setTextColor(TheLApp.getContext().getResources().getColor(R.color.text_selector));

            helper.setText(R.id.txt_desc, friendsBean.liveInfo.discription);
            helper.setText(R.id.txt_on_live_looker_num, friendsBean.liveInfo.looker + "");
            helper.setVisibility(R.id.img_on_live, VISIBLE);
            if (NearUserBean.LIVE_TYPE_VOICE.equals(friendsBean.liveInfo.type)) {//声音直播
                helper.setImageResource(R.id.img_on_live, R.mipmap.icon_voice_live);
            } else {
                helper.setImageResource(R.id.img_on_live, R.mipmap.icon_camera_live);
            }
        } else {
            helper.setVisibility(R.id.lin_live, View.GONE);

            if (!friendsBean.awaitStatus) {
                text = TheLApp.getContext().getString(R.string.expect_live);
                followView.setBackgroundResource(R.drawable.follow_unselector);
                followView.setTextColor(TheLApp.getContext().getResources().getColor(R.color.text_unselector));

            } else {
                text = TheLApp.getContext().getString(R.string.expected);
                followView.setBackgroundResource(R.drawable.excepted_selector);
                followView.setTextColor(TheLApp.getContext().getResources().getColor(R.color.white_alpha_40));

            }
            helper.setVisibility(R.id.img_on_live, GONE);

            helper.setText(R.id.txt_desc, friendsBean.intro);

        }
        followView.setText(text);

        followView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (friendsBean.liveInfo != null && friendsBean.liveInfo.live > 0) {
                    if (BusinessUtils.canIntoLiveRoom(TheLApp.getContext(), friendsBean.userId)) {
                        MobclickAgent.onEvent(TheLApp.getContext(), "enter_live_room_from_nearby");
                        Intent intent = new Intent(mContext, LiveWatchActivity.class);

                        intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, friendsBean.userId);
                        intent.putExtra(LiveWatchActivity.FROM_PAGE, LiveWatchActivity.FROM_PAGE_MOMENT);
                        mContext.startActivity(intent);
                        GrowingIOUtil.postWatchLiveEntry(GrowingIoConstant.G_LiveEntry_Followed_Host_List);

                    }
                } else if (friendsBean != null && !friendsBean.awaitStatus) {

                    Map<String, String> map = new HashMap<>();

                    map.put("userId", friendsBean.userId);

                    Flowable<BaseDataBean> flowable = DefaultRequestService.createUserRequestService().lookingForwardToLive(MD5Utils.generateSignatureForMap(map));
                    flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<>());
                    friendsBean.awaitStatus = true;
                    followView.setBackgroundResource(R.drawable.excepted_selector);
                    followView.setTextColor(TheLApp.getContext().getResources().getColor(R.color.white_alpha_40));
                    followView.setText(TheLApp.context.getString(R.string.expected));

                }

            }
        });

        if (mOnItemLongClickListener != null) {
            helper.setOnLongClickListener(R.id.item, new View.OnLongClickListener() {

                @Override
                public boolean onLongClick(View v) {
                    int position = helper.getLayoutPosition();
                    mOnItemLongClickListener.onItemLongClick(v, position, friendsBean);

                    return true;
                }
            });
        }

        helper.convertView.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                FlutterRouterConfig.Companion.gotoUserInfo(friendsBean.userId + "");
            }
        });
    }


    private OnItemLongClickListener mOnItemLongClickListener;

    public void setOnItemLongClickListener(OnItemLongClickListener mOnItemLongClickListener) {
        this.mOnItemLongClickListener = mOnItemLongClickListener;
    }

    public interface OnItemLongClickListener {
        void onItemLongClick(View view, int position, FriendsBean item);
    }
}
