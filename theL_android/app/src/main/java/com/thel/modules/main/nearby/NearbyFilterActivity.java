package com.thel.modules.main.nearby;

import android.content.Intent;
import android.os.Bundle;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.growingio.android.sdk.collection.GrowingIO;
import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.bean.user.NearUserBean;
import com.thel.bean.user.NearUserListNetBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.modules.main.me.bean.LogInfoBean;
import com.thel.modules.main.me.match.eventcollect.collect.LiveLogUtils;
import com.thel.modules.main.nearby.Utils.NearbyUtils;
import com.thel.modules.main.nearby.adapter.NearbyUserRecyclerViewAdapter;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestConstants;
import com.thel.network.service.DefaultRequestService;
import com.thel.ui.widget.MySwipeRefreshLayout;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.ViewUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class NearbyFilterActivity extends BaseActivity {

    private String pageId;

    @OnClick(R.id.lin_back)
    void lin_back() {
        finish();
    }

    @BindView(R.id.lin_more)
    LinearLayout lin_more;

    @BindView(R.id.swipe_container)
    MySwipeRefreshLayout swipe_container;

    @BindView(R.id.recyclerview)
    RecyclerView mRecyclerView;

    @BindView(R.id.empty_view)
    LinearLayout empty_view;

    @BindView(R.id.empty_image)
    ImageView empty_image;

    @BindView(R.id.empty_txt)
    TextView empty_txt;

    @BindView(R.id.txt_title)
    TextView txt_title;

    @OnClick(R.id.lin_more)
    void filter(View anchorView) {
        if (nearbyFilterFragment != null && nearbyFilterFragment.isAdded()) {
            removeFilterFragment();
        } else {
            addFilterFragment();
        }
    }

    private NearbyUserRecyclerViewAdapter mAdapter;

    private String wantRole = "";
    private String age = "";
    private String affection = "";
    private String active = "0";
    private String vip = "0";
    private String have_photo = "0";
    private String horoscope = "";

    private int curPage = 1;

    private boolean haveNextPage = true;

    private ArrayList<NearUserBean> list = new ArrayList<>();

    public static String FROM_TYPE = "from_type";
    public static int FROM_TYPE_WANT_ROLE = 0;
    public static int FROM_TYPE_AGE = 1;
    public static int FROM_TYPE_SINGLE = 2;
    public static int FROM_TYPE_FILTER = 3;
    private int fromType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nearby_filter);
        ButterKnife.bind(this);
        initRecyclerView();

        Intent intent = getIntent();
        fromType = intent.getIntExtra(FROM_TYPE, FROM_TYPE_FILTER);
        pageId = intent.getStringExtra("pageId");
        L.d("NearbyFilter", " fromType : " + fromType);

        if (fromType == FROM_TYPE_WANT_ROLE) {
            wantRole = ShareFileUtils.getString(ShareFileUtils.WANT_ROLE, "");
            if (wantRole.contains("2")) {//p
                wantRole = "2";
                txt_title.setText(getString(R.string.filter_p));
            } else if (wantRole.contains("1")) {//t
                wantRole = "1";
                txt_title.setText(getString(R.string.filter_t));
            } else if (wantRole.contains("3")) {//h
                wantRole = "3";
                txt_title.setText(getString(R.string.filter_h));
            } else {
                wantRole = "2";
                txt_title.setText(getString(R.string.filter_p));
            }
            swipe_container.setRefreshing(true);
            getNearbyPeopleFilterData(true);
        } else if (fromType == FROM_TYPE_AGE) {
            int currentAge = ShareFileUtils.getInt(ShareFileUtils.USER_AGE, 0);
            if (currentAge == 0) {
                txt_title.setText(R.string.nine_five_after);
            } else {
                txt_title.setText(R.string.contemporary);
            }
            age = getStartAge(currentAge) + "," + getEndAge(currentAge);
            swipe_container.setRefreshing(true);
            getNearbyPeopleFilterData(true);
        } else if (fromType == FROM_TYPE_SINGLE) {
            txt_title.setText(R.string.single_like);
            affection = "1";
            swipe_container.setRefreshing(true);
            getNearbyPeopleFilterData(true);
        } else {
            wantRole = ShareFileUtils.getString(RequestConstants.HER_ROLE_NAME, wantRole);
            age = ShareFileUtils.getString(RequestConstants.AGE, age);

            affection = ShareFileUtils.getString(RequestConstants.AFFECTION, affection);

            active = ShareFileUtils.getString(RequestConstants.ACTIVE, active);

            vip = ShareFileUtils.getString(RequestConstants.VIP, vip);

            have_photo = ShareFileUtils.getString(RequestConstants.HAVE_PHOTO, have_photo);

            horoscope = ShareFileUtils.getString(RequestConstants.HOROSCOPE, horoscope);

            lin_more.setVisibility(View.VISIBLE);
            lin_more.postDelayed(new Runnable() {
                @Override
                public void run() {
                    lin_more.performClick();
                }
            }, 300);
        }

    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private NearbyFilterFragment nearbyFilterFragment;

    private void addFilterFragment() {

        nearbyFilterFragment = new NearbyFilterFragment();
        Bundle bundle = new Bundle();
        bundle.putString(RequestConstants.HER_ROLE_NAME, wantRole);
        bundle.putString(RequestConstants.AGE, age);
        bundle.putString(RequestConstants.AFFECTION, affection);
        bundle.putString(RequestConstants.ACTIVE, active);
        bundle.putString(RequestConstants.VIP, vip);
        bundle.putString(RequestConstants.HAVE_PHOTO, have_photo);
        bundle.putString(RequestConstants.HOROSCOPE, horoscope);
        nearbyFilterFragment.setArguments(bundle);

        nearbyFilterFragment.setPageId(pageId);

        nearbyFilterFragment.setOnFilterListener(new NearbyFilterFragment.OnFilterListener() {
            @Override
            public void onFilter(String wantRole, String age, String affection, String active, String vip, String have_photo, String horoscope) {

                removeFilterFragment();

                NearbyFilterActivity.this.wantRole = wantRole;
                NearbyFilterActivity.this.age = age;
                NearbyFilterActivity.this.affection = affection;
                NearbyFilterActivity.this.active = active;
                NearbyFilterActivity.this.vip = vip;
                NearbyFilterActivity.this.have_photo = have_photo;
                NearbyFilterActivity.this.horoscope = horoscope;
                ShareFileUtils.setString(RequestConstants.HER_ROLE_NAME, wantRole);
                ShareFileUtils.setString(RequestConstants.AGE, age);
                ShareFileUtils.setString(RequestConstants.AFFECTION, affection);
                ShareFileUtils.setString(RequestConstants.ACTIVE, active);
                ShareFileUtils.setString(RequestConstants.VIP, vip);
                ShareFileUtils.setString(RequestConstants.HAVE_PHOTO, have_photo);
                ShareFileUtils.setString(RequestConstants.HOROSCOPE, horoscope);

                swipe_container.setRefreshing(true);
                getNearbyPeopleFilterData(true);

            }

            @Override
            public void onDismiss() {
                removeFilterFragment();
            }
        });

        getSupportFragmentManager().beginTransaction().add(R.id.filter_container, nearbyFilterFragment, NearbyFilterFragment.class.getName()).commit();

    }

    private void removeFilterFragment() {

        if (nearbyFilterFragment != null && nearbyFilterFragment.isAdded()) {
            getSupportFragmentManager().beginTransaction().remove(nearbyFilterFragment).commit();
        }

    }

    private void initRecyclerView() {
        ViewUtils.initSwipeRefreshLayout(swipe_container);
        GridLayoutManager manager = new GridLayoutManager(this, 3);
        mRecyclerView.setLayoutManager(manager);
        mAdapter = new NearbyUserRecyclerViewAdapter(list, false);
        mRecyclerView.setAdapter(mAdapter);

        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                curPage = 1;
                getNearbyPeopleFilterData(true);
            }
        });
        mAdapter.setOnLoadMoreListener(new BaseRecyclerViewAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                mRecyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        if (haveNextPage) {
                            getNearbyPeopleFilterData(false);
                        } else {
                            mAdapter.openLoadMore(0, false);
                            if (mAdapter.getData().size() > 0) {
                                View view = getLayoutInflater().inflate(R.layout.load_more_footer_layout, (ViewGroup) mRecyclerView.getParent(), false);
                                ((TextView) view.findViewById(R.id.text)).setText(getString(R.string.info_no_more));
                                mAdapter.addFooterView(view);
                            }
                        }
                    }
                });
            }
        });

        mAdapter.setReloadMoreListener(new BaseRecyclerViewAdapter.ReloadMoreListener() {
            @Override
            public void reloadMore() {
                mRecyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.removeAllFooterView();
                        mAdapter.openLoadMore(true);
                        getNearbyPeopleFilterData(false);
                    }
                });
            }
        });
        mAdapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                gotoUserInfoActivity(position);
            }
        });
    }

    private void getNearbyPeopleFilterData(boolean isRefresh) {
        Map<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_PAGESIZE, "20");
        data.put(RequestConstants.I_CURPAGE, curPage + "");
        data.put(RequestConstants.HER_ROLE_NAME, wantRole);
        data.put(RequestConstants.AGE, age);
        data.put(RequestConstants.AFFECTION, affection);
        data.put(RequestConstants.ACTIVE, active);
        data.put(RequestConstants.VIP, vip);
        data.put(RequestConstants.HAVE_PHOTO, have_photo);
        data.put(RequestConstants.HOROSCOPE, horoscope);
        data.put(RequestConstants.SECRCH, "1");
        L.d("getNearbyPeopleFilterData", data.toString());
        final Flowable<NearUserListNetBean> flowable = DefaultRequestService.createNearbyRequestService().getNearbySimpleList(data);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<NearUserListNetBean>() {

            @Override
            public void onNext(NearUserListNetBean nearUserListNetBean) {
                super.onNext(nearUserListNetBean);
                curPage++;
                if (isRefresh) {
                    showRefreshData(nearUserListNetBean);
                } else {
                    showLoadMoreData(nearUserListNetBean);
                }
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                if (isRefresh) {
                    emptyData(true);
                } else {
                    loadMoreFailed();
                }
            }

            @Override
            public void onComplete() {
                requestFinish();
            }
        });
    }

    private void gotoUserInfoActivity(int position) {
        try {

            final NearUserBean tempUser = mAdapter.getItem(position);
            if (tempUser.itemType == 1) return;
            int tempUserId = tempUser.userId;
//            Intent intent = new Intent();
//            intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, tempUserId + "");
//            intent.setClass(this, UserInfoActivity.class);
//            startActivity(intent);
            FlutterRouterConfig.Companion.gotoUserInfo(tempUserId + "");
            GrowingIO.getInstance().track("NearbyCell");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void showRefreshData(NearUserListNetBean nearUserListNetBean) {
        emptyData(false);
        if (nearUserListNetBean == null || nearUserListNetBean.data == null) {
            emptyData(true);
            return;
        }
        final NearUserListNetBean.NearUserListBean dataBean = nearUserListNetBean.data;
        haveNextPage = dataBean.map_list.size() > 0;
        NearbyUtils.filterBlack(dataBean.map_list);
        NearbyUtils.filterUser(dataBean.map_list);
        list.clear();
        list.addAll(dataBean.map_list);
        mAdapter.removeAllFooterView();
        mAdapter.setNewData(list);
        if (haveNextPage) {
            mAdapter.openLoadMore(list.size(), true);
        } else {
            mAdapter.openLoadMore(list.size(), false);
            if (list.isEmpty()) {
                emptyData(true);
            }
        }
    }

    private void emptyData(boolean empty) {
        empty_txt.setText(R.string.no_filter_result);
        empty_view.setVisibility(empty ? View.VISIBLE : View.GONE);
        empty_image.setVisibility(empty ? View.VISIBLE : View.GONE);
    }

    public void showLoadMoreData(NearUserListNetBean nearUserListNetBean) {
        if (nearUserListNetBean == null || nearUserListNetBean.data == null) {
            return;
        }
        final NearUserListNetBean.NearUserListBean dataBean = nearUserListNetBean.data;
        haveNextPage = dataBean.map_list.size() > 0;
        NearbyUtils.filterUser(dataBean.map_list);
        NearbyUtils.filterBlack(dataBean.map_list);
        NearbyUtils.filterEquallyMoment(list, dataBean.map_list);
        list.addAll(dataBean.map_list);
        mAdapter.notifyDataChangedAfterLoadMore(true, list.size());
    }

    public void requestFinish() {
        if (swipe_container != null)
            swipe_container.postDelayed(new Runnable() {

                @Override
                public void run() {
                    if (swipe_container != null && swipe_container.isRefreshing())
                        swipe_container.setRefreshing(false);
                }
            }, 1000);
    }

    public void loadMoreFailed() {
        mRecyclerView.post(new Runnable() {
            @Override
            public void run() {
                mAdapter.loadMoreFailed((ViewGroup) mRecyclerView.getParent());

            }
        });
    }


    private String getEndAge(int age) {

        if (age == 0) {
            return String.valueOf(24);
        } else {
            int endAge = age + 3;
            if (endAge > 40) {
                return "40+";
            }
            return String.valueOf(age + 3);
        }
    }

    private String getStartAge(int age) {
        if (age == 0) {
            return String.valueOf(18);
        } else {

            int startAge = age - 3;

            if (age - 3 > 18) {
                return String.valueOf(startAge);
            }
            return "18";
        }
    }

}