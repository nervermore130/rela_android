package com.thel.modules.main.userinfo;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.widget.FrameLayout;

import com.thel.R;
import com.thel.base.BaseImageViewerActivity;
import com.thel.constants.TheLConstants;
import com.thel.modules.main.messages.utils.PushUtils;

/**
 * Created by waiarl on 2017/10/11.
 */

public class UserInfoActivity extends BaseImageViewerActivity {
    private String userId;
    private FrameLayout rameLayout;
    private Fragment userInfoFragment;
    private FragmentManager manager;
    private FragmentTransaction tran;
    private String from_type = "";
    public static final String FROM_TYPE_FOLLOW = "from_type_follow";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);
        getIntentData();
        findViewById();
        initFragment();
        setListener();
        getNetData();

    }

    private void getIntentData() {
        try {
            final Intent intent = getIntent();
            String action = intent.getAction();
            if (Intent.ACTION_VIEW.equals(action)) {
                Uri uri = intent.getData();
                if (uri != null) {
                    userId = uri.getQueryParameter("userId");
                }
            } else {
                userId = intent.getStringExtra(TheLConstants.BUNDLE_KEY_USER_ID);
                from_type = intent.getStringExtra(TheLConstants.BUNDLE_KEY_INTENT_FROM);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void findViewById() {
        rameLayout = findViewById(R.id.frame);
    }

    private void initFragment() {
        if (userInfoFragment == null) {
            final Bundle bundle = new Bundle();
            bundle.putString(TheLConstants.BUNDLE_KEY_USER_ID, userId);
            bundle.putString(TheLConstants.BUNDLE_KEY_INTENT_FROM, from_type);
            userInfoFragment = UserInfoFragment.getInstance(bundle);
        }
        manager = getSupportFragmentManager();
        manager.beginTransaction().add(R.id.frame, userInfoFragment).commit();
    }

    private void setListener() {

    }

    private void getNetData() {
    }

    @Override
    public void onBackPressed() {
        if (userInfoFragment != null) {
            if (userInfoFragment instanceof UserInfoBackPressedListener) {
                if (!((UserInfoBackPressedListener) userInfoFragment).onSuperBackPressed()) {//
                    return;
                }
            }
        }
        PushUtils.finish(UserInfoActivity.this);
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (userInfoFragment != null)
            userInfoFragment.onActivityResult(requestCode, resultCode, data);
    }

    public interface UserInfoBackPressedListener {
        boolean onSuperBackPressed();
    }
}
