package com.thel.modules.main.home.moments.comment;

import com.thel.bean.comment.MomentCommentBean;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.service.DefaultRequestService;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by liuyun on 2017/10/23.
 */

public class MomentCommentPresenter implements MomentCommentContract.Presenter {

    private MomentCommentContract.View momentCommentView;

    public MomentCommentPresenter(MomentCommentContract.View momentCommentView) {
        this.momentCommentView = momentCommentView;
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {

    }

    @Override
    public void loadCommentList(String id, String limit, String cursor) {
//        Flowable<MomentCommentBean> flowable = DefaultRequestService.createAllRequestService().getCommentList(id, limit, cursor);
//        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<MomentCommentBean>() {
//
//            @Override
//            public void onNext(MomentCommentBean commentList) {
//
//                if (momentCommentView.isActive() && commentList != null && commentList.data != null && commentList.data.commentList != null) {
//
//                    momentCommentView.updateCommentUI(commentList.data.commentList);
//
//                }
//
//            }
//        });
    }
}
