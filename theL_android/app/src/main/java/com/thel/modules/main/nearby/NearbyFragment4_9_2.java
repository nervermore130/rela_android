package com.thel.modules.main.nearby;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.meituan.android.walle.WalleChannelReader;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseFragment;
import com.thel.callback.imp.PermissionsResultListener;
import com.thel.constants.TheLConstants;
import com.thel.imp.TittleClickListener;
import com.thel.modules.main.nearby.nearbymoment.NearbyMomentFragment;
import com.thel.ui.dialog.ActionSheetDialog;
import com.thel.ui.widget.IndicatorDrawable;
import com.thel.utils.L;
import com.thel.utils.LocationUtils;
import com.thel.utils.PhoneUtils;
import com.thel.utils.ShareFileUtils;
import com.umeng.analytics.MobclickAgent;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class NearbyFragment4_9_2 extends BaseFragment implements PermissionsResultListener {

    private static NearbyFragment4_9_2 instance;

    private TabLayout tablayout;

    private ViewPager viewpager;

    private RelativeLayout screen;

    private NearbyAdapter adapter;

    private List<String> titleList = new ArrayList<>();

    private List<Fragment> fragmentList = new ArrayList<>();

    public static final String TAG = "NearbyFragment4_9_2";
    private int ret;

    public NearbyPeopleFragment nearbyPeopleFragment;
    private TextView open_location;
    private String fineLocationPermissionString;
    private String coarseLocationPermissionString;
    private int PERMISSIONS_REQUEST_READ_LOCATION = 100;
    private Location lastLocation;
    private RelativeLayout rel_default_gps;
    private int GPS_REQUEST_CODE = 10;
    private boolean isOpenGPS = false;
    private static final int PERMISSION_REQUEST_CODE_LOCATION = 1;

    public static NearbyFragment4_9_2 getInstance() {
        instance = new NearbyFragment4_9_2();
        return instance;
    }

    @Override
    public void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(this.getClass().getName());
        //        tryRefreshCurrentFragment();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (!PhoneUtils.isGpsOpen() || ContextCompat.checkSelfPermission(TheLApp.context, fineLocationPermissionString) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(TheLApp.context, coarseLocationPermissionString) != PackageManager.PERMISSION_GRANTED) {
                rel_default_gps.setVisibility(View.VISIBLE);

            } else {
                rel_default_gps.setVisibility(View.GONE);

            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_nearby_fragment4_9_2, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tablayout = view.findViewById(R.id.tablayout);
        viewpager = view.findViewById(R.id.viewpager);
        rel_default_gps = view.findViewById(R.id.rel_default_gps);
        open_location = view.findViewById(R.id.open_location);
        screen = view.findViewById(R.id.screen);
        fineLocationPermissionString = Manifest.permission.ACCESS_FINE_LOCATION;
        coarseLocationPermissionString = Manifest.permission.ACCESS_COARSE_LOCATION;

        if (!PhoneUtils.isGpsOpen() || ContextCompat.checkSelfPermission(TheLApp.context, fineLocationPermissionString) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(TheLApp.context, coarseLocationPermissionString) != PackageManager.PERMISSION_GRANTED) {
            rel_default_gps.setVisibility(View.VISIBLE);

            //   ActivityCompat.requestPermissions(_a, new String[]{fineLocationPermissionString, coarseLocationPermissionString}, perCode);

        } else {
            rel_default_gps.setVisibility(View.GONE);

        }

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        titleList.clear();
        fragmentList.clear();
        initData();
        setTabs();
        setLisener();
    }

    private void setLisener() {
        open_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGPSSettings();

            }
        });
        rel_default_gps.setOnClickListener(null);
        screen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String wantRole = ShareFileUtils.getString(ShareFileUtils.WANT_ROLE, "");
                if (wantRole.contains("2")) {//p
                    wantRole = getString(R.string.filter_p);
                } else if (wantRole.contains("1")) {//t
                    wantRole = getString(R.string.filter_t);
                } else if (wantRole.contains("3")) {//h
                    wantRole = getString(R.string.filter_h);
                } else {
                    wantRole = getString(R.string.filter_p);
                }
                String ages = "";
                int age = ShareFileUtils.getInt(ShareFileUtils.USER_AGE, 0);
                if (age == 0) {
                    ages = getString(R.string.nine_five_after);
                } else {
                    ages = getString(R.string.contemporary);
                }
                String single = getString(R.string.single_like);
                String filter = getString(R.string.nearby_diy_filter_title);
                String[] arrays = {wantRole, ages, single, filter};

                ActionSheetDialog actionSheetDialog = new ActionSheetDialog(getActivity()).builder().setCancelable(true).setCanceledOnTouchOutside(true)
                        .setTitle(getString(R.string.nearby_screen_title));
                for (int i = 0; i < arrays.length; i++) {
                    actionSheetDialog.addSheetItem(arrays[i], ActionSheetDialog.SheetItemColor.BLACK, new ActionSheetDialog.OnSheetItemClickListener() {
                        @Override
                        public void onClick(int which) {
                            Intent intent = new Intent(getActivity(), NearbyFilterActivity.class);
                            switch (which) {
                                case 1:
                                    intent.putExtra(NearbyFilterActivity.FROM_TYPE, NearbyFilterActivity.FROM_TYPE_WANT_ROLE);
                                    break;
                                case 2:
                                    intent.putExtra(NearbyFilterActivity.FROM_TYPE, NearbyFilterActivity.FROM_TYPE_AGE);
                                    break;
                                case 3:
                                    intent.putExtra(NearbyFilterActivity.FROM_TYPE, NearbyFilterActivity.FROM_TYPE_SINGLE);
                                    break;
                                case 4:
                                    intent.putExtra(NearbyFilterActivity.FROM_TYPE, NearbyFilterActivity.FROM_TYPE_FILTER);
                                    break;
                            }
                            startActivity(intent);

                        }
                    });
                }
                actionSheetDialog.show();
            }
        });
    }

    private void initData() {
        String channel = WalleChannelReader.getChannel(TheLApp.context);
        if (channel != null && channel.equals("keke")) {
            titleList.add(getString(R.string.my_nearby_moment));
            titleList.add(getString(R.string.nearby_people));
            screen.setVisibility(View.INVISIBLE);
        } else {
            titleList.add(getString(R.string.nearby_people));
            titleList.add(getString(R.string.my_nearby_moment));
        }

    }

    private void setTabs() {
        Bundle bundle = new Bundle();
        bundle.putString(TheLConstants.BUNDLE_KEY_MOMENT_ID, "");
        bundle.putString("tag", "nearby");
        nearbyPeopleFragment = NearbyPeopleFragment.newInstance(bundle);
        String channel = WalleChannelReader.getChannel(TheLApp.context);
        if (channel != null && channel.equals("keke")) {
            Collections.addAll(fragmentList, NearbyMomentFragment.getInstance(bundle), nearbyPeopleFragment);
        } else {
            Collections.addAll(fragmentList, nearbyPeopleFragment, NearbyMomentFragment.getInstance(bundle));
        }
        adapter = new NearbyAdapter(getChildFragmentManager(), fragmentList, titleList);
        viewpager.setAdapter(adapter);
        viewpager.setOffscreenPageLimit(0);

        final int size = titleList.size();
        View tabStripView = tablayout.getChildAt(0);
        tabStripView.setBackground(new IndicatorDrawable(tabStripView, ContextCompat.getColor(TheLApp.context, R.color.white)));

        for (int i = 0; i < size; i++) {
            final TabLayout.Tab tab = tablayout.newTab();
            setSingleLineTab(tab);
            tablayout.addTab(tab);
        }

        tablayout.setupWithViewPager(viewpager);
        tablayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                String channel = WalleChannelReader.getChannel(TheLApp.context);

                int position;

                if (channel != null) {
                    position = channel.equals("keke") ? 1 : 0;
                    if (position == 1) {
                    }
                } else {
                    position = 0;
                }

                if (tab.getPosition() == position) {
                    screen.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

                String channel = WalleChannelReader.getChannel(TheLApp.context);

                int position;

                if (channel != null) {
                    position = channel.equals("keke") ? 1 : 0;
                } else {
                    position = 0;
                }

                if (tab.getPosition() == position) {
                    screen.setVisibility(View.GONE);
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                final int position = tab.getPosition();
                if (fragmentList.size() > position) {
                    final Fragment fragment = fragmentList.get(position);
                    if (fragment instanceof TittleClickListener) {
                        ((TittleClickListener) fragment).onTitleClick();
                    }
                }

            }
        });
    }

    private void setSingleLineTab(TabLayout.Tab tab) {
        try {
            final Field tabviewField = TabLayout.Tab.class.getDeclaredField("mView");
            tabviewField.setAccessible(true);
            final Object tabviewObj = tabviewField.get(tab);
            final Class tabviewClass = Class.forName("android.support.design.widget.TabLayout$TabView");
            final Field mDefaultMaxLines = tabviewClass.getDeclaredField("mDefaultMaxLines");
            mDefaultMaxLines.setAccessible(true);
            mDefaultMaxLines.set(tabviewObj, 1);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    class NearbyAdapter extends FragmentStatePagerAdapter {

        private final List<Fragment> list;
        private final List<String> titliList;

        public NearbyAdapter(FragmentManager childFragmentManager, List<Fragment> list, List<String> titleList) {
            super(childFragmentManager);
            this.list = list;
            this.titliList = titleList;
        }

        @Override
        public Fragment getItem(int position) {
            return list.get(position);
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (titliList != null) {
                return titliList.get(position % titliList.size());
            }
            return "";
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        //Disconnect the google client api connection.
    }

    @Override
    public void onPermissionGranted() {
        L.i(TAG, "onPermissionGranted");
        if (rel_default_gps != null) {
            rel_default_gps.setVisibility(View.GONE);
        }
    }

    @Override
    public void onPermissionDenied() {
        L.i(TAG, "onPermissionDenied");
        boolean permi = ShareFileUtils.getBoolean(ShareFileUtils.SHOW_REQUEST_PERMISSTION_RATIONALE, false);
        if (!permi) {
            open_location.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showRationaleDialog();

                }
            });
        }

    }

    /**
     * 弹出声明的 Dialog
     */
    private void showRationaleDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(TheLApp.context.getString(R.string.info_note)).setMessage(R.string.needs_to_acquire_location).setPositiveButton(TheLApp.context.getString(R.string.info_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                startAppSetting();

            }
        }).setNegativeButton(TheLApp.context.getString(R.string.info_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();

            }
        }).setCancelable(false).show();
    }

    private void startAppSetting() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, PERMISSIONS_REQUEST_READ_LOCATION);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //注意，这里不需要判断 resultCode == Activity.RESULT_OK ，因为设置页面是不会给我们设置结果的
        //设置
        if (requestCode == PERMISSIONS_REQUEST_READ_LOCATION) {
            L.i(TAG, "Setting Result ：RESULT_OK ");
            if (!PhoneUtils.isGpsOpen() || ContextCompat.checkSelfPermission(TheLApp.context, fineLocationPermissionString) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(TheLApp.context, coarseLocationPermissionString) != PackageManager.PERMISSION_GRANTED) {
                rel_default_gps.setVisibility(View.VISIBLE);

            } else {
                rel_default_gps.setVisibility(View.GONE);

            }
        } else if (requestCode == GPS_REQUEST_CODE) {
            L.i(TAG, "Setting Result ：GPS_REQUEST_CODE ");

            if (!PhoneUtils.isGpsOpen() || ContextCompat.checkSelfPermission(TheLApp.context, fineLocationPermissionString) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(TheLApp.context, coarseLocationPermissionString) != PackageManager.PERMISSION_GRANTED) {
                rel_default_gps.setVisibility(View.VISIBLE);

            } else {
                rel_default_gps.setVisibility(View.GONE);

            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        for (String per : permissions) {
            System.out.println("permissions are  " + per);
            L.d(TAG, per);

        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        List<Fragment> fragments = getChildFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                if (fragment != null) {
                    fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
                }
            }
        }
    }

    public void requestPermission(int perCode, Context _c, Activity _a) {
        if (!PhoneUtils.isGpsOpen()) {
            //没有打开则弹出对话框
            new AlertDialog.Builder(getActivity()).setMessage(R.string.needs_to_acquire_location)

                    .setNegativeButton(R.string.info_no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })

                    .setPositiveButton(R.string.setting_permission, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            Intent intent = new Intent();
                            if (!isOpenGPS) {
                                intent.setAction(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            } else {
                                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                                intent.setData(uri);
                            }
                            getActivity().startActivityForResult(intent, GPS_REQUEST_CODE);
                        }
                    })

                    .setCancelable(false).show();
        } else {
            try {
                performRequestPermissions(TheLApp.context.getString(R.string.needs_to_acquire_location), new String[]{fineLocationPermissionString, coarseLocationPermissionString}, perCode, this);

            } catch (Exception e) {
                e.printStackTrace();
            }


        }

    }

    /**
     * 跳转GPS设置
     */
    private void openGPSSettings() {
        requestPermission(PERMISSION_REQUEST_CODE_LOCATION, TheLApp.context, getActivity());

    }

    private void requestGaodeMapClient() {
        L.i(TAG, "aMapLocation getErrorCode111111");

        final AMapLocationClient mLocationClient = new AMapLocationClient(TheLApp.getContext());
        mLocationClient.setLocationListener(new AMapLocationListener() {
            @Override
            public void onLocationChanged(AMapLocation aMapLocation) {

                if (aMapLocation != null && aMapLocation.getErrorCode() == 0) {
                    L.i(TAG, "aMapLocation getLatitude" + aMapLocation.getLatitude() + "aMapLocation.getLongitude() " + aMapLocation.getLongitude());

                    LocationUtils.saveLocation(aMapLocation.getLatitude(), aMapLocation.getLongitude(), aMapLocation.getCity());
                } else if (aMapLocation != null && aMapLocation.getErrorCode() == 12) { //定位权限没有打开
                    rel_default_gps.setVisibility(View.VISIBLE);
                    LocationUtils.saveLocation(0.0, 0.0, "");

                } else {
                    L.i(TAG, "aMapLocation getErrorCode" + aMapLocation.getErrorCode());

                }

                mLocationClient.stopLocation();
                mLocationClient.onDestroy();
            }
        });
        final AMapLocationClientOption mLocationOption = new AMapLocationClientOption();
        mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Battery_Saving);
        mLocationClient.setLocationOption(mLocationOption);
        mLocationClient.startLocation();
    }

}
