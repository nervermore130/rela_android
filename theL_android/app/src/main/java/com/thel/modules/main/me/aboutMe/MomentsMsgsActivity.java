package com.thel.modules.main.me.aboutMe;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.bean.moments.MomentsMsgBean;
import com.thel.bean.moments.MomentsMsgsListBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.modules.main.me.adapter.MomentsMsgsListAdapter;
import com.thel.modules.main.messages.utils.PushUtils;
import com.thel.modules.main.settings.SettingsActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.ui.widget.DefaultEmptyView;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.decoration.DefaultItemDivider;
import com.thel.utils.DialogUtil;
import com.thel.utils.PhoneUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.ViewUtils;

import java.util.ArrayList;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * moment新消息页面
 * Created by lingwei on 2017/10/17.
 */

public class MomentsMsgsActivity extends BaseActivity {
    private LinearLayout lin_back;
    private TextView txt_title;
    private DefaultEmptyView bg_default;
    private SwipeRefreshLayout swipe_container;
    private RecyclerView mRecyclerView;
    private TextView publish_moment;
    private MomentsMsgsListAdapter mAdapter;
    private int pageSize = 30;
    private int curPage = 1; // 将要请求的页号
    // 刷新类型，全部刷新（即下拉刷新）为1，还是加载下一页为2
    public int refreshType = 0;
    public final int REFRESH_TYPE_ALL = 1;
    public final int REFRESH_TYPE_NEXT_PAGE = 2;

    private ArrayList<MomentsMsgBean> momentsMsgsList = new ArrayList<MomentsMsgBean>();
    private int activityLastResponseCount = -1; // 上次请求返回的数据个数

    private ImageView img_more;
    private LinearLayout lin_more;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.moments_msgs_activity);
//        findViewById();
//        setListener();
//
//        processBusiness();
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void processBusiness() {
        if (swipe_container != null && !swipe_container.isRefreshing()) {
            swipe_container.post(new Runnable() {
                @Override
                public void run() {
                    swipe_container.setRefreshing(true);
                }
            });
        }
        loadNetData(1, REFRESH_TYPE_ALL);

    }

    private void loadNetData(int page, int type) {
        refreshType = type;
        curPage = page;
        loadMomentMsgs(type);

    }

    private void loadMomentMsgs(final int type) {
        if (!(PhoneUtils.getNetWorkType() == PhoneUtils.TYPE_NO)) {
            Flowable<MomentsMsgsListBean> flowable = RequestBusiness.getInstance().getMomentsMsgsList(pageSize, curPage);
            flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<MomentsMsgsListBean>() {
                @Override
                public void onNext(MomentsMsgsListBean momentsMsgsListBean) {
                    super.onNext(momentsMsgsListBean);
                    refreshMsgs(momentsMsgsListBean);
                }

                @Override
                public void onError(Throwable t) {
                    loadError(type);
                }

                @Override
                public void onComplete() {
                    requestFinished();

                }
            });
        } else {
            DialogUtil.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.info_no_network));
            requestFinished();
        }
    }

    private void refreshMsgs(MomentsMsgsListBean momentsMsgsListBean) {
        // TODO: 2017/10/20 过滤黑名店
        if (momentsMsgsListBean == null) {
            return;
        }
        if (refreshType == REFRESH_TYPE_ALL) {
            momentsMsgsList.clear();
        }
        activityLastResponseCount = momentsMsgsListBean.activityList.size();
        momentsMsgsList.addAll(momentsMsgsListBean.activityList);
        if (momentsMsgsList.size() > 0) {
            bg_default.setVisibility(View.GONE);
        } else {
            bg_default.setVisibility(View.VISIBLE);
        }
        if (refreshType == REFRESH_TYPE_ALL) {
            mAdapter.removeAllFooterView();
            mAdapter.setNewData(momentsMsgsList);
            curPage = 2;
            if (momentsMsgsList.size() > 0) {
                mAdapter.openLoadMore(momentsMsgsList.size(), true);
            } else {
                mAdapter.openLoadMore(momentsMsgsList.size(), false);
            }
        } else {
            curPage++;
            mAdapter.notifyDataChangedAfterLoadMore(true, momentsMsgsList.size());
        }
        if (refreshType == REFRESH_TYPE_ALL) {
            if (mRecyclerView.getChildCount() > 0) {// 回到顶部
                mRecyclerView.scrollToPosition(0);
            }
        }
        ShareFileUtils.setInt(ShareFileUtils.UNREAD_MOMENTS_MSG_NUM, 0);
        // 清除消息提醒
        clearMsgRemind();
    }

    private void clearMsgRemind() {
        Intent intent = new Intent();
        intent.setAction(TheLConstants.BROADCAST_CLEAR_MOMENT_MSG_COUNT);
        intent.setAction(TheLConstants.BROADCAST_CLEAR_MOMENT_MSG_COUNT_TOP);
        TheLApp.getContext().sendBroadcast(intent);
    }

    private void findViewById() {
        bg_default = this.findViewById(R.id.bg_default);
        swipe_container = this.findViewById(R.id.swipe_container);
        ViewUtils.initSwipeRefreshLayout(swipe_container);
        mRecyclerView = findViewById(R.id.recyclerView);
        lin_back = this.findViewById(R.id.lin_back);
        txt_title = this.findViewById(R.id.txt_title);
        // publish_moment = (TextView) this.findViewById(R.id.publish_moment);
        txt_title.setText(getString(R.string.dynamic_notification));
        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(RecyclerView.VERTICAL);
        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(null);
        mAdapter = new MomentsMsgsListAdapter(momentsMsgsList);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addItemDecoration(new DefaultItemDivider(TheLApp.getContext(), LinearLayoutManager.VERTICAL, R.color.gray, 1, true, false));

        lin_more = findViewById(R.id.lin_more);
        img_more = findViewById(R.id.img_more);
        img_more.setImageResource(R.mipmap.btn_moments_activity);
    }

    private void setListener() {

        mAdapter.JumpUserinfoListener(new MomentsMsgsListAdapter.JumpUserinfoListener() {
            @Override
            public void jumpUserinfo(int position, View v) {
                int temp = (Integer) v.getTag(R.id.userid_tag);
//                Intent intent = new Intent();
//                intent.setClass(MomentsMsgsActivity.this, UserInfoActivity.class);
//                intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, temp + "");
//                startActivity(intent);
                FlutterRouterConfig.Companion.gotoUserInfo(temp+"");
            }
        });
        mAdapter.JumpMomentTypeListener(new MomentsMsgsListAdapter.JumpMomentTypeListener() {
            @Override
            public void directTo(int position, View v) {
                if (mAdapter.getData() == null) {
                    return;
                }
                MomentsMsgBean momentsMsgBean = mAdapter.getData().get(position);
                Intent intent = new Intent();
                // 日志
                if (v.getTag(R.id.moment_id_tag) != null || v.getTag(R.id.comment_id) != null) {
//                    intent.setClass(MomentsMsgsActivity.this, MomentCommentActivity.class);
//                    intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, momentsMsgBean.momentsId);
//                    startActivity(intent);
                    FlutterRouterConfig.Companion.gotoMomentDetails(momentsMsgBean.momentsId);
                    // 话题
                } else if (v.getTag(R.id.theme_id_tag) != null) {
//                    intent.setClass(MomentsMsgsActivity.this, ThemeDetailActivity.class);
//                    intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, momentsMsgBean.momentsId);
//                    startActivity(intent);
                    FlutterRouterConfig.Companion.gotoThemeDetails(momentsMsgBean.momentsId);
                } else if (v.getTag(R.id.comment_id) != null) {
//                    intent.setClass(MomentsMsgsActivity.this, MomentCommentReplyActivity.class);
//                    intent.putExtra(TheLConstants.BUNDLE_KEY_COMMENT_ID, momentsMsgBean.commentId);
//                    intent.putExtra(TheLConstants.BUNDLE_KEY_INTENT_FROM, MomentCommentReplyActivity.FROM_NOTICE);
//                    startActivity(intent);
                    FlutterRouterConfig.Companion.gotoCommentReply(momentsMsgBean.commentId);
                } else if (v.getTag(R.id.await_user_id) != null) {
//                    intent.setClass(MomentsMsgsActivity.this, UserInfoActivity.class);
//                    intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, String.valueOf(momentsMsgBean.userId));
//                    startActivity(intent);
                    FlutterRouterConfig.Companion.gotoUserInfo(momentsMsgBean.userId+"");
                }
            }
        });
        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                processBusiness();
            }
        });
        mAdapter.setOnLoadMoreListener(new BaseRecyclerViewAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                mRecyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        if (activityLastResponseCount > 0) {
                            loadNetData(curPage, REFRESH_TYPE_NEXT_PAGE);
                        } else {
                            mAdapter.openLoadMore(0, false);
                            if (momentsMsgsList.size() > 0) {
                                View view = getLayoutInflater().inflate(R.layout.load_more_footer_layout, (ViewGroup) mRecyclerView.getParent(), false);
                                ((TextView) view.findViewById(R.id.text)).setText(getString(R.string.info_no_more));
                                mAdapter.addFooterView(view);
                            }
                        }
                    }
                });
            }
        });
        mAdapter.setReloadMoreListener(new BaseRecyclerViewAdapter.ReloadMoreListener() {//加载更多失败后重新加载
            @Override
            public void reloadMore() {
                refreshType = REFRESH_TYPE_NEXT_PAGE;
                mAdapter.removeAllFooterView();
                mAdapter.openLoadMore(true);
                loadNetData(curPage, REFRESH_TYPE_NEXT_PAGE);
            }


        });
        lin_back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                ViewUtils.preventViewMultipleClick(arg0, 2000);
                PushUtils.finish(MomentsMsgsActivity.this);
            }
        });

       /* publish_moment.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                if (noBindPhone()){
                    LoginRegisterActivity.startActivity( MomentsMsgsActivity.this, LoginRegisterActivity.PHONE_VERIFY);

                }else {

                    ReleaseMomentDialog releaseMomentDialog = new ReleaseMomentDialog(MomentsMsgsActivity.this);
                    releaseMomentDialog.show();
                }
            }
        });*/

        lin_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MomentsMsgsActivity.this, SettingsActivity.class);
                intent.putExtra(SettingsActivity.PAGE_TYPE_TAG, SettingsActivity.PAGE_TYPE_PUSH);
                startActivity(intent);
            }
        });
    }

    public static boolean noBindPhone() {
        String cell = ShareFileUtils.getString(ShareFileUtils.BIND_CELL, "");
        return TextUtils.isEmpty(cell) || "null".equals(cell);
    }

    private void requestFinished() {

        swipe_container.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mRecyclerView != null) {
                    if (swipe_container != null && swipe_container.isRefreshing())
                        swipe_container.setRefreshing(false);
                }
            }
        }, 1000);
        closeLoading();
    }

    private void loadError(int type) {
        if (refreshType == type) {
            mRecyclerView.post(new Runnable() {
                @Override
                public void run() {
                    mAdapter.loadMoreFailed((ViewGroup) mRecyclerView.getParent());
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        PushUtils.finish(MomentsMsgsActivity.this);
    }
}
