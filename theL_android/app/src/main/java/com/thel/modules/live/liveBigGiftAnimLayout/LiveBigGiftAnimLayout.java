package com.thel.modules.live.liveBigGiftAnimLayout;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.kingsoft.media.httpcache.KSYProxyService;
import com.ksyun.media.player.IMediaPlayer;
import com.ksyun.media.player.KSYMediaPlayer;
import com.thel.constants.TheLConstants;
import com.thel.modules.live.liveBigGiftAnimLayout.anim.BalloonPathAnimator;
import com.thel.modules.live.liveBigGiftAnimLayout.anim.BaseAnimator;
import com.thel.modules.live.liveBigGiftAnimLayout.anim.BombAnimator;
import com.thel.modules.live.liveBigGiftAnimLayout.anim.BubbleAnimator;
import com.thel.modules.live.liveBigGiftAnimLayout.anim.ChristmasAnimator;
import com.thel.modules.live.liveBigGiftAnimLayout.anim.CoinDropAnimator;
import com.thel.modules.live.liveBigGiftAnimLayout.anim.CrownAnimator;
import com.thel.modules.live.liveBigGiftAnimLayout.anim.FerrisWheelAnimator;
import com.thel.modules.live.liveBigGiftAnimLayout.anim.FirecrackerAnimator;
import com.thel.modules.live.liveBigGiftAnimLayout.anim.FireworkAnimator;
import com.thel.modules.live.liveBigGiftAnimLayout.anim.HugHugAnimator;
import com.thel.modules.live.liveBigGiftAnimLayout.anim.KissAnimator;
import com.thel.modules.live.liveBigGiftAnimLayout.anim.PumpkinAnimator;
import com.thel.modules.live.liveBigGiftAnimLayout.anim.RingAnimator;
import com.thel.modules.live.liveBigGiftAnimLayout.anim.SnowmanAnimator;
import com.thel.modules.live.liveBigGiftAnimLayout.anim.StarShowerAnimator;
import com.thel.modules.live.liveBigGiftAnimLayout.anim.YuanXiaoAnimator;
import com.thel.modules.live.liveBigGiftAnimLayout.video.GiftFilterSurfaceView;
import com.thel.modules.live.liveBigGiftAnimLayout.video.LiveBigAnimVideoView;
import com.thel.utils.L;
import com.thel.utils.ScreenUtils;

import java.io.IOException;

import video.com.relavideolibrary.RelaVideoSDK;
import video.com.relavideolibrary.view.RelaBigGiftView;


public class LiveBigGiftAnimLayout extends RelativeLayout {

    private static final String TAG = "LiveBigGiftAnimLayout";

    private static final int DEFAULT_DURATION = 8000;
    public static int duration = 8000;
    private final Context mContext;
    private BalloonPathAnimator balloonAnimator;
    private StarShowerAnimator starShowerAnimator;
    private CoinDropAnimator coinDropAnimator;
    private CrownAnimator crownAnimator;
    private HugHugAnimator hugHugAnimator;
    private BubbleAnimator bubbleAnimator;
    private SnowmanAnimator snowmanAnimator;
    private PumpkinAnimator pumpkinAnimator;
    private KissAnimator kissAnimator;
    private FerrisWheelAnimator ferrisWheelAnimator;
    private RingAnimator ringAnimator;
    private ChristmasAnimator christmasAnimator;
    private BombAnimator bombAnimator;
    private YuanXiaoAnimator yuanXiaoAnimator;
    private FireworkAnimator fireworkAnimator;
    private FirecrackerAnimator firecrackerAnimator;
    private AeGiftAnimView aeGiftAnimView;
    private LiveBigAnimVideoView videoAnimView;
    private FrameLayout video_frame;
    private RelaBigGiftView liveBigAnimVideoView;
    private VideoAnimListener videoAnimListener;

    public LiveBigGiftAnimLayout(Context context) {
        this(context, null);
    }

    public LiveBigGiftAnimLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LiveBigGiftAnimLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }


    private void init() {

        setBackgroundColor(Color.TRANSPARENT);

        mMediaPlayer = new KSYMediaPlayer.Builder(getContext()).build();

        balloonAnimator = new BalloonPathAnimator(getContext(), DEFAULT_DURATION);

        starShowerAnimator = new StarShowerAnimator(getContext(), DEFAULT_DURATION);

        coinDropAnimator = new CoinDropAnimator(getContext(), DEFAULT_DURATION);

        crownAnimator = new CrownAnimator(getContext(), DEFAULT_DURATION);

        hugHugAnimator = new HugHugAnimator(getContext(), DEFAULT_DURATION);

        bubbleAnimator = new BubbleAnimator(getContext(), 10000);

        snowmanAnimator = new SnowmanAnimator(getContext(), 8000);

        pumpkinAnimator = new PumpkinAnimator(getContext(), 5000);

        kissAnimator = new KissAnimator(getContext(), 4000);

        ferrisWheelAnimator = new FerrisWheelAnimator(getContext(), 10000);

        ringAnimator = new RingAnimator(getContext(), 12000);

        christmasAnimator = new ChristmasAnimator(getContext(), 10000);

        bombAnimator = new BombAnimator(getContext(), 5000);

        yuanXiaoAnimator = new YuanXiaoAnimator(getContext(), 10000);

        fireworkAnimator = new FireworkAnimator(getContext(), 7500);

        firecrackerAnimator = new FirecrackerAnimator(getContext(), 5500);

        aeGiftAnimView = new AeGiftAnimView(getContext(), 5500);

        addFrame();
    }

    private void addFrame() {
        video_frame = new FrameLayout(mContext);


        int width = -1;

        int height = -1;

        addView(video_frame, width, height);

    }

    private void addVideoView(String url) {
        liveBigAnimVideoView = new RelaBigGiftView(mContext, url);
        video_frame.addView(liveBigAnimVideoView, FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
        final FrameLayout.LayoutParams param = (FrameLayout.LayoutParams) liveBigAnimVideoView.getLayoutParams();
        param.gravity = Gravity.CENTER;
        liveBigAnimVideoView.setLayoutParams(param);
    }


    public void clearAnimation() {
        final int count = getChildCount();
        for (int i = 0; i < count; i++) {
            getChildAt(i).clearAnimation();
        }
        for (int i = 0; i < count; i++) {
            final View view = getChildAt(i);
            if (view != video_frame) {
                removeView(view);
            } else {
                //                clearVideoView(videoAnimView);
                videoGiftReset();
            }
        }
    }

    private void clearVideoView(LiveBigAnimVideoView videoAnimView) {
        if (videoAnimView == null) {
            return;
        }
        setVideoAnimVisiable(View.INVISIBLE);
    }

    public void playBalloon() {
        duration = balloonAnimator.start(this);
    }

    public void playStarShower() {
        duration = starShowerAnimator.start(this);
    }

    public void playCoinDrop() {
        duration = coinDropAnimator.start(this);
    }

    public void playCrown() {
        duration = crownAnimator.start(this);
    }

    public void playHugHug() {
        duration = hugHugAnimator.start(this);
    }

    public void playBubble() {
        duration = bubbleAnimator.start(this);
    }

    public void playSnowman() {
        duration = snowmanAnimator.start(this);
    }

    public void playPumpkin() {
        duration = pumpkinAnimator.start(this);
    }

    public void playRing() {
        duration = ringAnimator.start(this);
    }

    public void playFerris() {
        duration = ferrisWheelAnimator.start(this);
    }

    public void playKiss() {
        duration = kissAnimator.start(this);
    }

    public void playChristmas() {
        duration = christmasAnimator.start(this);
    }

    public void playYuanXiao() {
        duration = yuanXiaoAnimator.start(this);
    }

    public void playBomb() {
        duration = bombAnimator.start(this);
    }

    public void playFireWork() {
        duration = fireworkAnimator.start(this);
    }

    public void playFirecracker() {
        duration = firecrackerAnimator.start(this);
    }

    public void playAEanimator(String url, int playTime) {
        setVideoAnimVisiable(View.INVISIBLE);
        duration = aeGiftAnimView.start(this, playTime);
        aeGiftAnimView.loadUrl(url);

        if (duration > 0) {
            postDelayed(endRunnable, duration);
        }
    }

    public void playVideoAnim(String url) {

        setVideoAnimVisiable(View.VISIBLE);
        initMediaPlayer(url);
        playBigGiftVideo();
    }

    /**
     * 是否是本地视频，是本地视频就播放，返回true,否则，返回false;
     *
     * @param mark
     * @return
     */
    public boolean playLocalAnim(String mark) {
        if (TextUtils.isEmpty(mark)) return false;
        if (mark.equals(TheLConstants.BIG_ANIM_BALLOON)) {//气球
            playBalloon();
        } else if (mark.equals(TheLConstants.BIG_ANIM_CROWN)) {//皇冠
            playCrown();
        } else if (mark.equals(TheLConstants.BIG_ANIM_STAR_SHOWER)) {//流星雨
            playStarShower();
        } else if (mark.equals(TheLConstants.BIG_ANIM_COIN_DROP)) {//掉金币
            playCoinDrop();
        } else if (TheLConstants.BIG_ANIM_HUG_HUG.equals(mark)) {//抱抱
            playHugHug();
        } else if (TheLConstants.BIG_ANIM_BUBBLE.equals(mark)) {//泡泡
            playBubble();
        } else if (TheLConstants.BIG_ANIM_SNOWMAN.equals(mark)) {//雪人
            playSnowman();
        } else if (TheLConstants.BIG_ANIM_KISS.equals(mark)) {//么么哒
            playKiss();
        } else if (TheLConstants.BIG_ANIM_FERRIS_WHEEL.equals(mark)) {//摩天轮
            playFerris();
        } else if (TheLConstants.BIG_ANIM_PUMPKIN.equals(mark)) {//囊
            playPumpkin();
        } else if (TheLConstants.BIG_ANIM_RING.equals(mark)) {//戒指
            playRing();
        } else if (TheLConstants.BIG_ANIM_CHRISTMAS.equals(mark)) {//圣诞老人
            playChristmas();
        } else if (TheLConstants.BIG_ANIM_BOMB.equals(mark)) {//炸弹
            playBomb();
        } else if (TheLConstants.BIG_ANIM_RICEBALL.equals(mark)) { //元宵汤圆
            playYuanXiao();
        } else if (TheLConstants.BIG_ANIM_FIREWORK.equals(mark)) { //烟花
            playFireWork();
        } else if (TheLConstants.BIG_ANIM_FIRECRACKER.equals(mark)) { //炮竹
            playFirecracker();
        } else {
            return false;
        }

        if (duration > 0) {
            postDelayed(endRunnable, duration);
        }

        setVideoAnimVisiable(View.INVISIBLE);
        return true;
    }

    private void setVideoAnimVisiable(int visiable) {
        if (videoAnimView != null) {
            videoAnimView.setVisibility(visiable);
        }
    }

    private GiftFilterSurfaceView mGiftFilterSurfaceView;

    private KSYMediaPlayer mMediaPlayer;

    private void playBigGiftVideo() {
        if (video_frame.getVisibility() != View.VISIBLE) {
            video_frame.setVisibility(View.VISIBLE);
        }

        if (video_frame.getChildCount() > 0) {
            video_frame.removeAllViews();
        }

        float screenWidth = ScreenUtils.getScreenWidth(getContext());

        float screenHeight = ScreenUtils.getScreenHeight(getContext());

        float videoScale = 16f / 9f;

        float screenScale = screenHeight / screenWidth;

        float videoWidth = 0;

        float videoHeight = 0;

        if (screenScale > videoScale) {
            videoWidth = screenWidth * (1 + screenScale - videoScale);
            videoHeight = screenHeight;
        } else {
            videoWidth = screenWidth;
            videoHeight = screenHeight;
        }

        mGiftFilterSurfaceView = new GiftFilterSurfaceView(getContext());
        video_frame.addView(mGiftFilterSurfaceView, (int) videoWidth, (int) videoHeight);
        final FrameLayout.LayoutParams param = (FrameLayout.LayoutParams) mGiftFilterSurfaceView.getLayoutParams();
        param.gravity = Gravity.CENTER;
        mGiftFilterSurfaceView.setLayoutParams(param);
        mGiftFilterSurfaceView.setMediaPlayer(mMediaPlayer);
        mGiftFilterSurfaceView.setSourceSize((int) videoWidth, (int) videoHeight);

    }


    private void initMediaPlayer(String url) {

        try {

            L.d("LiveBigGiftAnimLayout", " url : " + url);
            KSYProxyService ksyProxy = RelaVideoSDK.getKSYProxy();
            ksyProxy.startServer();
            String proxyUrl = ksyProxy.getProxyUrl(url);

            L.d("LiveBigGiftAnimLayout", " proxyUrl : " + proxyUrl);

            mMediaPlayer.softReset();
            mMediaPlayer.setDataSource(proxyUrl);
            mMediaPlayer.prepareAsync();
            mMediaPlayer.setOnPreparedListener(new IMediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(IMediaPlayer iMediaPlayer) {

                    L.d("LiveBigGiftAnimLayout", " onPrepared ");

                    mMediaPlayer.start();
                }
            });
            mMediaPlayer.setOnCompletionListener(new IMediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(IMediaPlayer iMediaPlayer) {

                    L.d("LiveBigGiftAnimLayout", " onCompletion ");

                    if (video_frame.getChildCount() > 0) {
                        video_frame.removeAllViews();
                    }
                    if (videoAnimListener != null) {
                        videoAnimListener.complete();
                    }
                }
            });

            mMediaPlayer.setOnErrorListener(new IMediaPlayer.OnErrorListener() {
                @Override public boolean onError(IMediaPlayer mp, int what, int extra) {

                    L.d("LiveBigGiftAnimLayout", " onError what : " + what + " , extra : " + extra);

                    return false;
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void setVideoAnimListener(VideoAnimListener videoAnimListener) {
        this.videoAnimListener = videoAnimListener;
    }

    public void onPause() {
        videoGiftPause();
    }

    public void onResume() {
        videoGiftResume();
    }

    public void onDestory() {
        if (mMediaPlayer != null) {
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }

    private void videoGiftPause() {
        if (mGiftFilterSurfaceView != null) {
            mGiftFilterSurfaceView.onPause();
        }
    }

    private void videoGiftResume() {
        if (mGiftFilterSurfaceView != null) {
            mGiftFilterSurfaceView.onResume();
        }
    }

    private void videoGiftReset() {
        video_frame.setVisibility(View.GONE);
        if (video_frame != null && video_frame.getChildCount() > 0) {
            video_frame.removeAllViews();
        }
        if (mMediaPlayer != null) {
            mMediaPlayer.softReset();
        }
    }

    private Runnable endRunnable = new Runnable() {
        @Override public void run() {
            if (videoAnimListener != null) {
                videoAnimListener.complete();
            }
        }
    };

    @Override protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (endRunnable != null) {
            removeCallbacks(endRunnable);
        }
    }
}