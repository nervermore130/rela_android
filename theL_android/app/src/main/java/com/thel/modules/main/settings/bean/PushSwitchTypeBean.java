package com.thel.modules.main.settings.bean;

import com.thel.base.BaseDataBean;

import java.io.Serializable;

/**
 * Created by test1 on 2017/8/24.
 * push开关列表
 */

public class PushSwitchTypeBean extends BaseDataBean implements Serializable {
    public PushSwitchTypeDataBean data;

    public class PushSwitchTypeDataBean {
        /**
         * 挤眼
         */
        public int wink;
        /**
         * 关注
         */
        public int follower;
        /**
         * 消息
         */
        public int message;
        /**
         * 日志评论
         */
        public int commentText;
        /**
         * 评论回复
         */
        public int commentReply;
        /**
         * 日志点赞
         */
        public int commentWink;
        /**
         * 直播
         */
        public int live;
        /**
         * 视频
         */
        public int video;
        /**
         * 话题
         */
        public int theme;
        public int view;
        /**
         * 超级喜欢
         */
        public int superLike;
        /**
         * 完美速配
         * **/
        public int perfectMatch;

        /**
         * 谁喜欢我
         */
        public int likeMe;

        /**
         * 期待开播
         * */
        public int await;

        /**
         * 小红点开关  谁看过我小红点开关 (1开启 2关闭, 默认开启)
         * */
        public int viewMeRedpoint;
    }

}
