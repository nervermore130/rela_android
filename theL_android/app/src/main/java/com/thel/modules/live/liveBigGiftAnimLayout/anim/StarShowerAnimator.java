package com.thel.modules.live.liveBigGiftAnimLayout.anim;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.Point;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.thel.R;
import com.thel.modules.live.liveBigGiftAnimLayout.LiveBigAnimUtils;
import com.thel.utils.Utils;

import java.util.Random;

/**
 * 流星雨的轨迹动画
 * created by Setsail on 2016.6.2
 */
public class StarShowerAnimator extends BaseAnimator{
    private final String foldPath;
    private Handler mHandler;
    private int duration;
    private int countDown;
    private Random mRandom;
    private Context mContext;
    private int size;
    private int rate = 400;
    //  private int[] imgs = new int[]{R.drawable.icn_animate_starrain_1, R.drawable.icn_animate_starrain_2, R.drawable.icn_animate_starrain_3, R.drawable.icn_animate_starrain_4};
    // private int[] dropRes = new int[]{R.drawable.drop1, R.drawable.drop2, R.drawable.drop3, R.drawable.drop4, R.drawable.drop5, R.drawable.drop6, R.drawable.drop7};
    private String[] imgs = new String[]{"icn_animate_starrain_1", "icn_animate_starrain_2", "icn_animate_starrain_3", "icn_animate_starrain_4"};
    private String[] dropRes = new String[]{"drop1", "drop2", "drop3", "drop4", "drop5", "drop6", "drop7"};
    private String[] stoneRes = new String[]{"falling_stone1", "falling_stone2", "falling_stone3"};
    private int mWidth;
    private int mHeight;
    private final int minDropWidth;
    private final int differDropWidth;
    private final int differRotateCycler;
    private final int differDropCount;
    private final int minDropCount;
    private boolean isPlaying;
    private final int differDropDuration;
    private final int dropDuration;
    private long stone_blue_delay_time;
    private long stone_red_delay_time;

    public StarShowerAnimator(Context context, int d) {
        mContext = context;
        size = Utils.dip2px(mContext, 150);
        duration = d;
        mHandler = new Handler(Looper.getMainLooper());
        mRandom = new Random();
        //最小花瓣大小
        minDropWidth = Utils.dip2px(mContext, 10);
        //碎片宽度变化区间
        differDropWidth = Utils.dip2px(mContext, 10);
        //碎片转动周期变化区间
        differRotateCycler = 500;
        //掉落时间变化区间
        differDropDuration = 2000;
        //碎片变化区间
        differDropCount = 4;
        //每次发射碎片最小数
        minDropCount = 4;
        dropDuration = 500;
        stone_red_delay_time = 150;//红球延迟进入时间
        stone_blue_delay_time = 100;//蓝求延迟进入时间
        foldPath = "anim/starshower";

    }

    public int start(final ViewGroup parent) {
        isPlaying = true;
        mWidth = parent.getMeasuredWidth();
        mHeight = parent.getMeasuredHeight();
        if (mWidth == 0 || mHeight == 0) {
            return 0;
        }

        final RelativeLayout backgound = new RelativeLayout(mContext);
        parent.addView(backgound, RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        setBackground(backgound);
        countDown = duration;
        backgound.post(new Runnable() {
            @Override
            public void run() {
                addOneStar(backgound);
                addOneStar(backgound);
                //                addOneStar(parent);
                if (countDown >= rate) {
                    countDown -= rate;
                    backgound.postDelayed(this, rate);
                }
            }
        });
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                isPlaying = false;
                backgound.clearAnimation();
                parent.removeView(backgound);
            }
        }, duration);
        return duration;
    }

    private void setBackground(RelativeLayout backgound) {
        //添加阴影
        addMask(backgound);
        //添加drop
        addDrop(backgound);
    }

    private void addDrop(final RelativeLayout backgound) {

        backgound.post(new Runnable() {
            @Override
            public void run() {
                int PetalCount = mRandom.nextInt(differDropCount) + minDropCount;
                for (int i = 0; i < PetalCount; i++) {
                    addDropOne(backgound);
                }
                if (isPlaying) {
                    backgound.postDelayed(this, rate);
                }
            }
        });

    }

    private void addDropOne(final RelativeLayout backgound) {
        final ImageView img_Drop = new ImageView(mContext);
        backgound.addView(img_Drop);
        int resIndex = mRandom.nextInt(dropRes.length);
        // int res = dropRes[resIndex];
        int w = minDropWidth + mRandom.nextInt(differDropWidth);
        int h = getImageHeight(resIndex, w);
        //   img_Drop.setImageResource(dropRes[resIndex]);
        LiveBigAnimUtils.setAssetImage(img_Drop, foldPath, dropRes[resIndex]);
        img_Drop.setLayoutParams(new RelativeLayout.LayoutParams(w, h));
        Point startPoint = new Point(mRandom.nextInt(mWidth), 0 - h);
        Point endPoint = new Point(startPoint.x, mHeight);
        Path path = new Path();
        path.moveTo(startPoint.x, startPoint.y);

        path.lineTo(endPoint.x, endPoint.y);
        // Path path = getBallonPath(w, h);
        int cycle = 0;
        if (resIndex == 0 || resIndex == 2) {
            cycle = minDropCount + mRandom.nextInt(differDropCount);
        }
        int dropDuration = differRotateCycler + mRandom.nextInt(differDropDuration);
        FloatAnimation floatAnimation = new FloatAnimation(path, backgound, img_Drop, dropDuration, cycle);
        floatAnimation.setDuration(dropDuration);
        floatAnimation.setInterpolator(new LinearInterpolator());
        floatAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                backgound.post(new Runnable() {
                    @Override
                    public void run() {
                        backgound.removeView(img_Drop);
                    }
                });
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        img_Drop.startAnimation(floatAnimation);
    }

    private int getImageHeight(int resId, int img_width) {
        int img_height;
        switch (resId) {
            case 0:
                img_height = img_width * 575 / 580;
                break;
            case 6:
                img_height = img_width;
                break;
            case 2:
                img_height = img_width * 340 / 296;
                break;
            default:
                img_height = img_width;
                break;
        }
        return img_height;
    }

    private void addMask(RelativeLayout backgound) {
        final View view = LayoutInflater.from(mContext).inflate(R.layout.live_big_anim_star_layout, null);
        final ImageView img_stone_blue = view.findViewById(R.id.img_stone_blue);
        final ImageView img_stone_yellow = view.findViewById(R.id.img_stone_yellow);
        final ImageView img_stone_red = view.findViewById(R.id.img_stone_red);
        LiveBigAnimUtils.setAssetImage(img_stone_blue, foldPath, stoneRes[0]);
        LiveBigAnimUtils.setAssetImage(img_stone_yellow, foldPath, stoneRes[1]);
        LiveBigAnimUtils.setAssetImage(img_stone_red, foldPath, stoneRes[2]);
        img_stone_blue.setVisibility(View.GONE);
        img_stone_yellow.setVisibility(View.GONE);
        img_stone_red.setVisibility(View.GONE);
        backgound.addView(view, RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        translateStone(img_stone_yellow, dropDuration);//最先移动黄球
        mHandler.postDelayed(new Runnable() {//移动蓝球
            @Override
            public void run() {
                translateStone(img_stone_blue, dropDuration);
            }
        }, stone_blue_delay_time);
        mHandler.postDelayed(new Runnable() {//移动红球
            @Override
            public void run() {
                translateStone(img_stone_red, dropDuration);
            }
        }, stone_red_delay_time);


        final int removeTime = duration - dropDuration - 500;//动画结束前0.5秒移除星球
        mHandler.postDelayed(new Runnable() {//同时移动出去
            @Override
            public void run() {
                removeStone(img_stone_blue, dropDuration);
                removeStone(img_stone_yellow, dropDuration);
                removeStone(img_stone_red, dropDuration);
            }
        }, removeTime);
    }

    private void removeStone(ImageView view, int duration) {
        final RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams) view.getLayoutParams();
        final int top_margin = param.topMargin;
        final int y = mHeight / 2 - top_margin;
        ObjectAnimator animator = ObjectAnimator.ofFloat(view, View.TRANSLATION_Y, 0, y);
        animator.setDuration(duration);
        animator.start();
    }

    private void translateStone(final ImageView view, int duration) {
        view.setVisibility(View.VISIBLE);
        final RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams) view.getLayoutParams();
        final int top_margin = param.topMargin;
        final int y = mHeight / 2 - top_margin;
        ObjectAnimator animator = ObjectAnimator.ofFloat(view, View.TRANSLATION_Y, y, 0);
        animator.setDuration(duration);
        animator.start();
    }

    private void addOneStar(final ViewGroup parent) {
        Path p = new Path();
        int type = mRandom.nextInt(2);
        int w;
        int h;
        int speed = parent.getWidth() / 3 * 5 / 1000;
        if (speed == 0)
            return;
        int dur;
        if (type == 0) {
            w = mRandom.nextInt(parent.getWidth());
            p.moveTo(w, 0);
            p.lineTo(0, w * 4 / 3);
            dur = w / 3 * 5 / speed;
        } else {
            h = mRandom.nextInt(parent.getHeight());
            p.moveTo(parent.getWidth(), h);
            p.lineTo(parent.getWidth() - (parent.getHeight() - h) * 3 / 4, parent.getHeight());
            dur = (parent.getHeight() - h) * 5 / 4 / speed;
        }

        if (dur > countDown)// 防止动画超出duration
            return;

        final ImageView child = new ImageView(mContext);
        child.setLayoutParams(new RelativeLayout.LayoutParams(size, ViewGroup.LayoutParams.WRAP_CONTENT));
        // child.setImageResource(imgs[mRandom.nextInt(4)]);
        LiveBigAnimUtils.setAssetImage(child, foldPath, imgs[mRandom.nextInt(4)]);

        parent.addView(child);

        FloatAnimation anim = new FloatAnimation(p, parent, child);

        anim.setDuration(dur);
        anim.setInterpolator(new LinearInterpolator());
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation animation) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        parent.removeView(child);
                    }
                });
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }

            @Override
            public void onAnimationStart(Animation animation) {
            }
        });
        child.startAnimation(anim);
    }

    static class FloatAnimation extends Animation {
        private PathMeasure mPm;
        private View mView;
        private float mDistance;

        public FloatAnimation(Path path, View parent, View child) {
            mPm = new PathMeasure(path, false);
            mDistance = mPm.getLength();
            mView = child;
            parent.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        }

        @Override
        protected void applyTransformation(float factor, Transformation transformation) {
            Matrix matrix = transformation.getMatrix();
            mPm.getMatrix(mDistance * factor, matrix, PathMeasure.POSITION_MATRIX_FLAG);
            mView.setRotation(1);
            mView.setScaleX(1);
            mView.setScaleY(1);
            if (factor >= 0.5)// 从50%开始淡出
                transformation.setAlpha(1.5F - factor);
        }

        public FloatAnimation(Path path, View parent, View child, int dropDuration, int cycle) {
            mPm = new PathMeasure(path, false);
            mDistance = mPm.getLength();
            mView = child;
            parent.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        }
    }
}

