package com.thel.modules.main.home.moments.mention;

import android.os.Bundle;
import androidx.annotation.Nullable;

import com.thel.R;
import com.thel.base.BaseActivity;

/**
 * Created by liuyun on 2017/10/24.
 */

public class MomentMentionedListActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_frame_normal);

        addFragment();

    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void addFragment() {
        MomentMentionedListFragment momentMentionedListFragment = (MomentMentionedListFragment) getSupportFragmentManager().findFragmentById(R.id.frame_content);

        if (momentMentionedListFragment == null) {
            momentMentionedListFragment = new MomentMentionedListFragment();
            momentMentionedListFragment.setArguments(getIntent().getExtras());
            getSupportFragmentManager().beginTransaction().add(R.id.frame_content, momentMentionedListFragment, MomentMentionedListFragment.class.getName()).commit();
        }

    }
}
