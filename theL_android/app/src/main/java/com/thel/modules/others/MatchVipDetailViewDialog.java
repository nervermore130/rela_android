package com.thel.modules.others;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.modules.main.me.aboutMe.BuyVipActivity;
import com.thel.modules.main.me.bean.LogInfoBean;
import com.thel.modules.main.me.match.eventcollect.collect.MatchLogUtils;
import com.thel.modules.others.beans.VipDetailBean;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.Utils;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;

/**
 * 匹配里的会员里的弹窗 显示更详细的功能信息
 *
 * @author lingwei
 * @date 2018/7/16
 */

public class MatchVipDetailViewDialog extends Dialog {

    public static final int VIP_WHO_LIKE_ME = 0;//谁喜欢我
    public static final int VIP_RECALL = 1;//无限撤回
    public static final int VIP_EXPOSURE = 2;//更多曝光


    private final Context mContext;

    private int type;
    private View close_dialog;
    private TextView vip_tile_dialog;
    private TextView vip_content_dialog;
    private ImageView vip_rsc_img;
    private RelativeLayout rl_purchase;
    private ViewPager view_pager;

    private ArrayList<VipDetailBean> Res_list = new ArrayList();
    private int[] picRes = new int[]{R.mipmap.dialog_who_like_me, R.mipmap.dialog_recall, R.mipmap.vip_pic_exposure};
    private LinearLayout mLinearLayout;

    //上一次点位高亮显示的位置
    private int prePosition = 0;
    private String pageId;
    private String frome_page_id;
    private String from_page;

    public MatchVipDetailViewDialog(@NonNull Context context) {
        this(context, R.style.CustomDialogBottom);
    }

    public MatchVipDetailViewDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        this.mContext = context;
        init();
        setLisener();
        initEvent();

    }


    public void setSeeingType(int type, String pageId, String page) {
        this.type = type;
        this.frome_page_id = pageId;
        this.from_page = page;
        initView();
    }

    private SparseArray<VipDetailBean> VipDetailMap = new SparseArray<VipDetailBean>() {
        {
            put(0, new VipDetailBean(VIP_WHO_LIKE_ME, TheLApp.getContext().getResources().getString(R.string.who_like_me_title), picRes[0], TheLApp.getContext().getResources().getString(R.string.right_now_with_her_march)));
            put(1, new VipDetailBean(VIP_RECALL, TheLApp.getContext().getResources().getString(R.string.while_recall), picRes[1], TheLApp.getContext().getResources().getString(R.string.can_reserve)));
            put(2, new VipDetailBean(VIP_EXPOSURE, TheLApp.getContext().getResources().getString(R.string.more_be_see_title), picRes[2], TheLApp.getContext().getResources().getString(R.string.more_be_see_text)));

        }
    };

    private void init() {
        final View view = LayoutInflater.from(mContext).inflate(R.layout.vip_detail_dialog_view, null);
        setContentView(view);
        close_dialog = view.findViewById(R.id.ll_close_dialog);
        rl_purchase = view.findViewById(R.id.rl_purchase);
        view_pager = view.findViewById(R.id.view_pager);
        mLinearLayout = view.findViewById(R.id.ll_points);
        int size = VipDetailMap.size();
        for (int i = 0; i < size; i++) {
            VipDetailBean bean = VipDetailMap.get(i);
            Res_list.add(bean);
            View v = new View(TheLApp.context);
            //添加底部灰点
            v.setBackgroundResource(R.drawable.gray_circle);
            //指定其大小
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(20, 20);
            if (i != 0)
                params.leftMargin = 20;
            v.setLayoutParams(params);
            mLinearLayout.addView(v);
        }

        view_pager.setAdapter(new VipDitailViewPager(mContext, type, Res_list));

    }

    private void initEvent() {

    }

    private void setLisener() {

        view_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                View ll_point_now = mLinearLayout.getChildAt(position);
                View ll_point_pre = mLinearLayout.getChildAt(prePosition);
                ll_point_pre.setBackgroundResource(R.drawable.gray_circle);

                ll_point_now.setBackgroundResource(R.drawable.green_circle);
                prePosition = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        rl_purchase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoBuyVip();
                dismiss();
            }
        });
        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

    }

    class VipDitailViewPager extends PagerAdapter {
        private final ArrayList<VipDetailBean> list;
        private LayoutInflater mInflater;
        private View convertView;

        public VipDitailViewPager(Context context, int type, ArrayList<VipDetailBean> arrayList) {
            mInflater = (LayoutInflater) TheLApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.list = arrayList;

        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            convertView = mInflater.inflate(R.layout.my_vip_detail_func, container, false);
            vip_tile_dialog = convertView.findViewById(R.id.vip_tile_dialog);
            vip_content_dialog = convertView.findViewById(R.id.vip_content_dialog);
            vip_rsc_img = convertView.findViewById(R.id.vip_rsc_img_dialog);

            VipDetailBean vipDetailBean = list.get(position);
            vip_tile_dialog.setText(vipDetailBean.title);
            vip_content_dialog.setText(vipDetailBean.desc);
            vip_rsc_img.setImageResource(vipDetailBean.res_Index);

            container.addView(convertView);


            return convertView;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((View) object);
        }
    }

    private void initView() {
        if (view_pager != null) {
            view_pager.setCurrentItem(type);
            if (type == 0) {
                View ll_point_now = mLinearLayout.getChildAt(0);
                ll_point_now.setBackgroundResource(R.drawable.green_circle);

            }
            pageId = Utils.getPageId();
        }

    }

    /**
     * 充值会员
     */
    private void gotoBuyVip() {
        String latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
        String longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");

        LogInfoBean logInfoBean = new LogInfoBean();
        logInfoBean.page = "match.who";
        logInfoBean.page_id = pageId;
        logInfoBean.activity = "viewbuy";
        logInfoBean.from_page = from_page;
        logInfoBean.from_page_id = frome_page_id;
        logInfoBean.lat = latitude;
        logInfoBean.lng = longitude;
        MatchLogUtils.getInstance().addLog(logInfoBean);
        MobclickAgent.onEvent(TheLApp.getContext(), "renew_member_click");
        Intent intent = new Intent(mContext, BuyVipActivity.class);
        intent.putExtra("isVip", false);
        Bundle bundle = new Bundle();
        bundle.putString(ShareFileUtils.MATCH_FROM_PAGE_ID, pageId);
        bundle.putString(ShareFileUtils.MATCH_FROM_PAGE, "match.who");
        intent.putExtras(bundle);
        mContext.startActivity(intent);
    }


}
