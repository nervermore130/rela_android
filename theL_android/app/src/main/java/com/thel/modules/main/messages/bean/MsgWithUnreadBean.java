package com.thel.modules.main.messages.bean;

import android.content.ContentValues;
import android.database.Cursor;

import com.thel.modules.main.messages.db.MessageDataBaseAdapter;
import com.thel.utils.GsonUtils;


public class MsgWithUnreadBean implements Comparable<MsgWithUnreadBean> {

    /**
     * 未读消息数量
     */
    public int unReadCount;

    public MsgBean msgBean;

    public static int WINK_NO = 0;//是否显示挤眼
    public static int WINK_YES = 1;//

    public int isWinked = WINK_NO;

    /**
     * 置顶的时间，根据置顶的时间来决定是否置顶以及置顶顺序，-1表示不置顶
     */
    public long stickyTop = -1;

    public void fromCursor(Cursor cursor) {
        msgBean = new MsgBean();
        msgBean.fromCursor(cursor, false);
        unReadCount = cursor.getInt(cursor.getColumnIndex(MessageDataBaseAdapter.F_MSG_UNREAD_COUNT));
        stickyTop = cursor.getLong(cursor.getColumnIndex(MessageDataBaseAdapter.F_MSG_STICKY_TOP));
    }

    public ContentValues getContentValues() {
        ContentValues contentValues = msgBean.getContentValues(false);
        contentValues.put(MessageDataBaseAdapter.F_MSG_UNREAD_COUNT, unReadCount);
        contentValues.put(MessageDataBaseAdapter.F_MSG_STICKY_TOP, stickyTop);
        return contentValues;
    }

    @Override
    public int compareTo(MsgWithUnreadBean another) {
        // 根据置顶排序
        if (stickyTop > another.stickyTop) {
            return -1;
        }
        if (stickyTop < another.stickyTop) {
            return 1;
        }
        // 为了做时间的排序用
        if (null != msgBean && null != another.msgBean) {
            if (msgBean.msgTime > another.msgBean.msgTime)
                return -1;
            if (msgBean.msgTime < another.msgBean.msgTime)
                return 1;
        }
        return 0;
    }


    @Override
    public String toString() {
        return GsonUtils.createJsonString(this);
    }
}
