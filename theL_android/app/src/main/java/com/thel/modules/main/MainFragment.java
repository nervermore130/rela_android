package com.thel.modules.main;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.provider.Settings;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseFragment;
import com.thel.bean.AdBean;
import com.thel.bean.moments.MomentsCheckBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.RelaBoostFlutterActivity;
import com.thel.modules.main.discover.DiscoverFragmentTwo;
import com.thel.modules.main.home.HomeFragment;
import com.thel.modules.main.me.MeFragment;
import com.thel.modules.main.me.bean.LogInfoBean;
import com.thel.modules.main.me.match.eventcollect.collect.LiveLogUtils;
import com.thel.modules.main.messages.MessageFragment;
import com.thel.modules.main.nearby.NearbyFragment4_9_2;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.ui.dialog.GuideDialogFragment;
import com.thel.utils.BusinessUtils;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.GsonUtils;
import com.thel.utils.InstallUtils;
import com.thel.utils.L;
import com.thel.utils.NotchUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SizeUtils;
import com.thel.utils.Utils;
import com.umeng.analytics.MobclickAgent;

import org.reactivestreams.Subscription;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import me.rela.rf_s_bridge.new_router.NewUniversalRouter;
import me.rela.rf_s_bridge.router.UniversalRouter;

/**
 * @author liuyun
 * @date 2017/9/13
 */

public class MainFragment extends BaseFragment implements MainContract.View {

    private final static int INSTALL_APK_REQUEST_CODE = 1;

    private MainContract.Presenter mPresenter;

    private HomeFragment mMomentsFragment;

    private NearbyFragment4_9_2 nearbyFragment;

    private MessageFragment mMessagesFragment;

    private MeFragment mMeFragment;

    private DiscoverFragmentTwo mDiscoverFragment;

    private FragmentTransaction mFragmentTransaction;

    @BindView(R.id.moments_iv)
    ImageView moments_iv;

    @BindView(R.id.nearby_iv)
    ImageView nearby_iv;

    @BindView(R.id.message_iv)
    ImageView message_iv;

    @BindView(R.id.me_sdv)
    ImageView me_sdv;

    @BindView(R.id.txt_unread_total)
    TextView txt_unread_total;

    @BindView(R.id.txt_moments_msg_total)
    TextView txt_moments_msg_total;

    @BindView(R.id.img_failed_moments)
    ImageView img_failed_moments;

    @BindView(R.id.img_complete_user_info)
    ImageView img_complete_user_info;

    @BindView(R.id.img_moments_new)
    TextView img_moments_new;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.lin_discover)
    LinearLayout lin_discover;

    @BindView(R.id.img_discover)
    ImageView img_discover;

    @BindView(R.id.nearby_fl)
    FrameLayout nearby_fl;

    @BindView(R.id.me_rl)
    FrameLayout me_rl;

    @BindView(R.id.txt_unread_point)
    TextView txt_unread_see_me_point;

    @BindView(R.id.view_home_guide)
    View view_home_guide;

    @BindView(R.id.view_discover_guide)
    View view_discover_guide;

    @BindView(R.id.root_rl)
    RelativeLayout root_rl;

    private String jumpTab = TheLConstants.MainFragmentPageConstants.FRAGMENT_HOME;

    private String currentTab = TheLConstants.MainFragmentPageConstants.FRAGMENT_HOME;

    public static final String NEARBY_TAG = TheLConstants.MainFragmentPageConstants.FRAGMENT_HOME;

    private int notReadCommentNum = 0;
    private String pageId;
    private String from_page_id;
    private String from_page;
    private int haveNotReadMomentsFlag = 0;
    private String from_tab; //从原来的tab页面跳转
    private String current_tab; //当前的tab页
    private String fragmentTag;

    public static MainFragment getInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        new MainPresenter(this);

        Intent intent = getActivity().getIntent();
        if (null != intent) {
            String from = intent.getStringExtra("fromPage");
            String togo = intent.getStringExtra(TheLConstants.BUNDLE_KEY_WHERE_TO_GO);
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                from_page_id = bundle.getString(ShareFileUtils.MATCH_FROM_PAGE_ID, "");
                from_page = bundle.getString(ShareFileUtils.MATCH_FROM_PAGE, "");

            }
            if (togo == null) {
                togo = "";
            }
            if (null != from && from.equals("notification")) {
                setTabBtnBg(TheLConstants.MainFragmentPageConstants.FRAGMENT_MESSAGE);
            } else if (togo.equals("5") || togo.equals(TheLConstants.MainFragmentPageConstants.FRAGMENT_ME)) {//跳转到MeActivity,语言切换的时候
                jumpTab = togo;
                setTabBtnBg(togo);
            } else {
                setTabBtnBg(TheLConstants.MainFragmentPageConstants.FRAGMENT_HOME);
            }
        } else {
            setTabBtnBg(TheLConstants.MainFragmentPageConstants.FRAGMENT_HOME);
        }


        int padingHeight = SizeUtils.dip2px(TheLApp.context, 24f);

        if (NotchUtils.getNotchHeight(TheLApp.context) > 0) {
            padingHeight = NotchUtils.getNotchHeight(TheLApp.context);
        }

        root_rl.setPadding(0, padingHeight, 0, 0);

        initNativeFragments();

        pageId = Utils.getPageId();

    }

    public void initNativeFragments() {

        final Bundle discoverBundle = new Bundle();

        if (!isAdded()) {
            return;
        }

        mFragmentTransaction = getChildFragmentManager().beginTransaction();

        String tag;

        /**
         * 添加HomeFragment
         */

        addFragment(new Fragment(), HomeFragment.class.getName());

        /**
         * 添加DiscoverFragmentTwo
         */
        mDiscoverFragment = (DiscoverFragmentTwo) findFragmentByTag(DiscoverFragmentTwo.class.getName());
        if (mDiscoverFragment == null) {
            mDiscoverFragment = DiscoverFragmentTwo.getInstance(discoverBundle);
        }

        L.d("MainFragment", " mDiscoverFragment : " + mDiscoverFragment);

        L.d("MainFragment", " mDiscoverFragment.isAdded() : " + mDiscoverFragment.isAdded());

        if (!mDiscoverFragment.isAdded()) {
            addFragment(mDiscoverFragment, DiscoverFragmentTwo.class.getName());
        }

        /**
         * 添加NearbyFragment
         */

        addFragment(new Fragment(), NearbyFragment4_9_2.class.getName());

        /**
         * 添加MessageFragment
         */
        mMessagesFragment = (MessageFragment) findFragmentByTag(MessageFragment.class.getName());
        if (mMessagesFragment == null) {
            mMessagesFragment = MessageFragment.getInstance();
        }

        if (!mMessagesFragment.isAdded()) {
            addFragment(mMessagesFragment, MessageFragment.class.getName());
        }

        /**
         * 添加MeFragment
         */

        addFragment(new Fragment(), MeFragment.class.getName());

        mFragmentTransaction.commit();

        L.d("MainFragment", " jumpTab : " + jumpTab);

//        switch (jumpTab) {
//            case TheLConstants.MainFragmentPageConstants.FRAGMENT_HOME:
        showOrHideFragment(HomeFragment.class.getName());
//                break;
//            case TheLConstants.MainFragmentPageConstants.FRAGMENT_DISCOVER:
//                showOrHideFragment(DiscoverFragmentTwo.class.getName());
//                break;
//            case TheLConstants.MainFragmentPageConstants.FRAGMENT_NEARBY:
//                showOrHideFragment(NearbyFragment4_9_2.class.getName());
//                break;
//            case TheLConstants.MainFragmentPageConstants.FRAGMENT_MESSAGE:
//                showOrHideFragment(MessageFragment.class.getName());
//                break;
//            case TheLConstants.MainFragmentPageConstants.FRAGMENT_ME:
//                showOrHideFragment(MeFragment.class.getName());
//                break;
//            default:
//                break;
//        }

    }

    private void traceLogReport(String fragmentHome, String jumpTab) {
        String latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
        String longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");
        LogInfoBean logInfoBean = new LogInfoBean();

        logInfoBean.page = "root";
        logInfoBean.page_id = pageId;
        logInfoBean.lat = latitude;
        logInfoBean.lng = longitude;

        LogInfoBean.LogsDataBean logsDataBean = new LogInfoBean.LogsDataBean();
        logsDataBean.from_tab = jumpTab;

        ShareFileUtils.setString(ShareFileUtils.rootSwitchPage, jumpTab);
        ShareFileUtils.setString(ShareFileUtils.rootSwitchPageId, pageId);

        switch (fragmentHome) {
            case TheLConstants.MainFragmentPageConstants.FRAGMENT_HOME:
                logInfoBean.activity = "click_home";
                logsDataBean.redspot = haveNotReadMomentsFlag;

                break;
            case TheLConstants.MainFragmentPageConstants.FRAGMENT_DISCOVER:
                logInfoBean.activity = "click_live";

                break;
            case TheLConstants.MainFragmentPageConstants.FRAGMENT_NEARBY:
                logInfoBean.activity = "click_find";

                break;
            case TheLConstants.MainFragmentPageConstants.FRAGMENT_MESSAGE:
                logInfoBean.activity = "click_message";
                logsDataBean.redspot = unReadTotal;

                break;
            case TheLConstants.MainFragmentPageConstants.FRAGMENT_ME:
                logInfoBean.activity = "click_my";
                // logsDataBean.redspot = totalNum;

                break;

        }
        logInfoBean.data = logsDataBean;

        LiveLogUtils.getInstance().addLog(logInfoBean);
    }

    private void addFragment(Fragment fragment, String tag) {

        mFragmentTransaction.add(R.id.frame_content, fragment, tag);
    }

    public void initFlutterFragment() {

//        if (getActivity() != null) {
//
//            RfSBridgeHandlerFactory.getInstance().setActivity(getActivity());
//
//            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
//
//            Fragment fragment = findFragmentByTag(RelaFlutterFragment.class.getName());
//
//            if (fragment == null) {
//                fragment = RelaFlutterFragment.getInstance();
//            }
//
//            fragmentManager.beginTransaction().add(R.id.flutter_frame_content, fragment, RelaFlutterFragment.class.getName()).commit();
//        }

    }

    private Fragment findFragmentByTag(String tag) {
        return getChildFragmentManager().findFragmentByTag(tag);
    }

    @OnClick(R.id.moments_rl)
    public void showMoments() {
//        showOrHideFragment(mMomentsFragment.getTag());
        traceLogReport(TheLConstants.MainFragmentPageConstants.FRAGMENT_HOME, from_tab);

//        flutter_frame_content.setVisibility(View.VISIBLE);

        if (getActivity() != null && getActivity() instanceof RelaBoostFlutterActivity) {
            ((RelaBoostFlutterActivity) getActivity()).showFlutterView();
        }

        checkFlutterTab("0");

        from_tab = "home";

    }


    @OnClick(R.id.lin_discover)
    void showDiscover() {

        L.d("MainFragment", " --------showDiscover ------");

        L.d("MainFragment", " showDiscover getActivity() : " + (getActivity() instanceof RelaBoostFlutterActivity));

        showOrHideFragment(mDiscoverFragment.getTag());
        traceLogReport(TheLConstants.MainFragmentPageConstants.FRAGMENT_DISCOVER, from_tab);

        if (getActivity() != null && getActivity() instanceof RelaBoostFlutterActivity) {
            ((RelaBoostFlutterActivity) getActivity()).hideFlutterView();
        }

        from_tab = "live";

    }


    @OnClick(R.id.nearby_fl)
    void showNearBy() {

//        flutter_frame_content.setVisibility(View.VISIBLE);

//        showOrHideFragment(nearbyFragment.getTag());
        traceLogReport(TheLConstants.MainFragmentPageConstants.FRAGMENT_NEARBY, from_tab);

        L.d("MainFragment", " --------showNearBy ------");

        L.d("MainFragment", " getActivity() : " + (getActivity() instanceof RelaBoostFlutterActivity));

        if (getActivity() != null && getActivity() instanceof RelaBoostFlutterActivity) {
            ((RelaBoostFlutterActivity) getActivity()).showFlutterView();
        }

        checkFlutterTab("2");

        from_tab = "find";

    }

    @OnClick(R.id.message_fl)
    void showMessage() {

        L.d("MainFragment", " showMessage : ");

        showOrHideFragment(mMessagesFragment.getTag());
        //切换至MessageFragment时发送广播更新列表
        Intent intent = new Intent();
        intent.setAction(TheLConstants.BROADCAST_CLICK_MESSAGE);
        getActivity().sendBroadcast(intent);
        traceLogReport(TheLConstants.MainFragmentPageConstants.FRAGMENT_MESSAGE, from_tab);

        from_tab = "message";

        if (getActivity() != null && getActivity() instanceof RelaBoostFlutterActivity) {
            ((RelaBoostFlutterActivity) getActivity()).hideFlutterView();
        }

    }

    @OnClick(R.id.me_rl)
    void showMe() {

        L.d("MainFragment", " showMe : ");

//        flutter_frame_content.setVisibility(View.VISIBLE);

        if (getActivity() != null && getActivity() instanceof RelaBoostFlutterActivity) {
            ((RelaBoostFlutterActivity) getActivity()).showFlutterView();
        }

//        showOrHideFragment(MeFragment.class.getName());
        traceLogReport(TheLConstants.MainFragmentPageConstants.FRAGMENT_ME, from_tab);

        checkFlutterTab("4");

        from_tab = "my";

    }

    private void checkFlutterTab(String tab) {
        NewUniversalRouter.sharedInstance.switchTo(tab, "{\"type\": \"tabSwitch\", \"tabIndex\":\"" + tab + "\"}");
    }


    public void showOrHideFragment(String fragmentTag) {

        try {

            mFragmentTransaction = getChildFragmentManager().beginTransaction();

            L.d("MainFragment", " fragmentTag : " + fragmentTag);

            this.fragmentTag = fragmentTag;

            String tag = "";

            if (fragmentTag.equals(DiscoverFragmentTwo.class.getName())) {
                mFragmentTransaction.show(mDiscoverFragment);
                tag = TheLConstants.MainFragmentPageConstants.FRAGMENT_DISCOVER;
            } else {
                mFragmentTransaction.hide(mDiscoverFragment);
            }


            if (fragmentTag.equals(MessageFragment.class.getName())) {
                mFragmentTransaction.show(mMessagesFragment);
                tag = TheLConstants.MainFragmentPageConstants.FRAGMENT_MESSAGE;
            } else {
                mFragmentTransaction.hide(mMessagesFragment);
            }

            mFragmentTransaction.commitAllowingStateLoss();

            setTabBtnBg(tag);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void setTabBtnBg(String tag) {

        currentTab = tag;

        if (tag.equals(TheLConstants.MainFragmentPageConstants.FRAGMENT_HOME)) {
            nearby_iv.setSelected(false);
            message_iv.setSelected(false);
            moments_iv.setSelected(true);
            img_discover.setSelected(false);
            isReleasing();
            tabScaleAnim(moments_iv);

        } else if (tag.equals(TheLConstants.MainFragmentPageConstants.FRAGMENT_NEARBY)) {
//            flutter_frame_content.setVisibility(View.INVISIBLE);

            L.d("MainFragment", " RelaBoostFlutterActivity : " + (getActivity() instanceof RelaBoostFlutterActivity));

            if (getActivity() != null && getActivity() instanceof RelaBoostFlutterActivity) {
                ((RelaBoostFlutterActivity) getActivity()).hideFlutterView();
            }
            nearby_iv.setSelected(true);
            message_iv.setSelected(false);
            moments_iv.setSelected(false);
            img_discover.setSelected(false);

            isReleasing();
            tabScaleAnim(nearby_iv);
        } else if (tag.equals(TheLConstants.MainFragmentPageConstants.FRAGMENT_MESSAGE)) {
//            flutter_frame_content.setVisibility(View.INVISIBLE);
            if (getActivity() != null && getActivity() instanceof RelaBoostFlutterActivity) {
                ((RelaBoostFlutterActivity) getActivity()).hideFlutterView();
            }
            nearby_iv.setSelected(false);
            message_iv.setSelected(true);
            moments_iv.setSelected(false);
            img_discover.setSelected(false);
            me_sdv.setSelected(false);
            isReleasing();
            tabScaleAnim(message_iv);
        } else if (tag.equals(TheLConstants.MainFragmentPageConstants.FRAGMENT_ME)) {
            nearby_iv.setSelected(false);
            message_iv.setSelected(false);
            moments_iv.setSelected(false);
            me_sdv.setSelected(true);
            img_discover.setSelected(false);
            isReleasing();
            tabScaleAnim(me_sdv);
        } else if (tag.equals(TheLConstants.MainFragmentPageConstants.FRAGMENT_DISCOVER)) {
            nearby_iv.setSelected(false);
            message_iv.setSelected(false);
            moments_iv.setSelected(false);
            img_discover.setSelected(true);
            me_sdv.setSelected(false);
            isReleasing();
            tabScaleAnim(img_discover);

        }

    }

    private void isReleasing() {

        if (progressBar != null) {
            if (progressBar.isShown()) {
                moments_iv.setVisibility(View.GONE);
            } else {
                moments_iv.setVisibility(View.VISIBLE);
            }
        }
    }

    private int unReadTotal = 0;

    /**
     * 更新总未读消息数
     *
     * @param msgAmount 如果是<=0，则全量更新；如果是>0，则更新数量
     */
    @Override
    public void updateUnreadTotal(int msgAmount) {

        if (msgAmount > 0) {
            unReadTotal += 1;
            refreshUnreadTotal(unReadTotal);
        } else {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    refreshUnreadTotal(unReadTotal);
                }
            });

        }

    }


    // 检查是否有推荐表情，有的话要刷新界面
    @Override
    public void updateRecommendStciker() {

    }

    @Override
    public void refreshUnreadTotal(int number) {

        unReadTotal = number;

        refreshMessageAndNotifTotal(unReadTotal, notReadCommentNum);
    }

    private void refreshMessageAndNotifTotal(int unReadTotal, int notReadCommentCount) {

        L.d("MainFragment", "unReadTotal" + unReadTotal + "notReadCommentCount" + notReadCommentCount);


        int unreadCount = unReadTotal + notReadCommentCount;

        String unreadCountStr;

        if (unreadCount >= 99) {
            unreadCountStr = "99+";
        } else {

            if (unreadCount > 0) {
                unreadCountStr = unreadCount + "";
            } else {
                unreadCountStr = "0";
            }

        }


        txt_unread_total.setText(unreadCountStr);

        txt_unread_total.setVisibility(unreadCount > 0 ? View.VISIBLE : View.GONE);
    }

    /**
     * 更新moments消息和密友请求消息数量
     *
     * @param momentsCheck
     */
    @Override
    public void updateMomentsAndRequestsTotal(MomentsCheckBean momentsCheck) {

        if (img_failed_moments.getVisibility() == View.VISIBLE) {// 如果有未发送成功的日志，则不刷新界面
            return;
        }

        if (momentsCheck == null) {// 初始化
            int unreadMomentsMsg = ShareFileUtils.getInt(ShareFileUtils.UNREAD_MOMENTS_MSG_NUM, 0);
            refreshMessageAndNotifTotal(unReadTotal, unreadMomentsMsg);
            L.d("meainfragment_msg", "unReadTotal 0 : " + unReadTotal + " notReadCommentNumSSS : " + notReadCommentNum);
            return;
        }

        haveNotReadMomentsFlag = momentsCheck.haveNotReadMomentsFlag;
        if (momentsCheck.notReadCommentNum > 0) {// 如果有未读消息，则显示条数
            notReadCommentNum = momentsCheck.notReadCommentNum;
            refreshMessageAndNotifTotal(unReadTotal, notReadCommentNum);
            L.d("meainfragment_msg", "unReadTotal 1 : " + unReadTotal + " notReadCommentNumAAA : " + notReadCommentNum);

        } else {

            L.d("meainfragment_msg", "unReadTotal 2 : " + unReadTotal + " notReadCommentNumAAA : " + notReadCommentNum);

            notReadCommentNum = 0;
            refreshMessageAndNotifTotal(unReadTotal, notReadCommentNum);
        }

        // 刷新密友请求
        updateUnreadTotal(0);
    }

    /**
     * 设为正在发送的图标
     */
    @Override
    public void updateSendingMoments() {
        progressBar.setVisibility(View.VISIBLE);
        moments_iv.setVisibility(View.GONE);
        img_failed_moments.setVisibility(View.GONE);
    }

    /**
     * 更新是否有发送失败的日志
     */
    @Override
    public void updateFailedMoments() {
        me_sdv.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void gotoMomentFragmentActivityPage() {
        try {
            setTabBtnBg(TheLConstants.MainFragmentPageConstants.FRAGMENT_HOME);
            showMoments();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getMomentCheckBean(MomentsCheckBean momentsCheckBean) {

    }

    @Override
    public void releaseSuccess(String momentsId, long releaseTime) {

        progressBar.setVisibility(View.INVISIBLE);
        moments_iv.setVisibility(View.VISIBLE);

    }

    @Override
    public void releaseFailed() {
        progressBar.setVisibility(View.GONE);
        moments_iv.setVisibility(View.VISIBLE);
    }


    @Override
    public void updateUnReadSeeMeTotal(MomentsCheckBean checkBean) {
        int totalNum = 0;

        if (checkBean == null) {
            totalNum = ShareFileUtils.getInt(ShareFileUtils.NEW_CHECK_NUM, 0) + ShareFileUtils.getInt(ShareFileUtils.NEW_LIKE_NUM, 0);
            L.d("mainfragmentpeint", "NEW_CHECK_NUM+" + ShareFileUtils.getInt(ShareFileUtils.NEW_CHECK_NUM, 0) + "NEW_LIKE_NUM" + ShareFileUtils.getInt(ShareFileUtils.NEW_LIKE_NUM, 0));

        } else {
            if (checkBean.notReadViewMeNum != 0 || checkBean.notReadCheckRemind != 0) {
                totalNum = checkBean.notReadViewMeNum;
            } else if (checkBean.notReadCheckRemind != 0) {
                totalNum = -1;
            }
            //会员有喜欢数相加

//            /**
//             * 统一所有最新喜欢的地方都复用一个sp里保存的
//             * **/
//            ShareFileUtils.setInt(ShareFileUtils.NEW_LIKE_NUM, checkBean.likeMeNum);
//            if (checkBean.likeMeNum != 0 && UserUtils.getUserVipLevel() > 0) {
//                totalNum = checkBean.likeMeNum + totalNum;
//            }
        }

        if (!(ShareFileUtils.getBoolean(ShareFileUtils.NEED_COMPLETE_USER_INFO, false) || (TextUtils.isEmpty(ShareFileUtils.getString(ShareFileUtils.BIND_CELL, "")) && TextUtils.isEmpty(ShareFileUtils.getString(ShareFileUtils.BIND_WX, "")) && TextUtils.isEmpty(ShareFileUtils.getString(ShareFileUtils.BIND_FB, ""))))) {

            if (totalNum > 0 && totalNum < 99) {
                txt_moments_msg_total.setVisibility(View.VISIBLE);
                txt_unread_see_me_point.setVisibility(View.GONE);
                txt_moments_msg_total.setText(totalNum + "");

            } else if (totalNum >= 99) {
                txt_moments_msg_total.setVisibility(View.VISIBLE);
                txt_unread_see_me_point.setVisibility(View.GONE);
                txt_moments_msg_total.setText("99+");

            } else if (totalNum == -1) {
                txt_moments_msg_total.setVisibility(View.GONE);
                txt_unread_see_me_point.setVisibility(View.GONE);

                boolean me_ads = ShareFileUtils.getBoolean(ShareFileUtils.ME_ADS, false);

            } else {
                txt_moments_msg_total.setVisibility(View.GONE);
                txt_unread_see_me_point.setVisibility(View.GONE);
                boolean me_ads = ShareFileUtils.getBoolean(ShareFileUtils.ME_ADS, false);
            }
        }

    }

    @Override
    public void updateLikeMeTotal(MomentsCheckBean likeMe) {

    }

    @Override
    public void clearSeeMeView() {
        ShareFileUtils.setInt(ShareFileUtils.NEW_CHECK_NUM, 0);
        int newLike = ShareFileUtils.getInt(ShareFileUtils.NEW_LIKE_NUM, 1);
        if (newLike > 0) {
            txt_moments_msg_total.setVisibility(View.VISIBLE);
            txt_unread_see_me_point.setVisibility(View.GONE);

            txt_moments_msg_total.setText(newLike + "");
        } else {
            txt_moments_msg_total.setVisibility(View.GONE);
            txt_unread_see_me_point.setVisibility(View.GONE);
            boolean me_ads = ShareFileUtils.getBoolean(ShareFileUtils.ME_ADS, false);

        }

    }

    @Override
    public void clearLikeMeNum() {
        ShareFileUtils.setInt(ShareFileUtils.NEW_LIKE_NUM, 0);
        int seeMe = ShareFileUtils.getInt(ShareFileUtils.NEW_CHECK_NUM, 1);
        if (seeMe > 0) {
            txt_moments_msg_total.setVisibility(View.VISIBLE);
            txt_unread_see_me_point.setVisibility(View.GONE);
            txt_moments_msg_total.setText(seeMe + "");
        } else {
            txt_moments_msg_total.setVisibility(View.GONE);
            txt_unread_see_me_point.setVisibility(View.GONE);
            boolean me_ads = ShareFileUtils.getBoolean(ShareFileUtils.ME_ADS, false);
        }
    }

    @Override
    public void clearNotReadCommentNum() {
        notReadCommentNum = 0;
        refreshMessageAndNotifTotal(unReadTotal, notReadCommentNum);
    }

    @Override
    public void initDataModel(MomentsCheckBean momentsCheckBean) {

    }

    @Override
    public void showLivePage() {
        showOrHideFragment(DiscoverFragmentTwo.class.getName());
        GrowingIOUtil.postNativePage("live_page");
    }

    @Override
    public void showMessagePage() {
        showOrHideFragment(MessageFragment.class.getName());
        GrowingIOUtil.postNativePage("message_page");
    }

    @Override
    public void showFlutter() {
        if (getActivity() != null && getActivity() instanceof RelaBoostFlutterActivity) {
            ((RelaBoostFlutterActivity) getActivity()).showFlutterView();
        }
    }

    private String apkUrl;

    @Override
    public void gotoAPKSetting(String apkUrl) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            this.apkUrl = apkUrl;

            Intent intent = new Intent(Settings.ACTION_MANAGE_UNKNOWN_APP_SOURCES);
            intent.putExtra("apkUrl", apkUrl);
            startActivityForResult(intent, INSTALL_APK_REQUEST_CODE);
        }
    }


    /**
     * 到话题界面
     */
    @Override
    public void gotoThemePage() {
        try {
            setTabBtnBg(TheLConstants.MainFragmentPageConstants.FRAGMENT_DISCOVER);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void gotoNearbyPage(int gotoTab) {
        try {
            nearby_fl.callOnClick();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 清除moments提醒
     *
     * @param type :1清除新日志 2清除未读消息
     */
    @Override
    public void clearMomentsTotal(int type) {
        if (img_moments_new == null || txt_moments_msg_total == null) {
            return;
        }

        L.d("MainFragment", " logType : " + type);

        L.d("MainFragment", " unReadTotal : " + unReadTotal);

        if (type == 1 && unReadTotal == 0) {
            txt_unread_total.setVisibility(View.GONE);
        } else if (type == 2 && unReadTotal == 0) {
            txt_unread_total.setVisibility(View.GONE);
        }
        if (type == 1) {
            ShareFileUtils.setInt(ShareFileUtils.UNREAD_MOMENTS_MSG_NUM, 0);
            refreshMessageAndNotifTotal(unReadTotal, 0);
        }
    }

    @Override
    public void refreshNeedCompleteUserInfo(MomentsCheckBean momentsCheck) {

    }


    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void setPresenter(MainContract.Presenter presenter) {

        mPresenter = presenter;

        presenter.initDataModel(this);

    }

    @Override
    public void onResume() {
        super.onResume();

        if (mPresenter != null) {
            mPresenter.subscribe();
        }

        MobclickAgent.onPageStart(this.getClass().getName());
        MobclickAgent.onResume(getContext());

    }

    @Override
    public void onPause() {
        super.onPause();
        if (mPresenter != null) {
            mPresenter.unSubscribe();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    public void refreshFollow() {
        if (mMomentsFragment != null) {
            mMomentsFragment.refreshFollow();
        }

        if (mMeFragment != null) {
            mMeFragment.refreshMeUi();
        }
    }

    public void showThemeFragment() {
        setTabBtnBg(TheLConstants.MainFragmentPageConstants.FRAGMENT_HOME);
        showMoments();
        if (mMomentsFragment != null) {
            mMomentsFragment.showTheme();
        }
    }

    public void showDiscoverVoice() {
        setTabBtnBg(TheLConstants.MainFragmentPageConstants.FRAGMENT_DISCOVER);
        showDiscover();
        if (mDiscoverFragment != null && mDiscoverFragment.isAdded()) {
            mDiscoverFragment.showVoice();
        }
    }

    private void tabScaleAnim(final View view) {

        if (view == null) {
            return;
        }

        ValueAnimator valueAnimator = ValueAnimator.ofFloat(1f, 1.3f, 0.9f, 1.15f, 0.95f, 1.02f, 1f);
        valueAnimator.setDuration(1000);
        valueAnimator.setInterpolator(new LinearInterpolator());
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float offset = (float) animation.getAnimatedValue();
                view.setScaleX(offset);
                view.setScaleY(offset);
            }
        });
        valueAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                view.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                view.setLayerType(View.LAYER_TYPE_NONE, null);

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        valueAnimator.start();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == INSTALL_APK_REQUEST_CODE) {
            if (apkUrl != null) {
                InstallUtils.installApk(getActivity(), apkUrl);
            }
        }
    }
}
