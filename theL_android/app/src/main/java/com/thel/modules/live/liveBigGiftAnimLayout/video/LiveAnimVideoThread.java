package com.thel.modules.live.liveBigGiftAnimLayout.video;

import android.graphics.BitmapFactory;
import android.graphics.SurfaceTexture;
import android.opengl.GLES11Ext;
import android.opengl.GLES20;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import androidx.annotation.RawRes;
import android.text.TextUtils;
import android.util.Log;
import android.view.Surface;

import com.ksyun.media.player.IMediaPlayer;
import com.ksyun.media.player.KSYMediaPlayer;
import com.thel.app.TheLApp;
import com.thel.utils.L;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.HashMap;

import baidu.filter.GPUImageExtRotationTexFilter;
import google.grafika.gles.EglCore;
import google.grafika.gles.GlUtil;
import google.grafika.gles.WindowSurface;
import jp.co.cyberagent.android.gpuimage.GPUImageFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageFilterGroup;
import jp.co.cyberagent.android.gpuimage.GPUImageLookupFilter;

/**
 * Created by waiarl on 2018/1/12.
 */

class LiveAnimVideoThread extends Thread implements SurfaceTexture.OnFrameAvailableListener, LiveAnimVideoHandlerConstant {
    private final Handler mUIHandler;//主线程handler
    private LiveAnimVideoHandler mHandler;//本线成handler
    private Object mStartLock = new Object();
    private boolean mReady;
    private EglCore mEglCore;

    int mTextureId = -1;

    final float[] CUBE = {
            -1.0f, -1.0f,
            1.0f, -1.0f,
            -1.0f, 1.0f,
            1.0f, 1.0f,
    };
    private FloatBuffer mGLCubeBuffer;
    private FloatBuffer mGLTextureBuffer;

    HashMap<SurfaceTexture, WindowSurface> windowSurfacesMap = new HashMap<SurfaceTexture, WindowSurface>();

    WindowSurface mWindowSurface1;
    GPUImageFilterGroup gpuImageFilter;

    int surfaceWidth = 0;
    int surfaceHeight = 0;
    private KSYMediaPlayer videoPlayer;
    private SurfaceTexture mCameraTexture;
    private boolean isReload = false;

    protected LiveAnimVideoThread(Handler uihandler) {
        this.mUIHandler = uihandler;
    }


    @Override
    public void run() {
        if (Looper.myLooper() == null) {
            Looper.prepare();
        }
        mHandler = new LiveAnimVideoHandler(this);
        synchronized (mStartLock) {
            mReady = true;
            mStartLock.notify();
        }
        mEglCore = new EglCore(null, 0);
        initPlayer();
        Looper.loop();

        Log.i(TAG, "looper quit");

        if (gpuImageFilter != null) {
            gpuImageFilter.destroy();
            gpuImageFilter = null;
        }

        mEglCore.release();

        synchronized (mStartLock) {
            mReady = false;
        }
    }

    protected void initPlayer() {
        videoPlayer = new KSYMediaPlayer.Builder(TheLApp.getContext()).build();
        isReload = false;
        try {
            String path = "/sdcard/thel/download/111.mp4";
            String p1 = "http://live-yf-hdl.huomaotv.cn/live/bcfpxN35275.flv?from=huomaoroom";
            videoPlayer.setDataSource(path);
            videoPlayer.prepareAsync();
            videoPlayer.setLooping(true);
            videoPlayer.setDecodeMode(KSYMediaPlayer.KSYDecodeMode.KSY_DECODE_MODE_HARDWARE);
            videoPlayer.setVideoScalingMode(KSYMediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
        } catch (Exception e) {
            e.printStackTrace();
        }
        videoPlayer.setOnPreparedListener(new IMediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(final IMediaPlayer iMediaPlayer) {
                L.i(TAG,"onPrepared");
                if (mUIHandler != null) {
                    Message.obtain(mUIHandler, MSG_VIDEO_PLAYER_PREPARED, iMediaPlayer.getVideoWidth(), iMediaPlayer.getVideoHeight()).sendToTarget();
                    iMediaPlayer.start();
                    isReload = true;
                }

            }
        });
        videoPlayer.setOnVideoSizeChangedListener(new IMediaPlayer.OnVideoSizeChangedListener() {
            @Override
            public void onVideoSizeChanged(final IMediaPlayer iMediaPlayer, int i, int i1, int i2, int i3) {
                if (mUIHandler != null) {
                    Message.obtain(mUIHandler, MSG_VIDEO_PLAYER_SIZE_CHANGED, iMediaPlayer.getVideoWidth(), iMediaPlayer.getVideoHeight()).sendToTarget();
                }
            }
        });
        videoPlayer.setOnInfoListener(new IMediaPlayer.OnInfoListener() {
            @Override
            public boolean onInfo(IMediaPlayer iMediaPlayer, int what, int extra) {
                return true;
            }
        });
    }

    protected void waitUntilReady() {
        synchronized (mStartLock) {
            while (!mReady) {
                try {
                    mStartLock.wait();
                } catch (InterruptedException ie) { /* not expected */ }
            }
        }
    }

    /**
     * Shuts everything down.
     */
    protected void shutdown() {
        Log.i(TAG, "shutdown");
        Looper.myLooper().quit();
    }

    protected LiveAnimVideoHandler getHandler() {
        return mHandler;
    }

    protected void onResume() {
        if (videoPlayer != null && !videoPlayer.isPlaying())
            videoPlayer.start();
    }

    protected void onPause() {
        if (videoPlayer != null && videoPlayer.isPlaying())
            videoPlayer.pause();
    }

    protected void releaseMediaPlayer() {
        if (videoPlayer != null)
            videoPlayer.release();
    }

    protected void reloadVideo() {
        if (videoPlayer != null) {
            videoPlayer.release();
            videoPlayer = null;
        }
    }

    protected void restartVideo() {
        if (videoPlayer != null) {
            videoPlayer.stop();
            videoPlayer.prepareAsync();
        }
    }

    protected KSYMediaPlayer getVideoPlayer() {
        return videoPlayer;
    }

    /**
     * Handles the surface-created callback from SurfaceView.  Prepares GLES and the Surface.
     */
    protected void surfaceAvailable(SurfaceTexture holder, int width, int height) {

        Log.i(TAG, "RenderThread surfaceCreated holder=" + holder.hashCode());
        Surface surface = new Surface(holder);
        mWindowSurface1 = new WindowSurface(mEglCore, surface, false);
        synchronized (windowSurfacesMap) {
            windowSurfacesMap.put(holder, mWindowSurface1);
            mWindowSurface1.makeCurrent();
        }
        GLES20.glViewport(0, 0, width, height);

        if (windowSurfacesMap.size() <= 1) {
            // only create once

            mTextureId = getPreviewTexture();
            Log.i(TAG, "mTextureId=" + mTextureId);
            mCameraTexture = new SurfaceTexture(mTextureId);

            //                mGLCubeBuffer = ByteBuffer.allocateDirect(CUBE.length * 4)
            //                        .order(ByteOrder.nativeOrder())
            //                        .asFloatBuffer();
            //                mGLCubeBuffer.put(CUBE).position(0);
            //
            //                mGLTextureBuffer = ByteBuffer.allocateDirect(TEXTURE_NO_ROTATION.length * 4)
            //                        .order(ByteOrder.nativeOrder())
            //                        .asFloatBuffer();
            //                mGLTextureBuffer.put(TEXTURE_NO_ROTATION).position(0);

            mGLCubeBuffer = ByteBuffer.allocateDirect(CUBE.length * 4)
                    .order(ByteOrder.nativeOrder())
                    .asFloatBuffer();
            mGLCubeBuffer.put(CUBE).position(0);

            mGLTextureBuffer = ByteBuffer.allocateDirect(GPUImageExtRotationTexFilter.FULL_RECTANGLE_TEX_COORDS.length * 4)
                    .order(ByteOrder.nativeOrder())
                    .asFloatBuffer();
            mGLTextureBuffer.put(GPUImageExtRotationTexFilter.FULL_RECTANGLE_TEX_COORDS).position(0);


            Log.i(TAG, "surfaceChanged should only once here");
            gpuImageFilter = generateGPUImageFilter(-1);
            gpuImageFilter.init();

            surfaceWidth = width;
            surfaceHeight = height;
            L.i(TAG, "surfaceWidth=" + width + ",surfaceHeight=" + height);
            GLES20.glUseProgram(gpuImageFilter.getProgram());
            gpuImageFilter.onOutputSizeChanged(width, height);

            mCameraTexture.setOnFrameAvailableListener(this);
            finishSurfaceSetup();
        }

    }

    protected int getPreviewTexture() {
        int textureId = -1;
        if (textureId == GlUtil.NO_TEXTURE) {
            textureId = GlUtil.createTextureObject(GLES11Ext.GL_TEXTURE_EXTERNAL_OES);
        }
        return textureId;
    }

    /**
     * Releases most of the GL resources we currently hold (anything allocated by
     * surfaceAvailable()).
     * <p>
     * Does not clean EglCore.
     */
    private void releaseGl(SurfaceTexture surfaceHolder) {
        GlUtil.checkGlError("releaseGl start");

        WindowSurface windowSurface = windowSurfacesMap.get(surfaceHolder);
        if (windowSurface != null) {

            windowSurfacesMap.remove(surfaceHolder);
            windowSurface.release();
        }

        GlUtil.checkGlError("releaseGl done");

    }

    /**
     * Sets up anything that depends on the window size.
     * <p>
     * Open the camera (to set mCameraAspectRatio) before calling here.
     */
    private void finishSurfaceSetup() {
        videoPlayer.setSurface(new Surface(mCameraTexture));
    }

    @Override   // SurfaceTexture.OnFrameAvailableListener; runs on arbitrary thread
    public void onFrameAvailable(SurfaceTexture surfaceTexture) {
        Message.obtain(mHandler, MSG_FRAME_AVAILABLE);
    }

    /**
     * Handles incoming frame of data from the camera.
     */
    protected void frameAvailable() {
        mCameraTexture.updateTexImage();
        mCameraTexture.getTransformMatrix(mSTMatrix);
        draw();
    }

    /**
     * Handles the surfaceChanged message.
     * <p>
     * We always receive surfaceChanged() after surfaceCreated(), but surfaceAvailable()
     * could also be called with a Surface created on a previous run.  So this may not
     * be called.
     */
    protected void surfaceChanged(SurfaceTexture surfaceHolder, int width, int height) {
        Log.i(TAG, "RenderThread surfaceChanged " + width + "x" + height + ";surfaceHolder=" + surfaceHolder.hashCode());

    }

    /**
     * Handles the surfaceDestroyed message.
     */
    protected void surfaceDestroyed(SurfaceTexture surfaceHolder) {
        // In practice this never appears to be called -- the activity is always paused
        // before the surface is destroyed.  In theory it could be called though.
        //            Log.i(TAG, "RenderThread surfaceDestroyed holder=" + surfaceHolder.hashCode());
        releaseGl(surfaceHolder);
    }

    /**
     * Draws the scene and submits the buffer.
     */
    protected void draw() {

        if (gpuImageFilter != null) {
            GlUtil.checkGlError("draw start >");
            WindowSurface windowSurface = mWindowSurface1;
            windowSurface.makeCurrent();
            gpuImageFilter.onDraw(mTextureId, mGLCubeBuffer, mGLTextureBuffer);
            windowSurface.swapBuffers();
            GlUtil.checkGlError("draw done >");
        }
    }

    float[] mSTMatrix = new float[16];

    private GPUImageFilterGroup generateGPUImageFilter(@RawRes int filterId) {
        GPUImageFilterGroup gpuImageFilter = new GPUImageFilterGroup();
        //        gpuImageFilter.addFilter(new GPUImageExtTexFilter());
        GPUImageExtRotationTexFilter ext = new GPUImageExtRotationTexFilter();
        ext.setTexMatrix(mSTMatrix);
        gpuImageFilter.addFilter(ext);
        if (filterId == -1) {
            //如果没有为当前视频设置过滤镜参数,就为它设置个原始滤镜
            gpuImageFilter.addFilter(new GPUImageFilter("" +
                    "attribute vec4 position;\n" +
                    "attribute vec4 inputTextureCoordinate;\n" +
                    " \n" +
                    "varying vec2 textureCoordinate;\n" +
                    " \n" +
                    "void main()\n" +
                    "{\n" +
                    "    gl_Position = position;\n" +
                    "    textureCoordinate = inputTextureCoordinate.xy;\n" +
                    "}", "" +
                    "varying highp vec2 textureCoordinate;\n" +
                    " \n" +
                    "uniform sampler2D inputImageTexture;\n" +
                    " \n" +
                    "void main()\n" +
                    "{\n" +
                    "highp vec2 st = textureCoordinate;\n" +
                    "highp vec2 lst = st * vec2(0.5, 1.0);\n" +
                    "highp vec2 rst = lst + vec2(0.5, 0.0);\n" +
                    "highp vec4 imgOrigin = texture2D(inputImageTexture, lst);\n" +
                    "highp vec4 imgMask = texture2D(inputImageTexture, rst);\n" +
                    "highp vec3 imgRGB = imgOrigin.rgb;\n" +
                    "highp float imgAlpha = imgMask.r;\n" +
                    "gl_FragColor = vec4(imgRGB.r,imgRGB.g,imgRGB.b, imgAlpha);\n" +
                    "}"));
        } else {

            GPUImageLookupFilter lookupFilter = new GPUImageLookupFilter();
            lookupFilter.setBitmap(BitmapFactory.decodeResource(TheLApp.getContext().getResources(), filterId));
            gpuImageFilter.addFilter(lookupFilter);
        }
        return gpuImageFilter;
    }

    public void playVideo(Object obj) {
        final String url = (String) obj;
        if (TextUtils.isEmpty(url)) {
            return;
        }
        if (videoPlayer == null) {
            initPlayer();
        }
        try {
            if (!isReload) {
                videoPlayer.setDataSource(url);
                videoPlayer.prepareAsync();
            } else {
                videoPlayer.reload(url, true);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
