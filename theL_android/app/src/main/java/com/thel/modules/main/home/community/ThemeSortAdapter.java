package com.thel.modules.main.home.community;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.List;

/**
 * Created by chad
 * Time 17/10/18
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */

public class ThemeSortAdapter extends FragmentStatePagerAdapter {

    private final List<ThemeSortListFragment> fragments;
    private final List<String> mTitles;

    public ThemeSortAdapter(List<ThemeSortListFragment> fragments, FragmentManager manager, List<String> titles) {
        super(manager);
        this.fragments = fragments;
        this.mTitles = titles;
    }

    @Override
    public Fragment getItem(int position) {

        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTitles == null ? null : mTitles.get(position);
    }

}
