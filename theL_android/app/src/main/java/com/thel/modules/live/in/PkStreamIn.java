package com.thel.modules.live.in;

import android.graphics.Bitmap;

/**
 * Created by waiarl on 2017/12/5.
 * 直播PkStream
 */

public interface PkStreamIn extends StreamerIn {
    /**
     * 设置流的旋转角度，最高360
     *
     * @param rotate
     */
    void setRotateDegrees(int rotate);

    /**
     * 是否设置镜像
     *
     * @param show
     */
    void setFrointCameraMirror(boolean show);

    /**
     * the sub screen position
     * must be set before registerRTC
     * Pk对方的流的位置
     *
     * @param width  0~1 default value 0.35f
     * @param height 0~1 default value 0.3f
     * @param left   0~1 default value 0.65f
     * @param top    0~1 default value 0.f
     * @param mode   scaling mode
     */
    void setRTCSubScreenRect(float left, float top, float width, float height, int mode);

    /**
     * 设置本地camera的位置
     *
     * @param left
     * @param top
     * @param width
     * @param height
     */
    void setmCameraScreenRect(float left, float top, float width, float height);

    /**
     * 自己的流的类型
     *
     * @param mode
     */
    void setRTCMainScreen(int mode);

    /**
     * 开始PK推流
     *
     * @param channel 推流地址
     */
    void startRTC(String channel);

    /**
     * 停止PK推流
     */
    void stopRTC();

    /**
     * 设置背景图片
     *
     * @param bitmap
     */
    void showBgPicture(Bitmap bitmap);

    /**
     * 隐藏背景图片
     */
    void hideBgPicture();
}
