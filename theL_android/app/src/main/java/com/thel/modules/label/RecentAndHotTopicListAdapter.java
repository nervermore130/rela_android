package com.thel.modules.label;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.RecentAndHotTagBean;
import com.thel.constants.TheLConstants;

import java.util.ArrayList;


public class RecentAndHotTopicListAdapter extends BaseAdapter {

    private ArrayList<RecentAndHotTagBean> topiclist = new ArrayList<RecentAndHotTagBean>();

    private LayoutInflater mInflater;

    private Context mContext;

    public RecentAndHotTopicListAdapter(Context context, ArrayList<RecentAndHotTagBean> searchlist) {
        mContext = context;

        mInflater = (LayoutInflater) TheLApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        freshAdapter(searchlist);
    }

    public void freshAdapter(ArrayList<RecentAndHotTagBean> searchlist) {
        this.topiclist = searchlist;
    }

    @Override
    public int getCount() {
        return topiclist.size();
    }

    @Override
    public Object getItem(int position) {
        return topiclist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        HoldView holdView = null;
        //        if (convertView == null) {
        // 不缓存，否则滑动的时候标签宽度不对
        convertView = mInflater.inflate(R.layout.recent_and_hot_topic_listitem, parent, false);
        holdView = new HoldView();
        holdView.topic_name = convertView.findViewById(R.id.topic_name);
        holdView.tag = convertView.findViewById(R.id.tag);
        convertView.setTag(holdView); // 把holdview缓存下来
        //        } else {
        //            holdView = (HoldView) convertView.getTag();
        //        }

        RecentAndHotTagBean searchBean = topiclist.get(position);
        final GradientDrawable gradientDrawable = (GradientDrawable) holdView.topic_name.getBackground();
        if (TextUtils.isEmpty(searchBean.topicColor)) {
            gradientDrawable.setColor(ContextCompat.getColor(TheLApp.getContext(), R.color.bg_green));
        } else {
            gradientDrawable.setColor(Color.parseColor("#" + searchBean.topicColor));
        }
        holdView.topic_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra(TheLConstants.BUNDLE_KEY_TOPIC_NAME, ((TextView) view).getText());
                ((Activity)mContext).setResult(TheLConstants.RESULT_CODE_WRITE_MOMENT_SELECT_TOPIC, intent);
                ((Activity)mContext).finish();
            }
        });
        holdView.topic_name.setText(searchBean.topicName);
        if (!TextUtils.isEmpty(searchBean.recentOrHotTag)) {
            holdView.tag.setText(searchBean.recentOrHotTag);
            holdView.tag.setVisibility(View.VISIBLE);
        } else {
            holdView.tag.setVisibility(View.GONE);
        }

        return convertView;
    }

    class HoldView {
        TextView tag;
        TextView topic_name;
    }

}
