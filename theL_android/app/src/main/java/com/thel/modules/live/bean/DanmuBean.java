package com.thel.modules.live.bean;

/**
 * Created by the L on 2016/9/8.
 * 弹幕对象
 */
public class DanmuBean {
    public String id;
    /**
     * 用户userId
     */
    public String userId;
    /**
     * 用户头像
     */
    public String avatar;
    /**
     * 用户会员等级
     */
    public int vip;
    /**
     * 用户昵称
     */
    public String nickName;
    /**
     * 弹幕内容
     */
    public String content;
    /**
     * 弹幕发送时间
     */
    public String time;
    /**
     * 弹幕类型，目前仅msg
     */
    public String type;

    /**
     * 用户等级
     */
    public int userLevel;

    /**
     * 弹幕种类
     */
    public String barrageId;

    /**
     * 是否显示等级图标
     */
    public int iconSwitch;

}
