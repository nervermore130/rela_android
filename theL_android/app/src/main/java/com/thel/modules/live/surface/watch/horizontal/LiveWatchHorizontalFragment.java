package com.thel.modules.live.surface.watch.horizontal;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.postprocessors.IterativeBoxBlurPostProcessor;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.pili.pldroid.player.AVOptions;
import com.pili.pldroid.player.widget.PLVideoTextureView;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.LiveAdInfoBean;
import com.thel.bean.LiveInfoLogBean;
import com.thel.bean.RecommendedModel;
import com.thel.bean.analytics.AnalyticsBean;
import com.thel.bean.live.LiveMultiSeatBean;
import com.thel.bean.message.DanmuResultBean;
import com.thel.callback.imp.OnEditTouchListener;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.growingio.GrowingIoConstant;
import com.thel.growingio.GIoGiftTrackBean;
import com.thel.imp.BaseFunctionFragment;
import com.thel.imp.black.BlackUtils;
import com.thel.imp.follow.FollowStatusChangedImpl;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.live.adapter.LiveRoomChatAdapter;
import com.thel.modules.live.bean.DanmuBean;
import com.thel.modules.live.bean.GiftSendMsgBean;
import com.thel.modules.live.bean.LiveRoomBean;
import com.thel.modules.live.bean.LiveRoomMsgBean;
import com.thel.modules.live.bean.LiveUserCardBean;
import com.thel.modules.live.bean.SendGiftSuccessBean;
import com.thel.modules.live.bean.SoftEnjoyBean;
import com.thel.modules.live.bean.SoftEnjoyNetBean;
import com.thel.modules.live.bean.SoftGiftBean;
import com.thel.modules.live.ctrl.GiftMsgCtrl;
import com.thel.modules.live.in.GiftLianSendCallback;
import com.thel.modules.live.in.LiveTextSizeChangedListener;
import com.thel.modules.live.liveBigGiftAnimLayout.LiveBigGiftAnimLayout;
import com.thel.modules.live.liveBigGiftAnimLayout.VideoAnimListener;
import com.thel.modules.live.utils.LiveShowDialogUtils;
import com.thel.modules.live.utils.LiveUtils;
import com.thel.modules.live.view.DanmuLayoutView;
import com.thel.modules.live.view.LiveShowCloseView;
import com.thel.modules.live.view.LiveUserAnchorCardDialogView;
import com.thel.modules.live.view.LiveUserCardDialogView;
import com.thel.modules.live.view.LiveVipEnterView;
import com.thel.modules.live.view.SoftGiftListView;
import com.thel.modules.live.view.SoftGiftMsgLayout;
import com.thel.modules.live.view.expensive.LiveTopGiftLayout;
import com.thel.modules.live.view.expensive.TopGiftBean;
import com.thel.modules.live.view.expensive.TopGiftItemImp;
import com.thel.modules.main.home.moments.ReportActivity;
import com.thel.modules.main.home.moments.SendCardActivity;
import com.thel.modules.welcome.WelcomeActivity;
import com.thel.ui.dialog.RechargeDialogFragment;
import com.thel.ui.widget.DanmuSelectedLayout;
import com.thel.chat.tlmsgclient.IMsgClient;
import com.thel.ui.popupwindow.GiftHorizontalPopupWindow;
import com.thel.ui.widget.LiveAdLayout;
import com.thel.ui.widget.ScaleRecyclerView;
import com.thel.ui.widget.SoftInputViewGroup;
import com.thel.ui.widget.TimeTextView;
import com.thel.utils.AppInit;
import com.thel.utils.DialogUtil;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.GsonUtils;
import com.thel.utils.ImageUtils;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SizeUtils;
import com.thel.utils.TextLimitWatcher;
import com.thel.utils.ToastUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LiveWatchHorizontalFragment extends BaseFunctionFragment implements LiveWatchHorizontalContract.View {

    private static final String TAG = "LiveWatchHorizontalFragment";

    @BindView(R.id.videoView)
    PLVideoTextureView videoView;

    @BindView(R.id.img_bg)
    SimpleDraweeView img_bg;

    @BindView(R.id.avatar)
    ImageView avatar;

    @BindView(R.id.txt_nickname)
    TextView txt_nickname;

    @BindView(R.id.txt_audience)
    TextView txt_audience;

    @BindView(R.id.img_live_type)
    ImageView img_live_type;

    @BindView(R.id.txt_watch_live)
    TextView txt_watch_live;

    @BindView(R.id.txt_follow)
    TextView txt_follow;

    @BindView(R.id.txt_soft_money)
    TextView txt_soft_money;

    @BindView(R.id.listview_chat)
    ScaleRecyclerView listview_chat;

    @BindView(R.id.chat_iv)
    ImageView chat_iv;

    @BindView(R.id.img_emoji)
    ImageView img_emoji;

    @BindView(R.id.live_vip_enter_view)
    LiveVipEnterView live_vip_enter_view;

    @BindView(R.id.gift_msg_view)
    SoftGiftMsgLayout gift_msg_view;

    @BindView(R.id.danmu_layout_view)
    DanmuLayoutView danmu_layout_view;

    @BindView(R.id.close_iv)
    ImageView close_iv;

    @BindView(R.id.img_send_anchor)
    ImageView img_send_anchor;

    @BindView(R.id.send_edit_msg)
    RelativeLayout send_edit_msg;

    @BindView(R.id.img_switch_danmu)
    ImageView img_switch_danmu;

    @BindView(R.id.send_tv)
    TextView send_tv;

    @BindView(R.id.edit_input)
    EditText edit_input;

    @BindView(R.id.big_gift_layout)
    LiveBigGiftAnimLayout big_gift_layout;

    @BindView(R.id.live_show_close_view)
    LiveShowCloseView live_show_close_view;

    @BindView(R.id.root_vg)
    SoftInputViewGroup root_vg;

    @BindView(R.id.rel_top)
    RelativeLayout rel_top;

    @BindView(R.id.danmu_layout)
    DanmuSelectedLayout danmu_layout;

    @BindView(R.id.ad_view)
    SimpleDraweeView ad_view;

    @BindView(R.id.top_tv)
    TextView top_tv;

    @BindView(R.id.offset_tv)
    TextView offset_tv;

    @BindView(R.id.layout_live_ad)
    LiveAdLayout layout_live_ad;

    @BindView(R.id.layer_view)
    View layer_view;

    @BindView(R.id.live_top_gift_layout)
    LiveTopGiftLayout live_top_gift_layout;

    private String liveId = null;

    private String userId = null;

    private List<LiveRoomMsgBean> msgList = Collections.synchronizedList(new ArrayList<>());

    private List<LiveRoomMsgBean> msgCacheList = Collections.synchronizedList(new ArrayList<>());

    private LiveRoomChatAdapter adapter;

    private LinearLayoutManager recyclerManager;

    private LiveWatchHorizontalContract.Presenter presenter;

    private SoftEnjoyBean softEnjoyBean;

    private List<GiftSendMsgBean> giftMsgWaitList = new ArrayList<>();//因为各种原因而 数据不全 不能显示的数据

    private List<GiftSendMsgBean> bigAnimGiftList = new ArrayList<>();//大动画播放列表

    private List<SoftGiftBean> offlineGiftList = new ArrayList<>();

    private GiftMsgCtrl mGiftMsgCtrl;

    private boolean isShowBigAnim = false;

    private boolean isGettingGiftData = false;//是否正在获取礼物列表数据

    private boolean liveClosed = false;

    private LiveRoomBean liveRoomBean;

    private String reportContent;

    private LiveShowDialogUtils dialogUtils;

    private TextLimitWatcher textWatcher;

    private int barrageId = 0;//当barrageId为0是为普通消息，大于0时为弹幕消息

    private static String GIFT_SEND_TYPE_SEND = "sendgift";

    private static String GIFT_SEND_TYPE_COMBO = "combogift";

    private int combo = 0;//连击次数,同一礼物连续发送次数，最开始为0

    private long intoTime; //刚进入直播间

    private long outTime;

    private static boolean hasSend = false;  //已经送过礼物

    private GiftHorizontalPopupWindow mGiftPopupWindow;

    public IMsgClient client;
    private LiveUserCardDialogView live_user_card_dialog_view;
    private LiveUserAnchorCardDialogView live_user_anchor_card_dialog_view;

    public static LiveWatchHorizontalFragment getFragment() {
        return new LiveWatchHorizontalFragment();
    }

    @Override
    public void setPresenter(LiveWatchHorizontalContract.Presenter presenter) {

        this.presenter = presenter;

        if (liveId != null || userId != null) {

            presenter.getLiveRoomDetail(userId, liveId);

        }

        presenter.initChatListUpdateTimer();

        presenter.subscribe();

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_live_watch_horizontal, container, false);

        ButterKnife.bind(this, view);

        initView();

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initChatList();

        initListener();

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getActivity() != null && isAdded() && getActivity().getIntent() != null) {

            Bundle bundle = getActivity().getIntent().getExtras();

            if (bundle != null) {

                userId = bundle.getString(TheLConstants.BUNDLE_KEY_USER_ID);

                liveId = bundle.getString(TheLConstants.BUNDLE_KEY_ID);

            }

            final String action = getActivity().getIntent().getAction();
            if (Intent.ACTION_VIEW.equals(action)) {//如果是从外部进入
                if (!ShareFileUtils.getBoolean(ShareFileUtils.HAS_LOGGED, false)) {//如果没登陆
                    startActivity(new Intent(getActivity(), WelcomeActivity.class));
                    getActivity().finish();
                    return;
                }
                Uri uri = getActivity().getIntent().getData();
                if (uri != null) {
                    liveId = uri.getQueryParameter("liveId");
                    userId = uri.getQueryParameter("userId");
                }
            }

        }

        initGift();

        new LiveWatchHorizontalPresenter(this);

        mGiftMsgCtrl = new GiftMsgCtrl(gift_msg_view);

        dialogUtils = new LiveShowDialogUtils();


    }

    @Override
    public void onDestroy() {

        if (presenter != null) {
            presenter.unSubscribe();
        }

        if (videoView != null) {
            videoView.stopPlayback();
            videoView = null;
        }

        if (msgCacheList != null) {
            msgCacheList.clear();
            msgCacheList = null;
        }

        if (msgList != null) {
            msgList.clear();
            msgList = null;
        }

        HorizontalLiveChat.getInstance().close();

        super.onDestroy();

    }

    private void initView() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            rel_top.setPadding(0, SizeUtils.dip2px(TheLApp.context, 24), 0, 0);

        }
        root_vg.setOnSoftInputShowListener(mOnSoftInputShowListener);

        root_vg.setNotInterceptViewsId(new int[]{R.id.send_edit_msg, R.id.danmu_layout});

        textWatcher = new TextLimitWatcher(100, true, "");

        edit_input.addTextChangedListener(textWatcher);

    }

    private void initChatList() {

        adapter = new LiveRoomChatAdapter(msgList, LiveTextSizeChangedListener.TEXT_SIZE_STANDARD);

        recyclerManager = new LinearLayoutManager(getActivity());

        recyclerManager.setOrientation(LinearLayoutManager.VERTICAL);

        listview_chat.setLayoutManager(recyclerManager);

        listview_chat.setAdapter(adapter);

    }

    private void initListener() {

        adapter.setLiveChatListener(new LiveRoomChatAdapter.ChatItemClickListener() {
            @Override
            public void clickAvatar(View v, int position, LiveRoomMsgBean bean) {
                if (!TextUtils.isEmpty(bean.content)) {
                    reportContent = bean.nickName + ":" + bean.content;
                }
                if (!TextUtils.isEmpty(bean.userId)) {
                    presenter.getLiveUserCard(bean.userId, LiveWatchHorizontalPresenter.USER_CARD);
                }
            }

            @Override
            public void clickText(View v, int position, LiveRoomMsgBean bean) {
                if (!TextUtils.isEmpty(bean.content)) {
                    reportContent = bean.nickName + ":" + bean.content;
                }
                if (!TextUtils.isEmpty(bean.userId)) {
                    presenter.getLiveUserCard(bean.userId, LiveWatchHorizontalPresenter.USER_CARD);
                }
            }
        });

        big_gift_layout.setVideoAnimListener(new VideoAnimListener() {
            @Override
            public void complete() {
                playNextBigAnim();
            }
        });

        edit_input.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    if (!TextUtils.isEmpty(edit_input.getText().toString().trim())) {
                        if (barrageId <= 0) {
                            sendMsg();
                        } else {
                            sendDanmu();
                        }
                    }
                    return true;
                }
                return false;
            }
        });

        edit_input.setOnTouchListener(new OnEditTouchListener() {
            @Override
            protected void onTouch(boolean isSingleClick) {

                if (isSingleClick) {
                    root_vg.showKeyBoardAndHideDanmuView();
                }
            }

        });

        send_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(edit_input.getText().toString().trim())) {
                    if (barrageId <= 0) {
                        sendMsg();
                    } else {
                        sendDanmu();
                    }
                }
            }
        });

        danmu_layout.setOnDanmuSelectedListener(new DanmuSelectedLayout.OnDanmuSelectedListener() {
            @Override
            public void onDanmuSelected(String selectedType) {
                switch (selectedType) {
                    case DanmuSelectedLayout.TYPE_DANMU_CLOSE:

                        img_switch_danmu.setImageResource(R.mipmap.btn_dan);

                        barrageId = 0;

                        root_vg.hideDanmu(true);

                        edit_input.setHint(TheLApp.context.getResources().getString(R.string.chat_activity_input_hint));

                        break;
                    case DanmuSelectedLayout.TYPE_DANMU_LEVEL:

                        img_switch_danmu.setImageResource(R.mipmap.btn_level_barrage);

                        barrageId = 1;

                        setInputHint(2);

                        break;
                    case DanmuSelectedLayout.TYPE_DANMU_BOTTLE:

                        img_switch_danmu.setImageResource(R.mipmap.btn_bottle);

                        barrageId = 2;

                        setInputHint(3);

                        break;
                    case DanmuSelectedLayout.TYPE_DANMU_CAT:

                        img_switch_danmu.setImageResource(R.mipmap.btn_cat);

                        barrageId = 3;

                        setInputHint(3);

                        break;
                    case DanmuSelectedLayout.TYPE_DANMU_CUPID:

                        img_switch_danmu.setImageResource(R.mipmap.btn_cupid);

                        barrageId = 4;

                        setInputHint(3);

                        break;
                    default:

                        img_switch_danmu.setImageResource(R.mipmap.btn_dan);

                        barrageId = 0;

                        break;
                }
            }
        });
        //显示礼物打赏界面
        img_emoji.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showGiftPW();

            }
        });

        live_top_gift_layout.setTopGiftClickListener(new TopGiftItemImp.TopGiftClickListener() {
            @Override
            public void clickSender(String userId) {
                if (presenter != null) {
                    presenter.getLiveUserCard(String.valueOf(liveRoomBean.user.id), LiveWatchHorizontalPresenter.USER_CARD);
                }
            }

            @Override
            public void clickReceiver(String userId) {
                if (presenter != null) {
                    presenter.getLiveUserCard(String.valueOf(liveRoomBean.user.id), LiveWatchHorizontalPresenter.ANCHOR_CARD);
                }
            }

            @Override
            public void clickJumpToLive(String userId) {

            }
        });

    }

    private ILiveChatHorizontal mILiveChatHorizontal = new ILiveChatHorizontal() {

        @Override
        public void clearInput() {
            edit_input.setText("");
        }

        @Override
        public void autoPingServer() {

        }

        @Override
        public void liveClosed() {

            liveClosed = true;
            if (getActivity() != null && !getActivity().isDestroyed() && dialogUtils != null && !dialogUtils.isDialogShowing()) {
                if ((getActivity().getRequestedOrientation() != ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)) {
                    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    showLiveClosedDialog();
                } else {
                    showLiveClosedDialog();
                }
            }

        }

        @Override
        public void showDialogChannelId() {
            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ToastUtils.showCautionToastShort(getActivity(), getString(R.string.live_notfound));
                    }
                });
            }
        }

        @Override
        public void showDialogRequest() {

            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ToastUtils.showCautionToastShort(getActivity(), getString(R.string.something_is_wrong));

                    }
                });
            }

        }

        @Override
        public void showAlertUserId() {
            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        DialogUtil.showAlert(getActivity(), "", getString(R.string.create_rela_id), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getActivity().finish();
                            }
                        });
                    }
                });
            }

        }

        @Override
        public void showAlertUserName() {

            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        DialogUtil.showAlert(getActivity(), "", getString(R.string.create_username), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getActivity().finish();
                            }
                        });
                    }
                });
            }
        }

        @Override
        public void updateBalance(String result) {

            try {
                if (getActivity() != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override public void run() {

                            SendGiftSuccessBean senGiftSuccessBean = GsonUtils.getObject(result, SendGiftSuccessBean.class);

                            int balance = senGiftSuccessBean.goldBalance;
                            if (balance < 0) {
                                return;
                            }

                            softEnjoyBean.gold = balance;

                            if (mGiftPopupWindow != null && mGiftPopupWindow.isShowing()) {
                                mGiftPopupWindow.updateBalance(balance);
                            }

                            if (danmu_layout != null) {
                                danmu_layout.setGold(balance);
                            }
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public void beenBlocked() {
            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        DialogUtil.showToastShort(getActivity(), getString(R.string.live_you_have_been_baned));

                    }
                });
            }
        }

        @Override
        public void speakTooFast() {

            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        DialogUtil.showToastShort(getActivity(), getString(R.string.info_speak_to_fast));

                    }
                });
            }
        }

        @Override
        public void lockInput() {
            edit_input.setText("");
        }

        @Override
        public void openInput() {

        }

        @Override
        public void setDanmuResult(String result) {

            try {

                DanmuResultBean danmuResultBean = GsonUtils.getObject(result, DanmuResultBean.class);

                if (danmuResultBean.success) {
                    if (danmuResultBean.goldBalance != -1) {
                        softEnjoyBean.gold = danmuResultBean.goldBalance;
                        if (mGiftPopupWindow != null && mGiftPopupWindow.isShowing()) {

                            mGiftPopupWindow.updateBalance(danmuResultBean.goldBalance);
                        }
                        if (danmu_layout != null) {
                            danmu_layout.setGold(danmuResultBean.goldBalance);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public void refreshMsg(LiveRoomMsgBean liveRoomMsgBean) {

        }

        @Override
        public void updateAudienceCount() {

            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (liveRoomBean != null) {
                            txt_watch_live.setText(liveRoomBean.liveUsersCount + "");
                            if (liveRoomBean.rank == 0)
                                txt_audience.setText("");
                            else if (liveRoomBean.rank == 1) //第一名
                                txt_audience.setText(TheLApp.context.getString(R.string.rank1, liveRoomBean.rank) + " | ");
                            else if (liveRoomBean.rank == 2)//第二名
                                txt_audience.setText(getString(R.string.rank2, liveRoomBean.rank) + " | ");
                            else if (liveRoomBean.rank == 3)//第三名
                                txt_audience.setText(getString(R.string.rank3, liveRoomBean.rank) + " | ");
                            else                            //其他
                                txt_audience.setText(getString(R.string.rank, liveRoomBean.rank) + " | ");
                            txt_soft_money.setText(getString(R.string.money_soft, liveRoomBean.gem));
                        }
                    }
                });
            }
        }

        @Override
        public void parseGiftMsg(String code, String payload) {

            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        final GiftSendMsgBean msgBean = GsonUtils.getObject(payload, GiftSendMsgBean.class);
                        if (msgBean == null) {
                            return;
                        }

                        if (code != null && code.equals(LiveRoomMsgBean.CODE_SPECIAL_GIFT)) {
                            showGiftMsgData(msgBean);
                            return;
                        }

                        L.d(TAG, " parseGiftMsg msgBean.ownerGem : " + msgBean.ownerGem);

                        if (!msgBean.ownerGem.equals("-1")) {//如果服务器有返回，就更新主播软妹币
                            txt_soft_money.setText(getString(R.string.money_soft, msgBean.ownerGem));
                        }

                        L.d(TAG, " parseGiftMsg softEnjoyBean : " + softEnjoyBean);

                        if (softEnjoyBean == null) {//如果没有礼物列表数据，因为有些打赏的显示数据无法获取，所以要重新请求礼物列表数据，并且把这个消息放到消息队列里去，等请求完再显示
                            putGiftMsgIntoWaitList(msgBean);//把礼物消息放到等待消息队里中
                            getGiftListNetData();
                            return;
                        }

                        if (getGiftMsgDataFromMemory(msgBean)) {//能够从本地内存获取数据
                            showGiftMsgData(msgBean);//显示消息
                        } else {
                            putGiftMsgIntoWaitList(msgBean);//不能，则连网请求数据
                            getSingleGift(msgBean.id);
                        }
                    }
                });
            }

            L.d(TAG, " parseGiftMsg parseGiftMsg : " + payload);


        }

        @Override
        public void showDanmu(String payload) {
            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        final DanmuBean danmuBean = GsonUtils.getObject(payload, DanmuBean.class);
                        danmu_layout_view.receiveMsg(danmuBean);
                    }
                });
            }
        }

        @Override
        public void freeBarrage() {

            if (danmu_layout != null && !danmu_layout.isFreeBarrage()) {

                danmu_layout.showFreeBarrage();
                if (softEnjoyBean != null) {
                    danmu_layout.setGold(softEnjoyBean.gold);
                }

            }
        }

        @Override
        public void joinVipUser(LiveRoomMsgBean liveRoomMsgBean) {

            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (null != live_vip_enter_view) {
                            live_vip_enter_view.joinNewUser(liveRoomMsgBean);
                        }
                    }
                });
            }

        }

        @Override
        public void refreshMsg() {

        }

        @Override
        public void addLiveRoomMsg(LiveRoomMsgBean liveRoomMsgBean) {

            if (msgCacheList != null) {
                msgCacheList.add(liveRoomMsgBean);
            }

        }

        @Override
        public void showTopGiftView(TopGiftBean topGiftBean) {

            if (topGiftBean == null || softEnjoyBean == null) {
                return;
            }
            final SoftGiftBean softGiftBean = LiveUtils.getGiftBeanById(topGiftBean.id, softEnjoyBean);
            if (softGiftBean == null) {
                return;
            }
            topGiftBean.softGiftBean = softGiftBean;
            live_top_gift_layout.receiveGift(topGiftBean);
        }

        @Override
        public void updateBroadcasterNetStatus(int status) {

        }
    };

    @Override
    public void showBlockAlertDialog() {

        if (getActivity() != null && !getActivity().isDestroyed()) {

            DialogUtil.showAlert(getActivity(), "", getString(R.string.info_had_bean_added_to_black), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    if (getActivity() != null && !getActivity().isDestroyed()) {
                        getActivity().finish();
                    }
                }
            });
        }

    }

    @Override
    public void requestFinish() {
        closeLoading();
    }

    @Override
    public void updateChatList() {

        if (msgList != null && msgCacheList != null && msgCacheList.size() > 0) {

            if (msgList.size() > 50) {
                msgList.remove(0);
            }

            if (msgCacheList.size() > 3) {
                List<LiveRoomMsgBean> liveRoomMsgBeans = msgCacheList.subList(0, 2);
                msgList.addAll(liveRoomMsgBeans);
                msgCacheList.removeAll(liveRoomMsgBeans);
            } else {
                msgList.addAll(msgCacheList);
                msgCacheList.clear();
            }

            if (adapter != null) {
                adapter.notifyDataSetChanged();
                recyclerManager.scrollToPosition(msgList.size() - 1);
            }
        }

    }

    @Override
    public void showLiveUserCard(LiveUserCardBean liveUserCardBean) {
        if (getActivity() == null) {
            return;
        }
        final Bundle bundle = new Bundle();
        bundle.putSerializable(LiveUserCardDialogView.BUNDLE_KEY_LIVE_USER_CARD_BEAN, liveUserCardBean);
        bundle.putBoolean(LiveUserCardDialogView.BUNDLE_KEY_CAN_SHARE, liveRoomBean.user.id == liveUserCardBean.userId);
        bundle.putString(LiveUserCardDialogView.BUNDLE_KEY_SHARE_TITLE, liveRoomBean.share.title);
        bundle.putString(LiveUserCardDialogView.BUNDLE_KEY_CONTENT, liveRoomBean.share.text);
        bundle.putString(LiveUserCardDialogView.BUNDLE_KEY_SINGLE_TITLE, liveRoomBean.share.title);
        bundle.putString(LiveUserCardDialogView.BUNDLE_KEY_IMAGE_URL, liveRoomBean.share.link);
        bundle.putBoolean(LiveUserCardDialogView.BUNDLE_KEY_IS_BROAD_CASTER, false);
        bundle.putInt(LiveUserCardDialogView.BUNDLE_KEY_LIVE_TYPE, liveRoomBean.audioType);
        live_user_card_dialog_view = LiveUserCardDialogView.getInstance(bundle);
        live_user_card_dialog_view.setReportListener(new ReportListener(liveUserCardBean.userId + ""));
        live_user_card_dialog_view.setDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                reportContent = "";
            }
        });
        live_user_card_dialog_view.show(getActivity().getSupportFragmentManager(), getClass().getSimpleName());
    }

    @Override
    public void showLiveAnchorUserCard(LiveUserCardBean liveUserCardBean) {
        if (getActivity() == null) {
            return;
        }
        if (!canGetAnchor()) {
            return;
        }
        final Bundle bundle = new Bundle();
        bundle.putSerializable(LiveUserCardDialogView.BUNDLE_KEY_LIVE_USER_CARD_BEAN, liveUserCardBean);
        bundle.putBoolean(LiveUserCardDialogView.BUNDLE_KEY_CAN_SHARE, false);
        bundle.putString(LiveUserCardDialogView.BUNDLE_KEY_SHARE_TITLE, liveRoomBean.share.title);
        bundle.putString(LiveUserCardDialogView.BUNDLE_KEY_CONTENT, liveRoomBean.share.text);
        bundle.putString(LiveUserCardDialogView.BUNDLE_KEY_SINGLE_TITLE, liveRoomBean.share.title);
        bundle.putString(LiveUserCardDialogView.BUNDLE_KEY_IMAGE_URL, liveRoomBean.share.link);
        bundle.putBoolean(LiveUserCardDialogView.BUNDLE_KEY_IS_BROAD_CASTER, false);
        bundle.putInt(LiveUserCardDialogView.BUNDLE_KEY_LIVE_TYPE, liveRoomBean.audioType);
        live_user_anchor_card_dialog_view = LiveUserAnchorCardDialogView.getInstance(bundle);
        live_user_anchor_card_dialog_view.setReportListener(new ReportListener(liveUserCardBean.userId + ""));
        live_user_anchor_card_dialog_view.setDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                reportContent = "";
            }
        });
        live_user_anchor_card_dialog_view.show(getActivity().getSupportFragmentManager(), getClass().getSimpleName());
    }

    @Override
    public void showLiveGiftList(SoftEnjoyBean softEnjoyBean) {
        if (getActivity() == null || softEnjoyBean == null) {
            return;
        }
        this.softEnjoyBean.list = softEnjoyBean.list;

        showGiftListData();//显示礼物打赏界面
    }

    @Override
    public void showAdView(LiveAdInfoBean liveAdInfoBean) {

        layout_live_ad.setData(liveAdInfoBean);

    }


    @Override
    public void initPlayer(LiveRoomBean data) {

        if (data != null && data.liveUrl != null) {
            AVOptions options = new AVOptions();
            options.setInteger(AVOptions.KEY_PREPARE_TIMEOUT, 10 * 1000);
            options.setInteger(AVOptions.KEY_LIVE_STREAMING, 1);
            options.setInteger(AVOptions.KEY_MEDIACODEC, AVOptions.MEDIA_CODEC_HW_DECODE);
            options.setInteger(AVOptions.KEY_LOG_LEVEL, 5);
            options.setInteger(AVOptions.KEY_VIDEO_DATA_CALLBACK, 1);
            options.setInteger(AVOptions.KEY_AUDIO_DATA_CALLBACK, 1);
            options.setInteger(AVOptions.KEY_FAST_OPEN, 1);
            options.setInteger(AVOptions.KEY_CACHE_BUFFER_DURATION, 500);
            options.setInteger(AVOptions.KEY_MAX_CACHE_BUFFER_DURATION, 4000);
            videoView.setAVOptions(options);
            videoView.setVideoPath(data.liveUrl);
            videoView.setCoverView(img_bg);
            videoView.setDisplayAspectRatio(PLVideoTextureView.ASPECT_RATIO_PAVED_PARENT);
            videoView.start();
        }

    }

    @Override
    public void initUI(LiveRoomBean liveRoomBean) {

        this.liveRoomBean = liveRoomBean;
        this.softEnjoyBean = liveRoomBean.softEnjoyBean;

        if (adapter != null) {

            adapter.setLiveRoomBean(liveRoomBean);

        }

        if (!TextUtils.isEmpty(liveRoomBean.imageUrl)) {
            setSimpleBlur(img_bg, liveRoomBean.imageUrl, AppInit.displayMetrics.widthPixels, AppInit.displayMetrics.heightPixels);
        }

        ImageLoaderManager.imageLoaderDefaultCircle(avatar, R.mipmap.icon_user, liveRoomBean.user.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE);

        final String nicknameStr = Utils.getLimitedStr(liveRoomBean.user.nickName, 14);
        txt_nickname.setText(nicknameStr);

        txt_watch_live.setText(String.valueOf(liveRoomBean.liveUsersCount));

        if (!TextUtils.isEmpty(liveRoomBean.gem)) {
            txt_soft_money.setText(getString(R.string.money_soft, liveRoomBean.gem));
        } else {
            txt_soft_money.setText("");
        }

        if (liveRoomBean.user != null) {
            refreshFollowBtn(liveRoomBean.user.isFollow);
        }

        if (danmu_layout != null) {
            danmu_layout.setGold(liveRoomBean.gold);
        }

        L.d("LiveWatchHorizontalFragment", " liveRoomBean.activityInfoUrl : " + liveRoomBean.activityInfoUrl);

        if (liveRoomBean.activityInfoUrl != null) {
            presenter.getLiveAdInfo(liveRoomBean.activityInfoUrl);
        }

    }

    private void initGift() {

        String json = ShareFileUtils.getString(ShareFileUtils.GIFT_LIST, "{}");

        SoftEnjoyNetBean softEnjoyNetBean = GsonUtils.getObject(json, SoftEnjoyNetBean.class);

        if (softEnjoyNetBean != null && softEnjoyNetBean.data != null) {

            this.softEnjoyBean = softEnjoyNetBean.data;

        }
    }

    @Override
    public void initChat(LiveRoomBean data) {

        HorizontalLiveChat.getInstance().createClient(data, mILiveChatHorizontal);

    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @OnClick(R.id.chat_iv)
    void showChatInput() {
        send_edit_msg.setVisibility(View.VISIBLE);
        showKeyboard();
    }

    @OnClick(R.id.close_iv)
    void onClose() {
        if (getActivity() != null) {
            getActivity().finish();
        }
    }

    @OnClick(R.id.img_send_anchor)
    void onShare(View v) {
        ViewUtils.preventViewMultipleClick(v,2000);
        if (UserUtils.isVerifyCell()) {
            close_iv.setEnabled(false);
            //关播防误触保护
            v.postDelayed(new Runnable() {
                @Override
                public void run() {
                    close_iv.setEnabled(true);
                }
            }, 500);
            sendCard();
        }
    }

    @OnClick(R.id.avatar)
    void showAnchorUserCard() {
        if (presenter != null && liveRoomBean != null && liveRoomBean.user != null) {
            presenter.getLiveUserCard(String.valueOf(liveRoomBean.user.id), LiveWatchHorizontalPresenter.ANCHOR_CARD);
        }
    }

    @OnClick(R.id.txt_follow)
    void followUser() {
        ViewUtils.preventViewMultipleClick(txt_follow, 2000);

        if (liveRoomBean != null && liveRoomBean.user != null) {
            FollowStatusChangedImpl.followUserWithNoDialog(liveRoomBean.user.id + "", ACTION_TYPE_FOLLOW, liveRoomBean.user.nickName, liveRoomBean.user.avatar);
        }
    }

    @OnClick(R.id.layer_view)
    void onDanmuLayerClick() {

        if (root_vg.isSoftExtend()) {
            root_vg.toggleInput();
        }

        if (root_vg.isDanmuShowing()) {
            root_vg.hideDanmu(false);
        }
    }

    /**
     * 发送弹幕（取消发送弹幕）
     */
    @OnClick(R.id.img_switch_danmu)
    void switchDanmuView() {

        L.d(TAG, " root_vg.isDanmuShowing() : " + root_vg.isDanmuShowing());

        if (!root_vg.isDanmuShowing()) {
            if (textWatcher != null) {
                textWatcher.setParams(60, true, getString(R.string.danmu_toast));
            }
            final String inputSt = edit_input.getText().toString().trim();
            if (Utils.charCount(inputSt) > 60) {
                edit_input.setText(Utils.getLimitedStr(inputSt, 60));
                edit_input.setSelection(edit_input.getText().length());
            }
            root_vg.showDanmu();
        }

    }

    private void setSimpleBlur(SimpleDraweeView simpleDraweeView, String url, int width, int height) {
        if (simpleDraweeView != null && url != null) {
            simpleDraweeView.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(url, width, height))).setPostprocessor(new IterativeBoxBlurPostProcessor(10, 10)).build()).setAutoPlayAnimations(true).build());
        }
    }

    /**
     * 处理礼物赠送广播消息
     *
     * @param msgBean
     */
    private void showGiftMsgData(GiftSendMsgBean msgBean) {

        L.d(TAG, " parseGiftMsg msgBean.mark : " + msgBean.mark);

        if (!TextUtils.isEmpty(msgBean.mark) || (msgBean.videoUrl != null && msgBean.videoUrl.endsWith(".mp4"))) {//如果大礼物标识不为空，说明是大动画
            putGiftMsgIntoBitAnimList(msgBean);//把礼物放到大动画播放列表中
        } else {
            mGiftMsgCtrl.receiveGiftMsg(msgBean);//处理接受到的礼物消息，显示
        }
    }

    /**
     * 把礼物放到大动画播放列表
     *
     * @param msgBean 要播放大动画的礼物
     */
    private void putGiftMsgIntoBitAnimList(GiftSendMsgBean msgBean) {
        final String mark = msgBean.mark;

        //如果标志在已有大动画里面
        if (mark.equals(TheLConstants.BIG_ANIM_BALLOON) || mark.equals(TheLConstants.BIG_ANIM_CROWN) || mark.equals(TheLConstants.BIG_ANIM_STAR_SHOWER) || mark.equals(TheLConstants.BIG_ANIM_COIN_DROP) || mark.equals(TheLConstants.BIG_ANIM_HUG_HUG) || TheLConstants.BIG_ANIM_BUBBLE.equals(mark) || TheLConstants.BIG_ANIM_SNOWMAN.equals(mark) || TheLConstants.BIG_ANIM_KISS.equals(mark) || TheLConstants.BIG_ANIM_FERRIS_WHEEL.equals(mark) || TheLConstants.BIG_ANIM_PUMPKIN.equals(mark) || TheLConstants.BIG_ANIM_RING.equals(mark) || TheLConstants.BIG_ANIM_CHRISTMAS.equals(mark) || TheLConstants.BIG_ANIM_BOMB.equals(mark) || TheLConstants.BIG_ANIM_RICEBALL.equals(mark) || TheLConstants.BIG_ANIM_FIREWORK.equals(mark) || TheLConstants.BIG_ANIM_FIRECRACKER.equals(mark) || mark.endsWith(".json") || (msgBean.videoUrl != null && msgBean.videoUrl.endsWith(".mp4"))) {
            bigAnimGiftList.add(msgBean);
            tryToPlayBigAnim();//试着播放大动画
        } else { //当小礼物显示
            mGiftMsgCtrl.receiveGiftMsg(msgBean);//处理接受到的礼物消息，显示
        }
    }

    /**
     * 试着播放大动画
     */
    private void tryToPlayBigAnim() {

        L.d(TAG, " bigAnimGiftList size : " + bigAnimGiftList.size());

        if (isShowBigAnim || bigAnimGiftList.size() <= 0) {//如果正在播放大动画或者大动画队列为0
            return;
        } else {
            showBigGiftAnim(bigAnimGiftList.get(0));//播放队列的第一个
        }
    }


    /**
     * 显示大动画
     *
     * @param giftSendMsgBean
     */
    public void showBigGiftAnim(final GiftSendMsgBean giftSendMsgBean) {
        int playTime = giftSendMsgBean.playTime;
        isShowBigAnim = true;
        /**
         * 上报礼物给后台大礼物播放
         * **/
        if (liveRoomBean != null) {
            HorizontalLiveChat.getInstance().postGiftPlayStatus(giftSendMsgBean.userId, liveRoomBean.user.id + "", giftSendMsgBean.id);
        }

        L.d(TAG, " big_gift_layout : " + big_gift_layout);

        L.d(TAG, " giftSendMsgBean.mark : " + giftSendMsgBean.mark);

        if (big_gift_layout.playLocalAnim(giftSendMsgBean.mark)) {//如果是本地大礼物，播放本地大礼物

        } else if (giftSendMsgBean.mark.endsWith(".json") || (giftSendMsgBean.videoUrl != null && giftSendMsgBean.videoUrl.endsWith(".mp4"))) {//ae动画
            //LiveBigGiftAnimLayout.
            /***4.5.0新增，如果是视频礼物，播放视频***/

            if (!TextUtils.isEmpty(giftSendMsgBean.videoUrl) && !GiftSendMsgBean.VIDEO_URL_NULL.equals(giftSendMsgBean.videoUrl)) {
                big_gift_layout.playVideoAnim(giftSendMsgBean.videoUrl);
            } else {
                big_gift_layout.playAEanimator(giftSendMsgBean.mark, playTime);
            }

        } else {
            mGiftMsgCtrl.receiveGiftMsg(giftSendMsgBean);//如果大礼物中不存在（版本没更新），当小礼物显示
        }
        bigAnimGiftList.remove(0);//播放后，去掉第一个

        showBigGiftMsg(giftSendMsgBean);
        /***如果是大礼物视频礼物，则走视频结束回调,在setlistener方法里面***/
        if (giftSendMsgBean.mark.endsWith(".json") || (!TextUtils.isEmpty(giftSendMsgBean.videoUrl) && !GiftSendMsgBean.VIDEO_URL_NULL.equals(giftSendMsgBean.videoUrl))) {

        } else {
            big_gift_layout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    playNextBigAnim();
                }
            }, LiveBigGiftAnimLayout.duration + 1000);//大动画播放6秒
        }
    }

    /**
     * 播放下一个大动画
     */
    private void playNextBigAnim() {
        isShowBigAnim = false;
        hideBigGiftMsg();
        tryToPlayBigAnim();
    }

    private void hideBigGiftMsg() {
        live_vip_enter_view.hideBigGiftText();
    }

    /**
     * 显示大礼物的消息信息
     *
     * @param giftSendMsgBean
     */
    private void showBigGiftMsg(final GiftSendMsgBean giftSendMsgBean) {
        if (giftSendMsgBean.avatar == null || giftSendMsgBean.nickName == null) {
            return;
        }
        View.OnClickListener onClickListener = null;
        if (!Utils.isMyself(giftSendMsgBean.userId)) {
            onClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.getLiveUserCard(giftSendMsgBean.userId, LiveWatchHorizontalPresenter.USER_CARD);
                    reportContent = "";
                }
            };
        }
        live_vip_enter_view.receiveBigGiftMsg(giftSendMsgBean, onClickListener);
    }

    /**
     * 由于某种原因而暂时无法显示的消息
     * 把礼物消息放到等待消息队列中去
     * 比如最新小礼物，在开启直播的时候获取到的小礼物消息中没有这个礼物
     *
     * @param msgBean 放到等到队里中的消息
     */
    private void putGiftMsgIntoWaitList(GiftSendMsgBean msgBean) {
        giftMsgWaitList.add(msgBean);
    }

    /**
     * 获取赠送礼物连网数据
     */
    private void getGiftListNetData() {
        if (isGettingGiftData) {//如果正在获取数据，则不重复获取
            return;
        }
        isGettingGiftData = true;
        presenter.getLiveGiftList();
        big_gift_layout.postDelayed(new Runnable() {
            @Override
            public void run() {
                isGettingGiftData = false;//万一请求失败，之类的，8秒后自动设置为false;
            }
        }, 8000);
    }

    /**
     * 举报
     */
    class ReportListener implements View.OnClickListener {
        String userId;

        public ReportListener(String userId) {
            this.userId = userId;
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getActivity(), ReportActivity.class);
            intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, this.userId);
            if (userId.equals(liveRoomBean.user.id + "")) {//举报主播
                intent.putExtra(TheLConstants.BUNDLE_KEY_REPORT_TYPE, ReportActivity.REPORT_TYPE_ANCHOR);
                intent.putExtra(TheLConstants.BUNDLE_KEY_LOCAL_IMAGE_PATH, getReportImagePath());
                intent.putExtra(TheLConstants.BUNDLE_KEY_LIVE_ROOM_ID, liveRoomBean.liveId);
            } else {//举报观众
                intent.putExtra(TheLConstants.BUNDLE_KEY_REPORT_TYPE, ReportActivity.REPORT_TYPE_AUDIENCE);
                intent.putExtra(TheLConstants.BUNDLE_KEY_REPORT_CONTENT, reportContent);
                intent.putExtra(TheLConstants.BUNDLE_KEY_LIVE_ROOM_ID, liveRoomBean.liveId);

            }
            startActivity(intent);
        }

    }

    //todo 添加举报主播的截图
    private String getReportImagePath() {

        return "";
    }

    /**
     * 从本地内存中获取GiftSendMsgBean（要显示的收到的打赏的礼物的消息）的其他数据
     *
     * @param msgBean 收到的打赏的礼物数据
     * @return true；获取到数据，false，没获取到数据
     */
    private boolean getGiftMsgDataFromMemory(GiftSendMsgBean msgBean) {
        if (softEnjoyBean == null || softEnjoyBean.list.isEmpty()) {
            getGiftListNetData();
            return false;
        }
        final int size = softEnjoyBean.list.size();
        for (int i = 0; i < size; i++) {
            SoftGiftBean bean = softEnjoyBean.list.get(i);//从本地内存中获取余下数据
            if (msgBean.id == bean.id) {
                msgBean.img = bean.img;
                msgBean.icon = bean.icon;
                msgBean.action = bean.action;
                msgBean.giftName = bean.title;
                msgBean.mark = bean.resource;
                msgBean.gold = bean.gold;
                msgBean.playTime = bean.playTime;
                msgBean.videoUrl = bean.videoUrl;
                return true;//说明获取到了，返回true
            }
        }
        return isOffLineGift(msgBean);
    }

    private boolean isOffLineGift(GiftSendMsgBean msgBean) {

        final int size = offlineGiftList.size();
        for (int i = 0; i < size; i++) {
            SoftGiftBean bean = offlineGiftList.get(i);//从本地内存中获取余下数据
            if (msgBean.id == bean.id) {
                msgBean.img = bean.img;
                msgBean.icon = bean.icon;
                msgBean.action = bean.action;
                msgBean.giftName = bean.title;
                msgBean.mark = bean.resource;
                msgBean.gold = bean.gold;
                msgBean.playTime = bean.playTime;
                msgBean.videoUrl = bean.videoUrl;
                return true;//说明获取到了，返回true
            }
        }
        return false;
    }

    private void getSingleGift(int id) {
        if (isGettingGiftData) {//如果正在获取数据，则不重复获取
            return;
        }
        isGettingGiftData = true;
        presenter.getSingleGiftDetail(id);
        big_gift_layout.postDelayed(new Runnable() {
            @Override
            public void run() {
                isGettingGiftData = false;//万一请求失败，之类的，8秒后自动设置为false;
            }
        }, 8000);
    }

    /**
     * 获取到打赏礼物列表data
     * 初始化到giftListView中
     */
    private void showGiftListData() {
        pushGiftMsgWaitList();
        setButtonClickable(true);
    }

    /**
     * 处理等待的消息队列
     */
    private void pushGiftMsgWaitList() {
        for (int i = 0; i < giftMsgWaitList.size(); i++) {
            GiftSendMsgBean msgBean = giftMsgWaitList.get(i);
            if (getGiftMsgDataFromMemory(msgBean)) {//如果从新的内存中获取到数据
                showGiftMsgData(msgBean);//显示处理数据
                giftMsgWaitList.remove(i);//从等待队列的数据中移除
            } else {//依旧不能从新的内存中获取到数据（可能数据依旧不是最新）
                getSingleGift(msgBean.id);//连网请求数据
            }
        }
    }

    /**
     * 一些按钮必须要在有数据以后才可以点击
     *
     * @param clickable
     */
    private void setButtonClickable(boolean clickable) {
        img_emoji.setClickable(clickable);
        img_switch_danmu.setClickable(clickable);
        avatar.setClickable(clickable);
        txt_nickname.setClickable(clickable);
    }

    /**
     * 关闭直播对话框
     */
    private void showLiveClosedDialog() {
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    live_show_close_view.initView(liveRoomBean, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            live_show_close_view.hide();
                            if (getActivity() != null) {
                                getActivity().finish();
                            }
                        }
                    }, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    }).show();
                }
            });
        }
    }

    /**
     * 显示软键盘
     */
    private void showKeyboard() {
        if (null != getActivity()) {
            ViewUtils.showSoftInput(getActivity(), edit_input);
        }
    }

    private SoftInputViewGroup.OnSoftInputShowListener mOnSoftInputShowListener = new SoftInputViewGroup.OnSoftInputShowListener() {
        @Override
        public void onSoftInputShow(boolean isShow, boolean isShowDanmu) {

            if (isShow || isShowDanmu) {
                send_edit_msg.setVisibility(View.VISIBLE);
                layer_view.setVisibility(View.VISIBLE);
            } else {
                send_edit_msg.setVisibility(View.GONE);
                layer_view.setVisibility(View.GONE);
            }

            //显示弹幕选择界面
            if (isShowDanmu) {
                edit_input.setFocusable(false);
                edit_input.setCursorVisible(false);
            } else {
                edit_input.setFocusable(true);
                edit_input.setFocusableInTouchMode(true);
                edit_input.requestFocus();
                edit_input.setCursorVisible(true);
            }

            L.d(TAG, " onSoftInputShow edit_input.isFocusable() : " + edit_input.isFocusable());

        }
    };

    private void setInputHint(int gold) {

        if (danmu_layout.isFreeBarrage()) {
            gold = 0;
        }
        edit_input.setHint(TheLApp.context.getResources().getString(R.string.danmu_hint, gold));

    }

    /**
     * 发送消息
     */
    private void sendMsg() {

        String msg = edit_input.getText().toString().trim();

        if (msg.contains("卡")) {
            AnalyticsBean analyticsBean = LiveInfoLogBean.getInstance().getLiveLowSpeedAnalytics();
            analyticsBean.logType = "liveTxtUpload";
            analyticsBean.roomId = liveRoomBean.liveId;
            analyticsBean.liveUserId = UserUtils.getMyUserId();
            analyticsBean.reason = msg;
            analyticsBean.inUrl = "";
            LiveUtils.pushLivePointLog(analyticsBean);
        }
        HorizontalLiveChat.getInstance().sendMsg(msg);
    }

    /**
     * 发送弹幕
     */
    private void sendDanmu() {
        if (softEnjoyBean.gold < liveRoomBean.barrageCost && danmu_layout != null && !danmu_layout.isFreeBarrage()) {//如果点击的价格高于余额
            String giftName = TheLApp.context.getResources().getString(R.string.danmu);
            showRechargeDialog(false, 0, giftName, liveRoomBean.barrageCost, softEnjoyBean.gold);//显示去充值对话框
            return;
        }

        String content = edit_input.getText().toString().trim();

        HorizontalLiveChat.getInstance().sendDanmu("30", content, String.valueOf(barrageId));

        if (danmu_layout != null) {

            if (danmu_layout.isFreeBarrage()) {
                if (barrageId == 0) {
                    edit_input.setHint(TheLApp.context.getResources().getString(R.string.chat_activity_input_hint));
                }

                if (barrageId == 1) {
                    edit_input.setHint(TheLApp.context.getResources().getString(R.string.danmu_hint, 2));
                }

                if (barrageId > 1) {
                    edit_input.setHint(TheLApp.context.getResources().getString(R.string.danmu_hint, 3));

                }
            }
            danmu_layout.setMoney();
        }

    }

    private void showGiftPW() {

        if (softEnjoyBean != null) {
            mGiftPopupWindow = new GiftHorizontalPopupWindow(getContext(), softEnjoyBean.gold);
            mGiftPopupWindow.setOnGiftClickListener(mGiftOnClickListener);
            mGiftPopupWindow.setOnGiftLianClickListener(mGiftLianClickListener);
            //mGiftPopupWindow.setShowGuestsInfoDialogListener(mGuestsShowInfoListener);
            mGiftPopupWindow.setGotoSoftMoneyLinsener(gotoRechargeSoftMoneyListener);
            mGiftPopupWindow.showAtLocation(avatar.getRootView(), Gravity.BOTTOM, 0, 0);
        }

    }

    private SoftGiftListView.GiftOnClickListener mGiftOnClickListener = new SoftGiftListView.GiftOnClickListener() {
        @Override
        public void OnGiftClicked(ViewGroup parent, View view, TimeTextView timeTextView, int pageIndex, int columnIndex, int lineIndex, int currentPosition, int position, SoftGiftBean softGiftBean, boolean sendAnchor, LiveMultiSeatBean seatBean) {
            sendGiftMsg(softGiftBean, timeTextView, sendAnchor, seatBean, position);
        }
    };
    public SoftGiftListView.GiftLianClickListener mGiftLianClickListener = new SoftGiftListView.GiftLianClickListener() {
        @Override
        public void onGiftClicked(ViewGroup parent, View itemView, View clickView, int pageIndex, int columnIndex, int lineIndex, int currentPosition, int position, SoftGiftBean softGiftBean, boolean isSendAnchor, int seatNum) {
            showLianGiftMsgDialog(softGiftBean, isSendAnchor, seatNum);

        }
    };

    private SoftGiftListView.GotoRechargeSoftMoneyListener gotoRechargeSoftMoneyListener = new SoftGiftListView.GotoRechargeSoftMoneyListener() {
        @Override
        public void onGotoRechageClicked() {
//            gotoSoftMoneyActivity();
            if (getActivity() != null) {
                RechargeDialogFragment chargeDialogFragment = new RechargeDialogFragment();
                chargeDialogFragment.setHorizontal(1);
                chargeDialogFragment.show(getActivity().getSupportFragmentManager(), RechargeDialogFragment.class.getName());
            }
        }
    };

    /**
     * 点 连 字连击某个礼物
     *
     * @param softGiftBean
     */
    private void showLianGiftMsgDialog(final SoftGiftBean softGiftBean, final boolean isSendAnchor, final int seatNum) {

        if (dialogUtils != null && dialogUtils.isDialogShowing()) {
            dialogUtils.closeDialog();
        }
        dialogUtils.showLianGiftMsgDialog(getActivity(), softGiftBean, GIFT_SEND_TYPE_COMBO, new GiftLianSendCallback() {

            @Override
            public void lianGift(SoftGiftBean softGiftBean, String type, int combo) {
                if (softEnjoyBean.gold < liveRoomBean.barrageCost) {//如果点击的价格高于余额
                    showRechargeDialog(true, combo, softGiftBean.title, softGiftBean.gold, softEnjoyBean.gold);//显示去充值对话框
                    return;
                }

                if (isSendAnchor) {//送主播礼物
                    HorizontalLiveChat.getInstance().sendSoftGift(softGiftBean, softGiftBean.id, GIFT_SEND_TYPE_COMBO, combo);

                }

                hasSend = true;
                /***growing 埋点购买礼物*/
                outTime = System.currentTimeMillis();  //用户送礼物的时间距离开始观看时间差值
                int allPlayDuration = (int) ((outTime - intoTime) / 1000);
                GIoGiftTrackBean giftTrackBean = new GIoGiftTrackBean(GrowingIoConstant.GIFT_PURCHASE, "", softGiftBean.id + "", combo * softGiftBean.gold + "", softGiftBean.title, liveRoomBean.user.id + "", liveRoomBean.user.nickName, allPlayDuration + "",GrowingIoConstant.MATERIAL_LIVE_VIDEO);
                GrowingIOUtil.payGiftTrack(giftTrackBean);
            }
        }, new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return false;
            }
        });

    }

    /**
     * 充值对话框
     */
    private void showRechargeDialog(boolean isCombo, int comboCount, String giftName, int giftGold, long balance) {

        if (getActivity() != null) {
            String tips;
            if (isCombo) {
                long difference_value = giftGold * comboCount - balance;
                tips = TheLApp.context.getResources().getString(R.string.balance_insufficient_2, comboCount, giftName, difference_value);
            } else {
                long difference_value = giftGold - balance;
                tips = TheLApp.context.getResources().getString(R.string.balance_insufficient_1, giftName, difference_value);
            }
            GrowingIOUtil.payTrack(GrowingIoConstant.LIVE_PAGE_POPWIN);
            GrowingIOUtil.payEvar(GrowingIoConstant.G_LivePagePopWin);
            RechargeDialogFragment rechargeDialogFragment = new RechargeDialogFragment();
            rechargeDialogFragment.setTips(tips);
            rechargeDialogFragment.setHorizontal(1);
            rechargeDialogFragment.show(getActivity().getSupportFragmentManager(), RechargeDialogFragment.class.getName());
        }
    }

    /**
     * 赠送礼物
     *
     * @param softGiftBean 赠送的礼物
     * @param timeTextView
     * @param sendAnchor
     * @param seatBean
     */
    private void sendGiftMsg(SoftGiftBean softGiftBean, TimeTextView timeTextView, boolean sendAnchor, LiveMultiSeatBean seatBean, int position) {

        if (softEnjoyBean.gold < softGiftBean.gold) {//如果点击的价格高于余额
            showRechargeDialog(false, 0, softGiftBean.title, softGiftBean.gold, softEnjoyBean.gold);//显示去充值对话框
            return;
        }
        if (timeTextView.isCombo()) {//如果是处于连击状态
            combo++;
        } else {
            combo = 1;
        }

        if (sendAnchor) {//送主播礼物
            HorizontalLiveChat.getInstance().sendSoftGift(softGiftBean, softGiftBean.id, GIFT_SEND_TYPE_SEND, 1);
        }
        hasSend = true;
        /***growing 埋点购买礼物*/
        outTime = System.currentTimeMillis();  //用户送礼物的时间距离开始观看时间差值
        int allPlayDuration = (int) ((outTime - intoTime) / 1000);
        GIoGiftTrackBean giftTrackBean = new GIoGiftTrackBean(GrowingIoConstant.GIFT_PURCHASE, "", softGiftBean.id + "", softGiftBean.gold + "", softGiftBean.title, liveRoomBean.user.id + "", liveRoomBean.user.nickName, allPlayDuration + "",GrowingIoConstant.MATERIAL_LIVE_VIDEO);
        GrowingIOUtil.payGiftTrack(giftTrackBean);
        GrowingIOUtil.payEvar(GrowingIoConstant.G_LiveGiftLis);

    }

    /**
     * 发送名片
     */
    private void sendCard() {
        if (liveRoomBean == null || getActivity() == null) {
            return;
        }
        RecommendedModel recommendedModel = new RecommendedModel();
        recommendedModel.recommendedType = "liveUser";
        if (liveRoomBean.audioType == 1) {
            recommendedModel.msgType = "moment_voicelive";
        }
        recommendedModel.imageUrl = liveRoomBean.imageUrl;
        recommendedModel.momentsText = liveRoomBean.text;
        recommendedModel.liveId = liveRoomBean.id;
        recommendedModel.avatar = liveRoomBean.user.avatar;
        recommendedModel.userId = (int) liveRoomBean.user.id;
        recommendedModel.isLandscape = liveRoomBean.isLandscape == 1;
        recommendedModel.nickname = liveRoomBean.user.nickName;
        recommendedModel.momentsType = "live_user";
        recommendedModel.recommendedType = "liveUser";
        FlutterRouterConfig.Companion.nativeShowRecommendDialog(GsonUtils.createJsonString(recommendedModel));
    }

    private void refreshFollowBtn(int status) {
        if (status == 0) {
            txt_follow.setText(TheLApp.getContext().getString(R.string.userinfo_activity_follow));
            txt_follow.setBackgroundResource(R.drawable.bg_follow_round_button_blue);
            LiveUtils.appearTextFollow(txt_follow);
        } else {
            txt_follow.setText(TheLApp.getContext().getString(R.string.userinfo_activity_followed));
            txt_follow.setBackgroundResource(R.drawable.bg_followed_round_button_gray);
            LiveUtils.disappearTextFollow(txt_follow);
        }
    }

    private boolean canGetAnchor() {
        if (getActivity() == null || getActivity().isDestroyed() || liveClosed) {
            return false;
        }

        if (liveRoomBean == null || liveRoomBean.user == null) {
            return false;
        }
        return !BlackUtils.isBlackOrBlock(liveRoomBean.user.id + "");
    }

    @Override
    public void onPause() {
        super.onPause();
        if (videoView != null) {
            videoView.pause();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (videoView != null) {
            videoView.start();
        }
    }

    @Override
    public void onFollowStatusChanged(int followStatus, String userId, String nickName, String avatar) {
        super.onFollowStatusChanged(followStatus, userId, nickName, avatar);
        if (liveRoomBean != null && liveRoomBean.user != null) {//由于是广播，很可能只是创建注册了，而数据还没获取
            if ((liveRoomBean.user.id + "").equals(userId)) {
                liveRoomBean.user.isFollow = followStatus;
                //                progress_bar.setVisibility(View.GONE);
                refreshFollowBtn(followStatus);
                if (followStatus == FollowStatusChangedImpl.FOLLOW_STATUS_FOLLOW) {
                    //  observer.addFollowFans();
                }
            }
        }
        if (live_user_card_dialog_view != null && live_user_card_dialog_view.getDialog() != null && live_user_card_dialog_view.getDialog().isShowing()) {
            live_user_card_dialog_view.onFollowStatusChanged(followStatus, userId, nickName, avatar);
        }
        if (live_show_close_view != null) {
            live_show_close_view.followStatusChanged(followStatus, userId, nickName, avatar);
        }
        if (live_user_anchor_card_dialog_view != null && live_user_anchor_card_dialog_view.getDialog() != null && live_user_anchor_card_dialog_view.getDialog().isShowing()) {
            live_user_anchor_card_dialog_view.onFollowStatusChanged(followStatus, userId, nickName, avatar);

        }

    }

}
