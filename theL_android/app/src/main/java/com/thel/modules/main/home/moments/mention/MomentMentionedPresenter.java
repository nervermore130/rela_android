package com.thel.modules.main.home.moments.mention;

import com.thel.bean.topic.TopicFollowerListBean;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.service.DefaultRequestService;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by liuyun on 2017/10/24.
 */

public class MomentMentionedPresenter implements MomentMentionedContract.Presenter {

    private MomentMentionedContract.View mentionView;

    public MomentMentionedPresenter(MomentMentionedContract.View mentionView) {
        this.mentionView = mentionView;
    }

    @Override public void subscribe() {

    }

    @Override public void unSubscribe() {

    }

    @Override public void loadMentions(String momentId) {

        Flowable<TopicFollowerListBean> flowable = DefaultRequestService.createAllRequestService().getMomentMentionedList(momentId);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<TopicFollowerListBean>() {
            @Override public void onNext(TopicFollowerListBean data) {
                super.onNext(data);
                if (mentionView != null && mentionView.isActive()) {
                    mentionView.showList(data);
                }
            }
        });

    }
}
