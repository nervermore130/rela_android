package com.thel.modules.main.messages.utils;

import com.thel.db.DBUtils;
import com.thel.manager.ChatServiceManager;
import com.thel.modules.main.messages.bean.MsgBean;


/**
 * 发送消息工具类，单例模式
 * <p>
 * Created by setsail on 16/2/19.
 */
public class SendMsgUtils {
    private static final String TAG = SendMsgUtils.class.getSimpleName();

    private static SendMsgUtils instance = new SendMsgUtils();

    /**
     * 单例
     */
    private SendMsgUtils() {

    }

    public static SendMsgUtils getInstance() {
        return instance;
    }

    public synchronized void sendMessage(MsgBean msgBean) {

        if (msgBean == null) {
            return;
        }
        ChatServiceManager.getInstance().insertNewMsg(msgBean, DBUtils.getMainProcessChatTableName(msgBean), 1);
        ChatServiceManager.getInstance().sendMsg(msgBean);
    }
}
