package com.thel.modules.live.bean;

import com.thel.base.BaseDataBean;

import java.util.ArrayList;
import java.util.List;

/**
 * 直播间礼物排行榜
 */
public class LiveGiftsRankingListBean extends BaseDataBean {

    public List<LiveGiftsRankingBean> list = new ArrayList<>();
    public LiveGiftsRankingInfoBean rankInfo;

}