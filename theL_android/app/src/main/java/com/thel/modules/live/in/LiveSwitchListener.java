package com.thel.modules.live.in;

/**
 * Created by waiarl on 2018/3/1.
 * 直播间内要求切换直播回调
 */

public interface LiveSwitchListener {
    /**
     * 切到下个直播
     *
     * @return
     */
    boolean switchNext();

    /**
     * 切到上个直播
     *
     * @return
     */
    boolean switchPrevious();

    /**
     * 切换到有全服最贵礼物的直播
     *
     * @param userId 全服最贵礼物的直播的id
     * @return
     */
    boolean switchMostExpensiveLive(String userId);
}
