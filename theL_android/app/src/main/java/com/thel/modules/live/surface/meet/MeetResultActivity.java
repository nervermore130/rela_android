package com.thel.modules.live.surface.meet;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.view.View;

import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.bean.live.LiveMultiSeatBean;
import com.thel.constants.TheLConstants;
import com.thel.ui.adapter.MyFragmentPageAdapter;
import com.thel.ui.widget.RoundRectIndicatorView;
import com.thel.utils.L;
import com.thel.utils.UserUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MeetResultActivity extends BaseActivity {

    public static final int BROADCASTER = 0;//主播
    public static final int GUEST = 1;//嘉宾
    public static final int AUDIENCE = 2;//观众
    public static final int GUESTALL = 3;//嘉宾、全场

    @BindView(R.id.meet_vp)
    ViewPager meet_vp;

    @BindView(R.id.indicatorView)
    RoundRectIndicatorView indicatorView;

    private List<Fragment> fragmentList = new ArrayList<>();

    private List<LiveMultiSeatBean.CoupleDetail> coupleDetails;

    private List<LiveMultiSeatBean.CoupleDetail> meCoupleDetail = new ArrayList<>();

    private int role = 0;

    private LiveCloseReceiver liveCloseReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.meet_result_layout);

        ButterKnife.bind(this);

        initData();

        initView();

        liveCloseReceiver = new LiveCloseReceiver();
        registerReceiver(liveCloseReceiver, new IntentFilter(TheLConstants.BROADCAST_LIVE_CLOSE));

    }

    @Override
    protected void onDestroy() {
        if (liveCloseReceiver != null)
            unregisterReceiver(liveCloseReceiver);
        super.onDestroy();
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void initData() {


        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {

            LiveMultiSeatBean liveMultiSeatBean = (LiveMultiSeatBean) bundle.getSerializable(TheLConstants.BUNDLE_KEY_LIVE_SEAT_ROOM);

            role = bundle.getInt(TheLConstants.BUNDLE_KEY_ROLE);

            L.d("MeetResultFragment", " liveMultiSeatBean: " + liveMultiSeatBean);

            if (liveMultiSeatBean != null) {

                coupleDetails = liveMultiSeatBean.coupleDetail;

                if (coupleDetails != null) {

                    for (int i = 0; i < coupleDetails.size(); i++) {

                        final LiveMultiSeatBean.CoupleDetail coupleDetail = coupleDetails.get(i);

                        final String userId = UserUtils.getMyUserId();

                        if (userId.equals(coupleDetail.userId)) {

                            boolean isEvenNumbers = isEvenNumbers(i);

                            if (isEvenNumbers) {
                                meCoupleDetail.add(coupleDetail);
                                meCoupleDetail.add(coupleDetails.get(i + 1));
                            } else {
                                meCoupleDetail.add(coupleDetail);
                                meCoupleDetail.add(coupleDetails.get(i - 1));
                            }

                        }

                    }

                }

            }

        }


    }

    private void initView() {

        switch (role) {
            case BROADCASTER:
            case AUDIENCE:

                indicatorView.setVisibility(View.GONE);

                MeetResultFragment meetResultFragment = new MeetResultFragment();

                if (coupleDetails != null && coupleDetails.size() > 0) {

                    meetResultFragment.setData(coupleDetails);
                    meetResultFragment.isShowShareBtn(true);

                } else {
                    meetResultFragment.isShowShareBtn(false);

                }

                if (role == BROADCASTER) {
                    meetResultFragment.setMeetResultRole(TheLConstants.MeetResultConstant.MEET_RESULT_BROADCASTER);
                } else {
                    meetResultFragment.setMeetResultRole(TheLConstants.MeetResultConstant.MEET_RESULT_AUDIENCE);
                }

                getSupportFragmentManager().beginTransaction().add(R.id.content_frame, meetResultFragment, MeetResultFragment.class.getName()).commit();

                break;
            case GUEST:

                indicatorView.setCount(2);

                indicatorView.setCurrent(0);

                initFragment();

                break;
            default:
                break;

        }

    }

    private void initFragment() {

        MeetResultFragment meMeetResultFragment = new MeetResultFragment();

        L.d("MeetResultFragment", " meCoupleDetail : " + meCoupleDetail);

        if (meCoupleDetail != null) {
            if (meCoupleDetail.size() == 0) {
                meMeetResultFragment.isShowShareBtn(false);

            } else {
                meMeetResultFragment.isShowShareBtn(true);

            }
            meMeetResultFragment.setData(meCoupleDetail);
            meMeetResultFragment.setAllData(coupleDetails);

            meMeetResultFragment.setMeetResultRole(TheLConstants.MeetResultConstant.MEET_RESULT_GUEST_ME);

            L.d("MeetResultFragment", " TheLConstants.MeetResultConstant.MEET_RESULT_GUEST_ME : " + TheLConstants.MeetResultConstant.MEET_RESULT_GUEST_ME);


        }

        MeetResultFragment meetResultFragment = new MeetResultFragment();

        if (coupleDetails != null) {
            meetResultFragment.setData(coupleDetails);

            if (coupleDetails.size() != 0) {
                meetResultFragment.isShowShareBtn(true);

            } else {
                meetResultFragment.isShowShareBtn(false);

            }


            meetResultFragment.setMeetResultRole(TheLConstants.MeetResultConstant.MEET_RESULT_GUEST_ALL);

            L.d("MeetResultFragment", " TheLConstants.MeetResultConstant.MEET_RESULT_GUEST_ALL : " + TheLConstants.MeetResultConstant.MEET_RESULT_GUEST_ALL);

        }

        fragmentList.add(meMeetResultFragment);

        fragmentList.add(meetResultFragment);

        meet_vp.setAdapter(new MyFragmentPageAdapter(getSupportFragmentManager(), fragmentList));

        meet_vp.addOnPageChangeListener(mOnPageChangeListener);

        meet_vp.setCurrentItem(0);

    }


    private ViewPager.OnPageChangeListener mOnPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageScrolled(int i, float v, int i1) {

        }

        @Override
        public void onPageSelected(int i) {
            indicatorView.setCurrent(i);
        }

        @Override
        public void onPageScrollStateChanged(int i) {

        }
    };

    //判断是否是偶数
    private boolean isEvenNumbers(int num) {

        return num % 2 == 0;
    }

    @OnClick(R.id.close_result_iv)
    void onBackClick() {
        MeetResultActivity.this.finish();
    }

    private class LiveCloseReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {

                if (TheLConstants.BROADCAST_LIVE_CLOSE.equals(intent.getAction())) {
                    MeetResultActivity.this.finish();
                }
            }
        }
    }
}
