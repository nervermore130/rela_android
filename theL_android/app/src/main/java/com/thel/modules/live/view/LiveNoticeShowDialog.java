package com.thel.modules.live.view;

import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.method.ScrollingMovementMethod;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.thel.R;
import com.thel.utils.AppInit;

import androidx.fragment.app.DialogFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import video.com.relavideolibrary.Utils.DensityUtils;

/**
 * Created by chad
 * Time 18/8/27
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */
public class LiveNoticeShowDialog extends DialogFragment {

    @OnClick(R.id.close)
    void close() {
        this.dismiss();
    }

    @BindView(R.id.notice_tips)
    TextView notice_tips;

    private String notice = "";

    public static LiveNoticeShowDialog newInstance(String notice) {
        LiveNoticeShowDialog liveNoticeShowDialog = new LiveNoticeShowDialog();
        Bundle bundle = new Bundle();
        bundle.putString("notice", notice);
        liveNoticeShowDialog.setArguments(bundle);
        return liveNoticeShowDialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) notice = getArguments().getString("notice");
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), R.style.DialogFadeAnim);

        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER); //可设置dialog的位置
        window.getDecorView().setPadding(0, 0, 0, 0); //消除边距
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.width = AppInit.displayMetrics.widthPixels - DensityUtils.dp2px(50);   //设置宽度充满屏幕
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        return dialog;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.live_notice_show_layout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        notice_tips.setMovementMethod(ScrollingMovementMethod.getInstance());
        notice_tips.setText(notice);
    }
}
