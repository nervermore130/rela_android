package com.thel.modules.main.me.aboutMe;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;

import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.BuildConfig;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.rela.pay.OnPayStatusListener;
import com.rela.pay.PayConstants;
import com.rela.pay.PayProxyImpl;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.base.IntentFilterActivity;
import com.thel.bean.PayOrderBean;
import com.thel.bean.StickerBean;
import com.thel.bean.StickerPackNetBean;
import com.thel.bean.user.VipConfigBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.imp.sticker.StickerUtils;
import com.thel.modules.main.me.bean.LogInfoBean;
import com.thel.modules.main.me.match.eventcollect.collect.LiveLogUtils;
import com.thel.modules.main.me.match.eventcollect.collect.MatchLogUtils;
import com.thel.modules.main.messages.utils.PushUtils;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.modules.welcome.WelcomeActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.RequestConstants;
import com.thel.ui.widget.emoji.PreviewGridView;
import com.thel.ui.widget.emoji.StickerGridViewAdapter;
import com.thel.utils.AppInit;
import com.thel.utils.DialogUtil;
import com.thel.utils.GsonUtils;
import com.thel.utils.ImageUtils;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;
import com.umeng.analytics.MobclickAgent;

import org.reactivestreams.Subscription;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.thel.R.id.img_avatar;
import static com.thel.R.id.img_bg;
import static com.thel.R.id.rel_bg;
import static com.thel.R.id.scroll_main;
import static com.thel.R.id.txt_desc;
import static com.thel.R.id.txt_reload;

/**
 * 表情详情
 * Created by lingwei on 2017/11/7.
 */

public class StickerPackDetailActivity extends BaseActivity {
    public final String TAG = StickerPackDetailActivity.class.getSimpleName();
    @BindView(img_bg)
    SimpleDraweeView imgBg;
    @BindView(img_avatar)
    SimpleDraweeView imgAvatar;
    @BindView(R.id.avatar_area)
    RelativeLayout avatarArea;
    @BindView(rel_bg)
    RelativeLayout relBg;
    @BindView(R.id.txt_author_name)
    TextView txtAuthorName;
    @BindView(R.id.txt_go_to_user_page)
    TextView txtGoToUserPage;
    @BindView(txt_desc)
    TextView txtDesc;
    @BindView(R.id.gridView)
    PreviewGridView gridView;
    @BindView(R.id.txt_copyright)
    TextView txtCopyright;
    @BindView(scroll_main)
    ScrollView scrollMain;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.btn_purchase)
    TextView btnPurchase;
    @BindView(R.id.rel_bottom)
    RelativeLayout relBottom;
    @BindView(R.id.rel_preview)
    RelativeLayout relPreview;
    @BindView(R.id.img_reload)
    ImageView imgReload;
    @BindView(txt_reload)
    TextView txtReload;
    @BindView(R.id.lin_reload)
    LinearLayout linReload;
    //表情包id
    private Long id;
    private boolean isDownload = false;
    private long userId;
    private StickerPackNetBean mStickerPackBean;
    private String pageId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sticker_detail_layout);
        ButterKnife.bind(this);
        initIntent();
        initData();
        setListener();
        pageId = Utils.getPageId();

    }

    private void setListener() {
        imgReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Animation anim = AnimationUtils.loadAnimation(TheLApp.getContext(), R.anim.rotate);
                LinearInterpolator lir = new LinearInterpolator();
                anim.setInterpolator(lir);
                view.setLayerType(View.LAYER_TYPE_HARDWARE, null);
                view.startAnimation(anim);
                txtReload.setText(getString(R.string.info_reloading));
                processBusiness(id, 0);
            }
        });

        btnPurchase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mStickerPackBean.data != null) {
                    if (RequestConstants.APPLICATION_ID_LOCAL.equals(BuildConfig.APPLICATION_ID)) {

                        DialogUtil.getInstance().showSelectionDialogWithIcon(StickerPackDetailActivity.this, getString(R.string.pay_options), new String[]{
                                getString(R.string.pay_alipay),
                                getString(R.string.pay_wx)}, new int[]{R.mipmap.icon_zhifubao,
                                R.mipmap.icon_wechat}, new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                DialogUtil.getInstance().closeDialog();
                                switch (position) {
                                    case 0://支付宝
                                        view.setVisibility(View.GONE);
                                        progressBar.setVisibility(View.VISIBLE);
                                        showLoadingNoBack();
                                        RequestBusiness.getInstance().createStickerPackOrder(TheLConstants.PRODUCT_TYPE_STICKER_PACK, mStickerPackBean.data.id + "", PayConstants.PAY_TYPE_ALIPAY, PayConstants.ALIPAY_SOURCE).observeOn(AndroidSchedulers.mainThread()).onBackpressureDrop().subscribeOn(Schedulers.io()).subscribe(new InterceptorSubscribe<PayOrderBean>() {
                                            @Override
                                            public void onNext(PayOrderBean data) {
                                                super.onNext(data);

                                                PayProxyImpl.getInstance().pay(PayConstants.PAY_TYPE_ALIPAY, StickerPackDetailActivity.this, GsonUtils.createJsonString(data.data), new OnPayStatusListener() {
                                                    @Override
                                                    public void onPayStatus(int payStatus, String errorInfo) {

                                                        if (payStatus == 1) {
                                                            MobclickAgent.onEvent(StickerPackDetailActivity.this, "sticker_pay_succeed");// 购买表情包支付成功事件统计
                                                            paySucceeded();
                                                        } else {
                                                            //判断resultStatus为非"9000"则代表可能支付失败
                                                            //"8000"代表支付结果因为支付渠道原因或者系统原因还在等待支付结果确认，最终交易是否成功以服务端异步通知为准（小概率状态）
                                                            if (TextUtils.equals(errorInfo, "8000")) {

                                                            } else {
                                                                if (TextUtils.equals(errorInfo, "6001")) {
                                                                    DialogUtil.showToastShort(TheLApp.getContext(), getString(R.string.pay_canceled));
                                                                } else {
                                                                    DialogUtil.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.pay_failed));
                                                                }
                                                                payFailed();
                                                            }
                                                        }
                                                    }

                                                });

                                            }
                                        });
                                        break;
                                    case 1://微信支付
                                        view.setVisibility(View.GONE);
                                        progressBar.setVisibility(View.VISIBLE);
                                        showLoadingNoBack();
                                        RequestBusiness.getInstance().createStickerPackOrder(TheLConstants.PRODUCT_TYPE_STICKER_PACK, mStickerPackBean.data.id + "", PayConstants.PAY_TYPE_WXPAY, PayConstants.WXPAY_SOURCE).observeOn(AndroidSchedulers.mainThread()).onBackpressureDrop().subscribeOn(Schedulers.io()).subscribe(new InterceptorSubscribe<PayOrderBean>() {
                                            @Override
                                            public void onNext(PayOrderBean data) {
                                                super.onNext(data);

                                                PayProxyImpl.getInstance().pay(PayConstants.PAY_TYPE_WXPAY, StickerPackDetailActivity.this, GsonUtils.createJsonString(data.data), new OnPayStatusListener() {
                                                    @Override
                                                    public void onPayStatus(int payStatus, String errorInfo) {

                                                        if (payStatus == 1) {
                                                            MobclickAgent.onEvent(StickerPackDetailActivity.this, "sticker_pay_succeed");// 购买表情包支付成功事件统计
                                                            paySucceeded();
                                                        } else {
                                                            payFailed();
                                                        }
                                                    }
                                                });

                                            }
                                        });
                                        break;
                                }
                            }
                        }, false, null);
                    } else {

                            //用户没有会员权限，点击会员付费表情，跳转到购买会员购买页面。
                            if (UserUtils.getUserVipLevel() == 0 && mStickerPackBean.data.vipFree == 1) {
                                MobclickAgent.onEvent(TheLApp.getContext(), "check_vip_free_sticker");
                                gotoBuyVip();
                            } else if (UserUtils.getUserVipLevel() > 0 && mStickerPackBean.data.vipFree == 1) {
                                //用户有会员权限，点击会员付费表情，直接下载。
                                view.setVisibility(View.GONE);
                                progressBar.setVisibility(View.VISIBLE);
                                isDownload = true;
                                processBusiness(mStickerPackBean.data.id, 1);

                            }


                    }
                }

            }
        });
    }

    private void getPayResult(PayOrderBean payOrderBean, String payType) {
        closeLoading();
        if (payOrderBean.data == null) {
            return;
        }
//        if (WXPayUtils.WXPAY.equals(payType)) {
//            WXPayUtils.getInstance().setTag(TAG).pay(payOrderBean.data.wxOrder);
//        }
//        else if (AlipayUtils.ALIPAY.equals(payType)) {
//            AlipayUtils.getInstance().pay(this, AlipayUtils.getInstance().getOrderInfo(payOrderBean.data.outTradeNo, payOrderBean.data.subject, payOrderBean.data.description, payOrderBean.data.totalFee, payOrderBean.data.alipayNotifyUrl, payOrderBean.data.expire), mHandler);
//        }
    }

    private void gotoBuyVip() {
        Intent intent = new Intent(this, BuyVipActivity.class);
        intent.putExtra("fromPage", "gallery.detail");
        intent.putExtra("pageId", pageId);

        startActivityForResult(intent, TheLConstants.REQUEST_CODE_BUY_VIP);
        traceWanderLog("clickpremium");
    }

    private void initData() {
        scrollMain.setVisibility(View.INVISIBLE);
        relBottom.setVisibility(View.INVISIBLE);
        processBusiness(id, 0);
    }

    private void processBusiness(long id, int download) {
        Flowable<StickerPackNetBean> flowable = RequestBusiness.getInstance().getStickerPackDetail(id, download);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<StickerPackNetBean>() {

            @Override
            public void onNext(StickerPackNetBean stickerPackBean) {
                super.onNext(stickerPackBean);
                getStickerPackBeanData(stickerPackBean);

            }

            @Override
            public void onComplete() {

            }

            @Override
            public void onError(Throwable t) {
                L.d("onerror", t.getMessage());

            }
        });
    }

    private void getStickerPackBeanData(StickerPackNetBean stickerPackBean) {
        if (stickerPackBean.data == null) {
            return;
        }
        traceWanderLog("clickpremium");
        if (isDownload) {
            stickerPackBean.data.status = 2;
            relBottom.setVisibility(View.GONE);
            saveOneStickerPack(stickerPackBean);
            Intent intent = new Intent();
            intent.putExtra("id", stickerPackBean.data.id);
            intent.putExtra("type", "download");
            setResult(RESULT_OK, intent);
        } else {
            this.mStickerPackBean = stickerPackBean;
            isStickerPackDownloadedOrBought(stickerPackBean);
            userId = stickerPackBean.data.authorId;
            initUi(stickerPackBean);
        }
    }

    private void initUi(StickerPackNetBean stickerPackBean) {
        linReload.setVisibility(View.GONE);
        scrollMain.setVisibility(View.VISIBLE);
        if (stickerPackBean.data.status == 2) { //已下載不顯示操作按鈕
            relBottom.setVisibility(View.GONE);
        } else {
            relBottom.setVisibility(View.VISIBLE);
            if (stickerPackBean.data.vipFree == 1) {//如果是會員免費
                btnPurchase.setText(getString(R.string.vip_free));
            } else {
                switch (stickerPackBean.data.status) {
                    case 0:
                        btnPurchase.setText(stickerPackBean.data.generatePriceShow());
                        break;
                    case 1:
                        btnPurchase.setText(getString(R.string.sticker_store_act_download));
                        break;
                }
            }
        }
        imgBg.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(stickerPackBean.data.cover))).build()).setAutoPlayAnimations(true).build());
        imgBg.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) (AppInit.displayMetrics.widthPixels / TheLConstants.ad_aspect_ratio)));
        relBg.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) (AppInit.displayMetrics.widthPixels / TheLConstants.ad_aspect_ratio + Utils.dip2px(this, 36))));
        imgAvatar.setImageURI(Uri.parse(stickerPackBean.data.authorAvatar));
        txtAuthorName.setText(TheLApp.getContext().getString(R.string.sticker_pack_act_author) + stickerPackBean.data.authorName);
        txtGoToUserPage.setVisibility(stickerPackBean.data.authorId == 0 ? View.GONE : View.VISIBLE);
        txtDesc.setText(stickerPackBean.data.description);
        txtCopyright.setText(stickerPackBean.data.copyright);
        ArrayList<String> arr = new ArrayList<String>();
        for (StickerBean stickerBean : stickerPackBean.data.stickers) {
            arr.add(stickerBean.img);
        }
        gridView.setPreviewRelativeLayout(findViewById(R.id.rel_preview), arr);
        StickerGridViewAdapter adapter = new StickerGridViewAdapter(stickerPackBean.data.stickers);
        // 对GridView进行适配
        gridView.setAdapter(adapter);
        scrollMain.post(new Runnable() {
            @Override
            public void run() {
                scrollMain.fullScroll(View.FOCUS_UP);
            }
        });

    }

    private void isStickerPackDownloadedOrBought(StickerPackNetBean stickerPackBean) {
        List<String> Sp_StickerId = StickerUtils.getMyStickerIdList();
        for (String sp_id : Sp_StickerId) {
            if ((sp_id).equals(String.valueOf(stickerPackBean.data.id))) {
                stickerPackBean.data.status = 2;
                break;
            }
        }
    }

    private void saveOneStickerPack(StickerPackNetBean stickerPackBean) {
        final String json = GsonUtils.createJsonString(stickerPackBean);
        StickerUtils.saveOneSticker(stickerPackBean.data, null);
    }

    private void initIntent() {
        Intent intent = getIntent();
        String action = intent.getAction();
        if (Intent.ACTION_VIEW.equals(action)) {
            if (!ShareFileUtils.getBoolean(ShareFileUtils.HAS_LOGGED, false)) {//
                startActivity(new Intent(this, WelcomeActivity.class));
                finish();
                return;
            }
            Uri uri = intent.getData();
            if (uri != null) {
                try {
                    id = Long.valueOf(uri.getQueryParameter("emojiId"));
                } catch (Exception e) {
                    finish();
                    return;
                }
            }
        } else {
            id = intent.getLongExtra("id", 0);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void payFailed() {
        mStickerPackBean.data.status = 0;
        progressBar.setVisibility(View.GONE);
        btnPurchase.setVisibility(View.VISIBLE);
    }

    private void paySucceeded() {
        mStickerPackBean.data.status = 1;
        progressBar.setVisibility(View.VISIBLE);
        btnPurchase.setVisibility(View.GONE);
        btnPurchase.setText(getString(R.string.sticker_store_act_download));
        isDownload = true;
        processBusiness(mStickerPackBean.data.id, 1);
        Intent intent = new Intent();
        intent.putExtra("id", mStickerPackBean.data.id);
        intent.putExtra("type", "buy");
        setResult(RESULT_OK, intent);
    }

    @OnClick(R.id.lin_back)
    public void onLinBackClicked() {
        PushUtils.finish(StickerPackDetailActivity.this);
    }

    @OnClick(R.id.txt_go_to_user_page)
    public void onTxtGoToUserPageClicked() {
//        Intent intent = new Intent(StickerPackDetailActivity.this, UserInfoActivity.class);
//        intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, userId + "");
//        startActivity(intent);
        FlutterRouterConfig.Companion.gotoUserInfo(userId + "");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_OK) {
            switch (requestCode) {
                case TheLConstants.REQUEST_CODE_BUY_VIP://如果是购买会员返回result_ok
                    loadVipConfig();
                    break;
            }
        }
    }

    private void loadVipConfig() {
        Flowable<VipConfigBean> flowable = RequestBusiness.getInstance().getVipConfig();
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<VipConfigBean>() {

            @Override
            public void onNext(VipConfigBean vipConfigBean) {
                super.onNext(vipConfigBean);
                //刷新缓存数据
                refreCache(vipConfigBean);
            }

        });
    }

    private void refreCache(VipConfigBean vipConfigBean) {
        if (vipConfigBean.data == null) {
            return;
        }
        UserUtils.setUserVipLevel(vipConfigBean.data.vipSetting.level);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        PushUtils.finish(StickerPackDetailActivity.this);
    }

    public void traceWanderLog(String activity) {
        try {

            LogInfoBean logInfoBean = new LogInfoBean();
            logInfoBean.page = "gallery.detail";
            logInfoBean.page_id = pageId;
            logInfoBean.activity = activity;

            MatchLogUtils.getInstance().addLog(logInfoBean);

        } catch (Exception e) {

        }
    }
}
