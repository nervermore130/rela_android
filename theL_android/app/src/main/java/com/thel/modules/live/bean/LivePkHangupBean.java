package com.thel.modules.live.bean;

import java.io.Serializable;

/**
 * Created by waiarl on 2017/12/4.
 */

public class LivePkHangupBean extends BaseLivePkBean implements Serializable {
    public int status;// 直播间当前状态 4: PK中 5: 结算中
    public int fromUserId;// 挂断方的用户ID
    /***pk挂断中***/
    public static final int HANGUP_STATUS_IN_PK = 4;//PK中
    public static final int HANGUP_STATUS_IN_SUMMARY = 5;//挂断状态 结算中
}
