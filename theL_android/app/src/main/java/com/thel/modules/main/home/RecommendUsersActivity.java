package com.thel.modules.main.home;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.bean.RecommendListBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.imp.follow.FollowStatusChangedImpl;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.service.DefaultRequestService;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.ViewUtils;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class RecommendUsersActivity extends BaseActivity {
    private SwipeRefreshLayout swipe_container;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager manager;
    // 推荐用户
    private List<RecommendListBean.GuideDataBean> recommendList = new ArrayList<>();
    private RecommendUsersdapter recommendUseAdapter;
    private ImageView back_iv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTheme(R.style.AppCompatTheme);


        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_recommend_users);


        initView();

        loadRecommendNetData();

        setListener();

    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void initView() {
        back_iv = findViewById(R.id.back_iv);
        swipe_container = findViewById(R.id.swipe_container);
        ViewUtils.initSwipeRefreshLayout(swipe_container);
        mRecyclerView = swipe_container.findViewById(R.id.listView);
        manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setHasFixedSize(true);

        recommendUseAdapter = new RecommendUsersdapter(recommendList);

        mRecyclerView.setAdapter(recommendUseAdapter);

    }

    private void setListener() {
        back_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        recommendUseAdapter.setOnRecyclerViewItemChildClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseRecyclerViewAdapter adapter, View view, int position) {
                switch (view.getId()) {
                    case R.id.txt_follow:
                        final int pos = (int) view.getTag() - adapter.getHeaderLayoutCount();
                        RecommendListBean.GuideDataBean dataBean = recommendUseAdapter.getItem(pos);
                        //关注/取消关注某个用户
                        String type = dataBean.isFollow ? ACTION_TYPE_CANCEL_FOLLOW : ACTION_TYPE_FOLLOW;
                        FollowStatusChangedImpl.followUser(dataBean.userId + "", type, dataBean.userName, dataBean.avatar);
                        dataBean.isFollow = !dataBean.isFollow;
                        recommendUseAdapter.notifyDataSetChanged();
                        break;
                }

            }
        });
        recommendUseAdapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                RecommendListBean.GuideDataBean dataBean = recommendUseAdapter.getData().get(position);

//                Intent intent = new Intent(RecommendUsersActivity.this, UserInfoActivity.class);
//                intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, dataBean.userId + "");
//                intent.putExtra("fromPage", RecommendUsersActivity.class.getName());
//                startActivity(intent);
                FlutterRouterConfig.Companion.gotoUserInfo(dataBean.userId+"");
            }
        });
    }

    private void loadRecommendNetData() {
        DefaultRequestService
                .createAllRequestService()
                .getRecommendUsers("all", "0", "40")
                .onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new InterceptorSubscribe<RecommendListBean>() {
                    @Override
                    public void onNext(RecommendListBean data) {
                        super.onNext(data);
                        if (data.data != null) {
                            showRecommendUser(data.data);
                        }
                    }
                });
    }

    private void showRecommendUser(RecommendListBean.RecommendDataBean data) {
        recommendList = data.list;

        if (data.list != null) {
            recommendUseAdapter.setNewData(recommendList);
        }

    }

    public class RecommendUsersdapter extends BaseRecyclerViewAdapter<RecommendListBean.GuideDataBean> {

        final OnItemChildClickListener mChildListener = new OnItemChildClickListener();

        public RecommendUsersdapter(List<RecommendListBean.GuideDataBean> data) {
            super(R.layout.search_recommend_listitem, data);
        }

        @Override
        protected void convert(BaseViewHolder helper, RecommendListBean.GuideDataBean bean) {
            if (bean.verifyType <= 0) {// 非加V用户，显示两行，第一行为昵称和简介，第二行为在线状态、距离、感情状态
                helper.setVisibility(R.id.line2, View.VISIBLE);
                helper.setVisibility(R.id.img_indentify, View.GONE);
                helper.setText(R.id.txt_desc, bean.recommendReason);

            } else {// 加V用户，显示一行，显示昵称、认证图标、认证信息
                helper.setVisibility(R.id.line2, View.VISIBLE);
                helper.setVisibility(R.id.img_indentify, View.VISIBLE);
                helper.setText(R.id.txt_desc, bean.verifyIntro);

                if (bean.verifyType == 1) {// 个人加V
                    helper.setImageResource(R.id.img_indentify, R.mipmap.icn_verify_person);
                } else {// 企业加V
                    helper.setImageResource(R.id.img_indentify, R.mipmap.icn_verify_enterprise);

                }
            }

            // 头像
            helper.setImageUrl(R.id.img_thumb, bean.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE);
            // 用户名
            helper.setText(R.id.txt_name, bean.nickName.trim());

            if (!ShareFileUtils.getString(ShareFileUtils.ID, "").equals(bean.userId + "")) {
                helper.setVisibility(R.id.txt_follow, View.VISIBLE);
                // 是否关注此用户
                if (!bean.isFollow) {//未关注
                    helper.setText(R.id.txt_follow, mContext.getString(R.string.follow));
                    helper.setBackgroundRes(R.id.txt_follow, R.drawable.bg_unfollow_shape_green_normal);
                    helper.setTextColor(R.id.txt_follow, mContext.getResources().getColor(R.color.White));

                } else {// 已关注
                    helper.setText(R.id.txt_follow, mContext.getString(R.string.userinfo_activity_followed));
                    helper.setBackgroundRes(R.id.txt_follow, R.drawable.bg_follow_shape_green_normal);
                    helper.setTextColor(R.id.txt_follow, mContext.getResources().getColor(R.color.text_color_green));
                }
            } else {
                helper.setVisibility(R.id.txt_follow, View.INVISIBLE);
            }
            helper.setOnItemChildClickListener(R.id.txt_follow, mChildListener);
            helper.setTag(R.id.txt_follow, helper.getLayoutPosition());
        }
    }


}
