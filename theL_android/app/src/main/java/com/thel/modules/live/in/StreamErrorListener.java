package com.thel.modules.live.in;

/**
 * Created by waiarl on 2017/5/11.
 */

public interface StreamErrorListener extends StreamListener {
    void onError(int what, int msg1, int msg2);
}
