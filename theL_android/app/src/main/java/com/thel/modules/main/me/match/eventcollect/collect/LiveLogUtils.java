package com.thel.modules.main.me.match.eventcollect.collect;

import com.thel.modules.main.me.bean.LogInfoBean;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.service.DefaultRequestService;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.FormBody;

public class LiveLogUtils {

    private List<LogInfoBean> logs = new ArrayList<>();
    private LogInfoBean logInfoBean = new LogInfoBean();

    private LiveLogUtils() {

    }

    private static LiveLogUtils instance = new LiveLogUtils();

    public static LiveLogUtils getInstance() {
        return instance;
    }

    public void addLog(LogInfoBean log) {

        logs.add(log);

        if (logs.size() > 0) {
            send();
            getLog();
            logs.clear();
        }


    }

    private void send() {

        String log = GsonUtils.createJsonString(logs);
        send(log);
    }

    public void send(String json) {
        L.d("LiveLogUtils", json);
        final FormBody formBody = new FormBody.Builder().add("logs", json).build();

        Flowable<String> flowable = DefaultRequestService.
                createTestReportRequestService().
                postMatchLogs(formBody);

        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<>());
    }


    public LogInfoBean getLog() {
        if (logs.size() > 0) {
            logInfoBean = logs.get(logs.size() - 1);

        }
        return logInfoBean;
    }

}
