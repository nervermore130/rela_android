package com.thel.modules.live.view;

import android.content.Context;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.constants.TheLConstants;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.live.bean.DanmuBean;
import com.thel.utils.Utils;


/**
 * Created by the L on 2016/9/8.
 */
public class DanmuItemView extends LinearLayout {
    private final Context mContext;
    public DanmuBean mDanmuBean;
    private ImageView img_avatar;
    private TextView txt_nickName;
    private TextView txt_content;
    public int itemPos = 0;//
    public boolean isAdd = false;
    private int mContentLimit;

    public DanmuItemView(Context context) {
        this(context, null);
    }

    public DanmuItemView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DanmuItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }

    private void init() {
        inflate(mContext, R.layout.live_danmu_item_view, this);
        img_avatar = findViewById(R.id.img_avatar);
        txt_nickName = findViewById(R.id.txt_nickname);
        txt_content = findViewById(R.id.txt_content);

        mContentLimit = 60;
    }

    public DanmuItemView initView(DanmuBean danmuBean, int itemPos) {
        mDanmuBean = danmuBean;
        ImageLoaderManager.imageLoaderDefaultCircle(img_avatar,R.mipmap.icon_user,danmuBean.avatar,TheLConstants.AVATAR_SMALL_SIZE,TheLConstants.AVATAR_SMALL_SIZE);
        txt_nickName.setText(danmuBean.nickName + ":");
        final String contentStr = Utils.getLimitedStr(danmuBean.content + "", mContentLimit);
        txt_content.setText(contentStr);
        this.itemPos = itemPos;
        if (danmuBean.vip > 0) {
            txt_nickName.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_red));
            txt_content.setTextColor(ContextCompat.getColor(mContext, R.color.red));
        }
        return this;
    }

}
