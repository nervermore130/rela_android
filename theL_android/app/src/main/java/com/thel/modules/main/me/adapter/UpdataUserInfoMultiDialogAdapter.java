package com.thel.modules.main.me.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;

import java.util.ArrayList;

/**
 * 多选dialog
 * @author zhangwenjia
 *
 */
public class UpdataUserInfoMultiDialogAdapter extends BaseAdapter {

	private LayoutInflater mInflater;

	ArrayList<Integer> selectPositions;

	private ArrayList<String> listData = new ArrayList<String>();


	public UpdataUserInfoMultiDialogAdapter(ArrayList<String> listData) {

		this.listData = listData;

		mInflater = (LayoutInflater) TheLApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public UpdataUserInfoMultiDialogAdapter(ArrayList<String> listData, ArrayList<Integer> selectPositions) {
		this.selectPositions = selectPositions;
		this.listData = listData;

		mInflater = (LayoutInflater) TheLApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public void refreshList(ArrayList<Integer> selectPositions) {
		this.selectPositions = selectPositions;
	}

	public void updataList(ArrayList<Integer> selectPositions, ArrayList<String> listData) {
		this.selectPositions = selectPositions;
		this.listData = listData;
	}

	@Override
	public int getCount() {
		return listData.size();
	}

	@Override
	public Object getItem(int position) {
		return listData.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, final ViewGroup parent) {
		convertView = mInflater.inflate(R.layout.updateuserinfo_dialog_listitem, parent, false);
		TextView txt = convertView.findViewById(R.id.txt);
		txt.setText(listData.get(position));
		int size = selectPositions.size();
		for(int i=0;i<size;i++){
			int selectPosition = selectPositions.get(i);
			if (position == selectPosition) {// 如果当前的行就是ListView中选中的一行，就更改显示样式
				convertView.setBackgroundColor(0xffd5e8e9);// 更改整行的背景色
			}
		}

		return convertView;
	}

}
