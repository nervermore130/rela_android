package com.thel.modules.main.me.aboutMe;

import android.graphics.Typeface;
import android.os.Bundle;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 充值记录页面
 */
public class RechargeRecordActivity extends BaseActivity implements View.OnClickListener {

    public static final String TAG_RECHARGE = "incoming";
    public static final String TAG_CONSUME = "outgoing";

    @BindView(R.id.lin_back)
    LinearLayout lin_back;

    @BindView(R.id.txt_title)
    TextView txt_title;

    @BindView(R.id.view_pager)
    ViewPager viewPager;

    @BindView(R.id.rel_hot)
    RelativeLayout rel_hot;

    @BindView(R.id.rel_new)
    RelativeLayout rel_new;

    @BindView(R.id.txt_hot_tab)
    TextView txt_hot_tab;

    @BindView(R.id.txt_new_tab)
    TextView txt_new_tab;

    @BindView(R.id.img_hot_buttomline)
    ImageView img_hot_buttomline;

    @BindView(R.id.img_new_buttomline)
    ImageView img_new_buttomline;

    private MyViewPagerAdapter adapter;

    private int currentTab = 0; // 当前选中的是第几个tab

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_recharge_record);

        ButterKnife.bind(this);

        initView();
    }

    private void initView() {
        txt_title.setText(R.string.recharge_and_consume_title);
        txt_hot_tab.setText(R.string.recharge_title);
        txt_new_tab.setText(R.string.consume_title);
        lin_back.setOnClickListener(this);
        rel_hot.setOnClickListener(this);
        rel_new.setOnClickListener(this);

        adapter = new MyViewPagerAdapter(getFragments());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                setTabStatus(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(this.getClass().getName());
        MobclickAgent.onResume(this);
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(this.getClass().getName());
        MobclickAgent.onPause(this);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
            return true;
        }
        return true;
    }

    private List<RechargeRecordFragment> list = new ArrayList<>();
    private RechargeRecordFragment rechargeFragment;
    private RechargeRecordFragment consumeFragment;

    private FragmentManager getFragments() {

        final FragmentManager manager = getSupportFragmentManager();

        rechargeFragment = (RechargeRecordFragment) manager.findFragmentByTag(TAG_RECHARGE);
        if (rechargeFragment == null) {
            rechargeFragment = RechargeRecordFragment.newInstance(TAG_RECHARGE);
        }

        consumeFragment = (RechargeRecordFragment) manager.findFragmentByTag(TAG_CONSUME);
        if (consumeFragment == null) {
            consumeFragment = RechargeRecordFragment.newInstance(TAG_CONSUME);
        }

        Collections.addAll(list, rechargeFragment, consumeFragment);

        return manager;
    }

    public void setTabStatus(int currentTab) {
        this.currentTab = currentTab;
        switch (currentTab) {
            case 0: // 最热回复
                img_hot_buttomline.setVisibility(View.VISIBLE);
                img_new_buttomline.setVisibility(View.INVISIBLE);
                txt_hot_tab.setTextColor(getResources().getColor(R.color.text_color_green));
                txt_new_tab.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_hot_tab.setTypeface(null, Typeface.BOLD);
                txt_new_tab.setTypeface(null, Typeface.NORMAL);
                viewPager.setCurrentItem(0);
                break;
            case 1: // 最新回复
                img_hot_buttomline.setVisibility(View.INVISIBLE);
                img_new_buttomline.setVisibility(View.VISIBLE);
                txt_hot_tab.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_new_tab.setTextColor(getResources().getColor(R.color.text_color_green));
                txt_hot_tab.setTypeface(null, Typeface.NORMAL);
                txt_new_tab.setTypeface(null, Typeface.BOLD);
                viewPager.setCurrentItem(1);
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lin_back:
                ViewUtils.preventViewMultipleClick(v, 2000);
                finish();
                break;
            case R.id.rel_hot:
                if (currentTab == 1) {
                    currentTab = 0;
                    setTabStatus(currentTab);
                } else {
                    if (rechargeFragment != null)
                        rechargeFragment.scrollToTop();
                }
                break;
            case R.id.rel_new:
                if (currentTab == 0) {
                    currentTab = 1;
                    setTabStatus(currentTab);
                } else {
                    if (consumeFragment != null)
                        consumeFragment.scrollToTop();
                }
                break;
        }
    }

    public class MyViewPagerAdapter extends FragmentStatePagerAdapter {
        public MyViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public RechargeRecordFragment getItem(int arg0) {
            return list.get(arg0);
        }

        @Override
        public int getCount() {
            return list.size();
        }
    }
}
