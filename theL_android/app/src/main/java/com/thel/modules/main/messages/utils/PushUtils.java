package com.thel.modules.main.messages.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;

import com.thel.app.TheLApp;
import com.thel.base.BaseDataBean;
import com.thel.constants.BundleConstants;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.manager.ActivityManager;
import com.thel.modules.live.surface.watch.LiveWatchActivity;
import com.thel.modules.live.surface.watch.horizontal.LiveWatchHorizontalActivity;
import com.thel.modules.main.MainActivity;
import com.thel.modules.main.home.moments.ReleaseThemeMomentActivity;
import com.thel.modules.main.home.moments.comment.MomentCommentActivity;
import com.thel.modules.main.home.moments.comment.MomentCommentReplyActivity;
import com.thel.modules.main.home.moments.theme.ThemeDetailActivity;
import com.thel.modules.main.home.moments.web.WebViewActivity;
import com.thel.modules.main.home.tag.TagDetailActivity;
import com.thel.modules.main.me.aboutMe.MatchActivity;
import com.thel.modules.main.me.aboutMe.MomentsMsgsActivity;
import com.thel.modules.main.me.aboutMe.MySoftMoneyActivity;
import com.thel.modules.main.me.aboutMe.StickerPackDetailActivity;
import com.thel.modules.main.me.aboutMe.StickerStoreActivity;
import com.thel.modules.main.me.bean.LogInfoBean;
import com.thel.modules.main.me.match.WhoLikesMeActivity;
import com.thel.modules.main.me.match.eventcollect.collect.MatchLogUtils;
import com.thel.modules.main.messages.ChatActivity;
import com.thel.modules.main.messages.bean.MsgBean;
import com.thel.modules.main.nearby.visit.VisitActivity;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.modules.main.video_discover.VideoDiscoverFragment;
import com.thel.modules.others.VipConfigActivity;
import com.thel.modules.video.UserVideoListActivity;
import com.thel.modules.welcome.WelcomeActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.service.DefaultRequestService;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;
import com.thel.utils.MD5Utils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.Utils;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import me.rela.rf_s_bridge.router.UniversalRouter;

/**
 * @author liuyun
 */
public class PushUtils {

    private static String pageId;
    private static String latitude;
    private static String longitude;

    public static void push(Context context, String msg) {

        L.d("PushUtils", " msg " + msg);

        if (TextUtils.isEmpty(msg)) {
            return;
        }
        Intent intent = new Intent();
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("startApp", "DefaultActivity");
        intent.putExtra("fromPage", "notification");
        MsgBean msgBean = GsonUtils.getObject(msg, MsgBean.class);
        pushActivity(msgBean, intent);

        pushLog(msg);

    }

    private static void pushLog(String msg) {

        latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
        longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");
        pageId = Utils.getPageId();

        LogInfoBean logInfoBean = new LogInfoBean();
        logInfoBean.page = "push";
        logInfoBean.page_id = pageId;
        logInfoBean.activity = "click";
        logInfoBean.lat = latitude;
        logInfoBean.lng = longitude;

        LogInfoBean.LogsDataBean logsDataBean = new LogInfoBean.LogsDataBean();
        if (!TextUtils.isEmpty(msg)) {
            logsDataBean.push_type = msg;

        }
        logInfoBean.data = logsDataBean;
        MatchLogUtils.getInstance().addLog(logInfoBean);

    }

    /**
     * 系统push跳转（非聊天跳转）
     *
     * @param msgBean
     * @param intent
     */
    public static void pushActivity(MsgBean msgBean, Intent intent) {

        if (msgBean == null) {
            return;
        }

        L.d("PushUtils", " msgBean pushId " + msgBean.pushId);

        L.d("PushUtils", " msgBean msgType " + msgBean.msgType);

        Map<String, String> map = new HashMap<>();

        map.put("id", msgBean.pushId);
        map.put("type", msgBean.msgType);
        map.put("action", "open_push");

        Flowable<BaseDataBean> flowable = DefaultRequestService.createAllRequestService().pushActionUpload(MD5Utils.generateSignatureForMap(map));

        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<>());
        L.d("PushUtils", " msgBean.messageTo " + msgBean.messageTo);

        intentActivity(msgBean, intent);
    }

    public static void intentActivity(MsgBean msgBean, Intent intent) {
        try {
            // 非聊天跳转
            if (!TextUtils.isEmpty(msgBean.messageTo)) {
                // 判断是否登录
                if (ShareFileUtils.getBoolean(ShareFileUtils.HAS_LOGGED, false)) {

                    Bundle bundle;

                    switch (Integer.parseInt(msgBean.messageTo)) {
                        // 打开软件
                        case 0:
                            intent.setClass(TheLApp.getContext(), WelcomeActivity.class);
                            if (intent.resolveActivity(TheLApp.getContext().getPackageManager()) != null) {
                                TheLApp.getContext().startActivity(intent);
                            }
                        case 1:
                            intent.setClass(TheLApp.getContext(), WelcomeActivity.class);
                            if (intent.resolveActivity(TheLApp.getContext().getPackageManager()) != null) {
                                TheLApp.getContext().startActivity(intent);
                            }
                            break;
                        // 发布日志
                        case 2:
                            intent.setClass(TheLApp.getContext(), ReleaseThemeMomentActivity.class);
                            intent.putExtra(TheLConstants.BUNDLE_KEY_RELEASE_TYPE, ReleaseThemeMomentActivity.RELEASE_TYPE_TOPIC);
                            intent.putExtra(TheLConstants.BUNDLE_KEY_TOPIC_NAME, msgBean.advertTitle);
                            intent.putExtra(TheLConstants.BUNDLE_KEY_AD_FLAG, false);
                            if (intent.resolveActivity(TheLApp.getContext().getPackageManager()) != null) {
                                TheLApp.getContext().startActivity(intent);
                            }
                            break;
                        // 话题详情
                        case 3:
                            FlutterRouterConfig.Companion.gotoTagDetails(msgBean.id, msgBean.advertTitle);
                            break;
                        // 聊天页  系统通知
                        case 4:
                            intent.setClass(TheLApp.getContext(), ChatActivity.class);
                            bundle = new Bundle();
                            bundle.putParcelable(TheLConstants.BUNDLE_KEY_MSG_BEAN, msgBean);

                            intent.putExtras(bundle);
                            if (intent.resolveActivity(TheLApp.getContext().getPackageManager()) != null) {
                                TheLApp.getContext().startActivity(intent);
                            }
                            break;
                        // 日志发现页
                        case 5:
                            intent.setClass(TheLApp.getContext(), MainActivity.class);
                            intent.putExtra(TheLConstants.BUNDLE_KEY_WHERE_TO_GO, TheLConstants.MainFragmentPageConstants.FRAGMENT_NEARBY);
                            intent.putExtra(TheLConstants.BUNDLE_KEY_GOTO_MOMENTSFRAGMENT_TAB, 1);
                            if (intent.resolveActivity(TheLApp.getContext().getPackageManager()) != null) {
                                TheLApp.getContext().startActivity(intent);
                            }
                            break;
                        // 广告页面
                        case 6:
                            intent.setClass(TheLApp.getContext(), WebViewActivity.class);
                            bundle = new Bundle();
                            bundle.putString(BundleConstants.URL, msgBean.advertUrl);
                            bundle.putString(BundleConstants.SITE, "ad");
                            bundle.putString("title", msgBean.advertTitle);
                            intent.putExtras(bundle);
                            if (intent.resolveActivity(TheLApp.getContext().getPackageManager()) != null) {
                                TheLApp.getContext().startActivity(intent);
                            }
                            break;
                        // 日志详情
                        case 7:
                            if (msgBean.msgType != null && msgBean.msgType.equals(TheLConstants.PushConstants.GETUI_RECEIVER_MSG_TYPE)) {
                                intent.setClass(TheLApp.getContext(), UserVideoListActivity.class);
                                intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, msgBean.fromUserId);
                                intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, msgBean.advertTitle);
                                if (intent.resolveActivity(TheLApp.getContext().getPackageManager()) != null) {
                                    TheLApp.getContext().startActivity(intent);
                                }
                            } else {
//                                intent.setClass(TheLApp.getContext(), MomentCommentActivity.class);
//                                intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, msgBean.advertTitle);
//                                intent.putExtra(TheLConstants.BUNDLE_KEY_BY_PUSH, MsgBean.COMMENTS_ID);
                                FlutterRouterConfig.Companion.gotoMomentDetails(msgBean.advertTitle);
                            }
                            break;
                        // 用户详情
                        case 8:
//                            intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, msgBean.advertTitle);
//                            intent.putExtra(TheLConstants.BUNDLE_KEY_BY_PUSH, MsgBean.COMMENTS_ID);
//                            intent.setClass(TheLApp.getContext(), UserInfoActivity.class);
                            FlutterRouterConfig.Companion.gotoUserInfo(msgBean.advertTitle);
                            break;
                        // 日志动态页
                        case 10:
//                            intent.setClass(TheLApp.getContext(), MomentsMsgsActivity.class);
//                            if (intent.resolveActivity(TheLApp.getContext().getPackageManager()) != null) {
//                                TheLApp.getContext().startActivity(intent);
//                            }
                            FlutterRouterConfig.Companion.gotoMomentMsg();
                            break;
                        // 表情商店
                        case 11:
                            intent.setClass(TheLApp.getContext(), StickerStoreActivity.class);
                            if (intent.resolveActivity(TheLApp.getContext().getPackageManager()) != null) {
                                TheLApp.getContext().startActivity(intent);
                            }
                            break;
                        // 表情详情页
                        case 12:
                            intent.setClass(TheLApp.getContext(), StickerPackDetailActivity.class);
                            try {
                                intent.putExtra("id", Long.valueOf(msgBean.advertTitle));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            if (intent.resolveActivity(TheLApp.getContext().getPackageManager()) != null) {
                                TheLApp.getContext().startActivity(intent);
                            }
                            break;
                        // 会员页面
                        case 13:
                            intent.setClass(TheLApp.getContext(), VipConfigActivity.class);
                            if (intent.resolveActivity(TheLApp.getContext().getPackageManager()) != null) {
                                TheLApp.getContext().startActivity(intent);
                            }
                            break;
                        // 话题详情
                        case 14:
//                            intent.setClass(TheLApp.getContext(), ThemeDetailActivity.class);
//                            intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, msgBean.advertTitle);
//                            intent.putExtra(TheLConstants.BUNDLE_KEY_BY_PUSH, MsgBean.COMMENTS_ID);
                            FlutterRouterConfig.Companion.gotoThemeDetails(msgBean.advertTitle);
                            break;
                        // 主播房间
                        case 15:
                            if (msgBean.isLandscape == 0) {
                                intent.setClass(TheLApp.getContext(), LiveWatchActivity.class);
                            } else {
                                intent.setClass(TheLApp.getContext(), LiveWatchHorizontalActivity.class);
                            }
                            intent.putExtra(TheLConstants.BUNDLE_KEY_ID, msgBean.advertTitle);
                            intent.putExtra(LiveWatchActivity.FROM_PAGE, LiveWatchActivity.FROM_PAGE_PUSH);
                            intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, msgBean.fromUserId);
                            if (intent.resolveActivity(TheLApp.getContext().getPackageManager()) != null) {
                                TheLApp.getContext().startActivity(intent);
                            }
                            break;
                        // 直播列表
                        case 16:
                            intent.setClass(TheLApp.getContext(), MainActivity.class);
                            intent.putExtra(TheLConstants.BUNDLE_KEY_WHERE_TO_GO, TheLConstants.MainFragmentPageConstants.FRAGMENT_DISCOVER);
                            intent.putExtra(TheLConstants.BUNDLE_KEY_GOTO_NEARBYFRAGMENT_TAB, 0);
                            if (intent.resolveActivity(TheLApp.getContext().getPackageManager()) != null) {
                                TheLApp.getContext().startActivity(intent);
                            }
                            break;
                        //评论回复详情
                        case 17:
//                            intent.setClass(TheLApp.getContext(), MomentCommentReplyActivity.class);
//                            intent.putExtra(TheLConstants.BUNDLE_KEY_COMMENT_ID, msgBean.advertTitle);
//                            intent.putExtra(TheLConstants.BUNDLE_KEY_INTENT_FROM, MomentCommentReplyActivity.FROM_MOMENT);
                            //此处不传commometId，避免评论盖楼，rela评论只限两层
                            if (msgBean.msgType.equals("at")) {
                                FlutterRouterConfig.Companion.gotoCommentReply(msgBean.commentsId + "");
                            } else {
                                FlutterRouterConfig.Companion.gotoCommentReply(msgBean.advertTitle + "");
                            }
                            break;
                        //跳转到访问记录页面
                        case 18:
                            intent.setClass(TheLApp.getContext(), VisitActivity.class);
                            intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, msgBean.advertTitle);
                            if (intent.resolveActivity(TheLApp.getContext().getPackageManager()) != null) {
                                TheLApp.getContext().startActivity(intent);
                            }
                            break;
                        case 19:
                            intent.setClass(TheLApp.getContext(), MainActivity.class);
                            intent.putExtra(TheLConstants.BUNDLE_KEY_WHERE_TO_GO, VideoDiscoverFragment.class.getName());
                            if (intent.resolveActivity(TheLApp.getContext().getPackageManager()) != null) {
                                TheLApp.getContext().startActivity(intent);
                            }
                            break;
                        case 20:
                            intent.setClass(TheLApp.getContext(), com.thel.modules.video.VideoListActivity.class);
                            if (intent.resolveActivity(TheLApp.getContext().getPackageManager()) != null) {
                                TheLApp.getContext().startActivity(intent);
                            }
                            break;
                        case 21:
                            intent.setClass(TheLApp.getContext(), MySoftMoneyActivity.class);
                            if (intent.resolveActivity(TheLApp.getContext().getPackageManager()) != null) {
                                TheLApp.getContext().startActivity(intent);
                            }
                            break;
                        case 22:
                            bundle = new Bundle();
                            bundle.putString(ShareFileUtils.MATCH_FROM_PAGE_ID, pageId);
                            bundle.putString(ShareFileUtils.MATCH_FROM_PAGE, "push");
                            intent.putExtras(bundle);
                            intent.setClass(TheLApp.getContext(), MatchActivity.class);
                            if (intent.resolveActivity(TheLApp.getContext().getPackageManager()) != null) {
                                TheLApp.getContext().startActivity(intent);
                            }
                            break;
                        case 23:
                            bundle = new Bundle();
                            bundle.putString(ShareFileUtils.MATCH_FROM_PAGE_ID, pageId);
                            bundle.putString(ShareFileUtils.MATCH_FROM_PAGE, "push");
                            intent.setClass(TheLApp.getContext(), WhoLikesMeActivity.class);
                            intent.putExtras(bundle);
                            if (intent.resolveActivity(TheLApp.getContext().getPackageManager()) != null) {
                                TheLApp.getContext().startActivity(intent);
                            }
                            break;
                        case 24:
                            intent.setClass(TheLApp.getContext(), ChatActivity.class);
                            bundle = new Bundle();
                            bundle.putParcelable(TheLConstants.BUNDLE_KEY_MSG_BEAN, msgBean);
                            intent.putExtras(bundle);
                            if (intent.resolveActivity(TheLApp.getContext().getPackageManager()) != null) {
                                TheLApp.getContext().startActivity(intent);
                            }
                            break;
                        default:
                            intent.setClass(TheLApp.getContext(), WelcomeActivity.class);
                            if (intent.resolveActivity(TheLApp.getContext().getPackageManager()) != null) {
                                TheLApp.getContext().startActivity(intent);
                            }
                            break;
                    }
                } else {
                    intent.setClass(TheLApp.getContext(), WelcomeActivity.class);
                    if (intent.resolveActivity(TheLApp.getContext().getPackageManager()) != null) {
                        TheLApp.getContext().startActivity(intent);
                    }
                }
            } else {
                intent.setClass(TheLApp.getContext(), WelcomeActivity.class);
                if (intent.resolveActivity(TheLApp.getContext().getPackageManager()) != null) {
                    TheLApp.getContext().startActivity(intent);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();

            intent.setClass(TheLApp.getContext(), WelcomeActivity.class);
            if (intent.resolveActivity(TheLApp.getContext().getPackageManager()) != null) {
                TheLApp.getContext().startActivity(intent);
            }
        }
    }

    /**
     * 获取魅族推送的数据
     *
     * @param intent
     * @return
     */
    public static MsgBean getMsgBeanByMZPush(Intent intent) {

        if (intent == null) {
            return null;
        }

        Uri uri = intent.getData();

        if (uri == null) {
            return null;
        }

        String fromUserId = uri.getQueryParameter("fromUserId");
        String fromNickname = uri.getQueryParameter("fromNickname");
        String toUserId = uri.getQueryParameter("toUserId");
        String messageTo = uri.getQueryParameter("messageTo");
        String advertTitle = uri.getQueryParameter("advertTitle");
        String dumpURL = uri.getQueryParameter("dumpURL");
        String msgType = uri.getQueryParameter("msgType");
        String extra = uri.getQueryParameter("extra");
        String expiry = uri.getQueryParameter("expiry");
        String payload = uri.getQueryParameter("payload");
        String withAll = uri.getQueryParameter("withAll");
        String advertUrl = uri.getQueryParameter("advertUrl");
        String pushId = uri.getQueryParameter("pushId");

        MsgBean msgBean = new MsgBean();
        msgBean.fromUserId = fromUserId;
        msgBean.fromNickname = fromNickname;
        msgBean.toUserId = toUserId;
        msgBean.messageTo = messageTo;
        msgBean.advertTitle = advertTitle;
        msgBean.dumpURL = dumpURL;
        msgBean.extra = extra;
        msgBean.expiry = expiry;
        msgBean.payload = payload;
        msgBean.withAll = withAll;
        msgBean.msgType = msgType;
        msgBean.advertUrl = advertUrl;
        msgBean.pushId = pushId;

        return msgBean;

    }

    /**
     * 获取华为推送的数据
     *
     * @param arg2
     * @return
     */
    public static MsgBean getMsgBeanByHWPush(Bundle arg2) {

        String fromUserId = arg2.getString("fromUserId", "");
        String fromNickname = arg2.getString("fromNickname", "");
        String toUserId = arg2.getString("toUserId", "");
        String messageTo = arg2.getString("messageTo", "");
        String advertTitle = arg2.getString("advertTitle", "");
        String dumpURL = arg2.getString("dumpURL", "");
        String msgType = arg2.getString("msgType", "");
        String extra = arg2.getString("extra", "");
        String expiry = arg2.getString("expiry", "");
        String payload = arg2.getString("payload", "");
        String withAll = arg2.getString("withAll", "");
        String advertUrl = arg2.getString("advertUrl", "");
        String pushId = arg2.getString("pushId");

        MsgBean msgBean = new MsgBean();
        msgBean.fromUserId = fromUserId;
        msgBean.fromNickname = fromNickname;
        msgBean.toUserId = toUserId;
        msgBean.messageTo = messageTo;
        msgBean.advertTitle = advertTitle;
        msgBean.dumpURL = dumpURL;
        msgBean.extra = extra;
        msgBean.expiry = expiry;
        msgBean.payload = payload;
        msgBean.withAll = withAll;
        msgBean.msgType = msgType;
        msgBean.advertUrl = advertUrl;
        msgBean.pushId = pushId;

        return msgBean;

    }

//    public void finish(Context context) {
//
//        Activity activity = ActivityManager.getInstance().getPreActivity();
//
//        L.d("BaseActivity", " finish activity : " + activity);
//
//        if (activity != null) {
//
//            boolean isMainActivity = activity instanceof CardMainActivity;
//
//            int stackActivityCount = ActivityManager.getInstance().getStackActivityCount();
//
//            L.d("BaseActivity", " finish isMainActivity : " + isMainActivity);
//
//            L.d("BaseActivity", " finish stackActivityCount : " + stackActivityCount);
//
//            L.d("BaseActivity", " finish getSimpleName : " + activity.getClass().getSimpleName());
//
//            if (!isMainActivity && ActivityManager.getInstance().getStackActivityCount() == 1) {
//                context.startActivity(new Intent(context, CardMainActivity.class));
//            }
//        }
//
//    }

    public static void finish(Activity activity) {
        if (ActivityManager.getInstance().getStackActivityCount() <= 1) {
            activity.startActivity(new Intent(activity, MainActivity.class));
        } else {
            activity.finish();
        }
    }

}