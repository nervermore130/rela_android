package com.thel.modules.main.home.search.tag;

import android.net.Uri;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.thel.R;
import com.thel.bean.SimpleTagBean;
import com.thel.constants.TheLConstants;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;
import com.thel.utils.ImageUtils;

import java.util.ArrayList;

/**
 * Created by chad
 * Time 17/10/23
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */

public class TrendingTagsRecyclerViewAdapter extends BaseRecyclerViewAdapter<SimpleTagBean> {

    public TrendingTagsRecyclerViewAdapter(ArrayList<SimpleTagBean> listPlusTrendingTags) {
        super(R.layout.trending_tags_item, listPlusTrendingTags);
    }

    @Override
    protected void convert(BaseViewHolder helper, SimpleTagBean simpleTagBean) {
        SimpleDraweeView img = helper.getView(R.id.img);
        TextView txt = helper.getView(R.id.txt);
        img.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(simpleTagBean.bgImg, TheLConstants.ICON_MIDDLE_SIZE, TheLConstants.ICON_MIDDLE_SIZE))).build()).setAutoPlayAnimations(true).build());
        txt.setText(simpleTagBean.tagsName);
    }
}
