package com.thel.modules.main.nearby.nearbymoment;

import com.thel.base.BasePresenter;
import com.thel.base.BaseView;
import com.thel.bean.moments.MomentsListBean;

/**
 * Created by waiarl on 2017/10/12.
 */

public class NearbyMomentContract {
    interface Presenter extends BasePresenter {
        /**
         * 获取刷新附近日志列表
         */
        void getRefreshNearbyMomentList();

        /**
         * 获取更多日志
         *
         * @param curPage
         */
        void getMoreNearbyMomentList(int curPage);

    }

    interface View extends BaseView<Presenter> {
        /**
         * 显示刷新附近日志列表
         *
         * @param momentsBeanList
         */
        void showRefreshNearbyMomentList(MomentsListBean momentsBeanList);

        /**
         * 显示更多附近日志列表
         *
         * @param momentsBeanList
         */
        void showMoreNearbyMomentList(MomentsListBean momentsBeanList);

        /**
         * 空数据
         */
        void emptyData();

        /**
         * 加载更多失败
         */
        void loadMoreFailed();

        /**
         * 请求结束
         */
        void requestFinished();

    }
}
