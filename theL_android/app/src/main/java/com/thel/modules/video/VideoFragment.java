package com.thel.modules.video;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import android.text.TextUtils;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.ksyun.media.player.IMediaPlayer;
import com.ksyun.media.player.KSYMediaPlayer;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseDataBean;
import com.thel.bean.PgcVideoSeekListBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.moments.MomentsBeanNew;
import com.thel.bean.video.VideoBean;
import com.thel.constants.BundleConstants;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.growingio.GrowingIoConstant;
import com.thel.imp.BaseFunctionFragment;
import com.thel.imp.ViewPagerScrollableCallback;
import com.thel.imp.follow.FollowStatusChangedImpl;
import com.thel.imp.like.MomentLikeUtils;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.main.home.moments.SendCardActivity;
import com.thel.modules.main.home.moments.comment.MomentCommentActivity;
import com.thel.modules.main.home.moments.winkcomment.WinkCommentsActivity;
import com.thel.modules.main.me.match.MatchSuccessActivity;
import com.thel.modules.main.messages.utils.PushUtils;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.modules.main.video_discover.autoplay.VideoSeenUtils;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestConstants;
import com.thel.network.service.DefaultRequestService;
import com.thel.ui.dialog.ActionSheetDialog;
import com.thel.ui.guide.GuidePopupBuilder;
import com.thel.ui.guide.GuidePopupUtils;
import com.thel.ui.guide.GuideViewBuilder;
import com.thel.ui.popupwindow.SharePopupWindow;
import com.thel.ui.popupwindow.VideoMorePW;
import com.thel.ui.widget.CommonTitleBar;
import com.thel.ui.widget.GestureFrameLayout;
import com.thel.ui.widget.LikeButtonView;
import com.thel.ui.widget.OnFullScreenListener;
import com.thel.ui.widget.OnMoreClickListener;
import com.thel.ui.widget.OnPlayStatusListener;
import com.thel.ui.widget.OnSeekListener;
import com.thel.ui.widget.VideoBottomLayout;
import com.thel.ui.widget.VideoLoadingLayout;
import com.thel.utils.AppInit;
import com.thel.utils.BusinessUtils;
import com.thel.utils.DialogUtil;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.L;
import com.thel.utils.MD5Utils;
import com.thel.utils.MomentUtils;
import com.thel.utils.ScreenUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SharedPrefUtils;
import com.thel.utils.StringUtils;
import com.thel.utils.ToastUtils;
import com.thel.utils.Utils;
import com.thel.utils.VideoUtils;
import com.thel.utils.ViewUtils;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * @author liuyun
 */
public class VideoFragment extends BaseFunctionFragment implements View.OnClickListener, TextureView.SurfaceTextureListener {

    public static final String VIDEO_LIST = "video_list";

    public static final String VIDEO_SINGER = "video_singer";

    private final int REQUEST_CODE_COMMENT = 0;
    private boolean hasShowFollowGuide = false;
    private boolean hasShowRecommendGuide = false;

    private VideoBean videoBean;
    private TextureView video_view;
    private ImageView pause_or_play_iv;
    private FrameLayout video_rl;
    private ImageView thumb_iv;
    private VideoLoadingLayout videoLoading;
    private FrameLayout video_container;
    private GestureFrameLayout root_rl;
    private VideoBottomLayout video_bottom_layout;
    private CommonTitleBar top_bar;
    private Surface mSurface;
    protected KSYMediaPlayer mSingerKSYMediaPlayer;

    private float defaultScreenWidth;
    private float defaultScaleHeight;
    private ImageView img_like_anim;
    protected String pageFrom;
    private float videoWidth;
    private float videoHeight;
    private long intoTime;//进入当前页面时间
    private long outTime;//离开当前页面时间
    private String entry = TheLConstants.EntryConstants.ENTRY_RECOMMENDVIDEOS;
    private long startTime = 0;
    private int playCount = 1;
    private boolean isFirstInto;
    private int loopingCount = 0;//循环播放次数
    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    private LinearLayout lin_recommend;
    private ViewPagerScrollableCallback viewPagerScrollableCallback;

    private GestureDetector mDetector;

    public static VideoFragment newInstance(VideoBean bean, String videoType, int position) {
        VideoFragment fragment = new VideoFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(TheLConstants.BUNDLE_KEY_MOMENT_BEAN, bean);
        bundle.putInt(TheLConstants.BUNDLE_KEY_VIDEO_LIST_POSITION, position);
        bundle.putString(TheLConstants.BUNDLE_KEY_PAGE_FROM, videoType);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            videoBean = (VideoBean) getArguments().getSerializable(TheLConstants.BUNDLE_KEY_MOMENT_BEAN);
            this.pageFrom = getArguments().getString(TheLConstants.BUNDLE_KEY_PAGE_FROM, VIDEO_SINGER);
        }
        intoTime = System.currentTimeMillis();
        isFirstInto = true;
    }

    @Override
    public View onCreateView(@Nullable LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_video2, container, false);
    }

    @Override
    public void onViewCreated(@Nullable View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        video_rl = view.findViewById(R.id.video_rl);
        thumb_iv = view.findViewById(R.id.thumb_iv);
        img_like_anim = view.findViewById(R.id.img_like_anim);
        pause_or_play_iv = view.findViewById(R.id.pause_or_play_iv);
        lin_recommend = view.findViewById(R.id.lin_recommend);
        videoLoading = view.findViewById(R.id.videoLoading);
        video_container = view.findViewById(R.id.video_container);
        root_rl = view.findViewById(R.id.root_rl);

        video_bottom_layout = view.findViewById(R.id.video_bottom_layout);
        top_bar = view.findViewById(R.id.top_bar);
    }

    public void setEntry(String entry) {
        this.entry = entry;
    }

    public void back() {
        if (getActivity() != null) {
            if (getActivity().getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
                setOrientation();
            } else {
                getActivity().finish();
            }
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        L.d("VideoFragment", " -------onActivityCreated------- : ");

        initListener();
        initPlayer();
        initView();
        timer();
        getMomentInfo();
    }

    private void initListener() {
        video_rl.setOnClickListener(this);
        pause_or_play_iv.setOnClickListener(this);
    }

    private void initPlayer() {

        if (getContext() == null) {
            return;
        }

        if (video_container != null && video_container.getChildCount() > 0) {
            video_container.removeAllViews();
        }

        video_view = new TextureView(getContext());

        video_container.addView(video_view);

        video_view.setSurfaceTextureListener(this);

        mSingerKSYMediaPlayer = new KSYMediaPlayer.Builder(getContext()).build();

        mSingerKSYMediaPlayer.setOnInfoListener(new IMediaPlayer.OnInfoListener() {
            @Override
            public boolean onInfo(IMediaPlayer iMediaPlayer, int i, int i1) {
                switch (i) {
                    case KSYMediaPlayer.MEDIA_INFO_BUFFERING_START://开始缓冲数据, 卡顿开始
                        videoLoading.setVisibility(View.VISIBLE);
                        break;
                    case KSYMediaPlayer.MEDIA_INFO_BUFFERING_END://数据缓冲完毕，卡顿结束
                        videoLoading.setVisibility(View.GONE);
                        break;
                }
                return false;
            }
        });

        mSingerKSYMediaPlayer.setOnErrorListener(new IMediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(IMediaPlayer iMediaPlayer, int what, int extra) {
                videoLoading.setVisibility(View.VISIBLE);
                ToastUtils.showToastShort(getActivity(), "Error code: " + what);
                return false;
            }
        });

        mSingerKSYMediaPlayer.setOnPreparedListener(new IMediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(IMediaPlayer iMediaPlayer) {

                startTime = System.currentTimeMillis();

                if (getUserVisibleHint()) {
                    pgcVideoAutoSeekPlay();
                } else {
                    mSingerKSYMediaPlayer.pause();
                }
            }
        });

        mSingerKSYMediaPlayer.setOnCompletionListener(new IMediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(IMediaPlayer iMediaPlayer) {

                mSingerKSYMediaPlayer.seekTo(0);

                loopingCount++;
            }
        });

        mSingerKSYMediaPlayer.setOnSeekCompleteListener(new IMediaPlayer.OnSeekCompleteListener() {
            @Override
            public void onSeekComplete(IMediaPlayer iMediaPlayer) {
                mSingerKSYMediaPlayer.start();
            }
        });

        mSingerKSYMediaPlayer.setBufferTimeMax(2);

        mSingerKSYMediaPlayer.setBufferSize(15);

        mSingerKSYMediaPlayer.setDecodeMode(KSYMediaPlayer.KSYDecodeMode.KSY_DECODE_MODE_AUTO);

        mSingerKSYMediaPlayer.setVideoScalingMode(KSYMediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
    }

    private void getMomentInfo() {

        if (videoBean == null) {
            return;
        }

        String momentId = videoBean.id;

        L.d("VideoFragment", " momentId : " + momentId);

        Flowable<MomentsBeanNew> flowable = DefaultRequestService.createAllRequestService().getMomentInfoV2(momentId);

        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<MomentsBeanNew>() {
            @Override
            public void onNext(MomentsBeanNew result) {
                super.onNext(result);
                if (getActivity() != null) {

                    if (!Utils.isMyself(videoBean.userId)) {
                        if (result.data != null) {
                            refreshFollowBtn(result.data.followerStatus);
                        }
                    }
                }
            }
        });

    }

    @Override
    public void onDestroy() {
        outTime = System.currentTimeMillis();
        int allPlayDuration = (int) ((outTime - intoTime) / 1000);
        GrowingIOUtil.uploadRecommendVideoDuration(allPlayDuration);

        if (mCompositeDisposable != null) {
            mCompositeDisposable.clear();
        }

        releasePlayer();
        uploadVideoInfo();

        GuidePopupUtils.dismiss();

        super.onDestroy();
    }

    public void setInfo(int progress, long currentPosition, long duration) {
        if ((double) currentPosition <= duration * 0.8 && (double) currentPosition >= duration * 0.7) {
            showFollowGuideView();
            showRecommendGuideView();
        }
//        if (long_video_seek != null) {
//            if (videoBean.playTime > 60) {
//                long_video_seek.setMax((int) (duration / 1000));
//                long_video_seek.setProgress(progress / 1000);
//                start_time.setText(Utils.formatTime2(currentPosition / 1000));
//                end_time.setText(Utils.formatTime2(duration / 1000));
//            } else {
//                short_video_seek.setMax((int) duration);
//                short_video_seek.setProgress((int) currentPosition);
//            }
//        }

        if (video_bottom_layout != null) {
            video_bottom_layout.setPlayTime(currentPosition, duration);
        }

    }

    @SuppressLint("SetTextI18n")
    private void initView() {
        if (videoBean != null) {
//            ImageLoaderManager.imageLoaderDefaultCircle(avatar, R.mipmap.icon_user, videoBean.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE);
//            moment_opts_emoji_txt.setText(videoBean.winkNum + " " + TheLApp.getContext().getString(R.string.like_msgs_title));
//            if (videoBean.commentNum > 0) {
//                moment_opts_comment_txt.setVisibility(View.VISIBLE);
//                moment_opts_comment_txt.setText(videoBean.commentNum + " " + TheLApp.getContext().getString(R.string.moment_comments_title));
//            } else {
//                moment_opts_comment_txt.setVisibility(View.INVISIBLE);
//            }
//            txt_nickname.setText(videoBean.nickname);
//            if (videoBean.text != null && videoBean.text.length() > 0) {
//                MomentUtils.setMomentContent(txt_desc, videoBean.text);
//            }
//            if (videoBean.winkFlag != MomentsBean.HAS_NOT_WINKED) {
//                moment_opts_like.setChecked(true, 1);
//            } else {
//                moment_opts_like.setChecked(false, 1);
//            }
//            if (videoBean.isFollow == FOLLOW_STATUS_FOLLOW || videoBean.isFollow == FOLLOW_STATUS_FRIEND || Utils.isMyself(videoBean.userId)) {
//                tv_follow.setVisibility(View.GONE);
//            } else {
//                tv_follow.setVisibility(View.VISIBLE);
//                tv_follow.setText(TheLApp.getContext().getString(R.string.userinfo_activity_follow));
//                tv_follow.setBackgroundResource(R.drawable.bg_follow_round_button_blue);
//                tv_follow.setTextColor(ContextCompat.getColor(TheLApp.getContext(), R.color.white));
//            }
//
            videoWidth = videoBean.pixelWidth;

            videoHeight = videoBean.pixelHeight;

            defaultScreenWidth = ScreenUtils.getScreenWidth(getContext());

            defaultScaleHeight = defaultScreenWidth / videoWidth * videoHeight;

            setVideoSize();
//
//            if (videoBean.playTime > 60) {//long video
//                //                end_time.setText(DensityUtils.longToChronometer(videoBean.playTime * 1000));
//                //                start_time.setVisibility(View.VISIBLE);
//                //                end_time.setVisibility(View.VISIBLE);
//                //                long_video_seek.setVisibility(View.VISIBLE);
//            } else {//short video
//                short_video_seek.setVisibility(View.VISIBLE);
//            }
//            ImageLoaderManager.imageLoader(thumb_iv, videoBean.image);
//
//            if (videoWidth / videoHeight > 1) {
//                full_screen_iv.setVisibility(View.VISIBLE);
//            } else {
//                full_screen_iv.setVisibility(View.GONE);
//            }
//
//            if (videoBean.videoTag > 0) {
//                lin_recommend.setVisibility(View.VISIBLE);
//            } else {
//                lin_recommend.setVisibility(View.GONE);
//            }

            video_bottom_layout.setData(videoBean);

            video_bottom_layout.setOnFullScreenListener(fullScreenStatus -> {

                if (fullScreenStatus == 0) {
                    Objects.requireNonNull(getActivity()).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    setVideoSize();
                } else {
                    Objects.requireNonNull(getActivity()).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                    setFullScreen();
                }

            });

            video_bottom_layout.setOnPlayStatusListener(playStatus -> {
                if (mSingerKSYMediaPlayer != null) {
                    if (playStatus == 0) {
                        mSingerKSYMediaPlayer.pause();
                        pause_or_play_iv.setVisibility(View.VISIBLE);
                    } else {
                        mSingerKSYMediaPlayer.start();
                        pause_or_play_iv.setVisibility(View.INVISIBLE);
                    }
                }
            });

            video_bottom_layout.setOnSeekListener(duration -> {
                if (mSingerKSYMediaPlayer != null) {
                    mSingerKSYMediaPlayer.seekTo(duration);
                }
            });
        }

        top_bar.setMOnMoreClickListener(view -> {
            VideoMorePW sharePopupWindow = new VideoMorePW(getContext(), videoBean);
            sharePopupWindow.showAtLocation(top_bar, Gravity.BOTTOM, 0, 0);
        });
    }

    private void setVideoSize() {

        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, (int) defaultScaleHeight);

        params.gravity = Gravity.CENTER;

        video_view.setLayoutParams(params);

        video_rl.setLayoutParams(params);

    }

    private void setFullScreen() {

        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);

        params.gravity = Gravity.CENTER;

        video_view.setLayoutParams(params);

        video_rl.setLayoutParams(params);

    }

    @Override
    public void onClick(View v) {

        L.d("VideoFragment", " onClick getId : " + v.getId());

        switch (v.getId()) {
            case R.id.pause_or_play_iv:
                play();
                break;
            case R.id.video_rl:

                boolean isShowSeekBar = ((mSingerKSYMediaPlayer != null && mSingerKSYMediaPlayer.getDuration() != 0));

                if (isShowSeekBar) {
                    if (video_bottom_layout.getVisibility() == View.VISIBLE) {
                        hideControlView();
                    } else {
                        showControlView();
                    }
                }
                break;
            case R.id.back:

                L.d("VideoFragment", " getActivity().getRequestedOrientation() : " + getActivity().getRequestedOrientation());

                if (getActivity().getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
                    setOrientation();
                } else {
                    PushUtils.finish(getActivity());
                }

                break;
            case R.id.more:

                boolean isCollect = BusinessUtils.isCollected(videoBean.id);

                String str;
                if (isCollect) {
                    str = getString(R.string.collection_cancel);
                } else {
                    str = getString(R.string.collection);
                }

                new ActionSheetDialog(getActivity()).builder().setCancelable(true).setCanceledOnTouchOutside(true).addSheetItem(getString(R.string.stranger_report), ActionSheetDialog.SheetItemColor.RED, new ActionSheetDialog.OnSheetItemClickListener() {
                    @Override
                    public void onClick(int which) {
                        MomentUtils.reportMoment(getActivity(), videoBean.id);

                    }
                }).addSheetItem(str, ActionSheetDialog.SheetItemColor.BLACK, new ActionSheetDialog.OnSheetItemClickListener() {
                    @Override
                    public void onClick(int which) {
                        if (videoBean != null) {
                            //是否被收藏,如果已经被收藏，则为true,要收藏，否则，要删除
                            final boolean isCollect = !BusinessUtils.isCollected(videoBean.id);
                            collect(isCollect, videoBean.id);
                        }
                    }
                }).show();

                break;
            case R.id.avatar:
                if (videoBean != null) {
                    ViewUtils.preventViewMultipleClick(v, 1000);
                    jumpToUserInfo(getActivity(), String.valueOf(videoBean.userId), "");
                }
                break;
            case R.id.tv_follow:
                if (videoBean != null) {
                    if (followStatus == 0) {
                        followStatus = 1;
                        FollowStatusChangedImpl.followUserWithNoDialog(videoBean.userId + "", FollowStatusChangedImpl.ACTION_TYPE_FOLLOW, videoBean.nickname, videoBean.avatar);
                    } else {
                        followStatus = 0;
                        FollowStatusChangedImpl.followUserWithNoDialog(videoBean.userId + "", FollowStatusChangedImpl.ACTION_TYPE_CANCEL_FOLLOW, videoBean.nickname, videoBean.avatar);
                    }

                    refreshFollowBtn(followStatus);

                }
                break;
            case R.id.iv_feed_recommend:
                if (videoBean != null && v.getContext() != null) {
                    recommendMoment(v.getContext(), videoBean);
                }
                break;
            case R.id.txt_desc:
                jumpToComment(false);
                break;
            case R.id.moment_opts_comment:
                jumpToComment(true);
                break;
            case R.id.moment_opts_like:
                ViewUtils.preventViewMultipleClick(v, 1000);
                if (MomentUtils.isBlackOrBlock(videoBean.id + "")) {
                    return;
                }
                if (videoBean.winkFlag == MomentsBean.HAS_NOT_WINKED) {
                    videoBean.winkNum++;
                    MomentLikeUtils.likeMoment(videoBean.id);
                    videoBean.winkFlag = MomentsBean.HAS_WINKED;
                    BusinessUtils.playSound(R.raw.sound_emoji);
                    showFollowGuideView();
                } else {
                    videoBean.winkNum--;
                    MomentLikeUtils.unLikeMoment(videoBean.id);
                    videoBean.winkFlag = MomentsBean.HAS_NOT_WINKED;
                }
                Intent broadIntent = new Intent(TheLConstants.BROADCAST_VIDEO_COUNT);
                broadIntent.putExtra(TheLConstants.BUNDLE_KEY_VIDEO_BEAN, videoBean);
                getActivity().sendBroadcast(broadIntent);
                break;
            case R.id.full_screen_iv:

                L.d("VideoFragment", " full_screen_iv: ");

                setOrientation();
                break;
            case R.id.small_screen_iv:

                L.d("VideoFragment", " small_screen_iv: ");

                setOrientation();
                break;
            case R.id.moment_opts_emoji_txt:
                ViewUtils.preventViewMultipleClick(v, 1000);
                Intent intent = new Intent(getContext(), WinkCommentsActivity.class);
                intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, videoBean.id);
                getContext().startActivity(intent);
                break;
            case R.id.moment_opts_comment_txt:
                jumpToComment(true);
                break;
            case R.id.img_collect:

                break;
            default:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_COMMENT && resultCode == Activity.RESULT_OK) {
            showFollowGuideView();
        }
    }

    private void showRecommendGuideView() {

        //20%概率弹出
        int n = new Random().nextInt(5);
        boolean isShow = false;
        if (n == 0) {
            isShow = true;
        }
        if (!isShow) return;

        if (getActivity().getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE)
            return;
        if (hasShowFollowGuide || hasShowRecommendGuide) return;
        if (videoBean.isFollow != FOLLOW_STATUS_FOLLOW && videoBean.isFollow != FOLLOW_STATUS_FRIEND)
            return;
        if (videoBean.winkFlag == MomentsBean.HAS_NOT_WINKED) return;
        final String[] guideText = {StringUtils.getString(R.string.video_guide_recommend_text)};
        final PopupWindow.OnDismissListener listener = new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {

            }
        };
        hasShowRecommendGuide = true;
    }

    private void showFollowGuideView() {

        if (getActivity() == null) {
            return;
        }

        //50%概率弹出
        int n = new Random().nextInt(2);
        boolean isShow = false;
        if (n == 0) {
            isShow = true;
        }
        if (!isShow) return;

        if (getActivity().getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE)
            return;

        if (hasShowFollowGuide || hasShowRecommendGuide) return;
        if (videoBean.isFollow == FOLLOW_STATUS_FOLLOW || videoBean.isFollow == FOLLOW_STATUS_FRIEND || Utils.isMyself(videoBean.userId))
            return;
        final String[] guideText = {StringUtils.getString(R.string.video_guide_follow_text)};
        final PopupWindow.OnDismissListener listener = new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
            }
        };

        hasShowFollowGuide = true;
    }

    private void setOrientation() {
        if (getActivity().getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
            //切换竖屏
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            setVideoSize();
            if (viewPagerScrollableCallback != null) {
                viewPagerScrollableCallback.canScroll(true);
            }
        } else {
            //切换横屏
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            setFullScreen();
            if (viewPagerScrollableCallback != null) {
                viewPagerScrollableCallback.canScroll(false);
            }
        }
    }

    private void jumpToUserInfo(Context context, String userId, String filter) {
        FlutterRouterConfig.Companion.gotoUserInfo(userId);
    }

    private void recommendMoment(Context context, VideoBean videoBean) {
        final String userId = videoBean.userId + "";

        L.d("VideoFragment", " userId : " + userId);

        if (MomentUtils.isBlackOrBlock(userId)) {
            return;
        }
        SharedPrefUtils.setBoolean(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.NEW_SHARE_MOMENT, true);
        final Intent intent = new Intent(context, SendCardActivity.class);
        intent.putExtra(TheLConstants.BUNDLE_KEY_INTENT_FROM, SendCardActivity.FROM_VIDEO);
        intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_BEAN, videoBean);
        intent.putExtra(TheLConstants.BUNDLE_KEY_PAGE_FROM, GrowingIoConstant.ENTRY_VIDEO);
        context.startActivity(intent);
    }

    private void jumpToComment(boolean isWriteMomentComment) {
        if (videoBean == null || TextUtils.isEmpty(videoBean.id) || getContext() == null) {
            return;
        }
        if (isWriteMomentComment) {
            FlutterRouterConfig.Companion.gotoMomentDetails(videoBean.id);
        }
    }

    int followStatus = -1;

    private void refreshFollowBtn(int status) {

        followStatus = status;

    }

    @Override
    public void onFollowStatusChanged(int followStatus, String userId, String nickName, String avatar) {
        closeLoading();

    }

    public Surface getSurface() {
        return mSurface;
    }

    private int mVideoProgress = 0;

    private void play() {
        if (mSingerKSYMediaPlayer != null) {
            if (mSingerKSYMediaPlayer.isPlaying()) {
                mSingerKSYMediaPlayer.pause();
                video_bottom_layout.pause();
            } else {
                mSingerKSYMediaPlayer.start();
                pause_or_play_iv.setVisibility(View.INVISIBLE);
                video_bottom_layout.play();
            }
        }
    }

    public void destroy() {
        uploadVideoInfo();
    }

    private void uploadVideoInfo() {
        long endTime = System.currentTimeMillis();

        long watchTime;

        if (startTime > 0 && endTime - startTime > 0) {
            watchTime = (endTime - startTime) / 1000;
        } else {
            watchTime = 1;
        }

        if (videoBean != null) {
            VideoUtils.postPlayVideoInfo(entry, videoBean.id, playCount, watchTime);
        }

    }


    private void timer() {
        Disposable disposable = Flowable.interval(1, TimeUnit.SECONDS).onBackpressureDrop().observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Long>() {
            @Override
            public void accept(Long aLong) {
                KSYMediaPlayer ksyMediaPlayer = null;

                if (mSingerKSYMediaPlayer != null) {
                    ksyMediaPlayer = mSingerKSYMediaPlayer;
                }

                if (ksyMediaPlayer != null) {

                    int position = (int) ksyMediaPlayer.getCurrentPosition();
                    long duration = ksyMediaPlayer.getDuration();
                    long currentPosition = ksyMediaPlayer.getCurrentPosition();
                    setInfo(position, currentPosition, duration);
                }

            }
        });
        mCompositeDisposable.add(disposable);
    }

    private void playCountRefresh() {
        Log.d("videoBean.playCount", "before: " + videoBean.playCount);
        videoBean.playCount++;
        Log.d("videoBean.playCount", "after: " + videoBean.playCount);

        //视频＋1通知
        if (getActivity() != null) {

            Intent intent = new Intent(TheLConstants.BROADCAST_VIDEO_COUNT);
            intent.putExtra(TheLConstants.BUNDLE_KEY_VIDEO_BEAN, videoBean);
            getActivity().sendBroadcast(intent);

            GrowingIOUtil.uploadVideoType(videoBean.videoType);

        }

    }

    /**
     * 收藏/取消收藏
     *
     * @param collect true为收藏，false为取消收藏
     */
    private void collect(boolean collect, final String momentsId) {

        Map<String, String> map = new HashMap<>();

        //收藏
        if (collect) {

            map.put(RequestConstants.FAVORITE_COUNT, momentsId);
            map.put(RequestConstants.FAVORITE_TYPE, RequestConstants.FAVORITE_TYPE_MOM);

            Flowable<BaseDataBean> flowable = DefaultRequestService.createMomentRequestService().getFavoriteCreate(MD5Utils.generateSignatureForMap(map));
            flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
                @Override
                public void onNext(BaseDataBean data) {
                    super.onNext(data);
                    String collectList = SharedPrefUtils.getString(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.FAVORITE_MOMENT_LIST, "");
                    String id = "[" + ShareFileUtils.getString(ShareFileUtils.ID, "") + "_" + momentsId + "]";
                    collectList += id;
                    SharedPrefUtils.setString(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.FAVORITE_MOMENT_LIST, collectList);
                }

                @Override
                public void onComplete() {
                    super.onComplete();

                    if (isAdded()) {

                        DialogUtil.showToastShort(TheLApp.context, getString(R.string.collection_success));

                    }

                }

            });
        } else {                  //取消收藏

            map.put(RequestConstants.FAVORITE_ID, "");
            map.put(RequestConstants.FAVORITE_COUNT, momentsId);
            map.put(RequestConstants.FAVORITE_TYPE, RequestConstants.FAVORITE_TYPE_MOM);

            Flowable<BaseDataBean> flowable = DefaultRequestService.createMomentRequestService().deleteFavoriteMoment(MD5Utils.generateSignatureForMap(map));
            flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
                @Override
                public void onNext(BaseDataBean data) {
                    super.onNext(data);
                    String collectList = SharedPrefUtils.getString(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.FAVORITE_MOMENT_LIST, "");
                    String id = "[" + ShareFileUtils.getString(ShareFileUtils.ID, "") + "_" + momentsId + "]";
                    collectList = collectList.replace(id, "");
                    SharedPrefUtils.setString(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.FAVORITE_MOMENT_LIST, collectList);
                    DialogUtil.showToastShort(getActivity(), getString(R.string.collection_canceled));
                }
            });
        }
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {

        if (mSingerKSYMediaPlayer != null) {
            try {
                mSingerKSYMediaPlayer.setDataSource(videoBean.videoUrl);
                if (mSurface == null) {
                    mSurface = new Surface(surface);
                }
                mSingerKSYMediaPlayer.setSurface(mSurface);
                mSingerKSYMediaPlayer.setVideoScalingMode(KSYMediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
                mSingerKSYMediaPlayer.prepareAsync();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {

        releasePlayer();
        return true;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (mSingerKSYMediaPlayer != null) {
                pgcVideoAutoSeekPlay();
                playCountRefresh();
            } else {
                if (getContext() != null) {
                    initPlayer();
                }
            }

            getMomentInfo();

        } else {
            if (mSingerKSYMediaPlayer != null) {
                mSingerKSYMediaPlayer.pause();
            }
        }
    }

    private boolean isFirstOnResume = true;

    @Override
    public void onResume() {
        super.onResume();
        if (getUserVisibleHint()) {
            if (mSingerKSYMediaPlayer != null) {

                if (pageFrom.equals(VIDEO_LIST) && isFirstOnResume) {
                    playCountRefresh();
                    isFirstOnResume = false;
                } else {
                    pgcVideoAutoSeekPlay();
                    playCountRefresh();
                }
            } else {
                initPlayer();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (getUserVisibleHint()) {

            if (mSingerKSYMediaPlayer != null) {
                mSingerKSYMediaPlayer.pause();
            }
        }
    }

    /**
     * PGC视频接着上次的继续播放
     */
    private void pgcVideoAutoSeekPlay() {

        if (videoBean.videoType == VideoBean.VIDEO_TYPE_PGC && isFirstInto) {
            List<PgcVideoSeekListBean.PgcVideoSeekBean> pgcVideoSeekList = VideoSeenUtils.getPgcVideoSeekList();
            if (pgcVideoSeekList != null) {
                for (int i = 0; i < pgcVideoSeekList.size(); i++) {
                    if (pgcVideoSeekList.get(i).videoId.equals(videoBean.id)) {
                        mSingerKSYMediaPlayer.seekTo(pgcVideoSeekList.get(i).seekTo);
                        break;
                    }
                }
            } else {
                mSingerKSYMediaPlayer.start();
            }
            isFirstInto = false;
        } else {
            if (mSingerKSYMediaPlayer != null) {
                mSingerKSYMediaPlayer.start();
            }
        }
    }

    private void releasePlayer() {
        if (mSingerKSYMediaPlayer != null) {

            //PGC视频，保存播放进度
            if (videoBean.videoType == VideoBean.VIDEO_TYPE_PGC) {
                VideoSeenUtils.saveOnePgcVideoSeekTo(videoBean.id, mSingerKSYMediaPlayer.getCurrentPosition());
            }

            mSingerKSYMediaPlayer.setSurface(null);
            mSingerKSYMediaPlayer.stop();
            mSingerKSYMediaPlayer.reset();
            mSingerKSYMediaPlayer = null;
        }
        if (mSurface != null) {
            mSurface.release();
            mSurface = null;
        }
    }

    public void setViewPagerScrollableCallback(ViewPagerScrollableCallback viewPagerScrollableCallback) {
        this.viewPagerScrollableCallback = viewPagerScrollableCallback;
    }

    private Disposable delayDisposable;

    private void delayHideControl() {

        if (mCompositeDisposable != null && delayDisposable != null) {
            mCompositeDisposable.remove(delayDisposable);
        }

        delayDisposable = Flowable.interval(0, 5, TimeUnit.SECONDS).subscribe(new Consumer<Long>() {
            @Override public void accept(Long aLong) throws Exception {
                hideControlView();
            }
        });

        mCompositeDisposable.add(delayDisposable);

    }

    private void showControlView() {
        video_bottom_layout.setVisibility(View.VISIBLE);
        top_bar.setVisibility(View.VISIBLE);
    }

    private void hideControlView() {
        video_bottom_layout.setVisibility(View.INVISIBLE);
        top_bar.setVisibility(View.INVISIBLE);
    }

}
