package com.thel.modules.main.nearby.visit;

import com.thel.base.BasePresenter;
import com.thel.base.BaseView;
import com.thel.bean.WhoSeenMeBean;
import com.thel.bean.WhoSeenMeListBean;

import java.util.List;

/**
 * Created by waiarl on 2017/10/12.
 */

public class VisitContract {
    interface Presenter extends BasePresenter {
        void getWhoSeenMeData();

        void getVisitData();

    }

    interface View extends BaseView<VisitContract.Presenter> {
        /**
         * 谁看过我
         *
         * @param whoSeenMeBeanList
         */
        void showWhoSeenMeData(WhoSeenMeListBean whoSeenMeBeanList);

        /**
         * 我看过谁
         *
         * @param whoSeenMeBeanList
         */
        void showVisitData(List<WhoSeenMeBean> whoSeenMeBeanList);

        void EmptyWhoSeenMeData();

        void EmptyVisitData();

        void requestFinish();

    }
}
