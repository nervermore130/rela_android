package com.thel.modules.preview_image;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.viewpager.widget.ViewPager;

import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseDataBean;
import com.thel.base.BaseDispatchTouchActivity;
import com.thel.bean.ImgShareBean;
import com.thel.bean.user.MyImageBean;
import com.thel.bean.user.UserInfoPicBean;
import com.thel.constants.TheLConstants;
import com.thel.modules.main.home.moments.ReleaseMomentActivity;
import com.thel.modules.main.me.aboutMe.UpdateUserInfoActivity;
import com.thel.modules.main.me.bean.UpdataInfoBean;
import com.thel.modules.main.messages.ChatActivity;
import com.thel.modules.select_image.ImageBean;
import com.thel.modules.select_image.SelectLocalImagesActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.ui.dialog.ActionSheetDialog;
import com.thel.ui.widget.zoom.VideoAndPhotoItemView;
import com.thel.utils.DialogUtil;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SharedPrefUtils;
import com.umeng.analytics.MobclickAgent;

import org.reactivestreams.Subscription;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


/**
 * 用户详情图片大图页
 */
public class UserInfoPhotoActivity extends BaseDispatchTouchActivity implements ViewPager.OnPageChangeListener {

    @BindView(R.id.rel_title)
    RelativeLayout rel_title;

    @BindView(R.id.img_back)
    ImageView allBtnBack;

    @BindView(R.id.lin_select)
    LinearLayout lin_select;

    @BindView(R.id.img_select)
    ImageView img_select;

    @BindView(R.id.done)
    Button done;

    @BindView(R.id.btn_set)
    Button btn_set;

    @BindView(R.id.img_delete)
    ImageView img_delete;

    @BindView(R.id.txt_title)
    TextView txt_title;

    @BindView(R.id.view_pager)
    ViewPager mViewPager;

    private int position;

    private String rela_id;

    private String userId;

    private boolean is_waterMark = true;

    private int totalPhotoCanSee;

    private boolean isDisplayTitle = true;

    // 设置功能
    private boolean canSet = false;

    private boolean canDelete = false;

    private boolean canSelect = false;

    private int selectAmountLimit = 0;

    private MyImageBean curMyImageBean;

    private ArrayList<?> dataList = null;

    // 选择图片，多图预览时，用来保存已选的图片的url
    private ArrayList<String> selectedPhotoUrls;
    private ArrayList<String> photoUrls;

    // 发布日志，多图预览时，用来保存已选的序号
    private ArrayList<Integer> selectedPhotoIndexes;

    private ArrayList<ImageBean> imageBeans;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_user_info_photo);

        ButterKnife.bind(this);

        initView();
    }

    private void initView() {
        Intent intent = getIntent();
        final String fromPage = intent.getStringExtra("fromPage");

        L.d("Zoom", " fromPage : " + fromPage);

        try {
            if (fromPage == null) {
//                rel_title.setVisibility(View.GONE);
                // 从其他页面，单纯展示图片
                ArrayList<String> photoUrls = intent.getStringArrayListExtra(TheLConstants.BUNDLE_KEY_PHOTOS);
                if (photoUrls == null) {
                    return;
                }
                rela_id = intent.getStringExtra(TheLConstants.BUNDLE_KEY_RELA_ID);
                is_waterMark = intent.getBooleanExtra(TheLConstants.IS_WATERMARK, true);
                ImgShareBean imageShareBean = (ImgShareBean) intent.getSerializableExtra(TheLConstants.BUNDLER_KEY_IMAGESHAREBEAN);
                position = intent.getIntExtra("position", 0);

                totalPhotoCanSee = photoUrls.size();
                mViewPager.setAdapter(new AlbumPhotosPagerAdapter(this, photoUrls, rela_id, is_waterMark, false, imageShareBean));
                mViewPager.setCurrentItem(position);
            } else if (fromPage.equals("UserInfoActivity")) { // 从用户详情页过来
                rel_title.setVisibility(View.GONE);
                ArrayList<UserInfoPicBean> userlist = (ArrayList<UserInfoPicBean>) intent.getSerializableExtra("userinfo");
                if (userlist == null) {
                    return;
                }
                ArrayList<String> photoUrls = new ArrayList<>();
                position = intent.getIntExtra("position", 0);
                ImgShareBean imageShareBean = (ImgShareBean) intent.getSerializableExtra(TheLConstants.BUNDLER_KEY_USER_IMAGE);
                UserInfoPicBean userinfoPicBean = (UserInfoPicBean) intent.getSerializableExtra(TheLConstants.BUNDLE_KEY_PIC_BEAN);
                int totalPhoto = userlist.size();
                for (int i = 0; i < totalPhoto; i++) {// 过滤掉没有密钥的加密图片
                    UserInfoPicBean tempBean = userlist.get(i);
                    photoUrls.add(tempBean.picUrl);
                }
                totalPhotoCanSee = photoUrls.size();
                rela_id = getIntent().getStringExtra(TheLConstants.BUNDLE_KEY_RELA_ID);
                userId = intent.getStringExtra("userId");
                final AlbumPhotosPagerAdapter adapter = new AlbumPhotosPagerAdapter(this, "UserInfoActivity", photoUrls, rela_id, is_waterMark, false, imageShareBean, userinfoPicBean, userId, new VideoAndPhotoItemView.DragListener() {
                    @Override
                    public void dismiss(boolean dismiss) {
                        finish();
                    }
                });
                mViewPager.setAdapter(adapter);
                mViewPager.setCurrentItem(position);
                mViewPager.setOffscreenPageLimit(2);
                /*adapter.setOnItemClickListener(new AlbumPhotosPagerAdapter.ItemClickListener() {
                    @Override
                    public void onItemClick(View v, int position) {
                        finish();
                    }
                });*/
            } else if (fromPage.equals("DetailedMatchUserinfoActivity")) { // 从匹配用户详情页面过来
                rel_title.setVisibility(View.GONE);
                ArrayList<String> photoUrlList = (ArrayList<String>) intent.getSerializableExtra("matchPicList");
                if (photoUrlList == null) {
                    return;
                }

                position = intent.getIntExtra("position", 0);
                totalPhotoCanSee = photoUrlList.size();
                rela_id = getIntent().getStringExtra(TheLConstants.BUNDLE_KEY_RELA_ID);
                userId = intent.getStringExtra("userId");

                final AlbumPhotosPagerAdapter adapter = new AlbumPhotosPagerAdapter(this, "DetailedMatchUserinfoActivity", photoUrlList, rela_id, is_waterMark, false, null, null, userId, new VideoAndPhotoItemView.DragListener() {
                    @Override
                    public void dismiss(boolean dismiss) {
                        finish();
                    }
                });
                mViewPager.setAdapter(adapter);
                mViewPager.setCurrentItem(position);

            } else if (fromPage.equals("UploadImageActivity")) { // 从上传照片页过来
                canSet = true;
                btn_set.setVisibility(View.VISIBLE);
                dataList = (ArrayList<MyImageBean>) intent.getSerializableExtra("userinfo");
                if (dataList == null) {
                    return;
                }

                L.d("UserInfoPhotoActivity", " dataList : " + dataList);

                position = intent.getIntExtra("position", 0);

                L.d("UserInfoPhotoActivity", " position : " + position);

                totalPhotoCanSee = dataList.size();

                AlbumPhotosPagerAdapter adapter = new AlbumPhotosPagerAdapter(this, dataList, rela_id, is_waterMark, false, null);
                mViewPager.setAdapter(adapter);
                mViewPager.setCurrentItem(position);
                mViewPager.setOffscreenPageLimit(5);

                curMyImageBean = (MyImageBean) dataList.get(position);

            } else if (fromPage.equals(ReleaseMomentActivity.class.getName())) {
                canDelete = true;
                img_delete.setVisibility(View.VISIBLE);
                photoUrls = (ArrayList<String>) intent.getSerializableExtra(TheLConstants.BUNDLE_KEY_PHOTOS);
                if (photoUrls == null) {
                    return;
                }
                selectedPhotoIndexes = new ArrayList<Integer>();
                for (int i = 0; i < photoUrls.size(); i++) {
                    selectedPhotoIndexes.add(i);
                }
                position = intent.getIntExtra("position", 0);
                totalPhotoCanSee = photoUrls.size();
                imageBeans = new ArrayList<>();
                for (int i = 0; i < photoUrls.size(); i++) {
                    ImageBean imageBean = new ImageBean();
                    imageBean.imageName = photoUrls.get(i);
                    imageBeans.add(imageBean);
                }
                LocalPhotoSimplePagerAdapter adapter = new LocalPhotoSimplePagerAdapter(imageBeans);
                mViewPager.setAdapter(adapter);
                mViewPager.setOffscreenPageLimit(1);
                mViewPager.setCurrentItem(position);
            } else if (fromPage.equals(ChatActivity.class.getName())) {
                canDelete = true;
                img_delete.setVisibility(View.GONE);
                photoUrls = (ArrayList<String>) intent.getSerializableExtra(TheLConstants.BUNDLE_KEY_PHOTOS);
                if (photoUrls == null) {
                    return;
                }
                selectedPhotoIndexes = new ArrayList<Integer>();
                for (int i = 0; i < photoUrls.size(); i++) {
                    selectedPhotoIndexes.add(i);
                }
                position = intent.getIntExtra("position", 0);
                totalPhotoCanSee = photoUrls.size();
//                imageBeans = new ArrayList<>();
//                for (int i = 0; i < photoUrls.size(); i++) {
//                    ImageBean imageBean = new ImageBean();
//                    imageBean.imageName = photoUrls.get(i);
//                    imageBeans.add(imageBean);
//                }
//                LocalPhotoSimplePagerAdapter adapter = new LocalPhotoSimplePagerAdapter(imageBeans);
                AlbumPhotosPagerAdapter adapter = new AlbumPhotosPagerAdapter(this, photoUrls, rela_id, is_waterMark, false, null);
                mViewPager.setAdapter(adapter);
                mViewPager.setOffscreenPageLimit(1);
                mViewPager.setCurrentItem(position);
            } else if (fromPage.equals(SelectLocalImagesActivity.class.getName())) {
                canSelect = true;
                allBtnBack.setVisibility(View.GONE);
                lin_select.setVisibility(View.VISIBLE);
                done.setVisibility(View.VISIBLE);
                photoUrls = new ArrayList<>();
                if (intent.getBooleanExtra("isPreview", true))
                    photoUrls.addAll((ArrayList<String>) intent.getSerializableExtra(TheLConstants.BUNDLE_KEY_PHOTOS));
                else
                    photoUrls.addAll(Arrays.asList(SharedPrefUtils.getStringWithoutEncrypt(SharedPrefUtils.FILE_PHOTO_PATH_CACHE, intent.getStringExtra("dir"), "").split(",")));
                selectedPhotoUrls = (ArrayList<String>) intent.getSerializableExtra(TheLConstants.BUNDLE_KEY_SELECTED_IMGS);
                position = intent.getIntExtra("position", 0);
                if (selectedPhotoUrls.contains(photoUrls.get(position))) {
                    img_select.setImageResource(R.mipmap.pictures_selected);
                } else {
                    img_select.setImageResource(R.mipmap.picture_unselected);
                }
                selectAmountLimit = intent.getIntExtra(TheLConstants.BUNDLE_KEY_SELECT_AMOUNT, 0);
                totalPhotoCanSee = photoUrls.size();
                ArrayList<ImageBean> imageBeans = new ArrayList<>();
                for (int i = 0; i < photoUrls.size(); i++) {
                    ImageBean imageBean = new ImageBean();
                    imageBean.imageName = photoUrls.get(i);
                    imageBeans.add(imageBean);
                }
                LocalPhotoSimplePagerAdapter adapter = new LocalPhotoSimplePagerAdapter(imageBeans);
                mViewPager.setAdapter(adapter);
                if (position >= 0) {
                    mViewPager.setCurrentItem(position);
                }
                refreshSelectedAmount();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        mViewPager.setOnPageChangeListener(this);
        txt_title.setText((position + 1) + "/" + totalPhotoCanSee);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(this.getClass().getName());
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(this.getClass().getName());
        MobclickAgent.onPause(this);
    }

    @Override
    public void finish() {
        if (canDelete) {
            Intent intent = new Intent();
            if (imageBeans != null) {
                intent.putParcelableArrayListExtra(TheLConstants.BUNDLE_KEY_IMAGE_OUTPUT_INFO, imageBeans);
            }
            if (selectedPhotoIndexes != null) {
                intent.putIntegerArrayListExtra(TheLConstants.BUNDLE_KEY_INDEX, selectedPhotoIndexes);
            }
            ImageBean imageBean = getIntent().getParcelableExtra(TheLConstants.BUNDLE_KEY_FLUTTER_VIDEO);
            intent.putExtra(TheLConstants.BUNDLE_KEY_OUT_FLUTTER_VIDEO, imageBean);
            setResult(TheLConstants.RESULT_CODE_WRITE_MOMENT_DELETE_PICTURE, intent);
        }
        super.finish();
    }

    public void refreshSelectedAmount() {
        int amount = selectedPhotoUrls.size();
        if (amount == 0) {
            done.setText(getString(R.string.info_done));
        } else {
            done.setText(getString(R.string.info_done) + " (" + amount + "/" + selectAmountLimit + ")");
        }
    }

    /**
     * 上滑关闭
     */

    float startX = 0, startY = 0, endX = 0, endY = 0;
    final int dis = 200;//滑动最小上下距离为200；

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                startX = event.getX();
                startY = event.getY();
                break;
            case MotionEvent.ACTION_UP:
                endX = event.getX();
                endY = event.getY();
                final float mx = endX - startX;
                final float my = endY - startY;
                if (Math.abs(mx) == 0 && my >= dis) {
                    finish();
                } else if (Math.abs(mx) > 0 && my >= dis) {
                    final double digree = Math.toDegrees(Math.atan(my / mx));
                    if (digree >= 70 || digree <= -70) {
                        finish();
                    }
                }
                break;
        }
        return super.dispatchTouchEvent(event);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int pos) {
        position = pos;
        txt_title.setText((pos + 1) + "/" + totalPhotoCanSee);
        if (canSelect) {// 可选择模式
            if (selectedPhotoUrls != null) {
                if (selectedPhotoUrls.contains(photoUrls.get(pos))) {
                    img_select.setImageResource(R.mipmap.pictures_selected);
                } else {
                    img_select.setImageResource(R.mipmap.picture_unselected);
                }
            }
        } else if (canSet) {
            curMyImageBean = (MyImageBean) dataList.get(pos);
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @OnClick(R.id.img_delete)
    void delete() {

        new ActionSheetDialog(this).builder().setCancelable(true).setCanceledOnTouchOutside(true)
                .setTitle(getString(R.string.uploadimage_activity_delete_confirm))
                .addSheetItem(getString(R.string.info_delete), ActionSheetDialog.SheetItemColor.RED, new ActionSheetDialog.OnSheetItemClickListener() {
                    @Override
                    public void onClick(int which) {
                        photoUrls.remove(mViewPager.getCurrentItem());
                        if (imageBeans != null) imageBeans.remove(mViewPager.getCurrentItem());
                        selectedPhotoIndexes.remove(mViewPager.getCurrentItem());
                        if (selectedPhotoIndexes.size() == 0) {
                            finish();
                            return;
                        }
                        totalPhotoCanSee = photoUrls.size();
                        mViewPager.getAdapter().notifyDataSetChanged();
                        txt_title.setText((mViewPager.getCurrentItem() + 1) + "/" + totalPhotoCanSee);
                    }
                })
                .show();
    }

    @OnClick(R.id.img_select)
    void select(View view) {
        if (selectedPhotoUrls != null) {
            if (selectedPhotoUrls.contains(photoUrls.get(mViewPager.getCurrentItem()))) {//如果已选中，则置为未选
                ((ImageView) view).setImageResource(R.mipmap.picture_unselected);
                selectedPhotoUrls.remove(photoUrls.get(mViewPager.getCurrentItem()));
            } else {// 如果未选中，则置为已选
                if (selectedPhotoUrls.size() == selectAmountLimit) {// 控制选择数量
                    return;
                }
                ((ImageView) view).setImageResource(R.mipmap.pictures_selected);
                selectedPhotoUrls.add(photoUrls.get(mViewPager.getCurrentItem()));
            }
            refreshSelectedAmount();
        }
    }

    @OnClick(R.id.img_back)
    void goBack() {
        finish();
    }

    @OnClick(R.id.done)
    void done() {
        Intent intent = new Intent();
        if (selectedPhotoUrls != null) {
            intent.putStringArrayListExtra(TheLConstants.BUNDLE_KEY_INDEX, selectedPhotoUrls);
            setResult(TheLConstants.RESULT_CODE_WRITE_MOMENT_DELETE_PICTURE, intent);
            finish();
        }
    }

    @OnClick(R.id.btn_set)
    void set() {
        if (curMyImageBean == null) {
            return;
        }
        if (curMyImageBean.coverFlag == 1) {// 封面图
            DialogUtil.getInstance().showSelectionDialog(UserInfoPhotoActivity.this, new String[]{TheLApp.getContext().getString(R.string.info_delete)}, new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
                    DialogUtil.getInstance().closeDialog();
                    switch (pos) {
                        case 0: // 删除
                            deletePhoto();
                            break;
                        default:
                            break;
                    }
                }
            }, true, 1, null);
        } else if (curMyImageBean.isPrivate == 1) {// 隐私图，可以解锁和删除
            DialogUtil.getInstance().showSelectionDialog(UserInfoPhotoActivity.this, new String[]{TheLApp.getContext().getString(R.string.uploadimage_activity_listitem_has_private), TheLApp.getContext().getString(R.string.info_delete)}, new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
                    DialogUtil.getInstance().closeDialog();
                    switch (pos) {
                        case 0:// 取消加密
                            showLoading();
                            setPrivateStatus();
                            break;
                        case 1: // 删除
                            deletePhoto();
                            break;
                        default:
                            break;
                    }
                }
            }, true, 1, null);
        } else {
            DialogUtil.getInstance().showSelectionDialog(UserInfoPhotoActivity.this, new String[]{TheLApp.getContext().getString(R.string.info_delete)}, new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
                    DialogUtil.getInstance().closeDialog();
                    switch (pos) {
                        case 0:// 删除
                            deletePhoto();
                            break;
                        default:
                            break;
                    }
                }
            }, true, 1, null);
        }
    }

    private void setPrivateStatus() {
        Flowable<BaseDataBean> flowable = RequestBusiness.getInstance().setPrivateStatus(curMyImageBean.picId + "");
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {

            @Override
            public void onNext(BaseDataBean baseDataBean) {
                super.onNext(baseDataBean);
                UpdateUserInfoActivity.needRefreshMyPhotos = true;
                curMyImageBean.isPrivate = 1 - curMyImageBean.isPrivate;
            }

        });
    }

    private void setCover() {
        Flowable<BaseDataBean> flowable = RequestBusiness.getInstance().setCover(curMyImageBean.picId + "");
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {

            @Override
            public void onNext(BaseDataBean baseDataBean) {
                super.onNext(baseDataBean);
                closeLoading();
                UpdateUserInfoActivity.needRefreshMyPhotos = true;
                curMyImageBean.coverFlag = 1;
                try {
                    // 将之前的封面图片刷为非封面
                    for (int i = 0; i < dataList.size(); i++) {
                        if (i == position) {
                            continue;
                        }
                        if (((MyImageBean) dataList.get(i)).coverFlag == 1) {
                            ((MyImageBean) dataList.get(i)).coverFlag = 0;
                            break;
                        }
                    }
                } catch (Exception e) {
                }
            }

        });
    }

    private void deletePhoto() {

        if (curMyImageBean == null || curMyImageBean.picUrl == null) {
            return;
        }

        String deleteMsg = TheLApp.getContext().getString(R.string.uploadimage_activity_delete_confirm);
        String type = curMyImageBean.picUrl.substring(curMyImageBean.picUrl.lastIndexOf("."));
        if (type.equals(".mp4")) {
            deleteMsg = getString(R.string.video_delete_confirm);
        }
        DialogUtil.getInstance().showConfirmDialog(this, "", deleteMsg, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                showLoading();
                Flowable<UpdataInfoBean> flowable = RequestBusiness.getInstance().deleteImage(curMyImageBean.picId + "");
                flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<UpdataInfoBean>() {

                    @Override
                    public void onNext(UpdataInfoBean baseDataBean) {
                        super.onNext(baseDataBean);
                        closeLoading();
                        if (baseDataBean.data != null) {

                            try {
                                UpdateUserInfoActivity.needRefreshMyPhotos = true;
                                ShareFileUtils.savaUserRatio(baseDataBean.data.ratio);

                                Intent intent = new Intent();
                                intent.setAction(TheLConstants.BROADCAST_UPDATA_RATIO);
                                TheLApp.getContext().sendBroadcast(intent);
                                dataList.remove(mViewPager.getCurrentItem());
                                if (dataList.size() == 0) {
                                    finish();
                                    return;
                                }
                                totalPhotoCanSee = dataList.size();
                                ((AlbumPhotosPagerAdapter) (mViewPager.getAdapter())).refreshAdapter(dataList);
                                mViewPager.getAdapter().notifyDataSetChanged();
                                txt_title.setText((mViewPager.getCurrentItem() + 1) + "/" + totalPhotoCanSee);
                                curMyImageBean = (MyImageBean) dataList.get(mViewPager.getCurrentItem());
                            } catch (Exception e) {
                                finish();
                                return;
                            }
                        }
                    }

                });
            }
        });
    }
}
