package com.thel.modules.live.bean;

/**
 * Created by waiarl on 2018/1/23.
 */

public class LiveSoundChoiceAppBean {
    public int id;
    public int icon;
    public String name;
    public int backColor;
    public String packageName;

    public LiveSoundChoiceAppBean(int id, int icon, String name, int backColor, String packageName) {
        this.id = id;
        this.icon = icon;
        this.name = name;
        this.backColor = backColor;
        this.packageName = packageName;
    }
}
