package com.thel.modules.main.me.bean;

import com.thel.base.BaseDataBean;

import java.util.ArrayList;

public class FriendsListBean extends BaseDataBean {
    public FriendListDataBean data;

    public class FriendListDataBean {

        /**
         * 朋友总数
         */
        public int friendTotal;

        /**
         * 关注总数
         */
        public int followersTotal;

        /**
         * 粉丝总数
         */
        public int fansTotal;

        @Override
        public String toString() {
            return "FriendListDataBean{" + "friendTotal=" + friendTotal + ", followersTotal=" + followersTotal + ", fansTotal=" + fansTotal + ", likeTotal=" + likeTotal + ", =" +   '}';
        }

        /**
         * liked总数
         */
        public int likeTotal;

        public boolean haveNextPage;

        public int cursor;

        public ArrayList<FriendsBean> users = new ArrayList<>();


    }


    @Override
    public String toString() {
        return "FriendsListBean{" + "data=" + data + '}';
    }
}
