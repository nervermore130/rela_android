package com.thel.modules.live.surface.watch

import android.os.Bundle
import android.os.Message
import com.google.gson.GsonBuilder
import com.thel.bean.RejectMicBean
import com.thel.bean.TopTodayListBean
import com.thel.bean.live.LinkMicRequestBean
import com.thel.bean.live.LiveMultiSeatBean
import com.thel.bean.live.MultiSpeakersBean
import com.thel.chat.live.interfaces.ILiveChat
import com.thel.chat.live.interfaces.OnMessageRequestListener
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_ANCHOR_ANDIENCE_NOT_IN
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_ANCHOR_CLOSE_DIALOG
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_ANCHOR_LINK_MIC_RESPONSE
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_ANCHOR_PK_CODE_500
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_ANCHOR_PK_IN_LINKMIC
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_ANCHOR_PK_IN_PK
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_ANCHOR_PK_LOWER_VERSION
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_ANCHOR_PK_NOT_LIVING
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_ANCHOR_PK_REQUEST_PK_OK
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_ANCHOR_PK_REQUEST_REJECT
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_ANCHOR_REJECT_LINK_MIC
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_GET_TOP_TODAY_LIST
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_MIC_LIST
import com.thel.modules.live.LiveCodeConstants.Companion.UI_EVENT_REFRESH_SEAT
import com.thel.modules.live.bean.AgoraBean
import com.thel.modules.live.bean.LiveRoomMsgBean
import com.thel.modules.live.interfaces.IChatRoomRequest
import com.thel.modules.live.surface.OnRelaMessageRequestListener
import com.thel.utils.GsonUtils
import com.thel.utils.L
import io.agora.rtc.IRtcEngineEventHandler
import org.json.JSONException
import org.json.JSONObject

class ChatRoomRequestImpl(private val iLiveChat: ILiveChat?, private val observer: LiveWatchObserver?) : IChatRoomRequest {

    private val tagName: String = "ChatRoomRequestImpl"

    override fun uploadAudioVolume(speakers: Array<IRtcEngineEventHandler.AudioVolumeInfo>) {
        val multiSpeakersBean = MultiSpeakersBean()
        multiSpeakersBean.method = LiveRoomMsgBean.TYPE_METHOD_UPLOAD_VOLUMN
        for (i in speakers.indices) {
            if (speakers[i].volume > 28) {
                val bean = MultiSpeakersBean.Speakers()
                bean.uid = speakers[i].uid
                bean.volumn = speakers[i].volume
                multiSpeakersBean.speakers.add(bean)
            }
        }

        if (multiSpeakersBean.speakers.size > 0) {

            val method = "audiobroadcast"

            val body = GsonBuilder().create().toJson(multiSpeakersBean);

            iLiveChat?.sendMsg(method, body, object : OnRelaMessageRequestListener() {
                override fun onMessageError(error: Throwable?) {
                }

                override fun onMessageSuccess(code: String, payload: String) {
                }

            })
        }

    }

    override fun banHer(userId: String) {

        val method = "ban"

        val body = JSONObject().put("userId", userId).toString()

        iLiveChat?.sendMsg(method, body, object : OnRelaMessageRequestListener() {
            override fun onMessageError(error: Throwable?) {
            }

            override fun onMessageSuccess(code: String, payload: String) {

            }

        })

    }

    override fun getMicSortList() {

        val method = "audiobroadcast"

        val body = JSONObject().put("method", LiveRoomMsgBean.TYPE_METHOD_MIC_LIST).toString()

        iLiveChat?.sendMsg(method, body, object : OnRelaMessageRequestListener() {
            override fun onMessageError(error: Throwable?) {
            }

            override fun onMessageSuccess(code: String, payload: String) {
                if ("OK" == code) {
                    L.d(tagName, "getMicSortList success")
                    val seat = GsonUtils.getObject(payload, LiveMultiSeatBean::class.java)
                    val msg = Message.obtain()
                    msg.obj = seat
                    msg.what = UI_EVENT_MIC_LIST
                    observer?.sendMessage(msg)
                }
            }

        })

    }

    override fun openMicSort() {

        val method = "audiobroadcast"

        val body = JSONObject()
                .put("method", LiveRoomMsgBean.TYPE_METHOD_MIC_ON).toString()

        iLiveChat?.sendMsg(method, body, object : OnRelaMessageRequestListener() {
            override fun onMessageError(error: Throwable?) {
            }

            override fun onMessageSuccess(code: String, payload: String) {
                L.d(tagName, " openMicSort onSuccess payload : $payload")

                if ("OK" == code) {
                    L.d(tagName, "openMicSort success")
                    observer?.sendEmptyMessage(UI_EVENT_REFRESH_SEAT)
                }
            }

        })

    }

    override fun closeMicSort() {

        val method = "audiobroadcast"

        val body = JSONObject()
                .put("method", LiveRoomMsgBean.TYPE_METHOD_MIC_OFF).toString()

        iLiveChat?.sendMsg(method, body, object : OnRelaMessageRequestListener() {
            override fun onMessageError(error: Throwable?) {
            }

            override fun onMessageSuccess(code: String, payload: String) {
            }
        })

    }


    override fun agreeOnSeat(userId: String, agora: AgoraBean) {
        val method = "audiobroadcast"

        val body = JSONObject()
                .put("userId", userId)
                .put("method", LiveRoomMsgBean.TYPE_METHOD_MIC_AGREE)
                .put("agora", GsonUtils.createJsonString(agora)).toString()

        iLiveChat?.sendMsg(method, body, object : OnRelaMessageRequestListener() {
            override fun onMessageError(error: Throwable?) {
            }

            override fun onMessageSuccess(code: String, payload: String) {
            }

        })
    }

    override fun startEncounter() {
        val method = "audiobroadcast"

        val body = JSONObject().put("method", LiveRoomMsgBean.TYPE_METHOD_START_ENCOUNTER).toString()

        iLiveChat?.sendMsg(method, body, object : OnRelaMessageRequestListener() {
            override fun onMessageError(error: Throwable?) {
            }

            override fun onMessageSuccess(code: String, payload: String) {
            }

        })
    }

    override fun sendPkRequestMsg(userId: String, x: Float, y: Float, width: Float, height: Float) {

        val obj = object : JSONObject() {
            init {
                put("method", LiveRoomMsgBean.PK_NOTICE_METHOD_REQUEST)
                put("toUserId", userId)
                put("x", x.toDouble())
                put("y", y.toDouble())
                put("height", height.toDouble())
                put("width", width.toDouble())
            }
        }

        val method = LiveRoomMsgBean.MSG_SEND_CODE_TYPE_PK

        val body = obj.toString()

        iLiveChat?.sendMsg(method, body, object : OnRelaMessageRequestListener() {
            override fun onMessageError(error: Throwable?) {

            }

            override fun onMessageSuccess(code: String, payload: String) {
                when (code) {
                    LiveRoomMsgBean.LIVE_MSG_RESPONSE_CODE_OK -> //正确返回
                        observer?.sendEmptyMessage(UI_EVENT_ANCHOR_PK_REQUEST_PK_OK)
                    LiveRoomMsgBean.TYPE_LIVE_PK_RESPONSE_CODE_NOT_LIVING -> {//主播不在直播
                        val msg = Message.obtain()
                        msg.what = UI_EVENT_ANCHOR_PK_NOT_LIVING
                        msg.obj = userId
                        observer?.sendMessage(msg)
                    }
                    LiveRoomMsgBean.TYPE_LIVE_PK_RESPONSE_CODE_IN_LINKMIC -> {//主播在连麦中
                        val msg = Message.obtain()
                        msg.what = UI_EVENT_ANCHOR_PK_IN_LINKMIC
                        msg.obj = userId
                        observer?.sendMessage(msg)
                    }
                    LiveRoomMsgBean.TYPE_LIVE_PK_RESPONSE_CODE_IN_PK -> {//主播在Pk中
                        val msg = Message.obtain()
                        msg.what = UI_EVENT_ANCHOR_PK_IN_PK
                        msg.obj = userId
                        observer?.sendMessage(msg)
                    }
                    LiveRoomMsgBean.TYPE_LIVE_PK_RESPONSE_CODE_LOW_CLI_VER -> //过低的客户端版本
                        observer?.sendEmptyMessage(UI_EVENT_ANCHOR_PK_LOWER_VERSION)
                    LiveRoomMsgBean.TYPE_LIVE_PK_RESPONSE_CODE_500 -> //服务端错误
                        observer?.sendEmptyMessage(UI_EVENT_ANCHOR_PK_CODE_500)
                    LiveRoomMsgBean.TYPE_LIVE_PK_RESPONSE_CODE_REJECT -> //上次发起PK被该主播拒绝，需等待10分钟才能对该主播再次发起PK
                        try {
                            val jsonObject = JSONObject(payload)
                            val leftTime = jsonObject.optString("leftTime")
                            val msg = Message.obtain()
                            msg.what = UI_EVENT_ANCHOR_PK_REQUEST_REJECT
                            msg.obj = leftTime
                            observer?.sendMessage(msg)
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                }
            }

        })

    }

    override fun connectMic(method: String, toUserId: String, x: Float, y: Float, height: Float, width: Float, nickName: String, avatar: String, dailyGuard: Boolean) {

        L.d(tagName," method : $method")

        val linkMicRequestBean = LinkMicRequestBean.ConnectBean(method, toUserId, x, y, height, width)

        val body = GsonUtils.createJsonString(linkMicRequestBean)

        L.d(tagName," body : $body")

        iLiveChat?.sendMsg("linkmic", body, object : OnRelaMessageRequestListener() {
            override fun onMessageError(error: Throwable?) {

            }

            override fun onMessageSuccess(code: String, payload: String) {
                when (code) {
                    "OK" -> {
                        val msg = Message.obtain()
                        msg.what = UI_EVENT_ANCHOR_LINK_MIC_RESPONSE
                        val bundle = Bundle()
                        bundle.putBoolean("dailyGuard", dailyGuard)
                        bundle.putString("toUserId", toUserId)
                        bundle.putString("nickName", nickName)
                        bundle.putString("avatar", avatar)
                        msg.data = bundle
                        observer?.sendMessage(msg)
                    }
                    "linkmic_reject" -> {
                        //观众拒绝连麦
                        val message = Message.obtain()
                        message.what = UI_EVENT_ANCHOR_REJECT_LINK_MIC
                        message.obj = GsonUtils.getObject(payload, RejectMicBean::class.java)
                        observer?.sendMessage(message)
                    }
                    "low_cli_ver" -> //版本号过低
                        //观众不在直播间
                        observer?.sendEmptyMessage(UI_EVENT_ANCHOR_ANDIENCE_NOT_IN)
                }
            }

        })

    }

    override fun responseAudienceLinkMic(method: String, body: String) {

        iLiveChat?.sendMsg(method, body, object : OnRelaMessageRequestListener() {
            override fun onMessageError(error: Throwable?) {
            }

            override fun onMessageSuccess(code: String, payload: String) {
                L.d(tagName, "payload: $payload")
                if ("OK" == code) {
                    L.d(tagName, "responseAudienceLinkMic success")
                    //                                    observer.sendEmptyMessage(UI_EVENT_REFRESH_SEAT);
                }
            }

        })
    }

    override fun getTopFansTodayList() {

        val method = "top_fans_today_list"

        val body = ""

        iLiveChat?.sendMsg(method, body, object : OnRelaMessageRequestListener() {
            override fun onMessageError(error: Throwable?) {

            }

            override fun onMessageSuccess(code: String, payload: String) {
                L.d(tagName, "payload: $payload")
                if ("OK" == code) {
                    L.d(tagName, "getTopFansTodayList success")
                    val list = GsonBuilder().create().fromJson<TopTodayListBean>(payload, TopTodayListBean::class.java).list
                    val message = Message.obtain()
                    message.what = UI_EVENT_GET_TOP_TODAY_LIST
                    message.obj = list
                    observer?.sendMessage(message)
                }
            }

        })

    }

    override fun exucuteColseRunable() {

        val method = "statistics"

        val body = ""

        iLiveChat?.sendMsg(method, body, object : OnMessageRequestListener {
            override fun onError(error: Throwable?) {

            }

            override fun onSuccess(code: String, payload: String) {

                if ("OK" == code) {
                    val msg = Message.obtain()
                    msg.what = UI_EVENT_ANCHOR_CLOSE_DIALOG
                    msg.obj = payload
                    observer?.sendMessage(msg)
                }
            }

        })

    }

    override fun sendColseMsg() {
        val method = "close"

        val body = ""

        iLiveChat?.sendMsg(method, body, object : OnRelaMessageRequestListener() {
            override fun onMessageError(error: Throwable?) {
            }

            override fun onMessageSuccess(code: String, payload: String) {



            }

        })
    }

    override fun arGiftReceipt(payload: String) {

        val method = "argift_receipt"

        val body = payload

        iLiveChat?.sendMsg(method, body, object : OnRelaMessageRequestListener() {
            override fun onMessageError(error: Throwable?) {
                L.d(tagName, " arGiftReceipt error : ${error?.message}")
            }

            override fun onMessageSuccess(code: String, payload: String) {
                L.d(tagName, " arGiftReceipt onSuccess code : $code")
            }

        })

    }

    override fun sendPkRequestCancelMsg(toUserId: String) {

        val obj = object : JSONObject() {
            init {
                put("method", LiveRoomMsgBean.PK_NOTICE_METHOD_CANCEL)
                put("toUserId", toUserId)
            }
        }


        val method = LiveRoomMsgBean.MSG_SEND_CODE_TYPE_PK

        val body = obj.toString()

        iLiveChat?.sendMsg(method, body, object : OnRelaMessageRequestListener() {
            override fun onMessageError(error: Throwable?) {

            }

            override fun onMessageSuccess(code: String, payload: String) {

            }

        })

    }

    override fun sendPkResponseMsg(toUserId: String, result: String, x: Float, y: Float, width: Float, height: Float) {

        val obj = object : JSONObject() {
            init {
                put("method", LiveRoomMsgBean.PK_NOTICE_METHOD_RESPONSE)
                put("toUserId", toUserId)
                put("result", result)
                put("x", x.toDouble())
                put("y", y.toDouble())
                put("height", height.toDouble())
                put("width", width.toDouble())

            }
        }

        val method = LiveRoomMsgBean.MSG_SEND_CODE_TYPE_PK

        val body = obj.toString()

        iLiveChat?.sendMsg(method, body, object : OnMessageRequestListener {
            override fun onError(error: Throwable?) {

            }

            override fun onSuccess(code: String, payload: String) {

            }

        })

    }

    override fun sendPkHangUpMsg(userId: String) {

        val obj = object : JSONObject() {
            init {
                put("method", LiveRoomMsgBean.PK_NOTICE_METHOD_HANGUP)
                put("toUserId", userId)
            }
        }

        val method = LiveRoomMsgBean.MSG_SEND_CODE_TYPE_PK

        val body = obj.toString()

        iLiveChat?.sendMsg(method, body, object : OnRelaMessageRequestListener() {
            override fun onMessageError(error: Throwable?) {
            }

            override fun onMessageSuccess(code: String, payload: String) {
            }

        })

    }

    override fun cancelConnectMic(method: String, toUserId: String) {

        val cancelBean = LinkMicRequestBean.CancelBean(method, toUserId)

        val body = GsonUtils.createJsonString(cancelBean)

        iLiveChat?.sendMsg("linkmic", body, object : OnMessageRequestListener {
            override fun onError(error: Throwable?) {

            }

            override fun onSuccess(code: String, payload: String) {

            }

        })

    }

}