package com.thel.modules.login.email

import android.app.Activity
import android.content.Intent
import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import com.thel.R
import com.thel.app.TheLApp
import com.thel.base.BaseUIFragment
import com.thel.flutter.bridge.RfSBridgeHandlerFactory
import com.thel.modules.login.LoginEventManager
import com.thel.network.LoginInterceptorSubscribe
import com.thel.network.RequestBusiness
import com.thel.network.api.loginapi.bean.SignInBean
import com.thel.utils.*
import com.umeng.analytics.MobclickAgent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_email_login.*
import me.rela.rf_s_bridge.RfSBridgePlugin
import me.rela.rf_s_bridge.router.UniversalRouter

class EmailLoginFragment : BaseUIFragment() {

    override fun initView() {

        val accounts = ShareFileUtils.getString(ShareFileUtils.ACCOUNT_AUTO_COMPLETE, "")
        if (!TextUtils.isEmpty(accounts)) {
            // 创建一个ArrayAdapter封装数组
            val av = activity?.let { ArrayAdapter<String>(it, android.R.layout.simple_dropdown_item_1line, accounts!!.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()) }
            auto_username.setAdapter(av)
        }

        button_login.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                ViewUtils.preventViewMultipleClick(view, 2000)
                val email = auto_username.text.toString().trim { it <= ' ' }

                if (!isEmail(email)) {
                    Toast.makeText(activity, getString(R.string.register_activity_wrong_email), Toast.LENGTH_SHORT).show()
                    return
                }

                var password = edit_password.text.toString().trim { it <= ' ' }

                if (email.length != 0 && password.length != 0) {
                    password = MD5Utils.md5(password)
                    showLoading()

                    RequestBusiness.getInstance()
                            .signIn("password", email, password, "", "", "", "", "", "", "", null)
                            .onBackpressureDrop().subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(object : LoginInterceptorSubscribe<SignInBean>() {
                                override fun onNext(data: SignInBean) {
                                    super.onNext(data)
                                    closeLoading()
                                    if (data.data == null || data.data.auth == null) return

                                    // 保存账户联想
                                    var accounts = ShareFileUtils.getString(ShareFileUtils.ACCOUNT_AUTO_COMPLETE, "")
                                    if (!accounts!!.contains(email)) {
                                        accounts = "$accounts$email,"
                                        ShareFileUtils.setString(ShareFileUtils.ACCOUNT_AUTO_COMPLETE, accounts)
                                    }
                                    activity?.setResult(Activity.RESULT_OK)

                                    UniversalRouter.sharedInstance.flutterPopToRoot(false)

                                    RfSBridgePlugin.getInstance().activeRpc("EventLogin", UserUtils.getMyUserId(), null)

                                    RfSBridgeHandlerFactory.getInstance().flutterPushImpl.pushUserInfo()

                                    RfSBridgeHandlerFactory.getInstance().flutterPushImpl.pushMyUserId()

                                    LoginEventManager.emailLoginCallback(activity, data)

                                    activity?.finish()
                                }

                                override fun onError(t: Throwable) {
                                    super.onError(t)
                                    closeLoading()
                                    if (errDataBean != null && !TextUtils.isEmpty(errDataBean.errdesc)) {
                                        ToastUtils.showCenterToastShort(activity, errDataBean.errdesc)
                                    }
                                }
                            })
                } else {
                    Toast.makeText(TheLApp.getContext(), TheLApp.getContext().getString(R.string.login_activity_password_not_empty), Toast.LENGTH_SHORT).show()
                }
            }

        })

        txt_forget.setOnClickListener {
            ViewUtils.preventViewMultipleClick(view, 2000)
            val intent = Intent(activity, EmailForgetActivity::class.java)
            startActivity(intent)
        }
    }

    override fun setLayout(): Int = R.layout.activity_email_login

    override fun onResume() {
        super.onResume()
        if (auto_username != null) {
            Handler(Looper.getMainLooper()).postDelayed({ ViewUtils.showSoftInput(activity, auto_username) }, 200)
        }
        MobclickAgent.onPageStart(this.javaClass.name)
        MobclickAgent.onResume(activity)
    }

    override fun onPause() {
        super.onPause()
        MobclickAgent.onPageEnd(this.javaClass.name)
        MobclickAgent.onPause(activity)
    }

    private fun isEmail(str: String): Boolean {
        return !TextUtils.isEmpty(str) && str.matches("^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$".toRegex())
    }
}