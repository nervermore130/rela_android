package com.thel.modules.live.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import androidx.fragment.app.Fragment;

import com.thel.R;
import com.thel.constants.TheLConstants;
import com.thel.utils.DialogUtil;

import static com.thel.utils.StringUtils.getString;

/**
 * Created by waiarl on 2017/11/10.
 */

public class LiveBroadcastImpl {
    private Fragment fragment;
    private LiveShowReceiver receiver;

    public interface LiveShowBroadcastListener {
        /**
         * 暂停
         */
        void pauseLiveShow();

        /**
         * 重启直播
         */
        void resumeLiveShow();

        /**
         * 微信分享成功
         */
        void wxShareSuccess();

        /**
         * 设置静音
         *
         * @param mulite
         */
        void setMulte(boolean mulite);
    }

    public void registReceiver(Fragment fragment) {
        this.fragment = fragment;
        receiver = new LiveShowReceiver();
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        intentFilter.addAction(TheLConstants.BROADCAST_WX_SHARE);
        intentFilter.addAction(TheLConstants.BROADCAST_VIDEO_PLAYING);//是否静音广播
        if (fragment != null && fragment.getActivity() != null) {
            fragment.getActivity().registerReceiver(receiver, intentFilter);
        }
    }

    public void unRegisterReceiver(Fragment fragment) {
        if (fragment != null && fragment.getActivity() != null) {
            try {
                fragment.getActivity().unregisterReceiver(receiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    class LiveShowReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent i) {
            if (i != null && fragment != null && fragment.getActivity() != null && fragment instanceof LiveShowBroadcastListener) {
                //网络
                if (ConnectivityManager.CONNECTIVITY_ACTION.equals(i.getAction())) {
                    ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();
                    if (activeNetInfo != null && activeNetInfo.isConnected()) {// 连上网络
                        int netType = activeNetInfo.getType();
                        if (netType != ConnectivityManager.TYPE_WIFI) {
                            ((LiveShowBroadcastListener) fragment).pauseLiveShow();
                            DialogUtil.showAlert(fragment.getActivity(), "", getString(R.string.live_net_changed), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    ((LiveShowBroadcastListener) fragment).resumeLiveShow();
                                }
                            });
                        }
                    }
                    //微信分享成功
                } else if (TheLConstants.BROADCAST_WX_SHARE.equals(i.getAction())) {// 微信、微博分享成功消息
                    if ("succeed".equals(i.getStringExtra("result"))) {
                        ((LiveShowBroadcastListener) fragment).wxShareSuccess();
                    }
                    // 静音
                } else if (TheLConstants.BROADCAST_VIDEO_PLAYING.equals(i.getAction())) {
                    boolean isPlaying = i.getBooleanExtra(TheLConstants.BUNDLE_KEY_VIDEO_PLAYING, false);
                    ((LiveShowBroadcastListener) fragment).setMulte(isPlaying);
                }
            }
        }
    }
}
