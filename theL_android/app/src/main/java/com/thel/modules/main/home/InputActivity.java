package com.thel.modules.main.home;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.text.Editable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.ReleasedCommentBeanV2New;
import com.thel.bean.StickerBean;
import com.thel.bean.comment.CommentBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.constants.TheLConstants;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestConstants;
import com.thel.network.service.DefaultRequestService;
import com.thel.ui.LoadingDialogImpl;
import com.thel.ui.widget.emoji.PreviewGridView;
import com.thel.ui.widget.emoji.SendEmojiLayout;
import com.thel.utils.BusinessUtils;
import com.thel.utils.DialogUtil;
import com.thel.utils.EmojiUtils;
import com.thel.utils.MD5Utils;
import com.thel.utils.MomentUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class InputActivity extends AppCompatActivity implements PreviewGridView.MyOnItemClickListener, AdapterView.OnItemClickListener {

    @BindView(R.id.input)
    EditText editText;

    @BindView(R.id.sendEmojiLayout)
    SendEmojiLayout sendEmojiLayout;// 表情键盘

    @BindView(R.id.rel_more_bottom)
    RelativeLayout rel_more_bottom;

    @BindView(R.id.img_emoji)
    ImageView img_emoji;

    @BindView(R.id.rel_preview)
    RelativeLayout rel_preview;

    @BindView(R.id.send)
    TextView send;

    private MomentsBean momentsBean;

    private LoadingDialogImpl loadingDialog;

    private int defaultSoftInputHeight = Utils.dip2px(TheLApp.getContext(), 200);

    private Handler mHandler = new Handler(Looper.getMainLooper());

    private int lastHeightDifference = 0;

    private boolean isSwitchEmoji = false;

    private ViewTreeObserver.OnGlobalLayoutListener onGlobalLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
        @Override
        public void onGlobalLayout() {
            Rect r = new Rect();
            View rootview = InputActivity.this.getWindow().getDecorView(); // this = activity
            rootview.getWindowVisibleDisplayFrame(r);

            int screenHeight = rootview.getHeight();
            int heightDifference = screenHeight - r.bottom;
            Log.d("getViewTreeObserver", "heightDifference: " + heightDifference);
            if (heightDifference > 300) {
                lastHeightDifference = heightDifference;
                int softHeight = ShareFileUtils.getInt(ShareFileUtils.SOFT_KEYBOARD_HEIGHT, 0);
                if (softHeight != heightDifference) {// 更新软键盘的高度
                    softHeight = heightDifference;
                    ShareFileUtils.setInt(ShareFileUtils.SOFT_KEYBOARD_HEIGHT, softHeight);
                }
                if (softHeight >= defaultSoftInputHeight) {
                    rel_more_bottom.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, softHeight));
                } else {
                    rel_more_bottom.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, defaultSoftInputHeight));
                }
                // 调整表情键盘的上间距
                sendEmojiLayout.refreshPaddingTop();

                hideBottomMore();
            } else {
                if (lastHeightDifference > 300 && !isSwitchEmoji && sendEmojiLayout.getVisibility() == GONE && rel_more_bottom.getVisibility() == GONE) {
                    InputActivity.this.finish();
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.O && isTranslucentOrFloating()) {
            boolean result = fixOrientation();
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);
        ButterKnife.bind(this);

        momentsBean = (MomentsBean) getIntent().getSerializableExtra(TheLConstants.BUNDLE_KEY_MOMENT_BEAN);

        loadingDialog = LoadingDialogImpl.LoadingDialogInstance.getInstance(this);

        initView();
    }

    private void initView() {

        sendEmojiLayout.initViews(rel_preview);

        int softHeight = ShareFileUtils.getInt(ShareFileUtils.SOFT_KEYBOARD_HEIGHT, 0);
        if (softHeight >= defaultSoftInputHeight) {
            rel_more_bottom.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, softHeight));
        } else {
            rel_more_bottom.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, defaultSoftInputHeight));
        }

        showSoftInputFromWindow();
        editText.getViewTreeObserver().addOnGlobalLayoutListener(onGlobalLayoutListener);

        img_emoji.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isSwitchEmoji = true;
                dismissKeyboard();
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (rel_more_bottom.getVisibility() == VISIBLE) {
                            if (sendEmojiLayout.getVisibility() == VISIBLE) {// 底部区域已弹出，则调出键盘
                                editText.getViewTreeObserver().removeOnGlobalLayoutListener(onGlobalLayoutListener);
                                isSwitchEmoji = false;
                                showKeyboard();
                            } else {
                                sendEmojiLayout.setVisibility(VISIBLE);
                                isSwitchEmoji = false;
                            }
                        } else {
                            rel_more_bottom.setVisibility(VISIBLE);
                            sendEmojiLayout.setVisibility(VISIBLE);
                            isSwitchEmoji = false;
                        }
                    }
                }, 100);
            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MomentUtils.isBlackOrBlock(momentsBean.userId + "")) {
                    return;
                }
                ViewUtils.preventViewMultipleClick(v, 2000);
                BusinessUtils.playSound(R.raw.sound_send);
                final String myComment = editText.getText().toString().replace("\n", " ").trim();

                if (TextUtils.isEmpty(myComment)) {
                    DialogUtil.showToastShort(InputActivity.this, getString(R.string.moment_comments_empty_tip));
                    return;
                }

//                loadingDialog.showLoading();

                Map<String, String> map = new HashMap<>();
                map.put(RequestConstants.I_TEXT, myComment);
                map.put(RequestConstants.I_TYPE, CommentBean.COMMENT_TYPE_TEXT);
                map.put(RequestConstants.I_TO_USER_ID, "");
                map.put(RequestConstants.I_MOMENT_ID, momentsBean.momentsId);

                Flowable<ReleasedCommentBeanV2New> flowable = DefaultRequestService.createMomentRequestService().replyMoment(MD5Utils.generateSignatureForMap(map));
                flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<ReleasedCommentBeanV2New>() {
                    @Override
                    public void onNext(ReleasedCommentBeanV2New data) {
                        super.onNext(data);
//                        loadingDialog.closeDialog();
                        Intent intentCast = new Intent();
                        intentCast.setAction(TheLConstants.BROADCAST_AUTO_RELEASE_NEW_COMMENT_ACTION);
                        intentCast.putExtra(RequestConstants.I_TEXT, myComment);
                        intentCast.putExtra(RequestConstants.I_TYPE, CommentBean.COMMENT_TYPE_TEXT);
                        intentCast.putExtra(RequestConstants.I_MOMENTS_ID, momentsBean.momentsId);
                        sendBroadcast(intentCast);
                        InputActivity.this.finish();
                    }

                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
//                        loadingDialog.closeDialog();
                    }
                });
            }
        });

        editText.addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (send == null) {
                    return;
                }

                if (s == null) {
                    send.setTextColor(ContextCompat.getColor(InputActivity.this, R.color.moment_chat_cancel_text));
                    send.setEnabled(false);
                    return;
                }

                int textLength = s.length();

                if (textLength > 0) {
                    send.setTextColor(ContextCompat.getColor(InputActivity.this, R.color.text_color_green));
                    send.setEnabled(true);
                } else {
                    send.setTextColor(ContextCompat.getColor(InputActivity.this, R.color.moment_chat_cancel_text));
                    send.setEnabled(false);
                }
            }

            @Override public void afterTextChanged(Editable s) {

            }
        });
    }

    private void hideBottomMore() {
        if (rel_more_bottom.getVisibility() == View.VISIBLE) {
            rel_more_bottom.setVisibility(View.GONE);
            sendEmojiLayout.setVisibility(View.GONE);
        }
    }

    /**
     * 关闭键盘
     */
    private void dismissKeyboard() {
        ViewUtils.hideSoftInput(this, editText);
    }

    /**
     * 开启键盘
     */
    private void showKeyboard() {
        hideBottomMore();
        ViewUtils.showSoftInput(this, editText);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                editText.getViewTreeObserver().addOnGlobalLayoutListener(onGlobalLayoutListener);
            }
        }, 500);
    }

    private void showSoftInputFromWindow() {
        editText.setFocusable(true);
        editText.setFocusableInTouchMode(true);
        editText.requestFocus();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

    public void setTranslucentBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //5.0 以上直接设置状态栏颜色
            View decorView = getWindow().getDecorView();
            int option = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
            decorView.setSystemUiVisibility(option);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
    }

    // 点击空白区域 自动隐藏软键盘
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (null != this.getCurrentFocus()) {
            /**
             * 点击空白位置 隐藏软键盘
             */
            InputMethodManager mInputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            return mInputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
        }
        return super.onTouchEvent(event);
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransition(0, 0);
    }

    @Override
    public void finish() {
        super.finish();
        // 参数1：MainActivity进场动画，参数2：SecondActivity出场动画
        overridePendingTransition(0, 0);
    }

    @Override
    protected void onDestroy() {
        loadingDialog.destroyDialog();
        editText.getViewTreeObserver().removeOnGlobalLayoutListener(onGlobalLayoutListener);
        super.onDestroy();
    }

    @Override
    public void onItemClick(View view) {
    }

    /**
     * 静态表情点击事件
     *
     * @param parent
     * @param view
     * @param position
     * @param id
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        StickerBean emoji = (StickerBean) view.findViewById(R.id.img).getTag();
        if (emoji.isDeleteBtn) {
            int selection = editText.getSelectionStart();
            String text = editText.getText().toString();
            if (selection > 0) {
                String text2 = text.substring(0, selection);
                if (text2.endsWith("]")) {
                    int start = text2.lastIndexOf("[");
                    int end = selection;
                    editText.getText().delete(start, end);
                    return;
                }
                editText.getText().delete(selection - 1, selection);
            }
        }
        if (!TextUtils.isEmpty(emoji.label)) {
            SpannableString spannableString = EmojiUtils.getInstace(EmojiUtils.DEFAULT_SIZE).addEmoji(this, (int) emoji.id, emoji.label);
            int index = editText.getSelectionStart();//获取光标所在位置
            Editable editable = editText.getEditableText();//获取EditText的文字
            if (index < 0 || index >= editText.length()) {
                editable.append(spannableString);
            } else {
                editable.insert(index, spannableString);//光标所在位置插入文字
            }
        }
    }


    private boolean isTranslucentOrFloating() {
        boolean isTranslucentOrFloating = false;
        try {
            int[] styleableRes = (int[]) Class.forName("com.android.internal.R$styleable").getField("Window").get(null);
            final TypedArray ta = obtainStyledAttributes(styleableRes);
            Method m = ActivityInfo.class.getMethod("isTranslucentOrFloating", TypedArray.class);
            m.setAccessible(true);
            isTranslucentOrFloating = (boolean) m.invoke(null, ta);
            m.setAccessible(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isTranslucentOrFloating;
    }

    private boolean fixOrientation() {
        try {
            Field field = Activity.class.getDeclaredField("mActivityInfo");
            field.setAccessible(true);
            ActivityInfo o = (ActivityInfo) field.get(this);
            o.screenOrientation = -1;
            field.setAccessible(false);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
