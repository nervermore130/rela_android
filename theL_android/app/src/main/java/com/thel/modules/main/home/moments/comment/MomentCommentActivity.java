package com.thel.modules.main.home.moments.comment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.SurfaceTexture;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ImageSpan;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.ksyun.media.player.IMediaPlayer;
import com.ksyun.media.player.KSYMediaPlayer;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseDataBean;
import com.thel.base.BaseImageViewerActivity;
import com.thel.bean.ReleasedCommentBeanV2;
import com.thel.bean.ReleasedCommentBeanV2New;
import com.thel.bean.ReleasedCommentReplyBean;
import com.thel.bean.ReleasedCommentReplyBeanNew;
import com.thel.bean.SharePosterBean;
import com.thel.bean.StickerBean;
import com.thel.bean.comment.CommentBean;
import com.thel.bean.comment.CommentListBeanNew;
import com.thel.bean.comment.CommentListBeanV2;
import com.thel.bean.gift.WinkCommentBean;
import com.thel.bean.moments.MomentParentBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.moments.MomentsBeanNew;
import com.thel.bean.video.VideoBean;
import com.thel.callback.event.UpdateMomentEvent;
import com.thel.constants.BundleConstants;
import com.thel.constants.MomentTypeConstants;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.growingio.GrowingIoConstant;
import com.thel.imp.black.BlackUtils;
import com.thel.imp.like.MomentLikeStatusChangedListener;
import com.thel.imp.like.MomentLikeUtils;
import com.thel.imp.momentblack.MomentBlackListener;
import com.thel.imp.momentblack.MomentBlackUtils;
import com.thel.imp.momentdelete.MomentDeleteUtils;
import com.thel.imp.sticker.StickerContract;
import com.thel.imp.sticker.StickerUtils;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.login.login_register.LoginRegisterActivity;
import com.thel.modules.main.home.moments.MomentDeleteActivity;
import com.thel.modules.main.home.moments.ReportActivity;
import com.thel.modules.main.home.moments.SendCardActivity;
import com.thel.modules.main.home.moments.theme.ThemeDetailActivity;
import com.thel.modules.main.home.moments.web.WebViewActivity;
import com.thel.modules.main.home.moments.winkcomment.WinkCommentsActivity;
import com.thel.modules.main.me.aboutMe.UpdateUserInfoActivity;
import com.thel.modules.main.me.bean.MyInfoBean;
import com.thel.modules.main.messages.utils.PushUtils;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.modules.others.VipConfigActivity;
import com.thel.modules.post.MomentPosterActivity;
import com.thel.modules.video.UserVideoListActivity;
import com.thel.modules.welcome.WelcomeActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestConstants;
import com.thel.network.service.DefaultRequestService;
import com.thel.ui.TextSpan;
import com.thel.ui.TopicClickSpan;
import com.thel.ui.adapter.MomentReplyRecyclerViewAdapter;
import com.thel.ui.decoration.DefaultItemDivider;
import com.thel.ui.widget.AutoSplitTextView;
import com.thel.ui.widget.FollowView;
import com.thel.ui.widget.LikeButtonView;
import com.thel.ui.widget.MomentParentViewOld;
import com.thel.ui.widget.MultiImageViewGroup;
import com.thel.ui.widget.RecommendLiveUserView;
import com.thel.ui.widget.RecommendWebView;
import com.thel.ui.widget.SquareTextureView;
import com.thel.ui.widget.emoji.PreviewGridView;
import com.thel.ui.widget.emoji.SendEmojiLayout;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.utils.AppInit;
import com.thel.utils.BusinessUtils;
import com.thel.utils.DeviceUtils;
import com.thel.utils.DialogUtil;
import com.thel.utils.DialogUtils;
import com.thel.utils.EmojiUtils;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.GsonUtils;
import com.thel.utils.ImageUtils;
import com.thel.utils.JsonUtils;
import com.thel.utils.L;
import com.thel.utils.MD5Utils;
import com.thel.utils.MomentUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SharedPrefUtils;
import com.thel.utils.SizeUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;
import com.thel.utils.VideoUtils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.UMShareAPI;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * moment详情评论页面
 *
 * @author Setsail
 */
public class MomentCommentActivity extends BaseImageViewerActivity implements OnClickListener, PreviewGridView.MyOnItemClickListener, OnItemClickListener, BaseRecyclerViewAdapter.RequestLoadMoreListener, BaseRecyclerViewAdapter.ReloadMoreListener, MomentBlackListener, StickerContract.StickerChangedListener, MomentLikeStatusChangedListener {

    private RelativeLayout rel_title;
    private ImageView img_share;
    private ImageView img_new;
    private LinearLayout lin_back;
    private TextView title_txt;
    private SwipeRefreshLayout swipe_container;
    private RecyclerView listView;
    private EditText input;
    private TextView textLength;
    private TextView submit;

    private RelativeLayout rel_more_bottom;
    private SendEmojiLayout sendEmojiLayout;// 表情键盘
    private ImageView img_emoji;
    private int defaultSoftInputHeight = SizeUtils.dip2px(TheLApp.getContext(), 200);

    private LinearLayout default_page;
    private RelativeLayout momentView;

    private Handler mHandler = new Handler(Looper.getMainLooper());

    // 当前的moment对象
    private MomentsBean momentBean;
    // 如果是从消息进入，则没有momentBean，只有momentsId，然后通过momentsId去请求momentBean
    private String momentsId;

    private ArrayList<CommentBean> commentList = new ArrayList<>();
    private MomentReplyRecyclerViewAdapter listAdapter;

    private int limit = 20;
    private int cursor = 0;

    private boolean haveNextPage = false;

    private static long lastClickTime = 0;

    // 要回复的评论
    private CommentBean toComment = null;
    private int toCommentPosition = -1;
    // 要操作的评论
    private CommentBean operateComment = null;

    // 返回moments列表时需不需要刷新该条moment
    private boolean needRefresh = false;

    // 0:moments页面 1:消息提醒页面
    private int fromPageType = 0;

    private boolean hasInitedUi = false;

    private String myComment;
    private String commentType;

    private String fromPage;

    private CallbackManager callbackManager;

    private TextWatcher textWatcher;

    /**
     * 推荐用户以外的日志
     */
    private LinearLayout moment_content;

    /**
     * 视频部分
     */
    private RelativeLayout rel_video;
    private KSYMediaPlayer player;
    private SquareTextureView mTextureView;
    private Surface mSurface;
    private ProgressBar progressbar_video;
    private SimpleDraweeView img_cover;
    private SimpleDraweeView img_play_video;
    private FollowView txt_follow;//header中新增加关注按钮
    private TextView txt_play_times;//播放次数

    private View layout_moment_theme;

    private ImageView img_theme;

    private TextView select_theme;

    /**
     * 推荐用户部分
     */
    private RelativeLayout rel_user_card;
    private SimpleDraweeView img_user_card;
    private LinearLayout mask_user_card;
    private SimpleDraweeView img_user_card_avatar;
    private TextView txt_user_card_nickname;
    private TextView txt_user_card_info;
    private TextView txt_user_card_intro;

    private LinearLayout lin_update_user_info;

    private LinearLayout wide_divider;
    private DataChangedReceiver dataChangedReceiver;
    private RecommendWebView recommend_web_view;
    private RecommendLiveUserView live_user_liveview;
    private MomentParentViewOld moment_parent_view;
    private BlackUtils blackUtils;
    private MomentBlackUtils momentBlackUtils;
    private StickerUtils stickerUtils;
    private MomentLikeUtils mMomentLikeUtils;
    private ImageView iv_feed_recommend;
    private ImageView moment_img_vip;
//    private ImageView moment_to_chat_iv;

    public static final String WriteMomentComment = "comment";//日志列表页面 点击评论框 自动滑动评论最后一条
    private String needGlide;
    private RelativeLayout moment_operations;
    private String vedio = "icon_cameralive.gif";
    private String voice = "icon_miclive.gif";
    private LinearLayout ll_live_status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.moment_comment_activity);

        findViewById();

        needRefreshStickers = false;
        /** 分享初始化 **/
        callbackManager = CallbackManager.Factory.create();
        /** 分享初始化 **/

        RelativeLayout rel_preview = findViewById(R.id.rel_preview);

        sendEmojiLayout.initViews(rel_preview);

        L.d("emoji", " sendEmojiLayout 2 : " + sendEmojiLayout);

        Intent intent = getIntent();

        // 如果是从外部进入
        String action = intent.getAction();
        if (Intent.ACTION_VIEW.equals(action)) {
            if (!ShareFileUtils.getBoolean(ShareFileUtils.HAS_LOGGED, false)) {//如果没登陆
                startActivity(new Intent(this, WelcomeActivity.class));
                finish();
                return;
            }
            Uri uri = intent.getData();
            if (uri != null) {
                swipe_container.post(new Runnable() {
                    @Override
                    public void run() {
                        swipe_container.setRefreshing(true);
                    }
                });
                momentsId = uri.getQueryParameter("momentId");
                fromPageType = 1;
                getMomentInfoV2(momentsId);

            }
        } else {
            fromPage = intent.getStringExtra("fromPage");
            String titleColor = intent.getStringExtra(TheLConstants.BUNDLE_KEY_TOPIC_COLOR);
            if (!TextUtils.isEmpty(titleColor)) {
                rel_title.setBackgroundColor(Color.parseColor("#" + titleColor));
            }
            if (intent.getSerializableExtra(TheLConstants.BUNDLE_KEY_MOMENT_BEAN) != null) {// 从moments页面进入

                Object object = intent.getSerializableExtra(TheLConstants.BUNDLE_KEY_MOMENT_BEAN);
                needGlide = intent.getStringExtra(BundleConstants.FROME_WRITE_COMMENT);
                if (object instanceof VideoBean) {

                    VideoBean videoBean = (VideoBean) object;

                    momentsId = videoBean.id;

                    getMomentInfoV2(momentsId);

                } else if (object instanceof MomentsBean) {
                    momentBean = (MomentsBean) object;

                    momentsId = momentBean.momentsId;

                    initUi();
                    processBusiness();
                    setListener();
                }

            } else if (intent.getStringExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID) != null) {// 从消息提醒进入
                swipe_container.post(new Runnable() {
                    @Override
                    public void run() {
                        swipe_container.setRefreshing(true);
                    }
                });
                momentsId = intent.getStringExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID);
                needGlide = intent.getStringExtra(BundleConstants.FROME_WRITE_COMMENT);

                fromPageType = 1;
                getMomentInfoV2(momentsId);
            }
            if (intent.getBooleanExtra("showKeyboard", false)) {
                input.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        showKeyboard();
                    }
                }, 150);
            }
        }
        if (moment_operations != null && WriteMomentComment.equals(needGlide) && listView != null) {
            moment_operations.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);
        }
        registerReceiver();

        startTime = System.currentTimeMillis();
    }

    private SimpleDraweeView img_thumb;
    private ImageView share_to;
    private TextView moment_user_name;
    private TextView moment_release_time;
    private TextView txt_update_now;
    private AutoSplitTextView moment_content_text;
    private RelativeLayout moment_content_pic;
    private MultiImageViewGroup layout_multi_image;
    private SimpleDraweeView img_live;
    private LinearLayout lin_live_status;
    private TextView txt_livestatus;
    private TextView moment_opts_emoji_txt;
    private TextView moment_opts_comment_txt;
    private ImageView moment_opts_more;
    private LikeButtonView moment_opts_like;
    private ImageView moment_opts_comment;

    // mentioned
    private LinearLayout moment_mentioned;
    private TextView moment_mentioned_text;

    // 音乐
    private LinearLayout moment_content_music;
    private SimpleDraweeView moment_content_music_pic;
    private ImageView img_play;
    private TextView song_name;
    private TextView album_name;
    private TextView artist_name;

    private SimpleDraweeView moments_tag;

    // 根日志区域
    private LinearLayout lin_participate_theme;
    private SimpleDraweeView img;
    private TextView txt_theme_title;
    private TextView txt_theme_desc;
    private TextView txt_deleted;

    private long startTime = 0;

    private int playCount = 1;

    private void initUi() {
        wide_divider = momentView.findViewById(R.id.wide_divider);
        img_thumb = momentView.findViewById(R.id.img_thumb);
        share_to = momentView.findViewById(R.id.share_to_img);
        moment_user_name = momentView.findViewById(R.id.moment_user_name);
        moment_img_vip = momentView.findViewById(R.id.img_vip);
        moment_release_time = momentView.findViewById(R.id.moment_release_time);
        moment_content_text = momentView.findViewById(R.id.moment_content_text);
        txt_update_now = momentView.findViewById(R.id.txt_update_now);
        moment_content_pic = momentView.findViewById(R.id.moment_content_pic);
        layout_multi_image = momentView.findViewById(R.id.layout_multi_image);
        img_live = momentView.findViewById(R.id.img_live);
        ll_live_status = momentView.findViewById(R.id.ll_live_status);
        lin_live_status = momentView.findViewById(R.id.lin_live_status);
        txt_livestatus = momentView.findViewById(R.id.txt_livestatus);

        moment_content = momentView.findViewById(R.id.moment_content);

        rel_video = momentView.findViewById(R.id.rel_video);
        progressbar_video = momentView.findViewById(R.id.progressbar_video);
        img_cover = momentView.findViewById(R.id.img_cover);
        img_play_video = momentView.findViewById(R.id.img_play_video);
        txt_play_times = momentView.findViewById(R.id.txt_play_times);

        rel_user_card = momentView.findViewById(R.id.rel_user_card);
        img_user_card = momentView.findViewById(R.id.img_user_card);
        mask_user_card = momentView.findViewById(R.id.mask_user_card);
        img_user_card_avatar = momentView.findViewById(R.id.img_user_card_avatar);
        txt_user_card_nickname = momentView.findViewById(R.id.txt_user_card_nickname);
        txt_user_card_info = momentView.findViewById(R.id.txt_user_card_info);
        txt_user_card_intro = momentView.findViewById(R.id.txt_user_card_intro);

        moment_content_music = momentView.findViewById(R.id.moment_content_music);
        moment_content_music_pic = momentView.findViewById(R.id.moment_content_music_pic);
        img_play = momentView.findViewById(R.id.img_play);
        song_name = momentView.findViewById(R.id.song_name);
        album_name = momentView.findViewById(R.id.album_name);
        artist_name = momentView.findViewById(R.id.artist_name);

        moment_opts_emoji_txt = momentView.findViewById(R.id.moment_opts_emoji_txt);
        moment_opts_comment_txt = momentView.findViewById(R.id.moment_opts_comment_txt);
        moment_opts_more = momentView.findViewById(R.id.moment_opts_more);
        moment_opts_like = momentView.findViewById(R.id.moment_opts_like);
        moment_opts_like.setLikeAnimImage(momentView.findViewById(R.id.img_like_anim));
        moment_opts_comment = momentView.findViewById(R.id.moment_opts_comment);
        iv_feed_recommend = momentView.findViewById(R.id.iv_feed_recommend);
        moment_operations = momentView.findViewById(R.id.moment_operations);
        //设置布局管理器

        moment_mentioned = momentView.findViewById(R.id.moment_mentioned);
        moment_mentioned_text = momentView.findViewById(R.id.moment_mentioned_text);

        moments_tag = momentView.findViewById(R.id.moments_tag);
        lin_participate_theme = momentView.findViewById(R.id.lin_participate_theme);
        img = momentView.findViewById(R.id.img);
        txt_theme_title = momentView.findViewById(R.id.txt_theme_title);
        txt_theme_desc = momentView.findViewById(R.id.txt_theme_desc);
        txt_deleted = momentView.findViewById(R.id.txt_deleted);
        txt_follow = momentView.findViewById(R.id.txt_follow);//新增加关注按钮
        //推荐网页
        recommend_web_view = momentView.findViewById(R.id.recommend_web_view);
        live_user_liveview = momentView.findViewById(R.id.live_user_liveview);
        moment_parent_view = momentView.findViewById(R.id.moment_parent_view);

        layout_moment_theme = momentView.findViewById(R.id.layout_moment_theme);

        img_theme = momentView.findViewById(R.id.img_theme);

        select_theme = momentView.findViewById(R.id.select_theme);

        setViewSize();

        initAdapter();

        moment_opts_comment.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MomentUtils.isBlackOrBlock(momentBean.userId + "")) {
                    return;
                }
                showKeyboard();
                if (listView != null && commentList != null) {

                    listView.scrollToPosition(commentList.size());
                }

            }
        });

        moment_opts_emoji_txt.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (momentBean != null)
                    WinkCommentsActivity.startActivity(MomentCommentActivity.this, momentBean.momentsId);
            }
        });

        // 更多操作
        moment_opts_more.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                if (momentBean.momentsType.equals(MomentsBean.MOMENT_TYPE_AD)) {
                    MomentUtils.showAdControlDialog(MomentCommentActivity.this, momentBean);

                    DialogUtils.showSelectionDialogWide(MomentCommentActivity.this, 3 / 4f, new String[]{"不感兴趣", "取消"}, new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            switch (position) {
                                case 0:
                                    MomentUtils.hideAdMoment(momentBean);
                                    MomentCommentActivity.this.finish();
                                    break;
                                case 1:
                                    break;
                            }

                            DialogUtils.dismiss();

                        }
                    }, false, 0, null);
                } else {
                    final boolean isCollect = !BusinessUtils.isCollected(momentBean.momentsId);//是否被收藏,如果已经被收藏，则为true,要收藏，否则，要删除
                    String collectMsg = isCollect ? getString(R.string.collection) : getString(R.string.collection_cancel);//收藏/取消收藏 文案
                    if (momentBean.myself == MomentsBean.IS_MINE) {
                        String stick;
                        if (momentBean.userBoardTop == 0) {
                            if (UserUtils.getUserVipLevel() > 0) {
                                stick = getString(R.string.moments_stick_moment_vip);
                            } else {
                                stick = getString(R.string.moments_stick_moment);
                            }
                        } else {
                            if (UserUtils.getUserVipLevel() > 0) {
                                stick = getString(R.string.moments_unstick_moment_vip);
                            } else {
                                stick = getString(R.string.moments_unstick_moment);
                            }
                        }
                        String setPrivate;
                        if (momentBean.shareTo < MomentsBean.SHARE_TO_ONLY_ME) {
                            setPrivate = getString(R.string.moments_set_as_private);
                        } else {
                            setPrivate = getString(R.string.moments_set_as_public);
                        }
                        DialogUtil.getInstance().showSelectionDialog(MomentCommentActivity.this, new String[]{stick, setPrivate, getString(R.string.info_delete)}, new OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                DialogUtil.getInstance().closeDialog();
                                switch (position) {
                                    case 0:
                                        if (UserUtils.getUserVipLevel() > 0) {
                                            if (momentBean.userBoardTop == 0)
                                                stickMomentData();
                                            else
                                                unstickMomentData();
                                        } else {
                                            MobclickAgent.onEvent(TheLApp.getContext(), "check_top_moment");
                                            startActivity(new Intent(MomentCommentActivity.this, VipConfigActivity.class));
                                        }
                                        break;
                                    case 1:
                                        if (momentBean.shareTo < MomentsBean.SHARE_TO_ONLY_ME) {
                                            setMomentAsPrivate();
                                        } else {
                                            setMomentAsPublic();
                                        }
                                        break;
                                    case 2:
                                        deleteMoment();
                                        break;

                                    default:
                                        break;
                                }
                            }
                        }, true, 0, null);
                    } else {
                        if (!TextUtils.isEmpty(fromPage) && UserInfoActivity.class.getName().equals(fromPage)) {// 如果是从用户详情点击进入，则只能举报，不能屏蔽
                            DialogUtils.showSelectionDialogWide(MomentCommentActivity.this, 3 / 4f, new String[]{collectMsg, getString(R.string.post_moment_share_to), getString(R.string.share_poster), getString(R.string.info_report)}, new OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    DialogUtils.dismiss();
                                    switch (position) {
                                        case 0://2.17 收藏
                                            collect(isCollect, momentBean.momentsId);
                                            break;
                                        case 1:
                                            share(0);
                                            break;
                                        case 2:
                                            share(1);
                                            break;
                                        case 3:
                                            reportMoment();
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }, true, 1, null);
                        } else {
                            DialogUtils.showSelectionDialogWide(MomentCommentActivity.this, 3 / 4f, new String[]{getString(R.string.info_report), getString(R.string.my_block_user_moments_activity_block_moment), getString(R.string.my_block_user_moments_activity_block)}, new OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    DialogUtils.dismiss();
                                    switch (position) {
                                        case 0:
                                            reportMoment();
                                            break;
                                        case 1:
                                            blockThisMoment();
                                            break;
                                        case 2:
                                            blockHerMoments();
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }, true, 0, null);
                        }
                    }
                }
            }
        });
        // 点赞
        moment_opts_like.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                ViewUtils.preventViewMultipleClick(v, 1000);
                if (MomentUtils.isBlackOrBlock(momentBean.userId + "")) {
                    return;
                }

                if (momentBean.winkFlag == MomentsBean.HAS_NOT_WINKED) {
                    momentBean.winkFlag = MomentsBean.HAS_WINKED;
                    likeMoment();
                } else {
                    momentBean.winkFlag = MomentsBean.HAS_NOT_WINKED;
                    unwinkMoment(momentBean.momentsId);
                }
            }
        });

        iv_feed_recommend.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                recommendMoment();
            }
        });
    }

    private TextureView.SurfaceTextureListener mSurfaceTextureListener = new TextureView.SurfaceTextureListener() {

        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {

            L.d("player1", "-----onSurfaceTextureAvailable-----");

            if (surface != null) {

                mSurface = new Surface(surface);

                if (player != null) {
                    player.release();
                    player = null;
                }

                player = new KSYMediaPlayer.Builder(TheLApp.context).build();

                player.setOnPreparedListener(mOnPreparedListener);

                player.setOnCompletionListener(mOnCompletionListener);

                player.setOnErrorListener(mOnErrorListener);

                try {
                    player.setDataSource(momentBean.videoUrl);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                progressbar_video.setVisibility(VISIBLE);

                img_play_video.setVisibility(GONE);

                txt_play_times.setVisibility(View.GONE);

                player.setDecodeMode(KSYMediaPlayer.KSYDecodeMode.KSY_DECODE_MODE_SOFTWARE);

                player.prepareAsync();

                uploadVidePlayCount();
            }
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

            L.d("player1", "-----onSurfaceTextureSizeChanged-----");

        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {

            L.d("player1", "-----onSurfaceTextureDestroyed-----");

            if (mSurface != null) {
                mSurface.release();
                mSurface = null;
            }

            return false;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {

            L.d("player1", "-----onSurfaceTextureUpdated-----");

        }
    };

    private IMediaPlayer.OnPreparedListener mOnPreparedListener = new IMediaPlayer.OnPreparedListener() {
        @Override
        public void onPrepared(IMediaPlayer iMediaPlayer) {
            L.d("player1", " onPrepared ");
            playCount++;
            progressbar_video.setVisibility(GONE);
            player.setSurface(mSurface);
            player.setDecodeMode(KSYMediaPlayer.KSYDecodeMode.KSY_DECODE_MODE_SOFTWARE);
            player.setVideoScalingMode(KSYMediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
            player.start();
        }
    };

    private IMediaPlayer.OnCompletionListener mOnCompletionListener = new IMediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(IMediaPlayer iMediaPlayer) {
            L.d("player1", " onCompletion ");
            if (player != null) {
                VideoUtils.saveVideoReport(momentBean.momentsId, (int) player.getDuration(), 0);
                player.seekTo(0);
                player.start();
            }
        }
    };

    private IMediaPlayer.OnErrorListener mOnErrorListener = new IMediaPlayer.OnErrorListener() {
        @Override
        public boolean onError(IMediaPlayer iMediaPlayer, int i, int i1) {
            stopPlayback();
            return false;
        }
    };

    private void initAdapter() {
        listAdapter = new MomentReplyRecyclerViewAdapter(this, commentList, this, MomentReplyRecyclerViewAdapter.PAGE_MOMENT_COMMENT);
        listAdapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {

            @Override
            public void onItemClick(View view, int position) {
                toCommentPosition = position;
                toComment = commentList.get(position);
                reply(toComment.nickname);
            }
        });

        // 列表长按
        listAdapter.setOnRecyclerViewItemLongClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemLongClickListener() {
            @Override
            public boolean onItemLongClick(View view, final int p) {
                operateComment = commentList.get(p);
                if ((operateComment.userId + "").equals(ShareFileUtils.getString(ShareFileUtils.ID, ""))) {// 我自己的评论
                    DialogUtils.showSelectionDialogWide(MomentCommentActivity.this, 3 / 4f, new String[]{getString(R.string.info_reply), getString(R.string.info_copy), getString(R.string.info_delete)}, new OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            DialogUtils.dismiss();
                            switch (position) {
                                case 0:// 回复
                                    toCommentPosition = p;
                                    toComment = operateComment;
                                    reply(operateComment.nickname);
                                    break;
                                case 1:// 复制
                                    DeviceUtils.copyToClipboard(MomentCommentActivity.this, operateComment.commentText);
                                    break;
                                case 2:// 删除
                                    deleteComment();
                                    break;

                                default:
                                    break;
                            }
                        }
                    }, true, 1, null);
                } else {
                    if (ShareFileUtils.getString(ShareFileUtils.ID, "").equals(momentBean.userId + "")) {// 我自己的日志，可以删除别人的评论
                        DialogUtil.getInstance().showSelectionDialog(MomentCommentActivity.this, new String[]{getString(R.string.info_delete), getString(R.string.info_reply), getString(R.string.info_copy), getString(R.string.info_report)}, new OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                switch (position) {
                                    case 0:// 回复
                                        deleteComment();

                                        break;
                                    case 1:// 复制

                                        toCommentPosition = p;
                                        toComment = operateComment;
                                        reply(operateComment.nickname);
                                        break;
                                    case 2:// 举报
                                        DeviceUtils.copyToClipboard(MomentCommentActivity.this, operateComment.commentText);

                                        break;
                                    case 3:// 删除
                                        report();

                                        break;

                                    default:
                                        break;
                                }
                                DialogUtil.getInstance().closeDialog();
                            }
                        }, true, 1, null);
                    } else {
                        DialogUtil.getInstance().showSelectionDialog(MomentCommentActivity.this, new String[]{getString(R.string.info_report), getString(R.string.info_reply), getString(R.string.info_copy)}, new OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                switch (position) {
                                    case 0:// 回复
                                        report();

                                        break;
                                    case 1:// 复制
                                        toCommentPosition = p;
                                        toComment = operateComment;
                                        reply(operateComment.nickname);
                                        break;
                                    case 2:// 举报
                                        DeviceUtils.copyToClipboard(MomentCommentActivity.this, operateComment.commentText);

                                        break;

                                    default:
                                        break;
                                }
                                DialogUtil.getInstance().closeDialog();
                            }
                        }, true, 1, null);
                    }
                }

                return false;
            }

        });
        listAdapter.setOnLoadMoreListener(this);
        listAdapter.setReloadMoreListener(this);
        listAdapter.addHeaderView(momentView);
        listView.setAdapter(listAdapter);
    }

    private Matcher topicMatcher = TheLConstants.TOPIC_PATTERN.matcher("");
    private Matcher urlMatcher = TheLConstants.URL_PATTERN.matcher("");

    @SuppressLint("StringFormatInvalid")
    private void refreshHeader() {

        L.d("MomentComment", " ------refreshHeader------");

        // 头像和昵称
        if (momentBean.secret == MomentsBean.IS_SECRET) {// 是否匿名
            img_thumb.setImageURI(Uri.parse(TheLConstants.RES_PIC_URL + R.mipmap.icon_user_anonymous));
            moment_user_name.setText(R.string.moments_msgs_notification_anonymous);
            img_thumb.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // 屏蔽匿名用户头像的点击
                }
            });
            moment_img_vip.setVisibility(GONE);
        } else {
            img_thumb.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(momentBean.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE)));
            if (MomentsBean.MOMENT_TYPE_USER_CARD.equals(momentBean.momentsType)) {
                GrowingIOUtil.track(MomentCommentActivity.this, "momentDetailPageView", "其他日志");
                moment_user_name.setText(buildThemeHeaderStr(momentBean.nickname, TheLApp.getContext().getString(R.string.recommend_a_user) + momentBean.momentsText));
            } else
                moment_user_name.setText(momentBean.nickname);
            img_thumb.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (momentBean != null) {
                        jumpToUserInfo(momentBean.userId + "");
                    }
                }
            });
            if (momentBean.level > 0) {

                moment_img_vip.setVisibility(VISIBLE);
                switch (momentBean.level) {
                    case 1:
                        moment_img_vip.setImageResource(R.mipmap.icn_vip_1);

                        break;
                    case 2:
                        moment_img_vip.setImageResource(R.mipmap.icn_vip_2);

                        break;
                    case 3:
                        moment_img_vip.setImageResource(R.mipmap.icn_vip_3);

                        break;
                    case 4:
                        moment_img_vip.setImageResource(R.mipmap.icn_vip_4);

                        break;
                }
            } else {
                moment_img_vip.setVisibility(GONE);

            }
        }
        // shareTo
        if (momentBean.shareTo == MomentsBean.SHARE_TO_FRIENDS) {
            share_to.setVisibility(VISIBLE);
            share_to.setImageResource(R.mipmap.icn_post_friends);
            iv_feed_recommend.setVisibility(View.VISIBLE);
        } else if (momentBean.shareTo == MomentsBean.SHARE_TO_ONLY_ME) {
            share_to.setVisibility(VISIBLE);
            share_to.setImageResource(R.mipmap.icn_post_privacy);
            iv_feed_recommend.setVisibility(View.GONE);
        } else {
            share_to.setVisibility(GONE);
            iv_feed_recommend.setVisibility(View.VISIBLE);
        }

        // 发布时间
        moment_release_time.setText(MomentUtils.getReleaseTimeShow(momentBean.momentsTime));

        // 推荐标签

        if (!TextUtils.isEmpty(momentBean.tag)) {
            if (momentBean.tag.equals(MomentsBean.TAG_TOP) && momentBean.topicFlag == 0) {
                moments_tag.setImageResource(R.mipmap.icn_feed_top);
                moments_tag.setVisibility(View.VISIBLE);
            } else if (momentBean.tag.equals(MomentsBean.TAG_RECOMMEND) && momentBean.topicFlag == 0) {
                moments_tag.setImageResource(R.mipmap.icn_feed_edited);
                moments_tag.setVisibility(View.VISIBLE);
            } else {
                moments_tag.setVisibility(GONE);
            }
        } else {
            moments_tag.setVisibility(GONE);
        }

        // 是否是用户个人置顶(显示优先级高于推荐标签)
        if (momentBean.userBoardTop == 1) {
            moments_tag.setImageResource(R.mipmap.icn_feed_top);
            moments_tag.setVisibility(VISIBLE);
        }
        /*******************日志内容*******************/
        moment_content_text.setVisibility(View.GONE);
        recommend_web_view.setVisibility(View.GONE);
        live_user_liveview.setVisibility(View.GONE);
        moment_parent_view.setVisibility(View.GONE);
        lin_participate_theme.setVisibility(View.GONE);
        rel_user_card.setVisibility(GONE);
        moment_content.setVisibility(GONE);
        layout_moment_theme.setVisibility(GONE);

        if (isNewType(momentBean.momentsType)) {
            String no_type_moment = TheLApp.context.getResources().getString(R.string.info_version_outdate);
            moment_content_text.setVisibility(VISIBLE);
            txt_update_now.setVisibility(VISIBLE);
            MomentUtils.setMomentContent(moment_content_text, no_type_moment);
        } else {
            if (MomentsBean.MOMENT_TYPE_LIVE_USER.equals(momentBean.momentsType)) {//4.0.0推荐主播
                GrowingIOUtil.track(MomentCommentActivity.this, "momentDetailPageView", "其他日志");

                /***注意，下面这一句只是临时解决问题的方法，后期建议另做处理***/
                final MomentsBean liveMoment = MomentUtils.getMomentByLiveMoment(momentBean.liveUserMoment, momentBean.imageUrl);
                live_user_liveview.initView(liveMoment);
            } else if (MomentsBean.MOMENT_TYPE_LIVE.equals(momentBean.momentsType) && !TextUtils.isEmpty(momentBean.liveId) || MomentsBean.MOMENT_TYPE_VOICE_LIVE.equals(momentBean.momentsType) && !TextUtils.isEmpty(momentBean.liveId)) {
                live_user_liveview.initView(momentBean);
            } else if (MomentsBean.MOMENT_TYPE_AD.equals(momentBean.momentsType)) {
                GrowingIOUtil.track(MomentCommentActivity.this, "momentDetailPageView", "广告日志");
                moment_content.setVisibility(View.VISIBLE);
                layout_moment_theme.setVisibility(View.VISIBLE);
                moment_content_text.setVisibility(View.VISIBLE);
                if (!isDestroyed()) {
                    ImageLoaderManager.imageLoader(img_theme, R.mipmap.bg_topic_default, momentBean.imageUrl);
                }
                moment_release_time.setBackgroundResource(R.drawable.shape_ad_text_bg);
                moment_release_time.setTextColor(TheLApp.context.getResources().getColor(R.color.white));
                moment_release_time.setTypeface(null, Typeface.BOLD);
                select_theme.setText("查看详情 >");
                select_theme.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Intent intent = new Intent(MomentCommentActivity.this, WebViewActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(BundleConstants.URL, momentBean.adUrl);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                });

                String adText = null;

                switch (momentBean.adType) {
                    case "official":
                        adText = TheLApp.context.getResources().getString(R.string.official);
                        break;
                    case "ad":
                        adText = TheLApp.context.getResources().getString(R.string.AD);
                        break;
                    default:
                        adText = "";
                        break;
                }

                moment_release_time.setText(adText);
                if (momentBean.momentsText != null) {
                    moment_content_text.setText(momentBean.momentsText);
                }

            } else {
                if (!TextUtils.isEmpty(momentBean.momentsText) && !MomentsBean.MOMENT_TYPE_USER_CARD.equals(momentBean.momentsType)) {// 推荐用户卡片类型的日志，momentText保存的是推荐理由，不显示在日志内容中
                    GrowingIOUtil.track(MomentCommentActivity.this, "momentDetailPageView", "其他日志");

                    /**4.0.0 由于网页日志的特殊性，所以要重新获取momentText**/
                    final String momentText = MomentUtils.getMomentTextFromMoment(momentBean);

                    moment_content_text.setVisibility(VISIBLE);
                    // 检查是否有输入话题，有的话把话题标红
                    topicMatcher.reset(momentText);
                    List<Integer> startIndexs = new ArrayList<Integer>();
                    List<Integer> endIndexs = new ArrayList<Integer>();
                    // 检查是否有输入网址，有的话把网址替换样式
                    urlMatcher.reset(momentText);
                    while (topicMatcher.find()) {
                        startIndexs.add(topicMatcher.start());
                        endIndexs.add(topicMatcher.start() + topicMatcher.group().length());
                    }
                    List<Integer> startIndexsUrl = new ArrayList<Integer>();
                    List<Integer> endIndexsUrl = new ArrayList<Integer>();
                    while (urlMatcher.find()) {
                        startIndexsUrl.add(urlMatcher.start());
                        endIndexsUrl.add(urlMatcher.start() + urlMatcher.group().length());
                    }
                    if (!startIndexs.isEmpty() || !startIndexsUrl.isEmpty()) {
                        // 创建一个 SpannableString对象
                        SpannableString sp = new SpannableString(momentText);
                        for (int i = 0; i < startIndexs.size(); i++) {
                            // 设置高亮样式
                            sp.setSpan(new TopicClickSpan(sp.subSequence(startIndexs.get(i), endIndexs.get(i))), startIndexs.get(i), endIndexs.get(i), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                        }
                        for (int i = 0; i < startIndexsUrl.size(); i++) {
                            final String url = momentText.substring(startIndexsUrl.get(i), endIndexsUrl.get(i));
                            sp.setSpan(new ImageSpan(TheLApp.getContext(), createUrlDrawble()), startIndexsUrl.get(i), endIndexsUrl.get(i), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                            sp.setSpan(new ClickableSpan() {
                                @Override
                                public void onClick(View view) {
                                    Intent intent = new Intent(TheLApp.getContext(), WebViewActivity.class);
                                    intent.putExtra(BundleConstants.URL, url);
                                    intent.putExtra("title", "");
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    TheLApp.getContext().startActivity(intent);
                                }
                            }, startIndexsUrl.get(i), endIndexsUrl.get(i), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        }
                        // SpannableString对象设置给TextView
                        moment_content_text.setText(sp);
                        moment_content_text.setMovementMethod(LinkMovementMethod.getInstance());
                    } else {
                        moment_content_text.setText(momentText);
                        GrowingIOUtil.track(MomentCommentActivity.this, "momentDetailPageView", "文本日志");

                        moment_content_text.setMovementMethod(LinkMovementMethod.getInstance());
                    }
                } else {
                    moment_content_text.setVisibility(GONE);
                }

                // 参与话题日志，要显示根话题日志区域
                final MomentParentBean parentMomentBean = momentBean.parentMoment;
                if (MomentsBean.MOMENT_TYPE_THEME_REPLY.equals(momentBean.momentsType)) {
                    GrowingIOUtil.track(MomentCommentActivity.this, "momentDetailPageView", "话题回复日志");

                    if (!parentMomentBean.momentsId.equals("0") && parentMomentBean.deleteFlag == 0) {
                        lin_participate_theme.setVisibility(VISIBLE);
                        txt_theme_title.setVisibility(VISIBLE);
                        txt_theme_desc.setVisibility(VISIBLE);
                        txt_deleted.setVisibility(GONE);
                        img.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(parentMomentBean.imgUrl, TheLConstants.ICON_MIDDLE_SIZE, TheLConstants.ICON_MIDDLE_SIZE))).build()).setAutoPlayAnimations(true).build());
                        txt_theme_title.setText(parentMomentBean.momentsText);
                        txt_theme_desc.setText(MomentUtils.buildDesc(parentMomentBean.joinTotal));
                        lin_participate_theme.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
//                                Intent intent = new Intent(MomentCommentActivity.this, ThemeDetailActivity.class);
//                                intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, parentMomentBean.momentsId + "");
//                                intent.putExtra(BundleConstants.THEMEPARTICIPATES, MomentUtils.buildDesc(parentMomentBean.joinTotal));
//                                startActivity(intent);
                                FlutterRouterConfig.Companion.gotoThemeDetails(parentMomentBean.momentsId);
                            }
                        });
                    } else if (parentMomentBean.deleteFlag == 1) {
                        lin_participate_theme.setVisibility(VISIBLE);
                        txt_theme_title.setVisibility(GONE);
                        txt_theme_desc.setVisibility(GONE);
                        txt_deleted.setVisibility(VISIBLE);
                        img.setImageURI(Uri.parse(TheLConstants.RES_PIC_URL + R.mipmap.icn_topic_delete));
                    } else {
                        lin_participate_theme.setVisibility(GONE);
                    }
                } else {
                    lin_participate_theme.setVisibility(GONE);
                }
                recommend_web_view.setVisibility(GONE);
                live_user_liveview.setVisibility(GONE);
                moment_parent_view.setVisibility(GONE);
                if (MomentsBean.MOMENT_TYPE_USER_CARD.equals(momentBean.momentsType)) {// 推荐用户卡片日志
                    GrowingIOUtil.track(MomentCommentActivity.this, "momentDetailPageView", "其他日志");

                    rel_user_card.setVisibility(VISIBLE);
                    moment_content.setVisibility(GONE);
                    img_user_card_avatar.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(momentBean.cardAvatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE)));
                    txt_user_card_nickname.setText(momentBean.cardNickName);
                    if (momentBean.cardIntro != null) {
                        txt_user_card_intro.setText(momentBean.cardIntro.replaceAll("\n", " "));
                    }
                    txt_user_card_info.setText(momentBean.buildUserCardInfoStr());
                    mask_user_card.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray_mask));
                    if (!TextUtils.isEmpty(momentBean.imageUrl)) {// 被推荐用户可能不带背景图，所以要显示默认背景
                        img_user_card.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(momentBean.imageUrl, TheLConstants.MOMENT_PIC_BIG_SIZE, TheLConstants.MOMENT_PIC_BIG_SIZE)));
                    } else {
                        img_user_card.setImageURI("");
                    }
                    rel_user_card.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            jumpToUserInfo(momentBean.cardUserId + "");
                        }
                    });
                } else if (MomentsBean.MOMENT_TYPE_WEB.equals(momentBean.momentsType)) { //4.0.0.-推荐网页
                    GrowingIOUtil.track(MomentCommentActivity.this, "momentDetailPageView", "其它日志");

                    recommend_web_view.initView(momentBean);

                } else if (MomentsBean.MOMENT_TYPE_RECOMMEND.equals(momentBean.momentsType)) {//4.0.0 推荐日志
                    GrowingIOUtil.track(MomentCommentActivity.this, "momentDetailPageView", "其它日志");

                    moment_parent_view.setVisibility(View.VISIBLE);
                    moment_parent_view.initView(momentBean);
                } else if (MomentsBean.MOMENT_TYPE_AD.equals(momentBean.momentsType)) {
                    GrowingIOUtil.track(MomentCommentActivity.this, "momentDetailPageView", "广告日志");

                } else {// 其他日志
                    moment_content.setVisibility(VISIBLE);
                    rel_user_card.setVisibility(GONE);
                    rel_video.setVisibility(GONE);// 先隐藏视频区域
                    img_live.setVisibility(GONE);
                    ll_live_status.setVisibility(GONE);
                    lin_live_status.setVisibility(GONE);

                    L.d("MomentCommentActivity", " momentBean.thumbnailUrl : " + momentBean.thumbnailUrl);

                    if (!TextUtils.isEmpty(momentBean.thumbnailUrl)) {// moment图片内容，包括视频\直播
                        GrowingIOUtil.track(MomentCommentActivity.this, "momentDetailPageView", "图片日志");

                        L.d("MomentCommentActivity", " momentBean.videoUrl : " + momentBean.videoUrl);

                        if (!TextUtils.isEmpty(momentBean.videoUrl)) {
                            moment_content_pic.setVisibility(GONE);
                            rel_video.setVisibility(VISIBLE);
                            img_cover.setVisibility(VISIBLE);
                            img_play_video.setVisibility(VISIBLE);
                            progressbar_video.setVisibility(GONE);
                            txt_play_times.setVisibility(View.VISIBLE);
                            String text;
                            if (momentBean.playTime > 0) {
                                text = Utils.formatTime(momentBean.playTime) + " / " + getString(R.string.play_count, momentBean.playCount);
                            } else {
                                text = getString(R.string.play_count, momentBean.playCount);
                            }
                            txt_play_times.setText(text);
                            img_cover.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(momentBean.thumbnailUrl, TheLConstants.MOMENT_PIC_BIG_SIZE, TheLConstants.MOMENT_PIC_BIG_SIZE)));
                            rel_video.setOnClickListener(this);

//                            if (VideoUtils.willAutoPlayVideo()) {
//
//                                startPlayback();
//                            }
                        } else {

                            layout_multi_image.initUI(momentBean.imageUrl, momentBean.userName, Utils.getImageShareBean(momentBean));
                            layout_multi_image.setVisibility(View.VISIBLE);
                            moment_content_pic.setVisibility(VISIBLE);
                        }
                    } else {
                        moment_content_pic.setVisibility(GONE);
                    }
                }
            }
            // mentioned
            if (momentBean.atUserList.size() > 0) {
                moment_mentioned_text.setText(MomentUtils.getMetionedShowString(momentBean.atUserList, momentBean.momentsId));
                moment_mentioned_text.setMovementMethod(LinkMovementMethod.getInstance());
                moment_mentioned.setVisibility(VISIBLE);
            } else {
                moment_mentioned.setVisibility(GONE);
            }

            updateLikeStatus();

            updateCommentNum();

            /**是否显示关注按钮*/
            if (momentBean.myself == 0 && momentBean.secret == MomentsBean.IS_NOT_SECRET) {
                txt_follow.setVisibility(VISIBLE);
                txt_follow.setFollowData(String.valueOf(momentBean.userId), momentBean.nickname, momentBean.avatar, momentBean.followerStatus);
            } else {
                txt_follow.setVisibility(GONE);
            }

        }
    }

    /**
     * 刷新点赞状态，包括点赞列表
     */

    private void updateLikeStatus() {
        // 是否已点过赞
        if (momentBean.winkFlag != MomentsBean.HAS_NOT_WINKED) {
            moment_opts_like.setChecked(true);
        } else {
            moment_opts_like.setChecked(false);
        }
        // 点赞数
        if (momentBean.winkNum <= 0) {
            moment_opts_emoji_txt.setVisibility(GONE);
        } else {
            moment_opts_emoji_txt.setVisibility(VISIBLE);
            moment_opts_emoji_txt.setText(TheLApp.getContext().getString(momentBean.winkNum == 1 ? R.string.like_count_odd : R.string.like_count_even, momentBean.winkNum));
        }
    }

    private void updateCommentNum() {
        if (momentBean.commentNum <= 0) {
            moment_opts_comment_txt.setVisibility(GONE);
        } else {
            moment_opts_comment_txt.setVisibility(VISIBLE);
            moment_opts_comment_txt.setText(TheLApp.getContext().getString(momentBean.commentNum <= 1 ? R.string.comment_count_odd : R.string.comment_count_even, momentBean.commentNum));
        }
    }

    private void likeMoment() {
        moment_opts_like.check();
        //取消点赞声音
        BusinessUtils.playSound(R.raw.sound_emoji);
        likeMoment(momentBean.momentsId);
    }

    private void jumpToUserInfo(String userId) {
        if (!TextUtils.isEmpty(userId)) {
//            Intent intent = new Intent();
//            intent.setClass(this, UserInfoActivity.class);
//            intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, userId + "");
//            startActivity(intent);
            FlutterRouterConfig.Companion.gotoUserInfo(userId);
        }
    }

    private void setViewSize() {
        int bigSize = (int) (AppInit.displayMetrics.widthPixels - TheLApp.getContext().getResources().getDimension(R.dimen.moment_list_margin) * 2);
        final RelativeLayout.LayoutParams bigSizeParams = new RelativeLayout.LayoutParams(bigSize, bigSize);
        bigSizeParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        img_cover.setLayoutParams(bigSizeParams);
        moment_content_music_pic.setLayoutParams(bigSizeParams);
        rel_user_card.setLayoutParams(bigSizeParams);
    }

    private void setVideoSize() {
        int bigSize = (int) (AppInit.displayMetrics.widthPixels - TheLApp.getContext().getResources().getDimension(R.dimen.moment_list_margin) * 2);
        final RelativeLayout.LayoutParams bigSizeParams = new RelativeLayout.LayoutParams(bigSize, bigSize);
        bigSizeParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        mTextureView.setLayoutParams(bigSizeParams);
    }

    private void findViewById() {
        rel_title = this.findViewById(R.id.rel_title);
        img_share = this.findViewById(R.id.img_share);
        img_share.setVisibility(VISIBLE);
        img_new = this.findViewById(R.id.img_new);
      /*  if (!SharedPrefUtils.getBoolean(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.NEW_COLLECT_MOMENT, false))
            img_new.setVisibility(VISIBLE);*/
        default_page = this.findViewById(R.id.default_page);
        ((ImageView) (default_page.findViewById(R.id.default_image))).setImageResource(R.mipmap.stay_filters_default);
        ((TextView) (default_page.findViewById(R.id.default_text))).setText(getString(R.string.moment_comments_error));
        momentView = (RelativeLayout) RelativeLayout.inflate(this, R.layout.moment_comment_header, null);
        lin_back = this.findViewById(R.id.lin_back);
        lin_back.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                ViewUtils.preventViewMultipleClick(arg0, 2000);

                close();

            }
        });
        title_txt = this.findViewById(R.id.title_txt);
        title_txt.setText(R.string.moment_comments_title);
        swipe_container = this.findViewById(R.id.swipe_container);
        ViewUtils.initSwipeRefreshLayout(swipe_container);
        listView = this.findViewById(R.id.recyclerview);
        //如果可以确定每个item的高度是固定的，设置这个选项可以提高性能
        listView.setHasFixedSize(true);
        listView.addItemDecoration(new DefaultItemDivider(TheLApp.getContext(), LinearLayoutManager.VERTICAL, R.color.gray, 1, false, false, false));
        listView.setLayoutManager(new LinearLayoutManager(TheLApp.getContext()));
        input = this.findViewById(R.id.input);
        input.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                View rootview = MomentCommentActivity.this.getWindow().getDecorView(); // this = activity
                rootview.getWindowVisibleDisplayFrame(r);

                int screenHeight = rootview.getHeight();
                int heightDifference = screenHeight - r.bottom;
                if (heightDifference > 300) {
                    int softHeight = ShareFileUtils.getInt(ShareFileUtils.SOFT_KEYBOARD_HEIGHT, 0);
                    if (softHeight != heightDifference) {// 更新软键盘的高度
                        softHeight = heightDifference;
                        ShareFileUtils.setInt(ShareFileUtils.SOFT_KEYBOARD_HEIGHT, softHeight);
                    }
                    if (softHeight >= defaultSoftInputHeight) {
                        rel_more_bottom.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, softHeight));
                    } else {
                        rel_more_bottom.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, defaultSoftInputHeight));
                    }
                    // 调整表情键盘的上间距
                    sendEmojiLayout.refreshPaddingTop();

                    hideBottomMore();
                }

            }
        });
        textLength = this.findViewById(R.id.write_comment_text_length);
        submit = this.findViewById(R.id.send);

        rel_more_bottom = this.findViewById(R.id.rel_more_bottom);
        img_emoji = this.findViewById(R.id.img_emoji);

        sendEmojiLayout = this.findViewById(R.id.sendEmojiLayout);

        int softHeight = ShareFileUtils.getInt(ShareFileUtils.SOFT_KEYBOARD_HEIGHT, 0);
        if (softHeight >= defaultSoftInputHeight) {
            rel_more_bottom.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, softHeight));
        } else {
            rel_more_bottom.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, defaultSoftInputHeight));
        }

        lin_update_user_info = findViewById(R.id.lin_update_user_info);
        lin_update_user_info.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                Intent intent = new Intent(MomentCommentActivity.this, UpdateUserInfoActivity.class);
                Bundle bundle = new Bundle();
                MyInfoBean userBean = ShareFileUtils.getUserData();
                bundle.putSerializable("my_info", userBean);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    private void hideBottomMore() {
        if (rel_more_bottom.getVisibility() == VISIBLE) {
            rel_more_bottom.setVisibility(GONE);
        }
    }

    public static boolean needRefreshStickers = false;

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(this.getClass().getName());
        MobclickAgent.onResume(this);

        if (needRefreshStickers && sendEmojiLayout != null) {
            needRefreshStickers = false;
            sendEmojiLayout.refreshUI();
        }

        if (player != null) {
            player.start();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (player != null) {
            player.pause();
        }
        MobclickAgent.onPageEnd(this.getClass().getName());
        MobclickAgent.onPause(this);
        stopPlayback();
    }

    protected void processBusiness() {
        swipe_container.post(new Runnable() {
            @Override
            public void run() {
                swipe_container.setRefreshing(true);
            }
        });
        loadAllData();
    }

    protected void setListener() {

        listView.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                dismissKeyboard();
                hideBottomMore();
                return false;
            }
        });

        moment_content_text.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                DialogUtil.getInstance().showSelectionDialog(MomentCommentActivity.this, new String[]{getString(R.string.info_copy)}, new OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        DialogUtil.getInstance().closeDialog();
                        switch (position) {
                            case 0:// 复制
                                DeviceUtils.copyToClipboard(MomentCommentActivity.this, MomentUtils.getMomentTextFromMoment(momentBean) + "");
                                break;

                            default:
                                break;
                        }
                    }
                }, false, 2, null);
                return false;
            }
        });

        //4.0.0,推荐日志（抓饭日志）
        img_share.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewUtils.preventViewMultipleClick(view, 1000);
                final boolean isCollect = !BusinessUtils.isCollected(momentBean.momentsId);//是否被收藏,如果已经被收藏，则为true,要收藏，否则，要删除
                collect(isCollect, momentBean.momentsId);
            }
        });

        momentView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                // 点击头部，即moment详情，则将输入置为发表say类型的评论
                resetInput();
            }
        });

        // 发布评论
        submit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isBindCell()) {
                    if (MomentUtils.isBlackOrBlock(momentBean.userId + "")) {
                        return;
                    }
                    ViewUtils.preventViewMultipleClick(v, 2000);
                    BusinessUtils.playSound(R.raw.sound_send);
                    submitComment();
                }
            }
        });

        img_emoji.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissKeyboard();
                mHandler.postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        if (rel_more_bottom.getVisibility() == VISIBLE) {
                            if (sendEmojiLayout.getVisibility() == VISIBLE) {// 底部区域已弹出，则调出键盘
                                showKeyboard();
                            } else {
                                sendEmojiLayout.setVisibility(VISIBLE);
                            }
                        } else {
                            rel_more_bottom.setVisibility(VISIBLE);
                            sendEmojiLayout.setVisibility(VISIBLE);
                        }
                    }
                }, 100);
            }
        });

        title_txt.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (MotionEvent.ACTION_DOWN == event.getAction()) {
                    if (System.currentTimeMillis() - lastClickTime < 300) {
                        ViewUtils.preventViewMultipleTouch(v, 2000);
                        if (listView != null) {
                            listView.scrollToPosition(0);
                        }
                    }
                    lastClickTime = System.currentTimeMillis();
                }
                return true;
            }
        });

        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                loadAllData();
            }
        });

        textWatcher = new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                textLength.setText((500 - s.length()) + "");

                if (s == null) {
                    submit.setTextColor(ContextCompat.getColor(MomentCommentActivity.this, R.color.moment_chat_cancel_text));
                    submit.setEnabled(false);
                    return;
                }

                int textLength = s.length();

                if (textLength > 0) {
                    submit.setTextColor(ContextCompat.getColor(MomentCommentActivity.this, R.color.text_color_green));
                    submit.setEnabled(true);
                } else {
                    submit.setTextColor(ContextCompat.getColor(MomentCommentActivity.this, R.color.moment_chat_cancel_text));
                    submit.setEnabled(false);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };

        input.addTextChangedListener(textWatcher);

    }

    /**
     * 没有绑定手机号码不能发布日志，直接跳到绑定手机页面
     */
    private boolean isBindCell() {
        String cell = ShareFileUtils.getString(ShareFileUtils.BIND_CELL, "");
        if (TextUtils.isEmpty(cell) || "null".equals(cell)) {
            //打开注册页面
            LoginRegisterActivity.startActivity(TheLApp.getContext(), LoginRegisterActivity.BUNDLE,false);//绑定手机
            return false;
        }
        return true;
    }

    /**
     * 4.0.0，推荐日志
     */
    private void recommendMoment() {
        final String userId = momentBean.userId + "";
        if (MomentUtils.isBlackOrBlock(userId)) {
            return;
        }
        SharedPrefUtils.setBoolean(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.NEW_SHARE_MOMENT, true);
        img_new.setVisibility(View.INVISIBLE);
        if (momentBean == null) {
            return;
        }
        final Intent intent = new Intent(this, SendCardActivity.class);
        intent.putExtra(TheLConstants.BUNDLE_KEY_INTENT_FROM, SendCardActivity.FROM_RECOMMEND_MOMENT);
        intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_BEAN, momentBean);
        intent.putExtra(TheLConstants.BUNDLE_KEY_PAGE_FROM, GrowingIoConstant.ENTRY_MOMENT_DETAIL_PAGE);
        startActivity(intent);
    }

    /**
     * 关闭键盘
     */
    private void dismissKeyboard() {
        ViewUtils.hideSoftInput(this, input);
    }

    /**
     * 开启键盘
     */
    private void showKeyboard() {
        ViewUtils.showSoftInput(this, input);
        hideBottomMore();
    }

    private void loadCommentsData(int cursor) {
        getCommentList(momentBean.momentsId, String.valueOf(limit), String.valueOf(cursor));
    }

    private void loadAllData() {

        getMomentInfoV2(momentBean.momentsId);
    }

    private CommentBean lastReleasedComment;
    private List<CommentBean> releasedComments = new ArrayList<CommentBean>();

    private void afterReleaseComment(ReleasedCommentBeanV2 releasedComment) {
        // 增加评论数
        momentBean.commentNum += 1;
        updateCommentNum();
        lastReleasedComment = new CommentBean();
        lastReleasedComment.momentsId = releasedComment.momentsId;
        lastReleasedComment.commentId = releasedComment.id;
        lastReleasedComment.userId = Integer.valueOf(ShareFileUtils.getString(ShareFileUtils.ID, ""));
        lastReleasedComment.nickname = ShareFileUtils.getString(ShareFileUtils.USER_NAME, "");
        lastReleasedComment.avatar = ShareFileUtils.getString(ShareFileUtils.AVATAR, "");
        lastReleasedComment.commentTime = "0_0";
        lastReleasedComment.commentText = myComment;
        lastReleasedComment.commentType = commentType;
        if (toComment != null) {
            lastReleasedComment.toUserId = toComment.userId;
            lastReleasedComment.toNickname = toComment.nickname;
            lastReleasedComment.toAvatar = toComment.toAvatar;
            lastReleasedComment.sayType = CommentBean.SAY_TYPE_TO;
        } else {
            lastReleasedComment.sayType = CommentBean.SAY_TYPE_SAY;
        }
        commentList.add(lastReleasedComment);
        releasedComments.add(lastReleasedComment);
        listAdapter.notifyDataSetChanged();
        listView.scrollToPosition(listAdapter.getItemCount() - 1);
        wide_divider.setVisibility(VISIBLE);
    }

    /**
     * 发布一条评论回复后，刷新这条评论的数据，增加一条最近回复
     *
     * @param releasedComment
     */
    private void afterReleaseCommentReply(ReleasedCommentReplyBean releasedComment) {
        L.d("MomentComment", "------------afterReleaseCommentReply------------" + GsonUtils.createJsonString(releasedComment));
        final CommentBean lastReleasedCommentReply = new CommentBean();
        lastReleasedCommentReply.momentsId = releasedComment.momentId + "";
        lastReleasedCommentReply.commentId = releasedComment.commentId + "";
        lastReleasedCommentReply.userId = Integer.valueOf(ShareFileUtils.getString(ShareFileUtils.ID, ""));
        lastReleasedCommentReply.nickname = ShareFileUtils.getString(ShareFileUtils.USER_NAME, "");
        lastReleasedCommentReply.avatar = ShareFileUtils.getString(ShareFileUtils.AVATAR, "");
        lastReleasedCommentReply.commentTime = "0_0";
        lastReleasedCommentReply.commentText = myComment;
        lastReleasedCommentReply.commentType = commentType;
        lastReleasedCommentReply.sayType = CommentBean.SAY_TYPE_SAY;
        if (toCommentPosition >= 0 && toCommentPosition < commentList.size()) {
            CommentBean comment = commentList.get(toCommentPosition);


            comment.commentNum += 1;

            if (comment.commentList == null) {
                comment.commentList = new ArrayList<>();
                comment.commentList.add(lastReleasedCommentReply);
            } else {
                comment.commentList.add(0, lastReleasedCommentReply);
            }

            if (comment.commentList.size() > 3) {
                comment.commentList = comment.commentList.subList(0, 3);
            }

            listAdapter.notifyItemChanged(toCommentPosition + listAdapter.getHeaderLayoutCount());

        }
    }

    private void submitComment() {

        myComment = input.getText().toString().replace("\n", " ").trim();

        commentType = CommentBean.COMMENT_TYPE_TEXT;

        if (TextUtils.isEmpty(myComment)) {
            DialogUtil.showToastShort(this, getString(R.string.moment_comments_empty_tip));
            return;
        }
        showLoadingNoBack();
        releaseComment(CommentBean.COMMENT_TYPE_TEXT);
    }

    private void releaseComment(String type) {

        Map<String, String> map = new HashMap<>();
        map.put(RequestConstants.I_TEXT, myComment);
        map.put(RequestConstants.I_TYPE, type);
        map.put(RequestConstants.I_TO_USER_ID, "");

        Intent intentCast = new Intent();
        intentCast.setAction(TheLConstants.BROADCAST_AUTO_RELEASE_NEW_COMMENT_ACTION);
        intentCast.putExtra(RequestConstants.I_TEXT, myComment);
        intentCast.putExtra(RequestConstants.I_TYPE, CommentBean.COMMENT_TYPE_TEXT);
        sendBroadcast(intentCast);

        if (toComment == null) {// say类型的评论

            map.put(RequestConstants.I_MOMENT_ID, momentBean.momentsId);

            Flowable<ReleasedCommentBeanV2New> flowable = DefaultRequestService.createMomentRequestService().replyMoment(MD5Utils.generateSignatureForMap(map));
            flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<ReleasedCommentBeanV2New>() {
                @Override
                public void onNext(ReleasedCommentBeanV2New data) {
                    super.onNext(data);

                    needRefresh = true;
                    // 添加评论后需要刷新moments列表
                    // 清空输入框
                    input.setText("");
                    afterReleaseComment(data.data);
                    resetInput();
                    requestFinished();

                }

                @Override
                public void onError(Throwable t) {
                    super.onError(t);
                    requestFinished();
                }
            });

        } else {// to类型的评论

            map.put(RequestConstants.I_COMMENT_ID, toComment.commentId);

            Flowable<ReleasedCommentReplyBeanNew> flowable = DefaultRequestService.createAllRequestService().replyComment(MD5Utils.generateSignatureForMap(map));
            flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<ReleasedCommentReplyBeanNew>() {
                @Override
                public void onNext(ReleasedCommentReplyBeanNew data) {
                    super.onNext(data);
                    input.setText("");
                    DialogUtil.showToastShort(MomentCommentActivity.this, getString(R.string.post_moment_posted));
                    afterReleaseCommentReply(data.data);
                    resetInput();
                    requestFinished();
                }

                @Override
                public void onError(Throwable t) {
                    super.onError(t);
                    requestFinished();
                }
            });

        }
    }

    private void resetInput() {
        input.setHint(getString(R.string.moments_comment_input_hint));
        toComment = null;
        toCommentPosition = -1;
    }

    private void reply(String nickname) {
        input.setHint(getString(R.string.moments_comment_input_reply) + " " + nickname);
        ViewUtils.showSoftInput(this, input);
    }


    private void reportMoment() {
        Intent intent = new Intent(this, ReportActivity.class);
        intent.putExtra(TheLConstants.BUNDLE_KEY_REPORT_TYPE, ReportActivity.REPORT_TYPE_ABUSE);
        intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, momentsId);
        startActivityForResult(intent, TheLConstants.BUNDLE_CODE_MOMENTS_ACTIVITY);
    }

    private void report() {
        DialogUtil.showConfirmDialog(this, null, getString(R.string.moments_report_comment_confirm), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (operateComment != null && !TextUtils.isEmpty(operateComment.commentId)) {
                    showLoading();

                    Map<String, String> map = new HashMap<>();
                    map.put(RequestConstants.I_ID, operateComment.commentId);

                    Flowable<BaseDataBean> flowable = DefaultRequestService.createAllRequestService().reportComment(MD5Utils.generateSignatureForMap(map));

                    flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
                        @Override
                        public void onNext(BaseDataBean data) {
                            super.onNext(data);
                            requestFinished();
                            DialogUtil.showToastShort(MomentCommentActivity.this, getString(R.string.userinfo_activity_report_success));
                        }
                    });

                }
            }
        });
    }

    /**
     * 屏蔽这个用户的所有日志
     */
    private void blockHerMoments() {
        DialogUtil.showConfirmDialog(this, null, getString(R.string.my_block_user_moments_activity_block_confirm), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

                MomentBlackUtils.blackHerMoment(momentBean.userId + "");

            }
        });
    }

    /**
     * 屏蔽这条日志
     */
    private void blockThisMoment() {
        Intent intent = new Intent(this, ReportActivity.class);
        intent.putExtra(TheLConstants.BUNDLE_KEY_REPORT_TYPE, ReportActivity.REPORT_TYPE_HIDE_MOMENT);
        intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, momentsId);
        startActivityForResult(intent, TheLConstants.BUNDLE_CODE_MOMENTS_ACTIVITY);
    }

    public void refreshComments(CommentListBeanV2 result) {
        if (cursor == 0)
            commentList.clear();
        haveNextPage = result.haveNextPage;
        commentList.addAll(result.commentList);
        if (cursor == 0) {
            listAdapter.removeAllFooterView();
            listAdapter.openLoadMore(commentList.size(), true);
            listAdapter.setNewData(commentList);
        } else {
            listAdapter.notifyDataChangedAfterLoadMore(true, commentList.size());
        }
        cursor = result.cursor;
        if (WriteMomentComment.equals(needGlide) && listView != null) {
            // moment_operations.setDescendantFocusability(ViewGroup.FOCUS_AFTER_DESCENDANTS);
            listView.scrollToPosition(commentList.size());
        }
        if (commentList.isEmpty())
            wide_divider.setVisibility(GONE);
        else
            wide_divider.setVisibility(VISIBLE);
    }

    public void refreshAll(MomentsBean result) {
        // 刷新头部moment详情
        momentBean = null;
        momentBean = result;
        //是否被收藏,如果已经被收藏，则为true,要收藏，否则，要删除
        boolean isCollect = BusinessUtils.isCollected(result.momentsId);

        if (isCollect) {
            img_share.setImageResource(R.mipmap.icn_collection_press);

        } else {
            img_share.setImageResource(R.mipmap.icn_collection_normal);

        }
        if (fromPageType == 1 && !hasInitedUi) {// 如果是从消息提醒页面进入，则需要初始化控件
            initUi();
            setListener();
            hasInitedUi = true;
        }
        refreshHeader();
    }

    private void saveHistorySticker(StickerBean stickerBean) {
        try {
            JSONObject object = stickerBean.toJSON();
            JSONArray jsonArray = new JSONArray(SharedPrefUtils.getString(SharedPrefUtils.FILE_STICKERS, SharedPrefUtils.FILE_STICKERS, "[]"));
            for (int i = 0; i < jsonArray.length(); i++) {
                if (jsonArray.getJSONObject(i).getLong("id") == object.getLong("id")) {
                    JsonUtils.removeIndexOfJSONArray(jsonArray, i);
                    break;
                }
            }
            JSONArray tempArr = new JSONArray();
            tempArr.put(0, object);
            for (int i = 0; i < jsonArray.length(); i++) {
                tempArr.put(i + 1, jsonArray.get(i));
                if (i == 6) {// 最多8个历史记录（1页）
                    break;
                }
            }
            SharedPrefUtils.setString(SharedPrefUtils.FILE_STICKERS, SharedPrefUtils.FILE_STICKERS, tempArr.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void requestFinished() {
        mHandler.postDelayed(new Runnable() {

            @Override
            public void run() {
                if (listView != null) {
                    if (swipe_container != null && swipe_container.isRefreshing())
                        swipe_container.setRefreshing(false);
                }
            }
        }, 1000);
        closeLoading();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);

        callbackManager.onActivityResult(requestCode, resultCode, data);

        if (resultCode == TheLConstants.RESULT_CODE_BLOCK_THIS_MOMENT) {
            Intent intent = new Intent();
            intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, momentsId);
            setResult(TheLConstants.RESULT_CODE_BLOCK_THIS_MOMENT, intent);
            finish();
        } else if (resultCode == TheLConstants.RESULT_CODE_REPORT_MOMENT_SUCCEED) {
            Intent intent = new Intent();
            intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, momentsId);
            setResult(TheLConstants.RESULT_CODE_REPORT_MOMENT_SUCCEED, intent);
            finish();
        } else if (resultCode == TheLConstants.RESULT_CODE_VIDEO_PLAY_COUNT) {
            final int playCount = data.getIntExtra(TheLConstants.BUNDLE_KEY_PLAY_COUNT, momentBean.playCount);
            momentBean.playCount = playCount;
            String text;
            if (momentBean.playTime > 0) {
                text = Utils.formatTime(momentBean.playTime) + " / " + getString(R.string.play_count, momentBean.playCount);
            } else {
                text = getString(R.string.play_count, momentBean.playCount);
            }
            txt_play_times.setText(text);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (getImageViewer() != null) {
            boolean b = getImageViewer().onKeyDown(keyCode, event);
            if (b) {
                return b;
            }
        }

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (rel_more_bottom != null) {
                if (rel_more_bottom.getVisibility() == VISIBLE) {
                    hideBottomMore();
                } else {
                    close();
                }
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void close() {
        if (needRefresh) {
            EventBus.getDefault().postSticky(new UpdateMomentEvent(momentBean));
        }
        if (!TextUtils.isEmpty(input.getText().toString().trim())) {
            DialogUtil.showConfirmDialog(MomentCommentActivity.this, null, getString(R.string.moments_release_moment_discard_tip), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    finish();
                }
            });
        } else {
            PushUtils.finish(MomentCommentActivity.this);
        }
    }

    @Override
    public void finish() {
        setResult(Activity.RESULT_OK);
        super.finish();
    }

    @Override
    public void onItemClick(View view) {
        showLoadingNoBack();
        StickerBean stickerBean = (StickerBean) view.findViewById(R.id.img).getTag();
        saveHistorySticker(stickerBean);
        myComment = stickerBean.buildStickerMsgText();
        commentType = CommentBean.COMMENT_TYPE_STICKER;

        if (isBindCell()) {
            releaseComment(commentType);
        }

    }

    /**
     * 静态表情点击事件
     *
     * @param parent
     * @param view
     * @param position
     * @param id
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        StickerBean emoji = (StickerBean) view.findViewById(R.id.img).getTag();
        if (emoji.isDeleteBtn) {
            int selection = input.getSelectionStart();
            String text = input.getText().toString();
            if (selection > 0) {
                String text2 = text.substring(0, selection);
                if (text2.endsWith("]")) {
                    int start = text2.lastIndexOf("[");
                    int end = selection;
                    input.getText().delete(start, end);
                    return;
                }
                input.getText().delete(selection - 1, selection);
            }
        }
        if (!TextUtils.isEmpty(emoji.label)) {
            SpannableString spannableString = EmojiUtils.getInstace(EmojiUtils.DEFAULT_SIZE).addEmoji(this, (int) emoji.id, emoji.label);
            int index = input.getSelectionStart();//获取光标所在位置
            Editable editable = input.getEditableText();//获取EditText的文字
            if (index < 0 || index >= input.length()) {
                editable.append(spannableString);
            } else {
                editable.insert(index, spannableString);//光标所在位置插入文字
            }
        }
    }

    @Override
    public void reloadMore() {
        listAdapter.removeAllFooterView();
        listAdapter.openLoadMore(true);
        loadCommentsData(cursor);
    }

    @Override
    public void onLoadMoreRequested() {
        listView.post(new Runnable() {
            @Override
            public void run() {
                if (haveNextPage) {
                    // 列表滑动到底部加载下一组数据
                    loadCommentsData(cursor);
                } else {
                    listAdapter.openLoadMore(commentList.size(), false);
                    View view = getLayoutInflater().inflate(R.layout.load_more_footer_layout, (ViewGroup) listView.getParent(), false);
                    ((TextView) view.findViewById(R.id.text)).setText(TheLApp.getContext().getString(R.string.info_no_more));
                    listAdapter.addFooterView(view);
                }
            }
        });
    }

    @Override
    public void onClick(final View v) {
        int viewId = v.getId();
        switch (viewId) {
            case R.id.img_thumb:
                ViewUtils.preventViewMultipleClick(v, 1000);
                jumpToUserInfo(String.valueOf(v.getTag()));
                break;
            case R.id.moment_content_music:

                break;
            case R.id.rel_video:
                ViewUtils.preventViewMultipleClick(v, 1000);
                gotoVideoPlayAct();
                break;
            default:
                break;
        }
    }

    private void gotoVideoPlayAct() {
        final VideoBean videoBean = Utils.getVideoFromMoment(momentBean);
        if (videoBean != null) {
            final int userId = momentBean.userId;
            Bundle bundle = new Bundle();
            bundle.putSerializable(TheLConstants.BUNDLE_KEY_VIDEO_BEAN, videoBean);
            bundle.putString(TheLConstants.BUNDLE_KEY_USER_ID, userId + "");
            Intent intent = new Intent(MomentCommentActivity.this, UserVideoListActivity.class);
            intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_BEAN, momentBean);
            intent.putExtras(bundle);
            startActivityForResult(intent, TheLConstants.RESULT_CODE_VIDEO_PLAY_COUNT);

            HashMap<String, String> data = new HashMap<>();
            data.put(RequestConstants.I_ID, momentBean.momentsId);
            data.put(RequestConstants.VIDEO_PLAY_COUNT, "1");
            Flowable<BaseDataBean> flowable = DefaultRequestService.createAllRequestService().postVideoPlayCountUpload(MD5Utils.generateSignatureForMap(data));
            flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
                @Override
                public void onNext(BaseDataBean data) {
                    super.onNext(data);
                }
            });
        }
    }

    private Bitmap createUrlDrawble() {
        View view = LayoutInflater.from(TheLApp.getContext()).inflate(R.layout.layout_url_view, null);
        view.setDrawingCacheEnabled(true);
        view.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        return view.getDrawingCache();
    }

    @Override
    protected void onDestroy() {
        try {
            if (textWatcher != null && input != null) {
                input.removeTextChangedListener(textWatcher);
            }
            unregisterReceiver(dataChangedReceiver);
            momentBlackUtils.unRegisterReceiver(this);
            stickerUtils.unRegisterReceiver(this);
            mMomentLikeUtils.unRegisterReceiver(this);
            long endTime = System.currentTimeMillis();
            int watchTime;
            if (startTime > 0 && endTime - startTime > 0) {
                watchTime = (int) ((endTime - startTime) / 1000);
            } else {
                watchTime = 1;
            }

            GrowingIOUtil.uploadCommonVideoDuration(watchTime);

            if (momentBean != null) {

                boolean isVideoMoment = momentBean.momentsType.equals(MomentsBean.MOMENT_TYPE_VIDEO);

                boolean isThemeVideoMoment = (momentBean.momentsType.equals(MomentTypeConstants.MOMENT_TYPE_THEME_REPLY) && momentBean.themeReplyClass != null && "video".equals(momentBean.themeReplyClass));

                if (isVideoMoment || isThemeVideoMoment) {
                    VideoUtils.postPlayVideoInfo(TheLConstants.EntryConstants.ENTRY_MOMENTDETAIL, momentsId, playCount, watchTime);
                }
            }

            dismissKeyboard();
            if (player != null) {
                player.release();
                player = null;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        super.onDestroy();
    }

    private void startPlayback() {

        L.d("player1", "-----startPlayback-----");

        if (mTextureView != null) {

            rel_video.removeView(mTextureView);

        }

        mTextureView = new SquareTextureView(MomentCommentActivity.this);

        setVideoSize();

        mTextureView.setSurfaceTextureListener(mSurfaceTextureListener);

        rel_video.addView(mTextureView);

    }


    private void stopPlayback() {

        L.d("player1", "-----stopPlayback-----");

        if (player != null) {
            try {
                player.seekTo(0);
                if (player.isPlaying())
                    player.stop();
            } catch (Exception e) {
                e.printStackTrace();
            }
            progressbar_video.setVisibility(GONE);
            txt_play_times.setVisibility(View.VISIBLE);
            img_play_video.setVisibility(VISIBLE);
        }
    }

    /**
     * 置顶moment数据
     */
    private void stickMomentData() {
        DialogUtil.showConfirmDialog(this, null, getString(R.string.moments_stick_moment_confirm), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (!TextUtils.isEmpty(momentBean.momentsId)) {
                    showLoadingNoBack();
                    MomentUtils.stickMomentData(MomentCommentActivity.this, momentBean);
                    momentBean.userBoardTop = 1;
                    refreshHeader();
                    requestFinished();

                }
            }
        });

    }

    /**
     * 取消置顶moment数据
     */
    private void unstickMomentData() {
        DialogUtil.showConfirmDialog(this, null, getString(R.string.moments_unstick_moment_confirm), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (!TextUtils.isEmpty(momentBean.momentsId)) {
                    showLoadingNoBack();
                    MomentUtils.unstickMomentData(MomentCommentActivity.this, momentBean);
                    momentBean.userBoardTop = 0;
                    refreshHeader();
                    requestFinished();
                }
            }
        });
    }

    /**
     * 将日志设为仅自己可见
     */
    private void setMomentAsPrivate() {
        DialogUtil.showConfirmDialog(this, null, getString(R.string.moments_set_private_moment_confirm), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (!TextUtils.isEmpty(momentBean.momentsId)) {
                    showLoadingNoBack();

                    Map<String, String> map = new HashMap<>();
                    map.put(RequestConstants.I_MOMENTS_ID, momentBean.momentsId);

                    Flowable<BaseDataBean> flowable = DefaultRequestService.createMomentRequestService().setMyMomentAsPrivate(MD5Utils.generateSignatureForMap(map));

                    flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
                        @Override
                        public void onNext(BaseDataBean data) {
                            super.onNext(data);
                            DialogUtil.showToastShort(MomentCommentActivity.this, getString(R.string.moments_set_as_private_succeed));
                            momentBean.shareTo = MomentsBean.SHARE_TO_ONLY_ME;
                            refreshHeader();
                            requestFinished();
                        }
                    });

                }
            }
        });
    }

    /**
     * 将日志设为公开
     */
    private void setMomentAsPublic() {
        DialogUtil.showConfirmDialog(this, null, getString(R.string.moments_set_public_moment_confirm), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (!TextUtils.isEmpty(momentBean.momentsId)) {
                    showLoadingNoBack();

                    Map<String, String> map = new HashMap<>();
                    map.put(RequestConstants.I_MOMENTS_ID, momentBean.momentsId);

                    Flowable<BaseDataBean> flowable = DefaultRequestService.createMomentRequestService().setMyMomentAsPublic(MD5Utils.generateSignatureForMap(map));

                    flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
                        @Override
                        public void onNext(BaseDataBean data) {
                            super.onNext(data);
                            DialogUtil.showToastShort(MomentCommentActivity.this, getString(R.string.moments_set_as_public_succeed));
                            momentBean.shareTo = MomentsBean.SHARE_TO_ALL;
                            refreshHeader();
                            requestFinished();
                        }
                    });

                }
            }
        });
    }

    /**
     * @param type 0: 分享链接  1: 分享海报
     */
    private void share(int type) {
        if (type == 0) {
            BusinessUtils.shareMoment(this, new DialogUtils(), momentBean, GrowingIoConstant.ENTRY_MOMENT_DETAIL_PAGE);
        } else {
            if (momentBean != null) {
                SharePosterBean sharePosterBean = new SharePosterBean();
                sharePosterBean.avatar = momentBean.avatar;
                sharePosterBean.momentsText = momentBean.momentsText;
                sharePosterBean.imageUrl = momentBean.imageUrl;
                sharePosterBean.nickname = momentBean.nickname;
                sharePosterBean.userName = momentBean.userName;
                sharePosterBean.from = MomentPosterActivity.FROM_MOMENT;
                MomentPosterActivity.gotoShare(sharePosterBean);
            }
        }
    }

    private SpannableString buildThemeHeaderStr(String nickName, String str) {
        SpannableString spannableString = new SpannableString(nickName + " " + str);
        spannableString.setSpan(new TextSpan(12, R.color.text_release_type), nickName.length(), nickName.length() + str.length() + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannableString;
    }

    //注册广播
    private void registerReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(TheLConstants.BROADCAST_REFRESH_VIDEOBEAN);//视频点赞数据更新
        intentFilter.addAction(TheLConstants.BROADCAST_AUTO_RELEASE_NEW_RECOMMEND_COMMENT_ACTION);//4.7.3推荐日志时，将推荐文字作为日志的一条评论
        intentFilter.addAction(TheLConstants.BROADCAST_MOMENT_DELETE_SUCCESS);
        dataChangedReceiver = new DataChangedReceiver();
        this.registerReceiver(dataChangedReceiver, intentFilter);
        followStatusChangedImpl.registerReceiver(this);
        momentBlackUtils = new MomentBlackUtils();
        momentBlackUtils.registerReceiver(this);
        stickerUtils = new StickerUtils();
        stickerUtils.registerReceiver(this);
        mMomentLikeUtils = new MomentLikeUtils();
        mMomentLikeUtils.registerReceiver(this);

    }

    @Override
    public void onBlackOneMoment(String momentId) {
        closeLoading();
        DialogUtil.showToastShort(this, getString(R.string.my_block_user_moments_activity_block_moment_succeed));
        finish();
    }

    @Override
    public void onBlackherMoment(String userId) {
        if (!isFinishing()) {
            DialogUtil.showToastShort(TheLApp.getContext(), getString(R.string.myblock_activity_block_success));
            finish();
        }
    }

    @Override
    public void onLikeStatusChanged(String momentId, boolean like, String myUserId, WinkCommentBean myWinkUserBean) {

        if (momentBean != null && momentId.equals(momentBean.momentsId)) {

            if (momentBean.winkFlag != MomentsBean.HAS_NOT_WINKED) {
                moment_opts_like.setChecked(true);
            } else {
                moment_opts_like.setChecked(false);
            }

            if (like) {
                momentBean.winkNum++;
            } else {
                momentBean.winkNum--;
            }

            // 点赞数
            if (momentBean.winkNum <= 0) {
                moment_opts_emoji_txt.setVisibility(GONE);
            } else {
                moment_opts_emoji_txt.setVisibility(VISIBLE);
                moment_opts_emoji_txt.setText(TheLApp.getContext().getString(momentBean.winkNum == 1 ? R.string.like_count_odd : R.string.like_count_even, momentBean.winkNum));
            }
        }

    }

    private class DataChangedReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (TheLConstants.BROADCAST_REFRESH_VIDEOBEAN.equals(intent.getAction())) {//视频日志点赞数据信息发生改变
                final VideoBean bean = (VideoBean) intent.getSerializableExtra(TheLConstants.BUNDLE_KEY_VIDEO_BEAN);
                refreshMoment(bean);
            } else if (TheLConstants.BROADCAST_AUTO_RELEASE_NEW_RECOMMEND_COMMENT_ACTION.equals(intent.getAction())) {//4.7.3推荐日志时，将推荐文字作为日志的一条评论
                commentType = intent.getStringExtra(RequestConstants.I_TYPE);
                myComment = intent.getStringExtra(RequestConstants.I_TEXT);
                toComment = null;
                needRefresh = true;
                // 添加评论后需要刷新moments列表
                // 清空输入框
                input.setText("");
                ReleasedCommentBeanV2 releasedCommentBeanV2 = new ReleasedCommentBeanV2();
                releasedCommentBeanV2.momentsId = "";
                releasedCommentBeanV2.id = "";
                afterReleaseComment(releasedCommentBeanV2);
            } else if (TheLConstants.BROADCAST_MOMENT_DELETE_SUCCESS.equals(intent.getAction())) {
                MomentCommentActivity.this.finish();
            }
        }
    }

    //刷新头部
    private void refreshMoment(VideoBean bean) {
        if (momentBean != null && bean != null) {
            momentBean.winkNum = bean.winkNum;
            momentBean.winkFlag = bean.winkFlag;
            if (bean.winkFlag == VideoBean.HAS_BEAN_WINKED) {
                WinkCommentBean winkCommentBean = new WinkCommentBean();
                winkCommentBean.avatar = ShareFileUtils.getString(ShareFileUtils.AVATAR, "");
                winkCommentBean.userId = Utils.getMyUserId();
            } else {
                updateLikeStatus();
            }
        }
    }

    private void getMomentInfoV2(String momentsId) {
        Flowable<MomentsBeanNew> flowable = DefaultRequestService.createAllRequestService().getMomentInfoV2(momentsId);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<MomentsBeanNew>() {
            @Override
            public void onNext(MomentsBeanNew result) {
                super.onNext(result);

                L.d("MomentCommentActivity", " ----------onNext---------- : ");

                if (!isDestroyed()) {
                    needRefresh = true;
                    refreshAll(result.data);
                    cursor = 0;
                    loadCommentsData(cursor);
                }
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);

                if (errDataBean != null) {
                    if (errDataBean.errcode.equals("87")) {
                        if (!isDestroyed()) {
                            startActivity(new Intent(MomentCommentActivity.this, MomentDeleteActivity.class));
                            MomentCommentActivity.this.finish();
                        }
                    }
                }
            }
        });
    }

    private void unwinkMoment(String momentsId) {


        MomentLikeUtils.unLikeMoment(momentsId);

    }

    /**
     * 收藏/取消收藏
     *
     * @param collect true为收藏，false为取消收藏
     */
    private void collect(boolean collect, final String momentsId) {

        Map<String, String> map = new HashMap<>();

        if (collect) {           //收藏
            img_share.setImageResource(R.mipmap.icn_collection_press);

            map.put(RequestConstants.FAVORITE_COUNT, momentsId);
            map.put(RequestConstants.FAVORITE_TYPE, RequestConstants.FAVORITE_TYPE_MOM);

            Flowable<BaseDataBean> flowable = DefaultRequestService.createMomentRequestService().getFavoriteCreate(MD5Utils.generateSignatureForMap(map));
            flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
                @Override
                public void onNext(BaseDataBean data) {
                    super.onNext(data);
                    String collectList = SharedPrefUtils.getString(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.FAVORITE_MOMENT_LIST, "");
                    String id = "[" + ShareFileUtils.getString(ShareFileUtils.ID, "") + "_" + momentsId + "]";
                    collectList += id;
                    SharedPrefUtils.setString(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.FAVORITE_MOMENT_LIST, collectList);
                }

                @Override
                public void onComplete() {
                    super.onComplete();
                    DialogUtil.showToastShort(TheLApp.context, getString(R.string.collection_success));

                }

            });
        } else {                  //取消收藏
            img_share.setImageResource(R.mipmap.icn_collection_normal);

            map.put(RequestConstants.FAVORITE_ID, "");
            map.put(RequestConstants.FAVORITE_COUNT, momentsId);
            map.put(RequestConstants.FAVORITE_TYPE, RequestConstants.FAVORITE_TYPE_MOM);

            Flowable<BaseDataBean> flowable = DefaultRequestService.createMomentRequestService().deleteFavoriteMoment(MD5Utils.generateSignatureForMap(map));
            flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
                @Override
                public void onNext(BaseDataBean data) {
                    super.onNext(data);
                    String collectList = SharedPrefUtils.getString(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.FAVORITE_MOMENT_LIST, "");
                    String id = "[" + ShareFileUtils.getString(ShareFileUtils.ID, "") + "_" + momentsId + "]";
                    collectList = collectList.replace(id, "");
                    SharedPrefUtils.setString(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.FAVORITE_MOMENT_LIST, collectList);
                    DialogUtil.showToastShort(MomentCommentActivity.this, getString(R.string.collection_canceled));
                }
            });
        }
    }

    private void likeMoment(String momentsId) {

        //        momentBean.winkNum += 1;
        //        momentBean.winkFlag = 1;
        needRefresh = true;
        //        sendVideoChangedBroadcast(1);
        requestFinished();

        MomentLikeUtils.likeMoment(momentsId);

    }

    private void getCommentList(String momentsId, final String limit, String cursor) {
        Flowable<CommentListBeanNew> flowable = DefaultRequestService.createAllRequestService().getCommentList(momentsId, limit, cursor);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<CommentListBeanNew>() {
            @Override
            public void onNext(CommentListBeanNew result) {
                super.onNext(result);
                if (!releasedComments.isEmpty() && commentList.size() > Integer.valueOf(limit)) {// 如果刚发布过评论，则需要删掉发过的所有评论
                    for (CommentBean comment : releasedComments) {
                        commentList.remove(comment);
                    }
                    releasedComments.clear();
                }
                //                result.data.filtBlockUsers();

                //4.7.3推荐增加一条评论后，从推荐进入评论页，置顶对应评论
                String parentMomentUserId = String.valueOf(getIntent().getIntExtra(TheLConstants.BUNDLE_KEY_USER_ID, 0));
                String parentMomentText = getIntent().getStringExtra(TheLConstants.BUNDLE_KEY_COMMENT_ON_TOP);
                if (parentMomentUserId.equals(ShareFileUtils.getString(ShareFileUtils.ID, ""))) {
                    for (int i = 0; i < result.data.commentList.size(); i++) {
                        CommentBean commentBean = result.data.commentList.get(i);
                        if (parentMomentText.equals(commentBean.commentText)) {
                            result.data.commentList.remove(i);
                            result.data.commentList.add(0, commentBean);
                            break;
                        }
                    }
                }
                refreshComments(result.data);
                requestFinished();
            }
        });
    }


    private void deleteComment() {
        DialogUtil.showConfirmDialog(this, null, operateComment.commentNum > 0 ? getString(R.string.delete_parent_comment_confirm) : getString(R.string.moments_delete_comment_confirm), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (operateComment != null && !TextUtils.isEmpty(operateComment.commentId)) {
                    showLoadingNoBack();

                    Map<String, String> map = new HashMap<>();

                    map.put(RequestConstants.I_ID, operateComment.commentId);

                    Flowable<BaseDataBean> flowable = DefaultRequestService.createAllRequestService().deleteComment(MD5Utils.generateSignatureForMap(map));

                    flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
                        @Override
                        public void onNext(BaseDataBean data) {
                            super.onNext(data);

                            needRefresh = true;
                            // 刷新列表
                            commentList.remove(operateComment);
                            listAdapter.notifyDataSetChanged();
                            momentBean.commentNum -= 1;
                            updateCommentNum();
                            if (commentList.isEmpty())
                                wide_divider.setVisibility(GONE);
                            else
                                wide_divider.setVisibility(VISIBLE);
                            requestFinished();
                        }
                    });

                }
            }
        });
    }

    private void deleteMoment() {

        MomentDeleteUtils.deleteMoment(MomentCommentActivity.this, momentBean.momentsId);


    }

    private void uploadVidePlayCount() {
        HashMap<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_ID, momentBean.momentsId);
        data.put(RequestConstants.VIDEO_PLAY_COUNT, "1");
        Flowable<BaseDataBean> flowable = DefaultRequestService.createAllRequestService().postVideoPlayCountUpload(MD5Utils.generateSignatureForMap(data));
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
            @Override
            public void onNext(BaseDataBean data) {
                super.onNext(data);
            }
        });
    }

    @Override
    public void onStickerChanged() {
        needRefreshStickers = true;
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            onUserInteraction();
        }
        if (getWindow().superDispatchTouchEvent(event)) {
            return true;
        }
        return onTouchEvent(event);
    }

    private boolean isNewType(String type) {

        try {
            if (MomentsBean.MOMENT_TYPE_TEXT.equals(type) || MomentsBean.MOMENT_TYPE_IMAGE.equals(type) || MomentsBean.MOMENT_TYPE_TEXT_IMAGE.equals(type) || MomentsBean.MOMENT_TYPE_VOICE.equals(type) || MomentsBean.MOMENT_TYPE_TEXT_VOICE.equals(type) || MomentsBean.MOMENT_TYPE_THEME.equals(type) || MomentsBean.MOMENT_TYPE_THEME_REPLY.equals(type) || MomentsBean.MOMENT_TYPE_VIDEO.equals(type) || MomentsBean.MOMENT_TYPE_LIVE.equals(type) || (MomentsBean.MOMENT_TYPE_USER_CARD).equals(type) || MomentsBean.MOMENT_TYPE_RECOMMEND.equals(type) || MomentsBean.MOMENT_TYPE_WEB.equals(type) || MomentsBean.MOMENT_TYPE_LIVE_USER.equals(type) || MomentsBean.MOMENT_TYPE_AD.equals(type) || MomentsBean.MOMENT_TYPE_VOICE_LIVE.equals(type)) {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return true;

        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        PushUtils.finish(MomentCommentActivity.this);
    }
}
