package com.thel.modules.main.settings;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.base.BaseDataBean;
import com.thel.bean.user.BlackBean;
import com.thel.bean.user.BlackMomentListBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.imp.momentblack.MomentBlackUtils;
import com.thel.modules.main.me.adapter.MyBlockMomentAdapter;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.ui.widget.recyclerview.decoration.DefaultItemDivider;
import com.thel.utils.DialogUtil;
import com.thel.utils.L;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * 我的屏蔽页(屏蔽某人的日志)
 * Created by lingwei on 2017/11/3.
 */

public class MyBlockUserMomentsActivity extends BaseActivity {
    @BindView(R.id.txt_title)
    TextView txtTitle;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.lin_back)
    LinearLayout linBack;
    @BindView(R.id.rel_title)
    RelativeLayout relTitle;
    @BindView(R.id.block_recyclerview)
    RecyclerView mRecyclerview;
    @BindView(R.id.default_tip)
    TextView defaultTip;
    @BindView(R.id.bg_myblock_default)
    LinearLayout bg_myblock_default;
    private ArrayList<BlackBean> listPlus = new ArrayList<BlackBean>();
    private MyBlockMomentAdapter myBlockAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.myblock_activity);
        ButterKnife.bind(this);
        initAdapter();
        initData();
        setListener();
        txtTitle.setText(getResources().getString(R.string.my_block_user_moments_activity_title));
        defaultTip.setText(getResources().getString(R.string.default_info_myblockusermoments));
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void setListener() {
        myBlockAdapter.setRemoveFromBlackListener(new MyBlockMomentAdapter.BlackUserLitener() {
            @Override
            public void removeFromBlackList(int pos, final BlackBean bean) {

                L.d("MyBlockUserMomentsActivity", " bean.userId : " + bean.userId);

                Flowable<BaseDataBean> flowable = RequestBusiness.getInstance().cancelBlockUserMoments(bean.userId + "");
                flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
                    @Override
                    public void onNext(BaseDataBean baseDataBean) {
                        super.onNext(baseDataBean);

                        L.d("MyBlockUserMomentsActivity", " onNext : " + baseDataBean.toString());

                        removeBlockData(bean);
                    }

                    @Override
                    public void onError(Throwable t) {

                        L.d("MyBlockUserMomentsActivity", " onError : " + t.getMessage());

                    }

                });
            }

            @Override
            public void goToUserInfo(int pos, BlackBean bean) {
//                Intent intent = new Intent(MyBlockUserMomentsActivity.this, UserInfoActivity.class);
//                intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, bean.userId + "");
//                startActivity(intent);
                FlutterRouterConfig.Companion.gotoUserInfo(bean.userId+"");
            }


        });
    }

    private void removeBlockData(BlackBean bean) {
        if (bean != null) {
            listPlus.remove(bean);
            //从缓存中删除这条数据
            //// TODO: 2017/11/3
            MomentBlackUtils.removeOneBlackUserMoment(bean.userId + "");
            bg_myblock_default.setVisibility(listPlus.size() == 0 ? View.VISIBLE : View.GONE);
            myBlockAdapter.setNewData(listPlus);
            myBlockAdapter.notifyDataSetChanged();
            DialogUtil.showToastShort(this, getString(R.string.my_circle_requests_act_canceled));
        }
    }

    private void initAdapter() {
        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerview.setLayoutManager(manager);
        mRecyclerview.addItemDecoration(new DefaultItemDivider(this, LinearLayoutManager.VERTICAL, R.color.gray, 1, true, true));
        mRecyclerview.setHasFixedSize(true);
        myBlockAdapter = new MyBlockMomentAdapter(listPlus);
        mRecyclerview.setAdapter(myBlockAdapter);
    }

    private void initData() {
        refreshData();
        Flowable<BlackMomentListBean> flowable = RequestBusiness.getInstance().getBlockUserListForMoments();
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BlackMomentListBean>() {
            @Override
            public void onNext(BlackMomentListBean blockListBean) {
                super.onNext(blockListBean);
                savaCache(blockListBean);
            }

            @Override
            public void onError(Throwable t) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void savaCache(BlackMomentListBean blockListBean) {
        if (blockListBean == null)
            return;
        ArrayList<BlackBean> beanArrayList = blockListBean.blackListUsers;
        for (int i = 0; i < beanArrayList.size(); i++) {
            MomentBlackUtils.savaOneBlackUserMoment(blockListBean.blackListUsers.get(i).userId + "");
        }
        listPlus.clear();
        listPlus.addAll(blockListBean.blackListUsers);
        bg_myblock_default.setVisibility(listPlus.size() == 0 ? View.VISIBLE : View.GONE);
        myBlockAdapter.setNewData(listPlus);
        myBlockAdapter.notifyDataSetChanged();
    }

    private void refreshData() {
       /* listPlus.clear();
        listPlus.addAll(BlackUtils.getBlackList());
        bg_myblock_default.setVisibility(listPlus.size() == 0 ? View.VISIBLE : View.GONE);
        if (null == myBlockAdapter) {
            myBlockAdapter = new MyBlockAdapter(listPlus);
            mRecyclerview.setAdapter(myBlockAdapter);

        } else {
            myBlockAdapter.setNewData(listPlus);
            myBlockAdapter.notifyDataSetChanged();
        }*/

    }

    @OnClick(R.id.lin_back)
    public void onViewClicked() {
        finish();
    }
}
