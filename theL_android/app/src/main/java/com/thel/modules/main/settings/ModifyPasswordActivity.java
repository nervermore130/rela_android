package com.thel.modules.main.settings;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.base.BaseDataBean;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.utils.DialogUtil;
import com.thel.utils.L;
import com.thel.utils.MD5Utils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.ViewUtils;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * 修改邮箱密码
 * Created by lingwei on 2017/11/1.
 */

public class ModifyPasswordActivity extends BaseActivity {
    private LinearLayout lin_back; // back键
    private TextView txt_email;
    private EditText edit_old;
    private EditText edit_new;
    private EditText edit_new_again;
    private Button button_ok;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.modify_password_activity);
        findViewById();
        processBusiness();
        setListener();
    }

    private void setListener() {
        lin_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                finish();
            }
        });
        button_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                String oldpassword = edit_old.getText().toString().trim();
                String newpassword = edit_new.getText().toString().trim();
                String newpassword_again = edit_new_again.getText().toString().trim();

                if (oldpassword.length() > 0) {
                    if (newpassword.length() > 0 && newpassword_again.length() > 0) {
                        if (newpassword.equals(newpassword_again)) {
                            Flowable<BaseDataBean> flowable = RequestBusiness.getInstance().modifyPassword(MD5Utils.md5(oldpassword), MD5Utils.md5(newpassword));
                            flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(new InterceptorSubscribe<BaseDataBean>() {
                                        @Override
                                        public void onNext(BaseDataBean baseDataBean) {
                                            super.onNext(baseDataBean);
                                            DialogUtil.showToastShort(ModifyPasswordActivity.this, TheLApp.getContext().getString(R.string.modifypassword_activity_success));
                                        }

                                        @Override
                                        public void onError(Throwable t) {
                                            L.d(" onError : " + t.getMessage());
                                        }

                                        @Override
                                        public void onComplete() {
                                            finish();

                                        }
                                    });
                        } else {
                            DialogUtil.showToastShort(ModifyPasswordActivity.this, TheLApp.getContext().getString(R.string.modifypassword_activity_not_same));
                        }
                    } else {
                        DialogUtil.showToastShort(ModifyPasswordActivity.this, TheLApp.getContext().getString(R.string.modifypassword_activity_new_password));

                    }
                } else {
                    Toast.makeText(ModifyPasswordActivity.this, TheLApp.getContext().getString(R.string.modifypassword_activity_old_password), Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void processBusiness() {
        String email = ShareFileUtils.getString(ShareFileUtils.EMAIL, "");
        txt_email.setText(email);
    }

    private void findViewById() {
        lin_back = this.findViewById(R.id.lin_back); // back键
        txt_email = this.findViewById(R.id.txt_email);
        edit_old = this.findViewById(R.id.edit_old);
        edit_new = this.findViewById(R.id.edit_new);
        edit_new_again = this.findViewById(R.id.edit_new_again);
        button_ok = this.findViewById(R.id.button_ok);
    }

    @Override
    public void finish() {
        ViewUtils.hideSoftInput(this, edit_new);
        super.finish();
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }
}
