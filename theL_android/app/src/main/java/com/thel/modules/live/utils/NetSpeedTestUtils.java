package com.thel.modules.live.utils;

import com.netease.LDNetDiagnoService.LDNetDiagnoListener;
import com.netease.LDNetDiagnoService.LDNetDiagnoService;
import com.thel.app.TheLApp;
import com.thel.bean.LiveInfoLogBean;
import com.thel.utils.L;
import com.thel.utils.UserUtils;

public class NetSpeedTestUtils {

    private final static String TAG = "NetSpeedTestUtils";

    private int requestCount = 1;

    private int finishRequestCount = 0;

    private int percent;

    private int delta = 1;

    private int currentPercent;

    private OnNetTestListener mOnNetTestListener;

    public void setRequestCount(int requestCount) {

        this.requestCount = requestCount;

        this.finishRequestCount = requestCount;

        percent = 100 / (requestCount * 5);

    }

    public void testIp(String ip, int port, String type) {

        if (ip != null && type != null) {

            L.d(TAG, " ip : " + ip);

            L.d(TAG, " logType : " + type);

            LDNetDiagnoService mLDNetDiagnoService = new LDNetDiagnoService(TheLApp.context,
                    "testDemo", "网络诊断应用", "1.0.0", "huipang@corp.netease.com",
                    "deviceID(option)", ip, port, "carriname", "ISOCountyCode",
                    "MobilCountryCode", "MobileNetCode", new MyLDNetDiagnoListener(type, ip));
            //设置是否使用JNIC 完成traceroute
            mLDNetDiagnoService.setIfUseJNICTrace(true);
//        mLDNetDiagnoService.setIfUseJNICConn(true);
            mLDNetDiagnoService.execute();

        }
    }

    private class MyLDNetDiagnoListener implements LDNetDiagnoListener {

        private String type;

        private String ip;

        public MyLDNetDiagnoListener(String type, String ip) {
            this.type = type;
            this.ip = ip;
        }

        @Override public void OnNetDiagnoFinished(String log) {
            L.d(TAG, " OnNetDiagnoFinished log : " + log);

            LiveInfoLogBean.getInstance().getLiveTraceRouteAnalytics().time = LiveUtils.getLiveTime();

            LiveInfoLogBean.getInstance().getLiveTraceRouteAnalytics().logType = type;

            LiveInfoLogBean.getInstance().getLiveTraceRouteAnalytics().traceRoute = log;

            LiveInfoLogBean.getInstance().getLiveTraceRouteAnalytics().userId = UserUtils.getMyUserId();

            LiveInfoLogBean.getInstance().getLiveTraceRouteAnalytics().cdnDomain = ip;

            LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getLiveTraceRouteAnalytics());

            finishRequestCount--;

            if (mOnNetTestListener != null && finishRequestCount <= 0) {

                mOnNetTestListener.onNetTest(100);

            }
        }

        @Override public void OnNetDiagnoUpdated(String log) {
            L.d(TAG, " OnNetDiagnoUpdated log : " + log);
            if (mOnNetTestListener != null && requestCount > 0) {
                if (log != null) {

                    L.d(TAG, " OnNetDiagnoUpdated percent : " + percent);

                    if (log.contains("开始诊断")) {
                        delta++;
                    }

                    if (log.contains("开始TCP连接测试")) {
                        delta++;
                    }

                    if (log.contains("开始ping")) {
                        delta++;
                    }

                    if (log.contains("开始traceroute")) {
                        delta++;
                    }

                    if (log.contains("网络诊断结束")) {
                        delta++;
                    }

                    if (currentPercent < 100 && currentPercent < delta * percent) {

                        currentPercent = delta * percent;

                    }

                    L.d(TAG, " OnNetDiagnoUpdated currentPercent : " + currentPercent);

                    if (currentPercent >= 100 || delta >= requestCount * 5) {
                        currentPercent = 100;
                    }

                    mOnNetTestListener.onNetTest(currentPercent);
                }
            }
        }
    }

    public void setOnNetTestListener(OnNetTestListener mOnNetTestListener) {
        this.mOnNetTestListener = mOnNetTestListener;
    }

    public interface OnNetTestListener {
        void onNetTest(int percent);
    }

    public static class NetSpeedTestBean {
        public String ip;
        public int port;
        public String type;
    }
}
