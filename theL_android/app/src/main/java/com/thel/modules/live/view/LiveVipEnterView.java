package com.thel.modules.live.view;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kingsoft.media.httpcache.KSYProxyService;
import com.liulishuo.filedownloader.FileDownloader;
import com.thel.app.TheLApp;
import com.thel.modules.live.bean.BigGiftMsgTextBean;
import com.thel.modules.live.bean.GiftSendMsgBean;
import com.thel.modules.live.bean.LiveRoomMsgBean;
import com.thel.modules.live.in.LivePkContract;
import com.thel.utils.AppInit;
import com.thel.utils.L;
import com.thel.utils.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import video.com.relavideolibrary.RelaVideoSDK;

/**
 * Created by waiarl on 2017/4/17.
 */

public class LiveVipEnterView extends LinearLayout {
    private static final String TAG = "LiveVipEnterView";
    private final Context mContext;
    private int anim_duration;

    private LinearInterpolator interpolator;
    private Handler mHandler;
    private boolean canShow = true;
    private int inSpDuration;
    private int left_padding;
    private TextView txt_biggift;
    private int showDuration;
    private LiveBigGiftMsgView liveBigGifgMisgView;
    private int tranX2;
    private OnUserClickListener mOnUserClickListener;
    private List<BigGiftMsgTextBean> bigGiftMsgTextBeanList = new ArrayList<>();
    private List<LiveRoomMsgBean> liveRoomMsgBeanList = new ArrayList<>();
    private String[] mpRes = {"enter/live_level_26_mp.mp3", "enter/live_level_30_mp.mp3"};
    private LiveEnterMediaPlayer player;

    public LiveVipEnterView(Context context) {
        this(context, null);
    }

    public LiveVipEnterView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LiveVipEnterView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        initConstant();
        init();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        for (int i = 0; i < getChildCount(); i++) {
            View v = getChildAt(i);
            v.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    private void initConstant() {
        mHandler = new Handler(Looper.getMainLooper());
        anim_duration = 300;
        interpolator = new LinearInterpolator();
        inSpDuration = 100;
        left_padding = Utils.dip2px(mContext, 10);
        showDuration = 1300;
        tranX2 = Utils.dip2px(TheLApp.getContext(), 20);
    }

    private void init() {
        setOrientation(VERTICAL);
        setGravity(Gravity.CENTER_VERTICAL);
        setPadding(left_padding, 0, 0, 0);
    }

    /**
     * 唯一对外公共方法，有会员进入房间调用
     * 4.6.0以后该为付费用户进入，需要等级大于5级
     *
     * @param bean
     */
    public void joinNewUser(LiveRoomMsgBean bean) {
        /***测试***/
        //    bean.userLevel = new Random().nextInt(2) == 0 ? 30 : 29;

        if (bean != null && bean.userLevel > 5) {
            liveRoomMsgBeanList.add(bean);
            tryToAnim();
        }
    }

    private void initView(final LiveRoomMsgBean bean) {
        if (bean == null) {
            canShow = true;
            tryToAnim();
            return;
        }
        if (getChildCount() > 0) {
            return;
        }

        final LiveLevelEnterView levelEnterView = new LiveLevelEnterView(getContext()).initView(bean);
        levelEnterView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnUserClickListener != null) {
                    mOnUserClickListener.clickUser(bean.userId);
                }
            }
        });
        addView(levelEnterView);

        canShow = false;
        measureChild(levelEnterView, MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
        liveRoomMsgBeanList.remove(bean);
        animIn(levelEnterView);
        if (!TextUtils.isEmpty(bean.soundEffect)) {
            playNetworkSound(bean.soundEffect, bean.userLevel);
        } else {
            playMp3(bean.userLevel);
        }
    }

    private void playMp3(int userLevel) {
        if (userLevel < 26) {
            return;
        }
        String res = mpRes[0];
        if (userLevel >= 30) {
            res = mpRes[1];
        }
        playSound(res, null);
    }

    /**
     * 进入
     *
     * @param view
     */
    private void animIn(final LiveLevelEnterView view) {
        view.clearAnimation();
        final long duration = view.getDuration();
        final int width = view.getMeasuredWidth();
        final LayoutParams param = (LayoutParams) view.getLayoutParams();
        final int left_margin = param.leftMargin;
        final int start = -(width + left_margin);
        ObjectAnimator animator = ObjectAnimator.ofFloat(view, TRANSLATION_X, start, 0);
        animator.setDuration(anim_duration);
        animator.setInterpolator(interpolator);

        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        animInS(view);
                        // animOut(view);
                    }
                }, duration);

            }

            @Override
            public void onAnimationCancel(Animator animation) {
                canShow = true;
                if (view != null && view.getParent() != null) {
                    ((ViewGroup) view.getParent()).removeView(view);
                }
                tryToAnim();

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        animator.start();
    }

    private void animInS(final View view) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(view, TRANSLATION_X, tranX2);
        animator.setDuration(showDuration);
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                animOut(view);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                canShow = true;
                if (view != null && view.getParent() != null) {
                    ((ViewGroup) view.getParent()).removeView(view);
                }
                tryToAnim();
            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        animator.start();

    }

    /**
     * 淡出
     *
     * @param view
     */
    private void animOut(final View view) {
        final int width = view.getMeasuredWidth();
        final LayoutParams param = (LayoutParams) view.getLayoutParams();
        final int left_margin = param.leftMargin;
        final int end = -(width + left_margin);
        ObjectAnimator animator = ObjectAnimator.ofFloat(view, View.TRANSLATION_X, end);
        animator.setDuration(anim_duration);
        animator.setInterpolator(interpolator);
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (view != null && view.getParent() != null) {
                    ((ViewGroup) view.getParent()).removeView(view);
                }
                canShow = true;
                tryToAnim();

            }

            @Override
            public void onAnimationCancel(Animator animation) {
                if (view != null && view.getParent() != null) {
                    ((ViewGroup) view.getParent()).removeView(view);
                }
                canShow = true;
                tryToAnim();
            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        animator.start();
    }

    private void tryToAnim() {
        if (canShow) {
            if (bigGiftMsgTextBeanList.size() > 0) {
                initBigGiftText(bigGiftMsgTextBeanList.get(0));
            } else if (liveRoomMsgBeanList.size() > 0) {
                initView(liveRoomMsgBeanList.get(0));
            }
        }
    }

    private void initBigGiftText(final BigGiftMsgTextBean bigGiftMsgTextBean) {
        if (mContext == null) {
            return;
        }
        if (bigGiftMsgTextBean == null) {
            tryToAnim();
        }
        liveBigGifgMisgView = new LiveBigGiftMsgView(mContext);
        addView(liveBigGifgMisgView);
        liveBigGifgMisgView.initView(bigGiftMsgTextBean.giftSendMsgBean, bigGiftMsgTextBean.listener);
        liveBigGifgMisgView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnUserClickListener != null) {
                    mOnUserClickListener.clickUser(bigGiftMsgTextBean.giftSendMsgBean.userId);
                }
            }
        });
        bigGiftMsgTextBeanList.remove(bigGiftMsgTextBean);
    }

    public void hideBigGiftText() {
        if (liveBigGifgMisgView != null) {
            liveBigGifgMisgView.destoryView();
            liveBigGifgMisgView.setVisibility(View.GONE);
            removeView(liveBigGifgMisgView);
            liveBigGifgMisgView = null;
        }
        tryToAnim();
    }

    public LiveVipEnterView receiveBigGiftMsg(GiftSendMsgBean bean, OnClickListener listener) {
        final BigGiftMsgTextBean msgTextBean = new BigGiftMsgTextBean();
        msgTextBean.giftSendMsgBean = bean;
        msgTextBean.listener = listener;
        bigGiftMsgTextBeanList.add(msgTextBean);
        if (canShow) {
            tryToAnim();
        }
        return this;
    }

    public interface OnUserClickListener {
        void clickUser(String userId);
    }

    public void setOnUserClickListener(OnUserClickListener listener) {
        mOnUserClickListener = listener;
    }

    @Override
    public void clearAnimation() {
        clear();
        super.clearAnimation();
    }

    public void clear() {
        bigGiftMsgTextBeanList.clear();
        liveRoomMsgBeanList.clear();
    }


    /**
     * 播放声音Assets文件
     *
     * @param assetPath
     * @param listener
     */
    public void playSound(String assetPath, final LivePkContract.LivePkPlaySoundListener listener) {
        if (TextUtils.isEmpty(assetPath)) {
            return;
        }
        try {
            final AssetFileDescriptor fileDescriptor = TheLApp.getContext().getAssets().openFd(assetPath);
            player = LiveEnterMediaPlayer.getInstance();
            try {
                if (player.isPlaying()) {
                    player.pause();
                    player.stop();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            player.reset();
            player.setDataSource(fileDescriptor.getFileDescriptor(), fileDescriptor.getStartOffset(), fileDescriptor.getLength());
            player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    if (listener != null) {
                        listener.onSoundPlay(player, LivePkContract.LivePkPlaySoundListener.SOUND_END);
                    }
                }
            });
            player.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    player.release();
                    return false;
                }
            });
            player.prepare();
            player.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playNetworkSound(String url, int userLevel) {

        L.d(TAG, " url : " + url);

        KSYProxyService ksyProxy = RelaVideoSDK.getKSYProxy();
        ksyProxy.startServer();
        String proxyUrl = ksyProxy.getProxyUrl(url);

        L.d(TAG, " proxyUrl : " + proxyUrl);

        player = LiveEnterMediaPlayer.getInstance();
        try {
            if (player.isPlaying()) {
                player.pause();
                player.stop();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        player.reset();
        try {
            player.setDataSource(proxyUrl);
            player.prepareAsync();
            player.setOnPreparedListener(mp -> {
                L.d(TAG, " -------onPrepared-------");
                player.start();
            });
            player.setOnErrorListener((mp, what, extra) -> {

                L.d(TAG, " -------onError-------" + what + " ,extra : " + extra);

                playMp3(userLevel);

                return false;
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        if (player != null) {
            player.stop();
            player.release();
            player = null;
        }

    }
}
