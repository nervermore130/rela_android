package com.thel.modules.main.userinfo;

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseDataBean;
import com.thel.bean.moments.MomentsListBean;
import com.thel.bean.user.UserInfoBean;
import com.thel.modules.main.userinfo.bean.HiddenLoveNetBean;
import com.thel.modules.main.userinfo.bean.UserInfoNetBean;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.api.nearbyapi.NearbyApiBusiness;
import com.thel.network.service.DefaultRequestService;
import com.thel.network.service.ErrorCodeConstants;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.ToastUtils;

import org.reactivestreams.Subscription;

import java.io.IOException;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by waiarl on 2017/10/18.
 */

public class UserInfoPresenter implements UserInfoContract.Presenter {
    private final UserInfoContract.View view;
    public final int pageSize = 10; // 每页请求条数

    public UserInfoPresenter(UserInfoContract.View view) {
        this.view = view;
        view.setPresenter(this);
    }

    @Override
    public void getUserInfoData(final String userId) {
        final Flowable<UserInfoNetBean> flowable = DefaultRequestService.createNearbyRequestService().getUserInfoBean(userId);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<UserInfoNetBean>() {

            @Override
            public void onNext(UserInfoNetBean userInfoNetBean) {
                super.onNext(userInfoNetBean);

                L.d("userInfoNetBean", " hasErrorCode " + hasErrorCode);

                L.d("userInfoNetBean", " userInfoNetBean.errcode " + userInfoNetBean.errcode);

                if (userInfoNetBean != null) {

                    if (userInfoNetBean.data != null) {

                        L.d("userInfoNetBean", " 1 ");

                        final UserInfoBean userInfoBean = userInfoNetBean.data;
                        userInfoBean.setHiddenLove();
                        view.showUserInfoCommonViewData(userInfoBean);
                        view.showUserInfoMainTopViewData(userInfoBean);
                        view.showUserInfoRightViewData(userInfoBean);
                        view.showUserInfoMomentHeaderView(userInfoBean);
                        //   getRefreshUserInfoMomentListData(userId);
                    } else if (hasErrorCode && userInfoNetBean.errcode.equals("999")) {//4.2.0后会根据errcode来判断主动屏蔽和被动屏蔽

                        L.d("userInfoNetBean", " 999 ");

                        view.isBlockMe();
                    } else if (hasErrorCode && userInfoNetBean.errcode.equals("80")) {//非法用户

                        L.d("userInfoNetBean", " 80 ");

                        view.illegalUser();
                    } else if (hasErrorCode && userInfoNetBean.errcode.equals("81")) {//此用户不存在

                        L.d("userInfoNetBean", " 81 ");

                        view.notExistUser();
                    } else if (hasErrorCode && userInfoNetBean.errcode.equals(ErrorCodeConstants.ERROR_CODE_USER_UNREGISTER)) {

                        L.d("userInfoNetBean", " user_register ");

                        view.unregisterUser();
                    } else if (hasErrorCode && userInfoNetBean.errcode.equals("1000")) {

                        L.d("userInfoNetBean", " 1000 ");

                        view.isMyBlack();
                    }

                }

            }

            @Override
            public void onError(Throwable t) {

                if (t instanceof HttpException) {
                    HttpException httpException = (HttpException) t;

                    try {
                        String body = httpException.response().errorBody().string();

                        BaseDataBean errorBaseData = GsonUtils.getObject(body, BaseDataBean.class);

                        if (errorBaseData != null && errorBaseData.errcode.equals("999")) {//我被她屏蔽
                            view.isBlockMe();
                        } else if (errorBaseData != null && errorBaseData.errcode.equals("80")) {//非法用户
                            view.illegalUser();
                        } else if (errorBaseData != null && errorBaseData.errcode.equals("user_unregister")) {//用户被注销
                            view.unregisterUser();
                        } else if (errorBaseData != null && errorBaseData.errcode.equals("1000")) {
                            view.isMyBlack();
                        } else if (errorBaseData != null && errorBaseData.errcode.equals("81")) {//此用户不存在
                            view.notExistUser();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onComplete() {
                view.requestFinish();
            }
        });
    }

    @Override
    public void getRefreshUserInfoMomentListData(String userId) {
        getUserInfoMomentListData(userId, 1);
    }

    @Override
    public void getLoadMoreUserInfoMomentListData(String userId, int cursor) {
        getUserInfoMomentListData(userId, cursor);
    }

    private void getUserInfoMomentListData(String userId, final int cursor) {
        if (!ShareFileUtils.getBoolean(ShareFileUtils.HAS_LOGGED, false)) {
            return;
        }
        final Flowable<MomentsListBean> flowable = DefaultRequestService.createNearbyRequestService().getUserInfoMomentsList(userId, pageSize + "", cursor + "", "");
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<MomentsListBean>() {
            @Override
            public void onNext(MomentsListBean data) {
                super.onNext(data);

                if (!hasErrorCode && data != null) {
                    if (cursor == 1) {
                        view.showRefreshMomentListData(data.momentsList);
                    } else {
                        view.showLoadMoreMomentListData(data.momentsList);
                    }
                    view.haveNextPage(data.haveNextPage);
                    if (cursor < 2) {
                        view.refreshMomentNum(data.momentsTotalNum);
                    }
                } else if (cursor > 1) {
                    view.loadMoreFailed();
                }
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                if (cursor > 1) {
                    view.loadMoreFailed();
                }
            }

            @Override
            public void onComplete() {
                super.onComplete();
                view.requestFinish();
            }
        });
    }

    @Override
    public void blackHer(final String userId) {

        L.d("UserInfoPresenter", " userId : " + userId);

        view.showLoadingDialog();
        final Flowable<BlockNetBean> flowable = NearbyApiBusiness.pullUserIntoBlackList(userId);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BlockNetBean>() {
            @Override
            public void onNext(BlockNetBean data) {
                super.onNext(data);

                L.d("UserInfoPresenter", " onNext : " + data.data.toString());

                L.d("UserInfoPresenter", " hasErrorCode : " + hasErrorCode);


                if (data != null && data.data != null) {
                    view.pullUserIntoBlock(data.data);
                }
            }

            @Override public void onError(Throwable t) {
                super.onError(t);

                L.d("UserInfoPresenter", " onError : " + t.getMessage());


            }

            @Override
            public void onComplete() {
                super.onComplete();
                view.requestFinish();
            }
        });
    }

    @Override
    public void showLoading() {
        view.showLoadingDialog();
    }

    @Override
    public void sendHiddenLove(String userId) {
        final Flowable<HiddenLoveNetBean> flowable = NearbyApiBusiness.sendHiddenLove(userId);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<HiddenLoveNetBean>() {
            @Override
            public void onNext(HiddenLoveNetBean data) {
                super.onNext(data);
                if (!hasErrorCode && data != null && data.data != null) {
                    view.showHiddenLove(data.data);
                }
            }

            @Override
            public void onComplete() {
                super.onComplete();
                view.requestFinish();
            }
        });
    }


    @Override
    public void secretlyFollow(final String userId, final int actionType) {
        view.showLoadingDialog();
        final Flowable<BaseDataBean> flowable = NearbyApiBusiness.secretlyFollow(userId, actionType + "");
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
            @Override
            public void onNext(BaseDataBean data) {
                super.onNext(data);
                if (!hasErrorCode && data != null) {
                    view.showSecretlyFollow(userId, actionType);
                }
            }

            @Override
            public void onComplete() {
                super.onComplete();
                view.requestFinish();
            }
        });

    }

    @Override
    public void postReportImageOrUser(String userId, String avatar, int type) {
        final Flowable<BaseDataBean> flowable = NearbyApiBusiness.reportImageOrUser(userId, avatar + "", type + "");
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
            @Override
            public void onNext(BaseDataBean data) {
                super.onNext(data);
                if (!hasErrorCode) {
                    ToastUtils.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.userinfo_activity_report_success));
                }
            }

            @Override
            public void onComplete() {
                super.onComplete();
                view.requestFinish();
            }
        });
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {

    }
}
