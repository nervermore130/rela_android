package com.thel.modules.main.me.aboutMe;

import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.base.BaseDataBean;
import com.thel.bean.user.MatchQuestionBean;
import com.thel.modules.main.me.adapter.UpdataUserInfoMultiDialogAdapter;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.utils.AppInit;
import com.thel.utils.L;
import com.thel.utils.ViewUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * 匹配设置
 * Created by lingwei on 2017/10/25.
 */

public class MatchConfigActivity extends BaseActivity {
    private TextView txt_save;
    private EditText edit_q1;
    private EditText edit_q2;
    private EditText edit_q3;
    private ImageView img_match_same_city;
    private RelativeLayout rel_match_age_range;
    private TextView txt_match_age_range;
    private RelativeLayout rel_lookingfor_role;
    private TextView edit_lookingfor_role;

    private int matchCity = 0;
    private int ageStart = 18;
    private int ageEnd = 55;
    private ArrayList<String> role_list = new ArrayList<>(); // 角色
    private ArrayList<Integer> lookingForRoleSelectPositions = new ArrayList<>(); // 寻找角色多选列表
    private ArrayList<Integer> lookingForRoleTemplist = new ArrayList<>(); // 寻找角色多选临时列表(在dialog没点确定之前记录)
    private StringBuilder lookingForRoleSB = new StringBuilder(); // 寻找角色数组下标字符串拼接

    // 初始化的匹配数据，用来判断用户是不是修改了匹配条件
    private int initAgeStart = ageStart;
    private int initAgeEnd = ageEnd;
    private int initMatchCity = matchCity;
    private String initLookingForRole = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.match_config_dialog);
        findViewById();
        initData();
        setListener();
    }

    private void findViewById() {
        ((TextView) (this.findViewById(R.id.txt_title))).setText(getString(R.string.match_config_title));
        txt_save = this.findViewById(R.id.txt_save);
        edit_q1 = this.findViewById(R.id.edit_q1);
        edit_q2 = this.findViewById(R.id.edit_q2);
        edit_q3 = this.findViewById(R.id.edit_q3);
        img_match_same_city = this.findViewById(R.id.img_match_same_city);
        rel_lookingfor_role = findViewById(R.id.rel_lookingfor_role);
        edit_lookingfor_role = findViewById(R.id.edit_lookingfor_role);
        rel_match_age_range = findViewById(R.id.rel_match_age_range);
        txt_match_age_range = findViewById(R.id.txt_match_age_range);
        View lin_more = findViewById(R.id.lin_more);
        lin_more.setVisibility(View.GONE);
        // 匹配年龄段
        setAgeRange();
    }

    private void initData() {
        String[] role_Array = this.getResources().getStringArray(R.array.role);
        role_list = new ArrayList<>(Arrays.asList(role_Array));
        // 寻找角色

        StringBuilder sbShow = new StringBuilder();
        for (int i = 0; i < role_list.size(); i++) {
            lookingForRoleSelectPositions.add(Integer.valueOf(i));
            lookingForRoleSB.append(i);
            sbShow.append(role_list.get(i));
            if (i != role_list.size() - 1) {
                lookingForRoleSB.append(",");
                sbShow.append(",");
            }
        }
        edit_lookingfor_role.setText(sbShow.toString());
        processBusiness();
    }

    private void setListener() {
        // 点击事件
        txt_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                showLoading();
                postMyQuestions(edit_q1.getText().toString(), edit_q2.getText().toString(), edit_q3.getText().toString(), ageStart, ageEnd == 55 ? 100 : ageEnd, encodeRoles(lookingForRoleSB), matchCity);
            }
        });

        this.findViewById(R.id.lin_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        img_match_same_city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                matchCity = 1 - matchCity;
                img_match_same_city.setImageResource(matchCity == 0 ? R.mipmap.btn_check_left : R.mipmap.btn_check_right);
            }
        });

        rel_match_age_range.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                displayMatchAgeRangePicker();
            }
        });

        rel_lookingfor_role.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                displayMultiChoiseDialog(role_list, lookingForRoleSelectPositions, lookingForRoleTemplist, lookingForRoleSB, edit_lookingfor_role);
            }
        });
    }

    private void postMyQuestions(String q1, String q2, String q3, int matchAgeBegin, int matchAgeEnd, String matchRole, int sameCity) {
        Flowable<BaseDataBean> flowable = RequestBusiness.getInstance().setMyQuestions(q1, q2, q3, matchAgeBegin, matchAgeEnd, matchRole, sameCity);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
            @Override
            public void onNext(BaseDataBean baseDataBean) {
                super.onNext(baseDataBean);
                setMyQuestions();
            }

            @Override
            public void onError(Throwable t) {
                L.d(" onError : " + t.getMessage());
            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void setMyQuestions() {
        if (ageStart != initAgeStart || ageEnd != initAgeEnd || initMatchCity != matchCity || !initLookingForRole.equals(lookingForRoleSB.toString())) {// 如果用户修改了匹配条件，则告知匹配页面刷新数据
            setResult(1);
        }
        finish();
    }

    /**
     * 数字段选择控件
     *
     * @return
     */
    public void displayMatchAgeRangePicker() {

        final Dialog mDialog = new Dialog(this, R.style.CustomDialogBottom);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.show();
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.gravity = Gravity.BOTTOM;
        lp.width = AppInit.displayMetrics.widthPixels; // 设置宽度
        mDialog.getWindow().setAttributes(lp);

        LayoutInflater flater = LayoutInflater.from(this);
        View view = flater.inflate(R.layout.number_range_picker_dialog, null);
        mDialog.setContentView(view);

        String[] valuesStart = new String[37];
        int valueStart = -1;
        for (int i = 0; i < 37; i++) {
            valuesStart[i] = (i + 18) + "";
            if (valuesStart[i].equals(ageStart + ""))
                valueStart = i;
        }

        String[] valuesEnd = new String[55 - ageStart];
        int valueEnd = -1;
        for (int i = 0; i < 55 - ageStart; i++) {
            if (i != 55 - ageStart - 1)
                valuesEnd[i] = (i + ageStart + 1) + "";
            else
                valuesEnd[i] = (i + ageStart + 1) + "+";
            if (valuesEnd[i].equals(ageEnd + ""))
                valueEnd = i;
        }

        final NumberPicker numberPickerStart = view.findViewById(R.id.np_start);
        numberPickerStart.setDisplayedValues(valuesStart);
        numberPickerStart.setMinValue(0);
        numberPickerStart.setMaxValue(valuesStart.length - 1);
        numberPickerStart.setValue(valueStart);
        final NumberPicker numberPickerEnd = view.findViewById(R.id.np_end);
        numberPickerEnd.setDisplayedValues(valuesEnd);
        numberPickerEnd.setMinValue(0);
        numberPickerEnd.setMaxValue(valuesEnd.length - 1);
        numberPickerEnd.setValue(valueEnd);

        LinearLayout lin_done = view.findViewById(R.id.lin_done);

        numberPickerStart.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                try {
                    String endStr = numberPickerEnd.getDisplayedValues()[numberPickerEnd.getValue()];
                    int index = -1;
                    String[] values = new String[37 - newVal];
                    for (int i = newVal + 1, j = 0; i < 38; i++, j++) {
                        if (i != 37)
                            values[j] = (i + 18) + "";
                        else
                            values[j] = (i + 18) + "+";
                        if (values[j].equals(endStr))
                            index = j;
                    }

                    numberPickerEnd.setDisplayedValues(null);
                    numberPickerEnd.setMinValue(0);
                    numberPickerEnd.setMaxValue(values.length - 1);
                    numberPickerEnd.setDisplayedValues(values);

                    if (index != -1)
                        numberPickerEnd.setValue(index);
                    else
                        numberPickerEnd.setValue(0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        lin_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    String start = numberPickerStart.getDisplayedValues()[numberPickerStart.getValue()];
                    String end = numberPickerEnd.getDisplayedValues()[numberPickerEnd.getValue()];
                    ageStart = Integer.valueOf(start.equals("55+") ? "55" : start);
                    ageEnd = Integer.valueOf(end.equals("55+") ? "55" : end);
                    setAgeRange();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mDialog.dismiss();
            }
        });
    }

    private void setAgeRange() {
        txt_match_age_range.setText(ageStart + getString(R.string.updatauserinfo_activity_age_unit) + " - " + (ageEnd == 55 ? "55" + getString(R.string.updatauserinfo_activity_age_unit) + "+" : ageEnd + getString(R.string.updatauserinfo_activity_age_unit)));
    }

    // 为了跟用户详情里的role下标一致，所以要+1
    private String encodeRoles(StringBuilder sb) {
        StringBuilder result = new StringBuilder();
        try {
            String[] arr = sb.toString().split(",");
            for (String str : arr) {
                result.append(Integer.parseInt(str) + 1);
                result.append(",");
            }
            if (result.length() > 0)
                result.deleteCharAt(result.length() - 1);
        } catch (Exception e) {
            return "";
        }
        return result.toString();
    }


    private void displayMultiChoiseDialog(final ArrayList<String> list, final ArrayList<Integer> selectPos, final ArrayList<Integer> tempList, final StringBuilder sb, final TextView editText) {
        final Dialog dialog = new Dialog(this, R.style.CustomDialogBottom);
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.gravity = Gravity.BOTTOM;
        lp.width = AppInit.displayMetrics.widthPixels; // 设置宽度
        dialog.getWindow().setAttributes(lp);

        LayoutInflater flater = LayoutInflater.from(this);
        View view = flater.inflate(R.layout.single_choice_dialog, null);
        dialog.setContentView(view);
        LinearLayout lin_next = view.findViewById(R.id.lin_next);
        ListView listview = view.findViewById(R.id.listview);
        TextView txt_title = view.findViewById(R.id.txt_title);
        txt_title.setText(TheLApp.getContext().getString(R.string.updatauserinfo_activity_dialog_title_multichoice));
        tempList.clear();
        tempList.addAll(selectPos);
        final UpdataUserInfoMultiDialogAdapter adapter = new UpdataUserInfoMultiDialogAdapter(list, selectPos);
        listview.setAdapter(adapter);
        listview.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                boolean remove = false;
                for (int i = 0; i < tempList.size(); i++) {
                    if (tempList.get(i) == position) {
                        if (tempList.size() == 1) {// 至少选一项
                            return;
                        }
                        tempList.remove(i);
                        remove = true;
                        break;
                    }
                }
                if (!remove) {
                    tempList.add(position);
                }
                adapter.refreshList(tempList);
                adapter.notifyDataSetChanged();
            }
        });

        lin_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                dialog.dismiss();
                selectPos.clear();
                selectPos.addAll(tempList);
                sb.delete(0, sb.length());

                if (!selectPos.isEmpty()) {
                    Collections.sort(selectPos);
                    String select = "";
                    for (int i = 0; i < selectPos.size(); i++) {
                        int j = selectPos.get(i);
                        sb.append(j);
                        select += list.get(j);
                        if (i != selectPos.size() - 1) {
                            select += ",";
                            sb.append(",");
                        }
                    }
                    editText.setText(select);
                } else {
                    editText.setText("");
                }
            }
        });
    }

    private void processBusiness() {
        showLoading();
        Flowable<MatchQuestionBean> flowable = RequestBusiness.getInstance().getMyQuestions();
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<MatchQuestionBean>() {
            @Override
            public void onNext(MatchQuestionBean matchQuestionBean) {
                super.onNext(matchQuestionBean);
                getMyQuestions(matchQuestionBean);
            }

            @Override
            public void onError(Throwable t) {
                L.d(" onError : " + t.getMessage());
            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void getMyQuestions(MatchQuestionBean questionBean) {
        if (questionBean.data == null || questionBean.data.question == null || questionBean.data.question.questions == null) {
            return;
        }
        closeLoading();
        edit_q1.setText(questionBean.data.question.questions.question1);
        edit_q2.setText(questionBean.data.question.questions.question2);
        edit_q3.setText(questionBean.data.question.questions.question3);
        initMatchCity = matchCity = questionBean.data.question.sameCity;
        img_match_same_city.setImageResource(matchCity == 0 ? R.mipmap.btn_check_left : R.mipmap.btn_check_right);

        // 匹配年龄段
        try {
            if (questionBean.data.question.matchAgeBegin < questionBean.data.question.matchAgeEnd) {// 正确的数据
                if (questionBean.data.question.matchAgeBegin < 55 && questionBean.data.question.matchAgeBegin >= 18)
                    initAgeStart = ageStart = questionBean.data.question.matchAgeBegin;
                if (questionBean.data.question.matchAgeEnd > 18)
                    initAgeEnd = ageEnd = questionBean.data.question.matchAgeEnd >= 55 ? 55 : questionBean.data.question.matchAgeEnd;
                txt_match_age_range.setText(ageStart + getString(R.string.updatauserinfo_activity_age_unit) + " - " + (ageEnd == 55 ? "55" + getString(R.string.updatauserinfo_activity_age_unit) + "+" : ageEnd + getString(R.string.updatauserinfo_activity_age_unit)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // 匹配角色
        if (!TextUtils.isEmpty(questionBean.data.question.matchRole)) {
            try {
                String[] lookingForRole = questionBean.data.question.matchRole.split(",");
                StringBuilder sb = new StringBuilder();// 寻找角色内容字符串拼接
                lookingForRoleSB = new StringBuilder();
                lookingForRoleSelectPositions.clear();
                int size = lookingForRole.length;
                for (int i = 0; i < size; i++) {
                    int index = Integer.parseInt(lookingForRole[i]) - 1;// 为了跟用户详情里的role下标一致，所以要-1
                    if (index < 0)
                        continue;
                    sb.append(role_list.get(index));
                    lookingForRoleSB.append(index);
                    lookingForRoleSelectPositions.add(index);
                    if (i != size - 1) {
                        sb.append(",");
                        lookingForRoleSB.append(",");
                    }
                }
                initLookingForRole = lookingForRoleSB.toString();
                edit_lookingfor_role.setText(sb.toString());
            } catch (Exception e) {
            }
        }
    }

    @Override
    public void finish() {
        ViewUtils.hideSoftInput(MatchConfigActivity.this);

        super.finish();

    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }
}
