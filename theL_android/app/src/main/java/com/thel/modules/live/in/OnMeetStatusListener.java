package com.thel.modules.live.in;

public interface OnMeetStatusListener {

    void onMeetStatus(int status);

}
