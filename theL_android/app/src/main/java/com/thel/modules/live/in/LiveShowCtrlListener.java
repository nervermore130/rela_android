package com.thel.modules.live.in;

import android.view.KeyEvent;
import android.view.MotionEvent;

import com.thel.modules.live.bean.LiveRoomBean;

/**
 * Created by waiarl on 2017/11/2.
 */

public interface LiveShowCtrlListener {
    /**
     * 初始化直播间（暂停直播并且初始化一切控件）
     */
    void initLiveShow();

    /**
     * 刷新直播间内容
     *
     * @param liveRoomBean
     */
    void refreshLiveShow(LiveRoomBean liveRoomBean);


    boolean dispatchTouchEvent(MotionEvent ev);

    boolean onKeyDown(int keyCode, KeyEvent event);

    boolean showLiveView(boolean show);

    void VideoPrepared();

}
