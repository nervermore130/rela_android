package com.thel.modules.live.in;


import com.thel.modules.live.bean.SoftGiftBean;

/**
 * Created by waiarl on 2017/4/19.
 * 连击dialog返回callback
 */

public interface GiftLianSendCallback {
    void lianGift(SoftGiftBean softGiftBean, String type, int combo);
}
