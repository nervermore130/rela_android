package com.thel.modules.main.messages.bean;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.thel.base.BaseDataBean;
import com.thel.modules.main.messages.db.MessageDataBaseAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

/**
 * @author liuyun
 */
public class MsgBean extends BaseDataBean implements Cloneable, Parcelable {

    // 文字"text",
    // 语音"newVoice",
    // 图片"image",
    // 地理位置"map",
    // 密匙"secretKey",
    // 暗恋"HiddenLove",
    // 挤眼"wink",
    // 关注"follow",
    // 系统消息"system"
    // 来访 "check"
    // 名片

    /**
     * 消息类型
     */
    public String msgType = "";
    @SerializedName("sqlid")

    /**
     * 消息在数据库中存储的递增id，不是消息的包id
     */
    public int id;
    @SerializedName("id")

    /**
     * 消息的包id
     */
    public String packetId = "";

    /**
     * 消息发送时间
     */
    public Long msgTime;

    /**
     * 日期绿色title(无需存入数据库，只是为了ui显示)
     */
    public String date = "";

    /**
     * 消息内容
     */
    public String msgText = "";

    public class MsgVideoBean {
        /**
         * 视频地址
         */
        public String video;

        /**
         * 缩略图
         */
        public String thumbnail;
    }

    public class MsgGiftBean {
        public String date;
        public String noteBody;
        public String img;
        public String description;
    }

    public class MsgQuestionsBean {
        public Questions questions;

        public class Questions {
            public String question1;
            public String question2;
            public String question3;
        }
    }

    public String msgDirection = ""; // 消息的方向 "0"表示发出, "1"表示收到
    public int hadRead = 0; // 是否读过: 0新消息, 1读过了, 2读过并发了回执 默认未读

    public String msgStatus = ""; // 消息状态(发送成功,失败等)
    public String userId = ""; // 仅消息列表用，对话对象的userId
    public String userName = ""; // 仅收到消息有用 发送消息者用户名
    public String avatar = ""; // 头像地址
    public String msgLng = ""; // 地图的经度
    public String msgLat = ""; // 地图的纬度
    public String msgMapUrl = "";
    public String receivedId = "";// 我自己的id
    public String msgImage = "";
    public String msgHeight = "";
    public String msgWidth = "";
    public String voiceTime = ""; // 声音播放时间
    public String filePath = ""; // 文件存储路径
    public int hadPlayed = 0; // 声音文件是否播放 0新消息, 1听过了,2下载失败,3正在下载 默认新消息
    public int isPlaying; // 声音文件正在播放 0没播放, 1正在播放
    public static final int PLAYING = 1;
    public static final int NOT_PLAYING = 0;

    // 这两个是视频消息msgText中的json对象的key，分别是缩略图和视频地址
    public static final String VIEDO_THUMBNAIL = "thumbnail";
    public static final String VIEDO_PATH = "video";


    // new message 用于传输拼成json
    public String fromUserId = ""; // 发送方id
    public String fromAvatar = ""; // 发送方用户头像
    public String fromNickname = ""; // 发送方用户名
    public String fromMessageUser = ""; // 发送方的openfire用户名

    public String toUserId = ""; // 接收方id
    public String toAvatar = ""; // 接收方头像
    public String toUserNickname = ""; // 接收方的用户名
    public String toMessageUser = ""; // 接收方的openfire用户名
    public long commentsId;
    /**
     * 表示横屏和竖屏
     */
    public int isLandscape;

    public int isSystem = 0; // 是否是系统消息(wink,follow) 1是系统消息 0不是系统消息

    public int isWinked = 0; // 是否有挤眼消息

    public int isTop = 0;//1为置顶 0为不置顶

    public static final int IS_NORMAL_MSG = 0;
    public static final int IS_SYSTEM_MSG = 1;
    public static final int IS_WINK_MSG = 2;
    public static final int IS_FOLLOW_MSG = 3;
    public static final int IS_STRANGER_MSG = 4;
    public static final int IS_REQUEST = 5;

    public String timezone = Calendar.getInstance().getTimeZone().getID();

    public static final String MSG_DIRECTION_TYPE_INCOME = "1";
    public static final String MSG_DIRECTION_TYPE_OUTGOING = "0";
    public static final int HAD_NOT_READ = 0;
    public static final int HAD_READ = 1;
    public static final int HAD_READ_AND_SENT_RECEIPT = 2;
    public static final String MSG_STATUS_FAILED = "0";
    public static final String MSG_STATUS_SUCCEEDED = "1";
    public static final String MSG_STATUS_SENDING = "2";
    public static final String MSG_STATUS_RECEIVED = "3";
    public static final String MSG_STATUS_CONFINE = "4";

    public static final String MSG_TYPE_MAP = "map";
    public static final String MSG_TYPE_CHECK = "check";
    public static final String MSG_TYPE_IMAGE = "image";
    public static final String MSG_TYPE_WINK = "wink";
    public static final String MSG_TYPE_FOLLOW = "follow";
    public static final String MSG_TYPE_UNFOLLOW = "unfollow";
    public static final String MSG_TYPE_VOICE = "newVoice";
    public static final String MSG_TYPE_VIDEO = "video";
    public static final String MSG_TYPE_USER_CARD = "userCard";//3.0新增 用户名片
    public static final String MSG_TYPE_STRANGER_WARNING = "strangerWarning";//4.0.0新增 陌生人消息警示
    public static final String MSG_TYPE_RELATION_TIP = "relationTip";//4.0.1新增 好友状态改变以后提醒
    public static final String MSG_TYPE_MSG_HIDING = "msg_hiding";
    public static final String MSG_TYPE_MAY_CHEAT_MSG_TYPE = "MayCheatMsgType";
    public static final String MSG_TYPE_MAY_AD_MSG_TYPE = "MayAdMsgType";
    public static final String MSG_TYPE_MAY_SEXUAL_MSG_TYPE = "MaySexualMsgType";
    public static final String MSG_TYPE_MAY_HACKED_MSG_TYPE = "MayHackedMsgType";
    public static final String MSG_TYPE_AMUSEMENT_PARK_MSG_TYPE = "AmusementParkMsgType";

    //4.0.0 发送到聊天的日志类型
    public static final String MSG_TYPE_MOMENT_TEXT = "moment_text";//推荐纯文本日志
    public static final String MSG_TYPE_MOMENT_IMAGE = "moment_image";//推荐图片日志
    public static final String MSG_TYPE_MOMENT_MUSIC = "moment_music";//推荐音乐日志
    public static final String MSG_TYPE_MOMENT_VIDEO = "moment_video";//推荐视频日志
    public static final String MSG_TYPE_MOMENT_LIVE = "moment_live";//推荐视频直播
    public static final String MSG_TYPE_MOMENT_VOICE = "moment_voice";//推荐声音直播

    public static final String MSG_TYPE_MOMENT_THEME = "moment_theme";//推荐话题日志
    public static final String MSG_TYPE_MOMENT_THEMEREPLY = "moment_themereply";//推荐话题参与日志
    public static final String MSG_TYPE_MOMENT_USERCARD = "moment_usercard";//荐推荐人的日志
    public static final String MSG_TYPE_MOMENT_RECOMMEND = "moment_recommend";//荐推荐人的日志
    public static final String MSG_TYPE_MOMENT_WEB = "moment_web";//推荐网页
    public static final String MSG_TYPE_WEB = "web";//推荐网页日志类型
    public static final String MSG_TYPE_MOMENT_AD = "moment_ad";//推荐网页

    //4.0.1 消息为空
    public static final String MSG_TYPE_EMPTY = "empty";
    public static final String MSG_TYPE_MOMENT_TO_CHAT = "moment_to_chat";
    public static final String MSG_TYPE_VIP_PUSH = "vipPush";

    /**
     * 密钥功能在2.19.1版本弃用
     */
    public static final String MSG_TYPE_SECRET_KEY = "secretKey";
    public static final String MSG_TYPE_HIDDEN_LOVE = "HiddenLove";
    public static final String MSG_TYPE_MENTION = "mention";
    public static final String MSG_TYPE_EMOJI = "Emoji";
    public static final String MSG_TYPE_MOMENTS_COMMENT = "moments_comment";
    public static final String MSG_TYPE_REPLY = "reply";
    public static final String MSG_TYPE_TEXT = "text";
    public static final String MSG_TYPE_STICKER = "Sticker";
    public static final String MSG_TYPE_LIKE = "Like";
    public static final String MSG_TYPE_MATCH = "Match";// 匹配成功
    public static final String MSG_TYPE_REQUEST = "request";// 密友请求
    public static final String MSG_TYPE_SYSTEM = "system";
    public static final String MSG_TYPE_ADD_TO_BLACK_LIST = "blackadd";
    public static final String MSG_TYPE_REMOVE_FROM_BLACK_LIST = "blackremove";
    public static final String MSG_TYPE_GIFT = "gift";
    public static final String MSG_TYPE_TYPING = "typing";


    /**
     * 正在打字中消息，这个消息不存数据库
     */
    public boolean isTypingMsg = false;

    //非聊天字段
    public String messageTo = "";// 跳转类型：0打开应用 1首页 2日志发布页 3话题详情页 4系统消息聊天页 5日志发现页 6广告页面
    public String advertTitle = "";// 存储关键信息：标签名、日志id、用户id等
    public String advertUrl = "";// 广告链接地址
    public String dumpURL;
    public String extra;
    public String expiry;
    public String payload;
    public String withAll;

    public String type = ""; // msg类型，消息协议需要的字段 : 目前有两种类型—— msg（普通消息） 和 received（消息回执）
    public static final String TYPE_MSG = "msg";
    public static final String TYPE_RECEIVED = "received";

    public static final String COMMENTS_ID = "comments_id";

    public String pushId;


    public String getPacketId() {
        return packetId;
    }

    public void setPacketId(String packetId) {
        this.packetId = packetId;
    }

    public int sendTimes = 0; // 尝试发送次数

    /**
     * 对于密友请求对话，如果msgText = show，则显示入口，否则不显示入口
     */
    public static final String SHOW_CIRCLE_REQUEST_CHAT = "show";

    public String buildReceipt() {
        JSONObject result = new JSONObject();
        try {
            result.put("id", packetId);
            result.put("fromUserId", toUserId);
            result.put("toUserId", fromUserId);
            result.put("type", TYPE_RECEIVED);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result.toString();
    }

    public void fromCursor(Cursor cursor, boolean isSendingMsg) {
        id = cursor.getInt(0);
        packetId = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_PACKETID));
        msgDirection = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_MSG_DIRECTION));
        msgStatus = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_MSG_STATUS));
        msgType = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_MSG_TYPE));
        msgTime = cursor.getLong(cursor.getColumnIndex(MessageDataBaseAdapter.F_MSG_TIME));
        msgText = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_MSG_TEXT));
        avatar = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_AVATAR));
        userId = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_USERID));
        userName = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_USERNAME));
        receivedId = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_RECEIVEDID));
        voiceTime = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_VOICETIME));
        filePath = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_FILEPATH));
        msgImage = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_MSG_IMAGE));
        msgHeight = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_MSG_HEIGHT));
        msgWidth = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_MSG_WIDTH));
        msgLng = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_MSG_LNG));
        msgLat = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_MSG_LAT));
        msgMapUrl = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_MSG_MAPURL));
        fromUserId = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_FROMUSERID));
        fromAvatar = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_FROMAVATAR));
        fromNickname = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_FROMNICKNAME));
        fromMessageUser = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_FROMMESSAGEUSER));
        toUserId = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_TOUSERID));
        toAvatar = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_TOAVATAR));
        toUserNickname = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_TONICKNAME));
        toMessageUser = cursor.getString(cursor.getColumnIndex(MessageDataBaseAdapter.F_TOMESSAGEUSER));
        hadRead = cursor.getInt(cursor.getColumnIndex(MessageDataBaseAdapter.F_HADREAD));
        hadPlayed = cursor.getInt(cursor.getColumnIndex(MessageDataBaseAdapter.F_HADPLAYED));
        isSystem = cursor.getInt(cursor.getColumnIndex(MessageDataBaseAdapter.F_MSG_RESERVED_ONE));

        if (isSendingMsg) {
            sendTimes = cursor.getInt(cursor.getColumnIndex(MessageDataBaseAdapter.F_MSG_SEND_TIMES));
        }
    }

    public ContentValues getContentValues(boolean isSendingMsg) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(MessageDataBaseAdapter.F_PACKETID, packetId);
        contentValues.put(MessageDataBaseAdapter.F_MSG_DIRECTION, msgDirection);
        contentValues.put(MessageDataBaseAdapter.F_MSG_STATUS, msgStatus);
        contentValues.put(MessageDataBaseAdapter.F_MSG_TYPE, msgType);
        contentValues.put(MessageDataBaseAdapter.F_MSG_TIME, msgTime);
        contentValues.put(MessageDataBaseAdapter.F_MSG_TEXT, msgText);
        contentValues.put(MessageDataBaseAdapter.F_AVATAR, avatar);
        contentValues.put(MessageDataBaseAdapter.F_USERID, userId);
        contentValues.put(MessageDataBaseAdapter.F_USERNAME, userName);
        contentValues.put(MessageDataBaseAdapter.F_RECEIVEDID, receivedId);
        contentValues.put(MessageDataBaseAdapter.F_VOICETIME, voiceTime);
        contentValues.put(MessageDataBaseAdapter.F_FILEPATH, filePath);
        contentValues.put(MessageDataBaseAdapter.F_MSG_IMAGE, msgImage);
        contentValues.put(MessageDataBaseAdapter.F_MSG_HEIGHT, msgHeight);
        contentValues.put(MessageDataBaseAdapter.F_MSG_WIDTH, msgWidth);
        contentValues.put(MessageDataBaseAdapter.F_MSG_LNG, msgLng);
        contentValues.put(MessageDataBaseAdapter.F_MSG_LAT, msgLat);
        contentValues.put(MessageDataBaseAdapter.F_MSG_MAPURL, msgMapUrl);
        contentValues.put(MessageDataBaseAdapter.F_FROMUSERID, fromUserId);
        contentValues.put(MessageDataBaseAdapter.F_FROMAVATAR, fromAvatar);
        contentValues.put(MessageDataBaseAdapter.F_FROMNICKNAME, fromNickname);
        contentValues.put(MessageDataBaseAdapter.F_FROMMESSAGEUSER, fromMessageUser);
        contentValues.put(MessageDataBaseAdapter.F_TOUSERID, toUserId);
        contentValues.put(MessageDataBaseAdapter.F_TOAVATAR, toAvatar);
        contentValues.put(MessageDataBaseAdapter.F_TONICKNAME, toUserNickname);
        contentValues.put(MessageDataBaseAdapter.F_TOMESSAGEUSER, toMessageUser);
        contentValues.put(MessageDataBaseAdapter.F_HADREAD, hadRead);
        contentValues.put(MessageDataBaseAdapter.F_HADPLAYED, hadPlayed);
        contentValues.put(MessageDataBaseAdapter.F_MSG_RESERVED_ONE, isSystem);

        if (isSendingMsg) {
            contentValues.put(MessageDataBaseAdapter.F_MSG_SEND_TIMES, sendTimes);
        }

        return contentValues;
    }

    @Override
    public MsgBean clone() {
        MsgBean obj = null;
        try {
            obj = (MsgBean) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return obj;
    }

    @Override public int describeContents() {
        return 0;
    }

    @Override public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.msgType);
        dest.writeInt(this.id);
        dest.writeString(this.packetId);
        dest.writeValue(this.msgTime);
        dest.writeString(this.date);
        dest.writeString(this.msgText);
        dest.writeString(this.msgDirection);
        dest.writeInt(this.hadRead);
        dest.writeString(this.msgStatus);
        dest.writeString(this.userId);
        dest.writeString(this.userName);
        dest.writeString(this.avatar);
        dest.writeString(this.msgLng);
        dest.writeString(this.msgLat);
        dest.writeString(this.msgMapUrl);
        dest.writeString(this.receivedId);
        dest.writeString(this.msgImage);
        dest.writeString(this.msgHeight);
        dest.writeString(this.msgWidth);
        dest.writeString(this.voiceTime);
        dest.writeString(this.filePath);
        dest.writeInt(this.hadPlayed);
        dest.writeInt(this.isPlaying);
        dest.writeString(this.fromUserId);
        dest.writeString(this.fromAvatar);
        dest.writeString(this.fromNickname);
        dest.writeString(this.fromMessageUser);
        dest.writeString(this.toUserId);
        dest.writeString(this.toAvatar);
        dest.writeString(this.toUserNickname);
        dest.writeString(this.toMessageUser);
        dest.writeInt(this.isSystem);
        dest.writeInt(this.isWinked);
        dest.writeInt(this.isTop);
        dest.writeString(this.timezone);
        dest.writeByte(this.isTypingMsg ? (byte) 1 : (byte) 0);
        dest.writeString(this.messageTo);
        dest.writeString(this.advertTitle);
        dest.writeString(this.advertUrl);
        dest.writeString(this.type);
        dest.writeInt(this.sendTimes);
    }

    public MsgBean() {
    }

    protected MsgBean(Parcel in) {
        this.msgType = in.readString();
        this.id = in.readInt();
        this.packetId = in.readString();
        this.msgTime = (Long) in.readValue(Long.class.getClassLoader());
        this.date = in.readString();
        this.msgText = in.readString();
        this.msgDirection = in.readString();
        this.hadRead = in.readInt();
        this.msgStatus = in.readString();
        this.userId = in.readString();
        this.userName = in.readString();
        this.avatar = in.readString();
        this.msgLng = in.readString();
        this.msgLat = in.readString();
        this.msgMapUrl = in.readString();
        this.receivedId = in.readString();
        this.msgImage = in.readString();
        this.msgHeight = in.readString();
        this.msgWidth = in.readString();
        this.voiceTime = in.readString();
        this.filePath = in.readString();
        this.hadPlayed = in.readInt();
        this.isPlaying = in.readInt();
        this.fromUserId = in.readString();
        this.fromAvatar = in.readString();
        this.fromNickname = in.readString();
        this.fromMessageUser = in.readString();
        this.toUserId = in.readString();
        this.toAvatar = in.readString();
        this.toUserNickname = in.readString();
        this.toMessageUser = in.readString();
        this.isSystem = in.readInt();
        this.isWinked = in.readInt();
        this.isTop = in.readInt();
        this.timezone = in.readString();
        this.isTypingMsg = in.readByte() != 0;
        this.messageTo = in.readString();
        this.advertTitle = in.readString();
        this.advertUrl = in.readString();
        this.type = in.readString();
        this.sendTimes = in.readInt();
    }

    public static final Parcelable.Creator<MsgBean> CREATOR = new Parcelable.Creator<MsgBean>() {
        @Override public MsgBean createFromParcel(Parcel source) {
            return new MsgBean(source);
        }

        @Override public MsgBean[] newArray(int size) {
            return new MsgBean[size];
        }
    };

    @Override public String toString() {
        return "MsgBean{" +
                "msgType='" + msgType + '\'' +
                ", id=" + id +
                ", packetId='" + packetId + '\'' +
                ", msgTime=" + msgTime +
                ", date='" + date + '\'' +
                ", msgText='" + msgText + '\'' +
                ", msgDirection='" + msgDirection + '\'' +
                ", hadRead=" + hadRead +
                ", msgStatus='" + msgStatus + '\'' +
                ", userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                ", avatar='" + avatar + '\'' +
                ", msgLng='" + msgLng + '\'' +
                ", msgLat='" + msgLat + '\'' +
                ", msgMapUrl='" + msgMapUrl + '\'' +
                ", receivedId='" + receivedId + '\'' +
                ", msgImage='" + msgImage + '\'' +
                ", msgHeight='" + msgHeight + '\'' +
                ", msgWidth='" + msgWidth + '\'' +
                ", voiceTime='" + voiceTime + '\'' +
                ", filePath='" + filePath + '\'' +
                ", hadPlayed=" + hadPlayed +
                ", isPlaying=" + isPlaying +
                ", fromUserId='" + fromUserId + '\'' +
                ", fromAvatar='" + fromAvatar + '\'' +
                ", fromNickname='" + fromNickname + '\'' +
                ", fromMessageUser='" + fromMessageUser + '\'' +
                ", toUserId='" + toUserId + '\'' +
                ", toAvatar='" + toAvatar + '\'' +
                ", toUserNickname='" + toUserNickname + '\'' +
                ", toMessageUser='" + toMessageUser + '\'' +
                ", isSystem=" + isSystem +
                ", isWinked=" + isWinked +
                ", isTop=" + isTop +
                ", timezone='" + timezone + '\'' +
                ", isTypingMsg=" + isTypingMsg +
                ", messageTo='" + messageTo + '\'' +
                ", advertTitle='" + advertTitle + '\'' +
                ", advertUrl='" + advertUrl + '\'' +
                ", logType='" + type + '\'' +
                ", sendTimes=" + sendTimes +
                '}';
    }
}
