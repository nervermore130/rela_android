package com.thel.modules.live.liveBigGiftAnimLayout.anim;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.Point;
import android.os.Handler;
import android.os.Looper;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.thel.modules.live.liveBigGiftAnimLayout.LiveBigAnimUtils;
import com.thel.modules.live.liveBigGiftAnimLayout.FrameAnimAssetSurfaceView;
import com.thel.utils.Utils;

import java.util.Random;

/**
 * Created by the L on 2017/1/11.
 */

public class FirecrackerAnimator {
    private final Context mContext;
    private final int mDuration;
    private final Random mRandom;
    private final Handler mHandler;
    //    private final int res_lantern;
    //    private final int res_star;
    //    private final int res_money;
    //    private final int[] res_fire;
    private final String res_lantern;
    private final String res_star;
    private final String res_money;
    private final String[] res_fire;
    private final int fre_drop_money;
    private final int addDropDuration;
    private int current_fre;
    private int blinkCount;
    private boolean isPlaying;
    private int mWidth;
    private int mHeight;
    private int lantern_drop_duration;
    private int rate_fire;
    private int differMoneywCount;
    private int minMoneyCount;
    private long rate_drop_money;
    private int minMoneyWidth;
    private int differMoneyWidth;
    private int minDropDuration;
    private boolean blinkStar;
    private long rate_blink_star;
    private int differDropDuration;
    private final String foldPath;

    public FirecrackerAnimator(Context context, int duration) {
        mContext = context;
        mDuration = duration;
        mRandom = new Random();
        mHandler = new Handler(Looper.getMainLooper());
        //        res_lantern = R.drawable.firecracker_lantern;
        //        res_star = R.drawable.firecracker_shining;
        //        res_money = R.drawable.firecracker_money;
        //        res_fire = new int[]{R.drawable.firecracker_1, R.drawable.firecracker_2, R.drawable.firecracker_3, R.drawable.firecracker_4, R.drawable.firecracker_5, R.drawable.firecracker_6, R.drawable.firecracker_7, R.drawable.firecracker_8};
        foldPath = "anim/firecracker";
        res_lantern = "firecracker_lantern";
        res_star = "firecracker_shining";
        res_money = "firecracker_money";
        res_fire = new String[]{"firecracker_1", "firecracker_2", "firecracker_3", "firecracker_4", "firecracker_5", "firecracker_6", "firecracker_7", "firecracker_8"};
        lantern_drop_duration = 300;
        blinkCount = 0;
        rate_blink_star = 600;
        rate_fire = 150;
        minMoneyCount = 4;
        differMoneywCount = 5;
        rate_drop_money = 300;
        minMoneyWidth = Utils.dip2px(mContext, 8);
        differMoneyWidth = Utils.dip2px(mContext, 25);
        minDropDuration = 800;
        differDropDuration = 700;
        addDropDuration = 150;
        fre_drop_money = 4;
        current_fre = 0;

    }

    public int start(final ViewGroup parent) {
        isPlaying = true;
        mWidth = parent.getMeasuredWidth();
        mHeight = parent.getMeasuredHeight();
        if (mWidth == 0 || mHeight == 0) {
            return 0;
        }
        final RelativeLayout background = new RelativeLayout(mContext);
        background.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
        parent.addView(background);
        setBackground(background);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                isPlaying = false;
                background.clearAnimation();
                parent.removeView(background);
            }
        }, mDuration);
        return mDuration;
    }

    private void setBackground(RelativeLayout background) {
        dropLantern(background);
    }

    private void dropLantern(final RelativeLayout background) {
        final ImageView img_lantern = new ImageView(mContext);
        background.addView(img_lantern);
        RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams) img_lantern.getLayoutParams();
        final int w = Math.min(mWidth, mHeight);
        final int h = w * 287 / 621;
        param.width = w;
        param.height = h;
        param.addRule(RelativeLayout.CENTER_HORIZONTAL);
        param.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        // img_lantern.setImageResource(res_lantern);
        LiveBigAnimUtils.setAssetImage(img_lantern, foldPath, res_lantern);
        final int start = -h;
        ObjectAnimator animator = ObjectAnimator.ofFloat(img_lantern, View.TRANSLATION_Y, start, 0);
        animator.setDuration(lantern_drop_duration);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                blinkStar(background, img_lantern);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        animator.start();
    }

    private void blinkStar(final RelativeLayout background, final ImageView img_lantern) {
        final ImageView img_star = new ImageView(mContext);
        background.addView(img_star);
        RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams) img_star.getLayoutParams();
        final int w = Math.min(mWidth, mHeight);
        final int h = w * 287 / 621;
        param.width = w;
        param.height = h;
        param.addRule(RelativeLayout.CENTER_HORIZONTAL);
        param.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        //img_star.setImageResource(res_star);
        LiveBigAnimUtils.setAssetImage(img_star, foldPath, res_star);
        blinkCount = 0;
        ObjectAnimator animator = ObjectAnimator.ofFloat(img_star, View.ALPHA, 0, 1, 0);
        animator.setDuration(rate_blink_star);
        animator.setRepeatMode(ValueAnimator.RESTART);
        animator.setRepeatCount(ValueAnimator.INFINITE);
        blinkStar = true;
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {
                blinkCount++;
                if (blinkCount == 2) {
                    addFire(background, img_lantern);
                }
                if (!blinkStar) {
                    animation.cancel();
                }
            }
        });
        animator.start();
    }

    private void addFire(final RelativeLayout background, final ImageView img_lantern) {
        final FrameAnimAssetSurfaceView img_fire = new FrameAnimAssetSurfaceView(mContext);
        background.addView(img_fire);
        RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams) img_fire.getLayoutParams();
        final float scale = Math.min((float) mWidth / 621, (float) mHeight / 1105);
        final int w = (int) (621 * scale);
        final int h = (int) (1105 * scale);
        param.width = w;
        param.height = h;
        param.addRule(RelativeLayout.CENTER_HORIZONTAL);
        param.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        img_fire.initView(background, foldPath, res_fire, rate_fire, true);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                img_fire.start();
            }
        }, 100);
        img_fire.setOnAnimListener(new FrameAnimAssetSurfaceView.AnimListener() {
            @Override
            public void animFinished(ViewGroup paant, SurfaceView surfaceView) {
                background.removeView(img_fire);
                dropMoney(background, img_lantern);
            }

            @Override
            public void timeFinished(ViewGroup parent, SurfaceView surfaceView, boolean shouldFinished) {
                background.removeView(img_fire);
                dropMoney(background, img_lantern);
            }


            @Override
            public void playIndex(int index) {

            }
        });
    }

    private void dropMoney(final RelativeLayout background, final ImageView img_lantern) {
        current_fre = 0;
        background.post(new Runnable() {
            @Override
            public void run() {
                final int moneyCount = mRandom.nextInt(differMoneywCount) + minMoneyCount;
                for (int i = 0; i < moneyCount; i++) {
                    dropOneMoney(background);
                }
                current_fre++;
                if (isPlaying && current_fre < fre_drop_money) {
                    background.postDelayed(this, rate_drop_money);
                }
                if (current_fre == fre_drop_money - 1) {
                    blinkStar = false;
                    removeLantern(background, img_lantern);
                }
            }
        });
    }

    private void removeLantern(final RelativeLayout background, final ImageView img_lantern) {
        final int h = -img_lantern.getMeasuredHeight();
        img_lantern.animate().translationYBy(h).setDuration(lantern_drop_duration).setInterpolator(new LinearInterpolator()).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                background.removeView(img_lantern);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    private void dropOneMoney(final ViewGroup background) {
        if (!isPlaying) {
            return;
        }
        final ImageView img_snow = new ImageView(mContext);
        background.addView(img_snow);
        int w = minMoneyWidth + mRandom.nextInt(differMoneyWidth);
        int h = w;
        //  img_snow.setImageResource(res_money);
        LiveBigAnimUtils.setAssetImage(img_snow, foldPath, res_money);
        img_snow.setLayoutParams(new RelativeLayout.LayoutParams(w, h));
        Point startPoint = new Point(mRandom.nextInt(mWidth), 0 - h);
        Point endPoint = new Point(startPoint.x, mHeight);
        Path path = new Path();
        path.moveTo(startPoint.x, startPoint.y);
        path.lineTo(endPoint.x, endPoint.y);
        final int dropDuration = minDropDuration + mRandom.nextInt(differDropDuration) + current_fre * addDropDuration;
        FloatAnimation anim = new FloatAnimation(path, background, img_snow, dropDuration);
        anim.setDuration(dropDuration);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                background.post(new Runnable() {
                    @Override
                    public void run() {
                        background.removeView(img_snow);
                    }
                });
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        img_snow.startAnimation(anim);
    }

    class FloatAnimation extends Animation {
        private final int mDropDuration;
        private PathMeasure mPm;
        private View mView;
        private float mDistance;

        public FloatAnimation(Path path, View parent, View child, int dropDuration) {
            mPm = new PathMeasure(path, false);
            mDistance = mPm.getLength();
            mView = child;
            parent.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            mDropDuration = dropDuration;
        }

        @Override
        protected void applyTransformation(float factor, Transformation transformation) {
            Matrix matrix = transformation.getMatrix();
            mPm.getMatrix(mDistance * factor, matrix, PathMeasure.POSITION_MATRIX_FLAG);
        }
    }
}
