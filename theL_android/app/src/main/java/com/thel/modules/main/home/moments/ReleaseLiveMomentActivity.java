package com.thel.modules.main.home.moments;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;

import androidx.annotation.Nullable;

import com.google.android.material.tabs.TabLayout;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.modules.live.bean.LiveRoomBean;
import com.thel.modules.live.stream.RtcPreviewManager;
import com.thel.modules.live.stream.RtcStreamManager;
import com.thel.modules.live.stream.TuSdkPreviewManager;
import com.thel.tusdk.plain.CameraConfig;
import com.thel.ui.widget.IndicatorDrawable;
import com.thel.ui.widget.NoScrollViewPager;
import com.thel.utils.L;
import com.thel.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

import javax.microedition.khronos.egl.EGLContext;

/**
 * 4.8.0 区分声音和视频直播及多人连麦
 * Created by lingwei on 2018/5/4.
 */

public class ReleaseLiveMomentActivity extends BaseActivity implements View.OnClickListener {

    public static final String TAG = "ReleaseLiveMoment";

    private int current_tab;
    private List<Fragment> fragmentList = new ArrayList<>();
    private List<String> titleList = new ArrayList<>();
    private LiveTypeMomentAdapter adapter;
    private NoScrollViewPager viewpager;
    private TabLayout tablayout;
    private EditVideoLiveMomentFragment videoContentFragment;
    private EditVoiceLiveMomentFragment voiceContentFragment;
    private EditVoiceLiveMomentFragment hotChatFragment;

    private RtcPreviewManager tuSdkPreviewManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //VmPolicy方式处理FileUriExposedException
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
        }

        setContentView(R.layout.release_moment_select_typelive);

        findViewById();
        setListener();

        GLSurfaceView cameraPreviewFrameView = findViewById(R.id.cameraPreview_surfaceView);

        tuSdkPreviewManager = new RtcPreviewManager(new CameraConfig(), cameraPreviewFrameView);

        tuSdkPreviewManager.initStreamingManager();

    }

    @Override
    protected void onResume() {
        super.onResume();
        tuSdkPreviewManager.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        tuSdkPreviewManager.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        tuSdkPreviewManager.onDestroy();
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return true;
    }

    public boolean switchCameraOrienation() {
        return tuSdkPreviewManager.switchCameraOrienation();
    }

    public void adjustBeauty(String key, float progress) {
        tuSdkPreviewManager.adjustBeautyParams(key, progress);
    }

    private void setListener() {
        tablayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                current_tab = tab.getPosition();

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                //添上这个函数，下面有函数的定义
                hideIputKeyboard();

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public void hideIputKeyboard() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                InputMethodManager mInputKeyBoard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (getCurrentFocus() != null) {
                    mInputKeyBoard.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                }
            }
        });
    }

    private void findViewById() {
        tablayout = findViewById(R.id.tablayout);
        findViewById(R.id.lin_done).setOnClickListener(this);
        viewpager = findViewById(R.id.viewpager);

        setTabs();

    }

    public void setViewpagerScroll(boolean isScroll) {
        viewpager.setScanScroll(isScroll);
    }

    private void setTabs() {
        addFragment();
        adapter = new LiveTypeMomentAdapter(getSupportFragmentManager(), fragmentList, titleList);
        viewpager.setAdapter(adapter);
        final int size = titleList.size();
        for (int i = 0; i < size; i++) {
            tablayout.addTab(tablayout.newTab());
        }
        View tabStripView = tablayout.getChildAt(0);
        tabStripView.setBackground(new IndicatorDrawable(tabStripView, ContextCompat.getColor(TheLApp.context, R.color.tab_normal)));
        tablayout.setupWithViewPager(viewpager);
        viewpager.setOffscreenPageLimit(2);
    }

    private void addFragment() {
        fragmentList.clear();
        titleList.clear();
        videoContentFragment = EditVideoLiveMomentFragment.getInstance();
        voiceContentFragment = EditVoiceLiveMomentFragment.getInstance(LiveRoomBean.SINGLE_LIVE);
        hotChatFragment = EditVoiceLiveMomentFragment.getInstance(LiveRoomBean.MULTI_LIVE);
        fragmentList.add(videoContentFragment);
        titleList.add(StringUtils.getString(R.string.live));
        fragmentList.add(voiceContentFragment);
        titleList.add(StringUtils.getString(R.string.radio_station));
        fragmentList.add(hotChatFragment);
        titleList.add(StringUtils.getString(R.string.hot_chat));

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lin_done:
                close();
                break;

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        close();
    }

    private void close() {
        if (0 == current_tab) {
            videoContentFragment.close();
        } else if (1 == current_tab) {
            voiceContentFragment.close();
        } else {
            hotChatFragment.close();
        }
    }

    /***
     * 内部类
     ***/

    class LiveTypeMomentAdapter extends FragmentStatePagerAdapter {

        private final List<Fragment> content;
        private final List<String> titleList;

        public LiveTypeMomentAdapter(FragmentManager fragmentManager, List<Fragment> fragmentList, List<String> titleList) {
            super(fragmentManager);
            this.content = fragmentList;
            this.titleList = titleList;
        }

        @Override
        public Fragment getItem(int i) {
            return content.get(i);
        }

        @Override
        public int getCount() {
            return content.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (titleList != null) {
                return titleList.get(position);
            }
            return "";
        }
    }
}
