package com.thel.modules.live.agora;

import android.view.SurfaceView;

public interface AGEventHandler {

    /**
     * 连麦或pk成功是返回的显示画面的view
     *
     * @param surfaceView
     */
    void onRemoteViewCreated(SurfaceView surfaceView);

    /**
     * 生网sdk的报错信息
     *
     * @param error
     */
    void onError(int error);
}
