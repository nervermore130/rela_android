package com.thel.modules.live.in;

import com.thel.modules.live.bean.LivePkHangupBean;
import com.thel.modules.live.bean.LivePkInitBean;
import com.thel.modules.live.bean.LivePkRequestNoticeBean;
import com.thel.modules.live.bean.LivePkResponseNoticeBean;
import com.thel.modules.live.bean.LivePkStartNoticeBean;
import com.thel.modules.live.bean.LivePkSummaryNoticeBean;
import com.thel.modules.live.bean.LiveRoomMsgConnectMicBean;
import com.thel.modules.live.bean.SoftGiftBean;
import com.thel.modules.live.surface.show.LiveShowObserver;
import com.thel.modules.live.surface.watch.LiveWatchObserver;
import com.thel.modules.live.utils.LiveStatus;

/**
 * @author waiarl
 * @date 2017/11/5
 */

public interface LiveShowCaptureStreamIn {
    void bindObserver(LiveWatchObserver observer);

    /**
     * 关闭推流
     */
    void finishStream();

    /**
     * 切换摄像头
     */
    void switchCamera();

    void adjustBeauty(String key, float progress);

    /**********************************************一下为直播PK的方法***********************************************************/
    /**
     * pk发起方的回复通知
     *
     * @param pkResponseNoticeBean
     */
    void showPkResponse(LivePkResponseNoticeBean pkResponseNoticeBean);

    /**
     * 直播Pk开始
     *
     * @param pkStartNoticeBean
     */
    void showPkStart(LivePkStartNoticeBean pkStartNoticeBean);

    /**
     * 直播PK取消
     *
     * @param pkCancelPayload
     */
    void showPkCancel(String pkCancelPayload);

    /**
     * 直播PK挂断
     *
     * @param pkHangupBean
     */
    void showPkHangup(LivePkHangupBean pkHangupBean);

    /**
     * 直播PK总结
     *
     * @param pkSummaryNoticeBean
     */
    void showPkSummary(LivePkSummaryNoticeBean pkSummaryNoticeBean);

    /**
     * 直播PK关闭
     *
     * @param pkStopPayload
     */
    void showPkStop(String pkStopPayload);

    /**
     * 结束PK
     */
    void finishPk();

    /**
     * 对方发起的PK申请
     *
     * @param pkRequestNoticeBean
     */
    void showPkRequest(LivePkRequestNoticeBean pkRequestNoticeBean);

    /**
     * 显示直播stream
     */
    void showPkStream();

    /**
     * pk断线重连
     *
     * @param livePkInitBean
     */
    void showPkInitStream(LivePkInitBean livePkInitBean);


    /**********************************************一上为直播PK的方法***********************************************************/

    void audienceLinkMicResponse();

    /**
     * 发起连麦请求
     *
     * @param liveRoomMsgConnectMicBean
     */
    void linkMicRequest(LiveRoomMsgConnectMicBean liveRoomMsgConnectMicBean);

    /**
     * 连麦响应
     *
     * @param liveRoomMsgConnectMicBean
     */
    void linkMicResponse(LiveRoomMsgConnectMicBean liveRoomMsgConnectMicBean);

    /**
     * 开始连麦
     */
    void linkMickStart();

    /**
     * 对方挂断连麦
     */
    void linkMicHangup();

    /**
     * 对方挂断连麦
     */
    void linkMicHangupByOwn();

    /**
     * 停止连麦
     */
    void linkMicStop();

    void muteLocalAudioStream(boolean mute);

    void playEffect(int position);//播放音效

    LiveStatus getListStatus();

    /**
     * 切换贴纸
     *
     * @param giftBean
     */
    void switchStricker(SoftGiftBean giftBean);

    /**
     * 同意观众连麦
     */
    void acceptAudienceLinkMic();
}
