package com.thel.modules.live.surface.show;

import android.net.Uri;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.Message;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.postprocessors.IterativeBoxBlurPostProcessor;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.netease.LDNetDiagnoService.LDNetSocket;
import com.qiniu.pili.droid.streaming.AudioSourceCallback;
import com.thel.BuildConfig;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseFragment;
import com.thel.bean.LiveInfoLogBean;
import com.thel.constants.TheLConstants;
import com.thel.manager.CDNBalanceManager;
import com.thel.modules.live.agora.OnJoinChannelListener;
import com.thel.modules.live.agora.RtcEngineHandler;
import com.thel.modules.live.bean.AgoraBean;
import com.thel.modules.live.bean.LivePkHangupBean;
import com.thel.modules.live.bean.LivePkInitBean;
import com.thel.modules.live.bean.LivePkRequestNoticeBean;
import com.thel.modules.live.bean.LivePkResponseNoticeBean;
import com.thel.modules.live.bean.LivePkStartNoticeBean;
import com.thel.modules.live.bean.LivePkSummaryNoticeBean;
import com.thel.modules.live.bean.LiveRoomBean;
import com.thel.modules.live.bean.LiveRoomMsgConnectMicBean;
import com.thel.modules.live.bean.SoftGiftBean;
import com.thel.modules.live.in.LiveShowCaptureStreamIn;
import com.thel.modules.live.stream.RtcStreamManager;
import com.thel.modules.live.stream.TuSdkStreamManager;
import com.thel.modules.live.surface.watch.LiveWatchObserver;
import com.thel.modules.live.utils.LiveGIOPush;
import com.thel.modules.live.utils.LiveStatus;
import com.thel.modules.live.utils.LiveUtils;
import com.thel.modules.live.utils.NetSpeedTestUtils;
import com.thel.modules.live.view.SoundSurfaceView;
import com.thel.tusdk.plain.CameraConfig;
import com.thel.utils.AppInit;
import com.thel.utils.DeviceUtils;
import com.thel.utils.GsonUtils;
import com.thel.utils.ImageUtils;
import com.thel.utils.L;
import com.thel.utils.NetworkUtils;
import com.thel.utils.PhoneUtils;
import com.thel.utils.ScreenUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.ToastUtils;
import com.thel.utils.UmentPushUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;

import java.nio.ByteBuffer;

import javax.microedition.khronos.egl.EGLContext;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.agora.rtc.IRtcEngineEventHandler;
import jp.wasabeef.glide.transformations.BlurTransformation;

import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_ANCHOR_LIVE_CONNECTING;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_ANCHOR_LIVE_CONNECT_SUCCESS;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_ANCHOR_PROGRESSBAR_GONE;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_NO_FACE;
import static com.thel.modules.live.agora.RtcEngineHandler.TYPE_AUDIENCE_LINK_MIC;
import static java.lang.Math.abs;

/**
 * @author waiarl
 * @date 2017/11/3
 */

public class LiveShowVideoStreamFragment extends BaseFragment implements LiveShowCaptureStreamIn {
    private static final String TAG = "LiveShowVideoStream";

    private LiveRoomBean mLiveRoomBean;

    private LiveWatchObserver mLiveWatchObserver;

    //是否被创建
    private boolean isCreate = false;

    private CameraConfig mCameraConfigBean;

    private RtcStreamManager mRtcStreamManager;

    private SurfaceView mRemoteView;

    private int type = RtcEngineHandler.TYPE_LINK_MIC;

    private boolean isPkRequest = false;

    private boolean isLinkMicRequest = false;

    private int cameraId = 0;

    private boolean isBeauty = true;

    private TuSdkStreamManager mTuSdkStreamManager;

    private LiveStatus mLiveStatus = LiveStatus.LIVE_NORMAL;

    @BindView(R.id.container)
    RelativeLayout container;

    @BindView(R.id.glsurface)
    GLSurfaceView mGlSurfaceView;

    @BindView(R.id.img_background)
    ImageView img_background;

    @BindView(R.id.img_background_top)
    ImageView img_background_top;

    private SoundSurfaceView sound_view;

    private SimpleDraweeView img_bg;

    public static LiveShowVideoStreamFragment getInstance(Bundle bundle) {
        LiveShowVideoStreamFragment instance = new LiveShowVideoStreamFragment();
        instance.setArguments(bundle);
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Bundle bundle = getArguments();
        mCameraConfigBean = (CameraConfig) bundle.getSerializable(TheLConstants.BUNDLE_KEY_LIVE_ROOM_CAMERA_CONFIG);
        mLiveRoomBean = (LiveRoomBean) bundle.getSerializable(TheLConstants.BUNDLE_KEY_LIVE_ROOM);

        L.d(TAG, " mLiveRoomBean : " + mLiveRoomBean);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_live_show_capture_video, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        ButterKnife.bind(this, view);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        UmentPushUtils.onMsgEvent(TheLApp.context, "anchor_first_push");

        initTuSdkStream();

        isCreate = true;
        if (mLiveWatchObserver != null) {
            mLiveWatchObserver.bindLiveShowCaptureStreamIn(this);
        }
        setListener();

    }

    private void initTuSdkStream() {

        String rtmpUrl;

        if (CDNBalanceManager.getInstance().isOtherCDN(mLiveRoomBean.pubUrl)) {

            rtmpUrl = mLiveRoomBean.pubUrl;

            LiveInfoLogBean.getInstance().getLivePlayAnalytics().reason = LiveInfoLogBean.CHANGE_CDN_USE_OTHER;

        } else {

            if (mLiveRoomBean.forceServerLiveUrl == 0) {

                rtmpUrl = getPubUrl();

                if (rtmpUrl == null || rtmpUrl.length() == 0) {

                    rtmpUrl = mLiveRoomBean.pubUrl;

                }
            } else {

                rtmpUrl = mLiveRoomBean.pubUrl;

            }

        }

        LiveInfoLogBean.getInstance().getLivePlayAnalytics().roomId = mLiveRoomBean.liveId;

        L.d(TAG, " rtmpUrl : " + rtmpUrl);

        NetSpeedTestUtils netSpeedUtils = new NetSpeedTestUtils();

        netSpeedUtils.testIp(CDNBalanceManager.getInstance().getIpFromUrl(rtmpUrl), LDNetSocket.PORT, "livePushAuto");

        mTuSdkStreamManager = new TuSdkStreamManager(mCameraConfigBean == null ? new CameraConfig() : mCameraConfigBean, mGlSurfaceView, rtmpUrl, mLiveRoomBean.pubUrl);

        mTuSdkStreamManager.setOnTuSdkListener(mTuSdkListener);

        if (LiveRoomBean.TYPE_VOICE == mLiveRoomBean.audioType) {
            mTuSdkStreamManager.setAudioSourceCallback(new AudioSourceCallback() {
                @Override
                public void onAudioSourceAvailable(ByteBuffer byteBuffer, int i, long l, boolean b) {

                    showSoundView(byteBuffer);
                }
            });
            initBackgroundView();

            initSoundView();

        }

        mTuSdkStreamManager.initStreamingManager();
        mTuSdkStreamManager.setOnStrickerListener(mOnStrickerListener);

        setLiveInfoLog(rtmpUrl, TheLConstants.LiveInfoLogConstants.TYPE_FIRST_PUSH);

    }

    private void initBackgroundView() {
        if (img_bg == null && container != null) {
            img_bg = new SimpleDraweeView(getContext());
            container.addView(img_bg, RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            setSimpleBlur(img_bg, mLiveRoomBean.imageUrl, AppInit.displayMetrics.widthPixels, AppInit.displayMetrics.heightPixels);
            ImageView imageView = new ImageView(getContext());
            imageView.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
            imageView.setImageResource(R.color.black_transparent_50);
            container.addView(imageView);
        }

    }

    private void initSoundView() {
        if (sound_view == null && mLiveRoomBean.isMulti == LiveRoomBean.SINGLE_LIVE) {
            sound_view = new SoundSurfaceView(getActivity());
            container.addView(sound_view, RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            sound_view.start();
        }
    }

    private void showSoundView(ByteBuffer byteBuffer) {
        if (sound_view == null) {
            return;
        }
        // final byte[] bt = new byte[BUFFER_SIZE];
        //byteBuffer.get(bt);
        final byte[] bt = new byte[byteBuffer.remaining()];
        byteBuffer.get(bt);
        //final byte[] bt = byteBuffer.array();
        final short[] shorts = Utils.toShortArray(bt);
        final int length = shorts.length;
        long summer = 0;
        for (int i = 0; i < length; i++) {
            final short ct = shorts[i];
            summer += ct * ct;
        }
        final float mean = summer / (float) length;
        final float volume = (float) (Math.sqrt(mean) / 30f);
        L.i(TAG, "volume=" + volume);
        sound_view.initView(volume);
    }

//    private void showSoundView(byte[] bytes) {
//        final int length = bytes.length;
//        long sum = 0;
//        for (int i = 0; i < length; i++) {
//            sum += abs(bytes[i]);
//        }
//        final double volume = sum / (double) length / 1.28 * 8;
////        L.i(TAG, "voluem=" + volume);
//        if (sound_view != null) {
//            sound_view.initView(volume);
//        }
//    }

    private void setSimpleBlur(SimpleDraweeView simpleDraweeView, String url, int width, int height) {
        simpleDraweeView.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(url, width, height))).setPostprocessor(new IterativeBoxBlurPostProcessor(10, 10)).build()).setAutoPlayAnimations(true).build());
    }


    private void initRtcEngine(final String appId, final String token, final String channelId, String pubUrl, final int uid) {

        L.d(TAG, " initRtcEngine : ");

        mRtcStreamManager = new RtcStreamManager(getActivity(), RtcEngineHandler.LIVE_TYPE_VIDEO, appId);

        mRtcStreamManager.setType(type);

        mRtcStreamManager.setOnJoinChannelSuccessListener(mOnJoinChannelSuccessListener);

        mRtcStreamManager.init(token, channelId, pubUrl, uid, mLiveRoomBean != null ? mLiveRoomBean.liveId : "");

        mRtcStreamManager.joinChannel();

    }

    private TuSdkStreamManager.OnStrickerListener mOnStrickerListener = new TuSdkStreamManager.OnStrickerListener() {

        @Override
        public void onStrickerDownloadFailed(SoftGiftBean stickBean) {
            L.d(TAG, "onStrickerDownloadFailed " + Thread.currentThread().getName() + " " + stickBean);
            mLiveWatchObserver.post(new Runnable() {
                @Override
                public void run() {
                    ToastUtils.showToastShort(TheLApp.getContext(), "您当前的网络状况不佳，" + stickBean.title +" 礼物效果无法正常播放，请尝试切换网络并重启APP");
                }
            });
            mLiveWatchObserver.sendStickFail();//blocking
        }
    };

    private OnJoinChannelListener mOnJoinChannelSuccessListener = new OnJoinChannelListener() {

        @Override
        public void onJoinChannelSuccess() {

            L.d(TAG, " onJoinChannelSuccess : ");

            connectChat();
        }

        @Override
        public void onLeaveChannelSuccess() {

            L.d(TAG, " onLeaveChannelSuccess : ");

            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        L.d(TAG, " container : " + container);

                        if (container != null) {

                            if (mRemoteView != null) {
                                container.removeView(mRemoteView);
                                mRemoteView = null;
                            }

                            L.d(TAG, " mGlSurfaceView : " + mGlSurfaceView);

                            RelativeLayout.LayoutParams cameraViewParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

                            mGlSurfaceView.setLayoutParams(cameraViewParams);

                        }

                        if (mTuSdkStreamManager != null) {
                            mTuSdkStreamManager.startStreaming();
                        }
                    }
                });

            }

        }

        @Override
        public void onUserJoined(int uid, SurfaceView surfaceView) {

            L.d(TAG, " onUserJoined uid : " + uid);

            L.d(TAG, " onUserJoined mRtcStreamManager : " + mRtcStreamManager);

            mRemoteView = surfaceView;

            if (mRtcStreamManager != null) {

                setPKSize();
            }
        }

        @Override
        public void onUserOffline(int uid) {

            L.d(TAG, " onUserOffline uid : " + uid);

        }

        @Override
        public void onAudioVolumeIndication(IRtcEngineEventHandler.AudioVolumeInfo[] speakers, int totalVolume) {

        }

        @Override
        public void onError(int error) {

            L.d(TAG, " onError error : " + error);

            switch (error) {
                case IRtcEngineEventHandler.ErrorCode.ERR_FAILED:
                    break;
                case IRtcEngineEventHandler.ErrorCode.ERR_ADM_GENERAL_ERROR:
                case IRtcEngineEventHandler.ErrorCode.ERR_ADM_RUNTIME_PLAYOUT_ERROR:
                case IRtcEngineEventHandler.ErrorCode.ERR_ADM_RUNTIME_RECORDING_ERROR:
                    break;
                default:
                    if (mLiveWatchObserver != null) {
                        mLiveWatchObserver.sendEmptyMessage(UI_EVENT_ANCHOR_LIVE_CONNECTING);
                        mLiveWatchObserver.postDelayed(new Runnable() {
                            @Override
                            public void run() {

                            }
                        }, 3000);
                    }
                    break;

            }
        }
    };

    private TuSdkStreamManager.OnTuSdkListener mTuSdkListener = new TuSdkStreamManager.OnTuSdkListener() {

        @Override
        public void onSurfaceCreated() {
            connectChat();
        }

        @Override
        public void onFrameAvailable(EGLContext eglContext, int textureId, int width, int height, float[] transformMatrix) {
            if (mRtcStreamManager != null) {
                mRtcStreamManager.onFrameAvailable(eglContext, textureId, width, height, transformMatrix);
            }
        }

        @Override
        public void onFaceDetection(boolean visible) {
            Message message = mLiveWatchObserver.obtainMessage();
            message.what = UI_EVENT_NO_FACE;
            message.obj = visible;
            mLiveWatchObserver.sendMessage(message);
        }
    };

    /**
     * 连接聊天
     */
    private void connectChat() {
        if (mLiveWatchObserver != null) {
            mLiveWatchObserver.sendEmptyMessage(UI_EVENT_ANCHOR_PROGRESSBAR_GONE);
            mLiveWatchObserver.sendEmptyMessage(UI_EVENT_ANCHOR_LIVE_CONNECT_SUCCESS);
        }
    }

    /**
     * 多人连麦只能使用百度最快的推流地址，其他模式则使用最快的推流地址
     *
     * @return 推流地址
     */
    private String getPubUrl() {

        if (mLiveRoomBean.audioType == LiveRoomBean.TYPE_VOICE && mLiveRoomBean.isMulti == LiveRoomBean.MULTI_LIVE) {

            String rtmpUrl = CDNBalanceManager.getInstance().getBaiduFastRtmpUrl();

            if (rtmpUrl != null) {
                return rtmpUrl;
            } else {
                return mLiveRoomBean.pubUrl;
            }

        } else {

            String fastRtmpUrl = CDNBalanceManager.getInstance().getFastRtmpUrl();

            if (fastRtmpUrl == null) {

                LiveInfoLogBean.getInstance().getLivePlayAnalytics().reason = LiveInfoLogBean.CHANGE_CDN_IP_IS_NOT;

                fastRtmpUrl = mLiveRoomBean.pubUrl;

            } else {
                LiveInfoLogBean.getInstance().getLivePlayAnalytics().reason = LiveInfoLogBean.CHANGE_CDN_DEFAULT;

            }

            return fastRtmpUrl;
        }

    }

    private void setPKSize() {

        final float[] pkParams = LiveUtils.getPkParams(ScreenUtils.getScreenWidth(TheLApp.context), ScreenUtils.getScreenHeight(TheLApp.context));

        final int x = (int) pkParams[0];

        final int y = (int) pkParams[1];

        final int width = (int) pkParams[2];

        final int height = (int) pkParams[3];

        RelativeLayout.LayoutParams remoteViewParams = new RelativeLayout.LayoutParams(width, height);

        remoteViewParams.topMargin = y;

        remoteViewParams.leftMargin = x;

        RelativeLayout.LayoutParams cameraViewParams = new RelativeLayout.LayoutParams(width, height);

        cameraViewParams.leftMargin = 0;

        cameraViewParams.topMargin = y;

        mGlSurfaceView.setLayoutParams(cameraViewParams);

        mRemoteView.setLayoutParams(remoteViewParams);

        container.addView(mRemoteView);

        if (type == RtcEngineHandler.TYPE_PK) {
            img_background.setImageResource(R.mipmap.pk_bg);
            img_background_top.setVisibility(View.GONE);
        } else {
            RequestOptions requestOptions = new RequestOptions().centerCrop()
                    .dontAnimate()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .skipMemoryCache(true)
                    .transform(new BlurTransformation(TheLApp.context, 25));

            Glide.with(img_background).load(mLiveRoomBean.imageUrl).apply(requestOptions)
                    .into(img_background);
            img_background_top.setVisibility(View.VISIBLE);
        }
    }

    private void setLiveInfoLog(String rtmpUrl, String type) {

        LiveInfoLogBean.getInstance().getLiveChatAnalytics().time = LiveUtils.getLiveTime();
        LiveInfoLogBean.getInstance().getLiveChatAnalytics().liveUserId = String.valueOf(mLiveRoomBean.user.id);
        LiveInfoLogBean.getInstance().getLiveChatAnalytics().roomId = mLiveRoomBean.liveId;
        LiveInfoLogBean.getInstance().getLiveChatAnalytics().relaVersion = BuildConfig.VERSION_NAME;
        LiveInfoLogBean.getInstance().getLiveChatAnalytics().network = NetworkUtils.getAPNType();

        LiveInfoLogBean.getInstance().getLivePlayAnalytics().time = LiveUtils.getLiveTime();
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().userId = Utils.getMyUserId();
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().liveUserId = String.valueOf(mLiveRoomBean.user.id);
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().logType = type;
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().inUrl = rtmpUrl;
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().outUrl = null;
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().cdnDomain = rtmpUrl;
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().relaVersion = BuildConfig.VERSION_NAME;
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().network = NetworkUtils.getAPNType();
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().roomId = mLiveRoomBean.liveId;
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().rtmpUrlSort = ShareFileUtils.getInt(ShareFileUtils.RTMP_URL_SORT, -1);
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().forceServerLiveUrl = mLiveRoomBean.forceServerLiveUrl;

        if (mLiveRoomBean.agora != null && mLiveRoomBean.agora.channelId != null) {
            LiveInfoLogBean.getInstance().getLivePlayAnalytics().channelId = mLiveRoomBean.agora.channelId;
        }

        LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getLivePlayAnalytics());


        LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().relaVersion = BuildConfig.VERSION_NAME;
        if (mLiveRoomBean.user != null) {
            LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().liveUserId = String.valueOf(mLiveRoomBean.user.id);
        }
        LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().time = LiveUtils.getLiveTime();
        LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().cSystem = DeviceUtils.getModel() + " ;" + "Android " + PhoneUtils.getOSVersion();

        LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().roomId = mLiveRoomBean.liveId;
        LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().network = NetworkUtils.getAPNType();
        LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().logType = "liveMsgLog";

    }

    private void setListener() {

    }

    @Override
    public void bindObserver(LiveWatchObserver ob) {

        this.mLiveWatchObserver = ob;
        if (isCreate) {
            mLiveWatchObserver.bindLiveShowCaptureStreamIn(this);
        }
    }

    @Override
    public void finishStream() {

        L.d(TAG, " -------finishStream-------- ");

    }

    @Override
    public void switchCamera() {

        if (cameraId == 0) {
            cameraId = 1;
        } else {
            cameraId = 0;
        }

        if (mTuSdkStreamManager != null) {
            mTuSdkStreamManager.switchCameraOrienation();
        }

    }

    @Override
    public void adjustBeauty(String key, float progress) {
        mTuSdkStreamManager.adjustBeautyParams(key, progress);
    }

    /**********************************************一下为直播PK的方法***********************************************************/
    /**
     * pk发起方的回复通知
     *
     * @param noticeBean
     */
    @Override
    public void showPkResponse(LivePkResponseNoticeBean noticeBean) {

        L.d(TAG, "-----------showPkResponse------------");


        if (noticeBean.result.equals(LivePkResponseNoticeBean.LIVE_PK_RESPONSE_RESULT_YES)) {
            if (noticeBean.agora != null) {

                L.d(TAG, " showPkResponse init agora " + noticeBean.agora);

                isPkRequest = false;

                type = RtcEngineHandler.TYPE_PK;

                switchRtcStream(noticeBean.agora.appId, noticeBean.agora.token, noticeBean.agora.channelId);

                LiveGIOPush.getInstance().setBroadcastPKCount();

                mLiveStatus = LiveStatus.LIVE_PK;

            }
        }

    }

    /**
     * 直播Pk开始
     *
     * @param pkStartNoticeBean
     */
    @Override
    public void showPkStart(LivePkStartNoticeBean pkStartNoticeBean) {

        L.d(TAG, "-----------showPkStart------------" + isPkRequest);

        if (isPkRequest) {

            type = RtcEngineHandler.TYPE_PK;

            switchRtcStream(mLiveRoomBean.agora.appId, mLiveRoomBean.agora.token, mLiveRoomBean.agora.channelId);
        }

    }

    /**
     * 开始显示直播推流状态
     */
    @Override
    public void showPkStream() {

        L.d(TAG, "-----------showPkStream------------");

        type = RtcEngineHandler.TYPE_PK;

    }

    /**
     * pk断线重连
     *
     * @param livePkInitBean
     */
    @Override
    public void showPkInitStream(LivePkInitBean livePkInitBean) {

        L.d(TAG, "-----------showPkInitStream------------");

        if (livePkInitBean == null) {
            return;
        }
        showPkStream();
    }

    @Override
    public void audienceLinkMicResponse() {

        L.d(TAG, "-----------audienceLinkMicResponse------------");

        if (mLiveRoomBean != null && mLiveRoomBean.agora != null) {
            type = TYPE_AUDIENCE_LINK_MIC;
            switchRtcStream(mLiveRoomBean.agora.appId, mLiveRoomBean.agora.token, mLiveRoomBean.agora.channelId);
        }

        LiveGIOPush.getInstance().setAudienceLinkMicCount();
        mLiveStatus = LiveStatus.LIVE_AUDIENCE_LINK_MIC;
    }

    /**
     * 直播PK取消
     * 对方取消Pk,只有在申请时才能取消，推流不做操作
     *
     * @param pkCancelPayload
     */
    @Override
    public void showPkCancel(String pkCancelPayload) {

        isPkRequest = false;

        L.d(TAG, "-----------showPkCancel------------");

    }

    /**
     * 直播PK挂断
     * 对方PK中挂断，推流不做操作
     *
     * @param pkHangupBean
     */
    @Override
    public void showPkHangup(LivePkHangupBean pkHangupBean) {

        L.d(TAG, "-----------showPkHangup------------");

    }

    /**
     * 直播PK总结
     * PK总结期间，推流端不做操作
     *
     * @param pkSummaryNoticeBean
     */
    @Override
    public void showPkSummary(LivePkSummaryNoticeBean pkSummaryNoticeBean) {

        L.d(TAG, "-----------showPkSummary------------");

    }

    /**
     * 直播PK关闭
     *
     * @param pkStopPayload
     */
    @Override
    public void showPkStop(String pkStopPayload) {

        L.d(TAG, "-----------showPkStop------------");

        isPkRequest = false;

        switchTuSdkStream();

    }

    /**
     * 结束PK
     */
    @Override
    public void finishPk() {

        L.d(TAG, "-----------finishPk------------");
        switchTuSdkStream();

        mLiveStatus = LiveStatus.LIVE_NORMAL;

    }

    /**
     * 对方发起的PK申请
     *
     * @param pkRequestNoticeBean
     */
    @Override
    public void showPkRequest(LivePkRequestNoticeBean pkRequestNoticeBean) {
        isPkRequest = true;
    }

    /**********************************************一上为直播PK的方法***********************************************************/


    @Override
    public void linkMicRequest(LiveRoomMsgConnectMicBean liveRoomMsgConnectMicBean) {

        L.d(TAG, "-----------linkMicRequest------------");

        type = RtcEngineHandler.TYPE_LINK_MIC;

        isLinkMicRequest = false;

    }

    @Override
    public void linkMicResponse(LiveRoomMsgConnectMicBean liveRoomMsgConnectMicBean) {

        L.d(TAG, "-----------linkMicResponse------------" + GsonUtils.createJsonString(liveRoomMsgConnectMicBean));

        if (liveRoomMsgConnectMicBean.agora != null) {

            type = RtcEngineHandler.TYPE_LINK_MIC;

            isLinkMicRequest = true;

            AgoraBean agora = liveRoomMsgConnectMicBean.agora;

            switchRtcStream(agora.appId, agora.token, agora.channelId);

            LiveGIOPush.getInstance().setBroadcastLinkMicCount();

            mLiveStatus = LiveStatus.LIVE_LINK_MIC;

        }

    }

    @Override
    public void linkMickStart() {

        L.d(TAG, "-----------linkMickStart------------" + GsonUtils.createJsonString(mLiveRoomBean.agora));

        L.d(TAG, " isLinkMicRequest " + isLinkMicRequest);

        if (!isLinkMicRequest) {

            type = RtcEngineHandler.TYPE_LINK_MIC;

            switchRtcStream(mLiveRoomBean.agora.appId, mLiveRoomBean.agora.token, mLiveRoomBean.agora.channelId);
        }
    }


    @Override
    public void linkMicHangup() {

        L.d(TAG, " linkMicHangup : ");

//        switchTuSdkStream();
    }

    @Override
    public void linkMicHangupByOwn() {

        L.d(TAG, " linkMicHangupByOwn : ");

//        switchTuSdkStream();
    }

    @Override
    public void linkMicStop() {

        L.d(TAG, " linkMicStop : ");


        switchTuSdkStream();

        mLiveStatus = LiveStatus.LIVE_NORMAL;

        isLinkMicRequest = false;

    }

    @Override
    public void muteLocalAudioStream(boolean mute) {

    }

    @Override
    public void playEffect(int position) {

    }

    @Override
    public LiveStatus getListStatus() {
        return mLiveStatus;
    }

    @Override
    public void switchStricker(SoftGiftBean giftBean) {
        mTuSdkStreamManager.addStricker(giftBean);
    }

    @Override
    public void acceptAudienceLinkMic() {

        if (mLiveRoomBean != null && mLiveRoomBean.agora != null) {
            type = RtcEngineHandler.TYPE_LINK_MIC;
            switchRtcStream(mLiveRoomBean.agora.appId, mLiveRoomBean.agora.token, mLiveRoomBean.agora.channelId);
        }

    }

    @Override
    public void onDestroy() {

        try {
            if (mRtcStreamManager != null) {

                mRtcStreamManager.leaveChannel();

                mRtcStreamManager.destroy();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        isCreate = false;

        if (mTuSdkStreamManager != null) {
            mTuSdkStreamManager.onDestroy();
        }

        if (container != null) {
            container.removeAllViews();
        }

        if (sound_view != null) {
            sound_view.destory();
        }

        super.onDestroy();

    }

    @Override
    public void onResume() {
        super.onResume();
        if (mTuSdkStreamManager != null) {
            mTuSdkStreamManager.onResume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mTuSdkStreamManager != null) {
            mTuSdkStreamManager.onPause();
        }
    }

    private void switchRtcStream(String appId, String token, String channelId) {

        if (mTuSdkStreamManager != null) {
            mTuSdkStreamManager.stopStreaming();
        }

        int uid = Integer.valueOf(UserUtils.getMyUserId());

        initRtcEngine(appId, token, channelId, mLiveRoomBean.pubUrl, uid);
    }

    private void switchTuSdkStream() {

        if (mRtcStreamManager != null) {

            mRtcStreamManager.leaveChannel();

            mRtcStreamManager = null;

        } else {

            if (container != null) {

                if (mRemoteView != null) {
                    container.removeView(mRemoteView);
                    mRemoteView = null;
                }

                RelativeLayout.LayoutParams cameraViewParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

                mGlSurfaceView.setLayoutParams(cameraViewParams);

            }

        }

    }

}