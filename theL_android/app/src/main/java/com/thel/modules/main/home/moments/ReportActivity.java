package com.thel.modules.main.home.moments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UploadManager;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.base.BaseDataBean;
import com.thel.bean.ReleasedCommentBean;
import com.thel.bean.user.UploadTokenBean;
import com.thel.constants.TheLConstants;
import com.thel.imp.momentblack.MomentBlackListener;
import com.thel.imp.momentblack.MomentBlackUtils;
import com.thel.imp.momentreport.MomentReportUtils;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.preview_image.UserInfoPhotoActivity;
import com.thel.modules.select_image.SelectLocalImagesActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.RequestConstants;
import com.thel.network.service.DefaultRequestService;
import com.thel.ui.adapter.ReportReasonAdapter;
import com.thel.ui.widget.MyListView;
import com.thel.utils.AndroidBug5497Workaround;
import com.thel.utils.DialogUtil;
import com.thel.utils.ImageUtils;
import com.thel.utils.L;
import com.thel.utils.MD5Utils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SharedPrefUtils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * 举报、折叠、bug反馈页面
 * Setsail
 */
public class ReportActivity extends BaseActivity implements MomentBlackListener {

    private static final String TAG = "ReportActivity";

    /**
     * 选择完成
     */
    private LinearLayout lin_done;
    private ImageView img_done;
    private LinearLayout lin_back;
    private TextView txt_title;

    private TextView heading1;
    private TextView heading2;
    private TextView number;
    private RelativeLayout rel_add_pics;
    private ImageView add;
    private ImageView add1;
    private ImageView add2;
    private ImageView add3;
    private ImageView add4;
    private int picAmount = 0;
    private EditText edt_report_reason;
    private RelativeLayout optional_layout;

    // 选择的图片
    private String pic1;
    private String pic2;
    private String pic3;
    private String pic4;
    private String picUrl1;
    private String picUrl2;
    private String picUrl3;
    private String picUrl4;

    private int picNum = 0;
    private int uploadSucceedNum = 0;

    /**
     * 举报选项
     */
    private MyListView listView;
    private ReportReasonAdapter adapter;

    private int selected = -1;

    private String type;

    public static final String REPORT_TYPE_HIDE_MOMENT = "hide_moment";
    public static final String REPORT_TYPE_ABUSE = "abuse";
    public static final String REPORT_TYPE_REPORT_USER = "report_user";
    public static final String REPORT_TYPE_REPORT_MATCH_USER = "report_match_user";

    public static final String REPORT_TYPE_BUG_FEEDBACK = "bug_feedback";
    public static final String REPORT_TYPE_ANCHOR = "report_anchor";//主播
    public static final String REPORT_TYPE_AUDIENCE = "report_AUDIENCE";//观众

    public static final int TYPE_ANCHOR = 1;
    public static final int TYPE_AUDIENCE = 2;
    public static final int TYPE_USER = 0;//普通用户
    public static final int TYPE_MATCH_USER = 3;//匹配用户

    private String momentsId;
    private String userId;

    // key是请求的唯一表示，value是要上传图片的本地路径
    private Map<String, String> requestMD5s = new HashMap<String, String>();
    // key是要上传图片的本地路径，value图片上传到七牛的地址
    private Map<String, String> picUploadPaths = new HashMap<String, String>();

    private TextWatcher textWatcher;
    private String reportContent = "";//举报观众 直播评论的内容
    private MomentBlackUtils momentBlackUtils;
    private String liveRoomId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout();
        findViewById();
        initRegister();
        Intent intent = getIntent();
        type = intent.getStringExtra(TheLConstants.BUNDLE_KEY_REPORT_TYPE);
        AndroidBug5497Workaround.assistActivity(this);
        L.d("ReportActivity", " logType : " + type);

        if (REPORT_TYPE_HIDE_MOMENT.equals(type)) {// 屏蔽日志
            momentsId = intent.getStringExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID);
            txt_title.setText(getString(R.string.report_activity_hide_moment_title));
            optional_layout.setVisibility(View.GONE);
            adapter = new ReportReasonAdapter(new String[]{getString(R.string.report_activity_hide_moment_reason1), getString(R.string.report_activity_hide_moment_reason2), getString(R.string.report_activity_hide_moment_reason3)});
        } else if (REPORT_TYPE_ABUSE.equals(type)) {// 举报日志
            momentsId = intent.getStringExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID);
            heading1.setText(getRedString(getString(R.string.report_activity_report_common_heading0), getString(R.string.report_activity_report_common_heading1)));
            txt_title.setText(getString(R.string.report_activity_report_abuse_title));
            edt_report_reason.setHint(getString(R.string.report_activity_report_common_hint));
            optional_layout.setVisibility(View.VISIBLE);
            adapter = new ReportReasonAdapter(new String[]{getString(R.string.report_activity_report_abuse_reason1), getString(R.string.report_activity_report_abuse_reason2), getString(R.string.report_activity_report_abuse_reason3)});
        } else if (REPORT_TYPE_REPORT_USER.equals(type)) {// 举报用户
            userId = intent.getStringExtra(TheLConstants.BUNDLE_KEY_USER_ID);
            heading1.setText(getRedString(getString(R.string.report_activity_report_common_heading0), getString(R.string.report_activity_report_common_heading1)));
            heading2.setText(getString(R.string.report_activity_report_user_heading2));
            txt_title.setText(getString(R.string.report_activity_report_user_title));
            optional_layout.setVisibility(View.VISIBLE);
            rel_add_pics.setVisibility(View.VISIBLE);
            adapter = new ReportReasonAdapter(getUserReasons());
        } else if (REPORT_TYPE_REPORT_MATCH_USER.equals(type)) {// 举报匹配用户
            userId = intent.getStringExtra(TheLConstants.BUNDLE_KEY_USER_ID);
            heading1.setText(getRedString(getString(R.string.report_activity_report_common_heading0), getString(R.string.report_activity_report_common_heading1)));
            heading2.setText(getString(R.string.report_activity_report_abuse_heading2));
            txt_title.setText(getString(R.string.report_activity_report_user_title));
            optional_layout.setVisibility(View.VISIBLE);
            rel_add_pics.setVisibility(View.GONE);
            adapter = new ReportReasonAdapter(getMatchUserReasons());
        } else if (REPORT_TYPE_BUG_FEEDBACK.equals(type)) {// bug反馈
            heading1.setText(getString(R.string.report_activity_feedback_heading1));
            heading2.setText(getString(R.string.report_activity_feedback_heading2));
            txt_title.setText(getString(R.string.report_activity_feedback_title));
            optional_layout.setVisibility(View.VISIBLE);
            edt_report_reason.setHint(getString(R.string.report_activity_feedback_heading2));
            rel_add_pics.setVisibility(View.VISIBLE);
            adapter = new ReportReasonAdapter(new String[]{getString(R.string.report_activity_feedback_reason1), getString(R.string.report_activity_feedback_reason2), getString(R.string.report_activity_feedback_reason3)});
        } else if (REPORT_TYPE_ANCHOR.equals(type)) {//举报主播
            userId = intent.getStringExtra(TheLConstants.BUNDLE_KEY_USER_ID);
            liveRoomId = intent.getStringExtra(TheLConstants.BUNDLE_KEY_LIVE_ROOM_ID);

            final String imgPath = intent.getStringExtra(TheLConstants.BUNDLE_KEY_LOCAL_IMAGE_PATH);
            if (!TextUtils.isEmpty(imgPath)) {
                photoSelected(imgPath);
            }
            txt_title.setText(getString(R.string.report_activity_report_anchor_title));
            heading1.setText(getRedString(getString(R.string.report_activity_report_common_heading0), getString(R.string.report_activity_report_common_heading1)));
            heading2.setText(getString(R.string.report_activity_report_user_heading2));
            optional_layout.setVisibility(View.VISIBLE);
            rel_add_pics.setVisibility(View.VISIBLE);
            adapter = new ReportReasonAdapter(getAnchorReasons());
        } else if (REPORT_TYPE_AUDIENCE.equals(type)) {
            userId = intent.getStringExtra(TheLConstants.BUNDLE_KEY_USER_ID);
            reportContent = intent.getStringExtra(TheLConstants.BUNDLE_KEY_REPORT_CONTENT);
            liveRoomId = intent.getStringExtra(TheLConstants.BUNDLE_KEY_LIVE_ROOM_ID);
            txt_title.setText(getString(R.string.report_activity_report_audience_title));
            heading1.setText(getRedString(getString(R.string.report_activity_report_common_heading0), getString(R.string.report_activity_report_common_heading1)));
            heading2.setText(getString(R.string.report_activity_report_user_heading2));
            optional_layout.setVisibility(View.VISIBLE);
            rel_add_pics.setVisibility(View.VISIBLE);
            adapter = new ReportReasonAdapter(getAudiencReasons());
        }
        listView.setAdapter(adapter);
        setListener();
    }

    private void initRegister() {
        momentBlackUtils = new MomentBlackUtils();
        momentBlackUtils.registerReceiver(this);
    }

    private List<ReportReasonAdapter.ReasonBean> getUserReasons() {
        List<ReportReasonAdapter.ReasonBean> data = new ArrayList<>();
        data.add(new ReportReasonAdapter.ReasonBean(getString(R.string.report_activity_report_user_reason1),5));
        data.add(new ReportReasonAdapter.ReasonBean(getString(R.string.report_activity_report_user_reason2),1));
        data.add(new ReportReasonAdapter.ReasonBean(getString(R.string.report_activity_report_user_reason3),2));
        data.add(new ReportReasonAdapter.ReasonBean(getString(R.string.report_activity_report_user_reason4),3));
        return data;
    }

    private List<ReportReasonAdapter.ReasonBean> getMatchUserReasons() {
        List<ReportReasonAdapter.ReasonBean> data = new ArrayList<>();
        data.add(new ReportReasonAdapter.ReasonBean(getString(R.string.report_activity_report_user_reason1),5));
        data.add(new ReportReasonAdapter.ReasonBean(getString(R.string.report_activity_report_user_reason2),1));
        data.add(new ReportReasonAdapter.ReasonBean(getString(R.string.report_activity_report_user_reason3),2));
        data.add(new ReportReasonAdapter.ReasonBean(getString(R.string.report_activity_report_user_reason4),3));
        data.add(new ReportReasonAdapter.ReasonBean(getString(R.string.report_activity_report_user_reason5),4));
        return data;
    }

    private List<ReportReasonAdapter.ReasonBean> getAudiencReasons() {
        List<ReportReasonAdapter.ReasonBean> data = new ArrayList<>();
        data.add(new ReportReasonAdapter.ReasonBean(getString(R.string.report_activity_report_audience_reason1),6));
        data.add(new ReportReasonAdapter.ReasonBean(getString(R.string.report_activity_report_audience_reason2),3));
        data.add(new ReportReasonAdapter.ReasonBean(getString(R.string.report_activity_report_audience_reason3),1));
        data.add(new ReportReasonAdapter.ReasonBean(getString(R.string.report_activity_report_audience_reason4),2));
        data.add(new ReportReasonAdapter.ReasonBean(getString(R.string.report_activity_report_audience_reason5),4));
        data.add(new ReportReasonAdapter.ReasonBean(getString(R.string.report_activity_report_audience_reason6),5));
        data.add(new ReportReasonAdapter.ReasonBean(getString(R.string.report_activity_report_audience_reason7),10));
        data.add(new ReportReasonAdapter.ReasonBean(getString(R.string.report_activity_report_audience_reason8),7));
        return data;
    }

    private List<ReportReasonAdapter.ReasonBean> getAnchorReasons() {
        List<ReportReasonAdapter.ReasonBean> data = new ArrayList<>();
        data.add(new ReportReasonAdapter.ReasonBean(getString(R.string.report_activity_report_anchor_reson1),7));
        data.add(new ReportReasonAdapter.ReasonBean(getString(R.string.report_activity_report_anchor_reson2),8));
        data.add(new ReportReasonAdapter.ReasonBean(getString(R.string.report_activity_report_anchor_reson3),3));
        data.add(new ReportReasonAdapter.ReasonBean(getString(R.string.report_activity_report_anchor_reson4),1));
        data.add(new ReportReasonAdapter.ReasonBean(getString(R.string.report_activity_report_anchor_reson5),2));
        data.add(new ReportReasonAdapter.ReasonBean(getString(R.string.report_activity_report_anchor_reson6),4));
        data.add(new ReportReasonAdapter.ReasonBean(getString(R.string.report_activity_report_anchor_reson7),5));
        data.add(new ReportReasonAdapter.ReasonBean(getString(R.string.report_activity_report_anchor_reson8),6));
        data.add(new ReportReasonAdapter.ReasonBean(getString(R.string.report_activity_report_anchor_reson9),12));
        data.add(new ReportReasonAdapter.ReasonBean(getString(R.string.report_activity_report_anchor_reson10),13));
        data.add(new ReportReasonAdapter.ReasonBean(getString(R.string.report_activity_report_anchor_reson11),9));
        return data;
    }

    private SpannableString getRedString(String head, String target) {
        final String st = head + target;
        final int start = st.lastIndexOf(target);
        final int end = start + target.length();
        final SpannableString ss = new SpannableString(st);
        ss.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.red)), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return ss;
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(this.getClass().getName());
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(this.getClass().getName());
        MobclickAgent.onPause(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (textWatcher != null && edt_report_reason != null) {

            edt_report_reason.removeTextChangedListener(textWatcher);
        }
        momentBlackUtils.unRegisterReceiver(this);

    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    protected void processBusiness() {

    }

    protected void setListener() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (REPORT_TYPE_BUG_FEEDBACK.equals(type)) {// 意见反馈，下面部分是必填的
                    if (!TextUtils.isEmpty(edt_report_reason.getText().toString().trim()) || !TextUtils.isEmpty(pic1))
                        setDoneButtonEnabled(true);
                } else if (REPORT_TYPE_REPORT_USER.equals(type)) {//举报用户
                    if (!TextUtils.isEmpty(edt_report_reason.getText().toString().trim()) && !TextUtils.isEmpty(pic1))
                        setDoneButtonEnabled(true);
                } else if (REPORT_TYPE_REPORT_MATCH_USER.equals(type)) {//举报匹配用户
                    setDoneButtonEnabled(true);
                } else {
                    setDoneButtonEnabled(true);
                }
                selected = position;
                adapter.select(position);
            }
        });


        lin_done.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                L.d("ReportActivity", "-------lin_done-------" + type);

                showLoading();
                if (REPORT_TYPE_HIDE_MOMENT.equals(type)) {// 屏蔽日志
                    blockThisMoment(momentsId, adapter.getReasonType(selected));
                    MomentBlackUtils.blackThisMomentSuccess(momentsId);
                } else if (REPORT_TYPE_ABUSE.equals(type)) {// 举报日志
                    reportMoment(momentsId, adapter.getReasonType(selected), edt_report_reason.getText().toString().trim());
                    MomentReportUtils.sendReportMomentBroad(momentsId);
                } else if (REPORT_TYPE_REPORT_USER.equals(type)) {// 举报用户
                    L.d("ReportActivity", "-------REPORT_TYPE_REPORT_USER-------");
                    picNum = 0;
                    uploadSucceedNum = 0;
                    String uploadPath;
                    String uploadRequestMD5;
                    if (!TextUtils.isEmpty(pic1)) {
                        picNum += 1;
                        uploadPath = RequestConstants.UPLOAD_FILE_ROOT_PATH_REPORT + ShareFileUtils.getString(ShareFileUtils.ID, "") + "/" + MD5Utils.calculateMD5(new File(pic1)) + ".jpg";
                        uploadRequestMD5 = getUploadToken(System.currentTimeMillis() + "", "", uploadPath);
                        L.d("ReportActivity", " uploadPath : " + uploadPath);
                        L.d("ReportActivity", " uploadRequestMD5 : " + uploadRequestMD5);
                        requestMD5s.put(uploadRequestMD5, pic1);
                        picUploadPaths.put(pic1, uploadPath);
                        if (!TextUtils.isEmpty(pic2)) {
                            picNum += 1;
                            uploadPath = RequestConstants.UPLOAD_FILE_ROOT_PATH_REPORT + ShareFileUtils.getString(ShareFileUtils.ID, "") + "/" + MD5Utils.calculateMD5(new File(pic2)) + ".jpg";
                            uploadRequestMD5 = getUploadToken(System.currentTimeMillis() + "", "", uploadPath);
                            requestMD5s.put(uploadRequestMD5, pic2);
                            picUploadPaths.put(pic2, uploadPath);
                            if (!TextUtils.isEmpty(pic3)) {
                                picNum += 1;
                                uploadPath = RequestConstants.UPLOAD_FILE_ROOT_PATH_REPORT + ShareFileUtils.getString(ShareFileUtils.ID, "") + "/" + MD5Utils.calculateMD5(new File(pic3)) + ".jpg";
                                uploadRequestMD5 = getUploadToken(System.currentTimeMillis() + "", "", uploadPath);
                                requestMD5s.put(uploadRequestMD5, pic3);
                                picUploadPaths.put(pic3, uploadPath);
                                if (!TextUtils.isEmpty(pic4)) {
                                    picNum += 1;
                                    uploadPath = RequestConstants.UPLOAD_FILE_ROOT_PATH_REPORT + ShareFileUtils.getString(ShareFileUtils.ID, "") + "/" + MD5Utils.calculateMD5(new File(pic4)) + ".jpg";
                                    uploadRequestMD5 = getUploadToken(System.currentTimeMillis() + "", "", uploadPath);
                                    requestMD5s.put(uploadRequestMD5, pic4);
                                    picUploadPaths.put(pic4, uploadPath);
                                }
                            }
                        }
                    } else {
                        reportUser(userId, adapter.getReasonType(selected), edt_report_reason.getText().toString().trim(), "", String.valueOf(TYPE_USER), reportContent);
                    }
                } else if (REPORT_TYPE_REPORT_MATCH_USER.equals(type)) {//举报匹配用户
                    reportUser(userId, adapter.getReasonType(selected), edt_report_reason.getText().toString().trim(), "", String.valueOf(TYPE_MATCH_USER), reportContent);

                } else if (REPORT_TYPE_BUG_FEEDBACK.equals(type)) {// bug反馈
                    picNum = 0;
                    uploadSucceedNum = 0;
                    String uploadPath;
                    String uploadRequestMD5;
                    if (!TextUtils.isEmpty(pic1)) {
                        picNum += 1;
                        uploadPath = RequestConstants.UPLOAD_FILE_ROOT_PATH_REPORT + ShareFileUtils.getString(ShareFileUtils.ID, "") + "/" + MD5Utils.calculateMD5(new File(pic1)) + ".jpg";
                        uploadRequestMD5 = getUploadToken(System.currentTimeMillis() + "", "", uploadPath);
                        requestMD5s.put(uploadRequestMD5, pic1);
                        picUploadPaths.put(pic1, uploadPath);
                        if (!TextUtils.isEmpty(pic2)) {
                            picNum += 1;
                            uploadPath = RequestConstants.UPLOAD_FILE_ROOT_PATH_REPORT + ShareFileUtils.getString(ShareFileUtils.ID, "") + "/" + MD5Utils.calculateMD5(new File(pic2)) + ".jpg";
                            uploadRequestMD5 = getUploadToken(System.currentTimeMillis() + "", "", uploadPath);
                            requestMD5s.put(uploadRequestMD5, pic2);
                            picUploadPaths.put(pic2, uploadPath);
                            if (!TextUtils.isEmpty(pic3)) {
                                picNum += 1;
                                uploadPath = RequestConstants.UPLOAD_FILE_ROOT_PATH_REPORT + ShareFileUtils.getString(ShareFileUtils.ID, "") + "/" + MD5Utils.calculateMD5(new File(pic3)) + ".jpg";
                                uploadRequestMD5 = getUploadToken(System.currentTimeMillis() + "", "", uploadPath);
                                requestMD5s.put(uploadRequestMD5, pic3);
                                picUploadPaths.put(pic3, uploadPath);
                                if (!TextUtils.isEmpty(pic4)) {
                                    picNum += 1;
                                    uploadPath = RequestConstants.UPLOAD_FILE_ROOT_PATH_REPORT + ShareFileUtils.getString(ShareFileUtils.ID, "") + "/" + MD5Utils.calculateMD5(new File(pic4)) + ".jpg";
                                    uploadRequestMD5 = getUploadToken(System.currentTimeMillis() + "", "", uploadPath);
                                    requestMD5s.put(uploadRequestMD5, pic4);
                                    picUploadPaths.put(pic4, uploadPath);
                                }
                            }
                        }
                    } else {
                        bugFeedback(adapter.getReasonType(selected), "", edt_report_reason.getText().toString().trim());
                    }
                } else if (REPORT_TYPE_ANCHOR.equals(type)) {//举报主播
                    if (!TextUtils.isEmpty(pic1)) {
                        uploadPic();
                    } else {
                        submit();
                    }

                } else if (REPORT_TYPE_AUDIENCE.equals(type)) {//举报观众
                    if (!TextUtils.isEmpty(pic1)) {
                        uploadPic();
                    } else {
                        submit();
                    }
                }
            }
        });

        lin_back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((edt_report_reason != null && !TextUtils.isEmpty(edt_report_reason.getText().toString().trim())) || picAmount > 0) {
                    DialogUtil.showConfirmDialog(ReportActivity.this, "", getString(R.string.updatauserinfo_activity_quit_confirm), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int i) {
                            dialog.dismiss();
                            finish();
                        }
                    });
                } else {
                    finish();
                }
            }
        });

        textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (REPORT_TYPE_BUG_FEEDBACK.equals(type)) {// 意见反馈，下面部分是必填的
                    if (selected != -1) {
                        if (charSequence.toString().trim().length() > 0) {
                            setDoneButtonEnabled(true);
                        } else {
                            if (TextUtils.isEmpty(pic1)) {
                                setDoneButtonEnabled(false);
                            }
                        }
                    }
                } else if (REPORT_TYPE_REPORT_USER.equals(type)) {//举报用户
                    if (selected != -1) {
                        if (charSequence.toString().trim().length() > 0 && !TextUtils.isEmpty(pic1)) {
                            setDoneButtonEnabled(true);
                        } else {
                            setDoneButtonEnabled(false);
                        }
                    }
                } else if (REPORT_TYPE_REPORT_MATCH_USER.equals(type)) { //举报匹配用户
                    if (selected != -1) {
                        setDoneButtonEnabled(true);
                    }
                }
                number.setText((100 - charSequence.length()) + "");
                if (charSequence.length() >= 100) {
                    DialogUtil.showToastShort(ReportActivity.this, getString(R.string.report_activity_tip));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };
        edt_report_reason.addTextChangedListener(textWatcher);

        // 上传第一张照片或者上传音乐
        add.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                Intent intent = new Intent(ReportActivity.this, SelectLocalImagesActivity.class);
                intent.putExtra(TheLConstants.BUNDLE_KEY_SELECT_AMOUNT, 4);
                startActivityForResult(intent, TheLConstants.BUNDLE_CODE_WRITE_MOMENT_ACTIVITY);
            }
        });

        add1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewUtils.preventViewMultipleClick(view, 1000);
                Intent i = new Intent(ReportActivity.this, UserInfoPhotoActivity.class);
                i.putExtra("fromPage", ReleaseMomentActivity.class.getName());
                ArrayList<String> photoUrls = new ArrayList<String>();
                photoUrls.add(pic1);
                if (!TextUtils.isEmpty(pic2)) {
                    photoUrls.add(pic2);
                    if (!TextUtils.isEmpty(pic3)) {
                        photoUrls.add(pic3);
                        if (!TextUtils.isEmpty(pic4)) {
                            photoUrls.add(pic4);
                        }
                    }
                }
                i.putExtra("position", 0);
                i.putStringArrayListExtra(TheLConstants.BUNDLE_KEY_PHOTOS, photoUrls);
                startActivityForResult(i, TheLConstants.BUNDLE_CODE_WRITE_MOMENT_ACTIVITY);
            }
        });

        add2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewUtils.preventViewMultipleClick(view, 1000);
                if (!TextUtils.isEmpty(pic2)) {// 已选中了2张照片，则去照片预览页面
                    Intent i = new Intent(ReportActivity.this, UserInfoPhotoActivity.class);
                    i.putExtra("fromPage", ReleaseMomentActivity.class.getName());
                    ArrayList<String> photoUrls = new ArrayList<String>();
                    photoUrls.add(pic1);
                    photoUrls.add(pic2);
                    if (!TextUtils.isEmpty(pic3)) {
                        photoUrls.add(pic3);
                        if (!TextUtils.isEmpty(pic4)) {
                            photoUrls.add(pic4);
                        }
                    }
                    i.putExtra("position", 1);
                    i.putStringArrayListExtra(TheLConstants.BUNDLE_KEY_PHOTOS, photoUrls);
                    startActivityForResult(i, TheLConstants.BUNDLE_CODE_WRITE_MOMENT_ACTIVITY);
                } else {
                    Intent intent = new Intent(ReportActivity.this, SelectLocalImagesActivity.class);
                    intent.putExtra(TheLConstants.BUNDLE_KEY_SELECT_AMOUNT, 3);
                    startActivityForResult(intent, TheLConstants.BUNDLE_CODE_WRITE_MOMENT_ACTIVITY);
                }
            }
        });

        add3.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewUtils.preventViewMultipleClick(view, 1000);
                if (!TextUtils.isEmpty(pic3)) {// 已选中了3张照片，则去照片预览页面
                    Intent i = new Intent(ReportActivity.this, UserInfoPhotoActivity.class);
                    i.putExtra("fromPage", ReleaseMomentActivity.class.getName());
                    ArrayList<String> photoUrls = new ArrayList<String>();
                    photoUrls.add(pic1);
                    photoUrls.add(pic2);
                    photoUrls.add(pic3);
                    if (!TextUtils.isEmpty(pic4)) {
                        photoUrls.add(pic4);
                    }
                    i.putExtra("position", 2);
                    i.putStringArrayListExtra(TheLConstants.BUNDLE_KEY_PHOTOS, photoUrls);
                    startActivityForResult(i, TheLConstants.BUNDLE_CODE_WRITE_MOMENT_ACTIVITY);
                } else {
                    Intent intent = new Intent(ReportActivity.this, SelectLocalImagesActivity.class);
                    intent.putExtra(TheLConstants.BUNDLE_KEY_SELECT_AMOUNT, 2);
                    startActivityForResult(intent, TheLConstants.BUNDLE_CODE_WRITE_MOMENT_ACTIVITY);
                }
            }
        });

        add4.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewUtils.preventViewMultipleClick(view, 1000);
                if (!TextUtils.isEmpty(pic4)) {// 已选中了4张照片，则去照片预览页面
                    Intent i = new Intent(ReportActivity.this, UserInfoPhotoActivity.class);
                    i.putExtra("fromPage", ReleaseMomentActivity.class.getName());
                    ArrayList<String> photoUrls = new ArrayList<String>();
                    photoUrls.add(pic1);
                    photoUrls.add(pic2);
                    photoUrls.add(pic3);
                    photoUrls.add(pic4);
                    i.putExtra("position", 3);
                    i.putStringArrayListExtra(TheLConstants.BUNDLE_KEY_PHOTOS, photoUrls);
                    startActivityForResult(i, TheLConstants.BUNDLE_CODE_WRITE_MOMENT_ACTIVITY);
                } else {
                    Intent intent = new Intent(ReportActivity.this, SelectLocalImagesActivity.class);
                    intent.putExtra(TheLConstants.BUNDLE_KEY_SELECT_AMOUNT, 1);
                    startActivityForResult(intent, TheLConstants.BUNDLE_CODE_WRITE_MOMENT_ACTIVITY);
                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == TheLConstants.RESULT_CODE_TAKE_PHOTO) {
            String photoPath = data.getStringExtra(TheLConstants.BUNDLE_KEY_IMAGE_OUTPUT_PATH);
            int selectAmountLimit = data.getIntExtra(TheLConstants.BUNDLE_KEY_SELECT_AMOUNT, 1);
            if (!TextUtils.isEmpty(photoPath)) {
                if (selectAmountLimit == 4) {
                    photoSelected(photoPath);
                } else {
                    photoSelected(photoPath, selectAmountLimit);
                }
            }
        } else if (resultCode == TheLConstants.RESULT_CODE_SELECT_LOCAL_IMAGE) {
            String photoPath = data.getStringExtra(TheLConstants.BUNDLE_KEY_LOCAL_IMAGE_PATH);
            int selectAmountLimit = data.getIntExtra(TheLConstants.BUNDLE_KEY_SELECT_AMOUNT, 1);
            if (!TextUtils.isEmpty(photoPath)) {
                if (selectAmountLimit == 4) {
                    photoSelected(photoPath);
                } else {
                    photoSelected(photoPath, selectAmountLimit);
                }
            } else {
                DialogUtil.showToastShort(this, TheLApp.getContext().getString(R.string.info_rechoise_photo));
            }
        } else if (resultCode == TheLConstants.RESULT_CODE_WRITE_MOMENT_DELETE_PICTURE) {// 删除图片
            if (data != null) {
                ArrayList<Integer> indexes = data.getIntegerArrayListExtra(TheLConstants.BUNDLE_KEY_INDEX);
                ArrayList<String> picUrlsTemp = new ArrayList<String>();// 剩下的图片url
                if (indexes != null && indexes.size() < picAmount) {// 如果有删除图片
                    if (!TextUtils.isEmpty(pic1)) {
                        if (indexes.contains(0)) {// 删除第一张
                            picUrlsTemp.add(pic1);
                        }
                        if (!TextUtils.isEmpty(pic2)) {
                            if (indexes.contains(1)) {// 删除第二张
                                picUrlsTemp.add(pic2);
                            }
                            if (!TextUtils.isEmpty(pic3)) {
                                if (indexes.contains(2)) {// 删除第三张
                                    picUrlsTemp.add(pic3);
                                }
                                if (!TextUtils.isEmpty(pic4)) {
                                    if (indexes.contains(3)) {// 删除第四张
                                        picUrlsTemp.add(pic4);
                                    }
                                }
                            }
                        }
                    }
                    pic1 = "";
                    pic2 = "";
                    pic3 = "";
                    pic4 = "";
                    add1.setImageBitmap(null);
                    add1.setVisibility(View.INVISIBLE);
                    add2.setImageBitmap(null);
                    add2.setVisibility(View.INVISIBLE);
                    add3.setImageBitmap(null);
                    add3.setVisibility(View.INVISIBLE);
                    add4.setImageBitmap(null);
                    add4.setVisibility(View.INVISIBLE);
                    picAmount = picUrlsTemp.size();
                    if (picUrlsTemp.size() == 0) {// 如果全删光
                        add.setVisibility(View.VISIBLE);
                        setDoneButtonEnabled(false);
                    } else {
                        if (!TextUtils.isEmpty(edt_report_reason.getText().toString().trim()) && selected != -1) {
                            setDoneButtonEnabled(true);
                        }
                        for (int i = 0; i < picUrlsTemp.size(); i++) {//如果没删光
                            switch (i) {
                                case 0:
                                    pic1 = picUrlsTemp.get(0);
                                    ImageLoaderManager.imageLoader(add1, pic1);
                                    add1.setVisibility(View.VISIBLE);
                                    add2.setVisibility(View.VISIBLE);
                                    break;
                                case 1:
                                    pic2 = picUrlsTemp.get(1);
                                    ImageLoaderManager.imageLoader(add2, pic2);
                                    add3.setVisibility(View.VISIBLE);
                                    break;
                                case 2:
                                    pic3 = picUrlsTemp.get(2);
                                    ImageLoaderManager.imageLoader(add3, pic3);
                                    add4.setVisibility(View.VISIBLE);
                                    break;
                                case 3:
                                    pic4 = picUrlsTemp.get(3);
                                    ImageLoaderManager.imageLoader(add4, pic4);
                                    break;
                            }
                        }
                    }
                }
                if (REPORT_TYPE_BUG_FEEDBACK.equals(type)) {// 意见反馈，下面部分是必填的
                    if (TextUtils.isEmpty(edt_report_reason.getText().toString().trim()) && picUrlsTemp.size() < 1) {
                        setDoneButtonEnabled(false);
                    }
                }
            }
        }
    }

    /**
     * 选好图片后刷新界面
     *
     * @param path
     */
    private void photoSelected(String path) {
        if (TextUtils.isEmpty(path)) {
            return;
        }
        String[] picUrlsTemp = path.split(",");
        List<String> picUrls = new ArrayList<>();
        for (int i = 0; i < picUrlsTemp.length; i++) {
            if (ImageUtils.isImageLegal(picUrlsTemp[i], 320, 320)) {
                picUrls.add(picUrlsTemp[i]);
            }
        }
        if (picUrls.size() < picUrlsTemp.length) {
            DialogUtil.showToastShort(this, getString(R.string.info_illegal_photo));
        }
        picAmount = 0;
        String outputName;
        if (picUrls.size() > 0) {
            outputName = "pic1_" + ImageUtils.getPicName();
            pic1 = TheLConstants.F_TAKE_PHOTO_ROOTPATH + outputName;
            picAmount += 1;
            if (REPORT_TYPE_BUG_FEEDBACK.equals(type)) {
                if (selected != -1) {
                    setDoneButtonEnabled(true);
                }
            } else if (REPORT_TYPE_REPORT_USER.equals(type)) {//举报用户
                if (edt_report_reason.getText().toString().trim().length() > 0 && selected != -1) {
                    setDoneButtonEnabled(true);
                }
            }

            L.d(TAG, " pic1 : " + pic1);

            ImageUtils.handleLocalPic(picUrls.get(0), TheLConstants.F_TAKE_PHOTO_ROOTPATH, outputName, TheLConstants.MAX_PIC_WIDTH, TheLConstants.MAX_PIC_HEIGHT, TheLConstants.PIC_QUALITY, true);
            add.setVisibility(View.GONE);
            add1.setVisibility(View.VISIBLE);
            add2.setVisibility(View.VISIBLE);
            ImageLoaderManager.imageLoader(add1, pic1);
            if (picUrls.size() > 1) {
                outputName = "pic2_" + ImageUtils.getPicName();
                pic2 = TheLConstants.F_TAKE_PHOTO_ROOTPATH + outputName;
                picAmount += 1;
                ImageUtils.handleLocalPic(picUrls.get(1), TheLConstants.F_TAKE_PHOTO_ROOTPATH, outputName, TheLConstants.MAX_PIC_WIDTH, TheLConstants.MAX_PIC_HEIGHT, TheLConstants.PIC_QUALITY, true);

                L.d(TAG, " pic2 : " + pic2);

                add3.setVisibility(View.VISIBLE);
                ImageLoaderManager.imageLoader(add2, pic2);
                if (picUrls.size() > 2) {
                    outputName = "pic3_" + ImageUtils.getPicName();
                    pic3 = TheLConstants.F_TAKE_PHOTO_ROOTPATH + outputName;
                    picAmount += 1;
                    ImageUtils.handleLocalPic(picUrls.get(2), TheLConstants.F_TAKE_PHOTO_ROOTPATH, outputName, TheLConstants.MAX_PIC_WIDTH, TheLConstants.MAX_PIC_HEIGHT, TheLConstants.PIC_QUALITY, true);

                    L.d(TAG, " pic3 : " + pic3);

                    add4.setVisibility(View.VISIBLE);
                    ImageLoaderManager.imageLoader(add3, pic3);
                    if (picUrls.size() > 3) {
                        outputName = "pic4_" + ImageUtils.getPicName();
                        pic4 = TheLConstants.F_TAKE_PHOTO_ROOTPATH + outputName;
                        picAmount += 1;
                        ImageUtils.handleLocalPic(picUrls.get(3), TheLConstants.F_TAKE_PHOTO_ROOTPATH, outputName, TheLConstants.MAX_PIC_WIDTH, TheLConstants.MAX_PIC_HEIGHT, TheLConstants.PIC_QUALITY, true);

                        L.d(TAG, " pic4 : " + pic4);

                        ImageLoaderManager.imageLoader(add4, pic4);
                    }
                }
            }
        }
    }

    /**
     * 选好图片后刷新界面(从第二张照片开始选)
     *
     * @param path
     * @param selectAmountLimit
     */
    private void photoSelected(String path, int selectAmountLimit) {
        if (TextUtils.isEmpty(path)) {
            return;
        }
        String[] picUrlsTemp = path.split(",");
        List<String> picUrls = new ArrayList<String>();
        for (int i = 0; i < picUrlsTemp.length; i++) {
            if (ImageUtils.isImageLegal(picUrlsTemp[i], 320, 320)) {
                picUrls.add(picUrlsTemp[i]);
            }
        }
        if (picUrls.size() < picUrlsTemp.length) {
            DialogUtil.showToastShort(this, getString(R.string.info_illegal_photo));
        }
        picAmount = 4 - selectAmountLimit;
        String outputName;
        switch (selectAmountLimit) {
            case 1://从第四张照片位置选图片
                if (picUrls.size() > 0) {
                    outputName = "pic4_" + ImageUtils.getPicName();
                    pic4 = TheLConstants.F_TAKE_PHOTO_ROOTPATH + outputName;
                    picAmount += 1;
                    ImageUtils.handleLocalPic(picUrls.get(0), TheLConstants.F_TAKE_PHOTO_ROOTPATH, outputName, TheLConstants.MAX_PIC_WIDTH, TheLConstants.MAX_PIC_HEIGHT, TheLConstants.PIC_QUALITY, true);
                    ImageLoaderManager.imageLoader(add4, pic4);
                }
                break;
            case 2://从第三张照片的位置选图片
                if (picUrls.size() > 0) {
                    outputName = "pic3_" + ImageUtils.getPicName();
                    pic3 = TheLConstants.F_TAKE_PHOTO_ROOTPATH + outputName;
                    picAmount += 1;
                    ImageUtils.handleLocalPic(picUrls.get(0), TheLConstants.F_TAKE_PHOTO_ROOTPATH, outputName, TheLConstants.MAX_PIC_WIDTH, TheLConstants.MAX_PIC_HEIGHT, TheLConstants.PIC_QUALITY, true);
                    add4.setVisibility(View.VISIBLE);
                    ImageLoaderManager.imageLoader(add3, pic3);
                    if (picUrls.size() > 1) {
                        outputName = "pic4_" + ImageUtils.getPicName();
                        pic4 = TheLConstants.F_TAKE_PHOTO_ROOTPATH + outputName;
                        picAmount += 1;
                        ImageUtils.handleLocalPic(picUrls.get(1), TheLConstants.F_TAKE_PHOTO_ROOTPATH, outputName, TheLConstants.MAX_PIC_WIDTH, TheLConstants.MAX_PIC_HEIGHT, TheLConstants.PIC_QUALITY, true);
                        ImageLoaderManager.imageLoader(add4, pic4);
                    }
                }
                break;
            case 3://从第二张照片的位置选图片
                if (picUrls.size() > 0) {
                    outputName = "pic2_" + ImageUtils.getPicName();
                    pic2 = TheLConstants.F_TAKE_PHOTO_ROOTPATH + outputName;
                    picAmount += 1;
                    ImageUtils.handleLocalPic(picUrls.get(0), TheLConstants.F_TAKE_PHOTO_ROOTPATH, outputName, TheLConstants.MAX_PIC_WIDTH, TheLConstants.MAX_PIC_HEIGHT, TheLConstants.PIC_QUALITY, true);
                    add3.setVisibility(View.VISIBLE);
                    ImageLoaderManager.imageLoader(add2, pic2);
                    if (picUrls.size() > 1) {
                        outputName = "pic3_" + ImageUtils.getPicName();
                        pic3 = TheLConstants.F_TAKE_PHOTO_ROOTPATH + outputName;
                        picAmount += 1;
                        ImageUtils.handleLocalPic(picUrls.get(1), TheLConstants.F_TAKE_PHOTO_ROOTPATH, outputName, TheLConstants.MAX_PIC_WIDTH, TheLConstants.MAX_PIC_HEIGHT, TheLConstants.PIC_QUALITY, true);
                        add4.setVisibility(View.VISIBLE);
                        ImageLoaderManager.imageLoader(add3, pic3);
                        if (picUrls.size() > 2) {
                            outputName = "pic4_" + ImageUtils.getPicName();
                            pic4 = TheLConstants.F_TAKE_PHOTO_ROOTPATH + outputName;
                            picAmount += 1;
                            ImageUtils.handleLocalPic(picUrls.get(2), TheLConstants.F_TAKE_PHOTO_ROOTPATH, outputName, TheLConstants.MAX_PIC_WIDTH, TheLConstants.MAX_PIC_HEIGHT, TheLConstants.PIC_QUALITY, true);
                            ImageLoaderManager.imageLoader(add4, pic4);
                        }
                    }
                }
                break;
        }
    }

    protected void setLayout() {
        setContentView(R.layout.activity_report);
    }

    protected void findViewById() {
        lin_back = findViewById(R.id.lin_back);
        lin_done = findViewById(R.id.lin_done);
        img_done = findViewById(R.id.img_done);
        setDoneButtonEnabled(false);
        txt_title = findViewById(R.id.txt_title);
        optional_layout = findViewById(R.id.optional_layout);
        heading1 = findViewById(R.id.heading1);
        heading2 = findViewById(R.id.heading2);
        number = findViewById(R.id.number);
        rel_add_pics = findViewById(R.id.rel_add_pics);
        add = this.findViewById(R.id.add);
        add1 = this.findViewById(R.id.add1);
        add2 = this.findViewById(R.id.add2);
        add3 = this.findViewById(R.id.add3);
        add4 = this.findViewById(R.id.add4);
        edt_report_reason = findViewById(R.id.edt_report_reason);
        listView = findViewById(R.id.listView);
    }

    private void saveReportedUserId() {
        String reportUserIds = SharedPrefUtils.getString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.REPORT_USER_LIST, "");
        if (TextUtils.isEmpty(reportUserIds)) {
            SharedPrefUtils.setString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.REPORT_USER_LIST, userId);
        } else {
            SharedPrefUtils.setString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.REPORT_USER_LIST, reportUserIds + "," + userId);
        }
    }

    private void submit() {

        L.d("ReportActivity", " submit : ");

        if (picNum == uploadSucceedNum) {
            StringBuilder urls = new StringBuilder();
            if (!TextUtils.isEmpty(picUrl1)) {
                urls.append(picUrl1).append(",");
                if (!TextUtils.isEmpty(picUrl2)) {
                    urls.append(picUrl2).append(",");
                    if (!TextUtils.isEmpty(picUrl3)) {
                        urls.append(picUrl3).append(",");
                        if (!TextUtils.isEmpty(picUrl4)) {
                            urls.append(picUrl4).append(",");
                        }
                    }
                }
            }
            if (urls.length() > 0) {
                urls.deleteCharAt(urls.length() - 1);
            }

            L.d("ReportActivity", " urls : " + urls);

            if (REPORT_TYPE_REPORT_USER.equals(type)) {
                L.d("ReportActivity", " REPORT_TYPE_REPORT_USER reportUser : ");
                reportUser(userId, adapter.getReasonType(selected), edt_report_reason.getText().toString().trim(), urls.toString(), String.valueOf(TYPE_USER), reportContent);

            } else if (REPORT_TYPE_BUG_FEEDBACK.equals(type)) {
                bugFeedback(adapter.getReasonType(selected), urls.toString(), edt_report_reason.getText().toString().trim());
            } else if (REPORT_TYPE_ANCHOR.equals(type)) {
                postReportLiveShowUser(userId, adapter.getReasonType(selected), edt_report_reason.getText().toString().trim(), urls.toString(), String.valueOf(TYPE_ANCHOR), reportContent, liveRoomId);
            } else if (REPORT_TYPE_AUDIENCE.equals(type)) {
                postReportLiveShowUser(userId, adapter.getReasonType(selected), edt_report_reason.getText().toString().trim(), urls.toString(), String.valueOf(TYPE_AUDIENCE), reportContent, liveRoomId);
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if ((edt_report_reason != null && !TextUtils.isEmpty(edt_report_reason.getText().toString().trim())) || picAmount > 0) {
                DialogUtil.showConfirmDialog(ReportActivity.this, "", getString(R.string.updatauserinfo_activity_quit_confirm), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                        finish();
                    }
                });
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void finish() {
        ViewUtils.hideSoftInput(this, edt_report_reason);
        super.finish();
    }

    private void setDoneButtonEnabled(boolean enabled) {
        if (enabled) {
            img_done.setImageResource(R.mipmap.btn_nav_complete_nor);
            lin_done.setEnabled(true);
        } else {
            img_done.setImageResource(R.mipmap.btn_nav_complete_pre);
            lin_done.setEnabled(false);
        }
    }

    private void uploadPic() {
        picNum = 0;
        uploadSucceedNum = 0;
        String uploadPath;
        String uploadRequestMD5;
        picNum += 1;
        uploadPath = RequestConstants.UPLOAD_FILE_ROOT_PATH_REPORT + ShareFileUtils.getString(ShareFileUtils.ID, "") + "/" + MD5Utils.calculateMD5(new File(pic1)) + ".jpg";
        uploadRequestMD5 = getUploadToken(System.currentTimeMillis() + "", "", uploadPath);
        requestMD5s.put(uploadRequestMD5, pic1);
        picUploadPaths.put(pic1, uploadPath);
        if (!TextUtils.isEmpty(pic2)) {
            picNum += 1;
            uploadPath = RequestConstants.UPLOAD_FILE_ROOT_PATH_REPORT + ShareFileUtils.getString(ShareFileUtils.ID, "") + "/" + MD5Utils.calculateMD5(new File(pic2)) + ".jpg";
            uploadRequestMD5 = getUploadToken(System.currentTimeMillis() + "", "", uploadPath);
            requestMD5s.put(uploadRequestMD5, pic2);
            picUploadPaths.put(pic2, uploadPath);
            if (!TextUtils.isEmpty(pic3)) {
                picNum += 1;
                uploadPath = RequestConstants.UPLOAD_FILE_ROOT_PATH_REPORT + ShareFileUtils.getString(ShareFileUtils.ID, "") + "/" + MD5Utils.calculateMD5(new File(pic3)) + ".jpg";
                uploadRequestMD5 = getUploadToken(System.currentTimeMillis() + "", "", uploadPath);
                requestMD5s.put(uploadRequestMD5, pic3);
                picUploadPaths.put(pic3, uploadPath);
                if (!TextUtils.isEmpty(pic4)) {
                    picNum += 1;
                    uploadPath = RequestConstants.UPLOAD_FILE_ROOT_PATH_REPORT + ShareFileUtils.getString(ShareFileUtils.ID, "") + "/" + MD5Utils.calculateMD5(new File(pic4)) + ".jpg";
                    uploadRequestMD5 = getUploadToken(System.currentTimeMillis() + "", "", uploadPath);
                    requestMD5s.put(uploadRequestMD5, pic4);
                    picUploadPaths.put(pic4, uploadPath);
                }
            }
        }

    }

    private String getUploadToken(String clientTime, String bucket, final String path) {

        HashMap<String, String> data = new HashMap<>();
        if (!TextUtils.isEmpty(bucket))
            data.put(RequestConstants.I_BUCKET, bucket);
        data.put(RequestConstants.I_CLIENT_TIME, clientTime);
        data.put(RequestConstants.I_PATH, path);

        Map<String, String> map = MD5Utils.getDefaultHashMap();
        map.putAll(data);

        final String contentMD5 = MD5Utils.md5(map.toString());

        Flowable<UploadTokenBean> flowable = DefaultRequestService.createAllRequestService().getUploadToken(MD5Utils.generateSignatureForMap(data));
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<UploadTokenBean>() {
            @Override
            public void onNext(UploadTokenBean uploadTokenBean) {
                super.onNext(uploadTokenBean);

                if (!TextUtils.isEmpty(uploadTokenBean.data.uploadToken)) {

                    final String picLocalPath = requestMD5s.get(contentMD5);

                    if (!TextUtils.isEmpty(picLocalPath)) {// 上传日志图片
                        final String picUploadPath = picUploadPaths.get(picLocalPath);

                        if (!TextUtils.isEmpty(picUploadPath)) {

                            UploadManager uploadManager = new UploadManager();
                            uploadManager.put(new File(picLocalPath), picUploadPath, uploadTokenBean.data.uploadToken, new UpCompletionHandler() {
                                @Override
                                public void complete(String key, ResponseInfo info, JSONObject response) {

                                    if (info != null && info.statusCode == 200 && picUploadPath.equals(key)) {
                                        if (picLocalPath.equals(pic1)) {// 上传第一张图成功
                                            picUrl1 = RequestConstants.FILE_BUCKET + key;
                                            uploadSucceedNum += 1;
                                        } else if (picLocalPath.equals(pic2)) {// 上传第二张图成功
                                            picUrl2 = RequestConstants.FILE_BUCKET + key;
                                            uploadSucceedNum += 1;
                                        } else if (picLocalPath.equals(pic3)) {// 上传第三张图成功
                                            picUrl3 = RequestConstants.FILE_BUCKET + key;
                                            uploadSucceedNum += 1;
                                        } else if (picLocalPath.equals(pic4)) {// 上传第四张图成功
                                            picUrl4 = RequestConstants.FILE_BUCKET + key;
                                            uploadSucceedNum += 1;
                                        }
                                        submit();
                                    }
                                }
                            }, null);
                        }
                    }
                }
            }
        });

        return contentMD5;
    }

    private void blockThisMoment(final String momentsId, String reasonType) {
        Flowable<BaseDataBean> flowable = DefaultRequestService.createAllRequestService().blockThisMoment(momentsId, reasonType);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
            @Override
            public void onNext(BaseDataBean data) {
                super.onNext(data);
                closeLoading();
                String hideMomentsList = SharedPrefUtils.getString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.HIDE_MOMENTS_LIST, "");
                if (TextUtils.isEmpty(hideMomentsList)) {
                    SharedPrefUtils.setString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.HIDE_MOMENTS_LIST, momentsId);
                } else {
                    SharedPrefUtils.setString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.HIDE_MOMENTS_LIST, hideMomentsList + "," + momentsId);
                }
                DialogUtil.showToastShort(ReportActivity.this, getString(R.string.my_block_user_moments_activity_block_moment_succeed));
                Intent intent = new Intent();
                intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, momentsId);
                setResult(TheLConstants.RESULT_CODE_BLOCK_THIS_MOMENT, intent);
                finish();

                MomentBlackUtils.saveOneBlackMoment(momentsId);
                MomentBlackUtils.sendBlackMomentSuccessBroakcast(momentsId);

            }
        });
        //        MomentBlackUtils.blackThisMoment(momentsId, reasonType);
    }

    private void reportMoment(final String momentsId, String reasonType, String reasonContent) {

        Flowable<BaseDataBean> flowable = DefaultRequestService.createAllRequestService().reportMoment(momentsId, reasonType, reasonContent);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
            @Override
            public void onNext(BaseDataBean data) {
                super.onNext(data);
                closeLoading();
                String reportMomentsIds = SharedPrefUtils.getString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.REPORT_MOMENTS_LIST, "");
                if (TextUtils.isEmpty(reportMomentsIds)) {
                    SharedPrefUtils.setString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.REPORT_MOMENTS_LIST, momentsId);
                } else {
                    SharedPrefUtils.setString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.REPORT_MOMENTS_LIST, reportMomentsIds + "," + momentsId);
                }
                DialogUtil.showToastShort(ReportActivity.this, getString(R.string.userinfo_activity_report_success));
                Intent intent = new Intent();
                intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, momentsId);
                setResult(TheLConstants.RESULT_CODE_REPORT_MOMENT_SUCCEED, intent);
                finish();
            }
        });
    }

    private void reportUser(String userId, String reasonType, String reasonContent, String url, String type, String commentWord) {

        // Flowable<BaseDataBean> flowable = DefaultRequestService.createAllRequestService().reportUser(userId, String.valueOf(reasonType), imageUrlList, reasonContent);
        Flowable<BaseDataBean> flowable = RequestBusiness.getInstance().postReportUser(userId, reasonType, reasonContent, url, type, commentWord);

        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
            @Override
            public void onNext(BaseDataBean data) {
                super.onNext(data);

                closeLoading();
                saveReportedUserId();
                DialogUtil.showToastShort(ReportActivity.this, getString(R.string.userinfo_activity_report_success));
                finish();
            }
        });
    }

    private void bugFeedback(String feedBackType, String imageUrlList, String feedBackContent) {
        Flowable<BaseDataBean> flowable = DefaultRequestService.createAllRequestService().bugFeedback(feedBackType, imageUrlList, feedBackContent);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
            @Override
            public void onNext(BaseDataBean data) {
                super.onNext(data);

                heading1.setText(getString(R.string.report_activity_feedback_heading1));
                heading2.setText(getString(R.string.report_activity_feedback_heading2));
                txt_title.setText(getString(R.string.report_activity_feedback_title));
                optional_layout.setVisibility(View.VISIBLE);
                edt_report_reason.setHint(getString(R.string.report_activity_feedback_heading2));
                rel_add_pics.setVisibility(View.VISIBLE);
                adapter = new ReportReasonAdapter(new String[]{getString(R.string.report_activity_feedback_reason1), getString(R.string.report_activity_feedback_reason2), getString(R.string.report_activity_feedback_reason3)});
            }
        });
    }

    private void postReportLiveShowUser(String userId, String reasonType, String reasonContent, String url, String type, String commentWord, String roomId) {
        Flowable<BaseDataBean> flowable = RequestBusiness.getInstance().postReportLiveShowUser(userId, reasonType, reasonContent, url, type, commentWord, roomId);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
            @Override
            public void onNext(BaseDataBean data) {
                super.onNext(data);

                closeLoading();
                DialogUtil.showToastShort(ReportActivity.this, getString(R.string.userinfo_activity_report_success));
                finish();
            }
        });
    }

    @Override
    public void onBlackOneMoment(String momentId) {

        finish();
    }

    @Override
    public void onBlackherMoment(String userId) {

    }

}