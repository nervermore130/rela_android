package com.thel.modules.live.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;

import androidx.core.content.ContextCompat;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.constants.TheLConstants;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.live.bean.LiveRoomBean;
import com.thel.modules.live.bean.LiveRoomMsgBean;
import com.thel.modules.live.in.LiveTextSizeChangedListener;
import com.thel.modules.live.utils.LiveUtils;
import com.thel.ui.widget.LinearGradientRL;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;
import com.thel.utils.L;
import com.thel.utils.Utils;

import java.util.List;

/**
 * Created by waiarl on 2017/10/27.
 */

public class LiveRoomChatAdapter extends BaseRecyclerViewAdapter<LiveRoomMsgBean> implements LiveTextSizeChangedListener {
    private static final String TAG = "LiveRoomChatAdapter";
    private final float giftIconSize;
    private final int levelIconSize;
    private int vipIconSize;
    private int textSize;

    public static final int STANDARD_TEXT_SIZE = 13;//标准字体大小 textsize
    public static final int LARGE_TEXT_SIZE = 15;//大号字体 textsize
    public static final int BIGGEST_TEXT_SIZE = 17;//最大字体 textsize
    private ChatItemClickListener listener;

    public LiveRoomChatAdapter(List<LiveRoomMsgBean> data, int textSize) {
        super(R.layout.adapter_live_room_chat_item_1, data);
        vipIconSize = Utils.dip2px(TheLApp.getContext(), 17);
        levelIconSize = (int) TheLApp.getContext().getResources().getDimension(R.dimen.live_chat_level_icon_height);
        this.textSize = getTextSize(textSize);
        giftIconSize = TheLApp.getContext().getResources().getDimension(R.dimen.gift_icon_size);
    }

    @SuppressLint("StringFormatMatches")
    @Override
    protected void convert(BaseViewHolder helper, final LiveRoomMsgBean item) {
        try {
            final TextView text = helper.getView(R.id.text);
            final ImageView avatar = helper.getView(R.id.avatar);
            final int position = helper.getLayoutPosition() - getHeaderLayoutCount();
            final ImageView img_gift = helper.getView(R.id.img_gift);
            final LinearGradientRL lin_content = helper.getView(R.id.lin_content);

            L.d(TAG, " item.type : " + item.type);

            if (!TextUtils.isEmpty(item.gradualChangeColor1) && !TextUtils.isEmpty(item.gradualChangeColor2)) {
                int startColor = Color.parseColor(item.gradualChangeColor1);
                int endColor = Color.parseColor(item.gradualChangeColor2);
                lin_content.setBGColor(startColor, endColor);
            } else {
                switch (item.type) {
                    case LiveRoomMsgBean.TYPE_CONNECT_SUCCEED:
                    case LiveRoomMsgBean.TYPE_CONNECTING:
                    case LiveRoomMsgBean.TYPE_CONNECT_FAILED:
                    case LiveRoomMsgBean.TYPE_CONNECT_INTERRUPTED:
                        lin_content.setBGColor(TheLApp.getContext().getResources().getColor(R.color.yellow));
                        break;
                    case LiveRoomMsgBean.TYPE_NOTICE:
                        lin_content.setBGColor(Color.parseColor("#CCf4cd47"));
                        break;
                    case LiveRoomMsgBean.TYPE_GIFT_MSG:
                        lin_content.setBGColor(Color.parseColor("#fe417a"), Color.parseColor("#ffad02"));
                        break;
                    case LiveRoomMsgBean.TYPE_FOLLOW:
                        lin_content.setBGColor(Color.parseColor("#CC4CDA64"));
                        break;
                    case LiveRoomMsgBean.TYPE_RECOMM:
                    case LiveRoomMsgBean.TYPE_MIC_SORT:
                    case LiveRoomMsgBean.TYPE_MIC_SORT_TIPS:
                        lin_content.setBGColor(Color.parseColor("#CC4bbabc"));
                        break;
                    case LiveRoomMsgBean.TYPE_SHARETO:
                        lin_content.setBGColor(Color.parseColor("#CCFFB61C"));
                        break;
                    case LiveRoomMsgBean.TYPE_GIFTCOMBO:
                        lin_content.setBGColor(Color.parseColor("#CCFF3366"));
                        break;
                    case LiveRoomMsgBean.TYPE_JOIN:
                    case LiveRoomMsgBean.TYPE_BANED:
                        lin_content.setBGColor(Color.parseColor("#994BBABC"));
                        break;
                    case LiveRoomMsgBean.TYPE_TOP_TODAY:
                        if (item.rank == 1) {
                            lin_content.setBGColor(getColor(R.color.live_rank_1_start), getColor(R.color.live_rank_1_end));
                        } else if (item.rank == 2) {
                            lin_content.setBGColor(getColor(R.color.live_rank_2_start), getColor(R.color.live_rank_2_end));
                        } else if (item.rank == 3) {
                            lin_content.setBGColor(getColor(R.color.live_rank_3_start), getColor(R.color.live_rank_3_end));
                        } else {
                            lin_content.setBGColor(getColor(R.color.white));
                        }

                        break;
                    default:
                        if (item.userLevel >= 30) {
                            lin_content.setBGColor(Color.parseColor("#353433"), Color.parseColor("#11100E"));
                        } else if (item.userLevel == 29) {
                            lin_content.setBGColor(Color.parseColor("#cc5600FF"), Color.parseColor("#ccFF0E0E"));
                        } else {
                            lin_content.setBGColor(Color.parseColor("#CCFFFFFF"), Color.parseColor("#CCFFFFFF"));
                        }
                        break;
                }
            }


            text.setTextSize(textSize);
            avatar.setVisibility(View.INVISIBLE);
            img_gift.setVisibility(View.GONE);
            text.setOnClickListener(null);
            avatar.setOnClickListener(null);

            if (item.type == null) {
                item.type = "";
            }

            if (LiveRoomMsgBean.TYPE_CONNECT_SUCCEED.equals(item.type)//消息连接状态
                    || LiveRoomMsgBean.TYPE_CONNECTING.equals(item.type)
                    || LiveRoomMsgBean.TYPE_CONNECT_FAILED.equals(item.type)
                    || LiveRoomMsgBean.TYPE_CONNECT_INTERRUPTED.equals(item.type)) {
                if (LiveRoomMsgBean.TYPE_CONNECT_SUCCEED.equals(item.type)) {
                    text.setText(R.string.live_chat_connected);
                } else if (LiveRoomMsgBean.TYPE_CONNECTING.equals(item.type)) {
                    text.setText(R.string.live_chat_connecting);
                } else if (LiveRoomMsgBean.TYPE_CONNECT_FAILED.equals(item.type)) {
                    text.setText(R.string.live_chat_connect_fail);
                } else {
                    text.setText(R.string.live_chat_connect_interrupted);
                }
                text.setTextColor(TheLApp.getContext().getResources().getColor(R.color.white));
            } else if (LiveRoomMsgBean.TYPE_NOTICE.equals(item.type)) {//通知
                if (!TextUtils.isEmpty(item.avatar)) {
                    avatar.setVisibility(View.VISIBLE);
                    setAvatarImage(helper, avatar, item);
                }

                text.setTextColor(TheLApp.getContext().getResources().getColor(R.color.text_color_black));
                text.setText(buildMsg(item.nickName, item.content, R.color.black, item.backgroundTextColor, true, 0, -1, item.iconSwitch));
            } else if (LiveRoomMsgBean.TYPE_GIFT_MSG.equals(item.type)) {//礼物

                L.d(TAG, " avatar : " + avatar);

                if (item.iconSwitch == 1) {
                    avatar.setVisibility(View.VISIBLE);
                    setAvatarImage(helper, avatar, item);
                } else {
                    avatar.setVisibility(View.GONE);
                }

                String content;

                if (item.toNickname != null && item.toNickname.length() > 0) {
                    content = String.format(TheLApp.context.getResources().getString(R.string.send_gift_to_somebody), item.toNickname, String.valueOf(item.combo), item.giftTitle);
                } else {
                    content = TheLApp.getContext().getString(R.string.sent_xx, item.combo + "", item.giftTitle);
                }

                text.setText(buildMsg(item.nickName, content, R.color.white, item.backgroundTextColor, true, 0, item.userLevel, item.iconSwitch));
                text.setTextColor(TheLApp.getContext().getResources().getColor(R.color.white));

                setListener(avatar, text, position, item);
            } else if (LiveRoomMsgBean.TYPE_FOLLOW.equals(item.type)) {//关注了主播
                avatar.setVisibility(View.VISIBLE);
                setAvatarImage(helper, avatar, item);
                text.setText(buildMsg(item.nickName, item.content, R.color.white, item.backgroundTextColor, true, 0, item.userLevel, item.iconSwitch));
                text.setTextColor(TheLApp.getContext().getResources().getColor(R.color.white));
                setListener(avatar, text, position, item);
            } else if (LiveRoomMsgBean.TYPE_RECOMM.equals(item.type)) {//推荐了主播
                avatar.setVisibility(View.VISIBLE);
                setAvatarImage(helper, avatar, item);
                text.setText(buildMsg(item.nickName, item.content, R.color.white, item.backgroundTextColor, true, 0, item.userLevel, item.iconSwitch));
                text.setTextColor(TheLApp.getContext().getResources().getColor(R.color.white));
                setListener(avatar, text, position, item);
            } else if (LiveRoomMsgBean.TYPE_SHARETO.equals(item.type)) {//分享了直播
                avatar.setVisibility(View.VISIBLE);
                setAvatarImage(helper, avatar, item);
                text.setText(buildMsg(item.nickName, item.content, R.color.white, item.backgroundTextColor, true, 0, item.userLevel, item.iconSwitch));
                text.setTextColor(TheLApp.getContext().getResources().getColor(R.color.white));
                setListener(avatar, text, position, item);
            } else if (LiveRoomMsgBean.TYPE_GIFTCOMBO.equals(item.type)) {//连击礼物（送了XXX个xxx）
                avatar.setVisibility(View.VISIBLE);
                setAvatarImage(helper, avatar, item);

                String content;

                if (item.toNickname != null && item.toNickname.length() > 0) {
                    content = String.format(TheLApp.context.getResources().getString(R.string.send_gift_to_somebody), item.toNickname, String.valueOf(item.combo), item.giftTitle);
                } else {
                    content = item.content;
                }

                text.setText(buildMsg(item.nickName, content, R.color.white, item.backgroundTextColor, true, 0, item.userLevel, item.iconSwitch));
                text.setTextColor(TheLApp.getContext().getResources().getColor(R.color.white));
                if (!TextUtils.isEmpty(item.giftIcon)) {
                    if (item.iconSwitch == 1) {
                        img_gift.setVisibility(View.VISIBLE);
                        helper.setImageViewUrl(R.id.img_gift, item.giftIcon, giftIconSize, giftIconSize);
                    } else {
                        img_gift.setVisibility(View.GONE);
                    }

                }
                setListener(avatar, text, position, item);
            } else {//正常普通发言
                avatar.setVisibility(View.VISIBLE);
                if (LiveRoomMsgBean.TYPE_JOIN.equals(item.type) || LiveRoomMsgBean.TYPE_BANED.equals(item.type)) {//屏蔽或者加入房间
                    text.setTextColor(Color.WHITE);
                    if (LiveRoomMsgBean.TYPE_JOIN.equals(item.type)) {
                        text.setText(item.nickName + " " + TheLApp.getContext().getString(R.string.enter_live_room));
                    } else if (LiveRoomMsgBean.TYPE_BANED.equals(item.type)) {
                        text.setText(item.nickName + " " + TheLApp.getContext().getString(R.string.live_someone_has_been_baned));
                    }
                    setAvatarImage(helper, avatar, item);
                } else if (LiveRoomMsgBean.TYPE_MIC_SORT.equals(item.type) || LiveRoomMsgBean.TYPE_MIC_SORT_TIPS.equals(item.type)) {//排麦模式
                    if (item.content != null) {
                        text.setTextColor(TheLApp.getContext().getResources().getColor(R.color.white));
                        // text.setText(liveRoomBean.user.nickName + item.content);
                        text.setText(buildMsg(liveRoomBean.user.nickName, item.content, R.color.white, item.backgroundTextColor, true, liveRoomBean.user.level, liveRoomBean.userLevel, item.iconSwitch));
                    }
                    LiveRoomMsgBean liveRoomMsgBean = new LiveRoomMsgBean();
                    liveRoomMsgBean.avatar = liveRoomBean.user.avatar;
                    setAvatarImage(helper, avatar, liveRoomMsgBean);
                } else if (LiveRoomMsgBean.TYPE_TOP_TODAY.equals(item.type)) {//直播间内守护日榜前三产生变动时触发消息
                    avatar.setVisibility(View.VISIBLE);
                    avatar.setImageResource(R.mipmap.btn_inform_chat);
                    if (item.rank == 1) {
                        text.setText(mContext.getString(R.string.guard_message_notice1, item.nickName, liveRoomBean.user.nickName));
                    } else if (item.rank == 2) {
                        text.setText(mContext.getString(R.string.guard_message_notice2, item.nickName, liveRoomBean.user.nickName));
                    } else if (item.rank == 3) {
                        text.setText(mContext.getString(R.string.guard_message_notice3, item.nickName, liveRoomBean.user.nickName));
                    }
                    text.setTextColor(TheLApp.getContext().getResources().getColor(R.color.white));
                } else {//普通发言
                    if (item.content != null) {
                        text.setTextColor(item.vip == 0 ? TheLApp.getContext().getResources().getColor(R.color.text_color_black) : Color.parseColor("#ff6666"));
                        text.setText(buildMsg(item.nickName, item.content, item.vip == 0 ? R.color.text_color_green : R.color.text_color_red, item.backgroundTextColor, true, item.vip, item.userLevel, item.iconSwitch));
                        if (item.userLevel >= 30) {
                            text.setTextColor(Color.parseColor("#FFDC95"));
                            text.setText(buildMsg(item.nickName, item.content, R.color.chat_content, item.backgroundTextColor, true, item.vip, item.userLevel, item.iconSwitch));

                        }
                    }
                    setAvatarImage(helper, avatar, item);
                }

                if (!TextUtils.isEmpty(item.backgroundTextColor)) {
                    text.setTextColor(Color.parseColor(item.backgroundTextColor));
                }

                setListener(avatar, text, position, item);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 设置背景
     *
     * @param view
     * @param position
     */
    private void setRelBg(View view, int position) {
        int[] colors = new int[]{getColor(R.color.transparent), getColor(R.color.transparent)};
        if (position == 1) {
            colors = new int[]{getColor(R.color.live_rank_1_start),
                    getColor(R.color.live_rank_1_end)};
        } else if (position == 2) {
            colors = new int[]{getColor(R.color.live_rank_2_start),
                    getColor(R.color.live_rank_2_end)};
        } else if (position == 3) {
            colors = new int[]{getColor(R.color.live_rank_3_start),
                    getColor(R.color.live_rank_3_end)};
        }
        final GradientDrawable drawable = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, colors);
        drawable.setCornerRadius(Utils.dip2px(TheLApp.getContext(), 15));
        view.setBackground(drawable);
    }

    private int getColor(int color) {
        return ContextCompat.getColor(TheLApp.getContext(), color);
    }

    /**
     * 设置头像
     *
     * @param helper
     * @param img_avatar
     * @param item
     */
    private void setAvatarImage(BaseViewHolder helper, ImageView img_avatar, LiveRoomMsgBean item) {
        //  helper.setImageUrl(R.id.avatar, item.avatar, TheLConstants.AVATAR_SMALL_SIZE, TheLConstants.AVATAR_SMALL_SIZE);
        ImageLoaderManager.imageLoaderDefaultCircle(img_avatar, R.mipmap.icon_user, item.avatar, TheLConstants.AVATAR_SMALL_SIZE, TheLConstants.AVATAR_SMALL_SIZE);

    }

    private void setListener(ImageView avatar, TextView text, final int position, final LiveRoomMsgBean item) {
        if (!Utils.isMyself(item.userId)) {
            avatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.clickAvatar(v, position, item);
                    }
                }
            });
            text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.clickText(v, position, item);
                    }
                }
            });
        }
    }

    public void setLiveChatListener(ChatItemClickListener listener) {
        this.listener = listener;
    }

    private LiveRoomBean liveRoomBean;

    public void setLiveRoomBean(LiveRoomBean liveRoomBean) {
        this.liveRoomBean = liveRoomBean;
    }

    @Override
    public void textSizeChanged(int type) {
        final int size = getTextSize(type);
        if (textSize != size) {
            textSize = size;
            notifyDataSetChanged();
        }
    }


    public interface ChatItemClickListener {
        void clickAvatar(View v, int position, LiveRoomMsgBean bean);

        void clickText(View v, int position, LiveRoomMsgBean bean);
    }


    private int getTextSize(int textSize) {
        vipIconSize = Utils.dip2px(TheLApp.getContext(), 17);
        switch (textSize) {
            case LiveTextSizeChangedListener.TEXT_SIZE_LARGE:
                vipIconSize = Utils.dip2px(TheLApp.getContext(), 19);
                return LARGE_TEXT_SIZE;
            case LiveTextSizeChangedListener.TEXT_SIZE_BIGGEST:
                vipIconSize = Utils.dip2px(TheLApp.getContext(), 21);

                return BIGGEST_TEXT_SIZE;
        }
        return STANDARD_TEXT_SIZE;
    }

    /**
     * 显示内容 SP
     *
     * @return
     */
    private SpannableString buildMsg(String nickName, String content, int nickNameColor, String backgroundTextColor, boolean nickNameBold, int vip, int userLevel, int iconSwitch) {
        if (nickName == null) {
            return new SpannableString(content);
        }
        SpannableString sp = null;
        String userLevelStr1;
        String userLevelStr2 = "";
        String vipStr1;
        String vipStr2 = "";
        int start1 = 0, end1 = 0, start2 = 0, end2 = 0;

        if (userLevel >= 0 && iconSwitch == 1) {
            userLevelStr1 = userLevel + "";
            userLevelStr2 = userLevelStr1 + " ";
            end1 = start1 + userLevelStr1.length();
        }

        if (vip > 0) {
            vipStr1 = vip + "";
            vipStr2 = vipStr1 + " ";
            start2 = start1 + iconSwitch == 1 ? userLevelStr2.length() : 0;
            end2 = start2 + vipStr1.length();
        }

        sp = new SpannableString((iconSwitch == 1 ? userLevelStr2 : "") + vipStr2 + nickName + ": " + content);

        //会员
        final Bitmap vipBitmap = getVipBitmap(vip);
        final MyIm myVipLevelIm = new MyIm(TheLApp.getContext(), vipBitmap);//为调整文字与会员图片保持居中
        sp.setSpan(myVipLevelIm, start2, end2, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);

        if (iconSwitch == 1) {
            //用户等级
            final Bitmap userLevelBitmap = getUserLevelBitmap(userLevel);
            final MyIm myUserLevelIm = new MyIm(TheLApp.getContext(), userLevelBitmap);
            sp.setSpan(myUserLevelIm, start1, end1, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        }

        int nickNameStart = userLevelStr2.length() + vipStr2.length();
        final int nickNameEnd = nickNameStart + nickName.length() + 1;

        int nickNameTextColor = ContextCompat.getColor(TheLApp.getContext(), nickNameColor);

        if (!TextUtils.isEmpty(backgroundTextColor)) {
            nickNameTextColor = Color.parseColor(backgroundTextColor);
        }

        sp.setSpan(new ForegroundColorSpan(nickNameTextColor), nickNameStart, nickNameEnd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        if (nickNameBold) {
            sp.setSpan(new StyleSpan(Typeface.BOLD), nickNameStart, nickNameEnd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        return sp;
    }

    /**
     * 获取用户等级bitmap
     *
     * @param userLevel
     * @return
     */
    private Bitmap getUserLevelBitmap(int userLevel) {
        Bitmap bitmap;
        if (userLevel >= TheLConstants.USER_LEVEL_RES.length) {
            userLevel = TheLConstants.USER_LEVEL_RES.length - 1;
        }
        if (userLevel < TheLConstants.USER_LEVEL_RES.length && userLevel >= 0) {
            bitmap = BitmapFactory.decodeResource(TheLApp.getContext().getResources(), TheLConstants.USER_LEVEL_RES[userLevel]);
        } else {
            bitmap = BitmapFactory.decodeResource(TheLApp.getContext().getResources(), TheLConstants.USER_LEVEL_RES[0]);

        }
        final int height = levelIconSize;
        final float width = LiveUtils.getLevelImageWidth(height, userLevel);

        if (bitmap != null) {
            bitmap = Bitmap.createScaledBitmap(bitmap, (int) width, height, true);
        }
        return bitmap;
    }

    /**
     * 根据vip等级获取相应的vip
     *
     * @param vip
     * @return
     */
    private Bitmap getVipBitmap(int vip) {
        Bitmap bitmap;
        if (vip >= TheLConstants.VIP_LEVEL_RES.length) {
            vip = TheLConstants.VIP_LEVEL_RES.length - 1;
        }
        if (vip < TheLConstants.VIP_LEVEL_RES.length && vip > 0) {
            bitmap = BitmapFactory.decodeResource(TheLApp.getContext().getResources(), TheLConstants.VIP_LEVEL_RES[vip]);
        } else {
            bitmap = BitmapFactory.decodeResource(TheLApp.getContext().getResources(), TheLConstants.VIP_LEVEL_RES[1]);
        }
        if (bitmap != null) {
            bitmap = Bitmap.createScaledBitmap(bitmap, vipIconSize, vipIconSize, true);
        }
        return bitmap;
    }


    public class MyIm extends ImageSpan {
        public MyIm(Context arg0, int arg1) {
            super(arg0, arg1);
        }

        public MyIm(Context arg0, Bitmap arg1) {
            super(arg0, arg1);
        }

        @Override
        public int getSize(Paint paint, CharSequence text, int start, int end, Paint.FontMetricsInt fm) {
            Drawable d = getDrawable();
            Rect rect = d.getBounds();
            if (fm != null) {
                Paint.FontMetricsInt fmPaint = paint.getFontMetricsInt();
                int fontHeight = fmPaint.bottom - fmPaint.top;
                int drHeight = rect.bottom - rect.top;

                int top = drHeight / 2 - fontHeight / 4;
                int bottom = drHeight / 2 + fontHeight / 4;

                fm.ascent = -bottom;
                fm.top = -bottom;
                fm.bottom = top;
                fm.descent = top;
            }
            return rect.right;
        }

        @Override
        public void draw(Canvas canvas, CharSequence text, int start, int end, float x, int top, int y, int bottom, Paint paint) {
            Drawable b = getDrawable();
            canvas.save();
            int transY = 0;
            transY = ((bottom - top) - b.getBounds().bottom) / 2 + top;
            canvas.translate(x, transY);
            b.draw(canvas);
            canvas.restore();
        }
    }
}
