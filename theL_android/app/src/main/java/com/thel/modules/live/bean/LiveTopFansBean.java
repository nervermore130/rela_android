package com.thel.modules.live.bean;

import com.thel.base.BaseDataBean;

import java.io.Serializable;

/**
 * Created by waiarl on 2017/4/20.
 */

public class LiveTopFansBean extends BaseDataBean implements Serializable {

    public String userId;
    public String avatar;
    public long gold;
    public int isCloaking;
}
