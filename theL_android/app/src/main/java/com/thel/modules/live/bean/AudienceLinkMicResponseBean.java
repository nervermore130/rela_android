package com.thel.modules.live.bean;

public class AudienceLinkMicResponseBean extends AudienceLinkMicRequestBean{

    public AgoraBean agora = new AgoraBean();

    @Override public String toString() {
        return "AudienceLinkMicResponseBean{" +
                "agora=" + agora +
                ", method='" + method + '\'' +
                ", x=" + x +
                ", height=" + height +
                ", y=" + y +
                ", result='" + result + '\'' +
                ", width=" + width +
                ", toUserId='" + toUserId + '\'' +
                '}';
    }
}
