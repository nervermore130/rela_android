package com.thel.modules.main.me.bean;

import com.thel.base.BaseDataBean;

import java.io.Serializable;

/**
 * @author Setsail
 */
public class VipBean extends BaseDataBean implements Serializable {

    /**
     * vip类型id
     */
    public long id;
    /**
     * 表情包名称
     */
    public String title;
    /**
     * vip类型缩略图
     */
    public String icon;
    /**
     * 价格
     */
    public String price;
    /**
     * 谷歌钱包直接显示价格
     */
    public String googlePrice;
    /**
     * vip简介
     */
    public String summary;
    /**
     * 是不是最新推荐表情
     */
    public int isHot;
    /**
     * 签名，防止数据伪造
     */
    public String sign;

    /**
     * 描述，例如：一个月的会员
     */
    public String description;
    /**
     * 月
     */
    public int months;

    public String iapId;
    /**
     * 状态码
     */
    public int status;
    /**
     * 排名
     */
    public int rank;
    /**
     * 软妹都定价
     */
    public int goldPrice;

    /**
     * 4.10.0做abtest  新人显示8折优惠
     */
    public String groupDesc;
    public String groupType;

    /**
     * 用google play支付的话需要从google play上获取价格
     */
    public boolean isPendingPrice = false;

    @Override
    public String toString() {
        return "VipBean{" + "id=" + id + ", title='" + title + '\'' + ", icon='" + icon + '\'' + ", price='" + price + '\'' + ", googlePrice='" + googlePrice + '\'' + ", summary='" + summary + '\'' + ", isHot=" + isHot + ", sign='" + sign + '\'' + ", description='" + description + '\'' + ", months=" + months + ", iapId='" + iapId + '\'' + ", status=" + status + ", rank=" + rank + ", goldPrice=" + goldPrice + ", groupDesc='" + groupDesc + '\'' + ", groupType='" + groupType + '\'' + ", isPendingPrice=" + isPendingPrice + '}';
    }
}
