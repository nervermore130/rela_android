package com.thel.modules.main.userinfo.fan;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.FollowerBean;
import com.thel.constants.TheLConstants;
import com.thel.imp.follow.FollowStatusChangedImpl;
import com.thel.ui.widget.popupwindow.UnFollowPopupWindow;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;
import com.thel.utils.UserUtils;

import java.util.List;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by waiarl on 2017/11/6.
 */

public class UserInfoFollowersAdapter extends BaseRecyclerViewAdapter<FollowerBean> {

    public UserInfoFollowersAdapter(List<FollowerBean> data) {
        super(R.layout.common_friend_list_item, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, FollowerBean bean) {
        if (bean.verifyType <= 0) {// 非加V用户，显示两行，第一行为昵称和简介，第二行为在线状态、距离、感情状态
            helper.setVisibility(R.id.line2, View.VISIBLE);
            helper.setVisibility(R.id.img_indentify, View.GONE);
            helper.setText(R.id.txt_desc, bean.intro + "");
            helper.setText(R.id.txt_distance, bean.distance);
        } else {// 加V用户，显示一行，显示昵称、认证图标、认证信息
            helper.setVisibility(R.id.line2, View.GONE);
            helper.setVisibility(R.id.img_indentify, View.VISIBLE);
            helper.setText(R.id.txt_desc, bean.verifyIntro + "");
            helper.setText(R.id.txt_distance, bean.distance);

            if (bean.verifyType == 1) {// 个人加V
                helper.setImageResource(R.id.img_indentify, R.mipmap.icn_verify_person);
            } else {// 企业加V
                helper.setImageResource(R.id.img_indentify, R.mipmap.icn_verify_enterprise);
            }
        }

        // 头像
        helper.setImageUrl(R.id.img_thumb, bean.Avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE);
        // 用户名
        helper.setText(R.id.txt_name, bean.nickName + "");

        if (UserUtils.getMyUserId().equals(bean.userId)) {
            helper.setVisibility(R.id.ll_statue_view, GONE);
        } else {
            helper.setVisibility(R.id.ll_statue_view, VISIBLE);
        }

        TextView followView = helper.getView(R.id.statue_view);
        String text = "";
        switch (bean.followStatus) {
            case 0:
                text = TheLApp.getContext().getString(R.string.userinfo_activity_follow);
                followView.setTextColor(TheLApp.getContext().getResources().getColor(R.color.text_selector));
                followView.setBackgroundResource(R.drawable.follow_selector);

                break;
            case 1:
                text = TheLApp.getContext().getString(R.string.userinfo_activity_followed);
                followView.setBackgroundResource(R.drawable.follow_unselector);
                followView.setTextColor(TheLApp.getContext().getResources().getColor(R.color.text_unselector));
                break;
            case 2:
                text = TheLApp.getContext().getString(R.string.repowder);
                followView.setBackgroundResource(R.drawable.follow_selector);
                followView.setTextColor(TheLApp.getContext().getResources().getColor(R.color.text_selector));

                break;
            case 3:
                text = TheLApp.getContext().getString(R.string.interrelated);
                followView.setBackgroundResource(R.drawable.follow_unselector);
                followView.setTextColor(TheLApp.getContext().getResources().getColor(R.color.text_unselector));
                break;
        }
        followView.setText(text);

        followView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (bean.followStatus) {
                    case 0:
                        FollowStatusChangedImpl.followUser(bean.userId + "", FollowStatusChangedImpl.ACTION_TYPE_FOLLOW, bean.nickName, bean.Avatar);
                        bean.followStatus = 1;
                        notifyDataSetChanged();
                        break;
                    case 2:
                        FollowStatusChangedImpl.followUser(bean.userId + "", FollowStatusChangedImpl.ACTION_TYPE_FOLLOW, bean.nickName, bean.Avatar);
                        bean.followStatus = 3;
                        notifyDataSetChanged();
                        break;
                    case 1:
                        UnFollowPopupWindow mUnFollowPopupWindow = new UnFollowPopupWindow(mContext, bean.nickName);
                        mUnFollowPopupWindow.setOnUnFollowListener(new UnFollowPopupWindow.OnUnFollowListener() {
                            @Override
                            public void onUnFollow() {
                                FollowStatusChangedImpl.followUser(bean.userId + "", FollowStatusChangedImpl.ACTION_TYPE_CANCEL_FOLLOW, bean.nickName, bean.Avatar);
                                bean.followStatus = 0;
                                notifyDataSetChanged();
                            }
                        });
                        mUnFollowPopupWindow.showAtLocation(v, Gravity.BOTTOM, 0, 0);
                        break;
                    case 3:
                        UnFollowPopupWindow mUnFollowPopupWindow1 = new UnFollowPopupWindow(mContext, bean.nickName);
                        mUnFollowPopupWindow1.setOnUnFollowListener(new UnFollowPopupWindow.OnUnFollowListener() {
                            @Override
                            public void onUnFollow() {
                                FollowStatusChangedImpl.followUser(bean.userId + "", FollowStatusChangedImpl.ACTION_TYPE_CANCEL_FOLLOW, bean.nickName, bean.Avatar);
                                bean.followStatus = 2;
                                notifyDataSetChanged();
                            }
                        });
                        mUnFollowPopupWindow1.showAtLocation(v, Gravity.BOTTOM, 0, 0);
                        break;
                }
            }
        });
    }

}
