package com.thel.modules.main;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import androidx.fragment.app.Fragment;

import com.thel.app.TheLApp;
import com.thel.bean.moments.MomentsCheckBean;
import com.thel.constants.TheLConstants;
import com.thel.utils.L;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by liuyun on 2017/9/14.
 */

public class MainPresenter implements MainContract.Presenter {

    private CompositeDisposable mDisposable;

    private MainContract.View mainView;

    private MessageReceiver receiver;

    private MomentsCheckReceiver momentsCheckReceiver;

    public MomentsCheckBean momentsCheck = null;

    public MainPresenter(MainContract.View mainView) {

        this.mainView = mainView;

        mDisposable = new CompositeDisposable();

        mainView.setPresenter(this);
    }

    @Override
    public void subscribe() {
        init();
        registerReceiver();
    }

    @Override
    public void unSubscribe() {
        if (mDisposable != null) {
            mDisposable.clear();
        }
    }

    @Override
    public void init() {

    }

    @Override
    public void registerReceiver() {
        if (TheLApp.context != null) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(TheLConstants.BROADCAST_NEW_MESSAGE);
            receiver = new MessageReceiver();
            TheLApp.context.registerReceiver(receiver, intentFilter);

            IntentFilter intentFilter1 = new IntentFilter();
            intentFilter1.addAction(TheLConstants.BROADCAST_MOMENTS_CHECK_ACTION);
            intentFilter1.addAction(TheLConstants.BROADCAST_FAILED_MOMENTS_CHECK_ACTION);
            intentFilter1.addAction(TheLConstants.BROADCAST_RELEASE_MOMENT_SUBMIT);
            intentFilter1.addAction(TheLConstants.BROADCAST_UPDATE_RECOMMEND_STICKER);
            intentFilter1.addAction(TheLConstants.BROADCAST_UPDATE_UNREAD_MSG_COUNT);
            intentFilter1.addAction(TheLConstants.BROADCAST_UPDATE_UNREAD_MSG_COUNT_COMPLETELY);
            intentFilter1.addAction(TheLConstants.BROADCAST_CLEAR_NEW_MOMENT_MSG);
            intentFilter1.addAction(TheLConstants.BROADCAST_CLEAR_MOMENT_MSG_COUNT);
            intentFilter1.addAction(TheLConstants.BROADCAST_REFRESH_NEED_COMPLETE_USER_INFO);
            intentFilter1.addAction(TheLConstants.BROADCAST_GOTO_LIVE_ROOMS_PAGE);
            intentFilter1.addAction(TheLConstants.BROADCAST_GOTO_THEME_PAGE);
            intentFilter1.addAction(TheLConstants.BROADCAST_GOTO_MOMENTFRAGMENTLIST_ACTIVITY_PAGE);
            intentFilter1.addAction(TheLConstants.BROADCAST_GOTO_THEME_TAB);
            intentFilter1.addAction(TheLConstants.BROADCAST_RELEASE_MOMENT_SUCCEED);
            intentFilter1.addAction(TheLConstants.BROADCAST_RELEASE_MOMENT_FAIL);
            intentFilter1.addAction(TheLConstants.BROADCAST_NEW_MOMENTS_CLEAR);
            intentFilter1.addAction(TheLConstants.BROADCAST_CLEAR_NEW_CHECK_ACTION);
            intentFilter1.addAction(TheLConstants.BROADCAST_CLEAR_LIKE_ME_NEW_CHECK_ACTION);
            intentFilter1.addAction(TheLConstants.BROADCAST_CLEAR_MOMENT_MSG_COUNT_TOP);
            intentFilter1.addAction(TheLConstants.BROADCAST_CLEAR_RELA_LISTENER_NEW_CHECK_ACTION);
            intentFilter.addAction(TheLConstants.BROADCAST_ACTION_WALKMAN_NEW);
            intentFilter1.addAction(TheLConstants.BROADCAST_GOTO_LIVE);
            intentFilter1.addAction(TheLConstants.BROADCAST_GOTO_MESSAGE);
            intentFilter1.addAction(TheLConstants.BROADCAST_GOTO_FLUTTER);
            intentFilter1.addAction(TheLConstants.BROADCAST_ACTION_APK_DOWNLOADED);
            momentsCheckReceiver = new MomentsCheckReceiver();
            TheLApp.context.registerReceiver(momentsCheckReceiver, intentFilter1);
        }
    }

    @Override
    public void initDataModel(final Fragment fragment) {

    }


    private class MessageReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            // 收到消息的时候, 更新总未读数
            if (mainView.isActive()) {
                mainView.updateUnreadTotal(1);
            }
        }
    }

    // 检查朋友圈和是否有发送失败的日志的广播接收器
    private class MomentsCheckReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            // 收到消息的时候, 更新朋友圈消息
            if (intent != null) {

                if (TheLConstants.BROADCAST_FAILED_MOMENTS_CHECK_ACTION.equals(intent.getAction())) {// 有发送失败日志或发送成功

                    if (mainView.isActive()) {
                        mainView.updateFailedMoments();
                    }

                } else if (TheLConstants.BROADCAST_MOMENTS_CHECK_ACTION.equals(intent.getAction())) {// 有新动态
                    try {
                        momentsCheck = intent.getParcelableExtra(TheLConstants.BUNDLE_KEY_MOMENTS_CHECK_BEAN);

                        if (momentsCheck != null) {

                            mainView.getMomentCheckBean(momentsCheck);
                            if (mainView.isActive()) {
                                mainView.updateMomentsAndRequestsTotal(momentsCheck);
                                mainView.updateUnReadSeeMeTotal(momentsCheck);
                                mainView.updateLikeMeTotal(momentsCheck);

                            }
                        }
                    } catch (Exception e) {
                    }
                } else if (TheLConstants.BROADCAST_RELEASE_MOMENT_SUBMIT.equals(intent.getAction())) {// 正在发布日志

                    if (mainView.isActive()) {
                        mainView.updateSendingMoments();
                    }

                } else if (TheLConstants.BROADCAST_UPDATE_RECOMMEND_STICKER.equals(intent.getAction())) {

                    if (mainView.isActive()) {
                        mainView.updateRecommendStciker();
                    }

                } else if (TheLConstants.BROADCAST_UPDATE_UNREAD_MSG_COUNT.equals(intent.getAction())) {

                    if (mainView.isActive()) {
                        mainView.refreshUnreadTotal(intent.getIntExtra(TheLConstants.COUNT, 0));
                    }

                } else if (TheLConstants.BROADCAST_UPDATE_UNREAD_MSG_COUNT_COMPLETELY.equals(intent.getAction())) {
                    if (mainView.isActive()) {
                        mainView.updateUnreadTotal(0);
                    }
                } else if (TheLConstants.BROADCAST_CLEAR_MOMENT_MSG_COUNT.equals(intent.getAction())) {
                    if (mainView.isActive()) {
                        mainView.clearMomentsTotal(2);
                    }
                } else if (TheLConstants.BROADCAST_CLEAR_NEW_MOMENT_MSG.equals(intent.getAction())) {
                    if (mainView.isActive()) {
                        mainView.clearMomentsTotal(1);
                    }
                } else if (TheLConstants.BROADCAST_REFRESH_NEED_COMPLETE_USER_INFO.equals(intent.getAction())) {
                    if (mainView.isActive()) {
                        mainView.refreshNeedCompleteUserInfo(momentsCheck);
                    }
                } else if (TheLConstants.BROADCAST_GOTO_LIVE_ROOMS_PAGE.equals(intent.getAction())) {
                    if (mainView.isActive()) {
                        mainView.gotoNearbyPage(0);
                    }
                } else if (TheLConstants.BROADCAST_GOTO_THEME_PAGE.equals(intent.getAction())) {
                    if (mainView.isActive()) {
                        mainView.gotoThemePage();
                    }
                } else if (TheLConstants.BROADCAST_GOTO_MOMENTFRAGMENTLIST_ACTIVITY_PAGE.equals(intent.getAction())) {
                    if (mainView.isActive()) {
                        mainView.gotoMomentFragmentActivityPage();
                    }
                } else if (TheLConstants.BROADCAST_GOTO_THEME_TAB.equals(intent.getAction())) {
                    if (mainView.isActive()) {
                        mainView.gotoNearbyPage(0);
                    }
                } else if (TheLConstants.BROADCAST_GOTO_LIVE.equals(intent.getAction())) {

                    L.d("FlutterPlatform", " TheLConstants.BROADCAST_GOTO_LIVE : ");

                    if (mainView.isActive()) {
                        mainView.showLivePage();
                    }
                } else if (TheLConstants.BROADCAST_GOTO_MESSAGE.equals(intent.getAction())) {
                    if (mainView.isActive()) {
                        mainView.showMessagePage();
                    }
                } else if (TheLConstants.BROADCAST_GOTO_FLUTTER.equals(intent.getAction())) {
                    if (mainView.isActive()) {
                        mainView.showFlutter();
                    }
                } else if (TheLConstants.BROADCAST_RELEASE_MOMENT_FAIL.equals(intent.getAction())) {
                    if (mainView.isActive()) {
                        mainView.releaseFailed();
                    }
                } else if (TheLConstants.BROADCAST_RELEASE_MOMENT_SUCCEED.equals(intent.getAction())) {// 发送成功，刷新数据
                    if (mainView.isActive()) {
                        final String momentsId = intent.getStringExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID);
                        long releaseTime = intent.getLongExtra(TheLConstants.BUNDLE_KEY_RELEASE_TIME, 0);
                        mainView.releaseSuccess(momentsId, releaseTime);
                    }
                } else if (TheLConstants.BROADCAST_NEW_MOMENTS_CLEAR.equals(intent.getAction())) {
                    if (mainView.isActive()) {
                        mainView.clearMomentsTotal(1);
                    }
                } else if (TheLConstants.BROADCAST_NEW_CHECK_ACTION.equals(intent.getAction())) { //谁来看过我提示数字

                    try {
                        momentsCheck = intent.getParcelableExtra(TheLConstants.BUNDLE_KEY_MOMENTS_CHECK_BEAN);
                        if (momentsCheck != null && mainView.isActive()) {
                            mainView.updateUnReadSeeMeTotal(momentsCheck);
                            mainView.updateLikeMeTotal(momentsCheck);
                        }
                    } catch (Exception e) {
                    }
                } else if (TheLConstants.BROADCAST_CLEAR_RELA_LISTENER_NEW_CHECK_ACTION.equals(intent.getAction()) || TheLConstants.BROADCAST_CLEAR_NEW_CHECK_ACTION.equals(intent.getAction()) || TheLConstants.BROADCAST_ACTION_WALKMAN_NEW.equals(intent.getAction())) {//清除数字 热拉随身听小红点提示
                    if (mainView.isActive()) {
                        mainView.clearSeeMeView();
                    }
                } else if (TheLConstants.BROADCAST_CLEAR_RELA_LISTENER_NEW_CHECK_ACTION.equals(intent.getAction()) || TheLConstants.BROADCAST_CLEAR_LIKE_ME_NEW_CHECK_ACTION.equals(intent.getAction()) || TheLConstants.BROADCAST_ACTION_WALKMAN_NEW.equals(intent.getAction())) {//清除谁喜欢我的数字  热拉随身听小红点提示
                    if (mainView.isActive()) {
                        mainView.clearLikeMeNum();
                    }
                } else if (TheLConstants.BROADCAST_CLEAR_MOMENT_MSG_COUNT_TOP.equals(intent.getAction())) {
                    if (mainView.isActive()) {
                        mainView.clearNotReadCommentNum();
                    }
                } else if (TheLConstants.BROADCAST_ACTION_APK_DOWNLOADED.equals(intent.getAction())) {
                    if (mainView.isActive()) {
                        String apkUrl = intent.getStringExtra("apkUrl");

                        mainView.gotoAPKSetting(apkUrl);
                    }
                }
            }
        }
    }

}
