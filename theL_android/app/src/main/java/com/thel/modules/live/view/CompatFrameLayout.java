package com.thel.modules.live.view;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import com.thel.app.TheLApp;
import com.thel.utils.ScreenUtils;

public class CompatFrameLayout extends FrameLayout {
    public CompatFrameLayout(@NonNull Context context) {
        super(context);
    }

    public CompatFrameLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CompatFrameLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int screenHeight = ScreenUtils.getScreenHeight(TheLApp.context);

        int screenWidth = ScreenUtils.getScreenWidth(TheLApp.context);

        if (screenHeight < screenWidth) {
            widthMeasureSpec = MeasureSpec.makeMeasureSpec(screenHeight, MeasureSpec.EXACTLY);
        }

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
