package com.thel.modules.main.me.bean;

import com.thel.utils.MD5Utils;
import com.thel.utils.ShareFileUtils;

import java.io.Serializable;
import java.text.SimpleDateFormat;

public class PayLogBean {

    public String page;
    public String page_id;
    public String from_page;
    public String from_page_id;
    public String activity;
    public String lat;
    public String lng;
    public String ua = MD5Utils.getUserAgent();
    public String user_id = ShareFileUtils.getString(ShareFileUtils.ID, "");
    public String time = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS").format(System.currentTimeMillis()) + "";


    public LogsDataBean data;

    public static class LogsDataBean implements Serializable {
        public String version;//4.19.0
        public int is_first; //购买软妹豆是否是第一次
        public String product_type;//6元30个
        public String pay_type; //alipay
        public int is_success;//1
        public String toast;//支付成功
        public String from_action;// 上一步动作
        public int choice; //快捷礼物弹窗选择

    }
}
