package com.thel.modules.live.liveBigGiftAnimLayout.anim;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.RelativeLayout;

import com.thel.modules.live.liveBigGiftAnimLayout.AnimImageView;
import com.thel.modules.live.liveBigGiftAnimLayout.LiveBigAnimUtils;
import com.thel.utils.Utils;

import java.util.Random;

/**
 * 热气球的轨迹动画
 * created by Setsail on 2016.6.2
 */
public class BalloonPathAnimator extends BaseAnimator {


    private final Context mContext;
    private final int mDuration;
    private final Handler mHandler;
    private final Random mRandom;
    //  private final int[] flags;
    // private final int[] chipRes;
    //private final int[] balloonRes;
    private final String[] flags;
    private final String[] chipRes;
    private final String[] balloonRes;
    private final LinearInterpolator mInterpolator;
    private final int startChipCount;
    private final int chipCount;
    private final int differChipCount;
    private final int chipRate;
    private boolean isPlaying;
    private int mWidth;
    private int mHeight;
    private final int flagRate;
    private final int differBallonCount;
    private final int defaultBalloonWidth;
    private final int minBallonCount;
    private final int rate;
    private final int minRibbonWidth;
    private final int differRibbonWidth;
    private final int startBallonCount;
    private final int differStartBallonCount;
    private final int differBallonWidth;
    private final int flowDuration;
    private final int differFlowDuration;
    private final String foldPath;

    public BalloonPathAnimator(Context context, int duration) {
        mContext = context;
        mDuration = duration;
        mHandler = new Handler(Looper.getMainLooper());
        mRandom = new Random();
       /* flags = new int[]{R.drawable.flag1, R.drawable.flag2, R.drawable.flag3};//热气球顶部旗子
        //热气球的绸带
        chipRes = new int[]{R.drawable.chip_1, R.drawable.chip_2, R.drawable.chip_3, R.drawable.chip_5, R.drawable.chip_6, R.drawable.chip_7, R.drawable.chip_8};
        //热气球资源
        balloonRes = new int[]{R.drawable.balloon_1, R.drawable.balloon_2, R.drawable.balloon_3, R.drawable.balloon_4, R.drawable.balloon_5};*/

        foldPath = "anim/balloon";//所在asset 目录
        flags = new String[]{"flag1", "flag2", "flag3"};//热气球顶部旗子
        //热气球的绸带
        chipRes = new String[]{"chip_1", "chip_2", "chip_3", "chip_5", "chip_6", "chip_7", "chip_8"};
        //热气球资源
        balloonRes = new String[]{"balloon_1", "balloon_2", "balloon_3", "balloon_4", "balloon_5"};
        //每次发射气球最小数
        minBallonCount = 2;
        //气球变化区间
        differBallonCount = 2;
        //默认气球大小
        defaultBalloonWidth = Utils.dip2px(mContext, 80);
        //气球大小变化区间
        differBallonWidth = Utils.dip2px(mContext, 40);
        //气球发射频率
        rate = 2000;
        //旗子动画间隔
        flagRate = 200;
        //最小花瓣大小
        minRibbonWidth = Utils.dip2px(mContext, 10);
        //绸带宽度变化区间
        differRibbonWidth = Utils.dip2px(mContext, 10);
        //最开始热气球数量
        startBallonCount = 2;
        //最开始热气球数变化区间
        differStartBallonCount = 2;
        //气球飞的时间
        flowDuration = 8000;
        //气球非的时间变化区间
        differFlowDuration = 3000;
        //气球和花瓣动画方式
        mInterpolator = new LinearInterpolator();
        //最开始花瓣数量
        startChipCount = 30;
        //花瓣默认添加数量
        chipCount = 3;
        //花瓣每次添加变化区间
        differChipCount = 2;
        //花瓣频率周期
        chipRate = 200;
    }

    public int start(final ViewGroup parent) {
        isPlaying = true;
        mWidth = parent.getMeasuredWidth();
        mHeight = parent.getMeasuredHeight();
        if (mWidth == 0 || mHeight == 0) {
            return 0;
        }
        final RelativeLayout background = new RelativeLayout(mContext);
        background.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
        parent.addView(background);
        setBackground(background);
        //执行完时间就将动画移除
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                isPlaying = false;
                parent.removeView(background);
            }
        }, mDuration);
        return mDuration;
    }

    private void setBackground(RelativeLayout background) {
        //添加旗帜
        addFlag(background);
        //添加气球
        addBollon(background);
        //添加绸带
        addRibbon(background);
    }

    private void addFlag(RelativeLayout background) {
        final AnimImageView flag_view = new AnimImageView(mContext);
        background.addView(flag_view);
        final RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams) flag_view.getLayoutParams();
        final int width = Math.min(mWidth, mHeight); //横屏兼容
        param.width = width;
        param.height = width * 216 / 750;
        param.addRule(RelativeLayout.CENTER_HORIZONTAL);
        param.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        //   flag_view.setBackgroundResource(flags[0]);
        LiveBigAnimUtils.setAssetBackground(flag_view, foldPath, flags[0]);

        LiveBigAnimUtils.setFrameAnim(mContext, flag_view, foldPath, flags, flagRate);
    }

    private void addBollon(final RelativeLayout background) {
        final int startCount = startBallonCount + mRandom.nextInt(differStartBallonCount);
        for (int i = 0; i < startCount; i++) {
            addOneImage(background, true, true);
        }
        background.post(new Runnable() {
            @Override
            public void run() {
                final int count = minBallonCount + mRandom.nextInt(differBallonCount);
                if (isPlaying) {
                    for (int i = 0; i < count; i++) {
                        addOneImage(background, true, false);
                    }
                    background.postDelayed(this, rate);
                }
            }
        });
    }

    private void addRibbon(final RelativeLayout background) {
        for (int i = 0; i < startChipCount; i++) {
            addOneImage(background, false, true);
        }
        background.post(new Runnable() {
            @Override
            public void run() {
                final int count = chipCount + mRandom.nextInt(differChipCount);
                if (isPlaying) {
                    for (int i = 0; i < count; i++) {
                        addOneImage(background, false, false);
                    }
                    background.postDelayed(this, chipRate);
                }
            }
        });
    }

    /**
     * 添加气球
     *
     * @param background background
     * @param isBalloon  true 气球，false 雪花
     * @param isStart    //true 最开始就有的气球/雪花，false 添加的新气球/雪花
     */

    private void addOneImage(RelativeLayout background, boolean isBalloon, boolean isStart) {
        if (!isPlaying) {
            return;
        }
        int length, w, h;
        String res;
        if (isBalloon) {//如果是气球
            length = balloonRes.length;
            res = balloonRes[mRandom.nextInt(length)];
            w = defaultBalloonWidth + mRandom.nextInt(differBallonWidth);
            h = getImageHeight(res, w);
        } else {//如果是雪花
            length = chipRes.length;
            res = chipRes[mRandom.nextInt(length)];
            w = minRibbonWidth + mRandom.nextInt(differRibbonWidth);
            h = w;
        }

        int left_margin, top_margin;
        if (isStart) {//如果是最开始就有的气球/雪花
            left_margin = -w / 2 + mRandom.nextInt(mWidth);
            top_margin = -h / 2 + mRandom.nextInt(mHeight);
        } else {//如果是从下往上添加的气球/雪花
            left_margin = -w / 2 + mRandom.nextInt(mWidth);
            top_margin = mHeight;
        }
        //    final ImageView image = new ImageView(mContext);
        final AnimImageView image = new AnimImageView(mContext);
        background.addView(image);
        //   image.setImageResource(res);
        LiveBigAnimUtils.setAssetImage(image, foldPath, res);
        final RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams) image.getLayoutParams();
        param.width = w;
        param.height = h;
        animView(image, w, h, left_margin, top_margin);
    }

    private void animView(final View view, int w, int h, int left_margin, int top_margin) {
        final int direct = mRandom.nextInt(2);//方向，0为向左，1为向右
        int max_x;
        if (0 == direct) {
            max_x = -w - left_margin;//气球/雪花向左移动消失的距离
        } else {
            max_x = mWidth - left_margin;//气球向右移动消失的距离
        }
        final int max_y = -(h + top_margin);//气球消失所需要移动的最大Y轴距离
        //若需要气球/雪花消失，需要最少满足X轴移动距离为max_x或者Y轴距离移动为max_y
        final int model = mRandom.nextInt(4);//消失方式，0为x轴消失，其他为y轴消失
        int x, y;//分别代表，x，y,轴移动距离
        if (0 == model) {
            x = max_x;
            y = max_y / 2 - mRandom.nextInt(-max_y / 2);//由于是向上移动，最小移动距离为最大距离的一半
        } else {
            x = -w + mRandom.nextInt(mWidth);//屏幕X随机位置
            y = max_y;
        }
        final int duration = flowDuration + mRandom.nextInt(differFlowDuration);
        AnimatorSet animator = new AnimatorSet();
        ObjectAnimator tranX = ObjectAnimator.ofFloat(view, View.TRANSLATION_X, left_margin, x);
        ObjectAnimator tranY = ObjectAnimator.ofFloat(view, View.TRANSLATION_Y, top_margin, y);
        animator.playTogether(tranX, tranY);
        animator.setDuration(duration);
        animator.setInterpolator(mInterpolator);
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (view != null) {
                    final ViewGroup parent = (ViewGroup) view.getParent();
                    if (parent != null) {
                        parent.removeView(view);
                    }
                }

                if (mOnAnimEndListener != null) {
                    mOnAnimEndListener.onAnimEnd();
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        animator.start();
    }


    private int getImageHeight(String resId, int w) {
        int h;
        switch (resId) {
            case "balloon_1":
                h = w * 384 / 285;
                break;
            case "balloon_2":
                h = w * 162 / 113;
                break;
            case "balloon_3":
                h = w * 275 / 205;
                break;
            case "balloon_4":
                h = w * 345 / 246;
                break;
            case "balloon_5":
                h = w * 123 / 83;
                break;
            default:
                h = w;
                break;
        }
        return h;
    }


}

