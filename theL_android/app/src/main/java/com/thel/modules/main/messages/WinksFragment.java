package com.thel.modules.main.messages;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jcodecraeer.xrecyclerview.ProgressStyle;
import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseFragment;
import com.thel.constants.TheLConstants;
import com.thel.db.DBConstant;
import com.thel.db.table.message.WinkMsgTable;
import com.thel.manager.ChatServiceManager;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.main.messages.adapter.WinksAdapter;
import com.thel.modules.main.userinfo.MyLinearLayoutManager;
import com.thel.ui.widget.CommonTitleBar;
import com.thel.ui.widget.OnMoreClickListener;
import com.thel.utils.SharedPrefUtils;

import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;

public class WinksFragment extends BaseFragment {

    private final static String TAG = "WinksFragment";

    @BindView(R.id.wink_rv) XRecyclerView wink_rv;

    @BindView(R.id.commonTitleBar) CommonTitleBar commonTitleBar;

    @BindView(R.id.bg_iv) ImageView bg_iv;

    private WinksAdapter mWinksAdapter;

    private MyLinearLayoutManager mMyLinearLayoutManager;

    private boolean isHasBG = false;

    private int limit = 20;

    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable @Override public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_winks, container, false);
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
    }

    @Override public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initView();

        getWinksData();

    }

    private void getWinksData() {

        Flowable
                .create(new FlowableOnSubscribe<List<WinkMsgTable>>() {

                    @Override public void subscribe(FlowableEmitter<List<WinkMsgTable>> flowableEmitter) {

                        List<WinkMsgTable> winkList = ChatServiceManager.getInstance().getWinkList(limit, mWinksAdapter.getData().size());

                        if (winkList != null) {
                            flowableEmitter.onNext(winkList);
                            flowableEmitter.onComplete();
                        }
                    }
                }, BackpressureStrategy.DROP)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<WinkMsgTable>>() {
                    @Override public void accept(List<WinkMsgTable> winkList) {

                        if (winkList.size() < limit) {
                            wink_rv.setPullRefreshEnabled(false);
                        }

                        if (winkList != null && winkList.size() - 1 >= 0) {
                            Collections.reverse(winkList);
                            mWinksAdapter.isHasBG(isHasBG);
                            mWinksAdapter.setDataNoCLear(0, winkList);
                        }

                        wink_rv.refreshComplete();
                    }
                });


    }

    private void initView() {
        mWinksAdapter = new WinksAdapter(getContext());
        mMyLinearLayoutManager = new MyLinearLayoutManager(getContext());
        mMyLinearLayoutManager.setOrientation(RecyclerView.VERTICAL);

        wink_rv.setLayoutManager(mMyLinearLayoutManager);
        wink_rv.setAdapter(mWinksAdapter);
        wink_rv.setRefreshProgressStyle(ProgressStyle.BallClipRotate);
        wink_rv.setPullRefreshEnabled(true);
        wink_rv.setLoadingMoreEnabled(false);
        wink_rv.setLoadingListener(mLoadingListener);

        commonTitleBar.setMOnMoreClickListener(new OnMoreClickListener() {
            @Override public void onMoreClick(@NotNull View view) {

                Intent intent = new Intent(getContext(), ChatOperationsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(TheLConstants.BUNDLE_KEY_WIDTH, wink_rv.getWidth());
                intent.putExtra(TheLConstants.BUNDLE_KEY_HEIGHT, wink_rv.getHeight());
                intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, DBConstant.Message.WINK_TABLE_NAME);
                getActivity().startActivityForResult(intent, TheLConstants.RESULT_CODE_CLEAR_CHAT);
            }
        });
    }

    @Override public void onResume() {
        super.onResume();

        String imgUrl = SharedPrefUtils.getString(SharedPrefUtils.FILE_NAME_CHAT_BG, SharedPrefUtils.CHAT_BG_ALL, "");

        if (!TextUtils.isEmpty(imgUrl)) {
            ImageLoaderManager.imageLoader(bg_iv, R.color.bg_color, imgUrl);
            isHasBG = true;
        } else {
            isHasBG = false;
        }

        if (mWinksAdapter != null && mWinksAdapter.getData().size() > 0) {
            mWinksAdapter.notifyDataSetChanged();
        }

    }

    @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK && requestCode == TheLConstants.RESULT_CODE_CLEAR_CHAT) {
            ChatServiceManager.getInstance().removeMsgTable(DBConstant.Message.WINK_TABLE_NAME);
        }

    }

    private XRecyclerView.LoadingListener mLoadingListener = new XRecyclerView.LoadingListener() {
        @Override public void onRefresh() {

            getWinksData();

        }

        @Override public void onLoadMore() {

        }
    };
}
