package com.thel.modules.main.me.aboutMe;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TextView;

import com.thel.R;
import com.thel.base.BaseFragment;
import com.thel.bean.RechargeRecord;
import com.thel.bean.RechargeRecordListBeanNew;
import com.thel.modules.main.me.adapter.RechargeRecordListAdapter;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class RechargeRecordFragment extends BaseFragment {

    private String mTag;

    private SwipeRefreshLayout swipe_container;

    // 刷新类型，全部刷新（即下拉刷新）为1，还是加载下一页为2
    private int refreshType = 0;
    private final int REFRESH_TYPE_ALL = 1;
    private final int REFRESH_TYPE_NEXT_PAGE = 2;
    private boolean canLoadMore = false;
    /**
     * 下一页数据的游标
     */
    private int cursor = 0;
    private int limit = 50;

    private ListView mListView;
    private TextView mDefault;
    private ArrayList<RechargeRecord> rechargeRecords = new ArrayList<>();
    private RechargeRecordListAdapter listAdapter;

    private boolean isFirstCreate = false;// 用来判断是不是第一次创建页面，懒加载数据

    public static RechargeRecordFragment newInstance(String tag) {
        final RechargeRecordFragment fragment = new RechargeRecordFragment();
        Bundle bundle = new Bundle();
        bundle.putString("tag", tag);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();
        mTag = bundle.getString("tag");
        isFirstCreate = true;
    }

    public RechargeRecordFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_recharge_record, container, false);

        if (swipe_container == null) {
            swipe_container = view.findViewById(R.id.swipe_container);
            ViewUtils.initSwipeRefreshLayout(swipe_container);
            swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

                @Override
                public void onRefresh() {
                    refreshAll();
                }
            });
        }
        if (mListView == null) {
            mListView = view.findViewById(R.id.list_view);
            mListView.setDivider(null);
            // 列表滚动
            mListView.setOnScrollListener(new AbsListView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {
                    switch (scrollState) {
                        // 当不滚动时
                        case AbsListView.OnScrollListener.SCROLL_STATE_IDLE:
                            int totalSize = view.getCount();
                            // 判断滚动到底部
                            if (view.getLastVisiblePosition() + 1 == totalSize && canLoadMore && cursor != 0) {
                                showLoading();
                                canLoadMore = false;
                                // 列表滑动到底部加载下一组数据
                                loadNetData(cursor, REFRESH_TYPE_NEXT_PAGE);
                            }
                            break;
                    }
                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                }
            });
        }
        if (mDefault == null) {
            mDefault = view.findViewById(R.id.txt_default);
            if (RechargeRecordActivity.TAG_RECHARGE.equals(mTag))
                mDefault.setText(R.string.no_recharge_record);
            else
                mDefault.setText(R.string.no_consume_record);
        }

        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser && isFirstCreate) {
            isFirstCreate = false;
            if (swipe_container != null)
                swipe_container.post(new Runnable() {
                    @Override
                    public void run() {
                        swipe_container.setRefreshing(true);
                    }
                });
            refreshAll();
        }

        if (!TextUtils.isEmpty(mTag))
            if (isVisibleToUser) {
                MobclickAgent.onPageStart(this.getClass().getName() + ":" + mTag);
            } else {
                MobclickAgent.onPageEnd(this.getClass().getName() + ":" + mTag);
            }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // support库版本>=24.0.0后，setUserVisibleHint方法只在onCreate方法之前调用，因此首页的刷新不能依赖setUserVisibleHint了，在fragment创建的时候，要主动刷一下列表
        if (getUserVisibleHint()) {
            isFirstCreate = false;
            if (swipe_container != null)
                swipe_container.post(new Runnable() {
                    @Override
                    public void run() {
                        swipe_container.setRefreshing(true);
                    }
                });
            refreshAll();
        }
    }

    public void refreshRechargeRecords(RechargeRecordListBeanNew rechargeRecordListBean) {

        if (refreshType == REFRESH_TYPE_ALL) {
            rechargeRecords.clear();
        }
        rechargeRecords.addAll(rechargeRecordListBean.data.list);
        if (rechargeRecords.size() > 0)
            mDefault.setVisibility(View.GONE);
        else
            mDefault.setVisibility(View.VISIBLE);
        canLoadMore = true;
        cursor = rechargeRecordListBean.data.cursor;
        closeLoading();

        // 遍历所有数据，判断哪些是需要显示头部一行的
        reviewData(rechargeRecords);

        if (null == listAdapter) {
            listAdapter = new RechargeRecordListAdapter(rechargeRecords);
            mListView.setAdapter(listAdapter);
        } else {
            listAdapter.notifyDataSetChanged();
        }
        if (refreshType == REFRESH_TYPE_ALL) {
            if (mListView.getCount() > 0) {// 回到顶部
                mListView.setSelection(0);
            }
        }
    }

    private void reviewData(ArrayList<RechargeRecord> rechargeRecords) {
        String showedDate = "";
        for (RechargeRecord rechargeRecord : rechargeRecords) {
            if (!getDate(rechargeRecord.createTime).equals(showedDate)) {
                rechargeRecord.showHeader = true;
                showedDate = getDate(rechargeRecord.createTime);
            } else {
                rechargeRecord.showHeader = false;
            }
        }
    }

    public void scrollToTop() {
        if (listAdapter != null && mListView != null) {
            mListView.setSelection(0);
        }
    }

    public void refreshAll() {
        cursor = 0;
        loadNetData(cursor, REFRESH_TYPE_ALL);
    }

    private void loadNetData(int curCursor, int type) {
        refreshType = type;
        RequestBusiness.getInstance()
                .getRechargeRecord(curCursor, limit, mTag)
                .onBackpressureDrop().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new InterceptorSubscribe<RechargeRecordListBeanNew>() {
                    @Override
                    public void onNext(RechargeRecordListBeanNew data) {
                        super.onNext(data);
                        refreshRechargeRecords(data);
                        requestFinished();
                    }

                    @Override public void onError(Throwable t) {
                        super.onError(t);
                    }
                });

    }

    private void requestFinished() {
        canLoadMore = true;
        if (swipe_container != null)
            swipe_container.postDelayed(new Runnable() {

                @Override
                public void run() {
                    if (swipe_container != null && swipe_container.isRefreshing())
                        swipe_container.setRefreshing(false);
                }
            }, 1000);
        closeLoading();
    }

    private String getDate(String createTime){
        final DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date d = null;
        try {
            d = df.parse(createTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        final DateFormat dfDate = new SimpleDateFormat("MM.dd.yyyy");
        return dfDate.format(d);
    }



}
