package com.thel.modules.main.me.match;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.constants.TheLConstants;
import com.thel.data.local.FileHelper;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.post.MomentPosterActivity;
import com.thel.utils.DateUtils;
import com.thel.utils.DialogUtil;
import com.thel.utils.ImageUtils;
import com.thel.utils.L;
import com.thel.utils.PermissionUtil;
import com.thel.utils.QRUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MatchPosterActivity extends BaseActivity {

    @BindView(R.id.img_qrcode)
    ImageView img_qrcode;

    @BindView(R.id.match_left_iv)
    ImageView match_left_iv;

    @BindView(R.id.match_left_tv)
    TextView match_left_tv;

    @BindView(R.id.match_right_iv)
    ImageView match_right_iv;

    @BindView(R.id.match_right_tv)
    TextView match_right_tv;

    @BindView(R.id.root_rl)
    RelativeLayout root_rl;

    @BindView(R.id.time_tv)
    TextView time_tv;

    private final static int IMG_QUALITY = 70;

    private final static float THUMB_SCALE = 0.3f;//缩略图比例

    private String otherUserAvatar = "";

    private String otherUserNickName = "";

    private String action;

    public static void goToShare(Context context, String action, String otherUserAvatar, String otherUserNickName) {
        Intent intent = new Intent(context, MatchPosterActivity.class);
        intent.putExtra(TheLConstants.BUNDLE_KEY_ACTION_TYPE, action);
        intent.putExtra(TheLConstants.BUNDLE_KEY_USER_AVATAR, otherUserAvatar);
        intent.putExtra(TheLConstants.BUNDLE_KEY_NICKNAME, otherUserNickName);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match_poaster);
        ButterKnife.bind(this);
        initData();
        initView();
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void initData() {

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            otherUserAvatar = bundle.getString(TheLConstants.BUNDLE_KEY_USER_AVATAR);
            otherUserNickName = bundle.getString(TheLConstants.BUNDLE_KEY_NICKNAME);
            action = bundle.getString(TheLConstants.BUNDLE_KEY_ACTION_TYPE);
        }

    }

    private void initView() {

        ImageLoaderManager.imageLoaderCircle(match_left_iv, R.mipmap.icon_user, UserUtils.getUserAvatar());

        match_left_tv.setText(UserUtils.getNickName());

        ImageLoaderManager.imageLoaderCircle(match_right_iv, R.mipmap.icon_user, otherUserAvatar);

        match_right_tv.setText(otherUserNickName);

        String year = DateUtils.getFormatTimeNow("yyyy");
        String month = DateUtils.monthFormat(DateUtils.getFormatTimeNow("MM"));
        String day = DateUtils.getFormatTimeNow("dd");

        try {
            time_tv.setText(month + "." + day + "." + year);
        } catch (Exception e) {

        }

        final Bitmap bitmapQr = QRUtils.createQRImage(MomentPosterActivity.URL_INVITE_FRIEND, Utils.dip2px(this, 80), Utils.dip2px(this, 80));

        img_qrcode.setImageBitmap(bitmapQr);

        root_rl.postDelayed(new Runnable() {
            @Override
            public void run() {
                PermissionUtil.requestStoragePermission(MatchPosterActivity.this, new PermissionUtil.PermissionCallback() {
                    @Override
                    public void granted() {
                        SHARE_MEDIA platform = null;
                        switch (action) {
                            case "friends_circle_iv":
                                platform = SHARE_MEDIA.WEIXIN_CIRCLE;
                                break;
                            case "friends_we_chat_iv":
                                platform = SHARE_MEDIA.WEIXIN;
                                break;
                            case "weibo_iv":
                                platform = SHARE_MEDIA.SINA;
                                break;
                            case "qq_iv":
                                platform = SHARE_MEDIA.QQ;
                                break;
                            case "qq_zone_iv":
                                platform = SHARE_MEDIA.QZONE;
                                break;
                            case "facebook_iv":
                                shareFacebook();
                                break;

                        }
                        share(platform);
                    }

                    @Override
                    public void denied() {

                    }

                    @Override
                    public void gotoSetting() {

                    }
                });
            }
        }, 2000);
    }

    /**
     * facebook分享
     */
    private void shareFacebook() {
        final String path = getShareImagePath(root_rl, IMG_QUALITY);
        if (!TextUtils.isEmpty(path)) {
            if (ShareDialog.canShow(SharePhotoContent.class)) {
                final SharePhoto photo = new SharePhoto.Builder().setBitmap(BitmapFactory.decodeFile(path)).build();
                final SharePhotoContent content = new SharePhotoContent.Builder().addPhoto(photo).build();
                ShareDialog shareDialog = new ShareDialog(this);
                shareDialog.show(content);
            } else {
                DialogUtil.showToastShort(this, getString(R.string.install_facebook));
            }
        }
    }

    /**
     * 朋友圈，微信，qq，qq空间，新浪微博 分享
     *
     * @param platform
     */
    public void share(SHARE_MEDIA platform) {
        MobclickAgent.onEvent(this, "start_share");
        final String path = getShareImagePath(root_rl, IMG_QUALITY);
        final String thumb_path = getZoomBitmapPath(path, THUMB_SCALE, IMG_QUALITY);
        if (null != platform && !TextUtils.isEmpty(path)) {
            final UMImage image = new UMImage(this, BitmapFactory.decodeFile(path));
            image.setThumb(new UMImage(this, BitmapFactory.decodeFile(thumb_path)));
            new ShareAction(this).setPlatform(platform).withMedia(image).setCallback(umShareListener).share();
        }
    }

    private Bitmap getZoomBitmap(Bitmap oldBitmap, float scale) {
        Bitmap bm;
        final float width = oldBitmap.getWidth();
        final float height = oldBitmap.getHeight();
        final Matrix matrix = new Matrix();
        matrix.postScale(scale, scale);
        bm = Bitmap.createBitmap(oldBitmap, 0, 0, (int) width, (int) height, matrix, true);
        return bm;
    }

    private String getZoomBitmapPath(String oldPath, float scale, int quality) {

        String zoomPath;

        if (TextUtils.isEmpty(oldPath)) {
            return null;
        }
        final Bitmap bitmap = BitmapFactory.decodeFile(oldPath);
        if (null == bitmap) {
            return null;
        }
        try {
            final Bitmap bm = getZoomBitmap(bitmap, scale);
            zoomPath = TheLConstants.F_TAKE_PHOTO_ROOTPATH + System.currentTimeMillis() + TheLConstants.MOMENT_POSTER_SHARE_IMG_END + ".jpg";
            if (ImageUtils.savePic(bm, zoomPath, quality)) {
                return zoomPath;
            }
        } catch (Exception e) {
            bitmap.recycle();
            zoomPath = null;
            e.printStackTrace();
        }
        return zoomPath;
    }

    /**
     * 获取分享的图片
     *
     * @param view
     * @return
     */
    private String getShareImagePath(View view, int quality) {
        L.d("poster_model", "width    :  +" + view.getWidth());
        L.d("poster_model", "height    :  +" + view.getHeight());

        String imgPath;

        TextView tips = view.findViewById(R.id.click_qrcode_tips);
        tips.setVisibility(View.VISIBLE);
        final Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        tips.setVisibility(View.GONE);

        File dir = new File(TheLConstants.F_TAKE_PHOTO_ROOTPATH);
        if (!dir.exists()) {
            dir.mkdirs();
        }

        imgPath = FileHelper.getInstance().getCameraDir() + System.currentTimeMillis() + TheLConstants.MOMENT_POSTER_SHARE_IMG_END + ".jpg";


        L.d("MomentPoster", " imgPath -1 : " + imgPath);

        try {
            if (ImageUtils.savePic(bitmap, imgPath, quality)) {
                Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                Uri uri = Uri.fromFile(new File(imgPath));
                intent.setData(uri);
                this.sendBroadcast(intent);
                L.d("MomentPoster", " imgPath 0 : " + imgPath);
                return imgPath;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        imgPath = null;

        L.d("MomentPoster", " imgPath 1 : " + imgPath);

        return imgPath;
    }

    private UMShareListener umShareListener = new UMShareListener() {

        @Override
        public void onStart(SHARE_MEDIA share_media) {

        }

        @Override
        public void onResult(SHARE_MEDIA platform) {
            MobclickAgent.onEvent(MatchPosterActivity.this, "share_succeeded");
        }

        @Override
        public void onError(SHARE_MEDIA platform, Throwable t) {
            if (t != null && t.getMessage() != null) {
                DialogUtil.showToastShort(MatchPosterActivity.this, t.getMessage());
            }
        }

        @Override
        public void onCancel(SHARE_MEDIA platform) {
            DialogUtil.showToastShort(MatchPosterActivity.this, TheLApp.getContext().getString(R.string.my_circle_requests_act_canceled));
        }
    };

}
