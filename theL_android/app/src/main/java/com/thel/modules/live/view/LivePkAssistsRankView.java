package com.thel.modules.live.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import android.net.Uri;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.constants.TheLConstants;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.live.bean.LivePkAssisterBean;
import com.thel.modules.live.in.LiveBaseView;
import com.thel.modules.live.in.LivePkContract;
import com.thel.ui.transfrom.GlideCircleTransform;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by waiarl on 2017/11/30.
 * 直播助攻view
 */

public class LivePkAssistsRankView extends LinearLayout implements LiveBaseView<LivePkAssistsRankView> {
    private final Context mContext;
    private TextView txt_best_assists;
    private RecyclerView recyclerView;
    private LivePkAssistsRandAdapter adapter;
    private List<LivePkAssisterBean> list = new ArrayList<>();
    private LinearLayoutManager manager;
    private LivePkContract.AssistRankClickListener assistRankClickListener;
    private String ourUserId;//我方主播的userId

    public LivePkAssistsRankView(Context context) {
        this(context, null);
    }

    public LivePkAssistsRankView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LivePkAssistsRankView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
        setListener();
    }

    @SuppressLint("WrongConstant")
    private void init() {
        inflate(mContext, R.layout.live_pk_assists_rank_view, this);
        txt_best_assists = findViewById(R.id.txt_best_assists);
        recyclerView = findViewById(R.id.recyclerview);
        adapter = new LivePkAssistsRandAdapter(list);
        manager = new LinearLayoutManager(mContext);
        manager.setOrientation(VERTICAL);
        recyclerView.setLayoutManager(manager);
        ((SimpleItemAnimator)recyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
        adapter.setHasStableIds(true);
        recyclerView.setAdapter(adapter);
    }

    private void setListener() {
        adapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (assistRankClickListener != null) {
                    assistRankClickListener.onAssisterClick(adapter.getItem(position), position);
                }
            }
        });
    }

    public LivePkAssistsRankView initView(String ourUserId, List<LivePkAssisterBean> beanList) {
        this.ourUserId = ourUserId;
        adapter.initOurUserId(ourUserId);
        if (beanList == null) {
            return this;
        }
        this.list.clear();
        this.list.addAll(beanList);
        adapter.setNewData(this.list);
        return this;
    }

    public void setOnAssisterClickListener(LivePkContract.AssistRankClickListener listener) {
        this.assistRankClickListener = listener;
    }

    @Override
    public LivePkAssistsRankView show() {
        setVisibility(View.VISIBLE);
        return this;
    }

    @Override
    public LivePkAssistsRankView hide() {
        return null;
    }

    @Override
    public LivePkAssistsRankView destroyView() {
        setVisibility(View.GONE);
        list.clear();
        adapter.notifyDataSetChanged();
        return this;
    }

    @Override
    public boolean isAnimating() {
        return false;
    }

    @Override
    public void setAnimating(boolean isAnimating) {

    }

    @Override
    public void showShade(boolean show) {

    }

    private class LivePkAssistsRandAdapter extends BaseRecyclerViewAdapter<LivePkAssisterBean> {
        private String ourUserId;

        public LivePkAssistsRandAdapter(List<LivePkAssisterBean> list) {
            super(R.layout.adapter_live_pk_assist_rank_item, list);
        }

        @Override
        protected void convert(BaseViewHolder helper, LivePkAssisterBean item) {
            //设置头像圈颜色
            final RelativeLayout rel_avatar = helper.getView(R.id.rel_avatar);
            final Drawable drawable = rel_avatar.getBackground();
            int colorId = R.color.bg_live_blood_green;//默认为我方
            if (!TextUtils.isEmpty(item.toUserId) && !item.toUserId.equals(ourUserId)) {
                colorId = R.color.bg_live_blood_red; //不为我放则为对方
            }
            drawable.setColorFilter(ContextCompat.getColor(TheLApp.getContext(), colorId), PorterDuff.Mode.SRC);
            //头像
            ImageView imageView = helper.getView(R.id.img_avatar);
            final Uri uri = ImageLoaderManager.getImageUri(item.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            Glide.with(mContext).load(uri).placeholder(R.mipmap.icon_user).dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL).circleCrop().into(imageView);
            //昵称
            helper.setText(R.id.txt_nickname, item.nickName + "");
            // 设置贡献软妹豆
            helper.setText(R.id.txt_gold, TheLApp.getContext().getString(R.string.dou, item.gold));
            //设置名次图标
            final int position = helper.getLayoutPosition() - getHeaderLayoutCount();
            int rankRes = R.mipmap.pk_one;
            switch (position) {
                case 0:
                    rankRes = R.mipmap.pk_one;
                    break;
                case 1:
                    rankRes = R.mipmap.pk_two;
                    break;
                case 2:
                    rankRes = R.mipmap.pk_three;
                    break;
            }
            helper.setImageResource(R.id.img_rank, rankRes);
        }

        public void initOurUserId(String ourUserId) {
            this.ourUserId = ourUserId;
        }
    }
}
