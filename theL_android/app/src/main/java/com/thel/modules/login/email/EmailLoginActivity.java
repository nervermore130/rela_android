package com.thel.modules.login.email;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.flutter.bridge.RfSBridgeHandlerFactory;
import com.thel.modules.login.LoginEventManager;
import com.thel.network.LoginInterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.api.loginapi.bean.SignInBean;
import com.thel.utils.L;
import com.thel.utils.MD5Utils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.ToastUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import me.rela.rf_s_bridge.RfSBridgePlugin;
import me.rela.rf_s_bridge.new_router.NewUniversalRouter;
import me.rela.rf_s_bridge.router.UniversalRouter;

public class EmailLoginActivity extends BaseActivity {

    @BindView(R.id.auto_username)
    AutoCompleteTextView auto_username;

    @BindView(R.id.edit_password)
    EditText edit_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_login);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_MASK_ADJUST | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ButterKnife.bind(this);
        String accounts = ShareFileUtils.getString(ShareFileUtils.ACCOUNT_AUTO_COMPLETE, "");
        if (!TextUtils.isEmpty(accounts)) {
            // 创建一个ArrayAdapter封装数组
            ArrayAdapter<String> av = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, accounts.split(","));
            auto_username.setAdapter(av);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (auto_username != null) {
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    ViewUtils.showSoftInput(EmailLoginActivity.this, auto_username);
                }
            }, 200);
        }
        MobclickAgent.onPageStart(this.getClass().getName());
        MobclickAgent.onResume(this);
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(this.getClass().getName());
        MobclickAgent.onPause(this);
    }

    @OnClick(R.id.button_login)
    void login(View view) {
        ViewUtils.preventViewMultipleClick(view, 2000);
        final String email = auto_username.getText().toString().trim();

        if (!isEmail(email)) {
            Toast.makeText(this, getString(R.string.register_activity_wrong_email), Toast.LENGTH_SHORT).show();
            return;
        }

        String password = edit_password.getText().toString().trim();

        if (email.length() != 0 && password.length() != 0) {
            password = MD5Utils.md5(password);
            showLoading();

            RequestBusiness.getInstance()
                    .signIn("password", email, password, "", "", "", "", "", "", "", null)
                    .onBackpressureDrop().subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new LoginInterceptorSubscribe<SignInBean>() {
                        @Override
                        public void onNext(SignInBean data) {
                            super.onNext(data);
                            closeLoading();
                            if (data == null || data.data == null || data.data.auth == null) return;

                            ShareFileUtils.saveUserData(data);

                            ShareFileUtils.setBoolean(ShareFileUtils.HAS_LOGGED, true);

                            // 保存账户联想
                            String accounts = ShareFileUtils.getString(ShareFileUtils.ACCOUNT_AUTO_COMPLETE, "");
                            if (!accounts.contains(email)) {
                                accounts = accounts + email + ",";
                                ShareFileUtils.setString(ShareFileUtils.ACCOUNT_AUTO_COMPLETE, accounts);
                            }
                            setResult(RESULT_OK);

                            NewUniversalRouter.sharedInstance.flutterPopToRoot(false);

                            RfSBridgeHandlerFactory.getInstance().getFlutterPushImpl().pushUserInfo();

                            RfSBridgeHandlerFactory.getInstance().getFlutterPushImpl().pushMyUserId();

                            L.d("androidABTest"," UserUtils.getMyUserId() : " + UserUtils.getMyUserId());

                            RfSBridgePlugin.getInstance().activeRpc("EventLogin", UserUtils.getMyUserId(), null);

                            LoginEventManager.emailLoginCallback(EmailLoginActivity.this, data);

                            EmailLoginActivity.this.finish();
                        }

                        @Override
                        public void onError(Throwable t) {
                            super.onError(t);
                            closeLoading();
                            if (errDataBean != null && !TextUtils.isEmpty(errDataBean.errdesc)) {
                                ToastUtils.showCenterToastShort(EmailLoginActivity.this, errDataBean.errdesc);
                            }
                        }
                    });
        } else {
            Toast.makeText(TheLApp.getContext(), TheLApp.getContext().getString(R.string.login_activity_password_not_empty), Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.txt_forget)
    void forget(View view) {
        ViewUtils.preventViewMultipleClick(view, 2000);
        Intent intent = new Intent(EmailLoginActivity.this, EmailForgetActivity.class);
        startActivity(intent);
    }

    private boolean isEmail(String str) {
        return !TextUtils.isEmpty(str)
                && str.matches("^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$");
    }
}
