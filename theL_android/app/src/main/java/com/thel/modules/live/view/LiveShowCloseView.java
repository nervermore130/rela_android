package com.thel.modules.live.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.imp.follow.FollowStatusChangedImpl;
import com.thel.imp.follow.FollowStatusChangedListener;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.live.bean.LiveRoomBean;
import com.thel.modules.live.in.LiveBaseView;
import com.thel.modules.main.userinfo.UserInfoActivity;

/**
 * Created by waiarl on 2017/10/28.
 */

public class LiveShowCloseView extends RelativeLayout implements LiveBaseView<LiveShowCloseView> {
    private final Context context;
    private TextView txt_follow;
    private ImageView avatar;
    private ImageView img_close;
    private TextView txt_nickname;
    private LiveRoomBean liveRoomBean;
    private RelativeLayout rel_avatar;
    private TextView txt_more_live;

    public LiveShowCloseView(Context context) {
        this(context, null);
    }

    public LiveShowCloseView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LiveShowCloseView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        init();
        setListener();
    }


    private LiveShowCloseView init() {

        inflate(context, R.layout.live_closed_dialog, this);
        rel_avatar = findViewById(R.id.rel_avatar);
        txt_follow = findViewById(R.id.txt_follow);
        img_close = findViewById(R.id.img_close);
        avatar = findViewById(R.id.avatar);
        txt_nickname = findViewById(R.id.txt_nickname);
        txt_more_live = findViewById(R.id.txt_more_live);
        return this;
    }

    public LiveShowCloseView initView(final LiveRoomBean liveRoomBean, View.OnClickListener closeListener, View.OnClickListener moreLiveListener) {
        this.liveRoomBean = liveRoomBean;
        ImageLoaderManager.imageLoaderDefaultCircle(avatar, R.mipmap.icon_user, liveRoomBean.user.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE);
        txt_nickname.setText(liveRoomBean.user.nickName);
        if (liveRoomBean.user.isFollow == 0) {
            txt_follow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    follow(liveRoomBean);
                }
            });
        } else {
            txt_follow.setText(TheLApp.getContext().getString(R.string.userinfo_activity_followed));
        }
        img_close.setOnClickListener(closeListener);
        txt_more_live.setOnClickListener(moreLiveListener);
        return this;
    }

    private void setListener() {
        rel_avatar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (liveRoomBean != null && liveRoomBean.user != null && liveRoomBean.user.id != 0) {
                    gotoUserInfoActivity(liveRoomBean.user.id + "");
                }
            }
        });

    }

    private void gotoUserInfoActivity(String userId) {
//        final Intent intent = new Intent(context, UserInfoActivity.class);
//        final Bundle bundle = new Bundle();
//        bundle.putSerializable(TheLConstants.BUNDLE_KEY_USER_ID, userId);
//        intent.putExtras(bundle);
//        context.startActivity(intent);
        FlutterRouterConfig.Companion.gotoUserInfo(userId);
    }

    @Override
    public LiveShowCloseView hide() {
        setVisibility(GONE);
        return this;
    }

    @Override
    public LiveShowCloseView destroyView() {
        liveRoomBean = null;
        setVisibility(GONE);
        return null;
    }

    @Override
    public boolean isAnimating() {
        return false;
    }

    @Override
    public void setAnimating(boolean isAnimating) {

    }

    @Override
    public void showShade(boolean show) {

    }
    @Override
    public LiveShowCloseView show() {
        setVisibility(VISIBLE);
        return this;
    }

    /**
     * 关注
     *
     * @param liveRoomBean
     */
    private void follow(LiveRoomBean liveRoomBean) {
        if (liveRoomBean == null) {
            return;
        }
        FollowStatusChangedImpl.followUserWithNoDialog(liveRoomBean.user.id + "", FollowStatusChangedListener.ACTION_TYPE_FOLLOW, liveRoomBean.user.nickName, liveRoomBean.user.avatar);
    }

    public void followStatusChanged(int followStatus, String userId, String nickName, String avatar) {
        if (liveRoomBean != null && liveRoomBean.user != null && (liveRoomBean.user.id + "").equals(userId)) {
            liveRoomBean.user.isFollow = followStatus;
            if (followStatus == FollowStatusChangedListener.FOLLOW_STATUS_FOLLOW || followStatus == FollowStatusChangedListener.FOLLOW_STATUS_FRIEND) {
                txt_follow.setText(TheLApp.getContext().getString(R.string.userinfo_activity_followed));
                txt_follow.setOnClickListener(null);
            }
        }
    }
}
