package com.thel.modules.main.video_discover.pgc;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.view.SimpleDraweeView;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.video.VideoBean;
import com.thel.modules.main.video_discover.autoplay.AutoPlayVideoCtrl;
import com.thel.modules.main.video_discover.autoplay.RecyclerViewStateImp;
import com.thel.modules.main.video_discover.autoplay.VideoAutoPlayImp;
import com.thel.modules.main.video_discover.videofalls.RecyclerViewStateBean;
import com.thel.modules.main.video_discover.videofalls.VideoFallsUtils;
import com.thel.utils.SimpleDraweeViewUtils;
import com.thel.utils.StringUtils;
import com.thel.utils.Utils;

/**
 * Created by waiarl on 2018/3/19.
 */

public class VideoFallsPgcAdapterView extends RelativeLayout implements VideoFallAdapterViewImp<VideoFallsPgcAdapterView>, VideoAutoPlayImp<VideoFallsPgcAdapterView> {
    private final Context mContext;
    private RelativeLayout rel;
    private SimpleDraweeView img_thumb;
    private LinearLayout lin_recommend;
    private TextView txt_title;
    private TextView txt_name;
    private TextView txt_time;
    private TextView txt_like_count;
    private VideoBean mVideoBean;
    private int mPosition;
    private RecyclerViewStateBean mStateBean;
    private FrameLayout video_container;
    private AutoPlayVideoCtrl mAutoPlayVideoCtrl;
    private RecyclerViewStateImp.RecyclerViewScrollStateChangedListener autoPlayRecyclerViewStateListener;

    public VideoFallsPgcAdapterView(Context context) {
        this(context, null);
    }

    public VideoFallsPgcAdapterView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public VideoFallsPgcAdapterView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
        initViewState();
        setListener();
    }

    private void init() {
        inflate(mContext, R.layout.adapter_video_falls_pgc_item, this);
        rel = findViewById(R.id.rel);
        img_thumb = findViewById(R.id.img_thumb);
        lin_recommend = findViewById(R.id.lin_recommend);
        txt_title = findViewById(R.id.txt_title);
        txt_name = findViewById(R.id.txt_name);
        txt_time = findViewById(R.id.txt_time);
        txt_like_count = findViewById(R.id.txt_like_count);
        video_container = findViewById(R.id.video_container);
    }

    private void initViewState() {
        lin_recommend.setVisibility(View.GONE);
        txt_title.setVisibility(View.GONE);
    }

    private void setListener() {
        autoPlayRecyclerViewStateListener = new RecyclerViewStateImp.RecyclerViewScrollStateChangedListener() {
            @Override
            public void stateChanged(int state) {
                if (state == RecyclerViewStateImp.State_IDLE) {
                    if (getVisibility() == View.VISIBLE && mAutoPlayVideoCtrl != null && getVisibleHeightPercent() > 0) {
                        mAutoPlayVideoCtrl.addVisibleView(VideoFallsPgcAdapterView.this);
                    }
                }
            }
        };
    }

    @Override
    public VideoFallsPgcAdapterView initView(VideoBean videoBean, int position, RecyclerViewStateBean recyclerViewStateBean) {
        this.mVideoBean = videoBean;
        this.mPosition = position;
        this.mStateBean = recyclerViewStateBean;
        if (mVideoBean == null) {
            return this;
        }
        setRecommend();
        setThumb();
        setTitle();
        setName();
        setTime();
        setLikeCount();
        return this;
    }

    private void setRecommend() {
        if (mVideoBean.videoTag > 0) {
            lin_recommend.setVisibility(View.VISIBLE);
        }
    }

    private void setThumb() {
        //img_thumb.initRecyclerViewState(mStateBean).setFullHeight(photoHeight);
        rel.getLayoutParams().height = (int) (pgcHeight);
        /*if (!TextUtils.isEmpty(mVideoBean.videoWebp)) {
            img_thumb.setWebpUrl(mVideoBean.videoWebp);
        } else {*/
        // img_thumb.setImageUrl(mVideoBean.image, pgcWidth, photoHeight);
        SimpleDraweeViewUtils.setImageUrl(img_thumb, mVideoBean.image, pgcWidth, pgcHeight);
        // }
        img_thumb.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadius(radius).setRoundingMethod(RoundingParams.RoundingMethod.OVERLAY_COLOR).setOverlayColor(ContextCompat.getColor(TheLApp.getContext(), R.color.white)));

        /***设置默认站位颜色***/
        int color = ContextCompat.getColor(TheLApp.getContext(), R.color.video_fall_default_color);
        final String videoColour = mVideoBean.videoColor;
        if (!TextUtils.isEmpty(videoColour) && videoColour.charAt(0) == '#') {
            try {
                color = Color.parseColor(videoColour);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        final Drawable drawable = new ColorDrawable(color);
        img_thumb.getHierarchy().setPlaceholderImage(drawable);
    }

    private void setTitle() {
        if (!TextUtils.isEmpty(mVideoBean.text)) {
            txt_title.setVisibility(View.VISIBLE);
        }
        txt_title.setText(mVideoBean.text + "");
    }

    private void setName() {
        txt_name.setText(mVideoBean.nickname + "");
    }

    private void setTime() {
        final String time = VideoFallsUtils.getFormatPlayTime(mVideoBean.playTime);
        txt_time.setText(time + "");
    }

    private void setLikeCount() {
        final int winkNum = mVideoBean.winkNum;
        String likeCount = winkNum + "";
        float count;
        if (winkNum > 9999) {
            if (Utils.isChaneseLanguage()) {//如果是中文版的，显示x.x万，保留小数点后一位
                count = Math.round(winkNum / 1000f) / 10f;
            } else {//显示x.xk，保留小数点后一位
                count = Math.round(winkNum / 100f) / 10f;
            }
            likeCount = StringUtils.getString(R.string.video_falls_like_count, count + "");
        }
        txt_like_count.setText(likeCount);
    }

    /********************************* 自动播放*********************************************/
    @Override
    public FrameLayout getContainer() {
        if (Utils.getTotalRam() > 2) {
            return video_container;
        } else
            return null;
    }

    @Override
    public int getVisibleHeightPercent() {
        if (isAttachedToWindow() && getVisibility() == View.VISIBLE) {
            final Rect localRect = new Rect();
            getLocalVisibleRect(localRect);
            if (localRect.left != 0) {
                return 0;
            }
            final int visibleHeight = Math.abs(localRect.height());
            return (int) (visibleHeight * 100 / pgcHeight);
        } else {
            return 0;
        }
    }

    @Override
    public int getPosition() {
        return mPosition;
    }

    @Override
    public VideoFallsPgcAdapterView bindAutoPlayVideoCtrl(AutoPlayVideoCtrl recyclerViewStateBean) {
        this.mAutoPlayVideoCtrl = recyclerViewStateBean;
        mAutoPlayVideoCtrl.addVideoAutoPlayView(this);
        return this;
    }

    @Override
    public VideoFallsPgcAdapterView getView() {
        return this;
    }

    @Override
    public RecyclerViewStateImp.RecyclerViewScrollStateChangedListener getRecyclerViewScrollStateChangedListener() {
        return autoPlayRecyclerViewStateListener;
    }

    @Override
    public VideoFallsPgcAdapterView stopPlay() {
        if (mAutoPlayVideoCtrl != null) {
            mAutoPlayVideoCtrl.stopPlay(this);
        }
        return this;
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        stopPlay();
    }

    @Override
    public VideoFallsPgcAdapterView showVideoLoading() {
        return this;
    }

    @Override
    public VideoFallsPgcAdapterView cancelVideoLoading() {
        return this;
    }

    @Override
    public String getVideoUrl() {
        if (mVideoBean != null) {
            return mVideoBean.videoUrl;
        }
        return null;
    }

    @Override
    public String getMomentId() {
        if (mVideoBean != null) {
            return mVideoBean.id;
        }
        return null;
    }

    @Override
    public VideoBean getVideoBean() {
        return mVideoBean;
    }

}
