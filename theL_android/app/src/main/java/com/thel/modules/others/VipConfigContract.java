package com.thel.modules.others;

import com.thel.base.BaseDataBean;
import com.thel.base.BasePresenter;
import com.thel.base.BaseView;
import com.thel.bean.AdBean;
import com.thel.bean.user.VipConfigBean;

/**
 * Created by lingwei on 2017/9/25.
 */

public class VipConfigContract {
    interface View extends BaseView<Presenter> {
        void getVipConfig(VipConfigBean vipConfigBean);//获取vip配置信息

        void getConfigResult(BaseDataBean baseDataBean);

        void getConfigResult(int liveHiding, int vipHiding, int hiding, int incognito, int followRemind,int msgHiding);

        void refreshAdArea(AdBean adBean);
    }

    interface Presenter extends BasePresenter {
        void processBusiness();

        void setVipConfigs(int liveHiding, int vipHiding, int hiding, int incognito, int followRemind,  int msgHiding,int flag);

        void loadAdvert();
    }

}
