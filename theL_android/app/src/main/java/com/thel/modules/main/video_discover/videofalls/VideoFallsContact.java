package com.thel.modules.main.video_discover.videofalls;

import com.thel.base.BasePresenter;
import com.thel.base.BaseView;
import com.thel.bean.AdBean;

import java.util.List;

/**
 * Created by waiarl on 2018/2/5.
 */

public class VideoFallsContact {
    interface Presenter extends BasePresenter {
        void getRefreshData();

        void getLoadMoreData(int cursor);

        void getAdsData();
    }

    interface View extends BaseView<Presenter> {
        void showRefreshData(VideoMomentListBean videoMomentListBean);

        void showMoreData(VideoMomentListBean videoMomentListBean);

        void emptyData();

        void loadMoreFailed();

        void requestFinished();

        void showAdsData(List<AdBean.Map_list> adList);


    }
}

