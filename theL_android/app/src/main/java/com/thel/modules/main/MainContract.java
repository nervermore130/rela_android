package com.thel.modules.main;

import androidx.fragment.app.Fragment;

import com.thel.base.BasePresenter;
import com.thel.base.BaseView;
import com.thel.bean.moments.MomentsCheckBean;

/**
 * Created by liuyun on 2017/9/14.
 */

public interface MainContract {
    interface View extends BaseView<Presenter> {

        boolean isActive();

        void updateUnreadTotal(int msgAmount);

        void updateMomentsAndRequestsTotal(MomentsCheckBean momentsCheck);

        void updateSendingMoments();

        void updateRecommendStciker();

        void refreshUnreadTotal(int number);

        void clearMomentsTotal(int type);

        void refreshNeedCompleteUserInfo(MomentsCheckBean momentsCheck);

        void updateFailedMoments();

        void gotoNearbyPage(int gotoTab);

        void gotoThemePage();

        void gotoMomentFragmentActivityPage();

        void getMomentCheckBean(MomentsCheckBean momentsCheckBean);

        void releaseSuccess(String momentsId, long releaseTime);

        void releaseFailed();

        void updateUnReadSeeMeTotal(MomentsCheckBean seeMeNum);

        void updateLikeMeTotal(MomentsCheckBean likeMe);

        void clearSeeMeView();

        void clearLikeMeNum();

        void clearNotReadCommentNum();

        void initDataModel(MomentsCheckBean momentsCheckBean);

        void showLivePage();

        void showMessagePage();

        void showFlutter();

        void gotoAPKSetting(String apkUrl);

    }

    interface Presenter extends BasePresenter {

        void init();

        void registerReceiver();

        void initDataModel(final Fragment fragment);

    }
}
