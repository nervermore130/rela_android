package com.thel.modules.main.me.aboutMe;

import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.base.BaseDataBean;
import com.thel.bean.FavoriteBean;
import com.thel.bean.FavoritesBean;
import com.thel.modules.main.me.adapter.FavoriteAdapter;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.RequestConstants;
import com.thel.ui.decoration.DefaultItemDivider;
import com.thel.ui.widget.DefaultEmptyView;
import com.thel.ui.widget.MySwipeRefreshLayout;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.utils.BusinessUtils;
import com.thel.utils.DialogUtil;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SharedPrefUtils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.thel.modules.main.home.community.ThemeSortListFragment.REFRESH_TYPE_ALL;
import static com.thel.modules.main.home.community.ThemeSortListFragment.REFRESH_TYPE_NEXT_PAGE;

public class FavoriteActivity extends BaseActivity {

    @BindView(R.id.txt_title)
    TextView txt_title;

    @BindView(R.id.recycleview)
    RecyclerView mRecyclerView;

    @BindView(R.id.swipe_container)
    MySwipeRefreshLayout swipe_container;

    @BindView(R.id.default_tip)
    DefaultEmptyView rel_defalut_tip;//默认界面

    // 刷新类型，全部刷新（即下拉刷新）为1，还是加载下一页为2
    public int refreshType = 0;
    public static final int LIMIT = 20;//分页加载每页个数
    public int cursor = 0;//分页加载当前页

    private FavoriteBean operateMoment;//操作的operateMmet
    private int operatePosition = -1;//操作的position

    private List<FavoriteBean> list = new ArrayList<>();//收藏日志集合

    private FavoriteAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_favorite);

        ButterKnife.bind(this);

        refreshType = REFRESH_TYPE_ALL;

        initList();

        initView();

        processBusiness();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(this.getClass().getName());
        MobclickAgent.onResume(this);
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(this.getClass().getName());
        MobclickAgent.onPause(this);
    }

    private void initList() {
        mAdapter = new FavoriteAdapter(list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addItemDecoration(new DefaultItemDivider(this, LinearLayoutManager.VERTICAL, R.color.gray, 1, true, false));

        mAdapter.setOnLoadMoreListener(new BaseRecyclerViewAdapter.RequestLoadMoreListener() {//加载更多
            @Override
            public void onLoadMoreRequested() {
                mRecyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        if (cursor > 0) {
                            // 列表滑动到底部加载下一组数据
                            refreshType = REFRESH_TYPE_NEXT_PAGE;
                            processBusiness();
                        } else {//为0说明没有更多数据
                            mAdapter.openLoadMore(0, false);
                            if (list.size() > 0) {
                                View view = getLayoutInflater().inflate(R.layout.load_more_footer_layout, (ViewGroup) mRecyclerView.getParent(), false);
                                ((TextView) view.findViewById(R.id.text)).setText(getString(R.string.info_no_more));
                                mAdapter.addFooterView(view);
                            }
                        }
                    }
                });
            }
        });
        mAdapter.setReloadMoreListener(new BaseRecyclerViewAdapter.ReloadMoreListener() {//加载更多失败后重新加载
            @Override
            public void reloadMore() {
                refreshType = REFRESH_TYPE_NEXT_PAGE;
                mAdapter.removeAllFooterView();
                mAdapter.openLoadMore(true);
                processBusiness();
            }


        });
        mAdapter.setOnRecyclerViewItemClickListener(mAdapter);
        mAdapter.setOnRecyclerViewItemLongClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemLongClickListener() {
            @Override
            public boolean onItemLongClick(View view, int position) {
                deleteMoment(position);//删除日志
                return false;
            }
        });
    }

    private void initView() {
        txt_title.setText(getString(R.string.collection));

        ViewUtils.initSwipeRefreshLayout(swipe_container);
        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                cursor = 0;
                refreshType = REFRESH_TYPE_ALL;
                processBusiness();
            }
        });
        swipe_container.setRefreshing(true);

    }

    /**
     * 删除收藏的某篇日志
     *
     * @param position
     */
    private void deleteMoment(int position) {
        operateMoment = list.get(position);
        operatePosition = position;
        if (operateMoment == null) {
            return;
        }
        DialogUtil.getInstance().showSelectionDialog(this, new String[]{getString(R.string.info_delete)}, new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View view, final int p, long arg3) {
                ViewUtils.preventViewMultipleClick(view, 2000);
                DialogUtil.getInstance().closeDialog();
                switch (p) {
                    case 0:
                        showLoading();
                        RequestBusiness.getInstance()
                                .deleteFavoriteMoment(operateMoment.id, "", RequestConstants.FAVORITE_TYPE_MOM)
                                .onBackpressureDrop()
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(new InterceptorSubscribe<BaseDataBean>() {
                                    @Override
                                    public void onNext(BaseDataBean data) {
                                        super.onNext(data);
                                        closeLoading();
                                        String collectList = SharedPrefUtils.getString(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.FAVORITE_MOMENT_LIST, "");
                                        if (operateMoment != null && BusinessUtils.isCollected(operateMoment.momentsId)) {
                                            String id = "[" + ShareFileUtils.getString(ShareFileUtils.ID, "") + "_" + operateMoment.momentsId + "]";
                                            collectList = collectList.replace(id, "");
                                            SharedPrefUtils.setString(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.FAVORITE_MOMENT_LIST, collectList);
                                        }
                                        if (operatePosition != -1) {
                                            list.remove(operatePosition);
                                            mAdapter.notifyDataSetChanged();
                                            operatePosition = -1;
                                            mAdapter.setPageSize(list.size());
                                            if (list.size() == 0) {
                                                rel_defalut_tip.setVisibility(View.VISIBLE);
                                                mAdapter.openLoadMore(0, false);
                                                mRecyclerView.post(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        mAdapter.removeAllFooterView();
                                                    }
                                                });
                                            }
                                        }
                                        DialogUtil.showToastShort(FavoriteActivity.this, getString(R.string.collection_canceled));
                                    }
                                });
                        break;
                    default:
                        break;
                }
            }
        }, false, 2, null);
    }

    private void processBusiness() {
        RequestBusiness.getInstance()
                .getFavoriteList(LIMIT, cursor)
                .onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new InterceptorSubscribe<FavoritesBean>() {
                    @Override
                    public void onNext(FavoritesBean data) {
                        super.onNext(data);
                        refreshList(data);
                        requestFinished();
                    }

                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                        mRecyclerView.post(new Runnable() {
                            @Override
                            public void run() {
                                mAdapter.loadMoreFailed((ViewGroup) mRecyclerView.getParent());
                            }
                        });
                        requestFinished();
                    }
                });
    }

    /**
     * 更新list数据
     *
     * @param bean
     */
    private void refreshList(FavoritesBean bean) {
        if (refreshType == REFRESH_TYPE_ALL) {//如果是刷新界面
            list.clear();
        }
        if (bean != null && bean.data != null) {
            cursor = bean.data.cursor;
            if (bean.data.list != null)
                list.addAll(bean.data.list);
        }
        if (refreshType == REFRESH_TYPE_ALL) {
            mAdapter.removeAllFooterView();
            mAdapter.setNewData(list);
            if (list.size() > 0) {
                mAdapter.openLoadMore(list.size(), true);
                rel_defalut_tip.setVisibility(View.GONE);
            } else {
                mAdapter.openLoadMore(list.size(), false);
                rel_defalut_tip.setVisibility(View.VISIBLE);
            }
        } else {
            mAdapter.notifyDataChangedAfterLoadMore(true, list.size());
        }
        if (refreshType == REFRESH_TYPE_ALL) {
            if (mRecyclerView.getChildCount() > 0) {// 回到顶部
                mRecyclerView.scrollToPosition(0);
            }
        }
        refreshShareCollect(list);//刷新sharepreference中的收藏数据
    }

    /**
     * 刷新sharepreference中的收藏数据
     *
     * @param list
     */
    private void refreshShareCollect(List<FavoriteBean> list) {
        for (int i = 0; i < list.size(); i++) {
            if (BusinessUtils.isCollected(list.get(i).momentsId)) {//如果存在，继续循环
                continue;
            }
            String collectList = SharedPrefUtils.getString(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.FAVORITE_MOMENT_LIST, "");
            String id = "[" + ShareFileUtils.getString(ShareFileUtils.ID, "") + "_" + list.get(i).momentsId + "]";
            collectList += id;
            SharedPrefUtils.setString(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.FAVORITE_MOMENT_LIST, collectList);
        }
    }

    private void requestFinished() {
        if (swipe_container != null)
            swipe_container.postDelayed(new Runnable() {

                @Override
                public void run() {
                    if (swipe_container != null && swipe_container.isRefreshing())
                        swipe_container.setRefreshing(false);
                }
            }, 1000);
        closeLoading();
    }

    @OnClick(R.id.lin_back)
    void back() {
        finish();
    }
}
