package com.thel.modules.live.view.sound;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.thel.R;
import com.thel.base.BaseDialogFragment;
import com.thel.modules.live.bean.LiveSoundChoiceAppBean;
import com.thel.utils.AppInit;
import com.thel.utils.SizeUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 多人连麦 选择声音特效
 * Created by lingwei on 2018/5/6.
 */

public class LiveSoundEffectDialogView extends BaseDialogFragment {
    private RecyclerView recyclerView;
    private TextView txt_cancel;
    private LinearLayoutManager manager;
    private List<LiveSoundChoiceAppBean> list = new ArrayList<>();
    private LinearLayout laugh_ll;
    private LinearLayout brava_ll;
    private LinearLayout fool_ll;
    private LinearLayout mock_ll;
    private LinearLayout applause_ll;
    private LottieAnimationView multi_sound_effect_daxiao;
    private LottieAnimationView multi_sound_effect_huanhu;
    private LottieAnimationView multi_sound_effect_chuqiu;
    private LottieAnimationView multi_sound_effect_chaoxiao;
    private LottieAnimationView multi_sound_effect_guzhang;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.live_sound_effect_choice_view, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        init(view);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setListener();
    }

    private void init(View view) {
        recyclerView = view.findViewById(R.id.recyclerview);
        txt_cancel = view.findViewById(R.id.txt_cancel);
        laugh_ll = view.findViewById(R.id.laugh_ll);
        brava_ll = view.findViewById(R.id.brava_ll);
        fool_ll = view.findViewById(R.id.fool_ll);
        mock_ll = view.findViewById(R.id.mock_ll);
        applause_ll = view.findViewById(R.id.applause_ll);

        multi_sound_effect_daxiao = view.findViewById(R.id.multi_sound_effect_daxiao);
        multi_sound_effect_huanhu = view.findViewById(R.id.multi_sound_effect_huanhu);
        multi_sound_effect_chuqiu = view.findViewById(R.id.multi_sound_effect_chuqiu);
        multi_sound_effect_chaoxiao = view.findViewById(R.id.multi_sound_effect_chaoxiao);
        multi_sound_effect_guzhang = view.findViewById(R.id.multi_sound_effect_guzhang);

    }

    private void setListener() {
        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LiveSoundEffectDialogView.this.dismiss();
            }
        });
        laugh_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(0);
                }
                setScalenListener(multi_sound_effect_daxiao);
            }
        });
        brava_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(1);
                }
                setScalenListener(multi_sound_effect_huanhu);

            }
        });
        fool_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(2);
                }
                setScalenListener(multi_sound_effect_chuqiu);

            }
        });
        mock_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(3);
                }
                setScalenListener(multi_sound_effect_chaoxiao);

            }
        });
        applause_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(4);
                }
                setScalenListener(multi_sound_effect_guzhang);

            }
        });

    }

    private void setScalenListener(LottieAnimationView lottieAnimationView) {
        AnimatorSet animatorSet = new AnimatorSet();
        ObjectAnimator scaleXAnimator = ObjectAnimator.ofFloat(lottieAnimationView, "scaleX", 1f, 1.8f, 1f);
        ObjectAnimator scaleYAnimator = ObjectAnimator.ofFloat(lottieAnimationView, "scaleY", 1f, 1.8f, 1f);
        animatorSet.playTogether(scaleXAnimator, scaleYAnimator);
        animatorSet.setDuration(1000);
        animatorSet.start();
    }


    @Override
    public void onResume() {
        super.onResume();
        int width = AppInit.displayMetrics.widthPixels;
        int height = SizeUtils.dip2px(getContext(), 130);
        getDialog().getWindow().setLayout(width, height);
    }

    private OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }
}
