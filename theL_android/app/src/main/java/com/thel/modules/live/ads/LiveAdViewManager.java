package com.thel.modules.live.ads;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewStub;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.thel.R;
import com.thel.bean.LiveAdInfoBean;
import com.thel.constants.BundleConstants;
import com.thel.modules.live.bean.LiveRoomBean;
import com.thel.modules.main.home.moments.web.WebViewActivity;
import com.thel.utils.L;

/**
 * 为观众端和主播端的广告view展示逻辑的统一管理
 * <p>
 * 左上活动页比右下活动页，多了offset逻辑，位置顺序也有差别
 * <p>
 * add by wangjun 2019.11.13
 */
public class LiveAdViewManager {
    private final static String TAG = LiveAdViewManager.class.getSimpleName();
    private ViewStub leftTopContainer;//为观众端和主播端的直播、电台设计的广告位容器
    private ViewStub rightBottomContainer;//为观众端和主播端的热聊设计的广告位容器

    private ViewStub currentContainer;

    private LiveAdInfoBean liveAdInfoBean;
    private LiveRoomBean liveRoomBean;

    private View leftTopInlateView;
    private View rightBottomInlateView;

    public LiveAdViewManager(ViewStub leftTopContainer, ViewStub rightBottomContainer) {
        this.leftTopContainer = leftTopContainer;
        this.rightBottomContainer = rightBottomContainer;

//        leftTopContainer.setVisibility(View.VISIBLE);
    }

    public void show() {
        show(liveRoomBean, liveAdInfoBean);
    }

    public void show(LiveRoomBean liveRoomBean, LiveAdInfoBean liveAdInfoBean) {
        L.d(TAG, "show " + liveAdInfoBean);
        if (liveAdInfoBean == null || liveRoomBean == null) {
            L.d(TAG, "show bean is null");
            return;
        }
        if (currentContainer != null) {
            currentContainer.setVisibility(View.INVISIBLE);
            currentContainer = null;
        }
        this.liveRoomBean = liveRoomBean;
        this.liveAdInfoBean = liveAdInfoBean;
        if (liveAdInfoBean.status <= 0 || liveAdInfoBean.list == null || liveAdInfoBean.list.size() == 0) {
            dismiss();
            return;
        }

        LiveAdInfoBean.LiveAdCellBean no1AdCell = liveAdInfoBean.list.get(0);
        LiveAdInfoBean.LiveAdCellBean no2AdCell = null;
        if (liveAdInfoBean.list.size() > 1) {
            no2AdCell = liveAdInfoBean.list.get(1);
        }

        if (LiveRoomBean.MULTI_LIVE == liveRoomBean.isMulti) {//热聊
            RightBottomHolder holder = null;
            if (rightBottomInlateView == null) {
                rightBottomInlateView = rightBottomContainer.inflate();
                holder = new RightBottomHolder(rightBottomInlateView);
                rightBottomInlateView.setTag(holder);
            } else {
                rightBottomInlateView.setVisibility(View.VISIBLE);
                holder = (RightBottomHolder) rightBottomInlateView.getTag();
            }

            updateAdImageView(holder.ad_view, no1AdCell.icon, no1AdCell.url);
            updateAdRankView(holder.top_tv, no1AdCell.rank);

            if (no2AdCell == null) {//目前2号活动位可能为空
                updateAdImageView(holder.ad_view2, null, null);
                updateAdRankView(holder.top_tv2, null);
            } else {
                updateAdImageView(holder.ad_view2, no2AdCell.icon, no2AdCell.url);
                updateAdRankView(holder.top_tv2, no2AdCell.rank);
            }
            currentContainer = rightBottomContainer;
        } else {//直播、电台
            LeftTopHolder holder = null;
            if (leftTopInlateView == null) {
                leftTopInlateView = leftTopContainer.inflate();
                holder = new LeftTopHolder(leftTopInlateView);
                leftTopInlateView.setTag(holder);
            } else {
                leftTopInlateView.setVisibility(View.VISIBLE);
                holder = (LeftTopHolder) leftTopInlateView.getTag();
            }

            updateAdImageView(holder.ad_view, no1AdCell.icon, no1AdCell.url);
            updateAdRankView(holder.top_tv, no1AdCell.rank);
            updateOffsetView(holder.offset_tv, no1AdCell.offset);

            if (no2AdCell == null) {//目前2号活动位可能为空
                updateAdImageView(holder.ad_view2, null, null);
                updateAdRankView(holder.top_tv2, null);
                updateOffsetView(holder.offset_tv2, null);
            } else {
                updateAdImageView(holder.ad_view2, no2AdCell.icon, no2AdCell.url);
                updateAdRankView(holder.top_tv2, no2AdCell.rank);
                updateOffsetView(holder.offset_tv2, no2AdCell.offset);
            }

            currentContainer = leftTopContainer;
        }
    }

    /**
     *
     */
    private void updateAdImageView(SimpleDraweeView simpleDraweeView, String icon, final String url) {
        if (!TextUtils.isEmpty(icon)) {
            Uri uri = Uri.parse(icon);
            DraweeController controller = Fresco.newDraweeControllerBuilder().setUri(uri).setAutoPlayAnimations(true).build();
            simpleDraweeView.setController(controller);
            simpleDraweeView.setVisibility(View.VISIBLE);

            simpleDraweeView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(v.getContext(), WebViewActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(BundleConstants.URL, url);
                    intent.putExtras(bundle);
                    v.getContext().startActivity(intent);
                }
            });
        } else {
            simpleDraweeView.setVisibility(View.INVISIBLE);
            simpleDraweeView.setOnClickListener(null);
        }
    }

    /**
     *
     */
    private void updateAdRankView(TextView textView, String rank) {
        if (!TextUtils.isEmpty(rank)) {
            textView.setText("NO." + rank);
            textView.setVisibility(View.VISIBLE);
        } else {
            textView.setVisibility(View.INVISIBLE);
        }
    }

    /**
     *
     */
    private void updateOffsetView(TextView textView, String offset) {
        if (!TextUtils.isEmpty(offset)) {
            textView.setVisibility(View.VISIBLE);
            textView.setText(liveAdInfoBean.offset);
        } else {
            textView.setVisibility(View.INVISIBLE);
        }
    }

    public void dismiss() {
        L.d(TAG, "dismiss");
        if (currentContainer != null) {
            currentContainer.setVisibility(View.INVISIBLE);
            currentContainer = null;
        }

//        if (leftTopContainer.getVisibility() == View.VISIBLE) {
//            leftTopContainer.setVisibility(View.GONE);
//        }
//        if (rightBottomContainer.getVisibility() == View.VISIBLE) {
//            rightBottomContainer.setVisibility(View.GONE);
//        }
    }


    private static class LeftTopHolder {

        public TextView offset_tv;
        public TextView offset_tv2;

        public SimpleDraweeView ad_view;
        public TextView top_tv;

        public SimpleDraweeView ad_view2;
        public TextView top_tv2;

        public LeftTopHolder(View view) {
            ad_view = view.findViewById(R.id.ad_view);
            top_tv = view.findViewById(R.id.top_tv);
            ad_view2 = view.findViewById(R.id.ad_view_2);
            top_tv2 = view.findViewById(R.id.top_tv_2);

            offset_tv = view.findViewById(R.id.offset_tv);
            offset_tv2 = view.findViewById(R.id.offset_tv_2);
        }

        public void init(LiveAdInfoBean.LiveAdCellBean no1AdCell, LiveAdInfoBean.LiveAdCellBean no2AdCell) {


        }
    }

    private static class RightBottomHolder {

        public SimpleDraweeView ad_view;
        public TextView top_tv;

        public SimpleDraweeView ad_view2;
        public TextView top_tv2;

        public RightBottomHolder(View view) {
            ad_view2 = view.findViewById(R.id.ad_multi_view);
            top_tv2 = view.findViewById(R.id.top_multi_tv);
            ad_view = view.findViewById(R.id.ad_multi_view_2);
            top_tv = view.findViewById(R.id.top_multi_tv_2);
        }


    }

}
