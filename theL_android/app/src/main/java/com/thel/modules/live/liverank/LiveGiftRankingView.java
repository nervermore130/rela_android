package com.thel.modules.live.liverank;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.constants.TheLConstants;
import com.thel.modules.live.in.LiveBaseView;
import com.thel.modules.live.utils.LiveUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.thel.utils.Utils.getColor;

/**
 * Created by waiarl on 2017/10/26.
 */

public class LiveGiftRankingView extends RelativeLayout implements View.OnClickListener, LiveBaseView<LiveGiftRankingView>, LiveGiftRankingListFragment.ShowRankInterface {
    private final Context mContext;
    private RelativeLayout rel_ranking_liset;
    private FrameLayout close_layout;
    private LinearLayout lin_ranking_list_tabs;
    private RelativeLayout rel_daily;
    private TextView txt_daily_tab;
    private ImageView img_daily_bottomline;
    private RelativeLayout rel_weekly;
    private TextView txt_weekly_tab;
    private ImageView img_weekly_bottomline;
    private RelativeLayout rel_total;
    private TextView txt_total_tab;
    private ImageView img_total_bottomline;
    private ViewPager viewpager;
    private FragmentManager manager;
    private String userId;
    private LiveGiftRankingListFragment dailyFragment;
    private LiveGiftRankingListFragment weeklyFragment;
    private LiveGiftRankingListFragment totalFragment;
    private List<Fragment> list = new ArrayList<>();
    private LiveGiftViewPagerAdapter adapter;
    private boolean isAnimating = false;
    private TextView txt_header;

    public LiveGiftRankingView(Context context) {
        this(context, null);
    }

    public LiveGiftRankingView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LiveGiftRankingView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }

    protected LiveGiftRankingView init() {
        inflate(mContext, R.layout.live_gift_ranking_view, this);

        rel_ranking_liset = findViewById(R.id.rel_ranking_list);
        close_layout = findViewById(R.id.close_layout);
        lin_ranking_list_tabs = findViewById(R.id.lin_ranking_list_tabs);
        rel_daily = findViewById(R.id.rel_daily);
        txt_daily_tab = findViewById(R.id.txt_daily_tab);
        img_daily_bottomline = findViewById(R.id.img_daily_bottomline);
        rel_weekly = findViewById(R.id.rel_weekly);
        txt_weekly_tab = findViewById(R.id.txt_weekly_tab);
        img_weekly_bottomline = findViewById(R.id.img_weekly_bottomline);
        rel_total = findViewById(R.id.rel_total);
        txt_total_tab = findViewById(R.id.txt_total_tab);
        img_total_bottomline = findViewById(R.id.img_total_bottomline);
        txt_header = findViewById(R.id.txt_header);
        viewpager = findViewById(R.id.view_pager_ranking_list);
        viewpager.setOffscreenPageLimit(0);
        return this;
    }

    public LiveGiftRankingView initView(FragmentManager manager, String userId) {
        this.manager = manager;
        this.userId = userId;
        if (!TextUtils.isEmpty(this.userId) && this.userId.equals(userId) && viewpager.getAdapter() != null && viewpager.getAdapter().getCount() > 0 && dailyFragment != null && weeklyFragment != null && totalFragment != null) {
            return this;
        }
        initViewPager();
        return this;
    }


    public LiveGiftRankingView initData() {
        return this;
    }

    /*************************一下为显示和隐藏的方法***************************************/
    public LiveGiftRankingView show() {
        LiveUtils.animInLiveShowView(this);
        return this;
    }

    public LiveGiftRankingView hide() {
        LiveUtils.animOutLiveShowView(this, false);
        return this;
    }

    public LiveGiftRankingView destroyView() {
        setVisibility(GONE);
        isAnimating = false;
        clearAnimation();
        setTranslationX(0f);
        setTranslationY(0f);
        setTab(0);
        viewpager.setAdapter(null);
        adapter = null;
        list.clear();
        dailyFragment = null;
        weeklyFragment = null;
        totalFragment = null;
        return this;
    }

    @Override
    public boolean isAnimating() {
        return isAnimating;
    }

    @Override
    public void setAnimating(boolean isAnimating) {
        this.isAnimating = isAnimating;
    }

    @Override
    public void showShade(boolean show) {

    }

    /*************************以上为显示和隐藏的方法***************************************/

    protected void initViewPager() {
        if (manager == null) {
            return;
        }
        getFragments();
        adapter = new LiveGiftViewPagerAdapter(manager, list);
        viewpager.setAdapter(adapter);
        //  viewpager.setOffscreenPageLimit(0);
        setTab(0);

      /*  SpannableString liveRankData = fragment.getLiveRankData();
        txt_header.setText(liveRankData);*/
        setListener();
    }

    private void setListener() {
        rel_daily.setOnClickListener(this);
        rel_weekly.setOnClickListener(this);
        rel_total.setOnClickListener(this);
        viewpager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                setTab(position);
            }
        });
    }

    private void getFragments() {
        dailyFragment = (LiveGiftRankingListFragment) manager.findFragmentByTag(LiveGiftRankingListFragment.TAG_DAILY);
        if (dailyFragment == null) {
            final Bundle bundle = new Bundle();
            bundle.putString(TheLConstants.BUNDLE_KEY_FRAGMENT_TAG, LiveGiftRankingListFragment.TAG_DAILY);
            bundle.putString(TheLConstants.BUNDLE_KEY_USER_ID, userId);
            dailyFragment = LiveGiftRankingListFragment.getInstance(bundle);
        }

        weeklyFragment = (LiveGiftRankingListFragment) manager.findFragmentByTag(LiveGiftRankingListFragment.TAG_WEEKLY);
        if (weeklyFragment == null) {
            final Bundle bundle = new Bundle();
            bundle.putString(TheLConstants.BUNDLE_KEY_FRAGMENT_TAG, LiveGiftRankingListFragment.TAG_WEEKLY);
            bundle.putString(TheLConstants.BUNDLE_KEY_USER_ID, userId);
            weeklyFragment = LiveGiftRankingListFragment.getInstance(bundle);
        }

        totalFragment = (LiveGiftRankingListFragment) manager.findFragmentByTag(LiveGiftRankingListFragment.TAG_TOTAL);
        if (totalFragment == null) {
            final Bundle bundle = new Bundle();
            bundle.putString(TheLConstants.BUNDLE_KEY_FRAGMENT_TAG, LiveGiftRankingListFragment.TAG_MOUTH);
            bundle.putString(TheLConstants.BUNDLE_KEY_USER_ID, userId);
            totalFragment = LiveGiftRankingListFragment.getInstance(bundle);
        }
        list.clear();
        Collections.addAll(list, dailyFragment, weeklyFragment, totalFragment);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rel_daily:
                setTab(0);
                break;
            case R.id.rel_weekly:
                setTab(1);
                break;
            case R.id.rel_total:
                setTab(2);
                break;

        }
    }

    private void setTab(int tab) {
        img_daily_bottomline.setVisibility(tab == 0 ? VISIBLE : GONE);
        img_weekly_bottomline.setVisibility(tab == 1 ? VISIBLE : GONE);
        img_total_bottomline.setVisibility(tab == 2 ? VISIBLE : GONE);
        txt_daily_tab.setTextColor(getResources().getColor(tab == 0 ? R.color.text_color_green : R.color.text_color_gray));
        txt_weekly_tab.setTextColor(getResources().getColor(tab == 1 ? R.color.text_color_green : R.color.text_color_gray));
        txt_total_tab.setTextColor(getResources().getColor(tab == 2 ? R.color.text_color_green : R.color.text_color_gray));
        txt_daily_tab.setTypeface(null, tab == 0 ? Typeface.BOLD : Typeface.NORMAL);
        txt_weekly_tab.setTypeface(null, tab == 1 ? Typeface.BOLD : Typeface.NORMAL);
        txt_total_tab.setTypeface(null, tab == 2 ? Typeface.BOLD : Typeface.NORMAL);
        if (viewpager.getCurrentItem() != tab) {
            viewpager.setCurrentItem(tab);
        }
        LiveGiftRankingListFragment fragment = (LiveGiftRankingListFragment) list.get(tab);
        SpannableString rankData = fragment.getLiveRankData();
        txt_header.setText(rankData);
        fragment.setLiveRankLisener(this);

    }

    public void setCloseLayoutClickListener(OnClickListener listener) {
        close_layout.setOnClickListener(listener);
    }

    @Override
    public void setLiveRankTitleSp(String content, String rank, String dou) {
        txt_header.setText(getLiveRankTitleSp(content, rank, dou));
    }

    private SpannableString getLiveRankTitleSp(String content, String rank, String dou) {
        final SpannableString sp = new SpannableString(content);
        final int start1 = content.indexOf(rank);
        final int end1 = rank.length() + start1;
        final int start2 = content.lastIndexOf(dou);
        final int end2 = start2 + dou.length();
        sp.setSpan(new ForegroundColorSpan(getColor(R.color.live_gift_rank_header_sp_color)), start1, end1, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        sp.setSpan(new AbsoluteSizeSpan(15, true), start1, end1, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        sp.setSpan(new ForegroundColorSpan(getColor(R.color.live_gift_rank_header_sp_color)), start2, end2, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        sp.setSpan(new AbsoluteSizeSpan(15, true), start2, end2, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        return sp;
    }

    /*************************************内部类*******************************************/
    class LiveGiftViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> list;

        public LiveGiftViewPagerAdapter(FragmentManager fm, List<Fragment> list) {
            super(fm);
            this.list = list;
        }

        @Override
        public Fragment getItem(int position) {
            return list.get(position);
        }

        @Override
        public int getCount() {
            return list.size();
        }
    }

}
