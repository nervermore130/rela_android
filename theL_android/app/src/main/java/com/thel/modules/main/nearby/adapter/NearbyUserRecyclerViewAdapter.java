package com.thel.modules.main.nearby.adapter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.postprocessors.IterativeBoxBlurPostProcessor;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.AdBean;
import com.thel.bean.user.NearUserBean;
import com.thel.constants.TheLConstants;
import com.thel.growingio.GrowingIoConstant;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.live.surface.watch.LiveWatchActivity;
import com.thel.modules.main.me.aboutMe.UpdateUserInfoActivity;
import com.thel.modules.main.me.bean.LogInfoBean;
import com.thel.modules.main.me.match.eventcollect.collect.LiveLogUtils;
import com.thel.modules.main.me.match.eventcollect.collect.MatchLogUtils;
import com.thel.modules.main.nearby.CoverPreviewActivity;
import com.thel.modules.main.nearby.HotStarActivity;
import com.thel.modules.main.nearby.NearbyPeopleFragment;
import com.thel.modules.others.VipConfigActivity;
import com.thel.network.service.DefaultRequestService;
import com.thel.ui.dialog.ActionSheetDialog;
import com.thel.ui.widget.BannerLayout;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;
import com.thel.utils.AppInit;
import com.thel.utils.BusinessUtils;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.ImageUtils;
import com.thel.utils.MD5Utils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import video.com.relavideolibrary.Utils.DensityUtils;

/**
 * Created by waiarl on 2017/10/11.
 * 附近用户瀑布流adapter
 */

public class NearbyUserRecyclerViewAdapter extends BaseRecyclerViewAdapter<NearUserBean> {
    //是否显示距离
    private final boolean hideNearbyDistance;
    private boolean isNearby = false;//是否是附近页面，附近页面的话，第一个是自己，距离要显示宇宙中心

    private float photoWidth; // 图片宽度
    private float photoHeigth; // 图片高度
    private float radius; // 图片高度
    private RelativeLayout.LayoutParams layoutParams;
    private NearbyPeopleFragment nearbyPeopleFragment;
    private String pageId;
    private String from_page;
    private String from_page_id;
    private String sort;

    public void setNearbyPeopleFragment(NearbyPeopleFragment nearbyPeopleFragment) {
        this.nearbyPeopleFragment = nearbyPeopleFragment;
    }

    private HidingBraodcast hidingBraodcast;

    private class HidingBraodcast extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (TheLConstants.BROADCAST_ACTION_HIDING.equals(intent.getAction())) {
                if (mData != null && mData.size() > 0) {
                    mData.get(0).hiding = intent.getIntExtra("hiding", 0);
                }
                notifyDataSetChanged();
            }
        }
    }

    public void registerReceiver(Context context) {
        hidingBraodcast = new HidingBraodcast();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(TheLConstants.BROADCAST_ACTION_HIDING);
        context.registerReceiver(hidingBraodcast, intentFilter);
    }

    public void setLogDatas(String pageId, String from_page, String from_page_id, String sort) {
        this.pageId = pageId;
        this.from_page = from_page;
        this.from_page_id = from_page_id;
        this.sort = sort;
    }

    public void unRegisterReceiver(Context context) {
        if (hidingBraodcast != null) context.unregisterReceiver(hidingBraodcast);
    }

    public NearbyUserRecyclerViewAdapter(List<NearUserBean> list, boolean hideNearbyDistance) {
        super(R.layout.adapter_nearby_user_recyclerview, list);
        this.hideNearbyDistance = hideNearbyDistance;

        photoWidth = (AppInit.displayMetrics.widthPixels - TheLApp.getContext().getResources().getDimension(R.dimen.nearby_user_view_padding_2x) * 4) / 3;
        photoHeigth = photoWidth / 9 * 16;
        radius = TheLApp.getContext().getResources().getDimension(R.dimen.nearby_user_view_radius);
    }

    public NearbyUserRecyclerViewAdapter(List<NearUserBean> list, boolean hideNearbyDistance, boolean isNearby) {
        super(R.layout.adapter_nearby_user_recyclerview, list);
        this.hideNearbyDistance = hideNearbyDistance;

        photoWidth = (AppInit.displayMetrics.widthPixels - TheLApp.getContext().getResources().getDimension(R.dimen.nearby_user_view_padding_2x) * 4) / 3;
        photoHeigth = photoWidth / 9 * 16;
        radius = TheLApp.getContext().getResources().getDimension(R.dimen.nearby_user_view_radius);
        this.isNearby = isNearby;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);

        RecyclerView.LayoutManager manager = recyclerView.getLayoutManager();
        if (manager instanceof GridLayoutManager) {
            GridLayoutManager gridManager = ((GridLayoutManager) manager);

            gridManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    //附近中的banner,附近的人有header
                    if (isNearby) {
                        if (getItemViewType(position) == HEADER_VIEW) {
                            return 3;
                        } else {
                            try {
                                NearUserBean item = getItem(position - getHeaderLayoutCount());
                                if (item != null && item.itemType == 2) return 3;
                            } catch (Exception e) {
                                e.printStackTrace();
                                return 1;
                            }

                        }
                    }
                    return 1;
                }
            });
        }
    }

    public void traceNearbyLog(String activity, int index, String rank_id, String userid, String sort) {
        try {
            String latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
            String longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");

            LogInfoBean logInfoBean = new LogInfoBean();
            logInfoBean.page = "around.list";
            logInfoBean.page_id = pageId;
            logInfoBean.activity = activity;
            logInfoBean.from_page_id = from_page_id;
            logInfoBean.from_page = from_page;
            logInfoBean.lat = latitude;
            logInfoBean.lng = longitude;

            LogInfoBean.LogsDataBean logsDataBean = new LogInfoBean.LogsDataBean();

            if (index != -1) {
                logsDataBean.index = index;
            }

            if (!TextUtils.isEmpty(rank_id)) {
                logsDataBean.rank_id = rank_id;
            }

            if (!TextUtils.isEmpty(userid)) {
                logsDataBean.user_id = userid;
            }
            logsDataBean.sort = sort;
            logInfoBean.data = logsDataBean;

            LiveLogUtils.getInstance().addLog(logInfoBean);

        } catch (Exception e) {

        }
    }

    @Override
    protected void convert(BaseViewHolder holdView, final NearUserBean userBean) {
        holdView.setVisibility(R.id.ll_distance, View.GONE);
        holdView.setVisibility(R.id.rl_vertify, View.GONE);
        holdView.setVisibility(R.id.ll_live_intro, View.GONE);
        holdView.setVisibility(R.id.lin_live, View.GONE);
        holdView.setVisibility(R.id.lin_hide, View.GONE);
        holdView.setVisibility(R.id.img_vip, View.GONE);
        holdView.setVisibility(R.id.img_setting, View.GONE);

        /**
         * 埋点上报
         * */
        traceNearbyLog("exposure", holdView.getAdapterPosition(), userBean.rank_id, userBean.userId + "", sort);
        //人气明星入口
        if (isNearby && userBean.itemType == 1) {
            holdView.setVisibility(R.id.star_hot_container, View.VISIBLE);
            holdView.setVisibility(R.id.image_container, View.GONE);
            holdView.setVisibility(R.id.banner, View.GONE);

            RelativeLayout star_hot_container = holdView.getView(R.id.star_hot_container);
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) star_hot_container.getLayoutParams();
            layoutParams.height = (int) photoHeigth;

            ImageView hot_iv = holdView.getView(R.id.hot_iv);
            ImageView star_iv = holdView.getView(R.id.star_iv);
            ImageLoaderManager.imageLoaderDefaultCorner(hot_iv, R.color.white, userBean.hs_images.hit_image, DensityUtils.dp2px(3));
            ImageLoaderManager.imageLoaderDefaultCorner(star_iv, R.color.white, userBean.hs_images.star_image, DensityUtils.dp2px(3));
            hot_iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, HotStarActivity.class);
                    intent.putExtra(HotStarActivity.KEY_TAB, HotStarActivity.TAG_HOT);
                    mContext.startActivity(intent);
                }
            });
            star_iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, HotStarActivity.class);
                    intent.putExtra(HotStarActivity.KEY_TAB, HotStarActivity.TAG_STAR);
                    mContext.startActivity(intent);
                }
            });
        }
        //banner
        else if (isNearby && userBean.itemType == 2) {
            holdView.setVisibility(R.id.star_hot_container, View.GONE);
            holdView.setVisibility(R.id.image_container, View.GONE);
            holdView.setVisibility(R.id.banner, View.VISIBLE);
            BannerLayout banner = holdView.getView(R.id.banner);
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) banner.getLayoutParams();
            lp.height = (int) (photoHeigth - DensityUtils.dp2px(45));

            final List<AdBean.Map_list> adList = userBean.adList;
            if (!adList.isEmpty()) {
                List<String> advs = new ArrayList<String>();
                for (int i = 0; i < adList.size(); i++) {
                    advs.add(adList.get(i).advertURL);
                }
                banner.setViewUrls(advs);
                // 添加点击事件的监听
                banner.setOnBannerItemClickListener(new BannerLayout.OnBannerItemClickListener() {
                    @Override
                    public void onItemClick(int position) {
                        BusinessUtils.AdRedirect(adList.get(position));
                    }
                });
            }
        } else {
            holdView.setVisibility(R.id.image_container, View.VISIBLE);
            holdView.setVisibility(R.id.star_hot_container, View.GONE);
            holdView.setVisibility(R.id.banner, View.GONE);

            if (userBean.verifyType <= 0) {//非加V用户
                holdView.setVisibility(R.id.ll_distance, View.VISIBLE);
                setDistance(holdView, userBean);
            } else {//加V
                holdView.setVisibility(R.id.rl_vertify, View.VISIBLE);
                setVerify(holdView, userBean);
            }

            if (userBean.level > 0) {// vip用户
                holdView.setVisibility(R.id.img_vip, View.VISIBLE);
                if (userBean.level < TheLConstants.VIP_LEVEL_RES.length) {
                    holdView.setImageUrl(R.id.img_vip, TheLConstants.RES_PIC_URL + TheLConstants.VIP_LEVEL_RES[userBean.level]);
                }
            }
            TextView nickname = holdView.convertView.findViewById(R.id.txt_nickname);
            nickname.getPaint().setFakeBoldText(true);
            holdView.setText(R.id.txt_nickname, userBean.nickname);

            final SimpleDraweeView imgThumb = holdView.getView(R.id.img_thumb);
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) imgThumb.getLayoutParams();
            lp.height = (int) photoHeigth;

            processOnLiveArea(holdView, userBean);

            if (isNearby && Utils.isMyself(userBean.userId + "")) {
                holdView.setVisibility(R.id.img_setting, View.VISIBLE);
                holdView.setOnClickListener(R.id.img_setting, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new ActionSheetDialog(mContext).builder()
                                .setCancelable(true)
                                .setCanceledOnTouchOutside(true)
                                .addSheetItem(mContext.getString(R.string.select_fengmian_bg), ActionSheetDialog.SheetItemColor.BLACK, new ActionSheetDialog.OnSheetItemClickListener() {
                                    @Override
                                    public void onClick(int which) {
                                        Intent intent = new Intent(mContext, CoverPreviewActivity.class);
                                        intent.putExtra(TheLConstants.BUNDLE_KEY_COVER, userBean.picUrl);
                                        nearbyPeopleFragment.startActivityForResult(intent, TheLConstants.REQUEST_CODE_COVER);
                                    }
                                })
                                .addSheetItem(mContext.getString(R.string.edit_profile), ActionSheetDialog.SheetItemColor.BLACK, new ActionSheetDialog.OnSheetItemClickListener() {
                                    @Override
                                    public void onClick(int which) {
                                        Intent intent = new Intent(mContext, UpdateUserInfoActivity.class);
                                        mContext.startActivity(intent);
                                    }
                                })
                                .addSheetItem(userBean.hiding == 1 ? mContext.getString(R.string.disable_invisibility) : mContext.getString(R.string.enable_invisibility), ActionSheetDialog.SheetItemColor.BLACK, new ActionSheetDialog.OnSheetItemClickListener() {
                                    @Override
                                    public void onClick(int which) {
                                        if (UserUtils.getUserVipLevel() > 0) {
                                            if (userBean.hiding == 1) userBean.hiding = 0;
                                            else userBean.hiding = 1;
                                            notifyDataSetChanged();

                                            Map<String, String> data = new HashMap<>();
                                            data.put("hiding", String.valueOf(userBean.hiding));
                                            data.put("vipHiding", "-1");
                                            data.put("incognito", "-1");
                                            data.put("followRemind", "-1");
                                            data.put("liveHiding", "-1");
                                            data.put("msgHiding", "-1");
                                            DefaultRequestService.createUserRequestService().vipConfig(MD5Utils.generateSignatureForMap(data))
                                                    .onBackpressureDrop()
                                                    .subscribeOn(Schedulers.io())
                                                    .observeOn(AndroidSchedulers.mainThread())
                                                    .subscribe();
                                        } else {
                                            Intent intent = new Intent(mContext, VipConfigActivity.class);
                                            intent.putExtra("showType", VipConfigActivity.SHOW_MSG_HIDING);
                                            intent.putExtra("fromPage", "nearby");
                                            intent.putExtra("fromPageId", pageId);
                                            mContext.startActivity(intent);
                                            traceNearbyUserLog("ghost");
                                        }
                                    }
                                }).show();
                    }
                });
            }

            /***如果处于隐身状态，则显示隐身图标，同时背景色高斯模糊，隐藏直播状态***/
            if (userBean.hiding == 1) {
                holdView.setVisibility(R.id.lin_hide, View.VISIBLE);
                String url = "";
                if (!TextUtils.isEmpty(userBean.picUrl)) {
                    url = userBean.picUrl;
                } else if (!TextUtils.isEmpty(userBean.avatar)) {
                    url = userBean.picUrl;
                }
                if (!TextUtils.isEmpty(url)) {//如果有url，设置背景色，没有，设置默认热拉色
                    imgThumb.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(userBean.picUrl, photoWidth, photoHeigth))).setPostprocessor(new IterativeBoxBlurPostProcessor(150, 10)).build()).setAutoPlayAnimations(true).build());
                } else {
                    imgThumb.setImageResource(R.color.tab_normal);
                }
            } else {
                imgThumb.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(userBean.picUrl, photoWidth, photoHeigth))).build()).setAutoPlayAnimations(true).build());
            }
            imgThumb.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadius(radius).setRoundingMethod(RoundingParams.RoundingMethod.OVERLAY_COLOR).setOverlayColor(ContextCompat.getColor(TheLApp.getContext(), R.color.white)));

        }
    }

    private void processOnLiveArea(BaseViewHolder holdView, final NearUserBean userBean) {
        if (userBean.live > 0) {
            holdView.setVisibility(R.id.lin_live, View.VISIBLE);
            holdView.setVisibility(R.id.ll_live_intro, View.VISIBLE);
            holdView.setText(R.id.txt_live_intro, userBean.liveText);
        }
        if (NearUserBean.LIVE_TYPE_VOICE.equals(userBean.liveType)) {//声音直播
            holdView.setImageResource(R.id.img_on_live, R.mipmap.icon_voice_live);
        } else {
            holdView.setImageResource(R.id.img_on_live, R.mipmap.icon_camera_live);
        }
        holdView.setBackgroundRes(R.id.rel_info, userBean.live == 0 ? R.color.transparent : R.drawable.bg_nearby_live_bg);

        holdView.setText(R.id.txt_on_live_looker_num, userBean.liveLooker + "");
        holdView.setOnClickListener(R.id.lin_live, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (BusinessUtils.canIntoLiveRoom(TheLApp.getContext(), userBean.userId + "")) {
                    MobclickAgent.onEvent(TheLApp.getContext(), "enter_live_room_from_nearby");
                    Intent intent = new Intent(TheLApp.getContext(), LiveWatchActivity.class);
                    intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, userBean.userId + "");
                    intent.putExtra(LiveWatchActivity.FROM_PAGE, LiveWatchActivity.FROM_PAGE_MOMENT);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    TheLApp.getContext().startActivity(intent);
                    GrowingIOUtil.postWatchLiveEntry(GrowingIoConstant.G_LiveEntry_Other);

                }
            }
        });

    }


    /***距离***/
    private void setDistance(BaseViewHolder holdView, NearUserBean userBean) {
        TextView txt_distance = holdView.convertView.findViewById(R.id.txt_distance);
        txt_distance.getPaint().setFakeBoldText(true);
        if (hideNearbyDistance) {// 五公里内的都显示为“附近”
            try {
                if (userBean.distance != null && (userBean.distance.contains(" Km") || userBean.distance.contains(" km"))) {
                    float dis = -1f;
                    if (userBean.distance.contains(" Km")) {
                        dis = Float.valueOf(userBean.distance.substring(0, userBean.distance.indexOf(" Km")));
                    } else if (userBean.distance.contains(" km")) {
                        dis = Float.valueOf(userBean.distance.substring(0, userBean.distance.indexOf(" km")));
                    }
                    if (dis <= 5) {
                        if (dis == -1f) {
                            holdView.setText(R.id.txt_distance, userBean.distance);
                        } else {
                            holdView.setText(R.id.txt_distance, TheLApp.getContext().getString(R.string.nearby_activity_nearby));
                        }
                    } else {
                        holdView.setText(R.id.txt_distance, userBean.distance);
                    }
                } else {
                    holdView.setText(R.id.txt_distance, TheLApp.getContext().getString(R.string.nearby_activity_nearby));
                }
            } catch (Exception e) {
                holdView.setText(R.id.txt_distance, userBean.distance);
            }
        } else {
            holdView.setText(R.id.txt_distance, userBean.distance);
        }
        final int position = holdView.getLayoutPosition() - getHeaderLayoutCount();
        if (position == 0 && isNearby && Utils.isMyself(userBean.userId + "")) {
            holdView.setText(R.id.txt_distance, R.string.universe_center);
        }
    }


    /***显示简介***/
    private void setVerify(BaseViewHolder holdView, NearUserBean userBean) {
        holdView.setText(R.id.txt_verify, userBean.verifyIntro);
        if (userBean.verifyType == 1) {// 个人加V
            //  holdView.setBackgroundRes(R.id.txt_verify, R.drawable.bg_textview_identification_yellow);
            holdView.setImageResource(R.id.iv_vertify, R.mipmap.icn_verify_person);
        } else if (userBean.verifyType == 2) {// 企业加V
            holdView.setImageResource(R.id.iv_vertify, R.mipmap.icn_verify_enterprise);
        } else {//默认个人加V
            holdView.setImageResource(R.id.iv_vertify, R.mipmap.icn_verify_person);

        }
    }
    public void traceNearbyUserLog(String activity) {
        try {

            LogInfoBean logInfoBean = new LogInfoBean();
            logInfoBean.page = "around.list";
            logInfoBean.page_id = pageId;
            logInfoBean.activity = activity;

            MatchLogUtils.getInstance().addLog(logInfoBean);

        } catch (Exception e) {

        }
    }
}
