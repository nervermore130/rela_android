package com.thel.modules.others.beans;

import com.thel.base.BaseDataBean;
import com.thel.modules.others.parsers.BaseParser;
import com.thel.utils.JsonUtils;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * vip功能配置页面广告解析
 *
 */
public class VipConfigAdParser extends BaseParser {

    @Override
    public BaseDataBean parse(String message) throws Exception {

        BannerAdListBean listBean = new BannerAdListBean();

        super.parse(message, listBean);
        if (listBean.result.equals("0")) {// 获取数据失败
            return listBean; // 不再解析具体数据了（也没有数据）
        }

        JSONObject object = new JSONObject(message);
        JSONArray jsonArray = JsonUtils.getJSONArray(JsonUtils.getJSONObject(object, "data"), "map_list");
        int arrayLength = jsonArray.length();
        for (int i = 0; i < arrayLength; i++) {
            BannerAdBean adBean = new BannerAdBean();
            JSONObject tempobj = jsonArray.getJSONObject(i);
            adBean.fromJson(tempobj);
            if (BannerAdBean.ADV_LOCATION_VIP.equals(adBean.advertLocation)) {
                listBean.adlist.add(adBean);
            }
        }
        return listBean;
    }

}
