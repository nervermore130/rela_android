package com.thel.modules.main.messages.utils;

import com.thel.constants.TheLConstants;
import com.thel.db.table.message.UserInfoTable;
import com.thel.manager.ChatServiceManager;
import com.thel.modules.main.messages.bean.MsgBean;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;

import java.math.BigDecimal;

/**
 * Created by kevin on 2017/9/29.
 */
public class MsgUtils {

    private final static String TAG = MsgUtils.class.getSimpleName();

    public static MsgBean createMsgBean(String msgType, String msgText, int isSystem, String toUserId, String toUserNickName, String toUserAvatar) {

        UserInfoTable userInfoTable = ChatServiceManager.getInstance().getUserInfo();

        if (userInfoTable == null) {
            return null;
        }

        MsgBean msgBean = new MsgBean();
        msgBean.msgStatus = TheLConstants.MsgSendingStatusConstants.MSG_SENDING;
        msgBean.packetId = MsgUtils.getMsgId();
        msgBean.msgType = msgType;
        msgBean.msgText = msgText;
        msgBean.msgTime = System.currentTimeMillis();
        msgBean.msgDirection = "0";
        msgBean.fromUserId = userInfoTable.id;
        msgBean.fromNickname = userInfoTable.nickName;
        msgBean.fromAvatar = userInfoTable.avatar;
        msgBean.fromMessageUser = userInfoTable.messageUser;
        msgBean.toUserId = toUserId;
        msgBean.toUserNickname = toUserNickName;
        msgBean.toAvatar = toUserAvatar;
        msgBean.toMessageUser = TheLConstants.MSG_ACCOUNT_USER_ID_STR + toUserId;
        // 发出的消息一律为读过
        msgBean.hadRead = 1;
        msgBean.isSystem = isSystem;
        return msgBean;
    }

    private static double sequence;

    private static double lastTimestamp;

    public static String getMsgId() {

        long userId = Long.valueOf(ShareFileUtils.getString(ShareFileUtils.ID, "123456789"));

        double currentTimestamp = Math.floor((System.currentTimeMillis() - 1500000000000L) / 234.375);

        if (currentTimestamp != lastTimestamp) {
            sequence = 0;
            lastTimestamp = currentTimestamp;
        } else if (++sequence >= 16) {
            return "-1";
        }

        double msgId = ((userId & 0xfffff) * 0x100000000L) + (currentTimestamp * 0x10) + sequence;

        BigDecimal bigDecimal = new BigDecimal(msgId);

        return String.valueOf(bigDecimal);
    }


}
