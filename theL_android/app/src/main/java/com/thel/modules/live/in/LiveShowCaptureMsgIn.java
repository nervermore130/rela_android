package com.thel.modules.live.in;

import com.thel.modules.live.bean.AgoraBean;
import com.thel.modules.live.surface.show.LiveShowObserver;

/**
 * Created by waiarl on 2017/11/5.
 */

public interface LiveShowCaptureMsgIn extends LiveShowCapturePkMsgIn, LiveShowMsgMultiIn {
    void bindObserver(LiveShowObserver observer);

    /**
     * 发送停止推流消息
     */
    void sendCloseMsg();

    /**
     * 试着创建连接
     */
    void tryCreateClient();

    /**
     * ping
     */
    void autoPingServer();

    /**
     * 屏蔽用户
     *
     * @param userId
     */
    void banHer(String userId);

    void onDestroy();

    /**
     * 获取直播关闭信息
     */
    void exucuteColseRunable();

    /**
     * 开启排麦
     */
    void openMicSort();

    /**
     * 关闭排麦
     */
    void closeMicSort();

    void agreeOnSeat(String userId, AgoraBean agoraBean);


    /**
     * 弹幕
     *
     * @param userLevel 用户等级
     * @param content   弹幕内容
     * @param barrageId 弹幕id
     */
    void sendDanmu(String userLevel, String content, String barrageId);

    /**
     * 发送消息
     *
     * @param msg
     */
    void sendMsg(String msg);


}
