package com.thel.modules.live.view;

import android.app.Dialog;
import android.content.Context;
import androidx.annotation.NonNull;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.constants.TheLConstants;
import com.thel.imp.follow.FollowStatusChangedImpl;
import com.thel.imp.follow.FollowStatusChangedListener;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.live.bean.LiveRoomBean;
import com.thel.utils.AppInit;

/**
 * Created by waiarl on 2017/10/28.
 */

public class LiveShowCloseDialog extends Dialog {
    private final Context context;
    private TextView txt_follow;
    private ImageView img_close;
    private ImageView avatar;
    private TextView txt_nickname;
    private LiveRoomBean liveRoomBean;
    private TextView txt_more_live;

    public LiveShowCloseDialog(@NonNull Context context) {
        this(context, R.style.CustomDialog);
    }

    public LiveShowCloseDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        this.context = context;
        init();
    }

    private LiveShowCloseDialog init() {

        View view = LayoutInflater.from(context).inflate(R.layout.live_closed_dialog, null);
        setContentView(view);
        txt_follow = view.findViewById(R.id.txt_follow);
        img_close = view.findViewById(R.id.img_close);
        avatar = view.findViewById(R.id.avatar);
        txt_nickname = view.findViewById(R.id.txt_nickname);
        txt_more_live = view.findViewById(R.id.txt_more_live);
        return this;
    }

    public LiveShowCloseDialog initDialog(final LiveRoomBean liveRoomBean, View.OnClickListener closeListener, View.OnClickListener moreLiveListener) {
        this.liveRoomBean = liveRoomBean;
        ImageLoaderManager.imageLoaderDefaultCircle(avatar, R.mipmap.icon_user, liveRoomBean.user.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE);
        txt_nickname.setText(liveRoomBean.user.nickName);
        if (liveRoomBean.user.isFollow == 0) {
            txt_follow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    follow(liveRoomBean);
                }
            });
        } else {
            txt_follow.setText(TheLApp.getContext().getString(R.string.userinfo_activity_followed));
        }
        img_close.setOnClickListener(closeListener);
        txt_more_live.setOnClickListener(moreLiveListener);
        show();
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.gravity = Gravity.CENTER;
        lp.width = AppInit.displayMetrics.widthPixels; // 设置宽度
        lp.height = AppInit.displayMetrics.heightPixels;
        lp.dimAmount = 0;
        getWindow().setAttributes(lp);
        return this;
    }

    /**
     * 关注
     *
     * @param liveRoomBean //todo
     */
    private void follow(LiveRoomBean liveRoomBean) {
        FollowStatusChangedImpl.followUserWithNoDialog(liveRoomBean.user.id + "", FollowStatusChangedListener.ACTION_TYPE_FOLLOW, liveRoomBean.user.nickName, liveRoomBean.user.avatar);
    }

    public void followStatusChanged(int followStatus, String userId, String nickName, String avatar) {
        if ((liveRoomBean.user.id + "").equals(userId)) {
            liveRoomBean.user.isFollow = followStatus;
            if (followStatus == FollowStatusChangedListener.FOLLOW_STATUS_FOLLOW || followStatus == FollowStatusChangedListener.FOLLOW_STATUS_FRIEND) {
                txt_follow.setText(TheLApp.getContext().getString(R.string.userinfo_activity_followed));
                txt_follow.setOnClickListener(null);
            }
        }
    }
}
