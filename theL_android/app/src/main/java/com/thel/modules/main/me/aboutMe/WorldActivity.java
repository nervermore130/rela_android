package com.thel.modules.main.me.aboutMe;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.maps2d.AMap;
import com.amap.api.maps2d.CameraUpdate;
import com.amap.api.maps2d.CameraUpdateFactory;
import com.amap.api.maps2d.MapView;
import com.amap.api.maps2d.model.BitmapDescriptorFactory;
import com.amap.api.maps2d.model.LatLng;
import com.amap.api.maps2d.model.Marker;
import com.amap.api.maps2d.model.MarkerOptions;
import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.network.RequestConstants;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SharedPrefUtils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

/**
 * 世界漫游
 * Created by lingwei on 2017/9/23.
 */

public class WorldActivity extends BaseActivity {
    private Double double_lng;
    private Double double_lat;
    private MapView mMapView;
    private AMap aMap;
    private Marker marker;
    private LinearLayout lin_back;
    private RelativeLayout main;
    private Button locate;
    private AMapLocationClient aMapLocationClient;
    private AMapLocationClientOption mLocationOption;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.world_activity);
        findViewById();
        String str_lng = ShareFileUtils.getString(RequestConstants.I_LNG, ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0"));
        String str_lat = ShareFileUtils.getString(RequestConstants.I_LAT, ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0"));
        try {

            double_lng = Double.parseDouble(str_lng);
            double_lat = Double.parseDouble(str_lat);
        }catch (Exception e){

        }

        // 定义Maker坐标点
        LatLng point = new LatLng(double_lat, double_lng);
        if (mMapView != null) {
            mMapView.onCreate(savedInstanceState);
            if (aMap == null) {
                aMap = mMapView.getMap();
                aMap.getUiSettings().setZoomControlsEnabled(false);
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(point, 12);
                aMap.moveCamera(cameraUpdate);
            }
        }
        // 构建Marker图标
        // 构建MarkerOption，用于在地图上添加Marker
        MarkerOptions option = new MarkerOptions().position(point).icon(BitmapDescriptorFactory.fromView(getLayoutInflater().inflate(R.layout.map_marker, null)));
        // 在地图上添加Marker，并显示
        marker = aMap.addMarker(option);
        setListener();

    }

    private void setListener() {
        aMap.setOnMapClickListener(new AMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng point) {
                marker.setPosition(point);
            }
        });
        lin_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                WorldActivity.this.finish();
            }
        });
        locate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // 缓存世界定位的经纬度
                ShareFileUtils.setString(SharedPrefUtils.WORLD_LAT, marker.getPosition().latitude + "");
                ShareFileUtils.setString(SharedPrefUtils.WORLD_LNG, marker.getPosition().longitude + "");

                Intent intent = new Intent(WorldActivity.this, DetailedFiltersResultActivity.class);
                intent.putExtra("fromPage", WorldActivity.class.getName());
                startActivity(intent);
            }
        });
    }

    private void findViewById() {
        lin_back = findViewById(R.id.lin_back);
        main = findViewById(R.id.main);
        mMapView = findViewById(R.id.mapview);
        locate = main.findViewById(R.id.locate);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMapView.onResume();
        MobclickAgent.onPageStart(this.getClass().getName());
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mMapView.onPause();
        MobclickAgent.onPageEnd(this.getClass().getName());
        MobclickAgent.onPause(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //在activity执行onSaveInstanceState时执行mMapView.onSaveInstanceState (outState)，保存地图当前的状态
        mMapView.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

}
