package com.thel.modules.preview_image;

import android.content.Context;
import android.net.Uri;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.drawable.ScalingUtils;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.generic.GenericDraweeHierarchyBuilder;
import com.facebook.drawee.interfaces.DraweeController;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.ImgShareBean;
import com.thel.constants.TheLConstants;
import com.thel.ui.widget.zoom.CircleProgressBarDrawablebg;
import com.thel.ui.widget.zoom.ZoomableDraweeView;
import com.thel.utils.ImageUtils;

import java.util.ArrayList;
import java.util.List;

public class PhotoNotSharePagerAdapter extends PagerAdapter {

    private final String mRela_id;
    private final boolean mIsWaterMark;
    private final ImgShareBean mImageShareBean;
    private List<String> picUrls = new ArrayList<String>();

    private LayoutInflater mInflater;

    private Context mContext;

    public PhotoNotSharePagerAdapter(Context ctx, List<String> pics, String rela_id, boolean isWaterMark, ImgShareBean imageShareBean) {
        this.mRela_id = rela_id;
        this.mIsWaterMark = isWaterMark;
        this.mImageShareBean = imageShareBean;
        mInflater = (LayoutInflater) TheLApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContext = ctx;
        freshAdapter(pics);
    }

    public void freshAdapter(List<String> pics) {
        this.picUrls = pics;
    }

    @Override
    public int getCount() {
        return picUrls.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View convertView;
        convertView = mInflater.inflate(R.layout.userinfo_photo_listitem, container, false);
        ZoomableDraweeView imgProduct = convertView.findViewById(R.id.img_thumb);
        String imgUrl;
        if (picUrls.get(position).contains(TheLConstants.F_TAKE_PHOTO_ROOTPATH)) {
            if (picUrls.get(position).contains(TheLConstants.FILE_PIC_URL))
                imgUrl = picUrls.get(position);
            else
                imgUrl = TheLConstants.FILE_PIC_URL + picUrls.get(position);
        } else {
            imgUrl = picUrls.get(position);
        }

        DraweeController ctrl = Fresco.newDraweeControllerBuilder().setUri(Uri.parse(ImageUtils.buildNetPictureUrl(imgUrl))).setTapToRetryEnabled(true).build();
        CircleProgressBarDrawablebg progressBarDrawable = new CircleProgressBarDrawablebg();
        progressBarDrawable.setColor(TheLApp.getContext().getResources().getColor(R.color.white));


        GenericDraweeHierarchy hierarchy = new GenericDraweeHierarchyBuilder(TheLApp.getContext().getResources()).setActualImageScaleType(ScalingUtils.ScaleType.FIT_CENTER).setProgressBarImage(progressBarDrawable).build();
        imgProduct.setController(ctrl);
        imgProduct.setHierarchy(hierarchy);

        container.addView(convertView, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);

        imgProduct.enableDoubleTapZoomNotShare(mContext, imgUrl, mRela_id, mIsWaterMark, mImageShareBean);

        return convertView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

}
