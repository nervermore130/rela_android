package com.thel.modules.main.home.search;

import android.view.View;

import com.thel.R;
import com.thel.constants.TheLConstants;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;
import com.thel.utils.ShareFileUtils;

import java.util.List;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by chad
 * Time 18/8/31
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */
public class SearchRecommendAdapter extends BaseRecyclerViewAdapter<SearchRecommendBean.UserList> {

    final OnItemChildClickListener mChildListener = new OnItemChildClickListener();

    public SearchRecommendAdapter(List<SearchRecommendBean.UserList> data) {
        super(R.layout.search_recommend_listitem, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, SearchRecommendBean.UserList bean) {
        if (bean.verifyType <= 0) {// 非加V用户，显示两行，第一行为昵称和简介，第二行为在线状态、距离、感情状态
            helper.setVisibility(R.id.line2, View.VISIBLE);
            helper.setVisibility(R.id.img_indentify, View.GONE);
            helper.setText(R.id.txt_desc, bean.recommendReason);

        } else {// 加V用户，显示一行，显示昵称、认证图标、认证信息
            helper.setVisibility(R.id.line2, View.VISIBLE);
            helper.setVisibility(R.id.img_indentify, View.VISIBLE);
            helper.setText(R.id.txt_desc, bean.verifyIntro);

            if (bean.verifyType == 1) {// 个人加V
                helper.setImageResource(R.id.img_indentify, R.mipmap.icn_verify_person);
            } else {// 企业加V
                helper.setImageResource(R.id.img_indentify, R.mipmap.icn_verify_enterprise);

            }
        }
        if (bean.hiding == 0) {
            helper.setText(R.id.txt_distance, bean.distance);

        } else {
            helper.setText(R.id.txt_distance, "");

        }
        if (bean.level > 0) {
            helper.setVisibility(R.id.img_vip, VISIBLE);
            switch (bean.level) {
                case 1:
                    helper.setImageResource(R.id.img_vip, R.mipmap.icn_vip_1);
                    break;
                case 2:
                    helper.setImageResource(R.id.img_vip, R.mipmap.icn_vip_2);

                    break;
                case 3:
                    helper.setImageResource(R.id.img_vip, R.mipmap.icn_vip_3);

                    break;
                case 4:
                    helper.setImageResource(R.id.img_vip, R.mipmap.icn_vip_4);

                    break;
            }
        } else {
            helper.setVisibility(R.id.img_vip, GONE);

        }
        // 角色设定 0=unknow,1=t,2=p,3=h,5=bi
        if (bean.roleName.equals("0")) {
            helper.setImageResource(R.id.img_role, R.mipmap.icn_role_unknow);
        } else if (bean.roleName.equals("1")) {
            helper.setImageResource(R.id.img_role, R.mipmap.icn_role_t);
        } else if (bean.roleName.equals("2")) {
            helper.setImageResource(R.id.img_role, R.mipmap.icn_role_p);
        } else if (bean.roleName.equals("3")) {
            helper.setImageResource(R.id.img_role, R.mipmap.icn_role_h);
        } else if (bean.roleName.equals("5")) {
            helper.setImageResource(R.id.img_role, R.mipmap.icn_role_bi);
        } else if (bean.roleName.equals("6")) {
            helper.setImageResource(R.id.img_role, R.mipmap.icn_role_s);
        } else {
            helper.setImageResource(R.id.img_role, R.mipmap.icn_role_unknow);
        }

        // 头像
        helper.setImageUrl(R.id.img_thumb, bean.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE);
        // 用户名
        helper.setText(R.id.txt_name, bean.nickName.trim());

        if (!ShareFileUtils.getString(ShareFileUtils.ID, "").equals(bean.userId + "")) {
            helper.setVisibility(R.id.txt_follow, View.VISIBLE);
            // 是否关注此用户
            if (!bean.isFollow) {//未关注
                helper.setText(R.id.txt_follow, mContext.getString(R.string.follow));
                helper.setBackgroundRes(R.id.txt_follow, R.drawable.bg_unfollow_shape_green_normal);
                helper.setTextColor(R.id.txt_follow, mContext.getResources().getColor(R.color.White));

            } else {// 已关注
                helper.setText(R.id.txt_follow, mContext.getString(R.string.userinfo_activity_followed));
                helper.setBackgroundRes(R.id.txt_follow, R.drawable.bg_follow_shape_green_normal);
                helper.setTextColor(R.id.txt_follow, mContext.getResources().getColor(R.color.text_color_green));
            }
        } else {
            helper.setVisibility(R.id.txt_follow, View.INVISIBLE);
        }
        helper.setOnItemChildClickListener(R.id.txt_follow, mChildListener);
        helper.setTag(R.id.txt_follow, helper.getLayoutPosition());
    }
}
