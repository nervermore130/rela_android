package com.thel.modules.main.home.search.tag;

import android.content.Intent;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.SimpleTagBean;
import com.thel.bean.TopicBean;
import com.thel.bean.TopicListBean;
import com.thel.bean.TrendingTagsBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.modules.main.home.search.BaseSearchFragment;
import com.thel.modules.main.home.tag.TagDetailActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.utils.LocationUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class SearchTagFragment extends BaseSearchFragment {

    @BindView(R.id.list_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.recyclerview_tags)
    RecyclerView recyclerview_tags;

    @BindView(R.id.rel_trending_tags)
    RelativeLayout rel_trending_tags;

    @BindView(R.id.lin_no_match)
    LinearLayout lin_no_match;

    private final int REFRESH_TYPE_ALL = 1;
    private final int REFRESH_TYPE_NEXT_PAGE = 2;

    private List<TopicBean> list = new ArrayList<>();

    private SearchTagAdapter mAdapter;

    private TrendingTagsRecyclerViewAdapter trendingAdapter;

    private ArrayList<SimpleTagBean> listPlusTrendingTags = new ArrayList<>();

    public static SearchTagFragment newInstance(String type) {
        SearchTagFragment fragment = new SearchTagFragment();
        Bundle bundle = new Bundle();
        bundle.putString(SEARCH_TYPE, type);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int setContentView() {
        return R.layout.fragment_search_tag;
    }

    @Override
    protected void initView() {

        this.mEmptyView = lin_no_match;

        initSearchTagRecycler();

        initTrandingTagRecycler();
    }

    @Override
    protected void initData() {
        //获取热门标签

        RequestBusiness.getInstance().getTrendingTags().observeOn(AndroidSchedulers.mainThread()).onBackpressureDrop().subscribeOn(Schedulers.io()).subscribe(new InterceptorSubscribe<TrendingTagsBean>() {
            @Override
            public void onNext(TrendingTagsBean data) {
                super.onNext(data);

                if (data == null || data.data == null || data.data.tagsList == null)
                    return;
                listPlusTrendingTags.clear();
                listPlusTrendingTags.addAll(data.data.tagsList);
                setTrendingView();
            }
        });
    }

    @Override
    protected void processBusiness(final int page, final int type) {
        super.processBusiness(page, type);

//        if (LocationUtils.isHasLocationPermission()) {
//            searchTopics(page, logType);
//        } else {
        final AMapLocationClient mLocationClient = new AMapLocationClient(TheLApp.getContext());
        mLocationClient.setLocationListener(new AMapLocationListener() {
            @Override
            public void onLocationChanged(AMapLocation aMapLocation) {

                if (aMapLocation != null && aMapLocation.getErrorCode() == 0) {
                    LocationUtils.saveLocation(aMapLocation.getLatitude(), aMapLocation.getLongitude(), aMapLocation.getCity());
                } else {
                    LocationUtils.saveLocation(0.0, 0.0, "");
                }

                if (!LocationUtils.isHasLocationPermission()) {
                    LocationUtils.saveLocation(0.0, 0.0, "");

                }

                searchTopics(page, type);

                mLocationClient.stopLocation();
                mLocationClient.onDestroy();
            }
        });
        final AMapLocationClientOption mLocationOption = new AMapLocationClientOption();
        mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Battery_Saving);
        mLocationClient.setLocationOption(mLocationOption);
        mLocationClient.startLocation();
//        }

    }

    private void searchTopics(int page, int type) {
        showLoading();
        RequestBusiness.getInstance().searchTopics(mKey, curPage).onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<TopicListBean>() {
            @Override
            public void onNext(TopicListBean data) {
                super.onNext(data);
                closeLoading();
                if (getActivity() != null) {
                    refreshList(data);
                }
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                closeLoading();
                if (getActivity() != null) {
                    mRecyclerView.post(new Runnable() {
                        @Override
                        public void run() {
                            mAdapter.loadMoreFailed((ViewGroup) mRecyclerView.getParent());
                        }
                    });
                }
            }
        });
    }

    @Override
    public void refresh() {
        super.refresh();
        if (TextUtils.isEmpty(mKey)) {
            setTrendingView();
        } else {
            processBusiness(1, REFRESH_TYPE_ALL);
        }
    }

    private void initTrandingTagRecycler() {
        //设置布局管理器
        LinearLayoutManager trendingManager = new LinearLayoutManager(TheLApp.getContext());
        trendingManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerview_tags.setLayoutManager(trendingManager);
        //如果可以确定每个item的高度是固定的，设置这个选项可以提高性能
        recyclerview_tags.setHasFixedSize(true);
        recyclerview_tags.setItemAnimator(null);
        trendingAdapter = new TrendingTagsRecyclerViewAdapter(listPlusTrendingTags);
        recyclerview_tags.setAdapter(trendingAdapter);

        trendingAdapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
//                goToTagDetail(trendingAdapter.getItem(position).tagsName);
                FlutterRouterConfig.Companion.gotoTagDetails((int) trendingAdapter.getItem(position).tagId, trendingAdapter.getItem(position).tagsName);
            }
        });
    }

    private void initSearchTagRecycler() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(TheLApp.getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        //如果可以确定每个item的高度是固定的，设置这个选项可以提高性能
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(null);
        mAdapter = new SearchTagAdapter(list);
        mRecyclerView.setAdapter(mAdapter);

        mAdapter.setOnLoadMoreListener(new BaseRecyclerViewAdapter.RequestLoadMoreListener() {//加载更多
            @Override
            public void onLoadMoreRequested() {
                mRecyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        if (currentCountForOnce > 0) {
                            // 列表滑动到底部加载下一组数据
                            processBusiness(curPage, REFRESH_TYPE_NEXT_PAGE);
                        } else {//说明没有更多数据
                            mAdapter.openLoadMore(0, false);
                            if (list.size() > 0) {
                                mAdapter.closeLoadMore(getActivity(), mRecyclerView);
                            }
                        }
                    }
                });
            }
        });
        mAdapter.setReloadMoreListener(new BaseRecyclerViewAdapter.ReloadMoreListener() {//加载更多失败后重新加载
            @Override
            public void reloadMore() {
                refreshType = REFRESH_TYPE_NEXT_PAGE;
                mAdapter.removeAllFooterView();
                mAdapter.openLoadMore(true);
                processBusiness(curPage, REFRESH_TYPE_NEXT_PAGE);
            }


        });
        mAdapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
//                goToTagDetail(mAdapter.getItem(position).topicName);
                try {
                    FlutterRouterConfig.Companion.gotoTagDetails(Integer.parseInt(mAdapter.getData().get(position).topicId), mAdapter.getData().get(position).topicName);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }

    /**
     * 设置推荐热门用户显示
     */
    private void setTrendingView() {
        if (listPlusTrendingTags.size() > 0) {
            trendingAdapter.setNewData(listPlusTrendingTags);
            rel_trending_tags.setVisibility(View.VISIBLE);
            lin_no_match.setVisibility(View.GONE);
        } else {
            if (lin_no_match != null) {
                lin_no_match.setVisibility(View.VISIBLE);
            }
        }
        list.clear();
        if (mAdapter != null) {
            mAdapter.removeAllFooterView();
            mAdapter.setNewData(list);
            mAdapter.openLoadMore(list.size(), false);
        }

    }

    /**
     * 跳转到标签详情
     *
     * @param topicName
     */
    private void goToTagDetail(String topicName) {
//        Intent intent = new Intent(getActivity(), TagDetailActivity.class);
//        intent.putExtra(TheLConstants.BUNDLE_KEY_TOPIC_NAME, topicName);
//        startActivity(intent);
    }

    private void refreshList(TopicListBean searchListBean) {
        rel_trending_tags.setVisibility(View.GONE);
        if (refreshType == REFRESH_TYPE_ALL) {
            list.clear();
        }
        if (searchListBean != null) {
            if (searchListBean.suggestList != null) {
                currentCountForOnce = searchListBean.suggestList.size();
                list.addAll(searchListBean.suggestList);
            }
        }
        if (refreshType == REFRESH_TYPE_ALL) {
            mAdapter.removeAllFooterView();
            mAdapter.setNewData(list);
            curPage = 2;
            if (list.size() > 0) {
                mAdapter.openLoadMore(list.size(), true);
                lin_no_match.setVisibility(View.GONE);
            } else {
                mAdapter.openLoadMore(list.size(), false);
                lin_no_match.setVisibility(View.VISIBLE);
            }
        } else {
            curPage++;
            mAdapter.notifyDataChangedAfterLoadMore(true, list.size());
        }
        if (refreshType == REFRESH_TYPE_ALL) {
            if (mRecyclerView.getChildCount() > 0) {// 回到顶部
                mRecyclerView.scrollToPosition(0);
            }
        }
    }

}
