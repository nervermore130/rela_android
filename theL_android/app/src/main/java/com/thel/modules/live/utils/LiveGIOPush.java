package com.thel.modules.live.utils;

import com.growingio.android.sdk.collection.GrowingIO;

public class LiveGIOPush {

    private static final String TAG = "LiveGIOPush";

    /**
     * 观众连麦次数
     */
    private int audienceLinkMicCount = 0;

    /**
     * 主播连麦次数
     */
    private int broadcastLinkMicCount = 0;

    /**
     * 主播pk次数
     */
    private int broadcastPKCount = 0;

    /**
     * 观众上麦次数
     */
    private int audienceOnSeatCount = 0;

    /**
     * 观看连麦次数
     */
    private int watchLinkMicCount = 0;

    /**
     * 观看PK次数
     */
    private int watchPKCount = 0;

    private static final LiveGIOPush ourInstance = new LiveGIOPush();

    public static LiveGIOPush getInstance() {
        return ourInstance;
    }

    private LiveGIOPush() {

    }

    public void setAudienceLinkMicCount() {
        audienceLinkMicCount++;
    }

    public void setBroadcastLinkMicCount() {
        broadcastLinkMicCount++;
    }

    public void setBroadcastPKCount() {
        broadcastPKCount++;
    }

    public void setAudienceOnSeatCount() {
        audienceOnSeatCount++;
    }

    public void setWatchLinkMicCount() {
        watchLinkMicCount++;
    }

    public void setWatchPKCount() {
        watchPKCount++;
    }


    public void onDestroy() {

        final GrowingIO instance = GrowingIO.getInstance();

        if (audienceLinkMicCount > 0) {//
            instance.track("audienceLinkMic", audienceLinkMicCount);
        }

        if (broadcastLinkMicCount > 0) {
            instance.track("hostLinkMic", broadcastLinkMicCount);
        }

        if (broadcastPKCount > 0) {
            instance.track("pk", broadcastPKCount);
        }

        if (audienceOnSeatCount > 0) {
            instance.track("voiceChatRoomGuest", audienceOnSeatCount);
        }

        if (watchLinkMicCount > 0) {
            instance.track("lookLinkMic", watchLinkMicCount);
        }

        if (watchPKCount > 0) {
            instance.track("lookPk", watchPKCount);
        }

        audienceLinkMicCount = 0;

        broadcastLinkMicCount = 0;

        broadcastPKCount = 0;

        audienceOnSeatCount = 0;

        watchLinkMicCount = 0;

        watchPKCount = 0;
    }

}
