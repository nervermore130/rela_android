package com.thel.modules.live.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ScrollView;

/**
 * Created by the L on 2016/10/31.
 */

public class LiveBigGiftAnimScrollView extends ScrollView {
    public LiveBigGiftAnimScrollView(Context context) {
        super(context);
    }

    public LiveBigGiftAnimScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LiveBigGiftAnimScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return false;
    }
}
