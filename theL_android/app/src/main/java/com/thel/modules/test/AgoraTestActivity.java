package com.thel.modules.test;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.SurfaceView;
import android.widget.RelativeLayout;

import com.thel.R;
import com.thel.utils.L;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.microedition.khronos.egl.EGLContext;

import io.agora.rtc.Constants;
import io.agora.rtc.IRtcEngineEventHandler;
import io.agora.rtc.RtcEngine;
import io.agora.rtc.live.LiveTranscoding;
import io.agora.rtc.video.AgoraVideoFrame;
import io.agora.rtc.video.VideoCanvas;
import video.com.relavideolibrary.camera.CameraView;

public class AgoraTestActivity extends AppCompatActivity {

    private RtcEngine mRtcEngine;

    private LiveTranscoding mLiveTranscoding;

    private Map<Integer, UserInfo> mUserInfo = new HashMap<>();

    private int mBigUserId = 0;

    private SurfaceView mBigView;

    private RelativeLayout root_rl;

    private CameraView mCameraView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agora_test);
        root_rl = findViewById(R.id.root_rl);
        initEngine();
    }

    private void initEngine() {
        try {
            mRtcEngine = RtcEngine.create(getApplicationContext(), "eb5595ea0c254bedac6ed1884486e55c", mRtcEngineEventHandler);

            mRtcEngine.setExternalVideoSource(true, true, true);

            mRtcEngine.setParameters("{\"rtc.log_filter\":32781}");

            mRtcEngine.setLogFile("/sdcard/test_cdn.log");

            mRtcEngine.setChannelProfile(Constants.CHANNEL_PROFILE_LIVE_BROADCASTING);

            mRtcEngine.setClientRole(Constants.CLIENT_ROLE_BROADCASTER);

            mRtcEngine.enableVideo();

            mRtcEngine.setVideoProfile(Constants.VIDEO_PROFILE_720P, true);

            initTranscoding(540, 960, 600);

//            mRtcEngine.joinChannel(null, "liuyun", "", mBigUserId);

//            mBigView = RtcEngine.CreateRendererView(AgoraTestActivity.this);
            if (root_rl.getChildCount() > 0)
                root_rl.removeAllViews();
//            mBigView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
//            mBigView.setZOrderMediaOverlay(false);
//            mBigView.setZOrderOnTop(false);
//            root_rl.addView(mBigView);
//
//            mRtcEngine.setupLocalVideo(new VideoCanvas(mBigView, Constants.RENDER_MODE_HIDDEN, mBigUserId));

            setupLocalVideo(this);


            //Log.i("tjy","settranscoding 4");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initTranscoding(int width, int height, int bitrate) {
        if (mLiveTranscoding == null) {
            mLiveTranscoding = new LiveTranscoding();
            mLiveTranscoding.width = width;
            mLiveTranscoding.height = height;
            mLiveTranscoding.videoBitrate = bitrate;
            // if you want high fps, modify videoFramerate
            mLiveTranscoding.videoFramerate = 15;
        }
    }

    private void setTranscoding() {
        ArrayList<LiveTranscoding.TranscodingUser> transcodingUsers;
        ArrayList<UserInfo> videoUsers = getAllVideoUser(mUserInfo);

        transcodingUsers = cdnLayout(mBigUserId, videoUsers, mLiveTranscoding.width, mLiveTranscoding.height);

        for (LiveTranscoding.TranscodingUser user : transcodingUsers) {

            mLiveTranscoding.addUser(user);

        }
        mRtcEngine.setLiveTranscoding(mLiveTranscoding);
    }

    public static ArrayList<UserInfo> getAllVideoUser(Map<Integer, UserInfo> userInfo) {
        ArrayList<UserInfo> users = new ArrayList<>();
        Iterator<Map.Entry<Integer, UserInfo>> iterator = userInfo.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<Integer, UserInfo> entry = iterator.next();
            UserInfo user = entry.getValue();
            users.add(user);
        }
        return users;
    }

    public static ArrayList<LiveTranscoding.TranscodingUser> cdnLayout(int bigUserId, ArrayList<UserInfo> publishers,
                                                                       int canvasWidth,
                                                                       int canvasHeight) {

        ArrayList<LiveTranscoding.TranscodingUser> users;
        int index = 0;
        float xIndex, yIndex;
        int viewWidth;
        int viewHEdge;

        if (publishers.size() <= 1)
            viewWidth = canvasWidth;
        else
            viewWidth = canvasWidth / 2;

        if (publishers.size() <= 2)
            viewHEdge = canvasHeight;
        else
            viewHEdge = canvasHeight / ((publishers.size() - 1) / 2 + 1);

        users = new ArrayList<>(publishers.size());

        LiveTranscoding.TranscodingUser user0 = new LiveTranscoding.TranscodingUser();
        user0.uid = bigUserId;
        user0.alpha = 1;
        user0.zOrder = 0;
        user0.audioChannel = 0;

        user0.x = 0;
        user0.y = 0;
        user0.width = viewWidth;
        user0.height = viewHEdge;
        users.add(user0);

        index++;
        for (UserInfo entry : publishers) {
            if (entry.uid == bigUserId)
                continue;

            xIndex = index % 2;
            yIndex = index / 2;
            LiveTranscoding.TranscodingUser tmpUser = new LiveTranscoding.TranscodingUser();
            tmpUser.uid = entry.uid;
            tmpUser.x = (int) ((xIndex) * viewWidth);
            tmpUser.y = (int) (viewHEdge * (yIndex));
            tmpUser.width = viewWidth;
            tmpUser.height = viewHEdge;
            tmpUser.zOrder = index + 1;
            tmpUser.audioChannel = 0;
            tmpUser.alpha = 1f;

            users.add(tmpUser);
            index++;
        }

        return users;
    }

    IRtcEngineEventHandler mRtcEngineEventHandler = new IRtcEngineEventHandler() {
        @Override
        public void onError(int errorCode) {
            super.onError(errorCode);
            sendMsg("-->onError<--" + errorCode);
        }

        @Override
        public void onWarning(int warn) {
            super.onWarning(warn);
            sendMsg("-->onWarning<--" + warn);
        }

        @Override
        public void onJoinChannelSuccess(String channel, final int uid, int elapsed) {
            super.onJoinChannelSuccess(channel, uid, elapsed);
            sendMsg("-->onJoinChannelSuccess<--" + channel + "  -->uid<--" + uid);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mBigUserId = uid;

                    UserInfo mUI = new UserInfo();
//                    mUI.view = mBigView;
//                    mUI.view.setZOrderOnTop(true);

                    mUI.uid = mBigUserId;
                    mUserInfo.put(mBigUserId, mUI);
                    Log.i("tjy", "settranscoding 6");
                    setTranscoding();

                    mRtcEngine.addPublishStreamUrl("rtmp://rtmp.push-baidu.rela.me/live/103351699test", true);

                }
            });
        }

        @Override
        public void onFirstLocalVideoFrame(int width, int height, int elapsed) {
            super.onFirstLocalVideoFrame(width, height, elapsed);
            sendMsg("-->onFirstLocalVideoFrame<--");
        }

        @Override
        public void onRejoinChannelSuccess(String channel, int uid, int elapsed) {
            super.onRejoinChannelSuccess(channel, uid, elapsed);
            sendMsg(uid + " -->RejoinChannel<--");
        }

        @Override
        public void onLeaveChannel(RtcStats stats) {
            super.onLeaveChannel(stats);
            sendMsg("-->leaveChannel<--");
        }

        @Override
        public void onConnectionInterrupted() {
            super.onConnectionInterrupted();
            sendMsg("-->onConnectionInterrupted<--");
        }

        @Override
        public void onConnectionLost() {
            super.onConnectionLost();
            sendMsg("-->onConnectionLost<--");
        }

        @Override
        public void onStreamPublished(String url, final int error) {

            sendMsg("-->onStreamUrlPublished<--" + url + " -->error code<--" + error);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // error code
                    // 19 republish
                    // 0 publish success
                    if (error != 0) {
                    }
                }
            });

        }

        @Override
        public void onStreamUnpublished(String url) {
            super.onStreamUnpublished(url);
            sendMsg("-->onStreamUrlUnpublished<--" + url);
        }

        @Override
        public void onTranscodingUpdated() {
            super.onTranscodingUpdated();
        }

        @Override
        public void onUserJoined(final int uid, int elapsed) {
            super.onUserJoined(uid, elapsed);
            sendMsg("-->onUserJoined<--" + uid);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    UserInfo mUI = new UserInfo();
                    mUI.view = RtcEngine.CreateRendererView(AgoraTestActivity.this);
                    mUI.uid = uid;
                    mUI.view.setZOrderOnTop(true);
                    mUserInfo.put(uid, mUI);
                    mRtcEngine.setupRemoteVideo(new VideoCanvas(mUI.view, Constants.RENDER_MODE_HIDDEN, uid));
                    //Log.i("tjy","settranscoding 2");
                    //setTranscoding();
                }
            });
        }

        @Override
        public void onUserOffline(final int uid, int reason) {
            super.onUserOffline(uid, reason);
            sendMsg("-->unPublishedByHost<--" + uid);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mUserInfo.remove(uid);
                    //Log.i("tjy","settranscoding 3");
                    //setTranscoding();
                }
            });
        }
    };

    private void sendMsg(String msg) {

        L.d("AgoraTestActivity", " msg : " + msg);

    }

    private void setupLocalVideo(Context ctx) {

        mCameraView = new CameraView(ctx);

        mCameraView.setZOrderOnTop(false);

        mCameraView.setOnFrameAvailableHandler(new CameraView.OnFrameAvailableListener() {
            @Override
            public void onFrameAvailable(EGLContext eglContext, int textureId, int width, int height) {
                AgoraVideoFrame vf = new AgoraVideoFrame();
                vf.format = AgoraVideoFrame.FORMAT_TEXTURE_2D;
                vf.timeStamp = System.currentTimeMillis();
                vf.stride = width;
                vf.height = height;
                vf.textureID = textureId;
                vf.syncMode = true;
                vf.eglContext11 = eglContext;
                //OES transform
//                vf.transform = new float[]{
//                        0.0f, 1.0f, 0.0f, 0.0f,
//                        1.0f, 0.0f, 0.0f, 0.0f,
//                        0.0f, 0.0f, 1.0f, 0.0f,
//                        0.0f, 0.0f, 0.0f, 1.0f
//                };
                //texture2d transform
                vf.transform = new float[]{
                        1.0f, 0.0f, 0.0f, 0.0f,
                        0.0f, 1.0f, 0.0f, 0.0f,
                        0.0f, 0.0f, 1.0f, 0.0f,
                        0.0f, 0.0f, 0.0f, 1.0f
                };

                if (mRtcEngine != null) {

//                    mRtcEngine.pushExternalVideoFrame(vf);

                    boolean result = mRtcEngine.pushExternalVideoFrame(vf);

                    L.d("onFrameAvailable", "onFrameAvailable \neglContext:" + eglContext + "\ntextureId: " + textureId + "\npushExternalVideoFrame return: " + result + "\nwidth:" + width + "\nheight:" + height);
                }
            }
        });

        mCameraView.setOnEGLContextHandler(new CameraView.OnEGLContextListener() {
            @Override
            public void onEGLContextReady() {

//                L.d(TAG, "joinChannel");

//                joinChannel(token, channel, uid);

                mRtcEngine.setClientRole(Constants.CLIENT_ROLE_BROADCASTER);

//                setTranscoding();

                mRtcEngine.joinChannel(null, "liuyun", "", mBigUserId);


            }
        });

        root_rl.addView(mCameraView);

    }

}
