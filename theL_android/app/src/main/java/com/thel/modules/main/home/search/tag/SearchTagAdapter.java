package com.thel.modules.main.home.search.tag;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.TopicBean;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;

import java.util.List;

/**
 * Created by chad
 * Time 17/10/23
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */

public class SearchTagAdapter extends BaseRecyclerViewAdapter<TopicBean> {

    public SearchTagAdapter(List<TopicBean> list) {
        super(R.layout.recent_and_hot_topic_listitem, list);
    }

    @Override
    protected void convert(BaseViewHolder helper, TopicBean bean) {
        final TextView txt_create_new = helper.getView(R.id.txt_create_new);
        final TextView topic_name = helper.getView(R.id.topic_name);
        final GradientDrawable myGrad = (GradientDrawable) topic_name.getBackground();

        topic_name.setText(bean.topicName + "");
        if (bean.topicId.equals("-1")) {// 创建新的话题
            txt_create_new.setVisibility(View.VISIBLE);
            txt_create_new.setText(TheLApp.getContext().getString(R.string.search_topic_create_new));
            myGrad.setColor(ContextCompat.getColor(TheLApp.getContext(), R.color.bg_green));
        } else {
            if (TextUtils.isEmpty(bean.topicColor)) {
                myGrad.setColor(ContextCompat.getColor(TheLApp.getContext(), R.color.bg_green));
            } else {
                myGrad.setColor(Color.parseColor("#" + bean.topicColor));
            }
        }
    }
}
