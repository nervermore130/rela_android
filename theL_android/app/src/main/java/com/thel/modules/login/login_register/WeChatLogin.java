package com.thel.modules.login.login_register;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;
import android.widget.Toast;

import com.tencent.mm.opensdk.modelmsg.SendAuth;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.RfSBridgeHandlerFactory;
import com.thel.modules.login.email.EmailLoginActivity;
import com.thel.modules.main.MainActivity;
import com.thel.network.LoginInterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.api.loginapi.bean.SignInBean;
import com.thel.ui.LoadingDialogImpl;
import com.thel.utils.DialogUtil;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.ToastUtils;
import com.thel.utils.UserUtils;
import com.umeng.analytics.MobclickAgent;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import me.rela.rf_s_bridge.RfSBridgePlugin;
import me.rela.rf_s_bridge.RfSResultCallback;
import me.rela.rf_s_bridge.new_router.NewUniversalRouter;
import me.rela.rf_s_bridge.router.UniversalRouter;

public class WeChatLogin {

    private static final String TAG = "WeChatLogin";

    private IWXAPI wxApi;

    private WxLoginReceiver wxLoginReceiver;

    private Activity activity;

    private static WeChatLogin instance;

    private LoadingDialogImpl loadingDialog;

    private RfSResultCallback rfSResultCallback;

    private WeChatLogin() {

    }

    public static WeChatLogin getInstance() {
        if (instance == null) {
            instance = new WeChatLogin();
        }
        return instance;
    }

    public void login(Activity activity, RfSResultCallback rfSResultCallback) {

        loadingDialog = LoadingDialogImpl.LoadingDialogInstance.getInstance(activity);

        loadingDialog.showLoading();

        this.activity = activity;

        this.rfSResultCallback = rfSResultCallback;

        wxApi = WXAPIFactory.createWXAPI(TheLApp.context, TheLConstants.WX_APP_ID, true);

        wxApi.registerApp(TheLConstants.WX_APP_ID);

        registerReceiver(activity);

        MobclickAgent.onEvent(activity, "wechat_login");// 微信注册转化率统计：点击微信登陆
        if (!wxApi.isWXAppInstalled()) {
            Toast.makeText(activity, TheLApp.context.getString(R.string.default_activity_wx_uninstalled), Toast.LENGTH_SHORT).show();
            return;
        }

        SendAuth.Req req = new SendAuth.Req();
        req.scope = "snsapi_userinfo";
        req.state = "thel_login";
        wxApi.sendReq(req);
    }

    public void registerReceiver(Context context) {
        try {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(TheLConstants.BROADCAST_WX_LOGIN_SUCCEED);
            intentFilter.addAction(TheLConstants.BROADCAST_WX_LOGIN_CANCEL);
            wxLoginReceiver = new WxLoginReceiver();
            context.registerReceiver(wxLoginReceiver, intentFilter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void unRegisterReceiver(Context context) {
        try {
            context.unregisterReceiver(wxLoginReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 微信登录成功接收器
    private class WxLoginReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(TheLConstants.BROADCAST_WX_LOGIN_SUCCEED)) {
                String openid = intent.getStringExtra("openid");
                String access_token = intent.getStringExtra("access_token");
                String avatar = intent.getStringExtra("avatar");
                String nickName = intent.getStringExtra("nickName");
                String unionid = intent.getStringExtra("unionid");
                int sex = intent.getIntExtra("sex", 2);
                checkSex(openid, access_token, unionid, avatar, nickName, sex);
            } else if (intent.getAction().equals(TheLConstants.BROADCAST_WX_LOGIN_CANCEL)) {
                ToastUtils.showToastShort(TheLApp.context, TheLApp.context.getString(R.string.cancel));

                if (loadingDialog != null) {
                    loadingDialog.closeDialog();
                }
            }
        }
    }

    public void checkSex(final String openid, final String access_token, final String unionid, final String avatar, final String nickName, int sex) {

        MobclickAgent.onEvent(TheLApp.context, "wechat_female_login");// 微信注册转化率统计：女性性别校验成功

        login(openid + "," + unionid, access_token, avatar, nickName);
    }

    private void login(String openid, String access_token, String avatar, String nickName) {
        if (TextUtils.isEmpty(access_token) || TextUtils.isEmpty(openid)) {
            ToastUtils.showToastShort(activity, "wechat login access_token null");
            return;
        }
        MobclickAgent.onEvent(activity, "begin_request_loginOrRegister_interface"); // 开始调用登录接口打点
        RequestBusiness.getInstance().signIn("wx", "", "", "", "", "", openid, access_token, avatar, nickName, null)
                .onBackpressureDrop().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new LoginInterceptorSubscribe<SignInBean>() {
                    @Override
                    public void onNext(SignInBean data) {
                        super.onNext(data);
                        if (activity != null) {
                            if (data == null || data.data == null)
                                return;

                            if (data.data.user != null) {

                                MobclickAgent.onEvent(activity, "register_or_login_succeed");// 手机注册转化率统计：注册或登录成功
                                ShareFileUtils.saveUserData(data);
                                ShareFileUtils.setBoolean(ShareFileUtils.HAS_LOGGED, true);

                                if (data.data.user.isNewUser()) {

                                    ShareFileUtils.setBoolean(ShareFileUtils.IS_NEW_USER, true);

                                    data.data.code = "wechat";

                                    String resJson = GsonUtils.createJsonString(data.data);
                                    try {
                                        if (rfSResultCallback != null) {
                                            rfSResultCallback.onResult(resJson);
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                } else {

                                    Toast.makeText(activity, activity.getString(R.string.login_activity_success), Toast.LENGTH_SHORT).show();

                                    ShareFileUtils.setBoolean(ShareFileUtils.IS_NEW_USER, false);

                                    NewUniversalRouter.sharedInstance.flutterPopToRoot(false);

                                    RfSBridgePlugin.getInstance().activeRpc("EventLogin", UserUtils.getMyUserId(), null);

                                    RfSBridgeHandlerFactory.getInstance().getFlutterPushImpl().pushUserInfo();

                                    RfSBridgeHandlerFactory.getInstance().getFlutterPushImpl().pushMyUserId();

//                                    Intent intent2 = new Intent(activity, MainActivity.class);
//                                    intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                                    intent2.putExtra("isNew", false);
//                                    intent2.putExtra(TheLConstants.BUNDLE_KEY_WHERE_TO_GO, "ONE");
//                                    activity.startActivity(intent2);

                                    if (activity instanceof MainActivity) {
                                        MainActivity mainActivity = (MainActivity) activity;
                                        mainActivity.init();
                                    }
                                }

                            }
                            L.d("wxlogin", "accept :" + data);
                            unRegisterReceiver(activity);
                            if (loadingDialog != null) {
                                loadingDialog.closeDialog();
                            }
                        }

                    }

                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                        if (activity != null) {
                            unRegisterReceiver(activity);
                        }
                        if (loadingDialog != null) {
                            loadingDialog.closeDialog();
                        }
                        if (errDataBean != null && !TextUtils.isEmpty(errDataBean.errdesc)) {
                            ToastUtils.showCenterToastShort(activity, errDataBean.errdesc);
                        }
                    }
                });
    }

    public void cancel() {
        if (loadingDialog != null) {
            loadingDialog.closeDialog();
        }
    }

}
