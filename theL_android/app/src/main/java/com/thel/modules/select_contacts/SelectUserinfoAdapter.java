package com.thel.modules.select_contacts;

import android.content.Context;

import com.thel.R;
import com.thel.bean.ContactBean;
import com.thel.constants.TheLConstants;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;

import java.util.ArrayList;

/**
 * Created by chad
 * Time 17/10/12
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */

public class SelectUserinfoAdapter extends BaseRecyclerViewAdapter<ContactBean> {
    public Context mContext;

    public SelectUserinfoAdapter(Context context, ArrayList<ContactBean> selected_recycler) {
        super(R.layout.select_recyclerview_item, selected_recycler);
        mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, ContactBean item) {
        // 头像
        helper.setImageUrl(R.id.img_select_thumb, item.avatar, TheLConstants.AVATAR_MIDDLE_SIZE, TheLConstants.AVATAR_MIDDLE_SIZE);
    }

}
