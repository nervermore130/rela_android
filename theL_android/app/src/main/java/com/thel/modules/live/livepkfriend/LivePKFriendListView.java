package com.thel.modules.live.livepkfriend;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.thel.R;
import com.thel.base.BaseDialogFragment;
import com.thel.modules.live.LiveConstant;
import com.thel.modules.live.bean.LivePkFriendBean;
import com.thel.modules.live.bean.LivePkFriendListBean;
import com.thel.modules.live.bean.LivePkFriendListNetBean;
import com.thel.modules.live.in.LiveBaseView;
import com.thel.modules.live.in.LivePkContract;
import com.thel.modules.live.utils.LinkMicOrPKRefuseUtils;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.service.DefaultRequestService;
import com.thel.ui.decoration.DefaultItemDivider;
import com.thel.utils.ScreenUtils;
import com.thel.utils.SizeUtils;
import com.thel.utils.ViewUtils;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by waiarl on 2017/11/29.
 * 直播中的好友列表，用于发起Pk或者连麦
 */

public class LivePKFriendListView extends BaseDialogFragment implements LiveBaseView<LivePKFriendListView> {
    private String userId;
    private RecyclerView recyclerView;
    private List<LivePkFriendBean> list = new ArrayList<>();
    private LiveLinkMicOrPKListAdapter adapter;
    private LinearLayoutManager manager;
    private SwipeRefreshLayout swipe_container;
    private int cursor;
    private LivePkContract.FriendRequestClickListener friendRequestClickListener;
    private TextView txt_title;
    private View lin_default;

    public static LivePKFriendListView getInstance(Bundle bundle) {
        LivePKFriendListView instance = new LivePKFriendListView();
        instance.setArguments(bundle);
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Bundle bundle = getArguments();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.live_pk_friendlist_view, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        findViewById(view);
    }

    @Override
    public void onResume() {
        super.onResume();
        int width = ScreenUtils.getScreenWidth(getContext());
        int height = SizeUtils.dip2px(getContext(), 336);
        getDialog().getWindow().setLayout(width, height);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setListener();
        getData();
    }

    private void findViewById(View view) {
        swipe_container = view.findViewById(R.id.swipe_container);
        ViewUtils.initSwipeRefreshLayout(swipe_container);
        recyclerView = view.findViewById(R.id.recyclerview);
        adapter = new LiveLinkMicOrPKListAdapter(list, LiveConstant.TYPE_PK);
        manager = new LinearLayoutManager(getContext());
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(manager);
        recyclerView.addItemDecoration(new DefaultItemDivider(getContext(), LinearLayoutManager.VERTICAL, R.color.gray, 1, false, false));
        recyclerView.setAdapter(adapter);

        txt_title = view.findViewById(R.id.txt_title);
        txt_title.setText(R.string.pk_with_friend);


        lin_default = view.findViewById(R.id.lin_default);

        lin_default.setVisibility(View.GONE);
    }

    private void setListener() {
        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getRefreshData();
            }
        });

        adapter.setOnFriendRequestClickListener(new LivePkContract.FriendRequestClickListener() {
            @Override
            public void onClickRequest(LivePkFriendBean friendBean, int type, int position) {

                if (friendRequestClickListener != null) {
                    friendRequestClickListener.onClickRequest(friendBean, type, position);
                }

            }
        });

    }

    private void getData() {
        swipe_container.post(new Runnable() {
            @Override
            public void run() {
                swipe_container.setRefreshing(true);
            }
        });
        getRefreshData();
    }

    private void getRefreshData() {
        cursor = 1;
        getLivePkFriendListData(cursor);
    }

    private void getLivePkFriendListData(final int cursor) {
        final Flowable<LivePkFriendListNetBean> flowable = DefaultRequestService.createLiveRequestService().getLivePkFriendListNetBean();
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new InterceptorSubscribe<LivePkFriendListNetBean>() {
                    @Override
                    public void onNext(LivePkFriendListNetBean data) {
                        super.onNext(data);
                        if (!hasErrorCode && data != null && data.data != null) {
                            showEmptyData(false);
                            if (cursor == 1) {
                                showRefreshData(data.data);
                            } else {
                                showMoreData(data.data);
                            }
                            if (cursor == 1 && data.data.list.size() == 0) {
                                showEmptyData(true);
                            }
                        }
                    }

                    @Override
                    public void onComplete() {
                        super.onComplete();
                        requestFinish();
                    }
                });
    }

    private void showRefreshData(LivePkFriendListBean data) {
        LinkMicOrPKRefuseUtils.initRefuseData();
        list.clear();
        list.addAll(data.list);
        adapter.setNewData(list);
    }

    private void showMoreData(LivePkFriendListBean data) {
        list.addAll(data.list);
        adapter.notifyDataChangedAfterLoadMore(false, list.size());
    }

    private void showEmptyData(boolean show) {
        lin_default.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void requestFinish() {
        if (swipe_container != null)
            swipe_container.postDelayed(new Runnable() {

                @Override
                public void run() {
                    if (swipe_container != null && swipe_container.isRefreshing())
                        swipe_container.setRefreshing(false);
                }
            }, 1000);
    }

    /**
     * 点击连麦或者Pk
     *
     * @param listener
     */
    public void setOnFriendRequestClickListener(LivePkContract.FriendRequestClickListener listener) {
        this.friendRequestClickListener = listener;
    }


    /****************************************一下为接口集成方法，暂时不实现**************************************/
    @Override
    public LivePKFriendListView show() {
        return this;
    }

    @Override
    public LivePKFriendListView hide() {
        return this;
    }

    @Override
    public LivePKFriendListView destroyView() {
        return this;
    }

    @Override
    public boolean isAnimating() {
        return false;
    }

    @Override
    public void setAnimating(boolean isAnimating) {

    }

    @Override
    public void showShade(boolean show) {

    }

    private boolean isDestroied = false;

    @Override
    public void onDestroy() {
        super.onDestroy();

        isDestroied = true;
    }

}
