package com.thel.modules.main.home.moments;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import androidx.annotation.Nullable;

import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.view.SimpleDraweeView;
import com.liulishuo.filedownloader.BaseDownloadTask;
import com.liulishuo.filedownloader.FileDownloadSampleListener;
import com.liulishuo.filedownloader.FileDownloader;
import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UploadManager;
import com.thel.BuildConfig;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseFragment;
import com.thel.bean.user.UploadTokenBean;
import com.thel.constants.BundleConstants;
import com.thel.constants.TheLConstants;
import com.thel.modules.live.bean.ARGiftNetBean;
import com.thel.modules.live.bean.LiveRoomBean;
import com.thel.modules.live.bean.LiveRoomNetBean;
import com.thel.modules.live.stream.RelaStickDownload;
import com.thel.modules.live.surface.show.LiveShowActivity;
import com.thel.modules.live.view.LiveNoticeEditDialog;
import com.thel.modules.main.home.moments.web.WebViewActivity;
import com.thel.modules.preview_image.UserInfoPhotoActivity;
import com.thel.modules.select_image.SelectLocalImagesActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.RequestConstants;
import com.thel.network.service.DefaultRequestService;
import com.thel.tusdk.plain.CameraConfig;
import com.thel.ui.dialog.BeautifulDialog;
import com.thel.ui.dialog.StickerDownloadDialog;
import com.thel.ui.widget.GuideView;
import com.thel.utils.DialogUtil;
import com.thel.utils.GsonUtils;
import com.thel.utils.ImageUtils;
import com.thel.utils.L;
import com.thel.utils.MD5Utils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.StringUtils;
import com.thel.utils.ToastUtils;
import com.thel.utils.ViewUtils;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * 发布视频直播的日志fragment
 * Created by lingwei on 2018/5/4.
 */

public class EditVideoLiveMomentFragment extends BaseFragment implements View.OnClickListener, View.OnTouchListener {

    private static final String TAG = "EditVideo";
    private TextView notice_board_tips;
    private TextView txt_live_agreement;
    private RelativeLayout root_rl;
    // 选择的图片地址
    private String bgPicUrl;
    private TextView edit_title;
    private SimpleDraweeView add_live;
    // 上传头像图片到七牛后的文件路径
    private String uploadPath = "";
    private String uploadUrl = "";
    private LiveRoomBean liveRoomBean;

    private LinearLayout camera_switch;
    private LinearLayout beautiful;

    private TextView start_live;
    private LinearLayout bottom_container;
    private TextView notice_board;
    private String noticeBroad = "";
    private GuideView mGuideView;

    private CameraConfig cameraConfigBean = new CameraConfig();

    public static EditVideoLiveMomentFragment getInstance() {
        return new EditVideoLiveMomentFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.release_moment_video_live, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        findviewById(view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getActivity() != null && isAdded()) {
            initView();
            initData();
            if (!ShareFileUtils.getBoolean(ShareFileUtils.LIVE_NOTICE_EDIT, false)) {

                ShareFileUtils.setBoolean(ShareFileUtils.LIVE_NOTICE_EDIT, true);

                mGuideView.show(notice_board, null);
            }

        }
    }

    private void initData() {

    }

    @SuppressLint("ClickableViewAccessibility")
    private void findviewById(View view) {
        root_rl = view.findViewById(R.id.root_rl);
        txt_live_agreement = view.findViewById(R.id.txt_live_agreement);
        edit_title = view.findViewById(R.id.write_moment_add_text);
        add_live = view.findViewById(R.id.add_live);
        beautiful = view.findViewById(R.id.beautiful);
        camera_switch = view.findViewById(R.id.camera_switch);
        start_live = view.findViewById(R.id.start_live);
        bottom_container = view.findViewById(R.id.bottom_container);
        notice_board = view.findViewById(R.id.notice_board);
        notice_board_tips = view.findViewById(R.id.notice_board_tips);
        mGuideView = view.findViewById(R.id.guide_view);
        start_live.setOnClickListener(this);
        camera_switch.setOnTouchListener(this);
        beautiful.setOnTouchListener(this);
        notice_board.setOnClickListener(this);

        edit_title.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 50) {
                    ToastUtils.showToastShort(getActivity(), "最多填写50字");
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void initView() {
        String text = StringUtils.getString(R.string.live_agreement);
        txt_live_agreement.setText(Html.fromHtml(text));
        txt_live_agreement.setOnClickListener(this);
        add_live.setOnClickListener(this);

        String title = ShareFileUtils.getString(ShareFileUtils.RELEASED_LIVE_TITLE, null);

        String image = ShareFileUtils.getString(ShareFileUtils.RELEASED_LIVE_IMAGE, null);

        if (title != null && image != null) {

            edit_title.setText(title);

            uploadUrl = image;
            add_live.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadius(12));
            add_live.setImageURI(uploadUrl);

            start_live.setAlpha(1.0f);
        }


    }

    /**
     * 发布直播 选好图片后刷新界面
     *
     * @param path
     */
    private void photoSelected(String path) {
        uploadUrl = "";
        if (TextUtils.isEmpty(path)) {
            return;
        }
        bgPicUrl = ImageUtils.handlePhoto1(path, TheLConstants.F_TAKE_PHOTO_ROOTPATH, ImageUtils.getPicName(), false, TheLConstants.MAX_PIC_WIDTH, TheLConstants.MAX_PIC_HEIGHT, TheLConstants.PIC_QUALITY, 3);
        if (!TextUtils.isEmpty(bgPicUrl)) {
            add_live.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadius(12));
            add_live.setImageURI(Uri.parse(TheLConstants.FILE_PIC_URL + bgPicUrl));
            start_live.setAlpha(1.0f);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        L.d("EditVideo", " onActivityResult resultCode : " + resultCode);

        if (resultCode == TheLConstants.RESULT_CODE_TAKE_PHOTO) {// 拍摄照片
            String takePhotoPath = data.getStringExtra(TheLConstants.BUNDLE_KEY_IMAGE_OUTPUT_PATH);
            photoSelected(takePhotoPath);
        } else if (resultCode == TheLConstants.RESULT_CODE_WRITE_MOMENT_DELETE_PICTURE) {// 删除图片
            if (data != null) {
                if (data.getIntegerArrayListExtra(TheLConstants.BUNDLE_KEY_INDEX) == null || data.getIntegerArrayListExtra(TheLConstants.BUNDLE_KEY_INDEX).size() == 0) {
                    add_live.getHierarchy().setPlaceholderImage(R.color.transparent);
                    add_live.setController(null);
                    bgPicUrl = "";
                    uploadPath = "";
                    uploadUrl = "";
                    start_live.setAlpha(0.4f);
                }
            }
        } else if (resultCode == TheLConstants.RESULT_CODE_SELECT_LOCAL_IMAGE) {// 选择了图片
            String photoPath = data.getStringExtra(TheLConstants.BUNDLE_KEY_LOCAL_IMAGE_PATH);
            photoSelected(photoPath);
        }
    }


    public void releaseLive() {
        if (!TextUtils.isEmpty(uploadUrl)) {// 如果图片已上传成功，则直接发布
            startLive();
        } else {
            if (!TextUtils.isEmpty(bgPicUrl)) {
                uploadPath = RequestConstants.UPLOAD_FILE_ROOT_PATH_TIMELINE + ShareFileUtils.getString(ShareFileUtils.ID, "") + "/" + MD5Utils.calculateMD5(new File(bgPicUrl)) + ".jpg";
                showLoading();

                if (!TextUtils.isEmpty(uploadPath)) {
                    RequestBusiness.getInstance().getUploadToken(System.currentTimeMillis() + "", "", uploadPath).onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<UploadTokenBean>() {
                        @Override
                        public void onNext(UploadTokenBean uploadTokenBean) {
                            super.onNext(uploadTokenBean);
                            closeLoading();
                            if (!TextUtils.isEmpty(uploadTokenBean.data.uploadToken)) {
                                UploadManager uploadManager = new UploadManager();
                                uploadManager.put(new File(bgPicUrl), uploadPath, uploadTokenBean.data.uploadToken, new UpCompletionHandler() {
                                    @Override
                                    public void complete(String key, ResponseInfo info, JSONObject response) {
                                        if (info != null && info.statusCode == 200 && uploadPath.equals(key) && getActivity() != null && !getActivity().isDestroyed()) {
                                            uploadUrl = RequestConstants.FILE_BUCKET + key;
                                            startLive();
                                        } else {
                                            requestFailed();
                                        }
                                    }
                                }, null);
                            } else {
                                requestFailed();
                            }
                        }
                    });
                }
            } else {
                DialogUtil.showToastShortGravityCenter(getActivity(), TheLApp.context.getString(R.string.uploadimage_activity_dialog_title));
            }
        }
    }

    private void requestFailed() {
        closeLoading();
        DialogUtil.showToastShort(getActivity(), StringUtils.getString(R.string.message_network_error));
    }

    private void startLive() {

        String title = edit_title.getText().toString().trim();

        showLoading();

        RequestBusiness.getInstance().createLiveRoom(title, uploadUrl, "", LiveRoomBean.TYPE_VIDEO, noticeBroad).onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<LiveRoomNetBean>() {
            @Override
            public void onNext(LiveRoomNetBean roomBean) {
                super.onNext(roomBean);
                closeLoading();
                if (roomBean == null)
                    return;
                if (roomBean.data == null) {
                    DialogUtil.showToastShort(getActivity(), roomBean.errdesc);
                    return;
                }
                liveRoomBean = roomBean.data;

                downloadSticker();

                ShareFileUtils.setString(ShareFileUtils.RELEASED_LIVE_TITLE, title);

                ShareFileUtils.setString(ShareFileUtils.RELEASED_LIVE_IMAGE, uploadUrl);
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                closeLoading();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.notice_board:
                LiveNoticeEditDialog liveNoticeDialog = LiveNoticeEditDialog.newInstence(noticeBroad);
                liveNoticeDialog.setEditNoticeListener(new LiveNoticeEditDialog.EditNoticeListener() {
                    @Override
                    public void notice(String notice) {
                        noticeBroad = notice;
                        if (!TextUtils.isEmpty(notice)) {
                            notice_board.setText(R.string.notice_boarded);
                            notice_board.setTextColor(Color.WHITE);
                        } else {
                            notice_board.setTextColor(getResources().getColor(R.color.rela_color));
                            notice_board.setText(R.string.notice_board);
                        }
                    }
                });
                liveNoticeDialog.show(getChildFragmentManager(), LiveNoticeEditDialog.class.getName());
                break;
            case R.id.txt_live_agreement:
                Intent intentLive = new Intent(getActivity(), WebViewActivity.class);
                intentLive.putExtra(BundleConstants.URL, TheLConstants.LIVE_AGREEMENT_PAGE_URL);
                intentLive.putExtra(BundleConstants.NEED_SECURITY_CHECK, false);
                startActivity(intentLive);
                break;
            case R.id.add_live:
                if (!TextUtils.isEmpty(bgPicUrl)) {
                    Intent i = new Intent(getActivity(), UserInfoPhotoActivity.class);
                    i.putExtra("fromPage", ReleaseMomentActivity.class.getName());
                    ArrayList<String> photoUrls = new ArrayList<String>();
                    photoUrls.add(bgPicUrl);
                    i.putExtra("position", 0);
                    i.putStringArrayListExtra(TheLConstants.BUNDLE_KEY_PHOTOS, photoUrls);
                    startActivityForResult(i, TheLConstants.BUNDLE_CODE_WRITE_MOMENT_ACTIVITY);
                } else {
                    startActivityForResult(new Intent(getActivity(), SelectLocalImagesActivity.class), TheLConstants.BUNDLE_CODE_SELECT_PHOTO);
                }
                break;
            case R.id.start_live:
                ViewUtils.preventViewMultipleClick(v, 2000);
                releaseLive();
                break;
        }
    }

    //摄像头切换保护
    private boolean mCameraEnable = true;
    private Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            mCameraEnable = true;
            return false;
        }
    });

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        ViewUtils.preventViewMultipleClick(v, 1000);
        ReleaseLiveMomentActivity activity = (ReleaseLiveMomentActivity) getActivity();
        if (activity != null) {
            if (v.getId() == R.id.camera_switch) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_UP:
                        camera_switch.setAlpha(1.0f);

                        if (mCameraEnable) {
                            mCameraEnable = false;
                            mHandler.sendEmptyMessageDelayed(0, 100);
                            cameraConfigBean.mFrontFacing = activity.switchCameraOrienation();
                        }
                        break;
                    case MotionEvent.ACTION_MOVE:
                    case MotionEvent.ACTION_DOWN:
                        camera_switch.setAlpha(0.35f);
                        break;
                }
                return true;
            } else if (v.getId() == R.id.beautiful) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_UP:
                        beautiful.setAlpha(1.0f);

                        bottom_container.setVisibility(View.GONE);
                        ((ReleaseLiveMomentActivity) getActivity()).setViewpagerScroll(false);
                        BeautifulDialog beautifulDialog = BeautifulDialog.newInstance(cameraConfigBean, true);
                        beautifulDialog.setBeautyListener(new BeautifulDialog.BeautyListener() {

                            @Override
                            public void adjustBeauty(String key, float progress) {
                                activity.adjustBeauty(key, progress);
                            }

                            @Override
                            public void dialogClose(CameraConfig cameraConfig) {
                                bottom_container.setVisibility(View.VISIBLE);
                                ((ReleaseLiveMomentActivity) getActivity()).setViewpagerScroll(true);
                                cameraConfigBean = cameraConfig;
                            }
                        });
                        beautifulDialog.show(getChildFragmentManager(), BeautifulDialog.class.getName());

                        break;
                    case MotionEvent.ACTION_MOVE:
                    case MotionEvent.ACTION_DOWN:

                        beautiful.setAlpha(0.35f);
                        break;
                }
                return true;
            }
        }
        return false;
    }

    public void close() {
        if (getActivity() == null) {
            return;
        }
        if (!TextUtils.isEmpty(uploadUrl)) {
            DialogUtil.showConfirmDialog(getActivity(), null, getString(R.string.moments_write_moment_discard_tip), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    getActivity().finish();
                }
            });
        } else {
            getActivity().finish();
        }
    }

    private void gotoLiveActivity() {
        try {
            if (liveRoomBean != null && getActivity() != null) {

                Intent intent = new Intent(getActivity(), LiveShowActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable(TheLConstants.BUNDLE_KEY_LIVE_ROOM, liveRoomBean);
                bundle.putSerializable(TheLConstants.BUNDLE_KEY_LIVE_ROOM_CAMERA_CONFIG, cameraConfigBean);
                intent.putExtras(bundle);
                startActivity(intent);
                getActivity().finish();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int lastDownloadCount;
    private int allDownloadCount;
    private StickerDownloadDialog downloadDialog;

    private void downloadSticker() {
        String json = ShareFileUtils.getString(ShareFileUtils.AR_GIFT_LIST, "");

        L.d(TAG, " downloadSticker json : " + json);

        if (TextUtils.isEmpty(json)) {
            final Flowable<ARGiftNetBean> flowable = DefaultRequestService.createLiveRequestService().getARGiftList(BuildConfig.APPLICATION_ID);
            flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<ARGiftNetBean>() {
                @Override
                public void onNext(ARGiftNetBean data) {
                    super.onNext(data);
                    ShareFileUtils.setString(ShareFileUtils.TU_SDK_MASTER_KEY, data.data.arGiftMasterKey);

                    String json = GsonUtils.createJsonString(data);
                    ShareFileUtils.setString(ShareFileUtils.AR_GIFT_LIST, json);
                    optionArGift(data);
                }
            });
        } else {
            ARGiftNetBean arGiftNetBean = GsonUtils.getObject(json, ARGiftNetBean.class);
            optionArGift(arGiftNetBean);
        }
    }

    private void optionArGift(ARGiftNetBean arGiftNetBean) {
        if (arGiftNetBean != null && arGiftNetBean.data != null && arGiftNetBean.data.list != null) {

            //清除所有的下载任务
            FileDownloader.getImpl().clearAllTaskData();

            for (ARGiftNetBean.ARGiftBean arGiftBean : arGiftNetBean.data.list) {
                String url = arGiftBean.arResource;

                L.d(TAG, " optionArGift url : " + url);

                if (!TextUtils.isEmpty(url)) {
                    String fileName = url.substring(url.lastIndexOf("/") + 1);

                    L.d(TAG, " optionArGift fileName : " + fileName);

                    String path = TheLConstants.F_AR_GIFT_ROOTPATH + fileName;
                    File file = new File(path);

                    if (file.exists()) {
                        RelaStickDownload.prepareSingleLocalSticker(fileName);
                    } else {
                        FileDownloader.getImpl().create(url)
                                .setPath(path)
                                .setWifiRequired(false)
                                .setForceReDownload(true)
                                .setCallbackProgressTimes(100)
                                .setListener(mQueueTarget)
                                .asInQueueTask()
                                .enqueue();
                        lastDownloadCount++;
                        allDownloadCount++;

                        L.d(TAG, " optionArGift lastDownloadCount : " + lastDownloadCount);

                        L.d(TAG, " optionArGift allDownloadCount : " + allDownloadCount);
                    }
                }
            }
        }

        if (lastDownloadCount > 0) {
            //串行执行该队列
            FileDownloader.getImpl().start(mQueueTarget, true);
            downloadDialog = new StickerDownloadDialog();
            downloadDialog.show(getChildFragmentManager(), StickerDownloadDialog.class.getName());
        } else {
            gotoLiveActivity();
        }
    }

    private final FileDownloadSampleListener mQueueTarget = new FileDownloadSampleListener() {

        @Override
        protected void progress(BaseDownloadTask task, int soFarBytes, int totalBytes) {

            int progress = (int) (soFarBytes * 1.0f / totalBytes * 100f / allDownloadCount + 100f / allDownloadCount * (allDownloadCount - lastDownloadCount));
            if (downloadDialog != null) {
                downloadDialog.setProgress(progress);
            }
        }

        @Override
        protected void completed(BaseDownloadTask task) {
            lastDownloadCount--;
            String fileName = task.getFilename();
            RelaStickDownload.prepareSingleLocalSticker(fileName);
            L.d(TAG, "completed: " + "fileName: " + fileName + " , lastDownloadCount : " + lastDownloadCount);
            if (lastDownloadCount == 0) {
                gotoLiveActivity();
                if (downloadDialog != null) downloadDialog.dismissAllowingStateLoss();
            }
        }

        @Override
        protected void error(BaseDownloadTask task, Throwable e) {
            super.error(task, e);

            L.d(TAG, "error: " + "Throwable: " + e.getMessage() + " error getUrl : " + task.getUrl());

        }
    };

}
