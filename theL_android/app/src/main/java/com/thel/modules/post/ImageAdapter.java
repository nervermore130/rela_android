package com.thel.modules.post;

import android.content.Context;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.thel.R;
import com.thel.bean.user.UserInfoPicBean;
import com.thel.utils.ImageUtils;

import java.util.List;

/**
 * Created by chad
 * Time 19/3/18
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */
class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ViewHolder> {

    int width;

    private Context mContext;

    private List<UserInfoPicBean> mData;

    public ImageAdapter(Context context, List<UserInfoPicBean> data, int width) {
        this.mContext = context;
        this.mData = data;
        this.width = width;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(View.inflate(mContext, R.layout.item_imageview, null));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {

        int itemHeight;
        if (getItemCount() == 1) {
            itemHeight = width;
        } else if (getItemCount() < 7) {
            itemHeight = width / 2;
        } else {
            itemHeight = width / 3;
        }
        viewHolder.container.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, itemHeight));

        viewHolder.image.setController(Fresco.
                newDraweeControllerBuilder().
                setImageRequest(ImageRequestBuilder
                        .newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(mData.get(position).picUrl, viewHolder.image.getWidth(), itemHeight)))
                        .build()).
                setAutoPlayAnimations(true).
                build());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull final RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        GridLayoutManager manager;
        if (recyclerView.getLayoutManager() instanceof GridLayoutManager) {
            manager = (GridLayoutManager) recyclerView.getLayoutManager();
            manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    int span;
                    switch (getItemCount()) {

                        case 1:
                        case 2://
                            span = 6;
                            break;
                        case 3:
                            if (position == 0) {
                                span = 6;
                            } else {
                                span = 3;
                            }
                            break;
                        case 4:
                            span = 3;
                            break;
                        case 5://
                            if (position == 0 || position == 1) {
                                span = 3;
                            } else {
                                span = 2;
                            }
                            break;
                        case 6://
                            span = 2;
                            break;
                        case 7:
                            if (position == 0) {
                                span = 6;
                            } else {
                                span = 2;
                            }
                            break;
                        case 8:
                            if (position == 0 || position == 1) {
                                span = 3;
                            } else {
                                span = 2;
                            }
                            break;
                        case 9:
                            span = 2;
                            break;
                        default:
                            span = 6;
                            break;
                    }
                    return span;
                }
            });
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public RelativeLayout container;

        public SimpleDraweeView image;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            container = itemView.findViewById(R.id.container);
            image = itemView.findViewById(R.id.image);
        }
    }
}
