package com.thel.modules.live.utils;

import com.google.gson.reflect.TypeToken;
import com.thel.modules.live.bean.LivePkRefuseBean;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SharedPrefUtils;
import com.thel.utils.Utils;

import java.util.Iterator;
import java.util.List;

public class LinkMicOrPKRefuseUtils {

    private static final String TAG = "LinkMicOrPKRefuseUtils";

    //拒绝后10分钟后才可以再次发送邀请
    private static final long MAX_REFUSE_TIME = 10 * 60 * 1000;

    /**
     * 初始化拒绝pk和连麦的数据
     */
    public static void initRefuseData() {

        List<LivePkRefuseBean> pkRefuseBeans = getPKRefuseList();

        Iterator<LivePkRefuseBean> pkIterator = pkRefuseBeans.iterator();

        while (pkIterator.hasNext()) {

            LivePkRefuseBean livePkRefuseBean = pkIterator.next();

            if (System.currentTimeMillis() - livePkRefuseBean.refuseTime > MAX_REFUSE_TIME) {

                pkIterator.remove();

            }

        }

        savePKRefuseData(pkRefuseBeans);


        List<LivePkRefuseBean> linkMicRefuseList = getLinkMicRefuseList();

        Iterator<LivePkRefuseBean> linkMicIterator = linkMicRefuseList.iterator();

        while (linkMicIterator.hasNext()) {

            LivePkRefuseBean livePkRefuseBean = linkMicIterator.next();

            if (System.currentTimeMillis() - livePkRefuseBean.refuseTime > MAX_REFUSE_TIME) {

                linkMicIterator.remove();

            }

        }

        saveLinkMicRefuseData(linkMicRefuseList);


        List<LivePkRefuseBean> audienceLinkMicRefuseList = getAudienceLinkMicRefuseList();

        Iterator<LivePkRefuseBean> audienceLinkMicIterator = audienceLinkMicRefuseList.iterator();

        while (audienceLinkMicIterator.hasNext()) {

            LivePkRefuseBean livePkRefuseBean = linkMicIterator.next();

            if (System.currentTimeMillis() - livePkRefuseBean.refuseTime > MAX_REFUSE_TIME) {

                linkMicIterator.remove();

            }

        }

        saveAudienceLinkMicRefuseData(audienceLinkMicRefuseList);
    }

    /**
     * 获取拒绝PK的主播列表
     *
     * @return
     */
    private static List<LivePkRefuseBean> getPKRefuseList() {

        final String json = ShareFileUtils.getString(getSharePkRefuseName(), "[]");

        return GsonUtils.getGson().fromJson(json, new TypeToken<List<LivePkRefuseBean>>() {
        }.getType());
    }

    /**
     * 获取拒绝连麦的主播列表
     *
     * @return
     */

    private static List<LivePkRefuseBean> getLinkMicRefuseList() {

        final String json = ShareFileUtils.getString(getShareLinkMicRefuseName(), "[]");

        return GsonUtils.getGson().fromJson(json, new TypeToken<List<LivePkRefuseBean>>() {
        }.getType());
    }

    /**
     * 获取拒绝连麦的主播列表
     *
     * @return
     */

    private static List<LivePkRefuseBean> getAudienceLinkMicRefuseList() {

        final String json = ShareFileUtils.getString(getAudienceLinkMicRefuseName(), "[]");

        L.d(TAG, " getAudienceLinkMicRefuseList json : " + json);

        return GsonUtils.getGson().fromJson(json, new TypeToken<List<LivePkRefuseBean>>() {
        }.getType());
    }

    /**
     * 添加拒绝pk的主播id
     *
     * @param uid
     */
    public static void addPkRefuse(String uid) {

        L.d(TAG, " addPkRefuse uid : " + uid);

        List<LivePkRefuseBean> pkRefuseBeans = getPKRefuseList();
        LivePkRefuseBean livePkRefuseBean = new LivePkRefuseBean(uid, System.currentTimeMillis());
        pkRefuseBeans.add(livePkRefuseBean);
        savePKRefuseData(pkRefuseBeans);
    }

    /**
     * 添加拒绝连麦的主播id
     *
     * @param uid
     */
    public static void addLinkMicRefuse(String uid) {

        List<LivePkRefuseBean> linkMicRefuseList = getLinkMicRefuseList();

        LivePkRefuseBean livePkRefuseBean = new LivePkRefuseBean(uid, System.currentTimeMillis());

        linkMicRefuseList.add(livePkRefuseBean);

        saveLinkMicRefuseData(linkMicRefuseList);
    }

    /**
     * 添加拒绝连麦的主播id
     *
     * @param uid
     */
    public static void addAudienceLinkMicRefuse(String uid) {

        L.d(TAG, " addAudienceLinkMicRefuse uid : " + uid);

        List<LivePkRefuseBean> audienceLinkMicRefuseList = getAudienceLinkMicRefuseList();

        LivePkRefuseBean livePkRefuseBean = new LivePkRefuseBean(uid, System.currentTimeMillis());

        audienceLinkMicRefuseList.add(livePkRefuseBean);

        saveAudienceLinkMicRefuseData(audienceLinkMicRefuseList);
    }

    /**
     * 是否被拒绝pk
     *
     * @param uid 被拒绝的主播的id
     * @return
     */
    public static boolean isPKRefuse(String uid) {

        List<LivePkRefuseBean> pkRefuseList = getPKRefuseList();

        if (pkRefuseList == null) {
            return false;
        }

        for (LivePkRefuseBean lprb : pkRefuseList) {
            if (lprb.userId.equals(uid) && System.currentTimeMillis() - lprb.refuseTime < MAX_REFUSE_TIME) {
                return true;
            }
        }
        return false;

    }

    /**
     * 是否被拒绝连麦
     *
     * @param uid 被拒绝的主播的id
     * @return
     */
    public static boolean isLinkMicRefuse(String uid) {

        List<LivePkRefuseBean> linkMicRefuseList = getLinkMicRefuseList();

        if (linkMicRefuseList == null) {
            return false;
        }

        for (LivePkRefuseBean lprb : linkMicRefuseList) {

            if (lprb.userId.equals(uid) && System.currentTimeMillis() - lprb.refuseTime < MAX_REFUSE_TIME) {
                return true;
            }
        }

        return false;
    }

    /**
     * 是否被观众拒绝连麦
     *
     * @param uid 被拒绝的主播的id
     * @return
     */
    public static boolean isAudienceLinkMicRefuse(String uid) {

        List<LivePkRefuseBean> linkMicRefuseList = getAudienceLinkMicRefuseList();

        if (linkMicRefuseList == null) {
            return false;
        }

        for (LivePkRefuseBean lprb : linkMicRefuseList) {

            if (lprb.userId.equals(uid) && System.currentTimeMillis() - lprb.refuseTime < MAX_REFUSE_TIME) {
                return true;
            }
        }

        return false;
    }

    public static LivePkRefuseBean getPkRefuseInfo(String uid) {

        List<LivePkRefuseBean> linkMicRefuseList = getPKRefuseList();

        if (linkMicRefuseList == null) {
            return null;
        }

        for (LivePkRefuseBean lprb : linkMicRefuseList) {


            if (lprb.userId.equals(uid) && System.currentTimeMillis() - lprb.refuseTime < MAX_REFUSE_TIME) {
                return lprb;
            }
        }

        return null;
    }

    public static LivePkRefuseBean getLinkMicRefuseInfo(String uid) {

        List<LivePkRefuseBean> linkMicRefuseList = getLinkMicRefuseList();

        if (linkMicRefuseList == null) {
            return null;
        }

        for (LivePkRefuseBean lprb : linkMicRefuseList) {


            if (lprb.userId.equals(uid) && System.currentTimeMillis() - lprb.refuseTime < MAX_REFUSE_TIME) {
                return lprb;
            }
        }

        return null;
    }

    public static LivePkRefuseBean getAudienceLinkMicRefuseInfo(String uid) {

        List<LivePkRefuseBean> audienceLinkMicRefuseList = getAudienceLinkMicRefuseList();

        if (audienceLinkMicRefuseList == null) {
            return null;
        }

        for (LivePkRefuseBean lprb : audienceLinkMicRefuseList) {

            if (lprb.userId.equals(uid) && System.currentTimeMillis() - lprb.refuseTime < MAX_REFUSE_TIME) {
                return lprb;
            }
        }

        return null;
    }

    /**
     * 保存被拒绝Pk的主播列表
     *
     * @param pkRefuseBeans
     */
    private static void savePKRefuseData(List<LivePkRefuseBean> pkRefuseBeans) {

        String json = GsonUtils.createJsonString(pkRefuseBeans);

        ShareFileUtils.setString(getSharePkRefuseName(), json);
    }

    /**
     * 保存被拒绝的连麦的主播列表
     *
     * @param linkMicRefuseList
     */
    private static void saveLinkMicRefuseData(List<LivePkRefuseBean> linkMicRefuseList) {

        String json = GsonUtils.createJsonString(linkMicRefuseList);

        ShareFileUtils.setString(getShareLinkMicRefuseName(), json);

    }

    /**
     * 保存被拒绝的连麦的主播列表
     *
     * @param audienceLinkMicRefuseList
     */
    private static void saveAudienceLinkMicRefuseData(List<LivePkRefuseBean> audienceLinkMicRefuseList) {

        String json = GsonUtils.createJsonString(audienceLinkMicRefuseList);

        ShareFileUtils.setString(getAudienceLinkMicRefuseName(), json);

    }

    private static String getSharePkRefuseName() {

        return SharedPrefUtils.FILE_LIVE_PK_REFUSER + Utils.getMyUserId();
    }

    private static String getShareLinkMicRefuseName() {

        return SharedPrefUtils.FILE_LIVE_LINk_MIC_REFUSER + Utils.getMyUserId();
    }

    private static String getAudienceLinkMicRefuseName() {

        return SharedPrefUtils.FILE_LIVE_AUDIENCE_LINk_MIC_REFUSER + Utils.getMyUserId();
    }

}
