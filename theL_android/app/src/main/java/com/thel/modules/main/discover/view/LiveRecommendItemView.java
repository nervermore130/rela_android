package com.thel.modules.main.discover.view;

import android.content.Context;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.view.SimpleDraweeView;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.imp.follow.FollowStatusChangedImpl;
import com.thel.modules.live.bean.LiveRoomBean;
import com.thel.modules.main.discover.LiveClassifyItemBaseView;
import com.thel.utils.AppInit;
import com.thel.utils.SimpleDraweeViewUtils;

/**
 * 直播分类推荐主播item
 * Created by lingwei on 2017/11/28.
 */

public class LiveRecommendItemView extends LinearLayout implements LiveClassifyItemBaseView<LiveRecommendItemView> {
    private final Context mContext;
    private float radius;
    private float padding;
    private int picSize;
    private SimpleDraweeView preview;
    private TextView txt_nickname;
    private TextView txt_distance;
    private TextView recommend_fans;
    private TextView tv_follow;
    private LiveRoomBean liveRoomBean;
    private TextView txt_label;

    public LiveRecommendItemView(Context context) {
        this(context, null);
    }

    public LiveRecommendItemView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LiveRecommendItemView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        initData();
        init();
        setListener();
    }

    private void initData() {
        radius = TheLApp.getContext().getResources().getDimension(R.dimen.live_new_user_item_radius);
        padding = TheLApp.getContext().getResources().getDimension(R.dimen.live_new_user_item_padding);
        picSize = (int) ((AppInit.displayMetrics.widthPixels - 3 * padding) / 2);
    }

    @Override
    public LiveRecommendItemView initMeasureDimen() {
        getLayoutParams().width = picSize;
        getLayoutParams().height = picSize;
        return this;
    }

    private void init() {
        inflate(mContext, R.layout.recommend_live_item_view, this);
        preview = findViewById(R.id.preview);
        txt_nickname = findViewById(R.id.nickname);
        txt_distance = findViewById(R.id.txt_distance);
        recommend_fans = findViewById(R.id.recommend_fans);
        tv_follow = findViewById(R.id.tv_follow);
        txt_label = findViewById(R.id.txt_label);

    }

    @Override
    public LiveRecommendItemView initView(LiveRoomBean liveRoomBean, String pageId, int currentType,int currentPosion) {
        if (liveRoomBean != null && liveRoomBean.user != null) {
            this.liveRoomBean = liveRoomBean;

            preview.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadius(radius));
            SimpleDraweeViewUtils.setImageUrl(preview, liveRoomBean.imageUrl, TheLApp.getContext().getResources().getDimension(R.dimen.moment_pic_thumbnail_big), TheLApp.getContext().getResources().getDimension(R.dimen.moment_pic_thumbnail_big));
            txt_nickname.setText(liveRoomBean.user.nickName);
            txt_distance.getPaint().setFakeBoldText(true);

            if (liveRoomBean.user.isFollow == 0) {
                tv_follow.setText(TheLApp.getContext().getString(R.string.userinfo_activity_follow));
                tv_follow.setBackgroundResource(R.drawable.bg_bottome_follow);
                tv_follow.setTextColor(ContextCompat.getColor(TheLApp.getContext(), R.color.white));
            } else {
                tv_follow.setText(TheLApp.getContext().getString(R.string.userinfo_activity_followed));
                tv_follow.setBackgroundResource(R.drawable.bg_bottom_followed);
                tv_follow.setTextColor(ContextCompat.getColor(TheLApp.getContext(), R.color.white_alpha_60));
            }
            txt_label.setVisibility(View.GONE);

            if (!TextUtils.isEmpty(liveRoomBean.label)) {

                txt_label.setVisibility(View.VISIBLE);
                txt_label.setText(liveRoomBean.label);
            }
            try {
                if (liveRoomBean.distance != null && (liveRoomBean.distance.contains(" Km") || liveRoomBean.distance.contains(" km"))) {
                    float dis = -1f;
                    if (liveRoomBean.distance.contains(" Km")) {
                        dis = Float.valueOf(liveRoomBean.distance.substring(0, liveRoomBean.distance.indexOf(" Km")));
                    } else if (liveRoomBean.distance.contains(" km")) {
                        dis = Float.valueOf(liveRoomBean.distance.substring(0, liveRoomBean.distance.indexOf(" km")));
                    }
                    if (dis <= 5) {
                        if (dis == -1f) {
                            txt_distance.setText(liveRoomBean.distance);
                        } else {
                            txt_distance.setText(liveRoomBean.distance);
                        }
                    } else {
                        txt_distance.setText(liveRoomBean.distance);
                    }
                } else {
                    txt_distance.setText(liveRoomBean.distance);
                }
            } catch (Exception e) {
                txt_distance.setText(liveRoomBean.distance);
            }
            recommend_fans.setText(liveRoomBean.user.fansNum + "");

        }

        // holdView.setText(R.id.recommend_fans, liveRoomBean.fansNum);
        return this;
    }

    /**
     * 对推荐关注的用户进行是否关注操作
     */
    private void setListener() {
        tv_follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (liveRoomBean != null && liveRoomBean.user != null) {
                    FollowStatusChangedImpl.followUserWithNoDialog(liveRoomBean.user.id + "", FollowStatusChangedImpl.ACTION_TYPE_FOLLOW, liveRoomBean.user.nickName, liveRoomBean.user.avatar);
                    view.setBackgroundResource(R.drawable.bg_bottom_followed);
                    tv_follow.setText(TheLApp.getContext().getString(R.string.userinfo_activity_followed));
                    tv_follow.setTextColor(ContextCompat.getColor(TheLApp.getContext(), R.color.white_alpha_60));
                }
            }
        });
    }

}
