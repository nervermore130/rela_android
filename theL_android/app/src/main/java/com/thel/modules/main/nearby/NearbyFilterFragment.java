package com.thel.modules.main.nearby;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.text.TextPaint;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kyleduo.switchbutton.SwitchButton;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseFragment;
import com.thel.modules.main.me.bean.LogInfoBean;
import com.thel.modules.main.me.match.eventcollect.collect.LiveLogUtils;
import com.thel.modules.main.me.match.eventcollect.collect.MatchLogUtils;
import com.thel.modules.others.VipConfigActivity;
import com.thel.network.RequestConstants;
import com.thel.ui.widget.RangeSeekBar;
import com.thel.ui.widget.flowlayout.FlowLayout;
import com.thel.ui.widget.flowlayout.TagAdapter;
import com.thel.ui.widget.flowlayout.TagFlowLayout;
import com.thel.utils.L;
import com.thel.utils.SizeUtils;
import com.thel.utils.UserUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;


public class NearbyFilterFragment extends BaseFragment implements View.OnClickListener {

    private TagFlowLayout self_identity_tag;

    private TagFlowLayout love_tag;

    private TagFlowLayout constellation_tag;

    private RangeSeekBar age_seekBar;

    private TextView txt_age;

    private TextView start_filter;

    private TextView look_vip;

    private SwitchButton active_switch;

    private SwitchButton vip_switch;

    private SwitchButton photo_switch;

    private RelativeLayout active_rl;

    private RelativeLayout vip_rl;

    private RelativeLayout photo_rl;

    private LinearLayout constellation_ll;

    private RelativeLayout root_rl;

    private String wantRole = "";
    private String age = "";
    private String affection = "";
    private String active = "0";
    private String vip = "0";
    private String have_photo = "0";
    private String horoscope = "";

    private List<TagBean> tagRoleBeans = new ArrayList<>();

    private List<TagBean> horoscopeBeans = new ArrayList<>();
    private String pageId;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            Bundle bundle = getArguments();
            wantRole = bundle.getString(RequestConstants.HER_ROLE_NAME);
            age = bundle.getString(RequestConstants.AGE);
            affection = bundle.getString(RequestConstants.AFFECTION);
            active = bundle.getString(RequestConstants.ACTIVE);
            vip = bundle.getString(RequestConstants.VIP);
            have_photo = bundle.getString(RequestConstants.HAVE_PHOTO);
            horoscope = bundle.getString(RequestConstants.HOROSCOPE);
            L.d("NearbyFilterFragment",
                    "wantRole:" + wantRole + "\n" +
                            "age:" + age + "\n" +
                            "affection:" + affection + "\n" +
                            "active:" + active + "\n" +
                            "vip:" + vip + "\n" +
                            "have_photo:" + have_photo + "\n" +
                            "horoscope:" + horoscope
            );
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.nearby_filter_popup, container, false);

        ButterKnife.bind(view);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        self_identity_tag = view.findViewById(R.id.self_identity_tag);
        love_tag = view.findViewById(R.id.love_tag);
        constellation_tag = view.findViewById(R.id.constellation_tag);
        age_seekBar = view.findViewById(R.id.age_seekBar);
        txt_age = view.findViewById(R.id.txt_age);
        start_filter = view.findViewById(R.id.start_filter);
        look_vip = view.findViewById(R.id.look_vip);
        active_switch = view.findViewById(R.id.active_switch);
        vip_switch = view.findViewById(R.id.vip_switch);
        photo_switch = view.findViewById(R.id.photo_switch);
        active_rl = view.findViewById(R.id.active_rl);
        vip_rl = view.findViewById(R.id.vip_rl);
        photo_rl = view.findViewById(R.id.photo_rl);
        constellation_ll = view.findViewById(R.id.constellation_ll);
        root_rl = view.findViewById(R.id.root_rl);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        init();
        initRoleMap();
        initHoroscopeBeans();
        initIdentity();
        initLove();
        initConstellation();
        setListener();
        showBackgroudColor();

    }

    private void init() {
        if (!age.isEmpty()) {
            String[] ages = age.split(",");
            float big;
            if (ages[1].contains("+")) {
                big = 40;
            } else {
                big = Float.parseFloat(ages[1]);
            }

            age_seekBar.setRange(Float.parseFloat(ages[0]), big);
        }

        if (!active.isEmpty())
            active_switch.setChecked(active.equals("1"));

        if (!vip.isEmpty())
            vip_switch.setChecked(vip.equals("1"));

        if (!have_photo.isEmpty())
            photo_switch.setChecked(have_photo.equals("1"));

    }

    private void showBackgroudColor() {
        root_rl.postDelayed(new Runnable() {
            @Override
            public void run() {
                root_rl.setBackgroundColor(TheLApp.context.getResources().getColor(R.color.black_transparent_50));
            }
        }, 500);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (UserUtils.getUserVipLevel() == 0) {
            active_switch.setEnabled(false);
            vip_switch.setEnabled(false);
            photo_switch.setEnabled(false);
        } else {
            active_switch.setEnabled(true);
            vip_switch.setEnabled(true);
            photo_switch.setEnabled(true);
        }
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

    /**
     * 感情状态选项
     */
    private void initLove() {
        love_tag.setMaxSelectCount(1);

        love_tag.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (affection.isEmpty()) {
                    love_tag.performItemClick(0);
                } else
                    love_tag.performItemClick(Integer.parseInt(affection));
            }
        }, 300);

        final LayoutInflater mInflater = LayoutInflater.from(getContext());

        String[] arrays = {getString(R.string.txt_all), getString(R.string.single), getString(R.string.no_single)};

        love_tag.setAdapter(new TagAdapter<String>(arrays) {

            @Override
            public View getView(FlowLayout parent, int position, String tag) {

                TextView textView = (TextView) mInflater.inflate(R.layout.item_role_tag,
                        love_tag, false);

                textView.setText(tag);

                int textViewMinWidth = SizeUtils.dip2px(TheLApp.context, 30);

                int marginRightWidth = SizeUtils.dip2px(TheLApp.context, 10);

                int marginTopWidth = SizeUtils.dip2px(TheLApp.context, 10);

                int textWidth = (int) getTextWidth(tag, 12) + textViewMinWidth;

                FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(textWidth, textViewMinWidth);
                params.gravity = Gravity.CENTER;
                params.setMargins(0, marginTopWidth, marginRightWidth, 0);
                textView.setLayoutParams(params);

                return textView;
            }
        });

        love_tag.setOnTagClickListener(new TagFlowLayout.OnTagClickListener() {
            @Override
            public boolean onTagClick(View view, int position, FlowLayout parent, boolean performClick) {

                love_tag.toggle(view, position, parent);

                affection = position + "";
                return true;
            }
        });
    }

    /**
     * 自我认同选项
     */
    private void initIdentity() {

        self_identity_tag.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (wantRole.isEmpty())
                    self_identity_tag.performItemClick(0);
                else {
                    String[] wantRoles = wantRole.split(",");
                    for (int i = 0; i < wantRoles.length; i++) {
                        self_identity_tag.performItemClick(Integer.parseInt(wantRoles[i]));
                    }
                }
            }
        }, 300);

        self_identity_tag.setAdapter(tagAdapter);

        self_identity_tag.setOnTagClickListener(new TagFlowLayout.OnTagClickListener() {
            @Override
            public boolean onTagClick(View view, int position, FlowLayout parent, boolean performClick) {

                TagBean tagBean = tagRoleBeans.get(position);

                tagBean.isSelected = !tagBean.isSelected;

                self_identity_tag.toggle2(view, position, parent);

                if (position == 0) {
                    for (int i = 0; i < tagRoleBeans.size(); i++) {
                        TagBean tb = tagRoleBeans.get(i);

                        tb.isSelected = i == 0;
                    }
                    wantRole = "";
                } else {
                    StringBuilder stringBuffer = new StringBuilder();

                    for (int i = 1; i < tagRoleBeans.size(); i++) {
                        TagBean tb = tagRoleBeans.get(i);
                        if (tb.isSelected) {
                            stringBuffer.append(tb.id);
                            if (i < tagRoleBeans.size() - 1) {
                                stringBuffer.append(",");
                            }
                        }
                    }

                    wantRole = stringBuffer.toString();

                    if (wantRole.endsWith(",")) {
                        wantRole = wantRole.substring(0, wantRole.length() - 1);
                    }

                }

                return true;
            }
        });

    }

    TagAdapter<TagBean> tagAdapter = new TagAdapter<TagBean>(tagRoleBeans) {

        @Override
        public View getView(FlowLayout parent, int position, TagBean tag) {

            TextView textView = (TextView) LayoutInflater.from(getContext()).inflate(R.layout.item_role_tag,
                    self_identity_tag, false);

            textView.setText(tag.title);

            int textViewMinWidth = SizeUtils.dip2px(TheLApp.context, 30);

            int marginRightWidth = SizeUtils.dip2px(TheLApp.context, 10);

            int marginTopWidth = SizeUtils.dip2px(TheLApp.context, 10);

            int textWidth = (int) getTextWidth(tag.title, 12) + textViewMinWidth;

            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(textWidth, textViewMinWidth);
            params.gravity = Gravity.CENTER;
            params.setMargins(0, marginTopWidth, marginRightWidth, 0);
            textView.setLayoutParams(params);

            return textView;
        }
    };

    /**
     * 星座选项
     */
    private void initConstellation() {

        constellation_tag.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (horoscope.isEmpty()) {
                    constellation_tag.performItemClick(0);
                } else {
                    String[] horoscopes = horoscope.split(",");
                    for (int i = 0; i < horoscopes.length; i++) {
                        constellation_tag.performItemClick(Integer.parseInt(horoscopes[i]) + 1);
                    }
                }
            }
        }, 300);

        final LayoutInflater mInflater = LayoutInflater.from(getContext());

        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) constellation_tag.getLayoutParams();
        layoutParams.width = ((int) getTextWidth(horoscopeBeans.get(0).title, 12) + SizeUtils.dip2px(TheLApp.context, 40)) * 5;
        constellation_tag.setLayoutParams(layoutParams);

        constellation_tag.setAdapter(new TagAdapter<TagBean>(horoscopeBeans) {

            @Override
            public View getView(FlowLayout parent, int position, TagBean tag) {

                TextView textView = (TextView) mInflater.inflate(R.layout.item_role_tag,
                        constellation_tag, false);

                textView.setText(tag.title);

                int textViewMinWidth = SizeUtils.dip2px(TheLApp.context, 30);

                int marginRightWidth = SizeUtils.dip2px(TheLApp.context, 10);

                int marginTopWidth = SizeUtils.dip2px(TheLApp.context, 10);

                int textWidth = (int) getTextWidth(tag.title, 12) + textViewMinWidth;

                FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(textWidth, textViewMinWidth);
                params.gravity = Gravity.CENTER;
                params.setMargins(0, marginTopWidth, marginRightWidth, 0);
                textView.setLayoutParams(params);

                return textView;
            }
        });

        constellation_tag.setOnTagClickListener(new TagFlowLayout.OnTagClickListener() {
            @Override
            public boolean onTagClick(View view, int position, FlowLayout parent, boolean performClick) {

                if (UserUtils.getUserVipLevel() == 0 && !performClick) {
                    gotoVipActivity();
                    return false;
                }

                TagBean tagBean = horoscopeBeans.get(position);

                tagBean.isSelected = !tagBean.isSelected;

                constellation_tag.toggle2(view, position, parent);

                if (position == 0) {
                    for (int i = 0; i < horoscopeBeans.size(); i++) {
                        TagBean tb = horoscopeBeans.get(i);

                        tb.isSelected = i == 0;
                    }
                    horoscope = "";
                } else {
                    StringBuilder stringBuffer = new StringBuilder();

                    for (int i = 1; i < horoscopeBeans.size(); i++) {
                        TagBean tb = horoscopeBeans.get(i);
                        if (tb.isSelected) {
                            stringBuffer.append(tb.id);
                            if (i < horoscopeBeans.size() - 1) {
                                stringBuffer.append(",");
                            }
                        }
                    }

                    horoscope = stringBuffer.toString();

                    if (horoscope.endsWith(",")) {
                        horoscope = horoscope.substring(0, horoscope.length() - 1);
                    }

                }

                L.d("NearbyFilterFragment", " horoscope : " + horoscope);

                return true;
            }
        });
    }

    private void setListener() {

        age_seekBar.setOnRangeChangedListener(new RangeSeekBar.OnRangeChangedListener() {
            @Override
            public void onRangeChanged(int lowerRange, int upperRange) {
                String text;
                if (upperRange == 40) {
                    text = lowerRange + " - " + upperRange + TheLApp.context.getResources().getString(R.string.updatauserinfo_activity_age_unit);
                } else {
                    text = lowerRange + " - " + upperRange + TheLApp.context.getResources().getString(R.string.updatauserinfo_activity_age_unit);
                }
                txt_age.setText(text);
                age = lowerRange + "," + (upperRange >= 40 ? "40+" : String.valueOf(upperRange));
            }
        });

        start_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onFilterListener != null) {
                    onFilterListener.onFilter(wantRole, age, affection, active, vip, have_photo, horoscope);
                }

            }
        });

        look_vip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                traceNearbyUserLog("info");
                gotoVipActivity();
            }
        });

        active_rl.setOnClickListener(this);
        active_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                active = isChecked ? "1" : "0";
            }
        });

        vip_rl.setOnClickListener(this);
        vip_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                vip = isChecked ? "1" : "0";
            }
        });

        photo_rl.setOnClickListener(this);
        photo_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                have_photo = isChecked ? "1" : "0";
            }
        });

        constellation_ll.setOnClickListener(this);

        root_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onFilterListener != null) {
                    onFilterListener.onDismiss();
                }
            }
        });

    }

    private float getTextWidth(String text, int textSize) {
        TextPaint paint = new TextPaint();
        float scaledDensity = TheLApp.getContext().getResources().getDisplayMetrics().scaledDensity;
        paint.setTextSize(scaledDensity * textSize);
        return paint.measureText(text);
    }

    @Override
    public void onClick(View v) {
        if (UserUtils.getUserVipLevel() == 0) gotoVipActivity();

        switch (v.getId()) {

            case R.id.active_rl:
                traceNearbyUserLog("active");

                break;
            case R.id.vip_rl:
                traceNearbyUserLog("vipuser");

                break;
            case R.id.photo_rl:
                traceNearbyUserLog("withphotos");

                break;
            case R.id.constellation_ll:
                traceNearbyUserLog("horoscope");

                break;

        }
    }


    public class TagBean {
        public String title;
        public String id;
        public boolean isSelected;
    }


    private void initRoleMap() {

        String[] roleName = TheLApp.context.getResources().getStringArray(R.array.filter_identity_array);

        String[] roleNameId = {"0", "1", "2", "3", "4"};

        for (int i = 0; i < roleName.length; i++) {
            TagBean tagBean = new TagBean();
            tagBean.id = roleNameId[i];
            tagBean.title = roleName[i];
            tagBean.isSelected = i == 0;
            tagRoleBeans.add(tagBean);
        }

    }

    private void initHoroscopeBeans() {

        String[] horoscopeName = getResources().getStringArray(R.array.userinfo_constellation2);

        String[] horoscopeNameId = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"};

        for (int i = 0; i < horoscopeName.length; i++) {
            TagBean tagBean = new TagBean();
            tagBean.id = horoscopeNameId[i];
            tagBean.title = horoscopeName[i];
            tagBean.isSelected = false;
            horoscopeBeans.add(tagBean);
        }

        TagBean tagBean = new TagBean();
        tagBean.isSelected = true;
        tagBean.id = "";
        tagBean.title = getString(R.string.txt_all);

        horoscopeBeans.add(0, tagBean);

    }

    private void gotoVipActivity() {
        Intent intent = new Intent(getContext(), VipConfigActivity.class);
        intent.putExtra("fromPage", "nearby");
        intent.putExtra("fromPageId", pageId);

        intent.putExtra("showType", VipConfigActivity.SHOW_NEARBY_FILTER);
        startActivity(intent);
    }

    private OnFilterListener onFilterListener;

    public void setOnFilterListener(OnFilterListener onFilterListener) {
        this.onFilterListener = onFilterListener;
    }

    public interface OnFilterListener {
        void onFilter(String wantRole, String age, String affection, String active, String vip, String have_photo, String horoscope);

        void onDismiss();
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (enter) {
            return AnimationUtils.loadAnimation(getActivity(), R.anim.top_in);
        } else {
            root_rl.setBackgroundColor(TheLApp.context.getResources().getColor(R.color.transparent));
            return AnimationUtils.loadAnimation(getActivity(), R.anim.top_out);
        }
    }

    public void traceNearbyUserLog(String activity) {
        try {

            LogInfoBean logInfoBean = new LogInfoBean();
            logInfoBean.page = "filter.list";
            logInfoBean.page_id = pageId;
            logInfoBean.activity = activity;

            MatchLogUtils.getInstance().addLog(logInfoBean);

        } catch (Exception e) {

        }
    }

}
