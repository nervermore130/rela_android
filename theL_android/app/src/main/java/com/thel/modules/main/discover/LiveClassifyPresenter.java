package com.thel.modules.main.discover;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.thel.app.TheLApp;
import com.thel.bean.AdBean;
import com.thel.modules.live.bean.LiveRoomsNetBean;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.service.DefaultRequestService;
import com.thel.utils.L;
import com.thel.utils.LocationUtils;

import org.reactivestreams.Subscription;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * 直播分类
 * Created by lingwei on 2017/11/22.
 */

public class LiveClassifyPresenter implements LiveClassifyRoomContact.Presenter {
    private final LiveClassifyRoomContact.View view;

    public static final String TYPE_HOT = "hot";
    public static final int PAGE_SIZE = 20;


    public LiveClassifyPresenter(LiveClassifyRoomContact.View view) {
        this.view = view;
        view.setPresenter(this);
    }

    /**
     * 获取热门直播列表
     * <p>
     * 刷新为默认cursor为0
     */
    @Override
    public void getRefreshHotRoomListData() {
        getHotRoomListData(0, TYPE_HOT);
    }

    /**
     * 获取更多热门直播列表
     *
     * @param cursor cursor
     */
    @Override
    public void getMoreHotRoomListData(int cursor) {
        getHotRoomListData(cursor, TYPE_HOT);
    }

    /**
     * 获取热门直播列表
     *
     * @param cursor
     */
    private void getHotRoomListData(final int cursor, final String type) {
        final Flowable<LiveRoomsNetBean> flowable = DefaultRequestService.createLiveRequestService().getLiveRoomsListBean(cursor + "", type);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<LiveRoomsNetBean>() {
            @Override
            public void onNext(LiveRoomsNetBean data) {
                super.onNext(data);
                if (data != null && data.data != null) {
                    if (cursor == 0) {
                        view.showRefreshHotRoomListData(data.data);
                        view.emptyData(data.data.list.size() == 0);
                    } else {
                        view.showMoreHotRoomListData(data.data);
                    }
                }
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                if (cursor > 0) {
                    view.loadMoreFailed();
                }
            }

            @Override
            public void onComplete() {
                super.onComplete();
                view.requestFinish();
            }
        });
    }

    /**
     * 获取 刷新 正在直播的新人列表
     * cursor默认为0
     */
    @Override
    public void getRefreshNewsLiveListData() {
        getNewsLiveListData(0);
    }

    /**
     * 获取更多正在直播的新人列表
     *
     * @param cursor cursor
     */
    @Override
    public void getMoreNewsLiveListData(int cursor) {
        getNewsLiveListData(cursor);
    }

    /**
     * 获取新人主播列表
     *
     * @param cursor
     */
    private void getNewsLiveListData(final int cursor) {
        final Flowable<LiveRoomsNetBean> flowable = DefaultRequestService.createDiscoveryRequestService().getNewAnchorLiveList(cursor, PAGE_SIZE);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<LiveRoomsNetBean>() {
            @Override
            public void onNext(LiveRoomsNetBean data) {
                super.onNext(data);
                if (data != null && data.data != null) {
                    if (cursor == 0) {
                        view.showRefreshNewsLiveListData(data.data);
                    } else {
                        view.showMoreNewsLiveListData(data.data);
                    }
                }
            }

            @Override
            public void onComplete() {
                super.onComplete();
                view.requestFinish();
            }
        });
    }

    /**
     * 获取刷新 推荐的新人列表
     * 默认cursor为0
     */
    @Override
    public void getRefreshRecommedNewAnchorsData() {
        getRecommendNetAnchorsData(0);
    }

    /**
     * 获取更多推荐的新人列表
     *
     * @param cursor cursor
     */
    @Override
    public void getMoreRecommedNewAnchorsData(int cursor) {
        getRecommendNetAnchorsData(cursor);

    }

    private void getRecommendNetAnchorsData(final int cursor) {
        final Flowable<LiveRoomsNetBean> flowable = DefaultRequestService.createDiscoveryRequestService().getRecommendNewFollowAnchorsList(cursor, PAGE_SIZE);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<LiveRoomsNetBean>() {
            @Override
            public void onNext(LiveRoomsNetBean data) {
                super.onNext(data);
                if (data != null && data.data != null) {
                    if (cursor == 0) {
                        view.showRefreshRecommandNewAnchorsData(data.data);
                    } else {
                        view.showMoreRecommandNewAnchorsData(data.data);
                    }
                }
            }

            @Override
            public void onComplete() {
                super.onComplete();
                view.requestFinish();
            }
        });
    }

    /**
     * 获取附近直播列表 刷新
     * 默认curosr 为0
     */
    @Override
    public void getRefreshNeighboursListData() {

        final AMapLocationClient mLocationClient = new AMapLocationClient(TheLApp.getContext());
        mLocationClient.setLocationListener(new AMapLocationListener() {
            @Override
            public void onLocationChanged(AMapLocation aMapLocation) {

                if (aMapLocation != null && aMapLocation.getErrorCode() == 0) {

                    if (aMapLocation.getLatitude() > 0 && aMapLocation.getLongitude() > 0) {
                        LocationUtils.saveLocation(aMapLocation.getLatitude(), aMapLocation.getLongitude(), aMapLocation.getCity());
                    } else {
                        LocationUtils.saveLocation(0.0, 0.0, "");
                    }

                } else {

                }

                mLocationClient.stopLocation();
                mLocationClient.onDestroy();

                getNeighboursListData(0);


            }

        });
        final AMapLocationClientOption mLocationOption = new AMapLocationClientOption();
        mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);
        mLocationClient.setLocationOption(mLocationOption);
        mLocationOption.setOnceLocation(true);
        mLocationClient.startLocation();

    }

    /**
     * 获取更多附近直播列表
     *
     * @param cursor cursor
     */
    @Override
    public void getMoreNeighboursListData(int cursor) {
        getNeighboursListData(cursor);
    }

    /**
     * 获取音频直播列表 刷新
     * 默认curosr 为0
     */
    @Override
    public void getRefreshAudioListData() {
//        getAudioListData(0);
    }

    /**
     * 获取电台列表
     */
    @Override
    public void getRefreshRadioListData() {
        getAudioListData(0, 0);
    }

    /**
     * 获取热聊
     */
    @Override
    public void getRefreshHotChatListData() {
        getAudioListData(0, 1);
    }

    /**
     * 获取更多音频直播列表
     *
     * @param cursor cursor
     */
    @Override
    public void getMoreAudioListData(int cursor) {
//        getAudioListData(cursor);
    }

    @Override
    public void getRefreshVedioListData() {
        getVedioListData(0);
    }

    @Override
    public void getMoreVedioListData(int cursor) {
        getVedioListData(cursor);

    }

    @Override
    public void getMoreRadioListData(int cursor) {
        getAudioListData(cursor, 0);
    }

    @Override
    public void getMoreHotChatListData(int cursor) {
        getAudioListData(cursor, 1);
    }


    /**
     * 获取附近直播列表
     *
     * @param cursor
     */
    private void getNeighboursListData(final int cursor) {
        final Flowable<LiveRoomsNetBean> flowable = DefaultRequestService.createDiscoveryRequestService().getNeighboursListListData(cursor, PAGE_SIZE);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<LiveRoomsNetBean>() {
            @Override
            public void onNext(LiveRoomsNetBean data) {
                super.onNext(data);
                if (data != null && data.data != null) {
                    if (cursor == 0) {
                        view.showRefreshNeighboursListData(data.data);
                    } else {
                        view.showMoreNeighboursListData(data.data);
                    }
                }
            }

            @Override
            public void onComplete() {
                super.onComplete();
                view.requestFinish();
            }
        });
    }

    /**
     * 获取音频直播列表
     *
     * @param cursor
     * @param isMulti 0电台1热聊
     */
    private void getAudioListData(final int cursor, int isMulti) {

        DefaultRequestService.createDiscoveryRequestService().getLiveAudioListBean(cursor, PAGE_SIZE, isMulti).onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<LiveRoomsNetBean>() {
            @Override
            public void onNext(LiveRoomsNetBean data) {
                super.onNext(data);
                if (data != null && data.data != null) {
                    if (cursor == 0) {
                        view.showRefreshAudioListData(data.data);
                    } else {
                        view.showMoreAudioListData(data.data);
                    }
                }
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                if (cursor > 0) {
                    view.loadMoreFailed();
                }
            }

            @Override
            public void onComplete() {
                super.onComplete();
                view.requestFinish();

            }
        });
    }

    /**
     * 4.9.1
     * 获取视频直播列表
     *
     * @param cursor
     */
    private void getVedioListData(final int cursor) {

        DefaultRequestService.createDiscoveryRequestService().getLiveVideoListBean(cursor, PAGE_SIZE).onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<LiveRoomsNetBean>() {
            @Override
            public void onNext(LiveRoomsNetBean data) {
                super.onNext(data);
                if (data != null && data.data != null) {
                    if (cursor == 0) {

                        view.showRefreshVedioListData(data.data);
                    } else {
                        view.showMoreVedioListData(data.data);
                    }
                }
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                if (cursor > 0) {
                    view.loadMoreFailed();
                }
            }

            @Override
            public void onComplete() {
                super.onComplete();
                view.requestFinish();

            }
        });
    }


    /**
     * 根据id 获取直播列表，刷新的列表
     * 默认cursor 为0
     *
     * @param id logType
     */
    @Override
    public void getRefreshLiveListWithTypeData(int id) {
        getLiveListWithTypeData(id, 0);
    }

    /**
     * 根据类型获取更更多直播
     *
     * @param id     logType
     * @param cursor
     */
    @Override
    public void getMoreLiveListWithTypeData(int id, int cursor) {
        getLiveListWithTypeData(id, cursor);

    }

    /**
     * 按类型获取直播
     *
     * @param id
     * @param cursor
     */
    private void getLiveListWithTypeData(int id, final int cursor) {
        final Flowable<LiveRoomsNetBean> flowable = DefaultRequestService.createDiscoveryRequestService().getLiveListWithTypeData(cursor, PAGE_SIZE, id);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<LiveRoomsNetBean>() {
            @Override
            public void onNext(LiveRoomsNetBean data) {
                super.onNext(data);
                if (data != null && data.data != null) {
                    if (cursor == 0) {
                        view.showRefreshLiveListWithTypeData(data.data);
                    } else {
                        view.showMoreLiveListWithTypeData(data.data);
                    }
                }
            }

            @Override
            public void onComplete() {
                super.onComplete();
                view.requestFinish();
            }
        });
    }

    /**
     * 获取 刷新  推荐相关类型的主播列表
     *
     * @param id id
     */
    @Override
    public void getRefreshRecommendFollowListTypeData(int id) {
        getRecommendFollowListTypeData(id, 0);
    }

    /**
     * 获取更多推荐关注相应类型的主播列表
     *
     * @param id     logType
     * @param cursor cursor
     */
    @Override
    public void getMoreRecommendFollowListTypeData(int id, int cursor) {
        getRecommendFollowListTypeData(id, cursor);
    }


    @Override
    public void loadAdvert() {
        Flowable<AdBean> flowable = RequestBusiness.getInstance().getAd();
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<AdBean>() {

            @Override
            public void onNext(AdBean adBean) {
                super.onNext(adBean);
                L.d("LIVECLASS_DATA", "ONNEXT");

                view.refreshAdArea(adBean);
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                L.d("LIVECLASS_DATA", "onError" + t.getMessage());
            }

            @Override
            public void onComplete() {
                //  vipView.closeLoading;

            }
        });
    }

    /**
     * 按类型获取推荐关注主播列表
     *
     * @param id
     * @param cursor
     */
    private void getRecommendFollowListTypeData(int id, final int cursor) {
        final Flowable<LiveRoomsNetBean> flowable = DefaultRequestService.createDiscoveryRequestService().getRecommendFollowListTypeData(cursor, PAGE_SIZE, id);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<LiveRoomsNetBean>() {
            @Override
            public void onNext(LiveRoomsNetBean data) {
                super.onNext(data);
                if (data != null && data.data != null) {
                    if (cursor == 0) {
                        view.showRefreshRecommendFollowListTypeData(data.data);
                    } else {
                        view.showMoreRecommendFollowListTypeData(data.data);
                    }
                }
            }

            @Override
            public void onComplete() {
                super.onComplete();
                view.requestFinish();
            }
        });
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {

    }
}
