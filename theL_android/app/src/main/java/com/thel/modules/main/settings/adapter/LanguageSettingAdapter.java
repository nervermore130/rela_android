package com.thel.modules.main.settings.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.thel.R;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;

import java.util.List;

/**
 * Created by lingwei on 2017/10/30.
 */

public class LanguageSettingAdapter extends BaseRecyclerViewAdapter<String> {

        private int mCheckedPostion;//当前语言位置
        private LanguageChangedListener languageChangedListener;//语言切换监听接口

        public LanguageSettingAdapter(Context context, List<String> data, int mCheckedPostion) {
            super(R.layout.language_setting_item, data);
            this.mCheckedPostion = mCheckedPostion;
        }

        @Override
        protected void convert(final BaseViewHolder helper, String item) {
            helper.setText(R.id.txt_name, item.trim());
            ImageView radioButton = helper.getView(R.id.radiobutton);
            if (helper.getAdapterPosition() == mCheckedPostion) {
                radioButton.setVisibility(View.VISIBLE);
            } else {
                radioButton.setVisibility(View.INVISIBLE);
            }
        }

        public void notifyDataSetChanged(int adapterPosition) {
            if (mCheckedPostion != adapterPosition) {
                mCheckedPostion = adapterPosition;
                if (languageChangedListener != null) {
                    languageChangedListener.languageChanged(mCheckedPostion);
                }
                notifyDataSetChanged();
            }
        }

        public interface LanguageChangedListener {
            void languageChanged(int position);
        }

        public void setLanguageChangedListener(LanguageChangedListener languageChangedListener) {
            this.languageChangedListener = languageChangedListener;
        }
}
