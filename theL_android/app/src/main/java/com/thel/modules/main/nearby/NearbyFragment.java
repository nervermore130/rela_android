package com.thel.modules.main.nearby;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseFragment;
import com.thel.imp.AutoRefreshImp;
import com.thel.imp.TittleClickListener;
import com.thel.modules.main.home.moments.ReleaseLiveMomentActivity;
import com.thel.modules.main.home.moments.web.WebViewActivity;
import com.thel.modules.main.nearby.nearbyChild.NearbyChildFragment;
import com.thel.modules.main.nearby.nearbyChild.NearbyChildNearbyFragment;
import com.thel.modules.main.nearby.visit.VisitActivity;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SharedPrefUtils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by liuyun on 2017/9/18.
 */

public class NearbyFragment extends BaseFragment implements NearbyContract.View, View.OnClickListener {

    private static NearbyFragment instance;
    private NearbyContract.Presenter presenter;
    private ImageView img_check;
    private TabLayout tablayout;
    private ImageView img_live_rank;
    private ViewPager viewpager;

    public static final String TAG = "tag";
    public static final String TAG_NEARBY = "nearby";
    public static final String TAG_HOT = "new";
    public static final String TAG_STAR = "hot";
    private Fragment nearbyChildFragment;
    private Fragment hotChildFragment;
    private Fragment starChildFragment;
    private List<Fragment> list = new ArrayList<>();
    private NearbyAdapter adapter;
    private List<String> titleList = new ArrayList();
    private LinearLayout lin_back;
    private FragmentTransaction fragmentTransaction;
    private TextView open_location;
    private RelativeLayout rel_default_gps;
    private int GPS_REQUEST_CODE = 10;

    public static NearbyFragment getInstance() {
        instance = new NearbyFragment();
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initData();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_nearby_1, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        findViewById(view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        new NearbyPresenter(this);
        setListener();
    }

    @Override
    public void setPresenter(NearbyContract.Presenter presenter) {
        this.presenter = presenter;
    }

    private void findViewById(View view) {
        //  img_check = (ImageView) view.findViewById(R.id.img_check);
        tablayout = view.findViewById(R.id.tablayout);
        //  img_live_rank = (ImageView) view.findViewById(R.id.img_live_rank);
        viewpager = view.findViewById(R.id.viewpager);
        //   lin_back = view.findViewById(R.id.lin_back);
        setTabs();
    }

    private void initData() {
        titleList.clear();
        titleList.add(getString(R.string.nearby_activity_nearby));
        titleList.add(getString(R.string.nearby_activity_new));
        titleList.add(getString(R.string.nearby_activity_hot));
    }

    private void setTabs() {
        addFragment();
        adapter = new NearbyAdapter(getChildFragmentManager(), list, titleList);
        viewpager.setAdapter(adapter);
        viewpager.setOffscreenPageLimit(3);
        final int size = titleList.size();
        for (int i = 0; i < size; i++) {
            final TabLayout.Tab tab = tablayout.newTab();
            setSingleLineTab(tab);
            tablayout.addTab(tab);
        }
        tablayout.setupWithViewPager(viewpager);
        /*final int leftMargin= SizeUtils.dip2px(TheLApp.getContext(),10);
        final int rightMargin= SizeUtils.dip2px(TheLApp.getContext(),10);
        //设置下划线等于字的长度
        TabLayoutUtils.makeLineLength(tablayout,leftMargin,rightMargin);*/


    }

    private void setSingleLineTab(TabLayout.Tab tab) {
        try {
            final Field tabviewField = TabLayout.Tab.class.getDeclaredField("mView");
            tabviewField.setAccessible(true);
            final Object tabviewObj = tabviewField.get(tab);
            final Class tabviewClass = Class.forName("android.support.design.widget.TabLayout$TabView");
            final Field mDefaultMaxLines = tabviewClass.getDeclaredField("mDefaultMaxLines");
            mDefaultMaxLines.setAccessible(true);
            mDefaultMaxLines.set(tabviewObj, 1);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void addFragment() {
        list.clear();
        nearbyChildFragment = initFragment(TAG_NEARBY);
        hotChildFragment = initFragment(TAG_HOT);
        starChildFragment = initFragment(TAG_STAR);
        Collections.addAll(list, nearbyChildFragment, hotChildFragment, starChildFragment);
    }

    private Fragment initFragment(String tag) {
        final Bundle bundle = new Bundle();
        bundle.putString(TAG, tag);
        NearbyChildFragment nearbyChildFragment;
        if (TAG_NEARBY.equals(tag)) {
            nearbyChildFragment = NearbyChildNearbyFragment.getInstance(bundle);
        } else {
            nearbyChildFragment = NearbyChildFragment.getInstance(bundle);
        }
        return nearbyChildFragment;
    }

    private void setListener() {
        //  img_check.setOnClickListener(this);
        //     img_live_rank.setOnClickListener(this);
        tablayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                final int position = tab.getPosition();
                if (list.size() > position) {
                    final Fragment fragment = list.get(position);
                    if (fragment instanceof TittleClickListener) {
                        ((TittleClickListener) fragment).onTitleClick();
                    }
                }

            }
        });
   /* //    lin_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });*/
    }

    @Override
    public void onClick(View v) {
        ViewUtils.preventViewMultipleClick(v, 1000);
        switch (v.getId()) {
           /* case R.id.img_check://点击后设为普通背景图片
                gotoVisitActivity();
                ShareFileUtils.setBoolean(ShareFileUtils.NEW_CHECK, false);
                updateNewVisitorUI();
*/
            // testAimi();
           /* case R.id.img_live_rank:
                gotoWebViewActivityForRank();
                //writeLiveMoment();
                break;*/
        }
    }

    private void writeLiveMoment() {
        final Intent intent = new Intent(getActivity(), ReleaseLiveMomentActivity.class);
        getActivity().startActivity(intent);
    }

    private void gotoWebViewActivityForRank() {
        // new QupaiVideoRecordActivity.Request(new VideoSessionClientFactoryImpl(), null).startForResult(getActivity(), TheLConstants.BUNDLE_CODE_RECORD_VIDEO);
        final Intent intent = new Intent(getActivity(), WebViewActivity.class);
        final String url = SharedPrefUtils.getString(SharedPrefUtils.FILE_LIVE, SharedPrefUtils.FILE_TOP_LINK, SharedPrefUtils.FILE_TOP_LINK);
        if (!TextUtils.isEmpty(url)) {
            final Bundle bundle = new Bundle();
            bundle.putString(WebViewActivity.URL, url);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }

    private void gotoVisitActivity() {
        if (getActivity() == null) {
            return;
        }
        MobclickAgent.onEvent(TheLApp.getContext(), "who_look_me_click");
        final Intent intent = new Intent(getActivity(), VisitActivity.class);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(this.getClass().getName());
        tryRefreshCurrentFragment();
       /* if (rel_default_gps != null && rel_default_gps.getVisibility() == View.VISIBLE) {
            if (PhoneUtils.isGpsOpen(getActivity())) {
                rel_default_gps.setVisibility(View.GONE);
            }else {
                rel_default_gps.setVisibility(View.VISIBLE);


            }
        }*/
        // registerReceiver();

    }

    /**
     * 尝试刷新当前的fragment
     */
    private void tryRefreshCurrentFragment() {
        final int currentTab = viewpager.getCurrentItem();
        if (list != null && list.size() > currentTab) {
            final Fragment fragment = list.get(currentTab);
            if (fragment instanceof AutoRefreshImp) {
                ((AutoRefreshImp) fragment).tryRefreshData();
            }
        }
    }

    private boolean hasNewVisitor() {
        return ShareFileUtils.getBoolean(ShareFileUtils.NEW_CHECK, false);
    }

    class NearbyAdapter extends FragmentStatePagerAdapter {

        private final List<Fragment> list;
        private final List<String> titliList;

        public NearbyAdapter(FragmentManager childFragmentManager, List<Fragment> list, List<String> titleList) {
            super(childFragmentManager);
            this.list = list;
            this.titliList = titleList;
        }

        @Override
        public Fragment getItem(int position) {
            return list.get(position);
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (titliList != null) {
                return titliList.get(position % titliList.size());
            }
            return "";
        }
    }

}
