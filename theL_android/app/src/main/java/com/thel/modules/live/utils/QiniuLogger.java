package com.thel.modules.live.utils;

import androidx.annotation.NonNull;
import android.util.Base64;

import com.thel.app.TheLApp;
import com.thel.bean.analytics.AnalyticsBean;
import com.thel.utils.DeviceUtils;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;
import com.thel.utils.PhoneUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;

import okhttp3.*;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class QiniuLogger {

    private static final String TAG = "QiniuLogger";

    private static final String repoUrl = "http://nb-pipeline.qiniuapi.com/v2/repos/rela_client_log_flowDataSource/data";
    private static QiniuLogger _singleton = new QiniuLogger();

    private OkHttpClient _client;
    private final SimpleDateFormat sdf =
            new SimpleDateFormat("EEE, dd MMM yyyy hh:mm:ss z", Locale.US);

    private QiniuLogger() {
        // Charles proxy
//        Proxy proxy = new Proxy(Proxy.Type.HTTP,
//                new InetSocketAddress("localhost", 8888));
//
//        OkHttpClient.Builder builder = new OkHttpClient.Builder().proxy(proxy);
//        _client = builder.build();

        _client = new OkHttpClient();

        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
    }

    public static QiniuLogger getInstance() {
        if (_singleton == null) _singleton = new QiniuLogger();
        return _singleton;
    }

    public void postLog(String rawString, Callback callback) {
        try {
            Request req = makeRequest(rawString);
            _client.newCall(req).enqueue(callback);

        } catch (NoSuchAlgorithmException | UnsupportedEncodingException | InvalidKeyException e) {
            e.printStackTrace();
            callback.onFailure(null, new IOException("Signing failed"));

        }
    }

    // MARK: - helpers
    private Request makeRequest(String rawString) throws NoSuchAlgorithmException, UnsupportedEncodingException, InvalidKeyException {
        MediaType s = MediaType.parse("");
        RequestBody body = RequestBody.create(s, rawString);
        Request request = new Request.Builder()
                .url(repoUrl)
                .headers(makeHeaders())
                .post(body)
                .build();

        return request;
    }

    private Headers makeHeaders() throws NoSuchAlgorithmException, UnsupportedEncodingException, InvalidKeyException {
        String date = gmtNow();
        String[] datas = new String[]{};
        try {
            datas = date.split("\\+");
        } catch (Exception e) {

        }

        String authorization = null;

        if (datas == null) {
            authorization = sign("POST\n\n\n" + datas + "\n/v2/repos/rela_client_log_flowDataSource/data");

        } else {
            authorization = sign("POST\n\n\n" + datas[0] + "\n/v2/repos/rela_client_log_flowDataSource/data");

        }
        Headers headers = new Headers.Builder()
                .add("date", datas[0])
                .add("authorization", authorization)
                .build();
        return headers;


    }

    private String sign(String message) throws NoSuchAlgorithmException, UnsupportedEncodingException, InvalidKeyException {
        byte[] keyBytes = "BYmCTId36H0qO5hmmRnwgBnZL55nUWKULE0_U0N2".getBytes(StandardCharsets.UTF_8);
        L.d(TAG, "message : " + message);

        SecretKey sk = new SecretKeySpec(keyBytes, "HmacSHA1");
        Mac mac = Mac.getInstance("HmacSHA1");
        mac.init(sk);
        byte[] result = mac.doFinal(message.getBytes(StandardCharsets.UTF_8));

        String base64String = Base64.encodeToString(result, Base64.NO_WRAP);
        base64String = base64String.replace('/', '_').replace('+', '-');

        L.d(TAG, " base64String : " + base64String);

        return "Pandora 321Ybrcdy461NOx5nvwF28I9QA_3NLdVCM22xhoQ:" + base64String;
    }

    private String gmtNow() {
        return sdf.format(new Date());
    }

    // MARK: test

    public static void main(AnalyticsBean args) {
        String log = GsonUtils.createJsonString(args);

        L.d(TAG, " log : " + log);

        QiniuLogger ql = QiniuLogger.getInstance();

        // String rawData = "userId=Android test 2\tuserKey=key\tbody=[]\tos=Java\tdeviceId=deviceId";
        String rawData = "logType=" + args.logType + "\t" + "userId=" + Utils.getMyUserId() + "\t" + "userKey=" + UserUtils.getUserKey() + "\t" + "os=" + "Android " + PhoneUtils.getOSVersion() + "\t" + "clientVersion=" + DeviceUtils.getVersionCode(TheLApp.getContext()) + "" + "\t" + "body=" + log;
        L.d(TAG, "rawData : " + rawData);

        ql.postLog(rawData, new Callback() {
            @Override
            public void onFailure(@NonNull Call call, IOException e) {
                L.d(TAG, "failure : " + e.getMessage());
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                L.d(TAG, "response : " + response.message());

            }
        });
    }

}
