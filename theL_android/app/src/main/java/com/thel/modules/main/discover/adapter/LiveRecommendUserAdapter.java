package com.thel.modules.main.discover.adapter;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.view.SimpleDraweeView;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.modules.live.bean.LiveRoomBean;
import com.thel.modules.main.discover.LiveClassifyRoomContact;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;
import com.thel.utils.AppInit;

import java.util.List;

/**
 * 推荐主播列表
 * Created by lingwei on 2017/11/21.
 */

public class LiveRecommendUserAdapter extends BaseRecyclerViewAdapter<LiveRoomBean> {
    private final float radius;
    private final float padding;
    private int picSize;
    public LiveFollowLintener liveFollowLinstenr;

    public LiveRecommendUserAdapter(List<LiveRoomBean> data, LiveClassifyRoomContact.Presenter presenter) {
        super(R.layout.recommend_live_item_view, data);
        radius = TheLApp.getContext().getResources().getDimension(R.dimen.live_new_user_item_radius);
        padding = TheLApp.getContext().getResources().getDimension(R.dimen.live_new_user_item_padding);
        picSize = (int) ((AppInit.displayMetrics.widthPixels - 3 * padding) / 2);
    }

    @Override
    protected void convert(BaseViewHolder holdView, LiveRoomBean liveRoomBean) {
        final LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) holdView.getView(R.id.rel_preview).getLayoutParams();
        params.width = picSize;
        params.height = picSize;
        final SimpleDraweeView preview = holdView.getView(R.id.preview);
       // preview.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadii(radius, radius, 0, 0).setRoundingMethod(RoundingParams.RoundingMethod.OVERLAY_COLOR).setOverlayColor(ContextCompat.getColor(TheLApp.getContext(), R.color.bg_color)));
        preview.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadius(radius));

        // 头像和昵称
        // holdView.setImageUrl(R.id.avatar, liveRoomBean.user.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE);
        holdView.setText(R.id.nickname, liveRoomBean.user.nickName);
       /* holdView.setOnClickListener(R.id.avatar, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(TheLApp.getContext(), UserInfoActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                //  intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, liveRoomBean.user.id + "");
                TheLApp.getContext().startActivity(intent);
            }
        });*/
        // 距离
        TextView txt_distance = holdView.convertView.findViewById(R.id.txt_distance);

        txt_distance.getPaint().setFakeBoldText(true);
        try {
            if (liveRoomBean.distance != null && (liveRoomBean.distance.contains(" Km") || liveRoomBean.distance.contains(" km"))) {
                float dis = -1f;
                if (liveRoomBean.distance.contains(" Km")) {
                    dis = Float.valueOf(liveRoomBean.distance.substring(0, liveRoomBean.distance.indexOf(" Km")));
                } else if (liveRoomBean.distance.contains(" km")) {
                    dis = Float.valueOf(liveRoomBean.distance.substring(0, liveRoomBean.distance.indexOf(" km")));
                }
                if (dis <= 5) {
                    if (dis == -1f) {
                        holdView.setText(R.id.txt_distance, liveRoomBean.distance);
                    } else {
                        holdView.setText(R.id.txt_distance, liveRoomBean.distance);
                    }
                } else {
                    holdView.setText(R.id.txt_distance, liveRoomBean.distance);
                }
            } else {
                holdView.setText(R.id.txt_distance, liveRoomBean.distance);
            }
        } catch (Exception e) {
            holdView.setText(R.id.txt_distance, liveRoomBean.distance);
        }
        // holdView.setText(R.id.recommend_fans, liveRoomBean.fansNum);
        holdView.setText(R.id.recommend_fans, liveRoomBean.user.fansNum + "");
        // 预览图
        holdView.setImageUrl(R.id.preview, liveRoomBean.imageUrl, TheLApp.getContext().getResources().getDimension(R.dimen.moment_pic_thumbnail_big), TheLApp.getContext().getResources().getDimension(R.dimen.moment_pic_thumbnail_big));
      /*  // 直播描述
        holdView.setText(R.id.text, liveRoomBean.text);
        // 前三标志
        holdView.setVisibility(R.id.badge, View.GONE);
        if (!TextUtils.isEmpty(liveRoomBean.badge)) {
            holdView.setVisibility(R.id.badge, View.VISIBLE);
            holdView.setImageUrl(R.id.badge, liveRoomBean.badge);
        }*/
        //2.20新增， 主播推荐标签
        /*if (TextUtils.isEmpty(liveRoomBean.label)) {
            holdView.setVisibility(R.id.txt_label, View.GONE);
            ((TextView) holdView.getView(R.id.text)).setMaxLines(3);
        } else {
            holdView.setVisibility(R.id.txt_label, View.VISIBLE);
            holdView.setText(R.id.txt_label, liveRoomBean.label + "");
            ((TextView) holdView.getView(R.id.text)).setMaxLines(1);
        }*/
        holdView.setOnClickListener(R.id.tv_follow, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (liveFollowLinstenr != null) {
                    liveFollowLinstenr.setOnclickItemFollow((TextView) v);
                }
            }
        });

    }

    public interface LiveFollowLintener {
        void setOnclickItemFollow(TextView view);
    }

    public void setLiveFollowLinstenr(LiveFollowLintener liveFollowLinstenr) {
        this.liveFollowLinstenr = liveFollowLinstenr;
    }
}
