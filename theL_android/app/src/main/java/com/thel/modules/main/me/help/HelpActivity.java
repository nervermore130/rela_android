package com.thel.modules.main.me.help;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.reflect.TypeToken;
import com.netease.LDNetDiagnoService.LDNetSocket;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.manager.CDNBalanceManager;
import com.thel.modules.live.utils.NetSpeedTestUtils;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;
import com.thel.utils.NetworkUtils;
import com.thel.utils.ShareFileUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HelpActivity extends BaseActivity {

    @BindView(R.id.txt_title)
    TextView txt_title;

    @BindView(R.id.icon_iv)
    ImageView icon_iv;

    @BindView(R.id.network_title_tv)
    TextView network_title_tv;

    @BindView(R.id.check_network_tv)
    TextView check_network_tv;

    @BindView(R.id.content_tv)
    TextView content_tv;

    @BindView(R.id.no_open_network_tips_ll)
    LinearLayout no_open_network_tips_ll;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        ButterKnife.bind(this);

        txt_title.setText("帮助");

        if (!NetworkUtils.isNetworkConnected(TheLApp.context)) {
            network_title_tv.setText("未启用移动网络或WIFI");
            no_open_network_tips_ll.setVisibility(View.VISIBLE);
            check_network_tv.setVisibility(View.GONE);
            content_tv.setVisibility(View.GONE);
            icon_iv.setImageResource(R.mipmap.icon_network_on_connect);
        }

    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    @OnClick(R.id.check_network_tv)
    void onNetworkCheck() {

        List<NetSpeedTestUtils.NetSpeedTestBean> ipList = new ArrayList<>();

        NetSpeedTestUtils netSpeedTestUtils = new NetSpeedTestUtils();

        netSpeedTestUtils.setOnNetTestListener(mOnNetTestListener);

        String ip = ShareFileUtils.getString(ShareFileUtils.CHAT_IP, null);

        if (ip != null) {

            Type typeOfDest = new TypeToken<List<String>>() {
            }.getType();

            List<String> imiPList = GsonUtils.getObjects(ip, typeOfDest);

            if (imiPList != null && imiPList.size() > 0) {
                for (int i = 0; i < imiPList.size(); i++) {

                    NetSpeedTestUtils.NetSpeedTestBean netSpeedTestBean = new NetSpeedTestUtils.NetSpeedTestBean();

                    netSpeedTestBean.ip = imiPList.get(i);

                    netSpeedTestBean.type = "im";

                    ipList.add(netSpeedTestBean);
                }
            }

        }

        String fastPlayIP = CDNBalanceManager.getInstance().getFastPlayIp();

        if (fastPlayIP != null) {

            NetSpeedTestUtils.NetSpeedTestBean netSpeedTestBean = new NetSpeedTestUtils.NetSpeedTestBean();

            netSpeedTestBean.ip = fastPlayIP;

            netSpeedTestBean.type = "livePush";

            ipList.add(netSpeedTestBean);

        }

        String fastRtmpIP = CDNBalanceManager.getInstance().getFastRtmpIp();

        if (fastRtmpIP != null) {

            NetSpeedTestUtils.NetSpeedTestBean netSpeedTestBean = new NetSpeedTestUtils.NetSpeedTestBean();

            netSpeedTestBean.ip = fastRtmpIP;

            netSpeedTestBean.port = LDNetSocket.PORT;

            netSpeedTestBean.type = "livePlay";

            ipList.add(netSpeedTestBean);

        }

        NetSpeedTestUtils.NetSpeedTestBean netSpeedTestBean = new NetSpeedTestUtils.NetSpeedTestBean();

        netSpeedTestBean.ip = "api.rela.me";

        netSpeedTestBean.port = 2345;

        netSpeedTestBean.type = "api";

        ipList.add(netSpeedTestBean);

        if (ipList.size() > 0) {

            netSpeedTestUtils.setRequestCount(ipList.size());

            for (int i = 0; i < ipList.size(); i++) {
                NetSpeedTestUtils.NetSpeedTestBean nstb = ipList.get(i);
                netSpeedTestUtils.testIp(nstb.ip, nstb.port, nstb.type);
            }
        }

        network_title_tv.setText("正在检查网络(0%)...");

        content_tv.setVisibility(View.VISIBLE);

        content_tv.setText("检查网络需要一些时间，请耐心等待");

        check_network_tv.setVisibility(View.GONE);

    }

    @OnClick(R.id.lin_back)
    void onBackClicked() {
        this.finish();
    }

    private NetSpeedTestUtils.OnNetTestListener mOnNetTestListener = new NetSpeedTestUtils.OnNetTestListener() {

        @Override public void onNetTest(final int percent) {
            runOnUiThread(new Runnable() {
                @Override public void run() {

                    L.d("HelpActivity", " percent : " + percent);

                    String content = "正在检查网络(" + percent + "%)...";
                    network_title_tv.setText(content);

                    if (percent == 100) {
                        network_title_tv.setText("网络正常");
                        content_tv.setText("目前网络流畅,可以正常使用");
                        icon_iv.setImageResource(R.mipmap.icon_network_ok);
                    }
                }
            });
        }
    };

}
