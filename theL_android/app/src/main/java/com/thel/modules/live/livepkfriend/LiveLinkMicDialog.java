package com.thel.modules.live.livepkfriend;

import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseDialogFragment;
import com.thel.constants.TheLConstants;
import com.thel.modules.live.bean.LivePkFriendBean;
import com.thel.modules.live.in.LivePkContract;
import com.thel.modules.live.surface.show.LiveShowBaseViewFragment;
import com.thel.ui.widget.IndicatorDrawable;
import com.thel.utils.ScreenUtils;
import com.thel.utils.SizeUtils;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LiveLinkMicDialog extends BaseDialogFragment {

    @BindView(R.id.viewPager)
    ViewPager viewPager;

    @BindView(R.id.tablayout)
    TabLayout tablayout;

    private LiveLinkMicAudienceListFragment mAudienceFragment;

    private LiveLinkMicFriendListFragment mFriendFragment;

    private List<Fragment> fragmentList = new ArrayList<>();

    private String userId = "";

    private List<LivePkFriendBean> linkMicData;

    private LivePkContract.FriendRequestClickListener mFriendRequestClickListener;

    private LiveLinkMicAudienceListFragment.LinkMicAudienceListener micAudienceListener;

    private int linkMicStatus = LiveShowBaseViewFragment.LINK_MIC_STATUS_NONE;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_live_link_mic, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initFragment();

        initTab();
    }

    public void setLinkMicData(List<LivePkFriendBean> linkMicData) {
        this.linkMicData = linkMicData;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setLinkMicStatus(int linkMicStatus) {
        this.linkMicStatus = linkMicStatus;
        if (mAudienceFragment != null)
            mAudienceFragment.setLinkMicStatus(linkMicStatus);
        if (mFriendFragment != null)
            mFriendFragment.setLinkMicStatus(linkMicStatus);
    }

    private void initTab() {
        String[] titles = new String[]{TheLApp.getContext().getString(R.string.andience_beam), TheLApp.getContext().getString(R.string.beam_with_friend)};

        viewPager.setAdapter(new FragmentStatePagerAdapter(getChildFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return fragmentList.get(position);
            }

            @Override
            public int getCount() {
                return fragmentList.size();
            }

            @Nullable
            @Override
            public CharSequence getPageTitle(int position) {
                return titles[position];
            }
        });

        tablayout.setupWithViewPager(viewPager);

        for (int i = 0; i < titles.length; i++) {
            setSingleLineTab(tablayout.getTabAt(i));
        }
        View tabStripView = tablayout.getChildAt(0);
        tabStripView.setBackground(new IndicatorDrawable(tabStripView, ContextCompat.getColor(TheLApp.context, R.color.rela_color)));
    }

    private void setSingleLineTab(TabLayout.Tab tab) {
        try {
            final Field tabviewField = TabLayout.Tab.class.getDeclaredField("mView");
            tabviewField.setAccessible(true);
            final Object tabviewObj = tabviewField.get(tab);
            final Class tabviewClass = Class.forName("android.support.design.widget.TabLayout$TabView");
            final Field mDefaultMaxLines = tabviewClass.getDeclaredField("mDefaultMaxLines");
            mDefaultMaxLines.setAccessible(true);
            mDefaultMaxLines.set(tabviewObj, 1);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void initFragment() {

        mAudienceFragment = new LiveLinkMicAudienceListFragment();

        Bundle bundle = new Bundle();
        bundle.putString(TheLConstants.BUNDLE_KEY_USER_ID, userId);
        bundle.putSerializable("linkMicData", (Serializable) linkMicData);

        mAudienceFragment.setArguments(bundle);

        mAudienceFragment.setOnFriendRequestClickListener(mFriendRequestClickListener);

        mAudienceFragment.setLinkMicAudienceListener(micAudienceListener);

        mAudienceFragment.setLinkMicStatus(linkMicStatus);

        mFriendFragment = new LiveLinkMicFriendListFragment();

        mFriendFragment.setOnFriendRequestClickListener(mFriendRequestClickListener);

        mFriendFragment.setLinkMicStatus(linkMicStatus);

        Collections.addAll(fragmentList, mAudienceFragment, mFriendFragment);

    }

    @Override
    public void onResume() {
        super.onResume();
        int width = ScreenUtils.getScreenWidth(getContext());
        int height = SizeUtils.dip2px(getContext(), 336);
        Objects.requireNonNull(getDialog().getWindow()).setLayout(width, height);
    }

    public void setOnFriendRequestClickListener(LivePkContract.FriendRequestClickListener mFriendRequestClickListener) {
        this.mFriendRequestClickListener = mFriendRequestClickListener;
    }

    public void setMicAudienceListener(LiveLinkMicAudienceListFragment.LinkMicAudienceListener micAudienceListener) {
        this.micAudienceListener = micAudienceListener;
    }

    public void addItem(LivePkFriendBean livePkFriendBean) {

        if (mAudienceFragment != null && mAudienceFragment.isAdded()) {
            mAudienceFragment.addItem(livePkFriendBean);
        }
    }

    public void removeItem(LivePkFriendBean livePkFriendBean) {
        if (mAudienceFragment != null && mAudienceFragment.isAdded()) {
            mAudienceFragment.removeItem(livePkFriendBean);
        }
    }

    public void startLinkMic(String userId) {

        if (mAudienceFragment != null && mAudienceFragment.isAdded()) {
            mAudienceFragment.startLinkMic(userId);
        }

    }

    public void addDialyList(List<LivePkFriendBean> list) {
        if (mAudienceFragment != null && mAudienceFragment.isAdded()) {
            mAudienceFragment.addDialyList(list);
        }
    }

}
