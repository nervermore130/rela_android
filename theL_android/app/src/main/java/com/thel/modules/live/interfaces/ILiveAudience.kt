package com.thel.modules.live.interfaces

import com.thel.bean.live.LinkMicResponseBean
import com.thel.modules.live.bean.AudienceLinkMicResponseBean
import com.thel.modules.live.bean.LiveRoomMsgConnectMicBean

interface ILiveAudience {

    fun liveClosed()

    fun isBottom(isBottom: Boolean)

    fun beenBlocked()

    fun hidePreviewImage()

    fun showCaching()

    fun hideCaching()

    fun updateBroadcasterNetStatus(broadcasterStatus: Int)

    fun updateMyNetStatusGood()

    fun speakTooFast()

    fun updateBalance(result: String)

    fun reconnecting()

    fun showDialogChannelId()

    fun showDialogRequest()

    fun showAlertUserId()

    fun showAlertUserName()

    fun showProgressbar(show: Boolean)

    /**
     * 主播正在连麦中
     */
    fun linkMicBusy(linkMicResponseBean: LinkMicResponseBean)

    /**
     * 主播结束连麦
     */
    fun linkMicStop()

    fun audienceLinkMicSuccess(audienceLinkMicResponseBean: AudienceLinkMicResponseBean)

    fun audienceLinkMicTimeOut()

    /**
     * 挂断
     */
    fun hangup()

    /**
     * 主动挂断
     */
    fun hangupByOwn()

    fun linkMicStart(linkMicResponseBean: LinkMicResponseBean)

    fun showLevelBelowSix()

    fun onSeatAuth()

    /**
     * 显示免费送豆弹窗
     */
    fun showFreeGoldView(gold: Int)

    fun linkMicHangup(liveRoomMsgConnectMicBean: LiveRoomMsgConnectMicBean)

    fun refreshLiveRoom()
}