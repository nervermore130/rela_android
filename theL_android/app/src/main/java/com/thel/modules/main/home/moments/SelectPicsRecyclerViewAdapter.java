package com.thel.modules.main.home.moments;

import com.facebook.drawee.view.SimpleDraweeView;
import com.thel.R;
import com.thel.constants.TheLConstants;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;

import java.util.List;

/**
 * Created by chad
 * Time 17/9/28
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */

public class SelectPicsRecyclerViewAdapter extends BaseRecyclerViewAdapter<PicBean> {

    public SelectPicsRecyclerViewAdapter(List<PicBean> list) {
        super(R.layout.simpledraweeview_pic, list);
    }

    @Override
    protected void convert(BaseViewHolder helper, PicBean bean) {

        final SimpleDraweeView simpleDraweeView = (SimpleDraweeView) helper.getConvertView();

        if (bean.isSelectBtn) {// 选择按钮
            simpleDraweeView.setBackgroundResource(R.mipmap.bg_feed_addpic);
            simpleDraweeView.getHierarchy().setPlaceholderImage(null);
            simpleDraweeView.setImageURI("");
        } else {// 非选择按钮，图片
            simpleDraweeView.getHierarchy().setPlaceholderImage(R.color.pic_placeholder_color);
            simpleDraweeView.setImageURI(TheLConstants.FILE_PIC_URL + bean.localUrl);
        }

    }
}
