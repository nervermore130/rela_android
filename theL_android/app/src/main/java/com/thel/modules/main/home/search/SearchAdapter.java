package com.thel.modules.main.home.search;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.List;

/**
 * Created by chad
 * Time 17/10/23
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */

public class SearchAdapter extends FragmentStatePagerAdapter {

    private final List<Fragment> fragments;

    public SearchAdapter(List<Fragment> fragments, FragmentManager manager) {
        super(manager);
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}
