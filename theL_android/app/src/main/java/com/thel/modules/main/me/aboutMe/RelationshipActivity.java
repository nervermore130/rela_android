package com.thel.modules.main.me.aboutMe;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.google.android.material.tabs.TabLayout;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.base.BaseFragment;
import com.thel.constants.TheLConstants;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import video.com.relavideolibrary.Utils.DensityUtils;

public class RelationshipActivity extends BaseActivity {

    @BindView(R.id.lin_more)
    LinearLayout lin_more;

    @BindView(R.id.txt_title)
    TextView txt_title;

    @BindView(R.id.tab_layout)
    TabLayout tab_layout;

    @BindView(R.id.viewPager)
    ViewPager viewPager;
    private String from_page;
    private String from_page_id;

    @OnClick(R.id.lin_back)
    void back() {
        finish();
    }

    public static final int TYPE_FOLLOW = 0;
    public static final int TYPE_FRIEND = 1;
    public static final int TYPE_FANS = 2;

    private int mType = TYPE_FOLLOW;

    private int followTotal, friendTotal, fansTotal;

    public static void startActivity(Context context, int type) {
        Intent intent = new Intent(context, RelationshipActivity.class);
        intent.putExtra("type", type);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relationship);

        ButterKnife.bind(this);

        lin_more.setVisibility(View.GONE);

        if (getIntent() != null) {
            Intent intent = getIntent();
            mType = intent.getIntExtra("type", TYPE_FOLLOW);
            from_page = intent.getStringExtra(TheLConstants.FROM_PAGE);
            from_page_id = intent.getStringExtra(TheLConstants.FROM_PAGE_ID);

        }

        initTab();
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void initTab() {
        String[] titles = new String[]{getString(R.string.follow), getString(R.string.me_activity_friends), getString(R.string.userinfo_activity_left_fans)};

        List<BaseFragment> fragments = new ArrayList<>();
        FollowingAndFansFragment followFragment = new FollowingAndFansFragment();
        Bundle bundle = new Bundle();
        bundle.putString(TheLConstants.BUNDLE_KEY_REPORT_TYPE, FollowingAndFansFragment.TYPE_FOLLOW);
        bundle.putString(TheLConstants.FROM_PAGE, from_page);
        bundle.putString(TheLConstants.FROM_PAGE_ID, from_page_id);
        followFragment.setArguments(bundle);
        fragments.add(followFragment);
        followFragment.setRelationshipListener(new FollowingAndFansFragment.RelationshipListener() {
            @Override
            public void total(int total) {
                followTotal = total;
                if (tab_layout.getSelectedTabPosition() == 0) {
                    if (total <= 1) {
                        txt_title.setText(total + TheLApp.context.getResources().getString(R.string.friends_activity_follow_odd));
                    } else {
                        txt_title.setText(total + TheLApp.context.getResources().getString(R.string.friends_activity_follow_even));
                    }
                }
            }
        });

        FollowingAndFansFragment friendFragment = new FollowingAndFansFragment();
        Bundle bundle2 = new Bundle();
        bundle2.putString(TheLConstants.FROM_PAGE, from_page);
        bundle2.putString(TheLConstants.FROM_PAGE_ID, from_page_id);
        bundle2.putString(TheLConstants.BUNDLE_KEY_REPORT_TYPE, FollowingAndFansFragment.TYPE_FRIEND);
        friendFragment.setArguments(bundle2);
        fragments.add(friendFragment);
        friendFragment.setRelationshipListener(new FollowingAndFansFragment.RelationshipListener() {
            @Override
            public void total(int total) {
                friendTotal = total;
                if (tab_layout.getSelectedTabPosition() == 1) {
                    if (total <= 1) {
                        txt_title.setText(friendTotal + getString(R.string.friends_activity_friends_odd));
                    } else {
                        txt_title.setText(friendTotal + getString(R.string.friends_activity_friends_even));
                    }
                }
            }
        });

        FollowingAndFansFragment fansFragment = new FollowingAndFansFragment();
        Bundle bundle1 = new Bundle();
        bundle1.putString(TheLConstants.BUNDLE_KEY_REPORT_TYPE, FollowingAndFansFragment.TYPE_FANS);
        bundle1.putString(TheLConstants.FROM_PAGE, from_page);
        bundle1.putString(TheLConstants.FROM_PAGE_ID, from_page_id);

        fansFragment.setArguments(bundle1);
        fragments.add(fansFragment);
        fansFragment.setRelationshipListener(new FollowingAndFansFragment.RelationshipListener() {
            @Override
            public void total(int total) {
                fansTotal = total;
                if (tab_layout.getSelectedTabPosition() == 2) {
                    if (total <= 1) {
                        txt_title.setText(total + TheLApp.context.getResources().getString(R.string.friends_activity_fans_odd));
                    } else {
                        txt_title.setText(total + TheLApp.context.getResources().getString(R.string.friends_activity_fans_even));
                    }
                }
            }
        });

        FragmentPagerAdapter mAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return fragments.get(position);
            }

            @Override
            public int getCount() {
                return fragments.size();
            }

            @Nullable
            @Override
            public CharSequence getPageTitle(int position) {
                return titles[position];
            }
        };
        viewPager.setAdapter(mAdapter);
        viewPager.setOffscreenPageLimit(3);
        setTabWidth(tab_layout, DensityUtils.dp2px(35));
        tab_layout.setupWithViewPager(viewPager);
        tab_layout.setTabsFromPagerAdapter(mAdapter);
        tab_layout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        if (followTotal <= 1) {
                            txt_title.setText(followTotal + TheLApp.context.getResources().getString(R.string.friends_activity_follow_odd));
                        } else {
                            txt_title.setText(followTotal + TheLApp.context.getResources().getString(R.string.friends_activity_follow_even));
                        }
                        break;
                    case 1:
                        if (friendTotal <= 1) {
                            txt_title.setText(friendTotal + getString(R.string.friends_activity_friends_odd));
                        } else {
                            txt_title.setText(friendTotal + getString(R.string.friends_activity_friends_even));
                        }
                        break;
                    case 2:
                        if (fansTotal <= 1) {
                            txt_title.setText(fansTotal + TheLApp.context.getResources().getString(R.string.friends_activity_fans_odd));
                        } else {
                            txt_title.setText(fansTotal + TheLApp.context.getResources().getString(R.string.friends_activity_fans_even));
                        }
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        viewPager.setCurrentItem(mType);
    }

    private void setTabWidth(final TabLayout tabLayout, final int padding) {
        //TabLayout width必须在布局文件中设置wrap_content
        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                try {
                    //拿到tabLayout的mTabStrip属性
                    LinearLayout mTabStrip = (LinearLayout) tabLayout.getChildAt(0);


                    for (int i = 0; i < mTabStrip.getChildCount(); i++) {
                        View tabView = mTabStrip.getChildAt(i);

                        //拿到tabView的mTextView属性  tab的字数不固定一定用反射取mTextView
                        Field mTextViewField = tabView.getClass().getDeclaredField("mTextView");
                        mTextViewField.setAccessible(true);

                        TextView mTextView = (TextView) mTextViewField.get(tabView);

                        tabView.setPadding(0, 0, 0, 0);

                        //因为我想要的效果是   字多宽线就多宽，所以测量mTextView的宽度
                        int width = 0;
                        width = mTextView.getWidth();
                        if (width == 0) {
                            mTextView.measure(0, 0);
                            width = mTextView.getMeasuredWidth();
                        }

                        //设置tab左右间距 注意这里不能使用Padding 因为源码中线的宽度是根据 tabView的宽度来设置的
                        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) tabView.getLayoutParams();
                        params.width = width;
                        params.leftMargin = padding;
                        params.rightMargin = padding;
                        tabView.setLayoutParams(params);

                        tabView.invalidate();
                    }

                } catch (NoSuchFieldException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        });

    }
}
