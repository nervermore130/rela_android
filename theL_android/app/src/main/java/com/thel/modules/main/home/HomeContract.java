package com.thel.modules.main.home;

import android.app.Activity;

import com.thel.base.BasePresenter;
import com.thel.base.BaseView;

/**
 * Created by liuyun on 2017/9/18.
 */

public class HomeContract {

    interface View extends BaseView<HomeContract.Presenter> {

        Activity getHomeActivity();

        boolean isActive();

        void showMusicFragment();
    }

    interface Presenter extends BasePresenter {

    }

}
