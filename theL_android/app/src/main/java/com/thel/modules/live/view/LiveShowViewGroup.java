package com.thel.modules.live.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import androidx.core.view.ViewCompat;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.thel.network.InterceptorSubscribe;
import com.thel.utils.L;
import com.thel.utils.ViewUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import androidx.customview.widget.ViewDragHelper;
import io.reactivex.Flowable;

/**
 * Created by waiarl on 2017/10/31.
 */

public class LiveShowViewGroup extends ViewGroup {
    private final String TAG = LiveShowViewGroup.class.getSimpleName();
    private ViewDragHelper mViewDragHelper;
    private View mTopView;
    private View mConvertView;
    private View mBottomView;
    private View mCoverView;
    private int finalTop;
    private ScrollPageChangedListener mScrollPageChangedListener;
    private GestureDetector mGestureDetector;
    private final float minDis = 8;
    private final float minFling = 200;

    private boolean isDrag = false;
    private boolean isFling = false;
    private boolean canDrag = true;//是否可以下滑
    private boolean isMeeting = false;//是否相遇中
    private boolean isAudienceLinkMic = false;

    private View ctTopView;
    private View ctCovertView;
    private View ctBottomView;

    public enum ViewStatus {
        PREVIOUS, CENTER, NEXT
    }

    protected enum ViewDragStatus {
        STATE_DRAGGING, STATE_SETTLING, STATE_IDLE
    }

    public enum Scroll_Limit {//滑动限制，all全部可以滑动，top:只能上滑，bottom:只能向下滑，none:不可滑动
        ALL, UP, DOWN, NONE
    }

    public enum Direction {
        LEFT, RIGHT
    }


    private Scroll_Limit mScrollLimit = Scroll_Limit.ALL;
    protected final float SNAP_VELOCITY = 200;
    protected final float SNAP_VELOCITY_DISTANCE = 200;
    protected float SNAP_DISTANCE;

    private ViewDragStatus mViewDragStatus = ViewDragStatus.STATE_IDLE;
    private ViewStatus mViewStatus = ViewStatus.CENTER;


    public LiveShowViewGroup(Context context) {
        this(context, null);
    }

    public LiveShowViewGroup(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LiveShowViewGroup(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        measureChildren(widthMeasureSpec, heightMeasureSpec);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        SNAP_DISTANCE = getMeasuredHeight() / 2;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    private void init() {
        mViewDragHelper = ViewDragHelper.create(this, 1.0f, mCallBack);
        setGestureListener();
    }

    public LiveShowViewGroup initView(View topView, View convertView, View bottomView, View coverView) {
        mTopView = topView;
        mConvertView = convertView;
        mBottomView = bottomView;
        mCoverView = coverView;
        initCtView(mTopView, mConvertView, mBottomView);
        return this;
    }


    ViewDragHelper.Callback mCallBack = new ViewDragHelper.Callback() {
        @Override
        public boolean tryCaptureView(View child, int pointerId) {
            //return child == mTopView || child == mConvertView || child == mBottomView;
            return mViewDragStatus == ViewDragStatus.STATE_IDLE;
        }

        @Override
        public int clampViewPositionVertical(View child, int top, int dy) {
            final int offset = top;
            if (!isDrag) {
                return 0;
            }
            if (!canDrag) {
                return 0;
            }

            if (isAudienceLinkMic) {
                return 0;
            }

            if (isMeeting) {
                return 0;
            }

            L.d(TAG, " isAudienceLinkMic : " + isAudienceLinkMic);

            L.d(TAG, " isMeeting : " + isMeeting);


//            if (!isCanScroll()) {
//                return 0;
//            }

            switch (mScrollLimit) {
                case ALL:
                    return offset;
                case UP:
                    return offset <= 0 ? offset : 0;
                case DOWN:
                    return offset >= 0 ? offset : 0;
                case NONE:
                    return 0;
            }

            return top;
        }

        @Override
        public int clampViewPositionHorizontal(View child, int left, int dx) {
            return super.clampViewPositionHorizontal(child, left, dx);
        }

        @Override
        public int getViewVerticalDragRange(View child) {
            return getMeasuredHeight();
        }

        @Override
        public int getViewHorizontalDragRange(View child) {
            return getMeasuredWidth();
        }

        @Override
        public void onViewReleased(View releasedChild, float xvel, float yvel) {
            DragReleased(releasedChild, xvel, yvel);
        }

        @Override
        public void onViewPositionChanged(View changedView, int left, int top, int dx, int dy) {
            onViewPosChanged(changedView, dy);
        }

        @Override
        public void onViewDragStateChanged(int state) {
            switch (state) {
                case ViewDragHelper.STATE_DRAGGING://正在被拖动
                    mViewDragStatus = ViewDragStatus.STATE_DRAGGING;
                    break;
                case ViewDragHelper.STATE_SETTLING://fling完毕后被防止到一个位置
                    mViewDragStatus = ViewDragStatus.STATE_SETTLING;
                    break;
                case ViewDragHelper.STATE_IDLE:/// view没有被拖拽或者 正在进行fling/snap
                    mViewDragStatus = ViewDragStatus.STATE_IDLE;
                    layoutView(mViewStatus);
                    mCoverView.layout(mCoverView.getLeft(), 0, mCoverView.getRight(), getMeasuredHeight());
                    mCoverView.requestLayout();
                  /*  mCoverView.invalidate();
                    mCoverView.setFocusable(true);
                    mCoverView.requestFocus();
                    mCoverView.setFocusableInTouchMode(true);
                    mCoverView.requestFocusFromTouch();*/
                    final ViewStatus status = mViewStatus;
                    if (status != ViewStatus.CENTER) {
                        if (mScrollPageChangedListener != null) {
                            mScrollPageChangedListener.onPreScrollToPage(mViewStatus);
                            mScrollPageChangedListener.initView(ctTopView, ctCovertView, ctBottomView);
                            L.i(TAG, "listener:,isDrag=" + isDrag + ",viewstatus=" + status);
                            canDrag = false;
                            Flowable.timer(100, TimeUnit.MILLISECONDS).subscribe(new InterceptorSubscribe<Long>() {//延迟100毫秒为了surface创建
                                @Override
                                public void onNext(Long data) {
                                    L.i(TAG, "scrollToPage,status=" + status);
                                    mScrollPageChangedListener.scrollToPage(status);
                                    canDrag = true;
                                }
                            });
                        }
                    }


                    break;
            }
        }
    };

    private void onViewPosChanged(View changedView, int dy) {
        L.i(TAG, "dy=" + dy + ",height=" + getMeasuredHeight() + "top=" + changedView.getTop());
        final int count = getChildCount();
        for (int i = 0; i < count; i++) {
            final View view = getChildAt(i);
            if (view != changedView) {
                view.layout(view.getLeft(), view.getTop() + dy, view.getRight(), view.getBottom() + dy);
            }
        }
    }

    private void layoutView(ViewStatus status) {
        if (mConvertView.getTop() == -2 * getMeasuredHeight()) {
            mTopView.layout(mTopView.getLeft(), 0, mTopView.getRight(), getMeasuredHeight());
            mConvertView.layout(mConvertView.getLeft(), getMeasuredHeight(), mConvertView.getRight(), 2 * getMeasuredHeight());
            mBottomView.layout(mBottomView.getLeft(), -getMeasuredHeight(), mBottomView.getRight(), 0);
            initCtView(mBottomView, mTopView, mConvertView);
        } else if (mConvertView.getTop() == -getMeasuredHeight()) {
            mTopView.layout(mTopView.getLeft(), getMeasuredHeight(), mTopView.getRight(), 2 * getMeasuredHeight());
            mConvertView.layout(mConvertView.getLeft(), -getMeasuredHeight(), mConvertView.getRight(), 0);
            mBottomView.layout(mBottomView.getLeft(), 0, mBottomView.getRight(), getMeasuredHeight());
            initCtView(mConvertView, mBottomView, mTopView);
        } else if (mConvertView.getTop() == 0) {
            mTopView.layout(mTopView.getLeft(), -getMeasuredHeight(), mTopView.getRight(), 0);
            mConvertView.layout(mConvertView.getLeft(), 0, mConvertView.getRight(), getMeasuredHeight());
            mBottomView.layout(mBottomView.getLeft(), getMeasuredHeight(), mBottomView.getRight(), 2 * getMeasuredHeight());
            initCtView(mTopView, mConvertView, mBottomView);
        } else if (mConvertView.getTop() == getMeasuredHeight()) {
            mTopView.layout(mTopView.getLeft(), 0, mTopView.getRight(), getMeasuredHeight());
            mConvertView.layout(mConvertView.getLeft(), getMeasuredHeight(), mConvertView.getRight(), 2 * getMeasuredHeight());
            mBottomView.layout(mBottomView.getLeft(), -getMeasuredHeight(), mBottomView.getRight(), 0);
            initCtView(mBottomView, mTopView, mConvertView);
        } else if (mConvertView.getTop() == 2 * getMeasuredHeight()) {
            mTopView.layout(mTopView.getLeft(), getMeasuredHeight(), mTopView.getRight(), 2 * getMeasuredHeight());
            mConvertView.layout(mConvertView.getLeft(), -getMeasuredHeight(), mConvertView.getRight(), 0);
            mBottomView.layout(mBottomView.getLeft(), 0, mBottomView.getRight(), getMeasuredHeight());
            initCtView(mConvertView, mBottomView, mTopView);
        }
    }

    /**
     * 按照上中下顺序排列的view
     *
     * @param top
     * @param convert
     * @param bottom
     */
    private void initCtView(View top, View convert, View bottom) {
        ctTopView = top;
        ctCovertView = convert;
        ctBottomView = bottom;
    }

    private void DragReleased(View releasedChild, float xvel, float yvel) {
        //  if (releasedChild == mConvertView) {
        L.i(TAG, "dragReleased:top=" + releasedChild.getTop() + ",yvel=" + yvel + ",limit=" + mScrollLimit + ",isDrag=" + isDrag);
        if ((releasedChild.getTop() > SNAP_DISTANCE || (yvel > SNAP_VELOCITY && releasedChild.getTop() > SNAP_VELOCITY_DISTANCE)) && (mScrollLimit == Scroll_Limit.DOWN || mScrollLimit == Scroll_Limit.ALL) && isDrag && canDrag) {//向下滑动
            mViewStatus = ViewStatus.PREVIOUS;
            finalTop = getMeasuredHeight();
        } else if ((releasedChild.getTop() < -SNAP_DISTANCE || (yvel < -SNAP_VELOCITY && releasedChild.getTop() < -SNAP_VELOCITY_DISTANCE)) && (mScrollLimit == Scroll_Limit.ALL || mScrollLimit == Scroll_Limit.UP) && isDrag && canDrag) {//向上滑动
            mViewStatus = ViewStatus.NEXT;
            finalTop = -getMeasuredHeight();
        } else {
            finalTop = 0;
            mViewStatus = ViewStatus.CENTER;
        }

        //      }
        L.i(TAG, "dragReleased:top=" + releasedChild.getTop() + ",yvel=" + yvel + ",limit=" + mScrollLimit + ",isDrag=" + isDrag + ",mveiwStatus=" + mViewStatus);

        if (mViewDragHelper.smoothSlideViewTo(releasedChild, 0, finalTop)) {
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    @Override
    public boolean onInterceptHoverEvent(MotionEvent event) {
        return mViewDragHelper.shouldInterceptTouchEvent(event);
    }

    boolean judge = false;
    float startX = 0, startY = 0;

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                isFling = false;
                isDrag = false;
                startX = event.getX();
                startY = event.getY();
                ViewUtils.hideSoftInput((Activity) getContext());
                break;
            case MotionEvent.ACTION_MOVE:
                if (!judge) {
                    final float dx = event.getX() - startX;
                    final float dy = event.getY() - startY;
                    final float ax = Math.abs(dx);
                    final float ay = Math.abs(dy);
                    //     L.i(TAG, "startX=" + startX + ",startY=" + startY + ",x=" + event.getX() + ",y=" + event.getY());
                    if (ax >= minDis || ay >= minDis) {
                        judge = true;
                        if (ax > ay) {
                            isFling = true;

                        } else {
                            isDrag = true;
                        }
                    }
                }
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                judge = false;
                startX = 0;
                startY = 0;
                break;
        }
        //   L.i(TAG, "isDrag=" + isDrag + ",isFling=" + isFling);
        mGestureDetector.onTouchEvent(event);
        mViewDragHelper.processTouchEvent(event);
        return true;
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        if (mConvertView.getTop() == 0) {
            final int height = getMeasuredHeight();
            mTopView.layout(l, -height, r, 0);
            mConvertView.layout(l, 0, r, height);
            mBottomView.layout(l, height, r, 2 * height);
            mCoverView.layout(l, 0, r, height);
        } else {
            final int count = getChildCount();
            for (int i = 0; i < count; i++) {
                final View view = getChildAt(i);
                view.layout(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
            }
        }
    }

    @Override
    public void computeScroll() {
        if (mViewDragHelper.continueSettling(true)) {
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    public void setScrollable(Scroll_Limit limit) {
        mScrollLimit = limit;
    }

    public interface ScrollPageChangedListener {
        void scrollToPage(ViewStatus status);

        void initView(View topView, View convertView, View bottomView);

        void onFling(Direction direction);

        void onPreScrollToPage(ViewStatus mViewStatus);
    }

    public void setOnScrollPageChangedListener(ScrollPageChangedListener listener) {
        mScrollPageChangedListener = listener;
    }

    private void setGestureListener() {
        mGestureDetector = new GestureDetector(getContext(), new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                final float x = e2.getX() - e1.getX();
                L.i(TAG, "onFling:x=" + x + ",e1x=" + e1.getX() + ",e2x=" + e2.getX() + ",vx=" + velocityX + ",vy=" + velocityY);
                if (x > minFling) {//向右
                    if (mScrollPageChangedListener != null) {
                        mScrollPageChangedListener.onFling(Direction.RIGHT);
                    }
                } else if (x < -minFling) {//向左
                    if (mScrollPageChangedListener != null) {
                        mScrollPageChangedListener.onFling(Direction.LEFT);
                    }
                }
                return super.onFling(e1, e2, velocityX, velocityY);
            }
        });

    }

    public LiveShowViewGroup setMeasureDimen(int width, int height) {
       /* setMeasuredDimension(width, height);
        requestLayout();*/
        return this;
    }

    /**
     * 返回当前的上下顺序的view
     *
     * @return
     */
    public List<View> getCtViews() {
        return new ArrayList<View>() {
            {
                add(ctTopView);
                add(ctCovertView);
                add(ctBottomView);
            }
        };
    }

    /**
     * 滑动到上一页
     *
     * @return
     */
    public boolean scrollToPrevious() {
        if (!canDrag || mScrollLimit == Scroll_Limit.UP || mScrollLimit == Scroll_Limit.NONE) {
            return false;
        }
        mViewStatus = ViewStatus.PREVIOUS;
        finalTop = getMeasuredHeight();
        if (mViewDragHelper.smoothSlideViewTo(ctCovertView, 0, finalTop)) {
            ViewCompat.postInvalidateOnAnimation(this);
            return true;
        }
        return false;
    }

    /**
     * 滑动到下一页
     *
     * @return
     */
    public boolean scrollToNext() {

        L.d(TAG," canDrag : " + canDrag);

        L.d(TAG," mScrollLimit : " + mScrollLimit);

        L.d(TAG," mScrollLimit : " + mScrollLimit);

        if (!canDrag || mScrollLimit == Scroll_Limit.DOWN || mScrollLimit == Scroll_Limit.NONE) {
            return false;
        }
        mViewStatus = ViewStatus.NEXT;
        finalTop = -getMeasuredHeight();
        if (mViewDragHelper.smoothSlideViewTo(ctCovertView, 0, finalTop)) {
            ViewCompat.postInvalidateOnAnimation(this);
            return true;
        }
        return false;
    }

    public void isGuestMeeting(boolean isMeeting) {
        this.isMeeting = isMeeting;
    }

    public void isAudienceLinkMic(boolean isAudienceLinkMic) {
        this.isAudienceLinkMic = isAudienceLinkMic;
    }

    public View getTopView() {
        if (mTopView.getTop() == 0) {
            return mTopView;
        }
        if (mConvertView.getTop() == 0) {
            return mConvertView;
        }
        if (mBottomView.getTop() == 0) {
            return mBottomView;
        }
        return null;
    }

    public boolean isCanScroll() {

        L.d(TAG, " !isMeeting : " + (!isMeeting));

        L.d(TAG, " !isAudienceLinkMic : " + (!isAudienceLinkMic));


        return !isMeeting;
    }
}
