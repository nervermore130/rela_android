package com.thel.modules.live.adapter;

import android.content.Context;
import android.net.Uri;
import androidx.core.content.ContextCompat;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.user.MyImageBean;
import com.thel.constants.TheLConstants;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;
import com.thel.utils.ImageUtils;

import java.util.List;


/**
 * Created by waiarl on 2017/4/19.
 */
public class LiveSimplePhotoListAdapter extends BaseRecyclerViewAdapter<MyImageBean> {
    private final float radius;

    public LiveSimplePhotoListAdapter(Context context, List<MyImageBean> urlList) {
        super(R.layout.adapter_live_simple_photo_list_item, urlList);
        radius = TheLApp.getContext().getResources().getDimension(R.dimen.nearby_user_view_radius);
    }

    @Override
    protected void convert(BaseViewHolder helper, MyImageBean item) {
        final SimpleDraweeView img_thumb = helper.getView(R.id.img_thumb);
        img_thumb.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(item.longThumbnailUrl, TheLConstants.ICON_SMALL_SIZE, TheLConstants.ICON_SMALL_SIZE))).build()).setAutoPlayAnimations(true).build());
        img_thumb.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadius(radius).setRoundingMethod(RoundingParams.RoundingMethod.OVERLAY_COLOR).setOverlayColor(ContextCompat.getColor(TheLApp.getContext(), R.color.white)));
    }
}
