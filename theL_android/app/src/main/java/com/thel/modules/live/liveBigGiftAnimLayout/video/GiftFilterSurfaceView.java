package com.thel.modules.live.liveBigGiftAnimLayout.video;

/**
 * Created by liuyun on 2017/2/10.
 */

import android.content.Context;
import android.graphics.PixelFormat;
import android.opengl.GLSurfaceView;
import android.view.Surface;

import com.kingsoft.media.httpcache.KSYProxyService;
import com.ksyun.media.player.KSYMediaPlayer;

import baidu.filter.GPUImageExtTexFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageFilterGroup;
import video.com.relavideolibrary.RelaVideoSDK;

/**
 * @author liuyun
 */
public class GiftFilterSurfaceView extends GLSurfaceView {

    private GiftGPUImageRenderer mRenderer;

    private KSYProxyService mKSYProxyService;

    public GiftFilterSurfaceView(Context context) {
        super(context);
        setEGLContextClientVersion(2);

        clear();

        GPUImageFilterGroup filterGroup = new GPUImageFilterGroup();

        filterGroup.addFilter(new GPUImageExtTexFilter());

        filterGroup.addFilter(new GPUImageFilter("" +
                "attribute vec4 position;\n" +
                "attribute vec4 inputTextureCoordinate;\n" +
                " \n" +
                "varying vec2 textureCoordinate;\n" +
                " \n" +
                "void main()\n" +
                "{\n" +
                "    gl_Position = position;\n" +
                "    textureCoordinate = inputTextureCoordinate.xy;\n" +
                "}", "" +
                "varying highp vec2 textureCoordinate;\n" +
                " \n" +
                "uniform sampler2D inputImageTexture;\n" +
                " \n" +
                "void main()\n" +
                "{\n" +
                "highp vec2 st = textureCoordinate;\n" +
                "highp vec2 lst = st * vec2(0.5, 1.0);\n" +
                "highp vec2 rst = lst + vec2(0.5, 0.0);\n" +
                "highp vec4 imgOrigin = texture2D(inputImageTexture, lst);\n" +
                "highp vec4 imgMask = texture2D(inputImageTexture, rst);\n" +
                "highp vec3 imgRGB = imgOrigin.rgb*imgMask.r;\n" +
                "highp float imgAlpha = imgMask.r;\n" +
                "gl_FragColor = vec4(imgRGB.r,imgRGB.g,imgRGB.b, imgAlpha);\n" +
                "}"));
        mRenderer = new GiftGPUImageRenderer(filterGroup);

        setRenderer(mRenderer);

        mKSYProxyService = RelaVideoSDK.getKSYProxy();
        if (mKSYProxyService != null) {
            mKSYProxyService.startServer();

        }

    }

    public Surface getSurface() {
        return mRenderer.getSurface();
    }

    public void setMediaPlayer(KSYMediaPlayer mediaPlayer) {
        mRenderer.setUpSurfaceTexture(mediaPlayer);
    }

    public void setSourceSize(int imageWidth, int imageHeight) {

        mRenderer.setSourceSize(imageWidth, imageHeight);
    }

    public void clear() {
        setZOrderOnTop(true);
        getHolder().setFormat(PixelFormat.TRANSLUCENT);
        setEGLConfigChooser(8, 8, 8, 8, 16, 0);

    }

}