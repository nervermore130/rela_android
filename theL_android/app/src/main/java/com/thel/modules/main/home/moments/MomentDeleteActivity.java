package com.thel.modules.main.home.moments;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.View;
import android.widget.LinearLayout;

import com.thel.R;
import com.thel.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MomentDeleteActivity extends BaseActivity {

    @BindView(R.id.lin_more) LinearLayout lin_more;

    @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_moment_delete);
        ButterKnife.bind(this);

        lin_more.setVisibility(View.GONE);

    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    @OnClick(R.id.lin_back) void onBackClick() {
        MomentDeleteActivity.this.finish();
    }

}
