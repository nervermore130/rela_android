package com.thel.modules.live.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.live.LiveMultiSeatBean;
import com.thel.imp.follow.FollowStatusChangedImpl;
import com.thel.imp.follow.bean.SingleUserRelationBean;
import com.thel.imp.follow.bean.SingleUserRelationNetBean;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.live.bean.SoftEnjoyBean;
import com.thel.modules.live.bean.SoftGiftBean;
import com.thel.modules.live.in.LiveBaseView;
import com.thel.modules.live.utils.LiveUtils;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.service.DefaultRequestService;
import com.thel.ui.widget.TimeTextView;
import com.thel.utils.L;
import com.thel.utils.ScreenUtils;
import com.thel.utils.SimpleDraweeViewUtils;
import com.thel.utils.SizeUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.thel.imp.follow.FollowStatusChangedListener.FOLLOW_STATUS_FAN;
import static com.thel.imp.follow.FollowStatusChangedListener.FOLLOW_STATUS_NO;

/**
 * Created by the L on 2016/5/25.
 * 礼物打赏界面
 */
public class SoftGiftListView extends RelativeLayout implements LiveBaseView<SoftGiftListView> {
    private final String TAG = SoftGiftListView.class.getSimpleName();
    private Context mContext;
    private ViewPager viewPager;//礼物打赏viewpager
    private TextView txt_soft_money;//我的软妹币
    private TextView txt_recharge;//去充值
    private LinearLayout lin_points;//viewpager想对应点
    private SoftEnjoyBean softEnjoyBean;//打赏页面请求返回数据封装对象
    private List<GridView> pagerViews;//viewpager的页面集合；
    private List<List<SoftGiftBean>> pagerGifts;//以页为单位的礼物集合
    private int pagerGiftLines = 2;//每页显示几行
    private int lineGiftSize = 5;//每行显示的礼物数
    private int pagerGiftSize = pagerGiftLines * lineGiftSize;//每页显示的礼物数
    private int pagerSize;//viewpager的总共页数
    private ViewPagerAdapter viewPagerAdatper;// viewpager adaapter
    private ArrayList<SimpleDraweeView> pointViews = new ArrayList<SimpleDraweeView>();//下方点集合
    private TimeTextView preTimeTextView;//上一个被点击的view中的timeTextView
    private GiftOnClickListener giftClickListener;
    public static final int SCREEN_PORTRAIT = 0;
    public static final int SCREEN_LANDSCAPE = 1;
    private int mOrientation = SCREEN_PORTRAIT;
    private GiftLianClickListener giftLianClickListener;
    private boolean isAnimating = false;
    private LinearLayout lin_recharge;
    private LinearLayout rl_multi_send_gift;
    private ImageView crowd_avatar;
    private TextView crowd_user_name;
    private TextView txt_crowd_follow;
    private LinearLayout ll_crowd_profile;
    private LiveMultiSeatBean seatBean;
    private boolean sendAnchor = true;
    private GuestsShowInfoListener guestsShowInfoListener;
    private View bottomView;

    public SoftGiftListView(Context context) {
        this(context, null, 0);
    }

    public SoftGiftListView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);

    }

    public SoftGiftListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }

    @Override protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int height = SizeUtils.dip2px(TheLApp.context, 275);
        heightMeasureSpec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    /**
     * 仅仅是初始化控件
     */
    private void init() {
        inflate(mContext, R.layout.live_gift_list_layout2, this);
        viewPager = findViewById(R.id.gift_list_viewpager);
        txt_soft_money = findViewById(R.id.txt_soft_money);
        txt_recharge = findViewById(R.id.txt_recharge);
        //   txt_recharge.setText(TheLApp.getContext().getString(R.string.go_to_recharge));
        lin_recharge = findViewById(R.id.lin_recharge);
        lin_points = findViewById(R.id.lin_point);
        /**
         * 多人连麦
         * **/
        rl_multi_send_gift = findViewById(R.id.rl_multi_send_gift);
        crowd_avatar = findViewById(R.id.crowd_avatar);
        crowd_user_name = findViewById(R.id.crowd_user_name);
        txt_crowd_follow = findViewById(R.id.txt_crowd_follow);
        ll_crowd_profile = findViewById(R.id.ll_crowd_profile);
        bottomView = findViewById(R.id.rel_bottom);
        setOrientation(mOrientation);
    }

    /**
     * 去充值
     *
     * @param listener
     */
    public void setOnRechargeClickListener(OnClickListener listener) {
        lin_recharge.setOnClickListener(listener);
    }

    /**
     * 初始化数据
     */
    public void initView(SoftEnjoyBean softEnjoyBean) {
        L.i(TAG, softEnjoyBean == null ? "softEnjoyBean=null" : softEnjoyBean.toString());
        if (softEnjoyBean == null) {
            return;
        }
        this.softEnjoyBean = softEnjoyBean;
        pagerSize = (int) Math.ceil(softEnjoyBean.list.size() / (double) pagerGiftSize);//总共多少页，pagergiftsize目前为8
        pagerViews = new ArrayList<>();//viewpager 子vIEW
        pagerGifts = new ArrayList<>();//以页为单位的礼物集合
        updateBalance(softEnjoyBean.gold);
        setListPagerViews();//设置viewpager要显示的view集合
        setPoints();//设置下面显示的点
        setViewpager();//对viewpager进行设置
        requestLayout();
    }

    public void showGiftTop(boolean isShow) {
        if (isShow) {
            rl_multi_send_gift.setVisibility(VISIBLE);
        } else {
            rl_multi_send_gift.setVisibility(INVISIBLE);

        }
    }

    public void setOrientation(int orientation) {
        if (mOrientation == orientation) {
            return;
        }
        mOrientation = orientation;
        if (SCREEN_PORTRAIT == orientation) {
            pagerGiftLines = 2;//每页显示几行
            lineGiftSize = 5;//每行显示的礼物数
        } else {
            pagerGiftLines = 1;//每页显示几行
            lineGiftSize = 9;//每行显示的礼物数
        }
        pagerGiftSize = pagerGiftLines * lineGiftSize;//每页显示的礼物数
        if (softEnjoyBean != null) {
            initView(softEnjoyBean);
        }
    }


    /**
     * 设置viewpager要显示的view集合
     */
    private void setListPagerViews() {
        for (int i = 0; i < softEnjoyBean.list.size(); i++) {          //求取pagerGifts集合
            if (i % pagerGiftSize == 0) {
                List<SoftGiftBean> gifts = new ArrayList<>();
                pagerGifts.add(gifts);
            }
            pagerGifts.get(i / pagerGiftSize).add(softEnjoyBean.list.get(i));
        }
        for (int i = 0; i < pagerSize; i++) {
            setPagers(i);//设置每一页
        }
    }

    /**
     * 设置viewpager的每一页显示
     *
     * @param pageIndex 第几页
     */
    private void setPagers(final int pageIndex) {
        final GridView gridView = new GridView(mContext);
        gridView.setNumColumns(lineGiftSize);
        gridView.setBackgroundColor(Color.TRANSPARENT);
        gridView.setStretchMode(GridView.STRETCH_COLUMN_WIDTH);
        gridView.setCacheColorHint(0);
        gridView.setSelector(new ColorDrawable(Color.TRANSPARENT));
        gridView.setGravity(Gravity.CENTER_HORIZONTAL);
        gridView.setVerticalSpacing(Utils.dip2px(TheLApp.getContext(), 7));//行间距7dp
        SoftGiftAdapter adapter = new SoftGiftAdapter(pagerGifts.get(pageIndex), pageIndex);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (giftClickListener != null) {
                    TimeTextView timeTextView = view.findViewById(R.id.txt_gift_name);
                    if (preTimeTextView != null && preTimeTextView.isCombo() && preTimeTextView != timeTextView) {//如果上一个计时器不为空并且不是同一个计时器，并且还处于连击状态
                        preTimeTextView.showContent();//显示正常内容
                    }
                    giftClickListener.OnGiftClicked(parent, view, timeTextView, pageIndex, position / lineGiftSize, position % lineGiftSize, position, pageIndex * pagerGiftSize + position, (SoftGiftBean) parent.getAdapter().getItem(position), sendAnchor, seatBean);
                    timeTextView.count();
                    preTimeTextView = timeTextView;
                }
            }
        });


        pagerViews.add(gridView);//添加到viewpager子页面列表

    }


    /**
     * 对外接口方法
     *
     * @param listener
     */
    public void setOnGiftClickListener(GiftOnClickListener listener) {
        giftClickListener = listener;
    }

    public void setOnGiftLianClickListener(GiftLianClickListener listener) {
        giftLianClickListener = listener;
    }

    public void setShowGuestsInfoDialogListener(GuestsShowInfoListener listener) {
        this.guestsShowInfoListener = listener;
    }

    /**
     * 设置下面显示的点
     */
    private void setPoints() {
        lin_points.removeAllViews();
        pointViews.clear();
        SimpleDraweeView imageView;
        if (pagerSize <= 1)
            return;
        for (int i = 0; i < pagerSize; i++) {
            imageView = new SimpleDraweeView(mContext);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(new ViewGroup.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
            layoutParams.leftMargin = 10;
            layoutParams.rightMargin = 10;
            layoutParams.width = Utils.dip2px(TheLApp.getContext(), 6);
            layoutParams.height = layoutParams.width;
            imageView.setBackgroundResource(R.drawable.circle_gray);
            lin_points.addView(imageView, layoutParams);
            if (i == 0) {
                imageView.setBackgroundResource(R.drawable.circle_main_color);
            }
            pointViews.add(imageView);
        }
    }

    /**
     * 对viewpager进行设置
     */
    private void setViewpager() {
        viewPagerAdatper = new ViewPagerAdapter(pagerViews);
        viewPager.setAdapter(viewPagerAdatper);
        viewPager.setCurrentItem(0);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                drawPoint(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    /**
     * 绘制游标背景
     */
    public void drawPoint(int index) {
        for (int i = 0; i < pointViews.size(); i++) {
            if (index == i) {
                pointViews.get(i).setBackgroundResource(R.drawable.circle_main_color);
            } else {
                pointViews.get(i).setBackgroundResource(R.drawable.circle_gray);
            }
        }
    }

    /**
     * 点击某个礼物后接口
     */
    public interface GiftOnClickListener {
        /**
         * @param parent          父控件 gridview
         * @param view            被点击的view
         * @param timeTextView    计时器timeTextView
         * @param pageIndex       当前页
         * @param columnIndex     所在页的当前行
         * @param lineIndex       所在行的位置
         * @param currentPosition 所在当前页的位置
         * @param position        在所有表情中的位置
         * @param softGiftBean    点击的对象softgiftbean
         * @param sendAnchor      true 送给主播 false送给嘉宾
         * @param seatBean        嘉宾座位信息
         */
        void OnGiftClicked(ViewGroup parent, View view, TimeTextView timeTextView, int pageIndex, int columnIndex, int lineIndex, int currentPosition, int position, SoftGiftBean softGiftBean, boolean sendAnchor, LiveMultiSeatBean seatBean);
    }

    public interface GiftLianClickListener {
        /**
         * @param parent          父控件 gridview
         * @param itemView        被点击的gridview的某个item
         * @param clickView       点击的连字按钮view
         * @param pageIndex       当前页
         * @param columnIndex     所在页的当前行
         * @param lineIndex       所在行的位置
         * @param currentPosition 所在当前页的位置
         * @param position        在所有表情中的位置
         * @param softGiftBean    点击的对象softgiftbean
         */
        void onGiftClicked(ViewGroup parent, View itemView, View clickView, int pageIndex, int columnIndex, int lineIndex, int currentPosition, int position, SoftGiftBean softGiftBean, boolean isSendAnchor, int seatNum);
    }

    public interface GuestsShowInfoListener {
        void onGuestsClicked(String userid);
    }
    public interface GotoRechargeSoftMoneyListener {
        void onGotoRechageClicked();
    }


    /**
     * 更新某个礼物
     *
     * @param gift
     */
    public void refreshSoftGiftBean(SoftGiftBean gift) {
        for (int i = 0; i < pagerViews.size(); i++) {
            for (int j = 0; j < pagerGifts.get(i).size(); j++) {
                if (gift.id == pagerGifts.get(i).get(j).id) {
                    ((SoftGiftAdapter) pagerViews.get(i).getAdapter()).notifyDataSetChanged();
                    return;
                }
            }
        }
    }

    public void refreshAll() {
        for (int i = 0; i < pagerViews.size(); i++) {
            ((SoftGiftAdapter) pagerViews.get(i).getAdapter()).notifyDataSetChanged();
        }
    }

    /**
     * 更新余额
     *
     * @param balance 余额
     */
    public void updateBalance(long balance) {
        if (balance > 1) {
            txt_soft_money.setText(getContext().getString(R.string.balance_credits, balance));
        } else {
            txt_soft_money.setText(getContext().getString(R.string.balance_credits, balance));
        }
    }

    /**
     * 送给主播
     *
     * @return
     */

    public SoftGiftListView show() {
        sendAnchor = true;
        rl_multi_send_gift.setVisibility(INVISIBLE);
//        requestLayout();
//        LiveUtils.animInLiveShowView(this);
        refreshAll();
        animIn();
        return this;
    }

    /**
     * 送给嘉宾
     *
     * @param item
     * @return
     */

    public SoftGiftListView showOther(final LiveMultiSeatBean item) {
        this.seatBean = item;
        sendAnchor = false;
        if (item != null) {
            getSingeUserRelation(seatBean.userId);
            ImageLoaderManager.imageLoaderCircle(crowd_avatar, R.mipmap.icon_user, item.avatar);
            crowd_user_name.setText(item.nickname);

            //关注
            txt_crowd_follow.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    ViewUtils.preventViewMultipleClick(v, 1500);

                    FollowStatusChangedImpl.followUserWithNoDialog(item.userId + "", FollowStatusChangedImpl.ACTION_TYPE_FOLLOW, item.nickname, item.avatar);

                    txt_crowd_follow.setVisibility(View.GONE);
                }
            });
            ll_crowd_profile.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (guestsShowInfoListener != null) {
                        guestsShowInfoListener.onGuestsClicked(item.userId);
                    }

                }
            });
        }

        rl_multi_send_gift.setVisibility(VISIBLE);

        refreshAll();
        animIn();
        return this;
    }

    /**
     * 获取是否是好友
     * 如果是好友就不显示关注
     *
     * @param userId
     */
    public void getSingeUserRelation(final String userId) {
        final Flowable<SingleUserRelationNetBean> flowable = DefaultRequestService.createCommonRequestService().getSingleUsersRelation(userId);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<SingleUserRelationNetBean>() {

            @Override
            public void onNext(SingleUserRelationNetBean singleUserRelationNetBean) {
                final SingleUserRelationBean bean = singleUserRelationNetBean.data;
                refreshFollowStatus(bean);
            }

        });
    }

    private void refreshFollowStatus(SingleUserRelationBean bean) {
        if (seatBean != null && bean != null) {
            seatBean.followState = bean.status;

            if (FOLLOW_STATUS_NO == bean.status || FOLLOW_STATUS_FAN == bean.status) {

                txt_crowd_follow.setVisibility(VISIBLE);
            } else {
                txt_crowd_follow.setVisibility(GONE);

            }
        }

    }

    public SoftGiftListView hide() {
        LiveUtils.animOutLiveShowView(this, false);
        seatBean = null;
        sendAnchor = true;
        return this;
    }

    public SoftGiftListView destroyView() {
        isAnimating = false;
        setVisibility(GONE);
        clearAnimation();
//        setTranslationX(0f);
//        setTranslationY(0f);
        animOut();
        return this;
    }

    @Override
    public boolean isAnimating() {
        return isAnimating;
    }

    @Override
    public void setAnimating(boolean isAnimating) {
        this.isAnimating = isAnimating;
    }

    @Override
    public void showShade(boolean show) {

    }


    /**
     * viewPager Adapter
     */
    class ViewPagerAdapter extends PagerAdapter {

        private final List<GridView> pagerViews;

        public ViewPagerAdapter(List<GridView> pagerViews) {
            super();
            this.pagerViews = pagerViews;

        }

        @Override
        public int getCount() {
            return pagerViews.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public int getItemPosition(Object object) {
            return super.getItemPosition(object);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView(pagerViews.get(position));
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            container.addView(pagerViews.get(position));
            return pagerViews.get(position);
        }
    }

    /**
     * GridView adapter
     */
    class SoftGiftAdapter extends BaseAdapter {

        private final LayoutInflater mInflater;
        private final int pageIndex;
        private List<SoftGiftBean> softGifts;
        private float size;

        public SoftGiftAdapter(List<SoftGiftBean> softGiftBeen, int pageIndex) {
            mInflater = (LayoutInflater) TheLApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            setGifts(softGiftBeen);
            size = TheLApp.getContext().getResources().getDimension(R.dimen.gift_icon_size);
            this.pageIndex = pageIndex;
        }

        private void setGifts(List<SoftGiftBean> softGiftBeen) {
            if (softGiftBeen != null) {
                this.softGifts = softGiftBeen;
            } else {
                softGifts = new ArrayList<>();
            }
        }

        @Override
        public int getCount() {
            return softGifts.size();
        }

        @Override
        public Object getItem(int position) {
            return softGifts.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, final ViewGroup parent) {
            ViewHolder holder = null;

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.live_soft_gift_item, parent, false);
                holder = new ViewHolder();
                holder.icon_gift_soft = convertView.findViewById(R.id.icon_soft_gift);
                holder.txt_gift_name = convertView.findViewById(R.id.txt_gift_name);
                holder.txt_gift_price = convertView.findViewById(R.id.txt_gift_price);
                holder.img_is_hot = convertView.findViewById(R.id.img_is_hot);
                holder.img_lian = convertView.findViewById(R.id.img_lian);
                holder.ll_heart_count = convertView.findViewById(R.id.ll_heart_count);
                holder.tx_heart_count = convertView.findViewById(R.id.tx_heart_count);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            // holder.icon_gift_soft.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(softGifts.get(position).icon, size, size))).build()).setAutoPlayAnimations(true).build());
            final SoftGiftBean softGiftBean = softGifts.get(position);
            SimpleDraweeViewUtils.setImageUrl(holder.icon_gift_soft, softGiftBean.icon, size, size);
            holder.txt_gift_name.getInstance(20, 100, 100).setContent(softGiftBean.title + "");
            if (softGiftBean.gold > 1) {
                holder.txt_gift_price.setText(mContext.getString(R.string.soft_moneys, softGiftBean.gold));
            } else {
                holder.txt_gift_price.setText(mContext.getString(R.string.soft_money, softGiftBean.gold));
            }
            holder.img_is_hot.setVisibility(View.GONE);
            if (seatBean != null && rl_multi_send_gift.getVisibility() == VISIBLE) {
                holder.ll_heart_count.setVisibility(VISIBLE);
                String hearts = "+" + softGiftBean.hearts;
                holder.tx_heart_count.setText(hearts);
            } else {
                holder.ll_heart_count.setVisibility(GONE);
            }


            /***此功能已取消***/
            /*if (softGifts.get(position).isNew != 0) {//如果是最新
                holder.img_is_hot.setImageResource(R.mipmap.icn_me_vip_new);
                holder.img_is_hot.setVisibility(View.VISIBLE);
            } else if (softGifts.get(position).isHot != 0) {
                holder.img_is_hot.setImageResource(R.mipmap.icn_status_hot);//hot
                holder.img_is_hot.setVisibility(View.VISIBLE);
            } else {
                holder.img_is_hot.setVisibility(View.GONE);
            }*/
            //点击连 字
           /* if (TextUtils.isEmpty(softGifts.get(position).mark) && softGifts.get(position).gold <= 20) {//大礼物标识，为空说明为小礼物，小礼物才有连击并且20以及以下软妹豆
                holder.img_lian.setVisibility(View.VISIBLE);
            } else {
                holder.img_lian.setVisibility(View.GONE);
            }*/
            if (softGiftBean.canCombo > 0) {
                holder.img_lian.setVisibility(View.VISIBLE);
            } else {
                holder.img_lian.setVisibility(View.GONE);
            }
            final View currentView = convertView;
            holder.img_lian.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {

                    L.d(TAG, " seatBean : " + seatBean);

                    if (giftLianClickListener != null) {
                        giftLianClickListener.onGiftClicked(parent, currentView, v, pageIndex, position / lineGiftSize, position % lineGiftSize, position, pagerSize * pageIndex + position, softGiftBean, sendAnchor, seatBean == null ? -1 : seatBean.seatNum);

                    }
                }
            });
            return convertView;
        }

        class ViewHolder {
            SimpleDraweeView icon_gift_soft;//图标
            TimeTextView txt_gift_name;//名字，title
            TextView txt_gift_price;//价格，gold
            ImageView img_is_hot;//是否热门（新加）
            ImageView img_lian;//连击按钮
            LinearLayout ll_heart_count;// 多人连麦 赠送礼物
            TextView tx_heart_count;//多人连麦 赠送礼物 积分
        }
    }

    private void animIn() {
        setVisibility(VISIBLE);
        View view = getRootView();
        int height = ScreenUtils.getScreenHeight(getContext());
        L.d(TAG, " height : " + height);
        int viewHeight = SizeUtils.dip2px(getContext(), 275);
        L.d(TAG, " viewHeight : " + viewHeight);
        ValueAnimator valueAnimator = ValueAnimator.ofInt(height, height - viewHeight);
        valueAnimator.setInterpolator(new LinearInterpolator());
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override public void onAnimationUpdate(ValueAnimator animation) {
                int offset = (int) animation.getAnimatedValue();
                L.d(TAG, " offset : " + offset);
//                scrollTo(0, offset);
                setY(offset);
            }
        });
        valueAnimator.addListener(new AnimatorListenerAdapter() {
            @Override public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                animation.cancel();
            }
        });
        valueAnimator.start();
    }

    private void animOut() {
        View view = getRootView();
        int height = view.getBottom();
        int viewHeight = SizeUtils.dip2px(getContext(), 275);

        final ValueAnimator valueAnimator = ValueAnimator.ofInt(height - viewHeight, height);
        valueAnimator.setInterpolator(new LinearInterpolator());
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override public void onAnimationUpdate(ValueAnimator animation) {
                int offset = (int) animation.getAnimatedValue();
//                scrollTo(0, offset);
                setY(offset);
            }
        });
        valueAnimator.addListener(new AnimatorListenerAdapter() {
            @Override public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                animation.cancel();
            }
        });
        valueAnimator.start();
    }


}
