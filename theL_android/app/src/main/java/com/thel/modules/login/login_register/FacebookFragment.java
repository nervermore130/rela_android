package com.thel.modules.login.login_register;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.modules.login.LoginEventManager;
import com.thel.network.LoginInterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.api.loginapi.bean.SignInBean;
import com.thel.utils.DialogUtil;
import com.thel.utils.JsonUtils;
import com.thel.utils.L;
import com.thel.utils.ToastUtils;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONObject;

import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 */
public class FacebookFragment extends Fragment implements FacebookCallback<LoginResult> {

    private LoginButton fbLogin;

    private BaseActivity activity;

    private String nickName;

    private String avatar;

    private CallbackManager callbackManager;

    //0圆圈FB登录按钮布局 1椭圆FB登录按钮布局
    private int type = 0;

    private static final String TYPE = "type";

    public FacebookFragment() {
        // Required empty public constructor
    }

    public static FacebookFragment newInstance(int type) {
        FacebookFragment facebookFragment = new FacebookFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(TYPE, type);
        facebookFragment.setArguments(bundle);
        return facebookFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            type = getArguments().getInt(TYPE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (type == 0) {
            return inflater.inflate(R.layout.fragment_facebook, container, false);
        } else if (type == 1) {
            return inflater.inflate(R.layout.facebook_login_register, container, false);
        } else {
            return inflater.inflate(R.layout.fragment_facebook, container, false);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        activity = (BaseActivity) getActivity();

        callbackManager = CallbackManager.Factory.create();

        fbLogin = new LoginButton(getActivity());
        fbLogin.setReadPermissions("public_profile");
        fbLogin.setFragment(this);
        fbLogin.registerCallback(callbackManager, this);

        view.findViewById(R.id.img_fb).setOnClickListener(clickListener);
    }


    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            MobclickAgent.onEvent(getActivity(), "fb_login");// 开始fb登录流程打点
            fbLogin.performClick();
        }
    };

    @Override
    public void onSuccess(final LoginResult loginResult) {
        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(final JSONObject jsonObject, GraphResponse graphResponse) {
                MobclickAgent.onEvent(getActivity(), "fb_auth_succeed");// facebook注册转化率统计：facebook授权成功
                // FB性别： male or female
                String gender = JsonUtils.getString(jsonObject, "gender", "");
                if ("male".equals(gender)) {
                    MobclickAgent.onEvent(getActivity(), "fb_male_login");// facebook登录性别是男性打点
                    final DialogInterface.OnClickListener listener0 = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    };
                    final DialogInterface.OnClickListener listener1 = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            MobclickAgent.onEvent(getActivity(), "check_i_am_a_lady");// 点击我是女生按钮
                            dialog.dismiss();
                            activity.showLoadingNoBack();
                            String id = JsonUtils.getString(jsonObject, "id", "");
                            nickName = JsonUtils.getString(jsonObject, "name", "");
                            avatar = "https://graph.facebook.com/" + id + "/picture?logType=large";

                            MobclickAgent.onEvent(getActivity(), "begin_request_loginOrRegister_interface");// 开始调用登录接口打点

                            RequestBusiness.getInstance()
                                    .signIn("fb", "", "", "", "", "", id, loginResult.getAccessToken().getToken(), avatar, nickName, null)
                                    .onBackpressureDrop().subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(new LoginInterceptorSubscribe<SignInBean>() {
                                        @Override
                                        public void onNext(SignInBean data) {
                                            super.onNext(data);
                                            activity.closeLoading();
                                            if (data.data != null) {
                                                LoginEventManager.loginCallback(getActivity(), data);
                                                getActivity().finish();
                                            }
                                        }
                                    });
                        }
                    };
                    DialogUtil.getInstance().showConfirmDialog(getActivity(), "", getString(R.string.register_activity_test_hint), getString(R.string.i_am_a_lady), getString(R.string.info_quit), listener1, listener0);
                    LoginManager.getInstance().logOut();
                    return;
                }

                MobclickAgent.onEvent(getActivity(), "fb_female_login");// 女性性别校验成功
                activity.showLoadingNoBack();
                String id = JsonUtils.getString(jsonObject, "id", "");
                nickName = JsonUtils.getString(jsonObject, "name", "");
                avatar = "https://graph.facebook.com/" + id + "/picture?logType=large";

                MobclickAgent.onEvent(getActivity(), "begin_request_loginOrRegister_interface");// 开始调用登录接口打点

                RequestBusiness.getInstance()
                        .signIn("fb", "", "", "", "", "", id, loginResult.getAccessToken().getToken(), avatar, nickName, null)
                        .onBackpressureDrop().subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new LoginInterceptorSubscribe<SignInBean>() {
                            @Override
                            public void onNext(SignInBean data) {
                                super.onNext(data);
                                LoginEventManager.loginCallback(getActivity(), data);
                                L.d("facebook", "sendBroadcast");
                                getActivity().finish();
                            }
                        });

                LoginManager.getInstance().logOut();
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,link,email,gender");
        request.setParameters(parameters);
        request.executeAsync();
    }

    @Override
    public void onCancel() {
        MobclickAgent.onEvent(getActivity(), "user_cancel_fb_login");
        ToastUtils.showToastShort(getActivity(), "facebook cancel");
    }

    @Override
    public void onError(FacebookException error) {
        MobclickAgent.onEvent(getActivity(), "fb_login_error");
        ToastUtils.showToastShort(getActivity(), "The signature is wrong and the application is in the development mode");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
