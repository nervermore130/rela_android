package com.thel.modules.live.view;

import android.content.Context;
import android.graphics.PorterDuff;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.constants.TheLConstants;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.live.bean.LiveTopFansBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by waiarl on 2017/4/20.
 */

public class LiveLookRankView extends RelativeLayout {
    private final Context mContext;
    private View rel_0;
    private ImageView img_rank_0;
    private View rel_1;
    private ImageView img_rank_1;
    private View rel_2;
    private ImageView img_rank_2;
    private View rel_3;
    private ImageView img_rank_3;
    private OnUserClickListener mOnUserClickListener;
    private List<LiveTopFansBean> topFans = new ArrayList<>();
    public static final int TYPE_SHOW = 1;
    public static final int TYPE_CAPTURE = 0;
    private int type = 0;

    public LiveLookRankView(Context context) {
        this(context, null);
    }

    public LiveLookRankView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LiveLookRankView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
        setListener();
    }

    private void init() {
        inflate(mContext, R.layout.live_look_rank_view, this);
        rel_0 = findViewById(R.id.rel_0);
        img_rank_0 = findViewById(R.id.img_rank_0);
        rel_1 = findViewById(R.id.rel_1);
        img_rank_1 = findViewById(R.id.img_rank_1);
        rel_2 = findViewById(R.id.rel_2);
        img_rank_2 = findViewById(R.id.img_rank_2);
        rel_3 = findViewById(R.id.rel_3);
        img_rank_3 = findViewById(R.id.img_rank_3);
        //        rel_0.setVisibility(View.VISIBLE);
        rel_0.setVisibility(View.GONE);
        rel_1.setVisibility(View.GONE);
        rel_2.setVisibility(View.GONE);
        rel_3.setVisibility(View.GONE);

        rel_1.getBackground().setColorFilter(ContextCompat.getColor(TheLApp.getContext(), R.color.live_look_rank_bg_1), PorterDuff.Mode.SRC);
        rel_2.getBackground().setColorFilter(ContextCompat.getColor(TheLApp.getContext(), R.color.live_look_rank_bg_2), PorterDuff.Mode.SRC);
        rel_3.getBackground().setColorFilter(ContextCompat.getColor(TheLApp.getContext(), R.color.live_look_rank_bg_3), PorterDuff.Mode.SRC);

    }

    public LiveLookRankView initView(List<LiveTopFansBean> topFans, int type) {
        this.topFans = topFans;
        this.type = type;
        initView();
        return this;
    }

    private LiveLookRankView initView() {
        //rel_0.setVisibility(View.VISIBLE);
        //rel_0.setVisibility(View.GONE);
        rel_0.setVisibility(type == TYPE_CAPTURE ? View.VISIBLE : View.GONE);
        rel_1.setVisibility(View.GONE);
        rel_2.setVisibility(View.GONE);
        rel_3.setVisibility(View.GONE);
        if (topFans == null) {
            return this;
        }
        final int length = topFans.size();
        for (int i = 0; i < length; i++) {
            LiveTopFansBean liveTopFansBean = topFans.get(i);
            if (i == 0) {
                rel_1.setVisibility(View.VISIBLE);
                if (liveTopFansBean.isCloaking == 1) {
                    ImageLoaderManager.imageLoader(img_rank_1, R.mipmap.liveroom_invisible);
                } else {
                    ImageLoaderManager.imageLoaderDefaultCircle(img_rank_1, R.mipmap.icon_user, topFans.get(0).avatar, TheLConstants.AVATAR_SMALL_SIZE, TheLConstants.AVATAR_SMALL_SIZE);
                }
            } else if (i == 1) {
                rel_2.setVisibility(View.VISIBLE);
                if (liveTopFansBean.isCloaking == 1) {
                    ImageLoaderManager.imageLoader(img_rank_2, R.mipmap.liveroom_invisible);
                } else {
                    ImageLoaderManager.imageLoaderDefaultCircle(img_rank_2, R.mipmap.icon_user, topFans.get(1).avatar, TheLConstants.AVATAR_SMALL_SIZE, TheLConstants.AVATAR_SMALL_SIZE);
                }

            } else if (i == 2) {
                rel_3.setVisibility(View.VISIBLE);
                if (liveTopFansBean.isCloaking == 1) {
                    ImageLoaderManager.imageLoader(img_rank_3, R.mipmap.liveroom_invisible);
                } else {
                    ImageLoaderManager.imageLoaderDefaultCircle(img_rank_3, R.mipmap.icon_user, topFans.get(2).avatar, TheLConstants.AVATAR_SMALL_SIZE, TheLConstants.AVATAR_SMALL_SIZE);
                }

            }
        }
        return this;
    }

    public LiveLookRankView initView(List<LiveTopFansBean> topFans) {
        this.topFans = topFans;
        initView();

        return this;
    }

    private void setListener() {
        rel_1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (topFans.size() > 0 && mOnUserClickListener != null) {
                    LiveTopFansBean liveTopFansBean = topFans.get(0);
                    if (liveTopFansBean.isCloaking == 0) {
                        mOnUserClickListener.onUserClick(liveTopFansBean);
                    }
                }
            }
        });
        rel_2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (topFans.size() > 1 && mOnUserClickListener != null) {
                    LiveTopFansBean liveTopFansBean = topFans.get(1);
                    if (liveTopFansBean.isCloaking == 0) {
                        mOnUserClickListener.onUserClick(liveTopFansBean);
                    }
                }
            }
        });
        rel_3.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (topFans.size() > 2 && mOnUserClickListener != null) {
                    LiveTopFansBean liveTopFansBean = topFans.get(2);
                    if (liveTopFansBean.isCloaking == 0) {
                        mOnUserClickListener.onUserClick(liveTopFansBean);
                    }
                }
            }
        });
    }

    public void setRankListener(OnClickListener listener) {
        rel_0.setOnClickListener(listener);
    }

    public void setOnUserClickListener(OnUserClickListener listener) {
        mOnUserClickListener = listener;
    }

    public interface OnUserClickListener {
        void onUserClick(LiveTopFansBean bean);
    }

}
