package com.thel.modules.main.me.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.modules.main.me.bean.IncomeRecord;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by lingwei on 2017/10/19.
 */

public class RevenueDetailsAdapter extends BaseAdapter {

    private ArrayList<IncomeRecord> incomeRecords = new ArrayList<>();
    private LayoutInflater mInflater;

    public RevenueDetailsAdapter(ArrayList<IncomeRecord> records) {
        mInflater = (LayoutInflater) TheLApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.incomeRecords = records;
    }

    @Override
    public int getCount() {
        return incomeRecords.size();
    }

    @Override
    public Object getItem(int position) {
        return incomeRecords.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void updateSingleRow(ListView listView, int position) {
        int firstVisiblePosition = listView.getFirstVisiblePosition();
        int lastVisiblePosition = listView.getLastVisiblePosition();
        if (position >= firstVisiblePosition && position <= lastVisiblePosition) {
            View view = listView.getChildAt(position - firstVisiblePosition);
            if (view.getTag() instanceof HoldView) {
                HoldView holdView = (HoldView) view.getTag();
                refreshItem(position - listView.getHeaderViewsCount(), holdView);
            }
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        HoldView holdView = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.revenue_record_listitem, parent, false);
            holdView = new HoldView();

            holdView.rel_header = convertView.findViewById(R.id.rel_header);
            holdView.txt_date = convertView.findViewById(R.id.txt_date);
            holdView.txt_desc = convertView.findViewById(R.id.txt_desc);
            holdView.txt_delta = convertView.findViewById(R.id.txt_delta);
            holdView.divider = convertView.findViewById(R.id.divider);
            holdView.txt_unit = convertView.findViewById(R.id.txt_unit);
            holdView.last_divider = convertView.findViewById(R.id.last_divider);

            convertView.setTag(holdView); // 把holdview缓存下来
        } else {
            holdView = (HoldView) convertView.getTag();
        }

        refreshItem(position, holdView);
        return convertView;
    }

    private void refreshItem(int position, final HoldView holdView) {

        final IncomeRecord incomeRecord = incomeRecords.get(position);

        holdView.txt_unit.setText(TheLApp.getContext().getString(R.string.royalties));

        if (incomeRecord.showHeader) {
            holdView.txt_date.setText(incomeRecord.date);
            holdView.rel_header.setVisibility(View.VISIBLE);
            holdView.divider.setVisibility(View.GONE);
        } else {
            holdView.rel_header.setVisibility(View.GONE);
            holdView.divider.setVisibility(View.VISIBLE);
        }

        if (position == incomeRecords.size() - 1) {
            holdView.last_divider.setVisibility(View.VISIBLE);
        } else {
            holdView.last_divider.setVisibility(View.GONE);
        }
        String time = parseTime(incomeRecord);

        holdView.txt_desc.setText(time + " " + incomeRecord.description);
        try {
            holdView.txt_delta.setText("+" + String.format("%.2f", incomeRecord.gemDelta));
        } catch (Exception e) {
            holdView.txt_delta.setText("+" + incomeRecord.gemDelta);
        }

    }

    public String parseTime(IncomeRecord incomeRecord) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date d;
        try {
            d = df.parse(incomeRecord.createTime);
            DateFormat dfTime = new SimpleDateFormat("HH:mm");
            String time = dfTime.format(d);
            return time;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return "";
    }

    class HoldView {
        TextView txt_date;
        TextView txt_desc;
        TextView txt_delta;
        TextView divider;
        TextView last_divider;
        TextView txt_unit;
        RelativeLayout rel_header;
    }
}
