package com.thel.modules.live.surface.watch.horizontal;

import android.graphics.PixelFormat;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;

import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.base.IntentFilterActivity;

import me.rela.rf_s_bridge.new_router.NewUniversalRouter;
import me.rela.rf_s_bridge.router.BoostFlutterView;
import me.rela.rf_s_bridge.router.UniversalRouter;

public class LiveWatchHorizontalActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_frame_normal);

        getWindow().setFormat(PixelFormat.TRANSLUCENT);//解决surfaceveiw第一次进入的时候会有闪屏现象
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }

        initFragment();

    }

    @Override
    public boolean isStatusBarTranslucent() {
        return true;
    }

    private void initFragment() {

        FragmentManager fragmentManager = getSupportFragmentManager();

        LiveWatchHorizontalFragment liveWatchHorizontalFragment = (LiveWatchHorizontalFragment) fragmentManager.findFragmentByTag(LiveWatchHorizontalFragment.class.getName());

        if (liveWatchHorizontalFragment == null) {
            liveWatchHorizontalFragment = LiveWatchHorizontalFragment.getFragment();
        }

        if (!liveWatchHorizontalFragment.isAdded()) {
            fragmentManager.beginTransaction().add(R.id.frame_content, liveWatchHorizontalFragment, LiveWatchHorizontalFragment.class.getName()).commit();
        }

    }


    private void setFullScreen(boolean enable) {
        if (enable) {
            WindowManager.LayoutParams attrs = getWindow().getAttributes();
            attrs.flags |= WindowManager.LayoutParams.FLAG_FULLSCREEN;
            getWindow().setAttributes(attrs);
        } else {
            WindowManager.LayoutParams attrs = getWindow().getAttributes();
            attrs.flags &= ~WindowManager.LayoutParams.FLAG_FULLSCREEN;
            getWindow().setAttributes(attrs);
        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                ViewGroup vp = findViewById(android.R.id.content);
                View view = vp.getChildAt(vp.getChildCount() - 1);
                if (view instanceof BoostFlutterView) {
                    vp.removeViewAt(vp.getChildCount() - 1);
                    NewUniversalRouter.sharedInstance.nativePop();

                } else
                    finish();// 关闭页面
            }
        }
        return true;
    }

}
