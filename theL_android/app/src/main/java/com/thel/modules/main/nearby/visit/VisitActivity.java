package com.thel.modules.main.nearby.visit;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.bean.WhoSeenMeBean;
import com.thel.bean.WhoSeenMeListBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.imp.vip.VipStatusChangerListener;
import com.thel.modules.main.me.bean.LogInfoBean;
import com.thel.modules.main.me.match.eventcollect.collect.LiveLogUtils;
import com.thel.modules.main.me.match.eventcollect.collect.MatchLogUtils;
import com.thel.modules.main.me.match.eventcollect.collect.SocialLogUtils;
import com.thel.modules.main.messages.utils.PushUtils;
import com.thel.modules.main.settings.SettingsActivity;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.modules.others.VipConfigActivity;
import com.thel.ui.decoration.DefaultItemDivider;
import com.thel.ui.widget.VisitHistoryLayout;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by waiarl on 2017/9/22.
 */

public class VisitActivity extends BaseActivity implements VisitContract.View, VipStatusChangerListener {
    private LinearLayout lin_back;
    private TextView txt_title;
    //    private TabLayout tablayout;
    //    private ViewPager viewpager;
    //    private List<ViewGroup> list = new ArrayList<>();
    private RelativeLayout whoSeenMeView;
    //    private RelativeLayout visitView;
    private RecyclerView WholeSeenMeRecyclerView;
    //    private RecyclerView visitRecyclerView;
    //    private List<String> titleList = new ArrayList<>();
    //    private VisitAdapter adapter;
    private VisitViewAdapter whoSeenMeAdapter;
    private LinearLayoutManager whoSeenMeManager;
    //    private VisitViewAdapter visitViewAdapter;
    //    private LinearLayoutManager visitViewManager;
    private List<WhoSeenMeBean> whoSeenMeBeanList = new ArrayList<>();
    //    private List<WhoSeenMeBean> visitBeanList = new ArrayList<>();
    private VisitContract.Presenter presenter;
    private View whoSeenMeEmptyView;
    //    private View visitEmptyView;
    private View whoSeenMeHeaderView;
    private View whoSeenMeFooterView;
    //    private View visitViewFoooterView;
    private TextView whoSeenMe_txt_count;
    private TextView tx_presentation;
    private LinearLayout show_notification;
    private String str1;
    private boolean first_record;
    private RelativeLayout empty_view;
    private VisitHistoryLayout mVisitHistoryLayout;
    private VisitHistoryLayout visit_layout;
    private LinearLayout ll_anonymous_browsing;
    private ImageView iv_close;
    private LinearLayout ll_turn_on_or_off;
    private ImageView img_off_or_on;
    private String pageId;
    private String from_page;
    private String from_page_id;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visit);
        new VisitPresenter(this);
        findViewById();
        initData();
        setListener();

        pageId = Utils.getPageId();

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void initData() {
        //        titleList.clear();
        //        titleList.add(getString(R.string.whoseenme_activity_title));
        //        titleList.add(getString(R.string.whoseenme_activity_i_saw_whom));
        int tagVisit = ShareFileUtils.getInt(ShareFileUtils.recordPush, 1);

        if (tagVisit == 1) {
            img_off_or_on.setImageResource(R.mipmap.lookme_icon_push_on);

        } else {
            img_off_or_on.setImageResource(R.mipmap.lookme_icon_push_off);

        }
        loadNetData();
        Intent intent = getIntent();
        if (intent != null) {
            from_page = intent.getStringExtra(TheLConstants.FROM_PAGE);
            from_page_id = intent.getStringExtra(TheLConstants.FROM_PAGE_ID);

        }


    }


    private void findViewById() {
        lin_back = findViewById(R.id.lin_back);
        txt_title = findViewById(R.id.txt_title);
        //  show_notification = findViewById(R.id.ll_notification);
        //  tx_presentation = findViewById(R.id.tx_presentation);
        ll_turn_on_or_off = findViewById(R.id.ll_turn_on_or_off);
        img_off_or_on = findViewById(R.id.img_off_or_on);
        empty_view = findViewById(R.id.empty_view);
        visit_layout = findViewById(R.id.visit_layout);
        //        tablayout = (TabLayout) findViewById(R.id.tablayout);
        //        viewpager = (ViewPager) findViewById(R.id.viewpager);
        initViewData();
        //        setTabs();
    }

    private void initViewData() {
        txt_title.setText(getString(R.string.whoseenme_activity_title));
        //        txt_title.setText(getString(R.string.visit_activity_title));
        //我看过谁
        //谁看过我
        whoSeenMeView = new RelativeLayout(this);
        WholeSeenMeRecyclerView = new RecyclerView(this);
        whoSeenMeAdapter = new VisitViewAdapter(whoSeenMeBeanList, true);
        whoSeenMeManager = new LinearLayoutManager(this);
        whoSeenMeManager.setOrientation(LinearLayoutManager.VERTICAL);
        WholeSeenMeRecyclerView.setLayoutManager(whoSeenMeManager);
        WholeSeenMeRecyclerView.setAdapter(whoSeenMeAdapter);
        WholeSeenMeRecyclerView.addItemDecoration(new DefaultItemDivider(this, LinearLayoutManager.VERTICAL, R.color.gray, 1, true, true, true));
        whoSeenMeView.addView(WholeSeenMeRecyclerView, RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        //  whoSeenMeEmptyView = getLayoutInflater().inflate(R.layout.bg_whoseenme_default, null);

        //whoSeenMeView.addView(whoSeenMeEmptyView, RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        whoSeenMeHeaderView = getLayoutInflater().inflate(R.layout.visit_last_seven_days_view_count, null);
        whoSeenMe_txt_count = whoSeenMeHeaderView.findViewById(R.id.text);
        whoSeenMeFooterView = getLayoutInflater().inflate(R.layout.visit_footer_view, null);
        whoSeenMeAdapter.addHeaderView(whoSeenMeHeaderView);
        ll_anonymous_browsing = whoSeenMeHeaderView.findViewById(R.id.ll_anonymous_browsing);
        iv_close = whoSeenMeHeaderView.findViewById(R.id.iv_close);
        //        if (TheLConstants.level < 1) {//如果不是会员
        ////            whoSeenMeAdapter.addFooterView(whoSeenMeFooterView);
        ////        }

        whoSeenMeHeaderView.setVisibility(View.GONE);
        ((FrameLayout) findViewById(R.id.container)).addView(whoSeenMeView);
    }

    //    private void setTabs() {
    //        addTabViews();
    //        adapter = new VisitAdapter(list, titleList);
    //        viewpager.setAdapter(adapter);
    //        final int size = titleList.size();
    //        for (int i = 0; i < size; i++) {
    //            tablayout.addTab(tablayout.newTab());
    //        }
    //        tablayout.setupWithViewPager(viewpager);
    //    }

    //    private void addTabViews() {
    //        list.clear();
    //        //谁看过我
    //        whoSeenMeView = new RelativeLayout(this);
    //        WholeSeenMeRecyclerView = new RecyclerView(this);
    //        whoSeenMeAdapter = new VisitViewAdapter(whoSeenMeBeanList, true);
    //        whoSeenMeManager = new LinearLayoutManager(this);
    //        whoSeenMeManager.setOrientation(LinearLayoutManager.VERTICAL);
    //        WholeSeenMeRecyclerView.setLayoutManager(whoSeenMeManager);
    //        WholeSeenMeRecyclerView.setAdapter(whoSeenMeAdapter);
    //        WholeSeenMeRecyclerView.addItemDecoration(new DefaultItemDivider(this, LinearLayoutManager.VERTICAL, R.color.gray, 1, true, true, true));
    //        whoSeenMeView.addView(WholeSeenMeRecyclerView, RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
    //        whoSeenMeEmptyView = getLayoutInflater().inflate(R.layout.bg_whoseenme_default, null);
    //        whoSeenMeView.addView(whoSeenMeEmptyView, RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
    //        whoSeenMeHeaderView = getLayoutInflater().inflate(R.layout.visit_last_seven_days_view_count, null);
    //        whoSeenMe_txt_count = whoSeenMeHeaderView.findViewById(R.id.text);
    //        whoSeenMeFooterView = getLayoutInflater().inflate(R.layout.visit_footer_view, null);
    //        whoSeenMeAdapter.addHeaderView(whoSeenMeHeaderView);
    //        if (TheLConstants.level < 1) {//如果不是会员
    //            whoSeenMeAdapter.addFooterView(whoSeenMeFooterView);
    //        }
    //        whoSeenMeHeaderView.setVisibility(View.GONE);
    //
    //        //我看过谁
    //        visitView = new RelativeLayout(this);
    //        visitRecyclerView = new RecyclerView(this);
    //        visitViewAdapter = new VisitViewAdapter(visitBeanList, false);
    //        visitViewManager = new LinearLayoutManager(this);
    //        visitViewManager.setOrientation(LinearLayoutManager.VERTICAL);
    //        visitRecyclerView.setLayoutManager(visitViewManager);
    //        visitRecyclerView.setAdapter(visitViewAdapter);
    //        visitRecyclerView.addItemDecoration(new DefaultItemDivider(this, LinearLayoutManager.VERTICAL, R.color.gray, 1, true, true, true));
    //        visitView.addView(visitRecyclerView, RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
    //        visitEmptyView = LayoutInflater.from(this).inflate(R.layout.bg_isawwhom_default, null);
    //        visitView.addView(visitEmptyView, RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
    //        visitViewFoooterView = getLayoutInflater().inflate(R.layout.visit_footer_view, null);
    //        if (TheLConstants.level < 1) {
    //            visitViewAdapter.addFooterView(visitViewFoooterView);
    //        }
    //        Collections.addAll(list, whoSeenMeView, visitView);
    //    }

    private void loadNetData() {
        presenter.getWhoSeenMeData();
        showLoading();
    }

    private void setListener() {
        lin_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PushUtils.finish(VisitActivity.this);
            }
        });
        whoSeenMeAdapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                gotoUserInfoActivity(whoSeenMeAdapter.getItem(position));
            }
        });

        whoSeenMeFooterView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoVipConfigActivity();
                traceNearbyUserLog("visitclickvip");

            }
        });

        ll_anonymous_browsing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(VisitActivity.this, VipConfigActivity.class);
                intent.putExtra("showType", VipConfigActivity.SHOW_ANONYMOUS_BROWSING);
                intent.putExtra("fromPage", "visit.who");
                intent.putExtra("fromPageId", pageId);
                startActivity(intent);
                traceNearbyUserLog("visitAD");
            }
        });
        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isClosed = ShareFileUtils.getBoolean(ShareFileUtils.CLOSE_NONYMOUS_BROWSING, false);

                if (!isClosed) {
                    ShareFileUtils.setBoolean(ShareFileUtils.CLOSE_NONYMOUS_BROWSING, true);
                }
                ll_anonymous_browsing.setVisibility(View.GONE);
                //保存当前点击的时间
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");// HH:mm:ss
                Date date = new Date(System.currentTimeMillis());
                ShareFileUtils.setString(ShareFileUtils.BEFORE_TIME, simpleDateFormat.format(date));


            }
        });
        ll_turn_on_or_off.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(VisitActivity.this, SettingsActivity.class);
                intent.putExtra(SettingsActivity.PAGE_TYPE_TAG, SettingsActivity.PAGE_TYPE_PUSH);
                startActivityForResult(intent, TheLConstants.BUNDLE_CODE_SETTINGS_ACTIVITY);

            }
        });
    }

    /**
     * 跳转到购买会员界面
     */
    private void gotoVipConfigActivity() {
        final Intent intent = new Intent(this, VipConfigActivity.class);
        intent.putExtra("fromPage", "visit.who");
        intent.putExtra("fromPageId", pageId);

        startActivity(intent);
    }

    /**
     * 跳转到个人中心
     *
     * @param whoSeenMeBean
     */
    private void gotoUserInfoActivity(WhoSeenMeBean whoSeenMeBean) {
        if (whoSeenMeBean == null) {
            return;
        }
//        final Intent intent = new Intent(this, UserInfoActivity.class);
//        intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, whoSeenMeBean.id + "");
//        startActivity(intent);
        FlutterRouterConfig.Companion.gotoUserInfo(whoSeenMeBean.id + "");
        SocialLogUtils.getInstance().traceSocialData("visit.who", pageId, "head", from_page, from_page_id);

    }

    @Override
    public void setPresenter(VisitContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showWhoSeenMeData(WhoSeenMeListBean whoSeenMeBeanList) {
        this.whoSeenMeBeanList.clear();
        this.whoSeenMeBeanList.addAll(whoSeenMeBeanList.map_list);
        whoSeenMeAdapter.notifyDataSetChanged();

        boolean isClosed = ShareFileUtils.getBoolean(ShareFileUtils.CLOSE_NONYMOUS_BROWSING, false);
        boolean isDifferTwentyfour = getDifferDays();

        if (ll_anonymous_browsing != null) {
            if (!isClosed && isDifferTwentyfour) {
                if (this.whoSeenMeBeanList.size() >= 5 && UserUtils.getUserVipLevel() == 0) {
                    ll_anonymous_browsing.setVisibility(View.VISIBLE);
                    SocialLogUtils.getInstance().traceSocialData("visit.who", pageId, "exposure", from_page, from_page_id);


                } else {
                    ll_anonymous_browsing.setVisibility(View.GONE);
                }

            } else {
                ll_anonymous_browsing.setVisibility(View.GONE);

            }
        }

        if (whoSeenMeBeanList.moreGirls != null) {
            if (UserUtils.getUserVipLevel() == 0) {
                if (whoSeenMeBeanList.moreGirls.size() > 0) {

                    if (mVisitHistoryLayout == null) {

                        mVisitHistoryLayout = new VisitHistoryLayout(VisitActivity.this);

                        mVisitHistoryLayout.setReportPage(pageId,"visit.who");

                        whoSeenMeAdapter.addFooterView(mVisitHistoryLayout);

                    }

                    mVisitHistoryLayout.setImage(whoSeenMeBeanList.moreGirls);

                    visit_layout.setVisibility(View.GONE);

                } else {

                    if (whoSeenMeAdapter.getFooterLayoutCount() > 0) {

                        whoSeenMeAdapter.removeAllFooterView();
                    }

                    visit_layout.setVisibility(View.VISIBLE);
                }

            } else {

                if (whoSeenMeAdapter.getFooterLayoutCount() > 0) {

                    whoSeenMeAdapter.removeAllFooterView();
                }

                visit_layout.setVisibility(View.GONE);

            }
        }
        // whoSeenMeEmptyView.setVisibility(View.GONE);
        whoSeenMeHeaderView.setVisibility(View.VISIBLE);
        whoSeenMe_txt_count.setText(getString(R.string.last_seven_days_view_count, whoSeenMeBeanList.viewCount));
        if (empty_view != null) {
            empty_view.setVisibility(View.GONE);
        }
    }

    @Override
    public void showVisitData(List<WhoSeenMeBean> whoSeenMeBeanList) {
        //        this.visitBeanList.clear();
        //        this.visitBeanList.addAll(whoSeenMeBeanList);
        //        visitViewAdapter.notifyDataSetChanged();
        //        visitEmptyView.setVisibility(View.GONE);
    }

    @Override
    public void EmptyWhoSeenMeData() {
        if (empty_view != null) {
            empty_view.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void EmptyVisitData() {
        //        visitEmptyView.setVisibility(View.VISIBLE);
    }

    @Override
    public void requestFinish() {
        if (!isFinishing()) {
            closeLoading();
        }

    }

    @Override
    public void onVipStatusChanged(boolean isVip, int vipLevel) {
        if (isVip) {
            whoSeenMeAdapter.removeAllFooterView();
            //            visitViewAdapter.removeAllFooterView();
            loadNetData();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            initData();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        PushUtils.finish(VisitActivity.this);
    }


    public void traceNearbyUserLog(String activity) {
        try {

            LogInfoBean logInfoBean = new LogInfoBean();
            logInfoBean.page = "visit.who";
            logInfoBean.page_id = pageId;
            logInfoBean.activity = activity;

            MatchLogUtils.getInstance().addLog(logInfoBean);

        } catch (Exception e) {

        }
    }

    public boolean getDifferDays() {
        String end_time = ShareFileUtils.getString(ShareFileUtils.BEFORE_TIME, "");
        L.d("VisitTime", "end_time" + end_time);
        boolean isClosed = ShareFileUtils.getBoolean(ShareFileUtils.CLOSE_NONYMOUS_BROWSING, false);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");
        if (end_time.isEmpty()) {
            return true;
        } else {
            try {
                //Date nowTime = new Date(System.currentTimeMillis());

                long nowTime = new Date(System.currentTimeMillis()).getTime();
                long lastTime = simpleDateFormat.parse(end_time).getTime();
                //获取时间差
                long deffer = nowTime - lastTime;
                long day = deffer / (24 * 60 * 60 * 1000);
                long hour = (deffer / (60 * 60 * 1000) - day * 24);
                long min = ((deffer / (60 * 1000)) - day * 24 * 60 - hour * 60);
                long s = (deffer / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
                L.d("VisitTime", "day" + day + "hour" + hour + "min" + min + "s" + s);

                //如果已经点击过，并且已经到达了24个小时 就重新返回一个true 表示展示出来
                if (isClosed && hour == 24) {
                    ShareFileUtils.setBoolean(ShareFileUtils.CLOSE_NONYMOUS_BROWSING, false);
                    return true;
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
        return false;
    }

}
