package com.thel.modules.main.home.search;

import android.os.Bundle;
import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thel.imp.BaseFunctionFragment;
import com.umeng.analytics.MobclickAgent;

import butterknife.ButterKnife;

/**
 * Created by chad
 * Time 17/10/25
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */

public abstract class BaseSearchFragment extends BaseFunctionFragment implements SearchActivity.SearchKeyChangedListener {

    protected String mType;

    protected boolean isCreated = false;// 用来判断页面是不是创建了，因为在onCreate前会先调一次setUserVisibleHint

    protected boolean isViewCreated = false;

    protected String mKey;

    protected boolean mIsLatestKey = false;//是否是最新关键字，false

    protected int curPage = 1;

    protected int currentCountForOnce = -1;

    protected int refreshType = 0;

    protected View mEmptyView;//空白页面显示

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            mType = bundle.getString(SEARCH_TYPE);
        }
        isCreated = true;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isCreated) {
            return;
        }
        if (mIsLatestKey && isVisibleToUser) {
            refresh();
        }
        if (isVisibleToUser) {
            MobclickAgent.onPageStart(this.getClass().getName() + ":" + mType);
        } else {
            MobclickAgent.onPageEnd(this.getClass().getName() + ":" + mType);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        isViewCreated = true;
        return inflater.inflate(setContentView(), container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initView();

        initData();
    }

    @LayoutRes
    protected abstract int setContentView();

    protected abstract void initView();

    protected abstract void initData();

    @Override
    public boolean isLatestKey() {
        return mIsLatestKey;
    }

    @Override
    public void setSearchKey(String key) {
        mKey = key;
        mIsLatestKey = true;
    }

    @Override
    public void refresh() {
        if (!isViewCreated) {
            return;
        }
        mIsLatestKey = false;
        mEmptyView.setVisibility(View.GONE);
    }

    @Override
    public void onDestroyView() {
        isViewCreated = false;
        super.onDestroyView();
    }

    protected void processBusiness(int Page, int type) {
        this.curPage = Page;
        refreshType = type;
    }
}
