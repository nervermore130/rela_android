package com.thel.modules.main.discover.bean;

import com.thel.base.BaseDataBean;
import com.thel.bean.user.SimpleUserBean;

import java.io.Serializable;
import java.util.ArrayList;

public class LiveRoomsEntranceBean extends BaseDataBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 当前正在直播的人数
     */
    public int totalBroadcasters;

    /**
     * top3的播主
     */
    public ArrayList<SimpleUserBean> broadcasters = new ArrayList<>();

    /**
     * 直播文案
     */
    public String text;

    /**
     * 从json对象封装对象
     *
     * @param tempobj
     *//*
    public void fromJson(JSONObject tempobj) {
        try {
            totalBroadcasters = JsonUtils.getInt(tempobj, "totalBroadcasters", 0);
            text = JsonUtils.getString(tempobj, "text", "");

            JSONArray jsonArray = JsonUtils.getJSONArray(tempobj, "broadcasters");
            for (int i = 0; i < jsonArray.length(); i++) {
                SimpleUserBean simpleUserBean = new SimpleUserBean();
                simpleUserBean.fromJson(jsonArray.getJSONObject(i));
                broadcasters.add(simpleUserBean);
            }
        } catch (Exception e) {
            if (e.getMessage() != null) {
                Log.e(MomentsBean.class.getName(), e.getMessage());
            }
        }
    }*/

}
