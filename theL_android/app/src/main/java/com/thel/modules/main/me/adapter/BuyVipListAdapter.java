package com.thel.modules.main.me.adapter;

import android.graphics.Paint;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.constants.TheLConstants;
import com.thel.modules.main.me.bean.VipBean;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

import static com.thel.utils.StringUtils.getString;

/**
 * Created by chad
 * Time 17/10/26
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */

public class BuyVipListAdapter extends BaseRecyclerViewAdapter<VipBean> {

    private double normalPrice;
    private OnItemChildClickListener mChildListener;
    private String channel = "";//abtest使用

    public BuyVipListAdapter(List<VipBean> data, double normalPrice) {
        super(R.layout.buy_vip_list_item, data);
        this.normalPrice = normalPrice;
        mChildListener = new OnItemChildClickListener();
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public void setNormalPrice(double normalPrice) {
        this.normalPrice = normalPrice;
    }

    @Override
    protected void convert(BaseViewHolder helper, VipBean item) {

        if (item.isPendingPrice) {
            helper.setVisibility(R.id.lin_middle, View.INVISIBLE);
            helper.setVisibility(R.id.txt_operation, View.INVISIBLE);
            helper.setVisibility(R.id.pb_pending, View.VISIBLE);
        } else {
            helper.setVisibility(R.id.lin_middle, View.VISIBLE);
            helper.setVisibility(R.id.txt_operation, View.VISIBLE);
            helper.setVisibility(R.id.pb_pending, View.GONE);
        }
        helper.setText(R.id.txt_name, getTitle(item));
        if (isHAWEIABTest()) {
            helper.setVisibility(R.id.txt_desc, View.GONE);
        } else {
            //原价加横线
            helper.setVisibility(R.id.txt_desc, View.VISIBLE);
            TextView txt_desc = helper.convertView.findViewById(R.id.txt_desc);
            txt_desc.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
//            helper.setText(R.id.txt_desc, getDesc(item));
            helper.setText(R.id.txt_desc, item.summary);
        }
        helper.setText(R.id.txt_operation, getOpera(item));

        /**
         *
         * 4.10.0 如果新会员 vivo显示
         * **/
        if (!TextUtils.isEmpty(item.groupDesc)) {
            helper.setBackgroundRes(R.id.txt_operation, R.drawable.bg_radius_pink);
            helper.setVisibility(R.id.txt_new_desc, View.VISIBLE);
            helper.setVisibility(R.id.img_type, View.VISIBLE);
            helper.setImageResource(R.id.img_type, R.mipmap.vip_pay_icon_hot);
            helper.getConvertView().setBackgroundColor(ContextCompat.getColor(TheLApp.context, R.color.vip_new_item_bg));
        } else {
            helper.setBackgroundRes(R.id.txt_operation, R.drawable.bg_radius_gold);
            helper.setVisibility(R.id.txt_new_desc, View.GONE);
            if (isHAWEIABTest()) {
                if (item.iapId.equals("7d")) {
                    helper.setVisibility(R.id.img_type, View.VISIBLE);
                    helper.setImageResource(R.id.img_type, R.mipmap.icn_stickershop_hot);
                    helper.getConvertView().setBackgroundColor(ContextCompat.getColor(TheLApp.context, R.color.light_gold));
                } else {
                    helper.setVisibility(R.id.img_type, View.GONE);
                    helper.getConvertView().setBackgroundColor(ContextCompat.getColor(TheLApp.context, R.color.transparent));
                }
            } else {
                if (item.isHot == 1) {
                    helper.setVisibility(R.id.img_type, View.VISIBLE);
                    helper.setImageResource(R.id.img_type, R.mipmap.icn_stickershop_hot);
                    helper.getConvertView().setBackgroundColor(ContextCompat.getColor(TheLApp.context, R.color.light_gold));
                } else {
                    helper.setVisibility(R.id.img_type, View.GONE);
                    helper.getConvertView().setBackgroundColor(ContextCompat.getColor(TheLApp.context, R.color.transparent));
                }
            }
        }

        if (!TextUtils.isEmpty(item.icon)) {
            helper.setImageUrl(R.id.img, item.icon, TheLConstants.AVATAR_MIDDLE_SIZE, TheLConstants.AVATAR_MIDDLE_SIZE);
        }
        helper.setOnItemChildClickListener(R.id.rl_operation, mChildListener);
        helper.setTag(R.id.rl_operation, helper.getLayoutPosition());
    }

    /**
     * 获取描述
     *
     * @param vipBean
     * @return
     */
    private String getDesc(VipBean vipBean) {
        final double discount = Double.valueOf(vipBean.price) / (vipBean.months * normalPrice);
        final String dis = NumberFormat.getPercentInstance().format(1 - discount);
        if (TextUtils.isEmpty(vipBean.googlePrice)) {
            final String totalPrice = "￥" + new DecimalFormat("0").format(Double.valueOf(vipBean.months * normalPrice) / 100);
            if (vipBean.months > 1) {
                return getString(R.string.vip_original_price, totalPrice);
            } else {
                if (vipBean.months == 1 && !TextUtils.isEmpty(vipBean.groupDesc)) {
                    return getString(R.string.vip_original_price, totalPrice);
                }

                return "";
            }
        } else {
            if (vipBean.months > 1) {
                final String unit = getUnitFomPrice(vipBean.googlePrice);
                final String totalPrice = unit + new DecimalFormat("0").format(Double.valueOf(vipBean.months * normalPrice) / 100);
                return getString(R.string.vip_original_price, totalPrice);
            } else {
                return "";
            }
        }
    }

    /**
     * 获取标题
     *
     * @param vipBean return
     */
    private String getTitle(VipBean vipBean) {
        if (vipBean.iapId.equals("7d")) {
            return vipBean.title;
        }
        try {
            if (vipBean.months > 1) {
                return getString(R.string.buy_vip_months, vipBean.months);
            } else {
                return getString(R.string.buy_vip_month);
            }
        } catch (Exception e) {
            return getString(R.string.buy_vip_month);
        }
    }

    /**
     * 获取折后的价格
     */
    private String getOpera(VipBean vipBean) {
        if (TextUtils.isEmpty(vipBean.googlePrice)) {
            Double price = Double.valueOf(vipBean.price);
            String pattern = "0";
            if (price < 100) pattern = "0.00";
            String result;
            if (vipBean.months == 1 || (!TextUtils.isEmpty(vipBean.groupDesc))) {
                result = "￥" + new DecimalFormat(pattern).format((price / 100));
            } else {
                result = "￥" + new DecimalFormat(pattern).format(Double.valueOf(vipBean.price) / 100);
            }
            if (price < 100) result = result.substring(0, result.length() - 1);
            return result;
        } else {
            return vipBean.googlePrice;
        }
    }

    private String getUnitFomPrice(String googlePrice) {
        final int length = googlePrice.length();
        final int start = 0;
        int end = 0;
        for (int i = 0; i < length; i++) {
            if (googlePrice.charAt(i) >= 48 && googlePrice.charAt(i) <= 57) {
                end = i;
                break;
            }
        }
        return googlePrice.substring(start, end);
    }

    private boolean isHAWEIABTest() {
        if (!TextUtils.isEmpty(channel) && channel.equals("huawei")) {
            for (int i = 0; i < mData.size(); i++) {
                if (mData.get(i).iapId.equals("7d")) {
                    return true;
                }
            }
        }
        return false;
    }
}
