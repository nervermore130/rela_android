package com.thel.modules.select_country;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.constants.TheLConstants;
import com.thel.utils.PinyinUtils;
import com.thel.utils.ViewUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SMSSelectCountryActivity extends BaseActivity implements TextWatcher {

    @BindView(R.id.et_put_identify)
    EditText etSearch;

    @BindView(R.id.clCountry)
    ListView listView;

    @BindView(R.id.llSearch)
    LinearLayout llSearch;

    @BindView(R.id.llTitle)
    RelativeLayout llTitle;

    //手动填写
    @BindView(R.id.txt_write)
    View txt_write;

    private String[] arr_countries;

    private List<String> list_countries = new ArrayList<String>();

    private List<String> filterList = new ArrayList<String>();

    private CountryAdapter adapter;

    private String name;
    private String code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_smsselect_country);


        ButterKnife.bind(this);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_MASK_ADJUST | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        etSearch.addTextChangedListener(this);

        initList();
    }

    private void initList() {
        arr_countries = getResources().getStringArray(R.array.countries);
        list_countries.addAll(Arrays.asList(arr_countries));
        adapter = new CountryAdapter(this, list_countries);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                if (position >= 0 && position < arr_countries.length) {
                    name = (String) view.getTag(R.id.txt_country_name);
                    code = (String) view.getTag(R.id.txt_country_code);
                    finish();
                }
            }
        });
    }

    @OnClick(R.id.back)
    void back() {
        finish();
    }

    @OnClick(R.id.ivSearch)
    void updateUi() {
        // 搜索
        llTitle.setVisibility(View.GONE);
        llSearch.setVisibility(View.VISIBLE);
        etSearch.getText().clear();
        etSearch.requestFocus();
    }

    @OnClick(R.id.iv_clear)
    void clearEdit() {
        etSearch.getText().clear();
    }

    @OnClick(R.id.txt_write)
    void showWrityDialog() {
        final Dialog dialog = new Dialog(this, R.style.CustomDialog);
        final View view = LayoutInflater.from(this).inflate(R.layout.write_country_code_dialog, null);
        dialog.addContentView(view, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        dialog.show();
        final EditText et_code_input = view.findViewById(R.id.et_code_input);
        final View bt_cancel = view.findViewById(R.id.txt_cancel);
        final View bt_ok = view.findViewById(R.id.txt_ok);
        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.hideSoftInput(SMSSelectCountryActivity.this, et_code_input);
                if (dialog != null || dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });
        bt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null || dialog.isShowing()) {
                    dialog.dismiss();
                }
                final String st_code = et_code_input.getText().toString().trim();
                setCode(st_code);
                finish();
            }
        });
        et_code_input.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (dialog != null && dialog.isShowing()) {
                    ViewUtils.showSoftInput(SMSSelectCountryActivity.this, et_code_input);
                }
            }
        }, 300);
    }

    private void setCode(String st_code) {
        for (String country : list_countries) {
            final String[] arr = country.split("\\+");
            String names = arr[0];
            String codes = arr[1];
            if (st_code.equals(codes)) {
                name = names;
                code = codes;
                return;
            }
        }
        code = st_code;
        name = "";
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        filterList.clear();
        if (TextUtils.isEmpty(s.toString().trim())) {
            adapter.refreshAdapter(list_countries);
        } else {
            for (String country : list_countries) {
                if (PinyinUtils.cn2Spell(country).contains(PinyinUtils.cn2Spell(s.toString().trim()))) {
                    filterList.add(country);
                }
            }
            adapter.refreshAdapter(filterList);
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case TheLConstants.BUNDLE_CODE_SMS_IDENTIFY:
                    setResult(RESULT_OK, data);
                    finish();
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void finish() {
        ViewUtils.hideSoftInput(this, etSearch);
        Intent intent = new Intent();
        intent.putExtra("name", name);
        intent.putExtra("code", code);
        setResult(RESULT_OK, intent);
        super.finish();
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }
}
