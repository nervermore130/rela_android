package com.thel.modules.main.me.aboutMe;

import com.thel.base.BasePresenter;
import com.thel.base.BaseView;
import com.thel.bean.CheckUserNameBean;
import com.thel.bean.user.MyImagesListBean;
import com.thel.bean.user.UploadTokenBean;
import com.thel.modules.main.me.bean.MyInfoBean;
import com.thel.modules.main.me.bean.UpdataInfoBean;

/**
 * Created by lingwei on 2017/10/10.
 */

public class UpdateUserInfoContract {
    interface View extends BaseView<UpdateUserInfoContract.Presenter> {

        void getMyInfo(MyInfoBean myInfoBean);

        void getUpdateUserInfo(UpdataInfoBean updataInfoBean);

        void refreshMyPhoto(MyImagesListBean myImagesListBean);

        void uploadToken(UploadTokenBean uploadTokenBean, String uploadType);

        void addImage(UpdataInfoBean updataInfoBean);

        void closeLoadingDialog();

        void checkUserNameResult(CheckUserNameBean data);

        void sortImagesSuccess();

        void popAlerSensitiveWords(String errdesc);

        void onRequestFinished();
    }

    interface Presenter extends BasePresenter {
        void getDataFromPrepage();

        void loadMyInfo();

        void checkUserName(String username);

        void updateUserInfo(String username, String birth, String lookingForRoleSB, String careerType, String avatarUrl, String nickname, String height, String weight, String role_cur_pos, String relationship_cur_pos, String purposeSB, String ethnicity_cur_pos, String occupation, String livecity,  String intro);

        void getMyImagesList(String pageSize, String curPage);

        void getUploadToken(String clientTime, String bucket, String path, String uploadType);

        void getVideoUploadToken(String clientTime, String bucket, String path, String gifPath, String gifType, String gifKey);

        void uploadImageUrl(String imageUrl, String imageWidth, String imageHeight);

        void sortImages(String imageIds);
    }
}
