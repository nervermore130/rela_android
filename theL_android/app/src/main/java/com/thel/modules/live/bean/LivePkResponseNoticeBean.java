package com.thel.modules.live.bean;

import java.io.Serializable;

/**
 * Created by waiarl on 2017/12/4.
 * PK发起方收到的PK接收方的回应bean
 */

public class LivePkResponseNoticeBean extends BaseLivePkBean implements Serializable {
    public static final String LIVE_PK_RESPONSE_RESULT_YES = "yes";//直播pk申请回复 同意
    public static final String LIVE_PK_RESPONSE_RESULT_NO = "no";//直播Pk申请回复，不同容易


    public String fromUserId;//pk接收方的用户ID
    public String toUserId;//pk发起方的用户Id
    public String result;//pk回应，yes，同意，no,不同意
    public AgoraBean agora;//声网数据

}
