package com.thel.modules.main.video_discover.autoplay;

import androidx.annotation.NonNull;
import android.text.TextUtils;
import android.util.SparseArray;

import com.thel.bean.video.VideoBean;
import com.thel.player.video.VideoInfoBuilder;
import com.thel.player.video.VideoPlayerProxy2;
import com.thel.utils.VideoUtils;

import java.util.Map;
import java.util.WeakHashMap;

/**
 * Created by waiarl on 2018/3/20.
 */

public class AutoPlayVideoCtrl implements RecyclerViewStateImp {
    private final String mEntry;
    public volatile int mState = State_IDLE;

    private Map<VideoAutoPlayImp, RecyclerViewScrollStateChangedListener> autoPlayImpRecyclerViewScrollStateChangedListenerMap = new WeakHashMap<>();

    private SparseArray<VideoAutoPlayImp> visibleViewList = new SparseArray<>();

    private volatile VideoAutoPlayImp playingView = null;

    public AutoPlayVideoCtrl(String entry) {
        this.mEntry = entry;
    }


    @Override
    public AutoPlayVideoCtrl setState(int state) {
        if (this.mState != state) {
            mState = state;
        }
        if (State_IDLE == mState) {
            visibleViewList.clear();
        }
        for (RecyclerViewScrollStateChangedListener listener : autoPlayImpRecyclerViewScrollStateChangedListenerMap.values()) {
            if (listener != null) {
                listener.stateChanged(state);
            }
        }
        if (State_IDLE == mState) {
            setAutoPlayView();
        }

        return this;
    }

    private void setAutoPlayView() {
        if (!VideoUtils.willAutoPlayVideo()) {
            if (playingView != null) {
                playingView.cancelVideoLoading();
            }
            return;
        }
        VideoAutoPlayImp playingViewX = getAutoPlayingView();
        if (playingViewX != playingView || (playingViewX != null && !TextUtils.isEmpty(playingViewX.getVideoUrl()) && !playingViewX.getVideoUrl().equals(VideoPlayerProxy2.getInstance().getUrl()))) {
            playingView = playingViewX;
            if (playingView != null) {
                autoPlay(playingView);
            }
        } else if (playingViewX != null) {
            continuePlay(playingViewX);
        }
        addToSeenList(playingView);

    }

    private void continuePlay(@NonNull VideoAutoPlayImp playingViewX) {
        if (playingViewX.getContainer() != null) {
            playingViewX.showVideoLoading();
            if (!VideoPlayerProxy2.getInstance().isPlaying()) {
                VideoPlayerProxy2.getInstance().tryResume(playingViewX.getContainer());
            }
        }
    }

    private void autoPlay(@NonNull VideoAutoPlayImp playingView) {
        if (playingView.getContainer() != null) {
            playingView.showVideoLoading();
            VideoInfoBuilder builder = new VideoInfoBuilder.Builder()
                    .setMomentId(playingView.getMomentId())
                    .setVideoContainer(playingView.getContainer())
                    .setUrl(playingView.getVideoUrl())
                    .setVolume(0)
                    .setPosition(playingView.getPosition())
                    .setEntry(mEntry)
                    .build();

            VideoPlayerProxy2.getInstance().play(builder);
        }
    }

    private void addToSeenList(VideoAutoPlayImp videoAutoPlayImp) {
        if (videoAutoPlayImp != null && videoAutoPlayImp.getVideoBean() != null) {
            final VideoBean videoBean = videoAutoPlayImp.getVideoBean();
            VideoSeenUtils.saveOneSeenVideo(videoBean);
        }
    }

    private VideoAutoPlayImp getAutoPlayingView() {
        final int size = visibleViewList.size();
        if (size == 0) {
            return null;
        } else if (size == 1) {
            return visibleViewList.valueAt(0);
        } else {
            final VideoAutoPlayImp videoAutoPlayImp1 = visibleViewList.valueAt(0);
            final VideoAutoPlayImp videoAutoPlayImp2 = visibleViewList.valueAt(1);
            final int percent1 = videoAutoPlayImp1.getVisibleHeightPercent();
            final int percent2 = videoAutoPlayImp2.getVisibleHeightPercent();
            if (percent1 >= percent2) {
                return videoAutoPlayImp1;
            } else {
                return videoAutoPlayImp2;
            }
        }
    }

    public void addVideoAutoPlayView(VideoAutoPlayImp videoAutoPlayImp) {
        autoPlayImpRecyclerViewScrollStateChangedListenerMap.put(videoAutoPlayImp, videoAutoPlayImp.getRecyclerViewScrollStateChangedListener());
    }

    public void addVisibleView(VideoAutoPlayImp videoAutoPlayImp) {
        visibleViewList.put(videoAutoPlayImp.getPosition(), videoAutoPlayImp);
    }

    public VideoAutoPlayImp getPlayingView() {
        return playingView;
    }

    public AutoPlayVideoCtrl stopPlay(VideoAutoPlayImp videoAutoPlayImp) {
        if (playingView == videoAutoPlayImp && VideoPlayerProxy2.getInstance().isPlaying()) {

        }
        return this;
    }

}
