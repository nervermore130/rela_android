package com.thel.modules.main.home.search;

import android.os.Bundle;

import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.thel.R;
import com.thel.base.BaseDispatchTouchActivity;
import com.thel.callback.imp.KeyboardChangeListener;
import com.thel.modules.main.home.search.friend.SearchUserFragment;
import com.thel.modules.main.home.search.tag.SearchTagFragment;
import com.thel.ui.widget.TablayoutCustomView;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchActivity extends BaseDispatchTouchActivity implements View.OnClickListener {

    @BindView(R.id.et_search)
    EditText et_search;

    @BindView(R.id.img_delete)
    ImageView img_delete;

    @BindView(R.id.txt_cancel)
    TextView txt_cancel;

    @BindView(R.id.viewpager)
    ViewPager viewPager;

    @BindView(R.id.view_shade)
    View view_shade;

    @BindView(R.id.tablayout)
    TabLayout tablayout;

    public static final String SEARCH_TYPE_FRIEND = "search_type_friend";//搜索类型，人物

    public static final String SEARCH_TYPE_TAG = "search_type_tag";//搜索类型，标签

    private String[] tabText;

    private String[] edit_hint;

    private SearchKeyChangedListener mSearchKeyChangedListener;

    private List<Fragment> fragments = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTheme(R.style.AppCompatTheme);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_search);

        ButterKnife.bind(this);

        initData();

        initViewPage();

        initView();
    }

    @Override public boolean isStatusBarTranslucent() {
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(this.getClass().getName());
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(this.getClass().getName());
        MobclickAgent.onPause(this);
    }

    @Override
    public void finish() {
        super.finish();
        dismissKeyboard();
    }

    /**
     * 点击屏幕空白处软键盘消失
     *
     * @param ev
     * @return
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (isShouldHideKeyboard(et_search, ev)) {
                dismissKeyboard();
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    /**
     * 根据EditText所在坐标和用户点击的坐标相对比，来判断是否隐藏键盘，因为当用户点击EditText时则不能隐藏
     *
     * @param v
     * @param event
     * @return
     */
    private boolean isShouldHideKeyboard(View v, MotionEvent event) {
        if (v != null && (v instanceof EditText)) {
            int[] l = {0, 0};
            v.getLocationInWindow(l);
            int left = l[0], top = l[1], bottom = top + v.getHeight(), right = left + v.getWidth();
            return !(event.getRawX() > left) || !(event.getRawX() < right) || !(event.getRawY() > top) || !(event.getRawY() < bottom);
        }
        // 如果焦点不是EditText则忽略，这个发生在视图刚绘制完，第一个焦点不在EditText上，和用户用轨迹球选择其他的焦点
        return false;
    }

    /**
     * 关闭键盘
     */
    private void dismissKeyboard() {
        // 取消键盘
        ViewUtils.hideSoftInput(this, et_search);
    }

    private void initData() {
        tabText = new String[]{/*getString(R.string.info_topic),*/
                getString(R.string.search_tab_people),
                getString(R.string.moments_activity_tag)};
        edit_hint = new String[]{getString(R.string.search_activity_content),
                getString(R.string.search_topic_hint)};
    }

    private void initViewPage() {
        viewPager.setOffscreenPageLimit(2);

        FragmentManager manager = getSupportFragmentManager();
        SearchUserFragment mFriendFragment = (SearchUserFragment) manager.findFragmentByTag(SEARCH_TYPE_FRIEND);
        if (mFriendFragment == null) {
            mFriendFragment = SearchUserFragment.newInstance(SEARCH_TYPE_FRIEND);
        }
        SearchTagFragment mTagFragment = (SearchTagFragment) manager.findFragmentByTag(SEARCH_TYPE_TAG);
        if (mTagFragment == null) {
            mTagFragment = SearchTagFragment.newInstance(SEARCH_TYPE_TAG);
        }

        Collections.addAll(fragments, mFriendFragment, mTagFragment);

        SearchAdapter mAdapter = new SearchAdapter(fragments, manager);
        viewPager.setAdapter(mAdapter);

        final int length = tabText.length;

        for (int i = 0; i < length; i++) {
            tablayout.addTab(tablayout.newTab());
        }
        tablayout.setupWithViewPager(viewPager);
        for (int i = 0; i < length; i++) {
            final TablayoutCustomView view = new TablayoutCustomView(this).bindTablayout(tablayout);
            tablayout.getTabAt(i).setCustomView(view);
            view.mTextView.setText(tabText[i]);
        }
        setTabStatus(0);
        tablayout.getTabAt(0).getCustomView().setSelected(true);
    }

    private void initView() {
        txt_cancel.setOnClickListener(this);
        img_delete.setOnClickListener(this);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                setTabStatus(position);
                dismissKeyboard();
                SearchKeyChangedListener listener = (SearchKeyChangedListener) fragments.get(position);

                //   DelateKeyChangedListener delateKeyChangedListener = (DelateKeyChangedListener) fragments.get(position);
                if (listener.isLatestKey()) {//如果是新关键字，说明还没搜索
                    listener.refresh();
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        // 输入法键盘完成事件
        et_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    goSearch();
                    dismissKeyboard();
                    return true;
                }
                return false;
            }
        });
        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    img_delete.setVisibility(View.VISIBLE);
                } else {
                    img_delete.setVisibility(View.GONE);
                }
            }
        });
        new KeyboardChangeListener(this).setKeyBoardListener(new KeyboardChangeListener.KeyBoardListener() {
            @Override
            public void onKeyboardChange(boolean isShow, int keyboardHeight) {//键盘是否弹出监听
                if (isShow) {
                    view_shade.setVisibility(View.VISIBLE);
                } else {
                    view_shade.setVisibility(View.GONE);
                }
            }
        });
        view_shade.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                view_shade.setVisibility(View.GONE);
                return true;
            }
        });
    }

    /**
     * 搜索
     */
    private void goSearch() {
        final String key = et_search.getText().toString().trim();
        final int size = fragments.size();
        for (int i = 0; i < size; i++) {
            if (fragments.get(i) instanceof SearchKeyChangedListener) {
                ((SearchKeyChangedListener) fragments.get(i)).setSearchKey(key);
            }
        }
        if (mSearchKeyChangedListener != null) {
            mSearchKeyChangedListener.refresh();
        }
    }

    /**
     * 设置标签
     *
     * @param currentTab
     */
    public void setTabStatus(int currentTab) {
        if (viewPager.getCurrentItem() != currentTab) {
            viewPager.setCurrentItem(currentTab);
        }
        if (fragments.get(currentTab) instanceof SearchKeyChangedListener) {
            mSearchKeyChangedListener = (SearchKeyChangedListener) fragments.get(currentTab);
        }
        et_search.setHint(edit_hint[currentTab]);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_cancel://取消
                finish();
                break;
            case R.id.img_delete:
                et_search.setText("");
                goSearch();
                break;
        }
    }

    /**
     * 搜索关键字改变
     */
    public interface SearchKeyChangedListener {
        String SEARCH_TYPE = "search_type";

        /**
         * @return （当前已经搜索的）是否是最新的搜索关键字
         * 已经搜索并且刷新数据后，返回false;
         * 刚输入的最近关键字还未搜索，返回true；
         */
        boolean isLatestKey();

        /**
         * 设置搜索关键字
         *
         * @param key 搜索关键字
         */
        void setSearchKey(String key);

        /**
         * 刷新
         */
        void refresh();

    }

}
