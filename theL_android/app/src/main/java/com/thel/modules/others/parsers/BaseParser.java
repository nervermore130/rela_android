package com.thel.modules.others.parsers;

import com.thel.base.BaseDataBean;
import com.thel.utils.JsonUtils;

import org.json.JSONObject;

/**
 * 服务器返回报文的解析基类
 * /v2接口返回结果的结构跟老的不一样，没有result、errorCode、errorDesc了，出错的情况直接会在HTTP code中体现
 *
 * @author zhangwenjia
 * @version 1.0.0
 */
public abstract class BaseParser {

    public abstract BaseDataBean parse(String message) throws Exception;

    /**
     * 解析方法
     *
     * @param message 报文字符串
     * @param base    基础数据bean
     * @return Object 解析后的对象
     * @throws Exception
     */
    public BaseDataBean parse(String message, BaseDataBean base) throws Exception {
        if (message != null && message.length() > 0) {
            JSONObject object = new JSONObject(message);
            base.errcode = JsonUtils.getString(object, "errcode", "");
            base.errdesc = JsonUtils.getString(object, "errdesc", "");
            base.errdesc_en = JsonUtils.getString(object, "errdesc_en", "");
            base.result = JsonUtils.getString(object, "result", "");
        }
        return base;
    }
}
