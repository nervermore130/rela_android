package com.thel.modules.live.bean;

/**
 * Created by waiarl on 2017/12/1.
 */

public class LivePkRefuseBean {
    public String userId;
    public long refuseTime;

    public LivePkRefuseBean(String userId, long refuseTime) {
        this.userId = userId;
        this.refuseTime = refuseTime;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof String && obj.equals(userId)) {
            return true;
        }
        return obj instanceof LivePkRefuseBean && ((LivePkRefuseBean) obj).userId.equals(userId) || super.equals(obj);

    }
}
