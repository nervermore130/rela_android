package com.thel.modules.select_country;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.thel.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chad
 * Time 17/9/22
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */

public class CountryAdapter extends BaseAdapter {
    private List<String> countries = new ArrayList<String>();

    private LayoutInflater mInflater;

    public CountryAdapter(Context context,List<String> countries) {
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.countries.clear();
        this.countries.addAll(countries);
    }

    public void refreshAdapter(List<String> countries) {
        this.countries.clear();
        this.countries.addAll(countries);
    }

    @Override
    public int getCount() {
        return countries.size();
    }

    @Override
    public Object getItem(int i) {
        return countries.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        HoldView holdView = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.sms_country_item, parent, false);
            holdView = new HoldView();
            holdView.txt_country_name = convertView.findViewById(R.id.txt_country_name);
            holdView.txt_country_code = convertView.findViewById(R.id.txt_country_code);
            convertView.setTag(holdView); // 把holdview缓存下来
        } else {
            holdView = (HoldView) convertView.getTag();
        }

        String[] arr = countries.get(position).split("\\+");
        holdView.txt_country_name.setText(arr[0]);
        holdView.txt_country_code.setText("+" + arr[1]);

        convertView.setTag(R.id.txt_country_name, arr[0]);
        convertView.setTag(R.id.txt_country_code, arr[1]);

        return convertView;
    }

    class HoldView {
        TextView txt_country_name;
        TextView txt_country_code;
    }
}
