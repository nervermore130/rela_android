package com.thel.modules.main.settings;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.kyleduo.switchbutton.SwitchButton;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.base.BaseDataBean;
import com.thel.modules.main.me.aboutMe.MyBlockActivity;
import com.thel.modules.main.settings.bean.PushSwitchTypeBean;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.ViewUtils;

import org.reactivestreams.Subscription;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.thel.R.id.img_newlike_push;
import static com.thel.R.id.lin_my_settings;
import static com.thel.R.id.rel_myblock;
import static com.thel.R.id.superlike;

/**
 * 设置的二级页面
 * Created by lingwei on 2017/11/1.
 */

public class SettingsActivity extends BaseActivity {
    public static final int PAGE_TYPE_VIDEO = 0;
    public static final int PAGE_TYPE_SOUND = 1;
    public static final int PAGE_TYPE_USER = 2;
    public static final int PAGE_TYPE_PUSH = 3;
    public static final int PAGE_TYPE_RED_POINT = 4;

    public static final String PAGE_TYPE_TAG = "pageType";
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.lin_back)
    LinearLayout linBack;
    @BindView(R.id.txt_title)
    TextView txtTitle;
    @BindView(R.id.img_more)
    ImageView imgMore;
    @BindView(R.id.lin_more)
    LinearLayout linMore;
    @BindView(R.id.title_layout)
    RelativeLayout titleLayout;
    @BindView(rel_myblock)
    RelativeLayout relMyblock;
    @BindView(R.id.rel_myBlockUserMoments)
    RelativeLayout relMyBlockUserMoments;
    @BindView(lin_my_settings)
    LinearLayout linMySettings;
    @BindView(R.id.img_message)
    SwitchButton imgMessage;
    @BindView(R.id.rel_messagesound)
    RelativeLayout relMessagesound;
    @BindView(R.id.lin_sound_settings)
    LinearLayout linSoundSettings;
    @BindView(R.id.txt_switch_title)
    TextView txt_switch_title;
    @BindView(R.id.img_autoplay_wifi)
    SwitchButton imgAutoplayWifi;
    @BindView(R.id.rel_autoplay_wifi)
    RelativeLayout relAutoplayWifi;
    @BindView(R.id.img_autoplay_mobile)
    SwitchButton imgAutoplayMobile;
    @BindView(R.id.rel_autoplay_mobile)
    RelativeLayout relAutoplayMobile;
    @BindView(R.id.lin_video_settings)
    LinearLayout linVideoSettings;
    @BindView(R.id.img_follow_push)
    SwitchButton imgFollowPush;
    @BindView(R.id.rel_follow_push)
    RelativeLayout relFollowPush;
    @BindView(R.id.img_wink_push)
    SwitchButton imgWinkPush;
    @BindView(R.id.rel_wink_push)
    RelativeLayout relWinkPush;
    @BindView(R.id.img_msg_push)
    SwitchButton imgMsgPush;
    @BindView(R.id.rel_msg_push)
    RelativeLayout relMsgPush;
    @BindView(img_newlike_push)
    SwitchButton imgNewlikePush;
    @BindView(R.id.rel_newlike_push)
    RelativeLayout relNewlikePush;
    @BindView(R.id.img_moment_comment_push)
    SwitchButton imgMomentCommentPush;
    @BindView(R.id.rel_moment_comment_push)
    RelativeLayout relMomentCommentPush;
    @BindView(R.id.img_reply_push)
    SwitchButton imgReplyPush;
    @BindView(R.id.rel_reply_push)
    RelativeLayout relReplyPush;
    @BindView(R.id.wide_divider)
    LinearLayout wideDivider;
    @BindView(R.id.img_live_push)
    SwitchButton imgLivePush;
    @BindView(R.id.rel_live_push)
    RelativeLayout relLivePush;
    @BindView(R.id.img_video_push)
    SwitchButton imgVideoPush;
    @BindView(R.id.rel_video_push)
    RelativeLayout relVideoPush;
    @BindView(R.id.img_theme_push)
    SwitchButton imgThemePush;
    @BindView(R.id.rel_theme_push)
    RelativeLayout relThemePush;
    @BindView(R.id.lin_push_settings)
    LinearLayout linPushSettings;
    @BindView(R.id.rel_record_push)
    RelativeLayout relRecordPush;
    @BindView(R.id.img_record_push)
    SwitchButton imgRecordView;
    @BindView(R.id.img_match_likeMe_push)
    SwitchButton img_match_likeMe_push;
    @BindView(R.id.img_match_superLike_push)
    SwitchButton img_match_superLike_push;
    @BindView(R.id.img_match_perfect)
    SwitchButton img_match_perfect;
    @BindView(R.id.tv_sound)
    TextView tv_sound;
    @BindView(R.id.rel_expect_live)
    RelativeLayout rel_expect_live;
    @BindView(R.id.img_expect_live_push)
    SwitchButton img_expect_live_push;

    private int pageType = -1;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);
        ButterKnife.bind(this);
        pageType = getIntent().getIntExtra(PAGE_TYPE_TAG, -1);
        initData();
        setListener();
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void setListener() {
        linBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                setResult(RESULT_OK);
                SettingsActivity.this.finish();
            }
        });
        imgMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                if (pageType == PAGE_TYPE_SOUND) {
                    ShareFileUtils.setBoolean(ShareFileUtils.MESSAGE_SOUND, !ShareFileUtils.getBoolean(ShareFileUtils.MESSAGE_SOUND, true));
                    setMessageUI();
                } else if (pageType == PAGE_TYPE_RED_POINT) {
                    Flowable<PushSwitchTypeBean> flowable = RequestBusiness.getInstance().pushRedPointConfig(ShareFileUtils.getInt(ShareFileUtils.MESSAGE_REDPOINT, 1) == 1 ? 2 : 1);
                    flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<PushSwitchTypeBean>() {


                        @Override
                        public void onNext(PushSwitchTypeBean bean) {
                            super.onNext(bean);
                            ShareFileUtils.setInt(ShareFileUtils.MESSAGE_REDPOINT, ShareFileUtils.getInt(ShareFileUtils.MESSAGE_REDPOINT, 1) == 1 ? 2 : 1);
                            setRedpointUI();
                            L.d("redpointPrint", " redpointPrint :success ");

                        }

                    });

                }

            }
        });
        imgAutoplayMobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 500);
                ShareFileUtils.setBoolean(ShareFileUtils.AUTO_PLAY_MOBILE, !ShareFileUtils.getBoolean(ShareFileUtils.AUTO_PLAY_MOBILE, true));
                setAutoplayMobileUI();
            }
        });
        imgAutoplayWifi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 500);
                ShareFileUtils.setBoolean(ShareFileUtils.AUTO_PLAY_WIFI, !ShareFileUtils.getBoolean(ShareFileUtils.AUTO_PLAY_WIFI, true));
                setAutoplayWifiUI();
            }
        });
        relMyblock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                Intent intent = new Intent(SettingsActivity.this, MyBlockActivity.class);
                startActivity(intent);
            }
        });
        relMyBlockUserMoments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                Intent intent = new Intent(SettingsActivity.this, MyBlockUserMomentsActivity.class);
                startActivity(intent);
            }
        });
        //收到关注
        imgFollowPush.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                Flowable<BaseDataBean> flowable = RequestBusiness.getInstance().pushConfig(ShareFileUtils.followerPush, ShareFileUtils.getInt(ShareFileUtils.LIVE_PUSH, 1), ShareFileUtils.getInt(ShareFileUtils.commentTextPush, 1), ShareFileUtils.getInt(ShareFileUtils.commentReplyPush, 1), ShareFileUtils.getInt(ShareFileUtils.recordPush, 1), ShareFileUtils.getInt(ShareFileUtils.commentWinkPush, 1), 1 - ShareFileUtils.getInt(ShareFileUtils.followerPush, 0), ShareFileUtils.getInt(ShareFileUtils.winkPush, 1), ShareFileUtils.getInt(ShareFileUtils.messagePush, 1), ShareFileUtils.getInt(ShareFileUtils.videoPush, 1), ShareFileUtils.getInt(ShareFileUtils.themePush, 1), ShareFileUtils.getInt(ShareFileUtils.likeMe, 1), ShareFileUtils.getInt(ShareFileUtils.superLike, 1), ShareFileUtils.getInt(ShareFileUtils.perfectMatch, 1), ShareFileUtils.getInt(ShareFileUtils.await, 1));
                flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {


                    @Override
                    public void onNext(BaseDataBean baseDataBean) {
                        super.onNext(baseDataBean);
                        refreshUi(ShareFileUtils.followerPush);
                    }

                });
            }
        });
        //收到擠眼
        imgWinkPush.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 500);
                Flowable<BaseDataBean> flowable = RequestBusiness.getInstance().pushConfig(ShareFileUtils.winkPush, ShareFileUtils.getInt(ShareFileUtils.LIVE_PUSH, 1), ShareFileUtils.getInt(ShareFileUtils.commentTextPush, 1), ShareFileUtils.getInt(ShareFileUtils.commentReplyPush, 1), ShareFileUtils.getInt(ShareFileUtils.recordPush, 1), ShareFileUtils.getInt(ShareFileUtils.commentWinkPush, 1), ShareFileUtils.getInt(ShareFileUtils.followerPush, 1), 1 - ShareFileUtils.getInt(ShareFileUtils.winkPush, 0), ShareFileUtils.getInt(ShareFileUtils.messagePush, 1), ShareFileUtils.getInt(ShareFileUtils.videoPush, 1), ShareFileUtils.getInt(ShareFileUtils.themePush, 1), ShareFileUtils.getInt(ShareFileUtils.likeMe, 1), ShareFileUtils.getInt(ShareFileUtils.superLike, 1), ShareFileUtils.getInt(ShareFileUtils.perfectMatch, 1), ShareFileUtils.getInt(ShareFileUtils.await, 1));
                flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {


                    @Override
                    public void onNext(BaseDataBean baseDataBean) {
                        super.onNext(baseDataBean);
                        refreshUi(ShareFileUtils.winkPush);
                    }

                });
            }
        });
        //收到消息
        imgMsgPush.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 500);
                Flowable<BaseDataBean> flowable = RequestBusiness.getInstance().pushConfig(ShareFileUtils.messagePush, ShareFileUtils.getInt(ShareFileUtils.LIVE_PUSH, 1), ShareFileUtils.getInt(ShareFileUtils.commentTextPush, 1), ShareFileUtils.getInt(ShareFileUtils.commentReplyPush, 1), ShareFileUtils.getInt(ShareFileUtils.recordPush, 1), ShareFileUtils.getInt(ShareFileUtils.commentWinkPush, 1), ShareFileUtils.getInt(ShareFileUtils.followerPush, 1), ShareFileUtils.getInt(ShareFileUtils.winkPush, 1), 1 - ShareFileUtils.getInt(ShareFileUtils.messagePush, 0), ShareFileUtils.getInt(ShareFileUtils.videoPush, 1), ShareFileUtils.getInt(ShareFileUtils.themePush, 1), ShareFileUtils.getInt(ShareFileUtils.likeMe, 1), ShareFileUtils.getInt(ShareFileUtils.superLike, 1), ShareFileUtils.getInt(ShareFileUtils.perfectMatch, 1), ShareFileUtils.getInt(ShareFileUtils.await, 1));
                flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {

                    @Override
                    public void onNext(BaseDataBean baseDataBean) {
                        super.onNext(baseDataBean);
                        refreshUi(ShareFileUtils.messagePush);

                    }

                });
            }
        });
        //新的喜歡
        imgMomentCommentPush.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 500);
                Flowable<BaseDataBean> flowable = RequestBusiness.getInstance().pushConfig(ShareFileUtils.commentWinkPush, ShareFileUtils.getInt(ShareFileUtils.LIVE_PUSH, 1), ShareFileUtils.getInt(ShareFileUtils.commentTextPush, 1), ShareFileUtils.getInt(ShareFileUtils.commentReplyPush, 1), ShareFileUtils.getInt(ShareFileUtils.recordPush, 1), 1 - ShareFileUtils.getInt(ShareFileUtils.commentWinkPush, 0), ShareFileUtils.getInt(ShareFileUtils.followerPush, 1), ShareFileUtils.getInt(ShareFileUtils.winkPush, 1), ShareFileUtils.getInt(ShareFileUtils.messagePush, 1), ShareFileUtils.getInt(ShareFileUtils.videoPush, 1), ShareFileUtils.getInt(ShareFileUtils.themePush, 1), ShareFileUtils.getInt(ShareFileUtils.likeMe, 1), ShareFileUtils.getInt(ShareFileUtils.superLike, 1), ShareFileUtils.getInt(ShareFileUtils.perfectMatch, 1), ShareFileUtils.getInt(ShareFileUtils.await, 1));
                flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {


                    @Override
                    public void onNext(BaseDataBean baseDataBean) {
                        super.onNext(baseDataBean);
                        refreshUi(ShareFileUtils.commentWinkPush);

                    }

                });
            }
        });
        //新的评论
        imgMomentCommentPush.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 500);
                Flowable<BaseDataBean> flowable = RequestBusiness.getInstance().pushConfig(ShareFileUtils.commentTextPush, ShareFileUtils.getInt(ShareFileUtils.LIVE_PUSH, 1), 1 - ShareFileUtils.getInt(ShareFileUtils.commentTextPush, 0), ShareFileUtils.getInt(ShareFileUtils.commentReplyPush, 1), ShareFileUtils.getInt(ShareFileUtils.recordPush, 1), ShareFileUtils.getInt(ShareFileUtils.commentWinkPush, 1), ShareFileUtils.getInt(ShareFileUtils.followerPush, 1), ShareFileUtils.getInt(ShareFileUtils.winkPush, 1), ShareFileUtils.getInt(ShareFileUtils.messagePush, 1), ShareFileUtils.getInt(ShareFileUtils.videoPush, 1), ShareFileUtils.getInt(ShareFileUtils.themePush, 1), ShareFileUtils.getInt(ShareFileUtils.likeMe, 1), ShareFileUtils.getInt(ShareFileUtils.superLike, 1), ShareFileUtils.getInt(ShareFileUtils.perfectMatch, 1), ShareFileUtils.getInt(ShareFileUtils.await, 1));
                flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {

                    @Override
                    public void onNext(BaseDataBean baseDataBean) {
                        super.onNext(baseDataBean);
                        refreshUi(ShareFileUtils.commentTextPush);

                    }

                    @Override
                    public void onError(Throwable t) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
            }
        });
        //新的回复
        imgReplyPush.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 500);
                Flowable<BaseDataBean> flowable = RequestBusiness.getInstance().pushConfig(ShareFileUtils.commentReplyPush, ShareFileUtils.getInt(ShareFileUtils.LIVE_PUSH, 1), ShareFileUtils.getInt(ShareFileUtils.commentTextPush, 1), 1 - ShareFileUtils.getInt(ShareFileUtils.commentReplyPush, 0), ShareFileUtils.getInt(ShareFileUtils.recordPush, 1), ShareFileUtils.getInt(ShareFileUtils.commentWinkPush, 1), ShareFileUtils.getInt(ShareFileUtils.followerPush, 1), ShareFileUtils.getInt(ShareFileUtils.winkPush, 1), ShareFileUtils.getInt(ShareFileUtils.messagePush, 1), ShareFileUtils.getInt(ShareFileUtils.videoPush, 1), ShareFileUtils.getInt(ShareFileUtils.themePush, 1), ShareFileUtils.getInt(ShareFileUtils.likeMe, 1), ShareFileUtils.getInt(ShareFileUtils.superLike, 1), ShareFileUtils.getInt(ShareFileUtils.perfectMatch, 1), ShareFileUtils.getInt(ShareFileUtils.await, 1));
                flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {

                    @Override
                    public void onNext(BaseDataBean baseDataBean) {
                        super.onNext(baseDataBean);
                        refreshUi(ShareFileUtils.commentReplyPush);

                    }

                });
            }
        });
        //新的直播
        imgLivePush.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 500);
                Flowable<BaseDataBean> flowable = RequestBusiness.getInstance().pushConfig(ShareFileUtils.LIVE_PUSH, 1 - ShareFileUtils.getInt(ShareFileUtils.LIVE_PUSH, 0), ShareFileUtils.getInt(ShareFileUtils.commentTextPush, 1), ShareFileUtils.getInt(ShareFileUtils.commentReplyPush, 1), ShareFileUtils.getInt(ShareFileUtils.recordPush, 1), ShareFileUtils.getInt(ShareFileUtils.commentWinkPush, 1), ShareFileUtils.getInt(ShareFileUtils.followerPush, 1), ShareFileUtils.getInt(ShareFileUtils.winkPush, 1), ShareFileUtils.getInt(ShareFileUtils.messagePush, 1), ShareFileUtils.getInt(ShareFileUtils.videoPush, 1), ShareFileUtils.getInt(ShareFileUtils.themePush, 1), ShareFileUtils.getInt(ShareFileUtils.likeMe, 1), ShareFileUtils.getInt(ShareFileUtils.superLike, 1), ShareFileUtils.getInt(ShareFileUtils.perfectMatch, 1), ShareFileUtils.getInt(ShareFileUtils.await, 1));
                flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {


                    @Override
                    public void onNext(BaseDataBean baseDataBean) {
                        super.onNext(baseDataBean);
                        refreshUi(ShareFileUtils.LIVE_PUSH);

                    }

                });
            }
        });
        //新的视频
        imgVideoPush.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 500);
                Flowable<BaseDataBean> flowable = RequestBusiness.getInstance().pushConfig(ShareFileUtils.videoPush, ShareFileUtils.getInt(ShareFileUtils.LIVE_PUSH, 1), ShareFileUtils.getInt(ShareFileUtils.commentTextPush, 1), ShareFileUtils.getInt(ShareFileUtils.commentReplyPush, 1), ShareFileUtils.getInt(ShareFileUtils.recordPush, 1), ShareFileUtils.getInt(ShareFileUtils.commentWinkPush, 1), ShareFileUtils.getInt(ShareFileUtils.followerPush, 1), ShareFileUtils.getInt(ShareFileUtils.winkPush, 1), ShareFileUtils.getInt(ShareFileUtils.messagePush, 1), 1 - ShareFileUtils.getInt(ShareFileUtils.videoPush, 0), ShareFileUtils.getInt(ShareFileUtils.themePush, 1), ShareFileUtils.getInt(ShareFileUtils.likeMe, 1), ShareFileUtils.getInt(ShareFileUtils.superLike, 1), ShareFileUtils.getInt(ShareFileUtils.perfectMatch, 1), ShareFileUtils.getInt(ShareFileUtils.await, 1));
                flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {

                    @Override
                    public void onNext(BaseDataBean baseDataBean) {
                        super.onNext(baseDataBean);
                        refreshUi(ShareFileUtils.videoPush);

                    }

                });
            }
        });
        //新的喜欢
        imgNewlikePush.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 500);
                Flowable<BaseDataBean> flowable = RequestBusiness.getInstance().pushConfig(ShareFileUtils.commentWinkPush, ShareFileUtils.getInt(ShareFileUtils.LIVE_PUSH, 1), ShareFileUtils.getInt(ShareFileUtils.commentTextPush, 0), ShareFileUtils.getInt(ShareFileUtils.commentReplyPush, 1), ShareFileUtils.getInt(ShareFileUtils.recordPush, 1), 1 - ShareFileUtils.getInt(ShareFileUtils.commentWinkPush, 0), ShareFileUtils.getInt(ShareFileUtils.followerPush, 1), ShareFileUtils.getInt(ShareFileUtils.winkPush, 1), ShareFileUtils.getInt(ShareFileUtils.messagePush, 1), ShareFileUtils.getInt(ShareFileUtils.videoPush, 1), ShareFileUtils.getInt(ShareFileUtils.themePush, 1), ShareFileUtils.getInt(ShareFileUtils.likeMe, 1), ShareFileUtils.getInt(ShareFileUtils.superLike, 1), ShareFileUtils.getInt(ShareFileUtils.perfectMatch, 1), ShareFileUtils.getInt(ShareFileUtils.await, 1));
                flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {

                    @Override
                    public void onNext(BaseDataBean baseDataBean) {
                        super.onNext(baseDataBean);
                        refreshUi(ShareFileUtils.commentWinkPush);

                    }

                });
            }
        });
        //新的話題
        imgThemePush.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 500);
                Flowable<BaseDataBean> flowable = RequestBusiness.getInstance().pushConfig(ShareFileUtils.themePush, ShareFileUtils.getInt(ShareFileUtils.LIVE_PUSH, 1), ShareFileUtils.getInt(ShareFileUtils.commentTextPush, 1), ShareFileUtils.getInt(ShareFileUtils.commentReplyPush, 1), ShareFileUtils.getInt(ShareFileUtils.recordPush, 1), ShareFileUtils.getInt(ShareFileUtils.commentWinkPush, 1), ShareFileUtils.getInt(ShareFileUtils.followerPush, 1), ShareFileUtils.getInt(ShareFileUtils.winkPush, 1), ShareFileUtils.getInt(ShareFileUtils.messagePush, 1), ShareFileUtils.getInt(ShareFileUtils.videoPush, 1), 1 - ShareFileUtils.getInt(ShareFileUtils.themePush, 0), ShareFileUtils.getInt(ShareFileUtils.likeMe, 1), ShareFileUtils.getInt(ShareFileUtils.superLike, 1), ShareFileUtils.getInt(ShareFileUtils.perfectMatch, 1), ShareFileUtils.getInt(ShareFileUtils.await, 1));
                flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {

                    @Override
                    public void onNext(BaseDataBean baseDataBean) {
                        super.onNext(baseDataBean);
                        refreshUi(ShareFileUtils.themePush);

                    }
                });
            }
        });
        //来访记录推送
        imgRecordView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 500);
                Flowable<BaseDataBean> flowable = RequestBusiness.getInstance().pushConfig(ShareFileUtils.recordPush, ShareFileUtils.getInt(ShareFileUtils.LIVE_PUSH, 1), ShareFileUtils.getInt(ShareFileUtils.commentTextPush, 1), ShareFileUtils.getInt(ShareFileUtils.commentReplyPush, 1), 1 - ShareFileUtils.getInt(ShareFileUtils.recordPush, 0), ShareFileUtils.getInt(ShareFileUtils.commentWinkPush, 1), ShareFileUtils.getInt(ShareFileUtils.followerPush, 1), ShareFileUtils.getInt(ShareFileUtils.winkPush, 1), ShareFileUtils.getInt(ShareFileUtils.messagePush, 1), ShareFileUtils.getInt(ShareFileUtils.videoPush, 1), ShareFileUtils.getInt(ShareFileUtils.themePush, 1), ShareFileUtils.getInt(ShareFileUtils.likeMe, 1), ShareFileUtils.getInt(ShareFileUtils.superLike, 1), ShareFileUtils.getInt(ShareFileUtils.perfectMatch, 1), ShareFileUtils.getInt(ShareFileUtils.await, 1));
                flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {

                    @Override
                    public void onNext(BaseDataBean baseDataBean) {
                        super.onNext(baseDataBean);
                        refreshUi(ShareFileUtils.recordPush);
                        L.d("settingsRecord", "我更改状态啦");
                    }
                });
            }
        });
        /**
         * 谁喜欢我
         * */
        img_match_likeMe_push.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewUtils.preventViewMultipleClick(view, 500);
                Flowable<BaseDataBean> flowable = RequestBusiness.getInstance().pushConfig(ShareFileUtils.likeMe, ShareFileUtils.getInt(ShareFileUtils.LIVE_PUSH, 1), ShareFileUtils.getInt(ShareFileUtils.commentTextPush, 1), ShareFileUtils.getInt(ShareFileUtils.commentReplyPush, 1), ShareFileUtils.getInt(ShareFileUtils.recordPush, 1), ShareFileUtils.getInt(ShareFileUtils.commentWinkPush, 1), ShareFileUtils.getInt(ShareFileUtils.followerPush, 1), ShareFileUtils.getInt(ShareFileUtils.winkPush, 1), ShareFileUtils.getInt(ShareFileUtils.messagePush, 1), ShareFileUtils.getInt(ShareFileUtils.videoPush, 1), ShareFileUtils.getInt(ShareFileUtils.themePush, 1), 1 - ShareFileUtils.getInt(ShareFileUtils.likeMe, 0), ShareFileUtils.getInt(ShareFileUtils.superLike, 1), ShareFileUtils.getInt(ShareFileUtils.perfectMatch, 1), ShareFileUtils.getInt(ShareFileUtils.await, 1));
                flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {

                    @Override
                    public void onNext(BaseDataBean baseDataBean) {
                        super.onNext(baseDataBean);
                        refreshUi(ShareFileUtils.likeMe);
                        L.d("settingsRecord", "我更改状态啦");
                    }

                });
            }
        });
        /**
         * 超级喜欢
         * */
        img_match_superLike_push.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewUtils.preventViewMultipleClick(view, 500);
                Flowable<BaseDataBean> flowable = RequestBusiness.getInstance().pushConfig(ShareFileUtils.superLike, ShareFileUtils.getInt(ShareFileUtils.LIVE_PUSH, 1), ShareFileUtils.getInt(ShareFileUtils.commentTextPush, 1), ShareFileUtils.getInt(ShareFileUtils.commentReplyPush, 1), ShareFileUtils.getInt(ShareFileUtils.recordPush, 1), ShareFileUtils.getInt(ShareFileUtils.commentWinkPush, 1), ShareFileUtils.getInt(ShareFileUtils.followerPush, 1), ShareFileUtils.getInt(ShareFileUtils.winkPush, 1), ShareFileUtils.getInt(ShareFileUtils.messagePush, 1), ShareFileUtils.getInt(ShareFileUtils.videoPush, 1), ShareFileUtils.getInt(ShareFileUtils.themePush, 1), ShareFileUtils.getInt(ShareFileUtils.likeMe, 1), 1 - ShareFileUtils.getInt(ShareFileUtils.superLike, 0), ShareFileUtils.getInt(ShareFileUtils.perfectMatch, 1), ShareFileUtils.getInt(ShareFileUtils.await, 1));
                flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {

                    @Override
                    public void onNext(BaseDataBean baseDataBean) {
                        super.onNext(baseDataBean);
                        refreshUi(ShareFileUtils.superLike);
                        L.d("settingsRecord", "我更改状态啦");
                    }
                });
            }
        });
        /**
         *
         * 完美匹配
         * */
        img_match_perfect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewUtils.preventViewMultipleClick(view, 500);
                Flowable<BaseDataBean> flowable = RequestBusiness.getInstance().pushConfig(ShareFileUtils.perfectMatch, ShareFileUtils.getInt(ShareFileUtils.perfectMatch, 1), ShareFileUtils.getInt(ShareFileUtils.commentTextPush, 1), ShareFileUtils.getInt(ShareFileUtils.commentReplyPush, 1), ShareFileUtils.getInt(ShareFileUtils.recordPush, 1), ShareFileUtils.getInt(ShareFileUtils.commentWinkPush, 1), ShareFileUtils.getInt(ShareFileUtils.followerPush, 1), ShareFileUtils.getInt(ShareFileUtils.winkPush, 1), ShareFileUtils.getInt(ShareFileUtils.messagePush, 1), ShareFileUtils.getInt(ShareFileUtils.videoPush, 1), ShareFileUtils.getInt(ShareFileUtils.themePush, 1), ShareFileUtils.getInt(ShareFileUtils.likeMe, 1), ShareFileUtils.getInt(ShareFileUtils.superLike, 1), 1 - ShareFileUtils.getInt(ShareFileUtils.perfectMatch, 0), ShareFileUtils.getInt(ShareFileUtils.await, 1));
                flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {

                    @Override
                    public void onNext(BaseDataBean baseDataBean) {
                        super.onNext(baseDataBean);
                        refreshUi(ShareFileUtils.perfectMatch);
                        L.d("settingsRecord", "完美匹配我更改状态啦");
                    }
                });
            }
        });
        /**
         * 期待开播
         * */
        img_expect_live_push.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewUtils.preventViewMultipleClick(view, 500);
                Flowable<BaseDataBean> flowable = RequestBusiness.getInstance().pushConfig(ShareFileUtils.await, ShareFileUtils.getInt(ShareFileUtils.await, 1), ShareFileUtils.getInt(ShareFileUtils.commentTextPush, 1), ShareFileUtils.getInt(ShareFileUtils.commentReplyPush, 1), ShareFileUtils.getInt(ShareFileUtils.recordPush, 1), ShareFileUtils.getInt(ShareFileUtils.commentWinkPush, 1), ShareFileUtils.getInt(ShareFileUtils.followerPush, 1), ShareFileUtils.getInt(ShareFileUtils.winkPush, 1), ShareFileUtils.getInt(ShareFileUtils.messagePush, 1), ShareFileUtils.getInt(ShareFileUtils.videoPush, 1), ShareFileUtils.getInt(ShareFileUtils.themePush, 1), ShareFileUtils.getInt(ShareFileUtils.likeMe, 1), ShareFileUtils.getInt(ShareFileUtils.superLike, 1), ShareFileUtils.getInt(ShareFileUtils.perfectMatch, 1), 1 - ShareFileUtils.getInt(ShareFileUtils.await, 0));
                flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {

                    @Override
                    public void onNext(BaseDataBean baseDataBean) {
                        super.onNext(baseDataBean);
                        refreshUi(ShareFileUtils.await);
                    }
                });
            }
        });

    }

    private void refreshUi(String type) {
        switch (type) {
            case ShareFileUtils.followerPush:
                ShareFileUtils.setInt(ShareFileUtils.followerPush, 1 - ShareFileUtils.getInt(ShareFileUtils.followerPush, 0));
                break;
            case ShareFileUtils.winkPush:
                ShareFileUtils.setInt(ShareFileUtils.winkPush, 1 - ShareFileUtils.getInt(ShareFileUtils.winkPush, 0));

                break;
            case ShareFileUtils.messagePush:
                ShareFileUtils.setInt(ShareFileUtils.messagePush, 1 - ShareFileUtils.getInt(ShareFileUtils.messagePush, 0));

                break;
            case ShareFileUtils.commentWinkPush:
                ShareFileUtils.setInt(ShareFileUtils.commentWinkPush, 1 - ShareFileUtils.getInt(ShareFileUtils.commentWinkPush, 0));

                break;
            case ShareFileUtils.commentTextPush:
                ShareFileUtils.setInt(ShareFileUtils.commentTextPush, 1 - ShareFileUtils.getInt(ShareFileUtils.commentTextPush, 0));

                break;
            case ShareFileUtils.commentReplyPush:
                ShareFileUtils.setInt(ShareFileUtils.commentReplyPush, 1 - ShareFileUtils.getInt(ShareFileUtils.commentReplyPush, 0));

                break;
            case ShareFileUtils.LIVE_PUSH:
                ShareFileUtils.setInt(ShareFileUtils.LIVE_PUSH, 1 - ShareFileUtils.getInt(ShareFileUtils.LIVE_PUSH, 0));

                break;
            case ShareFileUtils.videoPush:
                ShareFileUtils.setInt(ShareFileUtils.videoPush, 1 - ShareFileUtils.getInt(ShareFileUtils.videoPush, 0));

                break;
            case ShareFileUtils.themePush:
                ShareFileUtils.setInt(ShareFileUtils.themePush, 1 - ShareFileUtils.getInt(ShareFileUtils.themePush, 0));

                break;
            case ShareFileUtils.recordPush:
                ShareFileUtils.setInt(ShareFileUtils.recordPush, 1 - ShareFileUtils.getInt(ShareFileUtils.recordPush, 0));
                break;
            case ShareFileUtils.likeMe:
                ShareFileUtils.setInt(ShareFileUtils.likeMe, 1 - ShareFileUtils.getInt(ShareFileUtils.likeMe, 0));
                break;
            case ShareFileUtils.superLike:
                ShareFileUtils.setInt(ShareFileUtils.superLike, 1 - ShareFileUtils.getInt(ShareFileUtils.superLike, 0));
                break;
            case ShareFileUtils.perfectMatch:
                ShareFileUtils.setInt(ShareFileUtils.perfectMatch, 1 - ShareFileUtils.getInt(ShareFileUtils.perfectMatch, 0));
                break;
            case ShareFileUtils.await:
                ShareFileUtils.setInt(ShareFileUtils.await, 1 - ShareFileUtils.getInt(ShareFileUtils.await, 0));
                break;
        }

        setLivePushUI();
    }

    private void initData() {
        linMore.setVisibility(View.GONE);
        if (pageType == PAGE_TYPE_VIDEO) {
            txtTitle.setText(R.string.setting_activity_title_video_setting);
            linVideoSettings.setVisibility(View.VISIBLE);
            setAutoplayWifiUI();
            linPushSettings.setVisibility(View.GONE);
            setAutoplayMobileUI();
        } else if (pageType == PAGE_TYPE_SOUND) {
            txtTitle.setText(R.string.setting_activity_title_sound_setting);
            linSoundSettings.setVisibility(View.VISIBLE);
            linPushSettings.setVisibility(View.GONE);
            setMessageUI();
        } else if (pageType == PAGE_TYPE_RED_POINT) {
            txtTitle.setText(R.string.red_point_prompt);
            linSoundSettings.setVisibility(View.VISIBLE);
            linPushSettings.setVisibility(View.GONE);
            txt_switch_title.setText(TheLApp.context.getString(R.string.whoseenme_activity_title));
            tv_sound.setVisibility(View.GONE);

            Flowable<PushSwitchTypeBean> flowable = RequestBusiness.getInstance().getRedPointList();
            flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<PushSwitchTypeBean>() {

                @Override
                public void onNext(PushSwitchTypeBean pushSwitchTypeBean) {
                    super.onNext(pushSwitchTypeBean);
                    savaRedpointConfigs(pushSwitchTypeBean);
                    setRedpointUI();

                }

            });

        } else if (pageType == PAGE_TYPE_USER) {
            txtTitle.setText(R.string.other_settings);
            linMySettings.setVisibility(View.VISIBLE);
            linPushSettings.setVisibility(View.GONE);
        } else if (pageType == PAGE_TYPE_PUSH) {
            txtTitle.setText(getString(R.string.setting_activity_title_push));
            linPushSettings.setVisibility(View.VISIBLE);
            Flowable<PushSwitchTypeBean> flowable = RequestBusiness.getInstance().getPushSwitchList();
            flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<PushSwitchTypeBean>() {

                @Override
                public void onNext(PushSwitchTypeBean pushSwitchTypeBean) {
                    super.onNext(pushSwitchTypeBean);
                    savaConfigs(pushSwitchTypeBean);
                    setLivePushUI();

                }

            });
        }
    }

    private void setRedpointUI() {
        imgMessage.setChecked(ShareFileUtils.getInt(ShareFileUtils.MESSAGE_REDPOINT, 1) != 2);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            setResult(RESULT_OK);
            SettingsActivity.this.finish();
            return true;
        }
        return true;
    }

    private void setLivePushUI() {
        imgFollowPush.setChecked(ShareFileUtils.getInt(ShareFileUtils.followerPush, 1) != 0);
        imgWinkPush.setChecked(ShareFileUtils.getInt(ShareFileUtils.winkPush, 1) != 0);
        imgMsgPush.setChecked(ShareFileUtils.getInt(ShareFileUtils.messagePush, 1) != 0);
        imgNewlikePush.setChecked(ShareFileUtils.getInt(ShareFileUtils.commentWinkPush, 1) != 0);
        imgMomentCommentPush.setChecked(ShareFileUtils.getInt(ShareFileUtils.commentTextPush, 1) != 0);
        imgReplyPush.setChecked(ShareFileUtils.getInt(ShareFileUtils.commentReplyPush, 1) != 0);
        imgLivePush.setChecked(ShareFileUtils.getInt(ShareFileUtils.LIVE_PUSH, 1) != 0);
        imgVideoPush.setChecked(ShareFileUtils.getInt(ShareFileUtils.videoPush, 1) != 0);
        imgThemePush.setChecked(ShareFileUtils.getInt(ShareFileUtils.themePush, 1) != 0);
        imgRecordView.setChecked(ShareFileUtils.getInt(ShareFileUtils.recordPush, 1) != 0);
        img_match_likeMe_push.setChecked(ShareFileUtils.getInt(ShareFileUtils.likeMe, 1) != 0);
        img_match_superLike_push.setChecked(ShareFileUtils.getInt(ShareFileUtils.superLike, 1) != 0);
        img_match_perfect.setChecked(ShareFileUtils.getInt(ShareFileUtils.perfectMatch, 1) != 0);
        img_expect_live_push.setChecked(ShareFileUtils.getInt(ShareFileUtils.await, 1) != 0);


    }

    private void savaConfigs(PushSwitchTypeBean pushBean) {
        ShareFileUtils.setInt(ShareFileUtils.LIVE_PUSH, pushBean.data.live);// 直播推送开关
        ShareFileUtils.setInt(ShareFileUtils.commentTextPush, pushBean.data.commentText);
        ShareFileUtils.setInt(ShareFileUtils.commentReplyPush, pushBean.data.commentReply);
        ShareFileUtils.setInt(ShareFileUtils.commentWinkPush, pushBean.data.commentWink);
        ShareFileUtils.setInt(ShareFileUtils.themePush, pushBean.data.theme);
        ShareFileUtils.setInt(ShareFileUtils.followerPush, pushBean.data.follower);
        ShareFileUtils.setInt(ShareFileUtils.winkPush, pushBean.data.wink);
        ShareFileUtils.setInt(ShareFileUtils.messagePush, pushBean.data.message);
        ShareFileUtils.setInt(ShareFileUtils.videoPush, pushBean.data.video);
        ShareFileUtils.setInt(ShareFileUtils.recordPush, pushBean.data.view);
        ShareFileUtils.setInt(ShareFileUtils.superLike, pushBean.data.superLike);
        ShareFileUtils.setInt(ShareFileUtils.likeMe, pushBean.data.likeMe);
        ShareFileUtils.setInt(ShareFileUtils.perfectMatch, pushBean.data.perfectMatch);
        ShareFileUtils.setInt(ShareFileUtils.await, pushBean.data.await);

    }

    private void savaRedpointConfigs(PushSwitchTypeBean pushBean) {

        ShareFileUtils.setInt(ShareFileUtils.MESSAGE_REDPOINT, pushBean.data.viewMeRedpoint);

    }

    private void setMessageUI() {
        imgMessage.setChecked(ShareFileUtils.getBoolean(ShareFileUtils.MESSAGE_SOUND, true));

    }

    private void setAutoplayMobileUI() {
        imgAutoplayMobile.setChecked(ShareFileUtils.getBoolean(ShareFileUtils.AUTO_PLAY_MOBILE, false));

    }

    private void setAutoplayWifiUI() {
        imgAutoplayWifi.setChecked(ShareFileUtils.getBoolean(ShareFileUtils.AUTO_PLAY_WIFI, true));
    }

}
