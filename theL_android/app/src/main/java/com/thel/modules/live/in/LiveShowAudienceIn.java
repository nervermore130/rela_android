package com.thel.modules.live.in;

import com.thel.bean.live.LinkMicResponseBean;
import com.thel.modules.live.bean.AudienceLinkMicResponseBean;
import com.thel.modules.live.surface.watch.LiveWatchObserver;

public interface LiveShowAudienceIn {

    void bindObserver(LiveWatchObserver observer);

    void audienceLinkMicSuccess(AudienceLinkMicResponseBean audienceLinkMicResponseBean);

    /**
     * 挂断
     */
    void hangup();

    /**
     * 主动挂断
     */
    void hangupByOwn();

    /**
     * 主播结束连麦
     */
    void linkMicStop();

    void linkMicStart(LinkMicResponseBean linkMicResponseBean);
}
