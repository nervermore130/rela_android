package com.thel.modules.main.video_discover.autoplay;

import android.text.TextUtils;

import com.thel.bean.PgcVideoSeekListBean;
import com.thel.bean.video.VideoBean;
import com.thel.utils.GsonUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SharedPrefUtils;
import com.thel.utils.Utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by waiarl on 2018/4/4.
 */

public class VideoSeenUtils {
    public static class VideoSeenBean implements Serializable {
        public List<String> list = new ArrayList<>();

        @Override
        public String toString() {
            return "VideoSeenBean{" +
                    "list=" + list +
                    '}';
        }
    }

    public static List<String> getVideoSeenList() {
        final VideoSeenBean bean = getVideoSeenBean();
        return bean.list;
    }

    private static String getVideoSeenFileName() {
        return SharedPrefUtils.FILE_VIDEO + Utils.getMyUserId();
    }

    public static VideoSeenBean getVideoSeenBean() {
        final String json = getVideoSeenBeanJsonString();
        if (TextUtils.isEmpty(json)) {
            return new VideoSeenBean();
        }
        return GsonUtils.getObject(json, VideoSeenBean.class);
    }

    public static String getVideoSeenBeanJsonString() {
        return SharedPrefUtils.getString(getVideoSeenFileName(), SharedPrefUtils.KEY_VIDEO_SEEN_LIST, GsonUtils.createJsonString(new VideoSeenBean()));

    }

    public static void saveOneSeenVideo(VideoBean videoBean) {
        if (videoBean == null || TextUtils.isEmpty(videoBean.id)) {
            return;
        }
        saveOneSeenVideoId(videoBean.id);
    }

    public static void saveOneSeenVideoId(String id) {
        if (TextUtils.isEmpty(id)) {
            return;
        }
        final VideoSeenBean bean = getVideoSeenBean();
        bean.list.remove(id);
        bean.list.add(id);
        saveVideoSeenBean(bean);
    }

    public static void saveVideoSeenBean(VideoSeenBean bean) {
        final String json = GsonUtils.createJsonString(bean);
        SharedPrefUtils.setString(getVideoSeenFileName(), SharedPrefUtils.KEY_VIDEO_SEEN_LIST, json);
    }

    /**
     * 保存pgc视频播放进度
     *
     * @param videoId
     * @param seekTo
     */
    public static void saveOnePgcVideoSeekTo(String videoId, long seekTo) {
        if (!TextUtils.isEmpty(videoId)) {
            String oldJson = ShareFileUtils.getString(SharedPrefUtils.KEY_VIDEO_SEEK_LIST, "");
            PgcVideoSeekListBean bean;
            boolean isExist = false;
            if (TextUtils.isEmpty(oldJson)) {
                bean = new PgcVideoSeekListBean();
                bean.list = new ArrayList<>();
            } else {
                bean = GsonUtils.getObject(oldJson, PgcVideoSeekListBean.class);
                for (int i = 0; i < bean.list.size(); i++) {
                    if (bean.list.get(i).videoId.equals(videoId)) {
                        bean.list.get(i).seekTo = seekTo;
                        isExist = true;
                        break;
                    }
                }
            }
            if (!isExist) {
                PgcVideoSeekListBean.PgcVideoSeekBean pgcVideoSeekBean = new PgcVideoSeekListBean.PgcVideoSeekBean();
                pgcVideoSeekBean.videoId = videoId;
                pgcVideoSeekBean.seekTo = seekTo;
                bean.list.add(pgcVideoSeekBean);
            }
            String newJson = GsonUtils.createJsonString(bean);
            ShareFileUtils.setString(SharedPrefUtils.KEY_VIDEO_SEEK_LIST, newJson);
        }
    }

    /**
     * 获取pgc播放进度列表
     *
     * @return
     */
    public static List<PgcVideoSeekListBean.PgcVideoSeekBean> getPgcVideoSeekList() {
        List<PgcVideoSeekListBean.PgcVideoSeekBean> list = null;
        String oldJson = ShareFileUtils.getString(SharedPrefUtils.KEY_VIDEO_SEEK_LIST, "");
        if (!TextUtils.isEmpty(oldJson)) {
            PgcVideoSeekListBean bean = GsonUtils.getObject(oldJson, PgcVideoSeekListBean.class);
            list = bean.list;
        }
        return list;
    }
}
