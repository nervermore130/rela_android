package com.thel.modules.main.messages.utils;

import android.os.Handler;
import android.text.TextUtils;

import com.thel.app.TheLApp;
import com.thel.db.DBUtils;
import com.thel.imp.friend.FriendUtils;
import com.thel.manager.ChatServiceManager;
import com.thel.modules.main.messages.bean.MsgBean;
import com.thel.modules.main.messages.db.MessageDataBaseAdapter;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;

import org.json.JSONObject;

import java.util.Random;

import static com.thel.modules.main.messages.db.MessageDataBaseDelegate.sendRefreshUiBroadcast;

public class MessagesUtils {
    private static final String TAG = MessagesUtils.class.getSimpleName();

    public static void pushMessageByClient(MsgBean msgBean, Handler uiHandler) {
        // 增加客户端手动push
        String pushContent = "";
        switch (msgBean.msgType) {
            case MsgBean.MSG_TYPE_TEXT:
                pushContent = msgBean.msgText;
                break;
            case MsgBean.MSG_TYPE_MAP:
                pushContent = "[Map]";
                break;
            case MsgBean.MSG_TYPE_IMAGE:
                pushContent = "[Image]";
                break;
            case MsgBean.MSG_TYPE_WINK:
                pushContent = "[Wink]";
                break;
            case MsgBean.MSG_TYPE_FOLLOW:
                pushContent = "[Follow]";
                break;
            case MsgBean.MSG_TYPE_VOICE:
                pushContent = "[Voice]";
                break;
            case MsgBean.MSG_TYPE_SECRET_KEY:
                pushContent = "[Secret]";
                break;
            case MsgBean.MSG_TYPE_HIDDEN_LOVE:
                pushContent = "[Hidden Love]";
                break;
            case MsgBean.MSG_TYPE_EMOJI:
                pushContent = "[Emoji]";
                break;
            case MsgBean.MSG_TYPE_MENTION:
                pushContent = "[AtUser]";
                break;
            case MsgBean.MSG_TYPE_REPLY:
                pushContent = "[Reply]";
                break;
            case MsgBean.MSG_TYPE_MOMENTS_COMMENT:
                pushContent = "[Comment]";
                break;
            case MsgBean.MSG_TYPE_REQUEST:
                pushContent = "[Request]";
                break;
            default:
                L.e(TAG, pushContent);
                break;
        }

        if (pushContent.length() > 0) {
            //            new RequestBussiness().messagePushByClient(new OneRequestNetworkHelper(new UIDataListener() {
            //                @Override
            //                public void onDataChanged(RequestCoreBean rcb) {
            //                }
            //
            //                @Override
            //                public void onErrorHappened(VolleyError error, RequestCoreBean rcb) {
            //                }
            //            }), pushContent, ShareFileUtils.getString(ShareFileUtils.USER_NAME, ""), msgBean.toUserId, " ");
        }
    }


    public static String getStickerUrl(String msgText) {
        try {
            String img = new JSONObject(msgText).getString("img");
            if (!TextUtils.isEmpty(img)) {
                return img;
            }
        } catch (Exception e) {
            return "";
        }

        return "";
    }

    public static long getStickerPackId(String msgText) {
        try {
            String packId = new JSONObject(msgText).getString("packId");
            if (!TextUtils.isEmpty(packId)) {
                return Long.valueOf(packId);
            }
        } catch (Exception e) {
            return 0;
        }

        return 0;
    }

    /**
     * 判断消息是否为系统消息
     *
     * @param msgBean
     * @return
     */
    public static boolean isSystemMsg(MsgBean msgBean) {
        if (msgBean == null) {
            return false;
        }
        if (msgBean.isSystem == MsgBean.IS_WINK_MSG &&
                !msgBean.userId.equals(MessageDataBaseAdapter.WINK_TABLENAME)) {// 挤眼消息.4.0.0判断userid要等于表名
            return false;
        } else if (msgBean.isSystem == MsgBean.IS_FOLLOW_MSG
                && !msgBean.userId.equals(MessageDataBaseAdapter.FOLLOW_TABLENAME)) {// 关注消息，4.0.0判断userid要等于表名
            return false;
        } else return msgBean.isSystem >= MsgBean.IS_SYSTEM_MSG;
    }

    /**
     * 生成消息ID
     *
     * @return
     */
//    public static String generateMsgId() {
//        long currTime = System.currentTimeMillis();
//        String currTimeStr = String.valueOf(currTime);
//        if (currTimeStr.length() >= 7) {// 满7位截取尾部7位数
//            currTimeStr = currTimeStr.substring(currTimeStr.length() - 7);
//        } else {// 不足7位补足7位
//            if (currTime != 0) {
//                currTimeStr = currTimeStr + "0000000".substring(0, 7 - currTimeStr.length());
//            } else {
//                currTimeStr = "1111111";
//            }
//        }
//        return String.valueOf(1000000000 + Integer.valueOf(ShareFileUtils.getString(ShareFileUtils.ID, "0")) + Integer.valueOf(currTimeStr) + new Random().nextInt(20000000));
//    }

    private static long lastTimestamp = 0;

    private static long sequence = 0;

    public static String generateMsgId() {
        long userId = Long.valueOf(ShareFileUtils.getString(ShareFileUtils.ID, "123456789"));
        // 保证为整数型，不要是浮点数
        long currentTimestamp = ((System.currentTimeMillis() - 1500000000000L) / 60000);
        if (currentTimestamp != lastTimestamp) {
            sequence = 0;
            lastTimestamp = currentTimestamp;
        } else if (++sequence >= 4096) {
            //失败
            return "-1";
        }
        // 等价于  id = currentTimestamp * 4096 + sequence;
        return "" + (((userId & 0xfffff) << 32) | ((currentTimestamp & 0xfffff) << 12) | sequence);
    }

    /**
     * 是否是好友
     *
     * @param userId
     * @return
     */
    public static boolean isRelationFriend(String userId) {
        return FriendUtils.isFriend(userId);
    }

    /**
     * 判断消息是否为系统消息
     *
     * @param msgBean
     * @return
     */
    public static boolean isSystenMsg(MsgBean msgBean) {
        if (msgBean == null) {
            return false;
        }
        if (msgBean.isSystem == MsgBean.IS_WINK_MSG && !msgBean.userId.equals(MessageDataBaseAdapter.WINK_TABLENAME)) {// 挤眼消息.4.0.0判断userid要等于表名
            return false;
        } else if (msgBean.isSystem == MsgBean.IS_FOLLOW_MSG && !msgBean.userId.equals(MessageDataBaseAdapter.FOLLOW_TABLENAME)) {// 关注消息，4.0.0判断userid要等于表名
            return false;
        } else return msgBean.isSystem >= MsgBean.IS_SYSTEM_MSG;
    }

    public static boolean isWinkOrFollow(String msgType) {

        if (msgType.equals(MsgBean.MSG_TYPE_WINK)) {
            return true;
        }

        return msgType.equals(MsgBean.MSG_TYPE_FOLLOW);

    }


    public static void saveMessageToDB(MsgBean msgBean) {
        if (msgBean != null && !TextUtils.isEmpty(msgBean.toUserId)) {
            saveMessageToDB(msgBean, msgBean.toUserId);
        }
    }

    public static void saveMessageToDB(MsgBean msgBean, String toUserId) {
        ChatServiceManager.getInstance().insertNewMsg(msgBean, DBUtils.getMainProcessChatTableName(String.valueOf(toUserId)), 1);

        // if (msgBean.msgType.equals("check")) {
        // // check消息不存到消息表
        // return;
        // }
        // 更改msgText在列表中的显示内容
        //		if (msgBean.msgType.equals(MsgBean.MSG_TYPE_IMAGE)) {
        //			msgBean.msgText = TheLApp.getContext().getString(
        //					R.string.message_info_photo);
        //		} else if (msgBean.msgType.equals(MsgBean.MSG_TYPE_VOICE)) {
        //			msgBean.msgText = TheLApp.getContext().getString(
        //					R.string.message_info_voice);
        //		}
        // 存储消息到数据库
        /*MessageDataBaseAdapter dbAdatper = MessageDataBaseAdapter.getInstance(TheLApp.getContext(), ShareFileUtils.getString(ShareFileUtils.ID, ""));
        if (msgBean.isSystem == MsgBean.IS_NORMAL_MSG || msgBean.isSystem == MsgBean.IS_FOLLOW_MSG || msgBean.isSystem == MsgBean.IS_WINK_MSG) {//4.0.1（把挤眼消息和关注消息也放到聊天信息里面）
            if (msgBean.isSystem == MsgBean.IS_FOLLOW_MSG || msgBean.isSystem == MsgBean.IS_WINK_MSG) {//如果是挤眼消息或者关注消息，存储到对应人的聊天时，设为状态已读(4.0.1)新加
                msgBean.msgStatus = MsgBean.MSG_STATUS_SUCCEEDED;
            }
            dbAdatper.saveMessage(msgBean, toUserId);
            if (msgBean.isSystem == MsgBean.IS_FOLLOW_MSG || msgBean.isSystem == MsgBean.IS_WINK_MSG) {//如果是挤眼消息或者关注消息，存储到对应人的聊天时，由于本地存储了消息，所以要刷新列表
                sendRefreshUiBroadcast(msgBean);
            }
        } else {
            dbAdatper.saveMessage(msgBean, MessageDataBaseAdapter.SYSTEM_TABLENAME);
        }*/
    }
}
