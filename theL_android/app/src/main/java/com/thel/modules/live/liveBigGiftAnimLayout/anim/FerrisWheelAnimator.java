package com.thel.modules.live.liveBigGiftAnimLayout.anim;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PointF;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.thel.R;
import com.thel.modules.live.liveBigGiftAnimLayout.LiveBigAnimUtils;
import com.thel.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


/**
 * Created by the L on 2016/10/12.
 */

public class FerrisWheelAnimator {
    private final Context mContext;
    private final int mDuration;
    private final Handler mHandler;
    private final int mFrameFreq;
    private final int mRoomFreq;
    private final Random mRandom;
    private final String[] mRoomRes;
    private final String[] mFrameRes;
    private final String[] mBalloonRes;
    private final String[] mStarRes;
    private final int minBallonCount;
    private final int differBallonCount;
    private final int differBallonScale;
    private final int ballonDur;
    private final int rate;
    private final int defaultBalloonWidth;
    private final int mStarFreq;
    private final int mStarScaleDur;
    private final int mStarScaleFreq;
    private final int mInDuration;
    private final int mOutDuration;
    private final String foldPath;
    private boolean isPlaying;
    private int mWidth;
    private int mHeight;

    public FerrisWheelAnimator(Context context, int duration) {
        mContext = context;
        mDuration = duration;
        mHandler = new Handler(Looper.getMainLooper());
        mRandom = new Random();
        /*mFrameRes = new int[]{R.drawable.ferris_wheel_frame1, R.drawable.ferris_wheel_frame2};//摩天轮框架资源
        mRoomRes = new int[]{R.drawable.ferris_wheel_room1, R.drawable.ferris_wheel_room2};//摩天轮room资源
        mBalloonRes = new int[]{R.drawable.ferris_wheel_ballon1, R.drawable.ferris_wheel_ballon2, R.drawable.ferris_wheel_ballon3, R.drawable.ferris_wheel_ballon4};//摩天轮气球资源
        mStarRes = new int[]{R.drawable.ferris_wheel_star1, R.drawable.ferris_wheel_star2};//摩天轮星星资源*/
        foldPath = "anim/ferris";
        mFrameRes = new String[]{"ferris_wheel_frame1", "ferris_wheel_frame2"};//摩天轮框架资源
        mRoomRes = new String[]{"ferris_wheel_room1", "ferris_wheel_room2"};//摩天轮room资源
        mBalloonRes = new String[]{"ferris_wheel_ballon1", "ferris_wheel_ballon2", "ferris_wheel_ballon3", "ferris_wheel_ballon4"};//摩天轮气球资源
        mStarRes = new String[]{"ferris_wheel_star1", "ferris_wheel_star2"};//摩天轮星星资源
        minBallonCount = 2;//每次发射气球最小数
        differBallonCount = 4;//气球变化区间
        differBallonScale = 4;//气球变大倍数变化区间
        ballonDur = 3000;//气球持续时间
        defaultBalloonWidth = Utils.dip2px(mContext, 30);//默认气球大小
        mFrameFreq = 500;//框架变化频率
        mRoomFreq = 500 / mRoomRes.length;//轮 变化区域
        mStarFreq = 150;//星星闪烁频率
        mStarScaleDur = 500;//星星缩放时间
        mStarScaleFreq = 1200;//星星两次缩放之间周期
        rate = 400;//气球发射频率
        mInDuration = 1000;//动画淡入时间
        mOutDuration = 1000;//动画淡出时间
    }

    public int start(final ViewGroup parent) {
        isPlaying = true;
        mWidth = parent.getMeasuredWidth();
        mHeight = parent.getMeasuredHeight();
        if (mWidth == 0 || mHeight == 0) {
            return 0;
        }
        final RelativeLayout background = (RelativeLayout) RelativeLayout.inflate(mContext, R.layout.live_big_anim_ferris_wheel_layout, null);
        background.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
        parent.addView(background);
        initBackground(background);
        setBackground(background);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                isPlaying = false;
                background.clearAnimation();
                parent.removeView(background);
            }
        }, mDuration);
        return mDuration;
    }

    private void initBackground(RelativeLayout background) {
        final ImageView img_room = background.findViewById(R.id.img_room);
        final ImageView img_frame = background.findViewById(R.id.img_frame);
        final ImageView img_star1 = background.findViewById(R.id.img_star1);
        final ImageView img_star2 = background.findViewById(R.id.img_star2);
        final ImageView img_star3 = background.findViewById(R.id.img_star3);
        final ImageView img_star4 = background.findViewById(R.id.img_star4);
        final ImageView img_star5 = background.findViewById(R.id.img_star5);
        LiveBigAnimUtils.setAssetBackground(img_room, foldPath, mRoomRes[0]);
        LiveBigAnimUtils.setAssetBackground(img_frame, foldPath, mFrameRes[0]);
        LiveBigAnimUtils.setAssetBackground(img_star1, foldPath, mStarRes[0]);
        LiveBigAnimUtils.setAssetBackground(img_star2, foldPath, mStarRes[0]);
        LiveBigAnimUtils.setAssetBackground(img_star3, foldPath, mStarRes[0]);
        LiveBigAnimUtils.setAssetBackground(img_star4, foldPath, mStarRes[0]);
        LiveBigAnimUtils.setAssetBackground(img_star5, foldPath, mStarRes[0]);
    }

    private void setBackground(ViewGroup background) {
        final View rel_ferris = background.findViewById(R.id.rel_ferris);
        final ImageView img_frame = background.findViewById(R.id.img_frame);
        final ImageView img_room = background.findViewById(R.id.img_room);
        rel_ferris.setScaleX(0.1f);
        rel_ferris.setScaleY(0.1f);
        rel_ferris.animate().scaleXBy(1f).scaleYBy(1f).setInterpolator(new LinearInterpolator()).setDuration(mInDuration).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (isPlaying) {
                    //                    LiveBigGiftAnimLayout.setFrameAnim(mContext, img_frame, mFrameRes, mFrameFreq);
                    //                    LiveBigGiftAnimLayout.setFrameAnim(mContext, img_room, mRoomRes, mRoomFreq);
                    LiveBigAnimUtils.setFrameAnim(mContext, img_frame, foldPath, mFrameRes, mFrameFreq);
                    LiveBigAnimUtils.setFrameAnim(mContext, img_room, foldPath, mRoomRes, mRoomFreq);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        rel_ferris.postDelayed(new Runnable() {
            @Override
            public void run() {
                isPlaying = false;
                rel_ferris.animate().alpha(0).setDuration(mOutDuration);
            }
        }, mDuration - mOutDuration);
        addBalloon(background);
        lightingStar(background);
    }

    private void lightingStar(ViewGroup background) {
        final List<View> stars = new ArrayList<>();
        stars.add(background.findViewById(R.id.img_star1));
        stars.add(background.findViewById(R.id.img_star2));
        stars.add(background.findViewById(R.id.img_star3));
        stars.add(background.findViewById(R.id.img_star4));
        stars.add(background.findViewById(R.id.img_star5));
        for (View v : stars) {
            //  LiveBigGiftAnimLayout.setFrameAnim(mContext, v, mStarRes, mStarFreq);
            LiveBigAnimUtils.setFrameAnim(mContext, v, foldPath, mStarRes, mStarFreq);
            setScaleAnim(v);
        }
    }

    private void setScaleAnim(final View v) {
        if (!isPlaying) {
            return;
        }
        final int freq = mStarScaleFreq;
        AnimatorSet anim = new AnimatorSet();
        ObjectAnimator animX = ObjectAnimator.ofFloat(v, "scaleX", 1f, 0.1f, 1f, 0.6f, 1f);
        ObjectAnimator animY = ObjectAnimator.ofFloat(v, "scaleY", 1f, 0.1f, 1f, 0.6f, 1f);
        anim.play(animX).with(animY);
        anim.setDuration(mStarScaleDur);
        anim.setInterpolator(new LinearInterpolator());
        anim.start();
        anim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (isPlaying) {
                    v.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            setScaleAnim(v);
                        }
                    }, freq);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }


    private void addBalloon(final ViewGroup background) {
        background.post(new Runnable() {
            @Override
            public void run() {
                final int snowCount = mRandom.nextInt(differBallonCount) + minBallonCount;
                for (int i = 0; i < snowCount; i++) {
                    flowOneBallon(background);
                }
                if (isPlaying) {
                    background.postDelayed(this, rate);
                }
            }


        });
    }

    private void flowOneBallon(final ViewGroup background) {
        if (!isPlaying) {
            return;
        }
        final ImageView img_ballon = new ImageView(mContext);
        background.addView(img_ballon);
        final int resIndex = mRandom.nextInt(mBalloonRes.length);
        int w = defaultBalloonWidth;
        int h = getImageHeight(resIndex, w);
        //  img_ballon.setImageResource(mBalloonRes[resIndex]);
        LiveBigAnimUtils.setAssetImage(img_ballon, foldPath, mBalloonRes[resIndex]);
        img_ballon.setLayoutParams(new RelativeLayout.LayoutParams(w, h));
        final Path path = getBallonPath(w, h);
        final float scale = mRandom.nextFloat() * differBallonScale;
        FloatAnimation anim = new FloatAnimation(path, background, img_ballon, scale);
        anim.setDuration(ballonDur);
        anim.setInterpolator(new LinearInterpolator());
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                background.post(new Runnable() {
                    @Override
                    public void run() {
                        background.removeView(img_ballon);
                    }
                });
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        img_ballon.startAnimation(anim);
    }

    private Path getBallonPath(int w, int h) {
        Path p = new Path();
        p.moveTo(mWidth - mRandom.nextFloat() * mWidth / 4, mHeight + mHeight / 10);
        PointF p1 = new PointF(mRandom.nextFloat() * (mWidth + Utils.dip2px(mContext, 40) - Utils.dip2px(mContext, 20)), mHeight - mRandom.nextFloat() * mHeight / 2);
        PointF p2 = new PointF(mRandom.nextFloat() * (mWidth + Utils.dip2px(mContext, 40) - Utils.dip2px(mContext, 20)), p1.y - mRandom.nextFloat() * mHeight / 2);
        PointF p3 = new PointF(mRandom.nextFloat() * (mWidth + Utils.dip2px(mContext, 40) - Utils.dip2px(mContext, 20)), p2.y - mRandom.nextFloat() * mHeight / 2);
        p.cubicTo(p1.x, p1.y, p2.x, p2.y, p3.x, p3.y);
        return p;
    }

    private int getImageHeight(int resId, int w) {
        int h;
        switch (resId) {
            case 0:
                h = w * 457 / 188;
                break;
            case 1:
                h = w * 336 / 130;
                break;
            case 2:
                h = w * 324 / 130;
                break;
            case 3:
                h = w * 297 / 140;
                break;
            default:
                h = w;
                break;
        }
        return h;
    }

    static class FloatAnimation extends Animation {
        private final float mScale;
        private PathMeasure mPm;
        private View mView;
        private float mDistance;

        public FloatAnimation(Path path, View parent, View child, float scale) {
            mPm = new PathMeasure(path, false);
            mDistance = mPm.getLength();
            mView = child;
            parent.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            mScale = scale;
        }

        @Override
        protected void applyTransformation(float factor, Transformation transformation) {
            Matrix matrix = transformation.getMatrix();
            mPm.getMatrix(mDistance * factor, matrix, PathMeasure.POSITION_MATRIX_FLAG);
            if (factor <= 0.2) {
                mView.setScaleX((float) (mScale * (factor / 0.2)) + 1);
                mView.setScaleY((float) (mScale * (factor / 0.2)) + 1);
            }
            if (factor > 0.8) {
                mView.setAlpha(1 - (factor - 0.8f) / 0.2f);
            }
        }
    }
}
