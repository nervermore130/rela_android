package com.thel.modules.live.surface.show;


import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import com.thel.bean.RejectMicBean;
import com.thel.bean.live.LiveMultiOnCreateBean;
import com.thel.bean.live.LiveMultiSeatBean;
import com.thel.bean.live.MultiSpeakersBean;
import com.thel.callback.IConnectMic;
import com.thel.modules.live.bean.AgoraBean;
import com.thel.modules.live.bean.LivePkFriendBean;
import com.thel.modules.live.bean.LivePkGemNoticeBean;
import com.thel.modules.live.bean.LivePkHangupBean;
import com.thel.modules.live.bean.LivePkInitBean;
import com.thel.modules.live.bean.LivePkRequestNoticeBean;
import com.thel.modules.live.bean.LivePkResponseNoticeBean;
import com.thel.modules.live.bean.LivePkStartNoticeBean;
import com.thel.modules.live.bean.LivePkSummaryNoticeBean;
import com.thel.modules.live.bean.LiveRoomMsgBean;
import com.thel.modules.live.bean.LiveRoomMsgConnectMicBean;
import com.thel.modules.live.bean.SoftGiftBean;
import com.thel.modules.live.bean.TopTodayBean;
import com.thel.modules.live.in.LiveShowCaptureMsgIn;
import com.thel.modules.live.in.LiveShowCaptureStreamIn;
import com.thel.modules.live.in.LiveShowCaptureViewIn;
import com.thel.modules.live.utils.LinkMicOrPKRefuseUtils;
import com.thel.modules.live.utils.LiveStatus;
import com.thel.modules.live.view.expensive.TopGiftBean;
import com.thel.utils.L;

import java.util.List;

import io.agora.rtc.IRtcEngineEventHandler;

import static com.thel.modules.live.LiveCodeConstants.*;

/**
 * Created by waiarl on 2017/11/5.
 */

public class LiveShowObserver extends Handler{

    private LiveShowCaptureStreamIn stream;
    private LiveShowCaptureViewIn view;
    private LiveShowCaptureMsgIn client;
    private boolean destoryed = false;
    private AllBindListener listener;
    private IConnectMic iConnectMic;
    private boolean isAllBind = false;


    /**
     * 初始化LiveShowCaptureObserver的值
     */

    public LiveShowObserver() {
        super(Looper.getMainLooper());
    }

    public void bindStream(LiveShowCaptureStreamIn stream) {
        this.stream = stream;
        judgeBindAll();
    }

    public void bindView(LiveShowCaptureViewIn view) {
        this.view = view;
        judgeBindAll();

    }

    public void bindMsgClient(LiveShowCaptureMsgIn client) {
        this.client = client;
        judgeBindAll();
    }

    public void bindConnectMic(IConnectMic iConnectMic) {
        this.iConnectMic = iConnectMic;
        judgeBindAll();
    }

    private void judgeBindAll() {
        if (stream != null && view != null && client != null) {
            isAllBind = true;
            if (listener != null) {
                listener.bindAll();
            }
        } else {
            isAllBind = false;

        }
    }

    public void sendDanmu(String userLevel, String content, String barrageId) {
        if (isAllBind) {
            client.sendDanmu(userLevel, content, barrageId);
        }
    }

    public void sendInputMsg(String msg) {
        if (isAllBind) {
            client.sendMsg(msg);
        }
    }

    @Override
    public void handleMessage(Message msg) {
        if (destoryed) {
            return;
        }
        if (view == null) {
            return;
        }
        switch (msg.what) {
            case UI_EVENT_UPDATE_AUDIENCE_COUNT:
                view.updateAudienceCount();
                break;
            case UI_EVENT_UPDATE_BROADCASTER_NET_STATUS:
                view.refreshMsg();
                break;
            case UI_EVENT_SCROLL_TO_BOTTOM:
                view.smoothScrollToBottom();
                break;
            case UI_EVENT_RESET_IS_BOTTOM:
                view.resetIsBottom();
                break;
            case UI_EVENT_AUTO_PING:
                client.autoPingServer();
                break;
            case UI_EVENT_UPDATE_MY_NET_STATUS_BAD:
                view.updateMyNetStatusBad();
                break;
            case UI_EVENT_STOP_SHOW:
                view.showCloseAlert();
                break;
            case UI_EVENT_UPDATE_MY_NET_STATUS_GOOD:
                view.networkGood();
                break;
            case UI_EVENT_ANCHOR_LIVE_CONNECT_BREAK://连接中断
                view.connectBreak();
                break;
            case UI_EVENT_ANCHOR_LIVE_CONNECT_FAILED://连接失败
                view.connectFailed();
                break;
            case UI_EVENT_ANCHOR_LIVE_CONNECTING://连接中
                view.connectiong();
                break;
            case UI_EVENT_ANCHOR_LIVE_CONNECT_SUCCESS://连接成功
                view.connectSuccess();
                break;
            case UI_EVENT_GIFT_RECEIVE_MSG://收到 接收到礼物广播
                Bundle bundle = msg.getData();
                String payload = bundle.getString("payload");
                String code = bundle.getString("code");
                view.parseGiftMsg(payload, code);
                break;
            case UI_EVENT_DANMU_RECEIVE_MSG://弹幕
                String danMsg = (String) msg.obj;
                view.showDanmu(danMsg);
                break;
            case UI_EVENT_JOIN_USER://有人加入房间
                final LiveRoomMsgBean liveRoomMsgBean = (LiveRoomMsgBean) msg.obj;
                view.joinUser(liveRoomMsgBean);
                break;
            case UI_EVENT_JOIN_VIP_USER://有人加入房间
                final LiveRoomMsgBean liveRoomMsgBean2 = (LiveRoomMsgBean) msg.obj;
                view.joinVipUser(liveRoomMsgBean2);
                break;
            case UI_EVENT_ANCHOR_CLOSE_DIALOG:
                final String ob = (String) msg.obj;
                view.refreshCloseDialog(ob);
                break;
            case UI_EVENT_ANCHOR_PROGRESSBAR_GONE://progress_bar 为gone ，七牛sdk
                view.progressbarGone();
                break;
            case UI_EVENT_ANCHOR_FINISH:
                if (stream != null) {
                    stream.finishStream();
                }
                break;
            case UI_EVENT_ANCHOR_LIVE_DISCONNECTED:
                view.liveDisconnected();
                break;
            case UI_EVENT_ANCHOR_LIVE_NETWORK_WARNING_NOTIFY:
                final String text = (String) msg.obj;
                view.liveNetworkWaringNotify(text);
                break;
            case UI_EVENT_ANCHOR_LIVE_STREAM_FRAME_SEND_SLOW:
                view.networkSlow();
                break;
            case UI_EVENT_CLEAR_INPUT:
                view.clearInput();
                break;
            case UI_EVENT_ANCHOR_LOCK_INPUT:
                view.lockInput();

                break;
            case UI_EVENT_OPEN_INPUT_TRUE:
                String ms = (String) msg.obj;
                view.setDanmuResult(ms);//弹幕返回结果
                break;
            case UI_EVENT_ANCHOR_OPEN_INPUT_FALSE:
                view.openInput(false);
                break;
            /***********************************************一下为Pk**************************************************************/
            case UI_EVENT_ANCHOR_PK_REQUEST_NOTICE://收到PK申请通知
                final LivePkRequestNoticeBean pkRequestNoticeBean = (LivePkRequestNoticeBean) msg.obj;
                view.showPkRequestView(pkRequestNoticeBean);
                stream.showPkRequest(pkRequestNoticeBean);
                break;
            case UI_EVENT_ANCHOR_PK_RESPONSE_NOTICE://收到PK回复通知
                final LivePkResponseNoticeBean pkResponseNoticeBean = (LivePkResponseNoticeBean) msg.obj;
                view.showPkResponseView(pkResponseNoticeBean);
                stream.showPkResponse(pkResponseNoticeBean);
                break;
            case UI_EVENT_ANCHOR_PK_START_NOTICE://PK开始消息
                final LivePkStartNoticeBean pkStartNoticeBean = (LivePkStartNoticeBean) msg.obj;
                view.showPkStartView(pkStartNoticeBean);
                stream.showPkStart(pkStartNoticeBean);
                break;
            case UI_EVENT_ANCHOR_PK_CANCEL_NOTICE://收到PK取消通知
                final String pkCancelPayload = (String) msg.obj;
                view.showPkCancelView(pkCancelPayload);
                stream.showPkCancel(pkCancelPayload);
                break;
            case UI_EVENT_ANCHOR_PK_HANGUP_NOTICE://收到PK挂断 通知
                final LivePkHangupBean pkHangupBean = (LivePkHangupBean) msg.obj;
                view.showPkHangupView(pkHangupBean);
                stream.showPkHangup(pkHangupBean);
                break;
            case UI_EVENT_ANCHOR_PK_SUMMARY_NOTICE://Pk总结阶段 通知
                final LivePkSummaryNoticeBean pkSummaryNoticeBean = (LivePkSummaryNoticeBean) msg.obj;
                view.showPkSummaryView(pkSummaryNoticeBean);
                stream.showPkSummary(pkSummaryNoticeBean);
                break;
            case UI_EVENT_ANCHOR_PK_STOP_NOTICE://收到PK结束 通知
                final String pkStopPayload = (String) msg.obj;
                view.showPkStopView(pkStopPayload);
                stream.showPkStop(pkStopPayload);
                break;
            case UI_EVENT_ANCHOR_PK_GEM_NOTICE://主播收到礼物时 服务器发送主播软妹币增量通知给双方直播室内的所有人
                final LivePkGemNoticeBean pkGemNoticeBean = (LivePkGemNoticeBean) msg.obj;
                view.showPkGemView(pkGemNoticeBean);
                break;
            case UI_EVENT_ANCHOR_PK_REQUEST_PK_OK://发送PK申请成功
                break;
            case UI_EVENT_ANCHOR_PK_NOT_LIVING://主播不在直播
                view.showPkRequestErrorView(LiveRoomMsgBean.TYPE_LIVE_PK_RESPONSE_CODE_NOT_LIVING);
                break;
            case UI_EVENT_ANCHOR_PK_IN_LINKMIC://主播连麦中
                view.showPkRequestErrorView(LiveRoomMsgBean.TYPE_LIVE_PK_RESPONSE_CODE_IN_LINKMIC);
                break;
            case UI_EVENT_ANCHOR_PK_IN_PK://主播Pk中
                view.showPkRequestErrorView(LiveRoomMsgBean.TYPE_LIVE_PK_RESPONSE_CODE_IN_PK);
                break;
            case UI_EVENT_ANCHOR_PK_REQUEST_REJECT://上次发起PK被该主播拒绝，需等待10分钟才能对该主播再次发起PK
                view.showPkRequestErrorView(LiveRoomMsgBean.TYPE_LIVE_PK_RESPONSE_CODE_REJECT);
                break;
            case UI_EVENT_ANCHOR_PK_LOWER_VERSION://过低的版本
                view.showPkRequestErrorView(LiveRoomMsgBean.TYPE_LIVE_PK_RESPONSE_CODE_LOW_CLI_VER);
                break;
            case UI_EVENT_ANCHOR_PK_CODE_500://服务端错误
                view.showPkRequestErrorView(LiveRoomMsgBean.TYPE_LIVE_PK_RESPONSE_CODE_500);
                break;
            case UI_EVENT_PK_INIT_MSG://pk断线重连信息
                final LivePkInitBean livePkInitBean = (LivePkInitBean) msg.obj;
                view.showLivePkInitView(livePkInitBean);
                stream.showPkInitStream(livePkInitBean);
                break;
            case UI_EVENT_ANCHOR_PK_REQUEST_TIME_OUT://Pk请求超时超时
                final String userId = (String) msg.obj;
                view.showPkRequestTimeOutView(userId);
                break;
            /***********************************************以上为Pk**************************************************************/

            case UI_EVENT_ANCHOR_LINK_MIC_REQUEST://接收到连麦请求
                LiveRoomMsgConnectMicBean liveRoomMsgConnectMicBean = (LiveRoomMsgConnectMicBean) msg.obj;
                view.showConnectMicDialog(liveRoomMsgConnectMicBean);
                if (liveRoomMsgConnectMicBean != null && liveRoomMsgConnectMicBean.method != null) {

                    if (liveRoomMsgConnectMicBean.method.equals("start")) { //连麦开始
                        view.showLinkMicLayer(liveRoomMsgConnectMicBean.getNickName(), liveRoomMsgConnectMicBean.userId);
                        stream.linkMickStart();
                    }

                    if (liveRoomMsgConnectMicBean.method.equals("request")) {//发起连麦请求
                        stream.linkMicRequest(liveRoomMsgConnectMicBean);
                    }

                    if (liveRoomMsgConnectMicBean.method.equals("response")) {
                        String result = liveRoomMsgConnectMicBean.result;

                        if (result != null && result.equals("no")) {//请求被拒绝
                            view.lingMicCancel();
//                            LivePkUtils.saveOneLinkMicRefuser(liveRoomMsgConnectMicBean.toUserId);

                            L.d("LiveShowObserver", " liveRoomMsgConnectMicBean.fromUserId : " + liveRoomMsgConnectMicBean.fromUserId);

                            L.d("LiveShowObserver", " liveRoomMsgConnectMicBean.toUserId : " + liveRoomMsgConnectMicBean.toUserId);

                            LinkMicOrPKRefuseUtils.addLinkMicRefuse(liveRoomMsgConnectMicBean.fromUserId);
                        } else {     //请求被同意
                            stream.linkMicResponse(liveRoomMsgConnectMicBean);
                        }

                    }

                    if (liveRoomMsgConnectMicBean.method.equals("stop")) {
                        stream.linkMicStop();
                        view.linkMicHangup();
                    }

                    if (liveRoomMsgConnectMicBean.method.equals("hangup")) {
                        stream.linkMicHangup();
                        view.linkMicHangup();
                    }

                    L.d("videoLinkAdd", "-----UI_EVENT_LINK_MIC_REQUEST-----");

                    if (liveRoomMsgConnectMicBean.method.equals("videoLinkDel")) {
                        LivePkFriendBean addAudience = new LivePkFriendBean();
                        addAudience.avatar = liveRoomMsgConnectMicBean.avatar;
                        addAudience.nickName = liveRoomMsgConnectMicBean.getNickName();
                        addAudience.id = liveRoomMsgConnectMicBean.userId;
                        view.videoLinkDel(addAudience);
                    }

                    if (liveRoomMsgConnectMicBean.method.equals("videoLinkAdd")) {

                        L.d("videoLinkAdd", " method " + liveRoomMsgConnectMicBean.method);

                        LivePkFriendBean delAudience = new LivePkFriendBean();
                        delAudience.avatar = liveRoomMsgConnectMicBean.avatar;
                        delAudience.nickName = liveRoomMsgConnectMicBean.getNickName();
                        delAudience.id = liveRoomMsgConnectMicBean.userId;
                        view.videoLinkAdd(delAudience);
                    }
                }
                break;
            case UI_EVENT_ANCHOR_LINK_MIC_RESPONSE:
                Bundle data = msg.getData();
                boolean dailyGuard = data.getBoolean("dailyGuard");
                String toUserId = data.getString("toUserId");
                String nickName = data.getString("nickName");
                String avatar = data.getString("avatar");
                view.showLinkMicSendSuccess(toUserId, nickName, avatar, dailyGuard);
                break;
            case UI_EVENT_LINK_MIC_IS_ACCEPT://连麦请求是否被对方同意 yes : 同意 no : 不同意
                String result = (String) msg.obj;
                if (result.equals("yes")) {
                    view.linkMicAccept();
                } else {
                    view.lingMicCancel();
                }
                break;
            case UI_EVENT_TOP_GIFT:
                final TopGiftBean topGiftBean = (TopGiftBean) msg.obj;
                view.showTopGiftView(topGiftBean);
                break;
            /**********************************************以下为多人连麦**************************************************************/
            case UI_EVENT_INIT_MULTI:
                LiveMultiOnCreateBean liveMultiOnCreateBean = (LiveMultiOnCreateBean) msg.obj;
                view.initMultiAdapter(liveMultiOnCreateBean);
                break;
            case UI_EVENT_REFRESH_SEAT:
                LiveMultiSeatBean seat = (LiveMultiSeatBean) msg.obj;
                view.refreshSeat(seat);
                break;
            case UI_EVENT_GUEST_GIFT:
                LiveMultiSeatBean seat1 = (LiveMultiSeatBean) msg.obj;
                view.guestGift(seat1);
                break;
            case UI_EVENT_START_ENCOUNTER:
                LiveMultiSeatBean seat2 = (LiveMultiSeatBean) msg.obj;
                if (seat2 != null) {
                    view.startEncounter(seat2.leftTime);
                }
                break;
            case UI_EVENT_END_ENCOUNTER:
                LiveMultiSeatBean seat3 = (LiveMultiSeatBean) msg.obj;
                if (seat3 != null) {
                    view.endEncounter(seat3);
                }
                break;
            case UI_EVENT_ENCOUNTER_SB:
                break;
            case UI_EVENT_ENCOUNTER_SPEAKERS:
                MultiSpeakersBean multiSpeakersBean = (MultiSpeakersBean) msg.obj;
                if (multiSpeakersBean != null) {
                    view.speakers(multiSpeakersBean);
                }
                break;
            case UI_EVENT_MIC_ADD:
                LiveMultiSeatBean.CoupleDetail coupleDetail = (LiveMultiSeatBean.CoupleDetail) msg.obj;
                if (coupleDetail != null) {
                    view.addSortMic(coupleDetail);
                }
                break;
            case UI_EVENT_MIC_DEL:
                LiveMultiSeatBean liveMultiSeatBean = (LiveMultiSeatBean) msg.obj;
                if (liveMultiSeatBean != null) {
                    view.removeSortMic(liveMultiSeatBean.userId);
                }
                break;
            case UI_EVENT_MIC_LIST:
                LiveMultiSeatBean seat6 = (LiveMultiSeatBean) msg.obj;
                if (seat6 != null) {
                    view.getMicSortList(seat6.list);
                }
                break;
            case UI_EVENT_FREE_BARRAGE:
                view.freeBarrage();
                break;

            /**********************************************以上为多人连麦**************************************************************/

            case UI_EVENT_NO_FACE:
                boolean visible = (boolean) msg.obj;
                view.onFaceDetection(visible);
                break;
            case UI_EVENT_GET_TOP_TODAY_LIST:
                List<LivePkFriendBean> list = (List<LivePkFriendBean>) msg.obj;
                view.getTopFansTodayList(list);
                break;
            case UI_EVENT_TOP_TODAY:
                TopTodayBean topTodayBean = (TopTodayBean) msg.obj;
                view.topToday(topTodayBean);
                break;
            case UI_EVENT_ANCHOR_ANDIENCE_NOT_IN:
                view.andienceNotIn();
                break;
            case UI_EVENT_ANCHOR_REJECT_LINK_MIC:
                RejectMicBean rejectMicBean = (RejectMicBean) msg.obj;
                view.rejectMic(rejectMicBean);
                break;
            default:
                break;
        }
    }

    public void tryCreateClient() {
        if (client != null) {
            client.tryCreateClient();
        }
    }

    public void add(LiveRoomMsgBean liveRoomMsgBean) {
        view.addLiveRoomMsg(liveRoomMsgBean);
    }

    public void switchCamera() {
        stream.switchCamera();
    }

    public void onDestory() {
        destoryed = true;
        isAllBind = false;
        removeCallbacksAndMessages(null);
    }

    /**
     * 屏蔽某个用户
     *
     * @param userId
     */
    public void banHer(String userId) {
        client.banHer(userId);
    }

    /**
     * 调整美颜参数
     *
     * @param key
     * @param progress
     */
    public void adjustBeauty(String key, float progress) {
        stream.adjustBeauty(key, progress);
    }

    /**
     * 请求直播结束信息
     */
    public void exucuteColseRunable() {
        if (client != null) {
            client.exucuteColseRunable();
        }
    }

    /**
     * 发送关闭直播消息
     */
    public void sendColseMsg() {
        if (client != null) {
            client.sendCloseMsg();
        }
    }

    /***********************************************一下为Pk**************************************************************/
    /**
     * '
     * 发送PK申请信息
     *
     * @param userId
     * @param x
     * @param y
     * @param width
     * @param height
     */
    public void sendPkRequestMsg(String userId, float x, float y, float width, float height) {
        if (client != null) {
            client.sendPkRequest(userId, x, y, width, height);
        }
    }

    /**
     * 发送PK取消申请信息
     *
     * @param id
     */
    public void sendPkRequestCancelMsg(String id) {
        if (client != null) {
            client.sendPkCancel(id);
        }
    }

    /**
     * 发送PK申请的回复消息
     *
     * @param userId
     * @param result
     * @param x
     * @param y
     * @param width
     * @param height
     */
    public void sendPkResponseMsg(String userId, String result, float x, float y, float width, float height) {
        if (client != null) {
            client.sendPkResponse(userId, result, x, y, width, height);
        }
    }

    /**
     * 对方申请PK自己同意时，要立刻显示PK界面
     */
    public void showPkStream() {
        if (stream != null) {
            stream.showPkStream();
        }
    }

    /**
     * 发送PK挂断消息
     * 同时本地结束PK流回复正常流
     *
     * @param userId
     */
    public void sendPkHangUpMsg(String userId) {
        if (client != null) {
            client.sendPkHangup(userId);
        }
        if (stream != null) {
            stream.finishPk();
        }
    }

    /**
     * 停止Pk
     */
    public void stopPkStream() {
        if (stream != null) {
            stream.finishPk();
        }
    }

    /***********************************************一上为Pk**************************************************************/

    public void connectMic(String method, String toUserId, float x, float y, float height, float width, String nickName, String avatar, boolean dailyGuard) {
        if (iConnectMic != null) {
            iConnectMic.connectMic(method, toUserId, x, y, height, width, nickName, avatar, dailyGuard);
        }
    }

    public void responseConnectMic(String method, String toUserId, String result, float x, float y, float height, float width) {
        if (iConnectMic != null) {
            iConnectMic.rejectConnectMic(method, toUserId, result, x, y, height, width);
        }
    }

    public void cancelConnectMic(String method, String toUserId) {
        if (iConnectMic != null) {
            iConnectMic.cancelConnectMic(method, toUserId);
        }
    }

    public void hangupConnectMic(String method, String toUserId) {
        if (iConnectMic != null) {
            iConnectMic.hangupConnectMic(method, toUserId);
        }
        if (stream != null) {

            L.d("LiveShowCaptureView", " hangupConnectMic toUserId : " + toUserId);

            stream.linkMicHangupByOwn();
        }

    }

    public void responseAudienceLinkMic(String method, String toUserId) {
        if (iConnectMic != null) {
            iConnectMic.responseAudienceLinkMic(method, toUserId);
        }
        if (stream != null) {
            stream.audienceLinkMicResponse();
        }
    }


    public interface AllBindListener {
        void bindAll();
    }

    public void setAllBindListener(AllBindListener listener) {
        this.listener = listener;
    }

    /************************4.8.0start******************************/
    /**
     * 请求下麦
     *
     * @param position
     */
    public void offSeat(int position) {
        client.offSeat(position);
    }

    /**
     * 主播禁麦/开麦
     *
     * @param position
     * @param micStatus "close"禁麦   "on"开麦
     */
    public void mute(int position, String micStatus) {
        client.mute(position, micStatus);
    }

    /**
     * 开始相遇倒计时
     */
    public void startEncounter() {
        client.startEncounter();
    }

    /**
     * 说话声音音量提示
     */
    public void uploadAudioVolumn(IRtcEngineEventHandler.AudioVolumeInfo[] speakers) {
        if (client != null) {
            client.uploadAudioVolumn(speakers);
        }
    }

    /**
     * 开启排麦
     */
    public void openMicSort() {
        if (client != null) {
            client.openMicSort();
        }
    }

    /**
     * 关闭排麦
     */
    public void closeMicSort() {
        if (client != null) {
            client.closeMicSort();
        }
    }

    public void agreeOnSeat(String userId, AgoraBean agoraBean) {
        if (client != null) {
            client.agreeOnSeat(userId, agoraBean);
        }
    }

    public void getMicSortList() {
        if (client != null) {
            client.getMicSortList();
        }
    }

    public void getTopFansTodayList() {
        if (client != null) {
            client.getTopFansTodayList();
        }
    }

    /**
     * 静音/取消静音。该方法用于允许/禁止往网络发送本地音频流。
     *
     * @param mute
     */
    public void muteLocalAudioStream(boolean mute) {
        if (stream != null) {
            stream.muteLocalAudioStream(mute);
        }

        if (client != null) {
            client.muteAnchor(mute);
        }
    }

    /**
     * 播放音效
     *
     * @param position
     */
    public void playEffect(int position) {
        if (stream != null) {
            stream.playEffect(position);
        }
    }

    /************************4.8.0end******************************/

    public LiveStatus getLiveStatus() {
        if (stream != null) {
            return stream.getListStatus();
        }
        return null;
    }

    public void switchStricker(SoftGiftBean giftBean) {
        if (stream != null) stream.switchStricker(giftBean);
    }

    /**
     * 收到AR礼物，回执服务端
     */
    public void arGiftReceipt(String payload) {
        if (client != null) client.arGiftReceipt(payload);
    }
}
