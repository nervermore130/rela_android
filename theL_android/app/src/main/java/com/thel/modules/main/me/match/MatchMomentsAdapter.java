package com.thel.modules.main.me.match;

import android.content.Context;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseAdapter;
import com.thel.bean.moments.MomentsBean;
import com.thel.constants.MomentTypeConstants;
import com.thel.imp.black.BlackUtils;
import com.thel.ui.widget.MatchMultiImageViewGroup;
import com.thel.ui.widget.MultiImageViewGroup;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;
import com.thel.utils.L;
import com.thel.utils.MomentUtils;
import com.thel.utils.SharedPrefUtils;
import com.thel.utils.Utils;

import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;

/**
 * @author lingwei
 * @date 2018/9/7
 */

public class MatchMomentsAdapter extends BaseRecyclerViewAdapter<MomentsBean> {

    private static final String TAG = "MatchMomentsAdapter";

    final OnItemChildClickListener mChildListener = new OnItemChildClickListener();

    public MatchMomentsAdapter(List<MomentsBean> data) {
        super(R.layout.match_item_moments, data);
    }

    @Override
    protected BaseViewHolder onCreateDefViewHolder(ViewGroup parent, int viewType) {

        BaseViewHolder baseViewHolder = super.onCreateDefViewHolder(parent, viewType);
        return new MomentsViewHolder(baseViewHolder.convertView);
    }

    @Override
    protected void convert(final BaseViewHolder helper, final MomentsBean momentsBean) {

        L.d("MomentsAdapter", " momentsBean.momentsType : " + momentsBean.momentsType);

        final MomentsViewHolder holder = (MomentsViewHolder) helper;

        final Context context = helper.getConvertView().getContext();

        holder.center_layout.setVisibility(GONE);

        holder.layout_multi_image.setVisibility(GONE);

        holder.moment_release_time.setTypeface(null, Typeface.NORMAL);
        holder.moment_release_time.setBackgroundResource(R.color.white);
        holder.moment_release_time.setTextColor(TheLApp.context.getResources().getColor(R.color.theme_class_title_tab));
        holder.moment_release_time.setText(MomentUtils.getReleaseTimeShow(momentsBean.momentsTime));

        if (momentsBean.momentsText != null && momentsBean.momentsText.length() > 0) {

            holder.layout_moment_text.setVisibility(View.VISIBLE);

            MomentUtils.setMatchMomentContent(holder.moment_content_text, momentsBean.momentsText);

        } else {
            holder.layout_moment_text.setVisibility(View.GONE);
        }


        if (!TextUtils.isEmpty(momentsBean.momentsType))
            switch (momentsBean.momentsType) {

                case MomentTypeConstants.MOMENT_TYPE_TEXT:
                    holder.layout_moment_text.setVisibility(View.VISIBLE);

                    break;
                case MomentTypeConstants.MOMENT_TYPE_IMAGE:

                    holder.center_layout.setVisibility(View.VISIBLE);

                    holder.layout_multi_image.setVisibility(View.VISIBLE);

                    holder.layout_multi_image.initMatchNotShareUI(momentsBean.imageUrl, momentsBean.userName, Utils.getImageShareBean(momentsBean));

                    break;
                case MomentTypeConstants.MOMENT_TYPE_TEXT_IMAGE:

                    holder.layout_moment_text.setVisibility(View.VISIBLE);

                    holder.center_layout.setVisibility(View.VISIBLE);

                    holder.layout_multi_image.setVisibility(View.VISIBLE);

                    holder.layout_multi_image.initMatchNotShareUI(momentsBean.imageUrl, momentsBean.userName, Utils.getImageShareBean(momentsBean));

                    break;


            }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(holder.itemView, momentsBean, holder.getAdapterPosition());
                }

            }
        });


        holder.layout_moment_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.itemView.performClick();
            }
        });

    }

    public class MomentsViewHolder extends BaseViewHolder {

        @BindView(R.id.layout_moment_text)
        View layout_moment_text;

        @BindView(R.id.center_layout)
        RelativeLayout center_layout;

        @BindView(R.id.layout_multi_image)
        MatchMultiImageViewGroup layout_multi_image;

        /**
         * head
         */

        @BindView(R.id.moment_release_time)
        TextView moment_release_time;

        /**
         * text
         */
        @BindView(R.id.moment_content_text)
        TextView moment_content_text;

        public MomentsViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

        }
    }

    private BaseAdapter.OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(BaseAdapter.OnItemClickListener mOnItemClickListener) {
        this.mOnItemClickListener = mOnItemClickListener;
    }


    public static void filterMoments(List<MomentsBean> data) {
        if (data != null) {

            String str = SharedPrefUtils.getString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.HIDE_MOMENTS_USER_LIST, "");

            String[] blocks = str.split(",");

            List<String> blackList = Arrays.asList(blocks);

            ListIterator<MomentsBean> iterator = data.listIterator();

            while (iterator.hasNext()) {

                MomentsBean momentsBean = iterator.next();

                for (int i = 0; i < blackList.size(); i++) {

                    String blackUserId = blackList.get(i);

                    if (String.valueOf(momentsBean.userId).equals(blackUserId)) {

                        iterator.remove();

                    }
                }

            }
        }
    }

    public static void filterBlack(List<MomentsBean> data) {

        List<String> blockList = BlackUtils.getBlackList();

        ListIterator<MomentsBean> iterator = data.listIterator();

        while (iterator.hasNext()) {

            MomentsBean momentsBean = iterator.next();

            for (int i = 0; i < blockList.size(); i++) {

                String blackUserId = blockList.get(i);

                if (String.valueOf(momentsBean.userId).equals(blackUserId)) {
                    iterator.remove();
                }
            }
        }

    }


    private int getPositionByMomentId(String momentId) {

        List<MomentsBean> momentsBeans = getData();

        if (momentsBeans == null || momentsBeans.size() == 0) {
            return -1;
        }

        for (int i = 0; i < momentsBeans.size(); i++) {
            MomentsBean momentsBean = momentsBeans.get(i);

            if (momentsBean.momentsId.equals(momentId)) {
                return i;
            }
        }

        return -1;

    }

}
