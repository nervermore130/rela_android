package com.thel.modules.live.liveBigGiftAnimLayout.video;

import android.graphics.SurfaceTexture;
import android.os.Handler;
import android.os.Message;
import androidx.annotation.RawRes;

import com.thel.utils.L;

import java.lang.ref.WeakReference;

/**
 * Created by waiarl on 2018/1/12.
 */

public class LiveAnimVideoHandler extends Handler implements LiveAnimVideoHandlerConstant {

    private WeakReference<LiveAnimVideoThread> mWeakRenderThread;

    /**
     * Call from render thread.
     */
    public LiveAnimVideoHandler(LiveAnimVideoThread rt) {
        mWeakRenderThread = new WeakReference<LiveAnimVideoThread>(rt);
    }

    @Override
    public void handleMessage(Message msg) {
        super.handleMessage(msg);
        final LiveAnimVideoThread videoThread = mWeakRenderThread.get();
        if (videoThread == null) {
            L.i(TAG, "RenderHandler.handleMessage: weak ref is null");
            return;
        }

        switch (msg.what) {
            case MSG_SURFACE_AVAILABLE:
                videoThread.surfaceAvailable((SurfaceTexture) msg.obj, msg.arg1, msg.arg2);
                break;
            case MSG_SURFACE_CHANGED:
                videoThread.surfaceChanged((SurfaceTexture) msg.obj, msg.arg1, msg.arg2);
                break;
            case MSG_SURFACE_DESTROYED:
                videoThread.surfaceDestroyed((SurfaceTexture) msg.obj);
                break;
            case MSG_SHUTDOWN:
                videoThread.shutdown();
                break;
            case MSG_FRAME_AVAILABLE:
                videoThread.frameAvailable();
                break;

            case MSG_REDRAW:
                videoThread.draw();
                break;
            case MSG_RELEASE_PLAYER:
                videoThread.releaseMediaPlayer();
                break;
            case MSG_RESEUME_START_PLAY:
                videoThread.onResume();
                break;
            case MSG_PAUSE_STOP_PLAY:
                videoThread.onPause();
                break;
            case MSG_RELOAD_VIDEO:
                videoThread.reloadVideo();
                break;
            case MSG_RESTART_VIDEO:
                videoThread.restartVideo();
                break;
            case MSG_INIT_PLAYER:
                videoThread.initPlayer();
                break;
            case MSG_PLAY_VIDEO:
                videoThread.playVideo(msg.obj);
                break;
            default:
                throw new RuntimeException("unknown message " + msg.what);
        }
    }

    public void sendSurfaceAvailable(SurfaceTexture holder, int width, int height) {
        sendMessage(obtainMessage(MSG_SURFACE_AVAILABLE,
                width, height, holder));
    }

    public void sendSurfaceChanged(SurfaceTexture holder, int width,
            int height) {
        // ignore format
        sendMessage(obtainMessage(MSG_SURFACE_CHANGED, width, height, holder));
    }

    public void sendSurfaceDestroyed(SurfaceTexture holder) {
        sendMessage(obtainMessage(MSG_SURFACE_DESTROYED, 0, 0, holder));
    }

    public void sendMediaPlayerRelease() {
        sendEmptyMessage(MSG_RELEASE_PLAYER);
    }

    public void sendShutdown() {
        sendMessage(obtainMessage(MSG_SHUTDOWN));
    }

    public void sendResume() {
        sendEmptyMessage(MSG_RESEUME_START_PLAY);

    }

    public void sendPause() {
        sendEmptyMessage(MSG_PAUSE_STOP_PLAY);
    }

    public void sendFrameAvailable() {
        sendMessage(obtainMessage(MSG_FRAME_AVAILABLE));
    }

    public void sendSwitchFilter(@RawRes int fiterId) {
        sendMessage(obtainMessage(MSG_SWITCH_FILTER, fiterId));
    }

    public void sendReloadVideo() {
        sendEmptyMessage(MSG_RELOAD_VIDEO);
    }

    public void sendRestartVideo() {
        sendEmptyMessage(MSG_RESTART_VIDEO);
    }

    public void sendInitVideoPlayer() {
        sendEmptyMessage(MSG_INIT_PLAYER);
    }

    public void sendPlayVideo(String url) {
        sendMessage(obtainMessage(MSG_PLAY_VIDEO, url));
    }
}
