package com.thel.modules.others;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.modules.main.me.aboutMe.BuyVipActivity;
import com.thel.modules.others.beans.VipDetailBean;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;

/**
 * 会员里的弹窗 显示更纤细的功能信息
 *
 * @author lingwei
 * @date 2018/7/16
 */

public class VipDetailViewDialog extends Dialog {

    public static final int VIP_IDENTITY = 0; //会员身份
    public static final int VIP_NEARBY_FILTER = 1; //附近过滤

    public static final int VIP_NO_TRACE = 3; //无痕浏览
    public static final int VIP_HIDE = 2; //隐身
    public static final int VIP_SECRETLY_FOLLOW = 6; //悄悄关注
    public static final int VIP_MOMENT_TOP = 7; //置顶日志
    public static final int VIP_UNFOLLOW_REMIND = 11; //取消关注提醒
    public static final int VIP_WHO_LIKE_ME = 8;//谁喜欢我
    public static final int VIP_RECALL = 9;//无限撤回
    public static final int VIP_EXPOSURE = 10;//更多曝光

    public static final int VIP_SNEAK = 5;//悄悄查看
    public static final int VIP_TIME_MACINE = 4;//时光机

    private final Context mContext;

    private int type;
    private View close_dialog;
    private TextView vip_tile_dialog;
    private TextView vip_content_dialog;
    private ImageView vip_rsc_img;
    private RelativeLayout rl_purchase;
    private ViewPager view_pager;

    private ArrayList<VipDetailBean> Res_list = new ArrayList();
    private int[] picRes = new int[]{R.mipmap.vip_pic_identity, R.mipmap.vip_pic_nearby_filter,R.mipmap.vip_pic_invisible_dialog, R.mipmap.vip_pic_anonymous, R.mipmap.vip_pic_time_machine, R.mipmap.vip_pic_sneak, R.mipmap.vip_pic_secretly_follow, R.mipmap.vip_pic_top_moment, R.mipmap.dialog_who_like_me, R.mipmap.dialog_recall, R.mipmap.vip_pic_exposure, R.mipmap.vip_pic_unfollow_remind};
    private LinearLayout mLinearLayout;

    //上一次点位高亮显示的位置
    private int prePosition = 0;

    public VipDetailViewDialog(@NonNull Context context) {
        this(context, R.style.CustomDialogBottom);
    }

    public VipDetailViewDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        this.mContext = context;
        init();
        setLisener();
        initEvent();

    }


    public void setSeeingType(int type) {
        this.type = type;
        initView();
    }

    private SparseArray<VipDetailBean> VipDetailMap = new SparseArray<VipDetailBean>() {
        {
            put(0, new VipDetailBean(VIP_IDENTITY, TheLApp.getContext().getResources().getString(R.string.vip_config_act_show_info_title), picRes[0], TheLApp.getContext().getResources().getString(R.string.vip_config_act_show_info_desc)));
            put(1, new VipDetailBean(VIP_NEARBY_FILTER, TheLApp.getContext().getResources().getString(R.string.vip_title_nearby_filter), picRes[1], TheLApp.getContext().getResources().getString(R.string.vip_des_nearby_filter)));

            put(2, new VipDetailBean(VIP_NO_TRACE, TheLApp.getContext().getResources().getString(R.string.vip_config_act_hide_title), picRes[2], TheLApp.getContext().getResources().getString(R.string.stealth_state)));
            put(3, new VipDetailBean(VIP_HIDE, TheLApp.getContext().getResources().getString(R.string.vip_config_act_no_trace_title), picRes[3], TheLApp.getContext().getResources().getString(R.string.vip_config_act_no_trace_desc)));
            put(4, new VipDetailBean(VIP_TIME_MACINE, TheLApp.getContext().getResources().getString(R.string.vip_time_machine_title), picRes[4], TheLApp.getContext().getResources().getString(R.string.vip_time_machine_desc)));
            put(5, new VipDetailBean(VIP_SNEAK, TheLApp.getContext().getResources().getString(R.string.vip_sneaking_title), picRes[5], TheLApp.getContext().getResources().getString(R.string.vip_sneaking_desc)));
            put(6, new VipDetailBean(VIP_SECRETLY_FOLLOW, TheLApp.getContext().getResources().getString(R.string.vip_config_act_secretly_follow_title), picRes[6], TheLApp.getContext().getResources().getString(R.string.vip_config_act_secretly_follow_desc)));
            put(7, new VipDetailBean(VIP_MOMENT_TOP, TheLApp.getContext().getResources().getString(R.string.vip_config_act_stick_moment_title), picRes[7], TheLApp.getContext().getResources().getString(R.string.vip_config_act_stick_moment_desc)));
            put(8, new VipDetailBean(VIP_WHO_LIKE_ME, TheLApp.getContext().getResources().getString(R.string.who_like_me_title), picRes[8], TheLApp.getContext().getResources().getString(R.string.right_now_with_her_march)));
            put(9, new VipDetailBean(VIP_RECALL, TheLApp.getContext().getResources().getString(R.string.match_regret), picRes[9], TheLApp.getContext().getResources().getString(R.string.can_reserve)));
            put(10, new VipDetailBean(VIP_EXPOSURE, TheLApp.getContext().getResources().getString(R.string.more_be_see_title), picRes[10], TheLApp.getContext().getResources().getString(R.string.more_be_see_text)));

            put(11, new VipDetailBean(VIP_UNFOLLOW_REMIND, TheLApp.getContext().getResources().getString(R.string.vip_config_act_unfollow_title), picRes[11], TheLApp.getContext().getResources().getString(R.string.vip_config_act_unfollow_desc)));

        }
    };

    private void init() {
        final View view = LayoutInflater.from(mContext).inflate(R.layout.vip_detail_dialog_view, null);
        setContentView(view);
        close_dialog = view.findViewById(R.id.ll_close_dialog);
        rl_purchase = view.findViewById(R.id.rl_purchase);
        view_pager = view.findViewById(R.id.view_pager);
        mLinearLayout = view.findViewById(R.id.ll_points);
        int size = VipDetailMap.size();
        for (int i = 0; i < size; i++) {
            VipDetailBean bean = VipDetailMap.get(i);
            Res_list.add(bean);
            View v = new View(TheLApp.context);
            //添加底部灰点
            v.setBackgroundResource(R.drawable.gray_circle);
            //指定其大小
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(20, 20);
            if (i != 0)
                params.leftMargin = 20;
            v.setLayoutParams(params);
            mLinearLayout.addView(v);
        }

        view_pager.setAdapter(new VipDitailViewPager(mContext, type, Res_list));

    }

    private void initEvent() {

    }

    private void setLisener() {

        view_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                View ll_point_now = mLinearLayout.getChildAt(position);
                View ll_point_pre = mLinearLayout.getChildAt(prePosition);
                ll_point_pre.setBackgroundResource(R.drawable.gray_circle);

                ll_point_now.setBackgroundResource(R.drawable.green_circle);
                prePosition = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        rl_purchase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoBuyVip();
                dismiss();
            }
        });
        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

    }

    class VipDitailViewPager extends PagerAdapter {
        private final ArrayList<VipDetailBean> list;
        private LayoutInflater mInflater;
        private View convertView;

        public VipDitailViewPager(Context context, int type, ArrayList<VipDetailBean> arrayList) {
            mInflater = (LayoutInflater) TheLApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.list = arrayList;

        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            convertView = mInflater.inflate(R.layout.my_vip_detail_func, container, false);
            vip_tile_dialog = convertView.findViewById(R.id.vip_tile_dialog);
            vip_content_dialog = convertView.findViewById(R.id.vip_content_dialog);
            vip_rsc_img = convertView.findViewById(R.id.vip_rsc_img_dialog);

            VipDetailBean vipDetailBean = list.get(position);
            vip_tile_dialog.setText(vipDetailBean.title);
            vip_content_dialog.setText(vipDetailBean.desc);
            vip_rsc_img.setImageResource(vipDetailBean.res_Index);

            container.addView(convertView);


            return convertView;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((View) object);
        }
    }

    private void initView() {
        if (view_pager != null) {
            view_pager.setCurrentItem(type);
            if (type == 0) {
                View ll_point_now = mLinearLayout.getChildAt(0);
                ll_point_now.setBackgroundResource(R.drawable.green_circle);

            }
        }

    }

    /**
     * 充值会员
     */
    private void gotoBuyVip() {
        MobclickAgent.onEvent(TheLApp.getContext(), "renew_member_click");
        Intent intent = new Intent(mContext, BuyVipActivity.class);
        intent.putExtra("isVip", false);
        mContext.startActivity(intent);
    }


}
