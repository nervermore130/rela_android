package com.thel.modules.main.messages.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.thel.app.TheLApp;
import com.thel.bean.me.MyCircleRequestBean;
import com.thel.modules.main.me.bean.FriendsBean;
import com.thel.utils.PinyinUtils;

import java.util.ArrayList;
import java.util.List;


public class DataBaseAdapter {

    private static final String TAG = DataBaseAdapter.class.getSimpleName();

    /**
     * 数据库对象
     */
    private SQLiteDatabase db;

    /**
     * 数据库适配器对象
     */
    private static DataBaseAdapter dbAdapter = null;
    private DataBaseHelper dbHelper = null;

    /**
     * 数据库名
     */
    private static final String DB_NAME = "the_l.db";

    /**
     * 数据库版本号
     */
    private static final int DB_VERSION = 4;

	/*-----------------------------------------表名--------------------------------------*/

    /**
     * 系统表sqlite_sequence
     */
    public static final String TB_NAME_SQLITE_SEQUENCE = "sqlite_sequence";

    /**
     * 好友表
     */
    public static final String TB_NAME_FRIENDS = "table_friends_v3";

    /**
     * 喜欢过的人
     */
    public static final String TB_NAME_LIKED = "table_liked_v1";

    /**
     * 联系人表
     * 1.13.0 v2版本增加bgImage
     */
    public static final String TB_NAME_CONTACTS = "table_contacts_v2";

    /**
     * 附近表(瀑布流)
     */
    public static final String TB_NAME_NEAR_BY_WATERFALL = "table_nearby_new_waterfall_v2";
    /**
     * 附近表(列表)
     */
    public static final String TB_NAME_NEAR_BY_LIST = "table_nearby_new_list_v2";

    public static final String TB_NAME_BLACK_LIST = "blackList";

    public static final String TB_NAME_BLACK_USER_MOMENTS_LIST = "blackUserMomentsList";

    public static final String TB_USER_DETAIL = "userDetail";

    /**
     * 好友关系表
     */
    public static final String TB_RELATION_FRIEND = "table_relation_friend_v1";//朋友

    public static final String TB_RELATION_CONCERN = "table_relation_concern_v1";//关注


    /**
     * 最近标签和热门标签缓存表
     */
    public static final String TB_NAME_RECENT_AND_HOT_TAGS = "recentAndHotTags";

    /**
     * 分类标签缓存表
     */
    public static final String TB_NAME_TAG_CATEGORIES = "tagCategories";

    /**
     * 精选标签缓存表
     */
    public static final String TB_NAME_FEATURED_TAGS = "featuredTags";

    /**
     * 热门标签缓存表
     */
    public static final String TB_NAME_HOT_TAGS = "hotTags";

    /**
     * 密友请求列表
     */
    public static final String TB_NAME_CIRCLE_REQUESTS = "circleRequests";

    /**
     * 搜索历史表
     */
    public static final String TB_NAME_SEARCH_HISTORY = "searchHistory";
    public static final String TB_NAME_SEARCH_HISTORY_LIST = "searchList";

	/*----------------------------------------字段-------------------------------------*/
    /**
     * 系统主键
     */
    public static final String F_ID = "_id";

    /**
     * 图片高度
     */
    public static final String F_IMAGEHEIGHT = "imageHeight";

    /**
     * 图片宽度
     */
    public static final String F_IMAGEWIDTH = "imageWidth";

    /**
     * userId
     */
    public static final String F_USERID = "userId";

    /**
     * 图片地址
     */
    public static final String F_PICURL = "picUrl";

    /**
     * 方头像地址
     */
    public static final String F_AVATAR = "avatar";

    /**
     * 背景图地址
     */
    public static final String F_BG_IMAGE = "bgImage";

    /**
     * 距离
     */
    public static final String F_DISTANCE = "distance";

    /**
     * 是否在线
     */
    public static final String F_ONLINE = "online";

    /**
     * 年龄
     */
    public static final String F_AGE = "age";

    /**
     * 取向
     */
    public static final String F_LOVE = "love";

    /**
     * 签名
     */
    public static final String F_SIGN = "sign";

    /**
     * 用户名
     */
    public static final String F_USERNAME = "username";

    /**
     * 在线时段
     */
    public static final String F_ONLINE_TIME = "online_time";

    /**
     * 朋友类型
     */
    public static final String F_FRIEND_TYPE = "friend_type";

    /**
     * 朋友类型
     */
    public static final String F_USERNAME_PINYIN = "username_pinyin";

    /**
     * 来访时间
     */
    public static final String F_VISIT_TIME = "visit_time";

    /**
     * 角色
     */
    public static final String F_ROLE_NAME = "roleName";

    public static final String F_UNIT = "unit";
    public static final String F_LOGIN_TIME = "loginTime";
    public static final String F_AFFECTION = "affection";

    public static final String F_VERIFY_TYPE = "verifyType";
    public static final String F_VERIFY_INTRO = "verifyIntro";

    public static final String F_LEVEL = "level";
    public static final String F_HIDING = "hiding";

    /**
     * 时间戳
     */
    public static final String F_TIMESTAMP = "timestamp";

    /**
     * 字段类型
     */
    public static final String F_TYPE = "type";

    //
    public static final String F_MY_USER_ID = "myUserId";// 用户id
    public static final String F_USER_INTRO = "userIntro";// 被屏蔽的用户描述
    public static final String F_INTRO = "intro";// 被屏蔽的用户描述
    public static final String F_USER_ID = "userId"; // 被屏蔽的用户id
    public static final String F_USER_NAME = "userName"; // 被屏蔽的用户昵称
    public static final String F_USER_AVATAR = "userAvatar"; // 被屏蔽的用户头像url
    public static final String F_USER_AVATAR_LOCAL = "userAvatarLocal"; // 被屏蔽的用户头像本地存储地址

    public static final String F_TOPIC_ID = "topicId";
    public static final String F_TOPIC_NAME = "topicName";
    public static final String F_MOMENT_ID = "momentId";
    //

    public static final String F_MY_USER_HISTORY_ID = "myUserId";// 搜索历史用户自己的id
    public static final String F_USER_HISTORY_ID = "userId"; // 被搜索的用户id
    public static final String F_USER_HISTORY_NAME = "userName"; // 搜索历史的用户昵称
    public static final String F_USER_HISTORY_AVATAR = "userAvatar"; // 被搜索的用户头像url
    public static final String F_USER_HISTORY_AVATAR_LOCAL = "userAvatarLocal"; // 被搜索的用户头像本地存储地址

    /**
     * 日志图片(与topicColor二选一)
     */
    public static final String F_IMAGE_URL = "imageUrl";
    /**
     * 背景颜色
     */
    public static final String F_TOPIC_COLOR = "topicColor";
    /**
     * 话题下的参与人数
     */
    public static final String F_JOIN_COUNT = "joinCount";
    /**
     * 话题下的moments数量
     */
    public static final String F_MOMENTS_NUM = "momentsNum";
    /**
     * 最近还是热门话题的标识
     */
    public static final String F_RECENT_OR_HOT_TAG = "recentOrHotTag";
    /**
     * 标签分类id
     */
    public static final String F_CATE_ID = "cateId";
    /**
     * 标签分类名称
     */
    public static final String F_CATE_NAME = "cateName";
    /**
     * 标签logo
     */
    public static final String F_LOGO_IMG_URL = "logoImageUrl";
    /**
     * 是否音乐日志
     */
    public static final String F_MUSIC_FLAG = "musicFlag";
    /**
     * 密友圈请求id
     */
    public static final String F_REQUEST_ID = "requestId";
    /**
     * 密友圈请求状态
     */
    public static final String F_REQUEST_STATUS = "requestStatus";
    /**
     * 密友圈请求类型
     */
    public static final String F_REQUEST_TYPE = "requestType";
    /**
     * 昵称
     */
    public static final String F_NICK_NAME = "nickName";
    /**
     * 接收方id
     */
    public static final String F_RECEIVED_ID = "receivedId";
    /**
     * 创建时间
     */
    public static final String F_CREATE_TIME = "createTime";
    /**
     * 处理时间
     */
    public static final String F_HANDLE_TIME = "handleTime";
    /**
     * 结婚纪念日
     */
    public static final String F_ANNIVERSARY_DATE = "anniversaryDate";
    /**
     * 密友圈请求的消息内容
     */
    public static final String F_REQUEST_COMENT = "requestComent";

    /**
     * 朋友圈 用户ids
     */
    public static final String F_RELATION_FRIEND_LIST = "relation_friend_list";

    /*----------------------------------打开|关闭数据库----------------------------------*/

    public synchronized static DataBaseAdapter getInstance(Context context) {
        if (null == dbAdapter) {
            dbAdapter = new DataBaseAdapter();
        }
        return dbAdapter.openDataBase();
    }

    private DataBaseAdapter openDataBase() {
        if (dbHelper == null) {
            dbHelper = new DataBaseHelper(TheLApp.getContext());
        }
        try {
            db = dbHelper.getWritableDatabase();
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }

        return dbAdapter;
    }

    private void closeDataBase() {
    }

  /*  *//**
     * 4.0.0 用户关系列表获取
     *//*
    public String getRelationFriendList(String userId) {
        if (TextUtils.isEmpty(userId) || "-1".equals(userId)) {
            return null;
        }
        if (!isTableExist(TB_RELATION_FRIEND)) {
            createRelationFriendTable(db);
            return null;
        }
        Cursor cursor = null;
        String list = null;
        try {
            cursor = db.rawQuery("select * from " + TB_RELATION_FRIEND + " where " + F_MY_USER_ID + " = ?", new String[]{
                    userId + ""});
            while (cursor.moveToNext()) {
                list = cursor.getString(cursor.getColumnIndex(F_RELATION_FRIEND_LIST));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return list;
    }

    *//**
     * 4.0.0 是否是用户关系 朋友
     *
     * @param userId
     * @param friendId
     * @return
     *//*
    public boolean isRelationFriend(String userId, String friendId) {
        if (TextUtils.isEmpty(friendId) || "-1".equals(friendId)) {
            return false;
        }
        if (MessageConstants.RELA_ACCOUNT_IDS.contains(friendId)) {//如果是热拉官方账号,返回true
            return true;
        }
        if (!isTableExist(TB_RELATION_FRIEND)) {
            return false;
        }
        final String relationFriendList = getRelationFriendList(userId);
        L.d("refresh", "relationFriendList:list=" + relationFriendList + ",friendId=" + friendId);
        if (TextUtils.isEmpty(relationFriendList)) {
            return false;
        }
        if (relationFriendList.contains("[" + friendId + "]")) {
            return true;
        }
        return false;
    }

    public void deleteOneRelationFriend(String userId, String friendId) {
        if (isRelationFriend(userId, friendId)) {
            final String lists = getRelationFriendList(userId);
            final String friend = "[" + friendId + "]";
            lists.replace(friend, "");
            saveRelationFriendlist(userId, friendId);
        }
    }*/

    /**
     * 建表
     */
    public void createFriendsListTable(SQLiteDatabase db) {
        String sql = "create table if not exists " + TB_NAME_FRIENDS + "(" + F_ID + " integer primary key AUTOINCREMENT," + F_USERID + " text," + F_ONLINE + " text," + F_USERNAME + " text," + F_USERNAME_PINYIN + " text," + F_DISTANCE + " text," + F_AVATAR + " text," + F_ROLE_NAME + " text," + F_USER_INTRO + " text," + F_UNIT + " text," + F_LOGIN_TIME + " text," + F_VERIFY_TYPE + " integer," + F_VERIFY_INTRO + " text," + F_AGE + " integer," + F_AFFECTION + " integer," + F_LEVEL + " integer," + F_HIDING + " integer," + F_FRIEND_TYPE + " integer" + ")";

        db.execSQL(sql);
    }

    /**
     * 建表
     */
    public void createLikedListTable(SQLiteDatabase db) {
        String sql = "create table if not exists " + TB_NAME_LIKED + "(" + F_ID + " integer primary key AUTOINCREMENT," + F_USERID + " integer," + F_ONLINE + " text," + F_NICK_NAME + " text," + F_DISTANCE + " text," + F_AVATAR + " text," + F_ROLE_NAME + " text," + F_VERIFY_TYPE + " integer," + F_VERIFY_INTRO + " text," + F_AGE + " integer," + F_AFFECTION + " integer," + F_LEVEL + " integer," + F_HIDING + " integer," + F_INTRO + " text)";

        db.execSQL(sql);
    }

    /**
     * 建表
     */
    public void createBlackListTable(SQLiteDatabase db) {
        String sql = "create table if not exists " + TB_NAME_BLACK_LIST + "(" + F_ID + " integer primary key AUTOINCREMENT," + F_MY_USER_ID + " integer," + F_USER_ID + " integer," + F_USER_NAME + " text," + F_USER_INTRO + " text," + F_USER_AVATAR_LOCAL + " text," + F_USER_AVATAR + " text" + ")";
        db.execSQL(sql);
    }

    /**
     * 建表
     */
    public void createContactsListTable(SQLiteDatabase db) {
        String sql = "create table if not exists " + TB_NAME_CONTACTS + "(" + F_ID + " integer primary key AUTOINCREMENT," + F_USERID + " text UNIQUE," + F_AVATAR + " text," + F_BG_IMAGE + " text," + F_USER_NAME + " text," + F_USERNAME_PINYIN + " text" + ")";

        db.execSQL(sql);
    }

    /**
     * 建表
     */
    public void createRecentAndHotTags(SQLiteDatabase db) {
        String sql = "create table if not exists " + TB_NAME_RECENT_AND_HOT_TAGS + "(" + F_ID + " integer primary key AUTOINCREMENT," + F_TOPIC_ID + " text," + F_TOPIC_NAME + " text," + F_TOPIC_COLOR + " text," + F_MOMENTS_NUM + " integer," + F_JOIN_COUNT + " text," + F_RECENT_OR_HOT_TAG + " text" + ")";
        db.execSQL(sql);
    }

    /**
     * 建表
     */
    public void createFeaturedTags(SQLiteDatabase db) {
        String sql = "create table if not exists " + TB_NAME_FEATURED_TAGS + "(" + F_ID + " integer primary key AUTOINCREMENT," + F_MOMENT_ID + " text," + F_TOPIC_NAME + " text," + F_TOPIC_COLOR + " text," + F_IMAGE_URL + " text," + F_JOIN_COUNT + " text," + F_MUSIC_FLAG + " integer" + ")";
        db.execSQL(sql);
    }

    /**
     * 建表
     */
    public void createHotTags(SQLiteDatabase db) {
        String sql = "create table if not exists " + TB_NAME_HOT_TAGS + "(" + F_ID + " integer primary key AUTOINCREMENT," + F_TOPIC_ID + " text," + F_TOPIC_NAME + " text," + F_TOPIC_COLOR + " text," + F_MOMENTS_NUM + " integer," + F_JOIN_COUNT + " text" + ")";
        db.execSQL(sql);
    }

    /**
     * 建密友圈请求表
     */
    public void createCircleRequestsDB(SQLiteDatabase db) {
        String sql = "create table if not exists " + TB_NAME_CIRCLE_REQUESTS + "(" + F_ID + " integer primary key AUTOINCREMENT," + F_REQUEST_ID + " integer UNIQUE," + F_REQUEST_STATUS + " integer," + F_REQUEST_TYPE + " integer," + F_USER_ID + " integer," + F_NICK_NAME + " text," + F_AVATAR + " text," + F_RECEIVED_ID + " integer," + F_CREATE_TIME + " integer," + F_HANDLE_TIME + " integer," + F_ANNIVERSARY_DATE + " text," + F_REQUEST_COMENT + " text" + ")";
        db.execSQL(sql);
    }

    /**
     * 建表
     */
    public void createTagCategories(SQLiteDatabase db) {
        String sql = "create table if not exists " + TB_NAME_TAG_CATEGORIES + "(" + F_ID + " integer primary key AUTOINCREMENT," + F_TOPIC_ID + " text," + F_TOPIC_NAME + " text," + F_TOPIC_COLOR + " text," + F_MOMENTS_NUM + " integer," + F_JOIN_COUNT + " text," + F_LOGO_IMG_URL + " text," + F_CATE_ID + " text," + F_CATE_NAME + " text" + ")";
        db.execSQL(sql);
    }

    /**
     * 建表
     */
    public void createNearByWaterfallTable(SQLiteDatabase db) {
        // 创建"附近用户表"
        String sql = "create table if not exists " + TB_NAME_NEAR_BY_WATERFALL + "(" + F_ID + " integer primary key AUTOINCREMENT," + F_USERID + " integer," + F_IMAGEHEIGHT + " integer," + F_IMAGEWIDTH + " integer," + F_DISTANCE + " text," + F_ONLINE + " integer," + F_PICURL + " text," + F_AVATAR + " text," + F_AGE + " text," + F_LOVE + " text," + F_SIGN + " text," + F_USERNAME + " text," + F_UNIT + " text," + F_LOGIN_TIME + " text," + F_AFFECTION + " text," + F_ONLINE_TIME + " text," + F_VERIFY_TYPE + " integer," + F_VERIFY_INTRO + " text," + F_LEVEL + " integer," + F_TYPE + " text," + F_TIMESTAMP + " text" + ")";

        db.execSQL(sql);

    }

    /**
     * 建表
     */
    public void createNearByListTable(SQLiteDatabase db) {

        // 创建"附近用户表"
        String sql = "create table if not exists " + TB_NAME_NEAR_BY_LIST + "(" + F_ID + " integer primary key AUTOINCREMENT," + F_USERID + " integer," + F_IMAGEHEIGHT + " integer," + F_IMAGEWIDTH + " integer," + F_DISTANCE + " text," + F_ONLINE + " integer," + F_PICURL + " text," + F_AVATAR + " text," + F_AGE + " text," + F_LOVE + " text," + F_SIGN + " text," + F_USERNAME + " text," + F_UNIT + " text," + F_LOGIN_TIME + " text," + F_AFFECTION + " text," + F_ONLINE_TIME + " text," + F_VERIFY_TYPE + " integer," + F_VERIFY_INTRO + " text," + F_LEVEL + " integer," + F_TYPE + " text," + F_TIMESTAMP + " text" + ")";

        db.execSQL(sql);

    }

    /**
     * 建表
     */
    public void createHistoryListTable(SQLiteDatabase db) {
        String sql = "create table if not exists " + TB_NAME_SEARCH_HISTORY_LIST + "(" + F_ID + " integer primary key AUTOINCREMENT," + F_MY_USER_HISTORY_ID + " integer," + F_USER_HISTORY_ID + " integer," + F_USER_HISTORY_NAME + " text," + F_USER_HISTORY_AVATAR_LOCAL + " text," + F_USER_HISTORY_AVATAR + " text" + ")";
        db.execSQL(sql);
    }

    public void createBlackUserMomentsListTable(SQLiteDatabase db) {
        String sql = "create table if not exists " + TB_NAME_BLACK_USER_MOMENTS_LIST + "(" + F_ID + " integer primary key AUTOINCREMENT," + F_MY_USER_ID + " integer," + F_USER_ID + " integer," + F_USER_NAME + " text," + F_USER_INTRO + " text," + F_USER_AVATAR_LOCAL + " text," + F_USER_AVATAR + " text" + ")";
        db.execSQL(sql);
    }

    /**
     * 创建用户关系 朋友表
     */
    public void createRelationFriendTable(SQLiteDatabase db) {
        String sql = "create table if not exists "
                + TB_RELATION_FRIEND + "("
                + F_ID + " integer primary key AUTOINCREMENT,"
                + F_MY_USER_ID + " text,"
                + F_RELATION_FRIEND_LIST + " text" + ")";
        db.execSQL(sql);
    }

    public boolean isTableExist(String tabName) {
        boolean result = false;
        Cursor cursor = null;
        try {
            String sql = "select count(*) as c from sqlite_master where type ='table' and name ='" + tabName + "' ";
            cursor = db.rawQuery(sql, null);
            if (cursor.moveToNext()) {
                int count = cursor.getInt(0);
                if (count > 0) {
                    result = true;
                }
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 获取全部朋友(不区分是关注还是粉丝)
     *
     * @return
     */
    public ArrayList<FriendsBean> getAllFriends() {
        if (!isTableExist(TB_NAME_FRIENDS)) {
            createFriendsListTable(db);
        }
        ArrayList<FriendsBean> arrayList = new ArrayList<FriendsBean>();
        Cursor cursor = null;
        try {
            cursor = db.query(TB_NAME_FRIENDS, null, null, null, null, null, null);

            if (cursor != null && cursor.moveToFirst()) {
                do {
                    FriendsBean tempbean = new FriendsBean();

                    tempbean.fromCursor(cursor);

                    arrayList.add(tempbean);
                } while (cursor.moveToNext());
            }


            //            while (cursor.moveToNext()) {
            //                FriendsBean tempbean = new FriendsBean();
            //
            //                tempbean.data.fromCursor(cursor);
            //
            //                arrayList.add(tempbean);
            //            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != cursor) {
                cursor.close();
            }
            closeDataBase();
        }

        return arrayList;
    }

    /*-----------------------------------建表更新表-------------------------------------*/
    private class DataBaseHelper extends SQLiteOpenHelper {

        public DataBaseHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            String sql = "";
            // 创建"附近用户表"
            createNearByWaterfallTable(db);

            // 创建"附近用户表"
            createNearByListTable(db);

            createFriendsListTable(db);

            createLikedListTable(db);

            // 联系人表
            createContactsListTable(db);

            createBlackListTable(db);

            createBlackUserMomentsListTable(db);

            createRecentAndHotTags(db);

            createTagCategories(db);

            createFeaturedTags(db);

            createHotTags(db);

            createCircleRequestsDB(db);
            /**4.0.0 创建朋友关系表**/
            createRelationFriendTable(db);

            // 创建索引
            db.execSQL("CREATE INDEX IF NOT EXISTS [type] On [" + TB_NAME_NEAR_BY_WATERFALL + "] ( " + "[type] Collate BINARY ) ");
            db.execSQL("CREATE INDEX IF NOT EXISTS [type] On [" + TB_NAME_NEAR_BY_LIST + "] ( " + "[type] Collate BINARY ) ");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("drop table if exists " + TB_NAME_NEAR_BY_WATERFALL);
            db.execSQL("drop table if exists " + TB_NAME_FRIENDS);
            db.execSQL("drop table if exists " + TB_NAME_BLACK_LIST);
            db.execSQL("drop table if exists " + TB_NAME_BLACK_USER_MOMENTS_LIST);
            onCreate(db);
        }

    }
        /*-----------------------------------朋友表的操作---------------------------------*/

    /**
     * 保存朋友列表
     *
     * @param listData
     */
    public void saveFriendList(ArrayList<FriendsBean> listData) {
        if (!isTableExist(TB_NAME_FRIENDS)) {
            createFriendsListTable(db);
        }
        int length = listData.size();
        try {
            for (int i = 0; i < length; i++) {
                FriendsBean tempFriendBean = listData.get(i);
                tempFriendBean.userNamePinYin = PinyinUtils.cn2Spell(tempFriendBean.nickName);

                db.insert(TB_NAME_FRIENDS, null, tempFriendBean.getContentValues());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDataBase();
        }
    }

   /* *//**
     * 从接口获取的用户关系（朋友）存储
     *
     * @param userId
     * @param list   类型[A,B,C]
     *//*
    public void saveDataRelationFriendList(String userId, String list) {
        if (TextUtils.isEmpty(list)) {
            return;
        }
        if (!isTableExist(TB_RELATION_FRIEND)) {
            createRelationFriendTable(db);
        }

        saveRelationFriendlist(userId, Utils.getUsrsIdListStr(list));
    }

    *//**
     * 4.0.0 用户关系朋友列表存储
     * list:[A][B][C]
     *//*
    public void saveRelationFriendlist(String userId, String list) {
        if (TextUtils.isEmpty(list)) {
            return;
        }
        if (!isTableExist(TB_RELATION_FRIEND)) {
            createRelationFriendTable(db);
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put(F_MY_USER_ID, userId);
        contentValues.put(F_RELATION_FRIEND_LIST, list);
        if (getRelationFriendList(userId) == null) {
            db.insert(TB_RELATION_FRIEND, null, contentValues);
        } else {
            db.update(TB_RELATION_FRIEND, contentValues, F_MY_USER_ID + " =?", new String[]{userId});
        }
    }

    *//**
     * 4.0.0 保存一个用户关系，朋友
     *
     * @param userId
     * @param friendId
     *//*
    public void saveOneRelationFriend(String userId, String friendId) {
        if (TextUtils.isEmpty(friendId) || "-1".equals(friendId)) {
            return;
        }
        if (!isTableExist(TB_RELATION_FRIEND)) {
            createRelationFriendTable(db);
            saveRelationFriendlist(userId, "[" + friendId + "]");
            return;
        }
        final String relationFriendlist = getRelationFriendList(userId);
        if (TextUtils.isEmpty(relationFriendlist)) {
            saveRelationFriendlist(userId, "[" + friendId + "]");
            return;
        }
        if (isRelationFriend(userId, friendId)) {
            return;
        }
        saveRelationFriendlist(userId, relationFriendlist + "[" + friendId + "]");
    }
*/

    /**
     * 清空朋友表
     *
     * @return
     */
    public int clearUserData() {
        int delCount = 0;

        // 清空表内容,使主键_id从1开始
        // 系统表sqlite_sequence, seq是表中字段
        // update sqlite_sequence set seq=0 where name='表名';
        try {
            delCount = db.delete(TB_NAME_BLACK_LIST, null, null);
            delCount = db.delete(TB_NAME_BLACK_USER_MOMENTS_LIST, null, null);
            delCount = db.delete(TB_NAME_CONTACTS, null, null);
            delCount = db.delete(TB_NAME_FRIENDS, null, null);
            delCount = db.delete(TB_NAME_NEAR_BY_LIST, null, null);
            delCount = db.delete(TB_NAME_NEAR_BY_WATERFALL, null, null);
            delCount = db.delete(TB_NAME_CIRCLE_REQUESTS, null, null);
            ContentValues contentValues = new ContentValues();
            contentValues.put("seq", 0);
            String[] whereArgs = new String[]{TB_NAME_BLACK_LIST,
                    TB_NAME_BLACK_USER_MOMENTS_LIST,
                    TB_NAME_CONTACTS,
                    TB_NAME_FRIENDS,
                    TB_NAME_NEAR_BY_LIST,
                    TB_NAME_NEAR_BY_WATERFALL,
                    TB_NAME_CIRCLE_REQUESTS};
            db.update(TB_NAME_SQLITE_SEQUENCE, contentValues, "name" + " in (?,?,?,?,?,?,?,?)", whereArgs);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDataBase();
        }

        return delCount;
    }

    /**
     * 保存密友圈请求第一页数据
     *
     * @param listData
     */
    public void saveCircleRequests(List<MyCircleRequestBean> listData) {
        if (!isTableExist(TB_NAME_CIRCLE_REQUESTS)) {
            createCircleRequestsDB(db);
        }
        int length = listData.size();
        try {
            db.delete(TB_NAME_CIRCLE_REQUESTS, null, null);
            for (int i = 0; i < length; i++) {
                MyCircleRequestBean myCircleRequestBean = listData.get(i);
                db.insert(TB_NAME_CIRCLE_REQUESTS, null, myCircleRequestBean.getContentValues());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDataBase();
        }
    }

    /**
     * 更新请求的状态
     *
     * @param status
     * @param requestId
     */
    public void updateCircleRequestStatus(int status, long requestId) {
        try {
            db.execSQL("UPDATE " + TB_NAME_CIRCLE_REQUESTS + " SET " + F_REQUEST_STATUS + " = " + status + " WHERE " + F_REQUEST_ID + " = " + requestId);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDataBase();
        }
    }
}
