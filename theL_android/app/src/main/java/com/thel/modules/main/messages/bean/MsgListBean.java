package com.thel.modules.main.messages.bean;

import java.util.ArrayList;

/**
 * 消息列表对象
 * Created by setsail on 16/2/26.
 */
public class MsgListBean {

    public int unreadTotal = 0;

    public ArrayList<MsgWithUnreadBean> msgWithUnreadBeans = new ArrayList<>();

    @Override
    public String toString() {
        return "MsgListBean{" +
                "unreadTotal=" + unreadTotal +
                ", msgWithUnreadBeans=" + msgWithUnreadBeans +
                '}';
    }
}
