package com.thel.modules.main.messages.bean;

/**
 * Created by chad
 * Time 17/11/7
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */

public class UploadTokenConsumerBean {

    public int type;
    public String packetId;
    public String videoPath;
    public String videoThumnail;
    public String uploadVideoPath;
}
