package com.thel.modules.main.me.aboutMe;

import android.content.Intent;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.modules.main.me.adapter.FriendsListAdapter;
import com.thel.modules.main.me.bean.FriendsBean;
import com.thel.modules.main.me.bean.FriendsListBean;
import com.thel.modules.main.me.bean.FriendsListBeanNew;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.modules.others.VipConfigActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class SecretFollowActivity extends BaseActivity {

    @BindView(R.id.txt_title)
    TextView txt_title;

    @BindView(R.id.lin_more)
    LinearLayout lin_more;

    @BindView(R.id.emtpy_view)
    LinearLayout emtpy_view;

    @BindView(R.id.empty_view_txt)
    TextView empty_view_txt;

    @BindView(R.id.empty_view_txt_follow)
    TextView empty_view_txt_follow;

    @BindView(R.id.rl_purchase)
    RelativeLayout rl_purchase;

    @BindView(R.id.swipe_container)
    SwipeRefreshLayout swipe_container;

    @BindView(R.id.listView)
    RecyclerView mRecyclerView;

    @BindView(R.id.btn_purchase)
    TextView btn_purchase;

    public static final String TYPE_SECRET = "secret";
    private int total;
    private int curPage = 1; // 将要请求的页号
    private int page_size = 20;
    // 刷新类型，全部刷新（即下拉刷新）为1，还是加载下一页为2
    public int refreshType = 0;
    public final int REFRESH_TYPE_ALL = 1;
    public final int REFRESH_TYPE_NEXT_PAGE = 2;
    private int currentCountForOnce = -1;
    private FriendsListAdapter listAdapter;
    private ArrayList<FriendsBean> listPlus = new ArrayList<>();
    private FriendsBean friendsBean;
    private RelativeLayout vip_renew_container;

    @OnClick(R.id.lin_back)
    void back() {
        finish();
    }

    @OnClick(R.id.rl_purchase)
    void purchase() {
        MobclickAgent.onEvent(this, "renew_member_click");
        Intent intent = new Intent(this, BuyVipActivity.class);
        intent.putExtra("isVip", false);
        startActivity(intent);
    }

    @OnClick(R.id.empty_view_txt_follow)
    void secretFollowTip() {
        Intent intent = new Intent(this, VipConfigActivity.class);
        intent.putExtra("showType", VipConfigActivity.SHOW_SECRETLY_FOLLOW);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secret_follow);
        ButterKnife.bind(this);

        lin_more.setVisibility(View.GONE);
        txt_title.setText(R.string.secretly_follow);

        initRecycler();
        processBusiness(curPage, page_size, REFRESH_TYPE_ALL);
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void initRecycler() {
        ViewUtils.initSwipeRefreshLayout(swipe_container);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(RecyclerView.VERTICAL);
        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setHasFixedSize(true);
        listAdapter = new FriendsListAdapter(listPlus, TYPE_SECRET,this);
        View header = View.inflate(this, R.layout.secret_follow_head, null);
        LinearLayout lin_search_entrance = header.findViewById(R.id.lin_search_entrance);
        lin_search_entrance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                Intent intent = new Intent(SecretFollowActivity.this, SearchMyFriendsActivity.class);
                startActivity(intent);
            }
        });
        vip_renew_container = header.findViewById(R.id.vip_renew_container);
        listAdapter.addHeaderView(header);
        mRecyclerView.setAdapter(listAdapter);
        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshData();
            }
        });
        listAdapter.setOnLoadMoreListener(new BaseRecyclerViewAdapter.RequestLoadMoreListener() {//加载更多
            @Override
            public void onLoadMoreRequested() {
                mRecyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        if (currentCountForOnce > 0) {
                            processBusiness(curPage, page_size, REFRESH_TYPE_NEXT_PAGE);
                        } else {
                            listAdapter.openLoadMore(0, false);
                            if (listPlus.size() > 0) {
                                View view = LayoutInflater.from(SecretFollowActivity.this).inflate(R.layout.load_more_footer_layout, (ViewGroup) mRecyclerView.getParent(), false);
                                ((TextView) view.findViewById(R.id.text)).setText(getString(R.string.info_no_more));
                                listAdapter.addFooterView(view);
                            }
                        }
                    }
                });
            }
        });
        listAdapter.setReloadMoreListener(new BaseRecyclerViewAdapter.ReloadMoreListener() {
            @Override
            public void reloadMore() {
                listAdapter.removeAllFooterView();
                listAdapter.openLoadMore(true);
                processBusiness(curPage, page_size, REFRESH_TYPE_NEXT_PAGE);
            }
        });
        listAdapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                ViewUtils.preventViewMultipleClick(view, 2000);
                gotoUserInfoActivity(position);
            }
        });
    }

    private void processBusiness(int page, int pageSize, int type) {
        curPage = page;
        page_size = pageSize;
        refreshType = type;

        loadNetData(page, pageSize);

    }

    private void loadNetData(int page, int pageSize) {
        Flowable<FriendsListBeanNew> flowable = RequestBusiness.getInstance().getSecretFollowList(ShareFileUtils.getString(ShareFileUtils.ID, ""), pageSize + "", page + "");
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<FriendsListBeanNew>() {
            @Override
            public void onNext(FriendsListBeanNew data) {
                super.onNext(data);
                setFollowAndFans(data.data);
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                requestFiled();
            }

            @Override
            public void onComplete() {
                super.onComplete();
                requestFinished();
            }
        });

    }

    private void requestFiled() {
        if (refreshType == REFRESH_TYPE_NEXT_PAGE) {

            mRecyclerView.post(new Runnable() {
                @Override
                public void run() {
                    listAdapter.loadMoreFailed((ViewGroup) mRecyclerView.getParent());
                }
            });
        }
    }

    private void requestFinished() {
        if (swipe_container != null) {
            swipe_container.postDelayed(new Runnable() {

                @Override
                public void run() {
                    if (swipe_container != null && swipe_container.isRefreshing()) {
                        swipe_container.setRefreshing(false);
                    }
                }
            }, 1000);
        }
    }

    private void refreshData() {
        if (swipe_container != null && !swipe_container.isRefreshing()) {
            swipe_container.post(new Runnable() {
                @Override
                public void run() {
                    swipe_container.setRefreshing(true);
                }
            });
        }
        processBusiness(1, page_size, REFRESH_TYPE_ALL);
        mRecyclerView.scrollToPosition(0);

    }

    private void setFollowAndFans(FriendsListBean friendsListBean) {
        if (friendsListBean.data == null) {
            return;
        }
        //刷新
        if (REFRESH_TYPE_ALL == refreshType) {
            listPlus.clear();
        }

        total = friendsListBean.data.followersTotal;
        setEmptyStatue();
        currentCountForOnce = friendsListBean.data.users.size();
        listPlus.addAll(friendsListBean.data.users);

        if (REFRESH_TYPE_ALL == refreshType) {
            listAdapter.removeAllFooterView();
            listAdapter.setNewData(listPlus);
            curPage = 2;
            if (listPlus.size() > 0) {
                listAdapter.openLoadMore(listPlus.size(), true);
            } else {
                listAdapter.openLoadMore(listPlus.size(), false);
            }
        } else {
            curPage++;
            listAdapter.notifyDataChangedAfterLoadMore(true, listPlus.size());

        }

        //刷新回到顶部
        if (REFRESH_TYPE_ALL == refreshType) {
            if (mRecyclerView.getChildCount() > 0) {
                mRecyclerView.scrollToPosition(0);
            }
        }
    }

    private void gotoUserInfoActivity(int position) {
        friendsBean = listAdapter.getData().get(position);
//        Intent intent = new Intent(this, UserInfoActivity.class);
//        Bundle bundle = new Bundle();
//        bundle.putString(TheLConstants.BUNDLE_KEY_USER_ID, friendsBean.userId);
//        bundle.putString("fromPage", this.getClass().getName());
//        intent.putExtras(bundle);
//        startActivityForResult(intent, TheLConstants.BUNDLE_CODE_FRIENDS_ACTIVITY);
        FlutterRouterConfig.Companion.gotoUserInfo(friendsBean.userId);
    }

    private void setEmptyStatue() {
        if (total > 0) {
            emtpy_view.setVisibility(View.GONE);
            rl_purchase.setVisibility(View.GONE);
            if (UserUtils.getUserVipLevel() > 0) {
                vip_renew_container.setVisibility(View.GONE);
                rl_purchase.setVisibility(View.GONE);
            } else {
                vip_renew_container.setVisibility(View.VISIBLE);
                btn_purchase.setText(R.string.renew_vip);
                rl_purchase.setVisibility(View.VISIBLE);
            }
            return;
        } else {
            emtpy_view.setVisibility(View.VISIBLE);
            rl_purchase.setVisibility(View.VISIBLE);
        }
        if (UserUtils.getUserVipLevel() > 0) {
            empty_view_txt.setText(R.string.relationship_secret_emtpy2);
            empty_view_txt_follow.setVisibility(View.VISIBLE);
            rl_purchase.setVisibility(View.GONE);
        } else {
            empty_view_txt.setText(R.string.relationship_secret_emtpy);
            empty_view_txt_follow.setVisibility(View.GONE);
            rl_purchase.setVisibility(View.VISIBLE);
            btn_purchase.setText(R.string.right_now_go_to_recharge);
        }
    }
}