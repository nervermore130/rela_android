package com.thel.modules.others;

import com.thel.base.BaseDataBean;
import com.thel.modules.others.beans.BannerAdBean;
import com.thel.modules.others.beans.BannerAdListBean;
import com.thel.modules.others.parsers.BaseParser;
import com.thel.utils.JsonUtils;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * 广告解析
 *
 * @author lingwei
 */
public class BannerAdParser extends BaseParser {

    @Override
    public BaseDataBean parse(String message) throws Exception {

        BannerAdListBean listBean = new BannerAdListBean();

        super.parse(message, listBean);
        if (listBean.result.equals("0")) {// 获取数据失败
            return listBean; // 不再解析具体数据了（也没有数据）
        }

        JSONObject object = new JSONObject(message);
        JSONObject jsonObject = JsonUtils.getJSONObject(object, "data");
        JSONArray jsonArray = JsonUtils.getJSONArray(jsonObject, "map_list");
        int arrayLength = jsonArray.length();
        for (int i = 0; i < arrayLength; i++) {
            BannerAdBean adBean = new BannerAdBean();
            JSONObject tempobj = jsonArray.getJSONObject(i);
            adBean.fromJson(tempobj);
            listBean.adlist.add(adBean);
        }
        return listBean;
    }

}
