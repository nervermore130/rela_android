package com.thel.modules.main.me.aboutMe;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.modules.main.me.adapter.FriendsListAdapter;
import com.thel.modules.main.me.bean.FriendsBean;
import com.thel.modules.main.me.bean.FriendsListBean;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.ui.decoration.DefaultItemDivider;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

import org.reactivestreams.Subscription;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * 搜索我的粉丝和关注页
 * Created by lingwei on 2017/11/10.
 */

public class SearchMyFriendsActivity extends BaseActivity {
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.lin_back)
    LinearLayout linBack;
    @BindView(R.id.txt_title)
    TextView txtTitle;
    @BindView(R.id.img_more)
    ImageView imgMore;
    @BindView(R.id.lin_more)
    LinearLayout linMore;
    @BindView(R.id.rel_title_layout)
    RelativeLayout titleLayout;
    @BindView(R.id.edit_search)
    EditText editSearch;
    @BindView(R.id.img_search)
    ImageView imgSearch;
    @BindView(R.id.lin_search)
    LinearLayout linSearch;
    @BindView(R.id.decoration)
    ImageView decoration;
    @BindView(R.id.search_recyclerview)
    RecyclerView searchRecyclerview;
    @BindView(R.id.img)
    ImageView img;
    @BindView(R.id.no_match_tip)
    RelativeLayout noMatchTip;
    @BindView(R.id.rel_search_result)
    RelativeLayout relSearchResult;
    private String keyWord;
    private ArrayList<FriendsBean> searchListPlus = new ArrayList<>();
    private FriendsListAdapter friendsListAdapter;

    //刷新类型，全部刷新 即下拉刷新为1，加载下一页为2
    public int refreshType = 0;
    public final int REFRESH_TYPE_ALL = 1;
    public final int REFRESH_TYPE_NEXT_PAGE = 2;
    private int searchCurpage = 1;
    private int searchCountForOnce = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_my_friends_activity);
        ButterKnife.bind(this);
        initAdapter();
        setListener();
    }

    private void initAdapter() {
        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(RecyclerView.VERTICAL);
        searchRecyclerview.setLayoutManager(manager);
        searchRecyclerview.addItemDecoration(new DefaultItemDivider(this, LinearLayoutManager.VERTICAL, R.color.gray, 1, true, true));
        searchRecyclerview.setHasFixedSize(true);
        friendsListAdapter = new FriendsListAdapter(searchListPlus, FollowingAndFansFragment.TYPE_FRIEND,this);
        searchRecyclerview.setAdapter(friendsListAdapter);
    }

    private void setListener() {
        imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                ViewUtils.hideSoftInput(SearchMyFriendsActivity.this, v);

                keyWord = editSearch.getText().toString().trim();
                if (!TextUtils.isEmpty(keyWord)) {
                    searchListPlus.clear();
                    searchCurpage = 1;
                    searchMyFriend();
                } else {
                    searchListPlus.clear();
                    noMatchTip.setVisibility(View.VISIBLE);
                    friendsListAdapter.notifyDataSetChanged();
                }
            }
        });

        friendsListAdapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                goToUserinfoActivity(position);
            }
        });
    }

    private void goToUserinfoActivity(int position) {
        FriendsBean friendsBean = friendsListAdapter.getData().get(position);
//        Intent intent = new Intent(SearchMyFriendsActivity.this, UserInfoActivity.class);
//        Bundle bundle = new Bundle();
//        bundle.putString(TheLConstants.BUNDLE_KEY_USER_ID, friendsBean.userId);
//        bundle.putString("fromPage", this.getClass().getName());
//        intent.putExtras(bundle);
//        startActivityForResult(intent, TheLConstants.BUNDLE_CODE_FRIENDS_ACTIVITY);
        FlutterRouterConfig.Companion.gotoUserInfo(friendsBean.userId);
    }

    private void searchMyFriend() {
        Flowable<FriendsListBean> flowable = RequestBusiness.getInstance().searchMyFriends(0, keyWord, searchCurpage + "", "20");
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<FriendsListBean>() {

            @Override
            public void onNext(FriendsListBean friendsListBean) {
                super.onNext(friendsListBean);
                getSearchFriendData(friendsListBean);
            }

            @Override
            public void onComplete() {

            }
        });

    }

    private void getSearchFriendData(FriendsListBean friendsListBean) {
        if (friendsListBean.data == null || friendsListBean.data.users == null) {
            return;
        }
        if (refreshType == REFRESH_TYPE_ALL) {
            searchListPlus.clear();
        }
        for (FriendsBean friendsBean : friendsListBean.data.users) {
            friendsBean.followStatus = 3;
        }
        searchListPlus.addAll(friendsListBean.data.users);
        searchCountForOnce = friendsListBean.data.users.size();
        if (refreshType == REFRESH_TYPE_ALL) {
            friendsListAdapter.removeAllFooterView();
            friendsListAdapter.setNewData(searchListPlus);
            searchCurpage = 2;
            if (searchListPlus.size() > 0) {
                friendsListAdapter.openLoadMore(searchListPlus.size(), true);
            } else {
                friendsListAdapter.openLoadMore(searchListPlus.size(), false);
            }
        } else {
            searchCurpage++;
            friendsListAdapter.notifyDataChangedAfterLoadMore(true, searchListPlus.size());

        }
        if (searchListPlus.size() == 0) {
            noMatchTip.setVisibility(View.VISIBLE);
        } else {
            noMatchTip.setVisibility(View.GONE);
        }

    }


    @OnClick(R.id.lin_back)
    public void onLinBackClicked() {
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(this.getClass().getName());
        MobclickAgent.onResume(this);
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(this.getClass().getName());
        MobclickAgent.onPause(this);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            this.finish();
            return true;
        }
        return true;
    }
}
