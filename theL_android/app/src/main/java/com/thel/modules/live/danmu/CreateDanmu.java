package com.thel.modules.live.danmu;

import android.view.View;
import android.view.ViewGroup;

import com.thel.modules.live.bean.DanmuBean;

public interface CreateDanmu {

    View createDanmuView(ViewGroup viewGroup, DanmuBean danmuBean);

}
