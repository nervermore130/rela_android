package com.thel.modules.live.interfaces

import com.thel.modules.live.`in`.LiveShowViewMultiIn
import com.thel.modules.live.`in`.LiveShowViewPkIn
import com.thel.modules.live.bean.LiveRoomMsgBean
import com.thel.modules.live.bean.LiveRoomMsgConnectMicBean
import com.thel.modules.live.bean.ResponseGemBean
import com.thel.modules.live.bean.TopTodayBean
import com.thel.modules.live.surface.watch.LiveWatchObserver
import com.thel.modules.live.view.expensive.TopGiftBean

interface ILive : LiveShowViewPkIn, LiveShowViewMultiIn {

    fun bindObserver(observer: LiveWatchObserver)

    fun updateAudienceCount(responseGemBean: ResponseGemBean)

    fun refreshMsg()

    fun smoothScrollToBottom()

    fun clearInput()

    fun updateMyNetStatusBad()

    fun parseGiftMsg(payload: String, code: String)

    fun showDanmu(danMsg: String)

    fun openInput(open: Boolean)

    fun setDanmuResult(result: String)

    fun joinVipUser(liveRoomMsgBean: LiveRoomMsgBean)

    fun addLiveRoomMsg(liveRoomMsgBean: LiveRoomMsgBean)

    fun showTopGiftView(topGiftBean: TopGiftBean)

    /**
     * 主播发起连麦请求
     *
     * @param lIveRoomMsgConnectMicBean
     */
    fun showConnectMicDialog(lIveRoomMsgConnectMicBean: LiveRoomMsgConnectMicBean)

    /**
     * 对方同意连麦
     */
    fun linkMicAccept()

    /**
     * 对方取消连麦
     */

    fun lingMicCancel()


    /**
     * 守护棒变动消息
     */
    fun topToday(topTodayBean: TopTodayBean)

    /**
     * 展示11级用户升级的view
     */
    fun upgradeEffects(liveRoomMsgBean: LiveRoomMsgBean)

    /**
     * 展示35级以上用户升级的view
     */
    fun allUpgradeEffects(liveRoomMsgBean: LiveRoomMsgBean)


}