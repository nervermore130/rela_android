package com.thel.modules.main.userinfo;

import android.content.Context;
import androidx.core.content.ContextCompat;
import android.text.SpannableString;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.user.UserInfoBean;
import com.thel.modules.main.me.bean.RoleBean;
import com.thel.utils.DateUtils;
import com.thel.utils.DeviceUtils;
import com.thel.utils.DialogUtil;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by waiarl on 2017/10/17.
 */

public class UserInfoRightView extends RelativeLayout {
    private final Context mContext;
    private View lin_rela_id;
    private View lin_age;
    private View lin_info;
    private View lin_role;
    private View lin_lookfor;
    private View lin_relationship;
    private View lin_career;
    private View lin_place;
    private View lin_intro;
    private TextView txt_rela_id;
    private TextView txt_age;
    private TextView txt_info;
    private TextView txt_role;
    private TextView txt_lookfor;
    private TextView txt_relationship;
    private TextView txt_career;
    private TextView txt_place;
    private TextView txt_others;
    private TextView txt_intro;
    private LinearLayout lin_account_safe;
    private View txt_cancel;
    private View scrollview;
    private LinearLayout revise_info;
    private TextView txt_out_level_start;
    private LinearLayout lin_weight;
    private TextView txt_weight;
    private int listPoi;
    private SparseArray<RoleBean> relationshipMap = new SparseArray<RoleBean>();
    private TextView txt_copy_rela_id;
    private String userid;

    public UserInfoRightView(Context context) {
        this(context, null);
    }

    public UserInfoRightView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public UserInfoRightView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
        initLocalData();
        initRelationshipMap();
        setListener();

    }

    private void initRelationshipMap() {
        relationshipMap.put(1, new RoleBean(0, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[0]));
        relationshipMap.put(6, new RoleBean(1, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[1]));
        relationshipMap.put(7, new RoleBean(2, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[2]));
        relationshipMap.put(2, new RoleBean(3, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[3]));
        relationshipMap.put(3, new RoleBean(4, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[4]));
        relationshipMap.put(4, new RoleBean(5, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[5]));
        relationshipMap.put(5, new RoleBean(6, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[6]));
        relationshipMap.put(0, new RoleBean(7, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[7]));


    }

    private void init() {
        inflate(mContext, R.layout.userinfo_right, this);
        scrollview = findViewById(R.id.scrollview);
        lin_rela_id = findViewById(R.id.lin_rela_id);
        lin_age = findViewById(R.id.lin_age);
        lin_info = findViewById(R.id.lin_info);
        lin_weight = findViewById(R.id.lin_weight);
        lin_role = findViewById(R.id.lin_role);
        lin_lookfor = findViewById(R.id.lin_lookfor);
        lin_relationship = findViewById(R.id.lin_relationship);
        lin_career = findViewById(R.id.lin_career);
        lin_place = findViewById(R.id.lin_place);
        lin_intro = findViewById(R.id.lin_intro);

        txt_rela_id = findViewById(R.id.txt_rela_id);
        txt_age = findViewById(R.id.txt_age);
        txt_info = findViewById(R.id.txt_info);
        txt_role = findViewById(R.id.txt_role);
        txt_lookfor = findViewById(R.id.txt_lookfor);
        txt_relationship = findViewById(R.id.txt_relationship);
        txt_career = findViewById(R.id.txt_career);
        txt_place = findViewById(R.id.txt_place);
        txt_intro = findViewById(R.id.txt_intro);
        txt_weight = findViewById(R.id.txt_weight);

        lin_account_safe = findViewById(R.id.lin_account_safe);
        revise_info = findViewById(R.id.revise_info);
        txt_cancel = findViewById(R.id.txt_cancel);
        txt_copy_rela_id = findViewById(R.id.txt_copy_rela_id);

        txt_rela_id.setTextIsSelectable(true);
        txt_age.setTextIsSelectable(true);
        txt_info.setTextIsSelectable(true);
        txt_career.setTextIsSelectable(true);
        txt_place.setTextIsSelectable(true);
        txt_intro.setTextIsSelectable(true);
    }

    private void setListener() {
        scrollview.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    clearFocus();
                }
                return false;
            }
        });
        txt_copy_rela_id.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(userid)) {
                    String userName = userid.trim();
                    DeviceUtils.copyToClipboard(mContext, userName);
                    DialogUtil.showToastShort(mContext, TheLApp.context.getString(R.string.rela_id_had_copy));
                }

            }
        });
    }

    public void refreshUI(UserInfoBean userInfoBean) {

        /************************************基本资料********************************************************/
        //rela ID
        if (!TextUtils.isEmpty(userInfoBean.userName)) {
            lin_rela_id.setVisibility(View.VISIBLE);
            userid = userInfoBean.userName;
            setRelaId(lin_rela_id, txt_rela_id, userInfoBean.userName);
        }

        //年龄星座
        if (!TextUtils.isEmpty(userInfoBean.age)) {
            lin_age.setVisibility(VISIBLE);
            setAgeAndStar(lin_age, txt_age, userInfoBean);
        } else {
            lin_age.setVisibility(VISIBLE);

            set18AgeAndStar(lin_age, txt_age, userInfoBean);
        }

        //身高体重
        if (!TextUtils.isEmpty(userInfoBean.height)) {
            lin_info.setVisibility(VISIBLE);
            setHeight(lin_info, txt_info, userInfoBean);
        }
        //体重
        if (!TextUtils.isEmpty(userInfoBean.weight)) {
            lin_weight.setVisibility(VISIBLE);
            setWeight(lin_weight, txt_weight, userInfoBean);
        }

        // 角色设定
        setRole(lin_role, txt_role, userInfoBean.roleName);

        //寻找角色
        setLookfor(lin_lookfor, txt_lookfor, userInfoBean);

        //感情状态
        if (!TextUtils.isEmpty(userInfoBean.affection)) {
            lin_relationship.setVisibility(View.VISIBLE);
            setRelationShip(lin_relationship, txt_relationship, userInfoBean);
        }

        /************************************详细资料********************************************************/

        //职业描述
        if (!TextUtils.isEmpty(userInfoBean.occupation)) {
            lin_career.setVisibility(View.VISIBLE);
            setOccupation(lin_career, txt_career, userInfoBean.occupation);
        }
        //居住地
        if (!TextUtils.isEmpty(userInfoBean.livecity)) {
            lin_place.setVisibility(View.VISIBLE);
            setLiveCity(lin_place, txt_place, userInfoBean.livecity);
        }

        //详细自述
        if (!TextUtils.isEmpty(userInfoBean.intro)) {
            lin_intro.setVisibility(View.VISIBLE);
            setIntro(lin_intro, txt_intro, userInfoBean.intro);
        }
        //设置账号安全
        setAccountSafe(lin_account_safe, userInfoBean);

    }

    private void setWeight(LinearLayout lin_weight, TextView txt_weight, UserInfoBean userInfoBean) {
        String startstring = mContext.getResources().getString(R.string.detailed_filters_activity_weight);
        SpannableString spannaString = UserInfoUtils.getUserInfoSpannaString(startstring, getWeight(userInfoBean.weight));
        txt_weight.setText(spannaString);
    }

    //身高
    private void setHeight(View lin_info, TextView txt_info, UserInfoBean userInfoBean) {
        String startstring = mContext.getResources().getString(R.string.detailed_filters_activity_height);
        SpannableString spannaString = UserInfoUtils.getUserInfoSpannaString(startstring, getHeight(userInfoBean.height));
        txt_info.setText(spannaString);
    }

    //Rela ID
    private void setRelaId(View lin_rela_id, TextView txt_rela_id, String userName) {
        String startstring = mContext.getResources().getString(R.string.updatauserinfo_activity_username);
        SpannableString spannaString = UserInfoUtils.getUserInfoSpannaString(startstring, userName);
        txt_rela_id.setText(spannaString);
    }

    //年龄星座
    private void setAgeAndStar(View lin_rela_id, TextView txt_age, UserInfoBean userInfoBean) {
        String startstring = mContext.getResources().getString(R.string.updatauserinfo_activity_age_with);
        SpannableString spannaString = UserInfoUtils.getUserInfoSpannaString(startstring, getAgeAndStar(userInfoBean));
        txt_age.setText(spannaString);
    }

    //年龄未满18岁
    private void set18AgeAndStar(View lin_rela_id, TextView txt_age, UserInfoBean userInfoBean) {
        String startstring = mContext.getResources().getString(R.string.updatauserinfo_activity_age_with);
        SpannableString spannaString = UserInfoUtils.getUserInfoSpannaString(startstring, get18AgeAndStar(userInfoBean));
        txt_age.setText(spannaString);
    }

    //身高体重
    private void setHeightAndWeight(View lin_info, TextView txt_info, UserInfoBean userInfoBean) {
        String startstring = mContext.getResources().getString(R.string.detailed_filters_activity_height);
        SpannableString spannaString = UserInfoUtils.getUserInfoSpannaString(startstring, getHandW(userInfoBean.height, userInfoBean.weight));
        txt_info.setText(spannaString);
    }

    // 角色
    private void setRole(View lin, TextView txt_role, String role) {
        if (TextUtils.isEmpty(role)) {
            return;
        } else {
            try {

                int index = Integer.parseInt(role);
                RoleBean roleBean = ShareFileUtils.getRoleBean(index);
                String startstring = mContext.getResources().getString(R.string.slef_identity) + ":";
                SpannableString spannaString;
                if (roleBean != null) {
                    spannaString = UserInfoUtils.getUserInfoSpannaString(startstring, roleBean.contentRes);

                } else {
                    spannaString = UserInfoUtils.getUserInfoSpannaString(startstring, "");
                }
                txt_role.setText(spannaString);
                lin.setVisibility(VISIBLE);
            } catch (NumberFormatException e) {
                return;
            }
        }
    }

    //寻找角色
    private void setLookfor(View lin_lookfor, TextView txt_lookfor, UserInfoBean userInfoBean) {
        if (TextUtils.isEmpty(userInfoBean.wantRole))
            return;
        int otherRoleCount = 0;
        try {
            String[] lookingForRole = userInfoBean.wantRole.split(",");
            StringBuilder sb = new StringBuilder();// 交友目的内容字符串拼接
            int size = lookingForRole.length;
            ArrayList newRoleList = new ArrayList();
            for (int i = 0; i < size; i++) {
                int index = Integer.parseInt(lookingForRole[i]); //1.2.3.4.5.6.7

                if ((index == 6 || index == 7 || index == 5)) {
                    if (otherRoleCount < 1) {
                        index = 5;
                        otherRoleCount++;
                    } else {
                        continue;
                    }
                }
                RoleBean roleBean = ShareFileUtils.getRoleBean(index);
                if (roleBean != null) {
                    int listPoi = roleBean.listPoi;
                    if (listPoi == 5) {
                        newRoleList.add(listPoi);
                    } else {

                    }
                    sb.append(role_list.get(listPoi));
                } else {
                }
                if (i != size - 1) {

                    sb.append(",");
                }
            }

            int j = 0;
            for (int i = 0; i < sb.length(); i++) {
                if (newRoleList.contains(sb.charAt(i))) {
                    sb.deleteCharAt(i - j);
                }
            }
            String startstring = mContext.getResources().getString(R.string.userinfo_right_lookingfor_role);
            SpannableString spannaString = UserInfoUtils.getUserInfoSpannaString("", sb.toString());
            String content = spannaString.toString();
            if (content != null && content.length() > 0) {
                if (content.endsWith(",")) {
                    content = content.substring(0, content.length() - 1);
                }
                txt_lookfor.setText(content);
            }
            SpannableString spannaString2 = UserInfoUtils.getUserInfoSpannaString(startstring, content);
            txt_lookfor.setText(spannaString2);

            lin_lookfor.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //感情状态
    private void setRelationShip(View lin_relationship, TextView txt_relationship, UserInfoBean userinfoBean) {
        String startstring = mContext.getResources().getString(R.string.updatauserinfo_activity_lovestatus);
        try {

            int affection = Integer.parseInt(userinfoBean.affection);
            RoleBean relationshipBean = relationshipMap.get(affection);

            if (relationshipBean != null) {
                SpannableString spannaString = UserInfoUtils.getUserInfoSpannaString(startstring, relationshipBean.contentRes);

                txt_relationship.setText(spannaString);

            }


        } catch (Exception e) {
        }

    }

    //交友目的
    private void setGoal(View lin_goal, TextView txt_goal, UserInfoBean userInfoBean) {
        String[] purpose = userInfoBean.purpose.split(",");
        StringBuilder purposeContentSB = new StringBuilder();// 交友目的内容字符串拼接
        int purposeSize = purpose.length;
        try {
            for (int i = 0; i < purposeSize; i++) {
                int index = Integer.parseInt(purpose[i]);
                if (index < 0 || index >= lookingfor_list.size()) {
                    index = 0;
                }
                purposeContentSB.append(lookingfor_list.get(index));
                if (i != purposeSize - 1) {
                    purposeContentSB.append(",");
                }
            }
            String startstring = mContext.getResources().getString(R.string.updatauserinfo_activity_goal);
            SpannableString spannaString = UserInfoUtils.getUserInfoSpannaString(startstring, purposeContentSB.toString());
            txt_goal.setText(spannaString);
        } catch (Exception e) {
            e.printStackTrace();
            lin_goal.setVisibility(View.GONE);
        }
    }


    //职业描述
    private void setOccupation(View lin_career, TextView txt_career, String occupation) {
        String startstring = mContext.getResources().getString(R.string.updatauserinfo_activity_career_type);
        SpannableString spannaString = UserInfoUtils.getUserInfoSpannaString(startstring, occupation);
        txt_career.setText(spannaString);

    }

    //居住地
    private void setLiveCity(View lin_place, TextView txt_place, String livecity) {
        String startstring = mContext.getResources().getString(R.string.updatauserinfo_activity_place);
        SpannableString spannaString = UserInfoUtils.getUserInfoSpannaString(startstring, livecity);
        txt_place.setText(spannaString);
    }

    //详细自述
    private void setIntro(View lin_intro, TextView txt_intro, String intro) {
        String startstring = mContext.getResources().getString(R.string.personalized_signature) + ":";
        SpannableString spannaString = UserInfoUtils.getUserInfoSpannaString(startstring, intro);
        txt_intro.setText(spannaString);
    }

    //设置账号安全
    private void setAccountSafe(LinearLayout lin_account_safe, UserInfoBean userInfoBean) {
        final int count = lin_account_safe.getChildCount();
        lin_account_safe.removeViews(1, count - 1);
        lin_account_safe.setVisibility(View.GONE);
        if (!TextUtils.isEmpty(userInfoBean.cell)) {
            lin_account_safe.setVisibility(View.VISIBLE);
            ImageView imgCell = new ImageView(mContext);
            imgCell.setImageResource(R.mipmap.btn_login_phone_normal);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(Utils.dip2px(TheLApp.getContext(), 21), Utils.dip2px(TheLApp.getContext(), 21));
            params.setMargins(0, 0, Utils.dip2px(mContext, 10), 0);
            imgCell.setLayoutParams(params);
            lin_account_safe.addView(imgCell);
            lin_account_safe.setVisibility(View.VISIBLE);
        }
        if (!TextUtils.isEmpty(userInfoBean.wx)) {
            lin_account_safe.setVisibility(View.VISIBLE);
            ImageView imgWx = new ImageView(mContext);
            imgWx.setImageResource(R.mipmap.btn_share_wechat_normal);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(Utils.dip2px(TheLApp.getContext(), 21), Utils.dip2px(TheLApp.getContext(), 21));
            params.setMargins(0, 0, Utils.dip2px(mContext, 10), 0);
            imgWx.setLayoutParams(params);
            lin_account_safe.addView(imgWx);
            lin_account_safe.setVisibility(View.VISIBLE);
        }
        if (!TextUtils.isEmpty(userInfoBean.fb)) {
            lin_account_safe.setVisibility(View.VISIBLE);
            ImageView imgFb = new ImageView(mContext);
            imgFb.setImageResource(R.mipmap.btn_share_fb_normal);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(Utils.dip2px(TheLApp.getContext(), 21), Utils.dip2px(TheLApp.getContext(), 21));
            params.setMargins(0, 0, Utils.dip2px(mContext, 10), 0);
            imgFb.setLayoutParams(params);
            lin_account_safe.addView(imgFb);
            lin_account_safe.setVisibility(View.VISIBLE);
        }
    }

    //身高
    private String getHeight(String height) {
        int heightUnits = ShareFileUtils.getInt(ShareFileUtils.HEIGHT_UNITS, 0); // 身高单位(0=cm, 1=Inches)
        try {
            String h;
            if (heightUnits == 0) {
                h = Float.valueOf(height).intValue() + " cm";
            } else {
                h = Utils.cmToInches(height) + " Inches";
            }

            return h;
        } catch (Exception e) {
        }
        return "";
    }

    //体重
    private String getWeight(String weight) {
        int weightUnits = ShareFileUtils.getInt(ShareFileUtils.WEIGHT_UNITS, 0); // 体重单位(0=kg, 1=Lbs)
        try {
            String w;

            if (weightUnits == 0) {
                w = Float.valueOf(weight).intValue() + " kg";
            } else {
                w = Utils.kgToLbs(weight) + " Lbs";
            }
            return w;
        } catch (Exception e) {
        }
        return "";
    }

    //身高体重
    private String getHandW(String height, String weight) {
        int heightUnits = ShareFileUtils.getInt(ShareFileUtils.HEIGHT_UNITS, 0); // 身高单位(0=cm, 1=Inches)
        int weightUnits = ShareFileUtils.getInt(ShareFileUtils.WEIGHT_UNITS, 0); // 体重单位(0=kg, 1=Lbs)
        try {
            String h;
            String w;
            if (heightUnits == 0) {
                h = Float.valueOf(height).intValue() + " cm";
            } else {
                h = Utils.cmToInches(height) + " Inches";
            }
            if (weightUnits == 0) {
                w = Float.valueOf(weight).intValue() + " kg";
            } else {
                w = Utils.kgToLbs(weight) + " Lbs";
            }
            return h + "/" + w;
        } catch (Exception e) {
        }
        return "";
    }

    //年龄和星座
    private String getAgeAndStar(UserInfoBean userInfoBean) {
        if (!TextUtils.isEmpty(userInfoBean.birthday)) {
            return userInfoBean.age + ", " + DateUtils.date2Constellation(userInfoBean.birthday);
        } else {
            return "";
        }
    }

    //年龄和星座
    private String get18AgeAndStar(UserInfoBean userInfoBean) {
        if (!TextUtils.isEmpty(userInfoBean.birthday)) {
            return "18, " + DateUtils.date2Constellation(userInfoBean.birthday);
        } else {
            return "";
        }
    }

    //星座
    private String getHoroscope(String horoscope) {
        int horoscope_index = 0;
        try {
            horoscope_index = Integer.parseInt(horoscope);
        } catch (Exception e) {
            horoscope_index = 0;
        }
        if (horoscope_index < 0 || horoscope_index >= constellation_list.size()) {
            horoscope_index = 0;
        }
        return constellation_list.get(horoscope_index);
    }

    // 数据
    private ArrayList<String> constellation_list = new ArrayList<String>(); // 星座
    private ArrayList<String> relationship_list = new ArrayList<String>(); // 感情状态
    private ArrayList<String> ethnicity_list = new ArrayList<String>(); // 种族
    private ArrayList<String> lookingfor_list = new ArrayList<String>(); // 交友目的
    private ArrayList<String> out_level_list = new ArrayList<String>(); // 出柜程度
    private ArrayList<List<String>> careerTypeNames;// 职业类型
    private ArrayList<String> role_list = new ArrayList<String>(); // 角色

    private void initLocalData() {
        String[] constellation_Array = TheLApp.getContext().getResources().getStringArray(R.array.filter_constellation);
        String[] relationship_Array = TheLApp.getContext().getResources().getStringArray(R.array.userinfo_relationship);
        String[] ethnicity_Array = TheLApp.getContext().getResources().getStringArray(R.array.userinfo_ethnicity);
        String[] lookingfor_Array = TheLApp.getContext().getResources().getStringArray(R.array.userinfo_lookingfor);
        String[] out_level_Array = this.getResources().getStringArray(R.array.out_level);
        String[] role_Array = this.getResources().getStringArray(R.array.userinfo_role);
        constellation_list = new ArrayList<String>(Arrays.asList(constellation_Array));
        relationship_list = new ArrayList<String>(Arrays.asList(relationship_Array));
        ethnicity_list = new ArrayList<String>(Arrays.asList(ethnicity_Array));
        lookingfor_list = new ArrayList<String>(Arrays.asList(lookingfor_Array));
        out_level_list = new ArrayList<String>(Arrays.asList(out_level_Array));
        role_list = new ArrayList<String>(Arrays.asList(role_Array));

        String[] arr_tech = this.getResources().getStringArray(R.array.career_type_tech);
        String[] arr_culture = this.getResources().getStringArray(R.array.career_type_culture);
        String[] arr_entertainment = this.getResources().getStringArray(R.array.career_type_entertainment);
        String[] arr_economy = this.getResources().getStringArray(R.array.career_type_economy);
        String[] arr_manufacturing = this.getResources().getStringArray(R.array.career_type_manufacturing);
        String[] arr_transport = this.getResources().getStringArray(R.array.career_type_transport);
        String[] arr_service = this.getResources().getStringArray(R.array.career_type_service);
        String[] arr_utilities = this.getResources().getStringArray(R.array.career_type_utilities);
        String[] arr_student = this.getResources().getStringArray(R.array.career_type_student);
        String[] arr_other = new String[]{TheLApp.getContext().getString(R.string.info_other)};
        String[] arr_do_not_show = new String[]{TheLApp.getContext().getString(R.string.info_do_not_show)};
        careerTypeNames = new ArrayList<List<String>>();
        careerTypeNames.add(Arrays.asList(arr_tech));
        careerTypeNames.add(Arrays.asList(arr_culture));
        careerTypeNames.add(Arrays.asList(arr_entertainment));
        careerTypeNames.add(Arrays.asList(arr_economy));
        careerTypeNames.add(Arrays.asList(arr_manufacturing));
        careerTypeNames.add(Arrays.asList(arr_transport));
        careerTypeNames.add(Arrays.asList(arr_service));
        careerTypeNames.add(Arrays.asList(arr_utilities));
        careerTypeNames.add(Arrays.asList(arr_student));
        careerTypeNames.add(Arrays.asList(arr_other));
        careerTypeNames.add(Arrays.asList(arr_do_not_show));
    }

    public void setCancelListener(View.OnClickListener listener) {
        txt_cancel.setOnClickListener(listener);
    }

    public void setReviseInfoListener(View.OnClickListener listener) {
        revise_info.setOnClickListener(listener);
    }

    public void setReviseInfoShow() {
        revise_info.setVisibility(VISIBLE);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            clearFocus();
        }
        return super.onTouchEvent(event);
    }
}
