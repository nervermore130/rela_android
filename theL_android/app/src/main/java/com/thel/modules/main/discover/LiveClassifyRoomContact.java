package com.thel.modules.main.discover;

import com.thel.base.BasePresenter;
import com.thel.base.BaseView;
import com.thel.bean.AdBean;
import com.thel.bean.live.LiveRoomsBean;

/**
 * Created by lingwei on 2017/11/21.
 */

public interface LiveClassifyRoomContact {
    interface View extends BaseView<Presenter> {
        /**
         * 显示刷新热门直播列表数据
         *
         * @param data
         */
        void showRefreshHotRoomListData(LiveRoomsBean data);

        /**
         * 显示更多热门直播列表数据
         *
         * @param data
         */
        void showMoreHotRoomListData(LiveRoomsBean data);

        /**
         * 显示 刷新 新人正在直播列表
         *
         * @param data
         */
        void showRefreshNewsLiveListData(LiveRoomsBean data);

        /**
         * 显示 更多 新人正在直播列表
         *
         * @param data
         */
        void showMoreNewsLiveListData(LiveRoomsBean data);

        /**
         * 显示刷新推荐新人列表
         *
         * @param data
         */
        void showRefreshRecommandNewAnchorsData(LiveRoomsBean data);


        /**
         * 显示 更多 推荐新人直播列表
         *
         * @param data
         */
        void showMoreRecommandNewAnchorsData(LiveRoomsBean data);

        /**
         * 显示 刷新 附近直播列表
         *
         * @param data
         */
        void showRefreshNeighboursListData(LiveRoomsBean data);

        /**
         * 显示  更多附近直播列表
         *
         * @param data
         */
        void showMoreNeighboursListData(LiveRoomsBean data);

        /**
         * 显示 刷新 音频直播列表
         *
         * @param data
         */
        void showRefreshAudioListData(LiveRoomsBean data);

        /**
         * 显示  更多附近直播列表
         *
         * @param data
         */
        void showMoreAudioListData(LiveRoomsBean data);

        /**
         * 显示刷新  根据类型 获取的直播列表
         *
         * @param data
         */
        void showRefreshLiveListWithTypeData(LiveRoomsBean data);

        /**
         * 显示更多 根据类型获取的直播列表
         *
         * @param data
         */
        void showMoreLiveListWithTypeData(LiveRoomsBean data);

        /**
         * 显示刷新 推荐关注 类型的主播列表
         *
         * @param data
         */
        void showRefreshRecommendFollowListTypeData(LiveRoomsBean data);

        /**
         * 显示更多  推荐关注 类型的主播列表
         *
         * @param data
         */
        void showMoreRecommendFollowListTypeData(LiveRoomsBean data);

        void emptyData(boolean emptydata);

        void loadMoreFailed();

        void requestFinish();

        void refreshAdArea(AdBean adBean);

        /**
         * 显示刷新视频直播列表
         */
        void showRefreshVedioListData(LiveRoomsBean data);

        /**
         * 显示刷新视频更多数据
         */
        void showMoreVedioListData(LiveRoomsBean data);
    }

    interface View2 extends View {


    }

    interface Presenter extends BasePresenter {
        /**
         * 获取热门直播列表
         */
        void getRefreshHotRoomListData();

        /**
         * 获取更多热门直播列表
         *
         * @param cursor cursor
         */
        void getMoreHotRoomListData(int cursor);

        /**
         * 获取 刷新 正在直播的新人列表
         */
        void getRefreshNewsLiveListData();

        /**
         * 获取更多正在直播的新人列表
         *
         * @param cursor cursor
         */
        void getMoreNewsLiveListData(int cursor);

        /**
         * 获取更多推荐的新人列表
         *
         * @param cursor cursor
         */
        void getMoreRecommedNewAnchorsData(int cursor);

        /**
         * 获取刷新 推荐的新人列表
         */
        void getRefreshRecommedNewAnchorsData();

        /**
         * 获取附近直播列表 刷新
         */
        void getRefreshNeighboursListData();

        /**
         * 获取更多附近直播列表
         *
         * @param cursor cursor
         */
        void getMoreNeighboursListData(int cursor);

        /**
         * 获取音频直播列表 刷新
         */
        void getRefreshAudioListData();

        void getRefreshRadioListData();

        void getRefreshHotChatListData();

        /**
         * 获取更多音频直播列表
         *
         * @param cursor cursor
         */
        void getMoreAudioListData(int cursor);

        /**
         * 获取视频直播列表 刷新
         */
        void getRefreshVedioListData();

        /**
         * 获取更多视频直播列表
         *
         * @param cursor cursor
         */
        void getMoreVedioListData(int cursor);

        void getMoreRadioListData(int cursor);

        void getMoreHotChatListData(int cursor);


        /**
         * 根据id 获取直播列表，刷新的列表
         *
         * @param id logType
         */
        void getRefreshLiveListWithTypeData(int id);

        /**
         * 根据类型获取更更多直播
         *
         * @param id     logType
         * @param cursor
         */
        void getMoreLiveListWithTypeData(int id, int cursor);


        /**
         * 获取 刷新  推荐相关类型的主播列表
         *
         * @param id id
         */
        void getRefreshRecommendFollowListTypeData(int id);

        /**
         * 获取更多推荐关注相应类型的主播列表
         *
         * @param id     logType
         * @param cursor cursor
         */
        void getMoreRecommendFollowListTypeData(int id, int cursor);


        void loadAdvert();
    }

}
