package com.thel.modules.live.view;

import android.content.Context;
import android.content.DialogInterface;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.utils.DialogUtil;
import com.thel.utils.L;
import com.thel.utils.ScreenUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by liuyun on 2017/12/18.
 */

public class LinkMicInfoView extends RelativeLayout {

    private String toUserId;

    @BindView(R.id.link_mic_name_tv)
    TextView link_mic_name_tv;

    @BindView(R.id.link_mic_hangup_tv)
    TextView link_mic_hangup_tv;

    public LinkMicInfoView(Context context) {
        super(context);
        init(context);
    }

    public LinkMicInfoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public LinkMicInfoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int screenWidth = ScreenUtils.getScreenWidth(TheLApp.context) - 3;

        int screenHeight = ScreenUtils.getScreenHeight(TheLApp.context);

        int width = (int) (screenWidth * 0.5);

        int height = (int) (screenHeight * 0.375);

        widthMeasureSpec = MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY);

        heightMeasureSpec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    private void init(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_link_mic_layer, this, true);
        ButterKnife.bind(this, view);

    }

    public void show(String userName, String toUserId, boolean isUser) {

        L.d("LinkMicInfoView", " ---------show-------- : " + isUser);

        this.setVisibility(View.VISIBLE);

        this.toUserId = toUserId;

        if (isUser) {
            link_mic_hangup_tv.setVisibility(View.GONE);
        } else {
            link_mic_hangup_tv.setVisibility(View.VISIBLE);
        }

        link_mic_name_tv.setText(userName);

    }

    public void hide() {

        L.d("LinkMicInfoView", " ---------hide-------- : ");

        this.setVisibility(View.GONE);
    }

    @OnClick(R.id.link_mic_hangup_tv)
    void onHangup() {

        if (mOnLinkMicClickListener != null) {

            DialogUtil.showConfirmDialog(getContext(), "", getContext().getResources().getString(R.string.sure_end_mic), getContext().getResources().getString(R.string.info_ok), getContext().getResources().getString(R.string.info_no), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    mOnLinkMicClickListener.onHangup(toUserId);
                }
            });
        }
    }

    @OnClick(R.id.root_rl)
    void onClickNickName() {

        if (mOnLinkMicNickNameClickListener != null) {
            mOnLinkMicNickNameClickListener.onClickNickName(toUserId);
        }

    }

    private OnLinkMicHangupClickListener mOnLinkMicClickListener;

    private OnLinkMicNickNameClickListener mOnLinkMicNickNameClickListener;

    public void setOnLinkMicHangupClickListener(OnLinkMicHangupClickListener mOnLinkMicClickListener) {
        this.mOnLinkMicClickListener = mOnLinkMicClickListener;
    }

    public void setOnLinkMicNickNameClickListener(OnLinkMicNickNameClickListener mOnLinkMicNickNameClickListener) {
        this.mOnLinkMicNickNameClickListener = mOnLinkMicNickNameClickListener;
    }

    public interface OnLinkMicHangupClickListener {

        void onHangup(String toUserId);

    }

    public interface OnLinkMicNickNameClickListener {

        void onClickNickName(String toUserId);

    }

}
