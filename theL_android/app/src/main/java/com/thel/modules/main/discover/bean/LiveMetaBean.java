package com.thel.modules.main.discover.bean;

import com.thel.bean.user.SimpleUserBean;
import com.thel.constants.TheLConstants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by waiarl on 2017/11/7.
 */

public class LiveMetaBean {
    public String topLink = TheLConstants.LIVE_RANKING_LIST_URL;

    public List<SimpleUserBean> top = new ArrayList<>();
}
