package com.thel.modules.live.bean;

/**
 * Created by the L on 2016/6/1.
 */

import java.util.ArrayList;
import java.util.List;

/**
 * softGiftMsgLayout 要显示的消息的队列集合
 * 根据是否是同一礼物连击分类
 */
public class GiftMsgShowListBean {

    public int type = 0;

    public boolean isMultiConnectMic = false;

    /**
     * 是否是正在显示
     */
    private boolean isShow = false;//

    /**
     * 要显示（整在显示的消息）集合
     */
    public List<GiftSendMsgBean> msgList = new ArrayList<>();
    /**
     * 赠送的礼物id
     */
    public int id = 0;//赠送的礼物id;
    /**
     * 赠送的人的Id
     */
    public String userId = "";//赠送的人的Id
    /**
     * 连击次数
     */
    public int combo = 0;//连击次数
    /**
     * 把消息添加到集合中
     * @param msgbean 要添加的消息
     */
    /**
     * 曾送礼物的人昵称
     */
    public String nickName;
    /**
     * 赠送礼物的人头像
     */
    public String avatar;
    /**
     * 赠送礼物的图标
     */
    public String icon;
    /**
     * 礼物图片
     */
    public String img;
    /**
     *
     */
    public String action;
    /**
     * 礼物名字
     */
    public String giftName;
    /**
     * 4.6.0新增用户等级
     */
    public int userLevel;

    /**
     * 相遇时 被赠送者名字
     */
    public String toNickname;

    /**
     * 相遇时 被赠送者头像
     */
    public String toAvatar;

    /**
     * 相遇时 被赠送者用户id
     */
    public String toUserId;

    private DataAddedCallBack callBack;//新增数据时候的回调，当数据显示完（被移除完）又新增的时候使用
    /**--------------------------------------一下为方法-----------------------------**/

    /**
     * 添加消息到队列，并初始化一些基本属性
     *
     * @param msgbean
     */
    public void addGiftMsg(GiftSendMsgBean msgbean) {
        if (msgList.size() == 0 && !isShow) {//队列不为空切不是显示情况的时候，说明是新集合，
            this.id = msgbean.id;
            this.userId = msgbean.userId;
            this.avatar = msgbean.avatar;
            this.nickName = msgbean.nickName;
            this.icon = msgbean.icon;
            this.img = msgbean.img;
            this.action = msgbean.action;
            this.giftName = msgbean.giftName;
            this.userLevel = msgbean.userLevel;
            this.toNickname = msgbean.toNickname;
            this.toAvatar = msgbean.toAvatar;
            this.toUserId = msgbean.toUserId;
            this.type = msgbean.type;
            this.isMultiConnectMic = msgbean.isMultiConnectMic;
        }
        msgList.add(msgbean);
        combo = msgbean.combo;
        if (msgList.size() == 1 && isShow) {//正在显示，说明不是新集合
            if (callBack != null) {
                callBack.remindDataAdded();
            }
        }
    }

    /**
     * 如果一个消息已经显示或者正在显示，则从消息队里顶部（第一个移除）
     */
    public void removeTop() {
        if (msgList.size() > 0) {//如果消息数大于0
            msgList.remove(0);//移除队顶消息
        }
    }

    /**
     * 设置是否正在显示
     *
     * @param show 是否正在显示
     */
    public void setShow(boolean show) {
        isShow = show;
    }

    /**
     * 获取是否显示状态
     *
     * @return true，正在显示，false,不是
     */
    public boolean isShow() {
        return isShow;
    }

    public interface DataAddedCallBack {
        void remindDataAdded();//添加了新的数据
    }

    public void setDataAddedListener(DataAddedCallBack callBack) {
        this.callBack = callBack;
    }
}
