package com.thel.modules.live.bean;

import com.thel.base.BaseDataBean;

/**
 * Created by waiarl on 2017/11/29.
 * 直播Pk好友列表网络请求数据
 */

public class LivePkFriendListNetBean extends BaseDataBean {
    public LivePkFriendListBean data;
}
