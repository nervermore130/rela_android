package com.thel.modules.live.surface.watch;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.Message;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.postprocessors.IterativeBoxBlurPostProcessor;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.pili.pldroid.player.AVOptions;
import com.pili.pldroid.player.PLOnAudioFrameListener;
import com.pili.pldroid.player.PLOnCompletionListener;
import com.pili.pldroid.player.PLOnErrorListener;
import com.pili.pldroid.player.PLOnImageCapturedListener;
import com.pili.pldroid.player.PLOnInfoListener;
import com.pili.pldroid.player.PLOnPreparedListener;
import com.pili.pldroid.player.PLOnSeekCompleteListener;
import com.pili.pldroid.player.PLOnVideoFrameListener;
import com.pili.pldroid.player.widget.PLVideoTextureView;
import com.thel.BuildConfig;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseFragment;
import com.thel.bean.LiveInfoLogBean;
import com.thel.bean.live.LinkMicResponseBean;
import com.thel.constants.TheLConstants;
import com.thel.manager.CDNBalanceManager;
import com.thel.modules.live.agora.OnJoinChannelListener;
import com.thel.modules.live.agora.RtcEngineHandler;
import com.thel.modules.live.bean.AgoraBean;
import com.thel.modules.live.bean.AudienceLinkMicResponseBean;
import com.thel.modules.live.bean.LiveRoomBean;
import com.thel.modules.live.in.LiveShowAudienceIn;
import com.thel.modules.live.in.LiveShowCtrlListener;
import com.thel.modules.live.in.LiveShowVideoIn;
import com.thel.modules.live.in.LiveSoundViewIn;
import com.thel.modules.live.in.VideoPreparedListener;
import com.thel.modules.live.utils.LiveBroadcastImpl;
import com.thel.modules.live.utils.LiveUtils;
import com.thel.modules.live.view.SoundSurfaceView;
import com.thel.utils.AbstractVideoCDNChange;
import com.thel.utils.AppInit;
import com.thel.utils.DeviceUtils;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.ImageUtils;
import com.thel.utils.L;
import com.thel.utils.NetworkUtils;
import com.thel.utils.PhoneUtils;
import com.thel.utils.ScreenUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.UmentPushUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;
import com.umeng.analytics.MobclickAgent;

import java.io.File;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import io.agora.rtc.IRtcEngineEventHandler;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_HIDE_PREVIEW_IMAGE;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_LIVE_COMPELETE;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_LIVE_ERROR;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_SHOW_PROGRESSBAR;

/**
 * @author waiarl
 * @date 2017/11/2
 */

public class LiveWatchKsyStreamFragment extends BaseFragment implements LiveShowCtrlListener, LiveShowVideoIn, LiveShowAudienceIn, LiveBroadcastImpl.LiveShowBroadcastListener {
    private final String TAG = LiveWatchKsyStreamFragment.class.getSimpleName();
    private LiveRoomBean liveRoomBean;
    private LiveWatchObserver observer;
    private LiveBroadcastImpl liveBroadcastImpl;
    private RelativeLayout root_rl;
    private VideoPreparedListener mPrepareListener;
    //是否被创建
    private boolean isCreate = false;
    private SimpleDraweeView img_bg;
    private LiveSoundViewIn sound_view;
    private long startTime = 0;
    private boolean isPrepared = false;
    private RtcEngineHandler mRtcEngineHandler;
    private SurfaceView mRemoteView;

    private ImageView img_background;

    private ImageView img_background_top;

    private GLSurfaceView glSurface;

    private PLVideoTextureView mVideoView;

    private CompositeDisposable mCompositeDisposable;

    public static LiveWatchKsyStreamFragment getInstance(Bundle bundle) {
        LiveWatchKsyStreamFragment instance = new LiveWatchKsyStreamFragment();
        instance.setArguments(bundle);
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        liveBroadcastImpl = new LiveBroadcastImpl();
        liveBroadcastImpl.registReceiver(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_live_show_video_ks, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        L.d(TAG, "-----------onViewCreated------------");

        root_rl = view.findViewById(R.id.root_rl);
        img_background = view.findViewById(R.id.img_background);
        img_background_top = view.findViewById(R.id.img_background_top);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        isCreate = true;
        if (observer != null) {
            observer.bindVideo(this);
            observer.bindStreamAudience(this);
        }
    }

    private void initTexture(String playUrl) {

        L.d(TAG, " initTexture playUrl : " + playUrl);

        if (getContext() != null) {
            if (root_rl != null) {
                if (mVideoView != null) {
                    root_rl.removeView(mVideoView);
                    mVideoView = null;
                }
                mVideoView = new PLVideoTextureView(getContext());
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
                root_rl.addView(mVideoView, layoutParams);
                initPlayer(playUrl);
                setListener();
            }
        }
    }

    private void initPlayer(String playUrl) {

        try {
            AVOptions options = new AVOptions();
            options.setInteger(AVOptions.KEY_PREPARE_TIMEOUT, 10 * 1000);
            options.setInteger(AVOptions.KEY_LIVE_STREAMING, 1);
            options.setInteger(AVOptions.KEY_MEDIACODEC, AVOptions.MEDIA_CODEC_HW_DECODE);
            options.setInteger(AVOptions.KEY_LOG_LEVEL, 0);
            options.setInteger(AVOptions.KEY_VIDEO_DATA_CALLBACK, 1);
            options.setInteger(AVOptions.KEY_AUDIO_DATA_CALLBACK, 1);
            options.setInteger(AVOptions.KEY_FAST_OPEN, 1);
            options.setInteger(AVOptions.KEY_CACHE_BUFFER_DURATION, 500);
            options.setInteger(AVOptions.KEY_MAX_CACHE_BUFFER_DURATION, 4000);
            options.setString(AVOptions.KEY_SDK_ID, UserUtils.getMyUserId());
            mVideoView.setAVOptions(options);
            mVideoView.setVideoPath(playUrl);
            mVideoView.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setListener() {

        mVideoView.setOnInfoListener(mOnInfoListener);
        mVideoView.setOnPreparedListener(mPLOnPreparedListener);
        mVideoView.setOnSeekCompleteListener(mPLOnSeekCompleteListener);
        mVideoView.setOnErrorListener(mPLOnErrorListener);
        mVideoView.setOnCompletionListener(mPLOnCompletionListener);
        mVideoView.setOnImageCapturedListener(mPLOnImageCapturedListener);
        mVideoView.setOnVideoFrameListener(mPLOnVideoFrameListener);
        if (LiveRoomBean.TYPE_VOICE == liveRoomBean.audioType && liveRoomBean.isMulti != LiveRoomBean.MULTI_LIVE) {
            mVideoView.setOnAudioFrameListener(mPLOnAudioFrameListener);
        } else {
            mVideoView.setOnAudioFrameListener(null);
        }

    }

    @Override
    public void initLiveShow() {

        L.d(TAG, "-----------initLiveShow------------");

        if (mRtcEngineHandler != null) {

            mRtcEngineHandler.leaveChannel();

            mRtcEngineHandler.cameraDestroy();

            mRtcEngineHandler = null;

        }

        if (mVideoView != null && root_rl != null) {
            mVideoView.setOnAudioFrameListener(null);
            mVideoView.stopPlayback();
            root_rl.removeView(mVideoView);
            mVideoView = null;
        }
        if (sound_view != null) {
            sound_view.initView(0);
            sound_view.stop();
        }

        sound_view = null;
        img_bg = null;
        lastDrawTime = 0;
    }

    @Override
    public void refreshLiveShow(LiveRoomBean liveRoomBean) {

        UmentPushUtils.onMsgEvent(TheLApp.context, "audience_first_play");

        L.d(TAG, "-----------refreshLiveShow------------");

        this.liveRoomBean = liveRoomBean;

        if (liveRoomBean == null) {
            return;
        }

        String playUrl = getPlayUrl();

        if (mVideoView == null) {
            initTexture(playUrl);
        }

        /***如果是声音直播***/
        if (LiveRoomBean.TYPE_VOICE == liveRoomBean.audioType && liveRoomBean.isMulti != LiveRoomBean.MULTI_LIVE) {
            initSoundView();
        }
        uploadWatchTimeToGIO();

        initLiveInfoLog(playUrl);

        initFPSTimer();

    }

    String playUrl;

    private String getPlayUrl() {

        try {

            if (liveRoomBean.audioType == LiveRoomBean.TYPE_VOICE && liveRoomBean.isMulti == LiveRoomBean.MULTI_LIVE) {

                playUrl = CDNBalanceManager.getInstance().getFastMultiLinkMicVideoUrl(String.valueOf(liveRoomBean.user.id));

                if (playUrl == null) {
                    playUrl = liveRoomBean.liveUrl;
                }

            } else {

                if (liveRoomBean.forceServerLiveUrl == 0) {

                    playUrl = CDNBalanceManager.getInstance().getFastVideoUrl(String.valueOf(liveRoomBean.user.id));

                    if (playUrl == null) {
                        playUrl = liveRoomBean.liveUrl;
                    }
                } else {
                    playUrl = liveRoomBean.liveUrl;
                }


            }

            return playUrl;
        } catch (Exception e) {
            e.printStackTrace();

            return liveRoomBean.liveUrl;
        }

    }

    private void initSoundView() {

        L.d(TAG, " initSoundView 加载图片和遮罩 img_bg " + img_bg);

        if (img_bg == null && root_rl != null) {
            img_bg = new SimpleDraweeView(TheLApp.context);
            root_rl.addView(img_bg, RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            setSimpleBlur(img_bg, liveRoomBean.imageUrl, AppInit.displayMetrics.widthPixels, AppInit.displayMetrics.heightPixels);
            ImageView imageView = new ImageView(TheLApp.context);
            imageView.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
            imageView.setImageResource(R.color.black_transparent_50);
            root_rl.addView(imageView);
        }
        if (sound_view == null && root_rl != null) {
            sound_view = new SoundSurfaceView(TheLApp.context);
            root_rl.addView(sound_view.getView(), RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            sound_view.start();
        }
        mVideoView.setOnAudioFrameListener(mPLOnAudioFrameListener);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return false;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return false;
    }

    @Override
    public boolean showLiveView(boolean show) {
        return show;
    }

    @Override
    public void VideoPrepared() {

    }

    @Override
    public void onDestroy() {
        liveBroadcastImpl.unRegisterReceiver(this);
        isCreate = false;
        if (mCompositeDisposable != null) {
            mCompositeDisposable.clear();
        }
        uploadWatchTimeToGIO();

        destory();
        super.onDestroy();
    }

    @Override
    public boolean destory() {
        if (mVideoView != null) {
            //释放播放器
            mVideoView.stopPlayback();
        }
        if (sound_view != null) {
            sound_view.destory();
        }

        if (root_rl != null) {
            root_rl.removeAllViews();
        }

        if (mRtcEngineHandler != null) {
            mRtcEngineHandler.destroy();
        }

        glSurface = null;

        return true;
    }


    @Override
    public void onResume() {
        super.onResume();
        resume();
    }

    @Override
    public boolean resume() {
        if (mVideoView != null) {
            mVideoView.start();
        }
        return true;
    }

    @Override
    public void onPause() {
        super.onPause();
        pause();
        if (mRtcEngineHandler != null) {
            mRtcEngineHandler.pause();
        }
    }

    @Override
    public boolean pause() {

        if (mVideoView != null) {
            mVideoView.pause();
        }
        return true;
    }


    @Override
    public void bindObserver(LiveWatchObserver observer) {
        this.observer = observer;
        if (isCreate) {
            observer.bindVideo(this);
            observer.bindStreamAudience(this);
        }
    }

    @Override
    public void liveClosed() {
        if (sound_view != null) {
            sound_view.stop();
        }
    }

    @Override
    public void setClientRole(int role, AgoraBean agoraBean) {

    }

    @Override
    public void muteLocalAudioStream(boolean mute) {

    }

    @Override
    public void autoOpenLocalAudioStream() {

    }

    @Override
    public void autoCloseLocalAudioStream() {

    }

    @Override
    public void levelChannel() {

    }

    @Override
    public String getCurrentPlayUrl() {
        if (playUrl == null) {
            if (liveRoomBean != null) {
                return liveRoomBean.liveUrl;
            } else {
                return "";
            }
        } else {
            return playUrl;
        }
    }

    @Override
    public String getScreenShotImagePath() {

        if (mVideoView != null) {

            final Bitmap bitmap = mVideoView.getTextureView().getBitmap();

            if (bitmap == null) {
                return null;
            }
            final File dir = new File(TheLConstants.F_TAKE_PHOTO_ROOTPATH);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            final String imagePath = TheLConstants.F_TAKE_PHOTO_ROOTPATH + System.currentTimeMillis() + TheLConstants.REPORT_LIVESHOW_IMG_END + ".jpg";

            L.d(TAG, " imagePath : " + imagePath);

            boolean isSuccess = ImageUtils.savePic(bitmap, imagePath, TheLConstants.PIC_QUALITY);

            if (isSuccess) {
                return imagePath;
            }
        }
        return null;
    }

    @Override
    public void linkMicStop() {

        L.d(TAG, "-----------linkMicStop------------");

        if (root_rl != null) {

//            if (mCameraView != null) {
//
//                mCameraView.onDestroy();
//
//                mCameraView = null;
//
//            }

            if (mRemoteView != null) {

                root_rl.removeView(mRemoteView);

                mRemoteView = null;

            }

            if (glSurface != null) {

                root_rl.removeView(glSurface);

                glSurface = null;

            }

            root_rl.setBackgroundResource(R.color.transparent);

        }

        if (mRtcEngineHandler != null) {
            mRtcEngineHandler.stopMediaStreaming();
        }


        if (mRtcEngineHandler != null) {

            mRtcEngineHandler.leaveChannel();

            mRtcEngineHandler = null;

        }

        if (root_rl != null) {
            root_rl.postDelayed(new Runnable() {
                @Override
                public void run() {

                    String playUrl = getPlayUrl();

                    initTexture(playUrl);

                }
            }, 500);
        }


    }

    @Override
    public void linkMicStart(LinkMicResponseBean linkMicResponseBean) {

    }

    @Override
    public void audienceLinkMicSuccess(AudienceLinkMicResponseBean audienceLinkMicResponseBean) {

        L.d(TAG, " audienceLinkMicResponseBean : " + audienceLinkMicResponseBean.toString());

        if (mVideoView != null && root_rl != null) {
            mVideoView.setOnAudioFrameListener(null);
            mVideoView.stopPlayback();
            root_rl.removeView(mVideoView);
            mVideoView = null;

            if (img_bg == null && root_rl != null) {
                img_bg = new SimpleDraweeView(TheLApp.context);
                root_rl.addView(img_bg, RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
                setSimpleBlur(img_bg, liveRoomBean.imageUrl, AppInit.displayMetrics.widthPixels, AppInit.displayMetrics.heightPixels);
                ImageView imageView = new ImageView(TheLApp.context);
                imageView.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
                imageView.setImageResource(R.color.black_transparent_50);
                root_rl.addView(imageView);
            }
        }

        if (audienceLinkMicResponseBean.agora == null || audienceLinkMicResponseBean.agora.appId == null) {
            return;
        }

        mRtcEngineHandler = new RtcEngineHandler(getActivity(), RtcEngineHandler.LIVE_TYPE_VIDEO, audienceLinkMicResponseBean.agora.appId);

        mRtcEngineHandler.setType(RtcEngineHandler.TYPE_AUDIENCE_LINK_MIC);

        mRtcEngineHandler.setOnJoinChannelSuccessListener(mOnJoinChannelSuccessListener);

        int uid = Integer.valueOf(UserUtils.getMyUserId());

        String liveId = liveRoomBean != null ? liveRoomBean.liveId : "";

        glSurface = new GLSurfaceView(getActivity());

        root_rl.addView(glSurface);

        mRtcEngineHandler.init(audienceLinkMicResponseBean.agora.token, audienceLinkMicResponseBean.agora.channelId, null, uid, liveId, glSurface);

        LiveInfoLogBean.getInstance().getLivePlayAnalytics().channelId = audienceLinkMicResponseBean.agora.channelId;

    }

    @Override
    public void hangup() {

    }

    @Override
    public void hangupByOwn() {

    }

    private OnJoinChannelListener mOnJoinChannelSuccessListener = new OnJoinChannelListener() {
        @Override
        public void onJoinChannelSuccess() {


        }

        @Override
        public void onLeaveChannelSuccess() {

        }

        @Override
        public void onUserJoined(int uid, SurfaceView view) {

            mRemoteView = view;

            view.setZOrderOnTop(false);

            view.setZOrderMediaOverlay(false);

            final float[] pkParams = LiveUtils.getPkParams(ScreenUtils.getScreenWidth(TheLApp.context), ScreenUtils.getScreenHeight(TheLApp.context));

            final int x = (int) pkParams[0];

            final int y = (int) pkParams[1];

            final int width = (int) pkParams[2];

            final int height = (int) pkParams[3];

            L.d(TAG, " setPKSize x : " + x);

            L.d(TAG, " setPKSize y : " + y);

            L.d(TAG, " setPKSize width : " + width);

            L.d(TAG, " setPKSize height : " + height);

            RelativeLayout.LayoutParams remoteViewParams = new RelativeLayout.LayoutParams(width, height);

            remoteViewParams.leftMargin = 0;

            remoteViewParams.topMargin = y;

            RelativeLayout.LayoutParams cameraViewParams = new RelativeLayout.LayoutParams(width, height);

            cameraViewParams.leftMargin = x;

            cameraViewParams.topMargin = y;

            if (glSurface != null) {
                glSurface.setLayoutParams(cameraViewParams);
            }

            mRemoteView.setLayoutParams(remoteViewParams);

            root_rl.addView(mRemoteView);

        }

        @Override
        public void onUserOffline(int uid) {

        }

        @Override
        public void onAudioVolumeIndication(IRtcEngineEventHandler.AudioVolumeInfo[] speakers, int totalVolume) {

        }

        @Override
        public void onError(int error) {
            switch (error) {
                case IRtcEngineEventHandler.ErrorCode.ERR_FAILED:
                    break;
                case IRtcEngineEventHandler.ErrorCode.ERR_ADM_GENERAL_ERROR:
                case IRtcEngineEventHandler.ErrorCode.ERR_ADM_RUNTIME_PLAYOUT_ERROR:
                case IRtcEngineEventHandler.ErrorCode.ERR_ADM_RUNTIME_RECORDING_ERROR:
                    break;
                default:
                    break;

            }

        }
    };

    private void showProgressBar(boolean show) {
        final Message msg = Message.obtain();
        msg.what = UI_EVENT_SHOW_PROGRESSBAR;
        msg.obj = show;
        observer.sendMessage(msg);
        if (sound_view != null && show) {
            sound_view.initView(0);
        }
    }


    /***********************************************一下为广播方法*************************************************/
    @Override
    public void pauseLiveShow() {
        pause();
    }

    @Override
    public void resumeLiveShow() {
        resume();
    }

    @Override
    public void wxShareSuccess() {
        MobclickAgent.onEvent(getActivity(), "share_succeeded");

    }

    @Override
    public void setMulte(boolean mulite) {
        if (mVideoView != null) {
            if (mulite) {
                mVideoView.setVolume(0f, 0f);
            } else {
                mVideoView.setVolume(1f, 1f);
            }
        }
    }

    /*****************************************以上为广播方法,一下为监听内部类*******************************************************/

    private PLOnInfoListener mOnInfoListener = new PLOnInfoListener() {
        @Override
        public void onInfo(int what, int extra) {

            switch (what) {
                case PLOnInfoListener.MEDIA_INFO_BUFFERING_START:
                    showProgressBar(false);
                    break;
                case PLOnInfoListener.MEDIA_INFO_BUFFERING_END:
                    showProgressBar(false);
                    break;
                case PLOnInfoListener.MEDIA_INFO_VIDEO_RENDERING_START:
                    showProgressBar(false);
                    break;
                case PLOnInfoListener.MEDIA_INFO_AUDIO_RENDERING_START:
                    showProgressBar(false);
                    break;
                case PLOnInfoListener.MEDIA_INFO_VIDEO_FRAME_RENDERING:
                    break;
                case PLOnInfoListener.MEDIA_INFO_AUDIO_FRAME_RENDERING:
                    break;
                case PLOnInfoListener.MEDIA_INFO_VIDEO_GOP_TIME:
                    break;
                case PLOnInfoListener.MEDIA_INFO_SWITCHING_SW_DECODE:
                    break;
                case PLOnInfoListener.MEDIA_INFO_METADATA:
                    break;
                case PLOnInfoListener.MEDIA_INFO_VIDEO_BITRATE:
                    break;
                case PLOnInfoListener.MEDIA_INFO_VIDEO_FPS:

                    if (mVideoView != null) {

                        L.d(TAG, " mVideoView.getVideoFps() : " + mVideoView.getVideoFps());

//                        if (mVideoView.getVideoFps() < 10) {
//                            if (mAbstractVideoCDNChange != null) {
//                                mAbstractVideoCDNChange.videoBufferStart();
//                                mAbstractVideoCDNChange.videoBufferEnd();
//                            }
//                        }
                    }

                    break;
                case PLOnInfoListener.MEDIA_INFO_CONNECTED:
                    break;
                case PLOnInfoListener.MEDIA_INFO_VIDEO_ROTATION_CHANGED:
                    break;
                default:
                    break;
            }
        }
    };

    private PLOnPreparedListener mPLOnPreparedListener = new PLOnPreparedListener() {

        @Override
        public void onPrepared(int i) {

            L.d(TAG, " onPrepared i : " + i);

            if (mPrepareListener != null) {
                mPrepareListener.prepared();
            }

            if (mVideoView != null) {

                mVideoView.setDisplayAspectRatio(PLVideoTextureView.ASPECT_RATIO_PAVED_PARENT);

                mVideoView.start();
            }

            isPrepared = true;

            startTime = System.currentTimeMillis();

            observer.sendEmptyMessage(UI_EVENT_HIDE_PREVIEW_IMAGE);
        }
    };

    private PLOnSeekCompleteListener mPLOnSeekCompleteListener = new PLOnSeekCompleteListener() {
        @Override
        public void onSeekComplete() {
            observer.sendEmptyMessage(UI_EVENT_LIVE_COMPELETE);
            L.i(TAG, "reload..." + ",position=" + liveRoomBean.id);
            if (!TextUtils.isEmpty(liveRoomBean.liveUrl) && mVideoView != null) {
//                mKSYMediaPlayer.reload(liveRoomBean.liveUrl, false);
                L.i(TAG, "onCompletion:url=" + liveRoomBean.liveUrl + ",position=" + liveRoomBean.id);
            }
        }
    };

    private PLOnErrorListener mPLOnErrorListener = new PLOnErrorListener() {
        @Override
        public boolean onError(int errorCode) {

            L.d(TAG, " onError " + errorCode);

            switch (errorCode) {
                case PLOnErrorListener.ERROR_CODE_IO_ERROR:
                case PLOnErrorListener.ERROR_CODE_OPEN_FAILED:

//                    CDNBalanceManager.getInstance().setNetworkChanged();

                    if (mVideoView != null) {
                        mVideoView.setVideoPath(getPlayUrl());
                    }

                    break;
                case PLOnErrorListener.ERROR_CODE_SEEK_FAILED:
                    break;
                default:
                    observer.obtainMessage(UI_EVENT_LIVE_ERROR, errorCode).sendToTarget();
                    break;
            }
            return true;
        }
    };

    private PLOnVideoFrameListener mPLOnVideoFrameListener = new PLOnVideoFrameListener() {
        @Override
        public void onVideoFrameAvailable(byte[] data, int size, int width, int height, int format, long ts) {
            if (format == PLOnVideoFrameListener.VIDEO_FORMAT_SEI) {

                String hexString = bytesToHexString(Arrays.copyOfRange(data, 19, 23));

                String tsString = bytesToHexString(Arrays.copyOfRange(data, 23, 31));

            }
        }
    };

    /**
     * Convert byte[] to hex string
     *
     * @param src byte[] data
     * @return hex string
     */
    public static String bytesToHexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder();
        if (src == null || src.length <= 0) {
            return null;
        }
        for (int i = 0; i < src.length; i++) {
            int v = src[i] & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
        }
        return stringBuilder.toString();
    }

    private PLOnAudioFrameListener mPLOnAudioFrameListener = new PLOnAudioFrameListener() {
        @Override
        public void onAudioFrameAvailable(byte[] bytes, int i, int i1, int i2, int i3, long l) {
            showSoundView(bytes);
        }
    };

    private PLOnCompletionListener mPLOnCompletionListener = new PLOnCompletionListener() {
        @Override
        public void onCompletion() {
            isPrepared = false;
        }
    };

    private PLOnImageCapturedListener mPLOnImageCapturedListener = new PLOnImageCapturedListener() {
        @Override
        public void onImageCaptured(byte[] bytes) {

            L.d(TAG, " onImageCaptured bytes : " + Arrays.toString(bytes));

            Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);

            L.d(TAG, " onImageCaptured bitmap : " + bitmap);

            final File dir = new File(TheLConstants.F_TAKE_PHOTO_ROOTPATH);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            final String imagePath = TheLConstants.F_TAKE_PHOTO_ROOTPATH + System.currentTimeMillis() + TheLConstants.REPORT_LIVESHOW_IMG_END + ".jpg";

            boolean isSuccess = ImageUtils.savePic(bitmap, imagePath, TheLConstants.PIC_QUALITY);

            if (isSuccess) {

            }
        }
    };

    /**
     * 上传观看直播时长到GrowingIO
     */
    private void uploadWatchTimeToGIO() {
        long endTime = System.currentTimeMillis();

        long watchTime = endTime - startTime;

        if (startTime > 0) {
            if (watchTime > 0) {
                watchTime = watchTime / 1000;
            } else {
                watchTime = 1;
            }
        } else {
            watchTime = 1;
        }

        if (watchTime >= 120) {
            GrowingIOUtil.uploadWatchTimeLonger2M("liveWatchTimeLonger2M");
        }

        if (liveRoomBean != null) {
            if (LiveRoomBean.TYPE_VOICE == liveRoomBean.audioType) {
                if (LiveRoomBean.MULTI_LIVE == liveRoomBean.isMulti) {
                    GrowingIOUtil.uploadLivePageView(2);
                    //4.10.0上传时长有问题 改成新的方式
                    GrowingIOUtil.liveWatchTimeTrack(2, watchTime);

                } else {
                    GrowingIOUtil.uploadLivePageView(1);
                    //4.10.0上传时长有问题 改成新的方式
                    GrowingIOUtil.liveWatchTimeTrack(1, watchTime);

                }
            } else {
                GrowingIOUtil.uploadLivePageView(LiveRoomBean.TYPE_VIDEO);
                //4.10.0上传时长有问题 改成新的方式
                GrowingIOUtil.liveWatchTimeTrack(LiveRoomBean.TYPE_VIDEO, watchTime);


            }
        }
    }

    long lastDrawTime = 0;
    long drawDivider = 1000 / 40;

    private void showSoundView(ByteBuffer byteBuffer) {
        if (sound_view == null) {
            return;
        }
        if (System.currentTimeMillis() - lastDrawTime < drawDivider) {
            return;
        }
        lastDrawTime = System.currentTimeMillis();
        // final byte[] bt = new byte[BUFFER_SIZE];
        //byteBuffer.get(bt);
        final byte[] bt = new byte[byteBuffer.remaining()];
        byteBuffer.get(bt);
        //final byte[] bt = byteBuffer.array();
        final short[] shorts = Utils.toShortArray(bt);
        final int length = shorts.length;
        long summer = 0;
        for (int i = 0; i < length; i++) {
            final short ct = shorts[i];
            summer += ct * ct;
        }
        final float mean = summer / (float) length;
        final float volume = (float) (Math.sqrt(mean) / 30f);
        L.i(TAG, "volume=" + volume);
        sound_view.initView(volume);
    }

    private void showSoundView(byte[] bt) {
        if (sound_view == null) {
            return;
        }
        if (System.currentTimeMillis() - lastDrawTime < drawDivider) {
            return;
        }
        lastDrawTime = System.currentTimeMillis();

        final short[] shorts = Utils.toShortArray(bt);
        final int length = shorts.length;
        long summer = 0;
        for (int i = 0; i < length; i++) {
            final short ct = shorts[i];
            summer += ct * ct;
        }
        final float mean = summer / (float) length;
        final float volume = (float) (Math.sqrt(mean) / 30f);
        L.i(TAG, "volume=" + volume);
        sound_view.initView(volume);
    }

    @Override
    public void setVideoPreparedListener(VideoPreparedListener preparedListener) {
        this.mPrepareListener = preparedListener;
    }

    private void setSimpleBlur(SimpleDraweeView simpleDraweeView, String url, int width, int height) {
        simpleDraweeView.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(url, 100, 100))).setPostprocessor(new IterativeBoxBlurPostProcessor(24, 24)).build()).setAutoPlayAnimations(true).build());

    }

    private AbstractVideoCDNChange mAbstractVideoCDNChange = new AbstractVideoCDNChange() {
        @Override
        public void videoCDNChange() {
            if (mVideoView != null) {

                CDNBalanceManager.getInstance().setNetworkChanged();

                try {

                    String playUrl = getPlayUrl();

                    L.d(TAG, " AbstractVideoCDNChange playUrl : " + playUrl);

                    LiveInfoLogBean.getInstance().getLivePlayAnalytics().outUrl = LiveInfoLogBean.getInstance().getLivePlayAnalytics().inUrl;

                    LiveInfoLogBean.getInstance().getLivePlayAnalytics().inUrl = playUrl;

                    mVideoView.setVideoPath(playUrl);

                    mVideoView.start();

                    setLiveInfoLog(playUrl);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    };

    private void initLiveInfoLog(String playUrl) {

        LiveInfoLogBean.getInstance().clearLiveMsgLog();

        LiveInfoLogBean.getInstance().getLiveChatAnalytics().time = LiveUtils.getLiveTime();
        if (liveRoomBean.user != null) {
            LiveInfoLogBean.getInstance().getLiveChatAnalytics().liveUserId = String.valueOf(liveRoomBean.user.id);
        }
        LiveInfoLogBean.getInstance().getLiveChatAnalytics().roomId = liveRoomBean.liveId;
        LiveInfoLogBean.getInstance().getLiveChatAnalytics().relaVersion = BuildConfig.VERSION_NAME;
        LiveInfoLogBean.getInstance().getLiveChatAnalytics().network = NetworkUtils.getAPNType();

        LiveInfoLogBean.getInstance().getLivePlayAnalytics().time = LiveUtils.getLiveTime();
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().userId = Utils.getMyUserId();
        if (liveRoomBean.user != null) {
            LiveInfoLogBean.getInstance().getLivePlayAnalytics().liveUserId = String.valueOf(liveRoomBean.user.id);
        }
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().cdnDomain = playUrl;
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().inUrl = playUrl;
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().outUrl = null;
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().logType = TheLConstants.LiveInfoLogConstants.TYPE_FIRST_PLAY;
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().roomId = liveRoomBean.liveId;
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().relaVersion = BuildConfig.VERSION_NAME;
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().network = NetworkUtils.getAPNType();
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().liveUrlSort = ShareFileUtils.getInt(ShareFileUtils.LIVE_URL_SORT, -1);
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().forceServerLiveUrl = liveRoomBean.forceServerLiveUrl;
        LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getLivePlayAnalytics());

        LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().relaVersion = BuildConfig.VERSION_NAME;
        if (liveRoomBean.user != null) {
            LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().liveUserId = String.valueOf(liveRoomBean.user.id);
        }
        LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().time = LiveUtils.getLiveTime();
        LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().cSystem = DeviceUtils.getModel() + " ;" + "Android " + PhoneUtils.getOSVersion();

        LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().roomId = liveRoomBean.liveId;
        LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().network = NetworkUtils.getAPNType();
        LiveInfoLogBean.getInstance().getLiveMsgLogAnalytics().logType = "liveMsgLog";
    }

    private void setLiveInfoLog(String playUrl) {
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().outUrl = LiveInfoLogBean.getInstance().getLivePlayAnalytics().inUrl;
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().inUrl = playUrl;
        LiveInfoLogBean.getInstance().getLivePlayAnalytics().logType = TheLConstants.LiveInfoLogConstants.TYPE_PLAY_CHANGE_LOG;
        LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getLivePlayAnalytics());
    }

    private void initFPSTimer() {

        if (mCompositeDisposable == null) {
            mCompositeDisposable = new CompositeDisposable();
        }

        mCompositeDisposable.clear();

        Disposable disposable = Flowable.interval(0, 1000, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Long>() {
            @Override
            public void accept(Long aLong) throws Exception {
                if (mVideoView != null) {
                    int fps = mVideoView.getVideoFps();
                    if (fps < 10 && fps > 0) {
                        mAbstractVideoCDNChange.countLowSpeed();
                    }
                }
            }
        });

        mCompositeDisposable.add(disposable);

    }

}
