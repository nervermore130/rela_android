package com.thel.modules.live.view;

import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.modules.live.bean.SoftEnjoyBean;
import com.thel.modules.live.bean.SoftGiftBean;
import com.thel.modules.live.in.LiveBaseView;
import com.thel.utils.Utils;

/**
 * Created by waiarl on 2017/12/22.
 */

public class LiveGiftGridViewPup extends PopupWindow implements LiveBaseView<LiveGiftGridViewPup> {
    private final Context mContext;
    private SoftGiftListView softGiftListView;

    private final int PORTAINT_HEIGHT = Utils.dip2px(TheLApp.getContext(), 210);//竖屏高度
    private final int LANDSCAPE_HEIGHT = Utils.dip2px(TheLApp.getContext(), 110);//横屏高度

    public LiveGiftGridViewPup(Context context) {
        this(context, null);
    }

    public LiveGiftGridViewPup(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LiveGiftGridViewPup(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init(attrs, defStyleAttr);
    }

    private void init(AttributeSet attrs, int defStyleAttr) {
        softGiftListView = new SoftGiftListView(mContext);
        setContentView(softGiftListView);
        setOutsideTouchable(true);
        setFocusable(true);
        setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.color.transparent));
        setAnimationStyle(R.style.anim_bottom_in_out);
        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    public LiveGiftGridViewPup initView(SoftEnjoyBean softEnjoyBean, int... orientation) {
        softGiftListView.initView(softEnjoyBean);
        if (orientation.length > 0 && orientation[0] == SoftGiftListView.SCREEN_LANDSCAPE) {
            softGiftListView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, LANDSCAPE_HEIGHT));
            setHeight(LANDSCAPE_HEIGHT);

        } else {
            softGiftListView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, PORTAINT_HEIGHT));
            setHeight(PORTAINT_HEIGHT);
        }

        return this;
    }

    public LiveGiftGridViewPup show(View view) {
        showAtLocation(view, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
        this.show();
        return this;
    }

    public LiveGiftGridViewPup setOrientation(int orientation) {
        softGiftListView.setOrientation(orientation);
        return this;
    }

    /**
     * 对外接口方法
     *
     * @param listener
     */
    public void setOnGiftClickListener(SoftGiftListView.GiftOnClickListener listener) {
        softGiftListView.setOnGiftClickListener(listener);
    }

    public void setOnGiftLianClickListener(SoftGiftListView.GiftLianClickListener listener) {
        softGiftListView.setOnGiftLianClickListener(listener);
    }

    /**
     * 更新某个礼物
     *
     * @param gift
     */
    public LiveGiftGridViewPup refreshSoftGiftBean(SoftGiftBean gift) {
        softGiftListView.refreshSoftGiftBean(gift);
        return this;
    }

    public LiveGiftGridViewPup refreshAll() {
        softGiftListView.refreshAll();
        return this;
    }

    /**
     * 更新余额
     *
     * @param balance 余额
     */
    public void updateBalance(int balance) {
        softGiftListView.updateBalance(balance);
    }


    @Override
    public LiveGiftGridViewPup show() {
        return this;
    }

    @Override
    public LiveGiftGridViewPup hide() {
        dismiss();
        return this;
    }

    @Override
    public LiveGiftGridViewPup destroyView() {
        dismiss();
        return this;
    }

    @Override
    public boolean isAnimating() {
        return false;
    }

    @Override
    public void setAnimating(boolean isAnimating) {

    }

    @Override
    public void showShade(boolean show) {

    }


    @Override
    public void showAsDropDown(View anchor) {
        if (Build.VERSION.SDK_INT == 24) {
            Rect rect = new Rect();
            anchor.getGlobalVisibleRect(rect);
            int h = anchor.getResources().getDisplayMetrics().heightPixels - rect.bottom;
            setHeight(h);
        }
        super.showAsDropDown(anchor);
    }

    @Override
    public void showAsDropDown(View anchor, int xoff, int yoff) {
        if (Build.VERSION.SDK_INT == 24) {
            Rect rect = new Rect();
            anchor.getGlobalVisibleRect(rect);
            int h = anchor.getResources().getDisplayMetrics().heightPixels - rect.bottom;
            setHeight(h);
        }
        super.showAsDropDown(anchor, xoff, yoff);
    }

}
