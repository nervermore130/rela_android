package com.thel.modules.live.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by waiarl on 2017/12/12.
 * 直播PK中进入的的观众或者直播断线重连进入直播间
 * 的init消息返回bean
 */

public class LivePkInitBean implements Serializable {
    public String status;
    public String userId;//对方的userId
    public String nickName;//对方昵称
    public String avatar;//对方头像
    public float selfGem;//己方软妹币
    public float toGem;//对方软妹币
    public float x;
    public float y;
    public float width;
    public float height;
    public long leftTime;//总结剩余时间
    public String pkChannel;//pk频道
    public List<LivePkAssisterBean> assisters = new ArrayList<>();//援助榜
    public long totalTime;//PK总时间

    public static final String STATUS_PK_BUSY = "pk_busy";
    public static final String STATUS_PK_SUMMARY = "pk_summary";

}
