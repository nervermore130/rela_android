package com.thel.modules.main.home.moments.mention;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.thel.R;
import com.thel.base.BaseFragment;
import com.thel.bean.topic.TopicFollowerBean;
import com.thel.bean.topic.TopicFollowerListBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.ui.adapter.TopicFollowersListAdapter;
import com.thel.utils.L;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by liuyun on 2017/10/24.
 */

public class MomentMentionedListFragment extends BaseFragment implements MomentMentionedContract.View {

    @BindView(R.id.lin_back)
    LinearLayout lin_back;

    @BindView(R.id.txt_title)
    TextView txt_title;

    @BindView(R.id.listView)
    ListView listView;

    @BindView(R.id.bg_winklist_default)
    View bg_winklist_default;

    private ArrayList<TopicFollowerBean> listPlus = new ArrayList<>();

    private TopicFollowersListAdapter mAdapter;

    private MomentMentionedContract.Presenter presenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_mention_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        txt_title.setText(getString(R.string.moments_mentioned));

        String momentId = getArguments().getString(TheLConstants.BUNDLE_KEY_MOMENT_ID);

        presenter = new MomentMentionedPresenter(this);

        presenter.loadMentions(momentId);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TopicFollowerBean temp = listPlus.get(position);
                String tempUserId = temp.userId + "";
//                Intent intent = new Intent();
//                intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, tempUserId);
//                intent.setClass(getContext(), UserInfoActivity.class);
//                startActivity(intent);
                FlutterRouterConfig.Companion.gotoUserInfo(tempUserId);
            }
        });

    }

    @Override
    public void setPresenter(MomentMentionedContract.Presenter presenter) {

    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void showList(TopicFollowerListBean data) {

        if (data != null && data.userList != null) {

            L.d("MomentMentionedListFragment", " data.userList : " + data.userList.size());

            listPlus.addAll(data.userList);

            if (null == mAdapter) {
                mAdapter = new TopicFollowersListAdapter(listPlus);
                listView.setAdapter(mAdapter);
            } else {
                mAdapter.freshAdapter(listPlus);
                mAdapter.notifyDataSetChanged();
            }

//        if (data.userList != null && data.userList.size() > 0) {
//            bg_winklist_default.setVisibility(View.VISIBLE);
//        } else {
//            bg_winklist_default.setVisibility(View.GONE);
//        }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(this.getClass().getName());
        MobclickAgent.onResume(getContext());
    }

    @Override
    public void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(this.getClass().getName());
        MobclickAgent.onPause(getContext());
    }

    @OnClick(R.id.lin_back)
    void back() {
        getActivity().finish();
    }


}
