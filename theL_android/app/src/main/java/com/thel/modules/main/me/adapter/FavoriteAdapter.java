package com.thel.modules.main.me.adapter;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.FavoriteBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.theme.ThemeCommentBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.modules.main.home.moments.comment.MomentCommentActivity;
import com.thel.modules.main.home.moments.theme.ThemeDetailActivity;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;
import com.thel.utils.ImageUtils;

import java.util.Arrays;
import java.util.List;

import static com.thel.utils.StringUtils.getString;

/**
 * Created by chad
 * Time 17/11/1
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */

public class FavoriteAdapter extends BaseRecyclerViewAdapter<FavoriteBean> implements BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener {

    public FavoriteAdapter(List<FavoriteBean> collections) {
        super(R.layout.favorite_item, collections);
    }

    @Override
    protected void convert(final BaseViewHolder helper, final FavoriteBean item) {
        helper.setText(R.id.txt_time, item.getReleaseTimeShow());//时间
        if (item.isDelete) {//如果该日志已经删除
            helper.getView(R.id.txt_content).setVisibility(View.GONE);
            helper.getView(R.id.txt_deleted).setVisibility(View.VISIBLE);
            helper.setText(R.id.txt_deleted, getString(R.string.moments_deleted));
            helper.getView(R.id.img_thumb).setOnClickListener(null);//头像不可点击
            helper.setImageUrl(R.id.img_thumb, "");//头像
            helper.setText(R.id.txt_username, "");
        } else {
            helper.getView(R.id.txt_deleted).setVisibility(View.GONE);
            if (TextUtils.isEmpty(item.momentsText)) {           //标题内容
                helper.getView(R.id.txt_content).setVisibility(View.GONE);
            } else {
                helper.getView(R.id.txt_content).setVisibility(View.VISIBLE);
                String content = item.momentsText;
                if (content.contains("\n")) {
                    content = content.replace("\n", " ");
                }
                helper.setText(R.id.txt_content, content);
            }
            if (item.secret == MomentsBean.IS_SECRET) {//如果是匿名
                helper.setImageUrl(R.id.img_thumb, TheLConstants.RES_PIC_URL + R.mipmap.icon_user_anonymous);
                helper.setText(R.id.txt_username, TheLApp.getContext().getString(R.string.moments_msgs_notification_anonymous));
                helper.setOnClickListener(R.id.img_thumb, null);
            } else {
                helper.setImageUrl(R.id.img_thumb, item.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE);//头像
                helper.setText(R.id.txt_username, item.nickName);//昵称
                helper.getView(R.id.img_thumb).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        Intent intent = new Intent(mContext, UserInfoActivity.class);
//                        intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, item.userId);
//                        mContext.startActivity(intent);
                        FlutterRouterConfig.Companion.gotoUserInfo(item.userId);
                    }
                });
            }

        }
        HorizontalScrollView scrollView = helper.getView(R.id.scrollview_imgs);
        String[] imageArr = null;
        if (!TextUtils.isEmpty(item.imageUrl)) {
            imageArr = item.imageUrl.split(",");
        }
        if (imageArr != null && imageArr.length > 0) {//添加图片
            scrollView.setVisibility(View.VISIBLE);
            setImages(scrollView.findViewById(R.id.lin_imgs), Arrays.asList(imageArr), item.momentsType, helper.getLayoutPosition(), item);
        } else {
            scrollView.setVisibility(View.GONE);
        }
    }

    private void setImages(LinearLayout lin_imgs, List<String> imageList, String momentsType, final int position, FavoriteBean item) {
        for (int i = 0; i < lin_imgs.getChildCount(); i++) {
            if (i < imageList.size() && imageList.get(i) != null) {
                lin_imgs.getChildAt(i).setVisibility(View.VISIBLE);
                lin_imgs.getChildAt(i).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        gotoMomentsActivity(position);
                    }
                });
                SimpleDraweeView img_pic = lin_imgs.getChildAt(i).findViewById(R.id.img_pic);
                ImageView img_type = lin_imgs.getChildAt(i).findViewById(R.id.img_type);//非话题图标
                ImageView img_theme = lin_imgs.getChildAt(i).findViewById(R.id.img_theme);//话题图标
                img_pic.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(imageList.get(i), mContext.getResources().getDimension(R.dimen.moment_pic_thumbnail_small)
                        , mContext.getResources().getDimension(R.dimen.moment_pic_thumbnail_small)))).build()).setAutoPlayAnimations(true).build());
                img_theme.setVisibility(View.GONE);
                img_type.setVisibility(View.GONE);
                if (momentsType.equals(MomentsBean.MOMENT_TYPE_LIVE)) {//直播日志
                    img_type.setVisibility(View.VISIBLE);
                    img_type.setImageResource(R.mipmap.btn_live_play);
                } else if (momentsType.equals(MomentsBean.MOMENT_TYPE_THEME)) {//话题日志
                    img_theme.setVisibility(View.VISIBLE);
                    img_theme.setImageResource(R.mipmap.icn_topic_comma_01);
                } else if (momentsType.equals(MomentsBean.MOMENT_TYPE_VIDEO)) {//视频日志
                    img_type.setVisibility(View.VISIBLE);
                    img_type.setImageResource(R.mipmap.btn_play_video);
                } else if (MomentsBean.MOMENT_TYPE_VOICE.equals(momentsType) || MomentsBean.MOMENT_TYPE_TEXT_VOICE.equals(momentsType)) {// 音乐日志
                    img_type.setVisibility(View.VISIBLE);
                    img_type.setImageResource(R.mipmap.btn_feed_play_big);
                } else if (MomentsBean.MOMENT_TYPE_THEME_REPLY.equals(momentsType)) {//如果是话题回复
                    if (ThemeCommentBean.TYPE_CLASS_VOICE.equals(item.themeReplyClass)) {//话题回复下的音乐
                        img_type.setVisibility(View.VISIBLE);
                        img_type.setImageResource(R.mipmap.btn_play_video);
                    } else if (ThemeCommentBean.TYPE_CLASS_VIDEO.equals(item.themeReplyClass)) {//话题回复下的视频
                        img_type.setVisibility(View.VISIBLE);
                        img_type.setImageResource(R.mipmap.btn_feed_play_big);
                    }
                }
            } else {
                lin_imgs.getChildAt(i).setVisibility(View.GONE);
            }
        }

    }

    /**
     * 跳转到日志详情
     *
     * @param position
     */

    private void gotoMomentsActivity(int position) {
        FavoriteBean bean = mData.get(position);
        if (bean == null) {
            return;
        }
        if (MomentsBean.MOMENT_TYPE_THEME.equals(bean.momentsType)) {
//            Intent intent;
//            intent = new Intent(mContext, ThemeDetailActivity.class);
//            Bundle bundle = new Bundle();
//            bundle.putString(TheLConstants.BUNDLE_KEY_MOMENT_ID, bean.momentsId);
//            intent.putExtras(bundle);
//            mContext.startActivity(intent);
            FlutterRouterConfig.Companion.gotoThemeDetails(bean.momentsId);
        } else {
//            intent = new Intent(mContext, MomentCommentActivity.class);
            FlutterRouterConfig.Companion.gotoMomentDetails(bean.momentsId);
        }

    }

    @Override
    public void onItemClick(View view, int position) {
        gotoMomentsActivity(position);
    }
}
