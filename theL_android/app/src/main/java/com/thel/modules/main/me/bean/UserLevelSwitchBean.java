package com.thel.modules.main.me.bean;

import com.thel.base.BaseDataBean;

/**
 * Created by lingwei on 2018/4/20.
 */

public class UserLevelSwitchBean extends BaseDataBean {

    /**
     * data : {"userId":100797097,"entrySwitch":1}
     */

    public UserSwitchDataBean data;

    public static class UserSwitchDataBean {
        /**
         * userId : 100797097
         * entrySwitch : 1
         */

        public int userId;
        public int entrySwitch;
        public int levelIconSwitch;
        public int isCloaking; //是否开启榜单隐身 默认0
    }
}
