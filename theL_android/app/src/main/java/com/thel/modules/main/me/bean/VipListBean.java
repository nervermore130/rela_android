package com.thel.modules.main.me.bean;

import com.thel.base.BaseDataBean;

import java.io.Serializable;
import java.util.List;

/**
 * vip类型列表
 *
 * @author Setsail
 */
public class VipListBean extends BaseDataBean {

    /**
     * data : {"list":[{"id":5,"title":"12个月会员","summary":"12个月会员","description":"12个月会员","months":12,"iapId":"com.rela.product.vip.004","price":1,"status":1,"rank":40,"isHot":1,"icon":"http://file.thel.co/advert/1450349514913.jpg","goldPrice":1946},{"id":6,"title":"6个月会员","summary":"6个月会员","description":"6个月会员","months":6,"iapId":"com.rela.product.vip.003","price":1,"status":1,"rank":30,"isHot":0,"icon":"http://file.thel.co/advert/1450349982361.jpg","goldPrice":1036},{"id":7,"title":"3个月会员","summary":"3个月会员","description":"3个月会员","months":3,"iapId":"com.rela.product.vip.002","price":1,"status":1,"rank":20,"isHot":0,"icon":"http://file.thel.co/advert/1450349968784.jpg","goldPrice":546},{"id":8,"title":"1个月会员","summary":"1个月会员","description":"1个月会员","months":1,"iapId":"com.rela.product.vip.001","price":1,"status":1,"rank":10,"isHot":0,"icon":"http://file.thel.co/advert/1450349814105.jpg","goldPrice":210}],"gold":377,"buyUrl":"http://dev.rela.me/user/relaId?type=vip_payment"}
     */

    public VipLisDatatBean data;

    public static class VipLisDatatBean implements Serializable {
        /**
         * list : [{"id":5,"title":"12个月会员","summary":"12个月会员","description":"12个月会员","months":12,"iapId":"com.rela.product.vip.004","price":1,"status":1,"rank":40,"isHot":1,"icon":"http://file.thel.co/advert/1450349514913.jpg","goldPrice":1946},{"id":6,"title":"6个月会员","summary":"6个月会员","description":"6个月会员","months":6,"iapId":"com.rela.product.vip.003","price":1,"status":1,"rank":30,"isHot":0,"icon":"http://file.thel.co/advert/1450349982361.jpg","goldPrice":1036},{"id":7,"title":"3个月会员","summary":"3个月会员","description":"3个月会员","months":3,"iapId":"com.rela.product.vip.002","price":1,"status":1,"rank":20,"isHot":0,"icon":"http://file.thel.co/advert/1450349968784.jpg","goldPrice":546},{"id":8,"title":"1个月会员","summary":"1个月会员","description":"1个月会员","months":1,"iapId":"com.rela.product.vip.001","price":1,"status":1,"rank":10,"isHot":0,"icon":"http://file.thel.co/advert/1450349814105.jpg","goldPrice":210}]
         * gold : 377
         * buyUrl : http://dev.rela.me/user/relaId?type=vip_payment
         */

        public int gold;
        public String buyUrl;
        public String channel;
        public List<VipBean> list;
//        public HasBuy hasBuy;

        @Override
        public String toString() {
            return "VipLisDatatBean{" + "gold=" + gold + ", buyUrl='" + buyUrl + '\'' + ", list=" + list + '}';
        }
    }

    public class HasBuy {
        public int id;

        public int userId;

        public int vipLevel;

        public String startTime;

        public String endTime;

        public int itemId;

        public int months;
    }
}