package com.thel.modules.main.me.bean;

import com.thel.base.BaseDataBean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 收益记录列表
 * Created by Setsail on 2016/6/7.
 */
public class IncomeRecordListBean extends BaseDataBean implements Serializable {
    public IncomeRecordListDataBean data;

    public class IncomeRecordListDataBean {
        public int cursor;
        /**
         * 商品列表
         */
        public List<IncomeRecord> list = new ArrayList<>();
    }
}
