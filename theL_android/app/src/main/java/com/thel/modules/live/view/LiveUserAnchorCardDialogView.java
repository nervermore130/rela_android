package com.thel.modules.live.view;

import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.thel.app.TheLApp;
import com.thel.base.BaseDialogFragment;
import com.thel.imp.follow.FollowStatusChangedListener;
import com.thel.modules.live.bean.LiveUserCardBean;
import com.thel.modules.live.in.LiveBaseView;
import com.thel.utils.ScreenUtils;

/**
 * Created by waiarl on 2018/2/1.
 */

public class LiveUserAnchorCardDialogView extends BaseDialogFragment implements LiveBaseView<LiveUserAnchorCardDialogView>, FollowStatusChangedListener {

    private LiveAnchorUserCardView view;
    private String userId;

    public static final String BUNDLE_KEY_LIVE_USER_CARD_BEAN = "BUNDLE_KEY_LIVE_USER_CARD_BEAN";
    public static final String BUNDLE_KEY_CAN_SHARE = "BUNDLE_KEY_CAN_SHARE";
    public static final String BUNDLE_KEY_SHARE_TITLE = "BUNDLE_KEY_SHARE_TITLE";
    public static final String BUNDLE_KEY_CONTENT = "BUNDLE_KEY_CONTENT";
    public static final String BUNDLE_KEY_SINGLE_TITLE = "BUNDLE_KEY_SINGLE_TITLE";
    public static final String BUNDLE_KEY_IMAGE_URL = "BUNDLE_KEY_IMAGE_URL";
    public static final String BUNDLE_KEY_IS_BROAD_CASTER = "BUNDLE_KEY_IS_BROAD_CASTER";
    public static final String BUNDLE_KEY_LIVE_TYPE = "BUNDLE_KEY_LIVE_TYPE";

    private LiveUserCardBean user;
    private boolean canShare;
    private String shareTitle;
    private String shareContent;
    private String singTitle;
    private String url;
    private boolean isBroadcaster;
    private View.OnClickListener reportListener;
    private View.OnClickListener blockHerListener;
    private int liveType;

    public static LiveUserAnchorCardDialogView getInstance(Bundle bundle) {
        LiveUserAnchorCardDialogView instance = new LiveUserAnchorCardDialogView();
        instance.setArguments(bundle);
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Bundle bundle = getArguments();
        user = (LiveUserCardBean) bundle.getSerializable(BUNDLE_KEY_LIVE_USER_CARD_BEAN);
        canShare = bundle.getBoolean(BUNDLE_KEY_CAN_SHARE);
        shareTitle = bundle.getString(BUNDLE_KEY_SHARE_TITLE);
        shareContent = bundle.getString(BUNDLE_KEY_CONTENT);
        singTitle = bundle.getString(BUNDLE_KEY_SINGLE_TITLE);
        url = bundle.getString(BUNDLE_KEY_IMAGE_URL);
        liveType = bundle.getInt(BUNDLE_KEY_LIVE_TYPE, -1);
        isBroadcaster = bundle.getBoolean(BUNDLE_KEY_IS_BROAD_CASTER);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = new LiveAnchorUserCardView(getContext());
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
        setListener();
    }

    private void init() {
        view.initView(getActivity(), user, canShare, shareTitle, shareContent, singTitle, url, isBroadcaster);
    }

    private void setListener() {
        view.setBlockHerListener(blockHerListener);
        view.setReportListener(reportListener);
        view.setViewBlankClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

    }

    /**
     * 设置屏蔽监听
     */
    public LiveUserAnchorCardDialogView setBlockHerListener(View.OnClickListener listener) {
        this.blockHerListener = listener;
        if (view != null) {
            view.setBlockHerListener(listener);
        }
        return this;
    }

    /**
     * 设置 举报监听
     *
     * @param listener
     * @return
     */
    public LiveUserAnchorCardDialogView setReportListener(View.OnClickListener listener) {
        this.reportListener = listener;
        if (view != null) {
            view.setReportListener(listener);
        }
        return this;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        /*int width = AppInit.displayMetrics.widthPixels;
        int height = AppInit.displayMetrics.heightPixels;
        getDialog().getWindow().setLayout(width, height);*/
    }

    /*******************************************一下为接口继承方法******************************************************/
    @Override
    public LiveUserAnchorCardDialogView show() {
        return null;
    }

    @Override
    public LiveUserAnchorCardDialogView hide() {
        return null;
    }

    @Override
    public LiveUserAnchorCardDialogView destroyView() {
        dismiss();
        return this;
    }

    @Override
    public boolean isAnimating() {
        return false;
    }

    @Override
    public void setAnimating(boolean isAnimating) {

    }

    @Override
    public void showShade(boolean show) {

    }

    /**
     * 关注状态改变
     *
     * @param followStatus 关注状态
     * @param userId       关注的用户Id
     * @param nickName     关注的用户昵称
     * @param avatar       关注的用户头像
     */
    @Override
    public void onFollowStatusChanged(int followStatus, String userId, String nickName, String avatar) {
        if (view != null) {
            view.onFollowStatusChanged(followStatus, userId, nickName, avatar);
        }
    }

    @Override public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {

            int screenHeight = ScreenUtils.getScreenHeight(TheLApp.context);

            int screenWidth = ScreenUtils.getScreenWidth(TheLApp.context);

            if (screenHeight < screenWidth) {

                screenWidth = screenHeight;
            }

            DisplayMetrics dm = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
            WindowManager.LayoutParams attributes = dialog.getWindow().getAttributes();
            attributes.gravity = Gravity.BOTTOM;//对齐方式
            dialog.getWindow().setAttributes(attributes);
            dialog.getWindow().setLayout(screenWidth, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

}
