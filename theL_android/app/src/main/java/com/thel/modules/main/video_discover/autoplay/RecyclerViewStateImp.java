package com.thel.modules.main.video_discover.autoplay;

/**
 * Created by waiarl on 2018/3/20.
 */

public interface RecyclerViewStateImp {
    int State_Draging = 0;
    int State_IDLE = 1;
    int State_Settling = 2;

    RecyclerViewStateImp setState(int state);

    interface RecyclerViewScrollStateChangedListener {
        void stateChanged(int state);
    }


}
