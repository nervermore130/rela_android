package com.thel.modules.live.liveBigGiftAnimLayout.anim;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PointF;
import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.BounceInterpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.OvershootInterpolator;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.thel.modules.live.liveBigGiftAnimLayout.AnimImageView;
import com.thel.modules.live.liveBigGiftAnimLayout.LiveBigAnimUtils;
import com.thel.utils.Utils;

import java.util.Random;

/**
 * Created by the L on 2016/9/27.
 */
public class BubbleAnimator {
    private final Context mContext;
    private final Handler mHandler;
    private final Random mRandom;
    //    private final int[] bubbleId;
    //    private final int[] heartId;
    private final String[] bubbleId;
    private final String[] heartId;
    private final int mBubbleDuration;
    private final int jumpCircle;
    private final int mDuration;
    private final String foldPath;
    private boolean isPlaying;
    private int mWidth;
    private int mHeight;
    private int heartWidth;
    private int heartHeight;
    private long addHeartDuration;
    private long jumpDuration;
    private long heartScaleDuration;
    private int waveFreq;//波浪间隔周期
    private AnimationDrawable waveAnim;
    private long buggleFreq;
    private long jumpDelayTime;
    private int commonBubbleSize;

    public BubbleAnimator(Context context, int duration) {
        mContext = context;
        mDuration = duration;
        mHandler = new Handler(Looper.getMainLooper());
        mRandom = new Random();
        //        bubbleId = new int[]{R.drawable.icn_animate_bubble_01, R.drawable.icn_animate_bubble_02, R.drawable.icn_animate_bubble_03, R.drawable.icn_animate_bubble_04, R.drawable.icn_animate_bubble_05, R.drawable.icn_animate_bubble_06,};
        //        heartId = new int[]{R.drawable.icn_animate_bubbleheart_01, R.drawable.icn_animate_bubbleheart_02, R.drawable.icn_animate_bubbleheart_03, R.drawable.icn_animate_bubbleheart_04, R.drawable.icn_animate_bubbleheart_05, R.drawable.icn_animate_bubbleheart_06, R.drawable.icn_animate_bubbleheart_07,};
        foldPath = "anim/bubble";
        bubbleId = new String[]{"icn_animate_bubble_01",
                "icn_animate_bubble_02",
                "icn_animate_bubble_03",
                "icn_animate_bubble_04",
                "icn_animate_bubble_05",
                "icn_animate_bubble_06",};
        heartId = new String[]{"icn_animate_bubbleheart_01",
                "icn_animate_bubbleheart_02",
                "icn_animate_bubbleheart_03",
                "icn_animate_bubbleheart_04",
                "icn_animate_bubbleheart_05",
                "icn_animate_bubbleheart_06",
                "icn_animate_bubbleheart_07",};

        mBubbleDuration = 3000;//单个泡泡持续时间
        addHeartDuration = 1700;//最开始心出来跳动时间
        jumpDelayTime = 1000;//心跳延迟时间
        jumpDuration = 5000;//后期心跳中时间
        jumpCircle = 1800;//后期心跳周期
        heartScaleDuration = 500;//最后心放大动画时间
        heartWidth = Utils.dip2px(mContext, 150);//正常心宽度
        heartHeight = heartWidth * 323 / 370;//正常心高度
        waveFreq = 80;//波浪频率
        buggleFreq = 30;//突出泡泡频率
        commonBubbleSize = Utils.dip2px(mContext, 10);//最开始气球大小
    }

    //开始动画
    public int start(final ViewGroup parent) {
        isPlaying = true;
        mWidth = parent.getMeasuredWidth();
        mHeight = parent.getMeasuredHeight();
        final RelativeLayout backgound = new RelativeLayout(mContext);
        parent.addView(backgound, RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        setBackground(backgound);

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                backgound.clearAnimation();
                parent.removeView(backgound);
            }
        }, mDuration);
        return mDuration;
    }

    private void setBackground(final RelativeLayout backgound) {
        backgound.post(new Runnable() {
            @Override
            public void run() {
                addHeart(backgound);
            }
        });
    }

    //添加心
    private void addHeart(final ViewGroup parent) {
        final RelativeLayout bubbleParent = new RelativeLayout(mContext);
        parent.addView(bubbleParent);
        bubbleParent.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
        final AnimImageView heartIv = new AnimImageView(mContext);
        //  heartIv.setBackgroundResource(heartId[0]);
        LiveBigAnimUtils.setAssetBackground(heartIv, foldPath, heartId[0]);
        parent.addView(heartIv);
        final RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams) heartIv.getLayoutParams();
        param.width = heartWidth;
        param.height = heartHeight;
        param.addRule(RelativeLayout.CENTER_IN_PARENT);
        final AnimatorSet animatorSet = new AnimatorSet();
        final ObjectAnimator scalX = new ObjectAnimator().ofFloat(heartIv, "scaleX", 1f, 1.6f, 1f);
        final ObjectAnimator scalY = new ObjectAnimator().ofFloat(heartIv, "scaleY", 1f, 1.6f, 1f);
        animatorSet.setDuration(addHeartDuration);
        animatorSet.playTogether(scalX, scalY);
        animatorSet.setInterpolator(new MyBounceInterpolator());
        animatorSet.start();
        //LiveBigGiftAnimLayout.setFrameAnim(mContext, heartIv, heartId, waveFreq);
        LiveBigAnimUtils.setFrameAnim(mContext, heartIv, foldPath, heartId, waveFreq);
        addBubble(bubbleParent);
        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        jumpHeart(heartIv);
                    }
                }, jumpDelayTime);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    //心跳
    private void jumpHeart(final ImageView heartIv) {
        final AnimatorSet animatorSet = new AnimatorSet();
        final ObjectAnimator scalX = new ObjectAnimator().ofFloat(heartIv, "scaleX", 1f, 1.3f, 1f, 1f);
        final ObjectAnimator scalY = new ObjectAnimator().ofFloat(heartIv, "scaleY", 1f, 1.3f, 1f, 1f);
        scalX.setRepeatMode(ValueAnimator.RESTART);
        scalY.setRepeatMode(ValueAnimator.RESTART);
        scalX.setDuration(jumpCircle);
        scalY.setDuration(jumpCircle);
        scalX.setRepeatCount(ObjectAnimator.INFINITE);
        scalY.setRepeatCount(ObjectAnimator.INFINITE);
        animatorSet.setInterpolator(new OvershootInterpolator());
        animatorSet.playTogether(scalX, scalY);
        animatorSet.start();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (waveAnim != null && waveAnim.isRunning()) {
                    waveAnim.stop();
                }
                if (animatorSet != null && animatorSet.isRunning()) {
                    animatorSet.cancel();
                }
                scaleHeart(heartIv);
            }
        }, jumpDuration);

    }

    //最后心放大
    private void scaleHeart(final ImageView heartIv) {
        final float sx = (mWidth + 300) / heartWidth;
        final float sy = (mHeight + 300) / heartHeight;
        final float scale = Math.min(sx, sy);
        heartIv.animate().scaleXBy(scale).scaleYBy(scale).alpha(0.3f).setDuration(heartScaleDuration).start();
        heartIv.animate().setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                isPlaying = false;
                ViewGroup parent = (ViewGroup) heartIv.getParent();
                if (parent != null) {
                    parent.removeView(heartIv);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    //添加气泡
    private void addBubble(final ViewGroup parent) {
        parent.post(new Runnable() {
            @Override
            public void run() {
                addOneBubble(parent);

                if (isPlaying) {
                    parent.postDelayed(this, buggleFreq);
                }
            }
        });
    }

    private void addOneBubble(final ViewGroup parent) {
        final ImageView iv = new ImageView(mContext);
        final int id = mRandom.nextInt(bubbleId.length);
        // iv.setImageResource(bubbleId[id]);
        LiveBigAnimUtils.setAssetImage(iv, foldPath, bubbleId[id]);
        parent.addView(iv);
        iv.setLayoutParams(new RelativeLayout.LayoutParams(commonBubbleSize, commonBubbleSize));
        final RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams) iv.getLayoutParams();
        param.addRule(RelativeLayout.CENTER_IN_PARENT);
        FloatAnimation anim;
        final float scaleT = mRandom.nextFloat() * 9;
        final float alphaT = mRandom.nextFloat() * 7 / 3;
        anim = new FloatAnimation(createPath(), parent, iv, scaleT, alphaT);
        anim.setDuration(mBubbleDuration);
        anim.setInterpolator(new LinearInterpolator());
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        parent.removeView(iv);
                    }
                });
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        iv.startAnimation(anim);
    }

    public Path createPath() {
        Path p = new Path();
        PointF p1 = new PointF(-mWidth / 4 + mRandom.nextFloat() * mWidth / 2, -mHeight / 4 + mRandom.nextFloat() * mHeight / 2);
        PointF p2 = getPoint(p1);
        PointF p3 = getPoint(p2);
        p.cubicTo(p1.x, p1.y, p2.x, p2.y, p3.x, p3.y);
        return p;
    }

    private PointF getPoint(PointF point) {
        PointF p;
        if (point.x >= 0 && Math.abs(point.x) >= Math.abs(point.y)) {
            p = new PointF(point.x + mRandom.nextFloat() * mWidth / 8, point.y - mHeight / 4 + mRandom.nextFloat() * mHeight / 2);
        } else if (point.x <= 0 && Math.abs(point.x) >= Math.abs(point.y)) {
            p = new PointF(point.x - mRandom.nextFloat() * mWidth / 8, point.y - mHeight / 4 + mRandom.nextFloat() * mHeight / 2);
        } else if (point.y >= 0 && Math.abs(point.x) <= Math.abs(point.y)) {
            p = new PointF(point.x - mWidth / 4 + mRandom.nextFloat() * mWidth / 2, point.y + mRandom.nextFloat() * mHeight / 8);
        } else if (point.y <= 0 && Math.abs(point.x) <= Math.abs(point.y)) {
            p = new PointF(point.x - mWidth / 4 + mRandom.nextFloat() * mWidth / 2, point.y - mRandom.nextFloat() * mHeight / 8);
        } else {
            p = new PointF(point.x, point.y);
        }
        return p;
    }

    static class FloatAnimation extends Animation {
        private final float mScaleT;
        private final float mRotationFactor;
        private final Random mRandom;
        private final float mRotationDigree;
        private PathMeasure mPm;
        private View mView;
        private float mDistance;

        public FloatAnimation(Path path, View parent, View child, float scaleT, float alphaT) {
            mPm = new PathMeasure(path, false);
            mDistance = mPm.getLength();
            mView = child;
            parent.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            mScaleT = scaleT;
            mRandom = new Random();
            mRotationFactor = mRandom.nextFloat();
            mRotationDigree = mRandom.nextFloat() * 360 * (1 - mRotationFactor);
        }

        @Override
        protected void applyTransformation(float factor, Transformation transformation) {
            Matrix matrix = transformation.getMatrix();
            mPm.getMatrix(mDistance * factor, matrix, PathMeasure.POSITION_MATRIX_FLAG);
            mView.setScaleX(1 + factor * mScaleT);
            mView.setScaleY(1 + factor * mScaleT);
            if (factor >= 0.7)// 从70%开始淡出
                transformation.setAlpha(1 - (factor - 0.7f) / 0.3f);
            if (factor > mRotationFactor) {
                mView.setRotation(mRotationDigree * (factor - mRotationFactor) / (1 - mRotationFactor));
            }
        }

    }

    private class MyBounceInterpolator extends BounceInterpolator {
        public MyBounceInterpolator() {
        }

        @SuppressWarnings({"UnusedDeclaration"})
        public MyBounceInterpolator(Context context, AttributeSet attrs) {
        }

        private float bounce(float t) {
            return t * t * 8.0f;
        }

        private float finalSize;

        public float getInterpolation(float t) {
            t *= 1.1226f;
            if (t < 0.3535f)
                return bounce(t);
            else if (t < 0.7408f) {
                finalSize = bounce(t - 0.54719f) + 0.8f;
                return finalSize;
            } else
                return finalSize;
        }

    }

}
