package com.thel.modules.main.home.moments.web;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;

import androidx.core.content.ContextCompat;

import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UploadManager;
import com.tencent.mm.opensdk.modelbiz.WXLaunchMiniProgram;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.bean.RecommendedModel;
import com.thel.bean.ResultBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.recommend.RecommendWebBean;
import com.thel.bean.user.UploadTokenBean;
import com.thel.constants.TheLConstants;
import com.thel.constants.TheLConstantsExt;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.growingio.GrowingIoConstant;
import com.thel.modules.live.surface.watch.LiveWatchActivity;
import com.thel.modules.live.surface.watch.horizontal.LiveWatchHorizontalActivity;
import com.thel.modules.login.login_register.LoginRegisterActivity;
import com.thel.modules.main.MainActivity;
import com.thel.modules.main.home.moments.ReleaseMomentActivity;
import com.thel.modules.main.home.moments.SendCardActivity;
import com.thel.modules.main.home.moments.comment.MomentCommentActivity;
import com.thel.modules.main.home.moments.theme.ThemeDetailActivity;
import com.thel.modules.main.home.tag.TagDetailActivity;
import com.thel.modules.main.me.aboutMe.BuyVipActivity;
import com.thel.modules.main.me.aboutMe.StickerPackDetailActivity;
import com.thel.modules.main.me.aboutMe.StickerStoreActivity;
import com.thel.modules.main.me.aboutMe.WalkmanActivity;
import com.thel.modules.main.me.help.HelpActivity;
import com.thel.modules.main.messages.utils.PushUtils;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.modules.others.VipConfigActivity;
import com.thel.modules.preview_video.VideoActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.RequestConstants;
import com.thel.network.service.DefaultRequestService;
import com.thel.ui.adapter.ShareGridAdapter;
import com.thel.ui.widget.LollipopFixedWebView;
import com.thel.utils.AndroidBug5497Workaround;
import com.thel.utils.BusinessUtils;
import com.thel.utils.DeviceUtils;
import com.thel.utils.DialogUtil;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.GsonUtils;
import com.thel.utils.ImageUtils;
import com.thel.utils.JsonUtils;
import com.thel.utils.L;
import com.thel.utils.MD5Utils;
import com.thel.utils.PermissionUtil;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.ToastUtils;
import com.thel.utils.UMShareUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import androidx.fragment.app.Fragment;
import io.flutter.embedding.android.DrawableSplashScreen;
import io.flutter.embedding.android.FlutterView;
import io.flutter.embedding.android.SplashScreen;
import io.flutter.embedding.android.SplashScreenProvider;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import me.rela.rf_s_bridge.new_router.NewUniversalRouter;
import me.rela.rf_s_bridge.new_router.containers.FlutterActivityAndFragmentDelegate;
import me.rela.rf_s_bridge.new_router.containers.NewFlutterFragment;
import me.rela.rf_s_bridge.new_router.interfaces.OnFragmentListener;
import me.rela.rf_s_bridge.router.BoostFlutterView;
import me.rela.rf_s_bridge.router.FlutterBoostPlugin;
import me.rela.rf_s_bridge.router.UniversalRouter;

/**
 * Created by liuyun
 */

public class WebViewActivity extends BaseActivity implements OnFragmentListener, SplashScreenProvider {
    /**
     * 是否需要安全检查，如果是Rela自己的web页面，不需要安全检查
     */
    public static final String NEED_SECURITY_CHECK = "needSecurityCheck";
    public static final String URL = "url";
    public static final String SITE = "site";

    private ProgressBar progressBar;
    private LollipopFixedWebView webView;
    private RelativeLayout rel_title;
    private TextView txt_title;
    private LinearLayout rel_bottom;
    private LinearLayout lin_back;
    private ImageView img_more;
    private ImageView bottom_back;
    private ImageView bottom_next;
    private ImageView bottom_refresh;

    private String uid;

    private String site = "";

    private String title;

    private String url = null;

    private String dumpUrl;
    private String shareLogo;
    private String shareTitle;
    private String shareContent;

    private CallbackManager callbackManager;
    private UMShareListener umShareListener;

    private String url_getInfo = "thel://com.user/getInfo";
    private String url_follow = "thel://com.user/follow";
    private String url_unfollow = "thel://com.user/unfollow";
    private String url_user = "thel://com.user/path";
    private String url_moment = "thel://com.moment/path";
    private String url_theme = "thel://com.topic/path";
    private String url_tag = "thel://com.tag/path";
    private String url_sticker_pack = "thel://com.emoji_config/path";
    private String url_sticker_store = "thel://com.emojiShop/path";

    private String url_video = "thel://com.video/path";
    private String url_release_theme = "thel://com.writetopic/path";
    private String url_request = "thel-gap://request";
    private String url_alert = "thel-gap://alert";
    private String url_feedback = "thel://com.support/path";
    private String url_live_room = "thel://com.live/path";
    private String url_share = "thel://com.share/shareByClient";
    private String url_write_video = "thel://com.video/publish";
    private String url_audio = "thel://com.audio/path";
    public String url_buy_vip = "thelweb://com.buy.vip/path";
    public String url_vip_config = "thel://path.vipServiceView/path";
    public String url_test_network = "thel://com.testnetwork/path";
    public String url_h5_vip_config = "thelweb://com.vip/path";
    public String url_cusomerService = "thel://com.customerService/path";
    public static String url_walkman = "thel://com.relafm/path";
    public String url_live_horizontal = "thel://com.livehorizontal/path";
    private String url_getPicData = "thel://com.savepic?callback=getPicData";
    private String url_recording_start = "thel://com.audio/start";
    private String url_recording_end = "thel://com.audio/end";
    private String url_sharepic = "thel://com.sharepic";
    private String url_bind_phone = "thel://com.bindPhone/path";

    private String security_check = "https://security.rela.me/safe_redirect?url=";

    // 1：关注 2：取消关注
    private String followOrUnfollow;

    private String getMyInfoCallback;
    private String followCallback;
    private String unfollowCallback;
    private String shareSucceedCallback = "relaClientShareSuccess";
    private String recordingCallback;

    private String adUrl;
    private RelativeLayout rel_dialog;
    private View bottom_cancel;
    private View bottom_copy;
    private GridView gridView;
    private TextView bottom_title;
    private static final int SHOW_DIALOG = 0;
    private static final int WRITE_VIDEO_MOMENT = 1;
    private static final int SHOW_IMAGE_SHARE_DIALOG = 2;
    private View img_recommend;
    private FrameLayout flutter_container_fl;

    public static final String SHARE_TYPE_WEB = "web";//分享网页
    public static final String SHARE_TYPE_IMAGE = "image";//分享图片
    private LinearLayout ll_recommend;
    private LinearLayout lin_previous;
    private MediaRecorder mRecorder; // 录音机
    private long mRecorderStartTime;
    private long mRecorderEndTime;
    private String mFileName = Calendar.getInstance().getTimeInMillis() + ".aac"; // 录音文件名
    private String recordingPath = TheLConstants.F_MSG_VOICE_ROOTPATH + UserUtils.getMyUserId() + File.separator + mFileName;
    private String finishCallback;

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */

    private void getDataFromPrepage() {
        Intent intent = getIntent();
        site = intent.getStringExtra("site");
        uid = intent.getStringExtra("uid");
        Bundle bundle = intent.getExtras();

        if (bundle != null) {
            url = bundle.getString(URL, null);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setLayout();
        getDataFromPrepage();
        findViewById();

        /** 分享初始化 **/
        callbackManager = CallbackManager.Factory.create();
        umShareListener = new UMShareListener() {

            @Override
            public void onStart(SHARE_MEDIA share_media) {
            }

            @Override
            public void onResult(SHARE_MEDIA platform) {
                shareResult(1);
            }

            @Override
            public void onError(SHARE_MEDIA platform, Throwable t) {
                shareResult(0);
                ToastUtils.showToastShort(WebViewActivity.this, t.getMessage());
            }

            @Override
            public void onCancel(SHARE_MEDIA platform) {
                shareResult(0);
            }
        };
        /** 分享初始化 **/

        setListener();

        registerReceiver();

        WebChromeClient webChromeClient = new WebChromeClient() {

            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);

                if (WebViewActivity.this.title == null || WebViewActivity.this.title.length() == 0) {

                    if (!TextUtils.isEmpty(url) && url.equals(TheLConstants.USER_AGREEMENT_PAGE_URL)) {
                        txt_title.setText(R.string.default_activity_info_user_agreement);
                    } else if (title != null) {
                        txt_title.setText(title);
                    }
                }

            }

            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                if (newProgress == 100) {
                    progressBar.setVisibility(View.GONE);
                } else {
                    if (View.GONE == progressBar.getVisibility()) {
                        progressBar.setVisibility(View.VISIBLE);
                    }
                    progressBar.setProgress(newProgress);
                }
                super.onProgressChanged(view, newProgress);
            }

            @Override
            public void onShowCustomView(View view, WebChromeClient.CustomViewCallback callback) {
                if (myCallback != null) {
                    myCallback.onCustomViewHidden();
                    myCallback = null;
                    return;
                }

                long id = Thread.currentThread().getId();

                ViewGroup parent = (ViewGroup) webView.getParent();
                String s = parent.getClass().getName();
                parent.removeView(rel_title);
                parent.removeView(webView);
                parent.removeView(rel_bottom);
                parent.addView(view);
                myView = view;
                myCallback = callback;
                //                chromeClient = this;
            }

            private View myView = null;
            private CustomViewCallback myCallback = null;


            public void onHideCustomView() {

                long id = Thread.currentThread().getId();

                if (myView != null) {

                    if (myCallback != null) {
                        myCallback.onCustomViewHidden();
                        myCallback = null;
                    }

                    ViewGroup parent = (ViewGroup) myView.getParent();
                    parent.removeView(myView);
                    if (null != rel_title.getParent())     // fabric  #10541
                        ((ViewGroup) rel_title.getParent()).removeView(rel_title);
                    if (null != webView.getParent())
                        ((ViewGroup) webView.getParent()).removeView(webView);
                    if (null != rel_bottom.getParent())
                        ((ViewGroup) rel_bottom.getParent()).removeView(rel_bottom);
                    parent.addView(rel_title);
                    parent.addView(webView);
                    parent.addView(rel_bottom);
                    myView = null;
                }
            }
        };

        webView.getSettings().setUserAgentString(webView.getSettings().getUserAgentString() + " theL/" + DeviceUtils.getVersionName(this));
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.getSettings().setAppCacheEnabled(false);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setSavePassword(false);
        webView.addJavascriptInterface(new InJavaScriptLocalObj(), "local_obj");
        webView.addJavascriptInterface(new InJavaScriptRecomendWeb(), "getDefaultImage");
        webView.removeJavascriptInterface("searchBoxJavaBridge_");// 修复漏洞
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            WebView.setWebContentsDebuggingEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);

        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setTextSize(WebSettings.TextSize.NORMAL);
        webView.setWebChromeClient(webChromeClient);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                L.d("WebViewActivity", "should,url=" + url);
                if (TextUtils.isEmpty(url))
                    return true;
                if (url.contains("xiami://song/") || url.contains("www.xiami.com")) {
                    try {
                        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        startActivity(i);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else if (url.contains(url_getInfo)) {// 获取我的信息
                    if (filterUrl(webView.getUrl())) {
                        try {
                            Uri uri = Uri.parse(url);
                            getMyInfoCallback = uri.getQueryParameter("callback");
                            webView.loadUrl("javascript:" + getMyInfoCallback + "('" + buildCallbackParam(ShareFileUtils.getString(ShareFileUtils.ID, ""), ShareFileUtils.getString(ShareFileUtils.USER_NAME, ""), ShareFileUtils.getString(ShareFileUtils.AVATAR, ""), BusinessUtils.generateJsKey(), DeviceUtils.getLanguage()) + "')");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else if (url.contains(url_follow)) {// 关注某人
                    if (filterUrl(webView.getUrl())) {
                        try {
                            Uri uri = Uri.parse(url);
                            final String userId = uri.getQueryParameter("userId");
                            String nickName = uri.getQueryParameter("nickName");
                            String avatar = uri.getQueryParameter("avatar");
                            followCallback = uri.getQueryParameter("callback");
                            if (!TextUtils.isEmpty(userId)) {
                                showLoading();
                                followOrUnfollow = "1";

                                Flowable<ResultBean> flowable = RequestBusiness.getInstance().followUser(userId, followOrUnfollow);

                                flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<ResultBean>() {
                                    @Override
                                    public void onNext(ResultBean resultBean) {
                                        super.onNext(resultBean);
                                        if ("1".equals(followOrUnfollow)) {// 关注成功
                                            String js = "javascript:" + followCallback + "('" + buildCallbackParam(userId, null, null, null, null) + "')";
                                            webView.loadUrl(js);
                                            DialogUtil.showToastShort(WebViewActivity.this, getThelString(R.string.userinfo_activity_followed));
                                        } else {// 取消关注成功
                                            String js = "javascript:" + unfollowCallback + "('" + buildCallbackParam(userId, null, null, null, null) + "')";
                                            webView.loadUrl(js);
                                        }
                                        closeLoading();
                                    }

                                    @Override
                                    public void onError(Throwable t) {
                                        super.onError(t);
                                        closeLoading();

                                    }
                                });

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else if (url.contains(url_unfollow)) {// 取消关注某人
                    if (filterUrl(webView.getUrl())) {
                        try {
                            Uri uri = Uri.parse(url);
                            unfollowCallback = uri.getQueryParameter("callback");
                            final String userId = uri.getQueryParameter("userId");
                            if (!TextUtils.isEmpty(userId)) {
                                showLoading();
                                followOrUnfollow = "0";

                                Flowable<ResultBean> flowable = RequestBusiness.getInstance().followUser(uri.getQueryParameter("userId"), followOrUnfollow);

                                flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<ResultBean>() {
                                    @Override
                                    public void onNext(ResultBean resultBean) {
                                        super.onNext(resultBean);
                                        if ("1".equals(followOrUnfollow)) {// 关注成功
                                            //                                            MessageUtils.saveMessageToDB(MsgUtils.createMsgBean(MsgBean.MSG_TYPE_FOLLOW, "", MsgBean.IS_FOLLOW_MSG, userId, nickName, avatar));
                                            webView.loadUrl("javascript:" + followCallback + "('" + buildCallbackParam(userId, null, null, null, null) + "')");
                                            DialogUtil.showToastShort(WebViewActivity.this, getThelString(R.string.userinfo_activity_followed));
                                        } else {// 取消关注成功
                                            webView.loadUrl("javascript:" + unfollowCallback + "('" + buildCallbackParam(userId, null, null, null, null) + "')");
                                        }
                                        closeLoading();
                                    }

                                    @Override
                                    public void onError(Throwable t) {
                                        super.onError(t);
                                        closeLoading();

                                    }
                                });

                                //                                requestBussiness.followUser(new DefaultNetworkHelper(WebViewActivity.this), uri.getQueryParameter("userId"), followOrUnfollow);
                            }
                        } catch (Exception e) {
                        }
                    }
                } else if (url.contains(url_user)) {// 跳转到用户详情页
                    if (filterUrl(webView.getUrl())) {
                        try {
                            Uri uri = Uri.parse(url);
                            if (!TextUtils.isEmpty(uri.getQueryParameter("userId"))) {
//                                Intent intent = new Intent();
//                                intent.setClass(WebViewActivity.this, UserInfoActivity.class);
//                                intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, uri.getQueryParameter(TheLConstants.BUNDLE_KEY_USER_ID));
//                                startActivity(intent);
                                FlutterRouterConfig.Companion.gotoUserInfo(uri.getQueryParameter(TheLConstants.BUNDLE_KEY_USER_ID));
                            }
                        } catch (Exception e) {
                        }
                    }
                } else if (url.contains(url_moment)) {// 跳转到日志详情页
                    if (filterUrl(webView.getUrl())) {
                        try {
                            Uri uri = Uri.parse(url);
                            if (!TextUtils.isEmpty(uri.getQueryParameter("momentId"))) {
//                                Intent intent = new Intent();
//                                intent.setClass(WebViewActivity.this, MomentCommentActivity.class);
//                                intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, uri.getQueryParameter("momentId"));
//                                startActivity(intent);
                                FlutterRouterConfig.Companion.gotoMomentDetails(uri.getQueryParameter("momentId"));
                            }
                        } catch (Exception e) {
                        }
                    }
                } else if (url.contains(url_theme)) {// 跳转到话题详情页
                    if (filterUrl(webView.getUrl())) {
                        try {
                            Uri uri = Uri.parse(url);
                            if (!TextUtils.isEmpty(uri.getQueryParameter("momentId"))) {
//                                Intent intent = new Intent();
//                                intent.setClass(WebViewActivity.this, ThemeDetailActivity.class);
//                                intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, uri.getQueryParameter("momentId"));
//                                startActivity(intent);
                                FlutterRouterConfig.Companion.gotoThemeDetails(uri.getQueryParameter("momentId"));
                            }
                        } catch (Exception e) {
                        }
                    }
                } else if (url.contains(url_release_theme)) {
                    if (filterUrl(webView.getUrl())) {
                        try {
                            Uri uri = Uri.parse(url);
                            String tagName = uri.getQueryParameter("tagName");
                            Intent intent = new Intent();
                            intent.setClass(WebViewActivity.this, ReleaseMomentActivity.class);
                            if (!TextUtils.isEmpty(tagName)) {
                                intent.putExtra(TheLConstants.BUNDLE_KEY_TOPIC_NAME, tagName);
                            }
                            startActivity(intent);
                        } catch (Exception e) {
                        }
                    }
                } else if (url.contains(url_tag)) {// 跳转到标签详情页
                    if (filterUrl(webView.getUrl())) {
                        try {
                            Uri uri = Uri.parse(url);
                            if (!TextUtils.isEmpty(uri.getQueryParameter("tagName"))) {
//                                Intent intent = new Intent(WebViewActivity.this, TagDetailActivity.class);
//                                intent.putExtra(TheLConstants.BUNDLE_KEY_TOPIC_NAME, uri.getQueryParameter("tagName"));
//                                startActivity(intent);
                                FlutterRouterConfig.Companion.gotoTagDetails(-1, uri.getQueryParameter("tagName"));
                            }
                        } catch (Exception e) {
                        }
                    }
                } else if (url.contains(url_sticker_pack)) {// 跳转到表情包详情页
                    if (filterUrl(webView.getUrl())) {
                        try {
                            Uri uri = Uri.parse(url);
                            if (!TextUtils.isEmpty(uri.getQueryParameter("emojiId"))) {
                                Intent intent = new Intent(WebViewActivity.this, StickerPackDetailActivity.class);
                                intent.putExtra("id", Long.valueOf(uri.getQueryParameter("emojiId")));
                                startActivity(intent);
                            }
                        } catch (Exception e) {
                        }
                    }
                } else if (url.contains(url_sticker_store)) {// 跳转到表情商店
                    if (filterUrl(webView.getUrl())) {
                        Intent intent = new Intent(WebViewActivity.this, StickerStoreActivity.class);
                        startActivity(intent);
                    }
                } else if (url.contains(url_buy_vip)) {// 跳转到购买会员
                    if (filterUrl(webView.getUrl())) {
                        Intent intent = new Intent(WebViewActivity.this, BuyVipActivity.class);
                        startActivity(intent);
                    }
                } else if (url.contains(url_vip_config)) {// feed跳转会员权益页
                    if (filterUrl(webView.getUrl())) {
                        Intent intent = new Intent(WebViewActivity.this, VipConfigActivity.class);
                        GrowingIOUtil.goToWeb(GrowingIoConstant.AD_MOMENTS_GOTO_VIP);//跳转会员权益页 埋点
                        startActivity(intent);
                    }
                } else if (url.contains(url_h5_vip_config)) {// h5跳转会员权益页
                    if (filterUrl(webView.getUrl())) {
                        Intent intent = new Intent(WebViewActivity.this, VipConfigActivity.class);
                        GrowingIOUtil.goToWeb(GrowingIoConstant.AD_MOMENTS_GOTO_VIP);//跳转会员权益页 埋点
                        startActivity(intent);
                    }
                } else if (url.contains(url_cusomerService)) {// h5跳转到客服页面
                    if (filterUrl(webView.getUrl())) {
                        Utils.gotoUDesk();

                    }
                } else if (url.contains(url_video)) {// 播放视频

                    L.d("WebViewActivity", " url_video : " + url);

                    if (filterUrl(webView.getUrl())) {
                        try {
                            Uri uri = Uri.parse(url);

                            if (!TextUtils.isEmpty(uri.getQueryParameter("url"))) {
                                Intent intent = new Intent(WebViewActivity.this, VideoActivity.class);
                                intent.putExtra("url", uri.getQueryParameter("url"));
                                intent.putExtra("Referer", webView.getUrl());
                                startActivity(intent);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else if (url.contains(url_feedback)) {// 意见反馈
                    if (filterUrl(webView.getUrl())) {
                        MobclickAgent.onEvent(TheLApp.getContext(), "feedbacks_click");
                       /* Intent intent = new Intent(WebViewActivity.this, ReportActivity.class);
                        intent.putExtra(TheLConstants.BUNDLE_KEY_REPORT_TYPE, ReportActivity.REPORT_TYPE_BUG_FEEDBACK);
                        startActivity(intent);*/
                        Utils.gotoUDesk();

                    }
                } else if (url.contains(url_request)) {// 调用服务端的接口，然后将返回结果返回给网页
                    if (filterUrl(webView.getUrl())) {
                        try {
                            Uri uri = Uri.parse(url);
                            final String callbackFunc = uri.getQueryParameter("callbackFunc");
                            String path = uri.getQueryParameter("path");
                            L.d("WebViewActivity", " path : " + path);
                            if (!path.startsWith("/"))
                                path = "/" + path;
                            String method = uri.getQueryParameter("method");
                            JSONObject params = new JSONObject(uri.getQueryParameter("params"));
                            Iterator<String> iterator = params.keys();
                            Map<String, String> data = new HashMap<>();
                            data.put("isWeb", 1 + "");
                            while (iterator.hasNext()) {
                                String key = iterator.next();
                                data.put(key, params.getString(key));
                            }

                            if (method.equals("get")) {
                                Flowable<String> flowable = DefaultRequestService.createWebViewRequestService().getWebView(Uri.decode(path), data);

                                flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<String>() {
                                    @Override
                                    public void onNext(String data) {
                                        super.onNext(data);

                                        L.d("WebView", " data : " + data);

                                        String web = null;

                                        JSONObject resultObj = new JSONObject();

                                        try {
                                            resultObj.put("statusCode", 200);
                                            resultObj.put("message", "");
                                            resultObj.put("data", data);

                                            web = resultObj.toString().replaceAll("\\\\", "\\\\\\\\").replaceAll("\n", "").replaceAll("'", "\\\\\'");

                                            L.d("WebView", " web : " + web);

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                        //String str = "{\"statusCode\":200,\"message\":\"\",\"data\":{\"data\":[]}}";

                                        webView.loadUrl("javascript:" + callbackFunc + "('" + web + "')");

                                        closeLoading();
                                    }

                                    @Override
                                    public void onError(Throwable t) {
                                        super.onError(t);
                                        L.d("WebViewActivity", t.getMessage());
                                        DialogUtil.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.info_wrong));
                                        closeLoading();
                                    }
                                });
                            } else {
                                Flowable<String> flowable = DefaultRequestService.createWebViewRequestService().postWebView(Uri.decode(path), MD5Utils.generateSignatureForMap(data));

                                flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<String>() {
                                    @Override
                                    public void onNext(String data) {
                                        super.onNext(data);

                                        String web = null;

                                        JSONObject resultObj = new JSONObject();

                                        try {
                                            resultObj.put("statusCode", 200);
                                            resultObj.put("message", "");
                                            resultObj.put("data", data);

                                            web = resultObj.toString().replaceAll("\\\\", "\\\\\\\\").replaceAll("\n", "").replaceAll("'", "\\\\\'");
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                        webView.loadUrl("javascript:" + callbackFunc + "('" + web + "')");

                                        closeLoading();
                                    }

                                    @Override
                                    public void onError(Throwable t) {
                                        super.onError(t);
                                        DialogUtil.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.info_wrong));
                                        closeLoading();
                                    }
                                });
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else if (url.contains(url_alert)) {// 网页调用弹窗
                    if (filterUrl(webView.getUrl())) {
                        try {
                            Uri uri = Uri.parse(url);
                            final String callbackFunc = uri.getQueryParameter("callbackFunc");
                            String content = uri.getQueryParameter("content");
                            String title = uri.getQueryParameter("title");
                            JSONArray params = new JSONArray(uri.getQueryParameter("arr"));
                            HashMap<String, DialogInterface.OnClickListener> buttons = new HashMap<>();
                            for (int i = 0; i < params.length(); i++) {
                                final int index = i;
                                buttons.put(params.getString(i), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        webView.loadUrl("javascript:" + callbackFunc + "('" + index + "')");
                                    }
                                });
                            }
                            DialogUtil.showMultiButtonAlert(WebViewActivity.this, title, content, buttons);
                        } catch (Exception e) {
                        }
                    }
                } else if (url.contains(url_live_room)) {// 跳转直播房间
                    if (filterUrl(webView.getUrl())) {
                        try {
                            Uri uri = Uri.parse(url);
                            String liveId = uri.getQueryParameter("liveId");
                            String userId = uri.getQueryParameter("userId");
                            String isLandscape = uri.getQueryParameter("isLandscape");
                            Intent intent = new Intent();
                            if (TextUtils.isEmpty(isLandscape) || "0".equals(isLandscape)) {
                                intent.setClass(WebViewActivity.this, LiveWatchActivity.class);
                            } else {
                                intent.setClass(WebViewActivity.this, LiveWatchHorizontalActivity.class);
                            }
                            if (!TextUtils.isEmpty(liveId)) {
                                intent.putExtra(TheLConstants.BUNDLE_KEY_ID, liveId);
                                startActivity(intent);
                            } else if (!TextUtils.isEmpty(userId)) {
                                intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, userId);
                                startActivity(intent);
                            }
                        } catch (Exception e) {
                        }
                    }
                } else if (url.contains(url_bind_phone)) {
                    if (filterUrl(webView.getUrl())) {
                        LoginRegisterActivity.startActivity(TheLApp.getContext(), LoginRegisterActivity.PHONE_VERIFY, true);
                        try {
                            Uri bindedUrl = Uri.parse(url);
                            String redirectURL = bindedUrl.getQueryParameter("redirectURL");
                            ShareFileUtils.setString(ShareFileUtils.bindedPhoneUrl, redirectURL);
                        } catch (Exception e) {

                        }

                    }

                } else if (url.contains(url_share)) {// 分享页面
                    if (filterUrl(webView.getUrl())) {
                        try {
                            Uri uri = Uri.parse(url);
                            String title = uri.getQueryParameter("title");
                            String desc = uri.getQueryParameter("desc");
                            String jumpUrl = uri.getQueryParameter("url");
                            String logo = uri.getQueryParameter("imgUrl");
                            String type = uri.getQueryParameter("type");
                            if (SHARE_TYPE_IMAGE.equals(type)) {//4.0.0 社会化分享
                                Message msg = Message.obtain();
                                msg.what = SHOW_IMAGE_SHARE_DIALOG;
                                msg.obj = new DialogBean(title, desc, jumpUrl, logo);
                                mHandler.sendMessage(msg);
                            } else {
                                Message msg = Message.obtain();
                                msg.what = SHOW_DIALOG;
                                msg.obj = new DialogBean(title, desc, jumpUrl, logo);
                                mHandler.sendMessage(msg);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            Uri uri = Uri.parse(url);
                            String title = uri.getQueryParameter("title");
                            String desc = uri.getQueryParameter("desc");
                            String jumpUrl = uri.getQueryParameter("url");
                            String logo = uri.getQueryParameter("imgUrl");
                            String type = uri.getQueryParameter("type");
                            final DialogBean bean = new DialogBean(title, desc, jumpUrl, logo);
                            if (SHARE_TYPE_IMAGE.equals(type)) {//4.0.0 社会化分享
                                Message msg = Message.obtain();
                                msg.what = SHOW_IMAGE_SHARE_DIALOG;
                                msg.obj = new DialogBean(title, desc, jumpUrl, logo);
                                mHandler.sendMessage(msg);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else if (url.contains(url_write_video)) {
                    if (filterUrl(webView.getUrl())) {
                        mHandler.sendEmptyMessage(WRITE_VIDEO_MOMENT);
                    }
                } else if (url.contains(url_audio)) {

                    Intent intent = new Intent(WebViewActivity.this, MainActivity.class);

                    intent.putExtra(TheLConstants.BUNDLE_KEY_WHERE_TO_GO, TheLConstants.MainFragmentPageConstants.FRAGMENT_DISCOVER);

                    intent.putExtra("page", "voice");

                    startActivity(intent);

                } else if (url.contains(url_test_network)) {
                    Intent intent = new Intent(WebViewActivity.this, HelpActivity.class);
                    startActivity(intent);
                } else if (url.contains(url_walkman)) {
                    IWXAPI wxapi = WXAPIFactory.createWXAPI(WebViewActivity.this, TheLConstants.WX_APP_ID);
                    if (wxapi.isWXAppInstalled()) {

                        Uri uri = Uri.parse(url);

                        String path = uri.getQueryParameter("path");

                        WXLaunchMiniProgram.Req req = new WXLaunchMiniProgram.Req();
                        req.userName = TheLConstants.WX_MINIPROGRAM_RELA_WALKMAN_ID; // 填小程序原始id
                        req.path = path;//拉起小程序页面的可带参路径，不填默认拉起小程序首页
                        req.miniprogramType = WXLaunchMiniProgram.Req.MINIPTOGRAM_TYPE_RELEASE;// 可选打开 开发版，体验版和正式版
                        wxapi.sendReq(req);
                    } else {
                        ToastUtils.showToastShort(WebViewActivity.this, "wechat not install");
                    }

                } else if (url.contains(url_live_horizontal)) {
                    try {
                        Uri uri = Uri.parse(url);
                        String liveId = uri.getQueryParameter("liveId");
                        String isLandscape = uri.getQueryParameter("isLandscape");
                        Intent intent;
                        intent = new Intent(TheLApp.context, LiveWatchHorizontalActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra(TheLConstants.BUNDLE_KEY_ID, liveId);
                        intent.putExtra(LiveWatchActivity.FROM_PAGE, LiveWatchActivity.FROM_PAGE_MOMENT);
                        TheLApp.context.startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (url.contains(url_getPicData)) {//保存图片
                    try {
                        PermissionUtil.requestStoragePermission(WebViewActivity.this, new PermissionUtil.PermissionCallback() {
                            @Override
                            public void granted() {
                                Uri uri = Uri.parse(url);
                                String picDataCallback = uri.getQueryParameter("callback");
                                finishCallback = uri.getQueryParameter("finish");
                                webView.evaluateJavascript("javascript:" + picDataCallback + "()", new ValueCallback<String>() {
                                    @Override
                                    public void onReceiveValue(String value) {
                                        stringtoBitmapAndSave(value, "");
                                    }
                                });
                            }

                            @Override
                            public void denied() {

                            }

                            @Override
                            public void gotoSetting() {

                            }
                        });

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (url.contains(url_sharepic)) {//分享图片
                    try {
                        PermissionUtil.requestStoragePermission(WebViewActivity.this, new PermissionUtil.PermissionCallback() {
                            @Override
                            public void granted() {
                                Uri uri = Uri.parse(url);
                                String type = uri.getQueryParameter("type");
                                String picDataCallback = uri.getQueryParameter("callback");
                                webView.evaluateJavascript("javascript:" + picDataCallback + "()", new ValueCallback<String>() {
                                    @Override
                                    public void onReceiveValue(String value) {
                                        stringtoBitmapAndSave(value, type);
                                    }
                                });
                            }

                            @Override
                            public void denied() {

                            }

                            @Override
                            public void gotoSetting() {

                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (url.contains(url_recording_start)) {
                    if (ContextCompat.checkSelfPermission(WebViewActivity.this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED
                            && ContextCompat.checkSelfPermission(WebViewActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        startRecording();
                    } else
                        PermissionUtil.requestRecordPermission(WebViewActivity.this, new PermissionUtil.PermissionCallback() {
                            @Override
                            public void granted() {
                                PermissionUtil.requestStoragePermission(WebViewActivity.this, new PermissionUtil.PermissionCallback() {
                                    @Override
                                    public void granted() {

                                    }

                                    @Override
                                    public void denied() {

                                    }

                                    @Override
                                    public void gotoSetting() {

                                    }
                                });
                            }

                            @Override
                            public void denied() {

                            }

                            @Override
                            public void gotoSetting() {

                            }
                        });
                } else if (url.contains(url_recording_end)) {
                    try {
                        Uri uri = Uri.parse(url);
                        recordingCallback = uri.getQueryParameter("callback");
                        stopRecording();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    if (url.matches(TheLConstants.URL_PATTERN_STR))
                        webView.loadUrl(url);
                }

                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                String title = view.getTitle();
                if (title == null) {
                    title = "";
                }
                //                if (!"ad".equals(site))
                //                    txt_title.setText(title);
                //                dumpUrl = url;
                refreshBtns();
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
            }
        });

        if (site != null) {
            if (site.equals("sina")) {
                webView.loadUrl(TheLConstants.THEL_SINA_SITE);
                txt_title.setText(TheLApp.getContext().getString(R.string.webview_activity_sina));
            } else if (site.equals("instagram")) {
                webView.loadUrl(TheLConstants.THEL_INS_SITE);
                txt_title.setText("Instagram");
            } else if (site.equals("fb")) {
                webView.loadUrl(TheLConstants.THEL_FB_SITE);
                txt_title.setText("Facebook");
            } else if (site.equals("help")) {
                ll_recommend.setVisibility(View.GONE);
                webView.loadUrl(TheLConstants.THEL_HELP_SITE);
            } else if (site.equals("douban_cooperator")) {
                webView.loadUrl(TheLConstants.THEL_DOUBAN_COOPERATOR);
                txt_title.setText(TheLApp.getContext().getString(R.string.webview_activity_cooperater));
            } else if (site.equals("sina_user")) {
                webView.loadUrl("http://m.weibo.cn/u/" + uid);
                txt_title.setText(TheLApp.getContext().getString(R.string.webview_activity_sina));
            } else if (site.equals("ad") || site.equals("thelSite")) {
                img_more.setVisibility(View.GONE);
                dumpUrl = getIntent().getStringExtra("url");
                webView.loadUrl(dumpUrl);
                title = getIntent().getStringExtra("title");
                txt_title.setText(title);
                adUrl = dumpUrl;
            } else {
                loadOtherUrl();
            }
        } else {
            loadOtherUrl();
        }

        AndroidBug5497Workaround.assistActivity(this);

    }

    private WebViewReceiver webViewReceiver;

    // 注册广播接收器
    private void registerReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(TheLConstants.BROADCAST_WX_SHARE);
        webViewReceiver = new WebViewReceiver();
        this.registerReceiver(webViewReceiver, intentFilter);
    }

    // private Pattern pattern = Pattern.compile("(?<=http://|https://|\\.)[^.]*?\\.(co[a-z]*)", Pattern.CASE_INSENSITIVE);
    // private Pattern pattern = Pattern.compile("(?<=http://|https://|\\.)[^.]*?\\.(co[a-z]*)", Pattern.CASE_INSENSITIVE);

    private String pst = "^https?://([^/\\?]*)/?.*";
    private Pattern pattern = Pattern.compile(pst, Pattern.CASE_INSENSITIVE);
    private final String HOST_1 = ".rela.me";
    private final String HOST_2 = ".thel.co";
    private final String HOST_3 = "rela.me";
    private final String HOST_4 = "thel.co";

    /**
     * 如果是thel的页面，则返回true，否则false
     *
     * @param url
     * @return
     */
    private boolean filterUrl(String url) {
        try {
            final String host = Uri.parse(url).getHost();
            return host.endsWith(HOST_1) || host.endsWith(HOST_2) || host.equals(HOST_3) || host.equals(HOST_4);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private String buildCallbackParam(String userId, String nickName, String avatar, String key, String language) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("code", "1");
            JSONObject user = new JSONObject();
            user.put("userId", userId);
            if (!TextUtils.isEmpty(nickName)) {
                user.put("nickName", nickName);
            }
            if (!TextUtils.isEmpty(avatar)) {
                user.put("avatar", avatar);
            }
            if (!TextUtils.isEmpty(key)) {
                user.put("jskey", key);
            }
            if (!TextUtils.isEmpty(language)) {
                user.put("language", language);
            }
            jsonObject.put("data", user);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString().replaceAll("\\\\", "\\\\\\\\").replaceAll("\n", "").replaceAll("'", "\\\\\'");
    }

    //        private String buildCommonCallbackParam(VolleyError error, String result) {
    //            JSONObject resultObj = new JSONObject();
    //            if (error != null) {
    //                try {
    //                    resultObj.put("statusCode", error.networkResponse.statusCode);
    //                    if (error.getMessage() != null)
    //                        resultObj.put("message", error.getMessage());
    //                    else
    //                        resultObj.put("message", "");
    //                    resultObj.put("data", "");
    //                } catch (Exception e)
    //                    e.printStackTrace();
    //                }
    //            } else {
    //                try {
    //                    resultObj.put("statusCode", 200);
    //                    resultObj.put("message", "");
    //                    resultObj.put("data", result);
    //                } catch (Exception e) {
    //                    e.printStackTrace();
    //                }
    //            }
    //            return resultObj.toString().replaceAll("\\\\", "\\\\\\\\").replaceAll("\n", "").replaceAll("'", "\\\\\'");
    //        }

    private void refreshBtns() {
        if (webView.canGoBack()) {// 刷新返回按钮
            bottom_back.setAlpha(1f);
        } else {
            bottom_back.setAlpha(0.3f);
        }
        if (webView.canGoForward()) {// 刷新前进按钮
            bottom_next.setAlpha(1f);
        } else {
            bottom_next.setAlpha(0.3f);
        }
    }

    protected void setListener() {
        lin_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                PushUtils.finish(WebViewActivity.this);
            }
        });

        lin_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (webView.canGoBack()) {// 如果网页可以后退则后退
                    webView.goBack();
                } else {
                    finish();

                }
            }
        });
        bottom_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (webView.canGoBack()) {// 如果网页可以后退则后退
                    webView.goBack();
                }
            }
        });

        bottom_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (webView.canGoForward()) {// 如果网页可以前进则前进
                    webView.goForward();
                }
            }
        });

        bottom_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                webView.reload();
                rel_dialog.setVisibility(View.GONE);
            }
        });

        bottom_copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(webView.getUrl())) {
                    DeviceUtils.copyToClipboard(WebViewActivity.this, webView.getUrl());
                    DialogUtil.showToastShort(WebViewActivity.this, getThelString(R.string.info_copied));
                }
                rel_dialog.setVisibility(View.GONE);
            }
        });

        bottom_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rel_dialog.setVisibility(View.GONE);
            }
        });
        findViewById(R.id.bottom_shade).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rel_dialog.setVisibility(View.GONE);
            }
        });
        rel_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        img_recommend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                recommendWeb();
            }
        });

        img_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogUtil.getInstance().showSelectionDialog(WebViewActivity.this, new String[]{getString(R.string.webview_activity_open_in_browser), getString(R.string.webview_activity_copy_url)}, new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> adapter, View view, int position, long arg3) {
                        ViewUtils.preventViewMultipleClick(view, 2000);
                        DialogUtil.getInstance().closeDialog();
                        switch (position) {
                            case 0:
                                if (!TextUtils.isEmpty(webView.getUrl())) {
                                    Intent intent = new Intent();
                                    intent.setAction("android.intent.action.VIEW");
                                    Uri url = Uri.parse(webView.getUrl());
                                    intent.setData(url);
                                    startActivity(intent);
                                }
                                break;
                            case 1:
                                if (!TextUtils.isEmpty(webView.getUrl())) {
                                    DeviceUtils.copyToClipboard(WebViewActivity.this, webView.getUrl());
                                    DialogUtil.showToastShort(WebViewActivity.this, getString(R.string.info_copied));
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }, false, 2, null);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);

        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    protected void setLayout() {
        this.setContentView(R.layout.fragment_web);
    }

    protected void findViewById() {
        progressBar = this.findViewById(R.id.progressBar);
        webView = this.findViewById(R.id.weibview);
        rel_title = this.findViewById(R.id.rel_title);
        txt_title = this.findViewById(R.id.txt_title);
        lin_back = this.findViewById(R.id.lin_back);
        lin_previous = this.findViewById(R.id.lin_previous);
        img_more = this.findViewById(R.id.img_more);
        //2.13改版分享对话框为隐藏view
        rel_dialog = this.findViewById(R.id.rel_dialog);
        rel_bottom = this.findViewById(R.id.rel_bottom);
        bottom_back = this.findViewById(R.id.bottom_back);
        bottom_next = this.findViewById(R.id.bottom_next);
        bottom_refresh = this.findViewById(R.id.bottom_refresh);
        bottom_copy = findViewById(R.id.bottom_copy);
        bottom_cancel = findViewById(R.id.bottom_cancel);
        gridView = findViewById(R.id.gridView);
        bottom_title = findViewById(R.id.bottom_title);
        rel_dialog.setVisibility(View.GONE);
        img_recommend = findViewById(R.id.img_recommend);//网页推荐
        ll_recommend = findViewById(R.id.ll_recommend);
        if (!TextUtils.isEmpty(url) && url.equals(TheLConstants.USER_AGREEMENT_PAGE_URL)) {
            ll_recommend.setVisibility(View.INVISIBLE);
            lin_back.setVisibility(View.INVISIBLE);
        } else if (!TextUtils.isEmpty(url) && url.equals(TheLConstants.LOGIN_HELP_RUL)) {
            ll_recommend.setVisibility(View.INVISIBLE);
        }
        flutter_container_fl = findViewById(R.id.flutter_container_fl);
    }

    @Override
    protected void onResume() {
        if ("thelSite".equals(site) && !TextUtils.isEmpty(adUrl))// 如果是热拉周刊、热拉tv等主推版块的话，要将url作为路经统计名称
            MobclickAgent.onPageStart(adUrl);
        else
            MobclickAgent.onPageStart(this.getClass().getName());
        MobclickAgent.onResume(this);
        super.onResume();
        if (webView != null) {
            webView.onResume();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if ("thelSite".equals(site) && !TextUtils.isEmpty(adUrl))// 如果是热拉周刊、热拉tv等主推版块的话，要将url作为路经统计名称
            MobclickAgent.onPageEnd(adUrl);
        else
            MobclickAgent.onPageEnd(this.getClass().getName());
        MobclickAgent.onPause(this);
        if (webView != null) {
            webView.onPause();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(webViewReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        if (webView != null) {
            webView.destroy();
        }
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    @Override
    public void onAddFragment() {

        //处理闪屏问题
        flutter_container_fl.setAlpha(0.01f);

        NewFlutterFragment newFlutterFragment = (NewFlutterFragment) getSupportFragmentManager().findFragmentByTag("webview_NewFlutterFragment");

        if (newFlutterFragment == null) {
            newFlutterFragment = new NewFlutterFragment.NewEngineFragmentBuilder().transparencyMode(FlutterView.TransparencyMode.transparent).renderMode(FlutterView.RenderMode.surface).build();
        }

        getSupportFragmentManager().beginTransaction().add(R.id.flutter_container_fl, newFlutterFragment, "webview_NewFlutterFragment").commit();


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                flutter_container_fl.setAlpha(1f);

            }
        }, 1500);

    }

    @Override
    public void onRemoveFragment() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("webview_NewFlutterFragment");
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction().remove(fragment).commit();
            flutter_container_fl.setAlpha(1);
        }
    }

    final class InJavaScriptLocalObj {
        @JavascriptInterface
        public void showSource(String html) {
            if (!TextUtils.isEmpty(html)) {
                try {

                    L.d("WebViewActivity", " html : " + html);

                    JSONObject jsonObject = new JSONObject(html);
                    shareLogo = JsonUtils.getString(jsonObject, "img", "");
                    String language = DeviceUtils.getLanguage();
                    if ("zh_CN".equals(language)) {
                        shareTitle = JsonUtils.getString(jsonObject, "title_zh_CN", "");
                        shareContent = JsonUtils.getString(jsonObject, "content_zh_CN", "");
                    } else if ("zh_TW".equals(language)) {
                        shareTitle = JsonUtils.getString(jsonObject, "title_zh_TW", "");
                        shareContent = JsonUtils.getString(jsonObject, "content_zh_TW", "");
                    } else {
                        shareTitle = JsonUtils.getString(jsonObject, "title_en_US", "");
                        shareContent = JsonUtils.getString(jsonObject, "content_en_US", "");
                    }
                    String url = JsonUtils.getString(jsonObject, "url", "");
                    if (!TextUtils.isEmpty(url))
                        dumpUrl = url;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            Message msg = Message.obtain();
            msg.what = SHOW_DIALOG;
            msg.obj = new DialogBean(shareTitle, shareContent, dumpUrl, shareLogo);
            mHandler.sendMessage(msg);
        }
    }

    private void shareResult(final int succeed) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("code", succeed + "");
                    if (succeed == 1) {
                        jsonObject.put("msg", "分享成功");
                    } else {
                        jsonObject.put("msg", "分享失败");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                webView.loadUrl("javascript:" + shareSucceedCallback + "('" + jsonObject.toString() + "')");
            }
        });
    }

    private void loadOtherUrl() {

        Bundle bundle = getIntent().getExtras();

        String dumpUrl = null;

        if (bundle != null) {
            dumpUrl = bundle.getString(URL, null);
        }

        /**4.19.1跳转热拉游乐园*/
        if (TextUtils.isEmpty(dumpUrl)) {
            //scheme跳转WebviewActivity
            Uri uri = getIntent().getData();
            dumpUrl = uri.getQueryParameter("addUrl");
        }

        if (TextUtils.isEmpty(dumpUrl))
            return;
        if (dumpUrl.startsWith("www.")) {
            dumpUrl = "http://" + dumpUrl;
        }

        try {
            if (getIntent().getBooleanExtra(NEED_SECURITY_CHECK, true))
                dumpUrl = security_check + URLEncoder.encode(dumpUrl, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }


        L.d("WebViewActivity", " dumpUrl : " + dumpUrl);

        webView.loadUrl(dumpUrl);
        if (!TextUtils.isEmpty(getIntent().getStringExtra("title"))) {
            title = getIntent().getStringExtra("title");
            txt_title.setText(title);
        }
    }

    private class WebViewReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent i) {
            if (i != null) {
                if (TheLConstants.BROADCAST_WX_SHARE.equals(i.getAction())) {// 微信、微博分享成功消息
                    if ("succeed".equals(i.getStringExtra("result"))) {
                        shareResult(1);
                    } else {
                        shareResult(0);
                    }
                }
            }

        }
    }

    private void showDialog(DialogBean dialogBean) {
        rel_dialog.setVisibility(View.VISIBLE);
        final FacebookCallback facebookCallback = new FacebookCallback() {
            @Override
            public void onSuccess(Object o) {
                shareResult(1);
            }

            @Override
            public void onCancel() {
                shareResult(0);
            }

            @Override
            public void onError(FacebookException error) {
                shareResult(0);
            }
        };
        String title = getString(R.string.share_title_event);
        final String shareTitle = TextUtils.isEmpty(dialogBean.title) ? "Rela热拉" : dialogBean.title;
        final String shareContent = TextUtils.isEmpty(dialogBean.content) ? getString(R.string.share_content_event) : dialogBean.content;
        final String singleTitle = TextUtils.isEmpty(dialogBean.title) ? getString(R.string.share_content_event) : dialogBean.title;
        final String url = dialogBean.url;
        final String logo = TextUtils.isEmpty(dialogBean.logo) ? TheLConstantsExt.DEFAULT_SHARE_LOGO_URL : dialogBean.logo;
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    switch (position) {
                        case 0:// 微信朋友圈
                            MobclickAgent.onEvent(WebViewActivity.this, "start_share");
                            String newurl = getnewurl(url, "wx_timeline");
                            //new ShareAction(WebViewActivity.this).setPlatform(SHARE_MEDIA.WEIXIN_CIRCLE).withMedia(new UMImage(TheLApp.getContext(), logo)).withTargetUrl(newurl).withTitle(singleTitle).withText(singleTitle).setCallback(umShareListener).share();
                            UMShareUtils.share(WebViewActivity.this, SHARE_MEDIA.WEIXIN_CIRCLE, new UMImage(TheLApp.getContext(), ImageUtils.buildNetPictureUrl(logo, TheLConstants.WX_SHARE_IMAGE_SIZE, TheLConstants.WX_SHARE_IMAGE_SIZE)), newurl, singleTitle, singleTitle, umShareListener);

                            break;
                        case 1:// 微信消息
                            newurl = getnewurl(url, "wx_appmsg");
                            MobclickAgent.onEvent(WebViewActivity.this, "start_share");
                            //new ShareAction(WebViewActivity.this).setPlatform(SHARE_MEDIA.WEIXIN).withMedia(new UMImage(TheLApp.getContext(), logo)).withTargetUrl(newurl).withTitle(shareTitle).withText(shareContent).setCallback(umShareListener).share();
                            UMShareUtils.share(WebViewActivity.this, SHARE_MEDIA.WEIXIN, new UMImage(TheLApp.getContext(), ImageUtils.buildNetPictureUrl(logo, TheLConstants.WX_SHARE_IMAGE_SIZE, TheLConstants.WX_SHARE_IMAGE_SIZE)), newurl, shareTitle, shareContent, umShareListener);
                            break;
                        case 2:// 微博分享
                            newurl = getnewurl(url, "weibo");

                            MobclickAgent.onEvent(WebViewActivity.this, "start_share");
                            //   new ShareAction(WebViewActivity.this).setPlatform(SHARE_MEDIA.SINA).withMedia(new UMImage(TheLApp.getContext(), logo)).withTargetUrl(newurl).withTitle(singleTitle).withText(singleTitle).setCallback(umShareListener).share();
                            UMShareUtils.share(WebViewActivity.this, SHARE_MEDIA.SINA, new UMImage(TheLApp.getContext(), logo), newurl, singleTitle, singleTitle, umShareListener);
                            break;
                        case 3:// QQ空间
                            newurl = getnewurl(url, "qq_zone qq");

                            //   new ShareAction(WebViewActivity.this).setPlatform(SHARE_MEDIA.QZONE).withMedia(new UMImage(TheLApp.getContext(), logo)).withTargetUrl(newurl).withTitle(shareTitle).withText(shareContent).setCallback(umShareListener).share();
                            UMShareUtils.share(WebViewActivity.this, SHARE_MEDIA.QZONE, new UMImage(TheLApp.getContext(), logo), newurl, shareTitle, shareContent, umShareListener);
                            break;
                        case 4:// Facebook
                            newurl = getnewurl(url, "facebook");

                            MobclickAgent.onEvent(WebViewActivity.this, "start_share");
                            if (ShareDialog.canShow(ShareLinkContent.class)) {
                                ShareLinkContent contentS = new ShareLinkContent.Builder().setContentUrl(Uri.parse(newurl)).setContentTitle(shareTitle).setContentDescription(shareContent).setImageUrl(Uri.parse(logo)).build();
                                ShareDialog shareDialog = new ShareDialog(WebViewActivity.this);
                                if (callbackManager != null && facebookCallback != null)
                                    shareDialog.registerCallback(callbackManager, facebookCallback);
                                shareDialog.show(contentS);
                            }
                            break;
                        case 5:// QQ消息
                            newurl = getnewurl(url, "qq_appmsg");

                            MobclickAgent.onEvent(WebViewActivity.this, "start_share");
                            //   new ShareAction(WebViewActivity.this).setPlatform(SHARE_MEDIA.QQ).withMedia(new UMImage(TheLApp.getContext(), logo)).withTargetUrl(newurl).withTitle(shareTitle).withText(shareContent).setCallback(umShareListener).share();
                            UMShareUtils.share(WebViewActivity.this, SHARE_MEDIA.QQ, new UMImage(TheLApp.getContext(), logo), newurl, shareTitle, shareContent, umShareListener);
                            break;
                        case 6:
                            Intent intent = new Intent();
                            intent.setAction("android.intent.action.VIEW");
                            Uri uri = Uri.parse(url);
                            intent.setData(uri);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            TheLApp.getContext().startActivity(intent);
                            break;
                        default:
                            break;
                    }
                    rel_dialog.setVisibility(View.GONE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });
        bottom_title.setText(title);
        List<Integer> images = new ArrayList<Integer>();
        images.add(R.drawable.btn_share_moments_s);
        images.add(R.drawable.btn_share_wx_s);
        images.add(R.drawable.btn_share_sina_s);
        images.add(R.drawable.btn_share_qzone_s);
        images.add(R.drawable.btn_share_fb_s);
        images.add(R.drawable.btn_share_qq_s);
        List<String> texts = new ArrayList<String>();
        texts.add(TheLApp.getContext().getString(R.string.share_moments));
        texts.add(TheLApp.getContext().getString(R.string.share_wx));
        texts.add(TheLApp.getContext().getString(R.string.share_sina));
        texts.add(TheLApp.getContext().getString(R.string.share_qzone));
        texts.add(TheLApp.getContext().getString(R.string.share_fb));
        texts.add(TheLApp.getContext().getString(R.string.share_qq));
        images.add(R.drawable.btn_share_browser_s);
        texts.add(TheLApp.getContext().getString(R.string.webview_activity_open_in_browser));
        //4.0.0 下载图片
        // images.add(R.drawable.btn_sharesocial_download);
        //texts.add(TheLApp.getContext().getString(R.string.download_pic));
        gridView.setAdapter(new ShareGridAdapter(images, texts, gridView));
    }

    /**
     * 4.0.0，下载图片
     *
     * @param logo
     */
    private void downloadPic(String logo) {
        Utils.savePicFromBase64(logo);
    }

    private String getnewurl(String url, String addurl) {
        String newurl = url;
        if (url.contentEquals("?")) {
            newurl = url + "&rela_webview_share=" + addurl;
        } else {
            newurl = url + "?rela_webview_share=" + addurl;
        }
        return newurl;
    }


    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case SHOW_DIALOG:
                    DialogBean bean = (DialogBean) msg.obj;
                    showDialog(bean);
                    break;
                case WRITE_VIDEO_MOMENT:
                    gotoWriteVideoMoment();
                    break;
                case SHOW_IMAGE_SHARE_DIALOG:
                    DialogBean bean1 = (DialogBean) msg.obj;
                    showShareImageDialog(bean1);
                    break;
            }
        }
    };

    private void gotoWriteVideoMoment() {
        Intent intent = new Intent(TheLApp.getContext(), ReleaseMomentActivity.class);
        intent.putExtra(TheLConstants.BUNDLE_KEY_RELEASE_TYPE, ReleaseMomentActivity.RELEASE_TYPE_VIDEO);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        TheLApp.getContext().startActivity(intent);
    }

    private class DialogBean {
        public String title;
        public String content;
        public String url;
        public String logo;

        public DialogBean(String title, String content, String url, String logo) {
            this.content = content;
            this.logo = logo;
            this.title = title;
            this.url = url;
        }

        @Override
        public String toString() {
            return "DialogBean{" + "title='" + title + '\'' + ", content='" + content + '\'' + ", url='" + url + '\'' + ", logo='" + logo + '\'' + '}';
        }
    }

    private String recommendWebJs = "function getDefaultImg(width, height) { \n" + "var reg = /(http[s]?|ftp):\\/\\/[^\\/.]+?..+\\w$/g \n" + "var imgs = document.getElementsByTagName('img'); \n" + "for (var i = 0; i < imgs.length; i++) { \n" + "var img = imgs[i]; \n" + "var w = img.width; \n" + "var h = img.height; \n" + "if (w >= width && h >= height) { \n" + "var src = img.src; \n" + "if (reg.test(src)) return src; \n" + "} \n" + "} \n" + "return ''; \n" + "}";


    private String recommendWebJs2 = "function getDefaultImg(b,k){" + "var c=/(http[s]?|ftp):\\/\\/[^\\/\\.]+?\\..+\\w$/g;" + "var g=document.getElementsByTagName(\"img\");" + "for(var e=0;e<g.length;e++)" + "{var d=g[e];var j=d.width;var f=d.height;" + "if(j>=b&&f>=k)" + "{var a=d.src;if(c.test(a)){return a}}}" + "return\"\"};";

    private class InJavaScriptRecomendWeb {
        @JavascriptInterface
        public void recommendImage(String image) {
        }

    }


    /**
     * 推荐网页
     */
    private void recommendWeb() {
        inSertGetImageJs();//注入js
        getJsImage();
    }

    /**
     *
     */
    private void getJsImage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            webView.evaluateJavascript("getDefaultImg(300, 300)", new ValueCallback<String>() {
                @Override
                public void onReceiveValue(String value) {
                    L.d("refresh", "getJsImage:value=" + value);
                    String imageUrl = getUrlByValue(value);
                    L.d("refresh", "getJsImage:value=" + imageUrl);
                    String title = webView.getTitle();
                    RecommendedModel recommendedModel = new RecommendedModel();
                    recommendedModel.recommendedType = "web";
                    recommendedModel.momentsType = MomentsBean.MOMENT_TYPE_WEB;
                    recommendedModel.imageUrl = imageUrl;
                    recommendedModel.title = title;
                    recommendedModel.url = webView.getUrl();
                    recommendedModel.momentsText = shareContent;
                    FlutterRouterConfig.Companion.nativeShowRecommendDialog(GsonUtils.createJsonString(recommendedModel));
                }
            });
        }
    }

    private String getUrlByValue(String value) {
        if (value.startsWith("\"")) {
            value = value.substring(1);
        }
        if (value.endsWith("\"")) {
            value = value.substring(0, value.length() - 1);

        }
        return value;
    }

    /**
     * 获取图片
     */
    private void inSertGetImageJs() {
        webView.loadUrl("javascript:" + recommendWebJs2);//注入js

    }

    private String getImageWithParma() {

        return "getDefaultImg('300,300')";
    }

    /**
     * 4.0.0, 图片社会化分享
     *
     * @param dialogBean
     */
    private void showShareImageDialog(final DialogBean dialogBean) {
        if (TextUtils.isEmpty(dialogBean.logo)) {
            return;
        }
        byte[] d = null;
        try {
            d = Base64.decode(dialogBean.logo, Base64.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        if (null == d) {
            return;
        }
        final byte[] data = d;
        rel_dialog.setVisibility(View.VISIBLE);
        final FacebookCallback facebookCallback = new FacebookCallback() {
            @Override
            public void onSuccess(Object o) {
                shareResult(1);
            }

            @Override
            public void onCancel() {
                shareResult(0);
            }

            @Override
            public void onError(FacebookException error) {
                shareResult(0);
            }
        };
        String title = getString(R.string.share_title_event);
        final String shareTitle = TextUtils.isEmpty(dialogBean.title) ? "Rela热拉" : dialogBean.title;
        final String shareContent = TextUtils.isEmpty(dialogBean.content) ? getString(R.string.share_content_event) : dialogBean.content;
        final String singleTitle = TextUtils.isEmpty(dialogBean.title) ? getString(R.string.share_content_event) : dialogBean.title;
        final String url = dialogBean.url;
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    switch (position) {
                        case 0:// 微信朋友圈
                            MobclickAgent.onEvent(WebViewActivity.this, "start_share");
                            String newurl = getnewurl(url, "wx_timeline");
                            //new ShareAction(WebViewActivity.this).setPlatform(SHARE_MEDIA.WEIXIN_CIRCLE).withMedia(new UMImage(TheLApp.getContext(), logo)).withTargetUrl(newurl).withTitle(singleTitle).withText(singleTitle).setCallback(umShareListener).share();
                            UMShareUtils.share(WebViewActivity.this, SHARE_MEDIA.WEIXIN_CIRCLE, new UMImage(TheLApp.getContext(), data), umShareListener);

                            break;
                        case 1:// 微信消息
                            newurl = getnewurl(url, "wx_appmsg");
                            MobclickAgent.onEvent(WebViewActivity.this, "start_share");
                            //new ShareAction(WebViewActivity.this).setPlatform(SHARE_MEDIA.WEIXIN).withMedia(new UMImage(TheLApp.getContext(), logo)).withTargetUrl(newurl).withTitle(shareTitle).withText(shareContent).setCallback(umShareListener).share();
                            UMShareUtils.share(WebViewActivity.this, SHARE_MEDIA.WEIXIN, new UMImage(TheLApp.getContext(), data), umShareListener);
                            break;
                        case 2:// 微博分享
                            newurl = getnewurl(url, "weibo");

                            MobclickAgent.onEvent(WebViewActivity.this, "start_share");
                            //   new ShareAction(WebViewActivity.this).setPlatform(SHARE_MEDIA.SINA).withMedia(new UMImage(TheLApp.getContext(), logo)).withTargetUrl(newurl).withTitle(singleTitle).withText(singleTitle).setCallback(umShareListener).share();
                            UMShareUtils.share(WebViewActivity.this, SHARE_MEDIA.SINA, new UMImage(TheLApp.getContext(), data), umShareListener);
                            break;
                        case 3:// QQ空间
                            newurl = getnewurl(url, "qq_zone qq");

                            //   new ShareAction(WebViewActivity.this).setPlatform(SHARE_MEDIA.QZONE).withMedia(new UMImage(TheLApp.getContext(), logo)).withTargetUrl(newurl).withTitle(shareTitle).withText(shareContent).setCallback(umShareListener).share();
                            UMShareUtils.share(WebViewActivity.this, SHARE_MEDIA.QZONE, new UMImage(TheLApp.getContext(), data), umShareListener);
                            break;
                        case 4:// Facebook
                            newurl = getnewurl(url, "facebook");

                            MobclickAgent.onEvent(WebViewActivity.this, "start_share");
                            if (ShareDialog.canShow(SharePhotoContent.class)) {
                               /* ShareLinkContent contentS = new ShareLinkContent.Builder().setContentUrl(Uri.parse(newurl)).setContentTitle(shareTitle).setContentDescription(shareContent).setImageUrl(Uri.parse(logo)).build();
                                ShareDialog shareDialog = new ShareDialog(WebViewActivity.this);
                                if (callbackManager != null && facebookCallback != null)
                                    shareDialog.registerCallback(callbackManager, facebookCallback);
                                shareDialog.show(contentS)*/
                                /**4.0.0 facebook分享图片**/
                                SharePhoto photo = new SharePhoto.Builder().setBitmap(Utils.decodeBitmap(data)).build();
                                SharePhotoContent content = new SharePhotoContent.Builder().addPhoto(photo).build();
                            }
                            break;
                        case 5:// QQ消息
                            newurl = getnewurl(url, "qq_appmsg");

                            MobclickAgent.onEvent(WebViewActivity.this, "start_share");
                            //   new ShareAction(WebViewActivity.this).setPlatform(SHARE_MEDIA.QQ).withMedia(new UMImage(TheLApp.getContext(), logo)).withTargetUrl(newurl).withTitle(shareTitle).withText(shareContent).setCallback(umShareListener).share();
                            UMShareUtils.share(WebViewActivity.this, SHARE_MEDIA.QQ, new UMImage(TheLApp.getContext(), data), umShareListener);
                            break;
                        case 6:
                            downloadPic(dialogBean.logo);
                            break;

                        default:
                            break;
                    }
                    rel_dialog.setVisibility(View.GONE);
                } catch (Exception e) {
                }
            }

        });
        bottom_title.setText(title);
        List<Integer> images = new ArrayList<Integer>();
        images.add(R.drawable.btn_share_moments_s);
        images.add(R.drawable.btn_share_wx_s);
        images.add(R.drawable.btn_share_sina_s);
        images.add(R.drawable.btn_share_qzone_s);
        images.add(R.drawable.btn_share_fb_s);
        images.add(R.drawable.btn_share_qq_s);
        List<String> texts = new ArrayList<String>();
        texts.add(TheLApp.getContext().getString(R.string.share_moments));
        texts.add(TheLApp.getContext().getString(R.string.share_wx));
        texts.add(TheLApp.getContext().getString(R.string.share_sina));
        texts.add(TheLApp.getContext().getString(R.string.share_qzone));
        texts.add(TheLApp.getContext().getString(R.string.share_fb));
        texts.add(TheLApp.getContext().getString(R.string.share_qq));
        /** 4.0.0 下载图片**/
        texts.add(TheLApp.getContext().getString(R.string.download_pic));
        images.add(R.mipmap.btn_sharesocial_download);

        gridView.setAdapter(new ShareGridAdapter(images, texts, gridView));
    }

    /**
     * 开始录音
     */
    private void startRecording() {

        try {

            File fileDir = new File(TheLConstants.F_MSG_VOICE_ROOTPATH + UserUtils.getMyUserId());
            if (!fileDir.exists()) {
                fileDir.mkdirs();
            }

            if (mRecorder == null) {
                mRecorder = new MediaRecorder();
            } else {
                mRecorder.reset();
            }

            mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            mRecorder.setOutputFile(recordingPath);
            mRecorder.prepare();
            mRecorder.start();
            mRecorderStartTime = System.currentTimeMillis();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 停止录音
     */
    private void stopRecording() {
        if (mRecorder != null) {
            try {
                mRecorderEndTime = System.currentTimeMillis();
                mRecorder.stop();
                mRecorder.release();
                mRecorder = null;
                uploadFile();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void uploadFile() {

        String uploadPath = RequestConstants.UPLOAD_FILE_ROOT_PATH_VOICE + ShareFileUtils.getString(ShareFileUtils.ID, "") + "/" + MD5Utils.calculateMD5(new File(recordingPath)) + ".mp3";
        RequestBusiness.getInstance().getUploadToken(System.currentTimeMillis() + "", "", uploadPath)
                .onBackpressureDrop().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new InterceptorSubscribe<UploadTokenBean>() {
                    @Override
                    public void onNext(UploadTokenBean uploadTokenBean) {
                        super.onNext(uploadTokenBean);
                        if (!TextUtils.isEmpty(uploadTokenBean.data.uploadToken)) {
                            // 上传日志图片
                            UploadManager uploadManager = new UploadManager();
                            File file = new File(recordingPath);
                            if (!file.exists()) {
                                L.d("file path not exists");
                            }
                            uploadManager.put(file, uploadPath, uploadTokenBean.data.uploadToken, new UpCompletionHandler() {
                                @Override
                                public void complete(String key, ResponseInfo info, JSONObject response) {

                                    if (info != null && info.statusCode == 200 && uploadPath.equals(key)) {

                                        // 发送语音或图片url
                                        String url = RequestConstants.FILE_BUCKET + key;

                                        long time = mRecorderEndTime - mRecorderStartTime;

                                        Log.d("uploadFile", "url: " + url + "---time: " + time);
                                        webView.loadUrl("javascript:" + recordingCallback + "('" + url + "','" + time + "')");

                                    } else {
                                        webView.loadUrl("javascript:" + recordingCallback + "('')");
                                    }
                                }
                            }, null);
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                        webView.loadUrl("javascript:" + recordingCallback + "('')");
                    }
                });
    }

    /**
     * 将base64转换成bitmap图片
     *
     * @param base64 base64字符串
     * @return bitmap
     */

    private void stringtoBitmapAndSave(String base64, String type) {
        Bitmap bitmap = null;
        try {

            byte[] bitmapArray;

            bitmapArray = Base64.decode(base64, Base64.DEFAULT);

            bitmap = BitmapFactory.decodeByteArray(bitmapArray, 0, bitmapArray.length);

            Date date = new Date(System.currentTimeMillis());

            SimpleDateFormat dateFormat = new SimpleDateFormat("'IMG'_yyyyMMdd_HHmmss");

            //参数三：以当前时间作为图片名称。参数四：图片描述
            String insertImage = MediaStore.Images.Media.insertImage(WebViewActivity.this.getContentResolver(), bitmap, dateFormat.format(date), null);
            File file = new File(getRealPathFromURI(Uri.parse(insertImage), WebViewActivity.this));
            //更新图库
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            intent.setData(Uri.fromFile(file));
            sendBroadcast(intent);

            webView.loadUrl("javascript:" + finishCallback + "('0')");
        } catch (Exception e) {
            webView.loadUrl("javascript:" + finishCallback + "('1')");
            e.printStackTrace();
        }

        if (bitmap != null && !type.isEmpty()) {
            switch (type) {
                case "qq":
                    MobclickAgent.onEvent(WebViewActivity.this, "start_share");
                    UMShareUtils.share(WebViewActivity.this, SHARE_MEDIA.QQ, new UMImage(TheLApp.getContext(), bitmap), umShareListener);
                    break;
                case "wechat":
                    MobclickAgent.onEvent(WebViewActivity.this, "start_share");
                    UMShareUtils.share(WebViewActivity.this, SHARE_MEDIA.WEIXIN, new UMImage(TheLApp.getContext(), bitmap), umShareListener);
                    break;
                case "friends":
                    MobclickAgent.onEvent(WebViewActivity.this, "start_share");
                    UMShareUtils.share(WebViewActivity.this, SHARE_MEDIA.WEIXIN_CIRCLE, new UMImage(TheLApp.getContext(), bitmap), umShareListener);
                    break;
                case "weibo":
                    MobclickAgent.onEvent(WebViewActivity.this, "start_share");
                    UMShareUtils.share(WebViewActivity.this, SHARE_MEDIA.SINA, new UMImage(TheLApp.getContext(), bitmap), umShareListener);
                    break;
            }
        }
    }

    private String getRealPathFromURI(Uri contentUri, Context context) {
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String fileStr = cursor.getString(column_index);
        cursor.close();
        return fileStr;
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("webview_NewFlutterFragment");
        if (fragment == null) {
            // 如果网页可以后退则后退
            if (webView.canGoBack()) {
                webView.goBack();
            } else {
                finish();
            }
        } else {
            NewUniversalRouter.sharedInstance.nativePop();
        }
    }


    @Override
    public SplashScreen provideSplashScreen() {
        Drawable manifestSplashDrawable = getSplashScreenFromManifest();
        if (manifestSplashDrawable != null) {
            return new DrawableSplashScreen(manifestSplashDrawable, ImageView.ScaleType.CENTER, 500L);
        } else {
            return null;
        }
    }

    private static String SPLASH_SCREEN_META_DATA_KEY = "io.flutter.embedding.android.SplashScreenDrawable";

    private Drawable getSplashScreenFromManifest() {
        try {
            @SuppressLint("WrongConstant") ActivityInfo activityInfo = getPackageManager().getActivityInfo(
                    getComponentName(),
                    PackageManager.GET_META_DATA | PackageManager.GET_ACTIVITIES);
            Bundle metadata = activityInfo.metaData;
            Integer splashScreenId = metadata != null ? metadata.getInt(SPLASH_SCREEN_META_DATA_KEY) : null;
            return splashScreenId != null
                    ? Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP
                    ? getResources().getDrawable(splashScreenId, getTheme())
                    : getResources().getDrawable(splashScreenId)
                    : null;
        } catch (PackageManager.NameNotFoundException e) {
            // This is never expected to happen.
            return null;
        }
    }
}
