package com.thel.modules.live.surface.show;

import android.annotation.TargetApi;
import android.graphics.PixelFormat;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;

import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.constants.TheLConstants;
import com.thel.modules.live.bean.LiveRoomBean;
import com.thel.modules.live.surface.watch.LiveWatchMsgClientCtrl;
import com.thel.modules.live.surface.watch.LiveWatchObserver;
import com.thel.tusdk.plain.CameraConfig;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.L;

/**
 * Created by waiarl on 2017/11/3.
 */

public class LiveShowActivity extends BaseActivity {
    private static final String TAG = LiveShowActivity.class.getSimpleName();
    private FrameLayout frame_capture_view;
    private FragmentManager manager;
    private LiveShowVideoStreamFragment liveShowVideoStreamFragment;
    private LiveShowAudioStreamFragment liveShowAudioStreamFragment;
    private LiveShowBaseViewFragment liveShowBaseViewFragment;
    private LiveRoomBean liveRoomBean;
    private LiveWatchObserver observer;
    private LiveWatchMsgClientCtrl clentCtrl;
    private CameraConfig cameraConfigBean;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_live_show_capture);
        init();
        getIntentData();
        findViewById();
        addFragment();
    }

    public void init() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        //解决surfaceveiw第一次进入的时候会有闪屏现象
        getWindow().setFormat(PixelFormat.TRANSLUCENT);

    }

    private void getIntentData() {
        final Bundle bundle = getIntent().getExtras();
        liveRoomBean = (LiveRoomBean) bundle.getSerializable(TheLConstants.BUNDLE_KEY_LIVE_ROOM);
        cameraConfigBean = (CameraConfig) bundle.getSerializable(TheLConstants.BUNDLE_KEY_LIVE_ROOM_CAMERA_CONFIG);
    }

    private void findViewById() {
        frame_capture_view = findViewById(R.id.frame_capture_view);
    }

    private void addFragment() {
        final Bundle bundle = new Bundle();
        bundle.putSerializable(TheLConstants.BUNDLE_KEY_LIVE_ROOM, liveRoomBean);


        if (LiveRoomBean.TYPE_VOICE == liveRoomBean.audioType) {//声音
            if (LiveRoomBean.MULTI_LIVE == liveRoomBean.isMulti) {//多人
                liveShowAudioStreamFragment = LiveShowAudioStreamFragment.getInstance(bundle);
                liveShowBaseViewFragment = LiveShowBaseViewFragment.getInstance(bundle);
                GrowingIOUtil.setViewInfo(frame_capture_view, "多人声音连麦");
            } else {//单人
                liveShowVideoStreamFragment = LiveShowVideoStreamFragment.getInstance(bundle);
                liveShowBaseViewFragment = LiveShowSoundViewFragment.getInstance(bundle);
                GrowingIOUtil.setViewInfo(frame_capture_view, "SoundLiveActivity");
            }
        } else {//视频
            liveShowVideoStreamFragment = LiveShowVideoStreamFragment.getInstance(bundle);
            bundle.putSerializable(TheLConstants.BUNDLE_KEY_LIVE_ROOM_CAMERA_CONFIG, cameraConfigBean);
            liveShowBaseViewFragment = LiveShowBaseViewFragment.getInstance(bundle);
            GrowingIOUtil.setViewInfo(frame_capture_view, "NormalActivity");
        }

        observer = new LiveWatchObserver();
        observer.bindILive(liveShowBaseViewFragment);
        observer.bindILiveAnchor(liveShowBaseViewFragment);
        clentCtrl = new LiveWatchMsgClientCtrl();
        clentCtrl.bindObserver(observer);
        clentCtrl.refreshClient(liveRoomBean);
        liveShowBaseViewFragment.bindObserver(observer);
        if (liveShowVideoStreamFragment != null) {
            liveShowVideoStreamFragment.bindObserver(observer);
        }
        if (liveShowAudioStreamFragment != null) {
            liveShowAudioStreamFragment.bindObserver(observer);
        }

        manager = getSupportFragmentManager();
        if (liveShowVideoStreamFragment != null) {
            manager.beginTransaction().add(R.id.frame_capture_stream, liveShowVideoStreamFragment).commit();
        } else if (liveShowAudioStreamFragment != null) {
            manager.beginTransaction().add(R.id.frame_capture_stream, liveShowAudioStreamFragment).commit();
        }
        manager.beginTransaction().add(R.id.frame_capture_view, liveShowBaseViewFragment).commit();

    }

    @Override
    protected void onDestroy() {
        try {
            if (observer != null) {
                observer = null;
            }
            clentCtrl.onDestroy();
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return true;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //相遇过程中，禁止任何操作
        if (liveShowBaseViewFragment != null && liveShowBaseViewFragment.isMeeting) {
            return true;
        }
        if (liveShowBaseViewFragment != null) {
            if (!liveShowBaseViewFragment.onKeyDown(keyCode, event)) {
                return false;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        //相遇过程中，禁止任何操作
        if (liveShowBaseViewFragment != null && liveShowBaseViewFragment.isMeeting) {
            return true;
        }
        return super.dispatchTouchEvent(ev);
    }

}
