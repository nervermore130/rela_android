package com.thel.modules.main.nearby.nearbyChild;

import android.Manifest;
import android.content.pm.PackageManager;

import com.thel.app.TheLApp;
import com.thel.bean.moments.MomentsListBean;
import com.thel.bean.user.NearUserBean;
import com.thel.bean.user.NearUserListNetBean;
import com.thel.modules.main.nearby.NearbyFragment;
import com.thel.modules.main.nearby.Utils.NearbyUtils;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.service.DefaultRequestService;
import com.thel.utils.PhoneUtils;
import com.thel.utils.ShareFileUtils;

import org.reactivestreams.Subscription;

import java.util.ArrayList;
import java.util.List;

import androidx.core.content.ContextCompat;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by waiarl on 2017/10/11.
 */

public class NearbyChildPresenter implements NearbyChildContract.Presenter {

    private final NearbyChildContract.View view;
    private final String tag;

    private boolean firstRequestPic = true;
    private int pigCursor = 0;
    private final int PAGE_SIZE = 20;
    private int curPage = 1;
    // 刷新类型，全部刷新（即下拉刷新）为1，还是加载下一页为2
    public int refreshType = 0;
    public final int REFRESH_TYPE_ALL = 1;
    public final int REFRESH_TYPE_NEXT_PAGE = 2;
    public volatile List<NearUserBean> pigCache = new ArrayList<>();//小鲜肉缓存

    public NearbyChildPresenter(NearbyChildContract.View view, String tag) {
        this.view = view;
        this.tag = tag;
        view.setPresenter(this);
    }

    @Override
    public void getRefreshData() {
        curPage = 1;
        refreshType = REFRESH_TYPE_ALL;
        getData();
    }

    @Override
    public void getLoadMoreData(int page) {
        curPage = page;
        refreshType = REFRESH_TYPE_NEXT_PAGE;
        getData();
    }

    @Override
    public void getNearbyMomentData() {
        //        if (LocationUtils.isHasLocationPermission()) {
        //            loadNearbyMomentData();
        //        } else {


//        loadNearbyMomentData();

    }

    @Override
    public void getNearbyPigData(final boolean isRefresh) {
        if (isRefresh) {
            firstRequestPic = true;
            pigCursor = 0;
            pigCache.clear();
        }
        if (pigCache.size() >= 6) {
            List<NearUserBean> cache = new ArrayList<>();
            for (int i = 0; i < 6; i++) {
                cache.add(pigCache.get(i));
            }
            view.showNearbyPigData(cache, isRefresh, pigCursor != 0);
            for (NearUserBean item : cache) {
                pigCache.remove(item);
            }
            return;
        }
        //没数据了
        if (!firstRequestPic && pigCursor == 0) return;
        final Flowable<NearUserListNetBean> flowable = DefaultRequestService.createNearbyRequestService().getNearbyPigList(pigCursor, 40);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<NearUserListNetBean>() {
            @Override
            public void onNext(NearUserListNetBean momentsListBean) {
                super.onNext(momentsListBean);
                firstRequestPic = false;
                if (momentsListBean != null && momentsListBean.data != null && momentsListBean.data.map_list != null) {
                    momentsListBean.data.filterBlock();
                    pigCache.addAll(momentsListBean.data.map_list);
                    List<NearUserBean> cache = new ArrayList<>();

                    if (pigCursor == 0) {//第一页
                        if (pigCache.size() >= 3) {
                            for (int i = 0; i < 3; i++) {
                                cache.add(pigCache.get(i));
                            }
                        } else {
                            cache.addAll(pigCache);
                        }
                    } else {
                        if (pigCache.size() >= 6) {
                            for (int i = 0; i < 6; i++) {
                                cache.add(pigCache.get(i));
                            }
                        } else {
                            cache.addAll(pigCache);
                        }
                    }
                    for (NearUserBean item : cache) {
                        pigCache.remove(item);
                    }
                    view.showNearbyPigData(cache, isRefresh, pigCache.size() > 0);
                    pigCursor = momentsListBean.data.cursor;
                }
            }

        });
    }

    private void loadNearbyMomentData() {
        final Flowable<MomentsListBean> flowable = DefaultRequestService.createNearbyRequestService().getNearbyMomentsList("1");
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<MomentsListBean>() {
            @Override
            public void onNext(MomentsListBean momentsListBean) {
                super.onNext(momentsListBean);
                if (momentsListBean != null && momentsListBean.momentsList != null) {
                    momentsListBean.filterBlockMoments();
                    NearbyUtils.filtNearbyMoment(momentsListBean);//过滤黑名单
                    view.showNearbyMomentData(momentsListBean.momentsList);
                }
            }

        });
    }

    private void getData() {

        if (NearbyFragment.TAG_NEARBY.equals(tag)) {
            getNearbyData(refreshType);
        } else if (NearbyFragment.TAG_STAR.equals(tag)) {
            getStarData(refreshType);
        } else if (NearbyFragment.TAG_HOT.equals(tag)) {
            getHotData(refreshType);
        }

    }

    private void getNearbyData(final int refreshType) {
        if (!PhoneUtils.isGpsOpen() || ContextCompat.checkSelfPermission(TheLApp.context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(TheLApp.context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            view.emptyData(true);
            view.requestFinish();
        } else {
            final Flowable<NearUserListNetBean> flowable = DefaultRequestService.createNearbyRequestService().getNearbySimpleList(PAGE_SIZE + "", curPage + "", "", "");
            flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<NearUserListNetBean>() {

                @Override
                public void onNext(NearUserListNetBean nearUserListNetBean) {
                    super.onNext(nearUserListNetBean);
                    //       nearUserListNetBean.data.filterBlock();
                    if (REFRESH_TYPE_ALL == refreshType) {
                        view.showRefreshData(nearUserListNetBean);
                    } else {
                        view.showLoadMoreData(nearUserListNetBean);
                    }
                }

                @Override
                public void onError(Throwable t) {
                    super.onError(t);
                    if (REFRESH_TYPE_ALL == refreshType) {//如果是第一页，说明加载失败
                        view.emptyData(true);
                    } else {
                        view.loadMoreFailed();
                    }
                }

                @Override
                public void onComplete() {
                    view.requestFinish();
                }
            });
        }

    }

    private void getHotData(final int refreshType) {
        final Flowable<NearUserListNetBean> flowable = DefaultRequestService.createNearbyRequestService().getActiveUserList(PAGE_SIZE + "", this.curPage + "", "", "1");
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<NearUserListNetBean>() {

            @Override
            public void onNext(NearUserListNetBean nearUserListNetBean) {
                super.onNext(nearUserListNetBean);
                //    nearUserListNetBean.data.filterBlock();
                if (REFRESH_TYPE_ALL == refreshType) {
                    view.showRefreshData(nearUserListNetBean);
                } else {
                    view.showLoadMoreData(nearUserListNetBean);
                }
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                if (REFRESH_TYPE_ALL == refreshType) {//如果是第一页，说明加载失败
                    view.emptyData(true);
                } else {
                    view.loadMoreFailed();
                }
            }

            @Override
            public void onComplete() {
                view.requestFinish();
            }
        });
    }


    private void getStarData(final int refreshType) {
        final Flowable<NearUserListNetBean> flowable = DefaultRequestService.createNearbyRequestService().getNearbySimpleList(PAGE_SIZE + "", this.curPage + "", "1", "");
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<NearUserListNetBean>() {

            @Override
            public void onNext(NearUserListNetBean nearUserListNetBean) {
                super.onNext(nearUserListNetBean);
                //        nearUserListNetBean.data.filterBlock();
                if (REFRESH_TYPE_ALL == refreshType) {
                    view.showRefreshData(nearUserListNetBean);
                } else {
                    view.showLoadMoreData(nearUserListNetBean);
                }
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                if (REFRESH_TYPE_ALL == refreshType) {//如果是第一页，说明加载失败
                    view.emptyData(true);
                } else {
                    view.loadMoreFailed();
                }
            }

            @Override
            public void onComplete() {
                view.requestFinish();
            }
        });
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {

    }

}
