package com.thel.modules.select_image;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseFragment;

import java.util.ArrayList;

/**
 * Created by chad
 * Time 2019-05-31
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */
public class SelectLocalDirsFragment extends BaseFragment implements AdapterView.OnItemClickListener {

    private ArrayList<ImageFolderBean> dirs;

    private ListView listview;

    private RelativeLayout root_rl;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            dirs = (ArrayList<ImageFolderBean>) getArguments().getSerializable("folder");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_select_local_dirs, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listview = view.findViewById(R.id.listview);
        root_rl = view.findViewById(R.id.root_rl);
        final SelectLocalDirsAdapter adapter = new SelectLocalDirsAdapter(dirs);
        listview.setAdapter(adapter);
        listview.setOnItemClickListener(this);
        root_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectLocalDirsListener != null) selectLocalDirsListener.onDismiss();
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (selectLocalDirsListener != null) selectLocalDirsListener.onSelectDir(position);
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (enter) {
            return AnimationUtils.loadAnimation(getActivity(), R.anim.top_in);
        } else {
            root_rl.setBackgroundColor(TheLApp.context.getResources().getColor(R.color.transparent));
            return AnimationUtils.loadAnimation(getActivity(), R.anim.top_out);
        }
    }

    private SelectLocalDirsListener selectLocalDirsListener;

    public interface SelectLocalDirsListener {
        void onSelectDir(int position);

        void onDismiss();
    }

    public void setSelectLocalDirsListener(SelectLocalDirsListener selectLocalDirsListener) {
        this.selectLocalDirsListener = selectLocalDirsListener;
    }
}
