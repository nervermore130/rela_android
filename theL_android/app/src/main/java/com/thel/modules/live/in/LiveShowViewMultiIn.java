package com.thel.modules.live.in;

import com.thel.bean.live.LiveMultiOnCreateBean;
import com.thel.bean.live.LiveMultiSeatBean;
import com.thel.bean.live.MultiSpeakersBean;

import java.util.List;

/**
 * Created by chad
 * Time 18/5/10
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */
public interface LiveShowViewMultiIn {
    void initMultiAdapter(LiveMultiOnCreateBean liveMultiOnCreateBean);//刷新列表

    void refreshSeat(LiveMultiSeatBean seatBean);//刷新座位

    void guestGift(LiveMultiSeatBean seatBean);//嘉宾礼物通知

    void startEncounter(int leftTime);//开始相遇倒计时

    void endEncounter(LiveMultiSeatBean seatBean);//结束相遇

    void speakers(MultiSpeakersBean speakersBean);

    void addSortMic(LiveMultiSeatBean.CoupleDetail seatBean);//排麦列表增加一个

    void removeSortMic(String userId);//排麦列表减少一个

    void getMicSortList(List<LiveMultiSeatBean.CoupleDetail> list);//排麦列表

    void sortMicOn();//开始排麦

    void sortMicOff();//结束排麦
    /**
     * 免费弹幕消息
     */
    void freeBarrage();

    void onFaceDetection(boolean visible);
}
