package com.thel.modules.main.me.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.drawable.ScalingUtils;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.user.MyImageBean;
import com.thel.constants.TheLConstants;
import com.thel.modules.preview_image.UserInfoPhotoActivity;
import com.thel.utils.AppInit;
import com.thel.utils.ImageUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;

import java.util.ArrayList;

/**
 * Created by lingwei on 2017/10/16.
 */

public class MyImagesGridAdapter extends BaseAdapter {
    private ArrayList<MyImageBean> myImageslist = new ArrayList<>();

    private LayoutInflater mInflater;

    private Context context;

    public MyImagesGridAdapter(ArrayList<MyImageBean> myImageslist, Context c) {

        mInflater = (LayoutInflater) TheLApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        context = c;
        freshAdapter(myImageslist);
    }

    public void freshAdapter(ArrayList<MyImageBean> myImageslist) {
        this.myImageslist = myImageslist;
    }

    @Override
    public int getCount() {
        return myImageslist.size();
    }

    @Override
    public Object getItem(int position) {
        return myImageslist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        HoldView holdView = null;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.uploadimage_griditem, parent, false);
            holdView = new HoldView();
            holdView.img_thumb = convertView.findViewById(R.id.img_thumb);
            holdView.img_private = convertView.findViewById(R.id.img_private);
            holdView.text = convertView.findViewById(R.id.text);
            convertView.setTag(holdView); // 把holdview缓存下来
        } else {
            holdView = (HoldView) convertView.getTag();
        }

        int width = (AppInit.displayMetrics.widthPixels - Utils.dip2px(TheLApp.getContext(), 30)) / 3;

        // 是否是拍照按钮
        if (myImageslist.get(position).isCameraBtnFlag) {
            holdView.img_thumb.getHierarchy().setPlaceholderImage(R.mipmap.btn_post_album_camera, ScalingUtils.ScaleType.FIT_CENTER);
            holdView.img_thumb.setController(null);
            holdView.img_thumb.setOnClickListener((View.OnClickListener) context);
            holdView.text.setVisibility(View.VISIBLE);
            holdView.text.setText(myImageslist.size() - 1 + "/9");
            holdView.img_private.setVisibility(View.GONE);

        } else {
            holdView.text.setVisibility(View.GONE);
            // 是否隐私图片
            if (myImageslist.get(position).isPrivate == 0) {
                // 是否封面图片
                //  int coverFlag = Integer.parseInt(myImageslist.get(position).coverFlag);
                if (myImageslist.get(position).coverFlag == 0) {
                    holdView.img_private.setVisibility(View.GONE);
                } else {
                    holdView.img_private.setImageResource(R.mipmap.icn_photos_cover);
                    holdView.img_private.setVisibility(View.VISIBLE);
                }
            } else {
                holdView.img_private.setImageResource(R.mipmap.icn_locked);
                holdView.img_private.setVisibility(View.VISIBLE);
            }

            holdView.img_thumb.getHierarchy().setPlaceholderImage(R.drawable.bg_waterfull_item_shape);
            holdView.img_thumb.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(myImageslist.get(position).longThumbnailUrl, TheLConstants.ALBUM_PHOTO_SIZE, TheLConstants.ALBUM_PHOTO_SIZE))).build()).setAutoPlayAnimations(true).build());
            holdView.img_thumb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ViewUtils.preventViewMultipleClick(v, 2000);
                    Intent i = new Intent(context, UserInfoPhotoActivity.class);
                    i.putExtra("fromPage", "UploadImageActivity");
                    ArrayList<MyImageBean> imgs = new ArrayList<>();
                    if (myImageslist.get(0).isCameraBtnFlag) {// 小于9张图
                        imgs.addAll(myImageslist.subList(1, myImageslist.size()));
                        i.putExtra("userinfo", imgs);
                        i.putExtra("position", position - 1);
                    } else {// 9张图
                        imgs.addAll(myImageslist.subList(0, myImageslist.size()));
                        i.putExtra("userinfo", imgs);
                        i.putExtra("position", position);
                    }
                    context.startActivity(i);
                }
            });
        }
        holdView.img_thumb.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, width));

        return convertView;
    }

    class HoldView {
        SimpleDraweeView img_thumb;
        ImageView img_private;
        TextView text;
    }
}
