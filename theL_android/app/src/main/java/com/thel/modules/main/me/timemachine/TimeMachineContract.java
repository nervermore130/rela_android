package com.thel.modules.main.me.timemachine;

import com.thel.base.BasePresenter;
import com.thel.base.BaseView;
import com.thel.bean.moments.MomentListDataBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.moments.MomentsListBean;

import java.util.List;

public class TimeMachineContract {

    public interface View extends BaseView<Presenter> {

        void showMomentList(MomentsListBean data);

    }

    interface Presenter extends BasePresenter {

        /**
         * 获取删除日志列表
         *
         * @param cursor
         * @param limit
         */
        void getMoments(int cursor, int limit);

        /**
         * 恢复被删除的日志
         *
         * @param momentId
         */
        void recoverMoment(String momentId);

    }

}
