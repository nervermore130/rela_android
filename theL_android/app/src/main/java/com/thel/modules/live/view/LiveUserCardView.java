package com.thel.modules.live.view;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.user.MyImageBean;
import com.thel.constants.TheLConstants;
import com.thel.growingio.GIOShareTrackBean;
import com.thel.growingio.GrowingIoConstant;
import com.thel.imp.follow.FollowStatusChangedImpl;
import com.thel.imp.follow.FollowStatusChangedListener;
import com.thel.imp.wink.WinkUtils;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.live.adapter.LiveSimplePhotoListAdapter;
import com.thel.modules.live.bean.LivePkRefuseBean;
import com.thel.modules.live.bean.LiveRoomBean;
import com.thel.modules.live.bean.LiveUserCardBean;
import com.thel.modules.live.in.LiveBaseView;
import com.thel.modules.live.utils.LinkMicOrPKRefuseUtils;
import com.thel.modules.live.utils.LiveUtils;
import com.thel.modules.main.me.bean.RoleBean;
import com.thel.modules.main.userinfo.UserInfoUtils;
import com.thel.modules.preview_image.AlbumPhotosPagerAdapter;
import com.thel.ui.widget.FollowView;
import com.thel.ui.widget.UserLevelImageView;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.zoom.VideoAndPhotoItemView;
import com.thel.utils.DateUtils;
import com.thel.utils.DialogUtil;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.ToastUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


/**
 * Created by waiarl on 2017/4/25.
 */

public class LiveUserCardView extends FrameLayout implements FollowStatusChangedListener, LiveBaseView<LiveUserCardView>, GrowingIoConstant {
    private static final String TAG = "LiveUserCardView";
    private Context mContext;
    private LinearLayout lin_more_bottom;
    private ImageView avatar;
    private TextView txt_nickname;
    private TextView txt_winkNum;
    private RecyclerView recyclerView;
    private TextView txt_report;
    private FollowView txt_follow;
    private TextView block_tv;
    private TextView report_tv;
    private View line_view;

    private LinearLayoutManager manager;
    private List<MyImageBean> picList = new ArrayList<>();
    private ArrayList<String> urlList = new ArrayList<>();
    private LiveSimplePhotoListAdapter adapter;
    private View view_blank;
    private LiveDragViewPager viewpager;
    private String rela_id;
    private boolean is_waterMark = true;
    private VideoAndPhotoItemView.DragListener dragDimissListener;
    private LiveUserCardBean user;
    private DismissListener dismissListener;
    private boolean isAnimating = false;
    private View view_shade;
    private TextView txt_fans_num;
    private UserLevelImageView img_user_level;
    private ImageView img_vip_level;
    private ImageView img_verify;
    private TextView txt_verify;
    private boolean isBroadcaster;
    private OnClickListener blockHerListener;
    private OnClickListener reportListener;
    private GIOShareTrackBean trackBean = new GIOShareTrackBean();
    private FrameLayout frame_shadow;
    private ImageView iv_wink_mic;
    private Calendar calendar;
    private ImageView avatar_partner;
    private TextView txt_signature;
    private String tAge;
    private String tHoroscope;
    private SparseArray<RoleBean> relationshipMap = new SparseArray<RoleBean>();
    private String tAffection;
    private TextView txt_usercard_infos;
    private LottieAnimationView lottie_sendwink;
    private GiftWinkSuccessReceiver giftWinkSuccessReceiver;//礼物挤眼广播
    private int liveType;

    public enum shareType {
        weixin, wx_circle, qq, qzone, sina, facebook
    }

    public LiveUserCardView(Context context) {
        this(context, null);
    }

    public LiveUserCardView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LiveUserCardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        initData();
        init();
    }

    private void initData() {
        giftWinkSuccessReceiver = new GiftWinkSuccessReceiver();
        final IntentFilter giftFilter = new IntentFilter(TheLConstants.BROADCAST_GIFT_WINK_SUCCESS);
        mContext.registerReceiver(giftWinkSuccessReceiver, giftFilter);
        initRelationshipMap();
        trackBean.track = LIVE_SHARE;
        trackBean.shareBean.shareEntry = ENTRY_LIVE_AVATAR;
    }

    /**
     * 礼物挤眼广播回调
     */
    class GiftWinkSuccessReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            final Bundle bundle = intent.getExtras();
            final String userId = bundle.getString(TheLConstants.BUNDLE_KEY_USER_ID);
            final String type = bundle.getString(TheLConstants.BUNDLE_KEY_ACTION_TYPE);
            if (userId.equals(user.userId + "")) {//说明是对这个人的礼物几眼
                showWinkSuccess(type);
            }
        }
    }

    /**
     * 挤眼成功
     *
     * @param type 5种基本类型
     */
    public void showWinkSuccess(String type) {
        if (user != null) {
            user.winked = 1;
            int num = 1;
            // 更新挤眼数
            txt_winkNum.setText(getSpanText(TheLApp.context.getString(R.string.userinfo_activity_wink_even, user.winkNum + num + ""), user.winkNum + num + ""));

           /* if (getUserVisibleHint()) {//如果当前对用户可见，说明是在当前页面，要播放动画显示toast
                UserInfoUtils.playWinkAnim(type, img_sendwink, fab_wink);
                //显示增加人气toast
                UserInfoUtils.showAddWinksToast(num, WinkUtils.WINK_TYPE_WINK.equals(type));
            }*/

            UserInfoUtils.playWinkAnim(type, lottie_sendwink, iv_wink_mic);
            //显示增加人气toast
            UserInfoUtils.showAddWinksToast(num, WinkUtils.WINK_TYPE_WINK.equals(type));

        }
    }

    private SpannableString getSpanText(String txt, String target) {
        final int start = txt.indexOf(target);
        final int end = start + target.length();
        final SpannableString ss = new SpannableString(txt);
        ss.setSpan(new ForegroundColorSpan(ContextCompat.getColor(TheLApp.context, R.color.text_color_gray)), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return ss;
    }

    private void initRelationshipMap() {
        relationshipMap.put(1, new RoleBean(0, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[0]));
        relationshipMap.put(6, new RoleBean(1, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[1]));
        relationshipMap.put(7, new RoleBean(2, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[2]));
        relationshipMap.put(2, new RoleBean(3, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[3]));
        relationshipMap.put(3, new RoleBean(4, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[4]));
        relationshipMap.put(4, new RoleBean(5, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[5]));
        relationshipMap.put(5, new RoleBean(6, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[6]));
        relationshipMap.put(0, new RoleBean(7, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[7]));


    }

    private void init() {
        if (mContext == null) {
            mContext = TheLApp.getContext();
        }
//        if (getResources().getConfiguration().orientation != Configuration.ORIENTATION_PORTRAIT) {
//            inflate(mContext, R.layout.live_room_user_card_horizontal_view, this);
//        } else {
//            inflate(mContext, R.layout.live_room_user_card_view, this);
//        }
        inflate(mContext, R.layout.live_room_user_card_view, this);
        view_shade = findViewById(R.id.view_shade);
        view_blank = findViewById(R.id.view_blank);
        lin_more_bottom = findViewById(R.id.lin_more_bottom);
        avatar = findViewById(R.id.avatar);
        txt_nickname = findViewById(R.id.txt_nickname);
        txt_winkNum = findViewById(R.id.txt_winkNum);
        recyclerView = findViewById(R.id.recyclerview);
        txt_report = findViewById(R.id.txt_report);
        txt_follow = findViewById(R.id.txt_follow);
        manager = new LinearLayoutManager(mContext);
        manager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView.setLayoutManager(manager);
        recyclerView.setHasFixedSize(true);
        adapter = new LiveSimplePhotoListAdapter(mContext, picList);
        recyclerView.setAdapter(adapter);
        viewpager = findViewById(R.id.viewpager);
        viewpager.setVisibility(View.GONE);

        txt_fans_num = findViewById(R.id.txt_fans_num);

        img_user_level = findViewById(R.id.img_user_level);
        img_vip_level = findViewById(R.id.img_vip_level);
        img_verify = findViewById(R.id.img_verify);
        txt_verify = findViewById(R.id.txt_verify);
        frame_shadow = findViewById(R.id.frame_shadow);

        block_tv = findViewById(R.id.block_tv);
        report_tv = findViewById(R.id.report_tv);
        line_view = findViewById(R.id.line_view);
        iv_wink_mic = findViewById(R.id.iv_wink_mic);
        avatar_partner = findViewById(R.id.avatar_partner);
        txt_signature = findViewById(R.id.txt_signature);
        txt_usercard_infos = findViewById(R.id.txt_usercard_infos);
        lottie_sendwink = findViewById(R.id.img_sendwink);
        GrowingIOUtil.setViewInfo(txt_follow, "点击弹出");

    }

    /**
     * 设置可挤眼状态
     */
    private void setWinkVisible() {
        setTomorrowCalendar();
        if (user.winked == 0) {
            iv_wink_mic.setAlpha(1f);
        } else {
            iv_wink_mic.setAlpha(0.5f);
            WinkUtils.addMyTodayWinkUser(user.userId + "");
        }
    }

    private void setTomorrowCalendar() {
        calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
    }

    private void setListener() {
        adapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                // showWatcher(position);
                showViewpager(position);
            }
        });
        dragDimissListener = new VideoAndPhotoItemView.DragListener() {

            @Override
            public void dismiss(boolean dismiss) {
                if (dismiss) {
                    viewpager.setVisibility(View.GONE);
                    viewpager.setAdapter(null);
                    Utils.sendVideoPlayingBroadcast(false);//发送是否在播放视频广播，用于直播间的时候屏蔽主播声音
                }
            }
        };
        viewpager.setDragListener(new LiveDragViewPager.DragListener() {
            @Override
            public void dismiss(boolean dismiss) {
                if (dismiss) {
                    viewpager.setVisibility(View.GONE);
                    viewpager.setAdapter(null);
                    Utils.sendVideoPlayingBroadcast(false);//发送是否在播放视频广播，用于直播间的时候屏蔽主播声音
                }
            }
        });
        txt_report.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isBroadcaster && blockHerListener != null) {
                    blockHerListener.onClick(v);
                } else if (!isBroadcaster && reportListener != null) {
                    reportListener.onClick(v);
                }
            }
        });
        report_tv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (reportListener != null) {
                    reportListener.onClick(v);
                }
            }
        });

        block_tv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (blockHerListener != null) {
                    blockHerListener.onClick(v);
                }
            }
        });

        if (isBroadcaster && LiveRoomBean.TYPE_VIDEO == liveType) {
            //连麦
            iv_wink_mic.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (LinkMicOrPKRefuseUtils.isLinkMicRefuse(user.userId + "")) {
                        LivePkRefuseBean livePkRefuseBean = LinkMicOrPKRefuseUtils.getLinkMicRefuseInfo(user.userId + "");
                        showRefuseTime(livePkRefuseBean);
                        return;
                    }

                    if (liveUserCardListener != null) {
                        liveUserCardListener.LinkMic(user);
                    }
                }
            });
        } else {
            //挤眼
            iv_wink_mic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tryToWink();
                }
            });
        }
        if (user != null && !user.isFollow) {
            txt_follow.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(final View v) {
                    FollowStatusChangedImpl.followUser(user.userId + "", FollowStatusChangedListener.ACTION_TYPE_FOLLOW, user.nickName, user.avatar);
                    user.fansNum++;
                    if (user.fansNum > 1) {
                        txt_fans_num.setText(TheLApp.getContext().getString(R.string.userinfo_activity_fans_even, user.fansNum));
                    } else {
                        txt_fans_num.setText(TheLApp.getContext().getString(R.string.userinfo_activity_fans_old, user.fansNum));

                    }
                    if (isBroadcaster) {
                        GrowingIOUtil.followLiveUser(GrowingIoConstant.LIVE_USER_CARD, GrowingIoConstant.LIVE_TARGET_HOST);

                    } else {
                        GrowingIOUtil.followLiveUser(GrowingIoConstant.LIVE_USER_CARD, GrowingIoConstant.LIVE_TARGET_GUEST);

                    }

                }
            });
        } else {
            txt_follow.setOnClickListener(null);
        }

    }

    /**
     * 尝试挤眼
     */
    private void tryToWink() {
        if (user != null && user.winked != 0) {
            DialogUtil.showToastShort(mContext, mContext.getString(R.string.wink_tips, user.nickName));
            iv_wink_mic.setAlpha(0.5f);
        } else {
            sendWink(WinkUtils.WINK_TYPE_WINK);
        }
    }

    /**
     * 发送挤眼
     *
     * @param type
     */
    private void sendWink(String type) {
        ViewUtils.preventViewMultipleClick(iv_wink_mic, 1000);
        if (user != null) {
            WinkUtils.sendUserCardWink(user.userId + "", type);

        }
        iv_wink_mic.setAlpha(0.5f);
        WinkUtils.playWinkSound();

    }

    public LiveUserCardView initView(final Activity activity, final LiveUserCardBean user, boolean canShare, final String shareTitle, final String shareContent, final String singleTitle, final String url, boolean isBroadcaster, int liveType) {
        this.user = user;
        this.isBroadcaster = isBroadcaster;
        this.trackBean.shareBean.shareMaterialType = LiveRoomBean.TYPE_VOICE == liveType ? MATERIAL_LIVE_VOICE : MATERIAL_LIVE_VIDEO;
        this.liveType = liveType;
        if (!isBroadcaster) {
            block_tv.setVisibility(INVISIBLE);
            line_view.setVisibility(INVISIBLE);
        }
        txt_nickname.setText(user.nickName);
        final int fansNum = user.fansNum;
        //粉丝数
        initViewState();
        if (fansNum > 1) {
            txt_fans_num.setText(TheLApp.getContext().getString(R.string.userinfo_activity_fans_even, user.fansNum));
        } else {
            txt_fans_num.setText(TheLApp.getContext().getString(R.string.userinfo_activity_fans_old, user.fansNum));

        }

        //人气
        if (user.winkNum > 0) {
            txt_winkNum.setText(TheLApp.getContext().getString(R.string.userinfo_activity_wink_even, user.winkNum + ""));

        } else {
            txt_winkNum.setText(TheLApp.getContext().getString(R.string.userinfo_activity_wink_even, 0 + ""));

        }
        //认证图标
        if (user.verifyType > 0) {
            if (user.verifyType == 2) {
                img_verify.setImageResource(R.mipmap.icn_verify_enterprise);
            } else {
                img_verify.setImageResource(R.mipmap.icn_verify_person);
            }
        }
        //
        //认证信息
        txt_verify.setText(user.verifyIntro + "");
        //根据是否认证显示相应的view
        if (user.verifyType > 0) {
            img_verify.setVisibility(View.VISIBLE);
            txt_verify.setVisibility(View.VISIBLE);
        } else {
            txt_fans_num.setVisibility(View.VISIBLE);
            txt_winkNum.setVisibility(View.VISIBLE);
            if (!TextUtils.isEmpty(user.distance) && !"0 m".equals(user.distance)) {
                img_verify.setVisibility(View.VISIBLE);
                img_verify.setImageResource(R.mipmap.icn_live_usercard_location);
                txt_verify.setVisibility(View.VISIBLE);
                txt_verify.setText(user.distance);
            }
        }


        if (isBroadcaster && LiveRoomBean.TYPE_VIDEO == liveType) {
            txt_report.setText(R.string.userinfo_activity_dialog_block_title);
            iv_wink_mic.setImageResource(R.mipmap.btn_live_usercard_lianmai);
        } else {
            txt_report.setText(R.string.userinfo_activity_dialog_report_title);
            iv_wink_mic.setImageResource(R.mipmap.btn_info_wink_normal);
            setWinkVisible();

        }
        setFollowStatus(user.followStatus);

        // avatar.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(user.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE)));
        ImageLoaderManager.imageLoaderDefaultCircle(avatar, R.mipmap.icon_user, user.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE);

        if (user.paternerd != null) {
            avatar_partner.setVisibility(VISIBLE);
            ImageLoaderManager.imageLoaderDefaultCircle(avatar_partner, R.mipmap.icon_user, user.paternerd.paternerAvatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE);
        }

        setUserLevelImg();
        setVipLevelImg();
        rela_id = user.userName;
        initImageWatcher(user.picList);

      /*  if (isBroadcaster && liveType == 1) {  //是主播开播，并且是声音直播
            iv_wink_mic.setVisibility(GONE);
        } else {
        }
*/
        if (!TextUtils.isEmpty(user.intro)) {
            txt_signature.setVisibility(VISIBLE);
            txt_signature.setText(user.intro);
        }

        final StringBuilder sb = new StringBuilder();
        //年龄
        tAge = TextUtils.isEmpty(user.age) ? "" : user.age + TheLApp.context.getString(R.string.updatauserinfo_activity_age_unit);

        if (tAge.length() > 0) {
            sb.append(tAge).append(", ");
        } else {
            if (!TextUtils.isEmpty(user.birthday)) {
                sb.append("18岁").append(", ");
            }

        }

        //星座
        tHoroscope = !TextUtils.isEmpty(user.birthday) ? DateUtils.date2Constellation(user.birthday) : "";
        if (tHoroscope.length() > 0) {
            sb.append(tHoroscope).append(", ");
        }
        //身高
        String height = "";
        if (!user.height.equals("0")) {
            height = getHeight(user.height);
            sb.append(height).append(", ");

        }


        //体重
        String weight = "";
        if (!user.weight.equals("0")) {
            weight = getWeight(user.weight);
        }
        if (!TextUtils.isEmpty(height) && TextUtils.isEmpty(weight)) {
            sb.append(weight).append("/ ");

        } else {
            if (!TextUtils.isEmpty(weight)) {
                sb.append(weight).append(", ");

            }

        }

        //感情状态
        tAffection = getRelationShip(user.affection);

        if (tAffection.length() > 0) {
            sb.append(tAffection).append(", ");
        }

        String text = sb.toString();

        if (text.endsWith(", ")) {
            text = text.substring(0, text.length() - 2);
        }

        if (text.length() == 0) {
            txt_usercard_infos.setVisibility(GONE);
        } else {
            txt_usercard_infos.setVisibility(VISIBLE);
        }
        L.d("LiveUserCardView", " text : " + text);

        txt_usercard_infos.setText(text);
        setListener();


        return this;
    }

    public LiveUserCardView initView(final Activity activity, final LiveUserCardBean user, boolean canShare, final String shareTitle, final String shareContent, final String singleTitle, final String url, boolean isBroadcaster, OnClickListener blockHer, final OnClickListener followUser, final OnClickListener reportListener) {
        initView(activity, user, canShare, shareTitle, shareContent, singleTitle, url, isBroadcaster, -1);
        this.blockHerListener = blockHer;
        this.reportListener = reportListener;
        return this;
    }

    public void setBlockHerListener(OnClickListener listener) {
        this.blockHerListener = listener;
    }

    public void setReportListener(OnClickListener listener) {
        this.reportListener = listener;
    }

    public void setViewBlankClickListener(OnClickListener listener) {
        view_blank.setOnClickListener(listener);
        frame_shadow.setOnClickListener(listener);
    }

    private void initViewState() {
        txt_fans_num.setVisibility(View.GONE);
        txt_winkNum.setVisibility(View.GONE);
        img_verify.setVisibility(View.GONE);
        txt_verify.setVisibility(View.GONE);
    }

    private void setVipLevelImg() {
        if (user == null) {
            return;
        }
        if (user.level > 0 && user.level < TheLConstants.VIP_LEVEL_RES.length) {
            int res = TheLConstants.VIP_LEVEL_RES[user.level];
            img_vip_level.setVisibility(View.VISIBLE);
            img_vip_level.setImageResource(res);
        } else {
            img_vip_level.setVisibility(View.GONE);
        }
    }

    private void setUserLevelImg() {
        if (user == null) {
            return;
        }
        final int user_level = user.userLevel;
        img_user_level.initView(user_level);
    }

    private void setFollowStatus(final int followStatus) {
        if (followStatus == FOLLOW_STATUS_FAN) {
            txt_follow.setText(TheLApp.getContext().getString(R.string.repowder));

        } else if (followStatus == FOLLOW_STATUS_NO) {
            txt_follow.setText(TheLApp.getContext().getString(R.string.userinfo_activity_follow));

        } else if (followStatus == FOLLOW_STATUS_FRIEND) {
            txt_follow.setText(TheLApp.getContext().getString(R.string.interrelated));
            txt_follow.setBackgroundResource(R.drawable.follow_unselector);
            txt_follow.setTextColor(ContextCompat.getColor(TheLApp.context, R.color.tab_normal));
        } else {
            txt_follow.setText(TheLApp.getContext().getString(R.string.userinfo_activity_followed));
            txt_follow.setBackgroundResource(R.drawable.follow_unselector);
            txt_follow.setTextColor(ContextCompat.getColor(TheLApp.context, R.color.tab_normal));
        }


    }

    private void initImageWatcher(List<MyImageBean> picLists) {
        picList.clear();
        picList.addAll(picLists);
        urlList.clear();
        adapter.notifyDataSetChanged();
        if (picList.size() <= 0) {
            recyclerView.setVisibility(View.GONE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
        }
        final int size = picList.size();
        for (int i = 0; i < size; i++) {
            urlList.add(picList.get(i).picUrl);
        }
    }

    private void showViewpager(int position) {
        viewpager.setAdapter(new AlbumPhotosPagerAdapter(mContext, urlList, rela_id, is_waterMark, true, null, dragDimissListener));
        viewpager.setCurrentItem(position);
        viewpager.setOffscreenPageLimit(5);
        viewpager.setVisibility(View.VISIBLE);
    }


    @Override
    public void onFollowStatusChanged(int followStatus, String userId, String nickName, String avatar) {
        if (user != null && (user.userId + "").equals(userId)) {
            user.isFollow = followStatus == FOLLOW_STATUS_FOLLOW || followStatus == FOLLOW_STATUS_FRIEND;
            setFollowStatus(followStatus);
        }
    }

    public interface DismissListener {
        void dismiss();
    }

    public void setDismissListener(DismissListener listener) {
        this.dismissListener = listener;
    }

    public LiveUserCardView show() {
        LiveUtils.animInLiveShowView(this);
        return this;
    }

    @Override
    public LiveUserCardView destroyView() {
        if (dismissListener != null) {
            dismissListener.dismiss();
        }
        isAnimating = false;
        clearAnimation();
        setTranslationX(0f);
        setTranslationY(0f);
        setVisibility(GONE);
        return this;
    }

    @Override
    public boolean isAnimating() {
        return isAnimating;
    }

    @Override
    public void setAnimating(boolean isAnimating) {
        this.isAnimating = isAnimating;
    }

    @Override
    public void showShade(boolean show) {
        view_shade.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    //身高
    private String getHeight(String height) {
        int heightUnits = ShareFileUtils.getInt(ShareFileUtils.HEIGHT_UNITS, 0); // 身高单位(0=cm, 1=Inches)
        try {
            String h;
            if (heightUnits == 0) {
                h = Float.valueOf(height).intValue() + " cm";
            } else {
                h = Utils.cmToInches(height) + " Inches";
            }

            return h;
        } catch (Exception e) {
        }
        return "";
    }

    //体重
    private String getWeight(String weight) {
        int weightUnits = ShareFileUtils.getInt(ShareFileUtils.WEIGHT_UNITS, 0); // 体重单位(0=kg, 1=Lbs)
        try {
            String w;

            if (weightUnits == 0) {
                w = Float.valueOf(weight).intValue() + " kg";
            } else {
                w = Utils.kgToLbs(weight) + " Lbs";
            }
            return w;
        } catch (Exception e) {
        }
        return "";
    }

    //感情状态
    private String getRelationShip(int userAffection) {
        try {
            RoleBean relationshipBean = relationshipMap.get(userAffection);
            String affection = relationshipBean.contentRes;

            return affection;

        } catch (Exception e) {
            return "";

        }
    }

    @Override
    public LiveUserCardView hide() {
        if (dismissListener != null) {
            dismissListener.dismiss();
        }
        mContext.unregisterReceiver(giftWinkSuccessReceiver);

        viewpager.setVisibility(View.GONE);
        LiveUtils.animOutLiveShowView(this, false);
        return this;
    }

    private LiveUserCardListener liveUserCardListener;

    public void setLiveUserCardListener(LiveUserCardListener liveUserCardListener) {
        this.liveUserCardListener = liveUserCardListener;
    }

    public interface LiveUserCardListener {
        void LinkMic(LiveUserCardBean liveUserCardBean);
    }

    private void showRefuseTime(LivePkRefuseBean livePkRefuseBean) {

        L.d(TAG, " livePkRefuseBean : " + livePkRefuseBean);

        if (livePkRefuseBean != null) {

            long deltaTime = 10 * 60 * 1000 - (System.currentTimeMillis() - livePkRefuseBean.refuseTime);

            L.d(TAG, " deltaTime : " + deltaTime);

            String content = TheLApp.context.getResources().getString(R.string.invite_after_xx, Utils.formatTime(deltaTime / 1000));

            ToastUtils.showToastShort(TheLApp.context, content);

        }
    }

}
