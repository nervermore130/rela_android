package com.thel.modules.main.me.aboutMe;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.appevents.AppEventsLogger;
import com.meituan.android.walle.WalleChannelReader;
import com.rela.pay.OnPayStatusListener;
import com.rela.pay.PayConstants;
import com.rela.pay.PayProxyImpl;
import com.thel.BuildConfig;
import com.thel.R;
import com.thel.android.pay.IabHelper;
import com.thel.android.pay.IabResult;
import com.thel.android.pay.Inventory;
import com.thel.android.pay.Purchase;
import com.thel.android.pay.SkuDetails;
import com.thel.android.pay.SyncOrdersService;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.bean.GoogleIapNotifyResultBean;
import com.thel.bean.PayOrderBean;
import com.thel.constants.BundleConstants;
import com.thel.constants.TheLConstants;
import com.thel.db.MomentsDataBaseAdapter;
import com.thel.growingio.GIoPayTrackBean;
import com.thel.growingio.GrowingIoConstant;
import com.thel.imp.gold.GoldUtils;
import com.thel.modules.live.bean.SoftGold;
import com.thel.modules.live.bean.SoftMoneyListBean;
import com.thel.modules.main.home.moments.web.WebViewActivity;
import com.thel.modules.main.me.adapter.SoftMoneyAdapter;
import com.thel.modules.main.me.match.eventcollect.collect.PayLogUtils;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.RequestConstants;
import com.thel.utils.DialogUtil;
import com.thel.utils.FireBaseUtils;
import com.thel.utils.GooglePayUtils;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.GsonUtils;
import com.thel.utils.JsonUtils;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MySoftMoneyActivity extends BaseActivity implements View.OnClickListener {
    /**
     * 购买软妹豆页面
     */
    @BindView(R.id.txt_balance)
    TextView txt_balance;//余额

    @BindView(R.id.txt_recharge_record)
    TextView txt_recharge_record;//充值记录

    @BindView(R.id.gridView)
    GridView gridView;

    @BindView(R.id.lin_back)
    LinearLayout lin_back;

    @BindView(R.id.txt_title)
    TextView txt_title;

    @BindView(R.id.txt_pay_qa)
    TextView txt_pay_qa;

    @BindView(R.id.lin_reload)
    LinearLayout lin_reload;

    @BindView(R.id.img_reload)
    ImageView img_reload;

    @BindView(R.id.txt_reload)
    TextView txt_reload;
    @BindView(R.id.lin_more)
    LinearLayout lin_more;

    @BindView(R.id.ll_custom_service)
    LinearLayout ll_custom_service;

    private List<SoftGold> goldList = new ArrayList<>();

    private SoftMoneyAdapter softMoneyAdapter;

    private int resultCode = 0;//请求返回，充值成功后为  RESULT_CODE_RECHARGE=10018;//去充值,充值成功，表示充值过

    private IabHelper mHelper;

    private boolean iabSetuped = false;

    private String price_currency_code = "";//货币参数

    private boolean hasLoadedData = false;

    private boolean goldChanged = false;

    private String pageId;

    private String productType;

    private class PayConsumer extends InterceptorSubscribe<PayOrderBean> {

        private String payType;

        public PayConsumer(String payType) {
            this.payType = payType;
        }

        @Override
        public void onNext(PayOrderBean result) {
            super.onNext(result);
            closeLoading();
            MobclickAgent.onEvent(MySoftMoneyActivity.this, "create_order_succeed");
            if (GooglePayUtils.GOOGLEPAY.equals(payType)) {
                //购买软妹豆

                if (iabSetuped && mHelper != null) {
                    final String sku = result.data.iapId;
                    buyItem(sku, result.data.outTradeNo, result.data.totalFee);
                    final GIoPayTrackBean payTrackBean = new GIoPayTrackBean(GrowingIoConstant.RMDPURCHASE, result.data.outTradeNo, result.data.totalFee + "", GooglePayUtils.GOOGLEPAY);
                    GrowingIOUtil.payTrack(payTrackBean, 0);

                    //购买软妹豆金额

                    final GIoPayTrackBean payBean = new GIoPayTrackBean(GrowingIoConstant.RMD_PURCHASE_AMOUNT, result.data.outTradeNo, GooglePayUtils.GOOGLEPAY);
                    GrowingIOUtil.payTrack(payBean, 10);
                }
            }
        }
    }

    private class IapNotifyConsumer extends InterceptorSubscribe<GoogleIapNotifyResultBean> {

        private String outTradeNo;

        public IapNotifyConsumer(String outTradeNo) {
            this.outTradeNo = outTradeNo;
        }

        @Override
        public void onNext(GoogleIapNotifyResultBean googleIapNotifyResultBean) {
            super.onNext(googleIapNotifyResultBean);
            if (GooglePayUtils.getInstance().isNotified(googleIapNotifyResultBean, true)) {
                final Purchase purchase = MomentsDataBaseAdapter.getInstance(TheLApp.getContext(), ShareFileUtils.getString(ShareFileUtils.ID, "")).getInAppOrder(outTradeNo);
                mHelper.consumeAsync(purchase, new IabHelper.OnConsumeFinishedListener() {
                    @Override
                    public void onConsumeFinished(Purchase purchase, IabResult result) {
                        bugGoldSucceed();
                    }
                });
                MomentsDataBaseAdapter.getInstance(TheLApp.getContext(), ShareFileUtils.getString(ShareFileUtils.ID, "")).deleteInAppOrder(outTradeNo);
                closeLoading();
            } else {
                notifyFailed(outTradeNo);
            }

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_my_soft_money);

        ButterKnife.bind(this);

        initView();

        initList();

        getGoldListNetData();

        pageId = Utils.getPageId();

        FireBaseUtils.uploadGoogle(TheLConstants.FireBaseConstant.VIEW_ITEM_PURCHASE_DETAILS,this);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mHelper != null && mHelper.handleActivityResult(requestCode, resultCode, data)) {
            return;
        } else
            super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void finish() {
        setResult(TheLConstants.RESULT_CODE_RECHARGE);
        super.finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mHelper != null)
            mHelper.dispose();
        mHelper = null;
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void initList() {
        softMoneyAdapter = new SoftMoneyAdapter(goldList);
        gridView.setAdapter(softMoneyAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SoftGold gold = (SoftGold) parent.getAdapter().getItem(position);
                buyGold(gold);
                PayLogUtils.getInstance().reportShortPayLog(GrowingIoConstant.PAGE_PREPAY, pageId, "click_product", TheLConstants.IsFirstCharge, "", gold.gemPrice + "元" + gold.gold);

            }
        });
    }

    private void initView() {
        txt_title.setText(getString(R.string.my_softmoney));

        lin_more.setVisibility(View.GONE);

        lin_back.setOnClickListener(this);

        txt_recharge_record.setOnClickListener(this);

        img_reload.setOnClickListener(this);

        txt_pay_qa.setOnClickListener(this);

        ll_custom_service.setOnClickListener(this);
    }

    private void notifyFailed(final String outTradeNo) {
        DialogUtil.showConfirmDialog(this, "", getString(R.string.synchronize_order_failed), getString(R.string.retry), getString(R.string.info_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Purchase pur = MomentsDataBaseAdapter.getInstance(TheLApp.getContext(), ShareFileUtils.getString(ShareFileUtils.ID, "")).getInAppOrder(outTradeNo);
                if (pur != null) {
                    showLoading();
                    RequestBusiness.getInstance().iapNotify(pur.getDeveloperPayload(), pur.getOriginalJson(), pur.getSignature()).onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new IapNotifyConsumer(pur.getDeveloperPayload()));
                } else {
                    SyncOrdersService.startSyncService(MySoftMoneyActivity.this);
                }
            }
        }, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                SyncOrdersService.startSyncService(MySoftMoneyActivity.this);
            }
        });
    }

    /**
     * 获取我的余额以及商品列表数据
     */
    private void getGoldListNetData() {
        showLoading();
        String channel = WalleChannelReader.getChannel(getApplication());
        if (TextUtils.isEmpty(channel) || (!TextUtils.isEmpty(channel) && !channel.equals("vivo"))) {
            channel = "";
        }
        RequestBusiness.getInstance().getGoldListData(channel).onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<SoftMoneyListBean>() {
            @Override
            public void onNext(SoftMoneyListBean softMoney) {
                super.onNext(softMoney);
                if (softMoney == null || softMoney.data == null || softMoney.data.list == null)
                    return;
                hasLoadedData = true;
                goldList.clear();
                goldList.addAll(softMoney.data.list);
                softMoneyAdapter.setChannel(softMoney.data.channel);
                txt_balance.setText(softMoney.data.gold + "");
                GoldUtils.sendGoldChangedBroad(softMoney.data.gold);

                if (RequestConstants.APPLICATION_ID_GLOBAL.equals(BuildConfig.APPLICATION_ID)) {
                    final List<String> skuIds = new ArrayList<>();
                    for (SoftGold softGold : goldList) {
                        softGold.isPendingPrice = true;
                        skuIds.add(softGold.iapId);
                    }
                    refreshUi();
                    setupIap(skuIds);
                } else
                    requestFinished();
            }
        });

    }

    private void setupIap(final List<String> skuIds) {
        if (mHelper == null)
            mHelper = new IabHelper(TheLApp.getContext(), GooglePayUtils.KEY);
        if (!iabSetuped)
            mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
                @Override
                public void onIabSetupFinished(IabResult result) {
                    iabSetuped = result.isSuccess();
                    if (iabSetuped && mHelper != null) {
                        queryItems(skuIds);
                    } else {
                        loadFailed(result.getResponse() + "");
                        DialogUtil.showToastShort(MySoftMoneyActivity.this, getString(R.string.google_play_connect_error));
                        PayLogUtils.getInstance().reportLongPayLog(GrowingIoConstant.PAGE_PREPAY, pageId, "", TheLConstants.IsFirstCharge, productType, GooglePayUtils.GOOGLEPAY, 0, TheLApp.context.getString(R.string.google_play_connect_error));

                    }
                }
            });
        else
            queryItems(skuIds);
    }

    private void queryItems(final List<String> skuIds) {
        mHelper.queryInventoryAsync(true, skuIds, new IabHelper.QueryInventoryFinishedListener() {
            @Override
            public void onQueryInventoryFinished(IabResult result, Inventory inv) {
                if (result.isSuccess()) {
                    closeLoading();
                    for (SoftGold softGold : goldList) {
                        SkuDetails skuDetails = inv.getSkuDetails(softGold.iapId);
                        if (skuDetails != null) {
                            softGold.isPendingPrice = false;
                            softGold.googlePrice = skuDetails.getPrice();
                            try {
                                if (TextUtils.isEmpty(price_currency_code)) {//获取货币参数
                                    price_currency_code = JsonUtils.getString(new JSONObject(skuDetails.getJson()), "price_currency_code", "");
                                }
                                // google返回的price_amount_micros是*1000000，我们在这里只/10000，因为后面会再/100（为了跟国内版逻辑统一）
                                softGold.price = JsonUtils.getLong(new JSONObject(skuDetails.getJson()), "price_amount_micros", 0) / 10000d;
                            } catch (JSONException e) {
                                loadFailed("");
                                return;
                            }
                        }
                    }
                    refreshUi();
                } else {
                    loadFailed(result.getResponse() + "");
                }
            }
        });
    }

    /**
     * 请求借宿
     */
    private void requestFinished() {
        closeLoading();
        refreshUi();
    }

    private void refreshUi() {
        if (hasLoadedData) {
            lin_reload.setVisibility(View.GONE);
            softMoneyAdapter.notifyDataSetChanged();
        } else {
            loadFailed("");
        }
    }

    private void loadFailed(String googleError) {
        closeLoading();
        if (!TextUtils.isEmpty(googleError))
            DialogUtil.showToastShort(MySoftMoneyActivity.this, getString(R.string.google_play_connect_error) + "(" + googleError + ")");
        try {
            lin_reload.setVisibility(View.VISIBLE);
            img_reload.clearAnimation();
            txt_reload.setText(getString(R.string.info_reload));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 购买点击的商品
     *
     * @param gold 购买的商品
     */
    private void buyGold(final SoftGold gold) {
        long time = System.currentTimeMillis();
        try {
            if (RequestConstants.APPLICATION_ID_LOCAL.equals(BuildConfig.APPLICATION_ID))
                DialogUtil.getInstance().showSelectionDialogWithIcon(this, getString(R.string.pay_options), new String[]{getString(R.string.pay_alipay), getString(R.string.pay_wx)}, new int[]{R.mipmap.icon_zhifubao, R.mipmap.icon_wechat}, new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                        DialogUtil.getInstance().closeDialog();
                        switch (position) {
                            case 0:// 支付宝
                                RequestBusiness.getInstance().createStickerPackOrder(TheLConstants.PRODUCT_TYPE_SOFT_GOLD, gold.id + "", PayConstants.PAY_TYPE_ALIPAY, PayConstants.ALIPAY_SOURCE).observeOn(AndroidSchedulers.mainThread()).onBackpressureDrop().subscribeOn(Schedulers.io()).subscribe(new InterceptorSubscribe<PayOrderBean>() {
                                    @Override public void onNext(PayOrderBean data) {
                                        super.onNext(data);

                                        //购买软妹豆
                                        final GIoPayTrackBean payTrackBean = new GIoPayTrackBean(GrowingIoConstant.RMDPURCHASE, data.data.outTradeNo, data.data.totalFee + "", PayConstants.PAY_TYPE_ALIPAY);
                                        GrowingIOUtil.payTrack(payTrackBean, 0);
                                        //购买软妹豆金额

                                        final GIoPayTrackBean payBean = new GIoPayTrackBean(GrowingIoConstant.RMD_PURCHASE_AMOUNT, data.data.outTradeNo, PayConstants.PAY_TYPE_ALIPAY);
                                        GrowingIOUtil.payTrack(payBean, 10);

                                        PayProxyImpl.getInstance().pay(PayConstants.PAY_TYPE_ALIPAY, MySoftMoneyActivity.this, GsonUtils.createJsonString(data.data), new OnPayStatusListener() {
                                            @Override public void onPayStatus(int payStatus, String errorInfo) {

                                                MobclickAgent.onEvent(MySoftMoneyActivity.this, "create_order_succeed");

                                                if (payStatus == 1) {
                                                    bugGoldSucceed();
                                                    PayLogUtils.getInstance().reportLongPayLog(GrowingIoConstant.PAGE_PREPAY, pageId, "", TheLConstants.IsFirstCharge, productType, PayConstants.PAY_TYPE_ALIPAY, 1, "");
                                                } else {
                                                    if (TextUtils.equals(errorInfo, "6001")) {
                                                        PayLogUtils.getInstance().reportLongPayLog(GrowingIoConstant.PAGE_PREPAY, pageId, "", TheLConstants.IsFirstCharge, productType, PayConstants.PAY_TYPE_ALIPAY, 0, TheLApp.context.getString(R.string.pay_canceled));

                                                    } else {
                                                        PayLogUtils.getInstance().reportLongPayLog(GrowingIoConstant.PAGE_PREPAY, pageId, "", TheLConstants.IsFirstCharge, productType, PayConstants.PAY_TYPE_ALIPAY, 0, TheLApp.context.getString(R.string.pay_failed));

                                                    }
                                                    closeLoading();
                                                }
                                            }

                                        });

                                    }
                                });
                                break;
                            case 1: // 微信支付
                                showLoading();
                                RequestBusiness.getInstance().createStickerPackOrder(TheLConstants.PRODUCT_TYPE_SOFT_GOLD, gold.id + "", PayConstants.PAY_TYPE_WXPAY, PayConstants.WXPAY_SOURCE).observeOn(AndroidSchedulers.mainThread()).onBackpressureDrop().subscribeOn(Schedulers.io()).subscribe(new InterceptorSubscribe<PayOrderBean>() {
                                    @Override public void onNext(PayOrderBean data) {
                                        super.onNext(data);

                                        final GIoPayTrackBean payTrackBean = new GIoPayTrackBean(GrowingIoConstant.RMDPURCHASE, data.data.outTradeNo, data.data.totalFee + "", PayConstants.PAY_TYPE_WXPAY);
                                        GrowingIOUtil.payTrack(payTrackBean, 0);
                                        //购买软妹豆金额
                                        final GIoPayTrackBean payBean = new GIoPayTrackBean(GrowingIoConstant.RMD_PURCHASE_AMOUNT, data.data.outTradeNo, PayConstants.PAY_TYPE_WXPAY);
                                        GrowingIOUtil.payTrack(payBean, 10);

                                        MobclickAgent.onEvent(MySoftMoneyActivity.this, "create_order_succeed");

                                        PayProxyImpl.getInstance().pay(PayConstants.PAY_TYPE_WXPAY, MySoftMoneyActivity.this, GsonUtils.createJsonString(data.data), new OnPayStatusListener() {
                                            @Override public void onPayStatus(int payStatus, String errorInfo) {

                                                L.d("WXPayment", " payStatus : " + payStatus);

                                                if (payStatus == 1) {
                                                    bugGoldSucceed();
                                                    PayLogUtils.getInstance().reportLongPayLog(GrowingIoConstant.PAGE_PREPAY, pageId, "", TheLConstants.IsFirstCharge, productType, PayConstants.PAY_TYPE_WXPAY, 1, "");
                                                } else {
                                                    closeLoading();
                                                }

                                            }
                                        });

                                    }
                                });

                                PayLogUtils.getInstance().reportShortPayLog(GrowingIoConstant.PAGE_PREPAY, pageId, "click_paytype", TheLConstants.IsFirstCharge, PayConstants.PAY_TYPE_WXPAY, gold.gemPrice + "元" + gold.gold);

                                productType = gold.gemPrice + "元" + gold.gold;

                                break;
                            default:
                                break;
                        }
                    }
                }, false, null);
            else {// 国际版，用google wallet
                if (!gold.isPendingPrice) {
                    showLoadingNoBack();
                    RequestBusiness.getInstance().createStickerPackOrder(TheLConstants.PRODUCT_TYPE_SOFT_GOLD, gold.id + "", GooglePayUtils.GOOGLEPAY, GooglePayUtils.GOOGLEPAY_SOURCE).observeOn(AndroidSchedulers.mainThread()).onBackpressureDrop().subscribeOn(Schedulers.io()).subscribe(new PayConsumer(GooglePayUtils.GOOGLEPAY));
                    PayLogUtils.getInstance().reportShortPayLog(GrowingIoConstant.PAGE_PREPAY, pageId, "click_paytype", TheLConstants.IsFirstCharge, GooglePayUtils.GOOGLEPAY, gold.googlePrice + "元" + gold.gold);
                    productType = gold.googlePrice + "元" + gold.gold;

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void buyItem(final String sku, final String outTradeNo, final double totalFee) {
        mHelper.launchPurchaseFlow(MySoftMoneyActivity.this, sku, TheLConstants.REQUEST_CODE_ANDROID_PAY, new IabHelper.OnIabPurchaseFinishedListener() {
            @Override
            public void onIabPurchaseFinished(IabResult result, Purchase info) {
                showLoading();
                if (result.isSuccess()) {
                    onPaid(info);
                    AppEventsLogger logger = AppEventsLogger.newLogger(MySoftMoneyActivity.this);
                    if (logger != null && !TextUtils.isEmpty(price_currency_code)) {//facebook记录购买成功
                        logger.logPurchase(BigDecimal.valueOf(totalFee), Currency.getInstance(price_currency_code));
                    }
                } else {
                    if (IabHelper.IABHELPER_USER_CANCELLED == result.getResponse()) {
                        closeLoading();
                        DialogUtil.showToastShort(MySoftMoneyActivity.this, getString(R.string.pay_canceled));
                    } else if (IabHelper.BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED == result.getResponse()) { // 已购买过该商品，但是没有消耗掉，此时需要查询本地是否有未同步的订单
                        List<String> skus = new ArrayList<>();
                        skus.add(sku);
                        mHelper.queryInventoryAsync(true, skus, new IabHelper.QueryInventoryFinishedListener() {
                            @Override
                            public void onQueryInventoryFinished(IabResult result, Inventory inv) {
                                if (result.isSuccess())
                                    mHelper.consumeAsync(inv.getPurchase(sku), new IabHelper.OnConsumeFinishedListener() {
                                        @Override
                                        public void onConsumeFinished(Purchase purchase, IabResult result) {
                                            closeLoading();
                                            if (result.isSuccess()) {
                                                buyItem(sku, outTradeNo, totalFee);
                                            } else {
                                                DialogUtil.showToastShort(MySoftMoneyActivity.this, getString(R.string.google_play_connect_error));
                                            }
                                        }
                                    });
                                else {
                                    closeLoading();
                                    DialogUtil.showToastShort(MySoftMoneyActivity.this, getString(R.string.google_play_connect_error) + "(" + result.getResponse() + ")");
                                }
                            }
                        });
                    } else {
                        closeLoading();
                        DialogUtil.showToastShort(MySoftMoneyActivity.this, getString(R.string.pay_failed) + "(" + result.getResponse() + ")");
                    }
                }
            }
        }, outTradeNo);
    }

    private void onPaid(Purchase purchase) {
        MomentsDataBaseAdapter.getInstance(TheLApp.getContext(), ShareFileUtils.getString(ShareFileUtils.ID, "")).saveInAppOrder(purchase);
        RequestBusiness.getInstance().iapNotify(purchase.getDeveloperPayload(), purchase.getOriginalJson(), purchase.getSignature()).observeOn(AndroidSchedulers.mainThread()).onBackpressureDrop().subscribeOn(Schedulers.io()).subscribe(new IapNotifyConsumer(purchase.getDeveloperPayload()));
    }

    /**
     * 充值记录
     */
    private void gotoRechargeRecord() {
        Intent intent = new Intent(this, RechargeRecordActivity.class);
        startActivity(intent);
        PayLogUtils.getInstance().reportShortPayLog("prepay_bean", pageId, "click_record", TheLConstants.IsFirstCharge, "", "");
    }

    private void bugGoldSucceed() {
        DialogUtil.showToastShort(MySoftMoneyActivity.this, getString(R.string.buy_succeed));
        getGoldListNetData();
        resultCode = TheLConstants.RESULT_CODE_RECHARGE;//充值成功back后的返回值
        goldChanged = true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lin_back:
                finish();
                PayLogUtils.getInstance().reportShortPayLog("prepay_bean", pageId, "click_back", TheLConstants.IsFirstCharge, "", "");

                break;
            case R.id.txt_recharge_record:
                gotoRechargeRecord();
                break;
            case R.id.img_reload:
                Animation anim = AnimationUtils.loadAnimation(TheLApp.getContext(), R.anim.rotate);
                LinearInterpolator lir = new LinearInterpolator();
                anim.setInterpolator(lir);
                v.setLayerType(View.LAYER_TYPE_HARDWARE, null);
                v.startAnimation(anim);
                txt_reload.setText(getString(R.string.info_reloading));
                getGoldListNetData();
                break;
            case R.id.txt_pay_qa:
                ViewUtils.preventViewMultipleClick(v, 1000);
                Intent intent = new Intent(MySoftMoneyActivity.this, WebViewActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(BundleConstants.URL, TheLConstants.BUY_SOFT_MONEY_HELP_PAGE_URL);
                bundle.putBoolean(BundleConstants.NEED_SECURITY_CHECK, false);
                intent.putExtras(bundle);
                startActivity(intent);
                PayLogUtils.getInstance().reportShortPayLog("prepay_bean", pageId, "click_question", TheLConstants.IsFirstCharge, "", "");

                break;
            case R.id.ll_custom_service:
                ViewUtils.preventViewMultipleClick(v, 1000);
                Utils.gotoUDesk();
                PayLogUtils.getInstance().reportShortPayLog("prepay_bean", pageId, "click_service", TheLConstants.IsFirstCharge, "", "");

                break;
        }
    }

}
