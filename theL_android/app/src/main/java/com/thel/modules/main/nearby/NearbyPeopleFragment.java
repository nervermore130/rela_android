package com.thel.modules.main.nearby;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.view.View;

import com.thel.R;
import com.thel.bean.AdBean;
import com.thel.bean.user.NearUserBean;
import com.thel.bean.user.NearUserListNetBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.OnActivityResultCallback;
import com.thel.flutter.bridge.BridgeUtils;
import com.thel.imp.black.BlackUtils;
import com.thel.imp.momentblack.MomentBlackListener;
import com.thel.imp.momentblack.MomentBlackUtils;
import com.thel.modules.main.me.bean.LogInfoBean;
import com.thel.modules.main.me.match.eventcollect.collect.LiveLogUtils;
import com.thel.modules.main.nearby.Utils.NearbyUtils;
import com.thel.modules.main.nearby.nearbyChild.NearbyChildFragment;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class NearbyPeopleFragment extends NearbyChildFragment implements MomentBlackListener {

    private NearbyNewFaceView mNearbyNewFaceView;
    private MomentBlackUtils momentBlackUtils;
    private boolean headerReady = false;
    private boolean fallReady = false;
    private List<AdBean.Map_list> adList = new ArrayList<>();
    private String from_page_id;
    private String from_page;
    private String pageId;


    public static NearbyPeopleFragment newInstance(Bundle bundle) {
        NearbyPeopleFragment fragment = new NearbyPeopleFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageId = Utils.getPageId();

        from_page = ShareFileUtils.getString(ShareFileUtils.rootSwitchPage, "");
        from_page_id = ShareFileUtils.getString(ShareFileUtils.rootSwitchPageId, "");

        momentBlackUtils = new MomentBlackUtils();
        momentBlackUtils.registerReceiver(this);
        ShareFileUtils.setString(ShareFileUtils.nearPeoplePage, "around.list");
        ShareFileUtils.setString(ShareFileUtils.nearPeoplePageId, pageId);


    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getAd();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        momentBlackUtils.unRegisterReceiver(this);
    }

    @Override
    protected void findViewById(View view) {
        super.findViewById(view);
        mNearbyNewFaceView = new NearbyNewFaceView(getActivity());
        mNearbyNewFaceView.findViewById(R.id.more_pig_rl).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //获取更多小鲜肉
                presenter.getNearbyPigData(false);
                traceMoreNewFaceLog("click_more");
            }
        });
        mAdapter.addHeaderView(mNearbyNewFaceView);
        mAdapter.setNearbyPeopleFragment(this);
        mNearbyNewFaceView.setVisibility(View.GONE);
        List<String> userList = BlackUtils.getBlackList();
        View cover_guide = mNearbyNewFaceView.findViewById(R.id.cover_guide);
        boolean near_guide = ShareFileUtils.getBoolean(ShareFileUtils.NEARBY_PEOPLE_GUIDE, false);

        if (near_guide) {
            cover_guide.setVisibility(View.GONE);
        } else {
            mNearbyNewFaceView.findViewById(R.id.cover_close).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    mNearbyNewFaceView.findViewById(R.id.cover_guide).setVisibility(View.GONE);
                    mAdapter.notifyDataSetChanged();
                    ShareFileUtils.setBoolean(ShareFileUtils.NEARBY_PEOPLE_GUIDE, true);
                }
            });
        }


        open_location.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                BridgeUtils.gotoSystemLocationSetting(getActivity(), null);
            }
        });
    }

    public void traceMoreNewFaceLog(String activity) {
        try {
            String latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
            String longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");

            LogInfoBean logInfoBean = new LogInfoBean();
            logInfoBean.page = "around.list";
            logInfoBean.page_id = pageId;
            logInfoBean.activity = activity;
            logInfoBean.from_page_id = from_page_id;
            logInfoBean.from_page = from_page;
            logInfoBean.lat = latitude;
            logInfoBean.lng = longitude;

            LiveLogUtils.getInstance().addLog(logInfoBean);

        } catch (Exception e) {

        }
    }

    @Override
    protected void refreshData() {
        headerReady = false;
        fallReady = false;
        super.refreshData();
        presenter.getNearbyPigData(true);
    }

    @Override
    public void emptyData(boolean empty) {
        if (rel_default_gps != null) {
            if (empty) {
                rel_default_gps.setVisibility(View.VISIBLE);
            } else {
                rel_default_gps.setVisibility(View.INVISIBLE);
            }
        }
    }

    @Override
    public void showNearbyPigData(List<NearUserBean> userBeanList, boolean isRefresh, boolean hasMoreData) {
        super.showNearbyPigData(userBeanList, isRefresh, hasMoreData);
        headerReady = true;
        if (isRefresh) {
            if (userBeanList.size() > 0) {
                mNearbyNewFaceView.setVisibility(View.VISIBLE);
            } else {
                mNearbyNewFaceView.setVisibility(View.GONE);

            }
            mNearbyNewFaceView.initView(userBeanList, hasMoreData, pageId, from_page, from_page_id);

        } else {
            mNearbyNewFaceView.addDatas(userBeanList, hasMoreData, pageId, from_page, from_page_id);

        }
    }

    @Override
    public void showRefreshData(NearUserListNetBean nearUserListNetBean) {

        fallReady = true;
        emptyData(false);
        if (nearUserListNetBean == null || nearUserListNetBean.data == null) {
            emptyData(true);
            return;
        }
        final NearUserListNetBean.NearUserListBean dataBean = nearUserListNetBean.data;
        haveNextPage = dataBean.map_list.size() > 0;
        NearbyUtils.filterUser(dataBean.map_list);
        NearbyUtils.filterBlack(dataBean.map_list);
        list.clear();
        list.addAll(dataBean.map_list);
        mAdapter.removeAllFooterView();
        //本地添加一个空数据，用于人气明星入口站位
        NearUserBean userBean = new NearUserBean();
        userBean.itemType = 1;
        userBean.hs_images = dataBean.hs_images;
        if (list.size() >= 5) {
            list.add(4, userBean);
        } else {
            list.add(userBean);
        }
        //非会员本地添加banner数据
        if (UserUtils.getUserVipLevel() <= 0 && adList.size() > 0 && list.size() >= 9) {
            NearUserBean banner = new NearUserBean();
            banner.itemType = 2;
            banner.adList = adList;
            list.add(9, banner);
        }

        mAdapter.setNewData(list);
        curPage = 2;
        if (haveNextPage) {
            mAdapter.openLoadMore(list.size(), true);
        } else {
            mAdapter.openLoadMore(list.size(), false);
            if (list.isEmpty()) {
                emptyData(true);
            }
        }
    }

    @Override
    public void onBlackUsersChanged(String userId, boolean isAdd) {
        super.onBlackUsersChanged(userId, isAdd);
        mNearbyNewFaceView.filterNewBlack(userId);
    }

    @Override
    protected boolean canShowNewLiveGuide() {
        return super.canShowNewLiveGuide() && fallReady && headerReady;
    }

    @Override
    public void onBlackOneMoment(String momentId) {
//        mNearbyPigView.filterNewMoment(momentId);
    }

    @Override
    public void onBlackherMoment(String userId) {
//        mNearbyPigView.filterNewBlack(userId);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == TheLConstants.REQUEST_CODE_COVER) {
            refreshData();
        }
    }

    private void getAd() {
        adList.clear();
        String adJson = ShareFileUtils.getString(ShareFileUtils.ADS, "");
        if (!TextUtils.isEmpty(adJson)) {
            AdBean data = GsonUtils.getObject(adJson, AdBean.class);
            if (data != null && data.data != null && data.data.map_list != null) {
                for (int i = 0; i < data.data.map_list.size(); i++) {
                    AdBean.Map_list map_list = data.data.map_list.get(i);
                    if (map_list.advertLocation.equals("nearBy")) {
                        adList.add(map_list);
                    }
                }
            }
        }
    }
}
