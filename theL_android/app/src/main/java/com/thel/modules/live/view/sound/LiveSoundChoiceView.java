package com.thel.modules.live.view.sound;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.modules.live.LiveConstant;
import com.thel.modules.live.bean.LiveSoundChoiceAppBean;
import com.thel.modules.live.in.LiveBaseView;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.utils.DialogUtil;
import com.thel.utils.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by waiarl on 2018/1/23.
 */

public class LiveSoundChoiceView extends LinearLayout implements LiveBaseView<LiveSoundChoiceView> {
    private final Context mContext;
    private RecyclerView recyclerView;
    private TextView txt_cancel;
    private LinearLayoutManager manager;
    private List<LiveSoundChoiceAppBean> list = new ArrayList<>();
    private LiveSoundChoiceAdapter adapter;
    private LiveSoundAppChoiceListener listener;
    private LiveSoundAppCloseListener closeListener;

    public LiveSoundChoiceView(Context context) {
        this(context, null);
    }

    public LiveSoundChoiceView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LiveSoundChoiceView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
        setListener();
        initView(LiveConstant.soundChoiceAppBeanList);
    }

    private void init() {
        inflate(mContext, R.layout.live_sound_choice_view, this);
        recyclerView = findViewById(R.id.recyclerview);
        txt_cancel = findViewById(R.id.txt_cancel);

        manager = new LinearLayoutManager(mContext);
        manager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView.setLayoutManager(manager);
        adapter = new LiveSoundChoiceAdapter(list);
        recyclerView.setAdapter(adapter);
    }

    private void setListener() {
        adapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                final LiveSoundChoiceAppBean bean = adapter.getItem(position);
                gotoApp(bean);
                if (listener != null) {
                    listener.choiceApp(bean, position);
                }
            }
        });
        txt_cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (closeListener != null) {
                    closeListener.closeDialog();
                }
            }
        });
    }

    private void gotoApp(LiveSoundChoiceAppBean bean) {

        try {
            //应用过滤条件
            Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
            mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
            final PackageManager mPackageManager = TheLApp.getContext().getPackageManager();
            final List<ResolveInfo> mAllApps = mPackageManager.queryIntentActivities(mainIntent, 0);
            //按包名排序
            Collections.sort(mAllApps, new ResolveInfo.DisplayNameComparator(mPackageManager));

            for (ResolveInfo res : mAllApps) {
                //该应用的包名和主Activity
                String pkg = res.activityInfo.packageName;
                String cls = res.activityInfo.name;

                if (pkg.contains(bean.packageName)) {
                    ComponentName componet = new ComponentName(pkg, cls);

                    Intent intent = new Intent();
                    intent.setComponent(componet);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intent);
                    return;
                }
            }
            DialogUtil.showToastShort(mContext, StringUtils.getString(R.string.no_app, bean.name));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public LiveSoundChoiceView initView(List<LiveSoundChoiceAppBean> list) {
        final int start = 0;
        final int size = list.size();
        final int currentSize = this.list.size();
        this.list.clear();
        adapter.notifyItemRangeRemoved(0, currentSize);
        this.list.addAll(list);
        adapter.notifyItemRangeInserted(0, size);
        return this;
    }

    public LiveSoundChoiceView setLiveSoundAppChoiceListener(LiveSoundAppChoiceListener liveSoundAppChoiceListener) {
        this.listener = liveSoundAppChoiceListener;
        return this;
    }

    public interface LiveSoundAppChoiceListener {

        void choiceApp(LiveSoundChoiceAppBean liveSoundChoiceAppBean, int position);
    }

    public LiveSoundChoiceView setLiveSoundCloseDialogListener(LiveSoundAppCloseListener closeListener) {
        this.closeListener = closeListener;
        return this;
    }

    public interface LiveSoundAppCloseListener {

        void closeDialog();
    }


    @Override
    public LiveSoundChoiceView show() {
        return null;
    }

    @Override
    public LiveSoundChoiceView hide() {
        return null;
    }

    @Override
    public LiveSoundChoiceView destroyView() {
        return null;
    }

    @Override
    public boolean isAnimating() {
        return false;
    }

    @Override
    public void setAnimating(boolean isAnimating) {

    }

    @Override
    public void showShade(boolean show) {

    }

}
