package com.thel.modules.live.Certification;

import com.thel.modules.live.bean.ZmxyAuthBean;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.utils.L;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by lingwei on 2018/3/16.
 */

public class ZhimaCertificationPresenter implements ZhimaCertificationContract.Presenter {
    private final ZhimaCertificationContract.View view;

    public ZhimaCertificationPresenter(ZhimaCertificationContract.View view) {
        this.view = view;
        view.setPresenter(this);
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {

    }

    /**
     * 获取芝麻认证的url地址
     **/
    @Override
    public void postZmAuthUrl(String userName, String idCardNumber, String callback) {
        Flowable<ZmxyAuthBean> flowable = RequestBusiness.getInstance().postZmxyAuth(userName, idCardNumber, callback);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<ZmxyAuthBean>() {
            @Override
            public void onNext(ZmxyAuthBean data) {
                super.onNext(data);
                view.getAuthUrl(data);
                L.d("ZhimaCertificationActivity", "granted:" + data.data.authUrl);

            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                L.d("ZhimaCertificationActivity", "onerror:" + t.getMessage());

            }

            @Override
            public void onComplete() {
                super.onComplete();
            }
        });
    }
   /* *//**
     * 验证芝麻信用回调数据
     **//*
    @Override
    public void postCheckCallback(String params, String sign) {
        Flowable<ZmxyCheckCallBackBean> flowable = RequestBusiness.getInstance().postCheckCallback(sign,params);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<ZmxyCheckCallBackBean>() {
            @Override
            public void onNext(ZmxyCheckCallBackBean data) {
                super.onNext(data);
                view.getCheckCallBackResult(data);
                L.d("ZhimaCertiCallBack","granted:"+data.data.toString());

            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                L.d("ZhimaCertificationActivity","onerror:"+t.getMessage());

            }

            @Override
            public void onComplete() {
                super.onComplete();
            }
        });
    }*/
}
