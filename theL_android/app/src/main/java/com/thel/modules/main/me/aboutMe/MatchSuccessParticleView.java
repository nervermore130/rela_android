package com.thel.modules.main.me.aboutMe;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.Point;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.thel.utils.AppInit;

import java.util.Random;

/**
 * Created by test1 on 2017/10/25.
 */

public class MatchSuccessParticleView extends RelativeLayout {
    private final Context mContext;
    private int[] res;
    private Random mRandom;
    private int mWidth;
    private int mHeight;
    private boolean isCircle = true;
    private int minRotateCycle;
    private int difRotateCycle;
    private int minDropDuration;
    private int differDropDuration;
    private int rate;
    private int perCount;
    private Handler mHandler;
    private boolean isPlaying;

    public MatchSuccessParticleView(Context context) {
        this(context, null);
    }

    public MatchSuccessParticleView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MatchSuccessParticleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }

    private void init() {
        mRandom = new Random();
        minRotateCycle = 5;
        difRotateCycle = 8;
        minDropDuration = 10 * 1000;//最小掉落时间
        differDropDuration = 15 * 1000;//掉落时间变化区间
        rate = 400;
        perCount = 5;
        mHandler = new Handler(Looper.getMainLooper());
        isPlaying = false;
    }

    public MatchSuccessParticleView initView(int[] res) {
        this.res = res;
        return this;
    }

    public MatchSuccessParticleView initView(int[] res, boolean circle) {
        this.res = res;
        this.isCircle = circle;
        return this;
    }

    public MatchSuccessParticleView start() {
        setVisibility(View.VISIBLE);
        isPlaying = true;
        mWidth = getMeasuredWidth();
        mHeight = getMeasuredHeight();
        if (mWidth <= 0) {
            mWidth = AppInit.displayMetrics.widthPixels;
        }
        if (mHeight <= 0) {
            mHeight = AppInit.displayMetrics.heightPixels;
        }
        addViews();
        return this;
    }

    public MatchSuccessParticleView stop() {
        isPlaying = false;
        setVisibility(View.GONE);
        clearAnimation();
        removeAllViews();
        if (addViewRunnable != null) {
            removeCallbacks(addViewRunnable);
        }
        return this;
    }

    private void addViews() {
        post(addViewRunnable);
    }

    private Runnable addViewRunnable = new Runnable() {
        @Override public void run() {
            for (int i = 0; i < perCount; i++) {
                addView();
            }
            if (isPlaying) {
                postDelayed(this, rate);
            }
        }
    };

    private void addView() {
        final int length = res.length;
        final int resId = res[mRandom.nextInt(length)];
        final ImageView imageView = new ImageView(mContext);
        imageView.setAdjustViewBounds(true);
        imageView.setImageResource(resId);
        addView(imageView, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        measureChild(imageView, MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
        animView(imageView);
    }

    private void animView(final ImageView imageView) {
        final int width = imageView.getMeasuredWidth();
        final int height = imageView.getMeasuredHeight();
       /* final RelativeLayout.LayoutParams param= (LayoutParams) imageView.getLayoutParams();
        param.addRule(ALIGN_PARENT_TOP);
        param.addRule(ALIGN_PARENT_LEFT);
        param.leftMargin=mRandom.nextInt(mWidth+2*width)-width;
        param.topMargin=-height;*/
        final int startX = mRandom.nextInt(mWidth + width) - width;
        final int startY = -height;
        final int endX = mRandom.nextInt(7 * mWidth) - 3 * mWidth;
        final int endY = mHeight;
        Point startPoint = new Point(startX, startY);
        Point endPoint = new Point(endX, endY);
        Path path = new Path();
        path.moveTo(startPoint.x, startPoint.y);
        path.lineTo(endPoint.x, endPoint.y);
        int circle = 0;
        if (isCircle) {
            circle = minRotateCycle + mRandom.nextInt(difRotateCycle);
        }
        final int dropDuration = minDropDuration + mRandom.nextInt(differDropDuration);

        FloatAnimation anim = new FloatAnimation(path, this, imageView, dropDuration, circle);
        anim.setDuration(dropDuration);
        anim.setInterpolator(new LinearInterpolator());
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                post(new Runnable() {
                    @Override
                    public void run() {
                        remove(imageView);
                    }
                });
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        imageView.startAnimation(anim);
    }

    private void remove(ImageView imageView) {
        removeView(imageView);
        imageView.destroyDrawingCache();
        imageView = null;
    }

    @Override
    protected void onDetachedFromWindow() {
        removeAllViews();
        super.onDetachedFromWindow();
    }

    static class FloatAnimation extends Animation {
        private final int mCycle;
        private final int mDropDuration;
        private PathMeasure mPm;
        private View mView;
        private float mDistance;

        public FloatAnimation(Path path, View parent, View child, int dropDuration, int cycle) {
            mPm = new PathMeasure(path, false);
            mDistance = mPm.getLength();
            mView = child;
            parent.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            mCycle = cycle;
            mDropDuration = dropDuration;
        }

        @Override
        protected void applyTransformation(float factor, Transformation transformation) {
            Matrix matrix = transformation.getMatrix();
            mPm.getMatrix(mDistance * factor, matrix, PathMeasure.POSITION_MATRIX_FLAG);
            if (mCycle > 0) {
                mView.setRotation(360 * mCycle * factor);
            }
        }
    }
}
