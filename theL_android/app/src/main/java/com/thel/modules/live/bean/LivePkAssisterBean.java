package com.thel.modules.live.bean;

/**
 * Created by waiarl on 2017/11/30.
 * 直播PK的助攻bean
 */

public class LivePkAssisterBean {
    public String userId;
    public String nickName;
    public String avatar;
    public int gold;
    /**
     * 对谁谁的助攻
     */
    public String toUserId;

}
