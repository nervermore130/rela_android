package com.thel.modules.main.me.aboutMe;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseDataBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.imp.BaseFunctionFragment;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.main.me.adapter.FriendsListAdapter;
import com.thel.modules.main.me.bean.FriendsBean;
import com.thel.modules.main.me.bean.FriendsListBean;
import com.thel.modules.main.me.match.eventcollect.collect.SocialLogUtils;
import com.thel.modules.main.userinfo.MyLinearLayoutManager;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.utils.DialogUtil;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;

import java.util.ArrayList;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static android.app.Activity.RESULT_OK;

/**
 * 好友页面(关注的人和粉丝)
 * Created by lingwei on 2017/9/26.
 */

public class FollowingAndFansFragment extends BaseFunctionFragment {
    private String Type;
    private ArrayList<FriendsBean> listPlus = new ArrayList<>();
    private ArrayList<FriendsBean> hostPlus = new ArrayList<>();

    public static final String TYPE_FOLLOW = "following";
    public static final String TYPE_FANS = "fans";
    public static final String TYPE_FRIEND = "friend";
    private int total;
    private int curPage = 1; // 将要请求的页号
    private SwipeRefreshLayout swipe_container;
    private RecyclerView mRecyclerView;
    private FriendsListAdapter listAdapter;
    private LinearLayout empty_view;
    private TextView empty_view_txt;
    private int page_size = 20;
    private int currentCountForOnce = -1;
    private MyLinearLayoutManager manager;
    // 刷新类型，全部刷新（即下拉刷新）为1，还是加载下一页为2
    public int refreshType = 0;
    public final int REFRESH_TYPE_ALL = 1;
    public final int REFRESH_TYPE_NEXT_PAGE = 2;
    private View rootview;
    private FriendsBean friendsBean;
    private ImageView img_user_3;
    private ImageView img_user_2;
    private ImageView img_user_1;
    private View rel_1;
    private View rel_2;
    private View rel_3;
    private String pageId;
    private String from_page;
    private String from_page_id;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() != null && getArguments() != null) {
            Bundle bundle = getArguments();
            Type = bundle.getString(TheLConstants.BUNDLE_KEY_REPORT_TYPE);
            from_page = bundle.getString(TheLConstants.FROM_PAGE);
            from_page_id = bundle.getString(TheLConstants.FROM_PAGE_ID);
            pageId = Utils.getPageId();

        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.following_and_fans_activity, container, false);
        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showTranslucentView();
        findViewById();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getActivity() != null && isAdded()) {

            refreshData();

            setListener();

            processBusiness(curPage, page_size, REFRESH_TYPE_ALL);

            loadNetHostFollow();
        }

    }

    private void findViewById() {
        empty_view = rootview.findViewById(R.id.empty_view);
        empty_view_txt = rootview.findViewById(R.id.empty_view_txt);
        swipe_container = rootview.findViewById(R.id.swipe_container);
        ViewUtils.initSwipeRefreshLayout(swipe_container);
        mRecyclerView = swipe_container.findViewById(R.id.listView);
        manager = new MyLinearLayoutManager(getActivity());
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setHasFixedSize(true);
        listAdapter = new FriendsListAdapter(listPlus, Type, getActivity(),from_page,from_page_id,pageId);
        View header = View.inflate(getActivity(), R.layout.relationship_head, null);
        LinearLayout lin_search_entrance = header.findViewById(R.id.lin_search_entrance);
        /**
         * 关注的主播正在直播的头像
         * */
        rel_1 = header.findViewById(R.id.rel_1);
        img_user_1 = header.findViewById(R.id.img_user_1);
        rel_2 = header.findViewById(R.id.rel_2);
        img_user_2 = header.findViewById(R.id.img_user_2);
        rel_3 = header.findViewById(R.id.rel_3);
        img_user_3 = header.findViewById(R.id.img_user_3);

        lin_search_entrance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                Intent intent = new Intent(getActivity(), SearchMyFriendsActivity.class);
                startActivity(intent);
            }
        });
        RelativeLayout secret_container = header.findViewById(R.id.secret_container);
        secret_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), SecretFollowActivity.class);
                v.getContext().startActivity(intent);
            }
        });
        RelativeLayout rl_host_list = header.findViewById(R.id.rl_host_list);
        rl_host_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), HostFollowActivity.class);
                startActivity(intent);
            }
        });
        if (Type.equals(TYPE_FOLLOW)) {
            listAdapter.addHeaderView(header);
        } else if (Type.equals(TYPE_FRIEND)) {
            listAdapter.addHeaderView(header);
            secret_container.setVisibility(View.GONE);
            rl_host_list.setVisibility(View.GONE);
        }
        mRecyclerView.setAdapter(listAdapter);

    }

    private long lastClickTime = 0;

    private void setListener() {

        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshData();
            }
        });
        listAdapter.setOnLoadMoreListener(new BaseRecyclerViewAdapter.RequestLoadMoreListener() {//加载更多
            @Override
            public void onLoadMoreRequested() {
                mRecyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        if (currentCountForOnce > 0) {
                            processBusiness(curPage, page_size, REFRESH_TYPE_NEXT_PAGE);
                        } else {
                            listAdapter.openLoadMore(0, false);
                            if (listPlus.size() > 0 && getContext() != null) {
                                View view = LayoutInflater.from(getContext()).inflate(R.layout.load_more_footer_layout, (ViewGroup) mRecyclerView.getParent(), false);
                                ((TextView) view.findViewById(R.id.text)).setText(getString(R.string.info_no_more));
                                listAdapter.addFooterView(view);
                            }
                        }
                    }
                });
            }
        });
        listAdapter.setReloadMoreListener(new BaseRecyclerViewAdapter.ReloadMoreListener() {
            @Override
            public void reloadMore() {
                listAdapter.removeAllFooterView();
                listAdapter.openLoadMore(true);
                processBusiness(curPage, page_size, REFRESH_TYPE_NEXT_PAGE);
            }
        });
        listAdapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                ViewUtils.preventViewMultipleClick(view, 2000);
                gotoUserInfoActivity(position);
            }
        });
        /**
         * 粉丝页才可以进行移除操作
         * */
        if (TYPE_FANS.equals(Type)) {

            listAdapter.setOnItemLongClickListener(new FriendsListAdapter.OnItemLongClickListener() {
                @Override
                public void onItemLongClick(View view, int position, FriendsBean item) {
                    showDialog(item);
                }

            });
        }

    }

    @Override
    public void onFollowStatusChanged(int followStatus, String userId, String nickName, String avatar) {
        super.onFollowStatusChanged(followStatus, userId, nickName, avatar);
        changeFollowStatus(userId, followStatus);
    }

    private void changeFollowStatus(String followUserId, int followStatus) {
        final int count = listAdapter.getData().size();
        for (int i = 0; i < count; i++) {
            FriendsBean friendsBean = listAdapter.getItem(i);
            if (followUserId.equals(friendsBean.userId + "") && followStatus == 0) {
                listAdapter.notifyItemChanged(i + listAdapter.getHeaderLayoutCount());
                break;
            }
        }

    }

    private void showDialog(final FriendsBean item) {
        DialogUtil.getInstance().showSelectionDialog(getActivity(), new String[]{(TheLApp.getContext().getString(R.string.delete_fans)), TheLApp.getContext().getString(R.string.info_no)}, new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        //删除
                        final String userId = item.userId;
                        Flowable<BaseDataBean> flowable = RequestBusiness.getInstance().postRemoveFans(userId);
                        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
                            @Override
                            public void onNext(BaseDataBean baseDataBean) {
                                super.onNext(baseDataBean);
                                listPlus.remove(item);
                                listAdapter.setNewData(listPlus);

                                if (total <= 1) {
//                                    txt_title.setText(total + TheLApp.context.getString(R.string.friends_activity_fans_odd));
                                } else {
//                                    txt_title.setText(total - 1 + TheLApp.context.getString(R.string.friends_activity_fans_even));
                                    total--;
                                    if (relationshipListener != null)
                                        relationshipListener.total(total);
                                }
                            }

                            @Override
                            public void onError(Throwable t) {
                                DialogUtil.showToastShort(getActivity(), TheLApp.context.getString(R.string.operate_mistake));

                            }

                            @Override
                            public void onComplete() {
                                DialogUtil.showToastShort(getActivity(), TheLApp.context.getString(R.string.deleted_fans));

                            }
                        });

                        break;
                    case 1:
                        break;
                    default:
                        break;
                }
                DialogUtil.getInstance().closeDialog();

            }
        }, true, 1, null);
    }

    private void gotoUserInfoActivity(int position) {
        friendsBean = listAdapter.getData().get(position);
//        Intent intent = new Intent(getActivity(), UserInfoActivity.class);
//        Bundle bundle = new Bundle();
//        bundle.putString(TheLConstants.BUNDLE_KEY_USER_ID, friendsBean.userId);
//        bundle.putString("fromPage", this.getClass().getName());
//        if (TYPE_FOLLOW.equals(Type)) {
//            bundle.putString(TheLConstants.BUNDLE_KEY_INTENT_FROM, UserInfoActivity.FROM_TYPE_FOLLOW);
//        }
//        intent.putExtras(bundle);
//        startActivityForResult(intent, TheLConstants.BUNDLE_CODE_FRIENDS_ACTIVITY);
        if (TYPE_FANS.equals(Type)) {
            SocialLogUtils.getInstance().traceSocialData("my.fans", pageId, "head", from_page, from_page_id);
        }else if (TYPE_FRIEND.equals(Type)){
            SocialLogUtils.getInstance().traceSocialData("my.friend", pageId, "head", from_page, from_page_id);

        }else if (TYPE_FOLLOW.equals(Type)){
            SocialLogUtils.getInstance().traceSocialData("my.follow", pageId, "head", from_page, from_page_id);
        }

        FlutterRouterConfig.Companion.gotoUserInfo(friendsBean.userId);
    }

    private void processBusiness(int page, int pageSize, int type) {
        curPage = page;
        page_size = pageSize;
        refreshType = type;

        loadNetData(page, pageSize);

    }

    private void loadNetHostFollow() {

        Flowable<FriendsListBean> flowable = RequestBusiness.getInstance().getFollowAnchorsList(0 + "");
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<FriendsListBean>() {
            @Override
            public void onNext(FriendsListBean data) {
                super.onNext(data);
                setFollowAnchors(data);
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                requestFiled();
            }

            @Override
            public void onComplete() {
                super.onComplete();
                requestFinished();
            }
        });
    }

    private void setFollowAnchors(FriendsListBean followAnchors) {
        if (followAnchors.data == null) {
            return;
        }
        hostPlus.clear();

        hostPlus.addAll(followAnchors.data.users);

        int length = hostPlus.size();

        for (int i = 0; i < length; i++) {
            if (i == 0 && hostPlus.get(i).liveInfo != null && hostPlus.get(i).liveInfo.live > 0) {

                rel_1.setVisibility(View.VISIBLE);
                ImageLoaderManager.imageLoaderDefaultCircle(img_user_1, R.mipmap.icon_user, hostPlus.get(0).avatar, TheLConstants.AVATAR_SMALL_SIZE, TheLConstants.AVATAR_SMALL_SIZE);
            } else if (i == 1 && hostPlus.get(i).liveInfo != null && hostPlus.get(i).liveInfo.live > 0) {
                rel_2.setVisibility(View.VISIBLE);
                ImageLoaderManager.imageLoaderDefaultCircle(img_user_2, R.mipmap.icon_user, hostPlus.get(1).avatar, TheLConstants.AVATAR_SMALL_SIZE, TheLConstants.AVATAR_SMALL_SIZE);

            } else if (i == 2 && hostPlus.get(i).liveInfo != null && hostPlus.get(i).liveInfo.live > 0) {
                rel_3.setVisibility(View.VISIBLE);
                ImageLoaderManager.imageLoaderDefaultCircle(img_user_3, R.mipmap.icon_user, hostPlus.get(2).avatar, TheLConstants.AVATAR_SMALL_SIZE, TheLConstants.AVATAR_SMALL_SIZE);

            }
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        /*if (getActivity() != null && TYPE_FOLLOW.equals(Type)) {
            refreshData();
        }*/
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case TheLConstants.BUNDLE_CODE_FRIENDS_ACTIVITY:
                    int followState = data.getIntExtra("followState", 0);
                    String userId = data.getStringExtra("userId");
                    if (!TextUtils.isEmpty(userId) && TYPE_FOLLOW.equals(Type) && followState == 2 || followState == 0) {
                        listPlus.remove(friendsBean);
                        listAdapter.notifyDataSetChanged();
                        if (total <= 1) {
//                            txt_title.setText(total + TheLApp.context.getResources().getString(R.string.friends_activity_follow_odd));
                        } else {
//                            txt_title.setText(total - 1 + TheLApp.context.getResources().getString(R.string.friends_activity_follow_even));
                            total--;
                            if (relationshipListener != null) relationshipListener.total(total);
                        }
                    }

                    break;
            }
        }

    }

    private void refreshData() {
        if (swipe_container != null && !swipe_container.isRefreshing()) {
            swipe_container.post(new Runnable() {
                @Override
                public void run() {
                    swipe_container.setRefreshing(true);
                }
            });
        }
        processBusiness(1, page_size, REFRESH_TYPE_ALL);
        mRecyclerView.scrollToPosition(0);

    }

    private void loadNetData(int page, int pageSize) {
        if (TYPE_FOLLOW.equals(Type)) {
            Flowable<FriendsListBean> flowable = RequestBusiness.getInstance().getFollowList(ShareFileUtils.getString(ShareFileUtils.ID, ""), pageSize + "", page + "");
            flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<FriendsListBean>() {
                @Override
                public void onNext(FriendsListBean data) {
                    super.onNext(data);
                    setFollowAndFans(data, TYPE_FOLLOW);
                }

                @Override
                public void onError(Throwable t) {
                    super.onError(t);
                    requestFiled();
                }

                @Override
                public void onComplete() {
                    super.onComplete();
                    requestFinished();
                }
            });
        } else if (TYPE_FANS.equals(Type)) {
            Flowable<FriendsListBean> flowable = RequestBusiness.getInstance().getFansList(ShareFileUtils.getString(ShareFileUtils.ID, ""), page_size + "", page + "");
            flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<FriendsListBean>() {
                @Override
                public void onNext(FriendsListBean friendsListBean) {
                    super.onNext(friendsListBean);
                    setFollowAndFans(friendsListBean, TYPE_FANS);
                }

                @Override
                public void onError(Throwable t) {
                    requestFiled();
                }

                @Override
                public void onComplete() {
                    requestFinished();

                }
            });
        } else if (TYPE_FRIEND.equals(Type)) {
            Flowable<FriendsListBean> flowable = RequestBusiness.getInstance().getFriendsList(ShareFileUtils.getString(ShareFileUtils.ID, ""), page_size + "", page + "");
            flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<FriendsListBean>() {
                @Override
                public void onNext(FriendsListBean friendsListBean) {
                    super.onNext(friendsListBean);
                    setFollowAndFans(friendsListBean, TYPE_FRIEND);

                }

                @Override
                public void onError(Throwable t) {
                    requestFiled();
                }

                @Override
                public void onComplete() {
                    requestFinished();
                }
            });
        }

    }

    private void requestFiled() {
        if (refreshType == REFRESH_TYPE_NEXT_PAGE) {

            mRecyclerView.post(new Runnable() {
                @Override
                public void run() {
                    listAdapter.loadMoreFailed((ViewGroup) mRecyclerView.getParent());
                }
            });
        }
    }

    private void requestFinished() {
        if (swipe_container != null)
            swipe_container.postDelayed(new Runnable() {

                @Override
                public void run() {
                    if (swipe_container != null && swipe_container.isRefreshing())
                        swipe_container.setRefreshing(false);
                }
            }, 1000);
    }

    private void setFollowAndFans(FriendsListBean friendsListBean, String type) {
        if (friendsListBean.data == null)
            return;
        //刷新
        if (REFRESH_TYPE_ALL == refreshType) {
            listPlus.clear();
        }

        if (type.equals(TYPE_FOLLOW)) {
            total = friendsListBean.data.followersTotal;
//            if (total <= 1) {
//                txt_title.setText(total + TheLApp.context.getResources().getString(R.string.friends_activity_follow_odd));
//            } else {
//                txt_title.setText(total + TheLApp.context.getResources().getString(R.string.friends_activity_follow_even));
//            }
        } else if (type.equals(TYPE_FANS)) {
            total = friendsListBean.data.fansTotal;
//            if (total <= 1) {
//                txt_title.setText(total + TheLApp.context.getResources().getString(R.string.friends_activity_fans_odd));
//            } else {
//                txt_title.setText(total + TheLApp.context.getResources().getString(R.string.friends_activity_fans_even));
//            }
        } else if (type.equals(TYPE_FRIEND)) {
            total = friendsListBean.data.friendTotal;
        }
        if (relationshipListener != null) relationshipListener.total(total);
        currentCountForOnce = friendsListBean.data.users.size();
        listPlus.addAll(friendsListBean.data.users);

        if (REFRESH_TYPE_ALL == refreshType) {
            listAdapter.removeAllFooterView();
            listAdapter.setNewData(listPlus);
            curPage = 2;
            if (listPlus.size() > 0) {
                listAdapter.openLoadMore(listPlus.size(), true);
            } else {
                listAdapter.openLoadMore(listPlus.size(), false);
            }
        } else {
            curPage++;
            listAdapter.notifyDataChangedAfterLoadMore(true, listPlus.size());

        }

        //刷新回到顶部
        if (REFRESH_TYPE_ALL == refreshType) {
            if (mRecyclerView.getChildCount() > 0) {
                mRecyclerView.scrollToPosition(0);
            }
        }
    }

    private void setDefaultPage() {

        empty_view.setVisibility(total == 0 ? View.VISIBLE : View.GONE);
        if (TYPE_FOLLOW.equals(Type)) {
            empty_view_txt.setText(R.string.relationship_follow_emtpy);
        } else if (TYPE_FANS.equals(Type)) {
            empty_view_txt.setText(R.string.relationship_fans_emtpy);
        } else if (TYPE_FRIEND.equals(Type)) {
            empty_view_txt.setText(R.string.relationship_friend_emtpy);
        }
    }

    private RelationshipListener relationshipListener;

    public void setRelationshipListener(RelationshipListener relationshipListener) {
        this.relationshipListener = relationshipListener;
    }

    public interface RelationshipListener {
        void total(int total);
    }

}
