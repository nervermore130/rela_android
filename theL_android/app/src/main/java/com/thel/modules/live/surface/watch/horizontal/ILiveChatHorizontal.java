package com.thel.modules.live.surface.watch.horizontal;

import com.thel.modules.live.bean.LiveRoomMsgBean;
import com.thel.modules.live.view.expensive.TopGiftBean;

/**
 * Created by LiuYun
 */
public interface ILiveChatHorizontal {

    //UI_EVENT_CLEAR_INPUT
    void clearInput();

    //UI_EVENT_AUTO_PING
    void autoPingServer();

    //UI_EVENT_LIVE_CLOSED
    void liveClosed();

    //UI_EVENT_CHANNEL_ID
    void showDialogChannelId();

    //UI_EVENT_REQUEST
    void showDialogRequest();

    //UI_EVENT_USER_ID
    void showAlertUserId();

    //UI_EVENT_USER_NAME
    void showAlertUserName();

    //UI_EVENT_GIFT_SEND_RESULT_MSG
    void updateBalance(String result);

    //UI_EVENT_BEEN_BLOCKED
    void beenBlocked();

    //UI_EVENT_SPEAK_TO_FAST
    void speakTooFast();

    //UI_EVENT_LOCK_INPUT
    void lockInput();

    //UI_EVENT_OPEN_INPUT_FALSE
    void openInput();

    //UI_EVENT_OPEN_INPUT_TRUE
    void setDanmuResult(String result);

    //UI_EVENT_REFRESH_MSGS
    void refreshMsg(LiveRoomMsgBean liveRoomMsgBean);

    //UI_EVENT_UPDATE_AUDIENCE_COUNT
    void updateAudienceCount();

    //UI_EVENT_GIFT_RECEIVE_MSG
    void parseGiftMsg(String code, String payload);

    //UI_EVENT_DANMU_RECEIVE_MSG
    void showDanmu(String payload);

    //UI_EVENT_FREE_BARRAGE
    void freeBarrage();

    //UI_EVENT_JOIN_VIP_USER
    void joinVipUser(LiveRoomMsgBean liveRoomMsgBean);

    //UI_EVENT_REFRESH_MSGS
    void refreshMsg();

    //添加聊天消息
    void addLiveRoomMsg(LiveRoomMsgBean liveRoomMsgBean);

    //UI_EVENT_TOP_GIFT
    void showTopGiftView(TopGiftBean topGiftBean);

    //UI_EVENT_UPDATE_BROADCASTER_NET_STATUS
    void updateBroadcasterNetStatus(int status);

}
