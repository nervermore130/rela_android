package com.thel.modules.main.nearby.Utils;

import android.text.TextUtils;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.moments.MomentsListBean;
import com.thel.bean.user.NearUserBean;
import com.thel.bean.user.NearUserListNetBean;
import com.thel.imp.black.BlackUtils;
import com.thel.utils.L;
import com.thel.utils.SharedPrefUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by waiarl on 2017/10/11.
 */

public class NearbyUtils {

    private static final long oneDayInMillis = 24 * 60 * 60 * 1000;

    public static void filterUser(List<NearUserBean> blockUserList) {
        if (blockUserList != null) {

            ListIterator<NearUserBean> iterator = blockUserList.listIterator();

            while (iterator.hasNext()) {

                NearUserBean nearUserBean = iterator.next();

                if (BlackUtils.isUserBlock(String.valueOf(nearUserBean.userId))) {

                    iterator.remove();

                }
            }
        }
    }

    public static void filterBlack(List<NearUserBean> blockUserList) {

        if (blockUserList != null) {
            ListIterator<NearUserBean> iterator = blockUserList.listIterator();

            while (iterator.hasNext()) {

                NearUserBean nearUserBean = iterator.next();

                if (BlackUtils.isBlack(String.valueOf(nearUserBean.userId))) {
                    iterator.remove();
                }
            }
        }

    }

    public static void filterEquallyMoment(List<NearUserBean> oldData, List<NearUserBean> newData) {

        if (oldData != null && newData != null && oldData.size() > 0 && newData.size() > 0) {
            NearUserBean oldBean = oldData.get(oldData.size() - 1);
            NearUserBean newBean = oldData.get(0);
            if (oldBean.userId == newBean.userId) {
                newData.remove(newBean);
            }
        }

    }

    private static List<String> removeEmptyElem(List<String> processList) {
        List<String> list = new ArrayList<String>();
        list.add("");
        processList.removeAll(list);
        return processList;
    }

    public static void filtNearbyMoment(MomentsListBean momentsListBean) {
        //过滤黑名单和被屏蔽
        if (momentsListBean == null || momentsListBean.momentsList == null) {
            return;
        }
        // List<String> blackList = Arrays.asList(SharedPrefUtils.getString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.BLACK_LIST, "").split(","));
        List<String> blackList = BlackUtils.getBlackList();

        List<String> trimBlacklist = new ArrayList<>(blackList);
        trimBlacklist = removeEmptyElem(trimBlacklist);
        List<String> reportUserList = Arrays.asList(SharedPrefUtils.getString(SharedPrefUtils.FILE_NAME_BLOCK_FILE, SharedPrefUtils.REPORT_USER_LIST, "").split(","));
        List<String> trimReportUserList = new ArrayList<>(blackList);
        trimReportUserList = removeEmptyElem(trimReportUserList);
        List<String> blockMeList = BlackUtils.getblockMeList();
        List<String> trimBlockMeUserList = new ArrayList<>(blackList);
        trimBlockMeUserList = removeEmptyElem(blockMeList);
        List<String> blockUsers = new ArrayList<String>();
        if (trimBlacklist.size() > 0 && !TextUtils.isEmpty(trimBlacklist.get(0))) {
            blockUsers.addAll(trimBlacklist);
        }
        if (trimReportUserList.size() > 0 && !TextUtils.isEmpty(trimReportUserList.get(0))) {
            blockUsers.addAll(trimReportUserList);
        }
        if (trimBlockMeUserList.size() > 0 && !TextUtils.isEmpty(trimBlockMeUserList.get(0))) {
            blockUsers.addAll(trimBlockMeUserList);
        }
        if (blockUsers.size() > 0) {
            List<MomentsBean> blockUserList = new ArrayList<>();
            for (MomentsBean momentsBean : momentsListBean.momentsList) {
                for (String userId : blockUsers) {
                    if (userId.equals(momentsBean.userId + "")) {
                        blockUserList.add(momentsBean);
                        break;
                    }
                }
            }
            momentsListBean.momentsList.removeAll(blockUserList);
        }

    }

    public static void subNearbyMomentCount(MomentsListBean momentsListBean, int count) {
        final int size = momentsListBean.momentsList.size();
        //附近的日志9宫格最多只能9条数据
        if (size > count) {
            final int liCount = size - count;
            for (int i = 0; i < liCount; i++) {
                momentsListBean.momentsList.remove(count);
            }
        }
    }

    /**
     * 根据服务器返回时间与现在时间，显示具体时间
     *
     * @param create_date
     * @return
     */
    public static String getWhoSeenMeDate(String create_date) {

        if (TextUtils.isEmpty(create_date)) {
            return "";
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");
        try {
            Date date = sdf.parse(create_date);
            long time = date.getTime();
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(System.currentTimeMillis());

            Calendar calEndToday = (Calendar) cal.clone();
            calEndToday.set(Calendar.HOUR_OF_DAY, 23);
            calEndToday.set(Calendar.MINUTE, 59);
            calEndToday.set(Calendar.SECOND, 59);
            calEndToday.set(Calendar.MILLISECOND, 999);

            if (time > calEndToday.getTimeInMillis() - oneDayInMillis && time <= calEndToday.getTimeInMillis()) {// 今天的记录
                return TheLApp.getContext().getString(R.string.chat_activity_today);
            } else if (time > calEndToday.getTimeInMillis() - oneDayInMillis * 2 && time <= calEndToday.getTimeInMillis() - oneDayInMillis) {// 昨天的记录
                return TheLApp.getContext().getString(R.string.chat_activity_yesterday);
            } else if (time <= calEndToday.getTimeInMillis() - oneDayInMillis * 2 && time > calEndToday.getTimeInMillis() - oneDayInMillis * 365) {// 昨天之前365天以内的消息
                return ((int) ((cal.getTimeInMillis() - time) / oneDayInMillis) + 1) + TheLApp.getContext().getString(R.string.info_days_ago);
            } else {
                return new SimpleDateFormat("yyyy.MM.dd").format(date);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
}
