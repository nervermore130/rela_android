package com.thel.modules.live.view;

import android.media.MediaPlayer;

/**
 * Created by waiarl on 2017/12/13.
 * pk的一些音乐播放
 */

public class LiveEnterMediaPlayer extends MediaPlayer {
    private static LiveEnterMediaPlayer instance;

    public static LiveEnterMediaPlayer getInstance() {
        if (instance == null) {
            instance = new LiveEnterMediaPlayer();
        }
        return instance;
    }

    @Override
    public void release() {
        super.release();
        instance = null;
    }
}
