package com.thel.modules.main.userinfo;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseAdapter;
import com.thel.base.BaseDataBean;
import com.thel.bean.GuardBean;
import com.thel.bean.ImgShareBean;
import com.thel.bean.LivePermitBean;
import com.thel.bean.user.MyCircleFriendBean;
import com.thel.bean.user.UserInfoBean;
import com.thel.bean.user.UserInfoLiveBean;
import com.thel.bean.user.UserInfoPicBean;
import com.thel.constants.BundleConstants;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.growingio.GrowingIoConstant;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.live.Certification.ZhimaCertificationActivity;
import com.thel.modules.live.surface.watch.LiveWatchActivity;
import com.thel.modules.live.surface.watch.horizontal.LiveWatchHorizontalActivity;
import com.thel.modules.main.home.moments.ReleaseLiveMomentActivity;
import com.thel.modules.main.home.moments.web.WebViewActivity;
import com.thel.modules.main.me.AwaitLiveActivity;
import com.thel.modules.preview_image.UserInfoPhotoActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.service.DefaultRequestService;
import com.thel.ui.adapter.UserPicAdapter;
import com.thel.ui.widget.UserPicRecyclerView;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;
import com.thel.utils.AppInit;
import com.thel.utils.BusinessUtils;
import com.thel.utils.DialogUtil;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.MD5Utils;
import com.thel.utils.MomentUtils;
import com.thel.utils.PermissionUtil;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SizeUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.udesk.photoselect.decoration.GridSpacingItemDecoration;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.thel.utils.ShareFileUtils.IS_RELEASE_MOMENT;

/**
 * Created by waiarl on 2017/10/18.
 */

public class UserInfoMomentHeaderView extends LinearLayout {
    private final Context mContext;
    private LinearLayout lin_live;
    private ImageView img_live;
    private TextView txt_live_name;
    private TextView txt_live_info;
    private ImageView img_arrow;
    private LinearLayout lin_pics;
    private UserPicRecyclerView pic_rv;
    private LinearLayout lin_bffs;
    private TextView txt_my_bffs;
    private RecyclerView recyclerview_bffs;
    private TextView txt_moments_num;
    private UserInfoBean userInfoBean;
    private ImageView img_on_live;
    private TextView txt_on_live_looker_num;
    private TextView statue_view;
    private LinearLayout lin_live_bg;

    private RelativeLayout lin_guard;
    private LinearLayout guard_container;
    private RecyclerView recyclerview_guard;
    private TextView guard_empty;
    private ImageView img_match_arrow;
    private TextView live_title_tv;

    private int momentsNum = 0;
    private GridLayoutManager bffManager;
    private List<MyCircleFriendBean> bffList = new ArrayList<>();
    private BffAdapter bffAdapter;

    private GuardAdapter guardAdapter;
    private List<GuardBean.TopFans> guardList = new ArrayList<>();
    private UserPicAdapter mUserPicAdapter;
    private TextView txt_my_guard;
    private boolean expected = false;

    public enum MomentOperateType {
        DELETE, PUBLISH
    }

    public UserInfoMomentHeaderView(Context context) {
        this(context, null);
    }

    public UserInfoMomentHeaderView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public UserInfoMomentHeaderView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
        setListener();
    }

    public void init() {
        inflate(mContext, R.layout.userinfo_moment_header_view, this);
        lin_live = findViewById(R.id.lin_live);
        img_live = findViewById(R.id.img_live);
        txt_live_name = findViewById(R.id.txt_live_name);
        txt_live_info = findViewById(R.id.txt_live_info);
        img_arrow = findViewById(R.id.img_arrow);
        lin_pics = findViewById(R.id.lin_pics);
        lin_bffs = findViewById(R.id.lin_bffs);
        txt_my_bffs = findViewById(R.id.txt_my_bffs);
        recyclerview_bffs = findViewById(R.id.recyclerview_bffs);
        txt_moments_num = findViewById(R.id.txt_moments_num);
        lin_guard = findViewById(R.id.lin_guard);
        guard_container = findViewById(R.id.guard_container);
        recyclerview_guard = findViewById(R.id.recyclerview_guard);
        guard_empty = findViewById(R.id.guard_empty);
        img_match_arrow = findViewById(R.id.img_match_arrow);
        pic_rv = findViewById(R.id.pic_rv);
        img_on_live = findViewById(R.id.img_on_live);
        lin_live_bg = findViewById(R.id.lin_live_bg);
        txt_on_live_looker_num = findViewById(R.id.txt_on_live_looker_num);
        statue_view = findViewById(R.id.statue_view);
        live_title_tv = findViewById(R.id.live_title_tv);
        txt_my_guard = findViewById(R.id.txt_my_guard);
        initBiffAdapter();
        initGuardAdapter();
        initUserPic();
    }

    public UserInfoMomentHeaderView initView(UserInfoBean userInfoBean) {
        if (userInfoBean == null) {
            setVisibility(GONE);
            return this;
        }
        this.userInfoBean = userInfoBean;
        setVisibility(VISIBLE);
        setLiveView();
        setGuardView();
        setPicView();
        setBffView();
        setMomentsNum();
        return this;
    }

    private void initUserPic() {

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 3);
        pic_rv.setLayoutManager(gridLayoutManager);
        pic_rv.addItemDecoration(new GridSpacingItemDecoration(3,
                SizeUtils.dip2px(TheLApp.context, 2), false));
        mUserPicAdapter = new UserPicAdapter(getContext());
        pic_rv.setAdapter(mUserPicAdapter);

        mUserPicAdapter.setOnItemClickListener(new BaseAdapter.OnItemClickListener<UserInfoPicBean>() {
            @Override
            public void onItemClick(View view, UserInfoPicBean item, int position) {
                Intent i = new Intent(mContext, UserInfoPhotoActivity.class);
                i.putExtra("fromPage", "UserInfoActivity");
                i.putExtra("userinfo", userInfoBean.picList);
                i.putExtra("position", position);
                i.putExtra("isMyself", Utils.isMyself(userInfoBean.id + ""));
                i.putExtra("userId", userInfoBean.id + "");
                Bundle bundle = new Bundle();
                //ImgShareBean bean = Utils.getUserImageShareBean(userInfoBean);
                //bundle.putSerializable(TheLConstants.BUNDLER_KEY_USER_IMAGE, bean);
                bundle.putSerializable(TheLConstants.BUNDLE_KEY_PIC_BEAN, item);
                i.putExtras(bundle);
                i.putExtra(TheLConstants.BUNDLE_KEY_RELA_ID, userInfoBean.userName);//username后台定义的热拉id
                i.putExtra(TheLConstants.IS_WATERMARK, true);

                mContext.startActivity(i);
            }
        });

    }

    private void setMomentsNum() {
        if (momentsNum == 0 || momentsNum == 1) {
            txt_moments_num.setText(momentsNum + TheLApp.getContext().getString(R.string.userinfo_activity_moments_num_odd));
        } else {
            txt_moments_num.setText(momentsNum + TheLApp.getContext().getString(R.string.userinfo_activity_moments_num_even1));
        }
    }

    public void refreshMomentNum(int momentsNum) {
        this.momentsNum = momentsNum;
        setMomentsNum();
    }

    public void refreshMomentNum(MomentOperateType type) {
        switch (type) {
            case DELETE:
                momentsNum -= 1;
                break;
            case PUBLISH:
                momentsNum += 1;
                break;
        }
        setMomentsNum();
    }


    @SuppressLint("SetTextI18n")
    private void setLiveView() {

        UserInfoLiveBean liveInfo = userInfoBean.liveInfo;

        //拥有直播权限但未发起过直播，或没有直播权限的，个人主页不应展示直播信息栏。
        if (userInfoBean.guard == null || userInfoBean.guard.livePerm != 1 || userInfoBean.await == null || TextUtils.isEmpty(userInfoBean.await.lastLiveTime)) {
            lin_live.setVisibility(GONE);
            return;
        }

        if (userInfoBean.guard.topFansType.equals("today")) {
            txt_my_guard.setText(R.string.guard_today);
        } else if (userInfoBean.guard.topFansType.equals("week")) {
            txt_my_guard.setText(R.string.guard_week);
        } else {
            txt_my_guard.setText(R.string.guard_all);
        }

        if (!userInfoBean.id.equals(UserUtils.getMyUserId())) {

            if (liveInfo != null) {

                if (liveInfo.live >= 1) {

                    lin_live.setVisibility(VISIBLE);
                    live_title_tv.setText(userInfoBean.nickName + " " + mContext.getString(R.string.is_on_live));

                    txt_live_info.setText(liveInfo.discription);

                    ImageLoaderManager.imageLoaderDefaultCorner(img_live, R.mipmap.btn_post_live_1, liveInfo.imageUrl, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE, 15f);
                    if (liveInfo.type.equals("1")) {
                        img_arrow.setImageResource(R.mipmap.icon_green_voice_live);
                        img_on_live.setImageResource(R.mipmap.icon_voice_live);
                    } else if (liveInfo.type.equals("0")) {
                        img_arrow.setImageResource(R.mipmap.icon_green_camera_live);
                        img_on_live.setImageResource(R.mipmap.icon_camera_live);
                    }

                    txt_on_live_looker_num.setText(String.valueOf(liveInfo.looker));

                    statue_view.setText(TheLApp.context.getResources().getString(R.string.watch_live));
                    statue_view.setClickable(true);
                    statue_view.setSelected(false);
                }
            } else {

                lin_live_bg.setVisibility(GONE);

                img_live.setVisibility(GONE);

                if (!TextUtils.isEmpty(userInfoBean.await.lastLiveTime)) {
                    live_title_tv.setText(TheLApp.context.getResources().getString(R.string.last_live_broadcast_time, MomentUtils.getReleaseTimeShow(0, userInfoBean.await.lastLiveTime)));
                }

                if (expected) {
                    if (userInfoBean.await.awaitCount == 0) {
                        txt_live_info.setText(mContext.getString(R.string.call_on_host_for_live));
                        txt_live_info.setTextColor(TheLApp.context.getResources().getColor(R.color.text_color_gray));
                    } else if (userInfoBean.await.awaitCount == 1) {
                        txt_live_info.setText(TheLApp.context.getResources().getString(R.string.expecting_her_live_s, 1));
                        txt_live_info.setTextColor(TheLApp.context.getResources().getColor(R.color.rela_tab_normal));
                    } else {
                        txt_live_info.setText(TheLApp.context.getResources().getString(R.string.expecting_her_live, userInfoBean.await.awaitCount));
                        txt_live_info.setTextColor(TheLApp.context.getResources().getColor(R.color.rela_tab_normal));
                    }
                } else {

                    if (userInfoBean.await.awaitCount == 0) {
                        txt_live_info.setText(mContext.getString(R.string.call_on_host_for_live));
                        txt_live_info.setTextColor(TheLApp.context.getResources().getColor(R.color.text_color_gray));
                    } else if (userInfoBean.await.awaitCount == 1) {
                        txt_live_info.setText(TheLApp.context.getResources().getString(R.string.expecting_her_live_s, 1));
                        txt_live_info.setTextColor(TheLApp.context.getResources().getColor(R.color.rela_tab_normal));
                    } else {
                        txt_live_info.setText(TheLApp.context.getResources().getString(R.string.expecting_her_live, userInfoBean.await.awaitCount));
                        txt_live_info.setTextColor(TheLApp.context.getResources().getColor(R.color.rela_tab_normal));
                    }
                }

                if (userInfoBean.await.awaitStatus) {
                    statue_view.setText(TheLApp.context.getResources().getString(R.string.expected));
                    statue_view.setClickable(false);
                    statue_view.setSelected(true);
                    statue_view.setTextColor(TheLApp.context.getResources().getColor(R.color.text_color_gray));
                } else {
                    statue_view.setText(TheLApp.context.getResources().getString(R.string.expect_live));
                    statue_view.setClickable(true);
                    statue_view.setSelected(false);
                }

            }
        } else {

            lin_live_bg.setVisibility(GONE);

            img_live.setVisibility(GONE);

            if (userInfoBean.await.awaitCount == 0) {
                txt_live_info.setVisibility(GONE);
            } else {
                txt_live_info.setVisibility(VISIBLE);
                txt_live_info.setText(TheLApp.context.getResources().getString(R.string.expecting_your_live2, userInfoBean.await.awaitCount));
                txt_live_info.setTextColor(TheLApp.context.getResources().getColor(R.color.rela_tab_normal));

            }

            statue_view.setText(TheLApp.context.getResources().getString(R.string.start_live_show));
            statue_view.setClickable(true);
            statue_view.setSelected(false);

            if (!TextUtils.isEmpty(userInfoBean.await.lastLiveTime)) {
                live_title_tv.setText(TheLApp.context.getResources().getString(R.string.last_live_broadcast_time, MomentUtils.getReleaseTimeShow(0, userInfoBean.await.lastLiveTime)));
            }

        }

    }

    private void setPicView() {
        if (userInfoBean.picList == null || userInfoBean.picList.isEmpty()) {
            lin_pics.setVisibility(GONE);
        } else {
            lin_pics.setVisibility(VISIBLE);
//            picList.clear();
//            picList.addAll(userInfoBean.picList);
//            picAdapter.notifyDataSetChanged();
            pic_rv.setPicCount(userInfoBean.picList.size(), SizeUtils.dip2px(TheLApp.context, 2));
            if (mUserPicAdapter != null) {
                mUserPicAdapter.addData(userInfoBean.picList);
            }
        }
    }

    private void initGuardAdapter() {
        recyclerview_guard.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        recyclerview_guard.setHasFixedSize(true);
        guardAdapter = new GuardAdapter(guardList);
        recyclerview_guard.setAdapter(guardAdapter);
    }

    private void initBiffAdapter() {
        final float imgSize = mContext.getResources().getDimension(R.dimen.userinfo_bfs_avatar_size);
        final float rightMargin = Utils.dip2px(mContext, 10);
        final float startMargin = Utils.dip2px(mContext, 17);
        final int count = (int) ((AppInit.displayMetrics.widthPixels - startMargin) / (imgSize + rightMargin));
        bffManager = new GridLayoutManager(mContext, count);
        bffManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerview_bffs.setLayoutManager(bffManager);
        recyclerview_bffs.setHasFixedSize(true);
        bffAdapter = new BffAdapter(bffList);
        recyclerview_bffs.setAdapter(bffAdapter);
    }

    /**
     * 守护榜
     */
    private void setGuardView() {
        if (userInfoBean.guard != null && userInfoBean.guard.topFans != null) {
            guard_empty.setText(Utils.isMyself(userInfoBean.id) ? R.string.post_your_live : R.string.miss_her_live);
            guardList.clear();
            if (userInfoBean.guard.topFans.size() > 3) {
                guardList.addAll(userInfoBean.guard.topFans.subList(0, 3));
            } else {
                guardList.addAll(userInfoBean.guard.topFans);
            }
            if (guardList.size() == 0) {
                guard_empty.setVisibility(VISIBLE);
                guard_container.setVisibility(GONE);
            } else {
                guard_empty.setVisibility(GONE);
                guard_container.setVisibility(VISIBLE);
            }

         /*   if (guardList != null && guardList.size() > 0) {

                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) recyclerview_guard.getLayoutParams();

                layoutParams.width = guardList.size() * SizeUtils.dip2px(TheLApp.context, 45);

                recyclerview_guard.setLayoutParams(layoutParams);
            }*/

            guardAdapter.notifyDataSetChanged();
        }
    }

    private void setBffView() {
        if (userInfoBean.bffs.size() == 0) {
            lin_bffs.setVisibility(View.GONE);
            return;
        }
        lin_bffs.setVisibility(View.VISIBLE);
        bffList.clear();
        bffList.addAll(userInfoBean.bffs);
        bffAdapter.notifyDataSetChanged();

        final int bffs_count = userInfoBean.bffs.size();
        if (bffs_count > 1) {
            txt_my_bffs.setText(mContext.getString(R.string.userinfo_activity_bffs_even, bffs_count));
        } else {
            txt_my_bffs.setText(mContext.getString(R.string.userinfo_activity_bffs_old, bffs_count));
        }
    }


    private void setListener() {

        lin_live.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                if (userInfoBean != null && userInfoBean.id != null) {
                    if (BusinessUtils.canIntoLiveRoom(TheLApp.getContext(), userInfoBean.id + "")) {

                        if (userInfoBean.liveInfo != null) {
                            MobclickAgent.onEvent(TheLApp.getContext(), "enter_live_room_from_userinfo");
                            Intent intent;
                            if (userInfoBean.liveInfo.isLandscape == 0) {
                                intent = new Intent(mContext, LiveWatchActivity.class);
                            } else {
                                intent = new Intent(mContext, LiveWatchHorizontalActivity.class);
                            }
                            intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, userInfoBean.id + "");
                            intent.putExtra(LiveWatchActivity.FROM_PAGE, LiveWatchActivity.FROM_PAGE_MOMENT);
                            mContext.startActivity(intent);
                            GrowingIOUtil.postWatchLiveEntry(GrowingIoConstant.G_LiveEntry_PersonalPage);
                        }
                    }
                }
            }
        });

        bffAdapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                final MyCircleFriendBean bean = bffAdapter.getItem(position);
//                final Intent intent = new Intent(mContext, UserInfoActivity.class);
//                final Bundle bundle = new Bundle();
//                bundle.putString(TheLConstants.BUNDLE_KEY_USER_ID, bean.userId + "");
//                intent.putExtras(bundle);
//                mContext.startActivity(intent);
                FlutterRouterConfig.Companion.gotoUserInfo(bean.userId+"");
            }
        });

        guardAdapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                final GuardBean.TopFans bean = guardAdapter.getItem(position);
//                final Intent intent = new Intent(mContext, UserInfoActivity.class);
//                final Bundle bundle = new Bundle();
//                bundle.putString(TheLConstants.BUNDLE_KEY_USER_ID, bean.userId + "");
//                intent.putExtras(bundle);
//                mContext.startActivity(intent);
                FlutterRouterConfig.Companion.gotoUserInfo(bean.userId+"");
            }
        });

        lin_guard.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userInfoBean != null && userInfoBean.guard != null && !TextUtils.isEmpty(userInfoBean.guard.topFansLink)) {
                    Intent intent = new Intent(mContext, WebViewActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(BundleConstants.URL, userInfoBean.guard.topFansLink);
                    bundle.putBoolean(WebViewActivity.NEED_SECURITY_CHECK, false);
                    intent.putExtras(bundle);
                    mContext.startActivity(intent);
                }
            }
        });
        img_match_arrow.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userInfoBean != null && userInfoBean.guard != null && !TextUtils.isEmpty(userInfoBean.guard.topFansLink)) {
                    Intent intent = new Intent(mContext, WebViewActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(BundleConstants.URL, userInfoBean.guard.topFansLink);
                    bundle.putBoolean(WebViewActivity.NEED_SECURITY_CHECK, false);
                    intent.putExtras(bundle);
                    mContext.startActivity(intent);
                }
            }
        });

        statue_view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (userInfoBean != null && userInfoBean.id != null && !userInfoBean.id.equals(UserUtils.getMyUserId())) {
                    if (userInfoBean.liveInfo != null && userInfoBean.liveInfo.live == 1) {
                        Intent intent;
                        if (userInfoBean.liveInfo.isLandscape == 1) {
                            intent = new Intent(getContext(), LiveWatchHorizontalActivity.class);
                        } else {
                            intent = new Intent(getContext(), LiveWatchActivity.class);
                        }
                        intent.putExtra(TheLConstants.BUNDLE_KEY_ID, userInfoBean.liveInfo.liveId);
                        intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, userInfoBean.id + "");
                        intent.putExtra(LiveWatchActivity.FROM_PAGE, LiveWatchActivity.FROM_PAGE_MOMENT);
                        getContext().startActivity(intent);
                    } else {
                        if (userInfoBean != null && userInfoBean.await != null) {
                            if (!userInfoBean.await.awaitStatus) {

                                Map<String, String> map = new HashMap<>();

                                map.put("userId", userInfoBean.id);

                                Flowable<BaseDataBean> flowable = DefaultRequestService.createUserRequestService().lookingForwardToLive(MD5Utils.generateSignatureForMap(map));
                                flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<>());
                                userInfoBean.await.awaitCount++;
                                userInfoBean.await.awaitStatus = true;
                                expected = true;

                                setLiveView();
                            }
                        }
                    }

                } else {
                    if (UserUtils.isVerifyCell()) {

                        AppCompatActivity appCompatActivity = (AppCompatActivity) v.getContext();

                        ShareFileUtils.setBoolean(Utils.getMyUserId() + "_" + IS_RELEASE_MOMENT, true);

                        ViewUtils.preventViewMultipleClick(v, 2000);

                        if (ShareFileUtils.getInt(TheLConstants.live_permit, 0) == 1) {
                            PermissionUtil.requestCameraPermission(appCompatActivity, new PermissionUtil.PermissionCallback() {
                                @Override
                                public void granted() {
                                    Intent intent = new Intent(TheLApp.getContext(), ReleaseLiveMomentActivity.class);
                                    appCompatActivity.startActivity(intent);
                                }

                                @Override
                                public void denied() {

                                }

                                @Override
                                public void gotoSetting() {

                                }
                            });
                        } else {
                            RequestBusiness.getInstance().getLivePermit().onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<LivePermitBean>() {
                                @Override
                                public void onNext(LivePermitBean livePermitBean) {
                                    super.onNext(livePermitBean);
                                    if (livePermitBean != null && livePermitBean.data != null) {
                                        ShareFileUtils.setInt(TheLConstants.live_permit, livePermitBean.data.perm);
                                        if (livePermitBean.data.perm == 1) {
                                            Intent intent = new Intent(TheLApp.getContext(), ReleaseLiveMomentActivity.class);
                                            appCompatActivity.startActivity(intent);
                                        } else {
                                            DialogUtil.showConfirmDialog(appCompatActivity, "", TheLApp.getContext().getString(R.string.no_live_permition_tip), TheLApp.getContext().getString(R.string.info_continue), TheLApp.getContext().getString(R.string.info_no), new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.dismiss();
                                                    Intent intent = new Intent(TheLApp.getContext(), ZhimaCertificationActivity.class);
                                                    appCompatActivity.startActivity(intent);
                                                }
                                            });
                                        }
                                    }
                                }
                            });
                        }
                    }
                }
            }
        });

        txt_live_info.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userInfoBean.await.awaitCount != 0) {
                    Intent intent = new Intent(mContext, AwaitLiveActivity.class);
                    intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, userInfoBean.id);
                    mContext.startActivity(intent);
                }
            }
        });
    }

    /**
     * 密友adapter
     **/
    class BffAdapter extends BaseRecyclerViewAdapter<MyCircleFriendBean> {

        public BffAdapter(List<MyCircleFriendBean> bffList) {
            super(R.layout.adapter_user_info_biff_item, bffList);
        }

        @Override
        protected void convert(BaseViewHolder helper, MyCircleFriendBean item) {
            helper.setCircleImageViewUrl(R.id.img_thumb, R.mipmap.icon_user, item.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE);
        }
    }

    class GuardAdapter extends BaseRecyclerViewAdapter<GuardBean.TopFans> {

        private int[] resId = {R.mipmap.one, R.mipmap.two, R.mipmap.three, R.mipmap.four, R.mipmap.five};
        private int[] bgResId = {R.drawable.one_color_circle, R.drawable.two_color_circle, R.drawable.three_color_circle, R.drawable.four_color_circle, R.drawable.five_color_circle};

        public GuardAdapter(List<GuardBean.TopFans> data) {
            super(R.layout.adapter_user_info_guard_item, data);
        }

        @Override
        protected void convert(BaseViewHolder helper, GuardBean.TopFans item) {
            if (item.isCloaking == 0) {
                ImageLoaderManager.imageLoaderCircle(helper.getView(R.id.img_thumb), item.avatar);
            } else {
                ImageLoaderManager.imageLoader(helper.getView(R.id.img_thumb), R.mipmap.liveroom_invisible);
            }
            ImageLoaderManager.imageLoader(helper.getView(R.id.number), resId[helper.getPosition()]);
//            Glide.with(mContext).load(item.avatar).transform(new GlideCircleTransform(mContext)).into((ImageView) helper.getView(R.id.img_thumb));
//            Glide.with(mContext).load(resId[helper.getPosition()]).into((ImageView) helper.getView(R.id.number));
            helper.getView(R.id.img_thumb_container).setBackgroundResource(bgResId[helper.getPosition()]);
        }
    }
}
