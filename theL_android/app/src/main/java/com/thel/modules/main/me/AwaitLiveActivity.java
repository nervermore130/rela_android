package com.thel.modules.main.me;

import android.content.Intent;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.bean.AwaitLiveBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.modules.main.me.adapter.FriendsListAdapter;
import com.thel.modules.main.me.bean.FriendsBean;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.utils.ViewUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class AwaitLiveActivity extends BaseActivity {

    @BindView(R.id.txt_title)
    TextView txt_title;

    @BindView(R.id.swipe_container)
    SwipeRefreshLayout swipe_container;

    @BindView(R.id.listView)
    RecyclerView mRecyclerView;

    @OnClick(R.id.lin_back)
    void back() {
        finish();
    }

    @BindView(R.id.lin_more)
    LinearLayout lin_more;

    public static final String TYPE_AWAIt_LIVE = "await_live";
    private int cursor = 0; // 将要请求的页号
    private int limit = 20;
    private boolean haveNextPage = true;
    // 刷新类型，全部刷新（即下拉刷新）为1，还是加载下一页为2
    public int refreshType = 0;
    public final int REFRESH_TYPE_ALL = 1;
    public final int REFRESH_TYPE_NEXT_PAGE = 2;
    private int currentCountForOnce = -1;
    private FriendsListAdapter listAdapter;
    private ArrayList<FriendsBean> listPlus = new ArrayList<>();
    private FriendsBean friendsBean;
    private String mUserId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_await_live);
        ButterKnife.bind(this);

        lin_more.setVisibility(View.GONE);
        mUserId = getIntent().getStringExtra(TheLConstants.BUNDLE_KEY_USER_ID);

        txt_title.setText(R.string.expect_live);

        initRecycler();
        processBusiness(cursor, REFRESH_TYPE_ALL);
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void initRecycler() {
        ViewUtils.initSwipeRefreshLayout(swipe_container);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setHasFixedSize(true);
        listAdapter = new FriendsListAdapter(listPlus, TYPE_AWAIt_LIVE,this);
        mRecyclerView.setAdapter(listAdapter);
        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshData();
            }
        });
        listAdapter.setOnLoadMoreListener(new BaseRecyclerViewAdapter.RequestLoadMoreListener() {//加载更多
            @Override
            public void onLoadMoreRequested() {
                mRecyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        if (haveNextPage) {
                            processBusiness(cursor, REFRESH_TYPE_NEXT_PAGE);
                        } else {
                            listAdapter.openLoadMore(0, false);
                            if (listPlus.size() > 0) {
                                View view = LayoutInflater.from(AwaitLiveActivity.this).inflate(R.layout.load_more_footer_layout, (ViewGroup) mRecyclerView.getParent(), false);
                                ((TextView) view.findViewById(R.id.text)).setText(getString(R.string.info_no_more));
                                listAdapter.addFooterView(view);
                            }
                        }
                    }
                });
            }
        });
        listAdapter.setReloadMoreListener(new BaseRecyclerViewAdapter.ReloadMoreListener() {
            @Override
            public void reloadMore() {
                listAdapter.removeAllFooterView();
                listAdapter.openLoadMore(true);
                processBusiness(cursor, REFRESH_TYPE_NEXT_PAGE);
            }
        });
        listAdapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                ViewUtils.preventViewMultipleClick(view, 2000);
                gotoUserInfoActivity(position);
            }
        });
    }

    private void processBusiness(int page ,int type) {
        cursor = page;
        refreshType = type;

        loadNetData(page);

    }

    private void loadNetData(int page) {
        Flowable<AwaitLiveBean> flowable = RequestBusiness.getInstance().getAwaitLiveList(mUserId, limit + "", page + "");
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<AwaitLiveBean>() {
            @Override
            public void onNext(AwaitLiveBean data) {
                super.onNext(data);
                if (data != null && data.data != null && data.data.users != null) {
                    cursor = data.data.cursor;
                    haveNextPage = data.data.haveNextPage;
                    ArrayList<FriendsBean> friendsBeans = new ArrayList<>();
                    for (AwaitLiveBean.Users userBean : data.data.users) {
                        FriendsBean friendsBean = new FriendsBean();
                        friendsBean.followStatus = userBean.followStatus;
                        friendsBean.affection = Integer.parseInt(userBean.affection);
                        friendsBean.age = userBean.age;
                        friendsBean.avatar = userBean.avatar;
                        friendsBean.distance = userBean.distance;
                        friendsBean.hiding = userBean.hiding;
                        friendsBean.intro = userBean.intro;
                        friendsBean.isFollow = userBean.isFollow;
                        friendsBean.level = userBean.level;
                        friendsBean.nickName = userBean.nickName;
                        friendsBean.online = String.valueOf(userBean.online);
                        friendsBean.roleName = userBean.roleName;
                        friendsBean.secretly = userBean.secretly;
                        friendsBean.userId = String.valueOf(userBean.userId);
                        friendsBean.verifyIntro = userBean.verifyIntro;
                        friendsBean.verifyType = userBean.verifyType;
                        friendsBeans.add(friendsBean);
                    }
                    setFollowAndFans(friendsBeans);
                }
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                requestFiled();
            }

            @Override
            public void onComplete() {
                super.onComplete();
                requestFinished();
            }
        });

    }

    private void requestFiled() {
        if (refreshType == REFRESH_TYPE_NEXT_PAGE) {

            mRecyclerView.post(new Runnable() {
                @Override
                public void run() {
                    listAdapter.loadMoreFailed((ViewGroup) mRecyclerView.getParent());
                }
            });
        }
    }

    private void requestFinished() {
        if (swipe_container != null)
            swipe_container.postDelayed(new Runnable() {

                @Override
                public void run() {
                    if (swipe_container != null && swipe_container.isRefreshing())
                        swipe_container.setRefreshing(false);
                }
            }, 1000);
    }

    private void refreshData() {
        if (swipe_container != null && !swipe_container.isRefreshing()) {
            swipe_container.post(new Runnable() {
                @Override
                public void run() {
                    swipe_container.setRefreshing(true);
                }
            });
        }
        haveNextPage = true;
        processBusiness(0, REFRESH_TYPE_ALL);
        mRecyclerView.scrollToPosition(0);

    }

    private void setFollowAndFans(ArrayList<FriendsBean> friendsListBean) {
        //刷新
        if (REFRESH_TYPE_ALL == refreshType) {
            listPlus.clear();
        }

        currentCountForOnce = friendsListBean.size();
        listPlus.addAll(friendsListBean);

        if (REFRESH_TYPE_ALL == refreshType) {
            listAdapter.removeAllFooterView();
            listAdapter.setNewData(listPlus);
            if (listPlus.size() > 0) {
                listAdapter.openLoadMore(listPlus.size(), true);
            } else {
                listAdapter.openLoadMore(listPlus.size(), false);
            }
        } else {
            listAdapter.notifyDataChangedAfterLoadMore(true, listPlus.size());

        }

        //刷新回到顶部
        if (REFRESH_TYPE_ALL == refreshType) {
            if (mRecyclerView.getChildCount() > 0) {
                mRecyclerView.scrollToPosition(0);
            }
        }
    }

    private void gotoUserInfoActivity(int position) {
        friendsBean = listAdapter.getData().get(position);
//        Intent intent = new Intent(this, UserInfoActivity.class);
//        Bundle bundle = new Bundle();
//        bundle.putString(TheLConstants.BUNDLE_KEY_USER_ID, friendsBean.userId);
//        bundle.putString("fromPage", this.getClass().getName());
//        intent.putExtras(bundle);
//        startActivityForResult(intent, TheLConstants.BUNDLE_CODE_FRIENDS_ACTIVITY);
        FlutterRouterConfig.Companion.gotoUserInfo(friendsBean.userId);
    }
}
