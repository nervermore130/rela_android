package com.thel.modules.main.me.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.RechargeRecord;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

public class RechargeRecordListAdapter extends BaseAdapter {

    private ArrayList<RechargeRecord> mRechargeRecords = new ArrayList<>();
    private LayoutInflater mInflater;

    public RechargeRecordListAdapter(ArrayList<RechargeRecord> rechargeRecords) {
        mInflater = (LayoutInflater) TheLApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mRechargeRecords = rechargeRecords;
    }

    @Override
    public int getCount() {
        return mRechargeRecords.size();
    }

    @Override
    public Object getItem(int position) {
        return mRechargeRecords.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void updateSingleRow(ListView listView, int position) {
        int firstVisiblePosition = listView.getFirstVisiblePosition();
        int lastVisiblePosition = listView.getLastVisiblePosition();
        if (position >= firstVisiblePosition && position <= lastVisiblePosition) {
            View view = listView.getChildAt(position - firstVisiblePosition);
            if (view.getTag() instanceof HoldView) {
                HoldView holdView = (HoldView) view.getTag();
                refreshItem(position - listView.getHeaderViewsCount(), holdView);
            }
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        HoldView holdView = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.recharge_record_listitem, parent, false);
            holdView = new HoldView();

            holdView.rel_header = convertView.findViewById(R.id.rel_header);
            holdView.txt_date = convertView.findViewById(R.id.txt_date);
            holdView.txt_desc = convertView.findViewById(R.id.txt_desc);
            holdView.txt_delta = convertView.findViewById(R.id.txt_delta);
            holdView.divider = convertView.findViewById(R.id.divider);
            holdView.last_divider = convertView.findViewById(R.id.last_divider);

            convertView.setTag(holdView); // 把holdview缓存下来
        } else {
            holdView = (HoldView) convertView.getTag();
        }

        refreshItem(position, holdView);
        return convertView;
    }

    private void refreshItem(int position, final HoldView holdView) {

        final RechargeRecord rechargeRecord = mRechargeRecords.get(position);

        if (rechargeRecord.showHeader) {
            holdView.txt_date.setText(getDate(rechargeRecord.createTime));
            holdView.rel_header.setVisibility(View.VISIBLE);
            holdView.divider.setVisibility(View.GONE);
        } else {
            holdView.rel_header.setVisibility(View.GONE);
            holdView.divider.setVisibility(View.VISIBLE);
        }

        if (position == mRechargeRecords.size() - 1) {
            holdView.last_divider.setVisibility(View.VISIBLE);
        } else {
            holdView.last_divider.setVisibility(View.GONE);
        }

        holdView.txt_desc.setText(getTime(rechargeRecord.createTime) + " " + rechargeRecord.description);
        holdView.txt_delta.setText((rechargeRecord.goldDelta >= 0 ? "+" : "") + rechargeRecord.goldDelta);

    }

    class HoldView {
        TextView txt_date;
        TextView txt_desc;
        TextView txt_delta;
        TextView divider;
        TextView last_divider;
        RelativeLayout rel_header;
    }


    private String getTime(String createTime) {
        final DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date d = null;
        try {
            d = df.parse(createTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        final DateFormat dfTime = new SimpleDateFormat("HH:mm");
        return dfTime.format(d);
    }

    private String getDate(String createTime) {
        final DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date d = null;
        try {
            d = df.parse(createTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        final DateFormat dfDate = new SimpleDateFormat("MM.dd.yyyy");
        return dfDate.format(d);
    }

}