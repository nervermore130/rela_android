package com.thel.modules.main.me.aboutMe;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.modules.main.me.adapter.RevenueDetailsAdapter;
import com.thel.modules.main.me.bean.IncomeRecord;
import com.thel.modules.main.me.bean.IncomeRecordListBean;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.utils.ViewUtils;

import org.reactivestreams.Subscription;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.thel.R.id.swipe_container;

/**
 * 收益或提现记录页面
 * Created by lingwei on 2017/10/19.
 */

public class RevenueDetailsActivity extends BaseActivity {

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.lin_back)
    LinearLayout linBack;
    @BindView(R.id.txt_title)
    TextView txtTitle;
    @BindView(R.id.img_more)
    ImageView imgMore;
    @BindView(R.id.lin_more)
    LinearLayout linMore;
    @BindView(R.id.title_layout)
    RelativeLayout titleLayout;
    @BindView(R.id.txt_default)
    TextView txtDefault;
    @BindView(R.id.listView)
    ListView mListView;
    @BindView(swipe_container)
    SwipeRefreshLayout swipeContainer;
    private String filter;// incoming  增加（收益），outgoing 减少(提现)
    public static final String FILTER_TYPE_INCOMING = "incoming";
    private int cursor = 0;
    private int limit = 50;
    private ArrayList<IncomeRecord> listPlus = new ArrayList<>();
    private RevenueDetailsAdapter adapter;
    public boolean canLoadMore = false;
    private final int REFRESH_TYPE_ALL = 1;
    private final int REFRESH_TYPE_NEXT_PAGE = 2;
    private int refreshType = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.simple_swiperefresh_listview_layout);
        ButterKnife.bind(this);
        getintent();
        processBusiness();
        setListener();

    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void getintent() {
        filter = getIntent().getStringExtra("filter");
        if (FILTER_TYPE_INCOMING.equals(filter))
            ((TextView) findViewById(R.id.txt_title)).setText(R.string.income_detail);
        else
            ((TextView) findViewById(R.id.txt_title)).setText(R.string.withdraw__record);

    }

    private void processBusiness() {
        if (null != swipeContainer) {
            ViewUtils.initSwipeRefreshLayout(swipeContainer);
            swipeContainer.post(new Runnable() {
                @Override
                public void run() {
                    swipeContainer.setRefreshing(true);
                }
            });
        }
        cursor = 0;
        loadNetData(REFRESH_TYPE_ALL);
    }

    private void setListener() {
        linBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                cursor = 0;
                loadNetData(REFRESH_TYPE_ALL);
            }
        });
        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                switch (scrollState) {
                    // 当不滚动时
                    case AbsListView.OnScrollListener.SCROLL_STATE_IDLE:
                        int totalSize = view.getCount();
                        // 判断滚动到底部
                        if (view.getLastVisiblePosition() + 1 == totalSize && canLoadMore && cursor != 0) {
                            showLoading();
                            canLoadMore = false;
                            // 列表滑动到底部加载下一组数据
                            loadNetData(REFRESH_TYPE_NEXT_PAGE);
                        }
                        break;
                }
            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {

            }

        });
    }

    private void loadNetData(int type) {
        refreshType = type;
        Flowable<IncomeRecordListBean> flowable = RequestBusiness.getInstance().getIncomeOrWithdrawRecord(cursor, limit, filter);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<IncomeRecordListBean>() {

            @Override
            public void onNext(IncomeRecordListBean incomeRecordListBean) {
                super.onNext(incomeRecordListBean);
                refreshUi(incomeRecordListBean);
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                requestFinished();
            }

            @Override
            public void onComplete() {
                requestFinished();
            }
        });
    }

    private void refreshUi(IncomeRecordListBean incomeRecordListBean) {
        if (incomeRecordListBean.data != null) {
            if (refreshType == REFRESH_TYPE_ALL) {
                listPlus.clear();
            }
            listPlus.addAll(incomeRecordListBean.data.list);
            if (listPlus.size() > 0)
                txtDefault.setVisibility(View.GONE);
            else
                txtDefault.setVisibility(View.VISIBLE);
            canLoadMore = true;
            cursor = incomeRecordListBean.data.cursor;
            closeLoading();

            // 遍历所有数据，判断哪些是需要显示头部一行的
            reviewData(listPlus);

            if (null == adapter) {
                adapter = new RevenueDetailsAdapter(listPlus);
                mListView.setAdapter(adapter);
            } else {
                adapter.notifyDataSetChanged();
            }
            requestFinished();
        }

    }

    private void requestFinished() {
        canLoadMore = true;
        if (swipeContainer != null)
            swipeContainer.postDelayed(new Runnable() {

                @Override
                public void run() {
                    if (swipeContainer != null && swipeContainer.isRefreshing())
                        swipeContainer.setRefreshing(false);
                }
            }, 1000);
    }

    private void reviewData(ArrayList<IncomeRecord> rechargeRecords) {
        String showedDate = "";
        for (IncomeRecord incomeRecord : rechargeRecords) {
            String data = parseDate(incomeRecord);
            if (!data.equals(showedDate)) {
                incomeRecord.showHeader = true;
                showedDate = data;
                incomeRecord.date = data;
            } else {
                incomeRecord.showHeader = false;
            }
        }
    }

    public String parseDate(IncomeRecord incomeRecord) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        final Date d;
        try {
            d = df.parse(incomeRecord.createTime);
            DateFormat dfDate = new SimpleDateFormat("MM.dd.yyyy");
            String date = dfDate.format(d);
            return date;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }


}
