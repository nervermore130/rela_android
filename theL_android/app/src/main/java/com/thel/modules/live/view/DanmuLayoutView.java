package com.thel.modules.live.view;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;

import com.thel.modules.live.bean.DanmuBean;
import com.thel.ui.widget.DanmuView;
import com.thel.utils.AppInit;
import com.thel.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by the L on 2016/9/8.
 */
public class DanmuLayoutView extends ViewGroup {
    private final Context mContext;
    private int mWidth = 0;//宽度
    private int mSpead = 1;//弹出速度
    private int mDivideWidth = 0;//弹幕消息之间的宽度间隔
    private int mDivideHeight = 0;//上下弹幕消息之间的高度间隔
    private int mItemViewHeight = 0;
    private List<DanmuBean> list = new ArrayList<>();
    private int mDuration;//弹幕从出现到完全消失时间间隔
    private Handler mHander;
    private boolean[] callPos;//能够显示的位置，false 为这个位置不能显示，true为能显示，数组大小为能显示的行数
    private OnItemClickListener onItemClickListener;//弹幕view点击监听


    public DanmuLayoutView(Context context) {
        this(context, null);
    }

    public DanmuLayoutView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DanmuLayoutView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        for (int i = 0; i < getChildCount(); i++) {
            View v = getChildAt(i);
            v.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    private void init() {
        mDivideWidth = Utils.dip2px(mContext, 50);//弹幕左右间隔
        mDivideHeight = Utils.dip2px(mContext, 20);//弹幕上下间隔
        mDuration = 4000;//(现在改为从出现到屏幕左边的时间间隔)弹幕从出现到完全消失时间间隔
        mSpead = AppInit.displayMetrics.widthPixels * 1000 / mDuration + 1;//速度按秒计算+1防止为0
        mItemViewHeight = Utils.dip2px(mContext, 61);//itemview高度
        callPos = new boolean[]{true, true};
        mHander = new Handler();
    }

    public void receiveMsg(DanmuBean danmuBean) {
        list.add(danmuBean);
        callShow();
    }

    public void showDanmu(final DanmuBean danmuBean, int itemPos) {
        final DanmuView view = new DanmuView(mContext);
        view.initView(danmuBean, itemPos);
        list.remove(danmuBean);
        addView(view);
        callPos[view.itemPos] = false;
        view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    onItemClickListener.onClick(v, danmuBean);
                }
            }
        });
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        mWidth = r - l;
        final int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            final DanmuView view = (DanmuView) getChildAt(i);
            if (!view.isAdd) {
                view.isAdd = true;
                layout(view, mWidth, view.itemPos * (mItemViewHeight + mDivideHeight));//显示的位置
                setAnimation(view);
            }
        }
    }


    private void layout(View view, int left, int top) {
        view.layout(left, top, left + view.getMeasuredWidth(), top + view.getMeasuredHeight());
    }

    /**
     * 弹幕动画
     *
     * @param view
     */
    private void setAnimation(final DanmuView view) {
        final int viewWidth = view.getMeasuredWidth();
        final int transX = viewWidth + mWidth + 20;//要平移的距离
        final int duration = transX * 1000 / mSpead;//总计移动所需要的时间
        ObjectAnimator oa = ObjectAnimator.ofFloat(view, "translationX", -transX);//向左移动直到出屏幕，多20像素的误差容错
        oa.setDuration(duration);
        oa.setInterpolator(new LinearInterpolator());
        oa.start();
        oa.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

                view.setLayerType(LAYER_TYPE_HARDWARE, null);

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                view.setLayerType(LAYER_TYPE_NONE, null);
                removeView(view);//动画结束后移除
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        final int divideTime = (viewWidth + mDivideWidth) * duration / transX;
        mHander.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (view != null) {
                    callPos[view.itemPos] = true;
                } else {
                    callPos[0] = true;
                }
                callShow();
            }
        }, divideTime);
    }

    public void callShow() {//请求显示
        final int listSize = list.size();
        final int posSize = callPos.length;
        for (int i = 0; i < posSize; i++) {
            if (callPos[i] && listSize > 0) {//如果某个位置可以显示,并且listSize>0
                showDanmu(list.get(0), i);
                return;
            }
        }
    }


    //item点击监听接口
    public interface OnItemClickListener {
        void onClick(View v, DanmuBean danmuBean);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public void clearAnimation() {
        list.clear();
        super.clearAnimation();
    }
}
