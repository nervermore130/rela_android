package com.thel.modules.main.me.aboutMe;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.bean.ContactBean;
import com.thel.constants.TheLConstants;
import com.thel.modules.main.me.bean.VipBean;
import com.thel.modules.main.messages.ChatActivity;
import com.thel.utils.ImageUtils;

/**
 * 赠送成功页面
 * Created by lingwei on 2017/10/27.
 */

public class GiftSendSuccessActivity extends BaseActivity {
    private ContactBean friend;//朋友信息
    private VipBean vipbean;//vip信息
    private int pos;//vip所在的theconstant vip_tpye位置，从上页List中得到，可能会变
    private TextView txt_receive_result;//赠送结果
    private SimpleDraweeView img_head;//头像
    private SimpleDraweeView img_vip;//vip标志
    private TextView txt_friendname;//朋友用户名
    private TextView txt_chat;
    private TextView txt_back;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gift_send_success_activity);
        geIntentData();
        findViewById();
        initUi();
        setListener();
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void setListener() {
        txt_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        txt_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoChat();
            }
        });
    }

    private void gotoChat() {
        finish();
        Intent intent = new Intent(GiftSendSuccessActivity.this, ChatActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("toUserId", friend.userId);
        bundle.putString("toName", friend.nickName);
        bundle.putString("toAvatar", friend.avatar);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void findViewById() {
        txt_receive_result = findViewById(R.id.txt_receive_result);
        img_head = findViewById(R.id.img_head);
        img_vip = findViewById(R.id.img_vip);
        txt_friendname = findViewById(R.id.txt_username);
        txt_chat = findViewById(R.id.txt_chat);
        txt_back = findViewById(R.id.txt_back);
    }

    private void initUi() {
        if (pos != -1) {
            txt_receive_result.setText(String.format(getString(R.string.friend_received_vip), friend.nickName));//暂时不知道会员类型如何显示
        }
        img_head.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(friend.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE)));
        img_vip.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(vipbean.icon, TheLConstants.AVATAR_MIDDLE_SIZE, TheLConstants.AVATAR_MIDDLE_SIZE))).build()).setAutoPlayAnimations(true).build());
        txt_friendname.setText(friend.nickName);
    }

    /**
     * 获取传递的数据
     */
    private void geIntentData() {
        Bundle bd = getIntent().getExtras();
        if (bd != null) {
            friend = (ContactBean) bd.getSerializable(TheLConstants.BUNDLE_KEY_FRIEND);
            vipbean = (VipBean) bd.getSerializable(TheLConstants.BUNDLE_KEY_VIP);
            pos = bd.getInt("pos", -1);
        }
    }
}
