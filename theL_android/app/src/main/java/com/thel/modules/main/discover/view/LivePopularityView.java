package com.thel.modules.main.discover.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.constants.TheLConstants;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.live.bean.LivePopularityDataBean;

import java.util.List;

/**
 * 直播人气主播榜轮播
 * Created by lingwei on 2017/11/17.
 */

public class LivePopularityView extends RelativeLayout {

    private Context contexts;
    private ImageView img_user_3;
    private ImageView img_user_2;
    private ImageView img_user_1;
    private View rel_1;
    private View rel_2;
    private View rel_3;
    private List<LivePopularityDataBean.AnchorTodayBean> UserList;
    private TextView txt_title;

    public LivePopularityView(Context context) {
        this(context, null);
    }

    public LivePopularityView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LivePopularityView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.contexts = context;
        init();
    }

    private void init() {
        inflate(contexts, R.layout.liveuser_list_view, this);
        rel_1 = findViewById(R.id.rel_1);
        img_user_1 = findViewById(R.id.img_user_1);
        rel_2 = findViewById(R.id.rel_2);
        img_user_2 = findViewById(R.id.img_user_2);
        rel_3 = findViewById(R.id.rel_3);
        img_user_3 = findViewById(R.id.img_user_3);
        txt_title = findViewById(R.id.txt_title);
    }

    public LivePopularityView initView(List<LivePopularityDataBean.AnchorTodayBean> userList, String title) {
        this.UserList = userList;
        if (userList == null) {
            return this;
        }
        int length = userList.size();
        for (int i = 0; i < length; i++) {
            LivePopularityDataBean.AnchorTodayBean anchorTodayBean = userList.get(i);
            if (i == 0) {
                rel_1.setVisibility(View.VISIBLE);
                if (anchorTodayBean.isCloaking == 1) {
                    ImageLoaderManager.imageLoader(img_user_1, R.mipmap.liveroom_invisible);
                } else {
                    ImageLoaderManager.imageLoaderDefaultCircle(img_user_1, R.mipmap.icon_user, UserList.get(0).avatar, TheLConstants.AVATAR_SMALL_SIZE, TheLConstants.AVATAR_SMALL_SIZE);
                }
            } else if (i == 1) {
                rel_2.setVisibility(View.VISIBLE);
                if (anchorTodayBean.isCloaking == 1) {
                    ImageLoaderManager.imageLoader(img_user_2, R.mipmap.liveroom_invisible);
                } else {
                    ImageLoaderManager.imageLoaderDefaultCircle(img_user_2, R.mipmap.icon_user, UserList.get(1).avatar, TheLConstants.AVATAR_SMALL_SIZE, TheLConstants.AVATAR_SMALL_SIZE);
                }

            } else if (i == 2) {
                rel_3.setVisibility(View.VISIBLE);
                if (anchorTodayBean.isCloaking == 1) {
                    ImageLoaderManager.imageLoader(img_user_3, R.mipmap.liveroom_invisible);
                } else {
                    ImageLoaderManager.imageLoaderDefaultCircle(img_user_3, R.mipmap.icon_user, UserList.get(2).avatar, TheLConstants.AVATAR_SMALL_SIZE, TheLConstants.AVATAR_SMALL_SIZE);
                }
            }
        }
        txt_title.setText(title);

        return this;
    }
}
