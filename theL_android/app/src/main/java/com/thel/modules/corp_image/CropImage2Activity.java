package com.thel.modules.corp_image;

import android.app.Activity;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.WindowManager;

import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.constants.TheLConstants;
import com.thel.ui.imageviewer.cropiwa.AspectRatio;
import com.thel.ui.imageviewer.cropiwa.CropIwaView;
import com.thel.ui.imageviewer.cropiwa.config.CropIwaSaveConfig;
import com.thel.ui.imageviewer.cropiwa.shape.CropIwaOvalShape;
import com.thel.ui.imageviewer.cropiwa.shape.CropIwaRectShape;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 通过CropIwaResultReceiver拿到剪裁返回结果
 */
public class CropImage2Activity extends BaseActivity {

    @BindView(R.id.crop_view)
    CropIwaView cropView;

    @OnClick(R.id.cancel)
    void cancel() {
        finish();
    }

    @OnClick(R.id.sure)
    void sure() {

        cropView.crop(new CropIwaSaveConfig.Builder(Uri.fromFile(new File(
                getApplication().getFilesDir(),
                System.currentTimeMillis() + ".png")))
                .setQuality(100)
                .setCompressFormat(Bitmap.CompressFormat.PNG).build());
        setResult(Activity.RESULT_OK, getIntent());
        finish();
    }

    private boolean isAvatar;

    private boolean isCover;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_crop_image2);
        ButterKnife.bind(this);

        isAvatar = getIntent().getBooleanExtra("isAvatar", false);

        isCover = getIntent().getBooleanExtra("isCover", false);

        Uri uri = Uri.fromFile(new File(getIntent().getStringExtra(TheLConstants.BUNDLE_KEY_LOCAL_IMAGE_PATH)));
        cropView.setImageUri(uri);

        if (isAvatar) {
            cropView.configureOverlay()
                    .setAspectRatio(new AspectRatio(1, 1))
                    .setCropShape(new CropIwaOvalShape(cropView.configureOverlay()))
                    .apply();
        }

        if (isCover) {
            cropView.configureOverlay()
                    .setAspectRatio(new AspectRatio(9, 16))
                    .setCropShape(new CropIwaRectShape(cropView.configureOverlay()))
                    .apply();
        }
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }
}
