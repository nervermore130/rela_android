package com.thel.modules.live.liveBigGiftAnimLayout.anim;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.thel.R;
import com.thel.modules.live.liveBigGiftAnimLayout.LiveBigAnimUtils;
import com.thel.utils.Utils;

import java.util.Random;

/**
 * 抱抱的动画
 * created by Setsail on 2016.9.8
 */
public class HugHugAnimator {
    private final String[] hugRes;
    private final String foldPath;
    private Handler mHandler;
    private int duration;
    private int minDuration = 2000;
    private Random mRandom;
    private Context mContext;
    private int maxSize;
    private int minSize;
    private int rate = 400;
    private int bearAnimDuration = 800;
    private boolean animEnded = false;

    public HugHugAnimator(Context context, int d) {
        mContext = context;
        maxSize = Utils.dip2px(context, 40);
        minSize = Utils.dip2px(context, 20);
        duration = d;
        mHandler = new Handler(Looper.getMainLooper());
        mRandom = new Random();
        foldPath = "anim/bearhug";
        hugRes = new String[]{"live_gift_bearhug_01",
                "live_gift_bearhug_02",
                "live_gift_bearhug_03",
                "live_gift_bearhug_04",
                "live_gift_bearhug_05",
                "live_gift_bearhug_06",
                "live_gift_bearhug_07"};
    }

    public int start(final ViewGroup parent) {
        animEnded = false;
        final RelativeLayout backgound = new RelativeLayout(mContext);
        parent.addView(backgound, RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        setBackground(backgound);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                backgound.clearAnimation();
                parent.removeView(backgound);
            }
        }, duration);
        return duration;
    }

    private void setBackground(RelativeLayout parent) {
        final RelativeLayout background = showBear(parent);
        parent.post(new Runnable() {
            @Override
            public void run() {
                dropOneHeart(background);
                dropOneHeart(background);
                dropOneHeart(background);
                dropOneHeart(background);
                if (!animEnded) {
                    background.postDelayed(this, rate);
                }
            }
        });
    }

    private void dropOneHeart(final ViewGroup parent) {

        if (animEnded)
            return;

        int parentWidth = parent.getWidth();
        int parentHeight = parent.getHeight();
        if (parentWidth <= 0 || parentHeight <= 0)
            return;

        Path p = new Path();
        int w;
        int h = parentHeight;
        int dur = mRandom.nextInt(duration - minDuration) + minDuration;
        int size = mRandom.nextInt(maxSize - minSize) + minSize;
        w = mRandom.nextInt(parentWidth);
        p.moveTo(w, 0 - 2 * size);
        p.lineTo(w, h);

        final ImageView child = new ImageView(mContext);
        child.setLayoutParams(new RelativeLayout.LayoutParams(size, size));
        switch (mRandom.nextInt(3)) {
            case 0:
                // child.setImageResource(R.drawable.live_gift_bearhug_05);
                LiveBigAnimUtils.setAssetImage(child, foldPath, hugRes[4]);
                break;
            case 1:
                //  child.setImageResource(R.drawable.live_gift_bearhug_06);
                LiveBigAnimUtils.setAssetImage(child, foldPath, hugRes[5]);
                break;
            case 2:
                //  child.setImageResource(R.drawable.live_gift_bearhug_07);
                LiveBigAnimUtils.setAssetImage(child, foldPath, hugRes[6]);
                break;
            default:
                //   child.setImageResource(R.drawable.live_gift_bearhug_05);
                LiveBigAnimUtils.setAssetImage(child, foldPath, hugRes[4]);
                break;
        }

        parent.addView(child);

        FloatAnimation anim = new FloatAnimation(p, parent, child);

        anim.setDuration(dur);
        anim.setInterpolator(new LinearInterpolator());
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation animation) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        parent.removeView(child);
                    }
                });
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }

            @Override
            public void onAnimationStart(Animation animation) {
            }
        });
        child.startAnimation(anim);
    }

    private RelativeLayout showBear(final ViewGroup parent) {
        final RelativeLayout background = new RelativeLayout(mContext);
        background.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        parent.addView(background);

        final RelativeLayout rel_bear = (RelativeLayout) RelativeLayout.inflate(mContext, R.layout.live_big_anim_hughug_layout, null);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams((int) mContext.getResources().getDimension(R.dimen.hug_gift_width), (int) mContext.getResources().getDimension(R.dimen.hug_gift_height));
        lp.addRule(RelativeLayout.CENTER_IN_PARENT);
        rel_bear.setLayoutParams(lp);
        parent.addView(rel_bear);
        initBackground(rel_bear);

        final ImageView eyes = rel_bear.findViewById(R.id.img_eyes);
        startEyesAnim(eyes);

        final ImageView leftHand = rel_bear.findViewById(R.id.img_left_hand);
        startHandAnim(leftHand, 0);
        final ImageView rightHand = rel_bear.findViewById(R.id.img_right_hand);
        startHandAnim(rightHand, 1);

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                animEnded = true;
                eyes.clearAnimation();
                leftHand.clearAnimation();
                rightHand.clearAnimation();
                parent.removeView(rel_bear);
                parent.removeView(background);
            }
        }, duration);

        return background;
    }

    private void initBackground(RelativeLayout rel_bear) {
        final ImageView img_body = rel_bear.findViewById(R.id.img_body);
        final ImageView img_eyes = rel_bear.findViewById(R.id.img_eyes);
        final ImageView img_left_hand = rel_bear.findViewById(R.id.img_left_hand);
        final ImageView img_right_hand = rel_bear.findViewById(R.id.img_right_hand);

        LiveBigAnimUtils.setAssetImage(img_body, foldPath, hugRes[0]);
        LiveBigAnimUtils.setAssetImage(img_eyes, foldPath, hugRes[3]);
        LiveBigAnimUtils.setAssetImage(img_left_hand, foldPath, hugRes[1]);
        LiveBigAnimUtils.setAssetImage(img_right_hand, foldPath, hugRes[2]);
    }

    private void startHandAnim(final ImageView hand, final int type) {
        final RotateAnimation rotateAnimation = new RotateAnimation(0, type == 0 ? 50 : -50, Animation.RELATIVE_TO_SELF, type == 0 ? 1f : 0f, Animation.RELATIVE_TO_SELF, 0.95f);
        rotateAnimation.setDuration(bearAnimDuration);
        rotateAnimation.setInterpolator(new LinearInterpolator());
        rotateAnimation.setFillAfter(true);
        rotateAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                final RotateAnimation rotateAnimation1 = new RotateAnimation(type == 0 ? 50 : -50, 0, Animation.RELATIVE_TO_SELF, type == 0 ? 1f : 0f, Animation.RELATIVE_TO_SELF, 0.95f);
                rotateAnimation1.setDuration(bearAnimDuration);
                rotateAnimation1.setInterpolator(new LinearInterpolator());
                rotateAnimation1.setFillAfter(true);
                rotateAnimation1.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        hand.startAnimation(rotateAnimation);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                hand.startAnimation(rotateAnimation1);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        hand.startAnimation(rotateAnimation);
    }

    private void startEyesAnim(final ImageView eyes) {

        final ScaleAnimation scaleAnimationUp = new ScaleAnimation(1f, 1.3f, 1f, 1.3f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        scaleAnimationUp.setDuration(bearAnimDuration);
        scaleAnimationUp.setInterpolator(new LinearInterpolator());
        scaleAnimationUp.setFillAfter(true);
        scaleAnimationUp.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                final ScaleAnimation scaleAnimation = new ScaleAnimation(1.3f, 1f, 1.3f, 1f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                scaleAnimation.setDuration(bearAnimDuration);
                scaleAnimation.setInterpolator(new LinearInterpolator());
                scaleAnimationUp.setFillAfter(true);
                scaleAnimation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        eyes.startAnimation(scaleAnimationUp);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                eyes.startAnimation(scaleAnimation);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        eyes.startAnimation(scaleAnimationUp);
    }

    static class FloatAnimation extends Animation {
        private PathMeasure mPm;
        private View mView;
        private float mDistance;

        public FloatAnimation(Path path, View parent, View child) {
            mPm = new PathMeasure(path, false);
            mDistance = mPm.getLength();
            mView = child;
            parent.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        }

        @Override
        protected void applyTransformation(float factor, Transformation transformation) {
            Matrix matrix = transformation.getMatrix();
            mPm.getMatrix(mDistance * factor, matrix, PathMeasure.POSITION_MATRIX_FLAG);
            mView.setRotation(1);
            mView.setScaleX(1);
            mView.setScaleY(1);
            transformation.setAlpha(1);
        }
    }

}

