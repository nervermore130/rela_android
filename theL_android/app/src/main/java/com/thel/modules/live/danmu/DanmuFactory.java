package com.thel.modules.live.danmu;

import com.thel.modules.live.bean.DanmuBean;
import com.thel.ui.widget.DanmuSelectedLayout;
import com.thel.utils.L;

public class DanmuFactory {

    private DanmuFactory() {

    }

    public static CreateDanmu createDanmuView(DanmuBean danmuBean) {

        if (danmuBean != null && danmuBean.barrageId != null) {

            if (danmuBean.barrageId.equals(DanmuSelectedLayout.TYPE_DANMU_LEVEL)) {
                return new LevelDanmuView();
            } else {
                return new SpecialDanmuView();
            }
        }
        return null;
    }

}
