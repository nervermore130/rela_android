package com.thel.modules.main.me.adapter;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.constants.TheLConstants;
import com.thel.modules.live.bean.SoftGold;
import com.thel.utils.ImageUtils;

import java.util.List;

/**
 * Created by the L on 2016/5/22.
 */
public class SoftMoneyAdapter extends BaseAdapter {
    private final LayoutInflater mInflater;
    private List<SoftGold> goldList;

    private String channel = "";

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public SoftMoneyAdapter(List<SoftGold> goldList) {
        mInflater = (LayoutInflater) TheLApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        refreshAdapter(goldList);
    }

    public void refreshAdapter(List<SoftGold> goldList) {
        this.goldList = goldList;
    }

    @Override
    public int getCount() {
        return goldList.size();
    }

    @Override
    public Object getItem(int position) {
        return goldList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convetView, ViewGroup parent) {
        HoldView holdview = null;
        if (convetView == null) {
            convetView = mInflater.inflate(R.layout.soft_money_item, parent, false);
            holdview = new HoldView();
            holdview.img = convetView.findViewById(R.id.soft_gold_img);
            holdview.txt_desc = convetView.findViewById(R.id.txt_desc);
            holdview.txt_summary = convetView.findViewById(R.id.txt_summary);
            holdview.txt_price = convetView.findViewById(R.id.txt_price);
            holdview.pb_pending = convetView.findViewById(R.id.pb_pending);
            holdview.first_ChargeDesc = convetView.findViewById(R.id.first_ChargeDesc);
            holdview.bottom = convetView.findViewById(R.id.bottom);
            holdview.vivo_new_desc = convetView.findViewById(R.id.vivo_new_desc);
            holdview.parent = convetView.findViewById(R.id.parent);
            convetView.setTag(holdview);
        } else {
            holdview = (HoldView) convetView.getTag();
        }
        SoftGold gold = (SoftGold) getItem(position);

        if (gold.id == 24 && !TextUtils.isEmpty(channel) && channel.equals("vivo")) {
            holdview.bottom.setBackgroundResource(R.drawable.bg_border_bottom_red_shape);
            holdview.txt_desc.setTextColor(Color.parseColor("#FF7D98"));
            holdview.vivo_new_desc.setVisibility(View.VISIBLE);
            holdview.parent.setBackgroundResource(R.drawable.bg_border_shape_red_s);
        } else {
            holdview.bottom.setBackgroundResource(R.drawable.bg_border_bottom_green_shape);
            holdview.txt_desc.setTextColor(Color.parseColor("#4BBABC"));
            holdview.vivo_new_desc.setVisibility(View.GONE);
            holdview.parent.setBackgroundResource(R.drawable.bg_border_shape_green_s);
        }


        SpannableString ss = new SpannableString("x" + gold.gold);
        ss.setSpan(new RelativeSizeSpan(0.7f), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        holdview.txt_desc.setText(ss);
        if (TextUtils.isEmpty(gold.summary))
            holdview.txt_summary.setVisibility(View.GONE);
        else {
            holdview.txt_summary.setVisibility(View.VISIBLE);
            holdview.txt_summary.setText(gold.summary);
        }
        if (gold.isPendingPrice) {
            holdview.pb_pending.setVisibility(View.VISIBLE);
            holdview.txt_price.setVisibility(View.GONE);
        } else {
            holdview.pb_pending.setVisibility(View.GONE);
            holdview.txt_price.setVisibility(View.VISIBLE);
        }
        if (TextUtils.isEmpty(gold.firstChargeDesc)) {
            holdview.first_ChargeDesc.setVisibility(View.GONE);
        } else {
            holdview.first_ChargeDesc.setVisibility(View.VISIBLE);
            holdview.first_ChargeDesc.setText(gold.firstChargeDesc);
        }
        holdview.txt_price.setText(gold.generatePriceShow());
        holdview.img.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(gold.icon, TheLConstants.ICON_MIDDLE_SIZE, TheLConstants.ICON_MIDDLE_SIZE))).build()).setAutoPlayAnimations(true).build());

        return convetView;
    }

    class HoldView {
        SimpleDraweeView img;//图片
        TextView txt_desc;//描述
        TextView txt_summary;//描述第二行
        TextView txt_price;//价格
        ProgressBar pb_pending;// 加载价格中
        TextView first_ChargeDesc;//首充
        RelativeLayout bottom;
        TextView vivo_new_desc;
        RelativeLayout parent;
    }
}
