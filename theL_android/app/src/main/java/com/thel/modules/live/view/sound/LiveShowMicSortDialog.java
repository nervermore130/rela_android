package com.thel.modules.live.view.sound;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.base.BaseDialogFragment;
import com.thel.bean.live.LiveMultiSeatBean;
import com.thel.manager.ImageLoaderManager;
import com.thel.utils.AppInit;
import com.thel.utils.SizeUtils;

import java.util.Collection;
import java.util.List;

import video.com.relavideolibrary.BaseRecyclerAdapter;
import video.com.relavideolibrary.BaseViewHolder;

/**
 * Created by chad
 * Time 18/5/31
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */
public class LiveShowMicSortDialog extends BaseDialogFragment implements View.OnClickListener {

    private ImageView mic_open;
    private LinearLayout mic_list_empty;
    private RecyclerView mic_recycler;
    private TextView mic_sort_tip2;
    private TextView mic_sort_tip1;
    private MicSortListener micSortListener;
    private SortAdapter sortAdapter;
    private boolean full = false;//座位是否坐满了
    private boolean micSorting;

    public static LiveShowMicSortDialog newInstance(Bundle bundle) {
        LiveShowMicSortDialog instance = new LiveShowMicSortDialog();
        instance.setArguments(bundle);
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            micSorting = getArguments().getBoolean("micSorting");
            full = getArguments().getBoolean("full");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.live_show_mic_sort_view, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mic_open = view.findViewById(R.id.mic_open);
        mic_list_empty = view.findViewById(R.id.mic_list_empty);
        mic_recycler = view.findViewById(R.id.mic_recycler);
        mic_sort_tip2 = view.findViewById(R.id.mic_sort_tip2);
        mic_sort_tip1 = view.findViewById(R.id.mic_sort_tip1);
        mic_open.setOnClickListener(this);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mic_recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        mic_recycler.setHasFixedSize(false);
        sortAdapter = new SortAdapter(R.layout.sort_list_item, mic_recycler, null);
        mic_recycler.setAdapter(sortAdapter);

        if (micSorting) {
            mic_open.setTag("open");
            mic_open.setImageResource(R.mipmap.btn_check_right);
            mic_list_empty.setVisibility(View.VISIBLE);
            mic_recycler.setVisibility(View.VISIBLE);
            mic_sort_tip2.setVisibility(View.INVISIBLE);
            setSortMicCountText(0);
        }

        if (micSorting && micSortListener != null) micSortListener.onCreate();
    }

    @Override
    public void onResume() {
        super.onResume();
        int width = AppInit.displayMetrics.widthPixels;
        int height = SizeUtils.dip2px(getContext(), 300);
        getDialog().getWindow().setLayout(width, height);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mic_open:
                String tag = (String) mic_open.getTag();
                if (tag.equals("close")) {
                    mic_open.setTag("open");
                    mic_open.setImageResource(R.mipmap.btn_check_right);
                    mic_list_empty.setVisibility(View.VISIBLE);
                    mic_recycler.setVisibility(View.VISIBLE);
                    mic_sort_tip2.setVisibility(View.INVISIBLE);
                    setSortMicCountText(0);
                    //开启排麦
                    if (micSortListener != null) micSortListener.openMicSort();
                } else {
                    mic_open.setTag("close");
                    mic_open.setImageResource(R.mipmap.btn_check_left);
                    mic_list_empty.setVisibility(View.INVISIBLE);
                    mic_recycler.setVisibility(View.INVISIBLE);
                    mic_sort_tip2.setVisibility(View.VISIBLE);
                    mic_sort_tip1.setText(R.string.start_mic_sort_tips);
                    if (micSortListener != null) micSortListener.clostMicSort();
                }
                break;
        }
    }

    private void setSortMicCountText(int count) {
        mic_sort_tip1.setText(Html.fromHtml(getString(R.string.mic_sort_count, String.valueOf(count))));
    }

    public void agreeEnable(boolean full) {
        this.full = full;
        sortAdapter.notifyDataSetChanged();
    }

    public void addUser(LiveMultiSeatBean.CoupleDetail liveMultiSeatBean) {
        sortAdapter.addItem(liveMultiSeatBean);
        setSortMicCountText(sortAdapter.getData().size());
        mic_list_empty.setVisibility(View.INVISIBLE);
    }

    public void removeByUserId(String userId) {
        int count = sortAdapter.getData().size();
        for (int i = 0; i < count; i++) {
            if (sortAdapter.getData().get(i).userId.equals(userId)) {
                sortAdapter.removeData(i);
                break;
            }
        }
        if (sortAdapter.getData().size() == 0) {
            mic_list_empty.setVisibility(View.VISIBLE);
        }
        setSortMicCountText(sortAdapter.getData().size());
    }

    public void clearUser() {
        if (sortAdapter != null && sortAdapter.getData() != null) {
            sortAdapter.getData().clear();
            sortAdapter.notifyDataSetChanged();
        }
    }

    public void setData(List<LiveMultiSeatBean.CoupleDetail> data) {
        if (data != null && data.size() > 0) {
            sortAdapter.setData(data);
            mic_list_empty.setVisibility(View.INVISIBLE);
            setSortMicCountText(data.size());
        }
    }

    public List<LiveMultiSeatBean.CoupleDetail> getData() {
        return sortAdapter.getData();
    }

    private class SortAdapter extends BaseRecyclerAdapter<LiveMultiSeatBean.CoupleDetail> {

        public SortAdapter(int layoutId, RecyclerView recyclerView, Collection<LiveMultiSeatBean.CoupleDetail> list) {
            super(layoutId, recyclerView, list);
        }

        @Override
        public void dataBinding(final BaseViewHolder baseViewHolder, final LiveMultiSeatBean.CoupleDetail liveMultiSeatBean, final int i) {
            ImageView imageView = baseViewHolder.getView(R.id.avatar);
            ImageLoaderManager.imageLoaderCircle(imageView, R.mipmap.btn_avatar, liveMultiSeatBean.avatar);
            baseViewHolder.setText(R.id.nickname, liveMultiSeatBean.nickname);
            final Button button = baseViewHolder.getView(R.id.agree);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (micSortListener != null) {
                        micSortListener.agreeOnSeat(liveMultiSeatBean.userId);
                        mData.remove(i);
                        notifyDataSetChanged();
                        button.setEnabled(false);
                    }
                }
            });
            if (full) {
                button.setEnabled(false);
                button.setText(R.string.full);
                button.setTextColor(ContextCompat.getColor(mContext, R.color.gray));
                button.setBackgroundResource(R.drawable.bg_border_corner_button_gray);
            } else {
                button.setEnabled(true);
                button.setText(R.string.agree);
                button.setTextColor(ContextCompat.getColor(mContext, R.color.rela_color));
                button.setBackgroundResource(R.drawable.bg_border_corner_button_blue);
            }
        }
    }

    public void setMicSortListener(MicSortListener micSortListener) {
        this.micSortListener = micSortListener;
    }

    public interface MicSortListener {
        void onCreate();

        void openMicSort();

        void clostMicSort();

        void agreeOnSeat(String userId);

    }
}
