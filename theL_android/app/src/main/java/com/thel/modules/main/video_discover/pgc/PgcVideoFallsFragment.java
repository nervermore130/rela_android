package com.thel.modules.main.video_discover.pgc;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.AdBean;
import com.thel.bean.ReleaseMomentListBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.video.VideoBean;
import com.thel.constants.TheLConstants;
import com.thel.imp.AutoRefreshImp;
import com.thel.imp.BaseFunctionFragment;
import com.thel.imp.TittleClickListener;
import com.thel.modules.main.video_discover.autoplay.AutoPlayVideoCtrl;
import com.thel.modules.main.video_discover.autoplay.RecyclerViewStateImp;
import com.thel.modules.main.video_discover.autoplay.VideoSeenUtils;
import com.thel.modules.main.video_discover.videofalls.RecyclerViewStateBean;
import com.thel.modules.main.video_discover.videofalls.VideoFallsUtils;
import com.thel.modules.main.video_discover.videofalls.VideoMomentListBean;
import com.thel.modules.video.VideoListActivity;
import com.thel.player.video.VideoPlayerProxy2;
import com.thel.ui.widget.BannerLayout;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.utils.AppInit;
import com.thel.utils.BusinessUtils;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.GsonUtils;
import com.thel.utils.PhoneUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by waiarl on 2018/3/15.
 */

public class PgcVideoFallsFragment extends BaseFunctionFragment implements PgcVideoFallsContact.View, TittleClickListener, AutoRefreshImp {

    private RecyclerView recyclerView;
    private final int SPAN_COUNT = 2;
    private GridLayoutManager manager;
    private PgcVideoFallsAdapter adapter;
    private List<VideoBean> list = new ArrayList<>();
    private PgcVideoFallsContact.Presenter presenter;
    private int cursor = 0;
    private boolean haveNextPage = true;
    private SwipeRefreshLayout swipe_container;
    private View header;
    private BannerLayout banner;
    private List<AdBean.Map_list> adList = new ArrayList<>();
    private boolean isFirstCreate = true;
    private volatile RecyclerViewStateBean recyclerViewStateBean;
    private VideoMomentReleaseReceiver videoMomentReleaseReceiver;
    private List<VideoBean> VideoSendList = new ArrayList<>();
    private List<VideoBean> jumpList = new ArrayList<>();
    private long lastRefreshTime = 0;

    private int pgcCursor = 0;
    private List<VideoBean> pgcVideList = new ArrayList<>();
    private volatile PgcJudgeBean pgcJudgeBean;
    private List<VideoBean> showList = new ArrayList<>();
    private List<VideoBean> cacheList = new ArrayList<>();
    private boolean pgcHaveNextPage = false;
    private boolean isRequsetingPgc = false;

    private static final int UGC_P_COUNT = 4;

    private AutoPlayVideoCtrl autoPlayVideoCtrl;
    private List<VideoBean> repeatedSeenList = new ArrayList<>();

    public static PgcVideoFallsFragment getInstance(Bundle bundle) {
        PgcVideoFallsFragment instance = new PgcVideoFallsFragment();
        instance.setArguments(bundle);
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isFirstCreate = true;
        recyclerViewStateBean = new RecyclerViewStateBean();
        autoPlayVideoCtrl = new AutoPlayVideoCtrl("");
        pgcJudgeBean = new PgcJudgeBean().init();
        registerReceiver();
    }

    private void registerReceiver() {
        videoMomentReleaseReceiver = new VideoMomentReleaseReceiver();
        final IntentFilter intentFilter = new IntentFilter();
        // intentFilter.addAction(TheLConstants.BROADCAST_RELEASE_NEW_VIDEO_ACTION);
        // intentFilter.addAction(TheLConstants.BROADCAST_RELEASE_MOMENT_SUCCEED);
        intentFilter.addAction(TheLConstants.BROADCAST_VIDEO_COUNT);
        getContext().registerReceiver(videoMomentReleaseReceiver, intentFilter);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_video_falls, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        findViewById(view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        new PgcVideoFallsPresenter(this);
        setListener();

        if (getUserVisibleHint()) {
            initData();
        }

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        tryRefreshData();
        if (isVisibleToUser && isFirstCreate && presenter != null) {
            initData();
        }
    }

    @Override
    public void tryRefreshData() {
        if (getUserVisibleHint() && !isFirstCreate && !(PhoneUtils.getNetWorkType() == PhoneUtils.TYPE_NO)) {//如果不是第一次创建且为可见状态,且有网络
            if (lastRefreshTime != 0 && System.currentTimeMillis() - lastRefreshTime > REFRESH_RATE && swipe_container != null && !swipe_container.isRefreshing()) {//如果大于刷新时间并且不再刷新状态，则刷新
                initData();
            }
        }
    }

    private void initData() {
        isFirstCreate = false;
        pgcJudgeBean.init();
        if (null != swipe_container) {
            swipe_container.post(new Runnable() {
                @Override
                public void run() {
                    swipe_container.setRefreshing(true);
                }
            });
        }
        refreshData();
    }

    private void findViewById(View view) {
        swipe_container = view.findViewById(R.id.swipe_container);
        ViewUtils.initSwipeRefreshLayout(swipe_container);
        recyclerView = view.findViewById(R.id.recyclerview);
        manager = new GridLayoutManager(getActivity(), SPAN_COUNT);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(manager);
        recyclerView.setHasFixedSize(true);
        adapter = new PgcVideoFallsAdapter(showList, recyclerViewStateBean, autoPlayVideoCtrl);
        recyclerView.setAdapter(adapter);
        addHeader();
        final float padding = Utils.dip2px(TheLApp.getContext(), 2);
        VideoFallsUtils.setPgcFallsRecyclerViewDecoration(recyclerView, padding, SPAN_COUNT);
    }

    private void addHeader() {
        header = LayoutInflater.from(getContext()).inflate(R.layout.video_falls_ad_layout, null);
        banner = header.findViewById(R.id.banner);
        banner.setVisibility(View.GONE);
        adapter.addHeaderView(header);
        final int height = (int) (AppInit.displayMetrics.widthPixels / TheLConstants.ad_aspect_ratio);
        banner.getLayoutParams().height = height;
    }

    private void setListener() {
        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshData();
            }
        });

        adapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                gotoVideo(position);
            }
        });
        adapter.setOnLoadMoreListener(new BaseRecyclerViewAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                recyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        if (haveNextPage) {
                            getData(cursor);
                        } else {//为0说明没有更多数据
                            adapter.closeLoadMore(getContext(), recyclerView);
                        }
                    }
                });
            }
        });
        adapter.setReloadMoreListener(new BaseRecyclerViewAdapter.ReloadMoreListener() {
            @Override
            public void reloadMore() {
                recyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        adapter.removeAllFooterView();
                        adapter.openLoadMore(true);
                        getData(cursor);
                    }
                });
            }
        });
        banner.setOnBannerItemClickListener(new BannerLayout.OnBannerItemClickListener() {
            @Override
            public void onItemClick(int position) {
                if (position < adList.size()) {
                    BusinessUtils.AdRedirect(adList.get(position));
                }
            }
        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    if (recyclerViewStateBean != null) {
                        recyclerViewStateBean.setState(RecyclerViewStateBean.State_IDLE);
                    }
                    if (autoPlayVideoCtrl != null) {
                        autoPlayVideoCtrl.setState(RecyclerViewStateImp.State_IDLE);
                    }
                } else if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                    if (recyclerViewStateBean != null) {
                        recyclerViewStateBean.setState(RecyclerViewStateBean.State_Draging);
                    }
                    if (autoPlayVideoCtrl != null) {
                        autoPlayVideoCtrl.setState(RecyclerViewStateImp.State_Draging);
                    }
                } else if (newState == RecyclerView.SCROLL_STATE_SETTLING) {
                    if (recyclerViewStateBean != null) {
                        recyclerViewStateBean.setState(RecyclerViewStateBean.State_Settling);
                    }
                    if (autoPlayVideoCtrl != null) {
                        autoPlayVideoCtrl.setState(RecyclerViewStateImp.State_Settling);
                    }
                }
            }
        });
    }

    /**
     * 跳转
     *
     * @param position
     */
    private void gotoVideo(int position) {
        final VideoBean videoBean = adapter.getItem(position);
        //如果是预发日志，并且webp与video信息还没从服务端返回
        if (videoBean.clickStatus == VideoBean.CLICK_STATUS_DISABLE) {
            return;
        }
        //        List<VideoBean> videoBeans = adapter.getData();
        jumpList.clear();
        jumpList.addAll(adapter.getData());
        jumpList.removeAll(VideoSendList);
        Intent intent = new Intent(getContext(), VideoListActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(TheLConstants.BUNDLE_KEY_VIDEO_LIST, (Serializable) jumpList);
        bundle.putInt(TheLConstants.BUNDLE_KEY_POSITION, position - VideoSendList.size());
        bundle.putInt(TheLConstants.BUNDLE_KEY_CURSOR, cursor);
        bundle.putBoolean(TheLConstants.BUNDLE_KEY_HAVE_NEXT_PAGE, haveNextPage);
        intent.putExtras(bundle);
        GrowingIOUtil.track(this, "videoType", VideoBean.VIDEO_TYPE_PGC == videoBean.videoType ? "pgcVideo" : "ugcVideo");
        getContext().startActivity(intent);
    }

    private void refreshData() {
        lastRefreshTime = System.currentTimeMillis();
        getData(0);
        getPgcData(0);
        presenter.getAdsData();
    }

    private void getPgcData(int cursor) {
        this.pgcCursor = cursor;
        isRequsetingPgc = true;
        presenter.getLoadMorePgcData(pgcCursor);
    }

    private void getData(int cursor) {
        this.cursor = cursor;
        presenter.getLoadMoreData(cursor);
    }

    @Override
    public void setPresenter(PgcVideoFallsContact.Presenter presenter) {
        this.presenter = presenter;
    }


    @Override
    public void showRefreshData(VideoMomentListBean videoMomentListBean) {
        VideoSendList.clear();
        cursor = videoMomentListBean.cursor;
        haveNextPage = videoMomentListBean.haveNextPage;
        list.clear();
        list.addAll(videoMomentListBean.list);
        pgcJudgeBean.haveUgc = true;
        callShow();
        tryAutoPlayVideo();
    }

    private void tryAutoPlayVideo() {
        recyclerView.post(new Runnable() {
            @Override
            public void run() {
                if (recyclerView.getScrollState() == RecyclerView.SCROLL_STATE_IDLE) {
                    if (autoPlayVideoCtrl != null) {
                        autoPlayVideoCtrl.setState(RecyclerViewStateImp.State_IDLE);
                    }
                }
            }
        });
    }

    @Override
    public void showMoreData(VideoMomentListBean videoMomentListBean) {
        final List<VideoBean> momentList = videoMomentListBean.list;
        momentList.removeAll(list);//去重
        cursor = videoMomentListBean.cursor;
        haveNextPage = videoMomentListBean.haveNextPage;
        list.addAll(momentList);
        pgcJudgeBean.haveUgc = true;
        callShow();
        //adapter.notifyDataChangedAfterLoadMore(momentList, true);
    }

    @Override
    public void emptyData() {

    }

    @Override
    public void loadMoreFailed() {
        recyclerView.post(new Runnable() {
            @Override
            public void run() {
                adapter.loadMoreFailed((ViewGroup) recyclerView.getParent());
            }
        });
    }

    @Override
    public void requestFinished() {
        if (swipe_container != null)
            swipe_container.postDelayed(new Runnable() {

                @Override
                public void run() {
                    if (swipe_container != null && swipe_container.isRefreshing())
                        swipe_container.setRefreshing(false);
                }
            }, 1000);
    }

    @Override
    public void showAdsData(List<AdBean.Map_list> adsList) {
        if (adsList == null || adsList.isEmpty()) {
            return;
        }
        this.adList.clear();
        adList.addAll(adsList);
        final List<String> adUrlList = new ArrayList<>();
        for (AdBean.Map_list ad : adList) {
            adUrlList.add(ad.advertURL);
        }
        banner.setVisibility(View.VISIBLE);
        banner.setViewUrls(adUrlList);
    }

    @Override
    public void showRefreshPgcData(VideoMomentListBean videoMomentListBean) {
        this.pgcVideList.clear();
        final List<VideoBean> list = videoMomentListBean.list;
        setListAsPgc(list);
        addSortPgcList(list);
        pgcCursor = videoMomentListBean.cursor;
        pgcJudgeBean.havePgc = true;
        pgcHaveNextPage = videoMomentListBean.haveNextPage;
        callShow();
        isRequsetingPgc = false;
        tryAutoPlayVideo();
    }

    /**
     * 排序后的pgcList
     *
     * @param list
     */
    private void addSortPgcList(List<VideoBean> list) {
        repeatedSeenList.clear();
        final List<String> seenList = VideoSeenUtils.getVideoSeenList();
        for (String videoBeanId : seenList) {
            for (VideoBean bean : list) {
                if (bean.equals(videoBeanId)) {
                    repeatedSeenList.add(bean);
                }
            }
        }
        list.removeAll(repeatedSeenList);
        pgcVideList.addAll(list);
        pgcVideList.addAll(repeatedSeenList);
    }

    private void setListAsPgc(List<VideoBean> list) {
        if (list == null) {
            return;
        }
        for (VideoBean bean : list) {
            bean.videoType = VideoBean.VIDEO_TYPE_PGC;
        }
    }


    @Override
    public void showMorePgcData(VideoMomentListBean videoMomentListBean) {
        final List<VideoBean> list = videoMomentListBean.list;
        setListAsPgc(list);
        list.removeAll(pgcVideList);
        addSortPgcList(list);
        pgcCursor = videoMomentListBean.cursor;
        pgcJudgeBean.havePgc = true;
        pgcHaveNextPage = videoMomentListBean.haveNextPage;
        callShow();
        isRequsetingPgc = false;
    }

    @Override
    public void emptyPgcData() {

    }

    @Override
    public void loadRefreshPgcFailed(int failedCursor) {
        isRequsetingPgc = false;
        pgcJudgeBean.havePgc = true;
        callShow();
    }

    @Override
    public void loadMorePgcFailed(int failedCursor) {
        isRequsetingPgc = false;
    }

    private void callShow() {
        if (!pgcJudgeBean.canShow()) {
            return;
        }
        final int ugcSize = list.size();
        final int pgcSize = pgcVideList.size();
        final int needUgeSize = pgcSize * UGC_P_COUNT;
        cacheList.clear();
        /***pgc不够***/
        if (ugcSize >= needUgeSize) {
            for (int i = 0; i < pgcSize; i++) {
                Collections.addAll(cacheList, pgcVideList.get(i),
                        list.get(0 + UGC_P_COUNT * i + 0),
                        list.get(0 + UGC_P_COUNT * i + 1),
                        list.get(0 + UGC_P_COUNT * i + 2),
                        list.get(0 + UGC_P_COUNT * i + 3)
                );

            }
            cacheList.addAll(needUgeSize, list);
            tryGetMorPgc();
            /***UGC不够***/
        } else {
            int aCount = ugcSize / UGC_P_COUNT;
            int lCount = ugcSize % UGC_P_COUNT;
            for (int i = 0; i < aCount; i++) {
                Collections.addAll(cacheList, pgcVideList.get(i),
                        list.get(0 + UGC_P_COUNT * i + 0),
                        list.get(0 + UGC_P_COUNT * i + 1),
                        list.get(0 + UGC_P_COUNT * i + 2),
                        list.get(0 + UGC_P_COUNT * i + 3)
                );
            }
            if (lCount > 0) {
                cacheList.add(pgcVideList.get(aCount));
                for (int i = 0; i < lCount; i++) {
                    final VideoBean videoBean = list.get(aCount * UGC_P_COUNT + i);
                    cacheList.add(videoBean);
                }
            }
        }
        adapter.removeAllFooterView();
        showList.clear();
        showList.addAll(cacheList);
        // adapter.notifyDataSetChanged();
        adapter.setNewData(showList);
        adapter.openLoadMore(showList.size(), true);
    }

    private void tryGetMorPgc() {
        if (!isRequsetingPgc && pgcHaveNextPage) {
            getPgcData(pgcCursor);
        }
    }

    @Override
    public void onTitleClick() {
        if (swipe_container != null && !swipe_container.isRefreshing() && isVisible() && recyclerView != null) {
            recyclerView.smoothScrollToPosition(0);
        }
    }

    @Override
    public void onBlackUsersChanged(String userId, boolean isAdd) {
        if (TextUtils.isEmpty(userId) || userId.equals("0") || !isAdd) {
            return;
        }
        if (swipe_container != null && !swipe_container.isRefreshing() && adapter != null && adapter.getData() != null) {
            final List<VideoBean> removeList = new ArrayList<>();
            for (VideoBean bean : list) {
                if ((bean.userId + "").equals(userId)) {
                    removeList.add(bean);
                }
            }
            for (VideoBean bean : pgcVideList) {
                if ((bean.userId + "").equals(userId)) {
                    removeList.add(bean);
                }
            }
            list.removeAll(removeList);
            pgcVideList.removeAll(removeList);
            adapter.getData().removeAll(removeList);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onBlackOneMoment(String momentId) {
        if (TextUtils.isEmpty(momentId) || momentId.equals("0")) {
            return;
        }
        if (swipe_container != null && !swipe_container.isRefreshing() && adapter != null && adapter.getData() != null) {
            final VideoBean bean = new VideoBean();
            bean.id = momentId;
            final List<VideoBean> removeList = new ArrayList<>();
            removeList.add(bean);
            adapter.getData().removeAll(removeList);
            list.removeAll(removeList);
            pgcVideList.removeAll(removeList);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onMomentDelete(String momentId) {
        super.onMomentDelete(momentId);
        onBlackOneMoment(momentId);
    }

    @Override
    public void onMomentReport(String momentId) {
        super.onMomentReport(momentId);
        onBlackOneMoment(momentId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            getContext().unregisterReceiver(videoMomentReleaseReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBlackherMoment(String userId) {
        onBlackUsersChanged(userId, true);
    }


    class VideoMomentReleaseReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            //添加一个本地正在发布的视频数据
            if (TheLConstants.BROADCAST_RELEASE_NEW_VIDEO_ACTION.equals(intent.getAction())) {
                String releaseJson = ShareFileUtils.getString(TheLConstants.RELEASE_CONTENT_KEY, "");
                ReleaseMomentListBean releaseMomentListBean = GsonUtils.getObject(releaseJson, ReleaseMomentListBean.class);
                if (releaseMomentListBean != null && releaseMomentListBean.list != null && releaseMomentListBean.list.size() > 0) {
                    MomentsBean momentsBean = releaseMomentListBean.list.get(0);
                    if (MomentsBean.MOMENT_TYPE_VIDEO.equals(momentsBean.momentsType)) {
                        final VideoBean videoBean = VideoBean.getVideoBeanFromMoment(momentsBean);
                        addVideoToListTop(videoBean);
                    }
                }//发布未成功，但已经获取到了视频地址和webp地址
            } else if (TheLConstants.BROADCAST_RELEASE_MOMENT_SUCCEED.equals(intent.getAction())) {
                final Bundle bundle = intent.getExtras();
                final String momentId = bundle.getString(TheLConstants.BUNDLE_KEY_MOMENT_ID);
                final String videoUrl = bundle.getString(TheLConstants.BUNDLE_KEY_VIDEO_URL);
                final String videoWebp = bundle.getString(TheLConstants.BUNDLE_KEY_VIDEO_WEBP);
                final String imageUrl = bundle.getString(TheLConstants.BUNDLE_KEY_IMAGE_URL);
                final String momentSendId = bundle.getString(TheLConstants.BUNDLE_KEY_MOMENT_SEND_ID);
                updateVideoBean(momentSendId, momentId, videoUrl, videoWebp, imageUrl);
            } else if (TheLConstants.BROADCAST_VIDEO_COUNT.equals(intent.getAction())) {
                VideoBean videoBean = (VideoBean) intent.getExtras().getSerializable(TheLConstants.BUNDLE_KEY_VIDEO_BEAN);
                if (videoBean != null && adapter != null && adapter.getData() != null) {
                    final int size = adapter.getData().size();
                    for (int i = 0; i < size; i++) {
                        final VideoBean video = adapter.getData().get(i);
                        if (video.equals(videoBean)) {
                            video.playCount = videoBean.playCount;
                            video.winkFlag = videoBean.winkFlag;
                            video.winkNum = videoBean.winkNum;
                        }
                    }
                }
            }

        }

    }

    private void updateVideoBean(String momentSendId, String momentId, String videoUrl, String videoWebp, String imageUrl) {
        if (TextUtils.isEmpty(momentId) || TextUtils.isEmpty(videoUrl) || TextUtils.isEmpty(videoWebp) || adapter == null || adapter.getData() == null) {
            return;
        }
        final List<VideoBean> list = adapter.getData();
        final int size = list.size();
        for (int i = 0; i < size; i++) {
            final VideoBean videoBean = list.get(i);
            if (momentSendId.equals(videoBean.id)) {
                VideoSendList.remove(videoBean);
                videoBean.videoUrl = videoUrl;
                //videoBean.videoWebp = videoWebp;(因为webp转换需要时间不定，所以暂时这个数据先不更新)
                videoBean.image = imageUrl;
                videoBean.id = momentId;
                videoBean.clickStatus = VideoBean.CLICK_STATUS_ENABLE;
                final int updatePos = i + adapter.getHeaderLayoutCount();
                adapter.notifyItemChanged(updatePos);
                return;
            }
        }
    }

    private void addVideoToListTop(VideoBean videoBean) {
        if (adapter != null && adapter.getData() != null) {
            final List<VideoBean> list = adapter.getData();
            list.add(0, videoBean);
            VideoSendList.add(videoBean);
            final int insertPos = 0 + adapter.getHeaderLayoutCount();
            //adapter.notifyItemInserted(insertPos);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!isFirstCreate && adapter != null && adapter.getData() != null && autoPlayVideoCtrl != null) {
            autoPlayVideoCtrl.setState(RecyclerViewStateImp.State_IDLE);
        }
        VideoPlayerProxy2.getInstance().resume();
    }

    @Override public void onPause() {
        super.onPause();

        VideoPlayerProxy2.getInstance().pause();

    }
}
