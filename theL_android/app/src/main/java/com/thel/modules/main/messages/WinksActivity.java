package com.thel.modules.main.messages;

import android.os.Bundle;
import androidx.annotation.Nullable;

import com.thel.R;
import com.thel.base.BaseActivity;

public class WinksActivity extends BaseActivity {

    @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_frame_normal);

        WinksFragment winksFragment = new WinksFragment();

        getSupportFragmentManager().beginTransaction().add(R.id.frame_content, winksFragment, WinksFragment.class.getName()).commit();

    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }
}
