package com.thel.modules.main.discover.view;

import com.thel.bean.live.LiveTypeNetBean;
import com.thel.modules.live.bean.LivePopularityNetBean;
import com.thel.modules.main.discover.LiveRoomsListContract2;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.utils.L;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by lingwei on 2017/11/19.
 */

public class LiveRoomsListPresenter implements LiveRoomsListContract2.Presenter {

    private final LiveRoomsListContract2.View view;

    public LiveRoomsListPresenter(LiveRoomsListContract2.View view) {
        this.view = view;
        view.setPresenter(this);
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {

    }

    @Override
    public void getRefreshLiveClassfyData() {
        Flowable<LiveTypeNetBean> flowable = RequestBusiness.getInstance().getLiveTypeList();
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<LiveTypeNetBean>() {
            @Override
            public void onNext(LiveTypeNetBean data) {
                super.onNext(data);
                view.showRefreshLiveClassifyData(data.data);
                //                                    closeLoading();
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                L.d("LivePopularityNetBean " + "onerror" + t.getMessage());
            }
        });
    }

    @Override
    public void getRefreshTopLinkData() {
        Flowable<LivePopularityNetBean> flowable1 = RequestBusiness.getInstance().getLiveRank();
        flowable1.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<LivePopularityNetBean>() {
            @Override
            public void onNext(LivePopularityNetBean livePopularityNetBean) {
                super.onNext(livePopularityNetBean);

                view.showRefreshTopLinkData(livePopularityNetBean);
                //                     closeLoading();

            }

            @Override
            public void onComplete() {
                super.onComplete();
                view.requestFinish();
            }
        });
    }
}
