package com.thel.modules.video;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thel.R;
import com.thel.bean.video.VideoBean;
import com.thel.constants.TheLConstants;
import com.thel.imp.BaseFunctionFragment;
import com.thel.imp.ViewPagerScrollableCallback;
import com.thel.modules.main.discover.adapter.LiveClassifyFragmentAdapter;
import com.thel.modules.main.video_discover.videofalls.VideoFallsUtils;
import com.thel.modules.main.video_discover.videofalls.VideoMomentNetListBean;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.service.DefaultRequestService;
import com.thel.ui.widget.MyVerticalViewPager;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.NetworkUtils;

import org.reactivestreams.Subscription;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by waiarl on 2018/4/1.
 */

public class VideoListFragment extends BaseFunctionFragment {

    private final int DEFAULT_VALUE = 0;
    private List<VideoBean> videoBeanList;
    private int position;
    private int cursor;
    private MyVerticalViewPager viewpager;
    private List<Fragment> fragmentList = new ArrayList<>();
    private LiveClassifyFragmentAdapter adapter;
    private static final int PAGE_SIZE = 20;

    /**
     * 进入当前页面时间
     */
    private long intoTime;

    /**
     * 离开当前页面时间
     */
    private long outTime;
    private boolean isRequesting = false;
    private boolean haveNextPage = true;

    public static VideoListFragment newInstance(Bundle bundle) {
        VideoListFragment fragment = new VideoListFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        intoTime = System.currentTimeMillis();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_video_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        initData();

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setListener();
    }

    private void initView(View view) {
        viewpager = view.findViewById(R.id.vertical_vp);
        adapter = new LiveClassifyFragmentAdapter(fragmentList, getChildFragmentManager());
        viewpager.setAdapter(adapter);
    }

    private void initData() {
        final Bundle bundle = getArguments();
        videoBeanList = (List<VideoBean>) bundle.getSerializable(TheLConstants.BUNDLE_KEY_VIDEO_LIST);
        position = bundle.getInt(TheLConstants.BUNDLE_KEY_POSITION, DEFAULT_VALUE);
        cursor = bundle.getInt(TheLConstants.BUNDLE_KEY_POSITION, DEFAULT_VALUE);
        haveNextPage = bundle.getBoolean(TheLConstants.BUNDLE_KEY_HAVE_NEXT_PAGE, true);
        if (videoBeanList == null || videoBeanList.isEmpty()) {
            getVideoBeanListData(cursor);
        } else {
            initFragment(videoBeanList);
            if (position >= 0 && position < fragmentList.size()) {
                viewpager.setCurrentItem(position);
            }
        }
    }

    private void initFragment(List<VideoBean> videoBeanList) {
        final int fragmentSize = fragmentList.size();
        final int size = videoBeanList.size();
        for (int i = 0; i < size; i++) {
            final VideoFragment videoFragment = VideoFragment.newInstance(videoBeanList.get(i), VideoFragment.VIDEO_LIST, i + fragmentSize);
            fragmentList.add(videoFragment);
            videoFragment.setViewPagerScrollableCallback(new ViewPagerScrollableCallback() {
                @Override
                public void canScroll(boolean canScroll) {
                    if (viewpager != null) {
                        viewpager.setCanScrollable(canScroll);
                    }
                }
            });
        }
        adapter.notifyDataSetChanged();
    }

    private void getVideoBeanListData(final int cur) {
        isRequesting = true;
        final Flowable<VideoMomentNetListBean> flowable = DefaultRequestService.createNearbyRequestService().getVideoFallsMomentsList(cur, PAGE_SIZE);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<VideoMomentNetListBean>() {
            @Override
            public void onSubscribe(Subscription s) {
                super.onSubscribe(s);
            }

            @Override
            public void onNext(VideoMomentNetListBean data) {
                super.onNext(data);
                if (data.data != null && data.data.list != null) {
                    cursor = data.data.cursor;
                    haveNextPage = data.data.haveNextPage;
                    VideoFallsUtils.filtNearbyMoment(data.data);
                    videoBeanList.addAll(data.data.list);
                    initFragment(data.data.list);
                }
                isRequesting = false;
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                isRequesting = false;
            }

            @Override
            public void onComplete() {
                super.onComplete();
                isRequesting = false;
            }
        });
    }

    private void setListener() {
        viewpager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                final int pageSize = viewpager.getAdapter().getCount();
                if (pageSize - position < 2 && !isRequesting && NetworkUtils.isNetworkConnected(getContext()) && haveNextPage) {
                    getVideoBeanListData(cursor);
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        //4.7.3瀑布流推荐视频刘表GIO 埋点
        outTime = System.currentTimeMillis();
        int allPlayDuration = (int) ((outTime - intoTime) / 1000);
        GrowingIOUtil.uploadRecommendVideoDuration(allPlayDuration);
        // Log.d("recommendPlayDuration", String.valueOf(allPlayDuration));

        super.onDestroy();

    }

    @Override
    public void onMomentReport(String momentId) {
        super.onMomentReport(momentId);
        removeFragmentById(momentId);
    }

    @Override
    public void onBlackherMoment(String userId) {
        super.onBlackherMoment(userId);
        removeFragmentByUser(userId);
    }


    @Override
    public void onMomentDelete(String momentId) {
        super.onMomentDelete(momentId);
        removeFragmentById(momentId);
    }

    @Override
    public void onBlackOneMoment(String momentId) {
        super.onBlackOneMoment(momentId);
        removeFragmentById(momentId);
    }

    private void removeFragmentById(String momentId) {
        if (TextUtils.isEmpty(momentId)) {
            return;
        }
        int size = videoBeanList.size();
        for (int i = 0; i < size; i++) {
            final VideoBean bean = videoBeanList.get(i);
            if (bean.equals(momentId)) {
                videoBeanList.remove(i);
                removeFragment(i);
                i -= 1;
                size -= 1;
            }
        }

    }

    private void removeFragment(int index) {
        try {
            final int currentIndex = viewpager.getCurrentItem();
            if (index != currentIndex) {
                fragmentList.remove(index);
                viewpager.getAdapter().notifyDataSetChanged();
            } else if (currentIndex == index) {
                if (viewpager.getAdapter().getCount() == 1) {
                    getActivity().finish();
                } else if (index == viewpager.getAdapter().getCount() - 1) {
                    viewpager.setCurrentItem(index - 1);
                    fragmentList.remove(index);
                    viewpager.getAdapter().notifyDataSetChanged();
                } else {
                    //  viewpager.setCurrentItem(index + 1);
                    fragmentList.remove(index);
                    viewpager.getAdapter().notifyDataSetChanged();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void removeFragmentByUser(String userId) {
        if (TextUtils.isEmpty(userId)) {
            return;
        }
        int size = videoBeanList.size();
        for (int i = 0; i < size; i++) {
            final VideoBean bean = videoBeanList.get(i);
            if (bean.userId.equals(userId)) {
                videoBeanList.remove(i);
                removeFragment(i);
                i -= 1;
                size -= 1;
            }
        }
    }
}
