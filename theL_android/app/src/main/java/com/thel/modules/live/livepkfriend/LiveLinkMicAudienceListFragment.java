package com.thel.modules.live.livepkfriend;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.thel.R;
import com.thel.base.BaseFragment;
import com.thel.modules.live.LiveConstant;
import com.thel.modules.live.bean.LivePkFriendBean;
import com.thel.modules.live.in.LivePkContract;
import com.thel.modules.live.surface.show.LiveShowBaseViewFragment;
import com.thel.modules.live.utils.LinkMicOrPKRefuseUtils;
import com.thel.ui.decoration.DefaultItemDivider;
import com.thel.utils.L;
import com.thel.utils.ViewUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LiveLinkMicAudienceListFragment extends BaseFragment {

    private static final String TAG = "LiveLinkMicAudienceListFragment";

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.lin_default)
    LinearLayout lin_default;

    @BindView(R.id.swipe_container)
    SwipeRefreshLayout swipe_container;

    private LiveLinkMicOrPKListAdapter adapter;

    private LinearLayoutManager manager;

    private List<LivePkFriendBean> askLinkMicList = new ArrayList<>();//请求排麦名单

    private List<LivePkFriendBean> dailyList = new ArrayList<>();//日榜名单

    private List<LivePkFriendBean> allList = new ArrayList<>();

    private LivePkContract.FriendRequestClickListener friendRequestClickListener;

    private int linkMicStatus = LiveShowBaseViewFragment.LINK_MIC_STATUS_NONE;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_live_link_mic_audience_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);

        ViewUtils.initSwipeRefreshLayout(swipe_container);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initView();

        initData();

//        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                getDailyData();
//            }
//        });
    }

    public void setLinkMicStatus(int linkMicStatus) {
        this.linkMicStatus = linkMicStatus;
        if (adapter != null)
            adapter.setLinkMicStatus(linkMicStatus);
    }

    private void initView() {

        LinkMicOrPKRefuseUtils.initRefuseData();

        adapter = new LiveLinkMicOrPKListAdapter(allList, LiveConstant.TYPE_AUDIENCE_LINK_MIC);

        adapter.setOnFriendRequestClickListener(new LivePkContract.FriendRequestClickListener() {
            @Override
            public void onClickRequest(LivePkFriendBean friendBean, int type, int position) {

                if (friendRequestClickListener != null) {
                    friendRequestClickListener.onClickRequest(friendBean, type, position);
                }

            }
        });

        manager = new LinearLayoutManager(getContext());

        manager.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView.setLayoutManager(manager);

        recyclerView.addItemDecoration(new DefaultItemDivider(getContext(), LinearLayoutManager.VERTICAL, R.color.gray, 1, false, false));

        adapter.setLinkMicStatus(linkMicStatus);

        recyclerView.setAdapter(adapter);

        if (allList.size() == 0) {
            lin_default.setVisibility(View.VISIBLE);
        }

    }

    private void initData() {

        if (getArguments() != null) {
            askLinkMicList = (List<LivePkFriendBean>) getArguments().getSerializable("linkMicData");
        }

        if (askLinkMicList != null && askLinkMicList.size() > 0) {
            notifyDataSetChanged();
        }

        L.d(TAG, " askLinkMicList : " + askLinkMicList);

//        swipe_container.post(new Runnable() {
//            @Override
//            public void run() {
//                swipe_container.setRefreshing(true);
//            }
//        });
//        getDailyData();
    }

    /**
     * 拉取日榜名单
     */
    private void getDailyData() {
        if (linkMicAudienceListener != null) linkMicAudienceListener.getTopFansTodayList();
    }

    private void requestFinish() {
        if (swipe_container != null) {
            swipe_container.postDelayed(new Runnable() {

                @Override
                public void run() {
                    if (swipe_container != null && swipe_container.isRefreshing())
                        swipe_container.setRefreshing(false);
                }
            }, 1000);
        }
    }

    public void addItem(LivePkFriendBean livePkFriendBean) {
        if (!askLinkMicList.contains(livePkFriendBean))
            askLinkMicList.add(livePkFriendBean);
        notifyDataSetChanged();
    }

    public void removeItem(LivePkFriendBean livePkFriendBean) {

        startLinkMic(livePkFriendBean.id);
    }

    public void addDialyList(List<LivePkFriendBean> list) {
        requestFinish();

        if (list != null) {
            for (LivePkFriendBean bean : list) {
                bean.isGuard = true;
            }
            dailyList.clear();
            dailyList.addAll(list);
        }
        notifyDataSetChanged();
    }

    public void startLinkMic(String userId) {
        Iterator<LivePkFriendBean> iterator = askLinkMicList.iterator();

        while (iterator.hasNext()) {

            LivePkFriendBean lpb = iterator.next();

            if (lpb.id.equals(userId)) {

                iterator.remove();

            } else {
                lpb.liveStatus = LivePkFriendBean.LIVE_STATUS_LINK;
            }

        }

        notifyDataSetChanged();
    }

    private void notifyDataSetChanged() {
        allList.clear();
        allList.addAll(askLinkMicList);
        allList.addAll(dailyList);

        if (askLinkMicList.size() > 0 && dailyList.size() > 0) {
            adapter.setIndicatorIndex(askLinkMicList.size() - 1);
        } else {
            adapter.setIndicatorIndex(-1);
        }
        adapter.notifyDataSetChanged();

        showEmptyView();
    }

    private void showEmptyView() {
        if (allList.size() > 0) {
            lin_default.setVisibility(View.GONE);
        } else {
            lin_default.setVisibility(View.VISIBLE);
        }
    }

    /**
     * 点击连麦
     *
     * @param listener
     */
    public void setOnFriendRequestClickListener(LivePkContract.FriendRequestClickListener listener) {
        this.friendRequestClickListener = listener;
    }

    private LinkMicAudienceListener linkMicAudienceListener;

    public void setLinkMicAudienceListener(LinkMicAudienceListener linkMicAudienceListener) {
        this.linkMicAudienceListener = linkMicAudienceListener;
    }

    public interface LinkMicAudienceListener {
        void getTopFansTodayList();
    }
}
