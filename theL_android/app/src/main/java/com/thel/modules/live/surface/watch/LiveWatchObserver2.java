package com.thel.modules.live.surface.watch;


import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.thel.app.TheLApp;
import com.thel.bean.live.LinkMicResponseBean;
import com.thel.bean.live.LiveMultiOnCreateBean;
import com.thel.bean.live.LiveMultiSeatBean;
import com.thel.bean.live.MultiSpeakersBean;
import com.thel.callback.IConnectMic;
import com.thel.modules.live.bean.AgoraBean;
import com.thel.modules.live.bean.AudienceLinkMicResponseBean;
import com.thel.modules.live.bean.LiveFreeGoldReeponceBean;
import com.thel.modules.live.bean.LivePkGemNoticeBean;
import com.thel.modules.live.bean.LivePkHangupBean;
import com.thel.modules.live.bean.LivePkInitBean;
import com.thel.modules.live.bean.LivePkStartNoticeBean;
import com.thel.modules.live.bean.LivePkSummaryNoticeBean;
import com.thel.modules.live.bean.LiveRoomMsgBean;
import com.thel.modules.live.bean.LiveRoomMsgConnectMicBean;
import com.thel.modules.live.bean.ResponseGemBean;
import com.thel.modules.live.bean.SoftGiftBean;
import com.thel.modules.live.bean.TopTodayBean;
import com.thel.modules.live.in.LiveShowAudienceIn;
import com.thel.modules.live.in.LiveShowCaptureStreamIn;
import com.thel.modules.live.in.LiveShowMsgIn;
import com.thel.modules.live.in.LiveShowVideoIn;
import com.thel.modules.live.interfaces.IChatRoomRequest;
import com.thel.modules.live.interfaces.ILive;
import com.thel.modules.live.interfaces.ILiveAnchor;
import com.thel.modules.live.interfaces.ILiveAudience;
import com.thel.modules.live.utils.LiveStatus;
import com.thel.modules.live.view.expensive.TopGiftBean;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;
import com.thel.utils.ToastUtils;
import com.thel.utils.UserUtils;

import org.jetbrains.annotations.NotNull;

import io.agora.rtc.IRtcEngineEventHandler;

import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_ANCHOR_AUTO_PING;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_ANCHOR_LIVE_CONNECTING;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_ANCHOR_LIVE_CONNECT_BREAK;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_ANCHOR_LIVE_CONNECT_FAILED;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_ANCHOR_LIVE_CONNECT_SUCCESS;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_ANCHOR_REFRESH_MSGS;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_ANCHOR_RESET_IS_BOTTOM;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_ANCHOR_SCROLL_TO_BOTTOM;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_ANCHOR_STOP_SHOW;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_ANCHOR_UPDATE_AUDIENCE_COUNT;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_ANCHOR_UPDATE_MY_NET_STATUS_BAD;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_ANCHOR_UPDATE_MY_NET_STATUS_GOOD;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_AUDIENCE_BEAM_AVAILABLE_LEVEL_SIX;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_AUDIENCE_LINK_MIC_RESPONSE;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_AUTO_PING;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_BEEN_BLOCKED;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_CHANNEL_ID;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_CLEAR_INPUT;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_DAILY_LINK_MIC;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_DANMU_RECEIVE_MSG;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_ENCOUNTER_SB;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_ENCOUNTER_SPEAKERS;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_END_ENCOUNTER;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_FREE_BARRAGE;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_FREE_GOLD;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_GIFT_RECEIVE_MSG;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_GIFT_SEND_RESULT_MSG;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_GIFT_SEND_SUCCESS_MSG;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_GUEST_GIFT;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_GUEST_OFF_SEAT;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_GUEST_ONSEAR_AUTH;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_GUEST_ON_SEAT;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_HIDE_CACHING;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_HIDE_PREVIEW_IMAGE;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_INIT_MULTI;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_JOIN_USER;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_JOIN_VIP_USER;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_LINK_MIC_BUSY;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_LINK_MIC_CANCEL;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_LINK_MIC_HANGUP;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_LINK_MIC_IS_ACCEPT;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_LINK_MIC_STOP;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_LIVE_CLOSED;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_LIVE_COMPELETE;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_LIVE_ERROR;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_MIC_ADD;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_MIC_AGREE;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_MIC_DEL;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_MIC_LIST;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_MIC_OFF;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_MIC_ON;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_OPEN_INPUT_FALSE;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_OPEN_INPUT_TRUE;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_PK_GEM_NOTICE;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_PK_HANGUP_NOTICE;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_PK_INIT_MSG;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_PK_START_NOTICE;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_PK_STOP_NOTICE;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_PK_SUMMARY_NOTICE;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_RECONNECTING;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_REFRESH_MSGS;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_REFRESH_ROOM;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_REFRESH_SEAT;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_REQUEST;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_RESET_IS_BOTTOM;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_SCROLL_TO_BOTTOM;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_SHOW_CACHING;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_SHOW_PROGRESSBAR;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_SPEAK_TO_FAST;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_START_ENCOUNTER;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_TOP_GIFT;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_TOP_TODAY;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_UPDATE_AUDIENCE_COUNT;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_UPDATE_BROADCASTER_NET_STATUS;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_UPDATE_MY_NET_STATUS_BAD;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_UPDATE_MY_NET_STATUS_GOOD;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_USER_ID;
import static com.thel.modules.live.LiveCodeConstants.UI_EVENT_USER_NAME;
import static io.agora.rtc.Constants.CLIENT_ROLE_AUDIENCE;
import static io.agora.rtc.Constants.CLIENT_ROLE_BROADCASTER;

/**
 * Created by waiarl on 2017/11/5.
 */

public class LiveWatchObserver2 extends Handler implements IChatRoomRequest {
    private final String TAG = LiveWatchObserver2.class.getSimpleName();

    private LiveShowVideoIn video;
    public ILive iLive;
    private ILiveAudience iLiveAudience;
    private ILiveAnchor iLiveAnchor;
    private LiveShowMsgIn client;
    private LiveShowAudienceIn mStreamAudienceIn;
    private boolean destoryed = false;
    private long intoTime; //上麦的时间
    private long outTime;
    private IConnectMic iConnectMic;
    private IChatRoomRequest iChatRoomRequest;
    private LiveShowCaptureStreamIn liveShowCaptureStreamIn;

    public LiveWatchObserver2() {
        super(Looper.getMainLooper());
    }

    public void bindVideo(LiveShowVideoIn video) {
        this.video = video;
    }

    public void bindStreamAudience(LiveShowAudienceIn mAudienceIn) {
        this.mStreamAudienceIn = mAudienceIn;
    }

    public void bindILive(ILive iLive) {

        L.d(TAG, " bindILive iLive : " + iLive);
        this.iLive = iLive;
    }

    public void bindILiveAudience(ILiveAudience iLiveAudience) {
        this.iLiveAudience = iLiveAudience;
    }

    public void bindILiveAnchor(ILiveAnchor iLiveAnchor) {
        this.iLiveAnchor = iLiveAnchor;
    }

    public void bindMsgClient(LiveShowMsgIn client) {
        this.client = client;
    }

    public void bindConnectMic(IConnectMic iConnectMic) {
        this.iConnectMic = iConnectMic;
    }

    public void setIChatRoomRequest(IChatRoomRequest iChatRoomRequest) {
        this.iChatRoomRequest = iChatRoomRequest;
    }

    public void bindLiveShowCaptureStreamIn(LiveShowCaptureStreamIn liveShowCaptureStreamIn) {
        this.liveShowCaptureStreamIn = liveShowCaptureStreamIn;
    }

    @Override
    public void handleMessage(Message msg) {
//        if (destoryed) {
//            return;
//        }
//
//        L.i(TAG, "what=" + msg.what + ",msg=" + (msg.obj == null ? "" : msg.obj.toString()));
//
//        switch (msg.what) {
//            case UI_EVENT_UPDATE_AUDIENCE_COUNT:
//                if (iLive != null) {
//                    ResponseGemBean responseGemBean = (ResponseGemBean) msg.obj;
//                    iLive.updateAudienceCount(responseGemBean);
//                }
//                break;
//            case UI_EVENT_REFRESH_MSGS:
//                L.d(TAG, " handleMessage UI_EVENT_REFRESH_MSGS iLive : " + iLive);
//
//                L.d(TAG, " handleMessage UI_EVENT_REFRESH_MSGS iLiveAudience : " + iLiveAudience);
//
//                L.d(TAG, " handleMessage UI_EVENT_REFRESH_MSGS iLiveAnchor : " + iLiveAnchor);
//
//                if (iLive != null) {
//                    iLive.refreshMsg();
//                } else {
//                    ToastUtils.showToastShort(TheLApp.getContext(), "iLive is null");
//                }
//                break;
//            case UI_EVENT_SCROLL_TO_BOTTOM:
//                if (iLive != null) {
//                    iLive.smoothScrollToBottom();
//                }
//                break;
//            case UI_EVENT_CLEAR_INPUT:
//                if (iLive != null) {
//                    iLive.clearInput();
//                }
//                break;
//            case UI_EVENT_LIVE_CLOSED:
//                if (iLiveAudience != null) {
//                    iLiveAudience.liveClosed();
//                }
//                if (video != null) {
//                    video.liveClosed();
//                }
//                break;
//            case UI_EVENT_AUTO_PING:
//                if (client != null) {
//                    client.autoPingServer();
//                }
//                break;
//            case UI_EVENT_RESET_IS_BOTTOM:
//                if (iLiveAudience != null) {
//                    iLiveAudience.isBottom(true);
//                }
//                break;
//            case UI_EVENT_BEEN_BLOCKED:
//                if (iLiveAudience != null) {
//                    iLiveAudience.beenBlocked();
//                }
//                break;
//            case UI_EVENT_HIDE_PREVIEW_IMAGE:
//                if (iLiveAudience != null) {
//                    iLiveAudience.hidePreviewImage();
//                }
//                break;
//            case UI_EVENT_LIVE_ERROR:
//                Log.e(TAG, "error: " + msg.arg1);
//                break;
//            case UI_EVENT_SHOW_CACHING:
//                if (iLiveAudience != null) {
//                    iLiveAudience.showCaching();
//                }
//                break;
//            case UI_EVENT_HIDE_CACHING:
//                if (iLiveAudience != null) {
//                    iLiveAudience.hideCaching();
//                }
//                break;
//            case UI_EVENT_UPDATE_BROADCASTER_NET_STATUS:
//                final int status = (int) msg.obj;
//                if (iLiveAudience != null) {
//                    iLiveAudience.updateBroadcasterNetStatus(status);
//                }
//                break;
//            case UI_EVENT_UPDATE_MY_NET_STATUS_BAD:
//                if (iLive != null) {
//                    iLive.updateMyNetStatusBad();
//                }
//                break;
//            case UI_EVENT_UPDATE_MY_NET_STATUS_GOOD:
//                if (iLiveAudience != null) {
//                    iLiveAudience.updateMyNetStatusGood();
//                }
//                break;
//            case UI_EVENT_SPEAK_TO_FAST:
//                if (iLiveAudience != null) {
//                    iLiveAudience.speakTooFast();
//                }
//                break;
//            case UI_EVENT_GIFT_RECEIVE_MSG://收到 接收到礼物广播
//                Bundle bundle = msg.getData();
//                String payload = bundle.getString("payload");
//                String code = bundle.getString("code");
//                if (iLive != null) {
//                    iLive.parseGiftMsg(payload, code);
//                }
//                break;
//            case UI_EVENT_GIFT_SEND_RESULT_MSG://发送礼物，接受到的返回消息
//                String result = (String) msg.obj;
//                if (iLiveAudience != null) {
//                    iLiveAudience.updateBalance(result);//更新余额
//                }
//                break;
//            case UI_EVENT_LIVE_COMPELETE:
//                //                    DialogUtil.showAlert(LiveShowActivity.this, "", getString(R.string.live_ended), new DialogInterface.OnClickListener() {
//                //                        @Override
//                //                        public void onClick(DialogInterface dialog, int which) {
//                //                            dialog.dismiss();
//                //                            finish();
//                //                        }
//                //                    });
//                break;
//            case UI_EVENT_DANMU_RECEIVE_MSG://弹幕
//                String danMsg = (String) msg.obj;
//                if (iLive != null) {
//                    //显示弹幕
//                    iLive.showDanmu(danMsg);
//                }
//                break;
//            break;
//            case UI_EVENT_OPEN_INPUT_FALSE:
//                if (iLive != null) {
//                    iLive.openInput(false);
//                }
//                break;
//            case UI_EVENT_OPEN_INPUT_TRUE:
//                String ms = (String) msg.obj;
//                if (iLive != null) {
//                    iLive.setDanmuResult(ms);//弹幕返回结果
//                }
//                break;
//            case UI_EVENT_JOIN_USER:
//                break;
//            case UI_EVENT_JOIN_VIP_USER:
//                final LiveRoomMsgBean liveRoomMsgBean = (LiveRoomMsgBean) msg.obj;
//                if (iLive != null) {
//                    iLive.joinVipUser(liveRoomMsgBean);
//                }
//                break;
//            case UI_EVENT_RECONNECTING:
//                if (iLiveAudience != null) {
//                    iLiveAudience.reconnecting();
//                }
//                break;
//            case UI_EVENT_CHANNEL_ID:
//                if (iLiveAudience != null) {
//                    iLiveAudience.showDialogChannelId();
//                }
//                break;
//            case UI_EVENT_REQUEST:
//                if (iLiveAudience != null) {
//                    iLiveAudience.showDialogRequest();
//                }
//                break;
//            case UI_EVENT_USER_ID:
//                if (iLiveAudience != null) {
//                    iLiveAudience.showAlertUserId();
//                }
//                break;
//            case UI_EVENT_USER_NAME:
//                if (iLiveAudience != null) {
//                    iLiveAudience.showAlertUserName();
//                }
//                break;
//            case UI_EVENT_SHOW_PROGRESSBAR:
//                final boolean show = (boolean) msg.obj;
//                if (iLiveAudience != null) {
//                    iLiveAudience.showProgressbar(show);
//                }
//                break;
//            /***********************************************一下为Pk**************************************************************/
//            case UI_EVENT_PK_START_NOTICE://PK开始消息
//                if (iLive != null) {
//                    final LivePkStartNoticeBean pkStartNoticeBean = (LivePkStartNoticeBean) msg.obj;
//                    iLive.showPkStartView(pkStartNoticeBean);
//                }
//                break;
//            case UI_EVENT_PK_STOP_NOTICE://收到PK结束 通知
//                if (iLive != null) {
//                    final String pkStopPayload = (String) msg.obj;
//                    iLive.showPkStopView(pkStopPayload);
//                }
//                break;
//            case UI_EVENT_PK_SUMMARY_NOTICE://Pk总结阶段 通知
//                if (iLive != null) {
//                    final LivePkSummaryNoticeBean pkSummaryNoticeBean = (LivePkSummaryNoticeBean) msg.obj;
//                    iLive.showPkSummaryView(pkSummaryNoticeBean);
//                }
//                break;
//            case UI_EVENT_PK_GEM_NOTICE://主播收到礼物时 服务器发送主播软妹币增量通知给双方直播室内的所有人
//                if (iLive != null) {
//                    final LivePkSummaryNoticeBean pkSummaryNoticeBean = (LivePkSummaryNoticeBean) msg.obj;
//                    iLive.showPkSummaryView(pkSummaryNoticeBean);
//                }
//                break;
//            case UI_EVENT_PK_INIT_MSG://pk断线重连信息
//                if (iLive != null) {
//                    final LivePkInitBean livePkInitBean = (LivePkInitBean) msg.obj;
//                    iLive.showLivePkInitView(livePkInitBean);
//                }
//                break;
//            case UI_EVENT_PK_HANGUP_NOTICE://收到PK挂断 通知
//                if (iLive != null) {
//                    final LivePkHangupBean pkHangupBean = (LivePkHangupBean) msg.obj;
//                    iLive.showPkHangupView(pkHangupBean);
//                }
//                break;
//            /***********************************************以上为Pk**************************************************************/
//            case UI_EVENT_LINK_MIC_BUSY:
//                if (iLiveAudience != null) {
//                    LinkMicResponseBean linkMicResponseBean = (LinkMicResponseBean) msg.obj;
//                    iLiveAudience.linkMicBusy(linkMicResponseBean);
//                    iLiveAudience.linkMicStart(linkMicResponseBean);
//                }
//                break;
//            case UI_EVENT_LINK_MIC_STOP:
//                if (iLiveAudience != null) {
//                    iLiveAudience.linkMicStop();
//                }
//                if (mStreamAudienceIn != null) {
//                    mStreamAudienceIn.linkMicStop();
//                }
//                break;
//            case UI_EVENT_AUDIENCE_LINK_MIC_RESPONSE:
//                AudienceLinkMicResponseBean audienceLinkMicResponseBean = (AudienceLinkMicResponseBean) msg.obj;
//                if (mStreamAudienceIn != null) {
//                    mStreamAudienceIn.audienceLinkMicSuccess(audienceLinkMicResponseBean);
//                }
//                if (iLiveAudience != null) {
//                    iLiveAudience.audienceLinkMicSuccess(audienceLinkMicResponseBean);
//                }
//                break;
//            case UI_EVENT_TOP_GIFT:
//                if (iLive != null) {
//                    final TopGiftBean topGiftBean = (TopGiftBean) msg.obj;
//                    iLive.showTopGiftView(topGiftBean);
//                }
//                break;
//
//            /**********************************************以下为多人连麦**************************************************************/
//            case UI_EVENT_INIT_MULTI:
//                if (iLive != null) {
//                    LiveMultiOnCreateBean liveMultiOnCreateBean = (LiveMultiOnCreateBean) msg.obj;
//                    iLive.initMultiAdapter(liveMultiOnCreateBean);
//                }
//                break;
//            case UI_EVENT_REFRESH_SEAT:
//                if (video != null) {
//                    LiveMultiSeatBean seat = (LiveMultiSeatBean) msg.obj;
//                    if (seat != null) {
//                        if (iLive != null) {
//                            iLive.refreshSeat(seat);
//                        }
//                        L.d("LiveWatchMultiSound", " seat.method : " + seat.method);
//                        L.d("LiveWatchMultiSound", " seat.micStatus : " + seat.micStatus);
//                        String myUserId = UserUtils.getMyUserId();
//                        if (myUserId.equals(seat.userId)) {
//                            switch (seat.method) {
//                                case LiveRoomMsgBean.TYPE_METHOD_MUTE:
//                                    if (seat.micStatus.equals(LiveRoomMsgBean.TYPE_MIC_STATUS_ON)) {
//                                        video.muteLocalAudioStream(false);
//                                    } else {
//                                        video.muteLocalAudioStream(true);
//                                    }
//                                    break;
//                                case LiveRoomMsgBean.TYPE_METHOD_ONSEAT:
//
//                                    break;
//                                case LiveRoomMsgBean.TYPE_METHOD_OFFSEAT:
//                                    if (video != null) {
//                                        video.levelChannel();
//                                    }
//                                    break;
//                                default:
//                                    break;
//                            }
//                        }
//                    }
//                }
//                break;
//            case UI_EVENT_GUEST_GIFT:
//                if (iLive != null) {
//                    LiveMultiSeatBean seat1 = (LiveMultiSeatBean) msg.obj;
//                    iLive.guestGift(seat1);
//                }
//                break;
//            case UI_EVENT_START_ENCOUNTER:
//                LiveMultiSeatBean seat2 = (LiveMultiSeatBean) msg.obj;
//                if (seat2 != null) {
//                    if (iLive != null) {
//                        iLive.startEncounter(seat2.leftTime);
//                    }
//                    //开始相遇嘉宾闭麦
//                    if (video != null) {
//                        video.autoOpenLocalAudioStream();
//                    }
//                }
//                break;
//            case UI_EVENT_END_ENCOUNTER:
//                LiveMultiSeatBean seat3 = (LiveMultiSeatBean) msg.obj;
//                if (seat3 != null) {
//                    if (iLive != null) {
//                        iLive.endEncounter(seat3);
//                    }
//                    //结束相遇嘉宾恢复麦
//                    if (video != null) {
//                        video.autoCloseLocalAudioStream();
//                    }
//                }
//                break;
//            case UI_EVENT_ENCOUNTER_SB:
//                break;
//            case UI_EVENT_ENCOUNTER_SPEAKERS:
//                MultiSpeakersBean multiSpeakersBean = (MultiSpeakersBean) msg.obj;
//                if (multiSpeakersBean != null && iLive != null) {
//                    iLive.speakers(multiSpeakersBean);
//                }
//                break;
//            case UI_EVENT_MIC_ADD:
//                LiveMultiSeatBean.CoupleDetail coupleDetail = (LiveMultiSeatBean.CoupleDetail) msg.obj;
//                if (coupleDetail != null && iLive != null) {
//                    iLive.addSortMic(coupleDetail);
//                }
//                break;
//            case UI_EVENT_MIC_DEL:
//                LiveMultiSeatBean liveMultiSeatBean = (LiveMultiSeatBean) msg.obj;
//                if (liveMultiSeatBean != null && iLive != null) {
//                    iLive.removeSortMic(liveMultiSeatBean.userId);
//                }
//                break;
//            case UI_EVENT_MIC_LIST:
//                LiveMultiSeatBean seat6 = (LiveMultiSeatBean) msg.obj;
//                if (seat6 != null && iLive != null) {
//                    iLive.getMicSortList(seat6.list);
//                }
//                break;
//            case UI_EVENT_MIC_ON:
//                if (iLive != null) {
//                    iLive.sortMicOn();
//                }
//                break;
//            case UI_EVENT_MIC_OFF:
//                if (iLive != null) {
//                    iLive.sortMicOff();
//                }
//                break;
//            case UI_EVENT_AUDIENCE_BEAM_AVAILABLE_LEVEL_SIX:
//                if (iLiveAudience != null) {
//                    iLiveAudience.showLevelBelowSix();
//                }
//                break;
//            case UI_EVENT_GUEST_ON_SEAT:
//
//                L.d("LiveWatchMultiSound", "------UI_EVENT_GUEST_ON_SEAT-------");
//                if (video != null) {
//                    intoTime = System.currentTimeMillis();
//                    AgoraBean agoraBean = (AgoraBean) msg.obj;
//                    video.setClientRole(CLIENT_ROLE_BROADCASTER, agoraBean);
//                }
//                break;
//            case UI_EVENT_GUEST_OFF_SEAT:
//
//                L.d("LiveWatchMultiSound", "------UI_EVENT_GUEST_OFF_SEAT-------");
//                if (video != null) {
//                    outTime = System.currentTimeMillis();
//
//                    video.setClientRole(CLIENT_ROLE_AUDIENCE, null);
//                    /**
//                     * 多人连麦上传连麦次数，时长
//                     * */
//                    long onSeatTime = outTime - intoTime;
//
//                    if (onSeatTime > 0) {
//                        onSeatTime = onSeatTime / 1000;
//                    } else {
//                        onSeatTime = 1;
//                    }
//                    GrowingIOUtil.postGuestLinkMic(onSeatTime);
//                }
//                break;
//            case UI_EVENT_MIC_AGREE:
//
//                L.d("LiveWatchMultiSound", "------UI_EVENT_MIC_AGREE-------");
//                if (video != null) {
//                    LiveMultiSeatBean lmsb = (LiveMultiSeatBean) msg.obj;
//                    video.setClientRole(CLIENT_ROLE_BROADCASTER, lmsb.agora);
//                }
//                break;
//            /**********************************************以上为多人连麦**************************************************************/
//            case UI_EVENT_GIFT_SEND_SUCCESS_MSG:
//                break;
//            case UI_EVENT_FREE_BARRAGE:
//                if (iLive != null) {
//                    iLive.freeBarrage();
//                }
//                break;
//            case UI_EVENT_GUEST_ONSEAR_AUTH:
//                if (iLiveAudience != null) {
//                    iLiveAudience.onSeatAuth();
//                }
//                break;
//            case UI_EVENT_FREE_GOLD:
//                if (iLiveAudience != null) {
//                    LiveFreeGoldReeponceBean liveFreeGoldReeponceBean = (LiveFreeGoldReeponceBean) msg.obj;
//                    iLiveAudience.showFreeGoldView(liveFreeGoldReeponceBean.gold);
//                }
//                break;
//            case UI_EVENT_DAILY_LINK_MIC:
//                if (iLive != null) {
//                    LiveRoomMsgConnectMicBean liveRoomMsgConnectMicBean = (LiveRoomMsgConnectMicBean) msg.obj;
//                    iLive.showConnectMicDialog(liveRoomMsgConnectMicBean);
//                }
//                break;
//            case UI_EVENT_LINK_MIC_IS_ACCEPT://连麦请求是否被对方同意 yes : 同意 no : 不同意
//                String result2 = (String) msg.obj;
//                Bundle bundle1 = msg.getData();
//                String payload1 = bundle1.getString("payload");
//                AgoraBean agoraBean1 = GsonUtils.getObject(payload1, AgoraBean.class);
//                AudienceLinkMicResponseBean audienceLinkMicResponseBean1 = new AudienceLinkMicResponseBean();
//                audienceLinkMicResponseBean1.agora = agoraBean1;
//                if (result2.equals("yes")) {
//                    if (iLive != null) {
//                        iLive.linkMicAccept();
//                    }
//                    if (iLiveAudience != null) {
//                        iLiveAudience.audienceLinkMicSuccess(audienceLinkMicResponseBean1);
//                    }
//                    if (iLiveAudience != null) {
//                        mStreamAudienceIn.audienceLinkMicSuccess(audienceLinkMicResponseBean1);
//                    }
//                } else {
//                    if (iLive != null) {
//                        iLive.lingMicCancel();
//                    }
//                }
//                break;
//            case UI_EVENT_LINK_MIC_CANCEL:
//                if (iLive != null) {
//                    iLive.lingMicCancel();
//                }
//                break;
//            case UI_EVENT_TOP_TODAY:
//                if (iLive != null) {
//                    TopTodayBean topTodayBean = (TopTodayBean) msg.obj;
//                    iLive.topToday(topTodayBean);
//                }
//                break;
//            case UI_EVENT_LINK_MIC_HANGUP:
//                if (iLiveAudience != null) {
//                    LiveRoomMsgConnectMicBean lrmcm = (LiveRoomMsgConnectMicBean) msg.obj;
//                    iLiveAudience.linkMicHangup(lrmcm);
//                }
//                break;
//            case UI_EVENT_REFRESH_ROOM:
//                if (iLiveAudience != null) {
//                    iLiveAudience.refreshLiveRoom();
//                }
//                break;
//
//            /**===============================直播模块代码=========================**/
//
//            case UI_EVENT_ANCHOR_UPDATE_AUDIENCE_COUNT:
//                if (iLive != null) {
//                    ResponseGemBean responseGemBean = (ResponseGemBean) msg.obj;
//                    iLive.updateAudienceCount(responseGemBean);
//                }
//                break;
//            case UI_EVENT_ANCHOR_REFRESH_MSGS:
//                if (iLive != null) {
//                    iLive.refreshMsg();
//                }
//                break;
//            case UI_EVENT_ANCHOR_SCROLL_TO_BOTTOM:
//                if (iLive != null) {
//                    iLive.smoothScrollToBottom();
//                }
//                break;
//            case UI_EVENT_ANCHOR_RESET_IS_BOTTOM:
//                if (iLiveAnchor != null) {
//                    iLiveAnchor.resetIsBottom();
//                }
//                break;
//            case UI_EVENT_ANCHOR_AUTO_PING:
//                client.autoPingServer();
//                break;
//            case UI_EVENT_ANCHOR_UPDATE_MY_NET_STATUS_BAD:
//                if (iLive != null) {
//                    iLive.updateMyNetStatusBad();
//                }
//                break;
//            case UI_EVENT_ANCHOR_STOP_SHOW:
//                if (iLiveAnchor != null) {
//                    iLiveAnchor.showCloseAlert();
//                }
//                break;
//            case UI_EVENT_ANCHOR_UPDATE_MY_NET_STATUS_GOOD:
//                if (iLiveAnchor != null) {
//                    iLiveAnchor.networkGood();
//                }
//                break;
//            case UI_EVENT_ANCHOR_LIVE_CONNECT_BREAK://连接中断
//                if (iLiveAnchor != null) {
//                    iLiveAnchor.connectBreak();
//                }
//                break;
//            case UI_EVENT_ANCHOR_LIVE_CONNECT_FAILED://连接失败
//                if (iLiveAnchor != null) {
//                    iLiveAnchor.connectFailed();
//                }
//                break;
//            case UI_EVENT_ANCHOR_LIVE_CONNECTING://连接中
//                iLiveAnchor.connectiong();
//                break;
//            case UI_EVENT_ANCHOR_LIVE_CONNECT_SUCCESS://连接成功
//                iLiveAnchor.connectSuccess();
//                break;
//            case UI_EVENT_GIFT_RECEIVE_MSG://收到 接收到礼物广播
//                Bundle bundle = msg.getData();
//                String payload = bundle.getString("payload");
//                String code = bundle.getString("code");
//                view.parseGiftMsg(payload, code);
//                break;
//            case UI_EVENT_DANMU_RECEIVE_MSG://弹幕
//                String danMsg = (String) msg.obj;
//                view.showDanmu(danMsg);
//                break;
//            case UI_EVENT_JOIN_USER://有人加入房间
//                final LiveRoomMsgBean liveRoomMsgBean = (LiveRoomMsgBean) msg.obj;
//                view.joinUser(liveRoomMsgBean);
//                break;
//            case UI_EVENT_JOIN_VIP_USER://有人加入房间
//                final LiveRoomMsgBean liveRoomMsgBean2 = (LiveRoomMsgBean) msg.obj;
//                view.joinVipUser(liveRoomMsgBean2);
//                break;
//            case UI_CLOSE_DIALOG:
//                final String ob = (String) msg.obj;
//                view.refreshCloseDialog(ob);
//                break;
//            case UI_EVENT_PROGRESSBAR_GONE://progress_bar 为gone ，七牛sdk
//                view.progressbarGone();
//                break;
//            case UI_EVENT_FINISH:
//                if (stream != null) {
//                    stream.finishStream();
//                }
//                break;
//            case UI_EVENT_LIVE_DISCONNECTED:
//                view.liveDisconnected();
//                break;
//            case UI_EVENT_LIVE_NETWORK_WARNING_NOTIFY:
//                final String text = (String) msg.obj;
//                view.liveNetworkWaringNotify(text);
//                break;
//            case UI_EVENT_LIVE_STREAM_FRAME_SEND_SLOW:
//                view.networkSlow();
//                break;
//            case UI_EVENT_CLEAR_INPUT:
//                view.clearInput();
//                break;
//            case UI_EVENT_LOCK_INPUT:
//                view.lockInput();
//
//                break;
//            case UI_EVENT_OPEN_INPUT_TRUE:
//                String ms = (String) msg.obj;
//                view.setDanmuResult(ms);//弹幕返回结果
//                break;
//            case UI_EVENT_OPEN_INPUT_FALSE:
//                view.openInput(false);
//                break;
//            /***********************************************一下为Pk**************************************************************/
//            case UI_EVENT_PK_REQUEST_NOTICE://收到PK申请通知
//                final LivePkRequestNoticeBean pkRequestNoticeBean = (LivePkRequestNoticeBean) msg.obj;
//                view.showPkRequestView(pkRequestNoticeBean);
//                stream.showPkRequest(pkRequestNoticeBean);
//                break;
//            case UI_EVENT_PK_RESPONSE_NOTICE://收到PK回复通知
//                final LivePkResponseNoticeBean pkResponseNoticeBean = (LivePkResponseNoticeBean) msg.obj;
//                view.showPkResponseView(pkResponseNoticeBean);
//                stream.showPkResponse(pkResponseNoticeBean);
//                break;
//            case UI_EVENT_PK_START_NOTICE://PK开始消息
//                final LivePkStartNoticeBean pkStartNoticeBean = (LivePkStartNoticeBean) msg.obj;
//                view.showPkStartView(pkStartNoticeBean);
//                stream.showPkStart(pkStartNoticeBean);
//                break;
//            case UI_EVENT_PK_CANCEL_NOTICE://收到PK取消通知
//                final String pkCancelPayload = (String) msg.obj;
//                view.showPkCancelView(pkCancelPayload);
//                stream.showPkCancel(pkCancelPayload);
//                break;
//            case UI_EVENT_PK_HANGUP_NOTICE://收到PK挂断 通知
//                final LivePkHangupBean pkHangupBean = (LivePkHangupBean) msg.obj;
//                view.showPkHangupView(pkHangupBean);
//                stream.showPkHangup(pkHangupBean);
//                break;
//            case UI_EVENT_PK_SUMMARY_NOTICE://Pk总结阶段 通知
//                final LivePkSummaryNoticeBean pkSummaryNoticeBean = (LivePkSummaryNoticeBean) msg.obj;
//                view.showPkSummaryView(pkSummaryNoticeBean);
//                stream.showPkSummary(pkSummaryNoticeBean);
//                break;
//            case UI_EVENT_PK_STOP_NOTICE://收到PK结束 通知
//                final String pkStopPayload = (String) msg.obj;
//                view.showPkStopView(pkStopPayload);
//                stream.showPkStop(pkStopPayload);
//                break;
//            case UI_EVENT_PK_GEM_NOTICE://主播收到礼物时 服务器发送主播软妹币增量通知给双方直播室内的所有人
//                final LivePkGemNoticeBean pkGemNoticeBean = (LivePkGemNoticeBean) msg.obj;
//                view.showPkGemView(pkGemNoticeBean);
//                break;
//            case UI_EVENT_PK_REQUEST_PK_OK://发送PK申请成功
//                break;
//            case UI_EVENT_PK_NOT_LIVING://主播不在直播
//                view.showPkRequestErrorView(LiveRoomMsgBean.TYPE_LIVE_PK_RESPONSE_CODE_NOT_LIVING);
//                break;
//            case UI_EVENT_PK_IN_LINKMIC://主播连麦中
//                view.showPkRequestErrorView(LiveRoomMsgBean.TYPE_LIVE_PK_RESPONSE_CODE_IN_LINKMIC);
//                break;
//            case UI_EVENT_PK_IN_PK://主播Pk中
//                view.showPkRequestErrorView(LiveRoomMsgBean.TYPE_LIVE_PK_RESPONSE_CODE_IN_PK);
//                break;
//            case UI_EVENT_PK_REQUEST_REJECT://上次发起PK被该主播拒绝，需等待10分钟才能对该主播再次发起PK
//                view.showPkRequestErrorView(LiveRoomMsgBean.TYPE_LIVE_PK_RESPONSE_CODE_REJECT);
//                break;
//            case UI_EVENT_PK_LOWER_VERSION://过低的版本
//                view.showPkRequestErrorView(LiveRoomMsgBean.TYPE_LIVE_PK_RESPONSE_CODE_LOW_CLI_VER);
//                break;
//            case UI_EVENT_PK_CODE_500://服务端错误
//                view.showPkRequestErrorView(LiveRoomMsgBean.TYPE_LIVE_PK_RESPONSE_CODE_500);
//                break;
//            case UI_EVENT_PK_INIT_MSG://pk断线重连信息
//                final LivePkInitBean livePkInitBean = (LivePkInitBean) msg.obj;
//                view.showLivePkInitView(livePkInitBean);
//                stream.showPkInitStream(livePkInitBean);
//                break;
//            case UI_EVENT_PK_REQUEST_TIME_OUT://Pk请求超时超时
//                final String userId = (String) msg.obj;
//                view.showPkRequestTimeOutView(userId);
//                break;
//            /***********************************************以上为Pk**************************************************************/
//
//            case UI_EVENT_LINK_MIC_REQUEST://接收到连麦请求
//                LiveRoomMsgConnectMicBean liveRoomMsgConnectMicBean = (LiveRoomMsgConnectMicBean) msg.obj;
//                view.showConnectMicDialog(liveRoomMsgConnectMicBean);
//                if (liveRoomMsgConnectMicBean != null && liveRoomMsgConnectMicBean.method != null) {
//
//                    if (liveRoomMsgConnectMicBean.method.equals("start")) { //连麦开始
//                        view.showLinkMicLayer(liveRoomMsgConnectMicBean.getNickName(), liveRoomMsgConnectMicBean.userId);
//                        stream.linkMickStart();
//                    }
//
//                    if (liveRoomMsgConnectMicBean.method.equals("request")) {//发起连麦请求
//                        stream.linkMicRequest(liveRoomMsgConnectMicBean);
//                    }
//
//                    if (liveRoomMsgConnectMicBean.method.equals("response")) {
//                        String result = liveRoomMsgConnectMicBean.result;
//
//                        if (result != null && result.equals("no")) {//请求被拒绝
//                            view.lingMicCancel();
////                            LivePkUtils.saveOneLinkMicRefuser(liveRoomMsgConnectMicBean.toUserId);
//
//                            L.d("LiveShowObserver", " liveRoomMsgConnectMicBean.fromUserId : " + liveRoomMsgConnectMicBean.fromUserId);
//
//                            L.d("LiveShowObserver", " liveRoomMsgConnectMicBean.toUserId : " + liveRoomMsgConnectMicBean.toUserId);
//
//                            LinkMicOrPKRefuseUtils.addLinkMicRefuse(liveRoomMsgConnectMicBean.fromUserId);
//                        } else {     //请求被同意
//                            stream.linkMicResponse(liveRoomMsgConnectMicBean);
//                        }
//
//                    }
//
//                    if (liveRoomMsgConnectMicBean.method.equals("stop")) {
//                        stream.linkMicStop();
//                        view.linkMicHangup();
//                    }
//
//                    if (liveRoomMsgConnectMicBean.method.equals("hangup")) {
//                        stream.linkMicHangup();
//                        view.linkMicHangup();
//                    }
//
//                    L.d("videoLinkAdd", "-----UI_EVENT_LINK_MIC_REQUEST-----");
//
//                    if (liveRoomMsgConnectMicBean.method.equals("videoLinkDel")) {
//                        LivePkFriendBean addAudience = new LivePkFriendBean();
//                        addAudience.avatar = liveRoomMsgConnectMicBean.avatar;
//                        addAudience.nickName = liveRoomMsgConnectMicBean.getNickName();
//                        addAudience.id = liveRoomMsgConnectMicBean.userId;
//                        view.videoLinkDel(addAudience);
//                    }
//
//                    if (liveRoomMsgConnectMicBean.method.equals("videoLinkAdd")) {
//
//                        L.d("videoLinkAdd", " method " + liveRoomMsgConnectMicBean.method);
//
//                        LivePkFriendBean delAudience = new LivePkFriendBean();
//                        delAudience.avatar = liveRoomMsgConnectMicBean.avatar;
//                        delAudience.nickName = liveRoomMsgConnectMicBean.getNickName();
//                        delAudience.id = liveRoomMsgConnectMicBean.userId;
//                        view.videoLinkAdd(delAudience);
//                    }
//                }
//                break;
//            case UI_EVENT_LINK_MIC_RESPONSE:
//                Bundle data = msg.getData();
//                boolean dailyGuard = data.getBoolean("dailyGuard");
//                String toUserId = data.getString("toUserId");
//                String nickName = data.getString("nickName");
//                String avatar = data.getString("avatar");
//                view.showLinkMicSendSuccess(toUserId, nickName, avatar, dailyGuard);
//                break;
//            case UI_EVENT_LINK_MIC_IS_ACCEPT://连麦请求是否被对方同意 yes : 同意 no : 不同意
//                String result = (String) msg.obj;
//                if (result.equals("yes")) {
//                    view.linkMicAccept();
//                } else {
//                    view.lingMicCancel();
//                }
//                break;
//            case UI_EVENT_TOP_GIFT:
//                final TopGiftBean topGiftBean = (TopGiftBean) msg.obj;
//                view.showTopGiftView(topGiftBean);
//                break;
//            /**********************************************以下为多人连麦**************************************************************/
//            case UI_EVENT_INIT_MULTI:
//                LiveMultiOnCreateBean liveMultiOnCreateBean = (LiveMultiOnCreateBean) msg.obj;
//                view.initMultiAdapter(liveMultiOnCreateBean);
//                break;
//            case UI_EVENT_REFRESH_SEAT:
//                LiveMultiSeatBean seat = (LiveMultiSeatBean) msg.obj;
//                view.refreshSeat(seat);
//                break;
//            case UI_EVENT_GUEST_GIFT:
//                LiveMultiSeatBean seat1 = (LiveMultiSeatBean) msg.obj;
//                view.guestGift(seat1);
//                break;
//            case UI_EVENT_START_ENCOUNTER:
//                LiveMultiSeatBean seat2 = (LiveMultiSeatBean) msg.obj;
//                if (seat2 != null) {
//                    view.startEncounter(seat2.leftTime);
//                }
//                break;
//            case UI_EVENT_END_ENCOUNTER:
//                LiveMultiSeatBean seat3 = (LiveMultiSeatBean) msg.obj;
//                if (seat3 != null) {
//                    view.endEncounter(seat3);
//                }
//                break;
//            case UI_EVENT_ENCOUNTER_SB:
//                break;
//            case UI_EVENT_ENCOUNTER_SPEAKERS:
//                MultiSpeakersBean multiSpeakersBean = (MultiSpeakersBean) msg.obj;
//                if (multiSpeakersBean != null) {
//                    view.speakers(multiSpeakersBean);
//                }
//                break;
//            case UI_EVENT_MIC_ADD:
//                LiveMultiSeatBean.CoupleDetail coupleDetail = (LiveMultiSeatBean.CoupleDetail) msg.obj;
//                if (coupleDetail != null) {
//                    view.addSortMic(coupleDetail);
//                }
//                break;
//            case UI_EVENT_MIC_DEL:
//                LiveMultiSeatBean liveMultiSeatBean = (LiveMultiSeatBean) msg.obj;
//                if (liveMultiSeatBean != null) {
//                    view.removeSortMic(liveMultiSeatBean.userId);
//                }
//                break;
//            case UI_EVENT_MIC_LIST:
//                LiveMultiSeatBean seat6 = (LiveMultiSeatBean) msg.obj;
//                if (seat6 != null) {
//                    view.getMicSortList(seat6.list);
//                }
//                break;
//            case UI_EVENT_FREE_BARRAGE:
//                view.freeBarrage();
//                break;
//
//            /**********************************************以上为多人连麦**************************************************************/
//
//            case UI_EVENT_NO_FACE:
//                boolean visible = (boolean) msg.obj;
//                view.onFaceDetection(visible);
//                break;
//            case UI_EVENT_GET_TOP_TODAY_LIST:
//                List<LivePkFriendBean> list = (List<LivePkFriendBean>) msg.obj;
//                view.getTopFansTodayList(list);
//                break;
//            case UI_EVENT_TOP_TODAY:
//                TopTodayBean topTodayBean = (TopTodayBean) msg.obj;
//                view.topToday(topTodayBean);
//                break;
//            case UI_EVENT_ANDIENCE_NOT_IN:
//                view.andienceNotIn();
//                break;
//            case UI_EVENT_REJECT_LINK_MIC:
//                RejectMicBean rejectMicBean = (RejectMicBean) msg.obj;
//                view.rejectMic(rejectMicBean);
//                break;
//            default:
//                break;
//        }
    }

    public void onDestroy() {

        L.d(TAG, " ------onDestroy--------- ");

        destoryed = false;
        removeCallbacksAndMessages(null);
        video = null;
        iLive = null;
        iLiveAudience = null;
        iLiveAnchor = null;
        client = null;
    }

    /*********************通信方法**************************************/

    public void sendSoftGiftBean(SoftGiftBean softGiftBean, int id, String type, int comb, String hostUserId) {
        if (client != null) {
            client.sendSoftGift(softGiftBean, id, type, comb, hostUserId);
        }
    }

    public void sendInputMsg(String msg) {
        if (client != null) {
            client.sendMsg(msg);
        }
    }

    public void sendDanmu(String userLevel, String content, String barrageId) {
        if (client != null) {
            client.sendDanmu(userLevel, content, barrageId);
        }
    }

    /**
     * 关注主播后，新增主播粉丝
     */
    public void addFollowFans() {
        if (client != null) {
            client.addFollowFans();
        }
    }

    /**
     * 分享主播 向服务端发消息
     */
    public void shareToLive() {
        if (client != null) {
            client.shareToLive();
        }
    }

    /**
     * 推荐主播，向服务端发消息
     */
    public void recommendLive() {
        if (client != null) {
            client.recommendLive();
        }
    }

    public void addMsg(LiveRoomMsgBean liveRoomMsgBean) {

        L.d(TAG, " addMsg iLive : " + iLive);

        if (iLive != null) {
            iLive.addLiveRoomMsg(liveRoomMsgBean);
        }
    }

    /**
     * 获取截屏路径
     *
     * @return
     */
    public String getScreenShotImagePath() {
        if (video != null) {
            return video.getScreenShotImagePath();
        }
        return "";
    }

    public String getPlayUrl() {
        if (video != null) {
            return video.getCurrentPlayUrl();
        }
        return "";
    }

    @Override
    public void banHer(@NotNull String userId) {
        if (iChatRoomRequest != null) {
            iChatRoomRequest.banHer(userId);
        }
    }

    @Override
    public void getMicSortList() {
        if (iChatRoomRequest != null) {
            iChatRoomRequest.getMicSortList();
        }
    }

    @Override
    public void openMicSort() {
        if (iChatRoomRequest != null) {
            iChatRoomRequest.openMicSort();
        }
    }

    @Override
    public void closeMicSort() {
        if (iChatRoomRequest != null) {
            iChatRoomRequest.closeMicSort();
        }
    }

    @Override
    public void agreeOnSeat(@NotNull String userId, @NotNull AgoraBean agora) {
        if (iChatRoomRequest != null) {
            iChatRoomRequest.agreeOnSeat(userId, agora);
        }
    }

    @Override
    public void startEncounter() {
        if (iChatRoomRequest != null) {
            iChatRoomRequest.startEncounter();
        }
    }

    @Override
    public void sendPkRequestMsg(@NotNull String userId, float x, float y, float width, float height) {
        if (iChatRoomRequest != null) {
            iChatRoomRequest.sendPkRequestMsg(userId, x, y, width, height);
        }
    }

    @Override
    public void connectMic(@NotNull String method, @NotNull String toUserId, float x, float y, float height, float width, @NotNull String nickName, @NotNull String avatar, boolean dailyGuard) {
        if (iChatRoomRequest != null) {
            iChatRoomRequest.connectMic(method, toUserId, x, y, height, width, nickName, avatar, dailyGuard);
        }
    }

    @Override
    public void responseAudienceLinkMic(@NotNull String method, @NotNull String body) {
        if (iChatRoomRequest != null) {
            iChatRoomRequest.responseAudienceLinkMic(method, body);
        }
    }

    @Override
    public void getTopFansTodayList() {
        if (iChatRoomRequest != null) {
            iChatRoomRequest.getTopFansTodayList();
        }
    }

    @Override
    public void exucuteColseRunable() {
        if (iChatRoomRequest != null) {
            iChatRoomRequest.exucuteColseRunable();
        }
    }

    @Override
    public void sendColseMsg() {
        if (iChatRoomRequest != null) {
            iChatRoomRequest.sendColseMsg();
        }
    }

    @Override
    public void arGiftReceipt(@NotNull String payload) {
        if (iChatRoomRequest != null) {
            iChatRoomRequest.arGiftReceipt(payload);
        }
    }

    @Override
    public void sendPkRequestCancelMsg(@NotNull String id) {
        if (iChatRoomRequest != null) {
            iChatRoomRequest.sendPkRequestCancelMsg(id);
        }
    }

    @Override
    public void sendPkResponseMsg(@NotNull String toUserId, @NotNull String result, float x, float y, float width, float height) {
        if (iChatRoomRequest != null) {
            iChatRoomRequest.sendPkResponseMsg(toUserId, result, x, y, width, height);
        }
    }

    @Override
    public void sendPkHangUpMsg(@NotNull String userId) {
        if (iChatRoomRequest != null) {
            iChatRoomRequest.sendPkHangUpMsg(userId);
        }
    }

    @Override
    public void cancelConnectMic(@NotNull String method, @NotNull String toUserId) {
        if (iChatRoomRequest != null) {
            iChatRoomRequest.cancelConnectMic(method, toUserId);
        }
    }

    @Override public void uploadAudioVolume(@NotNull IRtcEngineEventHandler.AudioVolumeInfo[] speakers) {
        if (iChatRoomRequest != null) {
            iChatRoomRequest.uploadAudioVolume(speakers);
        }
    }

    /************************4.8.0start******************************/
    /**
     * 请求上麦
     *
     * @param position
     */
    public void onSeat(int position) {
        if (client != null) {
            client.onSeat(position);
        }
    }

    /**
     * 请求下麦
     *
     * @param position
     */
    public void offSeat(int position) {
        if (client != null) {
            client.offSeat(position);
        }
    }

    /**
     * 开麦/闭麦
     *
     * @param position
     * @param micStatus
     */
    public void mute(int position, String micStatus) {
        if (client != null) {
            client.mute(position, micStatus);
        }
    }

    public void guestGift(int giftId, int combo, int seatNum) {
        if (client != null) {
            client.guestGift(giftId, combo, seatNum);
        }
    }

    /**
     * 静音/取消静音。该方法用于允许/禁止往网络发送本地音频流。
     *
     * @param mute
     */
    public void muteLocalAudioStream(boolean mute) {
        video.muteLocalAudioStream(mute);
    }

    /**
     * 点击配对某人
     */
    public void encounterSb(int seatNum) {
        client.encounterSb(seatNum);
    }

    /**
     * 请求上麦
     */
    public void requestSortMic() {
        client.requestSortMic();
    }

    /**
     * 取消上麦
     */
    public void cancelSortMic() {
        client.cancelSortMic();
    }

    /**
     * 获取排麦列表
     */
    public void requestMicList() {
        client.requestMicList();
    }
    /************************4.8.0end******************************/

    /**
     * 观众发起连麦请求
     */
    public void requestLinkMicByAudience() {

        L.d("", " requestLinkMicByAudience client : " + client);

        client.requestLinkMicByAudience();
    }

    public void cancelLinkMicByAudience() {
        client.cancelLinkMIcByAudience();
    }

    public void linkMicHangup(String code, String toUserId) {
        client.linkMicHangup(code, toUserId);
        mStreamAudienceIn.hangup();
    }

    public void close(String code, String toUserId) {
        client.linkMicHangup(code, toUserId);
    }

    public void requestLinkMicAudienceList() {
        client.requestLinkMicAudienceList();
    }


    public void levelChannel() {

        L.d("LiveWatchMultiSound", "------ observer levelChannel------");

        if (video != null) {
            video.levelChannel();
        }
    }

    /***
     * 上报礼物播放 给后台
     *
     * @param userId
     * @param toUserId
     * @param id*/
    public void postGiftPlayStatus(String userId, String toUserId, int id) {
        client.postGiftplayStatus(userId, toUserId, id);
    }

    public void guestLeaveRoom() {
        client.guestLeaveRoom();
    }

    public void acceptFreeGold() {
        client.acceptFreeGold();
    }

    public void rejectFreeGold() {
        client.rejectFreeGold();
    }

    public void responseConnectMic(String method, String toUserId, String result, float x, float y, float height, float width) {
        if (iConnectMic != null) {
            iConnectMic.rejectConnectMic(method, toUserId, result, x, y, height, width);
        }
    }

    public void hangupConnectMic(String method, String toUserId) {
        if (iConnectMic != null) {
            iConnectMic.hangupConnectMic(method, toUserId);
        }
        if (mStreamAudienceIn != null) {

            L.d(TAG, " hangupConnectMic toUserId : " + toUserId);

            mStreamAudienceIn.hangupByOwn();
        }

    }

    public String getToken() {
        if (client != null) {
            return client.getToken();
        }
        return null;
    }

    public void switchStricker(SoftGiftBean giftBean) {
        if (liveShowCaptureStreamIn != null) {
            liveShowCaptureStreamIn.switchStricker(giftBean);
        }
    }

    public LiveStatus getLiveStatus() {
        if (liveShowCaptureStreamIn != null) {
            return liveShowCaptureStreamIn.getListStatus();
        }
        return null;
    }

    /**
     * 对方申请PK自己同意时，要立刻显示PK界面
     */
    public void showPkStream() {
        if (liveShowCaptureStreamIn != null) {
            liveShowCaptureStreamIn.showPkStream();
        }
    }

    /**
     * 停止Pk
     */
    public void stopPkStream() {
        if (liveShowCaptureStreamIn != null) {
            liveShowCaptureStreamIn.finishPk();
        }
    }

    /**
     * 播放音效
     *
     * @param position
     */
    public void playEffect(int position) {
        if (liveShowCaptureStreamIn != null) {
            liveShowCaptureStreamIn.playEffect(position);
        }
    }

    /**
     * 调整美颜参数
     *
     * @param key
     * @param progress
     */
    public void adjustBeauty(String key, float progress) {
        if (liveShowCaptureStreamIn != null) {
            liveShowCaptureStreamIn.adjustBeauty(key, progress);
        }
    }

    public void switchCamera() {
        if (liveShowCaptureStreamIn != null) {
            liveShowCaptureStreamIn.switchCamera();
        }
    }

}
