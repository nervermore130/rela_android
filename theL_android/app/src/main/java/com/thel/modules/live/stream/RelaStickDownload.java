package com.thel.modules.live.stream;

import android.text.TextUtils;
import android.util.Log;

import com.liulishuo.filedownloader.BaseDownloadTask;
import com.liulishuo.filedownloader.FileDownloadListener;
import com.liulishuo.filedownloader.FileDownloader;
import com.thel.BuildConfig;
import com.thel.constants.TheLConstants;
import com.thel.modules.live.bean.ARGiftNetBean;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.service.DefaultRequestService;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;

import org.lasque.tusdk.core.TuSdk;
import org.lasque.tusdk.core.TuSdkBundle;
import org.lasque.tusdk.core.TuSdkContext;
import org.lasque.tusdk.core.utils.json.JsonHelper;
import org.lasque.tusdk.modules.view.widget.sticker.StickerGroup;
import org.lasque.tusdk.modules.view.widget.sticker.StickerLocalPackage;

import java.io.File;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by chad
 * Time 19/1/21
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */
public class RelaStickDownload {

    //1250 1319
    public static String TAG = "RelaStickDownload";

    public void download() {

        //没有直播权限不下载
        if (ShareFileUtils.getInt(TheLConstants.live_permit, 0) == 0) return;

        final Flowable<ARGiftNetBean> flowable = DefaultRequestService.createLiveRequestService().getARGiftList(BuildConfig.APPLICATION_ID);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<ARGiftNetBean>() {
            @Override
            public void onNext(ARGiftNetBean data) {

                L.d(TAG, "data :" + data);

                if (data != null && data.data != null) {

                    L.d(TAG, " prepareSingleLocal data.data.arGiftMasterKey : " + data.data.arGiftMasterKey);

                    ShareFileUtils.setString(ShareFileUtils.TU_SDK_MASTER_KEY, data.data.arGiftMasterKey);

                    String json = GsonUtils.createJsonString(data);
                    ShareFileUtils.setString(ShareFileUtils.AR_GIFT_LIST, json);

                    if (data.data.list != null) {
                        try {
                            //清除所有的下载任务
                            FileDownloader.getImpl().clearAllTaskData();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        for (ARGiftNetBean.ARGiftBean model : data.data.list) {

                            String url = model.arResource;
                            if (!TextUtils.isEmpty(url)) {
                                String fileName = url.substring(url.lastIndexOf("/") + 1);
                                String path = TheLConstants.F_AR_GIFT_ROOTPATH + fileName;
                                File file = new File(path);

                                if (file.exists()) {
                                    prepareSingleLocalSticker(fileName);
                                } else {
                                    FileDownloader.getImpl().create(url)
                                            .setPath(path)
                                            .setWifiRequired(true)//wifi下才能下载
                                            .setForceReDownload(true)
                                            .setCallbackProgressTimes(0)
                                            .setListener(mQueueTarget)
                                            .asInQueueTask()
                                            .enqueue();
                                }
                            }
                        }

                        //并行执行该队列
                        FileDownloader.getImpl().start(mQueueTarget, false);
                    }
                }
            }
        });
    }

    public static boolean prepareSingleLocalSticker(String stickerFileName) {

        try {
            /**
             * step 1. 准备设置 Master 信息
             * 每次从 TUTU 控制台打包资源后，在 lsq_tusdk_configs.json 文件中可以获取到 master 信息。
             */
//            String asset = TuSdkBundle.sdkBundleOther(TuSdk.SDK_CONFIGS);
//            String json = TuSdkContext.getAssetsText(asset);
//            String localMaster = JsonHelper.json(json).getString("master");

            String master = ShareFileUtils.getString(ShareFileUtils.TU_SDK_MASTER_KEY, "");

            if (master.isEmpty()) {
                return false;
            }

            /**
             *
             * step 2. 将下载后的贴纸加入 TuSDKPFStickerLocalPackage.
             *        将本地贴纸加入 TuSDKPFStickerLocalPackage 后，将负责解析并生成 TuSDKPFStickerGroup 对象。
             */
            File stickerDir = new File(TheLConstants.F_AR_GIFT_ROOTPATH);

            if (!stickerDir.exists()) {
                return false;
            }

            // 解析该文件贴纸id (开发者可自己做对照表，这里根据文件名解析id)
            String groupId = stickerFileName.substring(stickerFileName.lastIndexOf("_") + 1, stickerFileName.lastIndexOf("."));

            File stickerFile = new File(stickerDir, stickerFileName);

            StickerLocalPackage.shared().removeDownloadWithIdt(Long.parseLong(groupId));

            //  将下载后的贴纸加入 TuSDKPFStickerLocalPackage
            boolean result = StickerLocalPackage.shared().addStickerGroupFile(stickerFile, Long.parseLong(groupId), master);


            L.d(TAG, "prepareSingleLocalSticker result : " + result);

            if (!result)
                Log.i(TAG, "isContain: " + StickerLocalPackage.shared().containsGroupId(Long.parseLong(groupId)));
            return result;

        } catch (Exception e) {
            e.printStackTrace();
        }

        /**
         * 延伸需求 : 如果开发者要移除指定的贴纸，可通过如下方法实现
         * 注意： TuSDKPFStickerLocalPackage 不负责移除物理贴纸文件，只是移除对贴纸的管理。
         */
        // [[TuSDKPFStickerLocalPackage package] removeDownloadWithIdt:1432];


        /**
         * step 3. 通过 addStickerGroupFile 加入后， 可以通过 TuSDKPFStickerLocalPackage 读取贴纸数据。
         */

        List<StickerGroup> localList = StickerLocalPackage.shared().getSmartStickerGroups();

        L.d(TAG, "localList :" + localList);

        return false;
    }

    private final FileDownloadListener mQueueTarget = new FileDownloadListener() {
        @Override
        protected void pending(BaseDownloadTask task, int soFarBytes, int totalBytes) {
            L.d(TAG, "pending");
        }

        @Override
        protected void connected(BaseDownloadTask task, String etag, boolean isContinue, int soFarBytes, int totalBytes) {
            L.d(TAG, "connected");
        }

        @Override
        protected void progress(BaseDownloadTask task, int soFarBytes, int totalBytes) {
            L.d(TAG, "progress");
        }

        @Override
        protected void blockComplete(BaseDownloadTask task) {
            L.d(TAG, "blockComplete");
        }

        @Override
        protected void retry(final BaseDownloadTask task, final Throwable ex, final int retryingTimes, final int soFarBytes) {
            L.d(TAG, "retry");
        }

        @Override
        protected void completed(BaseDownloadTask task) {
            String fileName = task.getFilename();
            prepareSingleLocalSticker(fileName);
            L.d(TAG, "completed: " + "fileName: " + fileName);
        }

        @Override
        protected void paused(BaseDownloadTask task, int soFarBytes, int totalBytes) {
            L.d(TAG, "paused");
        }

        @Override
        protected void error(BaseDownloadTask task, Throwable e) {
            L.d(TAG, "error: " + Log.getStackTraceString(e));
        }

        @Override
        protected void warn(BaseDownloadTask task) {
            L.d(TAG, "warn");
        }
    };
}
