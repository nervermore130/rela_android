package com.thel.modules.live.bean;

import java.io.Serializable;

/**
 *
 * @author waiarl
 * @date 2017/12/4
 * 收到的Pk请求解析的bean
 */

public class LivePkRequestNoticeBean extends BaseLivePkBean implements Serializable {

    /**
     * 请求Pk的用户id
     */
    public String userId;
    /**
     * 请求PK的用户昵称
     */
    public String nickName;
    /**
     * 请求PK的用户头像
     */
    public String avatar;
    /**
     * pk频道
     */
    public String pkChannel;

    @Override public String toString() {
        return "LivePkRequestNoticeBean{" +
                "userId='" + userId + '\'' +
                ", nickName='" + nickName + '\'' +
                ", avatar='" + avatar + '\'' +
                ", pkChannel='" + pkChannel + '\'' +
                '}';
    }
}
