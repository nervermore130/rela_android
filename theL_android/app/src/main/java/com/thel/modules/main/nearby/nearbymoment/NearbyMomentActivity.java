package com.thel.modules.main.nearby.nearbymoment;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.constants.TheLConstants;
import com.thel.imp.TittleClickListener;
import com.thel.ui.dialog.ReleaseMomentDialog;

/**
 * Created by waiarl on 2017/10/12.
 */

public class NearbyMomentActivity extends BaseActivity {
    private LinearLayout lin_back;
    private TextView txt_title;
    private LinearLayout lin_write_moment;
    private FrameLayout frame;
    private String jumpMomentId;
    private NearbyMomentFragment nearbyMomentFragment;
    private FragmentManager manager;
    private FragmentTransaction tran;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nearby_moment);
        getIntentData();
        findViewById();
        initViewData();
        initFragment();
        setListener();

    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void getIntentData() {
        final Intent intent = getIntent();
        jumpMomentId = intent.getStringExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID);
    }

    private void findViewById() {
        lin_back = findViewById(R.id.lin_back);
        txt_title = findViewById(R.id.txt_title);
        lin_write_moment = findViewById(R.id.lin_write_moment);
        frame = findViewById(R.id.frame);
    }

    private void initViewData() {
        txt_title.setText(getString(R.string.nearby_moment));
    }

    private void initFragment() {
        if (nearbyMomentFragment == null) {
            final Bundle bundle = new Bundle();
            bundle.putString(TheLConstants.BUNDLE_KEY_MOMENT_ID, jumpMomentId);
            nearbyMomentFragment = NearbyMomentFragment.getInstance(bundle);
        }
        manager = getSupportFragmentManager();
        tran = manager.beginTransaction();
        tran.add(R.id.frame, nearbyMomentFragment);
        tran.commit();
    }

    private void setListener() {
        lin_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        lin_write_moment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showWriteMomentDialog();
            }
        });
        txt_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (nearbyMomentFragment instanceof TittleClickListener) {
                    nearbyMomentFragment.onTitleClick();
                }
            }
        });
    }

    /**
     * 写日志
     */
    private void showWriteMomentDialog() {
        ReleaseMomentDialog releaseMomentDialog = new ReleaseMomentDialog(this);
        releaseMomentDialog.show();
    }

}
