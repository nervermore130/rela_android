package com.thel.modules.main.me.aboutMe;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.modules.main.me.adapter.HostFollowListAdapter;
import com.thel.modules.main.me.bean.FriendsBean;
import com.thel.modules.main.me.bean.FriendsListBean;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.utils.ViewUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class HostFollowActivity extends BaseActivity {

    @BindView(R.id.swipe_container)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.listView)
    RecyclerView mRecyclerview;

    @BindView(R.id.emtpy_view)
    LinearLayout emtpy_view;

    @BindView(R.id.img_more)
    ImageView img_more;

    @BindView(R.id.txt_title)
    TextView txt_title;

    @BindView(R.id.lin_more)
    LinearLayout lin_more;

    @BindView(R.id.lin_back)
    LinearLayout lin_back;

    private int cursor = 0;
    public final int REFRESH_TYPE_ALL = 1;
    public final int REFRESH_TYPE_NEXT_PAGE = 2;

    private ArrayList<FriendsBean> listPlus = new ArrayList<>();
    private SwipeRefreshLayout swipe_container;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager manager;
    private HostFollowListAdapter hostFollowListAdapter;
    private boolean haveNextPage;
    private int total;
    private int refreshType;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_host_follow);
        ButterKnife.bind(this);

        initView();
        setListener();
        refreshData();

    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void setListener() {
        lin_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshData();
            }
        });
        hostFollowListAdapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                ViewUtils.preventViewMultipleClick(view, 2000);
                gotoUserInfoActivity(position);
            }
        });
        hostFollowListAdapter.setReloadMoreListener(new BaseRecyclerViewAdapter.ReloadMoreListener() {
            @Override
            public void reloadMore() {
                hostFollowListAdapter.removeAllFooterView();
                hostFollowListAdapter.openLoadMore(true);
                loadNetData(cursor, REFRESH_TYPE_NEXT_PAGE);
            }
        });

        hostFollowListAdapter.setOnLoadMoreListener(new BaseRecyclerViewAdapter.RequestLoadMoreListener() {//加载更多
            @Override
            public void onLoadMoreRequested() {
                mRecyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        if (haveNextPage) {
                            loadNetData(cursor, REFRESH_TYPE_NEXT_PAGE);
                        } else {
                            hostFollowListAdapter.openLoadMore(0, false);
                            if (listPlus.size() > 0) {
                                View view = getLayoutInflater().inflate(R.layout.load_more_footer_layout, (ViewGroup) mRecyclerView.getParent(), false);
                                ((TextView) view.findViewById(R.id.text)).setText(getString(R.string.info_no_more));
                                hostFollowListAdapter.addFooterView(view);
                            }
                        }
                    }
                });
            }
        });
    }

    private void gotoUserInfoActivity(int position) {
        FriendsBean friendsBean = hostFollowListAdapter.getData().get(position);
//        Intent intent = new Intent(this, UserInfoActivity.class);
//        Bundle bundle = new Bundle();
//        bundle.putString(TheLConstants.BUNDLE_KEY_USER_ID, friendsBean.userId);
//        bundle.putString("fromPage", this.getClass().getName());
//        bundle.putString(TheLConstants.BUNDLE_KEY_INTENT_FROM, UserInfoActivity.FROM_TYPE_FOLLOW);
//        intent.putExtras(bundle);
//      //  startActivityForResult(intent, TheLConstants.BUNDLE_CODE_FRIENDS_ACTIVITY);
//        startActivity(intent);
//        FlutterRouterConfig.Companion.gotoUserInfo(friendsBean.userId);
    }

    private void refreshData() {
        loadNetData(0, REFRESH_TYPE_ALL);

    }

    private void initView() {
        swipe_container = findViewById(R.id.swipe_container);
        ViewUtils.initSwipeRefreshLayout(swipe_container);
        mRecyclerView = swipe_container.findViewById(R.id.listView);
        manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setHasFixedSize(true);

        hostFollowListAdapter = new HostFollowListAdapter(listPlus);
        mRecyclerView.setAdapter(hostFollowListAdapter);
        lin_more.setVisibility(View.GONE);
    }

    private void loadNetData(int cursor, int type) {
        refreshType = type;

        Flowable<FriendsListBean> flowable = RequestBusiness.getInstance().getFollowAnchorsList(cursor + "");
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<FriendsListBean>() {
            @Override
            public void onNext(FriendsListBean data) {
                super.onNext(data);
                setFollowAnchors(data);
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                requestFiled();
            }

            @Override
            public void onComplete() {
                super.onComplete();
                requestFinished();
            }
        });
    }

    private void requestFinished() {
        if (swipe_container != null)
            swipe_container.postDelayed(new Runnable() {

                @Override
                public void run() {
                    if (swipe_container != null && swipe_container.isRefreshing())
                        swipe_container.setRefreshing(false);
                }
            }, 1000);
    }

    private void requestFiled() {
        if (refreshType == REFRESH_TYPE_NEXT_PAGE) {

            mRecyclerView.post(new Runnable() {
                @Override
                public void run() {
                    hostFollowListAdapter.loadMoreFailed((ViewGroup) mRecyclerView.getParent());
                }
            });
        }
    }

    private void setFollowAnchors(FriendsListBean followAnchors) {
        if (followAnchors.data == null) {
            return;
        }

        cursor = followAnchors.data.cursor;
        if (REFRESH_TYPE_ALL == refreshType) {
            listPlus.clear();
        }
        haveNextPage = followAnchors.data.haveNextPage;
        total = followAnchors.data.followersTotal;

        listPlus.addAll(followAnchors.data.users);
        txt_title.setText(total + " " + TheLApp.context.getString(R.string.follow_host));
        if (listPlus.size() == 0) {
            setEmptyStatue();
        }

        if (REFRESH_TYPE_ALL == refreshType) {
            hostFollowListAdapter.removeAllFooterView();
            hostFollowListAdapter.setNewData(listPlus);

            if (listPlus.size() > 0) {
                hostFollowListAdapter.openLoadMore(listPlus.size(), true);
            } else {
                hostFollowListAdapter.openLoadMore(listPlus.size(), false);
            }
        } else {
            hostFollowListAdapter.notifyDataChangedAfterLoadMore(true, listPlus.size());

        }

    }

    private void setEmptyStatue() {
        if (total > 0) {
            emtpy_view.setVisibility(View.GONE);

        } else {
            emtpy_view.setVisibility(View.VISIBLE);
        }

    }
}
