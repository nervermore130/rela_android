package com.thel.modules.live.bean;

import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;

/**
 * Created by the L on 2016/5/27.
 * 打赏发送礼物广播消息
 */
public class GiftSendMsgBean {

    public static final int GIFT_TYPE_NORMAL = 0;

    public static final int GIFT_TYPE_GUEST = 1;

    public int type = 0;

    public boolean isMultiConnectMic = false;

    /**
     * 赠送人Id
     */
    public String userId;
    /**
     * 赠送人昵称
     */
    public String nickName;
    /**
     * 赠送人头像
     */
    public String avatar;
    /**
     * 礼物Id
     */
    public int id;
    /**
     * 连击次数
     */
    public int combo;
    /**
     * 礼物名称（从本地缓存中由礼物id获取）
     */
    public String giftName;
    /**
     * 礼物图标，（从本地缓存中有id获取）
     */
    public String icon;
    /**
     * 礼物图片，从本地缓存中获取
     */
    public String img;
    /**
     * 赠送时间（long 1970年至今秒）
     */
    public long time;
    /**
     * 赠送礼物的动作，从本地内存中获取
     */
    public String action;
    /**
     * 0普通礼物,1AR礼物
     */
    public boolean isArGift;
    /**
     * AR礼物Id
     */
    public int arGiftId;
    /**
     * AR礼物回执id
     */
    public long arGiftReceiptId;
    /**
     * 是否是大礼物标识（播放大动画）
     */
    public String mark = "";
    /**
     * 主播的软妹币数量,默认为-1(为-1的时候，服务器无返回，主播软妹币不变)
     */
    public String ownerGem = "-1";
    /**
     * ae动画播放时间
     */
    public int playTime;

    public String videoUrl = null;

    public String toNickname;

    public String toAvatar;

    public String toUserId;

    public int iconSwitch = 1;

    public static final String VIDEO_URL_NULL = "null";

    /**
     * 礼物价格
     */
    public int gold;
    /**
     * 用户等级
     */
    public int userLevel;

    /*  public void fromJson(JSONObject jsonObject) {
          userId = JsonUtils.getString(jsonObject, "userId", "");
          nickName = JsonUtils.getString(jsonObject, "nickName", "");
          avatar = JsonUtils.getString(jsonObject, "avatar", "");
          id = JsonUtils.getInt(jsonObject, "id", 0);
          combo = JsonUtils.getInt(jsonObject, "combo", 0);
          mark = JsonUtils.getString(jsonObject, "resource", "");
          ownerGem = JsonUtils.getString(jsonObject, "ownerGem", "0");
          playTime = JsonUtils.getInt(jsonObject, "playTime", 0);
      }
  */
    public SpannableString buildBigGiftAction() {
        SpannableString sp = new SpannableString(nickName + " " + action);
        final int start1 = 0;
        final int end1 = nickName.length() + 1 + start1;
        sp.setSpan(new StyleSpan(Typeface.BOLD), start1, end1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return sp;
    }

    @Override public String toString() {
        return "GiftSendMsgBean{" +
                "logType=" + type +
                ", isMultiConnectMic=" + isMultiConnectMic +
                ", userId='" + userId + '\'' +
                ", nickName='" + nickName + '\'' +
                ", avatar='" + avatar + '\'' +
                ", id=" + id +
                ", combo=" + combo +
                ", giftName='" + giftName + '\'' +
                ", icon='" + icon + '\'' +
                ", img='" + img + '\'' +
                ", time=" + time +
                ", action='" + action + '\'' +
                ", mark='" + mark + '\'' +
                ", ownerGem='" + ownerGem + '\'' +
                ", playTime=" + playTime +
                ", videoUrl='" + videoUrl + '\'' +
                ", toNickname='" + toNickname + '\'' +
                ", toAvatar='" + toAvatar + '\'' +
                ", toUserId='" + toUserId + '\'' +
                ", gold=" + gold +
                ", userLevel=" + userLevel +
                '}';
    }
}
