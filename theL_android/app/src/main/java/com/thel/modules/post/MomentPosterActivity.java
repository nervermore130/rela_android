package com.thel.modules.post;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;

import androidx.core.content.ContextCompat;

import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.bean.SharePosterBean;
import com.thel.bean.live.LiveMultiSeatBean;
import com.thel.constants.TheLConstants;
import com.thel.data.local.FileHelper;
import com.thel.growingio.GIOShareTrackBean;
import com.thel.growingio.GrowingIoConstant;
import com.thel.manager.ImageLoaderManager;
import com.thel.modules.live.surface.meet.MeetResultActivity;
import com.thel.ui.widget.MeetItemLayout2;
import com.thel.ui.widget.MeetShareItemLayout;
import com.thel.ui.widget.OvalSquareImageView;
import com.thel.ui.widget.SquareRelativeLayout;
import com.thel.utils.CropCircelBorderTransformation;
import com.thel.utils.DateUtils;
import com.thel.utils.DialogUtil;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.ImageUtils;
import com.thel.utils.L;
import com.thel.utils.PermissionUtil;
import com.thel.utils.QRUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.glide.transformations.ColorFilterTransformation;


public class MomentPosterActivity extends BaseActivity implements View.OnClickListener {

    public static final int FROM_MOMENT = 1;//从日志跳转过来 包括话题日志
    public static final int FROM_ADDFRIEND = 2;//从邀请好友跳转过来
    public static final int FROM_COMMENT = 3;//从话题评论跳转过来
    public static final int FROM_PHOTO = 4;
    public static final int FROM_THEME_COMMENT = 5;//新型话题回复，4.1.0

    public static final String URL_NORMAL = "https://s.growingio.com/JDmQXZ";
    public static final String URL_GLOBAL = "https://s.growingio.com/JjvK4A";
    public static final String URL_INVITE_FRIEND = "https://s.growingio.com/JZMb1m";
    public static final String URL_SHARE_MOMENT = "https://s.growingio.com/JYMBmB";
    public static final String URL_SHARE_ENCOUNTER = "https://gio.ren/o32JGN3";
    private final int IMG_QUALITY = 70;
    private final float THUMB_SCALE = 0.3f;//缩略图比例

    private String imgPath;//分享的图片保存的本地路径
    private String zoomPath;//分享的图片缩略图保存的本地路径

    private boolean isDownload = false;

    private View lin_back;
    private OvalSquareImageView img_bg;
    private ImageView img_avatar;
    private TextView txt_nickname;
    private TextView txt_rela_id;
    private TextView txt_content;
    private ImageView btn_wx_circle;
    private ImageView btn_wx;
    private ImageView btn_weibo;
    private ImageView btn_qq;
    private ImageView btn_qzone;
    private ImageView img_download;
    private ImageView btn_facebook;
    private View poster_model;
    private boolean recordEvent = true;
    private ImageView img_qrcode;
    private ImageView img_logo;
    private LinearLayout ll_share;
    private LinearLayout multi_share_ll;

    private GIOShareTrackBean gioShareTrackBean = new GIOShareTrackBean();
    private LiveMultiSeatBean liveMultiSeatBean;
    private List<LiveMultiSeatBean.CoupleDetail> coupleDetail;
    private List<LiveMultiSeatBean.CoupleDetail> meCoupleDetail = new ArrayList<>();

    private SquareRelativeLayout meet_rl;
    private LinearLayout moment_data;
    private MeetShareItemLayout meet_top_left_view;
    private MeetShareItemLayout meet_top_right_view;
    private MeetShareItemLayout meet_bottom_left_view;
    private MeetShareItemLayout meet_bottom_right_view;
    private MeetItemLayout2 meet_one_pair_view;
    private ImageView heart_iv;
    private int whoReslut;
    private RelativeLayout rel_bottom;
    private String action;
    private boolean isFailure = false;

    public static final int SHARE_TYPE_DOWNLOAD = 0;
    public static final int SHARE_TYPE_SHOW_CHANNEL = 1;
    private int shareType = SHARE_TYPE_SHOW_CHANNEL;

    public static void gotoShare(SharePosterBean sharePosterBean) {
        if (sharePosterBean != null) {
            Intent intent = new Intent(TheLApp.getContext(), MomentPosterActivity.class);
            intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_BEAN, sharePosterBean);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            TheLApp.getContext().startActivity(intent);
        }
    }

    public static void gotoShare(String action, LiveMultiSeatBean liveMultiSeatBean, int whoReslut, boolean isFailure) {
        if (liveMultiSeatBean != null) {
            Intent intent = new Intent(TheLApp.getContext(), MomentPosterActivity.class);
            intent.putExtra(TheLConstants.BUNDLE_KEY_LIVE_SEAT_ROOM, liveMultiSeatBean);
            intent.putExtra("action", action);
            intent.putExtra("whoReslut", whoReslut);
            intent.putExtra("isFailure", isFailure);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            TheLApp.getContext().startActivity(intent);

        }
    }

    private UMShareListener umShareListener = new UMShareListener() {

        @Override
        public void onStart(SHARE_MEDIA share_media) {

        }

        @Override
        public void onResult(SHARE_MEDIA platform) {
            if (recordEvent)
                MobclickAgent.onEvent(MomentPosterActivity.this, "share_succeeded");
        }

        @Override
        public void onError(SHARE_MEDIA platform, Throwable t) {
            if (t != null && t.getMessage() != null) {
                DialogUtil.showToastShort(MomentPosterActivity.this, t.getMessage());
            }
        }

        @Override
        public void onCancel(SHARE_MEDIA platform) {
            //这里注销掉cancel提示，分享成功点击返回也会走到onCancel
//            DialogUtil.showToastShort(MomentPosterActivity.this, TheLApp.getContext().getString(R.string.my_circle_requests_act_canceled));
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_moment_poster);
        Intent intent = getIntent();
        shareType = intent.getIntExtra("shareType",SHARE_TYPE_SHOW_CHANNEL);
        Object object = intent.getSerializableExtra(TheLConstants.BUNDLE_KEY_LIVE_SEAT_ROOM);
        action = intent.getStringExtra("action");
        whoReslut = intent.getIntExtra("whoReslut", 2);
        isFailure = intent.getBooleanExtra("isFailure", false);
        if (object != null) {
            liveMultiSeatBean = (LiveMultiSeatBean) object;
            coupleDetail = liveMultiSeatBean.coupleDetail;

        }
        findViewById();

        initData();

    }

    private void findMeetResultId() {
        meet_top_left_view = findViewById(R.id.meet_top_left_view);
        meet_top_right_view = findViewById(R.id.meet_top_right_view);
        meet_bottom_left_view = findViewById(R.id.meet_bottom_left_view);
        meet_bottom_right_view = findViewById(R.id.meet_bottom_right_view);
        meet_one_pair_view = findViewById(R.id.meet_one_pair_view);
        heart_iv = findViewById(R.id.heart_iv);
        rel_bottom = findViewById(R.id.rel_bottom);
        moment_data = findViewById(R.id.moment_data);
        //匹配结果分享
        meet_rl = findViewById(R.id.meet_rl);


    }

    private void initMeetResultView(String action) {

        if (action != null) {
            switch (action) {
                case "friends_we_chat_iv":
                    btn_wx.performClick();
                    break;
                case "weibo_iv":
                    btn_weibo.performClick();
                    break;
                case "qq_zone_iv":
                    btn_qzone.performClick();
                    break;
                case "qq_iv":
                    btn_qq.performClick();
                    break;
                case "facebook_iv":
                    btn_facebook.performClick();
                    break;
                case "friends_circle_iv":
                    btn_wx_circle.performClick();
                    break;
            }
        }
    }

    protected void findViewById() {
        lin_back = findViewById(R.id.lin_back);
        img_bg = findViewById(R.id.img_bg);
        img_avatar = findViewById(R.id.img_avatar);
        txt_nickname = findViewById(R.id.txt_nickname);
        txt_rela_id = findViewById(R.id.txt_rela_id);
        txt_content = findViewById(R.id.txt_content);
        btn_wx_circle = findViewById(R.id.btn_wx_circle);
        btn_wx = findViewById(R.id.btn_wx);
        btn_weibo = findViewById(R.id.btn_weibo);
        btn_qq = findViewById(R.id.btn_qq);
        btn_qzone = findViewById(R.id.btn_qzone);
        btn_facebook = findViewById(R.id.btn_facebook);
        img_download = findViewById(R.id.img_download);
        poster_model = findViewById(R.id.poster_model);
        img_qrcode = findViewById(R.id.img_qrcode);
        img_logo = findViewById(R.id.img_logo);
        ll_share = findViewById(R.id.ll_share);
        multi_share_ll = findViewById(R.id.multi_share_ll);

        if (shareType == SHARE_TYPE_DOWNLOAD) {
            ll_share.setVisibility(View.VISIBLE);
            multi_share_ll.setVisibility(View.INVISIBLE);
        } else {
            ll_share.setVisibility(View.INVISIBLE);
            multi_share_ll.setVisibility(View.VISIBLE);
        }

        TextView time_month = findView(R.id.time_month);
        TextView time_day = findView(R.id.time_day);
        TextView time_year = findView(R.id.time_year);
        String year = DateUtils.getFormatTimeNow("yyyy");
        String month = DateUtils.monthFormat(DateUtils.getFormatTimeNow("MM"));
        String day = DateUtils.getFormatTimeNow("dd");
        time_month.setText(month);
        time_day.setText(day);
        time_year.setText(year);
        findMeetResultId();

        lin_back.setOnClickListener(this);
        btn_wx_circle.setOnClickListener(this);
        btn_wx.setOnClickListener(this);
        btn_weibo.setOnClickListener(this);
        btn_qq.setOnClickListener(this);
        btn_qzone.setOnClickListener(this);
        btn_facebook.setOnClickListener(this);
        img_download.setOnClickListener(this);
        ll_share.setOnClickListener(this);
    }

    private void initData() {
        SharePosterBean sharePosterBean = (SharePosterBean) getIntent().getSerializableExtra(TheLConstants.BUNDLE_KEY_MOMENT_BEAN);
        if (sharePosterBean != null) {
            meet_rl.setVisibility(View.GONE);
            if (FROM_ADDFRIEND == sharePosterBean.from) {
                gioShareTrackBean.track = GrowingIoConstant.PERSONAL_POST_SHARE;
            } else {
                gioShareTrackBean.track = GrowingIoConstant.LOG_POST_SHARE;
            }

            if (!TextUtils.isEmpty(sharePosterBean.userName))
                txt_rela_id.setText(getString(R.string.rela_id, sharePosterBean.userName));
            else//因未完善资料而没有relaId的时候，整个控件不显示
                txt_rela_id.setVisibility(View.GONE);
            if (!TextUtils.isEmpty(sharePosterBean.momentsText)) {
                txt_content.setText(sharePosterBean.momentsText.replace("\n", " "));
            } else {
                txt_content.setText(getString(R.string.moment_poster_default_txt));
            }
            txt_nickname.setText(sharePosterBean.nickname + "");
            ImageLoaderManager.imageLoaderCropCircleBorder(img_avatar, sharePosterBean.avatar);
            final String imageUrl = getImageUrl(sharePosterBean.imageUrl);
            if (!TextUtils.isEmpty(imageUrl)) {
                //上面两个圆角，下面两个直角

                RequestOptions requestOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.mipmap.bg_topic_default);
                //带灰色蒙层
                Glide.with(img_bg).load(imageUrl).apply(requestOptions).into(img_bg);


            } else {
                //上面两个圆角，下面两个直角
                RequestOptions requestOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.mipmap.bg_topic_default);
                Glide.with(img_bg).load(R.color.black_alpha_60).apply(requestOptions).into(img_bg);
            }

            //二维码区分
            final String url = FROM_ADDFRIEND == sharePosterBean.from ? URL_INVITE_FRIEND : URL_SHARE_MOMENT;
            final Bitmap bitmapQr = QRUtils.createQRImage(url, Utils.dip2px(this, 80), Utils.dip2px(this, 80));
            img_qrcode.setImageBitmap(bitmapQr);
        } else if (!TextUtils.isEmpty(action)) {
            meet_rl.setVisibility(View.VISIBLE);
            moment_data.setVisibility(View.GONE);
            rel_bottom.setVisibility(View.GONE);
            gioShareTrackBean.track = GrowingIoConstant.ENCOUNTER_RESULT_SHARE;
            String content = "";
            switch (whoReslut) {
                case MeetResultActivity.BROADCASTER: //主播
                    content = getString(R.string.best_go_between);
                    break;
                case MeetResultActivity.GUEST: //我自己
                    content = getString(R.string.meet_result_success_guest);
                    break;
                case MeetResultActivity.GUESTALL: //嘉宾、全场
                    content = getString(R.string.meet_result_success_host, coupleDetail.size());
                    break;
                case MeetResultActivity.AUDIENCE: //观众
                    content = getString(R.string.meet_result_success_host, coupleDetail.size());
                    break;
                default:
                    break;

            }
            if (isFailure == true) {
                content = getString(R.string.waiting_to_meet_you);
            }
            //上面两个圆角，下面两个直角
            RequestOptions requestOptions = new RequestOptions().transform(new ColorFilterTransformation(this, ContextCompat.getColor(this, R.color.transparent)));
            //带灰色蒙层
            Glide.with(img_bg).load(R.mipmap.bg_meet_success_bg).apply(requestOptions).into(img_bg);
            img_logo.setImageResource(R.mipmap.btn_logo);
            if (!TextUtils.isEmpty(content)) {
                txt_content.setText(content);
                txt_content.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
            } else {
                txt_content.setVisibility(View.GONE);
            }
            ImageLoaderManager.imageLoaderCropCircleBorder(img_avatar, UserUtils.getUserAvatar());

            txt_nickname.setText(UserUtils.getUserName());
            if (!TextUtils.isEmpty(UserUtils.getMyUserId())) {
                txt_rela_id.setText(getString(R.string.rela_id, UserUtils.getMyUserId()));

            } else {//因未完善资料而没有relaId的时候，整个控件不显示
                txt_rela_id.setVisibility(View.GONE);

            }


            if (isMeetSuccessByMe()) {

                L.d("MeetResultFragment", " 1 : ");

                setCount(1);

                if (getMeetSuccessWithMeData().size() == 2) {

                    meet_one_pair_view.setData(getMeetSuccessWithMeData().get(0), getMeetSuccessWithMeData().get(1));

                }

            } else {

                //配对成功0对
                if (coupleDetail.size() == 0) {

                    L.d("MeetResultFragment", " 1 : ");

                    setCount(1);

                    meet_one_pair_view.setFilureData();

                }

                //配对成功1对
                if (coupleDetail.size() == 2) {

                    L.d("MeetResultFragment", " 1 : ");

                    setCount(1);

                    meet_one_pair_view.setData(coupleDetail.get(0), coupleDetail.get(1));

                }

                //配对成功2对
                if (coupleDetail.size() == 4) {
                    setCount(2);

                    L.d("MeetResultFragment", " 2 : ");


                    meet_top_left_view.setData(coupleDetail.get(0), coupleDetail.get(1));

                    meet_bottom_left_view.setData(coupleDetail.get(2), coupleDetail.get(3));

                }

                //配对成功3对
                if (coupleDetail.size() == 6) {
                    setCount(3);

                    L.d("MeetResultFragment", " 3 : ");

                    meet_top_left_view.setData(coupleDetail.get(0), coupleDetail.get(1));

                    meet_top_right_view.setData(coupleDetail.get(2), coupleDetail.get(3));

                    meet_bottom_left_view.setData(coupleDetail.get(4), coupleDetail.get(5));

                }

                //配对成功4对
                if (coupleDetail.size() == 8) {
                    setCount(4);

                    L.d("MeetResultFragment", " 4 : ");

                    meet_top_left_view.setData(coupleDetail.get(0), coupleDetail.get(1));

                    meet_top_right_view.setData(coupleDetail.get(2), coupleDetail.get(3));

                    meet_bottom_left_view.setData(coupleDetail.get(4), coupleDetail.get(5));

                    meet_bottom_right_view.setData(coupleDetail.get(6), coupleDetail.get(7));

                }

            }


            L.d("MeetResultFragment", " TheLConstants.MeetResultConstant.MEET_RESULT_GUEST_ME : " + TheLConstants.MeetResultConstant.MEET_RESULT_GUEST_ME);

        }

        //二维码区分
        final String url = URL_SHARE_ENCOUNTER;
        final Bitmap bitmapQr = QRUtils.createQRImage(url, Utils.dip2px(this, 80), Utils.dip2px(this, 80));
        img_qrcode.setImageBitmap(bitmapQr);
        poster_model.postDelayed(new Runnable() {
            @Override
            public void run() {
                initMeetResultView(action);

            }
        }, 2000);

    }


    //判断是否是偶数
    private boolean isEvenNumbers(int num) {

        return num % 2 == 0;
    }

    //设置相遇成功数量
    private void setCount(int count) {
        switch (count) {
            case 1:
                meet_top_left_view.setVisibility(View.GONE);
                meet_top_right_view.setVisibility(View.GONE);
                meet_bottom_left_view.setVisibility(View.GONE);
                meet_bottom_right_view.setVisibility(View.GONE);
                meet_one_pair_view.setVisibility(View.VISIBLE);
                heart_iv.setVisibility(View.VISIBLE);
                break;
            case 2:
                meet_top_left_view.setVisibility(View.VISIBLE);
                meet_top_right_view.setVisibility(View.GONE);
                meet_bottom_left_view.setVisibility(View.VISIBLE);
                meet_bottom_right_view.setVisibility(View.GONE);
                meet_one_pair_view.setVisibility(View.GONE);
                heart_iv.setVisibility(View.GONE);

                break;
            case 3:
                meet_top_left_view.setVisibility(View.VISIBLE);
                meet_top_right_view.setVisibility(View.VISIBLE);
                meet_bottom_left_view.setVisibility(View.VISIBLE);
                meet_bottom_right_view.setVisibility(View.GONE);
                meet_one_pair_view.setVisibility(View.GONE);
                heart_iv.setVisibility(View.GONE);

                break;
            case 4:
                meet_top_left_view.setVisibility(View.VISIBLE);
                meet_top_right_view.setVisibility(View.VISIBLE);
                meet_bottom_left_view.setVisibility(View.VISIBLE);
                meet_bottom_right_view.setVisibility(View.VISIBLE);
                meet_one_pair_view.setVisibility(View.GONE);
                heart_iv.setVisibility(View.GONE);
                break;
        }
    }

    private String getImageUrl(String urls) {
        if (TextUtils.isEmpty(urls)) {
            return "";
        }
        if (urls.contains(","))
            return urls.split(",")[0];
        else return urls;
    }

    @Override
    public void onClick(final View v) {
        if (v.getId() == R.id.lin_back) {
            finish();
            return;
        }
        PermissionUtil.requestStoragePermission(this, new PermissionUtil.PermissionCallback() {
            @Override
            public void granted() {
                SHARE_MEDIA platform = null;
                switch (v.getId()) {
                    case R.id.btn_wx_circle:
                        platform = SHARE_MEDIA.WEIXIN_CIRCLE;
                        break;
                    case R.id.btn_wx:
                        platform = SHARE_MEDIA.WEIXIN;
                        break;
                    case R.id.btn_weibo:
                        platform = SHARE_MEDIA.SINA;
                        break;
                    case R.id.btn_qq:
                        platform = SHARE_MEDIA.QQ;
                        break;
                    case R.id.btn_qzone:
                        platform = SHARE_MEDIA.QZONE;
                        break;
                    case R.id.btn_facebook:
                        shareFacebook();
                        return;
                    case R.id.img_download:
                    case R.id.ll_share:
                        download();
                        return;

                }
                share(platform);
            }

            @Override
            public void denied() {

            }

            @Override
            public void gotoSetting() {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void finish() {
        deleteFile();
        super.finish();
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    /**
     * 删除缓存文件
     */
    private void deleteFile() {
        try {
            if (!TextUtils.isEmpty(imgPath) && !isDownload) {
                final File f = new File(imgPath);
                if (f.exists()) {
                    f.delete();
                }
            }
            if (!TextUtils.isEmpty(zoomPath)) {
                final File zoomF = new File(zoomPath);
                if (zoomF.exists()) {
                    zoomF.delete();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 下载海报
     */
    private void download() {
        final String path = getShareImagePath(poster_model, IMG_QUALITY);
        if (!TextUtils.isEmpty(path)) {
            isDownload = true;
            setResult(Activity.RESULT_OK, getIntent());
            finish();
//            Toast.makeText(this, getString(R.string.poster_download_success), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * facebook分享
     */
    private void shareFacebook() {
        final String path = getShareImagePath(poster_model, IMG_QUALITY);
        if (!TextUtils.isEmpty(path)) {
            if (ShareDialog.canShow(SharePhotoContent.class)) {
                final SharePhoto photo = new SharePhoto.Builder().setBitmap(BitmapFactory.decodeFile(path)).build();
                final SharePhotoContent content = new SharePhotoContent.Builder().addPhoto(photo).build();
                ShareDialog shareDialog = new ShareDialog(this);
                shareDialog.show(content);
            } else {
                DialogUtil.showToastShort(this, getString(R.string.install_facebook));
            }
        }
    }

    /**
     * 朋友圈，微信，qq，qq空间，新浪微博 分享
     *
     * @param platform
     */
    public void share(SHARE_MEDIA platform) {
        MobclickAgent.onEvent(this, "start_share");
        final String path = getShareImagePath(poster_model, IMG_QUALITY);
        final String thumb_path = getZoomBitmapPath(path, THUMB_SCALE, IMG_QUALITY);
        if (null != platform && !TextUtils.isEmpty(path)) {
            final UMImage image = new UMImage(this, BitmapFactory.decodeFile(path));
            image.setThumb(new UMImage(this, BitmapFactory.decodeFile(thumb_path)));
            new ShareAction(this).setPlatform(platform).withMedia(image).setCallback(umShareListener).share();
            gioShareTrackBean.shareBean.shareType = GrowingIOUtil.getShareType(platform);
            GrowingIOUtil.shareTrack(gioShareTrackBean);
        }
    }

    /**
     * 获取分享的图片
     *
     * @param view
     * @return
     */
    private String getShareImagePath(View view, int quality) {
        L.d("poster_model", "width    :  +" + view.getWidth());
        L.d("poster_model", "height    :  +" + view.getHeight());
        if (!TextUtils.isEmpty(imgPath)) {
            return imgPath;
        }
        TextView tips = view.findViewById(R.id.click_qrcode_tips);
        tips.setVisibility(View.VISIBLE);
        final Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        tips.setVisibility(View.GONE);

        File dir = new File(TheLConstants.F_TAKE_PHOTO_ROOTPATH);
        if (!dir.exists()) {
            dir.mkdirs();
        }

        imgPath = FileHelper.getInstance().getCameraDir() + System.currentTimeMillis() + TheLConstants.MOMENT_POSTER_SHARE_IMG_END + ".jpg";


        L.d("MomentPoster", " imgPath -1 : " + imgPath);

        try {
            if (ImageUtils.savePic(bitmap, imgPath, quality)) {
                Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                Uri uri = Uri.fromFile(new File(imgPath));
                intent.setData(uri);
                this.sendBroadcast(intent);
                L.d("MomentPoster", " imgPath 0 : " + imgPath);
                return imgPath;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        imgPath = null;

        L.d("MomentPoster", " imgPath 1 : " + imgPath);

        return imgPath;
    }

    private String getZoomBitmapPath(String oldPath, float scale, int quality) {
        if (!TextUtils.isEmpty(zoomPath)) {
            return zoomPath;
        }
        if (TextUtils.isEmpty(oldPath)) {
            zoomPath = null;
            return zoomPath;
        }
        final Bitmap bitmap = BitmapFactory.decodeFile(oldPath);
        if (null == bitmap) {
            return null;
        }
        try {
            final Bitmap bm = getZoomBitmap(bitmap, scale);
            zoomPath = TheLConstants.F_TAKE_PHOTO_ROOTPATH + System.currentTimeMillis() + TheLConstants.MOMENT_POSTER_SHARE_IMG_END + ".jpg";
            if (ImageUtils.savePic(bm, zoomPath, quality)) {
                return zoomPath;
            }
        } catch (Exception e) {
            bitmap.recycle();
            zoomPath = null;
            e.printStackTrace();
        }
        return zoomPath;
    }

    private Bitmap getZoomBitmap(Bitmap oldBitmap, float scale) {
        Bitmap bm = null;
        final float width = oldBitmap.getWidth();
        final float height = oldBitmap.getHeight();
        final Matrix matrix = new Matrix();
        matrix.postScale(scale, scale);
        bm = Bitmap.createBitmap(oldBitmap, 0, 0, (int) width, (int) height, matrix, true);
        return bm;
    }

    private boolean isMeetSuccessByMe() {
        if (coupleDetail != null) {

            for (int i = 0; i < coupleDetail.size(); i++) {

                final LiveMultiSeatBean.CoupleDetail cd = coupleDetail.get(i);

                final String userId = UserUtils.getMyUserId();

                if (userId.equals(cd.userId)) {

                    return true;
                }

            }

        }
        return false;
    }

    private List<LiveMultiSeatBean.CoupleDetail> getMeetSuccessWithMeData() {

        List<LiveMultiSeatBean.CoupleDetail> meCoupleDetail = new ArrayList<>();

        if (coupleDetail != null) {

            for (int i = 0; i < coupleDetail.size(); i++) {

                final LiveMultiSeatBean.CoupleDetail cd = coupleDetail.get(i);

                final String userId = UserUtils.getMyUserId();

                if (userId.equals(cd.userId)) {

                    boolean isEvenNumbers = isEvenNumbers(i);

                    if (isEvenNumbers) {
                        meCoupleDetail.add(cd);
                        meCoupleDetail.add(coupleDetail.get(i + 1));
                    } else {
                        meCoupleDetail.add(cd);
                        meCoupleDetail.add(coupleDetail.get(i - 1));
                    }

                }

            }

        }

        return meCoupleDetail;

    }

}
