package com.thel.modules.main.messages.dialog;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thel.base.BaseDialogFragment;

/**
 * Created by kevin on 2017/9/26.
 */

public class MessagesDialog extends BaseDialogFragment{
    private static MessagesDialog instance = null;

    public static MessagesDialog getInstance() {
        if (instance == null) {
            instance = new MessagesDialog();
        }
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
