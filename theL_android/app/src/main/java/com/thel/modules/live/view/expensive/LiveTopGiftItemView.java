package com.thel.modules.live.view.expensive;

import android.content.Context;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.thel.R;
import com.thel.constants.TheLConstants;
import com.thel.utils.ImageUtils;
import com.thel.utils.StringUtils;
import com.thel.utils.Utils;

/**
 * Created by waiarl on 2018/3/2.
 */

public class LiveTopGiftItemView extends RelativeLayout implements TopGiftItemImp<LiveTopGiftItemView> {
    private final Context mContext;
    private TextView txt_content;
    private ImageView img_jump;
    private TopGiftBean topGiftBean;
    private int radius;
    private TopGiftClickListener mTopGiftClickListener;
    private boolean isAdded = false;
    private SimpleDraweeView img_sender;
    private SimpleDraweeView img_receiver;

    public LiveTopGiftItemView(Context context) {
        this(context, null);
    }

    public LiveTopGiftItemView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LiveTopGiftItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        initData();
        init();
        setListener();
    }

    private void initData() {
        radius = Utils.dip2px(mContext, 8);
    }

    private void init() {
        inflate(mContext, R.layout.live_top_gift_item_view, this);
        txt_content = findViewById(R.id.txt_content);
        img_jump = findViewById(R.id.img_jump);
        img_sender = findViewById(R.id.img_sender);
        img_receiver = findViewById(R.id.img_receiver);
    }

    private void setListener() {
        img_jump.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (topGiftBean != null && mTopGiftClickListener != null) {
                    mTopGiftClickListener.clickJumpToLive(topGiftBean.toUserId);
                }
            }
        });
        img_sender.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (topGiftBean != null && mTopGiftClickListener != null) {
                    mTopGiftClickListener.clickSender(topGiftBean.userId);
                }
            }
        });
        img_receiver.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (topGiftBean != null && mTopGiftClickListener != null) {
                    mTopGiftClickListener.clickReceiver(topGiftBean.toUserId);
                }
            }
        });
    }

    @Override
    public LiveTopGiftItemView initView(TopGiftBean topGiftBean) {
        this.topGiftBean = topGiftBean;
        final String content = StringUtils.getString(R.string.top_gift_content, topGiftBean.nickName, topGiftBean.toNickName, topGiftBean.softGiftBean.title);
        txt_content.setText(content);

        setImageUrl(img_sender, topGiftBean.avatar, TheLConstants.AVATAR_SMALL_SIZE, TheLConstants.AVATAR_SMALL_SIZE);
        setImageUrl(img_receiver, topGiftBean.toAvatar, TheLConstants.AVATAR_SMALL_SIZE, TheLConstants.AVATAR_SMALL_SIZE);
        return this;
    }

    @Override
    public LiveTopGiftItemView getView() {
        return this;
    }

    @Override
    public LiveTopGiftItemView setTopGiftClickListener(TopGiftClickListener listener) {
        this.mTopGiftClickListener = listener;
        return this;
    }

    @Override
    public boolean isAdded() {
        return isAdded;
    }

    @Override
    public void setIsAdded(boolean isAdded) {
        this.isAdded = isAdded;
    }

    private void setImageUrl(SimpleDraweeView view, String url, int width, int height) {
        view.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(url, width, height))).build()).setAutoPlayAnimations(true).build());
    }
}
