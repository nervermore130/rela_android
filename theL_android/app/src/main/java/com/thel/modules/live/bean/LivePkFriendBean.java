package com.thel.modules.live.bean;

import java.io.Serializable;

/**
 * Created by waiarl on 2017/11/29.
 * 直播连麦Pk 好友bean
 */

public class LivePkFriendBean implements Serializable {
    public String id;
    public String nickName;//http请求
    public String nickname;//tcp请求
    public String avatar;
    public int liveStatus;
    public int audioType;
    public int iconSwitch = 1;//1显示用户等级 0不显示用户等级
    public int userLevel = 0;//用户等级
    public int vip;
    public boolean isGuard;//是否是守护日榜用户
    public long gold;//软妹豆
    public int Rank;
    public String userId;//观众连麦日榜里用
    /**
     * 直播状态
     */
    public static final int LIVE_STATUS_NO = 0; //无状态（可以发起连麦或者PK）
    public static final int LIVE_STATUS_LINK = 1;//连麦
    public static final int LIVE_STATUS_PK = 2;//PK

    @Override public String toString() {
        return "LivePkFriendBean{" +
                "id='" + id + '\'' +
                ", nickName='" + nickName + '\'' +
                ", nickname='" + nickname + '\'' +
                ", avatar='" + avatar + '\'' +
                ", liveStatus=" + liveStatus +
                ", audioType=" + audioType +
                ", iconSwitch=" + iconSwitch +
                ", userLevel=" + userLevel +
                ", vip=" + vip +
                ", isGuard=" + isGuard +
                ", gold=" + gold +
                ", Rank=" + Rank +
                ", userId='" + userId + '\'' +
                '}';
    }
}
