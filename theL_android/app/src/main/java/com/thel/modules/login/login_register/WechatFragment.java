package com.thel.modules.login.login_register;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.tencent.mm.opensdk.modelmsg.SendAuth;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.constants.TheLConstants;
import com.thel.modules.login.LoginEventManager;
import com.thel.network.LoginInterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.api.loginapi.bean.SignInBean;
import com.thel.utils.DialogUtil;
import com.thel.utils.L;
import com.thel.utils.ToastUtils;
import com.umeng.analytics.MobclickAgent;

import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


/**
 * A simple {@link Fragment} subclass.
 */
public class WechatFragment extends Fragment {

    private static final String TAG = "WechatFragment";

    private IWXAPI wxApi;
    private WxLoginReceiver wxLoginReceiver;

    public WechatFragment() {
        // Required empty public constructor
    }

    //0圆圈微信登录按钮布局 1椭圆微信登录按钮布局
    private int mType;

    public static WechatFragment newInstance(int type) {
        WechatFragment fragment = new WechatFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("type", type);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mType = getArguments().getInt("type");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (mType == 0) {
            return inflater.inflate(R.layout.fragment_wechat, container, false);
        } else if (mType == 1) {
            return inflater.inflate(R.layout.fragment_wechat_register, container, false);
        } else {
            return inflater.inflate(R.layout.fragment_wechat, container, false);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        wxApi = WXAPIFactory.createWXAPI(TheLApp.context, TheLConstants.WX_APP_ID, true);
        wxApi.registerApp(TheLConstants.WX_APP_ID);

        //微信登录成功广播注册
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(TheLConstants.BROADCAST_WX_LOGIN_SUCCEED);
        intentFilter.addAction(TheLConstants.BROADCAST_WX_LOGIN_CANCEL);
        wxLoginReceiver = new WxLoginReceiver();
        getActivity().registerReceiver(wxLoginReceiver, intentFilter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (wxLoginReceiver != null)
            getActivity().unregisterReceiver(wxLoginReceiver);
    }

    @OnClick(R.id.img_wx)
    void login() {
        MobclickAgent.onEvent(getActivity(), "wechat_login");// 微信注册转化率统计：点击微信登陆
        if (!wxApi.isWXAppInstalled()) {
            Toast.makeText(getActivity(), TheLApp.context.getString(R.string.default_activity_wx_uninstalled), Toast.LENGTH_SHORT).show();
            return;
        }

        SendAuth.Req req = new SendAuth.Req();
        req.scope = "snsapi_userinfo";
        req.state = "thel_login";
        wxApi.sendReq(req);


//        String token = ShareFileUtils.getWxLoginToken();
//
//        WXLoginBean wxLoginBean = gson.fromJson(token, WXLoginBean.class);
//
//        String userInfo = ShareFileUtils.getWxLoginUserInfo();
//
//        WXUserInfoBean wxUserInfoBean = gson.fromJson(userInfo, WXUserInfoBean.class);
//
//        if (wxLoginBean == null || wxLoginBean.errcode != null || wxLoginBean.access_token == null || wxUserInfoBean == null || wxUserInfoBean.errcode != null || (System.currentTimeMillis() - ShareFileUtils.getWxLoginTime() >= 7200000)) {
//
//            L.d("WechatFragment", "---------WechatFragment-------");
//            SendAuth.Req req = new SendAuth.Req();
//            req.scope = "snsapi_userinfo";
//            req.state = "thel_login";
//            wxApi.sendReq(req);
//        } else {
//            login(wxLoginBean.openid, wxLoginBean.access_token, wxUserInfoBean.headimgurl, wxUserInfoBean.nickname);
//
//        }
    }

    // 微信登录成功接收器
    private class WxLoginReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(TheLConstants.BROADCAST_WX_LOGIN_SUCCEED)) {
                String openid = intent.getStringExtra("openid");
                String access_token = intent.getStringExtra("access_token");
                String avatar = intent.getStringExtra("avatar");
                String nickName = intent.getStringExtra("nickName");
                String unionid = intent.getStringExtra("unionid");
                int sex = intent.getIntExtra("sex", 2);
                checkSex(openid, access_token, unionid, avatar, nickName, sex);
            } else if (intent.getAction().equals(TheLConstants.BROADCAST_WX_LOGIN_CANCEL)) {
                ToastUtils.showToastShort(getActivity(), getActivity().getString(R.string.cancel));
            }
        }
    }

    private void checkSex(final String openid, final String access_token, final String unionid, final String avatar, final String nickName, int sex) {
        if (sex == 1) {// 如果是男性
            MobclickAgent.onEvent(getActivity(), "wechat_male_login");// 微信登录性别是男性打点
            final DialogInterface.OnClickListener listener0 = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            };
            final DialogInterface.OnClickListener listener1 = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    MobclickAgent.onEvent(getActivity(), "check_i_am_a_lady");// 点击我是女生按钮
//                                                    authSucceed(openid + "," + unionid, access_token);
                    login(openid + "," + unionid, access_token, avatar, nickName);
                }
            };
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DialogUtil.getInstance().showConfirmDialog(getActivity(), "", getString(R.string.register_activity_test_hint), getString(R.string.i_am_a_lady), getString(R.string.info_quit), listener1, listener0);
                }
            });
            return;
        }
        MobclickAgent.onEvent(getActivity(), "wechat_female_login");// 微信注册转化率统计：女性性别校验成功

        login(openid + "," + unionid, access_token, avatar, nickName);
    }

    private void login(String openid, String access_token, String avatar, String nickName) {
        if (TextUtils.isEmpty(access_token) || TextUtils.isEmpty(openid)) {
            ToastUtils.showToastShort(getActivity(), "wechat login access_token null");
            return;
        }
        ((BaseActivity) getActivity()).showLoadingNoBack();
        MobclickAgent.onEvent(TheLApp.getContext(), "begin_request_loginOrRegister_interface"); // 开始调用登录接口打点
        RequestBusiness.getInstance().signIn("wx", "", "", "", "", "", openid, access_token, avatar, nickName, null)
                .onBackpressureDrop().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new LoginInterceptorSubscribe<SignInBean>() {
                    @Override
                    public void onNext(SignInBean data) {
                        super.onNext(data);
                        if (getActivity() != null)
                            ((BaseActivity) getActivity()).closeLoading();
                        if (data == null || data.data == null || data.data.user == null)
                            return;
                        LoginEventManager.loginCallback(getActivity(), data);
                        if (mType == 0 && getActivity() != null)
                            getActivity().finish();
                        L.d("wxlogin", "accept :" + data);
                    }

                    @Override
                    public void onComplete() {
                        super.onComplete();
                        if (getActivity() != null)
                            ((BaseActivity) getActivity()).closeLoading();
                    }
                });
    }

}
