package com.thel.modules.live.liveBigGiftAnimLayout.anim;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.thel.modules.live.liveBigGiftAnimLayout.LiveBigAnimUtils;
import com.thel.utils.Utils;

import java.util.Random;

/**
 * 掉金币的轨迹动画
 * created by Setsail on 2016.6.2
 */
public class CoinDropAnimator {
    private final String flodPath;
    private final String res;
    private Handler mHandler;
    private int duration;
    private int minDuration = 2000;
    private int countDown;
    private Random mRandom;
    private Context mContext;
    private int maxSize;
    private int minSize;
    private int rate = 200;

    public CoinDropAnimator(Context context, int d) {
        mContext = context;
        maxSize = Utils.dip2px(context, 80);
        minSize = Utils.dip2px(context, 40);
        duration = d;
        mHandler = new Handler(Looper.getMainLooper());
        mRandom = new Random();
        flodPath = "anim/coin";
        res = "icn_animate_coinrain";
    }

    public int start(final ViewGroup parent) {
        countDown = duration;
        final RelativeLayout backgound = new RelativeLayout(mContext);
        parent.addView(backgound, RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        setBackground(backgound);

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                backgound.clearAnimation();
                parent.removeView(backgound);
            }
        }, duration);
        return duration;
    }

    private void setBackground(final RelativeLayout backgound) {
        backgound.post(new Runnable() {
            @Override
            public void run() {
                dropOneCoin(backgound);
                dropOneCoin(backgound);
                dropOneCoin(backgound);
                dropOneCoin(backgound);
                dropOneCoin(backgound);
                dropOneCoin(backgound);
                dropOneCoin(backgound);
                dropOneCoin(backgound);
                if (countDown >= rate) {
                    countDown -= rate;
                    backgound.postDelayed(this, rate);
                }
            }
        });
    }

    private void dropOneCoin(final ViewGroup parent) {

        int parentWidth = parent.getWidth();
        int parentHeight = parent.getHeight();
        if (parentWidth <= 0 || parentHeight <= 0)
            return;

        Path p = new Path();
        int w;
        int h = parentHeight;
        int dur = mRandom.nextInt(duration - minDuration) + minDuration;
        int size = mRandom.nextInt(maxSize - minSize) + minSize;
        w = mRandom.nextInt(parentWidth);
        p.moveTo(w, 0 - 2 * size);
        p.lineTo(w, h);

        if (dur > countDown)// 防止动画超出duration
            return;

        final ImageView child = new ImageView(mContext);
        child.setLayoutParams(new RelativeLayout.LayoutParams(size, size));
        //child.setImageResource(R.drawable.icn_animate_coinrain);
        LiveBigAnimUtils.setAssetImage(child, flodPath, res);

        parent.addView(child);

        FloatAnimation anim = new FloatAnimation(p, parent, child);

        anim.setDuration(dur);
        anim.setInterpolator(new LinearInterpolator());
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation animation) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        parent.removeView(child);
                    }
                });
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }

            @Override
            public void onAnimationStart(Animation animation) {
            }
        });
        child.startAnimation(anim);
    }

    static class FloatAnimation extends Animation {
        private PathMeasure mPm;
        private View mView;
        private float mDistance;

        public FloatAnimation(Path path, View parent, View child) {
            mPm = new PathMeasure(path, false);
            mDistance = mPm.getLength();
            mView = child;
            parent.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        }

        @Override
        protected void applyTransformation(float factor, Transformation transformation) {
            Matrix matrix = transformation.getMatrix();
            mPm.getMatrix(mDistance * factor, matrix, PathMeasure.POSITION_MATRIX_FLAG);
            mView.setRotation(1);
            mView.setScaleX(1);
            mView.setScaleY(1);
            transformation.setAlpha(1);
        }
    }
}

