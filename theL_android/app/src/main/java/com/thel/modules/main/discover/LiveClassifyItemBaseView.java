package com.thel.modules.main.discover;

import com.thel.modules.live.bean.LiveRoomBean;

/**
 * Created by waiarl on 2017/11/28.
 */

public interface LiveClassifyItemBaseView<T> {

    T initView(LiveRoomBean liveRoomBean, String pageId, int currentType, int currentPosition);

    T initMeasureDimen();
}
