package com.thel.modules.main.messages;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationManagerCompat;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.IMsgStatusCallback;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseFragment;
import com.thel.bean.moments.MomentsCheckBean;
import com.thel.constants.BundleConstants;
import com.thel.constants.TheLConstants;
import com.thel.db.DBConstant;
import com.thel.db.DBManager;
import com.thel.db.DBUtils;
import com.thel.db.table.message.MsgTable;
import com.thel.flutter.bridge.FlutterPushImpl;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.flutter.bridge.RfSBridgeHandlerFactory;
import com.thel.imp.black.BlackUtils;
import com.thel.manager.ChatServiceManager;
import com.thel.modules.main.home.moments.ReportActivity;
import com.thel.modules.main.home.moments.web.WebViewActivity;
import com.thel.modules.main.me.bean.LogInfoBean;
import com.thel.modules.main.me.bean.MyCirclePartnerBean;
import com.thel.modules.main.me.bean.MyInfoNetBean;
import com.thel.modules.main.me.match.eventcollect.collect.LiveLogUtils;
import com.thel.modules.main.me.match.eventcollect.collect.MatchLogUtils;
import com.thel.modules.main.messages.adapter.MessageAdapter;
import com.thel.modules.main.messages.bean.MsgBean;
import com.thel.modules.main.messages.utils.MsgUtils;
import com.thel.modules.main.userinfo.MyLinearLayoutManager;
import com.thel.modules.others.VipConfigActivity;
import com.thel.network.service.DefaultRequestService;
import com.thel.service.chat.ChatService;
import com.thel.ui.dialog.DeleteMsgDialog;
import com.thel.ui.widget.MessageParkEmptyLayout;
import com.thel.ui.widget.MySwipeRefreshLayout;
import com.thel.utils.DateUtils;
import com.thel.utils.ExecutorServiceUtils;
import com.thel.utils.FireBaseUtils;
import com.thel.utils.HeaderAndFooterRecyclerAdapter;
import com.thel.utils.L;
import com.thel.utils.PinyinUtils;
import com.thel.utils.ScreenUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.BiFunction;
import io.reactivex.schedulers.Schedulers;

/**
 * @author liuyun
 */
public class MessageFragment extends BaseFragment {

    private static final String TAG = "MessageFragment";

    private TextView txt_xmp_status_title;
    private LinearLayout lin_go_to_top;
    private EditText edit_search;
    private RecyclerView recyclerView;
    private MySwipeRefreshLayout swipe_refresh;
    private MessageAdapter messageAdapter;

    private MessageParkEmptyLayout bg_messages_default;
    private ImageView not_connet_image;
    private RelativeLayout notify_rl;
    private TextView moments_msg_count;
    private LinearLayout root_ll;
    private TextView update_tv;
    /**
     * 没有匹配的联系人提示
     */
    private TextView no_match_tip;

    private TextView start_push_tv;

    private RelativeLayout push_tips_rl;

    private ImageView close_push_tips_iv;

    private List<MsgTable> lastMsgWithUnreadList = new ArrayList<>();
    /**
     * 输入关键字过滤后的消息对象列表
     */
    private List<MsgTable> filterArr = new ArrayList<>();

    private TextWatcher textWatcher;
    private Handler mHandler = new Handler();

    private static long lastClickTime = 0;

    private HeaderAndFooterRecyclerAdapter mHeaderAndFooterRecyclerAdapter;

    public static MessageFragment getInstance() {
        return new MessageFragment();
    }

    private String pageId;
    private String from_page_id;
    private String from_page;
    private String latitude;
    private String longitude;

    private MyCirclePartnerBean partnerBean;//伴侣信息

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_message2, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        findViewById(view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        L.d(TAG, "------------onActivityCreated-----------");

        if (isAdded() && getActivity() != null) {

            if (mIMsgStatusCallback != null) {

                ChatServiceManager.getInstance().registerCallback(TAG, mIMsgStatusCallback);

            }

            final Bundle bundle = getArguments();
            from_page = ShareFileUtils.getString(ShareFileUtils.rootSwitchPage, "");
            from_page_id = ShareFileUtils.getString(ShareFileUtils.rootSwitchPageId, "");

            if (bundle != null) {
                from_page_id = bundle.getString(ShareFileUtils.MATCH_FROM_PAGE_ID, "");
                from_page = bundle.getString(ShareFileUtils.MATCH_FROM_PAGE, "");

            }
            registerReceiver();// 注册广播

            initList();

            setListener();
            pageId = Utils.getPageId();
            latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
            longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");


        }

    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        L.d(TAG, " onHiddenChanged : " + hidden);
    }

    @Override
    public void onResume() {
        super.onResume();

        L.d(TAG, " onResume : ");

        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    showConnectStatusUI(ChatServiceManager.getInstance().getConnectStatus());

                    reCountStrangerUnReadCount();

                }
            });
        }
        MobclickAgent.onPageStart(this.getClass().getName());
        MobclickAgent.onResume(getContext());
        L.d(TAG, " MessageFragment onResume stranger : ");

    }

    @Override
    public void onPause() {

        L.d(TAG, " onPause : ");

        super.onPause();
        //edit_search进入后台去除焦点
        if (edit_search != null && edit_search.hasFocus()) {
            edit_search.clearFocus();
        }
        MobclickAgent.onPageEnd(this.getClass().getName());
        MobclickAgent.onPause(getContext());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        L.d(TAG, " onDestroy : ");

        if (textWatcher != null && edit_search != null) {
            edit_search.removeTextChangedListener(textWatcher);
        }

        if (receiver != null && getActivity() != null) {
            getActivity().unregisterReceiver(receiver);

        }

        if (root_ll != null && mViewTreeObserver != null) {
            root_ll.getViewTreeObserver().removeOnGlobalLayoutListener(mViewTreeObserver);

        }

        if (mIMsgStatusCallback != null) {
            ChatServiceManager.getInstance().unRegisterCallback(TAG);
        }

    }

    private MsgTable getMsgHidingMsgBean() {

        MsgTable msgTable = new MsgTable();

        msgTable.userId = DBConstant.Message.MSG_HIDING;

        msgTable.msgType = MsgBean.MSG_TYPE_MSG_HIDING;

        msgTable.packetId = MsgUtils.getMsgId();

        msgTable.avatar = "";

        msgTable.userName = "";

        msgTable.unreadCount = 0;

        msgTable.msgTime = System.currentTimeMillis();

        return msgTable;

    }

    private MsgTable getMsgRelaParkMsgBean() {
        MsgTable msgTable = new MsgTable();

        msgTable.userId = DBConstant.Message.MSG_AMUSEMENT_PARK;

        msgTable.msgType = MsgBean.MSG_TYPE_AMUSEMENT_PARK_MSG_TYPE;

        msgTable.packetId = MsgUtils.getMsgId();

        msgTable.avatar = "";

        msgTable.userName = "";

        msgTable.unreadCount = 1;

        //置顶
        msgTable.msgTime = DBConstant.STICK_TOP_BASE_TIME + 1 + DBManager.getInstance().getStickTopCount();

        return msgTable;
    }

    protected void findViewById(View view) {
        lin_go_to_top = view.findViewById(R.id.go_to_top);
        bg_messages_default = view.findViewById(R.id.bg_messages_default);
        no_match_tip = view.findViewById(R.id.no_match_tip);
        txt_xmp_status_title = view.findViewById(R.id.go_to_top_txt);
        not_connet_image = view.findViewById(R.id.not_connet);
        recyclerView = view.findViewById(R.id.recycler_view);
        swipe_refresh = view.findViewById(R.id.swipe_refresh);
        notify_rl = view.findViewById(R.id.notify_rl);
        edit_search = view.findViewById(R.id.edit_search);
        moments_msg_count = view.findViewById(R.id.moments_msg_count);
        root_ll = view.findViewById(R.id.root_ll);
        update_tv = view.findViewById(R.id.update_tv);
        start_push_tv = view.findViewById(R.id.start_push_tv);
        String startPushText = TheLApp.context.getResources().getString(R.string.start_right_now) + " >";
        start_push_tv.setText(startPushText);
        push_tips_rl = view.findViewById(R.id.push_tips_rl);
        close_push_tips_iv = view.findViewById(R.id.close_push_tips_iv);
        showPushTipsView();
    }

    protected void setListener() {

        bg_messages_default.setMessageParkEmptyListener(new MessageParkEmptyLayout.MessageParkEmptyListener() {
            @Override
            public void openPush() {
                ShareFileUtils.setLong(ShareFileUtils.PUSH_TIPS_CLOSE_TIME, System.currentTimeMillis());

                push_tips_rl.setVisibility(View.GONE);
            }
        });

        lin_go_to_top.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (MotionEvent.ACTION_DOWN == event.getAction()) {
                    if (System.currentTimeMillis() - lastClickTime < 300) {
                        ViewUtils.preventViewMultipleTouch(lin_go_to_top, 2000);
                        recyclerView.scrollToPosition(0);
                    }
                    lastClickTime = System.currentTimeMillis();
                }
                return true;
            }
        });

        textWatcher = new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filterArr.clear();
                if (TextUtils.isEmpty(s.toString().trim())) {
                    messageAdapter.refreshData(lastMsgWithUnreadList);
                    no_match_tip.setVisibility(View.GONE);
                } else {
                    for (MsgTable msgBean : lastMsgWithUnreadList) {
                        String nickNamePinYin = null;
                        try {
                            if (MsgBean.MSG_DIRECTION_TYPE_OUTGOING.equals(msgBean.msgDirection)) {// 发出
                                nickNamePinYin = PinyinUtils.cn2Spell(msgBean.toUserNickname);
                            } else {// 收到
                                nickNamePinYin = PinyinUtils.cn2Spell(msgBean.fromNickname);
                            }
                        } catch (Exception e) {
                        }
                        if (nickNamePinYin != null && nickNamePinYin.contains(PinyinUtils.cn2Spell(s.toString().trim()))) {
                            filterArr.add(msgBean);
                        }
                    }
                    messageAdapter.refreshData(filterArr);
                    if (filterArr.isEmpty()) {
                        no_match_tip.setVisibility(View.VISIBLE);
                    } else {
                        no_match_tip.setVisibility(View.GONE);
                    }
                }
            }
        };
        edit_search.addTextChangedListener(textWatcher);

        root_ll.getViewTreeObserver().addOnGlobalLayoutListener(mViewTreeObserver);

        root_ll.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return false;
            }
        });

        start_push_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = getActivity().getString(R.string.push_permission);
                if (message.equals(getActivity().getString(R.string.push_permission_oppo_vivo))) {
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                    intent.setData(uri);
                    getActivity().startActivity(intent);
                    traceMessageLog("click_follow", -1, "", "");

                } else {
                    //vivo和oppo的设置页无法设置权限，需要到'i 管家'app里设置
                    Intent vivoIntent = getActivity().getPackageManager().getLaunchIntentForPackage("com.iqoo.secure");
                    Intent oppoIntent = getActivity().getPackageManager().getLaunchIntentForPackage("com.coloros.safecenter");

                    if (vivoIntent != null) {
                        getActivity().startActivity(vivoIntent);
                    } else if (oppoIntent != null) {
                        getActivity().startActivity(oppoIntent);
                    } else {
                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                        intent.setData(uri);
                        getActivity().startActivity(intent);
                        traceMessageLog("click_follow", -1, "", "");

                    }
                }
            }
        });

        close_push_tips_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ShareFileUtils.setLong(ShareFileUtils.PUSH_TIPS_CLOSE_TIME, System.currentTimeMillis());

                push_tips_rl.setVisibility(View.GONE);

            }
        });

    }

    private void initList() {

        ViewUtils.initSwipeRefreshLayout(swipe_refresh);
        messageAdapter = new MessageAdapter(null, pageId, from_page, from_page_id);
        recyclerView.setLayoutManager(new MyLinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(false);
        mHeaderAndFooterRecyclerAdapter = new HeaderAndFooterRecyclerAdapter(messageAdapter);
        recyclerView.setAdapter(mHeaderAndFooterRecyclerAdapter);
        messageAdapter.setOnItemClickListener(new MessageAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, MsgTable msgTable) {

                L.d(TAG, " msgTable.isSystem : " + msgTable.isSystem);

                if (msgTable.userId.equals(DBConstant.Message.REQUEST_TABLE_NAME)) {
                    startActivity(new Intent(getActivity(), MyCircleRequestsActivity.class));
                    msgTable.unreadCount = 0;
                    ChatServiceManager.getInstance().updateMsgUnread(msgTable);
                    /**如果是陌生人消息**/
                } else if (msgTable.userId.equals(DBConstant.Message.STRANGER_TABLE_NAME)) {
                    startActivity(new Intent(getActivity(), StrangerMsgActivity.class));
                    traceMessageLog("click_stranger", -1, "", "");

                } else if (msgTable.userId.equals(DBConstant.Message.WINK_TABLE_NAME)) {
                    startActivity(new Intent(getContext(), WinksActivity.class));
                    msgTable.unreadCount = 0;
                    ChatServiceManager.getInstance().updateMsgUnread(msgTable);
                    traceMessageLog("click_wink", -1, "", "");

                } else if (msgTable.userId.equals(DBConstant.Message.MSG_HIDING)) {
                    Intent intent = new Intent(getActivity(), VipConfigActivity.class);
                    intent.putExtra("showType", VipConfigActivity.SHOW_MSG_SNEAK_UP);
                    intent.putExtra("fromPage", "message.list");
                    intent.putExtra("fromPageId", pageId);
                    traceChatAdUserLog("clickchatAD");

                    getActivity().startActivity(intent);
                } else if (msgTable.userId.equals(DBConstant.Message.MSG_AMUSEMENT_PARK)) {
                    msgTable.unreadCount = 0;
                    ChatServiceManager.getInstance().updateMsgUnread(msgTable);
                    Intent intent = new Intent(getActivity(), WebViewActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(BundleConstants.URL, "https://www.rela.me/game/#/chat?entry=appchatpush");
                    intent.putExtras(bundle);
                    getActivity().startActivity(intent);
                } else {
                    msgTable.unreadCount = 0;
                    ChatServiceManager.getInstance().updateMsgUnread(msgTable);

                    String toUserId, toName;

                    if (msgTable.msgDirection.equals("0")) {// 发出的
                        toUserId = msgTable.toUserId;
                        toName = msgTable.toUserNickname;
                    } else { // 收到的
                        toUserId = msgTable.fromUserId;
                        toName = msgTable.fromNickname;
                    }

                    if (msgTable.userId.equals(DBConstant.Message.FOLLOW_TABLE_NAME)) {
                        msgTable.isSystem = MsgBean.IS_FOLLOW_MSG;
                    } else {
                        msgTable.isSystem = MsgBean.IS_NORMAL_MSG;
                    }

                    Intent intent = new Intent(getActivity(), ChatActivity.class);
                    intent.putExtra("toUserId", toUserId);
                    intent.putExtra("toName", toName);
                    intent.putExtra("toAvatar", msgTable.avatar);
                    intent.putExtra("fromPage", "MessagesActivity");
                    intent.putExtra("isSystem", msgTable.isSystem);
                    startActivity(intent);
                    traceMessageLog("click_message", position, "", toUserId);
                    FireBaseUtils.uploadGoogle(TheLConstants.FireBaseConstant.VIEW_ITEM_CHAT, getActivity());

                }
            }
        });

        messageAdapter.setOnItemLongClickListener(new MessageAdapter.OnItemLongClickListener() {
            @Override
            public void onItemLongClick(View view, int position, MsgTable msgTable) {
                if (filterArr.size() > 0) {
                    return;
                }

                if (getActivity() != null) {

                    deleteMessage(msgTable, view);

                }

            }
        });

        notify_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FlutterPushImpl.Companion.getMomentsCheckBean().notReadCommentNum = 0;
                RfSBridgeHandlerFactory.getInstance().getFlutterPushImpl().pushNotRead();
                clearMsgRemind();
                FlutterRouterConfig.Companion.gotoMomentMsg();
                traceMessageLog("click_inform", -1, "", "");
            }
        });

        swipe_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshData();
            }
        });
    }

    public void traceMessageLog(String activity, int index, String rank_id, String userid) {
        try {
            LogInfoBean logInfoBean = new LogInfoBean();
            logInfoBean.page = "message.list";
            logInfoBean.page_id = pageId;
            logInfoBean.activity = activity;

            logInfoBean.lat = latitude;
            logInfoBean.lng = longitude;

            LogInfoBean.LogsDataBean logsDataBean = new LogInfoBean.LogsDataBean();

            if (index != -1) {
                logsDataBean.index = index;
            }

            if (!TextUtils.isEmpty(rank_id)) {
                logsDataBean.rank_id = rank_id;
            }

            if (!TextUtils.isEmpty(userid)) {
                logsDataBean.user_id = userid;
            }
            logInfoBean.data = logsDataBean;

            LiveLogUtils.getInstance().addLog(logInfoBean);

        } catch (Exception e) {

        }
    }

    private void clearMsgRemind() {
        if (moments_msg_count == null) {
            return;
        }
        moments_msg_count.setText("");
        moments_msg_count.setVisibility(View.GONE);
        ShareFileUtils.setInt(ShareFileUtils.UNREAD_MOMENTS_MSG_NUM, 0);

        //清除底部tab 数字提示

        if (getActivity() != null) {
            Intent intent = new Intent();
            intent.setAction(TheLConstants.BROADCAST_CLEAR_NEW_MOMENT_MSG);
            getActivity().sendBroadcast(intent);
        }
    }

    /**
     * 删除这个对话（刷新界面）
     *
     * @param toUserId
     */
    private void removeChatOnUI(String toUserId) {
        int i = 0;
        for (MsgTable msgTable : lastMsgWithUnreadList) {
            if (toUserId.equals(msgTable.getUserId())) {
                lastMsgWithUnreadList.remove(i);
                messageAdapter.refreshData(lastMsgWithUnreadList);
                break;
            }
            i++;
        }
        // 更新总未读消息数
        Intent intent = new Intent();
        intent.setAction(TheLConstants.BROADCAST_UPDATE_UNREAD_MSG_COUNT_COMPLETELY);
        TheLApp.getContext().sendBroadcast(intent);
    }


    private void refreshData() {
        if (TextUtils.isEmpty(edit_search.getText().toString().trim())) {

            ExecutorServiceUtils.getInstatnce().exec(new Runnable() {
                @Override
                public void run() {
                    L.d(TAG, " MessageFragment refreshData stranger : ");

                    getMsgList();

                }
            });
        }
    }

    // 注册广播接收器
    private void registerReceiver() {

        L.d(TAG, " 注册广播 ");

        receiver = new MessageReceiver();
        IntentFilter intentFilter = new IntentFilter();
        //接受新消息
        intentFilter.addAction(TheLConstants.BROADCAST_NEW_MESSAGE);
        intentFilter.addAction(TheLConstants.BROADCAST_REFRESH_MESSAGE);
        intentFilter.addAction(TheLConstants.BROADCAST_CLICK_MESSAGE);
        //挤眼
        intentFilter.addAction(TheLConstants.BROADCAST_WINK_USER_CHANGED);
        intentFilter.addAction(TheLConstants.BROADCAST_MOMENTS_CHECK_ACTION);
        intentFilter.addAction(TheLConstants.BROADCAST_RELATION_CHANGED);
        //刷新
        intentFilter.addAction(TheLConstants.BROADCAST_ACTION_REFRESH_NATIVE);
        getActivity().registerReceiver(receiver, intentFilter);
    }

    private MessageReceiver receiver;

    // 消息的广播接收器
    private class MessageReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();
            L.d(TAG,"onReceive " + action);
            if (action.equals(TheLConstants.BROADCAST_WINK_USER_CHANGED)) {//挤眼后更新条目
                final String userId = intent.getStringExtra(TheLConstants.BUNDLE_KEY_USER_ID);
                final int count = messageAdapter.getItemCount();
                for (int i = 0; i < count; i++) {
                    MsgTable msgWithUnreadBean = messageAdapter.getItem(i);
                    if (msgWithUnreadBean.getUserId().equals(userId)) {//如果是当前条目挤眼，则清空所有聊天未读消息
                        msgWithUnreadBean.unreadCount = 0;
                        break;
                    }
                }
                messageAdapter.notifyDataSetChanged();
            } else if (action.equals(TheLConstants.BROADCAST_CLICK_MESSAGE)) {
                refreshData();
            } else if (action.equals(TheLConstants.BROADCAST_ACTION_REFRESH_NATIVE)) {
                String index = intent.getStringExtra("index");
                if ("3".equals(index)) {
                    swipe_refresh.post(new Runnable() {
                        @Override
                        public void run() {
                            swipe_refresh.setRefreshing(true);
                            recyclerView.smoothScrollToPosition(0);
                            refreshData();
                        }
                    });
                }
            } else {// 清除朋友圈消息
                if (TheLConstants.BROADCAST_MOMENTS_CHECK_ACTION.equals(intent.getAction())) {// 收到消息的时候, 更新朋友圈消

                    MomentsCheckBean momentsCheckBean = intent.getParcelableExtra(TheLConstants.BUNDLE_KEY_MOMENTS_CHECK_BEAN);

                    try {
                        if (momentsCheckBean != null) {

                            L.d(TAG, " momentsCheckBean : " + momentsCheckBean.toString());

                            updateMsgRemind(momentsCheckBean.notReadCommentNum);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (TheLConstants.BROADCAST_CLEAR_MOMENT_MSG_COUNT_TOP.equals(intent.getAction())) {// 清除朋友圈消息
                    clearMsgRemind();
                }
                refreshData();
            }
        }
    }

    //在聊天的页面通知上显示数字提醒
    private void updateMsgRemind(int unreadMomentsMsg) {

        // 如果有未读的朋友圈消息
        if (unreadMomentsMsg > 0) {
            if (unreadMomentsMsg >= 99) {
                moments_msg_count.setText("99+");
            } else {
                moments_msg_count.setText(String.valueOf(unreadMomentsMsg));
            }
            moments_msg_count.setVisibility(View.VISIBLE);
        }
    }

    private ViewTreeObserver.OnGlobalLayoutListener mViewTreeObserver = new ViewTreeObserver.OnGlobalLayoutListener() {


        @Override
        public void onGlobalLayout() {
            if (edit_search != null && getActivity() != null) {
                Rect r = new Rect();
                getActivity().getWindow().getDecorView().getWindowVisibleDisplayFrame(r);
                //如果屏幕高度和Window可见区域高度差值大于整个屏幕高度的1/3，则表示软键盘显示中，否则软键盘为隐藏状态。

                int statusBarHeight = 0;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    statusBarHeight = ScreenUtils.getStatusHeight(getContext());
                }

                int heightDifference = ScreenUtils.getScreenHeight(getContext()) - (r.bottom - r.top);


                if (heightDifference - statusBarHeight == 0) {
                    edit_search.setCursorVisible(false);
                } else {
                    edit_search.setCursorVisible(true);
                }
            }
        }
    };

    public IMsgStatusCallback mIMsgStatusCallback = new IMsgStatusCallback.Stub() {


        @Override
        public void onTransferPercents(final int percents) {

            L.d(TAG, " onTransferPercents percents : " + percents);

            if (getActivity() != null) {

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (update_tv != null) {

                            String text = TheLApp.context.getResources().getString(R.string.updating_data);

                            String str = text + percents + "%";

                            update_tv.setText(str);

                        }

                    }
                });
            }

        }

        @Override
        public void onConnectStatus(final int status) {

            if (getActivity() != null) {

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        showConnectStatusUI(status);
                    }
                });
            }
        }

        @Override
        public void onMsgSendStatus(MsgBean msgBean) {

        }

        @Override
        public void onNewMsgComing(MsgBean msgBean) {

            L.d(TAG, " -------------onNewMsgComing-------------- " + msgBean.toString());

            if (MsgBean.MSG_TYPE_REMOVE_FROM_BLACK_LIST.equals(msgBean.msgType) || MsgBean.MSG_TYPE_ADD_TO_BLACK_LIST.equals(msgBean.msgType)) {

                BlackUtils.saveMsgToSP(msgBean);

            }

        }

        @Override
        public void onUpdateMsg(MsgTable msgTable) {

            L.d(TAG, " -------------onUpdateMsg-------------- " + msgTable.userId);

            if (msgTable.isStranger == 1) {
                return;
            }

            if (lastMsgWithUnreadList != null) {

                for (int i = 0; i < lastMsgWithUnreadList.size(); i++) {

                    MsgTable mt = lastMsgWithUnreadList.get(i);

                    if (mt.userId != null && msgTable.userId != null) {
                        if (mt.userId.equals(msgTable.userId)) {
                            mt.avatar = msgTable.avatar;
                            mt.msgTime = msgTable.msgTime;
                            mt.msgText = msgTable.msgText;
                            mt.msgType = msgTable.msgType;
                            mt.unreadCount = msgTable.unreadCount;
                            mt.isWinked = msgTable.isWinked;
                            mt.fromAvatar = msgTable.fromAvatar;
                            L.d(TAG, " 更新旧数据 : ");
                            break;
                        }
                    }
                    if (i == lastMsgWithUnreadList.size() - 1) {
                        L.d(TAG, " 新添加数据 : ");
                        lastMsgWithUnreadList.add(0, msgTable);
                        break;
                    }
                }

                if (lastMsgWithUnreadList.size() == 0) {
                    lastMsgWithUnreadList.add(msgTable);
                }

            }

            L.d(TAG, " MessageFragment onUpdateMsg notify : " + msgTable.unreadCount);

            if (getActivity() != null) {

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        L.d(TAG, " MessageFragment onUpdateMsg messageAdapter : " + messageAdapter);

                        bg_messages_default.refreshUI(lastMsgWithUnreadList.size() == 0);
                        if (messageAdapter != null && lastMsgWithUnreadList.size() > 0) {
                            messageAdapter.refreshData(lastMsgWithUnreadList);

                            pushUnReadCount();
                        }

                    }
                });
            }

        }

        @Override
        public void onDeleteMsgTable(final String userId) {

            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (messageAdapter != null && userId != null) {
                            if (lastMsgWithUnreadList != null) {
                                for (MsgTable msgTable : lastMsgWithUnreadList) {
                                    if (msgTable.userId.equals(userId)) {
                                        int unreadCount = msgTable.unreadCount;
                                        lastMsgWithUnreadList.remove(msgTable);
                                        messageAdapter.notifyDataSetChanged();
                                        if (unreadCount > 0) {
                                            pushUnReadCount();
                                        }
                                        break;
                                    }
                                }
                            }

                            L.d(TAG, " messageAdapter.getData().size() : " + messageAdapter.getData().size());
                            if (bg_messages_default != null)
                                bg_messages_default.refreshUI(lastMsgWithUnreadList.size() == 0);
                        }

                    }
                });
            }
        }

        @Override
        public void onChatInfo() {

        }
    };

    private void showConnectStatusUI(int status) {

        L.d(TAG, " showConnectStatusUI status : " + status);

        String text;
        switch (status) {
            case ChatService.ConnectStatus.STOP:
            case ChatService.ConnectStatus.FAILED:
            case ChatService.ConnectStatus.ERROR:
                text = TheLApp.context.getResources().getString(R.string.not_cennet);
                txt_xmp_status_title.setText(text);
                not_connet_image.setVisibility(View.VISIBLE);
                break;
            case ChatService.ConnectStatus.CONNECTED:

                if (lastMsgWithUnreadList == null || lastMsgWithUnreadList.size() == 0) {
                    reCountStrangerUnReadCount();
                    getMsgList();
                }
                not_connet_image.setVisibility(View.GONE);
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (isAdded()) {
                            txt_xmp_status_title.setText(getText(R.string.message_activity_title));
                        }
                    }
                }, 500);

                break;
            case ChatService.ConnectStatus.DATA_TRANSFER_BEGIN:
                break;
            case ChatService.ConnectStatus.RECONNECT:
            case ChatService.ConnectStatus.CONNECTING:
            default:
                text = TheLApp.context.getResources().getString(R.string.message_connecting);
                not_connet_image.setVisibility(View.GONE);
                txt_xmp_status_title.setText(text);
                break;
        }
    }

    private void getMsgList() {

        Flowable<List<MsgTable>> flowable = Flowable.create(new FlowableOnSubscribe<List<MsgTable>>() {
                            @Override
                            public void subscribe(FlowableEmitter<List<MsgTable>> e) throws Exception {
                                List<MsgTable> msgTables = ChatServiceManager.getInstance().getMsgList(0);
                                e.onNext(msgTables);
                                e.onComplete();
                            }
                        }, BackpressureStrategy.DROP);
        Flowable.zip(getUserInfoFlowable(), flowable, new BiFunction<MyInfoNetBean, List<MsgTable>, List<MsgTable>>() {
            @Override
            public List<MsgTable> apply(MyInfoNetBean myInfoNetBean, List<MsgTable> msgTables) throws Exception {
                if (myInfoNetBean != null && myInfoNetBean.data != null) {
                    partnerBean = myInfoNetBean.data.paternerd;
                } else {
                    partnerBean = null;
                }
                return msgTables;
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<List<MsgTable>>() {
            @Override
            public void onSubscribe(Subscription s) {
                s.request(Integer.MAX_VALUE);
            }

            @Override
            public void onNext(List<MsgTable> msgTables) {

                try {
                    lastMsgWithUnreadList.clear();

                    lastMsgWithUnreadList.addAll(msgTables);

                    long closeTime = ShareFileUtils.getLong(ShareFileUtils.MSG_HIDING_CLOSE_TIME, 0);

                    long closeTimeDelta = System.currentTimeMillis() - closeTime;

                    if (closeTimeDelta > 24 * 3600 * 1000 && getChattingUserCount() >= 3 && UserUtils.getUserVipLevel() <= 0) {

                        MsgTable msgTable = getMsgHidingMsgBean();

                        ChatServiceManager.getInstance().insertMsgTable(msgTable);

                        if (!isExist(DBConstant.Message.MSG_HIDING)) {
                            lastMsgWithUnreadList.add(ChatServiceManager.getInstance().getMsgTableByUserId(DBConstant.Message.MSG_HIDING));
                        }
                    }
                    L.d(TAG,"onNext switch:" + ShareFileUtils.getInt(ShareFileUtils.perfectMatch, 1)+" size:"+msgTables.size());
                    if (ShareFileUtils.getInt(ShareFileUtils.perfectMatch, 1) != 0 && !isParkMsgExclude() && isMatchCondition()) {//已打开匹配通知

                        //未读数一天只加一次
                        boolean hasAddToady = false;
                        String lastDate = ShareFileUtils.getString(ShareFileUtils.AMUSEMENT_PARK_UNREAD_COUNT_DATE, "");
                        if (!TextUtils.isEmpty(lastDate)) {
                            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                            Date last = df.parse(lastDate);
                            Date now = df.parse(df.format(new Date()));
                            Calendar nowTime = Calendar.getInstance();
                            nowTime.setTime(now);
                            Calendar lastTime = Calendar.getInstance();
                            lastTime.setTime(last);
                            if (!nowTime.after(lastTime)) {
                                hasAddToady = true;
                            }
                        }
                        L.d(TAG,"onNext --------enter "+hasAddToady );
                        if (!isExist(DBConstant.Message.MSG_AMUSEMENT_PARK)) {

                            if (!hasAddToady) {
                                MsgTable msgTable = getMsgRelaParkMsgBean();

                                ChatServiceManager.getInstance().insertMsgTable(msgTable);

                                lastMsgWithUnreadList.add(ChatServiceManager.getInstance().getMsgTableByUserId(DBConstant.Message.MSG_AMUSEMENT_PARK));

                                ShareFileUtils.setString(ShareFileUtils.AMUSEMENT_PARK_UNREAD_COUNT_DATE, DateUtils.getNowToDay());
                            }
                        } else {
                            if (!hasAddToady)
                                for (MsgTable msgTable : lastMsgWithUnreadList) {
                                    if (msgTable.userId.equals(DBConstant.Message.MSG_AMUSEMENT_PARK)) {
                                        msgTable.unreadCount++;
                                        ChatServiceManager.getInstance().updateMsgUnread(msgTable);
                                        ShareFileUtils.setString(ShareFileUtils.AMUSEMENT_PARK_UNREAD_COUNT_DATE, DateUtils.getNowToDay());
                                        break;
                                    }
                                }
                        }
                    }

                    sort(lastMsgWithUnreadList);

                    messageAdapter.refreshData(lastMsgWithUnreadList);

                    bg_messages_default.refreshUI(lastMsgWithUnreadList.size() == 0);

                    pushUnReadCount();

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onError(Throwable t) {
                if (swipe_refresh.isRefreshing()) swipe_refresh.setRefreshing(false);
            }

            @Override
            public void onComplete() {
                if (swipe_refresh.isRefreshing()) swipe_refresh.setRefreshing(false);
            }
        });

    }

    public void sort(List<MsgTable> lastMsgWithUnreadList) {
        try {
            Collections.sort(lastMsgWithUnreadList, new Comparator<MsgTable>() {
                @Override
                public int compare(MsgTable msgTable, MsgTable t1) {

                    if (msgTable == null || msgTable.msgTime == null || t1 == null || t1.msgTime == null) {
                        return 0;
                    }

                    if (msgTable.msgTime > t1.msgTime) {
                        return -1;
                    } else if (msgTable.msgTime.equals(t1.msgTime)) {
                        return 0;
                    } else {
                        return 1;
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private Flowable<MyInfoNetBean> getUserInfoFlowable() {
        String cursor = ShareFileUtils.getString(ShareFileUtils.MATCH_NEW_LIKE_ME_COUNT, "");

        Flowable<MyInfoNetBean> flowable = DefaultRequestService.createUserRequestService().getMyInfo(cursor);
        return flowable;
    }

    /*
    * i. 更新后用户首次登陆app时
    * ii. 向18:55-19:05期间活跃的用户推送一条通知
    * */
    private boolean isMatchCondition() {
        if (ShareFileUtils.getBoolean(ShareFileUtils.PerfectMatch.IS_FIRST_OPEN,true)) {//匹配规则i
            ShareFileUtils.setBoolean(ShareFileUtils.PerfectMatch.IS_FIRST_OPEN,false);
            ShareFileUtils.setString(ShareFileUtils.AMUSEMENT_PARK_UNREAD_COUNT_DATE, "");//重新置空当天时间
            L.d(TAG,"match condition is first open");
            return true;
        }
        if (DateUtils.parkMsgActiveTime()) {//匹配规则ii
            L.d(TAG,"match condition is active time");
            return true;
        }
        return false;
    }

    /*
    * i. 通知对象过滤掉个人资料的感情状态中选择“稳定关系”和“已婚”的用户，以及绑定了情侣的用户。
    * ii. 之前已经关闭了匹配通知的用户，本次优化后不再推送通知。
    * 映射表参照 QueueConstants
    * */
    private boolean isParkMsgExclude() {
//        String role = ShareFileUtils.getString(ShareFileUtils.ROLE_NAME, "");//角色
        String affection = ShareFileUtils.getString(ShareFileUtils.AFFECTION, "");//感情状态
        if ("3".equals(affection) || "4".equals(affection)) {//匹配规则i中的“稳定关系”和“已婚”的用户
            L.d(TAG,"exclude condition affection ");
            return true;
        }
        if (partnerBean != null) {//匹配规则i中的 绑定了情侣的用户
            L.d(TAG,"exclude condition have parter ");
            return true;
        }
        if (ShareFileUtils.getInt(ShareFileUtils.perfectMatch, 1) == 0) {//匹配规则ii
            L.d(TAG,"exclude condition have close noti ");
            return true;
        }
        return false;
    }

    private void pushUnReadCount() {

        try {
            int unReadCount = 0;

            if (lastMsgWithUnreadList != null) {

                for (int i = 0; i < lastMsgWithUnreadList.size(); i++) {
                    MsgTable msgTable = lastMsgWithUnreadList.get(i);
                    if (msgTable != null) {
                        unReadCount += msgTable.unreadCount;
                    }
                }

                FlutterPushImpl.Companion.getMomentsCheckBean().notReadChat = unReadCount;

                RfSBridgeHandlerFactory.getInstance().getFlutterPushImpl().pushNotRead();

                // 更新总未读消息数
                Intent intent = new Intent();
                intent.setAction(TheLConstants.BROADCAST_UPDATE_UNREAD_MSG_COUNT);
                intent.putExtra(TheLConstants.COUNT, unReadCount);
                TheLApp.getContext().sendBroadcast(intent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void reCountStrangerUnReadCount() {

        Flowable.create(new FlowableOnSubscribe<List<MsgTable>>() {
            @Override
            public void subscribe(FlowableEmitter<List<MsgTable>> e) throws Exception {

                List<MsgTable> msgTables = ChatServiceManager.getInstance().getMsgList(1);

                e.onNext(msgTables);

                e.onComplete();
            }
        }, BackpressureStrategy.DROP).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<List<MsgTable>>() {
            @Override
            public void onSubscribe(Subscription s) {
                s.request(Integer.MAX_VALUE);
            }

            @Override
            public void onNext(List<MsgTable> msgTables) {
                if (msgTables != null && msgTables.size() > 0) {

                    int unReadCount = 0;

                    int isWinked = 0;

                    for (MsgTable msgTable : msgTables) {
                        unReadCount += msgTable.unreadCount;
                        isWinked += msgTable.isWinked;
                    }

                    if (isWinked > 0) {
                        isWinked = 1;
                    } else {
                        isWinked = 0;
                    }

                    ChatServiceManager.getInstance().updateStrangerUnreadCount(unReadCount, isWinked);
                }
            }

            @Override
            public void onError(Throwable t) {

            }

            @Override
            public void onComplete() {

            }
        });

    }

    private int getChattingUserCount() {

        int chattingUserCount = 0;

        try {

            if (lastMsgWithUnreadList == null) {
                return chattingUserCount;
            }

            for (int i = 0; i < lastMsgWithUnreadList.size(); i++) {

                MsgTable msgTable = lastMsgWithUnreadList.get(i);
                if (msgTable != null && !TextUtils.isEmpty(msgTable.userId)) {
                    boolean isUser = !msgTable.userId.equals(DBConstant.Message.REQUEST_TABLE_NAME)
                            && !msgTable.userId.equals(DBConstant.Message.STRANGER_TABLE_NAME)
                            && !msgTable.userId.equals(DBConstant.Message.WINK_TABLE_NAME)
                            && !msgTable.userId.equals(DBConstant.Message.MSG_HIDING);

                    if (isUser) {
                        chattingUserCount++;
                    }
                }


            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return chattingUserCount;

    }

    private boolean isExist(String userId) {
        for (MsgTable msgTable : lastMsgWithUnreadList) {
            if (!TextUtils.isEmpty(msgTable.userId) && msgTable.userId.equals(userId)) {
                return true;
            }
        }
        return false;
    }

    private void showPushTipsView() {

        if (getActivity() == null) {
            return;
        }

        long time = ShareFileUtils.getLong(ShareFileUtils.PUSH_TIPS_CLOSE_TIME, 0);

        long currentTime = System.currentTimeMillis();

        NotificationManagerCompat manager = NotificationManagerCompat.from(TheLApp.context);

        boolean isOpened = manager.areNotificationsEnabled();

        if (!isOpened && (time == 0 || currentTime - time > TheLConstants.DAY * 30)) {

            push_tips_rl.setVisibility(View.VISIBLE);

        } else {
            push_tips_rl.setVisibility(View.GONE);

        }
    }

    public class IDeleteMsg implements DeleteMsgDialog.OnDeleteMsgListener {

        private String userId;

        private MsgTable msgTable;

        public IDeleteMsg(MsgTable msgTable) {
            this.userId = msgTable.userId;
            this.msgTable = msgTable;
        }

        @Override
        public void onConfirm() {

            switch (msgTable.userId) {
                case DBConstant.Message.WINK_TABLE_NAME:
                    ChatServiceManager.getInstance().dropTable(DBConstant.Message.WINK_TABLE_NAME);
                    ChatServiceManager.getInstance().deleteMsgTable(DBConstant.Message.WINK_TABLE_NAME);
                    break;
                case DBConstant.Message.FOLLOW_TABLE_NAME:
                    ChatServiceManager.getInstance().dropTable(DBConstant.Message.FOLLOW_TABLE_NAME);
                    ChatServiceManager.getInstance().deleteMsgTable(DBConstant.Message.FOLLOW_TABLE_NAME);
                    break;
                case DBConstant.Message.STRANGER_TABLE_NAME:
                    ChatServiceManager.getInstance().dropTable(DBConstant.Message.STRANGER_TABLE_NAME);
                    ChatServiceManager.getInstance().deleteMsgTable(DBConstant.Message.STRANGER_TABLE_NAME);

                    List<MsgTable> strangerMsg = ChatServiceManager.getInstance().getStrangerMsg();

                    if (strangerMsg != null) {
                        for (MsgTable msgTable : strangerMsg) {
                            ChatServiceManager.getInstance().dropTable(msgTable.userId);
                            ChatServiceManager.getInstance().deleteMsgTable(msgTable.userId);
                        }
                    }

                    break;
                case DBConstant.Message.REQUEST_TABLE_NAME:
                    messageAdapter.removeData(msgTable);
                    break;
                default:

                    MsgTable mt = ChatServiceManager.getInstance().getMsgTableByUserId(userId);

                    String tableName = DBUtils.getChatTableName(userId);
                    ChatServiceManager.getInstance().dropTable(tableName);
                    ChatServiceManager.getInstance().deleteMsgTable(userId);

                    if (mt != null) {
                        if (mt.isStranger == 1) {
                            List<MsgTable> msgTables = ChatServiceManager.getInstance().getStrangerMsg();

                            if (msgTables.size() <= 0) {
                                ChatServiceManager.getInstance().removeMsgTable(DBUtils.getStrangerTableName());
                            } else {

                                MsgTable msgTable = msgTables.get(msgTables.size() - 1);

                                MsgTable strangerMsgTable = ChatServiceManager.getInstance().getMsgTableByUserId(DBConstant.Message.STRANGER_TABLE_NAME);

                                strangerMsgTable.userName = msgTable.userName;
                                strangerMsgTable.msgType = msgTable.msgType;
                                strangerMsgTable.msgText = msgTable.msgText;
                                if (strangerMsgTable.msgTime < DBConstant.STICK_TOP_BASE_TIME) {
                                    strangerMsgTable.msgTime = msgTable.msgTime;
                                }

                                ChatServiceManager.getInstance().updateMsgTableWithWink(strangerMsgTable);

                                ChatServiceManager.getInstance().pushUpdateMsg(strangerMsgTable);

                            }
                        }

                    }

                    break;
            }
        }

        @Override
        public void onCancel() {

        }
    }

    private void deleteMessage(MsgTable msgTable, View view) {
        switch (msgTable.userId) {
            case DBConstant.Message.WINK_TABLE_NAME:
            case DBConstant.Message.FOLLOW_TABLE_NAME:
            case DBConstant.Message.STRANGER_TABLE_NAME:
            case DBConstant.Message.REQUEST_TABLE_NAME:
                showDeleteMenu(msgTable, view);
                break;
            default:
                showDeleteMenuAndReport(msgTable, view);
                break;

        }
    }

    private void showDeleteMenu(MsgTable msgTable, View view) {
        PopupMenu popupMenu = new PopupMenu(view.getContext(), view);

        popupMenu.setGravity(Gravity.RIGHT);

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.delete_message:
                        DeleteMsgDialog deleteMsgDialog = new DeleteMsgDialog();
                        deleteMsgDialog.setOnDeleteMsgListener(new IDeleteMsg(msgTable));
                        deleteMsgDialog.show(getActivity().getSupportFragmentManager(), DeleteMsgDialog.class.getName());
                        break;
                }
                return false;
            }
        });
        popupMenu.inflate(R.menu.menu_message_delete);
        popupMenu.show();

    }

    private void showDeleteMenuAndReport(MsgTable msgTable, View view) {

        PopupMenu popupMenu = new PopupMenu(view.getContext(), view);

        popupMenu.setGravity(Gravity.RIGHT);

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.delete_message:
                        DeleteMsgDialog deleteMsgDialog = new DeleteMsgDialog();
                        deleteMsgDialog.setOnDeleteMsgListener(new IDeleteMsg(msgTable));
                        deleteMsgDialog.show(getActivity().getSupportFragmentManager(), DeleteMsgDialog.class.getName());
                        break;
                    case R.id.report_message:
                        Intent intent = new Intent(getActivity(), ReportActivity.class);
                        intent.putExtra(TheLConstants.BUNDLE_KEY_REPORT_TYPE, ReportActivity.REPORT_TYPE_REPORT_USER);
                        intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, msgTable.userId);
                        startActivity(intent);
                        break;
                }
                return false;
            }
        });
        popupMenu.inflate(R.menu.menu_message_delete_or_report);
        popupMenu.show();
    }

    public void traceChatAdUserLog(String activity) {
        try {

            LogInfoBean logInfoBean = new LogInfoBean();
            logInfoBean.page = "message.list";
            logInfoBean.page_id = pageId;
            logInfoBean.activity = activity;

            MatchLogUtils.getInstance().addLog(logInfoBean);

        } catch (Exception e) {

        }
    }
}