package com.thel.modules.main.discover;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseFragment;
import com.thel.bean.AdBean;
import com.thel.bean.live.LiveRoomsBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.imp.AutoRefreshImp;
import com.thel.imp.TittleClickListener;
import com.thel.modules.live.bean.LiveClassifyBean;
import com.thel.modules.live.bean.LiveRoomBean;
import com.thel.modules.live.surface.watch.LiveWatchActivity;
import com.thel.modules.live.surface.watch.horizontal.LiveWatchHorizontalActivity;
import com.thel.modules.main.discover.adapter.LiveClassifyItemAdapter;
import com.thel.modules.main.discover.utils.DiscoverUtils;
import com.thel.modules.main.discover.view.LivingUserRoomView;
import com.thel.modules.main.me.bean.LogInfoBean;
import com.thel.modules.main.me.match.eventcollect.collect.LiveLogUtils;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.ui.layoutmanager.MyGridLayoutManager;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.zoom.LiveBannersLayout;
import com.thel.utils.BusinessUtils;
import com.thel.utils.DialogUtil;
import com.thel.utils.L;
import com.thel.utils.LocationUtils;
import com.thel.utils.PhoneUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 直播列表分类直播 推荐主播和正在在直播
 * Created by lingwei on 2017/11/19.
 */

public class LiveClassifyFragment2 extends BaseFragment implements LiveClassifyRoomContact.View, TittleClickListener, AutoRefreshImp {
    private ArrayList<LiveRoomBean> list = new ArrayList<>();
    //LiveRoomBean的id的HashSet
    private Set<String> liveRoomBeanIdSets = new HashSet<>();
    public LiveClassifyItemAdapter adapter;
    protected LiveClassifyRoomContact.Presenter presenter;
    private LiveClassifyBean liveClassifyBean;

    private int cursor = 0;//正常数据cursor
    private int recommendCursor = 0;//有推荐列表数据的推荐列表cursor
    //    private final int TYPE_HOT = 0;
//    private final int TYPE_NEW = -1;
//    private final int TYPE_NEIGHBOUR = -2;
//    private final int TYPE_AUDIO = -3;
//    private final int TYPE_VEDIO = 1;
    public static final int TYPE_HOT = -1;
    public static final int TYPE_VIDEO = 0;
    public static final int TYPE_RADIO = 1;
    public static final int TYPE_HOT_CHAT = 2;
    private LivingUserRoomView livingUserRoomView;
    private SwipeRefreshLayout swipe_container;
    private Handler mHandler = new Handler(Looper.getMainLooper());
    private RecyclerView recyclerview;
    private LiveBannersLayout bannerImage;
    private LinearLayout mBannerHeader;
    //    private RefreshLisener refreshListener;
    private boolean isFirst = false; // 第一次呈现
    private boolean isCreate = false;
    private long lastRefreshTime;
    private LocationManager locationManager;
    private Location mLocation;
    private int ret;
    private String pageId;

    public static Fragment newInstance(Bundle bundle) {
        LiveClassifyFragment2 instance = new LiveClassifyFragment2();
        instance.setArguments(bundle);
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            liveClassifyBean = (LiveClassifyBean) bundle.getSerializable(TheLConstants.BUNDLE_KEY_MOMENT_BEAN);
        }

        checkGoogleServiceAvailable();
//        if (liveClassifyBean != null && TYPE_NEIGHBOUR == liveClassifyBean.id) {
//            buildGoogleApiClient();
//
//        }
        pageId = Utils.getPageId();

        isFirst = true;
    }

    private void checkGoogleServiceAvailable() {
        buildGaodeMapClient();
        //不支持时，可以利用getErrorDialog得到一个提示框, 其中第2个参数传入错误信息
        //提示框将根据错误信息，生成不同的样式
        //例如，我自己测试时，第一次Google Play Service不是最新的，
        //对话框就会显示这些信息，并提供下载更新的按键
    }

    private void buildGaodeMapClient() {
        final AMapLocationClient mLocationClient = new AMapLocationClient(TheLApp.getContext());
        mLocationClient.setLocationListener(new AMapLocationListener() {
            @Override
            public void onLocationChanged(AMapLocation aMapLocation) {
                if (aMapLocation != null && aMapLocation.getErrorCode() == 0) {
                    LocationUtils.saveLocation(aMapLocation.getLatitude(), aMapLocation.getLongitude(), aMapLocation.getCity());

                } else {
                    LocationUtils.saveLocation(0.0, 0.0, "");

                }

                mLocationClient.stopLocation();
                mLocationClient.onDestroy();
            }
        });
        final AMapLocationClientOption mLocationOption = new AMapLocationClientOption();
        mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Battery_Saving);
        mLocationClient.setLocationOption(mLocationOption);
        mLocationClient.startLocation();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBannerHeader = (LinearLayout) inflater.inflate(R.layout.live_main_list_banner, null, false);
        return inflater.inflate(R.layout.recommend_live_user_list, container, false);
    }

    private void initBanner(List<String> advs, List<AdBean.Map_list> ad_list) {
        if (bannerImage == null) {
            bannerImage = mBannerHeader.findViewById(R.id.banner); // 获取Gallery组件
            int galleryHeight = (int) (TheLApp.getContext().getResources().getDisplayMetrics().widthPixels / TheLConstants.LIVE_LIST_BANNER);
            bannerImage.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, galleryHeight));

            // 添加点击事件的监听
            bannerImage.setOnBannerItemClickListener(new LiveBannersLayout.OnBannerItemClickListener() {
                @Override
                public void onItemClick(int position) {
                    BusinessUtils.AdRedirect(ad_list.get(position));
                }
            });

            if (liveClassifyBean != null && liveClassifyBean.id == TYPE_HOT) {
                adapter.addHeaderView(mBannerHeader);
            }
        }

        bannerImage.setShowIndicator(advs.size() > 1);
        bannerImage.setViewUrls(advs);
    }

    private void getAdsData() {
        presenter.loadAdvert();
    }

    @Override
    public void refreshAdArea(AdBean adBean) {
        if (adBean == null || adBean.data == null || adBean.data.map_list == null) {
            return;
        }
        //原朋友圈广告
        final List<AdBean.Map_list> ad_list = adBean.data.map_list;
        List<AdBean.Map_list> willDelete = new ArrayList<>();
        for (AdBean.Map_list bean : ad_list) {
            if (!bean.advertLocation.equals("live")) {
                willDelete.add(bean);
            }

        }
        ad_list.removeAll(willDelete);
        if (!ad_list.isEmpty()) {
            List<String> advs = new ArrayList<>();
            for (int i = 0; i < ad_list.size(); i++) {
                advs.add(ad_list.get(i).advertURL);
            }

            initBanner(advs, ad_list);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        new LiveClassifyPresenter(this);
        findviewById(view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        isCreate = true;
        if (getUserVisibleHint()) {
            isFirst = false;
            if (swipe_container != null)
                swipe_container.post(new Runnable() {
                    @Override
                    public void run() {
                        swipe_container.setRefreshing(true);
                    }
                });
            getBaseRefreshData();

        }
        setListener();
    }

    protected void findviewById(View view) {
        recyclerview = view.findViewById(R.id.recyclerview);
        swipe_container = view.findViewById(R.id.swipe_container);
        ViewUtils.initSwipeRefreshLayout(swipe_container);
        initapter();

    }

    private void initapter() {
        adapter = getTypeAdapter();
        recyclerview.setHasFixedSize(true);
        recyclerview.setLayoutManager(new MyGridLayoutManager(getActivity(), 2));
        final float padding = TheLApp.context.getResources().getDimension(R.dimen.live_new_user_item_padding);
        DiscoverUtils.setLiveClassifyRecyclerViewDecoration(recyclerview, padding);

        recyclerview.setAdapter(adapter);

    }

    private LiveClassifyItemAdapter getTypeAdapter() {
        if (liveClassifyBean == null) {
            return new LiveClassifyItemAdapter(R.layout.adapter_liveclassfify_living_item, list, LiveClassifyItemAdapter.TYPE_LIVEING, pageId, liveClassifyBean.id);
        } else {
            switch (liveClassifyBean.id) {
                case TYPE_HOT:
//                case TYPE_AUDIO:
//                case TYPE_NEIGHBOUR:
//                case TYPE_NEW:
//                case TYPE_VEDIO:
                case TYPE_VIDEO:
                case TYPE_RADIO:
                case TYPE_HOT_CHAT:
                    return new LiveClassifyItemAdapter(R.layout.adapter_liveclassfify_living_item, list, LiveClassifyItemAdapter.TYPE_LIVEING, pageId, liveClassifyBean.id);

                default:
                    return new LiveClassifyItemAdapter(R.layout.adapter_liveclassfify_recommend_item, list, LiveClassifyItemAdapter.TYPE_RECOMMEND, pageId, liveClassifyBean.id);

            }
        }
    }


    private void setListener() {

        adapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                ViewUtils.preventViewMultipleClick(view, 2000);

                if (liveClassifyBean != null) {
                    switch (liveClassifyBean.id) {
                        case TYPE_HOT:
//                        case TYPE_AUDIO:
//                        case TYPE_NEIGHBOUR:
//                        case TYPE_NEW:
//                        case TYPE_VEDIO:
                        case TYPE_VIDEO:
                        case TYPE_RADIO:
                        case TYPE_HOT_CHAT:
                            if (position < adapter.getItemCount()) {
                                gotoLiveShow(adapter.getItem(position), position);
                            }
                            break;
                        default:
                            LiveRoomBean roomBean = adapter.getItem(position);
                            if (roomBean.active == 0) {
                                goToUserinfoActivity(position);

                            } else {
                                gotoLiveShow(roomBean, position);
                            }
                            break;
                    }
                }

            }
        });
        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {

                getBaseRefreshData();

            }
        });
        adapter.setOnLoadMoreListener(new BaseRecyclerViewAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                if (cursor != 0) {//说明有更多数据
                    //  getBaseMoreData();
                    getBaseMoreData(cursor);
                } else {
                    try {
                        adapter.closeLoadMore(getContext(), recyclerview);
                    } catch (Exception e) {
                        L.d("livingUserRoomView", "error：" + e.getMessage());
                    }

                }
            }
        });
        adapter.setReloadMoreListener(new BaseRecyclerViewAdapter.ReloadMoreListener() {
            @Override
            public void reloadMore() {
                adapter.removeAllFooterView();
                adapter.openLoadMore(true);
                getBaseMoreData(cursor);

            }
        });
    }

    /**
     * 跳转到直播页面
     *
     * @param position
     */
    private void gotoLiveShow(LiveRoomBean liveRoomNetBean, int position) {
        String userId = String.valueOf(adapter.getItem(position).user.id);
        if (BusinessUtils.canIntoLiveRoom(TheLApp.getContext(), userId)) {
            MobclickAgent.onEvent(getActivity(), "enter_live_room_from_list");
            if (getActivity() != null) {
                if (liveRoomNetBean.isLandscape == 0) {

                    ArrayList<LiveRoomBean> liveRoomBeans = new ArrayList<>();

                    if (list.size() > 0) {
//                        LiveRoomBean lrnb = list.get(0);
//                        if (lrnb.isLandscape == 1) {
//                            liveRoomBeans.addAll(list.subList(1, list.size()));
//                        } else {
//                            liveRoomBeans = list;
//                        }
                        liveRoomBeans = list;
                    }

                    Intent intent = new Intent(TheLApp.getContext(), LiveWatchActivity.class);
                    final Bundle bundle = new Bundle();
                    bundle.putString(LiveWatchActivity.FROM_PAGE, LiveWatchActivity.FROM_PAGE_LIVE_ROOM_LIST_FRAGMENT);
                    bundle.putSerializable(TheLConstants.BUNDLE_KEY_LIVEROOMBEAN_LIST, liveRoomBeans);
                    bundle.putInt(TheLConstants.BUNDLE_KEY_POSITION, position);
                    bundle.putInt(TheLConstants.BUNDLE_KEY_CURSOR, cursor);
                    intent.putExtras(bundle);
                    getActivity().startActivity(intent);
                } else {
                    Intent intent = new Intent(TheLApp.getContext(), LiveWatchHorizontalActivity.class);
                    final Bundle bundle = new Bundle();
                    bundle.putString(TheLConstants.BUNDLE_KEY_ID, liveRoomNetBean.id);
                    intent.putExtras(bundle);
                    getActivity().startActivity(intent);
                }

            }

        } else {

            DialogUtil.showAlert(getActivity(), "", getActivity().getString(R.string.info_had_bean_added_to_black), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        }
        reportLiveLog("click", liveRoomNetBean.rank_id, liveRoomNetBean.id, liveRoomNetBean.liveUsersCount, liveClassifyBean.id, position);
    }

    private void reportLiveLog(String activity, String rank_id, String live_id, int viewer, int sort, int position) {
        String latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
        String longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");
        try {
            LogInfoBean logInfoBean = new LogInfoBean();
            logInfoBean.page = "live";
            logInfoBean.page_id = pageId;

            logInfoBean.activity = activity;
            LogInfoBean.LogsDataBean logsDataBean = new LogInfoBean.LogsDataBean();
            logsDataBean.rank_id = rank_id;
            logsDataBean.live_id = live_id;
            logsDataBean.viewer = viewer;
            logsDataBean.index = position;
            switch (sort) {
                case TYPE_HOT:
                    logsDataBean.sort = "hot";
                    break;
//                case TYPE_AUDIO:
//                    logsDataBean.sort = "voice";
//                    break;
//                case TYPE_NEIGHBOUR:
//                    logsDataBean.sort = "near";
//                    break;
//                case TYPE_NEW:
//                    logsDataBean.sort = "new";
//                    break;
//                case TYPE_VEDIO:
//                    logsDataBean.sort = "video";
//                    break;
                case TYPE_VIDEO:
                    logsDataBean.sort = "video";
                    break;
                case TYPE_RADIO:
                    logsDataBean.sort = "radio";
                    break;
                case TYPE_HOT_CHAT:
                    logsDataBean.sort = "hot_chat";
                    break;
                default:
                    logsDataBean.sort = "video";
                    break;

            }
            logInfoBean.data = logsDataBean;
            logInfoBean.lat = latitude;
            logInfoBean.lng = longitude;

            LiveLogUtils.getInstance().addLog(logInfoBean);

        } catch (Exception e) {

        }

    }

    private void getBaseRefreshData() {
        lastRefreshTime = System.currentTimeMillis();
        if (liveClassifyBean == null) {
            return;
        }
        cursor = 0;
        switch (liveClassifyBean.id) {
            case TYPE_HOT://热门
                presenter.getRefreshHotRoomListData();
                getAdsData();
                break;
//            case TYPE_NEW://新人 ,正在直播新人列表，推荐新人直播列表
//                presenter.getRefreshNewsLiveListData();
//                break;
//            case TYPE_NEIGHBOUR://附近
//                presenter.getRefreshNeighboursListData();
//                break;
//            case TYPE_AUDIO://音频
//                presenter.getRefreshAudioListData();
//                break;
//            case TYPE_VEDIO: //视频
//                presenter.getRefreshVedioListData();
//                break;
            case TYPE_VIDEO:
                presenter.getRefreshVedioListData();
                break;
            case TYPE_RADIO:
                presenter.getRefreshRadioListData();
                break;
            case TYPE_HOT_CHAT:
                presenter.getRefreshHotChatListData();
                break;
            default://别的
                presenter.getRefreshLiveListWithTypeData(liveClassifyBean.id);
                presenter.getRefreshRecommendFollowListTypeData(liveClassifyBean.id);
                break;
        }
    }

    private void getBaseMoreData(int cursor) {
        if (liveClassifyBean == null) {
            return;
        }
        switch (liveClassifyBean.id) {
            case TYPE_HOT://热门
                presenter.getMoreHotRoomListData(cursor);
                break;
//            case TYPE_NEW://新人 ,正在直播新人列表
//                //   presenter.getMoreRecommedNewAnchorsData(cursor);
//                presenter.getMoreNewsLiveListData(cursor);
//                break;
//            case TYPE_NEIGHBOUR://附近
//                presenter.getMoreNeighboursListData(cursor);
//                break;
//            case TYPE_AUDIO://音频
//                presenter.getMoreAudioListData(cursor);
//                break;
//            case TYPE_VEDIO://视频
//                presenter.getMoreVedioListData(cursor);
//                break;
            case TYPE_VIDEO:
                presenter.getMoreVedioListData(cursor);
                break;
            case TYPE_RADIO:
                presenter.getMoreRadioListData(cursor);
                break;
            case TYPE_HOT_CHAT:
                presenter.getMoreHotChatListData(cursor);
                break;
            default://别的
                presenter.getMoreRecommendFollowListTypeData(liveClassifyBean.id, this.cursor);
                break;
        }
    }

    public void scrollToPositionAndRefresh() {
        if (swipe_container != null)
            swipe_container.post(new Runnable() {
                @Override
                public void run() {
                    swipe_container.setRefreshing(true);
                }
            });
        if (recyclerview != null) {
            recyclerview.smoothScrollToPosition(0);
        }

        getBaseRefreshData();
    }

    @Override
    public void tryRefreshData() {
        if (getUserVisibleHint() && !isFirst && liveClassifyBean != null && !(PhoneUtils.getNetWorkType() == PhoneUtils.TYPE_NO)) {//如果不是第一次创建且为可见状态,且有网络
            if (lastRefreshTime != 0 && System.currentTimeMillis() - lastRefreshTime > REFRESH_RATE && swipe_container != null && !swipe_container.isRefreshing()) {//如果大于刷新时间并且不再刷新状态，则刷新
                scrollToPositionAndRefresh();
            }
        }
    }

    /**
     * 直播列表
     */
    private void showBaseRefreshData(LiveRoomsBean data) {
        cursor = data.cursor;
        list.clear();
        list.addAll(data.list);
        buildRoomBeanIdSet();
        adapter.removeAllFooterView();
        adapter.setNewData(list);
        adapter.openLoadMore(list.size(), true);


    }

    private void showBaseMoreData(LiveRoomsBean data) {
        cursor = data.cursor;
        ArrayList<LiveRoomBean> newList = handleResponseList(data.list);
        list.addAll(newList);
        adapter.openLoadMore(list.size(), true);
        adapter.notifyDataChangedAfterLoadMore(true, list.size());
    }

    @Override
    public void setPresenter(LiveClassifyRoomContact.Presenter presenter) {
        this.presenter = presenter;
    }

    private void showMoreRecommendNew(LiveRoomsBean data) {
        recommendCursor = data.cursor;
        list.addAll(data.list);
        adapter.notifyDataChangedAfterLoadMore(list, true);
    }

    private void buildRoomBeanIdSet() {
        liveRoomBeanIdSets.clear();
        for (LiveRoomBean bean:list) {
            liveRoomBeanIdSets.add(bean.id);
        }
    }

    /*
    *  直播列表分页数据去重复
    * */
    private ArrayList<LiveRoomBean> handleResponseList(ArrayList<LiveRoomBean> moreList) {
        ArrayList<LiveRoomBean> tempList = new ArrayList<>();
        for (LiveRoomBean bean:moreList) {
            if (!liveRoomBeanIdSets.contains(bean.id)) {
                liveRoomBeanIdSets.add(bean.id);
                tempList.add(bean);
            }
        }
        return tempList;
    }

    /**
     * 显示刷新热门直播列表数据
     *
     * @param data
     */
    @Override
    public void showRefreshHotRoomListData(LiveRoomsBean data) {
        showBaseRefreshData(data);
    }

    /**
     * 显示更多热门直播列表数据
     *
     * @param data
     */
    @Override
    public void showMoreHotRoomListData(LiveRoomsBean data) {
        showBaseMoreData(data);
    }

    /**
     * 显示 刷新 新人正在直播列表
     *
     * @param data
     */
    @Override
    public void showRefreshNewsLiveListData(LiveRoomsBean data) {
        //  showRefreshHeaderData(data);
        showBaseRefreshData(data);

    }

    /**
     * 显示 更多 新人正在直播列表
     *
     * @param data
     */
    @Override
    public void showMoreNewsLiveListData(LiveRoomsBean data) {
        // showMoreHeaderData(data);
        showBaseMoreData(data);
    }

    /**
     * 显示刷新推荐新人列表
     *
     * @param data
     */
    @Override
    public void showRefreshRecommandNewAnchorsData(LiveRoomsBean data) {
        showBaseRefreshData(data);
    }

    /**
     * 显示 更多 推荐新人直播列表
     *
     * @param data
     */
    @Override
    public void showMoreRecommandNewAnchorsData(LiveRoomsBean data) {
        showBaseMoreData(data);
    }

    /**
     * 显示 刷新 附近直播列表
     *
     * @param data
     */
    @Override
    public void showRefreshNeighboursListData(LiveRoomsBean data) {
        showBaseRefreshData(data);
    }

    /**
     * 显示  更多附近直播列表
     *
     * @param data
     */
    @Override
    public void showMoreNeighboursListData(LiveRoomsBean data) {
        showBaseMoreData(data);
    }

    /**
     * 显示 刷新 音频直播列表
     *
     * @param data
     */
    @Override
    public void showRefreshAudioListData(LiveRoomsBean data) {
        showBaseRefreshData(data);
    }

    /**
     * 显示  更多音频直播列表
     *
     * @param data
     */
    @Override
    public void showMoreAudioListData(LiveRoomsBean data) {
        showBaseMoreData(data);

    }

    /**
     * 显示刷新视频列表
     **/
    @Override
    public void showRefreshVedioListData(LiveRoomsBean data) {
        showBaseRefreshData(data);

    }

    /**
     * 刷新更多视频列表
     */
    @Override
    public void showMoreVedioListData(LiveRoomsBean data) {
        showBaseMoreData(data);

    }

    /**
     * 显示刷新  根据类型 获取的直播列表
     *
     * @param data
     */
    @Override
    public void showRefreshLiveListWithTypeData(LiveRoomsBean data) {
        //showRefreshHeaderData(data);
    }

    /**
     * 显示更多 根据类型获取的直播列表
     *
     * @param data
     */
    @Override
    public void showMoreLiveListWithTypeData(LiveRoomsBean data) {
        //  showMoreHeaderData(data);

    }

    /**
     * 显示刷新 推荐关注 类型的主播列表
     *
     * @param data
     */
    @Override
    public void showRefreshRecommendFollowListTypeData(LiveRoomsBean data) {
        showBaseRefreshData(data);

    }

    /**
     * 显示更多  推荐关注 类型的主播列表
     *
     * @param data
     */
    @Override
    public void showMoreRecommendFollowListTypeData(LiveRoomsBean data) {
        showBaseMoreData(data);

    }

    private void goToUserinfoActivity(int position) {
        LiveRoomBean bean = adapter.getData().get(position);
//        Intent intent = new Intent(getActivity(), UserInfoActivity.class);
//        Bundle bundle = new Bundle();
//        bundle.putString(TheLConstants.BUNDLE_KEY_USER_ID, bean.user.id + "");
//        bundle.putString("fromPage", this.getClass().getName());
//        intent.putExtras(bundle);
//        startActivity(intent);
        FlutterRouterConfig.Companion.gotoUserInfo(bean.user.id + "");
        reportLiveLog("head", bean.rank_id, bean.id, bean.liveUsersCount, liveClassifyBean.id, position);

    }

    @Override
    public void emptyData(boolean emptydata) {

    }

    @Override
    public void loadMoreFailed() {

    }

    @Override
    public void requestFinish() {

        mHandler.postDelayed(new Runnable() {

            @Override
            public void run() {
                if (recyclerview != null) {
                    if (swipe_container != null && swipe_container.isRefreshing())
                        swipe_container.setRefreshing(false);
                }
            }
        }, 1000);
    }

    @Override
    public void onTitleClick() {
        if (swipe_container != null && !swipe_container.isRefreshing() && isVisible() && recyclerview != null) {
            recyclerview.smoothScrollToPosition(0);
        }
    }

//    public void setRefreshListener(RefreshLisener refreshListener) {
//        this.refreshListener = refreshListener;
//    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {

        if (isVisibleToUser && isFirst) {
            isFirst = false;
            if (swipe_container != null)
                swipe_container.post(new Runnable() {
                    @Override
                    public void run() {
                        swipe_container.setRefreshing(true);
                    }
                });
            getBaseRefreshData();

        }
        super.setUserVisibleHint(isVisibleToUser);

    }

    @Override
    public void onStart() {
        super.onStart();
//        if (getUserVisibleHint()) {
//            //  getBaseRefreshData();
//            if (liveClassifyBean != null && TYPE_NEIGHBOUR == liveClassifyBean.id && mGoogleApiClient != null) {
//                mGoogleApiClient.connect(); // ②
//
//            }
//
//        }
    }
}
