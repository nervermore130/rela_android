package com.thel.modules.live.in;

import com.thel.modules.live.surface.show.LiveShowObserver;

/**
 * Created by waiarl on 2017/12/4.
 * 直播直播端PK消息接口
 */

public interface LiveShowCapturePkMsgIn {
    /**
     * 绑定Oberserver
     *
     * @param observer
     */
    void bindObserver(LiveShowObserver observer);


    /**
     * 发起Pk 请求
     *
     * @param toUserId pk接受放的用户id
     * @param x
     * @param y
     * @param width
     * @param height
     */
    void sendPkRequest(String toUserId, float x, float y, float width, float height);


    /**
     * 收到Pk请求
     *
     * @param payload
     */
    void receivePkRequest(String payload);

    /**
     * 响应Pk 请求
     *
     * @param toUserId pk接受方的用户id
     * @param result   yes: 同意, no: 不同意
     * @param x
     * @param y
     * @param width
     * @param height
     */
    void sendPkResponse(String toUserId, String result, float x, float y, float width, float height);

    /**
     * 发起PK方收到对方回应
     *
     * @param payload
     */
    void receivePkResponse(String payload);

    /**
     * PK开始时 服务器发送PK开始通知给双方直播室内的所有人
     *
     * @param payload
     */
    void receiverPkStartMsg(String payload);

    /**
     * 发送PK取消申请
     *
     * @param toUserId
     */
    void sendPkCancel(String toUserId);

    /**
     * 收到取消PK取消的请求
     *
     * @param payload
     */
    void receivePkCancelMsg(String payload);

    /**
     * 发送PK挂断申请
     *
     * @param toUserId
     */
    void sendPkHangup(String toUserId);

    /**
     * 被挂断方收到服务器的PK挂断消息
     *
     * @param payload
     */
    void receiveHangupMsg(String payload);

    /**
     * 收到Pk超时消息
     *
     * @param payload
     */
    void receivePkTimeoutMsg(String payload);

    /**
     * 进入总结阶段 服务器发送 总结剩余时间 和 双方助攻前三名 给双方直播室内的所有人:
     *
     * @param payload
     */
    void receivePkSummaryMsg(String payload);

    /**
     * PK结束时 服务器发送 PK结束 和 双方助攻前三名 通知给双方直播室内的所有人
     *
     * @param payload
     */
    void receivePkStopMsg(String payload);

    /**
     * 主播收到礼物时 服务器发送主播软妹币增量通知给双方直播室内的所有人:
     *
     * @param payload
     */
    void receiverPkGiftMsg(String payload);

}
