package com.thel.modules.main.home.search;

import com.thel.base.BaseDataBean;

import java.util.List;


/**
 * Created by chad
 * Time 18/8/31
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */
public class SearchRecommendBean extends BaseDataBean{

    public Data data;

    public class Data {
        public List<UserList> list ;

        public int cursor;

        public boolean haveNextPage;
    }

    public class UserList {
        public int userId;

        public String nickName;

        public String avatar;

        public String userName;

        public int fansNum;

        public boolean isFollow;

        public int age;

        public String roleName;

        public int affection;

        public String recommendReason;

        public int verifyType;

        public String verifyIntro;

        public String intro;

        /**
         * 是否开启了隐身
         */
        public int hiding;

        /**
         * 距离
         */
        public String distance;

        /**
         * 会员等级
         */
        public int level;

        public List<PicList> picList ;
    }

    public class PicList {
        public int picId;

        public String longThumbnailUrl;

        public int isPrivate;

        public int isBlcak;

        public String picUrl;

        public int picWidth;

        public int picHeight;
    }
}
