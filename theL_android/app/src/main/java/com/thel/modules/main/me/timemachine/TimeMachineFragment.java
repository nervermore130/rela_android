package com.thel.modules.main.me.timemachine;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jcodecraeer.xrecyclerview.ProgressStyle;
import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseFragment;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.moments.MomentsListBean;
import com.thel.constants.TheLConstants;
import com.thel.growingio.GrowingIoConstant;
import com.thel.manager.ListVideoVisibilityManager;
import com.thel.modules.others.VipConfigActivity;
import com.thel.ui.adapter.MomentsAdapter;
import com.thel.ui.widget.MySwipeRefreshLayout;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.decoration.DefaultItemDivider;
import com.thel.utils.L;
import com.thel.utils.UserUtils;
import com.thel.utils.ViewUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TimeMachineFragment extends BaseFragment implements TimeMachineContract.View {

    private final static String TAG = "TimeMachineFragment";

    @BindView(R.id.lin_more)
    LinearLayout lin_more;

    @BindView(R.id.txt_title)
    TextView txt_title;

    @BindView(R.id.empty_view)
    LinearLayout empty_view;

    @BindView(R.id.time_machine_tips_ll)
    LinearLayout time_machine_tips_ll;

    @BindView(R.id.moment_rv)
    XRecyclerView moment_rv;

    private TimeMachineContract.Presenter presenter;

    private MomentsAdapter adapter;

    private boolean haveNextPage = true;
    private boolean isRefresh = true;
    private int curPage = 0;

    public static TimeMachineFragment getInstance() {
        return new TimeMachineFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        new TimeMachinePresenter(this);
        initView();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_time_machine, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initRecyclerView();
    }

    @Override
    public void setPresenter(TimeMachineContract.Presenter presenter) {

        L.d(TAG, " presenter : " + presenter);

        if (presenter != null) {
            this.presenter = presenter;
            presenter.getMoments(curPage, 10);

        }
    }

    private void initView() {
        lin_more.setVisibility(View.GONE);
        txt_title.setText(R.string.time_machine);
    }

    private void initRecyclerView() {
        final LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        manager.setOrientation(RecyclerView.VERTICAL);
        moment_rv.setLayoutManager(manager);
        moment_rv.addItemDecoration(new DefaultItemDivider(TheLApp.context, LinearLayoutManager.VERTICAL, R.color.bg_color, (int) TheLApp.getContext().getResources().getDimension(R.dimen.wide_divider_height), true, true));

        adapter = new MomentsAdapter(R.layout.item_moments, null, GrowingIoConstant.ENTRY_TIME_MACHINE);
        moment_rv.setAdapter(adapter);

        moment_rv.setRefreshProgressStyle(ProgressStyle.BallClipRotate);
        moment_rv.setPullRefreshEnabled(false);
        moment_rv.setLoadingMoreEnabled(true);
        moment_rv.setLoadingListener(new XRecyclerView.LoadingListener() {
            @Override public void onRefresh() {
                curPage = 0;
                isRefresh = true;
                presenter.getMoments(curPage, 10);
            }

            @Override public void onLoadMore() {
                if (haveNextPage) {
                    curPage++;
                    presenter.getMoments(curPage, 10);
                } else {
                    moment_rv.setLoadingMoreEnabled(false);
                }
            }
        });

        adapter.setTimeMachineListener(new MomentsAdapter.TimeMachineListener() {
            @Override
            public void onClick(int position) {
                MomentsBean momentsBean = adapter.getData().get(position);
                if (UserUtils.getUserVipLevel() > 0) {
                    momentsBean.deleteFlag = 1;
                    adapter.notifyItemChanged(position);
                    presenter.recoverMoment(adapter.getData().get(position).momentsId);
                } else {
                    Intent vipConfigIntent = new Intent(getActivity(), VipConfigActivity.class);
                    vipConfigIntent.putExtra("showType", VipConfigActivity.SHOW_MSG_TIME_MACHINE);
                    startActivity(vipConfigIntent);
                }
            }
        });

        moment_rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {

                    if (adapter != null && adapter.getData() != null) {

                        ListVideoVisibilityManager.getInstance().calculatorItemPercent(manager, newState, adapter, TheLConstants.EntryConstants.ENTRY_NEARBYMOMENTS);

                    }

                    try {
                        if (getContext() != null) {
                            Glide.with(getContext()).resumeRequests();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {

                    try {
                        if (getContext() != null) {
                            Glide.with(getContext()).pauseRequests();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

            }
        });
    }

    @Override
    public void showMomentList(MomentsListBean data) {
        moment_rv.setPullRefreshEnabled(true);
        moment_rv.loadMoreComplete();
        moment_rv.refreshComplete();
        if (data != null) {
            data.timeEncoder();
            curPage = data.cursor;
            haveNextPage = data.haveNextPage;
            if (isRefresh && (data.momentsList == null || data.momentsList.size() == 0)) {
                empty_view.setVisibility(View.VISIBLE);
                moment_rv.setVisibility(View.INVISIBLE);
            } else {
                empty_view.setVisibility(View.INVISIBLE);
                moment_rv.setVisibility(View.VISIBLE);
            }
            if (data.momentsList != null) {
                List<MomentsBean> momentsBeans = data.momentsList;
                if (isRefresh) {
                    adapter.setNewData(momentsBeans);
                    isRefresh = false;
                } else {
                    adapter.getData().addAll(momentsBeans);
                }
            }
        }
    }

    @OnClick(R.id.lin_back)
    void onBack() {
        if (getActivity() != null) {
            getActivity().finish();
        }
    }
}
