package com.thel.modules.main.home.follow;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;

import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseAdapter;
import com.thel.bean.RecommendListBean;
import com.thel.bean.ReleaseMomentListBean;
import com.thel.bean.gift.WinkCommentBean;
import com.thel.bean.live.LiveFollowingUsersBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.moments.MomentsCheckBean;
import com.thel.bean.moments.MomentsListBean;
import com.thel.constants.BundleConstants;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.growingio.GrowingIoConstant;
import com.thel.imp.BaseFunctionFragment;
import com.thel.manager.ListVideoVisibilityManager;
import com.thel.modules.main.MainFragment;
import com.thel.modules.main.home.moments.comment.MomentCommentActivity;
import com.thel.modules.main.home.moments.theme.ThemeDetailActivity;
import com.thel.modules.main.home.moments.web.WebViewActivity;
import com.thel.modules.main.me.aboutMe.MatchActivity;
import com.thel.modules.main.userinfo.MyLinearLayoutManager;
import com.thel.modules.others.VipConfigActivity;
import com.thel.player.video.VideoPlayerProxy;
import com.thel.ui.adapter.MomentsAdapter;
import com.thel.ui.adapter.ReleaseMomentFailedAdapter;
import com.thel.ui.dialog.GuideDialogFragment;
import com.thel.ui.widget.FriendLiveHomepage;
import com.thel.ui.widget.GuideLayout;
import com.thel.ui.widget.MySwipeRefreshLayout;
import com.thel.ui.widget.RecommendUsersRecyclerPage;
import com.thel.ui.widget.recyclerview.decoration.DefaultItemDivider;
import com.thel.utils.DialogUtil;
import com.thel.utils.GsonUtils;
import com.thel.utils.InstallUtils;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;

import java.util.List;
import java.util.ListIterator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.thel.utils.ShareFileUtils.IS_JUMP_UPDATE_USERINFO_ACTIVITY;
import static com.thel.utils.ShareFileUtils.IS_RELEASE_MOMENT;

/**
 * @author liuyun
 * @date 2017/9/20
 */

public class FollowFragment extends BaseFunctionFragment implements FollowContract.View, SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = "FollowFragment";

    private final static int INSTALL_APK_REQUEST_CODE = 1;

    private long refreshTime = 0;

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    @BindView(R.id.swipe_container)
    MySwipeRefreshLayout swipe_container;

    @BindView(R.id.listView_failed_moments)
    RecyclerView listView_failed_moments;

    @BindView(R.id.guide_frame)
    FrameLayout guide_frame;

    @BindView(R.id.lin_new_moments_remind)
    LinearLayout lin_new_moments_remind;

    @BindView(R.id.txt_new_moments_remind)
    TextView txt_new_moments_remind;

    @BindView(R.id.root_rl)
    RelativeLayout root_rl;

    private GuideLayout guideLayout;

    private FollowContract.Presenter mPresenter;

    private MomentsAdapter mAdapter;

    private MyLinearLayoutManager mLinearLayoutManager;

    private List<MomentsBean> momentsList;

    private int curPage = 1;

    private boolean isRefresh = true;

    private FriendLiveHomepage liveHomepage;

    private boolean haveNextPage = false;

    private String apkUrl;

    private ReleaseMomentFailedAdapter mReleaseMomentFailedAdapter;
    private RecommendUsersRecyclerPage recommendUsersRecyclerPage;

    public static FollowFragment getInstance() {
        return new FollowFragment();
    }

    private String pageId;
    private String from_page_id;
    private String from_page;
    private BroadcastReceiver followReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //添加一个本地正在发布的视频数据
            if (intent.getAction().equals(TheLConstants.BROADCAST_RELEASE_NEW_VIDEO_ACTION)) {
                String releaseJson = ShareFileUtils.getString(TheLConstants.RELEASE_CONTENT_KEY, "");
                ReleaseMomentListBean releaseMomentListBean = GsonUtils.getObject(releaseJson, ReleaseMomentListBean.class);
                if (releaseMomentListBean != null && releaseMomentListBean.list != null && releaseMomentListBean.list.size() > 0) {
                    MomentsBean momentsBean = releaseMomentListBean.list.get(0);
                    momentsBean.playTime = momentsBean.playTime / 1000;
                    mAdapter.add(0, momentsBean);
                    scrollToTop();
                }

            }
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_recycler_normal, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);

        liveHomepage = new FriendLiveHomepage(getContext());

        recommendUsersRecyclerPage = new RecommendUsersRecyclerPage(getContext());

        ViewUtils.initSwipeRefreshLayout(swipe_container);

        swipe_container.setOnRefreshListener(this);

        mReleaseMomentFailedAdapter = new ReleaseMomentFailedAdapter(getActivity());

        listView_failed_moments.setHasFixedSize(true);

        listView_failed_moments.addItemDecoration(new DefaultItemDivider(getActivity(), LinearLayoutManager.VERTICAL, R.color.gray, 1, true, true));

        listView_failed_moments.setLayoutManager(new MyLinearLayoutManager(getActivity()));

        listView_failed_moments.setAdapter(mReleaseMomentFailedAdapter);

        listView_failed_moments.setVisibility(View.VISIBLE);

        mReleaseMomentFailedAdapter.initData();

     /*   Bundle bundle = getArguments();
        if (bundle != null) {
            from_page_id = bundle.getString(ShareFileUtils.MATCH_FROM_PAGE_ID, "");
            from_page = bundle.getString(ShareFileUtils.MATCH_FROM_PAGE, "");

        }*/
      /*  from_page = ShareFileUtils.getString(ShareFileUtils.rootSwitchPage, "");
        from_page_id = ShareFileUtils.getString(ShareFileUtils.rootSwitchPageId, "");
*/
        from_page = ShareFileUtils.getString(ShareFileUtils.rootSwitchPage, "");
        from_page_id = ShareFileUtils.getString(ShareFileUtils.rootSwitchPageId, "");

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(TheLConstants.BROADCAST_RELEASE_NEW_VIDEO_ACTION);
        TheLApp.context.registerReceiver(followReceiver, intentFilter);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        pageId = Utils.getPageId();

        initRecyclerView();

        mPresenter = new FollowPresenter(this);

        mPresenter.register(getContext());

        boolean isNewUser = ShareFileUtils.getBoolean(ShareFileUtils.IS_NEW_USER, false);

        L.d(TAG, " isNewUser : " + isNewUser);

        if (!isNewUser) {


            L.d(TAG, " ------5------ ");

            mPresenter.loadData(String.valueOf(curPage));
        }

        ShareFileUtils.setBoolean(ShareFileUtils.IS_NEW_USER, false);

        mPresenter.loadFollowingUsersLiveData();

        mPresenter.getRecommendUserList();

        mAdapter.registerNewCommentNotify(getActivity());

    }

    public void scrollToTop() {
        if (mRecyclerView != null) {
            mRecyclerView.smoothScrollToPosition(0);
        }
    }

    private void initRecyclerView() {
        mAdapter = new MomentsAdapter(R.layout.item_moments, null, GrowingIoConstant.ENTRY_FOLLOW_PAGE, "friendmoments", pageId, from_page, from_page_id);
        ShareFileUtils.setString(ShareFileUtils.homePage, "friendmoments");
        ShareFileUtils.setString(ShareFileUtils.homePageId, pageId);
        mAdapter.setShowFollowBtn(false);
        mLinearLayoutManager = new MyLinearLayoutManager(getActivity());
        mLinearLayoutManager.setAutoMeasureEnabled(true);
        mRecyclerView.addItemDecoration(new DefaultItemDivider(TheLApp.context, LinearLayoutManager.VERTICAL, R.color.bg_color, (int) TheLApp.getContext().getResources().getDimension(R.dimen.wide_divider_height), true, false));
        mRecyclerView.setLayoutManager(mLinearLayoutManager);

        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new BaseAdapter.OnItemClickListener<MomentsBean>() {
            @Override
            public void onItemClick(View view, MomentsBean item, int position) {

                Intent intent;

                Bundle bundle = new Bundle();

                if (MomentsBean.MOMENT_TYPE_THEME.equals(item.momentsType)) {
//                    intent = new Intent(getContext(), ThemeDetailActivity.class);
//                    bundle.putString(ShareFileUtils.MATCH_FROM_PAGE_ID, pageId);
//                    bundle.putString(ShareFileUtils.MATCH_FROM_PAGE, "friendmoments");
//                    if (item.parentMoment != null) {
//                        intent.putExtra(BundleConstants.THEMEPARTICIPATES, String.valueOf(item.parentMoment.joinTotal));
//                    } else {
//                        intent.putExtra(BundleConstants.THEMEPARTICIPATES, String.valueOf(item.joinTotal));
//                    }
//
//                    bundle.putSerializable(TheLConstants.BUNDLE_KEY_MOMENT_BEAN, item);
//                    intent.putExtras(bundle);
//
//                    if (getActivity() != null) {
//                        getActivity().startActivityForResult(intent, TheLConstants.BUNDLE_CODE_MOMENTS_ACTIVITY);
//                    }
                    FlutterRouterConfig.Companion.gotoThemeDetails(item.momentsId);
                } else if (MomentsBean.MOMENT_TYPE_AD.equals(item.momentsType)) {
                    if (item.appSchemeUrl != null) {

                        if (TheLConstants.SchemeConstant.SCHEME_PATH_VIP.equals(item.appSchemeUrl)) {

                            intent = new Intent(getContext(), VipConfigActivity.class);

                            if (getActivity() != null) {
                                getActivity().startActivityForResult(intent, TheLConstants.BUNDLE_CODE_MOMENTS_ACTIVITY);
                            }

                        } else if (TheLConstants.SchemeConstant.SCHEME_PATH_MATCH.equals(item.appSchemeUrl)) {

                            intent = new Intent(getContext(), MatchActivity.class);

                            if (getActivity() != null) {
                                getActivity().startActivityForResult(intent, TheLConstants.BUNDLE_CODE_MOMENTS_ACTIVITY);
                            }

                        } else if (TheLConstants.SchemeConstant.SCHEME_PATH_THEME.equals(item.appSchemeUrl)) {

                            if (getActivity() != null) {

                                MainFragment mainFragment = (MainFragment) getActivity().getSupportFragmentManager().findFragmentByTag(MainFragment.class.getName());

                                mainFragment.showThemeFragment();

                            }


                        } else if (item.appSchemeUrl.contains(TheLConstants.SchemeConstant.SCHEME_PATH_THEMEDETAIL)) {

                            Uri uri = Uri.parse(item.appSchemeUrl);

                            String momentsId = uri.getQueryParameter("momentId");

//                            intent = new Intent(getContext(), ThemeDetailActivity.class);
//
//                            intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, momentsId);
//
//                            intent.putExtras(bundle);
//
//                            if (getActivity() != null) {
//                                getActivity().startActivityForResult(intent, TheLConstants.BUNDLE_CODE_MOMENTS_ACTIVITY);
//                            }
                            FlutterRouterConfig.Companion.gotoThemeDetails(momentsId);

                        } else if (item.appSchemeUrl.contains(TheLConstants.SchemeConstant.SCHEME_PATH_MOMENTDETAIL)) {

                            Uri uri = Uri.parse(item.appSchemeUrl);

                            String momentsId = uri.getQueryParameter("momentId");

//                            intent = new Intent(getContext(), MomentCommentActivity.class);
//
//                            bundle.putSerializable(TheLConstants.BUNDLE_KEY_MOMENT_ID, momentsId);
//                            intent.putExtras(bundle);
//                            if (getActivity() != null) {
//                                getActivity().startActivityForResult(intent, TheLConstants.BUNDLE_CODE_MOMENTS_ACTIVITY);
//                            }
                            FlutterRouterConfig.Companion.gotoMomentDetails(momentsId);

                        } else {
                            bundle.putString(BundleConstants.URL, item.adUrl);
                            intent = new Intent(getContext(), WebViewActivity.class);
                            intent.putExtras(bundle);
                            if (getActivity() != null) {
                                getActivity().startActivityForResult(intent, TheLConstants.BUNDLE_CODE_MOMENTS_ACTIVITY);
                            }
                        }
                    } else {
                        bundle.putString(BundleConstants.URL, item.adUrl);
                        intent = new Intent(getContext(), WebViewActivity.class);
                        intent.putExtras(bundle);
                        if (getActivity() != null) {
                            getActivity().startActivityForResult(intent, TheLConstants.BUNDLE_CODE_MOMENTS_ACTIVITY);
                        }
                    }

                } else {
//                    intent = new Intent(getContext(), MomentCommentActivity.class);
//                    bundle.putSerializable(TheLConstants.BUNDLE_KEY_MOMENT_BEAN, item);
//                    intent.putExtras(bundle);
//                    if (getActivity() != null) {
//                        getActivity().startActivityForResult(intent, TheLConstants.BUNDLE_CODE_MOMENTS_ACTIVITY);
//                    }
                    FlutterRouterConfig.Companion.gotoMomentDetails(item.momentsId);
                }


            }

        });

        mAdapter.setOnLoadMoreListener(new MomentsAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                if (haveNextPage) {
                    curPage++;
                    loadMore();
                } else {
                    mRecyclerView.post(new Runnable() {
                        @Override
                        public void run() {
                            mAdapter.openLoadMore(mAdapter.getData().size(), false);
                            View view = LayoutInflater.from(getContext()).inflate(R.layout.load_more_footer_layout, (ViewGroup) mRecyclerView.getParent(), false);
                            ((TextView) view.findViewById(R.id.text)).setText(getString(R.string.info_no_more));
                            mAdapter.addFooterView(view);
                        }
                    });

                }


            }
        });

        mAdapter.setReloadMoreListener(new MomentsAdapter.ReloadMoreListener() {
            @Override
            public void reloadMore() {
                mAdapter.removeAllFooterView();
                mAdapter.openLoadMore(true);
                loadMore();
            }
        });

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {


                if (newState == RecyclerView.SCROLL_STATE_IDLE) {

                    if (mAdapter != null && mAdapter.getData() != null && mLinearLayoutManager != null) {

                        ListVideoVisibilityManager.getInstance().calculatorItemPercent(mLinearLayoutManager, newState, mAdapter, TheLConstants.EntryConstants.ENTRY_FOLLOWING);

                    }

                }

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

            }
        });

        mAdapter.setLayoutManager(mLinearLayoutManager);

        initGuideLayout();

    }

    private void initGuideLayout() {
        boolean isShowGuideLayout = ShareFileUtils.getBoolean(Utils.getMyUserId() + "_" + ShareFileUtils.IS_SHOW_GUIDE_LAYOUT, true);
        boolean is_jump_update_userinfo_activity = ShareFileUtils.getBoolean(Utils.getMyUserId() + "_" + IS_JUMP_UPDATE_USERINFO_ACTIVITY, true);
        boolean is_release_moment = ShareFileUtils.getBoolean(Utils.getMyUserId() + "_" + IS_RELEASE_MOMENT, true);

        boolean isNeedShowGuide = isShowGuideLayout && (!is_release_moment || !is_jump_update_userinfo_activity);
        if (isNeedShowGuide) {
            guideLayout = new GuideLayout(getContext());
            if (mAdapter != null) {
                mAdapter.addHeaderView(guideLayout);
            }
        }
    }

    private void loadMore() {

        L.d(TAG, "--------4------");

        isRefresh = false;
        mPresenter.loadData(String.valueOf(curPage));
    }

    @Override
    public void setPresenter(FollowContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void showMomentList(MomentsListBean momentsListBean) {

        if (getActivity() != null && momentsListBean != null && !getActivity().isDestroyed()) {

            momentsListBean.filterBlockMoments();

            haveNextPage = momentsListBean.haveNextPage;

            momentsList = momentsListBean.momentsList;

            L.d(TAG, " momentsList : " + momentsList);

            if (momentsList != null) {

                MomentsAdapter.filterUserMoments(momentsList);
                MomentsAdapter.filterBlack(momentsList);
                MomentsAdapter.filterAdMoment(momentsList);
                MomentsAdapter.filterOneMoments(momentsList);

                if (isRefresh) {
                    mAdapter.setNewData(momentsList);
                    mAdapter.openLoadMore(momentsList.size(), true);
                } else {
                    if (mAdapter != null && mAdapter.getData() != null) {
                        MomentsAdapter.filterEquallyMoment(mAdapter.getData(), momentsList);
                    }
                    mAdapter.addData(momentsList);
                    mAdapter.notifyDataChangedAfterLoadMore(true);
                }

                if (mAdapter != null && mAdapter.getData() != null && mLinearLayoutManager != null) {

                    ListVideoVisibilityManager.getInstance().calculatorItemPercent(mLinearLayoutManager, RecyclerView.SCROLL_STATE_IDLE, mAdapter, TheLConstants.EntryConstants.ENTRY_FOLLOWING);

                }


            } else {
                guide_frame.setVisibility(View.VISIBLE);
            }

            isShowRefresh(false);
        }
    }

    @Override
    public void showFollowUserLiveList(LiveFollowingUsersBean bean) {
        if (liveHomepage != null) {
            if (bean != null && bean.list != null) {
                liveHomepage.initView(bean);
                if (bean.list.size() > 0) {
                    if (mAdapter != null && mAdapter.getHeaderLayoutCount() == 0) {
                        mAdapter.addHeaderView(liveHomepage);
                    }
                } else {
                    if (mAdapter != null && mAdapter.getHeaderLayoutCount() > 0) {
                        mAdapter.removeHeaderView(liveHomepage);
                    }
                }
            }
        }
    }

    private void isShowRefresh(final boolean isShowRefresh) {
        if (swipe_container != null) {
            swipe_container.post(new Runnable() {
                @Override
                public void run() {
                    swipe_container.setRefreshing(isShowRefresh);
                }
            });
        }
    }


    @Override
    public void newFollowingMoments(MomentsCheckBean data) {
        newUnreadMoments(data);
    }

    @Override
    public void releaseFailed() {
        if (mReleaseMomentFailedAdapter != null) {
            mReleaseMomentFailedAdapter.initData();
        }
    }

    @Override
    public void loadDataFailed() {
        isShowRefresh(false);
    }

    @Override
    public void showRecommendUser(List<RecommendListBean.GuideDataBean> list) {
        if (recommendUsersRecyclerPage != null) {
            if (list != null) {
                recommendUsersRecyclerPage.initView(list);
                if (list.size() > 0) {
                    if (mAdapter != null && mAdapter.getRecommendLayoutCount() == 0) {
                        mAdapter.addMiddleView(recommendUsersRecyclerPage);
                    }
                } else {
                    if (mAdapter != null && mAdapter.getRecommendLayoutCount() > 0) {
                        mAdapter.removeRecommendView(recommendUsersRecyclerPage);
                    }
                }
            }
        }
    }

    @Override
    public void gotoAPKSetting(String apkUrl) {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            this.apkUrl = apkUrl;

            Intent intent = new Intent(Settings.ACTION_MANAGE_UNKNOWN_APP_SOURCES);
            intent.putExtra("apkUrl", apkUrl);
            startActivityForResult(intent, INSTALL_APK_REQUEST_CODE);
        }
    }

    /**
     * 显示新日志提醒悬浮窗
     *
     * @param momentsCheck
     */
    private void newUnreadMoments(Object momentsCheck) {

        if (momentsCheck != null) {
            try {
                MomentsCheckBean momentsCheckBean = (MomentsCheckBean) momentsCheck;
                if (momentsCheckBean.notReadMomentCount > 0) {
//                    lin_new_moments_remind.setVisibility(View.VISIBLE);
//                    txt_new_moments_remind.setText(getString(R.string.new_moments_remind, momentsCheckBean.notReadMomentCount));
                } else {
                    hideNewMomentRemindVIew();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 隐藏新日志提醒悬浮窗
     */
    public void hideNewMomentRemindVIew() {
        lin_new_moments_remind.setVisibility(View.GONE);
    }

    @Override
    public void refreshData() {

        L.d("FollowPresenter", " refreshData : ");

        isShowRefresh(true);

        isRefresh = true;

        curPage = 1;

        if (mAdapter != null) {

            mAdapter.removeAllFooterView();
        }

        L.d(TAG, "--------3------");

        mPresenter.loadData(String.valueOf(curPage));
    }


    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mPresenter != null) {
            mPresenter.unRegister(getContext());
        }

        mAdapter.unregisterNewCommentNotify(getActivity());

    }

    @Override
    public void onMomentReport(String momentId) {
        super.onMomentReport(momentId);
        onBlackOneMoment(momentId);
    }

    @Override
    public void onMomentDelete(String momentId) {
        super.onMomentDelete(momentId);

        onBlackOneMoment(momentId);
    }

    /**
     * 屏蔽他的日志，刷新界面
     */
    @Override
    public void onBlackherMoment(String userId) {

        DialogUtil.showToastShort(TheLApp.getContext(), getString(R.string.is_hided));
        deleteMomentByUserId(userId);
        closeLoading();
    }

    @Override
    public void onBlackOneMoment(String momentId) {

        if (mAdapter != null) {
            List<MomentsBean> list = mAdapter.getData();
            if (list != null) {
                ListIterator<MomentsBean> iterable = list.listIterator();
                while (iterable.hasNext()) {
                    MomentsBean momentsBean = iterable.next();
                    if (String.valueOf(momentsBean.momentsId).equals(momentId)) {
                        iterable.remove();
                    }
                }
            }

            mAdapter.notifyDataSetChanged();
        }
        closeLoading();
    }

    @Override
    public void onLikeStatusChanged(String momentId, boolean like, String myUserId, WinkCommentBean myWinkUserBean) {
        super.onLikeStatusChanged(momentId, like, myUserId, myWinkUserBean);

        L.d("VideoFragment", " FollowFragment onLikeStatusChanged momentId : " + momentId);

    }

    @Override
    public void onBlackUsersChanged(String userId, boolean isAdd) {
        super.onBlackUsersChanged(userId, isAdd);
        deleteMomentByUserId(userId);
    }

    private void deleteMomentByUserId(String userId) {
        if (mAdapter != null) {
            List<MomentsBean> list = mAdapter.getData();
            if (list != null) {
                ListIterator<MomentsBean> iterable = list.listIterator();
                while (iterable.hasNext()) {
                    MomentsBean momentsBean = iterable.next();
                    if (String.valueOf(momentsBean.userId).equals(userId)) {
                        iterable.remove();
                    }
                }
            }
            mAdapter.notifyDataSetChanged();
        }
    }

    @OnClick(R.id.lin_new_moments_remind)
    void refreshUnreadMoment() {
        onRefresh();
        hideNewMomentRemindVIew();
    }

    @Override
    public void onRefresh() {

        isRefresh = true;

        curPage = 1;

        if (mAdapter != null) {

            mAdapter.removeAllFooterView();

        }

        if (mPresenter != null) {

            L.d(TAG, "--------2------");

            mPresenter.loadData(String.valueOf(curPage));

            mPresenter.loadFollowingUsersLiveData();

            mPresenter.getRecommendUserList();
        }

        isShowRefresh(true);

        if (getActivity() != null) {
            Intent intent = new Intent();
            intent.setAction(TheLConstants.BROADCAST_NEW_MOMENTS_CLEAR);
            getActivity().sendBroadcast(intent);
        }

        hideNewMomentRemindVIew();

    }

    @Override
    public void onResume() {
        super.onResume();
        L.d(TAG, " onResume : ");
        if (guideLayout != null) {
            guideLayout.updateUI();
        }

        long currentTime = System.currentTimeMillis();

        if (refreshTime != 0 && currentTime - refreshTime > 30 * 60 * 1000) {

            if (mRecyclerView != null) {

                mRecyclerView.scrollToPosition(0);

            }

            onRefresh();

        }

        refreshTime = currentTime;

        if (mAdapter != null && mAdapter.getData() != null && mLinearLayoutManager != null) {

            // ListVideoVisibilityManager.getInstance().calculatorItemPercent(mLinearLayoutManager, RecyclerView.SCROLL_STATE_IDLE, mAdapter, TheLConstants.EntryConstants.ENTRY_FOLLOWING);

        }

    }

    public void setHiddenChanged(boolean hidden) {
        if (hidden) {
            VideoPlayerProxy.getInstance().pause();
        } else {
            VideoPlayerProxy.getInstance().resume();

            if (getActivity() != null) {

                GuideDialogFragment guideDialogFragment = (GuideDialogFragment) getActivity().getSupportFragmentManager().findFragmentByTag(GuideDialogFragment.class.getName());

                if (guideDialogFragment != null && guideDialogFragment.isAdded()) {
                    if (mPresenter != null) {

                        mPresenter.loadData(String.valueOf(curPage));
                    }
                }
            }

        }


    }

    public void setFromPageAndId(String from_page, String pageId) {
        this.from_page = from_page;
        this.from_page_id = pageId;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == INSTALL_APK_REQUEST_CODE) {
            if (apkUrl != null) {
                InstallUtils.installApk(getActivity(), apkUrl);
            }
        }
    }
}
