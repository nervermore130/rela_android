package com.thel.modules.main.discover;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseFragment;
import com.thel.bean.LivePermitBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.BridgeUtils;
import com.thel.imp.AutoRefreshImp;
import com.thel.imp.TittleClickListener;
import com.thel.modules.live.Certification.ZhimaCertificationActivity;
import com.thel.modules.live.bean.LiveClassifyBean;
import com.thel.modules.live.bean.LivePopularityNetBean;
import com.thel.modules.main.discover.adapter.LiveClassifyFragmentAdapter;
import com.thel.modules.main.discover.view.LiveClassifyTabTitleView;
import com.thel.modules.main.discover.view.LiveNewUserListView;
import com.thel.modules.main.discover.view.LiveRoomsListPresenter;
import com.thel.modules.main.discover.view.LiveScrollViewpagerView;
import com.thel.modules.main.discover.view.LiveroomScroller;
import com.thel.modules.main.home.moments.ReleaseLiveMomentActivity;
import com.thel.modules.main.me.bean.LogInfoBean;
import com.thel.modules.main.me.match.eventcollect.collect.LiveLogUtils;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.utils.DialogUtil;
import com.thel.utils.L;
import com.thel.utils.PermissionUtil;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import me.rela.rf_s_bridge.router.FlutterBoostPlugin;

/**
 * Created by lingwei on 2017/11/6.
 */

public class LiveRoomsListFragment extends BaseFragment implements LiveRoomsListContract2.View {
    private static LiveRoomsListFragment instance;

    public final int REFRESH_TYPE_ALL = 1;
    public final int REFRESH_TYPE_NEXT_PAGE = 2;
    private boolean isCreate = false;
    private boolean isFirst = true;
    private SwipeRefreshLayout swipe_container;
    private RecyclerView recyclerview;
    private LiveClassifyFragmentAdapter adapter;
    private LiveNewUserListView liveNewUserListView;
    private int cursor = 0;
    private LiveRoomsListContract2.Presenter presenter;
    private TextView text_default;
    private View lin_add_friend;
    private View txt_title;
    private View lin_start_live;
    private LiveScrollViewpagerView popularity_view;
    private TabLayout tablayout;
    private ViewPager viewpager;
    private List<Fragment> fragmentList = new ArrayList<>();
    private List<LiveClassifyBean> mCommonDataList = new ArrayList<>();
    private List<LiveClassifyBean> mNetDatList = new ArrayList<>();
    private List<LiveClassifyBean> mDataList = new ArrayList<>();
    private FragmentManager manager;
    private LivePopularityNetBean livePopulartityBean;
    private LiveroomScroller scroller;

    private int NO_SCROLL_DURATION = 0;//viewpager 0滚动时间
    private int SCROLL_DURATION = 1000;//viewpager 1秒滚动时间
    public static final String TAG = "LiveRoomsListFragment";
    private AppBarLayout appbarlayout;
    private String pageId;
    private String fromPage;
    private String fromPageId;

    public static LiveRoomsListFragment getInstance(Bundle bundle) {
        instance = new LiveRoomsListFragment();
        instance.setArguments(bundle);
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_live_rooms_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //initHeaderView();
        getCommonData();
        findViewById(view);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        new LiveRoomsListPresenter(this);
        setListener();
        isCreate = true;

        pageId = Utils.getPageId();
        fromPage = ShareFileUtils.getString(ShareFileUtils.rootSwitchPage, "");
        fromPageId = ShareFileUtils.getString(ShareFileUtils.rootSwitchPageId, "");

        receiver = new LiveRoomReceiver();
        IntentFilter intentFilter = new IntentFilter();
        //刷新
        intentFilter.addAction(TheLConstants.BROADCAST_ACTION_REFRESH_NATIVE);
        if (getActivity() != null)
            getActivity().registerReceiver(receiver, intentFilter);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getActivity() != null) getActivity().unregisterReceiver(receiver);
    }

    private void setListener() {
        swipe_container.setEnabled(false);
        /**
         * 这里要用set这个方法而不能用add，因为，要在super.onTabSelected方法之前设置一些参数
         */

//        tablayout.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewpager) {
//            @Override
//            public void onTabSelected(TabLayout.Tab tab) {
//                final int index = viewpager.getCurrentItem();//在super之前，也就是viewpager滑动之前
//                final int position = tab.getPosition();
//                if (scroller != null) {
//                    if ((Math.abs(position - index)) > 1) {//如果要跳转的页面与当前页面间隔超过1，则滑动时间为0，否则，滑动时间为1秒
//                        scroller.setScrollDuration(NO_SCROLL_DURATION);
//                    } else {
//                        scroller.setScrollDuration(SCROLL_DURATION);
//                    }
//                }
//                // tab.getTabAt(position).getCustomView().setSelected(true);
//                reportLiveLog(index);
//
//                super.onTabSelected(tab);
//            }
//
//            @Override
//            public void onTabReselected(TabLayout.Tab tab) {
//                super.onTabReselected(tab);
//                final int position = tab.getPosition();
//                if (fragmentList.size() > position) {
//                    final Fragment fragment = fragmentList.get(position);
//                    if (fragment instanceof TittleClickListener) {
//                        ((TittleClickListener) fragment).onTitleClick();
//                    }
//                }
//            }
//        });

        lin_start_live.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BridgeUtils.gotoReleaseLiveActivity(getActivity());
            }
        });

//        lin_start_live.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View view) {
//                int i = 1/0;
//                return false;
//            }
//        });
    }

    private void reportLiveLog(int index) {
        try {
            String latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
            String longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");
            LogInfoBean logInfoBean = new LogInfoBean();
            logInfoBean.page = "live";
            logInfoBean.page_id = pageId;
            switch (index) {
                case 0:
                    logInfoBean.activity = "video";

                    break;
                case 1:
                    logInfoBean.activity = "fm";

                    break;
                case 2:
                    logInfoBean.activity = "voice";

                    break;

            }
            logInfoBean.from_page = fromPage;
            logInfoBean.from_page_id = fromPageId;
            logInfoBean.lat = latitude;
            logInfoBean.lng = longitude;

            LiveLogUtils.getInstance().addLog(logInfoBean);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 发布直播日志
     */
    private void writeLiveMoment() {
        if (ShareFileUtils.getInt(TheLConstants.live_permit, 0) == 1) {
            //测试
            final Intent intent = new Intent(getActivity(), ReleaseLiveMomentActivity.class);
            startActivity(intent);


        } else {
            RequestBusiness.getInstance().getLivePermit().onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<LivePermitBean>() {
                @Override
                public void onNext(LivePermitBean livePermitBean) {
                    super.onNext(livePermitBean);
                    if (livePermitBean != null && livePermitBean.data != null) {
                        ShareFileUtils.setInt(TheLConstants.live_permit, livePermitBean.data.perm);
                        if (ShareFileUtils.getInt(TheLConstants.live_permit, 0) == 1) {
                            Intent intent = new Intent(TheLApp.getContext(), ReleaseLiveMomentActivity.class);

                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            TheLApp.getContext().startActivity(intent);
                        } else {
                            DialogUtil.showConfirmDialog(getContext(), "", TheLApp.getContext().getString(R.string.no_live_permition_tip), TheLApp.getContext().getString(R.string.info_continue), TheLApp.getContext().getString(R.string.info_no), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                   /* Intent intent = new Intent(getActivity(), WebViewActivity.class);
                                    Bundle bundle = new Bundle();
                                    bundle.putString(BundleConstants.URL, TheLConstants.applyLivePermitPage);
                                    bundle.putBoolean(BundleConstants.NEED_SECURITY_CHECK, false);
                                    intent.putExtras(bundle);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    TheLApp.getContext().startActivity(intent);*/
                                    startActivity(new Intent(getActivity(), ZhimaCertificationActivity.class));

                                }
                            });

                        }
                    }
                }
            });
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (livePopulartityBean == null) {
            getRefreshData();
        }
        super.setUserVisibleHint(isVisibleToUser);
    }

    private void getCommonData() {
        mDataList.clear();
        mCommonDataList.clear();//清空固数据
        int[] arrSelected = new int[]{R.mipmap.live_nav_btn_hot_selected, R.mipmap.live_nav_btn_video_selected, R.mipmap.live_nav_btn_chat_selected, R.mipmap.live_nav_btn_fm_selected};
        int[] arrRes = new int[]{R.mipmap.live_nav_btn_hot_normal, R.mipmap.live_nav_btn_video_normal, R.mipmap.live_nav_btn_chat_normal, R.mipmap.live_nav_btn_fm_normal};

        String explore_recommend = TheLApp.getContext().getResources().getString(R.string.explore_recommend);

        L.d(TAG, " explore_recommend : " + explore_recommend);

        mCommonDataList.add(new LiveClassifyBean(LiveClassifyFragment2.TYPE_HOT, TheLApp.getContext().getResources().getString(R.string.explore_recommend), arrSelected[0], arrRes[0], "", ""));
        mCommonDataList.add(new LiveClassifyBean(LiveClassifyFragment2.TYPE_VIDEO, TheLApp.getContext().getResources().getString(R.string.live), arrSelected[1], arrRes[1], "", ""));
        mCommonDataList.add(new LiveClassifyBean(LiveClassifyFragment2.TYPE_HOT_CHAT, TheLApp.getContext().getResources().getString(R.string.hot_chat), arrSelected[2], arrRes[2], "", ""));
        mCommonDataList.add(new LiveClassifyBean(LiveClassifyFragment2.TYPE_RADIO, TheLApp.getContext().getResources().getString(R.string.radio_station), arrSelected[3], arrRes[3], "", ""));
        mDataList.addAll(mCommonDataList);//把固定数据加载到总数据列表里面
    }

    private void findViewById(View view) {
        lin_add_friend = view.findViewById(R.id.lin_add_friend);
        txt_title = view.findViewById(R.id.txt_title);
        lin_start_live = view.findViewById(R.id.lin_start_live);
        swipe_container = view.findViewById(R.id.swipe_container);
        ViewUtils.initSwipeRefreshLayout(swipe_container);
        recyclerview = view.findViewById(R.id.recyclerview);
        popularity_view = view.findViewById(R.id.popularity_view);
        tablayout = view.findViewById(R.id.tablayout_live);
        text_default = view.findViewById(R.id.txt_default);
        text_default.setText(R.string.no_live_streams);
        viewpager = view.findViewById(R.id.viewpager);
        appbarlayout = view.findViewById(R.id.appbarlayout);
        initViewPagerAdapter();
        //  initAdapter();
    }

    private void initViewPagerAdapter() {
        manager = getChildFragmentManager();
        adapter = new LiveClassifyFragmentAdapter(fragmentList, manager);
        viewpager.setAdapter(adapter);
        viewpager.setOffscreenPageLimit(3);
        adapter.setCurrentPosition(0);
        fragmentList.clear();
        fragmentList.addAll(getFragmentlist(mDataList));
        adapter.notifyDataSetChanged();
        tablayout.setupWithViewPager(viewpager);
        setTablayout();
        tablayout.getTabAt(0).getCustomView().setSelected(true);
        setViewPagerScrollSpeed();
    }

    /**
     * 设置viewpager的滚动
     */
    private void setViewPagerScrollSpeed() {
        try {
            final Field mScroller;
            mScroller = ViewPager.class.getDeclaredField("mScroller");
            mScroller.setAccessible(true);
            scroller = new LiveroomScroller(getActivity());
            mScroller.set(viewpager, scroller);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setTablayout() {
        if (getActivity() != null) {

            final int mDataSize = mDataList.size();
            for (int i = 0; i < mDataSize; i++) {
                TabLayout.Tab tab = tablayout.getTabAt(i);
                final LiveClassifyTabTitleView v = new LiveClassifyTabTitleView(getActivity()).initView(mDataList.get(i));
                tab.setCustomView(v);
            }
        }
    }

    private List<Fragment> getFragmentlist(List<LiveClassifyBean> mList) {
        final List<Fragment> list = new ArrayList<>();
        final int size = mList.size();
        for (int i = 0; i < size; i++) {
            final Bundle bundle = new Bundle();
            bundle.putSerializable(TheLConstants.BUNDLE_KEY_MOMENT_BEAN, mList.get(i));
            if (!TextUtils.isEmpty(pageId)) {
                bundle.putString("pageId", pageId);

            }
            final Fragment fragment = LiveClassifyFragment2.newInstance(bundle);
            list.add(fragment);
//            if (fragment instanceof LiveClassifyFragment2) {
//                ((LiveClassifyFragment2) fragment).setRefreshListener(new RefreshLisener() {
//                    @Override
//                    public void refresh() {
//                        presenter.getRefreshTopLinkData();
//                        //  presenter.getRefreshLiveClassfyData();
//
//                    }
//                });
//            }
        }
        return list;
    }

    @Override
    public void showRefreshLiveClassifyData(List<LiveClassifyBean> beanList) {
        if (beanList == null)
            return;
        if (beanList.size() != 0) {
            final List<LiveClassifyBean> mCurrentNetList = new ArrayList<>();
            mCurrentNetList.addAll(beanList);
            final int size0 = mCommonDataList.size();
            final int size = mDataList.size();
            final int size1 = beanList.size();
            final List<Fragment> deleteFragment = new ArrayList<>();
            final List<LiveClassifyBean> deleteDatalist = new ArrayList<>();
            boolean haveDelete = false;
            ourter:
            for (int i = size0; i < size; i++) {//因为固定数据不需要更新，所以要x从固定数据以后的下标开始循环
                final LiveClassifyBean bean = mDataList.get(i);
                for (int j = 0; j < size1; j++) {
                    if (bean.equals(beanList.get(j))) {
                        continue ourter;
                    } else {
                        if (j == size1 - 1) {//如果最後一個還沒有与当前相等的，说明這個已經刪除了
                            deleteFragment.add(fragmentList.get(i));
                            deleteDatalist.add(bean);
                            haveDelete = true;
                        }
                    }
                }
            }
            if (haveDelete) {//如果有删除的，为防止删除的是当前条目，所以先设置当前条目为第一个
                viewpager.setCurrentItem(0);
            }
            fragmentList.removeAll(deleteFragment);//移除被移除的fragment
            beanList.removeAll(mNetDatList);//移除掉重複的數據，只剩下以前没有的数据

            fragmentList.addAll(getFragmentlist(beanList));//添加所有新的fragment
            mDataList.removeAll(deleteDatalist);//总数据列表移除掉删除的数据
            mDataList.addAll(beanList);//添加以前没有的数据
            adapter.notifyDataSetChanged();//刷新adapter
            mNetDatList.clear();//重置网络获取数据
            mNetDatList.addAll(mCurrentNetList);
            setTablayout();//刷新tablayout
        }
    }

    @Override
    public void showRefreshTopLinkData(LivePopularityNetBean bean) {
        if (bean == null)
            return;
        this.livePopulartityBean = bean;
        popularity_view.initView(bean);
    }

    @Override
    public void onResume() {
        if (livePopulartityBean != null) {
            popularity_view.initView(livePopulartityBean);
        } else {
            getRefreshData();
        }
        tryRefreshCurrentFragment();
        super.onResume();
    }

    /**
     * 尝试刷新当前的fragment
     */
    private void tryRefreshCurrentFragment() {
        if (viewpager == null || viewpager.getAdapter() == null) {
            return;
        }

        final int currentTab = viewpager.getCurrentItem();
        if (fragmentList != null && fragmentList.size() > currentTab) {
            final Fragment fragment = fragmentList.get(currentTab);
            if (fragment instanceof AutoRefreshImp) {
                ((AutoRefreshImp) fragment).tryRefreshData();
            }
        }
    }

    public void getRefreshData() {
        //   presenter.getRefreshLiveClassfyData();
        if (!TextUtils.isEmpty(UserUtils.getUserKey()) && !TextUtils.isEmpty(UserUtils.getMyUserId())) {
            if (livePopulartityBean == null) {
                presenter.getRefreshTopLinkData();
            } else {
                popularity_view.initView(livePopulartityBean);
            }
        }
    }

    @Override
    public void setPresenter(LiveRoomsListContract2.Presenter presenter) {
        this.presenter = presenter;

    }

    @Override
    public void requestFinish() {
        closeLoading();
        if (swipe_container != null)
            swipe_container.postDelayed(new Runnable() {

                @Override
                public void run() {
                    if (swipe_container != null && swipe_container.isRefreshing())
                        swipe_container.setRefreshing(false);
                }
            }, 1000);
    }

    public void showVoice() {
        if (viewpager != null) {
            viewpager.setCurrentItem(1);
        }
    }

    private LiveRoomReceiver receiver;

    private class LiveRoomReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(TheLConstants.BROADCAST_ACTION_REFRESH_NATIVE)) {
                String index = intent.getStringExtra("index");
                if ("1".equals(index)) {
                    LiveClassifyFragment2 liveClassifyFragment2 = (LiveClassifyFragment2) fragmentList.get(tablayout.getSelectedTabPosition());
                    liveClassifyFragment2.scrollToPositionAndRefresh();
                }
            }
        }
    }

}
