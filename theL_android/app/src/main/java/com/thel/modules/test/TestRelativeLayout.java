package com.thel.modules.test;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import com.thel.utils.L;

public class TestRelativeLayout extends RelativeLayout{
    public TestRelativeLayout(Context context) {
        super(context);
    }

    public TestRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TestRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        L.d("TestRelativeLayout","------------onDraw----------");

    }

    @Override public void draw(Canvas canvas) {
        super.draw(canvas);

        L.d("TestRelativeLayout","------------draw----------");

    }
}
