package com.thel.modules.live.bean;

import com.thel.base.BaseDataBean;

import java.io.Serializable;

/**
 * Created by lingwei on 2018/3/19.
 */

public class ZmxyCheckCallBackBean extends BaseDataBean implements Serializable {

    /**
     * data : {"perm":1,"permRank":100231}
     */

    public ZmxyCheckCallBackDataBean data;

    public static class ZmxyCheckCallBackDataBean {
        /**
         * perm : 1
         * permRank : 100231
         */

        public int perm;
        /**
         * 第多少位申请到的直播权限的主播
         */
        public int permRank;
        /**
         * 失败原因
         **/
        public String reason;
    }
}
