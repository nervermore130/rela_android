package com.thel.modules.main.me.match.eventcollect.collect;

import android.text.TextUtils;

import com.thel.modules.main.me.bean.PayLogBean;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.service.DefaultRequestService;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.FormBody;

public class PayLogUtils {

    private List<PayLogBean> logs = new ArrayList<>();
    private PayLogBean logInfoBean = new PayLogBean();

    private PayLogUtils() {

    }

    private static PayLogUtils instance = new PayLogUtils();

    public static PayLogUtils getInstance() {
        return instance;
    }

    public void addLog(PayLogBean log) {

        logs.add(log);

        if (logs.size() > 5) {
            send();
            getLog();
            logs.clear();
        }


    }

    private void send() {

        String log = GsonUtils.createJsonString(logs);
        send(log);
    }

    public void send(String json) {
        L.d("PayLogUtils", json);
        final FormBody formBody = new FormBody.Builder().add("logs", json).build();

        Flowable<String> flowable = DefaultRequestService.
                createTestReportRequestService().
                postMatchLogs(formBody);

        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<>());
    }


    public PayLogBean getLog() {
        if (logs.size() > 0) {
            logInfoBean = logs.get(logs.size() - 1);

        }
        return logInfoBean;
    }

    /**
     * 软妹豆页面购买
     */
    public void reportShortPayLog(String page, String pageId, String activity, int isfirst, String paytype, String product_type) {
        String latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
        String longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");
        try {
            PayLogBean logBean = new PayLogBean();
            logBean.page = page;
            logBean.page_id = pageId;
            logBean.activity = activity;
            PayLogBean.LogsDataBean logsDataBean = new PayLogBean.LogsDataBean();
            logsDataBean.is_first = isfirst;
            if (!TextUtils.isEmpty(paytype)) {
                logsDataBean.pay_type = paytype;
            }
            if (!TextUtils.isEmpty(product_type)) {
                logsDataBean.product_type = product_type;
            }

            logBean.data = logsDataBean;

            logBean.lat = latitude;
            logBean.lng = longitude;

            addLog(logBean);

        } catch (Exception e) {

        }

    }

    public void reportLongPayLog(String page, String pageId, String activity, int isfirst, String product_type, String paytype, int is_success, String toast) {
        String latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
        String longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");
        try {
            PayLogBean logBean = new PayLogBean();
            logBean.page = page;
            logBean.page_id = pageId;
            logBean.activity = activity;
            logBean.lat = latitude;
            logBean.lng = longitude;
            PayLogBean.LogsDataBean logsDataBean = new PayLogBean.LogsDataBean();
            logsDataBean.is_first = isfirst;
            logsDataBean.product_type = product_type;
            logsDataBean.pay_type = paytype;
            logsDataBean.is_success = is_success;
            logsDataBean.toast = toast;
            logBean.data = logsDataBean;


            addLog(logBean);

        } catch (Exception e) {

        }

    }

    public void reportSoftOrVipPayLog(String page, String pageId, String activity, int isfirst) {
        String latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
        String longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");
        try {
            PayLogBean logBean = new PayLogBean();
            logBean.page = page;
            logBean.page_id = pageId;
            logBean.activity = activity;
            PayLogBean.LogsDataBean logsDataBean = new PayLogBean.LogsDataBean();
            logsDataBean.is_first = isfirst;

            logBean.data = logsDataBean;

            logBean.lat = latitude;
            logBean.lng = longitude;

            addLog(logBean);

        } catch (Exception e) {

        }

    }

    /**
     * 会员购买
     */
    public void reportVipLog(String page, String pageId, String activity, int isfirst, String product_type,String pay) {
        String latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
        String longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");
        try {
            PayLogBean logBean = new PayLogBean();
            logBean.page = page;
            logBean.page_id = pageId;
            logBean.activity = activity;
            PayLogBean.LogsDataBean logsDataBean = new PayLogBean.LogsDataBean();
            logsDataBean.is_first = isfirst;
            if (!TextUtils.isEmpty(product_type)) {
                logsDataBean.product_type = product_type;
            }

            logBean.data = logsDataBean;

            logBean.lat = latitude;
            logBean.lng = longitude;

            addLog(logBean);

        } catch (Exception e) {

        }

    }

    /**
     * 直播间页面购买
     */
    public void reportShortLiveSoftPayLog(String page, String pageId, String activity, int isfirst, String from_action, String product_type, int choice) {
        String latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
        String longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");
        try {
            PayLogBean logBean = new PayLogBean();
            logBean.page = page;
            logBean.page_id = pageId;
            logBean.activity = activity;
            PayLogBean.LogsDataBean logsDataBean = new PayLogBean.LogsDataBean();
            logsDataBean.is_first = isfirst;
            if (!TextUtils.isEmpty(from_action)) {
                logsDataBean.from_action = from_action;
            }
            if (!TextUtils.isEmpty(product_type)) {
                logsDataBean.product_type = product_type;
            }

            if (choice != -1) {
                logsDataBean.choice = choice;
            }

            logBean.data = logsDataBean;

            logBean.lat = latitude;
            logBean.lng = longitude;

            addLog(logBean);

        } catch (Exception e) {

        }

    }

    /**
     * 直播间页面购买
     */
    public void reportLongLiveSoftPayLog(String page, String pageId, String activity, int isfirst, String from_action, String product_type, String paytype, int issuccess, String toast) {
        String latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
        String longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");
        try {
            PayLogBean logBean = new PayLogBean();
            logBean.page = page;
            logBean.page_id = pageId;
            logBean.activity = activity;
            PayLogBean.LogsDataBean logsDataBean = new PayLogBean.LogsDataBean();
            logsDataBean.is_first = isfirst;
            if (!TextUtils.isEmpty(from_action)) {
                logsDataBean.from_action = from_action;
            }
            if (!TextUtils.isEmpty(product_type)) {
                logsDataBean.product_type = product_type;
            }

            if (!TextUtils.isEmpty(paytype)) {
                logsDataBean.pay_type = paytype;
            }

            if (issuccess != -1) {
                logsDataBean.is_success = issuccess;
            }

            if (!TextUtils.isEmpty(toast)) {
                logsDataBean.toast = toast;
            }


            logBean.data = logsDataBean;

            logBean.lat = latitude;
            logBean.lng = longitude;

            addLog(logBean);

        } catch (Exception e) {

        }

    }
}
