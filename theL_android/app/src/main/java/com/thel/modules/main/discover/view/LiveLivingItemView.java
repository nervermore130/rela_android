package com.thel.modules.main.discover.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.view.SimpleDraweeView;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.modules.live.bean.LiveRoomBean;
import com.thel.modules.main.discover.LiveClassifyItemBaseView;
import com.thel.modules.main.me.bean.LogInfoBean;
import com.thel.modules.main.me.match.eventcollect.collect.LiveLogUtils;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.utils.AppInit;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SimpleDraweeViewUtils;
import com.thel.utils.SizeUtils;

/**
 * 直播分类正在直播item
 * Created by lingwei on 2017/11/28.
 */

public class LiveLivingItemView extends LinearLayout implements LiveClassifyItemBaseView<LiveLivingItemView> {

    private final Context mContext;
    private float radius;
    private float padding;
    private int picSize;
    private SimpleDraweeView preview;
    private TextView txt_nickname;
    private TextView txt_audience;
    private TextView text;
    private SimpleDraweeView badge;
    private TextView txt_label;
    private LiveRoomBean liveRoomBean;
    private TextView txt_label_other;
    private TextView txt_distance;
    private RelativeLayout ll_label_other;
    private ImageView iv_pk_or_mic;
    private SimpleDraweeView img_avatar;
    private RelativeLayout rela_user;
    private final int TYPE_HOT = 0;
    private final int TYPE_NEW = -1;
    private final int TYPE_NEIGHBOUR = -2;
    private final int TYPE_AUDIO = -3;
    private final int TYPE_VEDIO = 1;

    public LiveLivingItemView(Context context) {
        this(context, null);
    }

    public LiveLivingItemView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LiveLivingItemView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        initData();
        init();
        setListener();
    }

    private void initData() {
        radius = TheLApp.getContext().getResources().getDimension(R.dimen.live_new_user_item_radius);
        padding = TheLApp.getContext().getResources().getDimension(R.dimen.live_new_user_item_radius);
        picSize = (int) ((AppInit.displayMetrics.widthPixels - 3 * padding) / 2);
    }

    @Override
    public LiveLivingItemView initMeasureDimen() {
        getLayoutParams().width = picSize;
        getLayoutParams().height = picSize;
        return this;
    }

    private void init() {
        inflate(mContext, R.layout.adapter_live_rooms, this);
        preview = findViewById(R.id.preview);
        txt_nickname = findViewById(R.id.nickname);
        txt_audience = findViewById(R.id.audience);
        text = findViewById(R.id.text);
        //  badge = findViewById(R.id.badge);
        txt_label = findViewById(R.id.txt_label);
        txt_label_other = findViewById(R.id.txt_label_other);
        txt_distance = findViewById(R.id.txt_distance);
        ll_label_other = findViewById(R.id.ll_label_other);
        iv_pk_or_mic = findViewById(R.id.iv_pk_or_mic);
        img_avatar = findViewById(R.id.img_avatar);
        rela_user = findViewById(R.id.rela_user);
    }

    private void setListener() {
        rela_user.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoUserInfo();
            }
        });
    }

    private void gotoUserInfo() {
        if (liveRoomBean != null && liveRoomBean.user != null) {
            final String userId = liveRoomBean.user.id + "";
//            final Intent intent = new Intent(getContext(), UserInfoActivity.class);
//            final Bundle bundle = new Bundle();
//            bundle.putString(TheLConstants.BUNDLE_KEY_USER_ID, userId);
//            intent.putExtras(bundle);
//            getContext().startActivity(intent);
            FlutterRouterConfig.Companion.gotoUserInfo(userId);
        }
    }

    @Override
    public LiveLivingItemView initView(LiveRoomBean liveRoomBean, String pageId, int currentType, int currentPosition) {
        this.liveRoomBean = liveRoomBean;
        if (liveRoomBean == null) {
            return this;
        }
        preview.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadius(radius));
        SimpleDraweeViewUtils.setImageUrl(preview, liveRoomBean.imageUrl, TheLApp.getContext().getResources().getDimension(R.dimen.moment_pic_thumbnail_big), TheLApp.getContext().getResources().getDimension(R.dimen.moment_pic_thumbnail_big));
        txt_nickname.setText(liveRoomBean.user.nickName);
        // 观看人数
        txt_audience.setText(liveRoomBean.liveUsersCount + "");
        // 直播描述
        text.setText(liveRoomBean.text);
        //2.20新增， 主播推荐标签
        txt_label.setVisibility(View.GONE);

        if (!TextUtils.isEmpty(liveRoomBean.label)) {

            txt_label.setVisibility(View.VISIBLE);
            txt_label.setText(liveRoomBean.label);
        }

        if (liveRoomBean.liveStatus == 1) {
            ll_label_other.setVisibility(View.VISIBLE);
            String content = TheLApp.context.getString(R.string.connect_mic);
            ll_label_other.setBackgroundResource(R.drawable.bg_live_list_lablel_shape_tabnormal);
            iv_pk_or_mic.setImageResource(R.mipmap.icn_linkmic);
            txt_label_other.setText(content);
        } else if (liveRoomBean.liveStatus == 2) {
            ll_label_other.setVisibility(View.VISIBLE);
            String content = TheLApp.context.getString(R.string.pk_now);
            txt_label_other.setText(content);
            iv_pk_or_mic.setImageResource(R.mipmap.icn_pk);
            ViewGroup.LayoutParams params = iv_pk_or_mic.getLayoutParams();
            params.height = SizeUtils.dip2px(mContext, 13);
            params.width = SizeUtils.dip2px(mContext, 12);
            iv_pk_or_mic.setLayoutParams(params);
            ll_label_other.setBackgroundResource(R.drawable.bg_live_list_lablel_shape_red);

        } else {
            ll_label_other.setVisibility(View.GONE);

        }
   /*     //多人连麦显示
        if (liveRoomBean.isMulti == 1) {
            ll_label_other.setVisibility(View.VISIBLE);
            String content = TheLApp.context.getString(R.string.multi_mic);
            txt_label_other.setText(content);
            iv_pk_or_mic.setImageResource(R.mipmap.icn_multi_mic);
            ViewGroup.LayoutParams params = iv_pk_or_mic.getLayoutParams();
            params.height = SizeUtils.dip2px(mContext, 10);
            params.width = SizeUtils.dip2px(mContext, 12);
            iv_pk_or_mic.setLayoutParams(params);
            ll_label_other.setBackgroundResource(R.drawable.bg_live_list_lablel_shape_pink);
        }*/
        if (!TextUtils.isEmpty(liveRoomBean.distance)) {
            txt_distance.setVisibility(VISIBLE);
            txt_distance.setText(liveRoomBean.distance);
        } else {
            txt_distance.setVisibility(GONE);
        }
        final String avatar = liveRoomBean.user != null ? liveRoomBean.user.avatar : "";
        SimpleDraweeViewUtils.setImageUrl(img_avatar, avatar, TheLConstants.AVATAR_SMALL_SIZE, TheLConstants.AVATAR_SMALL_SIZE);
        reportLiveLog("exposure", pageId, liveRoomBean.rank_id, liveRoomBean.id, liveRoomBean.liveUsersCount, currentType, currentPosition);
        return this;
    }

    private void reportLiveLog(String activity, String pageid, String rank_id, String live_id, int viewer, int sort, int currentPosition) {
        String latitude = ShareFileUtils.getString(ShareFileUtils.LATITUDE, "0.0");
        String longitude = ShareFileUtils.getString(ShareFileUtils.LONGITUDE, "0.0");
        String fromPage = ShareFileUtils.getString(ShareFileUtils.rootSwitchPage, "");
        String fromPageId = ShareFileUtils.getString(ShareFileUtils.rootSwitchPageId, "");

        try {
            LogInfoBean logInfoBean = new LogInfoBean();
            logInfoBean.page = "live";
            logInfoBean.page_id = pageid;

            logInfoBean.activity = activity;

            LogInfoBean.LogsDataBean logsDataBean = new LogInfoBean.LogsDataBean();
            logsDataBean.rank_id = rank_id;
            logsDataBean.live_id = live_id;
            logsDataBean.viewer = viewer;
            logsDataBean.index = currentPosition;
            logInfoBean.from_page_id = fromPageId;
            logInfoBean.from_page = fromPage;
            switch (sort) {
                case TYPE_HOT:
                    logsDataBean.sort = "hot";
                    break;
                case TYPE_AUDIO:
                    logsDataBean.sort = "voice";
                    break;
                case TYPE_NEIGHBOUR:
                    logsDataBean.sort = "near";
                    break;
                case TYPE_NEW:
                    logsDataBean.sort = "new";
                    break;
                case TYPE_VEDIO:
                    logsDataBean.sort = "video";
                    break;

            }
            logInfoBean.data = logsDataBean;

            logInfoBean.lat = latitude;
            logInfoBean.lng = longitude;

            LiveLogUtils.getInstance().addLog(logInfoBean);

        } catch (Exception e) {

        }

    }
}
