package com.thel.modules.live.utils;

public enum LiveStatus {

    LIVE_PK, LIVE_LINK_MIC, LIVE_AUDIENCE_LINK_MIC, LIVE_NORMAL,LIVE_MULTI_LINK_MIC

}
