package com.thel.modules.main.me.bean;

import android.content.ContentValues;
import android.database.Cursor;

import com.thel.base.BaseDataBean;
import com.thel.db.MomentsDataBaseAdapter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Match bean
 *
 * @author lingwei
 */
public class MatchBean extends BaseDataBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * userId : 100903806
     * isFollow :
     * picList : [{"picWidth":204,"picHeight":272,"picUrl":"http://static.thel.co/app/album/100903806/3c2b0c30fc13d5697d2c644d1f9e5e53.jpg-wh2","longThumbnailUrl":"http://static.thel.co/app/album/100903806/3c2b0c30fc13d5697d2c644d1f9e5e53.jpg-w204"},{"picWidth":204,"picHeight":272,"picUrl":"http://static.thel.co/app/album/100903806/fd452e2c5c7cc7693f092e94dc58445a.jpg-wh2","longThumbnailUrl":"http://static.thel.co/app/album/100903806/fd452e2c5c7cc7693f092e94dc58445a.jpg-w204"},{"picWidth":204,"picHeight":272,"picUrl":"http://static.thel.co/app/album/100903806/80e75af3eec2fb7763a9b1d62bf3337a.jpg-wh2","longThumbnailUrl":"http://static.thel.co/app/album/100903806/80e75af3eec2fb7763a9b1d62bf3337a.jpg-w204"},{"picWidth":204,"picHeight":272,"picUrl":"http://static.thel.co/app/album/100903806/523ae2c129c4491309bbd6214df52d02.jpg-wh2","longThumbnailUrl":"http://static.thel.co/app/album/100903806/523ae2c129c4491309bbd6214df52d02.jpg-w204"},{"picWidth":204,"picHeight":153,"picUrl":"http://static.thel.co/app/album/100903806/e2bfb8714fa3ae0f354d713b206448cb.jpg-wh2","longThumbnailUrl":"http://static.thel.co/app/album/100903806/e2bfb8714fa3ae0f354d713b206448cb.jpg-w204"},{"picWidth":204,"picHeight":272,"picUrl":"http://static.thel.co/app/album/100903806/6372fc637b2732e547bbdcd199ecd94a.jpg-wh2","longThumbnailUrl":"http://static.thel.co/app/album/100903806/6372fc637b2732e547bbdcd199ecd94a.jpg-w204"},{"picWidth":204,"picHeight":272,"picUrl":"http://static.thel.co/app/album/100903806/7aa94cb719c760b8703bbb52e30fe787.jpg-wh2","longThumbnailUrl":"http://static.thel.co/app/album/100903806/7aa94cb719c760b8703bbb52e30fe787.jpg-w204"},{"picWidth":204,"picHeight":276,"picUrl":"http://static.thel.co/app/album/100903806/297e933689fdd44a9b82a02604cf0c51.jpg-wh2","longThumbnailUrl":"http://static.thel.co/app/album/100903806/297e933689fdd44a9b82a02604cf0c51.jpg-w204"},{"picWidth":204,"picHeight":272,"picUrl":"http://static.thel.co/app/album/100903806/b99becadfe057846937bb112e1a744e3.jpg-wh2","longThumbnailUrl":"http://static.thel.co/app/album/100903806/b99becadfe057846937bb112e1a744e3.jpg-w204"}]
     * nickName : Aaron✨药切克闹
     * intro : WX：1027116490
     * purpose : 0
     * age : 23
     * height : 170
     * weight : 54
     * horoscope : 9
     * professionalTypes : 1,0
     * matchCount : 56
     * birthday :
     * bgimg : http://static.rela.me/app/bgimg/9997/150d655d0d9cb9dd8a245216ae2bf58b.jpg
     * avatar :
     */
    /**
     * 用户图片url以逗号分隔
     */
    public String picLists;
    public long userId;
    public String isFollow;
    public String nickName;
    public String intro;
    public String purpose;
    public int age;
    public int height;
    public int weight;
    public String horoscope;
    public String professionalTypes;
    public int matchCount;
    public String birthday;
    public String bgimg;
    public String roleName;
    public int isSuperLike;//1表示对方超级喜欢自己
    public int isNewLike;//是否是新的喜欢

    public String superLikeNote;
    public String superLikeTime;
    public String activeTime;
    public String wantRole;
    public int vipLevel;
    public String location;
    public String distance;
    public int type;//0左滑，1右滑
    public int dataType;//0默认 1新手引导喜欢 2新手引导不喜欢 3匹配玩法提示 4完善个人资料提示
    public String occupation;  //职业
    public String livecity; //居住地
    public String affection; //感情状态 // (0=不想透露，1=单身，2=约会中，3=稳定关系，4=已婚，5=开放关系)
    public int hasBindingPartner; //是否绑定情侣，绑定1，未绑定0
    /**
     * 是否是回滚的数据
     */
    public int isRollback = 0;
    public String avatar;
    /**
     * 是否是向导页
     */
    public Boolean isGuide = false;
    public List<PicListBean> picList = new ArrayList<>();

    public static class PicListBean implements Serializable {
        /**
         * picWidth : 204
         * picHeight : 272
         * picUrl : http://static.thel.co/app/album/100903806/3c2b0c30fc13d5697d2c644d1f9e5e53.jpg-wh2
         * longThumbnailUrl : http://static.thel.co/app/album/100903806/3c2b0c30fc13d5697d2c644d1f9e5e53.jpg-w204
         */

        public int picWidth;
        public int picHeight;
        public String picUrl;
        public String longThumbnailUrl;

    }

    public void fromCursor(Cursor cursor) {

        userId = cursor.getLong(cursor.getColumnIndex(MomentsDataBaseAdapter.F_USERID));
        weight = cursor.getInt(cursor.getColumnIndex(MomentsDataBaseAdapter.F_WEIGHT));
        height = cursor.getInt(cursor.getColumnIndex(MomentsDataBaseAdapter.F_HEIGHT));
        age = cursor.getInt(cursor.getColumnIndex(MomentsDataBaseAdapter.F_AGE));
        matchCount = cursor.getInt(cursor.getColumnIndex(MomentsDataBaseAdapter.F_MATCH_COUNT));
        birthday = cursor.getString(cursor.getColumnIndex(MomentsDataBaseAdapter.F_BIRTHDAY));
        intro = cursor.getString(cursor.getColumnIndex(MomentsDataBaseAdapter.F_INTRO));
        horoscope = cursor.getString(cursor.getColumnIndex(MomentsDataBaseAdapter.F_HOROSCOPE));
        //        isFollow = cursor.getString(cursor.getColumnIndex(MomentsDataBaseAdapter.F_IS_FOLLOW));
        nickName = cursor.getString(cursor.getColumnIndex(MomentsDataBaseAdapter.F_NICK_NAME));
        picLists = cursor.getString(cursor.getColumnIndex(MomentsDataBaseAdapter.F_PICLIST));
        professionalTypes = cursor.getString(cursor.getColumnIndex(MomentsDataBaseAdapter.F_PROFESSIONAL_TYPES));
        purpose = cursor.getString(cursor.getColumnIndex(MomentsDataBaseAdapter.F_PURPOSE));
        bgimg = cursor.getString(cursor.getColumnIndex(MomentsDataBaseAdapter.F_BG_IMG));
        isRollback = cursor.getInt(cursor.getColumnIndex(MomentsDataBaseAdapter.F_IS_ROLLBACK));
    }

    public ContentValues getContentValues() {

        // 构造图片URL
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < picList.size(); i++) {
            MatchBean.PicListBean picListBean = picList.get(i);
            sb.append(picListBean.picUrl).append(",");
        }
        if (sb.length() > 0) {
            sb.deleteCharAt(sb.length() - 1);
        }
        picLists = sb.toString();
        ContentValues contentValues = new ContentValues();
        contentValues.put(MomentsDataBaseAdapter.F_USERID, userId);
        contentValues.put(MomentsDataBaseAdapter.F_WEIGHT, weight);
        contentValues.put(MomentsDataBaseAdapter.F_HEIGHT, height);
        contentValues.put(MomentsDataBaseAdapter.F_AGE, age);
        contentValues.put(MomentsDataBaseAdapter.F_MATCH_COUNT, matchCount);
        contentValues.put(MomentsDataBaseAdapter.F_BIRTHDAY, birthday);
        contentValues.put(MomentsDataBaseAdapter.F_INTRO, intro);
        contentValues.put(MomentsDataBaseAdapter.F_HOROSCOPE, horoscope);
        //        contentValues.put(MomentsDataBaseAdapter.F_IS_FOLLOW, isFollow);
        contentValues.put(MomentsDataBaseAdapter.F_NICK_NAME, nickName);
        contentValues.put(MomentsDataBaseAdapter.F_PICLIST, picLists);
        contentValues.put(MomentsDataBaseAdapter.F_PROFESSIONAL_TYPES, professionalTypes);
        contentValues.put(MomentsDataBaseAdapter.F_PURPOSE, purpose);
        contentValues.put(MomentsDataBaseAdapter.F_BG_IMG, bgimg);
        contentValues.put(MomentsDataBaseAdapter.F_IS_ROLLBACK, isRollback);

        return contentValues;
    }

    public void getUserPics() {


    }

    @Override
    public String toString() {
        return "MatchBean{" +
                "picLists='" + picLists + '\'' +
                ", userId=" + userId +
                ", isFollow='" + isFollow + '\'' +
                ", nickName='" + nickName + '\'' +
                ", intro='" + intro + '\'' +
                ", purpose='" + purpose + '\'' +
                ", age=" + age +
                ", height=" + height +
                ", weight=" + weight +
                ", horoscope='" + horoscope + '\'' +
                ", professionalTypes='" + professionalTypes + '\'' +
                ", matchCount=" + matchCount +
                ", birthday='" + birthday + '\'' +
                ", bgimg='" + bgimg + '\'' +
                ", roleName='" + roleName + '\'' +
                ", isSuperLike=" + isSuperLike +
                ", superLikeNote='" + superLikeNote + '\'' +
                ", superLikeTime='" + superLikeTime + '\'' +
                ", activeTime='" + activeTime + '\'' +
                ", wantRole='" + wantRole + '\'' +
                ", vipLevel=" + vipLevel +
                ", location='" + location + '\'' +
                ", distance='" + distance + '\'' +
                ", logType=" + type +
                ", dataType=" + dataType +
                ", occupation='" + occupation + '\'' +
                ", livecity='" + livecity + '\'' +
                ", isRollback=" + isRollback +
                ", avatar='" + avatar + '\'' +
                ", isGuide=" + isGuide +
                ", picList=" + picList +
                '}';
    }
}
