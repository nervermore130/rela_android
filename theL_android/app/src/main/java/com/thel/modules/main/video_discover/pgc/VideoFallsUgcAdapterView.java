package com.thel.modules.main.video_discover.pgc;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.generic.RoundingParams;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.video.VideoBean;
import com.thel.modules.main.video_discover.videofalls.RecyclerViewStateBean;
import com.thel.modules.main.video_discover.videofalls.widget.WebpSimpleDraweeView;
import com.thel.utils.StringUtils;
import com.thel.utils.Utils;

/**
 * Created by waiarl on 2018/3/19.
 */

public class VideoFallsUgcAdapterView extends RelativeLayout implements VideoFallAdapterViewImp<VideoFallsUgcAdapterView> {
    private final Context mContext;
    private RelativeLayout rel;
    private WebpSimpleDraweeView img_thumb;
    private LinearLayout lin_recommend;
    private TextView txt_like_count;
    private VideoBean mVideoBean;
    private int mPosition;
    private RecyclerViewStateBean mStateBean;
    private TextView txt_desc;
    private TextView txt_title;


    public VideoFallsUgcAdapterView(Context context) {
        this(context, null);
    }

    public VideoFallsUgcAdapterView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public VideoFallsUgcAdapterView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
        initViewState();
    }

    private void init() {
        inflate(mContext, R.layout.adapter_video_falls_ugc_item, this);
        rel = findViewById(R.id.rel);
        img_thumb = findViewById(R.id.img_thumb);
        lin_recommend = findViewById(R.id.lin_recommend);
        txt_like_count = findViewById(R.id.txt_like_count);
        txt_desc = findViewById(R.id.txt_desc);
        txt_title = findViewById(R.id.txt_title);
    }

    private void initViewState() {
        lin_recommend.setVisibility(View.GONE);
        txt_title.setVisibility(View.GONE);
        txt_desc.setVisibility(View.GONE);
    }

    @Override
    public VideoFallsUgcAdapterView initView(VideoBean videoBean, int position, RecyclerViewStateBean recyclerViewStateBean) {
        this.mVideoBean = videoBean;
        this.mPosition = position;
        this.mStateBean = recyclerViewStateBean;
        if (mVideoBean == null) {
            return this;
        }
        setRecommend();
        setThumb();
        setLikeCount();
        setDesc();
        setTitle();
        return this;
    }

    private void setRecommend() {
        if (mVideoBean.videoTag > 0) {
            lin_recommend.setVisibility(View.VISIBLE);
        }
    }

    private void setThumb() {
        img_thumb.initRecyclerViewState(mStateBean).setFullHeight(photoHeight);
        rel.getLayoutParams().height = (int) (photoHeight + divider);
        if (!TextUtils.isEmpty(mVideoBean.videoWebp) && Utils.getTotalRam() > 2) {
            img_thumb.setWebpUrl(mVideoBean.videoWebp);
        } else {
            img_thumb.setImageUrl(mVideoBean.image, photoWidth, photoHeight);
        }
        img_thumb.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadius(radius).setRoundingMethod(RoundingParams.RoundingMethod.OVERLAY_COLOR).setOverlayColor(ContextCompat.getColor(TheLApp.getContext(), R.color.white)));

        /***设置默认站位颜色***/
        int color = ContextCompat.getColor(TheLApp.getContext(), R.color.video_fall_default_color);
        final String videoColour = mVideoBean.videoColor;
        if (!TextUtils.isEmpty(videoColour) && videoColour.charAt(0) == '#') {
            try {
                color = Color.parseColor(videoColour);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        final Drawable drawable = new ColorDrawable(color);
        img_thumb.getHierarchy().setPlaceholderImage(drawable);
    }


    private void setLikeCount() {
        final int winkNum = mVideoBean.winkNum;
        String likeCount = winkNum + "";
        float count;
        if (winkNum > 9999) {
            if (Utils.isChaneseLanguage()) {//如果是中文版的，显示x.x万，保留小数点后一位
                count = Math.round(winkNum / 1000f) / 10f;
            } else {//显示x.xk，保留小数点后一位
                count = Math.round(winkNum / 100f) / 10f;
            }
            likeCount = StringUtils.getString(R.string.video_falls_like_count, count + "");
        }
        txt_like_count.setText(likeCount);
    }

    private void setDesc() {
        String desc = getDesc();
        if (!TextUtils.isEmpty(desc)) {
            txt_desc.setVisibility(View.VISIBLE);
            txt_desc.setText(desc);
        }

    }

    private String getDesc() {
        if (mVideoBean.distance <= 5000 && mVideoBean.distance >= 0) {
            if (mVideoBean.distance >= 1000) {
                final float dis = Math.round(mVideoBean.distance / 100f) / 10f;
                final String dist = dis + " km";
                return dist;
            } else {
                return StringUtils.getString(R.string.nearby_activity_nearby);
            }
        } else if (mVideoBean.interval < DAY && mVideoBean.interval >= 0) {
            if (mVideoBean.interval >= HOUR) {
                final long h = mVideoBean.interval / HOUR;
                return h + StringUtils.getString(R.string.info_hours_ago);
            } else if (mVideoBean.interval >= MINUTI) {
                final long m = mVideoBean.interval / MINUTI;
                return m + StringUtils.getString(R.string.info_minutes_ago);
            } else if (mVideoBean.interval > 0) {
                return mVideoBean.interval + StringUtils.getString(R.string.info_seconds_ago);
            } else {
                return StringUtils.getString(R.string.info_just_now);
            }
        }
        return null;
    }


    private void setTitle() {
        if (!TextUtils.isEmpty(mVideoBean.text)) {
            txt_title.setVisibility(View.VISIBLE);
        }
        txt_title.setText(mVideoBean.text + "");
    }

    @Override
    public VideoFallsUgcAdapterView getView() {
        return this;
    }


}
