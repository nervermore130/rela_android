package com.thel.modules.main.me.aboutMe;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.bean.SimpleStikerBean;
import com.thel.bean.StickerPackListBean;
import com.thel.bean.StickerPackListNetBean;
import com.thel.bean.StickerPackNetBean;
import com.thel.constants.TheLConstants;
import com.thel.imp.sticker.StickerUtils;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.utils.ImageUtils;
import com.thel.utils.L;

import org.reactivestreams.Subscription;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * 我购买过的表情包页面
 * Created by lingwei on 2017/11/2.
 */

public class MyPurchasedStickerPacksActivity extends BaseActivity {
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.lin_back)
    LinearLayout linBack;
    @BindView(R.id.txt_title)
    TextView txtTitle;
    @BindView(R.id.img_more)
    ImageView imgMore;
    @BindView(R.id.lin_more)
    LinearLayout linMore;
    @BindView(R.id.title_layout)
    RelativeLayout titleLayout;
    @BindView(R.id.listView)
    ListView listView;
    @BindView(R.id.img_reload)
    ImageView imgReload;
    @BindView(R.id.txt_reload)
    TextView txtReload;
    @BindView(R.id.lin_reload)
    LinearLayout linReload;
    @BindView(R.id.lin_default)
    LinearLayout linDefault;
    private ArrayList<SimpleStikerBean> listPlus = new ArrayList<SimpleStikerBean>();
    private boolean hasLoadData;
    private PurchasedStickerPacksAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sticker_store_activity);
        ButterKnife.bind(this);
        initData();
        setLisenter();
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void setLisenter() {

        adapter.setStickerLinstener(new PurcheasedStickerListener() {
            @Override
            public void getStickerPackDetail(final int position, SimpleStikerBean baen) {

                Flowable<StickerPackNetBean> flowable = RequestBusiness.getInstance().getStickerPackDetail(baen.id, position);
                flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<StickerPackNetBean>() {

                    @Override
                    public void onNext(StickerPackNetBean stickerPackBean) {
                        super.onNext(stickerPackBean);
                        getStickerPackDetailBean(stickerPackBean, position);
                    }

                    @Override
                    public void onError(Throwable t) {
                        L.d("onerror", t.getMessage());
                        loadFailed();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
            }
        });
    }

    private void getStickerPackDetailBean(StickerPackNetBean stickerPackBean, int position) {
        if (stickerPackBean.data == null)
            return;
        listPlus.get(position).status = 2;
        adapter.updateSingleRow(listView, position + listView.getHeaderViewsCount());
        StickerUtils.saveOneSticker(stickerPackBean.data,null);
      //  saveStickPackDetail(stickerPackBean, TheLConstants.F_DOWNLOAD_ROOTPATH, Utils.getStickerFileName() + stickerPackBean.data.id);

    }

    private void initData() {
        txtTitle.setText(R.string.my_purchased_sticker_packs);
        adapter = new PurchasedStickerPacksAdapter(this, listPlus);
        listView.setAdapter(adapter);
        listPlus.clear();
        Flowable<StickerPackListNetBean> flowable = RequestBusiness.getInstance().getPurchasedStickerPacks();
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<StickerPackListNetBean>() {

            @Override
            public void onNext(StickerPackListNetBean stickerPackListBean) {
                super.onNext(stickerPackListBean);
                getPurchasedStickerPacks(stickerPackListBean.data);

            }

            @Override
            public void onError(Throwable t) {
                loadFailed();

            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void getPurchasedStickerPacks(StickerPackListBean stickerPackListBean) {
        if (stickerPackListBean == null)
            return;
        if (stickerPackListBean.list != null) {

            filterDwoenloadedStickerPacks(stickerPackListBean.list);
            listPlus.addAll(stickerPackListBean.list);
            hasLoadData = true;
            refreshUi();
            if (listPlus.size() == 0) {
                linDefault.setVisibility(View.VISIBLE);
            }
        }
    }

    private void refreshUi() {
        if (hasLoadData) {
            linReload.setVisibility(View.GONE);
            adapter.notifyDataSetChanged();

        } else {
            loadFailed();
        }
    }

    private void loadFailed() {
        try {
            linReload.setVisibility(View.VISIBLE);
            imgReload.clearAnimation();
            txtReload.setText(getString(R.string.info_reload));
        } catch (Exception e) {

        }
    }

    private void filterDwoenloadedStickerPacks(final ArrayList<SimpleStikerBean> stickerPackBeanArrayList) {
        List<String> Sp_StickerId = StickerUtils.getMyStickerIdList();

        for (SimpleStikerBean bean : stickerPackBeanArrayList) {
            for (String id : Sp_StickerId) {
                if (id.equals(String.valueOf(bean.id))) {// 已下载的表情包状态置为2
                    bean.status = 2;
                    L.d("Sp_StickerId", "hahahah" + id);

                }
            }
        }

    }

    @OnClick(R.id.lin_back)
    public void onViewClicked() {
        finish();
    }

    private class PurchasedStickerPacksAdapter extends BaseAdapter {

        private ArrayList<SimpleStikerBean> stickerPacks;
        private LayoutInflater mInflater;

        private Context context;
        private PurcheasedStickerListener mpurcheasedStickerListener;

        public PurchasedStickerPacksAdapter(Context context, ArrayList<SimpleStikerBean> beans) {
            this.context = context;
            mInflater = (LayoutInflater) TheLApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            stickerPacks = beans;
        }

        @Override
        public int getCount() {
            return stickerPacks.size();
        }

        @Override
        public Object getItem(int position) {
            return stickerPacks.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public void updateSingleRow(ListView listView, int position) {
            int firstVisiblePosition = listView.getFirstVisiblePosition();
            int lastVisiblePosition = listView.getLastVisiblePosition();
            if (position >= firstVisiblePosition && position <= lastVisiblePosition) {
                View view = listView.getChildAt(position - firstVisiblePosition);
                if (view.getTag() instanceof HoldView) {
                    HoldView holdView = (HoldView) view.getTag();
                    refreshItem(position - listView.getHeaderViewsCount(), holdView);
                }
            }
        }

        private void refreshItem(final int position, HoldView holdView) {
            holdView.txt_title.setVisibility(View.VISIBLE);
            holdView.txt_downloaded.setVisibility(View.GONE);
            holdView.progress_bar.setVisibility(View.GONE);
            holdView.txt_operation.setVisibility(View.GONE);
            holdView.img_type.setVisibility(View.GONE);

            final SimpleStikerBean stickerPackBean = stickerPacks.get(position);

            holdView.txt_title.setVisibility(View.GONE);

            holdView.txt_name.setText(stickerPackBean.title);
            holdView.txt_desc.setText(stickerPackBean.summary);

            if (stickerPackBean.thumbnail != null)
                holdView.img.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(stickerPackBean.thumbnail, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE))).build()).setAutoPlayAnimations(true).build());

            if (stickerPackBean.status == 0) {
                holdView.txt_operation.setVisibility(View.VISIBLE);
                holdView.txt_operation.setText(stickerPackBean.generatePriceShow());
            } else if (stickerPackBean.status == 1) {
                holdView.txt_operation.setVisibility(View.VISIBLE);
                holdView.txt_operation.setText(getString(R.string.sticker_store_act_download));
            } else if (stickerPackBean.status == 2) {
                holdView.txt_downloaded.setVisibility(View.VISIBLE);
            }


            holdView.txt_operation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mpurcheasedStickerListener != null) {

                        mpurcheasedStickerListener.getStickerPackDetail(position, stickerPackBean);
                    }
                }
            });
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            HoldView holdView = null;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.sticker_store_list_item, parent, false);
                holdView = new HoldView();

                holdView.img = convertView.findViewById(R.id.img);
                holdView.txt_title = convertView.findViewById(R.id.txt_title);
                holdView.txt_name = convertView.findViewById(R.id.txt_name);
                holdView.txt_downloaded = convertView.findViewById(R.id.txt_downloaded);
                holdView.txt_operation = convertView.findViewById(R.id.txt_operation);
                holdView.progress_bar = convertView.findViewById(R.id.progress_bar);
                holdView.img_type = convertView.findViewById(R.id.img_type);
                holdView.txt_desc = convertView.findViewById(R.id.txt_desc);

                convertView.setTag(holdView); // 把holdview缓存下来
            } else {
                holdView = (HoldView) convertView.getTag();
            }

            refreshItem(position, holdView);

            return convertView;
        }

        class HoldView {
            SimpleDraweeView img;
            TextView txt_title;
            TextView txt_name;
            TextView txt_desc;
            TextView txt_downloaded;
            TextView txt_operation;
            ProgressBar progress_bar;
            SimpleDraweeView img_type;
        }

        public void setStickerLinstener(PurcheasedStickerListener purcheasedStickerListener) {
            this.mpurcheasedStickerListener = purcheasedStickerListener;
        }
    }

    interface PurcheasedStickerListener {
        void getStickerPackDetail(int position, SimpleStikerBean baen);
    }

}
