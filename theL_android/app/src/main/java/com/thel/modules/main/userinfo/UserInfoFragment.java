package com.thel.modules.main.userinfo;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.tabs.TabLayout;

import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.google.gson.reflect.TypeToken;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.HiddenLoveBean;
import com.thel.bean.UserInfoChatGuideBean;
import com.thel.bean.gift.WinkCommentBean;
import com.thel.bean.gift.WinkGiftBean;
import com.thel.bean.gift.WinkGiftListBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.user.BlockBean;
import com.thel.bean.user.UserInfoBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.growingio.GrowingIoConstant;
import com.thel.imp.AutoRefreshImp;
import com.thel.imp.BaseFunctionFragment;
import com.thel.imp.black.BlackUtils;
import com.thel.imp.follow.FollowStatusChangedImpl;
import com.thel.imp.follow.FollowStatusChangedListener;
import com.thel.imp.wink.WinkUtils;
import com.thel.manager.ChatServiceManager;
import com.thel.modules.main.me.aboutMe.UpdateUserInfoActivity;
import com.thel.modules.main.me.timemachine.TimeMachineActivity;
import com.thel.modules.main.messages.ChatActivity;
import com.thel.modules.main.messages.bean.MsgBean;
import com.thel.modules.main.messages.utils.MsgUtils;
import com.thel.modules.main.messages.utils.PushUtils;
import com.thel.modules.main.userinfo.moment.UserInfoMomentFragment;
import com.thel.ui.widget.GuideView;
import com.thel.ui.widget.tourguide.MyTourGuide;
import com.thel.utils.AppInit;
import com.thel.utils.DeviceUtils;
import com.thel.utils.DialogUtil;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SharedPrefUtils;
import com.thel.utils.SizeUtils;
import com.thel.utils.StringUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import tourguide.tourguide.Overlay;
import tourguide.tourguide.Pointer;
import tourguide.tourguide.ToolTip;

import static android.app.Activity.RESULT_OK;

/**
 * Created by waiarl on 2017/10/18.
 */

public class UserInfoFragment extends BaseFunctionFragment implements UserInfoContract.View, AppBarLayout.OnOffsetChangedListener, UserInfoActivity.UserInfoBackPressedListener {
    public static final String TAG = UserInfoFragment.class.getSimpleName();
    private String userId;

    private DrawerLayout drawlayout;
    private UserInfoRightView userinfoRightView;
    private SwipeRefreshLayout swipe_container;
    private AppBarLayout appbarlayout;
    private CollapsingToolbarLayout toolbarlayout;
    private LinearLayout lin_parent;
    private UserInfoTopMainView userInfoTopMainView;
    private View top_gradient;
    private Toolbar toolbar;
    private RelativeLayout rel_title;
    private LinearLayout lin_back;
    private LinearLayout lin_title;
    private TextView txt_title;
    private LinearLayout lin_title_cions;
    private LinearLayout lin_right;
    private LinearLayout lin_more;
    private LinearLayout lin_write_moment;
    private UserInfoFloatingLayout lin_floating;
    private ImageView fab_chat;
    private ImageView fab_wink;
    private RelativeLayout bg_block;
    private ImageView bg_block_img;
    private TextView txt_content;
    private TextView show_cancel;
    private TextView txt_block_ok;
    private TextView txt_block_her;
    private LottieAnimationView img_sendwink;
    private UserInfoMomentHeaderView userInfoMomentHeaderView;


    private UserInfoContract.Presenter presenter;
    private UserInfoBean userInfoBean;
    private List<MomentsBean> momentsBeanList = new ArrayList<>();
    private boolean isMySelf = false;//是否是自己
    private ImageView img_send_card;
    private ImageView img_new_card_point;
    private int followStatus;
    private String isBlackYa = "";
    private String isHiddenLoveYa;
    private Calendar calendar;
    private int currentOffset = 0;
    private long lastClickTime = 0;
    private WinkGiftListBean winkGiftListBean;
    private GiftWinkSuccessReceiver giftWinkSuccessReceiver;//礼物挤眼广播
    private int curPage;
    private boolean haveNextPage;
    private boolean isErrorBlack = false;
    private MomentReleaseReceiver receiver;
    private StickMomentReceiver mStickMomentReceiver;
    private String from_type;
    private MyTourGuide tourGuide;
    private boolean isRefreshing = false;
    private ViewPager viewpager;
    private List<Fragment> fragmentList = new ArrayList<>();
    private TabLayout tablayout;
    private UserInfoMomentAdapter adapter;
    private UserInfoTabView tabview_moment;
    private UserInfoTabView tabview_video;
    private UserInfoMomentFragment mUserInfoMomentFragment;
    private UserInfoMomentFragment videoFragment;
    private ImageView time_machine_iv;
    private GuideView mGuideView;
    private GuideView mChatGuideView;
    private ImageView iv_look_me;
    private boolean vip_incognito = false;
    private TextView anonymous_browsing_tv;

    public static Fragment getInstance(Bundle bundle) {
        UserInfoFragment instance = new UserInfoFragment();
        instance.setArguments(bundle);
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Bundle bundle = getArguments();
        if (bundle != null) {
            userId = bundle.getString(TheLConstants.BUNDLE_KEY_USER_ID);
            from_type = bundle.getString(TheLConstants.BUNDLE_KEY_INTENT_FROM);
        }
        isMySelf = ShareFileUtils.getString(ShareFileUtils.ID, "").equals(userId);
        vip_incognito = ShareFileUtils.getInt(ShareFileUtils.VIP_INCOGNITO, 0) == 1;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_user_info, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        findViewById();
        setDelete();
        setImamersion();
        initViewState();
        initViewData();
        registerReceiver();
    }

    private void registerReceiver() {
        giftWinkSuccessReceiver = new GiftWinkSuccessReceiver();
        final IntentFilter giftFilter = new IntentFilter(TheLConstants.BROADCAST_GIFT_WINK_SUCCESS);
        getContext().registerReceiver(giftWinkSuccessReceiver, giftFilter);
        receiver = new MomentReleaseReceiver();
        getContext().registerReceiver(receiver, new IntentFilter(TheLConstants.BROADCAST_FAILED_MOMENTS_CHECK_ACTION));
        mStickMomentReceiver = new StickMomentReceiver();
        getContext().registerReceiver(mStickMomentReceiver, new IntentFilter(TheLConstants.BROADCAST_STICK_MOMENT));

    }

    private void setDelete() {
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        new UserInfoPresenter(this);
        setListener();
        if (isBlack()) {//先本地查询是否已屏蔽了对方
            return;
        }
        getData();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            getContext().unregisterReceiver(giftWinkSuccessReceiver);
            getContext().unregisterReceiver(receiver);
            getContext().unregisterReceiver(mStickMomentReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void findViewById() {
        //外层
        drawlayout = (DrawerLayout) findViewById(R.id.drawerlayout);
        userinfoRightView = (UserInfoRightView) findViewById(R.id.user_info_right_view);
        swipe_container = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        ViewUtils.initSwipeRefreshLayout(swipe_container);
        appbarlayout = (AppBarLayout) findViewById(R.id.appbarlayout);
        toolbarlayout = (CollapsingToolbarLayout) findViewById(R.id.toolbarlayout);
        lin_parent = (LinearLayout) findViewById(R.id.lin_parent);
        userInfoTopMainView = (UserInfoTopMainView) findViewById(R.id.user_info_top_main_view);
        top_gradient = findViewById(R.id.top_gradient);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        //title
        rel_title = (RelativeLayout) findViewById(R.id.rel_title);
        lin_back = (LinearLayout) findViewById(R.id.lin_back);
        lin_title = (LinearLayout) findViewById(R.id.lin_title);
        txt_title = (TextView) findViewById(R.id.txt_title);
        iv_look_me = (ImageView) findViewById(R.id.iv_look_me);
        anonymous_browsing_tv = (TextView) findViewById(R.id.anonymous_browsing_tv);
        lin_title_cions = (LinearLayout) findViewById(R.id.lin_title_icons);
        lin_right = (LinearLayout) findViewById(R.id.lin_right);
        img_send_card = (ImageView) findViewById(R.id.img_send_card);
        img_new_card_point = (ImageView) findViewById(R.id.img_new_card_point);
        lin_more = (LinearLayout) findViewById(R.id.lin_more);
        lin_write_moment = (LinearLayout) findViewById(R.id.lin_write_moment);
        //recyclerview
        //floating
        lin_floating = (UserInfoFloatingLayout) findViewById(R.id.lin_floating);
        fab_chat = (ImageView) findViewById(R.id.fab_chat);
        fab_wink = (ImageView) findViewById(R.id.fab_wink);
        //block
        bg_block = (RelativeLayout) findViewById(R.id.bg_block);
        bg_block_img = (ImageView) findViewById(R.id.bg_block_img);
        txt_content = (TextView) findViewById(R.id.txt_content);
        show_cancel = (TextView) findViewById(R.id.show_cancel);
        txt_block_ok = (TextView) findViewById(R.id.txt_block_ok);
        txt_block_her = (TextView) findViewById(R.id.txt_block_her);
        //lottie
        img_sendwink = (LottieAnimationView) findViewById(R.id.img_sendwink);

        userInfoMomentHeaderView = (UserInfoMomentHeaderView) findViewById(R.id.user_info_moment_header_view);
        final ViewGroup.LayoutParams param = userinfoRightView.getLayoutParams();
        param.width = AppInit.displayMetrics.widthPixels;
        viewpager = (ViewPager) findViewById(R.id.viewpager);
        tabview_moment = (UserInfoTabView) findViewById(R.id.tabview_moment);
        tabview_video = (UserInfoTabView) findViewById(R.id.tabview_video);
        time_machine_iv = (ImageView) findViewById(R.id.time_machine_iv);
        mGuideView = userInfoTopMainView.findViewById(R.id.guide_view);
        mChatGuideView = (GuideView) findViewById(R.id.chat_guide_view);
        setViewpager();

    }

    private void setViewpager() {
        initFragmentList();
        adapter = new UserInfoMomentAdapter(getChildFragmentManager(), fragmentList, null);
        viewpager.setAdapter(adapter);
        setMomentNum(0);
        setVideoNum(0);
        viewpager.setCurrentItem(0);
        setCurrentTab(0);
    }

    private void setCurrentTab(int index) {
        tabview_moment.setSelected(index == 0);
        tabview_video.setSelected(index == 1);
    }

    private void setMomentNum(int num) {
        String title = "";
        if (num <= 1) {
            title = num + StringUtils.getString(R.string.userinfo_activity_moments_num_odd);
        } else {
            title = num + StringUtils.getString(R.string.userinfo_activity_moments_num_even1);
        }
        tabview_moment.initView(title);
    }

    private void setVideoNum(int num) {
        String title = "";
        if (num <= 1) {
            title = num + " " + StringUtils.getString(R.string.videos);
        } else {
            title = num + " " + StringUtils.getString(R.string.videos);
        }
        tabview_video.initView(title);

    }


    private void initFragmentList() {
        fragmentList.clear();
        final Bundle bundle = new Bundle();
        bundle.putString(TheLConstants.BUNDLE_KEY_USER_ID, userId);
        bundle.putInt(TheLConstants.BUNDLE_KEY_ACTION_TYPE, UserInfoMomentFragment.TYPE_MOMENT);
        mUserInfoMomentFragment = UserInfoMomentFragment.getInstance(bundle);

        final Bundle bundle1 = new Bundle();
        bundle1.putString(TheLConstants.BUNDLE_KEY_USER_ID, userId);
        bundle1.putInt(TheLConstants.BUNDLE_KEY_ACTION_TYPE, UserInfoMomentFragment.TYPE_VIDEO);
        videoFragment = UserInfoMomentFragment.getInstance(bundle1);
        Collections.addAll(fragmentList, mUserInfoMomentFragment, videoFragment);

        mUserInfoMomentFragment.setMomentNumCallback(new UserInfoMomentFragment.MomentNumCallback() {
            @Override
            public void getMomentNum(int num, int type) {
                setMomentNum(num);
            }
        });

    }

    private void initViewData() {
        userInfoTopMainView.initView(userId);
    }

    private void initViewState() {
        final ViewGroup.LayoutParams param = userinfoRightView.getLayoutParams();
        param.width = AppInit.displayMetrics.widthPixels;
        txt_title.setAlpha(0);
        setButtonEnable(false);
        setMySelfView();
        if (TheLConstants.RELA_ACCOUNT.equals(userId)) {//如果是L君的ID,则下面聊天图标变化
            fab_chat.setImageResource(R.mipmap.btn_info_chat_rela_account);
        }
    }

    private void setButtonEnable(boolean enable) {
        lin_more.setEnabled(enable);
        fab_chat.setEnabled(enable);
        fab_wink.setEnabled(enable);
        userInfoTopMainView.setFollowEnable(enable);
    }

    protected View findViewById(int resId) {
        return getView().findViewById(resId);
    }

    private void getData() {
        swipe_container.post(new Runnable() {
            @Override
            public void run() {
                swipe_container.setRefreshing(true);
            }
        });
        isRefreshing = true;
        presenter.getUserInfoData(userId);
        // presenter.getRefreshUserInfoMomentListData(userId);

    }

    /**
     * 沉浸式 顶部阴影高度
     */
    private void setImamersion() {
        final ViewGroup.LayoutParams param = top_gradient.getLayoutParams();
        final int statusHeight = DeviceUtils.getStatusBarHeight(TheLApp.getContext());
        param.height = (int) (getResources().getDimension(R.dimen.title_height) + statusHeight);
    }

    /**
     * 是自己的个人主页还是别人的个人主页
     */
    private void setMySelfView() {
        uploadGIO();

        userInfoTopMainView.initView(userId);
        img_new_card_point.setVisibility(View.INVISIBLE);
        if (isMySelf) {
            iv_look_me.setVisibility(View.GONE);
            lin_write_moment.setVisibility(View.VISIBLE);
            lin_floating.setVisibility(View.GONE);
            lin_floating.setNeverShow(true);
            img_send_card.setVisibility(View.GONE);
            userinfoRightView.setReviseInfoShow();
            lin_more.setVisibility(View.GONE);
            time_machine_iv.setVisibility(View.VISIBLE);
        } else {
            if (vip_incognito) {
                iv_look_me.setVisibility(View.VISIBLE);

            }
            lin_write_moment.setVisibility(View.GONE);
            lin_more.setVisibility(View.VISIBLE);
            img_send_card.setVisibility(View.VISIBLE);
            if (SharedPrefUtils.getBoolean(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.REMEMBER_CARD_FIRST_CLICK, false)) {
                img_new_card_point.setVisibility(View.VISIBLE);
            }
            lin_floating.setVisibility(View.VISIBLE);
            lin_floating.setNeverShow(false);
            time_machine_iv.setVisibility(View.GONE);
            show_to_guide();
        }
    }

    private void uploadGIO() {
        GrowingIOUtil.track(getActivity(), GrowingIoConstant.KEY_USER_PAGE, Utils.isMyself(userId) ? GrowingIoConstant.USER_PAGE_ME : GrowingIoConstant.USER_PAGE_HER);
    }

    /**
     * 新手挤眼引导
     */
    private void show_to_guide() {
        if (!SharedPrefUtils.getBoolean(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.WINK_GUIDE, false)) {
            tourGuide = MyTourGuide.init(getActivity(), new MyTourGuide.GlobalLayoutCallback() {
                @Override
                public void onGlobalLayout() {
                    final com.thel.ui.widget.tourguide.FrameLayoutWithHole overlay1 = tourGuide.getOverlay();
                    L.i(TAG, "overlay1==null?" + (overlay1 == null));
                    if (overlay1 != null) {
                        overlay1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                clearTourGuide();
                            }
                        });
                    }
                }
            }).with(MyTourGuide.Technique.Click).setPointer(new Pointer()).setToolTip(new ToolTip().setBackgroundColor(getResources().getColor(R.color.tab_normal)).setTitle(TheLApp.getContext().getString(R.string.info_note)).setDescription(TheLApp.getContext().getString(R.string.wink_hint)).setGravity(Gravity.TOP)).setOverlay(new Overlay().setBackgroundColor(ContextCompat.getColor(TheLApp.getContext(), R.color.view_shade_color))).playOn(fab_wink);
        }
    }

    private void clearTourGuide() {
        if (tourGuide != null) {
            tourGuide.cleanUp();
            SharedPrefUtils.setBoolean(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.WINK_GUIDE, true);
        }
    }

    @Override
    public void setPresenter(UserInfoContract.Presenter presenter) {
        this.presenter = presenter;
        userInfoTopMainView.setPresenter(presenter);
    }

    @Override
    public void showUserInfoCommonViewData(UserInfoBean userInfoBean) {
        this.userInfoBean = userInfoBean;
        followStatus = userInfoBean.followStatus;
        isBlackYa = userInfoBean.isBlack;
        isHiddenLoveYa = userInfoBean.isHiddenLove;
        setWinkVisible();
        setButtonEnable(true);
        setVideoNum(userInfoBean.videosTotalNum);
        showChatGuide();
    }

    /**
     * 设置可挤眼状态
     */
    private void setWinkVisible() {
        setTomorrowCalendar();
        if (userInfoBean.winked.free == 0) {
            fab_wink.setAlpha(1f);
        } else {
            fab_wink.setAlpha(0.5f);
            WinkUtils.addMyTodayWinkUser(userId);
        }
    }

    private void setTomorrowCalendar() {
        calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
    }

    @Override
    public void showUserInfoMainTopViewData(UserInfoBean userInfoBean) {
        userInfoTopMainView.initView(userInfoBean);
    }

    @Override
    public void showUserInfoRightViewData(UserInfoBean userInfoBean) {
        userinfoRightView.refreshUI(userInfoBean);
    }

    @Override
    public void showUserInfoMomentHeaderView(UserInfoBean userInfoBean) {
        userInfoMomentHeaderView.initView(userInfoBean);
    }

    @Override
    public void showRefreshMomentListData(List<MomentsBean> list) {
    }

    @Override
    public void showLoadMoreMomentListData(List<MomentsBean> list) {
    }

    @Override
    public void loadMoreFailed() {
    }

    @Override
    public void emptyMoment() {

    }

    @Override
    public void requestFinish() {
        L.i("refresh", "requestFinish");
        if (swipe_container != null) {
            swipe_container.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (swipe_container.isRefreshing())
                        swipe_container.setRefreshing(false);
                }
            }, 1500);
        }
        isRefreshing = false;
        closeLoading();
    }


    @Override
    public void showLoadingDialog() {
        showLoading();
    }

    @Override
    public void showSecretlyFollow(String userId, int actionType) {
        if (userInfoBean.secretly == 1) {       //只要是成功返回，悄悄关注状态就相反。
            userInfoBean.secretly = 0;
        } else {
            userInfoBean.secretly = 1;
        }
        userInfoTopMainView.refreshFollowBtn(userInfoBean.followStatus);
    }

    @Override
    public boolean haveNextPage(boolean haveNextPage) {
        return haveNextPage;
    }

    @Override
    public void refreshMomentNum(int momentsTotalNum) {
        userInfoMomentHeaderView.refreshMomentNum(momentsTotalNum);
    }

    @Override
    public void pullUserIntoBlock(BlockBean blockBean) {
        if (isBlackYa.equals("0")) {
            isBlackYa = "1";
        } else {
            isBlackYa = "0";
        }
        userInfoBean.isBlack = isBlackYa;
        //          DataBaseAdapter.getInstance(getActivity()).saveBlack((BlockBean) rcb.responseDataObj);
        // 发送一条屏蔽消息
        //        if (userInfoBean == null) {// 已被她屏蔽
        //            SendMsgUtils.getInstance().sendMessage(MsgUtils.createMsgBean(MsgBean.MSG_TYPE_ADD_TO_BLACK_LIST, "", 1, blockBean.userId + "", blockBean.userName, blockBean.avatar));
        //        } else {// 未被她屏蔽
        ChatServiceManager.getInstance().sendMsg(MsgUtils.createMsgBean(MsgBean.MSG_TYPE_ADD_TO_BLACK_LIST, "", 1, userInfoBean.id, userInfoBean.nickName, userInfoBean.avatar));
        //        }
        closeLoading();
        DialogUtil.showToastShort(getActivity(), getThelString(R.string.myblock_activity_block_success));
        // NearbyFragmentActivity.needRefresh = true;
        if (getActivity() != null) {
            getActivity().finish();
        }
        /**
         * 由本地已经做过处理，所以只保存于发广播
         */
        BlackUtils.saveOneBlackUser(userInfoBean.id);
    }

    @Override
    public void showHiddenLove(HiddenLoveBean hiddenLoveBean) {
        if (isHiddenLoveYa.equals("0")) {
            isHiddenLoveYa = "1";
        } else if (isHiddenLoveYa.equals("1")) {
            isHiddenLoveYa = "0";
        }
        // 如果双向暗恋了，则要发消息并且提示双方已经暗恋了
        if (hiddenLoveBean.HiddenLoveSuccess.equals(HiddenLoveBean.HIDEN_LOVE_EACH_OTHER)) {
            isHiddenLoveYa = "3";
            // todo 发暗恋消息
            // MessageUtils.saveMessageToDB(MsgUtils.createMsgBean(MsgBean.MSG_TYPE_HIDDEN_LOVE, "", 1, userId, userInfoBean.nickname, userInfoBean.avatar));
            DialogUtil.showToastShort(getActivity(), TheLApp.getContext().getString(R.string.message_text_hiddenLove));
        } else if (hiddenLoveBean.HiddenLoveSuccess.equals(HiddenLoveBean.HIDEN_LOVE_NOT_EACH_OTHER)) { // 如果单方暗恋，则提示“已暗恋”
            if (isHiddenLoveYa.equals("1")) {
                DialogUtil.showToastShort(getActivity(), TheLApp.getContext().getString(R.string.userinfo_activity_hiddenlove_success));
            } else if (isHiddenLoveYa.equals("0")) {
                DialogUtil.showToastShort(getActivity(), TheLApp.getContext().getString(R.string.userinfo_activity_hiddenlove_cancel));
            }
        }
        userInfoBean.isHiddenLove = isHiddenLoveYa;
    }

    /**
     * 把我屏蔽了
     */
    @Override
    public void isBlockMe() {

        if (getActivity() != null) {
            if (tourGuide != null) {
                tourGuide.cleanUp();

            }
            swipe_container.setRefreshing(false);
            bg_block_img.setImageResource(R.mipmap.icn_default_forbid);
            bg_block.setVisibility(View.VISIBLE);
            txt_block_ok.setVisibility(View.VISIBLE);
            txt_block_her.setVisibility(View.VISIBLE);
            txt_content.setText(TheLApp.getContext().getString(R.string.info_had_bean_added_to_black));
            lin_more.setVisibility(View.GONE);
            txt_block_her.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ViewUtils.preventViewMultipleClick(v, 1000);
                    showLoading();
                    isErrorBlack = true;
                    BlackUtils.saveOneNetBlacker(userId);
                    txt_block_her.setVisibility(View.INVISIBLE);
                }
            });
            txt_block_ok.setOnClickListener(new View.OnClickListener() {//屏蔽的时候关闭页面
                @Override
                public void onClick(View v) {
                    ViewUtils.preventViewMultipleClick(v, 1000);
                    getActivity().finish();
                }
            });
            userInfoTopMainView.setMeBackgroundClickListener(null);
            // }
        }
    }

    @Override
    public void illegalUser() {
        if (getActivity() == null) {
            return;
        }
        if (tourGuide != null) {
            tourGuide.cleanUp();
        }
        swipe_container.setRefreshing(false);
        bg_block.setVisibility(View.VISIBLE);
        bg_block_img.setImageResource(R.mipmap.icn_default_forbid);
        txt_content.setText(TheLApp.getContext().getString(R.string.info_user_was_blocked));
        lin_more.setVisibility(View.GONE);
        if (UserInfoActivity.FROM_TYPE_FOLLOW.equals(from_type)) {//只有来之关注页的才显示取消关注按钮
            txt_block_her.setVisibility(View.VISIBLE);
            txt_block_her.setText(TheLApp.getContext().getString(R.string.unfollow));

            txt_block_her.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ViewUtils.preventViewMultipleClick(v, 1000);
                    //   if (cancelFollow == true) {
                    showLoading();
                    show_cancel.setVisibility(View.VISIBLE);
                    //  txt_block_her.setText(getString(R.string.info_back));
                    //requestBussiness.followUser(new DefaultNetworkHelper(UserInfoActivity.this), userId, "0");
                    FollowStatusChangedImpl.followUserWithNoDialog(userId, FollowStatusChangedImpl.ACTION_TYPE_CANCEL_FOLLOW, "", "");
                    getActivity().finish();
/*
                            } else {
                                //                            isUserAbandoned = true;
                                cancelFollow = false;
                                closeLoading();
                                finish();
                            }*/

                }
            });
        } else {
            txt_block_ok.setVisibility(View.VISIBLE);
            txt_block_ok.setOnClickListener(new View.OnClickListener() {//关闭页面
                @Override
                public void onClick(View v) {
                    ViewUtils.preventViewMultipleClick(v, 1000);
                    getActivity().finish();
                }
            });
        }
    }

    @Override
    public void notExistUser() {
        bg_block.setVisibility(View.VISIBLE);
        bg_block_img.setImageResource(R.mipmap.icn_default_forbid);
        txt_content.setText(TheLApp.getContext().getString(R.string.user_not_available));
        lin_more.setVisibility(View.GONE);
        txt_block_her.setVisibility(View.VISIBLE);
        txt_block_her.setText(TheLApp.getContext().getString(R.string.info_ok));
        txt_block_her.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
        if (UserInfoActivity.FROM_TYPE_FOLLOW.equals(from_type)) {
            show_cancel.setVisibility(View.VISIBLE);
            show_cancel.setText(TheLApp.getContext().getString(R.string.unfollow));
            show_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FollowStatusChangedImpl.followUserWithNoDialog(userId, FollowStatusChangedListener.ACTION_TYPE_CANCEL_FOLLOW, "", "");
                    getActivity().finish();
                }
            });
        }
    }

    /**
     * 此用户已注销
     */
    @Override
    public void unregisterUser() {
        if (getActivity() == null) {
            return;
        }
        if (tourGuide != null) {
            tourGuide.cleanUp();
        }
        swipe_container.setRefreshing(false);
        bg_block.setVisibility(View.VISIBLE);
        bg_block_img.setImageResource(R.mipmap.icn_default_forbid);
        txt_content.setText(TheLApp.getContext().getString(R.string.deleted_her_account));
        lin_more.setVisibility(View.GONE);
        txt_block_her.setVisibility(View.VISIBLE);
        txt_block_her.setText(TheLApp.getContext().getString(R.string.info_ok));
        txt_block_her.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
        if (UserInfoActivity.FROM_TYPE_FOLLOW.equals(from_type)) {
            show_cancel.setVisibility(View.VISIBLE);
            show_cancel.setText(TheLApp.getContext().getString(R.string.unfollow));
            show_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FollowStatusChangedImpl.followUserWithNoDialog(userId, FollowStatusChangedListener.ACTION_TYPE_CANCEL_FOLLOW, "", "");
                    getActivity().finish();
                }
            });
        }
    }

    @Override
    public void isMyBlack() {
        bg_block.setVisibility(View.VISIBLE);
        bg_block_img.setImageResource(R.mipmap.icn_my_black);
        txt_content.setText(TheLApp.getContext().getString(R.string.chat_activity_block_her));
        lin_more.setVisibility(View.GONE);
        txt_block_ok.setVisibility(View.VISIBLE);
        txt_block_her.setVisibility(View.INVISIBLE);
        txt_block_ok.setOnClickListener(new View.OnClickListener() {//屏蔽的时候关闭页面
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        final int totalRange = appBarLayout.getTotalScrollRange();
        if (swipe_container != null) {
            swipe_container.setEnabled(verticalOffset >= 0);
        }
        if (!lin_floating.isNeverShow()) {
            if (verticalOffset > currentOffset && lin_floating.getVisibility() != View.VISIBLE) {
                MyUserInfoFABBehavior.animateIn(lin_floating);
            } else if (verticalOffset < currentOffset && lin_floating.getVisibility() == View.VISIBLE && !lin_floating.mIsAnimatingOut) {
                MyUserInfoFABBehavior.animateOut(lin_floating);
            }
            currentOffset = verticalOffset;
        }
        final float start = (0 - totalRange) * 0.7f;
        final float end = (0 - totalRange) * 0.9f;
        if (verticalOffset < start && verticalOffset > end) {
            final float alp = (verticalOffset - end) / (totalRange * 0.2f);
            userInfoTopMainView.setInfoViewAlpha(alp);
            txt_title.setAlpha(1 - alp);
            txt_title.setText("");

            top_gradient.setAlpha(alp);
        } else if (verticalOffset >= start) {
            userInfoTopMainView.setInfoViewAlpha(1);
            txt_title.setAlpha(0);
            txt_title.setText("");

            top_gradient.setAlpha(1);
        } else if (verticalOffset <= end) {

            if (userInfoBean != null && userInfoBean.nickName != null) {
                txt_title.setText(userInfoBean.nickName);
            }
            userInfoTopMainView.setInfoViewAlpha(0);
            txt_title.setAlpha(1);
            top_gradient.setAlpha(0);
        }
    }

    private void setListener() {
        appbarlayout.addOnOffsetChangedListener(this);
        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //  refreshType = REFRESH_TYPE_ALL;
                //requestBussiness.getUserInfo(new DefaultNetworkHelper(UserInfoActivity.this), userId);
                isRefreshing = true;
                presenter.getUserInfoData(userId);
                final Fragment fragment = adapter.getItem(viewpager.getCurrentItem());
                if (fragment instanceof AutoRefreshImp) {
                    ((AutoRefreshImp) fragment).tryRefreshData();
                }
            }
        });
        //返回
        lin_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                preventClick(v);
                if (UserInfoActivity.FROM_TYPE_FOLLOW.equals(from_type)) {//只有来之关注页的才显示取消关注按钮
                    Intent intent = new Intent();
                    int followValue = userInfoTopMainView.refreshFollowValue();
                    intent.putExtra("followState", followValue);
                    intent.putExtra("userId", userId);
                    intent.putExtras(intent);
                    getActivity().setResult(RESULT_OK, intent);
                }
                PushUtils.finish(getActivity());
            }
        });
        //更多
        lin_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                L.i("refresh", "lin_more click");
                preventClick(v);
                UserInfoUtils.showMoreDialog(getActivity(), presenter, userInfoBean);

            }

        });
        //发送名片
        img_send_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                preventClick(v);
                if (UserUtils.isVerifyCell()) {
                    UserInfoUtils.sendCard(getActivity(), userInfoBean, userId, userInfoTopMainView.getValue(UserInfoTopMainView.ValueName.TAGE), userInfoTopMainView.getValue(UserInfoTopMainView.ValueName.TAFFECTION), userInfoTopMainView.getValue(UserInfoTopMainView.ValueName.THOROSCOPE));
                }
            }
        });
        //写日志
        lin_write_moment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                preventClick(v);
                if (UserUtils.isVerifyCell()) {
                    UserInfoUtils.showReleaseMomentDialog(getActivity());
                }
            }
        });
        //聊天
        fab_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                preventClick(v);
                if (UserUtils.isVerifyCell()) {
                    gotoChatActivity();
                }
            }
        });
        //挤眼
        fab_wink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tryToWink();
            }
        });
        /**
         * 详细资料（右侧）
         */
        userInfoTopMainView.setLinProfileClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                preventClick(v);
                drawlayout.openDrawer(Gravity.RIGHT);
            }
        });
        /**
         * 回到顶部
         */
        lin_title.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (MotionEvent.ACTION_DOWN == event.getAction()) {
                    if (System.currentTimeMillis() - lastClickTime < 300) {
                        ViewUtils.preventViewMultipleTouch(v, 2000);
                        gotoTop();
                    }
                    lastClickTime = System.currentTimeMillis();
                }
                return true;
            }
        });
        userinfoRightView.setCancelListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawlayout.closeDrawer(Gravity.RIGHT);
            }
        });
        userinfoRightView.setReviseInfoListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), UpdateUserInfoActivity.class);
                startActivityForResult(intent, TheLConstants.BUNDLE_CODE_UPDATE_USER_INFO_ACTIVITY);
                drawlayout.closeDrawer(Gravity.RIGHT);
            }
        });

        userInfoTopMainView.setReviseInfoListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), UpdateUserInfoActivity.class);
                startActivityForResult(intent, TheLConstants.BUNDLE_CODE_UPDATE_USER_INFO_ACTIVITY);
            }
        });
        bg_block.setOnClickListener(null);
        viewpager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                setCurrentTab(position);
            }
        });
        tabview_moment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewpager.setCurrentItem(0);
            }
        });
        tabview_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewpager.setCurrentItem(1);
            }
        });

        time_machine_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FlutterRouterConfig.Companion.gotoTimeMachine();
//                startActivity(new Intent(getActivity(), TimeMachineActivity.class));
            }
        });
        iv_look_me.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                anonymous_browsing_tv.setVisibility(View.VISIBLE);

                int guideWidth = anonymous_browsing_tv.getWidth();

                int[] screenArray = new int[2];

                txt_title.getLocationInWindow(screenArray);

                int marginTop = screenArray[1] + SizeUtils.dip2px(TheLApp.context, 25);

                int marginLeft = (screenArray[0] - guideWidth / 2 - SizeUtils.dip2px(TheLApp.context, 16));

                anonymous_browsing_tv.setY(marginTop);

                anonymous_browsing_tv.setX(marginLeft);

                startAnimator(marginTop, marginLeft, anonymous_browsing_tv);

            }
        });
    }

    private ValueAnimator valueAnimator;


    private void startAnimator(int startY, final int x, final View view) {
        final ValueAnimator valueAnimator = ValueAnimator.ofInt(startY, startY + SizeUtils.dip2px(TheLApp.context, 10), startY);
        valueAnimator.setDuration(1000);
        valueAnimator.setRepeatCount(5);
        valueAnimator.setRepeatMode(ValueAnimator.RESTART);
        valueAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                if (view != null) {
                    view.setLayerType(View.LAYER_TYPE_HARDWARE, null);
                }

                L.d(TAG, " onAnimationStart : ");

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (view != null) {
                    view.setLayerType(View.LAYER_TYPE_NONE, null);
                    view.setVisibility(View.INVISIBLE);
                }

                valueAnimator.cancel();

                L.d(TAG, " onAnimationEnd : ");

            }

            @Override
            public void onAnimationCancel(Animator animation) {

                L.d(TAG, " onAnimationCancel : ");

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

                L.d(TAG, " onAnimationRepeat : ");

            }
        });
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int offset = (int) animation.getAnimatedValue();
                view.setY(offset);
                view.setX(x);

            }
        });
        valueAnimator.setInterpolator(new LinearInterpolator());
        valueAnimator.start();
    }

    private void stopAnimator() {
        if (valueAnimator != null) {
            valueAnimator.cancel();
            valueAnimator = null;
        }
    }

    /**
     * 跳到顶部
     */
    private void gotoTop() {
    }

    /**
     * 尝试挤眼
     */
    private void tryToWink() {
        clearTourGuide();
        if (userInfoBean.winked.free != 0) {
            DialogUtil.showToastShort(getActivity(), getString(R.string.wink_tips, userInfoBean.nickName));
            fab_wink.setAlpha(0.5f);
        } else {
            sendWink(WinkUtils.WINK_TYPE_WINK);
        }
    }

    /**
     * 发送挤眼
     *
     * @param type
     */
    private void sendWink(String type) {
        ViewUtils.preventViewMultipleClick(fab_wink, 1000);

        WinkUtils.sendWink(userId, type, userInfoBean.nickName, userInfoBean.avatar);

        WinkUtils.playWinkSound();

    }

    /**
     * 挤眼成功
     *
     * @param type 5种基本类型
     */
    public void showWinkSuccess(String type) {
        if (userInfoBean != null) {
            userInfoBean.winked.free = 1;
            int num = 1;
            final String winkType = WinkUtils.WINK_TYPE.get(type);
            if (winkGiftListBean != null) {
                final int length = winkGiftListBean.list.size();
                for (int i = 0; i < length; i++) {
                    WinkGiftBean bean = winkGiftListBean.list.get(i);
                    if (winkType.equals(bean.id + "")) {
                        num = bean.popularity;
                        winkGiftListBean.gold = winkGiftListBean.gold - bean.price;
                        break;
                    }
                }
            }
            // 更新挤眼数
            setWinkNums(Integer.parseInt(userInfoBean.winkNum) + num + "");
            if (getUserVisibleHint()) {//如果当前对用户可见，说明是在当前页面，要播放动画显示toast
                UserInfoUtils.playWinkAnim(type, img_sendwink, fab_wink);
                //显示增加人气toast
                UserInfoUtils.showAddWinksToast(num, WinkUtils.WINK_TYPE_WINK.equals(type));
            }
        }
    }

    /**
     * 更新挤眼数
     *
     * @param winkNumStr
     */
    private void setWinkNums(final String winkNumStr) {
        userInfoTopMainView.setWinkNums(winkNumStr);
    }

    /**
     * 跳转到聊天界面
     */
    private void gotoChatActivity() {
        Intent intent = new Intent(getActivity(), ChatActivity.class);
        intent.putExtra("toUserId", userId);
        intent.putExtra("toName", userInfoBean.nickName);
        intent.putExtra("toAvatar", userInfoBean.avatar);
        intent.putExtra("fromPage", "UserInfoActivity");
        startActivity(intent);
    }

    private boolean isBlack() {

        L.d("UserInfoBlack", " isBlack userId : " + userId);

        if (BlackUtils.isBlack(userId)) {// 本地查询是否屏蔽了对方
            isMyBlack();
            return true;
        }
        return false;
    }

    private void preventClick(View v) {
        ViewUtils.preventViewMultipleClick(v, 1000);
    }

    private void showChatGuide() {

        try {

            int isShowChatGuide = ShareFileUtils.getInt(ShareFileUtils.IS_SHOW_USER_INFO_CHAT_GUIDE, -1);

            if (isShowChatGuide == -1) {

                if (!isShowChatGuide()) {
                    ShareFileUtils.setInt(ShareFileUtils.IS_SHOW_USER_INFO_CHAT_GUIDE, 0);
                    isShowChatGuide = 0;
                } else {
                    ShareFileUtils.setInt(ShareFileUtils.IS_SHOW_USER_INFO_CHAT_GUIDE, 1);
                }
            }

            if (isShowChatGuide == 0 || (userInfoBean != null && userInfoBean.followStatus == 3)) {
                return;
            }

            if (userId.equals(UserUtils.getMyUserId())) {
                return;
            }

            String chatGuideStr = ShareFileUtils.getString(ShareFileUtils.USER_INFO_CHAT_GUIDE, "[]");

            List<UserInfoChatGuideBean> userInfoGuideBeans = GsonUtils.getGson().fromJson(chatGuideStr, new TypeToken<List<UserInfoChatGuideBean>>() {
            }.getType());

            UserInfoChatGuideBean userInfoGuideBean = getChatGuide(userInfoGuideBeans);

            if (userInfoGuideBean.openCount == 3 && userInfoGuideBeans.size() < 3) {

                mChatGuideView.show(fab_chat, TheLApp.context.getResources().getString(R.string.talk_with_her));
                GrowingIOUtil.postVisitToChat(userId);
            }

            ShareFileUtils.setString(ShareFileUtils.USER_INFO_CHAT_GUIDE, GsonUtils.createJsonString(userInfoGuideBeans));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean isShowChatGuide() {
        String userId = UserUtils.getMyUserId();

        L.d(TAG, " userId : " + userId);

        if (userId != null) {
            return userId.endsWith("0") || userId.endsWith("2") || userId.endsWith("3") || userId.endsWith("4") || userId.endsWith("5");
        }
        return false;
    }

    private UserInfoChatGuideBean getChatGuide(List<UserInfoChatGuideBean> userInfoGuideBeans) {
        for (UserInfoChatGuideBean userInfoGuideBean : userInfoGuideBeans) {
            if (userInfoGuideBean.userId.equals(userId)) {
                userInfoGuideBean.openCount++;
                return userInfoGuideBean;
            }
        }
        UserInfoChatGuideBean userInfoGuideBean = new UserInfoChatGuideBean();
        userInfoGuideBean.userId = userId;
        userInfoGuideBean.openCount = 1;
        userInfoGuideBeans.add(userInfoGuideBean);
        return userInfoGuideBean;
    }


    @Override
    public boolean onSuperBackPressed() {
        if (drawlayout.isDrawerVisible(Gravity.RIGHT)) {
            drawlayout.closeDrawer(Gravity.RIGHT);
            return false;
        }
        return true;
    }


    /**
     * 礼物挤眼广播回调
     */
    class GiftWinkSuccessReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            final Bundle bundle = intent.getExtras();
            final String userId = bundle.getString(TheLConstants.BUNDLE_KEY_USER_ID);
            final String type = bundle.getString(TheLConstants.BUNDLE_KEY_ACTION_TYPE);
            if (UserInfoFragment.this.userId.equals(userId)) {//说明是对这个人的礼物几眼
                showWinkSuccess(type);
            }
        }
    }

    /**
     * 普通挤眼成功回调
     *
     * @param userId
     * @param type
     */
    @Override
    public void onWinkSucceess(String userId, String type) {
        super.onWinkSucceess(userId, type);
        if (userInfoBean == null || userInfoBean.winked == null || TextUtils.isEmpty(userId)) {
            return;
        }
        if (this.userId.equals(userId)) {//说明是对这个人的挤眼
            userInfoBean.winked.free = 1;
            showWinkSuccess(type);
            setWinkVisible();
        }
    }

    @Override
    public void onLikeStatusChanged(String momentId, boolean like, String myUserId, WinkCommentBean myWinkUserBean) {

    }

    @Override
    public void onBlackUsersChanged(String userId, boolean isAdd) {
        super.onBlackUsersChanged(userId, isAdd);
        closeLoading();
        if (!isErrorBlack) {//如果不是被屏蔽的时候点的屏蔽对方
            isBlack();
        }
    }

    @Override
    public void onFollowStatusChanged(int followStatus, String userId, String nickName, String avatar) {
        super.onFollowStatusChanged(followStatus, userId, nickName, avatar);
        if (this.userId.equals(userId)) {
            userInfoTopMainView.followStatusChanged(followStatus, userId, nickName, avatar);
        }
    }

    class MomentReleaseReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (presenter != null && !TextUtils.isEmpty(userId) && !isRefreshing) {
                presenter.getUserInfoData(userId);
            }
        }
    }

    class StickMomentReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                String action = intent.getAction();
                if (action != null && action.equals(TheLConstants.BROADCAST_STICK_MOMENT)) {
                    String momentsId = intent.getStringExtra("momentsId");
                    int stick_status = intent.getIntExtra("stick_status", -1);

                    L.d(TAG, " momentsId : " + momentsId);

                    L.d(TAG, " stick_status : " + stick_status);

                    if (mUserInfoMomentFragment != null) {
                        mUserInfoMomentFragment.stickStatusChange(momentsId, stick_status);
                    }

                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (userInfoTopMainView != null)
            userInfoTopMainView.onActivityResult(requestCode, resultCode, data);
        if (requestCode == TheLConstants.BUNDLE_CODE_UPDATE_USER_INFO_ACTIVITY && resultCode == RESULT_OK) {
            getData();
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if (isMySelf && ShareFileUtils.getBoolean(ShareFileUtils.TIME_MACHANE_NOT_READ, false)) {

            ShareFileUtils.setBoolean(ShareFileUtils.TIME_MACHANE_NOT_READ, false);

            mGuideView.show(time_machine_iv, null);
        }
    }
}
