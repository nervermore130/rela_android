package com.thel.modules.qrcode;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PointF;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dlazaro66.qrcodereaderview.QRCodeReaderView;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.DecodeHintType;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.RGBLuminanceSource;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.QRCodeReader;
import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.constants.BundleConstants;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.modules.main.home.moments.web.WebViewActivity;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.modules.select_image.SelectLocalImagesActivity;
import com.thel.utils.ImageUtils;
import com.thel.utils.QRUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Hashtable;

public class QRActivity extends BaseActivity implements OnClickListener, QRCodeReaderView.OnQRCodeReadListener {

    private LinearLayout lin_avatar;
    private ImageView img_qr;
    private ImageView img_avatar;
    private ImageView img_switch_scan;
    private SimpleDraweeView img_switch_avatar;
    private TextView txt_tip;
    private QRCodeReaderView qrCodeReaderView;
    private LinearLayout switch_container;
    private TextView btn_photo;
    private RelativeLayout rl_img_qr;

    private Bitmap bitmapQr;

    public static final String URL = "http://" + TheLConstants.URL_TITLE + "/mobile/share/user.html";

    private String event_id_qr_scan_count = "qr_scan_count";

    private boolean scanEnable = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.qr_activity);

        findViewById();
        setListener();

        bitmapQr = QRUtils.createQRImage(URL + "?userId=" + ShareFileUtils.getString(ShareFileUtils.ID, ""), Utils.dip2px(this, 290), Utils.dip2px(this, 290));
        img_qr.setImageBitmap(bitmapQr);
        img_avatar.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(ShareFileUtils.getString(ShareFileUtils.AVATAR, ""), TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE)));
        img_switch_avatar.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(ShareFileUtils.getString(ShareFileUtils.AVATAR, ""), TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE)));

        switchUI();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(this.getClass().getName());
        MobclickAgent.onResume(this);
        scanEnable = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            qrCodeReaderView.stopCamera();
        } catch (Exception e) {
            e.printStackTrace();
        }
        MobclickAgent.onPageEnd(this.getClass().getName());
        MobclickAgent.onPause(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (bitmapQr != null && !bitmapQr.isRecycled()) {
            bitmapQr.recycle();
        }
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == TheLConstants.RESULT_CODE_SELECT_LOCAL_IMAGE) {// 选择了图片
            String photoPath = data.getStringExtra(TheLConstants.BUNDLE_KEY_LOCAL_IMAGE_PATH);
            Log.d("onActivityResult", "photoPath :" + photoPath);
            if (!TextUtils.isEmpty(photoPath)) {
                jumpToUserInfoPage(scanLocalQRCode(photoPath));
            } else {
                Toast.makeText(this, getString(R.string.info_rechoise_photo), Toast.LENGTH_SHORT).show();
            }

        }
    }

    private String scanLocalQRCode(String path) {
        String formart = null;
        Bitmap mBitmap = BitmapFactory.decodeFile(path);
        Result result = null;
        if (mBitmap != null) {

            Hashtable<DecodeHintType, String> hints = new Hashtable<DecodeHintType, String>();
            hints.put(DecodeHintType.CHARACTER_SET, "utf-8"); // 设置二维码内容的编码
            Bitmap scanBitmap = Bitmap.createBitmap(mBitmap);

            int[] px = new int[scanBitmap.getWidth() * scanBitmap.getHeight()];
            scanBitmap.getPixels(px, 0, scanBitmap.getWidth(), 0, 0,
                    scanBitmap.getWidth(), scanBitmap.getHeight());
            RGBLuminanceSource source = new RGBLuminanceSource(
                    scanBitmap.getWidth(), scanBitmap.getHeight(), px);
            BinaryBitmap tempBitmap = new BinaryBitmap(new HybridBinarizer(source));
            QRCodeReader reader = new QRCodeReader();
            try {
                result = reader.decode(tempBitmap, hints);
            } catch (NotFoundException | ChecksumException | FormatException e) {
                e.printStackTrace();
                return null;
            }
        }

        if (result != null) {
            try {
                boolean ISO = Charset.forName("ISO-8859-1").newEncoder()
                        .canEncode(result.toString());
                if (ISO) {
                    formart = new String(result.toString().getBytes(StandardCharsets.ISO_8859_1), "GB2312");
                } else {
                    formart = result.toString();
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return null;
            }
        }

        if (!TextUtils.isEmpty(formart)) {
            return formart;
        }
        return null;
    }

    private void setListener() {
        findViewById(R.id.lin_back).setOnClickListener(this);
        qrCodeReaderView.setOnQRCodeReadListener(this);
        try {
            qrCodeReaderView.startCamera();
        } catch (Exception e) {
            e.printStackTrace();
        }
        switch_container.setOnClickListener(this);
        btn_photo.setOnClickListener(this);

    }

    private void findViewById() {
        lin_avatar = findViewById(R.id.lin_avatar);
        img_qr = findViewById(R.id.img_qr);
        img_avatar = findViewById(R.id.img_avatar);
        img_switch_scan = findViewById(R.id.img_switch_scan);
        txt_tip = findViewById(R.id.txt_tip);
        img_switch_avatar = findViewById(R.id.img_switch_avatar);
        qrCodeReaderView = findViewById(R.id.qrdecoderview);
        switch_container = findViewById(R.id.switch_container);
        btn_photo = findViewById(R.id.btn_photo);
        rl_img_qr = findViewById(R.id.rl_img_qr);
    }

    @Override
    public void onClick(View v) {
        ViewUtils.preventViewMultipleClick(v, 1000);
        int viewId = v.getId();
        switch (viewId) {
            case R.id.img_share:
                break;
            case R.id.lin_back:
                finish();
                break;
            case R.id.switch_container:
                switchUI();
                break;
            case R.id.btn_photo:
                Intent intent = new Intent(QRActivity.this, SelectLocalImagesActivity.class);
                intent.putExtra(TheLConstants.BUNDLE_KEY_SELECT_AMOUNT, 1);
                startActivityForResult(intent, TheLConstants.BUNDLE_CODE_WRITE_MOMENT_ACTIVITY);
                break;
            default:
                break;
        }

    }

    private int scanType = 1;

    private void switchUI() {
        if (scanType == 0) {// 显示二维码
//            qrCodeReaderView.setVisibility(View.GONE);
            rl_img_qr.setVisibility(View.VISIBLE);
            lin_avatar.setVisibility(View.VISIBLE);
            img_switch_avatar.setVisibility(View.GONE);
            img_switch_scan.setVisibility(View.VISIBLE);
            txt_tip.setText(getString(R.string.qr_activity_tip_scan));
            scanType = 1;
        } else {// 扫描二维码
            rl_img_qr.setVisibility(View.INVISIBLE);
            lin_avatar.setVisibility(View.INVISIBLE);
            img_switch_avatar.setVisibility(View.VISIBLE);
            img_switch_scan.setVisibility(View.GONE);
            txt_tip.setText(getString(R.string.qr_activity_tip_show));
            scanType = 0;
        }
    }

    @Override
    public void onQRCodeRead(String text, PointF[] points) {
        if (scanEnable && scanType == 0) {
            jumpToUserInfoPage(text);
        }
    }

    private void jumpToUserInfoPage(String text) {
        if (!TextUtils.isEmpty(text)) {
            scanEnable = false;
            Uri uri = Uri.parse(text);
            if (TheLConstants.URL_TITLE.equals(uri.getHost()) && "/mobile/share/user.html".equals(uri.getPath())) {
                String userId = uri.getQueryParameter("userId");
                if (!TextUtils.isEmpty(userId)) {
                    MobclickAgent.onEvent(this, event_id_qr_scan_count);
//                    Intent intent = new Intent();
//                    intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, userId);
//                    intent.setClass(QRActivity.this, UserInfoActivity.class);
//                    startActivity(intent);
                    FlutterRouterConfig.Companion.gotoUserInfo(userId);
                }
            } else {
                Intent intent = new Intent(this, WebViewActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(BundleConstants.URL, text);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        }
    }
}
