package com.thel.modules.main.messages.db;

import android.content.Intent;
import android.text.TextUtils;

import com.thel.app.TheLApp;
import com.thel.constants.TheLConstants;
import com.thel.db.DBUtils;
import com.thel.manager.ChatServiceManager;
import com.thel.modules.main.messages.bean.MsgBean;
import com.thel.modules.main.messages.bean.MsgListBean;
import com.thel.modules.main.messages.bean.MsgWithUnreadBean;

/**
 * Created by kevin on 2017/9/30.
 */

public class MessageDataBaseDelegate {

    private static MessageDataBaseDelegate instance = null;

    public static MessageDataBaseDelegate getInstance() {
        if (instance == null) {
            instance = new MessageDataBaseDelegate();
        }
        return instance;
    }

    private void judgeStrangerEmpty(MsgListBean msgListBean, String userId) {
        if (msgListBean == null) {
            return;
        }
        final int size = msgListBean.msgWithUnreadBeans.size();
        final int count = MessageDataBaseAdapter.getInstance(TheLApp.getContext(), userId).getStrangTableCount();
        final int unReadCount = MessageDataBaseAdapter.getInstance(TheLApp.getContext(), userId).getStrangerUnReadMsgCount();
        boolean haveStrangerWink = MessageDataBaseAdapter.getInstance(TheLApp.getContext(), userId).haveStrangerWink();

        for (int i = 0; i < size; i++) {
            MsgWithUnreadBean msgWithUnreadBean = msgListBean.msgWithUnreadBeans.get(i);
            if (MessageDataBaseAdapter.STRANGER_MSG_USER_ID.equals(msgWithUnreadBean.msgBean.userId)) {//如果是陌生人消息
                if (count <= 0) {
                    msgWithUnreadBean.msgBean.msgType = MsgBean.MSG_TYPE_EMPTY;//如果列表为空，设置消息类型为empty
                }
                msgWithUnreadBean.unReadCount = unReadCount;
                /**如果陌生人里面有可以回挤的人，则显示挤眼图标**/
                msgWithUnreadBean.isWinked = haveStrangerWink ? MsgWithUnreadBean.WINK_YES : MsgWithUnreadBean.WINK_NO;
            }
        }

    }

    /**
     * 如果是这些是在当前的聊天列表消息，要刷新聊天列表
     *
     * @param msgBean
     */
    public static void sendRefreshUiBroadcast(MsgBean msgBean) {
        final Intent intent = new Intent();
        intent.setAction(TheLConstants.BROADCAST_CHAT_ACTIVITY_REFRESH_UI);
        intent.putExtra(TheLConstants.BUNDLE_KEY_MSG_BEAN, msgBean);
        TheLApp.getContext().sendBroadcast(intent);
    }
}
