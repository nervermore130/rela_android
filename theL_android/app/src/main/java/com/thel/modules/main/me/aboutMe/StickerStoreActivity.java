package com.thel.modules.main.me.aboutMe;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.rela.pay.OnPayStatusListener;
import com.rela.pay.PayConstants;
import com.rela.pay.PayProxyImpl;
import com.thel.BuildConfig;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.IntentFilterActivity;
import com.thel.bean.AdBean;
import com.thel.bean.PayOrderBean;
import com.thel.bean.SimpleStikerBean;
import com.thel.bean.StickerPackListBean;
import com.thel.bean.StickerPackListNetBean;
import com.thel.bean.StickerPackNetBean;
import com.thel.bean.recommend.RecommendedStickerListBean;
import com.thel.bean.recommend.RecommendedStickerListNetBean;
import com.thel.constants.TheLConstants;
import com.thel.growingio.GIoGiftTrackBean;
import com.thel.growingio.GrowingIoConstant;
import com.thel.imp.sticker.StickerUtils;
import com.thel.imp.vip.VipStatusChangerListener;
import com.thel.modules.main.me.bean.LogInfoBean;
import com.thel.modules.main.me.match.eventcollect.collect.LiveLogUtils;
import com.thel.modules.main.me.match.eventcollect.collect.MatchLogUtils;
import com.thel.modules.main.me.match.eventcollect.collect.PayLogUtils;
import com.thel.modules.main.messages.utils.PushUtils;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.RequestConstants;
import com.thel.ui.widget.BannerLayout;
import com.thel.utils.DialogUtil;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.GsonUtils;
import com.thel.utils.ImageUtils;
import com.thel.utils.L;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;
import com.umeng.analytics.MobclickAgent;

import org.reactivestreams.Subscription;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * 表情商店
 * Created by lingwei on 2017/10/27.
 */

public class StickerStoreActivity extends IntentFilterActivity implements VipStatusChangerListener {
    public final String TAG = StickerStoreActivity.class.getSimpleName();
    public final String FILENAME = "stickers";
    private LinearLayout lin_reload;
    private ImageView img_reload;
    private TextView txt_reload;
    private ListView listView;
    private LinearLayout lin_back;
    private StickerStoreListAdapter adapter;
    private ArrayList<SimpleStikerBean> listPlus = new ArrayList<SimpleStikerBean>();
    private LinearLayout lin_header;
    // 广告控件
    private BannerLayout banner;
    /**
     * 下一页数据的游标
     */
    private int cursor = 0;
    /**
     * 单页最大的数据量
     */
    private int limit = 30;
    private boolean hasLoadRecommendedData = false;
    private boolean hasLoadMoreData = false;
    private long buyingStickerPackId = -1;
    private DialogUtil dialogUtils;
    private int vipLevel;
    private String pageId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sticker_store_activity);
        findViewById();
        processBusiness();
        setListener();
        pageId = Utils.getPageId();

    }

    private void setListener() {
        lin_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PushUtils.finish(StickerStoreActivity.this);
            }
        });
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                switch (scrollState) {
                    // 当不滚动时
                    case AbsListView.OnScrollListener.SCROLL_STATE_IDLE:
                        int totalSize = view.getCount();
                        // 判断滚动到底部
                        if (view.getLastVisiblePosition() + 1 == totalSize && cursor != 0) {
                            showLoading();
                            // 列表滑动到底部加载下一组数据
                            loadStickerStoreMore(cursor, limit);
                        }
                        break;
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            }
        });
        img_reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Animation anim = AnimationUtils.loadAnimation(TheLApp.getContext(), R.anim.rotate);
                LinearInterpolator lir = new LinearInterpolator();
                anim.setInterpolator(lir);
                view.setLayerType(View.LAYER_TYPE_HARDWARE, null);
                view.startAnimation(anim);
                txt_reload.setText(getString(R.string.info_reloading));
                processBusiness();
            }
        });
        lin_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(StickerStoreActivity.this, MyStickersActivity.class), 1);
            }
        });
        setAdapterListener();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                jumpToPackDetail(listPlus.get(position - listView.getHeaderViewsCount()).id,listPlus.get(position - listView.getHeaderViewsCount()).vipFree==1);

            }
        });

    }

    private void jumpToPackDetail(long id,boolean isFree) {
        Intent intent = new Intent(this, StickerPackDetailActivity.class);
        intent.putExtra("id", id);
        startActivityForResult(intent, 2);

        if (isFree){
            traceWanderLog("premium");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case 1:
                    ArrayList<Integer> removedPackIds = data.getIntegerArrayListExtra("ids");
                    refreshDownloadedStickerPacks(removedPackIds);
                    break;
                case 2:
                    if (data != null) {
                        long id = data.getLongExtra("id", 0);
                        String type = data.getStringExtra("type");
                        if ("download".equals(type)) {
                            refreshStickerPackStatus(id, 2);
                        } else if ("buy".equals(type)) {
                            refreshStickerPackStatus(id, 1);
                        }
                    }
                    break;
            }
        }
    }

    private void refreshDownloadedStickerPacks(ArrayList<Integer> removedPackIds) {
        try {
            for (Integer id : removedPackIds) {
                for (int i = 0; i < listPlus.size(); i++) {
                    if (listPlus.get(i).id == id) {                        //已购买未下载的表情包状态置为1
                        listPlus.get(i).status = 1;
                        adapter.updateSingleRow(listView, i + listView.getHeaderViewsCount());
                    }
                }
            }
        } catch (Exception e) {

        }
    }

    private void goTOBuy(View view, final int pos, final SimpleStikerBean bean) {
        if (bean.status == 1) {
            view.setVisibility(View.GONE);
            ((RelativeLayout) (view.getParent())).findViewById(R.id.progress_bar).setVisibility(View.VISIBLE);
            loadStickerPackDetail(bean.id, 1, pos);
        } else if (bean.status == 0) {// 未购买
            if (bean.vipFree == 1) {//如果是会员免费表情
                if (UserUtils.getUserVipLevel() > 0) {//如果是会员
                    view.setVisibility(View.GONE);
                    ((RelativeLayout) (view.getParent())).findViewById(R.id.progress_bar).setVisibility(View.VISIBLE);
                    loadStickerPackDetail(bean.id, 1, pos);
                } else {//如果不是会员
                    MobclickAgent.onEvent(TheLApp.getContext(), "check_vip_free_sticker");
                    gotoBuyVip();
                }
            } else if ("0".equals(String.valueOf(bean.price))) {// 免费表情
                view.setVisibility(View.GONE);
                ((RelativeLayout) (view.getParent())).findViewById(R.id.progress_bar).setVisibility(View.VISIBLE);
                loadStickerPackDetail(bean.id, 1, pos);

            } else {
                buyingStickerPackId = bean.id;
                if (RequestConstants.APPLICATION_ID_LOCAL.equals(BuildConfig.APPLICATION_ID))
                    dialogUtils.showSelectionDialogWithIcon(this, getString(R.string.pay_options), new String[]{getString(R.string.pay_alipay), getString(R.string.pay_wx)}, new int[]{R.mipmap.icon_zhifubao, R.mipmap.icon_wechat}, new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            dialogUtils.closeDialog();
                            switch (position) {
                                case 0:// 支付宝
                                    view.setVisibility(View.GONE);
                                    ((RelativeLayout) (view.getParent())).findViewById(R.id.progress_bar).setVisibility(View.VISIBLE);
                                    showLoadingNoBack();
//                                    loadCreateStickerPackOrder(TheLConstants.PRODUCT_TYPE_STICKER_PACK, bean.id + "", AlipayUtils.ALIPAY,AlipayUtils.ALIPAY_SOURCE);
                                    RequestBusiness.getInstance().createStickerPackOrder(TheLConstants.PRODUCT_TYPE_STICKER_PACK, bean.id + "", PayConstants.PAY_TYPE_ALIPAY, PayConstants.ALIPAY_SOURCE).observeOn(AndroidSchedulers.mainThread()).onBackpressureDrop().subscribeOn(Schedulers.io()).subscribe(new InterceptorSubscribe<PayOrderBean>() {
                                        @Override
                                        public void onNext(PayOrderBean data) {
                                            super.onNext(data);

                                            PayProxyImpl.getInstance().pay(PayConstants.PAY_TYPE_ALIPAY, StickerStoreActivity.this, GsonUtils.createJsonString(data.data), new OnPayStatusListener() {
                                                @Override
                                                public void onPayStatus(int payStatus, String errorInfo) {

                                                    MobclickAgent.onEvent(StickerStoreActivity.this, "create_order_succeed");

                                                    // 判断resultStatus 为非“9000”则代表可能支付失败
                                                    // “8000”代表支付结果因为支付渠道原因或者系统原因还在等待支付结果确认，最终交易是否成功以服务端异步通知为准（小概率状态）
                                                    if (payStatus == 1) {
                                                        MobclickAgent.onEvent(StickerStoreActivity.this, "sticker_pay_succeed");// 购买表情包支付成功事件统计
                                                        downLoadStickerPack();
                                                    } else {
                                                        // 其他值就可以判断为支付失败，包括用户主动取消支付，或者系统返回的错误
                                                        if (TextUtils.equals(errorInfo, "6001")) {
                                                            DialogUtil.showToastShort(TheLApp.getContext(), getString(R.string.pay_canceled));
                                                        } else {
                                                            DialogUtil.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.pay_failed));
                                                        }
                                                        refreshStickerPackStatus(buyingStickerPackId, 0);
                                                    }
                                                }

                                            });

                                        }
                                    });
                                    break;
                                case 1: // 微信支付
                                    view.setVisibility(View.GONE);
                                    ((RelativeLayout) (view.getParent())).findViewById(R.id.progress_bar).setVisibility(View.VISIBLE);
                                    showLoadingNoBack();
//                                    loadCreateStickerPackOrder(TheLConstants.PRODUCT_TYPE_STICKER_PACK, bean.id + "", WXPayUtils.WXPAY,WXPayUtils.WXPAY_SOURCE);
                                    RequestBusiness.getInstance().createStickerPackOrder(TheLConstants.PRODUCT_TYPE_STICKER_PACK, bean.id + "", PayConstants.PAY_TYPE_WXPAY, PayConstants.WXPAY_SOURCE).observeOn(AndroidSchedulers.mainThread()).onBackpressureDrop().subscribeOn(Schedulers.io()).subscribe(new InterceptorSubscribe<PayOrderBean>() {
                                        @Override
                                        public void onNext(PayOrderBean data) {
                                            super.onNext(data);

                                            PayProxyImpl.getInstance().pay(PayConstants.PAY_TYPE_WXPAY, StickerStoreActivity.this, GsonUtils.createJsonString(data.data), new OnPayStatusListener() {
                                                @Override
                                                public void onPayStatus(int payStatus, String errorInfo) {
                                                    L.d("WxPay", " payStatus : " + payStatus);

                                                    L.d("WxPay", " errorInfo : " + errorInfo);

                                                    MobclickAgent.onEvent(StickerStoreActivity.this, "create_order_succeed");

                                                    if (payStatus == 1) {
                                                        MobclickAgent.onEvent(StickerStoreActivity.this, "sticker_pay_succeed");// 购买表情包支付成功事件统计
                                                        downLoadStickerPack();
                                                    } else {
                                                        closeLoading();
                                                        refreshStickerPackStatus(buyingStickerPackId, 0);
                                                    }
                                                }
                                            });

                                        }
                                    });
                                    break;
                                default:
                                    break;
                            }
                        }
                    }, false, null);
                else
                    DialogUtil.showAlert(this, "", getString(R.string.chat_activity_coming_soon), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            }
        }
    }

    private void processBusiness() {
        adapter = new StickerStoreListAdapter(this, listPlus);
        listView.addHeaderView(lin_header);
        listView.setAdapter(adapter);
        loadStickerStoreRecommended();
        loadStickerStoreMore(cursor, limit);
    }

    private void loadStickerStoreMore(int cursor, int limit) {
        Flowable<StickerPackListNetBean> flowable = RequestBusiness.getInstance().getStickerStoreMore(cursor, limit);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<StickerPackListNetBean>() {

            @Override
            public void onNext(StickerPackListNetBean stickerPackListNetBean) {
                super.onNext(stickerPackListNetBean);
                getStickerStoreData(stickerPackListNetBean.data);
            }

            @Override
            public void onError(Throwable t) {
                requestFinished();
            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void getStickerStoreData(StickerPackListBean stickerPackListBean) {
        if (stickerPackListBean == null || stickerPackListBean.list == null) {
            return;
        }
        filterDownloadedOrBoughtStickerPacks(stickerPackListBean.list);
        cursor = stickerPackListBean.cursor;
        if (stickerPackListBean.list.size() > 0) {
            if (!hasLoadMoreData) {
                stickerPackListBean.list.get(0).showTitle = 2;// 第一个更多表情要显示标题
                listPlus.addAll(stickerPackListBean.list);
            }
        }
        hasLoadMoreData = true;
        requestFinished();
    }

    private void loadStickerStoreRecommended() {
        Flowable<RecommendedStickerListNetBean> flowable = RequestBusiness.getInstance().getStickerStoreRecommended();
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<RecommendedStickerListNetBean>() {

            @Override
            public void onNext(RecommendedStickerListNetBean bean) {
                super.onNext(bean);
                getRecommendStickerListData(bean.data);
            }

            @Override
            public void onError(Throwable t) {
                requestFinished();

            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void getRecommendStickerListData(RecommendedStickerListBean bean) {
        if (bean == null || bean.hots == null)
            return;
        hasLoadRecommendedData = true;
        filterDownloadedOrBoughtStickerPacks(bean.hots);
        if (bean.hots.size() > 0) {
            bean.hots.get(0).showTitle = 1;// 第一个推荐表情要显示标题
            SimpleStikerBean packBean = bean.hots.get(0);
            if (packBean != null) {
                packBean.showTitle = 1;
            }
            listPlus.addAll(bean.hots);
        }
        requestFinished();
    }

    private void filterDownloadedOrBoughtStickerPacks(final List<SimpleStikerBean> stickerPackBeans) {

        List<String> Sp_StickerId = StickerUtils.getMyStickerIdList();
        for (SimpleStikerBean bean : stickerPackBeans) {
            for (String id : Sp_StickerId) {
                if (id.equals(String.valueOf(bean.id))) {// 已下载的表情包状态置为2
                    bean.status = 2;
                    break;
                }
            }
        }

    }

    private void requestFinished() {
        closeLoading();
        refreshUi();
    }

    private void refreshUi() {
        if (hasLoadRecommendedData && hasLoadMoreData) {
            lin_reload.setVisibility(View.GONE);

            adapter.notifyDataSetChanged();
        } else {
            loadFailed();
        }
    }

    private void setAdapterListener() {
        adapter.setBuyListener(new BuyListener() {
            @Override
            public void buy(View view, int position, SimpleStikerBean bean) {
                goTOBuy(view, position, bean);
            }
        });
    }

    private void findViewById() {
        dialogUtils = new DialogUtil();
        Intent intent = getIntent();
        vipLevel = intent.getIntExtra("vipLevel", 0);
        lin_reload = this.findViewById(R.id.lin_reload);
        img_reload = this.findViewById(R.id.img_reload);
        txt_reload = this.findViewById(R.id.txt_reload);
        ((TextView) findViewById(R.id.txt_title)).setText(getString(R.string.sticker_store_act_title));
        lin_back = findViewById(R.id.lin_back);
        listView = findViewById(R.id.listView);
        lin_header = (LinearLayout) LinearLayout.inflate(this, R.layout.sticker_store_header, null);
        ((TextView) lin_header.findViewById(R.id.txt_inner_page)).setText(getString(R.string.sticker_store_act_my_stickers));
        ((ImageView) lin_header.findViewById(R.id.img_logo_go_to_inner_page)).setImageResource(R.mipmap.icn_me_sticker_my);
        initBanner();

    }

    private void initBanner() {
        banner = lin_header.findViewById(R.id.banner); // 获取Gallery组件
        banner.setVisibility(View.GONE);
        banner.setShowIndicator(false);
        banner.setLayoutParams(new LinearLayout.LayoutParams(ListView.LayoutParams.MATCH_PARENT, (int) (TheLApp.getContext().getResources().getDisplayMetrics().widthPixels / TheLConstants.ad_aspect_ratio)));
        RequestBusiness.getInstance().getAd().onBackpressureDrop().observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new InterceptorSubscribe<AdBean>() {
            @Override
            public void onNext(AdBean data) {
                super.onNext(data);
                refreshAdArea(data);
            }
        });
    }

    private void refreshAdArea(AdBean adBean) {
        if (adBean == null || adBean.data == null || adBean.data.map_list == null)
            return;
        final List<AdBean.Map_list> adList = adBean.data.map_list;

        List<AdBean.Map_list> willDelete = new ArrayList<>();
        for (AdBean.Map_list bean : adList) {
            if (!bean.advertLocation.equals("sticker"))
                willDelete.add(bean);
        }
        adList.removeAll(willDelete);

        if (!adList.isEmpty()) {
            List<String> advs = new ArrayList<String>();
            for (int i = 0; i < adList.size(); i++) {
                advs.add(adList.get(i).advertURL);
            }
            banner.setVisibility(View.VISIBLE);
            banner.setViewUrls(advs);
            // 添加点击事件的监听
            banner.setOnBannerItemClickListener(new BannerLayout.OnBannerItemClickListener() {
                @Override
                public void onItemClick(int position) {
                    try {
                       /* if (adList.get(position).dumpType.equals(AdBean.ADV_DUMP_TYPE_STICKER)) { // 跳转表情详情页面
                            Intent intent = new Intent(StickerStoreActivity.this, StickerPackDetailActivity.class);
                            intent.putExtra("id", Long.valueOf(adList.get(position).dumpURL));// 表情表id
                            startActivityForResult(intent, 2);
                        } else
                            BusinessUtils.AdRedirect(adList.get(position));*/
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private void loadStickerPackDetail(long id, int download, final int position) {
        Flowable<StickerPackNetBean> flowable = RequestBusiness.getInstance().getStickerPackDetail(id, download);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<StickerPackNetBean>() {

            @Override
            public void onNext(StickerPackNetBean stickerPackBean) {
                super.onNext(stickerPackBean);
                getStickerPackBeanData(stickerPackBean, position);

            }

            @Override
            public void onError(Throwable t) {
                L.d("onerror", t.getMessage());

            }
        });
    }

    private void getStickerPackBeanData(StickerPackNetBean stickerPackBean, int position) {
        listPlus.get(position).status = 2;
        adapter.updateSingleRow(listView, position + listView.getHeaderViewsCount());

        StickerUtils.saveOneSticker(stickerPackBean.data, null);
        //  saveStickPackDetail(stickerPackBean, TheLConstants.F_DOWNLOAD_ROOTPATH, Utils.getStickerFileName() + stickerPackBean.data.id);

    }

    private void downLoadStickerPack() {
        for (int i = 0; i < listPlus.size(); i++) {
            if (listPlus.get(i).id == buyingStickerPackId) {
                loadStickerPackDetail(buyingStickerPackId, 1, i);
                break;
            }
        }
    }

    private void refreshStickerPackStatus(long id, int status) {
        for (int i = 0; i < listPlus.size(); i++) {
            if (listPlus.get(i).id == id) {
                listPlus.get(i).status = status;
                adapter.updateSingleRow(listView, i + listView.getHeaderViewsCount());
                break;
            }
        }
    }

    /**
     * 跳到购买会员页面
     */
    private void gotoBuyVip() {
        Intent intent = new Intent(this, BuyVipActivity.class);
        intent.putExtra("fromPage", "gallery");
        intent.putExtra("pageId", pageId);
        startActivityForResult(intent, TheLConstants.REQUEST_CODE_BUY_VIP);
        traceWanderLog("clickpremium");
    }

    private class StickerStoreListAdapter extends BaseAdapter {

        private ArrayList<SimpleStikerBean> stickerPacks;
        private LayoutInflater mInflater;
        private BuyListener listener;
        private Context context;

        public StickerStoreListAdapter(Context context, ArrayList<SimpleStikerBean> beans) {
            this.context = context;
            mInflater = (LayoutInflater) TheLApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            stickerPacks = beans;
        }

        @Override
        public int getCount() {
            return stickerPacks.size();
        }

        @Override
        public Object getItem(int position) {
            return stickerPacks.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public void updateSingleRow(ListView listView, int position) {
            int firstVisiblePosition = listView.getFirstVisiblePosition();
            int lastVisiblePosition = listView.getLastVisiblePosition();
            if (position >= firstVisiblePosition && position <= lastVisiblePosition) {
                View view = listView.getChildAt(position - firstVisiblePosition);
                if (view.getTag() instanceof HoldView) {
                    HoldView holdView = (HoldView) view.getTag();
                    refreshItem(position - listView.getHeaderViewsCount(), holdView);
                }
            }
        }

        private void refreshItem(final int position, HoldView holdView) {
            holdView.txt_title.setVisibility(View.VISIBLE);
            holdView.txt_downloaded.setVisibility(View.GONE);
            holdView.progress_bar.setVisibility(View.GONE);
            holdView.txt_operation.setVisibility(View.GONE);

            final SimpleStikerBean simpleBean = stickerPacks.get(position);
            if (simpleBean.showTitle == 1) {// 显示推荐表情标题
                holdView.txt_title.setText(TheLApp.getContext().getString(R.string.sticker_store_act_recommended_title));
            } else if (simpleBean.showTitle == 2) {// 显示更多表情标题
                holdView.txt_title.setText(TheLApp.getContext().getString(R.string.sticker_store_act_more_title));
            } else {
                holdView.txt_title.setVisibility(View.GONE);
            }

            if (simpleBean.isNew == 1) {
                holdView.img_type.setVisibility(View.VISIBLE);
            } else {
                holdView.img_type.setVisibility(View.GONE);
            }

            holdView.txt_name.setText(simpleBean.title);
            holdView.txt_desc.setText(simpleBean.summary);

            if (simpleBean.thumbnail != null)
                holdView.img.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(simpleBean.thumbnail, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE))).build()).setAutoPlayAnimations(true).build());

            if (simpleBean.status == 0) {
                holdView.txt_operation.setVisibility(View.VISIBLE);
                if (simpleBean.vipFree == 1) {//如果是会员免费下载
                    holdView.txt_operation.setText(getString(R.string.vip_free));
                } else {
                    holdView.txt_operation.setText(simpleBean.generatePriceShow());
                }
            } else if (simpleBean.status == 1) {
                holdView.txt_operation.setVisibility(View.VISIBLE);
                if (simpleBean.vipFree == 1) {//如果是会员免费下载
                    holdView.txt_operation.setText(getString(R.string.vip_free));
                } else {
                    holdView.txt_operation.setText(getString(R.string.sticker_store_act_download));

                }
            } else if (simpleBean.status == 2) {
                holdView.txt_downloaded.setVisibility(View.VISIBLE);
            }

            holdView.txt_operation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.buy(view, position, simpleBean);
                    }
                }
            });
        }

        public void setBuyListener(BuyListener listener) {
            this.listener = listener;
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            HoldView holdView = null;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.sticker_store_list_item, parent, false);
                holdView = new HoldView();

                holdView.img = convertView.findViewById(R.id.img);
                holdView.txt_title = convertView.findViewById(R.id.txt_title);
                holdView.txt_name = convertView.findViewById(R.id.txt_name);
                holdView.txt_downloaded = convertView.findViewById(R.id.txt_downloaded);
                holdView.txt_operation = convertView.findViewById(R.id.txt_operation);
                holdView.progress_bar = convertView.findViewById(R.id.progress_bar);
                holdView.img_type = convertView.findViewById(R.id.img_type);
                holdView.txt_desc = convertView.findViewById(R.id.txt_desc);

                convertView.setTag(holdView); // 把holdview缓存下来
            } else {
                holdView = (HoldView) convertView.getTag();
            }

            refreshItem(position, holdView);

            return convertView;
        }

        class HoldView {
            SimpleDraweeView img;
            TextView txt_title;
            TextView txt_name;
            TextView txt_desc;
            TextView txt_downloaded;
            TextView txt_operation;
            ProgressBar progress_bar;
            SimpleDraweeView img_type;
        }

    }

    interface BuyListener {
        void buy(View view, int position, SimpleStikerBean bean);
    }

    private void loadFailed() {
        closeLoading();
        try {
            lin_reload.setVisibility(View.VISIBLE);
            img_reload.clearAnimation();
            txt_reload.setText(getString(R.string.info_reload));
        } catch (Exception e) {
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * @param isVip    现在是否是会员
     * @param vipLevel 当前会员等级
     */
    @Override
    public void onVipStatusChanged(boolean isVip, int vipLevel) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        PushUtils.finish(StickerStoreActivity.this);
    }

    public void traceWanderLog(String activity) {
        try {

            LogInfoBean logInfoBean = new LogInfoBean();
            logInfoBean.page = "gallery";
            logInfoBean.page_id = pageId;
            logInfoBean.activity = activity;

            MatchLogUtils.getInstance().addLog(logInfoBean);

        } catch (Exception e) {

        }
    }
}
