package com.thel.modules.main.discover.adapter;

import android.view.View;

import com.thel.R;
import com.thel.modules.live.bean.LiveRoomBean;
import com.thel.modules.main.discover.LiveClassifyItemBaseView;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;

import java.util.List;

/**
 * Created by lingwei on 2017/11/28.
 */

public class LiveClassifyItemAdapter extends BaseRecyclerViewAdapter<LiveRoomBean> {
    public static final int TYPE_RECOMMEND = 0;
    public static final int TYPE_LIVEING = 1;
    private final int type;
    private final String pageId;
    private final int currentType;


    public LiveClassifyItemAdapter(int layoutResId, List<LiveRoomBean> data, int type,String pageid,int  currentType) {
        super(layoutResId, data);
        this.type = type;
        this.pageId = pageid;
        this.currentType = currentType;
    }

    @Override
    protected void convert(BaseViewHolder helper, LiveRoomBean item) {
        final View view = helper.getView(R.id.item);
        if (view instanceof LiveClassifyItemBaseView) {
            ((LiveClassifyItemBaseView) view).initMeasureDimen();
            ((LiveClassifyItemBaseView) view).initView(item,pageId,currentType,helper.getLayoutPosition());
        }

    }
}
