package com.thel.modules.login.login_register;


import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;

import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.base.BaseDataBean;
import com.thel.constants.BundleConstants;
import com.thel.constants.TheLConstants;
import com.thel.modules.login.LoginActivityManager;
import com.thel.modules.login.LoginEventManager;
import com.thel.modules.main.MainActivity;
import com.thel.modules.main.home.moments.web.WebViewActivity;
import com.thel.modules.select_country.SMSSelectCountryActivity;
import com.thel.network.LoginInterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.api.loginapi.bean.CheckNumberBean;
import com.thel.network.api.loginapi.bean.CodeBean;
import com.thel.network.api.loginapi.bean.SignInBean;
import com.thel.network.service.DefaultRequestService;
import com.thel.utils.MD5Utils;
import com.thel.utils.PhoneUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static android.app.Activity.RESULT_OK;
import static com.thel.modules.login.login_register.LoginRegisterActivity.BUNDLE;
import static com.thel.modules.login.login_register.LoginRegisterActivity.BUNDLE_CHANGE;
import static com.thel.modules.login.login_register.LoginRegisterActivity.LOGIN;
import static com.thel.modules.login.login_register.LoginRegisterActivity.PHONE_VERIFY;
import static com.thel.modules.login.login_register.LoginRegisterActivity.REGISTER;


/**
 * A simple {@link Fragment} subclass.
 */
public class InputNumberFragment extends Fragment implements TextWatcher {

    @BindView(R.id.txt_register)
    TextView txt_register;

    @BindView(R.id.txt_phone_verify)
    TextView txt_phone_verify;

    @BindView(R.id.txt_agreement)
    TextView txt_agreement;

    @BindView(R.id.rel_bind_question)
    RelativeLayout rel_bind_question;

    @BindView(R.id.txt_not_bind)
    TextView txt_not_bind;

    @BindView(R.id.txt_meet_question)
    TextView txt_meet_question;

    @BindView(R.id.et_phone_num)
    EditText et_phone_num;

    @BindView(R.id.et_code_input)
    EditText et_code_input;

    @BindView(R.id.txt_country_code)
    TextView txt_country_code;

    @BindView(R.id.txt_send_code)
    TextView txt_send_code;
    @BindView(R.id.rl_help)
    RelativeLayout txt_help;
    private int mType;
    private String currentName;
    private String currentCode;
    private String mSmsRequestId;
    private Handler mCountDownHandler;
    /**
     * 倒计时
     */
    private Runnable countDownRunnable = new Runnable() {
        int countDownTime = 60;

        @Override
        public void run() {
            countDownTime--;
            if (countDownTime == 0) {
                txt_send_code.setText(getString(R.string.send_sms_again));
                txt_send_code.setEnabled(true);
//                txt_send_code.setTextColor(ContextCompat.getColor(TheLApp.getContext(), R.color.text_color_green));
                countDownTime = 60;
            } else {
                txt_send_code.setText(getString(R.string.count_down, countDownTime));
                txt_send_code.setEnabled(false);
//                txt_send_code.setTextColor(ContextCompat.getColor(TheLApp.getContext(), R.color.text_color_gray));
                mCountDownHandler.postDelayed(this, 1000);
            }
        }
    };
    private boolean isWeb;


    public InputNumberFragment() {
        // Required empty public constructor
    }

    public static InputNumberFragment newInstance(int type,boolean isWeb) {
        InputNumberFragment fragment = new InputNumberFragment();
        Bundle bundle = new Bundle(1);
        bundle.putInt(LoginRegisterActivity.TYPE, type);
        bundle.putBoolean(LoginRegisterActivity.ISWEB,isWeb);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mType = getArguments().getInt(LoginRegisterActivity.TYPE);
            isWeb = getArguments().getBoolean(LoginRegisterActivity.ISWEB);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_input_number, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        et_code_input.addTextChangedListener(this);
        et_phone_num.addTextChangedListener(this);
        setDefaultCountry();
        initView();
        mCountDownHandler = new Handler();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        new SMSBroadcastReceiver().setOnReceivedMessageListener(new SMSBroadcastReceiver.MessageListener() {
//            public void OnReceived(String code) {
//                et_code_input.setText(code);
//            }
//        });
    }

    private void initView() {
        String registerText = "";
        switch (mType) {
            case LOGIN:
                registerText = getString(R.string.login_activity_login_btn);

                setSpanableText();
                break;
            case REGISTER:
                registerText = getString(R.string.default_activity_register);

                setSpanableText();
                break;
            case PHONE_VERIFY:
                registerText = getString(R.string.info_yes);

                setPhoneVerify(true);
                break;
            case BUNDLE:
            case BUNDLE_CHANGE:
                registerText = getString(R.string.bundle_mobile);
                break;
        }
        setRegisterText(registerText);
    }

    //设置默认国家
    private void setDefaultCountry() {

        String str = getResources().getStringArray(R.array.countries)[0];

        String[] arrDefault = str.split("\\+");
        currentName = arrDefault[0];
        currentCode = "+" + arrDefault[1];
        setTxt_country_code();
    }

    private void setTxt_country_code() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(currentName);
        stringBuilder.append(" (");
        stringBuilder.append(currentCode);
        stringBuilder.append(")");
        txt_country_code.setText(stringBuilder.toString());
    }


    /**
     * 显示用户协议提示
     */
    private void setSpanableText() {
        txt_agreement.setVisibility(View.VISIBLE);
        final String st1 = getString(R.string.login_txt1) + " ";
        final String st2 = getString(R.string.login_user_agreement);
        final int color = R.color.user_agreement_color;
        final String st = st1 + st2;
        SpannableString sp = new SpannableString(st);
        final int start = st.lastIndexOf(st2);
        final int end = start + st2.length();
        sp.setSpan(new ClickableSpan() {
            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setColor(ContextCompat.getColor(getActivity(), color));
                ds.setUnderlineText(false); // 去掉下划线
            }

            @Override
            public void onClick(View widget) {
                ViewUtils.preventViewMultipleClick(widget, 1000);

                Intent intent = new Intent(getActivity(), WebViewActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(BundleConstants.URL, TheLConstants.USER_AGREEMENT_PAGE_URL);
                bundle.putBoolean(BundleConstants.NEED_SECURITY_CHECK, false);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        }, start, end, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        txt_agreement.setText(sp);
        txt_agreement.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void setRegisterText(String text) {
        txt_register.setText(text);
    }

    private void setPhoneVerify(boolean visible) {
        txt_phone_verify.setVisibility(visible ? View.VISIBLE : View.GONE);
        rel_bind_question.setVisibility(visible ? View.VISIBLE : View.GONE);
        txt_not_bind.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);
        txt_meet_question.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);
    }

    @OnClick(R.id.txt_register)
    void registerBtn() {

        ((BaseActivity) getActivity()).showLoadingNoBack();
        final String verificationCode = et_code_input.getText().toString().trim();
        final String phone = et_phone_num.getText().toString().trim().replaceAll("\\s*", "");
        String code = currentCode;
        if (code.startsWith("+")) {
            code = code.substring(1);
        }
        final String finalCode = code;
        if (mType == BUNDLE || mType == BUNDLE_CHANGE || mType == PHONE_VERIFY) {//如果是换帮手机号
            RequestBusiness.getInstance()
                    .rebind("cell", phone, "+" + code, verificationCode, "", "", mSmsRequestId)
                    .onBackpressureDrop().subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new LoginInterceptorSubscribe<BaseDataBean>() {
                        @Override
                        public void onNext(BaseDataBean data) {
                            super.onNext(data);
                            ((BaseActivity) getActivity()).closeLoading();
                            if (!hasErrorCode && data != null && !data.errcode.equals("invalid_sms_code")) {
                                ShareFileUtils.setString(ShareFileUtils.BIND_CELL, phone);
                                if (mType == PHONE_VERIFY) {
                                    ShareFileUtils.setBoolean(ShareFileUtils.HAS_LOGGED, true);
                                    MobclickAgent.onEvent(getActivity(), "enter_main_page");// 注册转化率统计：进入主界面
                                     //从web页面过来绑定完手机号请求到新的web页面
                                    if (isWeb){
                                        Intent intent = new Intent(getActivity(), WebViewActivity.class);
                                        Bundle bundle = new Bundle();
                                        bundle.putString(BundleConstants.URL,ShareFileUtils.getString(ShareFileUtils.bindedPhoneUrl,""));
                                        bundle.putBoolean(WebViewActivity.NEED_SECURITY_CHECK, false);
                                        intent.putExtras(bundle);
                                        startActivity(intent);
                                    }else {
                                        Intent intent4 = new Intent(getActivity(), MainActivity.class);
                                        intent4.putExtra(TheLConstants.BUNDLE_KEY_WHERE_TO_GO, "ONE");
                                        startActivity(intent4);
                                        LoginActivityManager.getInstanse().finishAllActivity();

                                    }
                                } else {
                                    Intent intent = new Intent();
                                    intent.putExtra("country", finalCode);
                                    intent.putExtra("phone", phone);
                                    getActivity().setResult(RESULT_OK, intent);
                                    getActivity().finish();
                                }
                            } else {
                                getActivity().finish();

                            }
                        }

                        @Override
                        public void onError(Throwable t) {
                            super.onError(t);
                            ((BaseActivity) getActivity()).closeLoading();
                        }
                    });
        } else {
            //// TODO: 17/9/22
            RequestBusiness.getInstance()
                    .signIn("cell", "", "", phone, "+" + code, verificationCode, "", "", "", "", mSmsRequestId)
                    .onBackpressureDrop().subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new LoginInterceptorSubscribe<SignInBean>() {
                        @Override
                        public void onNext(SignInBean signInBean) {
                            super.onNext(signInBean);
                            ((BaseActivity) getActivity()).closeLoading();
                            if (signInBean.data == null || signInBean.data.user == null)
                                return;
                            LoginEventManager.loginCallback(getActivity(), signInBean);
                            getActivity().finish();
                        }

                        @Override
                        public void onError(Throwable t) {
                            super.onError(t);
                            if (getActivity() != null) {
                                ((BaseActivity) getActivity()).closeLoading();
                            }
                        }
                    });
        }
    }

    @OnClick(R.id.rl_help)
    void jumpHelpWeb() {
        final Intent intent = new Intent(getActivity(), WebViewActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(BundleConstants.URL, TheLConstants.LOGIN_HELP_RUL);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @OnClick(R.id.txt_meet_question)
    void question() {
        final Intent intent = new Intent(getActivity(), WebViewActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(BundleConstants.URL, TheLConstants.LOGIN_HELP_RUL);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    //忽略绑定手机
    @OnClick(R.id.txt_not_bind)
    void skip() {

        boolean isFirstStart = ShareFileUtils.getBoolean(ShareFileUtils.IS_FIRST_START, true);

        if (!isFirstStart) {
            ShareFileUtils.setBoolean(ShareFileUtils.IS_FIRST_START, true);
            ShareFileUtils.setBoolean(Utils.getMyUserId() + "_" + ShareFileUtils.IS_SHOW_GUIDE_LAYOUT, true);
        }

//        Intent intent = new Intent(getActivity(), MainActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        intent.putExtra("isNew", false);
//        intent.putExtra(TheLConstants.BUNDLE_KEY_WHERE_TO_GO, "ONE");
//        getActivity().startActivity(intent);
//
//        LoginActivityManager.getInstanse().finishAllActivity();
        if (getActivity() != null) {
            getActivity().finish();
        }
    }

    @OnClick(R.id.rel_country_code)
    void selectCountry() {
        this.startActivityForResult(new Intent(getActivity(), SMSSelectCountryActivity.class), TheLConstants.BUNDLE_CODE_SMS_SELECT_COUNTRY);
    }

    @OnClick(R.id.txt_send_code)
    void sendCode() {
        requestCode();
//        PermissionUtil.requestSMSPermission(getActivity(), new PermissionUtil.PermissionCallback() {
//            @Override
//            public void granted() {
//                requestCode();
//            }
//
//            @Override
//            public void denied() {
//                requestCode();
//            }
//
//            @Override
//            public void gotoSetting() {
//
//            }
//        });

    }

    private void requestCode() {
        if (mType == BUNDLE || mType == BUNDLE_CHANGE || mType == PHONE_VERIFY) {//如果是绑定手机，先检测是否手机已经绑定
            ((BaseActivity) getActivity()).showLoading();
            //// TODO: 17/9/22
            RequestBusiness.getInstance().
                    checkPhoneNumber(currentCode, et_phone_num.getText().toString().trim().replaceAll("\\s*", ""))
                    .onBackpressureDrop().subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new LoginInterceptorSubscribe<CheckNumberBean>() {
                        @Override
                        public void onNext(CheckNumberBean bean) {
                            super.onNext(bean);
                            if (getActivity() != null) {
                                ((BaseActivity) getActivity()).closeLoading();
                            }
                            if (bean.getData() == null) {
                                Toast.makeText(getActivity(), bean.errdesc, Toast.LENGTH_SHORT).show();
                                return;
                            }
                            boolean isRegistered = bean.getData().isExists();
                            if (isRegistered) {
                                Toast.makeText(getActivity(), getString(R.string.cell_already_registered), Toast.LENGTH_SHORT).show();
                            } else {
                                doNext();
                            }
                        }
                    });

        } else {//登录或者注册
            if (PhoneUtils.getNetWorkType() == PhoneUtils.TYPE_NO) {//如果没信号
                Toast.makeText(getActivity(), getString(R.string.info_no_network), Toast.LENGTH_SHORT).show();
                return;
            }
            doNext();
        }
    }

    /**
     * 检查电话号码
     */
    private void doNext() {
        String mPhone = et_phone_num.getText().toString().trim().replaceAll("\\s*", "");
        String mCode = currentCode;
        if (mCode.startsWith("+")) {
            mCode = mCode.substring(1);
        }

        if (TextUtils.isEmpty(mPhone)) {
            Toast.makeText(getActivity(), getString(R.string.smssdk_write_mobile_phone), Toast.LENGTH_SHORT).show();
            return;
        }

        MobclickAgent.onEvent(getActivity(), "start_request_send_code"); // 开始调用发送验证码接口打点
        ((BaseActivity) getActivity()).showLoading();

        Map<String, String> map = new HashMap<>();
        map.put("cell", mPhone.trim());
        map.put("zone", mCode);

        DefaultRequestService
                .createLoginRequestService()
                .sendCode(MD5Utils.generateSignatureForMap(map))
                .onBackpressureDrop().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new LoginInterceptorSubscribe<CodeBean>() {
                    @Override
                    public void onNext(CodeBean codeBean) {
                        super.onNext(codeBean);
                        if (getActivity() != null) {

                            ((BaseActivity) getActivity()).closeLoading();

                        }

                        if (codeBean != null && codeBean.getData() != null && !codeBean.errcode.equals("sms_yp_2")) {//手机号码格式错误
                            mSmsRequestId = codeBean.getData().getSmsRequestId();
                            mCountDownHandler.post(countDownRunnable);
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                    }
                });
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        boolean phone = et_phone_num.getText().toString().length() > 0;
        boolean code = et_code_input.getText().toString().length() > 0;
        txt_register.setEnabled(phone && code);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (RESULT_OK == resultCode) {
            switch (requestCode) {
                case TheLConstants.BUNDLE_CODE_SMS_SELECT_COUNTRY://选择国家  
                    if (!TextUtils.isEmpty(data.getStringExtra("code"))) {
                        // 国家列表返回
                        currentName = data.getStringExtra("name");
                        currentCode = "+" + data.getStringExtra("code");
                        setTxt_country_code();
                    }
                    break;
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mCountDownHandler.removeCallbacks(countDownRunnable);
    }
}
