package com.thel.modules.main.home.moments;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.facebook.drawee.view.SimpleDraweeView;
import com.thel.BuildConfig;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.bean.ContactBean;
import com.thel.bean.ContactsListBean;
import com.thel.bean.RecommendedModel;
import com.thel.bean.ReleaseMomentSucceedBean;
import com.thel.bean.SharePosterBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.recommend.RecommendWebBean;
import com.thel.bean.theme.ThemeCommentBean;
import com.thel.bean.user.UserCardBean;
import com.thel.bean.user.UserInfoBean;
import com.thel.bean.video.VideoBean;
import com.thel.constants.MomentTypeConstants;
import com.thel.constants.TheLConstants;
import com.thel.db.table.message.MsgTable;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.manager.ChatServiceManager;
import com.thel.modules.live.bean.LiveRoomBean;
import com.thel.modules.live.surface.LiveShowPresenter;
import com.thel.modules.main.messages.ChatActivity;
import com.thel.modules.main.userinfo.recommandcard.RecommendCardActivity;
import com.thel.modules.post.MomentPosterActivity;
import com.thel.modules.select_contacts.SelectContactsAdapter;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.ui.ShareRadioGroup;
import com.thel.utils.DialogUtil;
import com.thel.utils.GsonUtils;
import com.thel.utils.ImageUtils;
import com.thel.utils.L;
import com.thel.utils.PinyinUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.ViewUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by test1 on 2017/2/15.
 */

public class SendCardActivity extends BaseActivity {

    /**
     * 所有好友的对象列表
     */
    private ArrayList<ContactBean> listPlus = new ArrayList<>();
    /**
     * 输入关键字过滤后的朋友对象列表
     */
    private ArrayList<ContactBean> filterArr = new ArrayList<>();

    private String requestType = null;
    // 已经发送过请求的密友id列表
    private String circleFriendIds = null;
    private EditText mSearchContent;
    private ListView listView;
    private TextView tv_back;
    private SelectContactsAdapter adapter;
    private ContactBean friend;
    private RelativeLayout my_daily;
    private RelativeLayout share_to_wxchat;
    private SimpleDraweeView my_portrait;
    private TextWatcher textWatcher;
    private UserCardBean useriCard;
    private RelativeLayout ll_search;
    private RelativeLayout rel_portrait_me;
    private ImageView btn_press_me;
    private String my_portraits;
    private SimpleDraweeView select_portrait;
    private ShareRadioGroup share_radio_button;
    private TextView friend_count_tv;

    public static final int FROM_RECOMMEND_MOMENT = 1;
    public static final int FROM_FLUTTER = 2;//从flutter页面过来
    public static final int FROM_LIVE_USER = 3;
    public static final int FROM_CECOMMEND_WEBVIEW = 4;//从网页过来
    public static final int FROM_VIDEO = 5;
    public static final int FORM_THEME_RECOMMEND = 6;
    public static final int FROM_MATCH_SUCCESS = 7;
    public static final int FROM_USER_INFO = 8;//从他人主页
    private int from_page = 0;
    private MomentsBean recommendMomentBean;//推荐日志
    private RecommendWebBean recommendWebBean;
    private LiveRoomBean liveRoomBean;
    private LinearLayout poster_model;
    private View headerView;

    private String gio_page_from;
    private boolean isReleaseMoment;
    private String flutterJsonString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        L.d("SendCard", " onCreate : ");

        setContentView(R.layout.activity_send_card);
        findViewById();
        getIntentData();
        filterData();
        getChatList();
        initAdapter();
        setListener();

        ll_search.setVisibility(View.GONE);
    }

    private void getIntentData() {
        final Intent intent = getIntent();

        L.d("SendCard", " intent : " + intent);

        if (intent != null) {
            from_page = intent.getIntExtra(TheLConstants.BUNDLE_KEY_INTENT_FROM, 0);
            gio_page_from = intent.getStringExtra(TheLConstants.BUNDLE_KEY_PAGE_FROM);
            share_radio_button.initGIOPageFrom(gio_page_from);
            L.d("SendCard", " from_page : " + from_page);

            if (FROM_RECOMMEND_MOMENT == from_page) {//推荐日志
                recommendMomentBean = (MomentsBean) intent.getSerializableExtra(TheLConstants.BUNDLE_KEY_MOMENT_BEAN);

                SharePosterBean sharePosterBean = new SharePosterBean();
                sharePosterBean.avatar = recommendMomentBean.avatar;
                sharePosterBean.momentsText = recommendMomentBean.momentsText;
                sharePosterBean.imageUrl = recommendMomentBean.imageUrl;
                sharePosterBean.nickname = recommendMomentBean.nickname;
                sharePosterBean.userName = recommendMomentBean.userName;
                sharePosterBean.userId = String.valueOf(recommendMomentBean.userId);
                sharePosterBean.from = MomentPosterActivity.FROM_MOMENT;
                share_radio_button.setShareImagePath(poster_model, sharePosterBean);
                share_radio_button.setVisibility(View.VISIBLE);
                share_to_wxchat.setVisibility(View.VISIBLE);
                share_radio_button.shareMoment(SendCardActivity.this, recommendMomentBean);

            } else if (FROM_CECOMMEND_WEBVIEW == from_page) {//推荐网页
                recommendWebBean = (RecommendWebBean) intent.getSerializableExtra(TheLConstants.BUNDLE_KEY_RECOMMEND_WEB_BEAN);
                share_radio_button.shareMoment(SendCardActivity.this, recommendMomentBean);
            } else if (FROM_LIVE_USER == from_page) {
                liveRoomBean = (LiveRoomBean) intent.getSerializableExtra(TheLConstants.BUNDLE_KEY_MOMENT_BEAN);
                share_radio_button.setVisibility(View.VISIBLE);
                share_to_wxchat.setVisibility(View.VISIBLE);
                recommendMomentBean = new MomentsBean();
                recommendMomentBean.thumbnailUrl = liveRoomBean.imageUrl;
                recommendMomentBean.momentsType = LiveRoomBean.TYPE_VOICE == liveRoomBean.audioType ? MomentTypeConstants.MOMENT_TYPE_VOICE_LIVE : MomentTypeConstants.MOMENT_TYPE_LIVE;
                recommendMomentBean.momentsId = liveRoomBean.momentId;
                recommendMomentBean.momentsText = liveRoomBean.share.text;
                recommendMomentBean.title = liveRoomBean.share.title;
                recommendMomentBean.userId = (int) liveRoomBean.user.id;
                recommendMomentBean.nickname = liveRoomBean.user.nickName;
                recommendMomentBean.isLandscape = liveRoomBean.isLandscape;
                share_radio_button.shareMoment(SendCardActivity.this, recommendMomentBean);


            } else if (FROM_VIDEO == from_page) {
                VideoBean videoBean = (VideoBean) intent.getSerializableExtra(TheLConstants.BUNDLE_KEY_MOMENT_BEAN);

                SharePosterBean sharePosterBean = new SharePosterBean();
                sharePosterBean.avatar = videoBean.avatar;
                sharePosterBean.momentsText = videoBean.text;
                sharePosterBean.imageUrl = videoBean.image;
                sharePosterBean.nickname = videoBean.nickname;
                sharePosterBean.userName = videoBean.userId;
                sharePosterBean.from = MomentPosterActivity.FROM_MOMENT;
                sharePosterBean.userId = videoBean.userId;

                recommendMomentBean = new MomentsBean();
                recommendMomentBean.thumbnailUrl = videoBean.image;
                recommendMomentBean.momentsType = videoBean.type;
                recommendMomentBean.momentsId = videoBean.id;
                recommendMomentBean.momentsText = videoBean.text;
                recommendMomentBean.avatar = videoBean.avatar;
                recommendMomentBean.nickname = videoBean.nickname;
                recommendMomentBean.imageUrl = videoBean.image;
                recommendMomentBean.videoUrl = videoBean.videoUrl;
                recommendMomentBean.videoTag = videoBean.videoTag;
                recommendMomentBean.videoColor = videoBean.videoColor;
                recommendMomentBean.videoWebp = videoBean.videoWebp;
                recommendMomentBean.pixelHeight = videoBean.pixelHeight;
                recommendMomentBean.pixelWidth = videoBean.pixelWidth;
                recommendMomentBean.userId = Integer.valueOf(videoBean.userId);
                recommendMomentBean.momentsType = MomentTypeConstants.MOMENT_TYPE_VIDEO;
                share_radio_button.setShareImagePath(poster_model, sharePosterBean);
                share_radio_button.setVisibility(View.VISIBLE);
                share_to_wxchat.setVisibility(View.VISIBLE);
                share_radio_button.shareMoment(SendCardActivity.this, recommendMomentBean);

            } else if (FORM_THEME_RECOMMEND == from_page) {
                ThemeCommentBean themeCommentBean = (ThemeCommentBean) intent.getSerializableExtra(TheLConstants.BUNDLE_KEY_MOMENT_BEAN);

                SharePosterBean sharePosterBean = new SharePosterBean();
                sharePosterBean.avatar = themeCommentBean.avatar;
                sharePosterBean.momentsText = themeCommentBean.commentText;
                sharePosterBean.imageUrl = themeCommentBean.imageUrl;
                sharePosterBean.nickname = themeCommentBean.nickname;
                sharePosterBean.userName = String.valueOf(themeCommentBean.userId);
                sharePosterBean.from = MomentPosterActivity.FROM_MOMENT;

                recommendMomentBean = new MomentsBean();
                recommendMomentBean.thumbnailUrl = themeCommentBean.imageUrl;
                recommendMomentBean.momentsType = themeCommentBean.commentType;
                recommendMomentBean.momentsId = themeCommentBean.commentId;
                recommendMomentBean.momentsText = themeCommentBean.commentText;
                recommendMomentBean.avatar = themeCommentBean.avatar;
                recommendMomentBean.nickname = themeCommentBean.nickname;
                recommendMomentBean.imageUrl = themeCommentBean.imageUrl;
                if (themeCommentBean.videoComment != null) {
                    recommendMomentBean.videoUrl = themeCommentBean.videoComment.videoUrl;
                    recommendMomentBean.pixelHeight = themeCommentBean.videoComment.pixelHeight;
                    recommendMomentBean.pixelWidth = themeCommentBean.videoComment.pixelWidth;
                }
                recommendMomentBean.userId = themeCommentBean.userId;
                share_radio_button.setShareImagePath(poster_model, sharePosterBean);
                share_radio_button.setVisibility(View.VISIBLE);
                share_to_wxchat.setVisibility(View.VISIBLE);
                share_radio_button.shareMoment(SendCardActivity.this, recommendMomentBean);

            } else if (FROM_MATCH_SUCCESS == from_page) {
                share_radio_button.setVisibility(View.VISIBLE);
            } else if (FROM_USER_INFO == from_page) {
                useriCard = (UserCardBean) intent.getSerializableExtra("usercardbean");

                SharePosterBean sharePosterBean = new SharePosterBean();
                sharePosterBean.avatar = useriCard.avatar;
                sharePosterBean.momentsText = useriCard.recommendDesc;
                sharePosterBean.imageUrl = useriCard.bgImage;
                sharePosterBean.nickname = useriCard.nickName;
                sharePosterBean.userName = String.valueOf(useriCard.userId);
                sharePosterBean.userId = useriCard.userId;
                sharePosterBean.from = MomentPosterActivity.FROM_MOMENT;

                share_radio_button.setVisibility(View.VISIBLE);
                share_radio_button.setShareImagePath(poster_model, sharePosterBean, true);

            } else if (FROM_FLUTTER == from_page) {
                flutterJsonString = intent.getStringExtra("flutterJsonString");
                try {
                    share_radio_button.setVisibility(View.VISIBLE);
                    JSONObject jsonObject = new JSONObject(flutterJsonString);
                    SharePosterBean sharePosterBean = new SharePosterBean();
                    sharePosterBean.nickname = jsonObject.optString("nickname");
                    sharePosterBean.avatar = jsonObject.optString("avatar");
                    sharePosterBean.momentsText = jsonObject.optString("momentsText");
                    sharePosterBean.imageUrl = jsonObject.optString("imageUrl");
                    sharePosterBean.userName = jsonObject.optString("userName");
                    sharePosterBean.userId = jsonObject.optString("userId");
                    sharePosterBean.from = MomentPosterActivity.FROM_MOMENT;
                    share_radio_button.setShareImagePath(poster_model, sharePosterBean);

                    MomentsBean momentsBean = new MomentsBean();
                    momentsBean.nickname = sharePosterBean.nickname;
                    momentsBean.avatar = sharePosterBean.avatar;
                    momentsBean.momentsText = sharePosterBean.momentsText;
                    momentsBean.imageUrl = sharePosterBean.imageUrl;
                    momentsBean.userName = sharePosterBean.userName;
                    momentsBean.userId = Integer.parseInt(sharePosterBean.userId);
                    momentsBean.momentsId = jsonObject.optString("momentsId");
                    String momentsType = jsonObject.optString("momentsType");
                    if (momentsType == null || momentsType.isEmpty())
                        momentsType = jsonObject.optString("commentType");
                    momentsBean.momentsType = momentsType;
                    share_radio_button.shareMoment(SendCardActivity.this, momentsBean);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {

                share_radio_button.setVisibility(View.VISIBLE);

                useriCard = (UserCardBean) intent.getSerializableExtra("usercardbean");
            }
        }
    }

    private void initAdapter() {

        adapter = new SelectContactsAdapter(listPlus, false);
        listView.setAdapter(adapter);
        listView.addHeaderView(headerView);
    }

    private void setListener() {
        tv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                position = position - listView.getHeaderViewsCount();
                if (position < 0) {
                    return;
                }

                friend = (ContactBean) adapter.getItem(position);

                isReleaseMoment = false;
                send();

                Intent intent = new Intent();
                intent.setAction(LiveShowPresenter.BR_RECOMMEND);
                LocalBroadcastManager.getInstance(SendCardActivity.this).sendBroadcast(intent);
            }
        });

        //跳转到我的日志
        my_daily.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                friend = null;
                isReleaseMoment = true;
                send();

                Intent intent = new Intent();
                intent.setAction(LiveShowPresenter.BR_RECOMMEND);
                LocalBroadcastManager.getInstance(SendCardActivity.this).sendBroadcast(intent);
            }
        });

        textWatcher = new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                filterArr.clear();
                if (adapter == null) {
                    return;
                }
                if (TextUtils.isEmpty(arg0.toString().trim())) {
                    adapter.refreshAdapter(listPlus);
                } else {
                    for (ContactBean contact : listPlus) {
                        if (contact.nickNamePinYin != null && contact.nickNamePinYin.contains(PinyinUtils.cn2Spell(arg0.toString().trim()))) {
                            filterArr.add(contact);
                        }
                    }
                    adapter.refreshAdapter(filterArr);
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
        mSearchContent.addTextChangedListener(textWatcher);

        select_portrait.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.setVisibility(View.GONE);
                btn_press_me.setVisibility(View.INVISIBLE);
                rel_portrait_me.setBackgroundResource(R.drawable.bg_border_round_normal);

                ll_search.setVisibility(View.GONE);
            }
        });

        share_to_wxchat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                share_radio_button.findViewById(R.id.btn_wx).performClick();
            }
        });
    }

    private void send() {
        if (FROM_FLUTTER == from_page) {
            sendToFlutter();
        } else {
            if (FROM_RECOMMEND_MOMENT == from_page || FROM_VIDEO == from_page || FORM_THEME_RECOMMEND == from_page) {
                recommendMoment();
            } else if (FROM_CECOMMEND_WEBVIEW == from_page) {
                recommendWeb();//推荐网页
            } else if (FROM_LIVE_USER == from_page) {
                recommendLiveUser();
            } else if (FROM_USER_INFO == from_page) {
                recommendCard();
            } else {//发送名片
                if (listView != null && !isReleaseMoment) {
                    Intent intent = new Intent(SendCardActivity.this, ChatActivity.class);
                    Bundle bundle = new Bundle();
                    intent.putExtra("toUserId", friend.userId);
                    intent.putExtra("toAvatar", friend.avatar);
                    intent.putExtra("toName", friend.nickName);
                    bundle.putSerializable("usercardbean", useriCard);
                    intent.putExtras(bundle);
                    intent.putExtra("send", 1);
                    intent.putExtra("fromPage", "SendCardActivity");
                    startActivity(intent);
                } else {
                    showLoading();

                    Flowable<ReleaseMomentSucceedBean> flowable = RequestBusiness.getInstance().
                            releaseUserCardMoment(useriCard.userId + "", useriCard.recommendDesc);

                    flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<ReleaseMomentSucceedBean>() {
                        @Override
                        public void onNext(ReleaseMomentSucceedBean data) {
                            super.onNext(data);
                            closeLoading();
                            DialogUtil.showToastShort(SendCardActivity.this, getResources().getString(R.string.post_moment_posted));
                            finish();
                        }

                        @Override
                        public void onComplete() {
                            super.onComplete();
                            closeLoading();
                        }
                    });
                }
            }
        }
    }

    private void sendToFlutter() {
        try {
            RecommendedModel recommendedModel = new RecommendedModel();
            JSONObject jsonObject = new JSONObject(flutterJsonString);
            String momentsType = jsonObject.optString("momentsType");
            if (momentsType == null || momentsType.isEmpty())
                momentsType = jsonObject.optString("commentType");
            String recommendType = jsonObject.optString("recommendType");

            if (recommendType.equals("userCard")) {
                UserInfoBean userInfoBean = GsonUtils.getObject(flutterJsonString, UserInfoBean.class);
                if (userInfoBean != null) {
                    recommendedModel.cardUserId = userInfoBean.id;
                    recommendedModel.cardAffection = userInfoBean.affection;
                    recommendedModel.cardAge = userInfoBean.age;
                    recommendedModel.cardAvatar = userInfoBean.avatar;
                    recommendedModel.cardWeight = userInfoBean.weight;
                    recommendedModel.cardHeight = userInfoBean.height;
                    recommendedModel.cardIntro = userInfoBean.intro;
                    recommendedModel.cardNickName = userInfoBean.nickName;
                    recommendedModel.cardBirthday = userInfoBean.birthday;
                    recommendedModel.cardBgImage = userInfoBean.bgImage;
                    recommendedModel.msgType = "userCard";
                    recommendedModel.recommendedType = "user";

                }
            } else {
                if (momentsType.equals(MomentsBean.MOMENT_TYPE_RECOMMEND)) {
                    jsonObject = new JSONObject(jsonObject.optString("parentMoment"));
                }
                recommendedModel.avatar = jsonObject.optString("avatar");
                recommendedModel.imageUrl = jsonObject.optString("imageUrl");
                recommendedModel.momentsId = jsonObject.optLong("momentsId");
                if (recommendedModel.momentsId == 0)
                    recommendedModel.momentsId = jsonObject.optLong("commentId");
                recommendedModel.momentsText = jsonObject.optString("momentsText");
                if (recommendedModel.momentsText == null || recommendedModel.momentsText.isEmpty())
                    recommendedModel.momentsText = jsonObject.optString("commentText");
                recommendedModel.nickname = jsonObject.optString("nickname");
                recommendedModel.momentsType = momentsType;
                recommendedModel.liveId = jsonObject.optString("liveId");
                recommendedModel.playTime = jsonObject.optInt("playTime");
                recommendedModel.playCount = jsonObject.optInt("playTime");
                recommendedModel.pixelHeight = jsonObject.optInt("pixelHeight");
                recommendedModel.pixelHeight = jsonObject.optInt("pixelHeight");
                recommendedModel.shareTo = jsonObject.optInt("shareTo");
                recommendedModel.userId = jsonObject.optInt("userId");
                recommendedModel.commentNum = jsonObject.optInt("commentNum");
                recommendedModel.adUrl = jsonObject.optString("adUrl");
                recommendedModel.title = jsonObject.optString("title");
                recommendedModel.isLandscape = jsonObject.optBoolean("isLandscape");
                recommendedModel.description = jsonObject.optString("description");
                recommendedModel.liveStatus = String.valueOf(jsonObject.optInt("liveStatus"));

                if (momentsType.equals(MomentsBean.MOMENT_TYPE_THEME)) {
                    recommendedModel.recommendedType = "theme";
                    String minText = jsonObject.optString("minText");
                    if (minText != null && !minText.isEmpty()) {
                        recommendedModel.momentsText = minText;
                    }
                    String minMediaJsonString = jsonObject.optString("minMedia");
                    if (minMediaJsonString != null && !minMediaJsonString.isEmpty() && !minMediaJsonString.equals("[]")) {
                        JSONArray jsonArray = new JSONArray(minMediaJsonString);
                        JSONObject mediaJsonObj = (JSONObject) jsonArray.get(0);
                        recommendedModel.contentType = mediaJsonObj.getString("contentType");
                        recommendedModel.imageUrl = mediaJsonObj.getString("imageUrl");
                    }
                } else if (momentsType.equals(MomentsBean.MOMENT_TYPE_LIVE) || momentsType.equals(MomentsBean.MOMENT_TYPE_VOICE_LIVE)) {
                    recommendedModel.description = jsonObject.optString("momentsText");
                    recommendedModel.momentsText = recommendedModel.nickname + " is on LIVE" + recommendedModel.momentsText;
                    recommendedModel.recommendedType = "liveUser";
                } else if (momentsType.equals(MomentsBean.MOMENT_TYPE_WEB)) {
                    recommendedModel.recommendedType = "web";
                    String jsonWeb = jsonObject.optString("momentsText");
                    jsonObject = new JSONObject(jsonWeb);
                    recommendedModel.momentsText = jsonObject.optString("momentsText");
                    recommendedModel.imageUrl = jsonObject.optString("imageUrl");
                    recommendedModel.title = jsonObject.optString("title");
                    recommendedModel.url = jsonObject.optString("url");
                }
            }
            flutterReleaseRecommend(recommendedModel, momentsType);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void flutterReleaseRecommend(RecommendedModel recommendedModel, String momentsType) {
        if (friend != null) {
            recommendedModel.receiveUserId = Integer.parseInt(friend.userId);
            recommendedModel.receiveNickName = friend.nickName;
            recommendedModel.receiveAvatar = friend.avatar;
            if (momentsType.equals(MomentsBean.MOMENT_TYPE_TEXT) || momentsType.equals(MomentsBean.MOMENT_TYPE_LIVE_USER)) {
                recommendedModel.msgType = "moment_text";
            } else if (momentsType.equals(MomentsBean.MOMENT_TYPE_IMAGE) || momentsType.equals(MomentsBean.MOMENT_TYPE_TEXT_IMAGE)) {
                recommendedModel.msgType = "moment_image";
            } else if (momentsType.equals(MomentsBean.MOMENT_TYPE_VOICE) || momentsType.equals(MomentsBean.MOMENT_TYPE_TEXT_VOICE)) {
                recommendedModel.msgType = "moment_music";
            } else if (momentsType.equals(MomentsBean.MOMENT_TYPE_THEME)) {
                recommendedModel.msgType = "moment_theme";
            } else if (momentsType.equals(MomentsBean.MOMENT_TYPE_THEME_REPLY)) {
                recommendedModel.msgType = "moment_themereply";
            } else if (momentsType.equals(MomentsBean.MOMENT_TYPE_VIDEO)) {
                recommendedModel.msgType = "moment_video";
            } else if (momentsType.equals(MomentsBean.MOMENT_TYPE_LIVE)) {
                recommendedModel.msgType = "moment_live";
            } else if (momentsType.equals(MomentsBean.MOMENT_TYPE_USER_CARD)) {
                recommendedModel.msgType = "moment_usercard";
            } else if (momentsType.equals(MomentsBean.MOMENT_TYPE_RECOMMEND)) {
                recommendedModel.msgType = "moment_recommend";
            } else if (momentsType.equals(MomentsBean.MOMENT_TYPE_WEB)) {
                recommendedModel.msgType = "web";
            } else if (momentsType.equals(MomentsBean.MOMENT_TYPE_AD)) {
                recommendedModel.msgType = "moment_ad";
            } else if (momentsType.equals(MomentsBean.MOMENT_TYPE_VOICE_LIVE)) {
                recommendedModel.msgType = "moment_voicelive";
            } else {
                recommendedModel.msgType = "moment_text";
            }
        }
        FlutterRouterConfig.Companion.gotoReleaseRecommend(GsonUtils.createJsonString(recommendedModel));
    }

    private void recommendCard() {
        Intent intent = new Intent(SendCardActivity.this, RecommendCardActivity.class);

        if (friend != null) {
            intent.putExtra("toAvatar", friend.avatar);
            intent.putExtra("toUserId", friend.userId);
            intent.putExtra("toName", friend.nickName);
        }

        intent.putExtra("usercardbean", useriCard);
        if (listView != null && !isReleaseMoment) {
            intent.putExtra("type", "chat");
        } else {
            intent.putExtra("type", "moment");
        }
        startActivityForResult(intent, 0);
    }

    private void recommendLiveUser() {
        if (BuildConfig.VERSION_CODE < 50000) {
            if (listView != null && !isReleaseMoment && liveRoomBean != null) {//聊天

                final Intent intent = new Intent(this, WriteRecommendMomentToChatActivity.class);
                Bundle bundle = new Bundle();
                intent.putExtra("toUserId", friend.userId);
                intent.putExtra("toAvatar", friend.avatar);
                intent.putExtra("toName", friend.nickName);

                bundle.putSerializable(TheLConstants.BUNDLE_KEY_LIVEUSER_BEAN, liveRoomBean);
                intent.putExtra(TheLConstants.BUNDLE_KEY_INTENT_FROM, WriteRecommendMomentToChatActivity.FROM_PAGE_WRITE_LIVEUSER);
                intent.putExtras(bundle);
                startActivityForResult(intent, 0);

            } else if (isReleaseMoment && liveRoomBean != null) {
                final Intent intent = new Intent(this, WriteRecommendMomentActivity.class);
                intent.putExtra(TheLConstants.BUNDLE_KEY_LIVEUSER_BEAN, liveRoomBean);
                intent.putExtra(TheLConstants.BUNDLE_KEY_INTENT_FROM, WriteRecommendMomentActivity.FROM_PAGE_LIVEUSER);
                startActivityForResult(intent, 0);
            }
        } else {
            RecommendedModel recommendedModel = new RecommendedModel();
            recommendedModel.imageUrl = liveRoomBean.imageUrl;
            recommendedModel.momentsText = liveRoomBean.text;
            recommendedModel.liveId = liveRoomBean.liveId;
            recommendedModel.avatar = liveRoomBean.user.avatar;
            recommendedModel.userId = (int) liveRoomBean.user.id;
            recommendedModel.isLandscape = liveRoomBean.isLandscape == 1;
            recommendedModel.nickname = liveRoomBean.user.nickName;
            recommendedModel.momentsType = "live_user";
            recommendedModel.recommendedType = "liveUser";
            flutterReleaseRecommend(recommendedModel, MomentsBean.MOMENT_TYPE_LIVE_USER);
        }
    }

    /**
     * 推荐网页
     */
    private void recommendWeb() {
        if (BuildConfig.VERSION_CODE < 50000) {
            if (listView != null && !isReleaseMoment && recommendWebBean != null) {//聊天

                final Intent intent = new Intent(this, WriteRecommendMomentToChatActivity.class);
                Bundle bundle = new Bundle();
                intent.putExtra("toUserId", friend.userId);
                intent.putExtra("toAvatar", friend.avatar);
                intent.putExtra("toName", friend.nickName);

                bundle.putSerializable(TheLConstants.BUNDLE_KEY_RECOMMEND_WEB_BEAN, recommendWebBean);
                intent.putExtra(TheLConstants.BUNDLE_KEY_INTENT_FROM, WriteRecommendMomentToChatActivity.FROM_PAGE_WRITE_WEBVIEW);
                intent.putExtras(bundle);
                startActivity(intent);
                finish();

            } else if (isReleaseMoment && recommendWebBean != null) {//推荐日志（转发网页）
                final Intent intent = new Intent(this, WriteRecommendMomentActivity.class);
                intent.putExtra(TheLConstants.BUNDLE_KEY_RECOMMEND_WEB_BEAN, recommendWebBean);
                intent.putExtra(TheLConstants.BUNDLE_KEY_INTENT_FROM, ChatActivity.FROM_PAGE_WEBVIEW);
                startActivityForResult(intent, 0);
            }
        } else {
            RecommendedModel recommendedModel = new RecommendedModel();
            recommendedModel.imageUrl = recommendWebBean.imageUrl;
            recommendedModel.title = recommendWebBean.title;
            recommendedModel.url = recommendWebBean.url;
            recommendedModel.momentsText = recommendWebBean.momentsText;
            recommendedModel.recommendedType = "web";
            flutterReleaseRecommend(recommendedModel, MomentsBean.MOMENT_TYPE_WEB);
        }
    }

    /**
     * 推荐日志
     */
    private void recommendMoment() {
        if (listView != null && !isReleaseMoment) {//聊天
            final Intent intent = new Intent(this, WriteRecommendMomentToChatActivity.class);
            Bundle bundle = new Bundle();
            intent.putExtra("toUserId", friend.userId);
            intent.putExtra("toAvatar", friend.avatar);
            intent.putExtra("toName", friend.nickName);

            bundle.putSerializable(TheLConstants.BUNDLE_KEY_MOMENT_BEAN, recommendMomentBean);
            intent.putExtra(TheLConstants.BUNDLE_KEY_INTENT_FROM, WriteRecommendMomentToChatActivity.FROM_PAGE_WRITE_MOMENT);
            intent.putExtras(bundle);
            startActivityForResult(intent, 0);
        } else if (isReleaseMoment) {//推荐日志（转发日志）
            final Intent intent = new Intent(this, WriteRecommendMomentActivity.class);
            intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_BEAN, recommendMomentBean);
            intent.putExtra(TheLConstants.BUNDLE_KEY_INTENT_FROM, "from_page_moment");
            startActivityForResult(intent, 0);
        }
    }

    private void findViewById() {

        headerView = LayoutInflater.from(this).inflate(R.layout.layout_send_card_header, null);
        mSearchContent = findViewById(R.id.edit_search);
        listView = findViewById(R.id.listview);
        tv_back = findViewById(R.id.tv_back);
        listView.setSelector(new ColorDrawable(Color.TRANSPARENT));
        my_daily = headerView.findViewById(R.id.my_daily);
        share_to_wxchat = headerView.findViewById(R.id.share_to_wxchat);
        my_portrait = headerView.findViewById(R.id.my_portrait);
        ll_search = findViewById(R.id.ll_search);
        rel_portrait_me = headerView.findViewById(R.id.rel_portrait_me);
        btn_press_me = headerView.findViewById(R.id.btn_press_me);
        select_portrait = findViewById(R.id.select_portrait);
        share_radio_button = findViewById(R.id.share_radio_button);
        poster_model = findViewById(R.id.poster_model);
        friend_count_tv = headerView.findViewById(R.id.friend_count_tv);
        initData();
    }

    private void initData() {
        my_portraits = ShareFileUtils.getString(ShareFileUtils.AVATAR, "");
        my_portrait.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(my_portraits, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE)));
        my_daily.setVisibility(View.VISIBLE);
        headerView.findViewById(R.id.divider).setVisibility(View.VISIBLE);
    }

    /**
     * 排除不能选择的用户
     */
    private void filterData() {
        if (!TextUtils.isEmpty(circleFriendIds) && requestType != null) {
            try {
                if (listPlus.size() > 0) {
                    List<ContactBean> filterList = new ArrayList<>();
                    for (ContactBean contactBean : listPlus) {
                        if (circleFriendIds.contains(contactBean.userId + ",")) {
                            filterList.add(contactBean);
                        }
                    }
                    listPlus.removeAll(filterList);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void getChatList() {

//        Flowable.create(new FlowableOnSubscribe<List<MsgTable>>() {
//            @Override
//            public void subscribe(FlowableEmitter<List<MsgTable>> e) throws Exception {
//                List<MsgTable> msgTables = ChatServiceManager.getInstance().getMsgList(0);
//                e.onNext(msgTables);
//                e.onComplete();
//
//            }
//        }, BackpressureStrategy.DROP).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<List<MsgTable>>() {
//            @Override
//            public void onSubscribe(Subscription s) {
//                s.request(Integer.MAX_VALUE);
//            }
//
//            @Override
//            public void onNext(List<MsgTable> msgTables) {
//            }
//
//            @Override
//            public void onError(Throwable t) {
//
//            }
//
//            @Override
//            public void onComplete() {
//
//            }
//        });

        getContactsList();

    }

    private void getContactsList() {
        RequestBusiness.getInstance().getContactsList()
                .onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new InterceptorSubscribe<ContactsListBean>() {
                    @Override
                    public void onNext(ContactsListBean data) {
                        super.onNext(data);
                        listPlus.clear();
                        ContactsListBean contacts = data;
                        if (contacts.data != null && contacts.data.users != null) {
                            listPlus.addAll(contacts.data.users);

                            for (ContactBean contactBean : listPlus){
                                contactBean.nickNamePinYin = PinyinUtils.cn2Spell(contactBean.nickName);
                            }

                            filterData();
                            //重新返回到选择好友界面
                            if (listPlus.size() > 0) {
                                adapter.notifyDataSetChanged();
                                friend_count_tv.setText(TheLApp.context.getResources().getString(R.string.friends_title_odd, listPlus.size()));
                            } else {
                                friend_count_tv.setText(TheLApp.context.getResources().getString(R.string.friends_title_odd, 0));
                            }

                        }
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        share_radio_button.onActivityResult(requestCode, resultCode, data);
        switch (resultCode) {
            case TheLConstants.BUNDLE_CODE_SEND_CARD_ACTIVITY:
            case TheLConstants.RESULT_CODE_RECOMMEND_TO_CHAT_SUCCESS://推荐到聊天成功
            case TheLConstants.RESULT_CODE_RECOMMEND_TO_MOMENT_SUCCESS://推荐到日志成功
                setResult(TheLConstants.RESULT_CODE_RECOMMEND_SUCCESS);//设置返回消息为推荐成功
                finish();
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mSearchContent != null) {
            ViewUtils.hideSoftInput(SendCardActivity.this);
        }
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }
}
