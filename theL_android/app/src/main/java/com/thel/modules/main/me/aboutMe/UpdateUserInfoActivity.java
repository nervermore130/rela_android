package com.thel.modules.main.me.aboutMe;

import android.app.Dialog;
import android.app.Service;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.ItemTouchHelper;

import android.text.TextUtils;
import android.util.SparseArray;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UploadManager;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.base.BaseAdapter;
import com.thel.bean.CheckUserNameBean;
import com.thel.bean.UploadVideoAlbumBean;
import com.thel.bean.user.MyImageBean;
import com.thel.bean.user.MyImagesListBean;
import com.thel.bean.user.UploadTokenBean;
import com.thel.constants.TheLConstants;
import com.thel.modules.main.me.adapter.UpdataUserInfoDialogAdapter;
import com.thel.modules.main.me.adapter.UpdataUserInfoMultiDialogAdapter;
import com.thel.modules.main.me.bean.MyInfoBean;
import com.thel.modules.main.me.bean.RoleBean;
import com.thel.modules.main.me.bean.UpdataInfoBean;
import com.thel.modules.main.nearby.CoverPreviewActivity;
import com.thel.modules.main.settings.SettingActivity;
import com.thel.modules.select_image.SelectLocalImagesActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.RequestConstants;
import com.thel.ui.ChatGuideLayout;
import com.thel.ui.adapter.MyImageAdapter;
import com.thel.ui.dialog.ActionSheetDialog;
import com.thel.ui.imageviewer.cropiwa.image.CropIwaResultReceiver;
import com.thel.utils.AndroidBug5497Workaround;
import com.thel.utils.DateUtils;
import com.thel.utils.DeviceUtils;
import com.thel.utils.DialogUtil;
import com.thel.utils.ImageUtils;
import com.thel.utils.L;
import com.thel.utils.MD5Utils;
import com.thel.utils.PermissionUtil;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SizeUtils;
import com.thel.utils.ToastUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import cn.udesk.photoselect.decoration.GridSpacingItemDecoration;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import video.com.relavideolibrary.RelaVideoSDK;
import video.com.relavideolibrary.Utils.DensityUtils;
import video.com.relavideolibrary.onRelaVideoActivityResultListener;

import static com.thel.utils.DeviceUtils.getFilePathFromUri;

/**
 * 我的资料页
 * Created by lingwei on 2017/9/25.
 */

public class UpdateUserInfoActivity extends BaseActivity implements UpdateUserInfoContract.View, View.OnClickListener {
    private UpdateUserInfoContract.Presenter presenter;
    private TextView txt_title;
    private LinearLayout lin_done;
    private LinearLayout lin_back;
    private SimpleDraweeView img_avatar;
    private RelativeLayout rel_age;
    private TextView edit_age;
    private TextView edit_ethnicity;
    private RelativeLayout rel_height;
    private TextView edit_height;
    private RelativeLayout rel_weight;
    private TextView edit_weight;
    private RelativeLayout rel_relationship;
    private TextView edit_relationship;
    private RelativeLayout rel_role;
    private TextView edit_role;
    private RelativeLayout rel_lookingfor_role;
    private TextView edit_lookingfor_role;
    private RelativeLayout rel_nickname;
    private TextView edit_name;
    private TextView edit_user_name;
    private LinearLayout lin_user_id_tip;
    private TextView edit_career;
    private TextView edit_apartment;
    private TextView edit_selfintroduction;
    private RecyclerView user_pic_rv;
    private LinearLayout root_LinearLayout;

    private ArrayList<List<String>> careerTypeNames;
    private ArrayList<MyImageBean> myImageslist = new ArrayList<>();
    private int total = 0;
    // 用户可选项列表
    private ArrayList<String> relationship_list = new ArrayList<String>(); // 感情状态
    private ArrayList<String> role_list = new ArrayList<String>(); // 角色
    private ArrayList<String> role_list_index = new ArrayList<>();//角色对应的key
    private ArrayList<String> weight_list_kg = new ArrayList<String>(); // 体重kg
    private ArrayList<String> weight_list_lbs = new ArrayList<String>(); // 体重lbs
    private ArrayList<String> height_list_cm = new ArrayList<String>(); // 身高cm
    private ArrayList<String> height_list_inches = new ArrayList<String>(); // 身高inches
    private ArrayList<Integer> selectPositions = new ArrayList<Integer>(); // 交友目的多选列表
    private ArrayList<Integer> templist = new ArrayList<Integer>(); // 交友目的多选临时列表(在dialog没点确定之前记录)
    private ArrayList<Integer> lookingForRoleSelectPositions = new ArrayList<Integer>(); // 寻找角色多选列表
    private ArrayList<Integer> lookingForRoleTemplist = new ArrayList<Integer>(); // 寻找角色多选临时列表(在dialog没点确定之前记录)

    private StringBuilder lookingForRoleSB = new StringBuilder(); // 寻找角色数组下标字符串拼接

    private StringBuilder purposeSB = new StringBuilder(); // 交友目的数组下标字符串拼接
    private String careerType;

    private int tempCurrent1 = 0; // 记录临时位置
    private int tempCurrent2 = 0; // 记录临时位置
    private int heightUnits = 0; // 身高单位(0=cm, 1=Inches)
    private int weightUnits = 0; // 体重单位(0=kg, 1=Lbs)

    // 各列表的选择位置
    private int height_cm_cur_pos = 0;
    private int height_inches_cur_pos = 0;
    private int weight_kg_cur_pos = 0;
    private int weight_Lbs_cur_pos = 0;
    private int role_cur_pos = 0;
    private int role_posiotn = 0;
    private int role_current = 0;
    private int relationship_cur_pos = 0;
    private int relationship_role_posiotn = 0;
    private int ethnicity_cur_pos = 0;

    public static final String FROM_PAGE = "from_page";
    public static final String FROM_PAGE_USERINFOACTIVITY = "from_page_useriInfoActivity";
    public static final String FROM_PAGE_SIGN_NAME = "updateSignName";//更新签名
    private MyInfoBean myInfoBean;
    private String avatarUrl;
    private boolean hasUserName = false;
    private Calendar birthday = Calendar.getInstance();
    private DialogUtil dialogUtil;

    private int editWhich = 0; // 当前正在编辑的是1:年龄, 2:星座, 3:身高, 4:体重, 5:角色, 6:感情状态,7 寻找角色
    private MyImageBean cameraImageBean;
    private String albumPhotoPath = "";// 要上传的相册图片的本地地址
    public static boolean needRefreshMyPhotos = false;

    private SparseArray<String> array = new SparseArray<>();

    private String videoThumnail;//上传视频缩略图地址
    private String uploadVideoPath;//上传的视频加密后路径
    private String videoPath;//上传的视频本地地址
    private String uploadGifpath;//上传视频gif地址
    private SparseArray<RoleBean> roleMap = new SparseArray<RoleBean>();
    private SparseArray<RoleBean> relationshipMap = new SparseArray<RoleBean>();

    private TextView tx_bools_num;
    private MyImageAdapter mMyImageAdapter;
    private ItemTouchHelper mItemTouchHelper;
    private GridLayoutManager mGridLayoutManager;
    private ChatGuideLayout mGuideLayout;
    private TextView photo_count;
    private ScrollView scrollView;
    private RelativeLayout rel_livein;
    private RelativeLayout rel_edit_slogan;
    private RelativeLayout rel_career;

    private RelativeLayout rel_nearby_cover;
    private TextView tv_photo_tips;
    private CropIwaResultReceiver cropResultReceiver;
    private TextView copy_user_id;
    private View copy_user_line;

    @Override

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        //        setTheme(android.R.style.Theme_Holo_Light_NoActionBar);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.update_user_info_activity);
        //  showTranslucentView();
        AndroidBug5497Workaround.assistActivity(this);
        new UpdateUserInfoPresenter(this);
        dialogUtil = new DialogUtil();
        findViewById();
        getDataFromPrepage();
        initImage();
        setListener();
        initData();
        initUI();

        String type = getIntent().getStringExtra(FROM_PAGE);
        //如果是更新签名，自动滑倒底部，并跳转到修改签名页
        if (!TextUtils.isEmpty(type) && type.equals(FROM_PAGE_SIGN_NAME)) {
            edit_selfintroduction.setText(getIntent().getStringExtra("selfintroduction"));
            rel_edit_slogan.performClick();
            scrollView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //                    scrollView.fullScroll(ScrollView.FOCUS_DOWN);
                    scrollView.scrollTo(0, rel_nickname.getTop() - DensityUtils.dp2px(20));
                }
            }, 100);
        }

        cropResultReceiver = new CropIwaResultReceiver();
        cropResultReceiver.register(this);
        cropResultReceiver.setListener(new CropIwaResultReceiver.Listener() {
            @Override
            public void onCropSuccess(Uri croppedUri) {
                String path = getFilePathFromUri(UpdateUserInfoActivity.this, croppedUri);
                if (!TextUtils.isEmpty(path)) {
                    avatarPhotoPath = path;
                    img_avatar.setImageURI(Uri.parse(TheLConstants.FILE_PIC_URL + path));
                } else {
                    Toast.makeText(UpdateUserInfoActivity.this, TheLApp.getContext().getString(R.string.info_rechoise_photo), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCropFailed(Throwable e) {
                ToastUtils.showToastShort(UpdateUserInfoActivity.this, "crop failed");
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cropResultReceiver.unregister(this);
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void initMap(View view) {
        if (view instanceof ViewGroup) {
            final int size = ((ViewGroup) view).getChildCount();
            for (int i = 0; i < size; i++) {
                initMap(((ViewGroup) view).getChildAt(i));
            }
        } else if (view instanceof TextView) {
            saveData(view.getId(), ((TextView) view).getText().toString().trim());
        }
    }

    private boolean showDialog(View view) {
        if (view instanceof ViewGroup) {
            final int size = ((ViewGroup) view).getChildCount();
            for (int i = 0; i < size; i++) {
                if (showDialog(((ViewGroup) view).getChildAt(i))) {
                    return true;
                }
            }
        } else if (view instanceof TextView) {
            final String content = ((TextView) view).getText().toString();
            final String ct = array.get(view.getId());

            return !TextUtils.isEmpty(ct) && !ct.equals(content);

        }

        return false;
    }

    private void saveData(int viewId, String text) {

        array.put(viewId, text);
    }

    private void findViewById() {
        lin_done = findViewById(R.id.lin_done);
        lin_back = findViewById(R.id.lin_back);
        user_pic_rv = findViewById(R.id.user_pic_rv);
        img_avatar = findViewById(R.id.img_avatar);
        rel_age = findViewById(R.id.rel_age);
        edit_age = findViewById(R.id.edit_age);
        copy_user_id = findViewById(R.id.copy_user_id);
        copy_user_line = findViewById(R.id.copy_user_line);
        //        edit_horoscope = (TextView) findViewById(R.id.edit_horoscope);
        rel_height = findViewById(R.id.rel_height);
        edit_height = findViewById(R.id.edit_height);
        rel_weight = findViewById(R.id.rel_weight);
        edit_weight = findViewById(R.id.edit_weight);
        rel_relationship = findViewById(R.id.rel_relationship);
        edit_relationship = findViewById(R.id.edit_relationship);
        rel_role = findViewById(R.id.rel_role);
        edit_role = findViewById(R.id.edit_role);
        rel_lookingfor_role = findViewById(R.id.rel_lookingfor_role);
        edit_lookingfor_role = findViewById(R.id.edit_lookingfor_role);
        rel_nickname = findViewById(R.id.rel_nickname);
        edit_name = findViewById(R.id.edit_name);
        edit_user_name = findViewById(R.id.edit_user_name);
        lin_user_id_tip = findViewById(R.id.lin_user_id_tip);
        edit_career = findViewById(R.id.edit_career);
        edit_apartment = findViewById(R.id.edit_livein);

        edit_selfintroduction = findViewById(R.id.edit_selfintroduction);
        root_LinearLayout = findViewById(R.id.root_LinearLayout);
        mGuideLayout = findViewById(R.id.guide_layout);
        photo_count = findViewById(R.id.photo_count);
        scrollView = findViewById(R.id.scrollView);
        rel_livein = findViewById(R.id.rel_livein);
        rel_edit_slogan = findViewById(R.id.rel_edit_slogan);
        rel_career = findViewById(R.id.rel_career);
        rel_nearby_cover = findViewById(R.id.rel_nearby_cover);
        tv_photo_tips = findViewById(R.id.tv_photo_tips);
        //  remain_days = findViewById(R.id.remain_days);
    }

    private void initUserPic() {

        mGridLayoutManager = new GridLayoutManager(UpdateUserInfoActivity.this, 3);
        user_pic_rv.setLayoutManager(mGridLayoutManager);
        user_pic_rv.addItemDecoration(new GridSpacingItemDecoration(3, SizeUtils.dip2px(TheLApp.context, 4), false));
        mMyImageAdapter = new MyImageAdapter(UpdateUserInfoActivity.this);
        user_pic_rv.setAdapter(mMyImageAdapter);

        mMyImageAdapter.setOnDragItemClickListener(new BaseAdapter.OnDragItemClickListener() {
            @Override
            public void OnDragItemClick(RecyclerView.ViewHolder viewHolder) {

                L.d("UserInfoMoment", " viewHolder.getLayoutPosition() : " + viewHolder.getLayoutPosition());

                if (mMyImageAdapter.getData().size() <= 2) {
                    return;
                } else {
                    if (mMyImageAdapter.getData().size() - 1 == viewHolder.getLayoutPosition()) {
                        return;
                    }
                }

                if (mMyImageAdapter.getData().size() > 1) {
                    mItemTouchHelper.startDrag(viewHolder);

                    //获取系统震动服务
                    Vibrator vib = (Vibrator) getSystemService(Service.VIBRATOR_SERVICE);//震动70毫秒

                    if (vib != null) {
                        vib.vibrate(70);
                    }
                }
            }
        });

        mItemTouchHelper = new ItemTouchHelper(new ItemTouchHelper.Callback() {
            @Override
            public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {

                L.d("UserInfoMoment", " ------------getMovementFlags----------");

                if (viewHolder.getLayoutPosition() == mMyImageAdapter.getData().size() - 1) {
                    return ItemTouchHelper.ACTION_STATE_IDLE;
                }

                if (recyclerView.getLayoutManager() instanceof GridLayoutManager) {
                    final int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN | ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT;
                    final int swipeFlags = 0;
                    return makeMovementFlags(dragFlags, swipeFlags);
                } else {
                    final int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
                    final int swipeFlags = 0;
                    return makeMovementFlags(dragFlags, swipeFlags);
                }
            }

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {

                L.d("UserInfoMoment", " ------------onMove----------");

                //得到当拖拽的viewHolder的Position
                int fromPosition = viewHolder.getAdapterPosition();

                //拿到当前拖拽到的item的viewHolder
                int toPosition = target.getAdapterPosition();

                if (fromPosition < toPosition) {
                    for (int i = fromPosition; i < toPosition; i++) {
                        Collections.swap(mMyImageAdapter.getData(), i, i + 1);
                    }
                } else {
                    for (int i = fromPosition; i > toPosition; i--) {
                        Collections.swap(mMyImageAdapter.getData(), i, i - 1);
                    }
                }

                if (toPosition == mMyImageAdapter.getData().size() - 1) {
                    toPosition = mMyImageAdapter.getData().size() - 2;
                }

                mMyImageAdapter.notifyItemMoved(fromPosition, toPosition);

                int size;

                if (mMyImageAdapter.getData().size() < 9) {
                    size = mMyImageAdapter.getData().size() - 2;
                } else {
                    size = mMyImageAdapter.getData().size() - 1;
                }

                StringBuilder stringBuilder = new StringBuilder();

                for (int i = 0; i <= size; i++) {

                    MyImageBean myImageBean = mMyImageAdapter.getData().get(i);

                    if (myImageBean.picId != 0) {

                        stringBuilder.append(myImageBean.picId);
                        if (i != size) {
                            stringBuilder.append(",");
                        }
                    }

                }

                if (presenter != null) {
                    presenter.sortImages(stringBuilder.toString());
                }

                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

                L.d("UserInfoMoment", " ------------onSwiped----------");

            }

            /**
             * 重写拖拽可用
             * @return
             */
            @Override
            public boolean isLongPressDragEnabled() {
                return false;
            }
        });

        mItemTouchHelper.attachToRecyclerView(user_pic_rv);

    }


    private void getDataFromPrepage() {

        presenter.loadMyInfo();
        showLoading();

    }

    //初始化图片区域
    private void initImage() {
        cameraImageBean = new MyImageBean();
        cameraImageBean.isCameraBtnFlag = true;
        myImageslist.add(cameraImageBean);
        initUserPic();
        presenter.getMyImagesList("9", "1");
    }

    @Override
    protected void onResume() {
        if (needRefreshMyPhotos) {
            needRefreshMyPhotos = false;
            presenter.getMyImagesList("9", "1");
        }
        super.onResume();
        MobclickAgent.onPageStart(this.getClass().getName());
        MobclickAgent.onResume(this);
        // getText(root_LinearLayout);
    }

    private void initUI() {
        if (null != myInfoBean) {
            //头像
            if (!TextUtils.isEmpty(myInfoBean.avatar)) {
                img_avatar.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(myInfoBean.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE)));
            }
            //获取昵称
            if (TextUtils.isEmpty(myInfoBean.nickName)) {

            }
            edit_name.setText(myInfoBean.nickName + "");
            if (myInfoBean.nicknameModifyDayLeft != null) {
                String text = TheLApp.context.getString(R.string.rewrite_username, myInfoBean.nicknameModifyDayLeft);
                //   L.d("updateUserinfoActivityPrint", "text" + text);
/*
                String text1 = TheLApp.context.getString(R.string.rewrite_username);
                L.d("updateUserinfoActivityPrint", "text1" + text1);

                String text2 = text1.replace("%s", myInfoBean.nicknameModifyDayLeft);


                L.d("updateUserinfoActivityPrint", "text2" + text2);

                remain_days.setText(Html.fromHtml(text2));*/
                //  remain_days.setText(text);

            } else {
                //remain_days.setVisibility(View.GONE);

            }
            //获取年龄
            if (!TextUtils.isEmpty(myInfoBean.birthday)) {
                setBirthday(myInfoBean.birthday);
                edit_age.setText(getBirthdayStr(birthday));
                edit_age.setTextColor(ContextCompat.getColor(this, R.color.update_have_edited_text));

            } else {
                edit_age.setText(TheLApp.context.getString(R.string.update_birth_hint));
                edit_age.setTextColor(ContextCompat.getColor(this, R.color.tab_normal));

            }

            // 身高
            if (heightUnits == 0) {
                edit_height.setTextColor(ContextCompat.getColor(this, R.color.update_have_edited_text));

                if (!TextUtils.isEmpty(myInfoBean.height)) {
                    try {
                        int height = Float.valueOf(myInfoBean.height).intValue();
                        if (height <= 140) {
                            edit_height.setText("≤" + 140 + " cm");
                        } else if (height >= 201) {
                            edit_height.setText("≥" + 201 + " cm");
                        } else {
                            edit_height.setText(height + " cm");
                        }
                        int i = 0;
                        for (String h : height_list_cm) {
                            if (h.equals(height + " cm")) {
                                height_cm_cur_pos = i;
                                break;
                            }
                            if (i == height_list_cm.size() - 1 && h.equals("≥" + height + " cm")) {
                                height_cm_cur_pos = i;
                                break;
                            }
                            i++;
                        }
                    } catch (Exception e) {
                    }
                } else {
                    edit_height.setText(TheLApp.context.getString(R.string.your_height));
                    edit_height.setTextColor(ContextCompat.getColor(this, R.color.tab_normal));

                }
            } else {
                if (!TextUtils.isEmpty(myInfoBean.height)) {
                    try {
                        String height = Utils.cmToInches(myInfoBean.height);
                        if (Float.valueOf(myInfoBean.height) <= 140.0) {
                            edit_height.setText("≤" + Utils.cmToInches("140") + " Inches");
                        } else if (Float.valueOf(myInfoBean.height) >= 201.0) {
                            edit_height.setText("≥" + Utils.cmToInches("201") + " Inches");
                        } else {
                            edit_height.setText(height + " Inches");
                        }
                        int i = 0;
                        for (String h : height_list_inches) {
                            if (h.equals(height + " Inches")) {
                                height_inches_cur_pos = i;
                                break;
                            }
                            if (i == height_list_inches.size() - 1 && h.equals("≥" + height + " Inches")) {
                                height_inches_cur_pos = i;
                                break;
                            }
                            i++;
                        }
                    } catch (Exception e) {
                    }
                } else {
                    edit_height.setText(TheLApp.context.getString(R.string.your_height));
                    edit_height.setTextColor(ContextCompat.getColor(this, R.color.tab_normal));

                }
            }

            // 体重
            if (weightUnits == 0) {

                if (!TextUtils.isEmpty(myInfoBean.weight)) {
                    edit_weight.setTextColor(ContextCompat.getColor(this, R.color.update_have_edited_text));

                    try {
                        int weight = Float.valueOf(myInfoBean.weight).intValue();
                        if (weight <= 41) {
                            edit_weight.setText("≤" + 41 + " kg");
                        } else if (weight >= 80) {
                            edit_weight.setText("≥" + 80 + " kg");
                        } else {
                            edit_weight.setText(weight + " kg");
                        }
                        int i = 0;
                        for (String w : weight_list_kg) {
                            if (w.equals(weight + " kg")) {
                                weight_kg_cur_pos = i;
                                break;
                            }
                            if (i == weight_list_kg.size() - 1 && w.equals("≥" + weight + " kg")) {
                                weight_kg_cur_pos = i;
                                break;
                            }
                            i++;
                        }
                    } catch (Exception e) {
                    }
                } else {
                    edit_weight.setText(TheLApp.context.getString(R.string.your_weight));
                    edit_weight.setTextColor(ContextCompat.getColor(this, R.color.tab_normal));

                }
            } else {

                if (!TextUtils.isEmpty(myInfoBean.weight)) {
                    edit_weight.setTextColor(ContextCompat.getColor(this, R.color.update_have_edited_text));

                    try {
                        int weight = Float.valueOf(myInfoBean.weight).intValue();
                        String weightLbs = Utils.kgToLbs(myInfoBean.weight);
                        if (weight <= 41) {
                            edit_weight.setText("≤" + Utils.kgToLbs("41") + " Lbs");
                        } else if (weight >= 80) {
                            edit_weight.setText("≥" + Utils.kgToLbs("80") + " Lbs");
                        } else {
                            edit_weight.setText(weightLbs + " Lbs");
                        }
                        int i = 0;
                        for (String w : weight_list_lbs) {
                            if (w.equals(weightLbs + " Lbs")) {
                                weight_Lbs_cur_pos = i;
                                break;
                            }
                            if (i == weight_list_lbs.size() - 1 && w.equals("≥" + weightLbs + " Lbs")) {
                                weight_Lbs_cur_pos = i;
                                break;
                            }
                            i++;
                        }
                    } catch (Exception e) {
                    }
                } else {
                    edit_weight.setText(TheLApp.context.getString(R.string.your_weight));
                    edit_weight.setTextColor(ContextCompat.getColor(this, R.color.tab_normal));

                }
            }
            // 角色
            if (!TextUtils.isEmpty(myInfoBean.roleName)) {
                try {
                    role_cur_pos = Integer.parseInt(myInfoBean.roleName);
                    if (role_cur_pos < 0) {
                        edit_role.setText(TheLApp.context.getString(R.string.your_lable));
                        edit_role.setTextColor(ContextCompat.getColor(this, R.color.tab_normal));

                    } else {
                        if (role_cur_pos == 7 || role_cur_pos == 6) {
                            role_cur_pos = 5;
                        }
                        edit_role.setTextColor(ContextCompat.getColor(this, R.color.update_have_edited_text));

                    }
                } catch (Exception e) {
                    if (myInfoBean.roleName.equalsIgnoreCase("t")) {
                        role_cur_pos = 0;
                    } else if (myInfoBean.roleName.equalsIgnoreCase("p")) {
                        role_cur_pos = 1;
                    } else if (myInfoBean.roleName.equalsIgnoreCase("h")) {
                        role_cur_pos = 2;
                    } else if (myInfoBean.roleName.equalsIgnoreCase("bi")) {
                        role_cur_pos = 4;
                    } else if (myInfoBean.roleName.equalsIgnoreCase("other")) {
                        role_cur_pos = 5;
                    } else {
                        role_cur_pos = 0;
                    }
                }
                // 由于是写死在客户端本地的，所以如果拓展则无法兼容旧版，这边做个容错，给用户提示更新
                if (role_cur_pos >= role_list.size()) {
                    edit_role.setText("版本不兼容");
                    edit_role.setEnabled(false);
                } else {
                    RoleBean roleBean = roleMap.get(role_cur_pos);
                    if (roleBean != null) {
                        // role_cur_pos = roleBean.listPoi;
                        role_posiotn = roleBean.listPoi;
                        edit_role.setText(roleBean.contentRes);
                    }
                }
            } else {
                edit_role.setText(TheLApp.context.getString(R.string.your_lable));
                edit_role.setTextColor(ContextCompat.getColor(this, R.color.tab_normal));

            }
            // 感情状态
            if (!TextUtils.isEmpty(myInfoBean.affection)) {
                try {
                    relationship_cur_pos = Integer.parseInt(myInfoBean.affection);
                    if (relationship_cur_pos < 0) {
                        relationship_cur_pos = -1;
                        edit_relationship.setText(TheLApp.context.getString(R.string.your_ralationship_State));
                        edit_relationship.setTextColor(ContextCompat.getColor(this, R.color.tab_normal));

                    } else {
                        RoleBean relationshipBean = relationshipMap.get(relationship_cur_pos);
                        if (relationshipBean != null) {
                            relationship_role_posiotn = relationshipBean.listPoi;
                            edit_relationship.setText(relationshipBean.contentRes);

                        }
                        edit_relationship.setTextColor(ContextCompat.getColor(this, R.color.update_have_edited_text));

                    }

                } catch (Exception e) {
                }
            } else {
                edit_relationship.setText(TheLApp.context.getString(R.string.your_ralationship_State));
                edit_relationship.setTextColor(ContextCompat.getColor(this, R.color.tab_normal));

                relationship_cur_pos = -1;
            }

            // 寻找角色
            if (!TextUtils.isEmpty(myInfoBean.wantRole)) {
                edit_lookingfor_role.setTextColor(ContextCompat.getColor(this, R.color.update_have_edited_text));
                int otherRoleCount = 0;

                try {
                    String[] lookingForRole = myInfoBean.wantRole.split(",");
                    StringBuilder sb = new StringBuilder();// 寻找角色内容字符串拼接
                    lookingForRoleSB = new StringBuilder();
                    lookingForRoleSelectPositions.clear();
                    int size = lookingForRole.length;
                    ArrayList newRoleList = new ArrayList();

                    for (int i = 0; i < size; i++) {
                        int index = Integer.parseInt(lookingForRole[i]);
                        if ((index == 6 || index == 7 || index == 5)) {
                            if (otherRoleCount < 1) {
                                index = 5;
                                otherRoleCount++;
                            } else {
                                continue;
                            }
                        }
                        RoleBean roleBean = roleMap.get(index);
                        if (roleBean != null) {
                            int listPoi = roleBean.listPoi;
                            if (listPoi == 5) {
                                newRoleList.add(listPoi);
                            } else {

                            }
                            sb.append(role_list.get(listPoi));
                            lookingForRoleSB.append(index);
                            lookingForRoleSelectPositions.add(listPoi);

                        } else {

                        }
                        if (i != size - 1) {
                            sb.append(",");
                            lookingForRoleSB.append(",");
                        }
                    }
                    String content = sb.toString();
                    if (content != null && content.length() > 0) {
                        if (content.endsWith(",")) {
                            content = content.substring(0, content.length() - 1);
                        }
                        edit_lookingfor_role.setText(content);
                    }
                    //  edit_lookingfor_role.setText(sb.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                edit_lookingfor_role.setText(TheLApp.context.getString(R.string.your_faborat_label));
                edit_lookingfor_role.setTextColor(ContextCompat.getColor(this, R.color.tab_normal));
            }

            //4.5.0新用户在第一次注册完成后，可以修改一次RelaId
            if (myInfoBean.userNameRevisable == 1) {
                edit_user_name.setEnabled(true);
                edit_user_name.setText(myInfoBean.userName.trim());
                lin_user_id_tip.setVisibility(View.VISIBLE);

            } else {
                edit_user_name.setText(myInfoBean.userName.trim());
                edit_user_name.setEnabled(false);
                edit_user_name.setTextColor(ContextCompat.getColor(this, R.color.homepage_item_username));
                lin_user_id_tip.setVisibility(View.GONE);
            }
            /**
             * 4.7.0 复制用户的ID号
             * */
            if (!TextUtils.isEmpty(myInfoBean.userName)) {
                copy_user_id.setVisibility(View.VISIBLE);
                copy_user_line.setVisibility(View.VISIBLE);
            }

            // 职业
            if (TextUtils.isEmpty(myInfoBean.occupation)) {
                edit_career.setText(TheLApp.context.getString(R.string.occupation_type_hint));
                edit_career.setTextColor(ContextCompat.getColor(this, R.color.tab_normal));

            } else {
                edit_career.setText(myInfoBean.occupation);
                edit_career.setTextColor(ContextCompat.getColor(this, R.color.update_have_edited_text));

            }
            // 居住地
            if (TextUtils.isEmpty(myInfoBean.livecity)) {
                edit_apartment.setText(TheLApp.context.getString(R.string.updateuserinfo_live_place_hint));
                edit_apartment.setTextColor(ContextCompat.getColor(this, R.color.tab_normal));

            } else {
                edit_apartment.setText(myInfoBean.livecity);
                edit_apartment.setTextColor(ContextCompat.getColor(this, R.color.update_have_edited_text));

            }


            // 个性签名
            if (TextUtils.isEmpty(myInfoBean.intro)) {
                edit_selfintroduction.setText(TheLApp.context.getString(R.string.edit_intro));
                edit_selfintroduction.setTextColor(ContextCompat.getColor(this, R.color.tab_normal));

            } else {
                edit_selfintroduction.setText(myInfoBean.intro);
                edit_selfintroduction.setTextColor(ContextCompat.getColor(this, R.color.update_have_edited_text));
            }

            initMap(root_LinearLayout);
        }
    }

    private void setBirthday(String birth) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            birthday.setTime(sdf.parse(birth));
        } catch (Exception e) {
        }
    }

    /**
     * 格式：12岁,双子座
     *
     * @param birth
     * @return
     */
    private String getBirthdayStr(Calendar birth) {
        if (null == birth) {
            return "";
        }
        Calendar today = Calendar.getInstance();
        StringBuilder sb = new StringBuilder();
        try {
            int age = today.get(Calendar.YEAR) - birth.get(Calendar.YEAR);
            if (age < 18) {
                sb.append(18).append(getString(R.string.updatauserinfo_activity_age_unit)).append(", ").append(DateUtils.date2Constellation(birth));

            } else {
                sb.append(today.get(Calendar.YEAR) - birth.get(Calendar.YEAR)).append(getString(R.string.updatauserinfo_activity_age_unit)).append(", ").append(DateUtils.date2Constellation(birth));

            }
        } catch (Exception e) {
            return "";
        }

        return sb.toString();
    }

    /**
     * 初始化数据
     */
    private void initData() {
        String[] lookingfor_Array = this.getResources().getStringArray(R.array.userinfo_lookingfor);
        String[] ethnicity_Array = this.getResources().getStringArray(R.array.userinfo_ethnicity);
        String[] relationship_Array = this.getResources().getStringArray(R.array.userinfo_relationship);
        String[] role_Array = this.getResources().getStringArray(R.array.userinfo_role);
        String[] role_index_array = this.getResources().getStringArray(R.array.userinfo_role_index);
        String[] out_level_Array = this.getResources().getStringArray(R.array.out_level);
        String[] arr_tech = this.getResources().getStringArray(R.array.career_type_tech);
        String[] arr_culture = this.getResources().getStringArray(R.array.career_type_culture);
        String[] arr_entertainment = this.getResources().getStringArray(R.array.career_type_entertainment);
        String[] arr_economy = this.getResources().getStringArray(R.array.career_type_economy);
        String[] arr_manufacturing = this.getResources().getStringArray(R.array.career_type_manufacturing);
        String[] arr_transport = this.getResources().getStringArray(R.array.career_type_transport);
        String[] arr_service = this.getResources().getStringArray(R.array.career_type_service);
        String[] arr_utilities = this.getResources().getStringArray(R.array.career_type_utilities);
        String[] arr_student = this.getResources().getStringArray(R.array.career_type_student);
        String[] arr_other = new String[]{TheLApp.getContext().getString(R.string.info_other)};
        String[] arr_do_not_show = new String[]{TheLApp.getContext().getString(R.string.info_do_not_show)};
        careerTypeNames = new ArrayList<List<String>>();
        careerTypeNames.add(Arrays.asList(arr_tech));
        careerTypeNames.add(Arrays.asList(arr_culture));
        careerTypeNames.add(Arrays.asList(arr_entertainment));
        careerTypeNames.add(Arrays.asList(arr_economy));
        careerTypeNames.add(Arrays.asList(arr_manufacturing));
        careerTypeNames.add(Arrays.asList(arr_transport));
        careerTypeNames.add(Arrays.asList(arr_service));
        careerTypeNames.add(Arrays.asList(arr_utilities));
        careerTypeNames.add(Arrays.asList(arr_student));
        careerTypeNames.add(Arrays.asList(arr_other));
        careerTypeNames.add(Arrays.asList(arr_do_not_show));

        relationship_list = new ArrayList<String>(Arrays.asList(relationship_Array));
        role_list = new ArrayList<String>(Arrays.asList(role_Array));
        role_list_index = new ArrayList<>(Arrays.asList(role_index_array));
        // 组合身高cm的list
        for (int i = 140; i < 202; i++) {
            int height = i;
            if (i == 140) {
                height_list_cm.add("≤" + height + " cm");
            } else if (i == 201) {
                height_list_cm.add("≥" + height + " cm");
            } else {
                height_list_cm.add(height + " cm");
            }
        }

        // 组合身高inches的list
        String inchesStr = "";
        int feet = 4;
        int inches = 7;
        for (int i = 1; i < 4; i++) {
            for (int j = 1; j < 12; j++) {
                if (feet == 6 && inches >= 8) {
                    break;
                }
                if (inches >= 12) {
                    ++feet;
                    inchesStr = feet + "\'";
                    height_list_inches.add(inchesStr + " Inches");
                    inches = 1;
                    continue;
                }
                inchesStr = feet + "\'" + inches + "\"";
                if (feet == 4 && inches == 7) {
                    height_list_inches.add("≤" + inchesStr + " Inches");
                } else if (feet == 6 && inches == 7) {
                    height_list_inches.add("≥" + inchesStr + " Inches");
                } else {
                    height_list_inches.add(inchesStr + " Inches");
                }
                inches++;
            }
        }

        // 组合体重kg的list
        for (int i = 41; i < 81; i++) {
            int weight = i;
            if (i == 41) {
                weight_list_kg.add("≤" + weight + " kg");
            } else if (i == 80) {
                weight_list_kg.add("≥" + weight + " kg");
            } else {
                weight_list_kg.add(weight + " kg");
            }
        }

        // 组合体重Lbs的list
        for (int i = 90; i < 177; i++) {
            int weight = i;
            if (i == 90) {
                weight_list_lbs.add("≤" + weight + " Lbs");
            } else if (i == 176) {
                weight_list_lbs.add("≥" + weight + " Lbs");
            } else {
                weight_list_lbs.add(weight + " Lbs");
            }
        }

        getUnits();

        initRoleMap();

        initRelationshipMap();

    }

    private void initRoleMap() {
        roleMap.put(0, new RoleBean(5, TheLApp.context.getResources().getStringArray(R.array.userinfo_role)[5]));
        roleMap.put(1, new RoleBean(0, TheLApp.context.getResources().getStringArray(R.array.userinfo_role)[0]));
        roleMap.put(2, new RoleBean(1, TheLApp.context.getResources().getStringArray(R.array.userinfo_role)[1]));
        roleMap.put(3, new RoleBean(2, TheLApp.context.getResources().getStringArray(R.array.userinfo_role)[2]));
        roleMap.put(4, new RoleBean(3, TheLApp.context.getResources().getStringArray(R.array.userinfo_role)[3]));
        roleMap.put(5, new RoleBean(4, TheLApp.context.getResources().getStringArray(R.array.userinfo_role)[4]));


    }

    private void initRelationshipMap() {
        relationshipMap.put(1, new RoleBean(0, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[0]));
        relationshipMap.put(6, new RoleBean(1, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[1]));
        relationshipMap.put(7, new RoleBean(2, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[2]));
        relationshipMap.put(2, new RoleBean(3, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[3]));
        relationshipMap.put(3, new RoleBean(4, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[4]));
        relationshipMap.put(4, new RoleBean(5, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[5]));
        relationshipMap.put(5, new RoleBean(6, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[6]));
        relationshipMap.put(0, new RoleBean(7, TheLApp.context.getResources().getStringArray(R.array.userinfo_relationship)[7]));


    }

    private void getUnits() {
        weightUnits = ShareFileUtils.getInt(ShareFileUtils.WEIGHT_UNITS, 0); // 获取体重单位
        heightUnits = ShareFileUtils.getInt(ShareFileUtils.HEIGHT_UNITS, 0); // 获取身高单位
    }

    // 表单字段校验
    private boolean checkIsEmpty() {
        if (TextUtils.isEmpty(avatarUrl) && TextUtils.isEmpty(avatarPhotoPath)) {
            return true;

        } else if (TextUtils.isEmpty(edit_name.getText().toString().trim())) {
            DialogUtil.showToastShort(UpdateUserInfoActivity.this, TheLApp.getContext().getString(R.string.updatauserinfo_activity_input_nickname));
            return true;
        } else {
            return false;
        }
    }

    private void setListener() {
        img_avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                new ActionSheetDialog(UpdateUserInfoActivity.this).builder().setCancelable(true).setCanceledOnTouchOutside(true)
                        .addSheetItem(getString(R.string.change_avatar), ActionSheetDialog.SheetItemColor.BLACK, new ActionSheetDialog.OnSheetItemClickListener() {
                            @Override
                            public void onClick(int which) {
                                Intent intent = new Intent(UpdateUserInfoActivity.this, SelectLocalImagesActivity.class);
                                intent.putExtra("isAvatar", true);
                                startActivityForResult(intent, TheLConstants.BUNDLE_CODE_UPDATE_USER_INFO_ACTIVITY);
                            }
                        }).show();
            }
        });
        lin_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                if (showDialog(root_LinearLayout)) {
                    DialogUtil.showConfirmDialog(UpdateUserInfoActivity.this, "", getString(R.string.updatauserinfo_activity_quit_confirm), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            UpdateUserInfoActivity.this.finish();
                            ViewUtils.hideSoftInput(UpdateUserInfoActivity.this);

                        }
                    });
                } else {
                    UpdateUserInfoActivity.this.finish();
                    ViewUtils.hideSoftInput(UpdateUserInfoActivity.this);


                }
            }
        });

        lin_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                String userName = edit_user_name.getText().toString().trim();
                if (!hasUserName) {// 如果没有Lid才校验格式
                    // 校验用户名格式
                    if (!userName.matches("[_A-Za-z0-9]{6,20}")) {
                        DialogUtil.showToastShort(UpdateUserInfoActivity.this, getString(R.string.updatauserinfo_activity_username_illegal));
                        return;
                    }
                }
                if (!checkIsEmpty()) {
                    if (!hasUserName) {// 已经有用户名，则不校验
                        showLoadingNoBack();
                        presenter.checkUserName(userName.toLowerCase());
                    } else {
                        showLoadingNoBack();
                        submit();

                    }
                }

            }
        });
        rel_age.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                Calendar minDate = Calendar.getInstance();
                minDate.set(Calendar.YEAR, minDate.get(Calendar.YEAR) - 18);
                Calendar maxDate = Calendar.getInstance();
                maxDate.set(Calendar.YEAR, maxDate.get(Calendar.YEAR) - 100);
                dialogUtil.showDatePicker(UpdateUserInfoActivity.this, null, birthday, maxDate.getTimeInMillis(), minDate.getTimeInMillis(), true, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogUtil.closeDialog();
                        DatePicker datePicker = (DatePicker) view.getTag();
                        birthday.set(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth());
                        edit_age.setText(getBirthdayStr(birthday));
                        edit_age.setTextColor(ContextCompat.getColor(UpdateUserInfoActivity.this, R.color.update_have_edited_text));

                    }
                });
            }
        });
        //角色
        rel_role.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                editWhich = 5;
                //   displayNormalChoiseDialog(role_list, role_cur_pos);
                displayNormalChoiseRoleDialog(role_list, role_posiotn);
            }
        });
        // 寻找角色
        rel_lookingfor_role.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                //    displayMultiChoiseDialog(role_list, lookingForRoleSelectPositions, lookingForRoleTemplist, lookingForRoleSB, edit_lookingfor_role);
                editWhich = 7;

                displayMultiRoleChoiseDialog(role_list, lookingForRoleSelectPositions, lookingForRoleTemplist, lookingForRoleSB, edit_lookingfor_role);
            }
        });
        //身高
        rel_height.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                editWhich = 3;
                displayHeightUnitsChangeDialog();
            }
        });
        //体重
        rel_weight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                editWhich = 4;
                displayWeightUnitsChangeDialog();
            }
        });
        //感情状态
        rel_relationship.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 1000);
                editWhich = 6;
                displayNormalChoiseDialog(relationship_list, relationship_role_posiotn);
            }
        });

        //修改昵称
        rel_nickname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edit_name != null) {
                   /* edit_name.requestFocus();
                    ViewUtils.showSoftInput(UpdateUserInfoActivity.this, edit_name);*/
                    toEditNickName();
                }
            }
        });
        //填写详细自述
        rel_edit_slogan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent intent = new Intent(UpdateUserInfoActivity.this, EditUpdateUserInfoActivity.class);
                intent.putExtra("requestFrom", "intro");
                String content = edit_selfintroduction.getText().toString().trim();
                if (!content.equals(TheLApp.context.getString(R.string.edit_intro))) {
                    intent.putExtra("introContent", content);
                } else {
                    intent.putExtra("introContent", "");
                }
                startActivityForResult(intent, 1);
            }
        });

        /**
         * 跳转修改relaId
         * */
        edit_user_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent intent = new Intent(UpdateUserInfoActivity.this, EditUpdateUserInfoActivity.class);
                intent.putExtra("requestFrom", "RelaId");
                String content = edit_user_name.getText().toString().trim();
                intent.putExtra("introContent", content);
                startActivityForResult(intent, 2);
            }
        });
        /**
         * 跳转修改居住地
         * */
        rel_livein.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent intent = new Intent(UpdateUserInfoActivity.this, EditUpdateUserInfoActivity.class);
                intent.putExtra("requestFrom", "livecity");
                String livein = edit_apartment.getText().toString().trim();
                if (!TheLApp.context.getString(R.string.updateuserinfo_live_place_hint).equals(livein)) {
                    intent.putExtra(TheLConstants.BUNDLE_KEY_LIVEIN, livein);

                } else {
                    intent.putExtra(TheLConstants.BUNDLE_KEY_LIVEIN, "");

                }
                startActivityForResult(intent, 3);
            }
        });
        /***
         * 跳转修改自己的职业
         * */
        rel_career.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent intent = new Intent(UpdateUserInfoActivity.this, EditUpdateUserInfoActivity.class);
                intent.putExtra("requestFrom", "occupation");
                String career = edit_career.getText().toString().trim();
                if (!TheLApp.context.getString(R.string.occupation_type_hint).equals(career)) {
                    intent.putExtra(TheLConstants.BUNDLE_KEY_CAREER, career);

                } else {
                    intent.putExtra(TheLConstants.BUNDLE_KEY_CAREER, "");

                }
                startActivityForResult(intent, 4);
            }
        });

        /**
         * 上传封面
         */
        rel_nearby_cover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UpdateUserInfoActivity.this, CoverPreviewActivity.class);
                if (myInfoBean != null && myInfoBean.picUrl != null) {
                    intent.putExtra(TheLConstants.BUNDLE_KEY_COVER, myInfoBean.picUrl);
                }
                startActivityForResult(intent, TheLConstants.REQUEST_CODE_COVER);
            }
        });
        /**
         * 复制用户的id
         * */
        copy_user_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userName = edit_user_name.getText().toString().trim();
                DeviceUtils.copyToClipboard(UpdateUserInfoActivity.this, userName);
                DialogUtil.showToastShort(UpdateUserInfoActivity.this, getThelString(R.string.rela_id_had_copy));

            }
        });

    }

    private void toEditNickName() {
        final Intent intent = new Intent(this, EditMyNicknameActivity.class);
        final String nickname = edit_name.getText().toString();
        intent.putExtra(TheLConstants.BUNDLE_KEY_NICKNAME, nickname);
        intent.putExtra("requestFrom", "nickName");
        if (myInfoBean != null && myInfoBean.nicknameModifyDayLeft != null) {
            intent.putExtra("day", myInfoBean.nicknameModifyDayLeft);

        } else {
            intent.putExtra("day", "0");

        }
        startActivityForResult(intent, 0);
    }

    // 要上传的头像的图片的本地地址
    private String avatarPhotoPath = "";
    // 上传头像图片到七牛后的文件路径
    private String uploadAvatarPath = "";

    private void submit() {
        if (!TextUtils.isEmpty(avatarPhotoPath)) {// 上传头像
            uploadAvatarPath = RequestConstants.UPLOAD_FILE_ROOT_PATH_AVATAR + ShareFileUtils.getString(ShareFileUtils.ID, "") + "/" + MD5Utils.calculateMD5(new File(avatarPhotoPath)) + ".jpg";
            //  uploadAvatarRequestMD5 = requestBussiness.getUploadToken(new DefaultNetworkHelper(this), System.currentTimeMillis() + "", "", uploadAvatarPath);
            presenter.getUploadToken(System.currentTimeMillis() + "", "", uploadAvatarPath, "avatar");
        } else {
            submitUserInfo();
        }
    }

    private void submitUserInfo() {
        try {
            String userName = edit_user_name.getText().toString().trim();
            String nickname = edit_name.getText().toString().trim();
            String intro = edit_selfintroduction.getText().toString().trim();
            String livecity = edit_apartment.getText().toString().trim();
            String occupation = edit_career.getText().toString().trim();

            String birth = "";
            String age = edit_age.getText().toString().trim();


            if (!TextUtils.isEmpty(age) && birthday != null && !TheLApp.context.getString(R.string.update_birth_hint).equals(age)) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    birth = sdf.format(birthday.getTime());
                } catch (Exception e) {
                }
            } else {
                birth = "";
            }

            String height = edit_height.getText().toString().trim();
            if (!height.equals(TheLApp.context.getString(R.string.your_height)) && height.length() > 0) {
                if (heightUnits == 0) {
                    if (height.contains("≤") || height.contains("≥")) {
                        height = height.substring(1, height.indexOf(" cm"));
                    } else {
                        height = height.substring(0, height.indexOf(" cm"));
                    }
                } else {
                    if (height.contains("≤") || height.contains("≥")) {
                        height = height.substring(1, height.indexOf(" Inches"));
                    } else {
                        height = height.substring(0, height.indexOf(" Inches"));
                    }
                    height = Utils.inchesToCm(height);
                }
            } else {
                height = "";
            }


            String weight = edit_weight.getText().toString().trim();
            if (!weight.equals(TheLApp.context.getString(R.string.your_weight)) && weight.length() > 0) {
                if (weightUnits == 0) {
                    if (weight.contains("≤") || weight.contains("≥")) {
                        weight = weight.substring(1, weight.indexOf(" kg"));
                    } else {
                        weight = weight.substring(0, weight.indexOf(" kg"));
                    }
                } else {
                    if (weight.contains("≤") || weight.contains("≥")) {
                        weight = weight.substring(1, weight.indexOf(" Lbs"));
                    } else {
                        weight = weight.substring(0, weight.indexOf(" Lbs"));
                    }
                    weight = Utils.LbsTokg(weight);
                }
            } else {
                weight = "";
            }

            //职业
            if (TheLApp.context.getString(R.string.occupation_type_hint).equals(occupation)) {
                occupation = "";
            }

            //个性签名
            if (TheLApp.context.getString(R.string.edit_slogan).equals(intro)) {
                intro = "";
            }
            //所在地
            if (TheLApp.context.getString(R.string.updateuserinfo_live_place_hint).equals(livecity)) {
                livecity = "";
            }
            presenter.updateUserInfo(userName.toLowerCase(), birth, lookingForRoleSB.toString(), careerType + "", avatarUrl, nickname, height, weight, role_cur_pos + "", relationship_cur_pos + "", purposeSB.toString(), ethnicity_cur_pos == -1 ? "" : ethnicity_cur_pos + "", occupation, livecity, intro);
            updateMyInfoBean(userName.toLowerCase(), birth, lookingForRoleSB.toString(), careerType + "", avatarUrl, nickname, height, weight, role_cur_pos + "", relationship_cur_pos + "", purposeSB.toString(), ethnicity_cur_pos == -1 ? "" : ethnicity_cur_pos + "", occupation, livecity, intro);

        } catch (Exception e) {
            e.printStackTrace();
            closeLoading();
        }
    }

    private void updateMyInfoBean(String userName, String birthday, String wantRole, String professionalTypes, String avatarURL, String nickName, String height, String weight, String roleName, String relationship, String purpose, String ethnicity, String occupation, String liveCity, String intro) {
        myInfoBean.userName = userName;
        myInfoBean.birthday = birthday;
        myInfoBean.wantRole = wantRole;
        myInfoBean.professionalTypes = professionalTypes;
        myInfoBean.avatar = avatarURL;
        myInfoBean.nickName = nickName;
        myInfoBean.height = height;
        myInfoBean.weight = weight;
        myInfoBean.roleName = roleName;
        myInfoBean.affection = relationship;
        myInfoBean.purpose = purpose;
        myInfoBean.ethnicity = ethnicity;
        myInfoBean.occupation = occupation;
        myInfoBean.livecity = liveCity;
        myInfoBean.intro = intro;
        ShareFileUtils.saveUserData(myInfoBean);
    }

    /**
     * 显体重dialog
     */
    private void displayWeightUnitsChangeDialog() {
        ArrayList<String> list = new ArrayList<String>();
        final Dialog dialog = new Dialog(UpdateUserInfoActivity.this, R.style.CustomDialogBottom);
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
        WindowManager windowManager = getWindowManager();
        Display display = windowManager.getDefaultDisplay();
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.gravity = Gravity.BOTTOM;
        lp.width = display.getWidth(); // 设置宽度
        dialog.getWindow().setAttributes(lp);

        LayoutInflater flater = LayoutInflater.from(UpdateUserInfoActivity.this);
        View view = flater.inflate(R.layout.updateuserinfo_unitchange_dialog, null);
        dialog.setContentView(view);

        LinearLayout lin_next = view.findViewById(R.id.lin_next);
        ListView listview = view.findViewById(R.id.listview);
        LinearLayout lin_unitschange = view.findViewById(R.id.lin_unitschange);
        TextView txt_left = view.findViewById(R.id.txt_left);
        TextView txt_right = view.findViewById(R.id.txt_right);
        final ImageView img_middle = view.findViewById(R.id.img_middle);

        txt_left.setText("kg");
        txt_right.setText("Lbs");
        weightUnits = ShareFileUtils.getInt(ShareFileUtils.WEIGHT_UNITS, 0);
        final UpdataUserInfoDialogAdapter adapter;
        if (weightUnits == 0) {
            tempCurrent1 = weight_kg_cur_pos;
            list = weight_list_kg;
            img_middle.setImageResource(R.mipmap.btn_check_left);
            adapter = new UpdataUserInfoDialogAdapter(list, weight_kg_cur_pos);
            listview.setAdapter(adapter);
            listview.setSelection(weight_kg_cur_pos);
        } else {
            tempCurrent2 = weight_Lbs_cur_pos;
            list = weight_list_lbs;
            img_middle.setImageResource(R.mipmap.btn_check_right);
            adapter = new UpdataUserInfoDialogAdapter(list, weight_Lbs_cur_pos);
            listview.setAdapter(adapter);
            listview.setSelection(weight_Lbs_cur_pos);
        }
        listview.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (weightUnits == 0) {
                    tempCurrent1 = position;
                } else {
                    tempCurrent2 = position;
                }
                adapter.refreshList(position);
                adapter.notifyDataSetChanged();
            }
        });

        lin_unitschange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 500);
                weightUnits = weightUnits == 0 ? (weightUnits + 1) : (weightUnits - 1);
                if (weightUnits == 0) {
                    img_middle.setImageResource(R.mipmap.btn_check_left);
                    adapter.updataList(tempCurrent1, weight_list_kg);
                    adapter.notifyDataSetChanged();
                } else {
                    img_middle.setImageResource(R.mipmap.btn_check_right);
                    adapter.updataList(tempCurrent2, weight_list_lbs);
                    adapter.notifyDataSetChanged();
                }
            }
        });

        lin_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                dialog.dismiss();
                if (weightUnits == 0) {
                    weight_kg_cur_pos = tempCurrent1;
                    edit_weight.setText(weight_list_kg.get(weight_kg_cur_pos));
                    edit_weight.setTextColor(ContextCompat.getColor(UpdateUserInfoActivity.this, R.color.update_have_edited_text));

                    ShareFileUtils.setInt(ShareFileUtils.WEIGHT_UNITS, 0);
                } else {
                    weight_Lbs_cur_pos = tempCurrent2;
                    edit_weight.setText(weight_list_lbs.get(weight_Lbs_cur_pos));
                    edit_weight.setTextColor(ContextCompat.getColor(UpdateUserInfoActivity.this, R.color.update_have_edited_text));

                    ShareFileUtils.setInt(ShareFileUtils.WEIGHT_UNITS, 1);
                }
            }
        });
    }

    /**
     * 显示身高dialog
     */
    private void displayHeightUnitsChangeDialog() {
        ArrayList<String> list = new ArrayList<String>();
        final Dialog dialog = new Dialog(UpdateUserInfoActivity.this, R.style.CustomDialogBottom);
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
        WindowManager windowManager = getWindowManager();
        Display display = windowManager.getDefaultDisplay();
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.gravity = Gravity.BOTTOM;
        lp.width = display.getWidth(); // 设置宽度
        dialog.getWindow().setAttributes(lp);

        LayoutInflater flater = LayoutInflater.from(UpdateUserInfoActivity.this);
        View view = flater.inflate(R.layout.updateuserinfo_unitchange_dialog, null);
        dialog.setContentView(view);

        LinearLayout lin_next = view.findViewById(R.id.lin_next);
        ListView listview = view.findViewById(R.id.listview);
        LinearLayout lin_unitschange = view.findViewById(R.id.lin_unitschange);
        TextView txt_left = view.findViewById(R.id.txt_left);
        TextView txt_right = view.findViewById(R.id.txt_right);
        final ImageView img_middle = view.findViewById(R.id.img_middle);

        txt_left.setText("cm");
        txt_right.setText("Inches");
        heightUnits = ShareFileUtils.getInt(ShareFileUtils.HEIGHT_UNITS, 0);

        final UpdataUserInfoDialogAdapter adapter;
        if (heightUnits == 0) {
            tempCurrent1 = height_cm_cur_pos;
            list = height_list_cm;
            img_middle.setImageResource(R.mipmap.btn_check_left);
            adapter = new UpdataUserInfoDialogAdapter(list, height_cm_cur_pos);
            listview.setAdapter(adapter);
            listview.setSelection(height_cm_cur_pos);
        } else {
            tempCurrent2 = height_inches_cur_pos;
            list = height_list_inches;
            img_middle.setImageResource(R.mipmap.btn_check_right);
            adapter = new UpdataUserInfoDialogAdapter(list, height_inches_cur_pos);
            listview.setAdapter(adapter);
            listview.setSelection(height_inches_cur_pos);
        }
        listview.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (heightUnits == 0) {
                    tempCurrent1 = position;
                } else {
                    tempCurrent2 = position;
                }
                adapter.refreshList(position);
                adapter.notifyDataSetChanged();
            }
        });

        lin_unitschange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 500);
                heightUnits = heightUnits == 0 ? (heightUnits + 1) : (heightUnits - 1);
                if (heightUnits == 0) {
                    img_middle.setImageResource(R.mipmap.btn_check_left);
                    adapter.updataList(tempCurrent1, height_list_cm);
                } else {
                    img_middle.setImageResource(R.mipmap.btn_check_right);
                    adapter.updataList(tempCurrent2, height_list_inches);
                }
                adapter.notifyDataSetChanged();
            }
        });

        lin_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                dialog.dismiss();
                if (heightUnits == 0) {
                    height_cm_cur_pos = tempCurrent1;
                    edit_height.setText(height_list_cm.get(height_cm_cur_pos));
                    edit_height.setTextColor(ContextCompat.getColor(UpdateUserInfoActivity.this, R.color.update_have_edited_text));

                    ShareFileUtils.setInt(ShareFileUtils.HEIGHT_UNITS, 0);
                } else {
                    height_inches_cur_pos = tempCurrent2;
                    edit_height.setText(height_list_inches.get(height_inches_cur_pos));
                    edit_height.setTextColor(ContextCompat.getColor(UpdateUserInfoActivity.this, R.color.update_have_edited_text));

                    ShareFileUtils.setInt(ShareFileUtils.HEIGHT_UNITS, 1);
                }
            }
        });
    }

    //寻找角色
    private void displayMultiRoleChoiseDialog(final ArrayList<String> list, final ArrayList<Integer> selectPos, final ArrayList<Integer> tempList, final StringBuilder sb, final TextView editText) {
        final Dialog dialog = new Dialog(UpdateUserInfoActivity.this, R.style.CustomDialogBottom);
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
        WindowManager windowManager = getWindowManager();
        Display display = windowManager.getDefaultDisplay();
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.gravity = Gravity.BOTTOM;
        lp.width = display.getWidth(); // 设置宽度
        dialog.getWindow().setAttributes(lp);

        LayoutInflater flater = LayoutInflater.from(UpdateUserInfoActivity.this);
        View view = flater.inflate(R.layout.single_choice_dialog, null);
        dialog.setContentView(view);

        LinearLayout lin_next = view.findViewById(R.id.lin_next);
        ListView listview = view.findViewById(R.id.listview);
        TextView txt_title = view.findViewById(R.id.txt_title);
        txt_title.setText(TheLApp.getContext().getString(R.string.updatauserinfo_activity_dialog_title_multichoice));

        tempList.clear();
        tempList.addAll(selectPos);
        final UpdataUserInfoMultiDialogAdapter adapter = new UpdataUserInfoMultiDialogAdapter(list, selectPos);
        listview.setAdapter(adapter);
        listview.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (list == role_list && position == role_list.size() - 1) {// 选寻找角色时要特殊处理，选了第一项就清除其他项，选择了其他项要清除第一项
                    tempList.clear();
                    tempList.add(role_list.size() - 1);
                } else {
                    if (list == role_list) {// 选寻找角色时要特殊处理，选了第一项就清除其他项，选择了其他项要清除第一项
                        tempList.remove(Integer.valueOf(role_list.size() - 1));
                    }
                    boolean remove = false;
                    for (int i = 0; i < tempList.size(); i++) {
                        if (tempList.get(i) == position) {
                            tempList.remove(i);
                            remove = true;
                            break;
                        }
                    }
                    if (!remove) {
                        tempList.add(position);
                    }
                }
                adapter.refreshList(tempList);
                adapter.notifyDataSetChanged();
            }
        });

        lin_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                try {
                    dialog.dismiss();
                    selectPos.clear();
                    selectPos.addAll(tempList);
                    sb.delete(0, sb.length());

                    if (!selectPos.isEmpty()) {
                        Collections.sort(selectPos);
                        String select = "";
                        for (int i = 0; i < selectPos.size(); i++) {
                            int j = selectPos.get(i);
                            String data = role_list_index.get(j);
                            int parseInt = Integer.parseInt(data);
                            sb.append(parseInt);
                            // select += roleMap.get(parseInt);
                            RoleBean roleBean = roleMap.get(parseInt);
                            String contentRes = roleBean.contentRes;
                            if (contentRes != null) {
                                select += contentRes;
                            }
                            if (i != selectPos.size() - 1) {

                                select += ",";
                                sb.append(",");
                            }
                        }
                        editText.setText(select);
                        editText.setTextColor(ContextCompat.getColor(UpdateUserInfoActivity.this, R.color.update_have_edited_text));

                    } else {
                        editText.setText("");
                    }
                } catch (Exception e) {

                }
                //afterChoised();

            }
        });
    }

    private void displayMultiChoiseDialog(final ArrayList<String> list, final ArrayList<Integer> selectPos, final ArrayList<Integer> tempList, final StringBuilder sb, final TextView editText) {
        final Dialog dialog = new Dialog(UpdateUserInfoActivity.this, R.style.CustomDialogBottom);
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
        WindowManager windowManager = getWindowManager();
        Display display = windowManager.getDefaultDisplay();
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.gravity = Gravity.BOTTOM;
        lp.width = display.getWidth(); // 设置宽度
        dialog.getWindow().setAttributes(lp);

        LayoutInflater flater = LayoutInflater.from(UpdateUserInfoActivity.this);
        View view = flater.inflate(R.layout.single_choice_dialog, null);
        dialog.setContentView(view);

        LinearLayout lin_next = view.findViewById(R.id.lin_next);
        ListView listview = view.findViewById(R.id.listview);
        TextView txt_title = view.findViewById(R.id.txt_title);
        txt_title.setText(TheLApp.getContext().getString(R.string.updatauserinfo_activity_dialog_title_multichoice));

        tempList.clear();
        tempList.addAll(selectPos);
        final UpdataUserInfoMultiDialogAdapter adapter = new UpdataUserInfoMultiDialogAdapter(list, selectPos);
        listview.setAdapter(adapter);
        listview.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (list == role_list && position == role_list.size() - 1) {// 选寻找角色时要特殊处理，选了第一项就清除其他项，选择了其他项要清除第一项
                    tempList.clear();
                    tempList.add(role_list.size() - 1);
                } else {
                    if (list == role_list) {// 选寻找角色时要特殊处理，选了第一项就清除其他项，选择了其他项要清除第一项
                        tempList.remove(Integer.valueOf(role_list.size() - 1));
                    }
                    boolean remove = false;
                    for (int i = 0; i < tempList.size(); i++) {
                        if (tempList.get(i) == position) {
                            tempList.remove(i);
                            remove = true;
                            break;
                        }
                    }
                    if (!remove) {
                        tempList.add(position);
                    }
                }
                adapter.refreshList(tempList);
                adapter.notifyDataSetChanged();
            }
        });

        lin_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                dialog.dismiss();
                selectPos.clear();
                selectPos.addAll(tempList);
                sb.delete(0, sb.length());

                if (!selectPos.isEmpty()) {
                    Collections.sort(selectPos);
                    String select = "";
                    for (int i = 0; i < selectPos.size(); i++) {
                        int j = selectPos.get(i);
                        sb.append(j);
                        select += list.get(j);
                        if (i != selectPos.size() - 1) {
                            select += ",";
                            sb.append(",");
                        }
                    }
                    editText.setText(select);
                } else {
                    editText.setText("");
                }
            }
        });
    }


    // 普通dialog 单选
    private void displayNormalChoiseRoleDialog(ArrayList<String> list, int curPosition) {
        final Dialog dialog = new Dialog(UpdateUserInfoActivity.this, R.style.CustomDialogBottom);
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
        WindowManager windowManager = getWindowManager();
        Display display = windowManager.getDefaultDisplay();
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.gravity = Gravity.BOTTOM;
        lp.width = display.getWidth(); // 设置宽度
        dialog.getWindow().setAttributes(lp);

        LayoutInflater flater = LayoutInflater.from(UpdateUserInfoActivity.this);
        View view = flater.inflate(R.layout.single_choice_dialog, null);
        dialog.setContentView(view);

        LinearLayout lin_next = view.findViewById(R.id.lin_next);
        ListView listview = view.findViewById(R.id.listview);

        final UpdataUserInfoDialogAdapter adapter = new UpdataUserInfoDialogAdapter(list, curPosition);
        listview.setAdapter(adapter);
        listview.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listview.smoothScrollByOffset(curPosition);
        tempCurrent1 = curPosition;
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                tempCurrent1 = position;
              /*  String key = role_list_index.get(position);
                try {
                    int data = Integer.parseInt(key);
                    tempCurrent1 = data;

                } catch (Exception e) {
                    tempCurrent1 = position;
                    e.printStackTrace();
                }*/
                adapter.refreshList(position);
                adapter.notifyDataSetChanged();
            }
        });

        lin_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                dialog.dismiss();
                switch (editWhich) {
                    case 1:
                        break;
                    case 2:
                        break;
                    case 5:
                        role_posiotn = tempCurrent1;
                        String key = role_list_index.get(role_posiotn);
                        role_cur_pos = Integer.parseInt(key);
                        break;
                    case 8:
                        ethnicity_cur_pos = tempCurrent1;
                        break;
                    case 9:
                        break;
                }
                afterChoised();
            }
        });
    }

    // 普通dialog 单选  感情状态
    private void displayNormalChoiseDialog(ArrayList<String> list, int curPosition) {
        final Dialog dialog = new Dialog(UpdateUserInfoActivity.this, R.style.CustomDialogBottom);
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
        WindowManager windowManager = getWindowManager();
        Display display = windowManager.getDefaultDisplay();
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.gravity = Gravity.BOTTOM;
        lp.width = display.getWidth(); // 设置宽度
        dialog.getWindow().setAttributes(lp);

        LayoutInflater flater = LayoutInflater.from(UpdateUserInfoActivity.this);
        View view = flater.inflate(R.layout.single_choice_dialog, null);
        dialog.setContentView(view);

        LinearLayout lin_next = view.findViewById(R.id.lin_next);
        ListView listview = view.findViewById(R.id.listview);

        final UpdataUserInfoDialogAdapter adapter = new UpdataUserInfoDialogAdapter(list, curPosition);
        listview.setAdapter(adapter);
        listview.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listview.smoothScrollByOffset(curPosition);
        tempCurrent1 = curPosition;
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                tempCurrent1 = position;
                adapter.refreshList(position);
                adapter.notifyDataSetChanged();
            }
        });

        lin_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                dialog.dismiss();
                switch (editWhich) {
                    case 5:
                        role_cur_pos = tempCurrent1;
                        break;
                    case 6:
                        if (tempCurrent1 == 0) {
                            relationship_cur_pos = 1;
                        } else if (tempCurrent1 == 1) {
                            relationship_cur_pos = 6;
                        } else if (tempCurrent1 == 2) {
                            relationship_cur_pos = 7;

                        } else if (tempCurrent1 == 3) {
                            relationship_cur_pos = 2;

                        } else if (tempCurrent1 == 4) {
                            relationship_cur_pos = 3;

                        } else if (tempCurrent1 == 5) {
                            relationship_cur_pos = 4;

                        } else if (tempCurrent1 == 6) {
                            relationship_cur_pos = 5;

                        } else if (tempCurrent1 == 7) {
                            relationship_cur_pos = 0;

                        } else {
                            relationship_cur_pos = -1;

                        }

                     /*   role_posiotn = tempCurrent1;
                        String key = role_list_index.get(role_posiotn);
                        role_cur_pos = Integer.parseInt(key);*/
                        break;
                    case 8:
                        ethnicity_cur_pos = tempCurrent1;
                        break;
                    case 9:
                        break;
                }
                afterChoised();
            }
        });
    }

    private void afterChoised() {
        switch (editWhich) {
            case 1:
                //                edit_age.setText(age_list.get(age_cur_pos));
                //                edit_age.setTextColor(0xff232323);
                break;
            case 2:
                //                edit_horoscope.setText(constellation_list.get(horoscope_cur_pos) + "");
                //                edit_horoscope.setTextColor(0xff232323);
                break;
            case 5:
                if (role_posiotn >= 0) {
                    edit_role.setText(role_list.get(role_posiotn) + "");
                    edit_role.setTextColor(ContextCompat.getColor(this, R.color.update_have_edited_text));
                }
                break;
            case 6:
                if (relationship_cur_pos >= 0) {
                    edit_relationship.setText(relationship_list.get(tempCurrent1) + "");
                    edit_relationship.setTextColor(ContextCompat.getColor(this, R.color.update_have_edited_text));

                }
                break;

            case 8:

                break;
            case 9:

                break;
        }
    }

    @Override
    public void setPresenter(UpdateUserInfoContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void getMyInfo(MyInfoBean myInfoBean) {
        closeLoading();
        lin_back.setVisibility(View.VISIBLE);
        if (myInfoBean != null) {
            avatarUrl = myInfoBean.avatar;
            hasUserName = !TextUtils.isEmpty(myInfoBean.userName);
        }
        this.myInfoBean = myInfoBean;
        initUI();
    }

    @Override
    public void getUpdateUserInfo(UpdataInfoBean updataInfoBean) {
        MobclickAgent.onEvent(TheLApp.getContext(), "fill_personal_info_succeed");
        closeLoading();
        ShareFileUtils.saveUserData(myInfoBean);
        if (updataInfoBean.data != null) {
            SettingActivity.needRefreshMyRatio = true;
            ShareFileUtils.savaUserRatio(updataInfoBean.data.ratio);
            ShareFileUtils.setString(ShareFileUtils.NO_RATIO, String.valueOf(updataInfoBean.data.ratio));
        }
        ShareFileUtils.setBoolean(ShareFileUtils.NEED_COMPLETE_USER_INFO, false);
        setResult(RESULT_OK);

        //        DialogUtil.showToastShort(UpdateUserInfoActivity.this, TheLApp.getContext().getString(R.string.updatauserinfo_activity_updata_success));
        Intent intent = new Intent();
        intent.setAction(TheLConstants.BROADCAST_REFRESH_NEED_COMPLETE_USER_INFO);
        intent.setAction(TheLConstants.BROADCAST_UPDATA_RATIO);
        TheLApp.getContext().sendBroadcast(intent);
        UpdateUserInfoActivity.this.finish();
    }

    @Override
    public void refreshMyPhoto(MyImagesListBean myImagesListBean) {
        closeLoading();
        if (myImagesListBean.data != null && myImagesListBean.data.imagesList != null) {
            total = myImagesListBean.data.imagesList.size();
            if (total > 0) {
                setResult(RESULT_OK);
            }
            myImageslist.clear();

            try {
                for (int i = 0; i < total; i++) {
                    myImageslist.add(myImagesListBean.data.imagesList.get(i));
                }

                // 如果是9张，则不显示拍照按钮，否则要显示拍照按钮
                myImageslist.add(cameraImageBean);

                L.d("UpdateUserInfoActivity", " myImageslist.size() : " + myImageslist.size());

                if (myImageslist.size() > 0) {
                    String text = myImageslist.size() - 1 + "/9)";
                    photo_count.setText("(" + text);
                } else {
                    photo_count.setText("(0/9");

                }

                if (myImageslist.size() > 9) {
                    mMyImageAdapter.setData(myImageslist.subList(0, 9));

                } else {
                    mMyImageAdapter.setData(myImageslist);
                }


                if (myImagesListBean.data.imagesList.size() > 0) {

                    tv_photo_tips.setText(TheLApp.context.getString(R.string.long_press_drag_image_change));
                    boolean isShowGuide = ShareFileUtils.getBoolean(ShareFileUtils.SHOW_DRAG_PHOTO_GUIDE, false);

                    if (!isShowGuide) {

                        String content = TheLApp.context.getResources().getString(R.string.long_press_drag_image_change);

                        mGuideLayout.show(content);

                        ShareFileUtils.setBoolean(ShareFileUtils.SHOW_DRAG_PHOTO_GUIDE, true);
                    }
                } else {
                    tv_photo_tips.setText(TheLApp.context.getString(R.string.photo_tips));

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        user_pic_rv.post(new Runnable() {
            @Override
            public void run() {
                initMap(root_LinearLayout);

            }
        });

    }

    // 上传到相册图片到七牛后的文件路径
    private String uploadAlbumPath = "";

    private void uploadImage() {
        if (TextUtils.isEmpty(albumPhotoPath)) {
            return;
        }
        showLoadingNoBack();
        uploadAlbumPath = RequestConstants.UPLOAD_FILE_ROOT_PATH_ALBUM + ShareFileUtils.getString(ShareFileUtils.ID, "") + "/" + MD5Utils.calculateMD5(new File(albumPhotoPath)) + ".jpg";

        presenter.getUploadToken(System.currentTimeMillis() + "", "", uploadAlbumPath, "image");
    }

    @Override
    public void uploadToken(UploadTokenBean uploadTokenBean, String uploadType) {
        L.d("更改资料print拿到" + uploadTokenBean.data.toString() + uploadType);

        if (uploadTokenBean.data != null && !TextUtils.isEmpty(uploadTokenBean.data.uploadToken)) {
            UploadManager uploadManager = new UploadManager();
            if (uploadType.equals("video")) {//小视频
                if (!TextUtils.isEmpty(videoPath) && !TextUtils.isEmpty(uploadVideoPath)) {
                    uploadManager.put(new File(videoPath), uploadVideoPath, uploadTokenBean.data.uploadToken, new UpCompletionHandler() {
                        @Override
                        public void complete(String key, ResponseInfo responseInfo, JSONObject jsonObject) {
                            if (responseInfo != null && responseInfo.statusCode == 200 && uploadVideoPath.equals(key) && videoThumnail != null && uploadGifpath != null) {
                                setResult(RESULT_OK);
                                presenter.getMyImagesList("9", "1");
                            } else {
                                requestFailed();
                            }
                        }
                    }, null);
                } else {
                    requestFailed();
                }
            } else if (uploadType.equals("image")) {//相册
                if (!TextUtils.isEmpty(uploadAlbumPath) && !TextUtils.isEmpty(albumPhotoPath)) {
                    uploadManager.put(new File(albumPhotoPath), uploadAlbumPath, uploadTokenBean.data.uploadToken, new UpCompletionHandler() {
                        @Override
                        public void complete(String key, ResponseInfo info, JSONObject response) {
                            if (info != null && info.statusCode == 200 && uploadAlbumPath.equals(key)) {
                                List<Integer> wh = ImageUtils.measurePicWidthAndHeight(albumPhotoPath);
                                if (wh.size() == 2) {
                                    presenter.uploadImageUrl(RequestConstants.FILE_BUCKET + key, wh.get(0) + "", wh.get(1) + "");
                                }
                            } else {

                            }
                        }
                    }, null);
                } else {
                    requestFailed();
                }
            } else if (uploadType.equals("avatar")) {//头像
                L.d("更改资料print拿到-----" + uploadTokenBean.data.toString() + uploadType);

                if (!TextUtils.isEmpty(uploadAvatarPath) && !TextUtils.isEmpty(avatarPhotoPath)) {
                    uploadManager.put(new File(avatarPhotoPath), uploadAvatarPath, uploadTokenBean.data.uploadToken, new UpCompletionHandler() {
                        @Override
                        public void complete(String key, ResponseInfo info, JSONObject response) {
                            if (info != null && info.statusCode == 200 && uploadAvatarPath.equals(key)) {
                                avatarUrl = RequestConstants.FILE_BUCKET + key;
                                avatarPhotoPath = "";// 把base64Image设为空，以免在上传图片成功但是其它接口错误后，再次提交重复上传
                                L.d("更改资料print拿到======" + uploadTokenBean.data.toString() + uploadType);

                                submitUserInfo();
                            } else {
                                requestFailed();
                            }
                        }
                    }, null);
                } else {
                    requestFailed();
                }
            }
        }

    }

    private void requestFailed() {
        closeLoading();
        DialogUtil.showToastShort(this, getString(R.string.message_network_error));
    }

    /**
     * 增加图片
     *
     * @param updataInfoBean
     */
    @Override
    public void addImage(UpdataInfoBean updataInfoBean) {
        setResult(RESULT_OK);
        if (updataInfoBean.data != null) {
            SettingActivity.needRefreshMyRatio = true;
            ShareFileUtils.savaUserRatio(updataInfoBean.data.ratio);
            Intent intent = new Intent();
            intent.setAction(TheLConstants.BROADCAST_UPDATA_RATIO);
            TheLApp.getContext().sendBroadcast(intent);
        }
        presenter.getMyImagesList("9", "1");
    }

    @Override
    public void closeLoadingDialog() {
        closeLoading();
    }

    @Override
    public void checkUserNameResult(CheckUserNameBean data) {
        if (data.data != null && data.data.exists) {
            DialogUtil.showToastShort(this, getString(R.string.updatauserinfo_activity_username_exists));
            closeLoading();
            return;
        }

        submit();
    }

    @Override
    public void sortImagesSuccess() {
        //        presenter.getMyImagesList("9", "1");
        if (mMyImageAdapter != null) {
            mMyImageAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void popAlerSensitiveWords(String errdesc) {
        onRequestFinished();
        DialogUtil.showAlert(this, "", errdesc, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onRequestFinished() {
        closeLoading();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (showDialog(root_LinearLayout)) {
                DialogUtil.showConfirmDialog(UpdateUserInfoActivity.this, "", getString(R.string.updatauserinfo_activity_quit_confirm), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        UpdateUserInfoActivity.this.finish();
                    }
                });
            } else {
                UpdateUserInfoActivity.this.finish();

            }
        }
        return super.onKeyDown(keyCode, event);
    }

    private void handlePhoto(String imagePath) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true; // 只读入图边界
        BitmapFactory.decodeFile(imagePath, options);

        int zoom = 1;
        int quality = 100;

        // 先判断照片的横竖
        boolean isLandscape = options.outWidth > options.outHeight;
        if (isLandscape) {
            zoom = (int) (options.outWidth / (float) 200);
        } else {
            zoom = (int) (options.outHeight / (float) 200);
        }

        if (zoom < 1) {
            avatarPhotoPath = imagePath;
        } else {
            options.inSampleSize = zoom;
            options.inJustDecodeBounds = false; // 读取全部图信息
            Bitmap bitmap = BitmapFactory.decodeFile(imagePath, options); // 图片的压缩
            String handlePhotoPath = imagePath;
            try {
                File file = new File(handlePhotoPath);
                BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
                bitmap.compress(Bitmap.CompressFormat.JPEG, quality, bos);
                bos.flush();
                bos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            avatarPhotoPath = handlePhotoPath;
        }
        // 置为非空
        avatarUrl = " ";
    }

    private String takePhotoPath;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        L.d("onActivityResult", " requestCode " + requestCode);

        L.d("onActivityResult", " resultCode " + resultCode);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case TheLConstants.REQUEST_CODE_COVER:
                    String cover_url = data.getStringExtra(TheLConstants.BUNDLE_KEY_COVER);
                    if (!TextUtils.isEmpty(cover_url)) {
                        if (myInfoBean != null) {
                            myInfoBean.picUrl = data.getStringExtra(TheLConstants.BUNDLE_KEY_COVER);
                        }
                    }
                    break;
                case DeviceUtils.CUT_PHOTO://头像拍摄照片
                    String takePhotoPath = data.getStringExtra(TheLConstants.BUNDLE_KEY_LOCAL_IMAGE_PATH);

                    if (!TextUtils.isEmpty(takePhotoPath)) {
                        avatarPhotoPath = takePhotoPath;
                        img_avatar.setImageURI(Uri.parse(TheLConstants.FILE_PIC_URL + takePhotoPath));
                    } else {
                        Toast.makeText(UpdateUserInfoActivity.this, TheLApp.getContext().getString(R.string.info_rechoise_photo), Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        } else if (TheLConstants.RESULT_CODE_EDIT_NICKNAME == resultCode) {
            final String nickname = data.getStringExtra(TheLConstants.BUNDLE_KEY_NICKNAME);
            edit_name.setText(nickname);
        } else if (TheLConstants.RESULT_CODE_EDIT_RELAID == resultCode) {
            final String relaid = data.getStringExtra(TheLConstants.BUNDLE_KEY_RELAID);
            edit_user_name.setText(relaid);
            lin_user_id_tip.setVisibility(View.GONE);
        } else if (TheLConstants.RESULT_CODE_EDIT_INTRO == resultCode) {
            String intro = data.getStringExtra(TheLConstants.BUNDLE_KEY_INTROCONTENT);
            if (TextUtils.isEmpty(intro)) {
                edit_selfintroduction.setText(TheLApp.context.getString(R.string.edit_intro));
                edit_selfintroduction.setTextColor(ContextCompat.getColor(this, R.color.tab_normal));

            } else {
                edit_selfintroduction.setText(intro);
                edit_selfintroduction.setTextColor(ContextCompat.getColor(this, R.color.update_have_edited_text));
            }


        } else if (TheLConstants.RESULT_CODE_EDIT_LIVEIN == resultCode) {    //居住地
            String liveCity = data.getStringExtra(TheLConstants.BUNDLE_KEY_LIVEIN);

            if (TextUtils.isEmpty(liveCity)) {
                edit_apartment.setText(TheLApp.context.getString(R.string.updateuserinfo_live_place_hint));
                edit_apartment.setTextColor(ContextCompat.getColor(this, R.color.tab_normal));

            } else {
                edit_apartment.setText(liveCity);
                edit_apartment.setTextColor(ContextCompat.getColor(this, R.color.update_have_edited_text));

            }

        } else if (TheLConstants.RESULT_CODE_EDIT_CAREER == resultCode) {    //职业
            String career = data.getStringExtra(TheLConstants.BUNDLE_KEY_CAREER);

            if (TextUtils.isEmpty(career)) {
                edit_career.setText(TheLApp.context.getString(R.string.occupation_type_hint));
                edit_career.setTextColor(ContextCompat.getColor(this, R.color.tab_normal));

            } else {
                edit_career.setText(career);
                edit_career.setTextColor(ContextCompat.getColor(this, R.color.update_have_edited_text));

            }

        } else if (resultCode == TheLConstants.RESULT_CODE_TAKE_PHOTO || resultCode == TheLConstants.RESULT_CODE_SELECT_LOCAL_IMAGE) {
            if (requestCode == TheLConstants.BUNDLE_CODE_UPLOAD_IMAGE_ACTIVITY) {// 上传图片
                String path = data.getStringExtra(TheLConstants.BUNDLE_KEY_IMAGE_OUTPUT_PATH);
                if (!TextUtils.isEmpty(path)) {
                    albumPhotoPath = ImageUtils.handlePhoto1(path, TheLConstants.F_TAKE_PHOTO_ROOTPATH, ImageUtils.getPicName(), true, TheLConstants.MAX_PIC_WIDTH, TheLConstants.MAX_PIC_HEIGHT, TheLConstants.PIC_QUALITY, 3);
                    uploadImage();
                } else {
                    Toast.makeText(this, TheLApp.getContext().getString(R.string.info_rechoise_photo), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }


    @Override
    public void onClick(View view) {
        ViewUtils.preventViewMultipleClick(view, 1000);
        int viewId = view.getId();
        switch (viewId) {
            // 上传照片
            case R.id.img_thumb:
                showSelectedDialog();//显示功能选择对话框(具有上传视频功能)
                break;
        }
    }

    /**
     * 显示功能选择对话框，上传图片或者视频
     */
    private void showSelectedDialog() {
        new ActionSheetDialog(this).builder().setCancelable(true).setCanceledOnTouchOutside(true).setTitle(this.getString(R.string.add_selfie)).addSheetItem(getString(R.string.chat_activity_photo), ActionSheetDialog.SheetItemColor.BLACK, new ActionSheetDialog.OnSheetItemClickListener() {
            @Override
            public void onClick(int which) {
                startActivityForResult(new Intent(UpdateUserInfoActivity.this, SelectLocalImagesActivity.class), TheLConstants.BUNDLE_CODE_UPLOAD_IMAGE_ACTIVITY);
            }
        }).addSheetItem(getString(R.string.videos), ActionSheetDialog.SheetItemColor.BLACK, new ActionSheetDialog.OnSheetItemClickListener() {
            @Override
            public void onClick(int which) {
                PermissionUtil.requestStoragePermission(UpdateUserInfoActivity.this, new PermissionUtil.PermissionCallback() {
                    @Override
                    public void granted() {
                        RelaVideoSDK.startVideoGalleryActivity(UpdateUserInfoActivity.this, new onRelaVideoActivityResultListener() {
                            @Override
                            public void onRelaVideoActivityResult(Intent intent) {
                                Bundle bundle = intent.getExtras();
                                videoPath = bundle.getString(video.com.relavideolibrary.Utils.Constant.BundleConstants.RESULT_VIDEO_PATH);
                                videoThumnail = bundle.getString(video.com.relavideolibrary.Utils.Constant.BundleConstants.RESULT_VIDEO_THUMB);
                                uploadVideo(videoPath, videoThumnail);//上传视频路径到服务器
                            }
                        });
                    }

                    @Override
                    public void denied() {
                    }

                    @Override
                    public void gotoSetting() {
                    }
                });
            }
        }).show();
    }

    /**
     * 上传视频路径到服务器,获取key
     *
     * @param videoPath     本地视频地址
     * @param videoThumnail 视频缩略图
     */
    private void uploadVideo(String videoPath, String videoThumnail) {
        showLoading();
        String fileType = videoPath.substring(videoPath.lastIndexOf("."));//文件类型
        uploadVideoPath = RequestConstants.UPLOAD_FILE_ROOT_PATH_ALBUM + ShareFileUtils.getString(ShareFileUtils.ID, "") + "/" + MD5Utils.calculateMD5(new File(videoPath)) + fileType;//上传地址
        uploadGifpath = RequestConstants.UPLOAD_FILE_ROOT_PATH_ALBUM_GIF + ShareFileUtils.getString(ShareFileUtils.ID, "") + "/" + MD5Utils.calculateMD5(new File(videoPath)) + ".gif";
        if (uploadVideoPath != null && videoThumnail != null && uploadGifpath != null) {
            List<Integer> wh = ImageUtils.measurePicWidthAndHeight(videoThumnail);
            if (wh.size() == 2) {
                Flowable<UploadVideoAlbumBean> flowable = RequestBusiness.getInstance().uploadAlbumVideoUrl(wh.get(0) + "", wh.get(1) + "", RequestConstants.VIDEO_BUCKET + uploadVideoPath);
                flowable.onBackpressureDrop().onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<UploadVideoAlbumBean>() {

                    @Override
                    public void onNext(UploadVideoAlbumBean baseDataBean) {
                        super.onNext(baseDataBean);
                        presenter.getVideoUploadToken(System.currentTimeMillis() + "", RequestConstants.QINIU_BUCKET_VIDEO, uploadVideoPath, uploadGifpath, "album", baseDataBean.data.picId);
                    }

                    @Override
                    public void onError(Throwable t) {
                        closeLoading();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
            }
        }
    }

}
