package com.thel.modules.live.in;

import com.thel.modules.live.bean.LivePkGemNoticeBean;
import com.thel.modules.live.bean.LivePkHangupBean;
import com.thel.modules.live.bean.LivePkInitBean;
import com.thel.modules.live.bean.LivePkStartNoticeBean;
import com.thel.modules.live.bean.LivePkSummaryNoticeBean;

/**
 * Created by waiarl on 2017/12/13.
 * 直播观看端Pk view
 */

public interface LiveShowViewPkIn {

    /**
     * 显示直播PK开始view
     *
     * @param pkStartNoticeBean
     */
    void showPkStartView(LivePkStartNoticeBean pkStartNoticeBean);

    /**
     * 显示直播总结view
     *
     * @param pkSummaryNoticeBean
     */
    void showPkSummaryView(LivePkSummaryNoticeBean pkSummaryNoticeBean);

    /**
     * 显示直播关闭view
     *
     * @param pkStopPayload
     */
    void showPkStopView(String pkStopPayload);

    /**
     * 显示直播收到软妹币变化
     *
     * @param pkGemNoticeBean
     */
    void showPkGemView(LivePkGemNoticeBean pkGemNoticeBean);


    /**
     * pk断线重连的init消息
     *
     * @param livePkInitBean
     */
    void showLivePkInitView(LivePkInitBean livePkInitBean);


    /**
     * 显示直播挂断view
     *
     * @param pkHangupBean
     */
    void showPkHangupView(LivePkHangupBean pkHangupBean);
}
