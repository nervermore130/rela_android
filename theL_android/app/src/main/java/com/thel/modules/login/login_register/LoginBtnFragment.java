package com.thel.modules.login.login_register;


import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.utils.SizeUtils;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class LoginBtnFragment extends Fragment {

    public static final String WECHAT = "wechat";
    public static final String FACE_BOOK = "facebook";
    public static final String EMAIL = "email";
    public static final String TIPS = "tips";

    private boolean wxVisible;
    private boolean fbVisible;
    private boolean emailVisible;
    private String tips;

    @BindView(R.id.txt_others)
    TextView tv_tips;

    @BindView(R.id.button_container)
    LinearLayout button_container;

    public LoginBtnFragment() {
        // Required empty public constructor
    }

    public static LoginBtnFragment newInstance(String tips, boolean wechat, boolean facebook, boolean email) {
        LoginBtnFragment fragment = new LoginBtnFragment();
        Bundle bundle = new Bundle(4);
        bundle.putBoolean(WECHAT, wechat);
        bundle.putBoolean(FACE_BOOK, facebook);
        bundle.putBoolean(EMAIL, email);
        bundle.putString(TIPS, tips);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            Bundle bundle = getArguments();
            wxVisible = bundle.getBoolean(WECHAT);
            fbVisible = bundle.getBoolean(FACE_BOOK);
            emailVisible = bundle.getBoolean(EMAIL);
            tips = bundle.getString(TIPS);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login_btn, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        tv_tips.setText(tips);
        initLoginWayButton();
    }

    private void initLoginWayButton() {
        if (wxVisible) {
            FrameLayout frameLayout = new FrameLayout(getActivity());
            frameLayout.setId(R.id.wechat_container);
            FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
            fragmentTransaction.add(frameLayout.getId(), WechatFragment.newInstance(0));
            fragmentTransaction.commit();
            button_container.addView(frameLayout);
        }

        if (fbVisible) {
            FrameLayout frameLayout = new FrameLayout(getActivity());
            frameLayout.setId(R.id.facebook_container);
            FragmentTransaction fragmentTransaction1 = getChildFragmentManager().beginTransaction();
            fragmentTransaction1.add(frameLayout.getId(), FacebookFragment.newInstance(0));
            fragmentTransaction1.commit();
            button_container.addView(frameLayout);
            //设置间距
            if (wxVisible) {
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.setMargins(SizeUtils.dip2px(TheLApp.context, 25), 0, 0, 0);
                frameLayout.setLayoutParams(layoutParams);
            }
        }

        if (emailVisible) {
            FrameLayout frameLayout = new FrameLayout(getActivity());
            frameLayout.setId(R.id.email_container);
            FragmentTransaction fragmentTransaction2 = getChildFragmentManager().beginTransaction();
            fragmentTransaction2.add(frameLayout.getId(), new EmailFragment());
            fragmentTransaction2.commit();
            button_container.addView(frameLayout);
            if (wxVisible || fbVisible) {
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.setMargins(SizeUtils.dip2px(TheLApp.context, 25), 0, 0, 0);
                frameLayout.setLayoutParams(layoutParams);
            }
        }
    }

}
