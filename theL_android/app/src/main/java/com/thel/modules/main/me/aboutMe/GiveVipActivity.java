package com.thel.modules.main.me.aboutMe;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.base.BaseDataBean;
import com.thel.bean.ContactBean;
import com.thel.constants.TheLConstants;
import com.thel.growingio.GIoGiftTrackBean;
import com.thel.growingio.GrowingIoConstant;
import com.thel.imp.gold.GoldChangedListener;
import com.thel.imp.gold.GoldUtils;
import com.thel.modules.main.me.bean.VipBean;
import com.thel.modules.main.me.bean.VipListBean;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;
import com.thel.ui.widget.recyclerview.decoration.DefaultItemDivider;
import com.thel.utils.DialogUtil;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.L;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.thel.utils.StringUtils.getString;

/**
 * 赠送Vip
 * Created by lingwei on 2017/10/25.
 */

public class GiveVipActivity extends BaseActivity implements GoldChangedListener {
    private boolean is_give_vip;
    private ContactBean friend;
    private String say_something;
    private String friend_userid;
    private boolean hasLoadedData = false;//是否有数据
    private ArrayList<VipBean> listPlus = new ArrayList<>();
    private int normalPrice = 1;//赠送会员一般价格，即只购买一个月的时候的价格
    private LinearLayout lin_back;
    private TextView txt_title;
    private TextView txt_balance;
    private TextView txt_recharge;
    private RecyclerView mRecyclerview;
    private LinearLayout lin_reload;
    private ImageView img_reload;
    private TextView txt_reload;
    private VipListBean vipListBean;
    private GiveVipListAdapter adapter;
    private int operatePos = -1;//操作的item postion
    private static final int RECHARGE_CODE = 1;//充值的请求requestcode
    private GoldUtils goldUtils;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.give_vip_activity);
        getIntentData();
        findViewById();
        setListener();
        processBusiness();
        goldUtils = new GoldUtils();
        goldUtils.registerReceiver(this);
    }

    private void findViewById() {
        lin_back = findViewById(R.id.lin_back);
        txt_title = findViewById(R.id.txt_title);
        txt_balance = findViewById(R.id.txt_balance);
        txt_recharge = findViewById(R.id.txt_recharge);
        mRecyclerview = findViewById(R.id.give_vip_recyclerview);
        mRecyclerview.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mRecyclerview.addItemDecoration(new DefaultItemDivider(this, LinearLayoutManager.VERTICAL, R.color.gray, 1, false, true));
        lin_reload = this.findViewById(R.id.lin_reload);
        img_reload = this.findViewById(R.id.img_reload);
        txt_reload = this.findViewById(R.id.txt_reload);
        txt_title.setText(getString(R.string.give_vip));//赠送会员
    }

    private void processBusiness() {
        Flowable<VipListBean> flowable = RequestBusiness.getInstance().getBuyVipList("");
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<VipListBean>() {
            @Override
            public void onNext(VipListBean vipListBean) {
                super.onNext(vipListBean);
                getBuyVipList(vipListBean);
            }

            @Override
            public void onError(Throwable t) {
                L.d(" onError : " + t.getMessage());
            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void setListener() {
        txt_recharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoRecharge();
            }
        });
        lin_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void getBuyVipList(VipListBean vipListBean) {
        if (vipListBean.data != null) {
            this.vipListBean = vipListBean;
            hasLoadedData = true;
            listPlus.clear();
            listPlus.addAll(vipListBean.data.list);
            requestFinished();
        }

    }

    private void getIntentData() {
        Bundle bd = getIntent().getExtras();
        if (bd != null) {
            if (is_give_vip = bd.getBoolean(TheLConstants.BUNDLE_KEY_GIVE_VIP, false)) {
                friend = (ContactBean) bd.getSerializable(TheLConstants.BUNDLE_KEY_FRIEND);
                say_something = bd.getString(TheLConstants.BUNDLE_KEY_SAY_SOMETHING);
                friend_userid = friend.userId;
                ((TextView) findViewById(R.id.txt_title)).setText(getString(R.string.give_vip));
            }
        }
    }

    private void requestFinished() {
        closeLoading();
        refreshUi();
    }

    /**
     * 更新Ui
     */
    private void refreshUi() {
        if (hasLoadedData) {
            getNormalPrice();
            lin_reload.setVisibility(View.GONE);
            if (vipListBean.data.gold > 1) {//单复数
                txt_balance.setText(getString(R.string.soft_money, vipListBean.data.gold));
            } else {
                txt_balance.setText(getString(R.string.soft_moneys, vipListBean.data.gold));
            }
            if (adapter == null) {
                adapter = new GiveVipListAdapter(listPlus, normalPrice);
                mRecyclerview.setAdapter(adapter);
                adapter.setOnRecyclerViewItemChildClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemChildClickListener() {
                    @Override
                    public void onItemChildClick(BaseRecyclerViewAdapter adapter, View view, int position) {
                        switch (view.getId()) {
                            case R.id.txt_operation://点击某个item中赠送
                                operatePos = (int) view.getTag() - adapter.getHeaderLayoutCount();
                                if (operatePos >= 0) {
                                    sendVip(listPlus.get(operatePos));
                                }
                                break;
                        }
                    }
                });
            } else {
                adapter.notifyDataSetChanged();
            }
        } else {
            loadFailed();
        }
    }

    private void loadFailed() {
        try {
            lin_reload.setVisibility(View.VISIBLE);
            img_reload.clearAnimation();
            txt_reload.setText(getString(R.string.info_reload));
        } catch (Exception e) {
        }
    }

    /**
     * 赠送会员
     *
     * @param vipBean 要赠送的会员的种类
     */
    private void sendVip(VipBean vipBean) {
        if (vipBean.goldPrice > vipListBean.data.gold) {//余额不足
            showRechargeDialog();
        } else {
            showLoadingNoBack();
            postBuyWithGold(TheLConstants.PRODUCT_TYPE_VIP, vipBean.id, friend_userid, say_something);
        }
    }

    private void postBuyWithGold(final String productTypeVip, final long id, String friend_userid, String say_something) {
        Flowable<BaseDataBean> flowable = RequestBusiness.getInstance().buyWithGold(productTypeVip, id, friend_userid, say_something);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<BaseDataBean>() {
            @Override
            public void onNext(BaseDataBean baseDataBean) {
                super.onNext(baseDataBean);
                getResult(baseDataBean);
                if (baseDataBean.errcode.equals("not_enough_gold")) {//软妹币不足

                } else {
                    final GIoGiftTrackBean payTrackBean = new GIoGiftTrackBean(GrowingIoConstant.MEMBERSHIP_PURCHASEASGIFT, id + "", productTypeVip);
                    GrowingIOUtil.payGiftTrack(payTrackBean);
                }
            }

            @Override
            public void onError(Throwable t) {
                L.d(" onError : " + t.getMessage());
            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void getResult(BaseDataBean baseDataBean) {
        closeLoading();
        if (baseDataBean.errcode.equals("not_enough_gold")) {//软妹币不足
            gotoRecharge();
        } else {
            gotoGiftSendSuccessActivity();//赠送成功
        }
    }

    /**
     * 赠送成功
     */
    private void gotoGiftSendSuccessActivity() {
        if (operatePos >= 0) {
            Intent intent = new Intent(this, GiftSendSuccessActivity.class);
            Bundle bd = new Bundle();
            bd.putSerializable(TheLConstants.BUNDLE_KEY_FRIEND, friend);//朋友
            bd.putSerializable(TheLConstants.BUNDLE_KEY_VIP, listPlus.get(operatePos));//会员信息
            bd.putInt("pos", operatePos);//会员位置，用于显示会员描述
            intent.putExtras(bd);
            startActivity(intent);
            closeBeforeActivity();
            finish();
        }
    }

    /**
     * 关闭之前界面
     */
    private void closeBeforeActivity() {
        Intent intent = new Intent();
        intent.setAction(TheLConstants.BROADCAST_ACTION_CLOSE_BEFORE_ACTIVITY);
        intent.putExtra(TheLConstants.BUNDLE_KEY_CLOSE_BEFORE_ACTIVITY, true);
        TheLApp.getContext().sendBroadcast(intent);
    }

    /**
     * 提示充值对话框
     */
    private void showRechargeDialog() {
        DialogUtil.showConfirmDialog(this, "", getString(R.string.not_enough_point), getString(R.string.recharge_title), getString(R.string.info_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                gotoRecharge();//去充值
                dialog.dismiss();
            }
        });
    }

    /**
     * 去充值
     */
    private void gotoRecharge() {
        //// TODO: 2017/10/25
        Intent intent = new Intent(this, MySoftMoneyActivity.class);
        startActivityForResult(intent, RECHARGE_CODE);
    }

    private void getNormalPrice() {
        if (normalPrice == 1) {
            for (VipBean bean : listPlus) {
                if (bean.months == 1) {
                    normalPrice = bean.goldPrice;
                    return;
                }
            }
        }
    }

    /**
     * 软妹豆变化
     *
     * @param gold 当前软妹豆数量（这个数据仅做参考）
     */
    @Override
    public void onGoldChanged(long gold) {
        processBusiness();//重新请求数据
    }

    class GiveVipListAdapter extends BaseRecyclerViewAdapter<VipBean> {
        private final int normalPrice;
        private OnItemChildClickListener mChildListener;

        public GiveVipListAdapter(List<VipBean> data, int normalPrice) {
            super(R.layout.buy_vip_list_item, data);
            mChildListener = new OnItemChildClickListener();
            this.normalPrice = normalPrice;
        }

        @Override
        protected void convert(BaseViewHolder helper, VipBean item) {
            helper.setText(R.id.txt_operation, getString(R.string.give));
            if (item.isHot == 1) {
                helper.setVisibility(R.id.img_type, View.VISIBLE);
                helper.getConvertView().setBackgroundColor(ContextCompat.getColor(TheLApp.getContext(), R.color.light_gold));
            } else {
                helper.setVisibility(R.id.img_type, View.GONE);
                helper.getConvertView().setBackgroundColor(ContextCompat.getColor(TheLApp.getContext(), R.color.transparent));
            }
            helper.setText(R.id.txt_name, getTitle(item));
            helper.setText(R.id.txt_desc, getDesc(item));
            //原价加横线
            TextView txt_desc = helper.convertView.findViewById(R.id.txt_desc);
            txt_desc.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
            helper.setText(R.id.txt_operation, getOpera(item));
            if (!TextUtils.isEmpty(item.icon)) {
                helper.setImageUrl(R.id.img, item.icon, TheLConstants.AVATAR_MIDDLE_SIZE, TheLConstants.AVATAR_MIDDLE_SIZE);
            }
            helper.setOnItemChildClickListener(R.id.txt_operation, mChildListener);
            helper.setTag(R.id.txt_operation, helper.getLayoutPosition());
        }
    }

    /**
     * 获取标题
     *
     * @param vipBean return
     */
    private String getTitle(VipBean vipBean) {
        try {
            if (vipBean.months > 1) {
                return getString(R.string.buy_vip_months, vipBean.months);
            } else {
                return getString(R.string.buy_vip_month);
            }
        } catch (Exception e) {
            return getString(R.string.buy_vip_month);
        }

    }

    /**
     * 获取描述
     *
     * @param vipBean
     * @return
     */
    private String getDesc(VipBean vipBean) {
        if (vipBean.months > 1) {
            final int totalPirce = vipBean.months * normalPrice;
            if (vipBean.goldPrice > 1) {
                return getString(R.string.give_vip_original, totalPirce);
            } else {
                return "";
            }
        } else {
            return "";
        }
    }

    /**
     * 获取折后的价格
     */
    private String getOpera(VipBean vipBean) {
        try {
            return getString(R.string.soft_money, vipBean.goldPrice);
        } catch (Exception e) {
            return getString(R.string.soft_money, vipBean.goldPrice);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case RECHARGE_CODE:
                if (resultCode == TheLConstants.RESULT_CODE_RECHARGE) {//充值成功后返回值
                    showLoading();
                    processBusiness();//重新请求数据
                }
                break;
        }
    }

    @Override
    protected void onDestroy() {
        goldUtils.unRegisterReceiver(this);
        super.onDestroy();
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }
}
