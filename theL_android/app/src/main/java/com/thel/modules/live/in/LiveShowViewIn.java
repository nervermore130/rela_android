package com.thel.modules.live.in;

import com.thel.bean.live.LinkMicResponseBean;
import com.thel.modules.live.bean.AgoraBean;
import com.thel.modules.live.bean.AudienceLinkMicResponseBean;
import com.thel.modules.live.bean.LivePkGemNoticeBean;
import com.thel.modules.live.bean.LivePkHangupBean;
import com.thel.modules.live.bean.LivePkInitBean;
import com.thel.modules.live.bean.LivePkStartNoticeBean;
import com.thel.modules.live.bean.LivePkSummaryNoticeBean;
import com.thel.modules.live.bean.LiveRoomMsgBean;
import com.thel.modules.live.bean.LiveRoomMsgConnectMicBean;
import com.thel.modules.live.bean.TopTodayBean;
import com.thel.modules.live.surface.watch.LiveWatchObserver;
import com.thel.modules.live.view.expensive.TopGiftBean;

/**
 * Created by waiarl on 2017/11/5.
 */

public interface LiveShowViewIn extends LiveShowViewPkIn, LiveShowViewMultiIn {
    void bindObserver(LiveWatchObserver observer);

    void updateAudienceCount();

    void refreshMsg();

    void smoothScrollToBottom();

    void clearInput();

    void liveClosed();//主播回调没有

    void isBottom(boolean isBottom);//主播回调没有

    void beenBlocked(); //主播回调没有

    void hidePreviewImage(); //主播回调没有

    void showCaching(); //主播回调没有

    void hideCaching(); //主播回调没有

    void updateBroadcasterNetStatus(int broadcasterStatus); //主播回调没有

    void updateMyNetStatusBad();

    void updateMyNetStatusGood(); //主播回调没有

    void speakTooFast(); //主播回调没有

    void parseGiftMsg(String payload, String code);

    void updateBalance(String result); //主播回调没有

    void showDanmu(String danMsg);

    void openInput(boolean open);

    void setDanmuResult(String result);

    void joinVipUser(LiveRoomMsgBean liveRoomMsgBean);

    void reconnecting(); //主播回调没有

    void showDialogChannelId(); //主播回调没有

    void showDialogRequest(); //主播回调没有

    void showAlertUserId(); //主播回调没有

    void showAlertUserName(); //主播回调没有

    void addLiveRoomMsg(LiveRoomMsgBean liveRoomMsgBean);

    void showProgressbar(boolean show); //主播回调没有

    /**
     * 主播正在连麦中
     */
    void linkMicBusy(LinkMicResponseBean linkMicResponseBean);//主播回调没有

    /**
     * 主播结束连麦
     */
    void linkMicStop();//主播回调没有

    void showTopGiftView(TopGiftBean topGiftBean);

    void showLevelBelowSix();

    /**
     * 免费弹幕消息
     */
    @Override void freeBarrage();

    void onSeatAuth();

    /**
     * 显示免费送豆弹窗
     */
    void showFreeGoldView(int gold);

    /**
     * 主播发起连麦请求
     *
     * @param lIveRoomMsgConnectMicBean
     */
    void showConnectMicDialog(LiveRoomMsgConnectMicBean lIveRoomMsgConnectMicBean);

    /**
     * 对方同意连麦
     */
    void linkMicAccept();

    /**
     * 对方取消连麦
     */

    void lingMicCancel();

    /**
     * 对方挂断连麦
     */

    void dailyLinkMicHangup();

    /**
     * 守护棒变动消息
     */
    void topToday(TopTodayBean topTodayBean);

    void linkMicHangup(LiveRoomMsgConnectMicBean liveRoomMsgConnectMicBean);

    /**
     * 刷新直播间
     */
    void refreshLiveRoom();
}
