package com.thel.modules.live.bean;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseDataBean;
import com.thel.bean.user.MyImageBean;
import com.thel.bean.user.UserInfoWinkBean;
import com.thel.modules.main.me.bean.MyCirclePartnerBean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 用户名片
 */
public class LiveUserCardBean extends BaseDataBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户id(目前只在挤眼功能中使用)
     */
    public long userId;

    /**
     * 是否在线( 0 不在,1 在,2忙)
     */
    public String online;

    /**
     * 用户名
     */
    public String nickName;

    /**
     * 头像
     */
    public String avatar;


    /**
     * 认证状态，是否是加V用户 0:否 1:个人V 2:企业V
     */
    public int verifyType;
    /**
     * 认证说明
     */
    public String verifyIntro;
    /**
     * 年龄
     */
    public String age;
    /**
     * 感情状态
     */
    public int affection;
    /**
     * 我是否关注她
     */
    public boolean isFollow;

    public String workField;

    public long winkNum;

    public String height;

    public String weight;

    public String userName;//热拉id

    public int fansNum;//粉丝数

    public List<MyImageBean> picList = new ArrayList<>();
    /**
     * 4.6.0 会员等级
     **/
    public int level = 0;
    /**
     * 4.6.0 用户等级
     * 默认为0
     **/
    public int userLevel = 0;

    /**
     * 距离
     */
    public String distance;

    /**
     * 介绍
     */
    public String intro;
    /**
     * 是否免费挤过眼，0表示没今天没挤过眼
     */
    public int winked;
    // 是否有伴侣
    public MyCirclePartnerBean paternerd = null;

    public String birthday;

    /**
     * 关注状态
     * */
    public int followStatus;


    /**
     * 从json对象封装对象
     *
     * @param tempobj
     */
    /*public void fromJson(JSONObject tempobj) {
        try {
            userId = JsonUtils.getLong(tempobj, "userId", 0);
            nickName = JsonUtils.getString(tempobj, "nickName", "");
            avatar = JsonUtils.getString(tempobj, "avatar", "");
            verifyIntro = JsonUtils.getString(tempobj, "verifyIntro", "");
            age = JsonUtils.getInt(tempobj, "age", 18);
            affection = JsonUtils.getInt(tempobj, "affection", 0);
            isFollow = JsonUtils.getBoolean(tempobj, "isFollow", false);
            workField = JsonUtils.getString(tempobj, "workField", "");
            winkNum = JsonUtils.getLong(tempobj, "winkNum", 0);
            height = JsonUtils.getString(tempobj, "height", "-1");
            weight = JsonUtils.getString(tempobj, "weight", "-1");
            funsNum = JsonUtils.getInt(tempobj, "fansNum", 0);
            userName = JsonUtils.getString(tempobj, "userName", "");
            final String v = JsonUtils.getString(tempobj, "verifyType", "0");
            if (!TextUtils.isEmpty(v))
                verifyType = Integer.parseInt(v);
            final JSONArray array = JsonUtils.getJSONArray(tempobj, "picList");
            final int length = array.length();
            for (int i = 0; i < length; i++) {
                final JSONObject obj = array.getJSONObject(i);
                final MyImageBean bean = new MyImageBean();
                bean.fromJson(obj);
                picList.add(bean);
            }
        } catch (Exception e) {
            if (e.getMessage() != null) {
                Log.e(MomentsBean.class.getName(), e.getMessage());
            }
        }
    }
*/

    /**
     * 获取简介 1岁,2cm,3kg,单身
     *
     * @return
     */
    public String getIntro() {
        StringBuilder sb = new StringBuilder();
        sb.append(age).append(TheLApp.getContext().getString(R.string.updatauserinfo_activity_age_unit));
        if (!"-1".equals(height)) {
            sb.append(",").append(height).append("cm");
        }
        if (!"-1".equals(weight)) {
            sb.append(",").append(weight).append("kg");
        }
        final String[] arr = TheLApp.getContext().getResources().getStringArray(R.array.userinfo_relationship);
        if (affection >= 0 && affection < arr.length) {
            sb.append(",").append(arr[affection]);
        }
        return sb.toString();
    }
}
