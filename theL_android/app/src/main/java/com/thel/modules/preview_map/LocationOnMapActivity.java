package com.thel.modules.preview_map;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.maps2d.AMap;
import com.amap.api.maps2d.CameraUpdate;
import com.amap.api.maps2d.CameraUpdateFactory;
import com.amap.api.maps2d.LocationSource;
import com.amap.api.maps2d.MapView;
import com.amap.api.maps2d.model.BitmapDescriptorFactory;
import com.amap.api.maps2d.model.LatLng;
import com.amap.api.maps2d.model.MarkerOptions;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.utils.DialogUtil;
import com.thel.utils.L;
import com.umeng.analytics.MobclickAgent;

public class LocationOnMapActivity extends BaseActivity implements LocationSource, AMapLocationListener {

    private double double_lng;
    private double double_lat;

    private MapView mMapView;
    private AMap aMap;

    //如果str_lng和str_lat不正常我们重新定位
    private AMapLocationClient mLocationClient = null;//定位发起端
    private AMapLocationClientOption mLocationOption = null;//定位参数
    private LocationSource.OnLocationChangedListener mListener = null;//定位监听器

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_on_map);

        findViewById(R.id.lin_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mMapView = findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        aMap = mMapView.getMap();
        aMap.getUiSettings().setZoomControlsEnabled(false);

        Intent intent = getIntent();
        String str_lng = intent.getStringExtra("lng");
        String str_lat = intent.getStringExtra("lat");
        if (!TextUtils.isEmpty(str_lng)) {
            double_lng = Double.parseDouble(str_lng);
        }
        if (!TextUtils.isEmpty(str_lat)) {
            double_lat = Double.parseDouble(str_lat);
        }
        L.d("thel", "纬度" + double_lat);
        L.d("thel", "经度" + double_lng);

        if (TextUtils.isEmpty(str_lng) || TextUtils.isEmpty(str_lat)) {
            mLocationClient = new AMapLocationClient(getApplicationContext());
            mLocationClient.setLocationListener(this);
            mLocationClient.startLocation();
        } else {
            makeMarker();
        }
    }

    private void makeMarker() {
        // 定义Maker坐标点
        LatLng point = new LatLng(double_lat, double_lng);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(point, 14);
        aMap.moveCamera(cameraUpdate);
        // 构建Marker图标
        // 构建MarkerOption，用于在地图上添加Marker
        MarkerOptions option = new MarkerOptions().position(point).icon(BitmapDescriptorFactory.fromView(getLayoutInflater().inflate(R.layout.map_marker, null)));
        // 在地图上添加Marker，并显示
        aMap.addMarker(option);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mMapView.onSaveInstanceState(outState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(this.getClass().getName());
        MobclickAgent.onResume(this);
        mMapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(this.getClass().getName());
        MobclickAgent.onPause(this);
        mMapView.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onLocationChanged(AMapLocation aMapLocation) {
        if (aMapLocation != null) {
            if (aMapLocation.getErrorCode() == 0) {//定位成功回调信息，设置相关消息
                double_lng = aMapLocation.getLongitude();//获取经度
                double_lat = aMapLocation.getLatitude();//获取纬度
                makeMarker();
            } else {
                L.e("MapError", "location Error, ErrCode:" + aMapLocation.getErrorCode() + ", errInfo:" + aMapLocation.getErrorInfo());
                DialogUtil.showToastLong(TheLApp.getContext(), "定位失败");
            }
        }
    }

    @Override
    public void activate(OnLocationChangedListener onLocationChangedListener) {

    }

    @Override
    public void deactivate() {

    }
}
