package com.thel.modules.main.video_discover.pgc;

import com.thel.bean.AdBean;
import com.thel.modules.main.video_discover.videofalls.VideoFallsUtils;
import com.thel.modules.main.video_discover.videofalls.VideoMomentListBean;
import com.thel.modules.main.video_discover.videofalls.VideoMomentNetListBean;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.service.DefaultRequestService;

import org.reactivestreams.Subscription;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by waiarl on 2018/3/15.
 */

public class PgcVideoFallsPresenter implements PgcVideoFallsContact.Presenter {
    private final PgcVideoFallsContact.View view;
    private final int PAGE_SIZE = 20;

    public PgcVideoFallsPresenter(PgcVideoFallsContact.View view) {
        this.view = view;
        view.setPresenter(this);
    }

    @Override
    public void getRefreshData() {
        getVideoListData(0);

    }

    @Override
    public void getLoadMoreData(int cursor) {
        getVideoListData(cursor);
    }

    private void getVideoListData(final int cursor) {
        final Flowable<VideoMomentNetListBean> flowable = DefaultRequestService.createNearbyRequestService().getVideoFallsMomentsList(cursor, PAGE_SIZE);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<VideoMomentNetListBean>() {
            @Override
            public void onSubscribe(Subscription s) {
                super.onSubscribe(s);
            }

            @Override
            public void onNext(VideoMomentNetListBean data) {
                super.onNext(data);
                if (!hasErrorCode && data != null && data.data != null) {
                    final VideoMomentListBean listBean = data.data;
                    VideoFallsUtils.filtNearbyMoment(listBean);
                    if (0 == cursor) {
                        view.showRefreshData(listBean);
                        if (listBean.list.isEmpty()) {
                            view.emptyData();
                        }
                    } else {
                        view.showMoreData(listBean);
                    }
                } else if (hasErrorCode && cursor > 0) {
                    view.loadMoreFailed();
                }


            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                if (cursor == 0) {
                    view.emptyData();
                } else {
                    view.loadMoreFailed();
                }
            }

            @Override
            public void onComplete() {
                super.onComplete();
                view.requestFinished();
            }
        });
    }

    @Override
    public void getAdsData() {
        Flowable<AdBean> flowable = RequestBusiness.getInstance().getAd();
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<AdBean>() {


            @Override
            public void onNext(AdBean adBean) {
                super.onNext(adBean);
                if (!hasErrorCode && adBean != null && adBean.data != null && adBean.data.map_list != null) {
                    final List<AdBean.Map_list> totalList = adBean.data.map_list;
                    final List<AdBean.Map_list> adList = new ArrayList<>();

                    final int size = totalList.size();
                    for (AdBean.Map_list ad : totalList) {
                        if (AdBean.ADV_LOCATION_VIDEO.equals(ad.advertLocation)) {
                            adList.add(ad);
                        }
                    }
                    view.showAdsData(adList);
                }

            }

        });
    }

    @Override
    public void getRefreshPgcData() {
        getPgcVideoListData(0);
    }

    @Override
    public void getLoadMorePgcData(int cursor) {
        getPgcVideoListData(cursor);
    }

    private void getPgcVideoListData(final int cursor) {
        //测试
        // final Flowable<VideoMomentNetListBean> flowable = DefaultRequestService.createNearbyRequestService().getVideoFallsMomentsList(cursor, PAGE_SIZE);

        final Flowable<VideoMomentNetListBean> flowable = DefaultRequestService.createNearbyRequestService().getPgcVideoFallsMomentsList(cursor, PAGE_SIZE);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<VideoMomentNetListBean>() {
            @Override
            public void onSubscribe(Subscription s) {
                super.onSubscribe(s);
            }

            @Override
            public void onNext(VideoMomentNetListBean data) {
                super.onNext(data);
                if (!hasErrorCode && data != null && data.data != null) {
                    final VideoMomentListBean listBean = data.data;
                    VideoFallsUtils.filtNearbyMoment(listBean);
                    if (0 == cursor) {
                        view.showRefreshPgcData(listBean);
                        if (listBean.list.isEmpty()) {
                            view.emptyPgcData();
                        }
                    } else {
                        view.showMorePgcData(listBean);
                    }
                } else if (hasErrorCode) {
                    if (cursor == 0) {
                        view.loadRefreshPgcFailed(cursor);
                    } else {
                        view.loadMorePgcFailed(cursor);
                    }
                }


            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                if (cursor == 0) {
                    view.loadRefreshPgcFailed(cursor);
                } else {
                    view.loadMorePgcFailed(cursor);
                }
            }

            @Override
            public void onComplete() {
                super.onComplete();
                view.requestFinished();
            }
        });
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {

    }
}
