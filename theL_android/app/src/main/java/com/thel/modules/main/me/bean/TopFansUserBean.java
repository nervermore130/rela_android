package com.thel.modules.main.me.bean;

import com.thel.base.BaseDataBean;

import java.io.Serializable;

/**
 * 钱包里人物信息
 * Created by lingwei on 2017/10/24.
 */

public class TopFansUserBean extends BaseDataBean implements Serializable {

    public long id;
    public String nickName;
    public String avatar;
    public String intro;
    public int age;
    public int sex;
    public int status;
    public int affection;
    public String roleName;
    public int perm;
}
