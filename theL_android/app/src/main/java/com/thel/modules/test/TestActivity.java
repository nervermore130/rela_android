package com.thel.modules.test;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.widget.VideoView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.pili.pldroid.player.widget.PLVideoView;
import com.thel.R;

/**
 * Created by liuyun on 2018/3/8.
 */

public class TestActivity extends AppCompatActivity {

    public static final String TAG = "TestActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        String videoUrl = "http://video.rela.me/app/video/106703001/e22c37ed0941e44e8bb7f0478f6fcea7.mp4";

        VideoView videoView = findViewById(R.id.videoView);
        videoView.setVideoPath(videoUrl);
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                mediaPlayer.setLooping(true);
            }
        });
        videoView.start();
    }
}
