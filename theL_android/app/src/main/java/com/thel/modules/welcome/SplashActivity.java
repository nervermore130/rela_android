package com.thel.modules.welcome;

import android.content.Intent;
import android.os.Bundle;

import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.modules.main.messages.utils.PushUtils;
import com.thel.utils.L;

public class SplashActivity extends BaseActivity {

    private static final String TAG = "SplashActivity";

    public final static String PUSH_MSG = "push_msg";

    private String msg;

    private boolean isJumpToNextPage = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showStatusBar();

        Intent intent = getIntent();
        if (intent != null) {
            msg = intent.getStringExtra(PUSH_MSG);
            if (msg != null) {
                PushUtils.push(TheLApp.context, msg);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isJumpToNextPage) {

            L.d(TAG," onResume finish activity");

            SplashActivity.this.finish();
        }

        isJumpToNextPage = true;



    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

}
