package com.thel.modules.main.nearby;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Animatable;
import android.net.Uri;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.image.ImageInfo;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UploadManager;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.bean.ReleaseMomentSucceedBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.user.UploadTokenBean;
import com.thel.constants.TheLConstants;
import com.thel.modules.select_image.SelectLocalImagesActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.RequestConstants;
import com.thel.ui.dialog.ActionSheetDialog;
import com.thel.ui.imageviewer.cropiwa.image.CropIwaResultReceiver;
import com.thel.utils.ImageUtils;
import com.thel.utils.MD5Utils;
import com.thel.utils.PermissionUtil;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.ToastUtils;
import com.thel.utils.Utils;

import org.json.JSONObject;
import org.reactivestreams.Subscription;

import java.io.File;
import java.util.List;

import javax.annotation.Nullable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import video.com.relavideolibrary.RelaVideoSDK;
import video.com.relavideolibrary.Utils.Constant;
import video.com.relavideolibrary.onRelaVideoActivityResultListener;

import static com.thel.utils.DeviceUtils.getFilePathFromUri;

public class CoverPreviewActivity extends BaseActivity {

    private String coverUrl;

    private String newCoverUrl;

    private String imageLocalPath;
    private String uploadPath;//上传的视频加密后路径
    private String videoPath;//上传的视频本地地址
    private String videoThumnail;//上传视频缩略图地址
    private String uploadAlbumPath;
    private int playTime;
    private int pixelWidth;
    private int pixelHeight;
    private String videoMainColor;
    private String path;
    private int imageWidth;
    private int imageHeight;
    private String videoUrl;

    @OnClick(R.id.lin_back)
    void back() {
        finish();
    }

    @OnClick(R.id.lin_more)
    void sure() {
        if (!TextUtils.isEmpty(newCoverUrl)) {
            if (newCoverUrl.contains("webp")) {
                uploadCover(MomentsBean.MOMENT_TYPE_VIDEO, "", videoUrl, new File(videoPath).length() / 1000, playTime, pixelWidth, pixelHeight, videoMainColor, newCoverUrl);
            } else {
                uploadCover(MomentsBean.MOMENT_TYPE_IMAGE, newCoverUrl, "", new File(uploadAlbumPath).length() / 1000, 0, imageWidth, imageHeight, "", "");
            }
        }


    }

    @OnClick(R.id.complete)
    void changeCover() {
        new ActionSheetDialog(this).builder().setCancelable(true).setCanceledOnTouchOutside(true)
                .setTitle(this.getString(R.string.select_fengmian_bg))
                .addSheetItem(getString(R.string.chat_activity_photo), ActionSheetDialog.SheetItemColor.BLACK, new ActionSheetDialog.OnSheetItemClickListener() {
                    @Override
                    public void onClick(int which) {
                        Intent intent = new Intent(CoverPreviewActivity.this, SelectLocalImagesActivity.class);
                        intent.putExtra("isCover", true);
                        startActivityForResult(intent, TheLConstants.BUNDLE_CODE_UPDATE_USER_INFO_ACTIVITY);
                    }
                })
                .addSheetItem(getString(R.string.videos), ActionSheetDialog.SheetItemColor.BLACK, new ActionSheetDialog.OnSheetItemClickListener() {
                    @Override
                    public void onClick(int which) {
                        PermissionUtil.requestStoragePermission(CoverPreviewActivity.this, new PermissionUtil.PermissionCallback() {
                            @Override
                            public void granted() {
                                RelaVideoSDK.startVideoGalleryActivity(CoverPreviewActivity.this, new onRelaVideoActivityResultListener() {
                                    @Override
                                    public void onRelaVideoActivityResult(Intent intent) {
                                        Bundle bundle = intent.getExtras();
                                        videoPath = bundle.getString(video.com.relavideolibrary.Utils.Constant.BundleConstants.RESULT_VIDEO_PATH);
                                        videoThumnail = bundle.getString(video.com.relavideolibrary.Utils.Constant.BundleConstants.RESULT_VIDEO_THUMB);
                                        playTime = Integer.parseInt(bundle.getString(Constant.BundleConstants.RESULT_VIDEO_DURATION));
                                        pixelWidth = Integer.parseInt(bundle.getString(Constant.BundleConstants.RESULT_VIDEO_WIDTH));
                                        pixelHeight = Integer.parseInt(bundle.getString(Constant.BundleConstants.RESULT_VIDEO_HEIGHT));
                                        videoMainColor = bundle.getString(Constant.BundleConstants.RESULT_VIDEO_MAIN_COLOR);
                                        uploadVideo();//上传视频路径到服务器
                                    }
                                });
                            }

                            @Override
                            public void denied() {
                            }

                            @Override
                            public void gotoSetting() {
                            }
                        });
                    }
                })
                .show();
    }

    @BindView(R.id.img_cover)
    SimpleDraweeView img_cover;

    private CropIwaResultReceiver cropResultReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_cover_preview);


        ButterKnife.bind(this);

        coverUrl = getIntent().getStringExtra(TheLConstants.BUNDLE_KEY_COVER);

        img_cover.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(coverUrl, 720, 1280))).build()).setAutoPlayAnimations(true).build());

        img_cover.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadius(0).setRoundingMethod(RoundingParams.RoundingMethod.OVERLAY_COLOR).setOverlayColor(ContextCompat.getColor(TheLApp.getContext(), R.color.white)));

        cropResultReceiver = new CropIwaResultReceiver();
        cropResultReceiver.register(this);
        cropResultReceiver.setListener(new CropIwaResultReceiver.Listener() {
            @Override
            public void onCropSuccess(Uri croppedUri) {
                imageLocalPath = getFilePathFromUri(CoverPreviewActivity.this, croppedUri);
                //上传封面
                uploadImage();
            }

            @Override
            public void onCropFailed(Throwable e) {
                ToastUtils.showToastShort(CoverPreviewActivity.this, "crop failed");
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cropResultReceiver.unregister(this);
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void uploadImage() {
        if (TextUtils.isEmpty(imageLocalPath)) {
            return;
        }
        uploadAlbumPath = RequestConstants.UPLOAD_FILE_ROOT_PATH_ALBUM + ShareFileUtils.getString(ShareFileUtils.ID, "") + "/" + MD5Utils.calculateMD5(new File(imageLocalPath)) + ".jpg";
        showLoading();
        getUploadToken(System.currentTimeMillis() + "", "", uploadAlbumPath, "image");
    }

    private void getUploadToken(String clientTime, String bucket, String path, final String uploadType) {
        Flowable<UploadTokenBean> flowable = RequestBusiness.getInstance().getUploadToken(clientTime, bucket, path);
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<UploadTokenBean>() {

            @Override
            public void onNext(UploadTokenBean uploadTokenBean) {
                super.onNext(uploadTokenBean);
                uploadToken(uploadTokenBean, uploadType);
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                closeLoading();
            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void uploadToken(UploadTokenBean uploadTokenBean, String uploadType) {
        UploadManager uploadManager = new UploadManager();
        if (uploadType.equals("image")) {//相册
            if (!TextUtils.isEmpty(uploadAlbumPath) && !TextUtils.isEmpty(imageLocalPath)) {
                uploadManager.put(new File(imageLocalPath), uploadAlbumPath, uploadTokenBean.data.uploadToken, new UpCompletionHandler() {
                    @Override
                    public void complete(String key, ResponseInfo info, JSONObject response) {
                        if (info != null && info.statusCode == 200 && uploadAlbumPath.equals(key)) {
                            List<Integer> wh = ImageUtils.measurePicWidthAndHeight(imageLocalPath);
                            if (wh.size() == 2) {
                                newCoverUrl = RequestConstants.FILE_BUCKET + key;
                                imageWidth = wh.get(0);
                                imageHeight = wh.get(1);

                                closeLoading();
                                img_cover.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(newCoverUrl, 720, 1280))).build()).setAutoPlayAnimations(true).build());

                                img_cover.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadius(0).setRoundingMethod(RoundingParams.RoundingMethod.OVERLAY_COLOR).setOverlayColor(ContextCompat.getColor(TheLApp.getContext(), R.color.white)));

                            }
                        }
                    }
                }, null);
            }
        }
    }

    private void uploadVideo() {
        showLoading();
        String fileType = ".mp4";
        // 把文件地址作为uuid，等到上传成功返回结果后根据这个字符串来更新数据库未上传的图片列表
        uploadPath = RequestConstants.UPLOAD_FILE_ROOT_PATH_TIMELINE + ShareFileUtils.getString(ShareFileUtils.ID, "") + "/" + MD5Utils.calculateMD5(new File(videoPath)) + fileType;
        // 如果是上传视频的话要穿『桶』名
        RequestBusiness.getInstance()
                .getUploadToken(System.currentTimeMillis() + "", RequestConstants.QINIU_BUCKET_VIDEO, uploadPath, true)
                .onBackpressureDrop().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new InterceptorSubscribe<UploadTokenBean>() {
                    @Override
                    public void onNext(UploadTokenBean uploadTokenBean) {
                        super.onNext(uploadTokenBean);
                        if (!TextUtils.isEmpty(uploadTokenBean.data.uploadToken)) {
                            if (!TextUtils.isEmpty(videoPath) && !TextUtils.isEmpty(uploadPath)) {
                                UploadManager uploadManager = new UploadManager();
                                uploadManager.put(new File(videoPath), uploadPath, uploadTokenBean.data.uploadToken, new UpCompletionHandler() {
                                    @Override
                                    public void complete(String key, ResponseInfo info, JSONObject response) {
                                        try {
                                            if (info != null && info.statusCode == 200 && uploadPath.equals(key)) {
                                                videoUrl = RequestConstants.VIDEO_BUCKET + key;
                                                newCoverUrl = RequestConstants.FILE_BUCKET + Utils.getWebpPathByVideoPath(key);
                                                img_cover.setController(Fresco.newDraweeControllerBuilder().setControllerListener(videoControllerListener).setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(newCoverUrl, 720, 1280))).build()).setAutoPlayAnimations(true).build());

                                                img_cover.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadius(0).setRoundingMethod(RoundingParams.RoundingMethod.OVERLAY_COLOR).setOverlayColor(ContextCompat.getColor(TheLApp.getContext(), R.color.white)));
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }, null);
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                        closeLoading();
                    }
                });
    }

    private void uploadCover(String momentType, final String imageUrl, String videoUrl, long mediaSize, int playTime, int pixelWidth, int pixelHeight, String videoColor, final String videoWebp) {
        showLoading();
        RequestBusiness.getInstance()
                .uploadCover(momentType, imageUrl, videoUrl, mediaSize, playTime, pixelWidth, pixelHeight, videoColor, videoWebp)
                .onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new InterceptorSubscribe<ReleaseMomentSucceedBean>() {
                    @Override
                    public void onNext(ReleaseMomentSucceedBean data) {
                        super.onNext(data);
                        closeLoading();
                        Intent intent = getIntent();
                        intent.putExtra(TheLConstants.BUNDLE_KEY_COVER, newCoverUrl);
                        setResult(Activity.RESULT_OK, intent);
                        finish();
                    }

                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                        closeLoading();
                    }
                });
    }

    private BaseControllerListener videoControllerListener = new BaseControllerListener<ImageInfo>() {
        @Override
        public void onFinalImageSet(String id, @Nullable ImageInfo imageInfo, @Nullable Animatable animatable) {
            super.onFinalImageSet(id, imageInfo, animatable);
            closeLoading();
        }

        @Override
        public void onFailure(String id, Throwable throwable) {
            super.onFailure(id, throwable);
            img_cover.postDelayed(new Runnable() {
                @Override
                public void run() {
                    img_cover.setController(Fresco.newDraweeControllerBuilder().setControllerListener(videoControllerListener).setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(newCoverUrl, 720, 1280))).build()).setAutoPlayAnimations(true).build());

                    img_cover.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadius(0).setRoundingMethod(RoundingParams.RoundingMethod.OVERLAY_COLOR).setOverlayColor(ContextCompat.getColor(TheLApp.getContext(), R.color.white)));
                }
            }, 3000);
        }
    };
}
