package com.thel.modules.main.home.community;

import android.content.Context;
import android.net.Uri;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.AdBean;
import com.thel.ui.widget.emoji.OnRecyclerViewListener;
import com.thel.utils.ImageUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;

import java.util.ArrayList;

/**
 * Created by chad
 * Time 17/10/18
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */

public class AdsRecyclerViewAdapter extends RecyclerView.Adapter {

    private ArrayList<AdBean.Map_list> tags = new ArrayList<>();

    private LayoutInflater mInflater;
    private Context context;

    public AdsRecyclerViewAdapter(ArrayList<AdBean.Map_list> tagBeans, Context context) {
        this.context = context;
        mInflater = LayoutInflater.from(context);

        this.tags = tagBeans;
    }

    public void setOnRecyclerViewListener(OnRecyclerViewListener onRecyclerViewListener) {
        this.onRecyclerViewListener = onRecyclerViewListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = mInflater.inflate(R.layout.discovery_ad_item, null);
        return new AdsRecyclerViewAdapter.HoldView(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        HoldView holdView = (HoldView) viewHolder;
        if (position == 0) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.leftMargin = (int) context.getResources().getDimension(R.dimen.moment_list_margin);
            holdView.lin_main.setLayoutParams(params);
        }
        if (position == tags.size() - 1) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.rightMargin = Utils.dip2px(TheLApp.getContext(), 2);
            holdView.lin_main.setLayoutParams(params);
        }
        holdView.position = position;
        final AdBean.Map_list adBean = tags.get(position);
        holdView.img.setController(Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(ImageUtils.buildNetPictureUrl(adBean.advertURL, context.getResources().getDimension(R.dimen.discover_page_ad_width), context.getResources().getDimension(R.dimen.discover_page_ad_height)))).build()).setAutoPlayAnimations(true).build());
        holdView.img.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadius(10));
        holdView.txt.setText(adBean.advertTitle);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return tags.size();
    }

    private OnRecyclerViewListener onRecyclerViewListener;

    class HoldView extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        public LinearLayout lin_main;
        public SimpleDraweeView img;
        public TextView txt;
        public int position;

        public HoldView(View itemView) {
            super(itemView);
            lin_main = itemView.findViewById(R.id.lin_main);
            img = itemView.findViewById(R.id.img);
            txt = itemView.findViewById(R.id.txt);
            lin_main.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            ViewUtils.preventViewMultipleClick(view, 2000);
            if (null != onRecyclerViewListener) {
                onRecyclerViewListener.onItemClick(position);
            }
        }

        @Override
        public boolean onLongClick(View view) {
            ViewUtils.preventViewMultipleClick(view, 2000);
            if (null != onRecyclerViewListener) {
                return onRecyclerViewListener.onItemLongClick(position);
            }
            return false;
        }
    }
}
