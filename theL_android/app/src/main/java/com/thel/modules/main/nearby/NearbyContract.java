package com.thel.modules.main.nearby;

import com.thel.base.BasePresenter;
import com.thel.base.BaseView;

/**
 * Created by waiarl on 2017/9/21.
 */

public class NearbyContract {

    interface Presenter extends BasePresenter {

    }

    interface View extends BaseView<NearbyContract.Presenter> {

    }
}
