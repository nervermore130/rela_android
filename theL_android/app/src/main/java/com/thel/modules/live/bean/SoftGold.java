package com.thel.modules.live.bean;

import android.text.TextUtils;

import com.thel.base.BaseDataBean;

import java.io.Serializable;
import java.text.DecimalFormat;

/**
 * Created by the L on 2016/5/22.
 * 软妹币商品
 */
public class SoftGold extends BaseDataBean implements Serializable {

    /**
     * 商品id
     */
    public int id;

    /**
     * 金币数量
     */
    public long gold = 0;

    /**
     * 显示的软妹豆
     */
    public int showGold;

    /**
     * 价格（单位 分）
     */
    public double price = 0;

    /**
     * 商品图标
     */
    public String icon;

    /**
     * 商品描述
     */
    public String summary;

    /**
     * google play商品id
     */
    public String iapId;

    /**
     * 用google play支付的话需要从google play上获取价格
     */
    public boolean isPendingPrice = false;

    /**
     * 谷歌钱包直接显示价格
     */
    public String googlePrice;

    /**
     * 首充提示
     */
    public String firstChargeDesc;

    public String gemPrice;

    public String generatePriceShow() {
        if (TextUtils.isEmpty(googlePrice))
            try {
                DecimalFormat df = new DecimalFormat("0.00");
                return "￥" + df.format(price / 100);
            } catch (Exception e) {
                return "￥" + price;
            }
        else
            return googlePrice;
    }

    @Override
    public String toString() {
        return "SoftGold{" +
                "id=" + id +
                ", gold=" + gold +
                ", price=" + price +
                ", icon='" + icon + '\'' +
                ", summary='" + summary + '\'' +
                ", iapId='" + iapId + '\'' +
                ", isPendingPrice=" + isPendingPrice +
                ", googlePrice='" + googlePrice + '\'' +
                ", firstChargeDesc='" + firstChargeDesc + '\'' +
                ", gemPrice='" + gemPrice + '\'' +
                '}';
    }
}
