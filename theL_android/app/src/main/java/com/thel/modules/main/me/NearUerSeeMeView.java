package com.thel.modules.main.me;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.thel.R;
import com.thel.bean.user.NearUserBean;
import com.thel.constants.TheLConstants;
import com.thel.manager.ImageLoaderManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lingwei on 2018/3/13.
 */

public class NearUerSeeMeView extends RelativeLayout {
    private final Context mContext;
    private View rel_0;
    private ImageView img_rank_0;
    private View rel_1;
    private ImageView img_rank_1;
    private View rel_2;
    private ImageView img_rank_2;
    private View rel_3;
    private ImageView img_rank_3;
    private List<NearUserBean> nearUserList = new ArrayList<>();

    public NearUerSeeMeView(Context context) {
        this(context, null);
    }

    public NearUerSeeMeView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NearUerSeeMeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }

    private void init() {
        inflate(mContext, R.layout.me_near_user_view, this);
        rel_1 = findViewById(R.id.rel_1);
        img_rank_1 = findViewById(R.id.img_rank_1);
        rel_2 = findViewById(R.id.rel_2);
        img_rank_2 = findViewById(R.id.img_rank_2);
        rel_3 = findViewById(R.id.rel_3);
        img_rank_3 = findViewById(R.id.img_rank_3);
        //        rel_0.setVisibility(View.VISIBLE);
        rel_1.setVisibility(View.GONE);
        rel_2.setVisibility(View.GONE);
        rel_3.setVisibility(View.GONE);


    }

    public NearUerSeeMeView initData(List<NearUserBean> nearUserBeans) {
        this.nearUserList = nearUserBeans;
        initView();
        return this;
    }

    private NearUerSeeMeView initView() {

        rel_1.setVisibility(View.GONE);
        rel_2.setVisibility(View.GONE);
        rel_3.setVisibility(View.GONE);
        if (nearUserList == null) {
            return this;
        }
        final int length = nearUserList.size();
        for (int i = 0; i < length; i++) {
            if (i == 0) {
                rel_1.setVisibility(View.VISIBLE);
                ImageLoaderManager.imageLoaderDefaultCircle(img_rank_1, R.mipmap.icon_user, nearUserList.get(0).avatar, TheLConstants.AVATAR_SMALL_SIZE, TheLConstants.AVATAR_SMALL_SIZE);
            } else if (i == 1) {
                rel_2.setVisibility(View.VISIBLE);
                ImageLoaderManager.imageLoaderDefaultCircle(img_rank_2, R.mipmap.icon_user, nearUserList.get(1).avatar, TheLConstants.AVATAR_SMALL_SIZE, TheLConstants.AVATAR_SMALL_SIZE);

            } else if (i == 2) {
                rel_3.setVisibility(View.VISIBLE);
                ImageLoaderManager.imageLoaderDefaultCircle(img_rank_3, R.mipmap.icon_user, nearUserList.get(2).avatar, TheLConstants.AVATAR_SMALL_SIZE, TheLConstants.AVATAR_SMALL_SIZE);

            }
        }
        return this;
    }

}
