package com.thel.modules.main.me.aboutMe;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.constants.TheLConstants;
import com.thel.modules.main.messages.ChatActivity;
import com.thel.utils.ImageUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

public class MatchSucceedActivity extends BaseActivity {

    private SimpleDraweeView img_avatar_left;
    private SimpleDraweeView img_avatar_right;
    private TextView txt_match;

    private long userId;
    private String nickName;
    private String avatar;
    private MatchSuccessParticleView matchSuccessView;
    int[] src = new int[]{R.mipmap.match_color1, R.mipmap.match_color2, R.mipmap.match_color3, R.mipmap.match_color4, R.mipmap.match_color5};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.match_succeed_dialog);
        findViewById();
        Intent intent = getIntent();

        userId = intent.getLongExtra("userId", 0);
        nickName = intent.getStringExtra("nickName");
        avatar = intent.getStringExtra("avatar");
        setListener();

        img_avatar_left.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(ShareFileUtils.getString(ShareFileUtils.AVATAR, ""), TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE)));
        img_avatar_right.setImageURI(Uri.parse(ImageUtils.buildNetPictureUrl(avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE)));
        txt_match.setText(String.format(TheLApp.getContext().getString(R.string.match_perfect_match_line2), nickName));
        setListener();
       /* txt_match.postDelayed(new Runnable() {
            @Override
            public void run() {*//*
                new ParticleSystem(MatchSucceedActivity.this, 20, R.drawable.match_color1, 10000).setSpeedModuleAndAngleRange(0f, 0.1f, 0, 180).setAcceleration(0.000017f, 90).setRotationSpeed(144).emit(findViewById(R.id.emiter_top1), 2);
                new ParticleSystem(MatchSucceedActivity.this, 20, R.drawable.match_color2, 10000).setSpeedModuleAndAngleRange(0f, 0.1f, 0, 180).setAcceleration(0.000017f, 90).setRotationSpeed(144).emit(findViewById(R.id.emiter_top2), 2);
                new ParticleSystem(MatchSucceedActivity.this, 20, R.drawable.match_color3, 10000).setSpeedModuleAndAngleRange(0f, 0.1f, 0, 180).setAcceleration(0.000017f, 90).setRotationSpeed(144).emit(findViewById(R.id.emiter_top3), 2);
                new ParticleSystem(MatchSucceedActivity.this, 20, R.drawable.match_color4, 10000).setSpeedModuleAndAngleRange(0f, 0.1f, 0, 180).setAcceleration(0.000017f, 90).setRotationSpeed(144).emit(findViewById(R.id.emiter_top4), 2);
                new ParticleSystem(MatchSucceedActivity.this, 20, R.drawable.match_color5, 10000).setSpeedModuleAndAngleRange(0f, 0.1f, 0, 180).setAcceleration(0.000017f, 90).setRotationSpeed(144).emit(findViewById(R.id.emiter_top5), 2);*//*
            }
        }, 50);*/
        matchSuccessView.initView(src);
        matchSuccessView.start();

    }

    protected void findViewById() {
        img_avatar_left = findViewById(R.id.img_avatar_left);
        img_avatar_right = findViewById(R.id.img_avatar_right);
        txt_match = findViewById(R.id.txt_match);
        matchSuccessView = findViewById(R.id.matchSuccessView);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(this.getClass().getName());
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(this.getClass().getName());
        MobclickAgent.onPause(this);
    }

    protected void setListener() {

        // 点击事件
        findViewById(R.id.txt_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                finish();
                Intent intent = new Intent(MatchSucceedActivity.this, ChatActivity.class);
                intent.putExtra("toUserId", userId + "");
                intent.putExtra("toName", nickName);
                intent.putExtra("toAvatar", avatar);
                startActivity(intent);
            }
        });

        findViewById(R.id.txt_maybe_next_time).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                finish();
            }
        });
    }

    @Override
    protected void onDestroy() {
        matchSuccessView.stop();
        super.onDestroy();
    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }
}
