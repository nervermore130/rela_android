package com.thel.modules.main.me.aboutMe;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amap.api.services.core.LatLonPoint;
import com.amap.api.services.geocoder.GeocodeResult;
import com.amap.api.services.geocoder.GeocodeSearch;
import com.amap.api.services.geocoder.RegeocodeQuery;
import com.amap.api.services.geocoder.RegeocodeResult;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.base.BaseActivity;
import com.thel.bean.user.LocationNameBean;
import com.thel.bean.user.NearUserBean;
import com.thel.bean.user.NearUserListNetBean;
import com.thel.bean.user.VipConfigBean;
import com.thel.constants.TheLConstants;
import com.thel.flutter.bridge.FlutterRouterConfig;
import com.thel.modules.main.me.bean.LogInfoBean;
import com.thel.modules.main.me.match.eventcollect.collect.LiveLogUtils;
import com.thel.modules.main.me.match.eventcollect.collect.MatchLogUtils;
import com.thel.modules.main.nearby.adapter.NearbyUserRecyclerViewAdapter;
import com.thel.modules.main.userinfo.UserInfoActivity;
import com.thel.modules.others.VipConfigActivity;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.ui.widget.DefaultEmptyView;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.utils.DeviceUtils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.SharedPrefUtils;
import com.thel.utils.UserUtils;
import com.thel.utils.Utils;
import com.thel.utils.ViewUtils;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * 复合筛选结果
 * Created by lingwei on 2017/10/23.
 */

public class DetailedFiltersResultActivity extends BaseActivity {
    private LinearLayout lin_back;
    private TextView title;
    private SwipeRefreshLayout swipe_container;
    private LinearLayout footview;
    // 世界
    private String lat;
    private String lng;
    private GeocodeSearch geocoderSearch;

    private NearUserListNetBean nearUserListbeanNet;
    private ArrayList<NearUserBean> list = new ArrayList<NearUserBean>();
    private RecyclerView mRecyclerView;
    private StaggeredGridLayoutManager manager;
    private LocationNameBean locationNameBean;
    private static long lastClickTime = 0;
    // 刷新类型，全部刷新（即下拉刷新）为1，还是加载下一页为2
    private int refreshType = 0;
    private final int REFRESH_TYPE_ALL = 1;
    private final int REFRESH_TYPE_NEXT_PAGE = 2;
    private int currentCountForOnce = -1; // 每次请求服务器，返回的数据条数
    private final int pageSize = 20;
    private int curPage = 1; // 将要请求的页号
    private NearbyUserRecyclerViewAdapter mAdapter;
    private DefaultEmptyView empty_default_world;
    private String pageId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detailed_filters_result_activity);
        findViewById();
        getIntentData();
        setListener();
        loadNetData(true);
        pageId = Utils.getPageId();

    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (RESULT_OK == resultCode && TheLConstants.REQUEST_CODE_BUY_VIP == requestCode) {//购买了会员返回值
            //获取会员信息
            loadVipConfig();
        }
    }

    private void getIntentData() {
        Intent intent = getIntent();
        if (null == intent) {
            finish();
            return;
        }
        geocoderSearch = new GeocodeSearch(this);
        lat = ShareFileUtils.getString(SharedPrefUtils.WORLD_LAT, "");
        lng = ShareFileUtils.getString(SharedPrefUtils.WORLD_LNG, "");
        title.setText("");
    }

    private void loadVipConfig() {
        Flowable<VipConfigBean> flowable = RequestBusiness.getInstance().getVipConfig();
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<VipConfigBean>() {
            @Override
            public void onNext(VipConfigBean data) {
                super.onNext(data);
                getVipInfo(data);
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                refreshError();
            }

            @Override
            public void onComplete() {
                super.onComplete();
                refreshFinish();
            }
        });
    }

    private void getVipInfo(VipConfigBean vipConfigBean) {
        if (vipConfigBean.data != null) {
            UserUtils.setUserVipLevel(vipConfigBean.data.vipSetting.level);
            if (UserUtils.getUserVipLevel() > 0) {
                mAdapter.removeAllFooterView();
                footview.setVisibility(View.GONE);
                if (currentCountForOnce > 0) {
                    mAdapter.openLoadMore(list.size(), true);
                    loadNetData(false);
                } else {
                    mAdapter.openLoadMore(list.size(), false);
                }
            }
        }
    }

    private void loadNetData(boolean refreshAll) {
        if (refreshAll) {
            curPage = 1;
            refreshType = REFRESH_TYPE_ALL;
        } else {
            refreshType = REFRESH_TYPE_NEXT_PAGE;
        }
        loadWorldUsers();
    }

    private void loadWorldUsers() {
        Flowable<NearUserListNetBean> flowable = RequestBusiness.getInstance().getWorldUsers(lat, lng, pageSize + "", curPage + "");
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<NearUserListNetBean>() {
            @Override
            public void onNext(NearUserListNetBean data) {
                super.onNext(data);
                getWorldUsers(data);
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                refreshError();
            }

            @Override
            public void onComplete() {
                super.onComplete();
                refreshFinish();
            }
        });
        getLocationName(lat, lng);
    }

    private void getWorldUsers(NearUserListNetBean nearUserListNetBean) {
        if (nearUserListNetBean.data == null)
            return;
        currentCountForOnce = nearUserListNetBean.data.map_list.size();
        refreshUI(nearUserListNetBean);
    }

    private void refreshUI(NearUserListNetBean nearUserListNetBean) {
        if (REFRESH_TYPE_ALL == refreshType) {
            list.clear();
        }
        list.addAll(nearUserListNetBean.data.map_list);
        setDefaultView(list);
        if (REFRESH_TYPE_ALL == refreshType) {
            mAdapter.removeAllFooterView();
            mAdapter.setNewData(list);
            curPage = 2;
            if (currentCountForOnce > 0) {
                mAdapter.openLoadMore(list.size(), true);
            } else {
                if (UserUtils.getUserVipLevel() <=0){
                    footview.setVisibility(View.VISIBLE);
                }else {
                    mAdapter.openLoadMore(list.size(), false);

                }
            }
        } else {
            curPage++;
            if (UserUtils.getUserVipLevel() <=0){
                footview.setVisibility(View.VISIBLE);
            }else {
                mAdapter.notifyDataChangedAfterLoadMore(true, list.size());

            }
        }
    }

    private void getLocationName(final String lat, final String lng) {
        geocoderSearch.setOnGeocodeSearchListener(new GeocodeSearch.OnGeocodeSearchListener() {
            @Override
            public void onRegeocodeSearched(RegeocodeResult regeocodeResult, int i) {
                try {
                    String city = regeocodeResult.getRegeocodeAddress().getProvince() + regeocodeResult.getRegeocodeAddress().getCity();
                    if (TextUtils.isEmpty(city)) {
                        loadLocationName(lat, lng);
                    } else {
                        title.setText(city);
                    }
                } catch (Exception e) {
                    loadLocationName(lat, lng);
                }
            }

            @Override
            public void onGeocodeSearched(GeocodeResult geocodeResult, int i) {
            }
        });
        // 第一个参数表示一个Latlng，第二参数表示范围多少米，第三个参数表示是火系坐标系还是GPS原生坐标系
        try {
            LatLonPoint latLonPoint = new LatLonPoint(Double.parseDouble(lat), Double.parseDouble(lng));
            RegeocodeQuery query = new RegeocodeQuery(latLonPoint, 200, GeocodeSearch.AMAP);
            geocoderSearch.getFromLocationAsyn(query);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void loadLocationName(String lat, String lng) {
        Flowable<LocationNameBean> flowable = RequestBusiness.getInstance().getLocationName(lat, lng, DeviceUtils.getLanguage());
        flowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<LocationNameBean>() {
            @Override
            public void onNext(LocationNameBean data) {
                super.onNext(data);
                refreshLocationName(locationNameBean);
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
            }

            @Override
            public void onComplete() {
                super.onComplete();
                refreshFinish();
            }
        });
    }

    private void refreshLocationName(LocationNameBean locationNameBean) {
        if (TextUtils.isEmpty(locationNameBean.cityName)) {
            title.setText(getString(R.string.detailed_filters_result_activity_unknown));
        } else {
            title.setText(locationNameBean.cityName);
        }
    }

    private void setListener() {
        lin_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.preventViewMultipleClick(v, 2000);
                DetailedFiltersResultActivity.this.finish();
            }
        });

        title.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (MotionEvent.ACTION_DOWN == event.getAction()) {
                    if (System.currentTimeMillis() - lastClickTime < 300) {
                        ViewUtils.preventViewMultipleTouch(v, 2000);
                        ShareFileUtils.setBoolean(ShareFileUtils.IS_GO_TO_TOP_TIP_SHOWED, true);
                        mRecyclerView.scrollToPosition(0);
                    }
                    lastClickTime = System.currentTimeMillis();
                }
                return true;
            }
        });

        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                loadNetData(true);
            }
        });

        footview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MobclickAgent.onEvent(TheLApp.getContext(), "check_see_more_filt_result");
                gotoBuyVip();
            }
        });

        mAdapter.setOnRecyclerViewItemClickListener(new BaseRecyclerViewAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                final NearUserBean tempUser = mAdapter.getItem(position);
                int tempUserId = tempUser.userId;
//                Intent intent = new Intent();
//                intent.putExtra(TheLConstants.BUNDLE_KEY_USER_ID, tempUserId + "");
//                intent.setClass(DetailedFiltersResultActivity.this, UserInfoActivity.class);
//                startActivity(intent);
                FlutterRouterConfig.Companion.gotoUserInfo(tempUserId + "");
            }
        });
        mAdapter.setOnLoadMoreListener(new BaseRecyclerViewAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                if (currentCountForOnce > 0) {
                    if (UserUtils.getUserVipLevel() <=0){
                        footview.setVisibility(View.VISIBLE);
                    }else {
                        loadNetData(false);

                    }
                } else {
                    mAdapter.openLoadMore(0, false);
                    if (list.size() > 0) {
                        if (UserUtils.getUserVipLevel() <=0){
                            footview.setVisibility(View.VISIBLE);
                        }else {
                            mAdapter.notifyDataChangedAfterLoadMore(true, list.size());

                        }
                    }
                }
            }
        });
        mAdapter.setReloadMoreListener(new BaseRecyclerViewAdapter.ReloadMoreListener() {
            @Override
            public void reloadMore() {
                mAdapter.removeAllFooterView();
                mAdapter.openLoadMore(true);
                loadNetData(false);
            }
        });
    }

    /**
     * 购买会员
     */
    private void gotoBuyVip() {
        Intent intent = new Intent(DetailedFiltersResultActivity.this, VipConfigActivity.class);
        intent.putExtra("fromPage", "wander");
        intent.putExtra("fromPageId", pageId);

        startActivityForResult(intent, TheLConstants.REQUEST_CODE_BUY_VIP);
        traceWanderLog("more_result");
    }

    private void findViewById() {
        lin_back = findViewById(R.id.lin_back);
        title = findViewById(R.id.txt_title);
        swipe_container = this.findViewById(R.id.swipe_container);
        ViewUtils.initSwipeRefreshLayout(swipe_container);
        mRecyclerView = findViewById(R.id.recyclerview);
        manager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(manager);
        mAdapter = new NearbyUserRecyclerViewAdapter(list, false);
        mRecyclerView.setAdapter(mAdapter);
        empty_default_world = findViewById(R.id.lin_default_world);
        footview = findViewById(R.id.layout_no_vip_defalut);
        footview.setVisibility(View.GONE);
        swipe_container.post(new Runnable() {
            @Override
            public void run() {
                swipe_container.setRefreshing(true);
            }
        });
    }

    private void refreshFinish() {
        if (swipe_container != null && swipe_container.isRefreshing()) {
            swipe_container.postDelayed(new Runnable() {
                @Override
                public void run() {
                    swipe_container.setRefreshing(false);
                }
            }, 1000);
        }
    }

    private void refreshError() {
        if (mRecyclerView != null && refreshType == REFRESH_TYPE_NEXT_PAGE) {
            mRecyclerView.post(new Runnable() {
                @Override
                public void run() {
                    mAdapter.loadMoreFailed((ViewGroup) mRecyclerView.getParent());
                }
            });
        }
    }

    private void setDefaultView(ArrayList<NearUserBean> list) {
        empty_default_world.setVisibility(list.size() > 0 ? View.GONE : View.GONE);

    }

    public void traceWanderLog(String activity) {
        try {

            LogInfoBean logInfoBean = new LogInfoBean();
            logInfoBean.page = "wander";
            logInfoBean.page_id = pageId;
            logInfoBean.activity = activity;

            MatchLogUtils.getInstance().addLog(logInfoBean);

        } catch (Exception e) {

        }
    }
}
