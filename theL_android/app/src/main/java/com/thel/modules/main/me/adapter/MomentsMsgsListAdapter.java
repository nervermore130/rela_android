package com.thel.modules.main.me.adapter;

import android.text.TextUtils;
import android.view.View;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.comment.CommentBean;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.moments.MomentsMsgBean;
import com.thel.constants.TheLConstants;
import com.thel.ui.widget.recyclerview.BaseRecyclerViewAdapter;
import com.thel.ui.widget.recyclerview.BaseViewHolder;
import com.thel.utils.EmojiUtils;
import com.thel.utils.Utils;

import java.util.List;

/**
 * Created by lingwei on 2017/10/19.
 */

public class MomentsMsgsListAdapter extends BaseRecyclerViewAdapter<MomentsMsgBean> {
    public JumpUserinfoListener jumpUserinfoListener;
    public JumpMomentTypeListener momentListener;

    public MomentsMsgsListAdapter(List<MomentsMsgBean> list) {
        super(R.layout.moments_msgs_listitem, list);
    }

    @Override
    protected void convert(final BaseViewHolder helper, MomentsMsgBean momentsMsgBean) {
        helper.setVisibility(R.id.moments_msgs_left_area, View.VISIBLE);
        helper.setVisibility(R.id.moments_msgs_middle_area, View.VISIBLE);
        helper.setVisibility(R.id.moments_msgs_right_area, View.VISIBLE);
        helper.setVisibility(R.id.img_sticker, View.GONE);
        helper.setVisibility(R.id.moments_msgs_commment_text, View.GONE);
        helper.setVisibility(R.id.txt_update_now, View.GONE);
        helper.setVisibility(R.id.rel_moments_msgs, View.VISIBLE);


        // 把控件上的tag全删除掉，否则会错乱
        helper.setTag(R.id.moments_msgs_middle_area, R.id.moment_id_tag, null);
        helper.setTag(R.id.moments_msgs_right_area, R.id.moment_id_tag, null);
        helper.setTag(R.id.moments_msgs_middle_area, R.id.comment_id, null);
        helper.setTag(R.id.moments_msgs_right_area, R.id.comment_id, null);

        if (MomentsBean.MOMENT_TYPE_THEME.equals(momentsMsgBean.momentsType)) {
            // 如果是话题评论的话，跳转到评论的那条日志页面
            if (MomentsMsgBean.MSG_TYPE_SAY.equals(momentsMsgBean.activityType)) {
                helper.setTag(R.id.moments_msgs_middle_area, R.id.moment_id_tag, momentsMsgBean.commentId);
                helper.setTag(R.id.moments_msgs_right_area, R.id.moment_id_tag, momentsMsgBean.commentId);
            } else {
                helper.setTag(R.id.moments_msgs_middle_area, R.id.theme_id_tag, momentsMsgBean.commentId);
                helper.setTag(R.id.moments_msgs_right_area, R.id.theme_id_tag, momentsMsgBean.commentId);

            }
            // 如果是评论二级页面
        } else if (MomentsMsgBean.TYPE_COMMENT_REPLY.equals(momentsMsgBean.type)) {
            helper.setTag(R.id.moments_msgs_middle_area, R.id.comment_id, momentsMsgBean.momentsId);
            helper.setTag(R.id.moments_msgs_right_area, R.id.comment_id, momentsMsgBean.momentsId);
        } else if ((MomentsMsgBean.MSG_TYPE_AWAIT).equals(momentsMsgBean.type)) {
            helper.setTag(R.id.moments_msgs_middle_area, R.id.await_user_id, momentsMsgBean.userId);
            helper.setTag(R.id.moments_msgs_right_area, R.id.await_user_id, momentsMsgBean.userId);
        } else if (MomentsBean.MSG_TYPE_UNSUPPORTED.equals(momentsMsgBean.type)) {
            helper.setVisibility(R.id.txt_update_now, View.VISIBLE);
            helper.setText(R.id.moments_msgs_commment_text, TheLApp.getContext().getString(R.string.info_version_outdate));
        } else {
            helper.setTag(R.id.moments_msgs_middle_area, R.id.comment_id, momentsMsgBean.momentsId);
            helper.setTag(R.id.moments_msgs_right_area, R.id.comment_id, momentsMsgBean.momentsId);
        }

        // 头像和昵称 是否匿名
        if (momentsMsgBean.secret == 1) {
            helper.setImageResource(R.id.moments_msgs_img_thumb, R.mipmap.icon_user_anonymous);
            helper.setText(R.id.moments_msgs_user_name, TheLApp.getContext().getString(R.string.moments_msgs_notification_anonymous));
        } else {
            helper.setText(R.id.moments_msgs_user_name, momentsMsgBean.nickname);
            helper.setImageUrl(R.id.moments_msgs_img_thumb, momentsMsgBean.avatar, TheLConstants.AVATAR_BIG_SIZE, TheLConstants.AVATAR_BIG_SIZE);
            helper.setTag(R.id.moments_msgs_left_area, R.id.userid_tag, momentsMsgBean.userId);
        }
        // 点赞类型
        if (momentsMsgBean.activityType.equals(MomentsMsgBean.MSG_TYPE_WINK)) {
            if (momentsMsgBean.winkCommentType > 0) {
                helper.setImageResource(R.id.moments_msgs_img_wink_type, TheLConstants.emojiImagsSmall[momentsMsgBean.winkCommentType - 1]);
            }
            helper.setVisibility(R.id.moments_msgs_img_wink_type, View.VISIBLE);
            helper.setVisibility(R.id.rel_text, View.GONE);
            // 评论类型
        } else if (momentsMsgBean.activityType.equals(MomentsMsgBean.MSG_TYPE_SAY)) {
            // 话题评论
            if (MomentsBean.MOMENT_TYPE_THEME.equals(momentsMsgBean.momentsType)) {
                helper.setImageResource(R.id.moments_msgs_img_wink_type, R.mipmap.icn_activity_topic);
            } else if (MomentsMsgBean.MSG_TYPE_RECOMMEND.equals(momentsMsgBean.commentType)) {
                helper.setImageResource(R.id.moments_msgs_img_wink_type, R.mipmap.icn_activity_recommend);
            } else {
                helper.setImageResource(R.id.moments_msgs_img_wink_type, R.mipmap.icn_activity_comment);
            }
            if (CommentBean.COMMENT_TYPE_STICKER.equals(momentsMsgBean.commentType)) {
                helper.setVisibility(R.id.img_sticker, View.VISIBLE);
            } else {
                helper.setText(R.id.moments_msgs_commment_text, EmojiUtils.getInstace(EmojiUtils.DEFAULT_SIZE).getExpressionString(TheLApp.getContext(), momentsMsgBean.commentText));
                helper.setVisibility(R.id.moments_msgs_commment_text, View.VISIBLE);
            }
            helper.setVisibility(R.id.rel_text, View.VISIBLE);
        } else if (momentsMsgBean.activityType.equals(MomentsMsgBean.MSG_TYPE_TO)) {
            // 回复类型
            helper.setImageResource(R.id.moments_msgs_img_wink_type, R.mipmap.icn_activity_comment);
            if (CommentBean.COMMENT_TYPE_STICKER.equals(momentsMsgBean.commentType)) {
                helper.setVisibility(R.id.img_sticker, View.VISIBLE);
            } else {
                helper.setText(R.id.moments_msgs_commment_text, EmojiUtils.getInstace(EmojiUtils.DEFAULT_SIZE).getExpressionString(TheLApp.getContext(), momentsMsgBean.commentText));
                helper.setVisibility(R.id.moments_msgs_commment_text, View.VISIBLE);
            }
            helper.setVisibility(R.id.rel_text, View.VISIBLE);
        } else if (momentsMsgBean.activityType.equals(MomentsMsgBean.MSG_TYPE_AT)) {
            helper.setImageResource(R.id.moments_msgs_img_wink_type, R.mipmap.icn_activity_emoji_at);
            if (momentsMsgBean.commentType.equals(MomentsMsgBean.COMMENT_TYPE_AT)) {
                helper.setText(R.id.moments_msgs_commment_text, TheLApp.getContext().getString(R.string.moments_msgs_mention));
            } else if (momentsMsgBean.commentType.equals(MomentsMsgBean.COMMENT_TYPE_AT_COMMENT)) {
                helper.setText(R.id.moments_msgs_commment_text, TheLApp.getContext().getString(R.string.moments_msgs_mention_comment));
            }
            helper.setVisibility(R.id.moments_msgs_commment_text, View.VISIBLE);
            helper.setVisibility(R.id.rel_text, View.VISIBLE);
        }

        // 发布时间
        helper.setText(R.id.moments_msgs_release_time, momentsMsgBean.getReleaseTimeShow());
        helper.setVisibility(R.id.img_play, View.GONE);

        // moment部分 2.20.0版本新增评论的回复类型
        if (MomentsMsgBean.TYPE_COMMENT_REPLY.equals(momentsMsgBean.type)) {
            if (CommentBean.COMMENT_TYPE_STICKER.equals(momentsMsgBean.momentsType)) {
                helper.setVisibility(R.id.my_moment_text, View.GONE);
                helper.setVisibility(R.id.my_moment_img, View.VISIBLE);
                helper.setText(R.id.my_moment_text, "");
            } else {
                helper.setVisibility(R.id.my_moment_text, View.VISIBLE);
                helper.setVisibility(R.id.my_moment_img, View.GONE);
                helper.setText(R.id.my_moment_text, EmojiUtils.getInstace(EmojiUtils.DEFAULT_SIZE).getExpressionString(TheLApp.getContext(), momentsMsgBean.momentsText));
            }
        } else if (MomentsBean.MOMENT_TYPE_TEXT.equals(momentsMsgBean.momentsType) || MomentsBean.MOMENT_TYPE_THEME.equals(momentsMsgBean.momentsType) || MomentsBean.MOMENT_TYPE_RECOMMEND.equals(momentsMsgBean.momentsType)) {
            // comment文字内容
            helper.setVisibility(R.id.my_moment_text, View.VISIBLE);
            helper.setVisibility(R.id.my_moment_img, View.GONE);
            helper.setText(R.id.my_moment_text, momentsMsgBean.momentsText);
            // 如果该评论的日志主体是话题回复
        } else if (MomentsBean.MOMENT_TYPE_THEME_REPLY.equals(momentsMsgBean.momentsType)) {
            // 如果有图片，则显示图片，没图片则显示文字
            if (!TextUtils.isEmpty(momentsMsgBean.thumbnailUrl)) {
                helper.setVisibility(R.id.my_moment_text, View.GONE);
                helper.setVisibility(R.id.my_moment_img, View.VISIBLE);
                helper.setText(R.id.my_moment_text, "");
                helper.setImageUrl(R.id.my_moment_img, momentsMsgBean.thumbnailUrl, TheLConstants.ICON_SMALL_SIZE, TheLConstants.ICON_SMALL_SIZE);
            } else {
                helper.setVisibility(R.id.my_moment_text, View.VISIBLE);
                helper.setVisibility(R.id.my_moment_img, View.GONE);
                helper.setText(R.id.my_moment_text, momentsMsgBean.momentsText);

            }
        } else if (momentsMsgBean.momentsType.equals(MomentsBean.MOMENT_TYPE_VOICE) || momentsMsgBean.momentsType.equals(MomentsBean.MOMENT_TYPE_TEXT_VOICE) || MomentsBean.MOMENT_TYPE_VIDEO.equals(momentsMsgBean.momentsType)) {
            helper.setVisibility(R.id.my_moment_text, View.GONE);
            helper.setVisibility(R.id.my_moment_img, View.VISIBLE);
            helper.setText(R.id.my_moment_text, "");
            helper.setVisibility(R.id.img_play, View.VISIBLE);
            if (MomentsBean.MOMENT_TYPE_VIDEO.equals(momentsMsgBean.momentsType)) {
                helper.setImageResource(R.id.img_play, R.mipmap.btn_play_video);
                helper.setImageUrl(R.id.my_moment_img, momentsMsgBean.thumbnailUrl, TheLConstants.ICON_SMALL_SIZE, TheLConstants.ICON_SMALL_SIZE);

            } else {
                helper.setImageResource(R.id.img_play, R.mipmap.btn_feed_play_big);
                helper.setImageUrl(R.id.my_moment_img, momentsMsgBean.albumLogo100, TheLConstants.ICON_SMALL_SIZE, TheLConstants.ICON_SMALL_SIZE);

            }
        } else {
            // comment图片内容

            if (!momentsMsgBean.momentsType.equals(MomentsBean.MSG_TYPE_UNSUPPORTED)) {

                helper.setVisibility(R.id.my_moment_text, View.GONE);
                helper.setVisibility(R.id.my_moment_img, View.VISIBLE);
                helper.setText(R.id.my_moment_text, "");
                helper.setImageUrl(R.id.my_moment_img, momentsMsgBean.thumbnailUrl, TheLConstants.ICON_SMALL_SIZE, TheLConstants.ICON_SMALL_SIZE);

            }

        }
        //期待直播
        if (momentsMsgBean.type != null && momentsMsgBean.type.equals(MomentsMsgBean.MSG_TYPE_AWAIT)) {
            helper.setImageResource(R.id.moments_msgs_img_wink_type, R.mipmap.icn_push_live);
            helper.setText(R.id.moments_msgs_commment_text, mContext.getString(R.string.expect_your_live));
            helper.setVisibility(R.id.moments_msgs_commment_text, View.VISIBLE);
            helper.setVisibility(R.id.moments_msgs_right_area, View.GONE);
        }
        final int position = helper.getLayoutPosition() - getHeaderLayoutCount();

        helper.setOnClickListener(R.id.moments_msgs_left_area, new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (jumpUserinfoListener != null) {
                    jumpUserinfoListener.jumpUserinfo(position, v);
                }
            }
        });
        helper.setOnClickListener(R.id.txt_update_now, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.checkUpdate(v.getContext());
            }
        });
        //期待直播
        if ((MomentsMsgBean.MSG_TYPE_AWAIT).equals(momentsMsgBean.type)) {
            helper.setOnClickListener(R.id.moments_msgs_middle_area, new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (momentListener != null) {

                        momentListener.directTo(position, v);
                    }
                }
            });

            helper.setOnClickListener(R.id.moments_msgs_right_area, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (momentListener != null) {

                        momentListener.directTo(position, v);
                    }
                }
            });

        } else {
            helper.setOnClickListener(R.id.moments_msgs_middle_area, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (momentListener != null) {

                        momentListener.directTo(position, v);
                    }
                }
            });
            helper.setOnClickListener(R.id.moments_msgs_middle_area, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    momentListener.directTo(position, v);

                }
            });
            helper.setOnClickListener(R.id.moments_msgs_right_area, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    momentListener.directTo(position, v);

                }
            });
        }

    }

    public void JumpUserinfoListener(JumpUserinfoListener jumpUserinfoListener) {
        this.jumpUserinfoListener = jumpUserinfoListener;
    }

    public void JumpMomentTypeListener(JumpMomentTypeListener momentListener) {
        this.momentListener = momentListener;
    }

    public interface JumpUserinfoListener {
        void jumpUserinfo(int position, View v);

    }

    public interface JumpMomentTypeListener {

        void directTo(int position, View v);
    }
}
