package com.thel.modules.live_classification;

import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

import com.thel.R;
import com.thel.base.BaseActivity;
import com.thel.bean.LiveClassificationBean;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.utils.HeaderAndFooterRecyclerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class LiveClassificationActivity extends BaseActivity {

    @BindView(R.id.live_classification_recycler)
    RecyclerView recyclerView;
    private LiveClassificationAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_classification);
        ButterKnife.bind(this);

        initList();

        initData();

    }

    @Override
    public boolean isStatusBarTranslucent() {
        return false;
    }

    private void initData() {
        RequestBusiness.getInstance().getLiveType().onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<LiveClassificationBean>() {
            @Override
            public void onNext(LiveClassificationBean data) {
                super.onNext(data);
                if (data != null && data.data != null)
                    adapter.setData(data.data);
            }
        });
    }

    private void initList() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        adapter = new LiveClassificationAdapter();
        HeaderAndFooterRecyclerAdapter headerAndFooterRecyclerAdapter = new HeaderAndFooterRecyclerAdapter(adapter);
        View header = LayoutInflater.from(this).inflate(R.layout.head_live_calssification_list, recyclerView, false);
        headerAndFooterRecyclerAdapter.addHeaderView(header);
        recyclerView.setAdapter(headerAndFooterRecyclerAdapter);
    }

    @OnClick(R.id.lin_back)
    void onclick() {
        finish();
    }
}
