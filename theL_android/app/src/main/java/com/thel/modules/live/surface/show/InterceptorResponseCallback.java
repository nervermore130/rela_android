package com.thel.modules.live.surface.show;

import com.thel.app.TheLApp;
import com.thel.chat.tlmsgclient.MsgPacket;
import com.thel.chat.tlmsgclient.ResponseCallback;
import com.thel.utils.GsonUtils;
import com.thel.utils.ToastUtils;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class InterceptorResponseCallback implements ResponseCallback {

    @Override public void onError(Throwable throwable) {

    }

    @Override public void onResponse(MsgPacket msgPacket) {

        if (msgPacket.getCode().equals("errorAlert")) {

            Flowable.just(msgPacket)
                    .onBackpressureDrop()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<MsgPacket>() {
                        @Override public void accept(MsgPacket msgPacket) {

                            String errMessage = new String(msgPacket.getPayload());

                            ErrorMessageBean errorMessageBean = GsonUtils.getObject(errMessage, ErrorMessageBean.class);

                            ToastUtils.showToastShort(TheLApp.context, errorMessageBean.errdesc);

                        }
                    });

        }

    }

    public class ErrorMessageBean {

        public String errdesc;

    }
}
