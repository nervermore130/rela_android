package com.thel.modules.live.view;

import android.app.Dialog;
import android.content.Context;
import androidx.annotation.NonNull;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;

import com.thel.R;
import com.thel.modules.live.in.LiveTextSizeChangedListener;
import com.thel.utils.AppInit;
import com.thel.utils.SharedPrefUtils;

/**
 * Created by waiarl on 2017/11/3.
 */

public class LiveTextSizeDialog extends Dialog implements LiveTextSizeChangedListener {
    private final Context mContext;
    private View img_standard;
    private View img_large;
    private View img_biggest;
    private View txt_confirm;
    private int selectSize;

    public LiveTextSizeDialog(@NonNull Context context) {
        this(context, R.style.CustomDialogBottom);
    }

    public LiveTextSizeDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        mContext = context;
        init();
    }

    private void init() {
        final View view = LayoutInflater.from(mContext).inflate(R.layout.live_show_capture_textsize_dialog, null);
        setContentView(view);
        img_standard = view.findViewById(R.id.img_standard);
        img_large = view.findViewById(R.id.img_large);
        img_biggest = view.findViewById(R.id.img_biggest);
        txt_confirm = view.findViewById(R.id.txt_confirm);
    }

    public LiveTextSizeDialog initDialog(final LiveTextSizeChangedListener listener) {
        final int mTextSize = SharedPrefUtils.getInt(SharedPrefUtils.FILE_CACHE, SharedPrefUtils.LIVE_SHOW_CAPTURE_TEXTSIZE, TEXT_SIZE_STANDARD);
        setViewSelected(img_standard, img_large, img_biggest, mTextSize);
        selectSize = mTextSize;
        img_standard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectSize = TEXT_SIZE_STANDARD;
                setViewSelected(img_standard, img_large, img_biggest, selectSize);
            }
        });
        img_large.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectSize = TEXT_SIZE_LARGE;
                setViewSelected(img_standard, img_large, img_biggest, selectSize);

            }
        });
        img_biggest.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                selectSize = TEXT_SIZE_BIGGEST;
                setViewSelected(img_standard, img_large, img_biggest, selectSize);
            }
        });
        txt_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null && selectSize != mTextSize) {
                    listener.textSizeChanged(selectSize);
                }
                dismiss();
            }
        });
        show();
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.gravity = Gravity.BOTTOM;
        lp.width = AppInit.displayMetrics.widthPixels; // 设置宽度
        getWindow().setAttributes(lp);
        return this;
    }

    private void setViewSelected(View img_standard, View img_large, View img_biggest, int mTextSize) {
        img_standard.setSelected(TEXT_SIZE_STANDARD == mTextSize);
        img_large.setSelected(TEXT_SIZE_LARGE == mTextSize);
        img_biggest.setSelected(TEXT_SIZE_BIGGEST == mTextSize);
    }

    @Override
    public void textSizeChanged(int type) {

    }
}
