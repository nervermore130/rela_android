package com.thel.wxapi;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.tencent.mm.opensdk.constants.ConstantsAPI;
import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.modelbiz.WXLaunchMiniProgram;
import com.tencent.mm.opensdk.modelmsg.SendAuth;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.WXLoginBean;
import com.thel.bean.WXUserInfoBean;
import com.thel.constants.TheLConstants;
import com.thel.modules.login.login_register.WeChatLogin;
import com.thel.utils.DialogUtil;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.ToastUtils;
import com.umeng.analytics.MobclickAgent;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by setsail on 15/5/12.
 */
//public class WXEntryActivity extends WXCallbackActivity implements IWXAPIEventHandler {
public class WXEntryActivity extends Activity implements IWXAPIEventHandler {

    public static final String TAG = "WXEntryActivity";

    private IWXAPI api;

    private String code;

    private String state;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        api = WXAPIFactory.createWXAPI(TheLApp.context, TheLConstants.WX_APP_ID, false);
        api.handleIntent(getIntent(), this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(this.getClass().getName());
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(this.getClass().getName());
        MobclickAgent.onPause(this);
    }

    @Override
    public void onReq(BaseReq arg0) {
    }

    @Override
    public void onResp(BaseResp resp) {

        if (ConstantsAPI.COMMAND_SENDAUTH == resp.getType()) {// 微信登录授权
            switch (resp.errCode) {
                case BaseResp.ErrCode.ERR_OK:

                    MobclickAgent.onEvent(WXEntryActivity.this, "wechat_login_response_ok");// 微信登录回调成功
                    code = ((SendAuth.Resp) resp).code;
                    state = ((SendAuth.Resp) resp).state;

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            loadWXData();
                        }
                    }).start();
                    break;

                case BaseResp.ErrCode.ERR_USER_CANCEL:
                    MobclickAgent.onEvent(WXEntryActivity.this, "user_cancel_wx_login");// 用户主动取消微信授权登录
                    Intent intent = new Intent();
                    intent.setAction(TheLConstants.BROADCAST_WX_LOGIN_CANCEL);
                    TheLApp.context.sendBroadcast(intent);
                    WXEntryActivity.this.finish();
                    break;
                case BaseResp.ErrCode.ERR_COMM:
                    MobclickAgent.onEvent(WXEntryActivity.this, "wx_login_error");// 用户微信账号被挤掉了
                    Intent inten = new Intent();
                    inten.setAction(TheLConstants.BROADCAST_WX_LOGIN_CANCEL);
                    TheLApp.context.sendBroadcast(inten);
                    Toast.makeText(this, resp.errStr + "", Toast.LENGTH_SHORT).show();
                    WXEntryActivity.this.finish();
                    break;

                default:
                    MobclickAgent.onEvent(WXEntryActivity.this, "wx_login_error");// 微信登录失败
                    Toast.makeText(this, resp.errCode + "", Toast.LENGTH_SHORT).show();
                    WXEntryActivity.this.finish();
                    break;
            }
        } else if (ConstantsAPI.COMMAND_UNKNOWN == resp.getType()) {
            ToastUtils.showToastShort(this, "error code :" + resp.getType());
            WXEntryActivity.this.finish();
            WeChatLogin.getInstance().cancel();
        } else if (resp.getType() == ConstantsAPI.COMMAND_LAUNCH_WX_MINIPROGRAM) {//微信小程序
            WXLaunchMiniProgram.Resp launchMiniProResp = (WXLaunchMiniProgram.Resp) resp;
            String extraData = launchMiniProResp.extMsg; // 对应JsApi navigateBackApplication中的extraData字段数据
        } else {// 微信分享（消息和朋友圈）
            Intent intent = new Intent();
            if (BaseResp.ErrCode.ERR_OK == resp.errCode) {// 分享成功
                intent.putExtra("result", "succeed");
            } else {
                intent.putExtra("result", "fail");
            }

            WXEntryActivity.this.finish();

            intent.setAction(TheLConstants.BROADCAST_WX_SHARE);
            TheLApp.context.sendBroadcast(intent);
        }
    }

    int sex;

    String nickName;

    String avatar;

    private void loadWXData() {
        //上面的code就是接入指南里要拿到的code

        String url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=" + TheLConstants.WX_APP_ID + "&secret=" + TheLConstants.WX_SECRET + "&code=" + code + "&grant_type=authorization_code";

        final String result = executeHttpGet(url);

        ShareFileUtils.setWxLoginToken(result);

        WXLoginBean wxLoginBean = GsonUtils.getObject(result, WXLoginBean.class);

//        if (wxLoginBean != null && wxLoginBean.errcode != null) {
//            Log.d(TAG, "wxLoginBean errcode null");
//            return;
//        }

        if (wxLoginBean == null || TextUtils.isEmpty(wxLoginBean.access_token) || TextUtils.isEmpty(wxLoginBean.openid) || TextUtils.isEmpty(wxLoginBean.unionid)) {
            return;
        }

        final String access_token = wxLoginBean.access_token;
        final String openid = wxLoginBean.openid;
        final String unionid = wxLoginBean.unionid;
        nickName = "";
        avatar = "";
        sex = 2;

        MobclickAgent.onEvent(WXEntryActivity.this, "wechat_login_access_token");// 微信注册转化率统计：开始获取token

        String userInfo = executeHttpGet("https://api.weixin.qq.com/sns/userinfo?access_token=" + access_token + "&openid=" + openid);

        ShareFileUtils.setWxLoginTime();

        MobclickAgent.onEvent(WXEntryActivity.this, "wechat_auth_succeed");// 微信注册转化率统计：微信获取token成功

        if (!TextUtils.isEmpty(userInfo)) {

            ShareFileUtils.setWxLoginUserInfo(userInfo);

            WXUserInfoBean wxUserInfoBean = GsonUtils.getObject(userInfo, WXUserInfoBean.class);
            if (wxUserInfoBean.errcode != null) {
                return;
            }
            // 微信性别：1男性 2女性
            sex = wxUserInfoBean.sex;
            nickName = !TextUtils.isEmpty(wxUserInfoBean.nickname) ? wxUserInfoBean.nickname : "";
            avatar = !TextUtils.isEmpty(wxUserInfoBean.headimgurl) ? wxUserInfoBean.headimgurl : "";

        }

        WXEntryActivity.this.runOnUiThread(new Runnable() {
            @Override public void run() {
                // 如果是男性
                if (sex == 1) {
                    MobclickAgent.onEvent(TheLApp.context, "wechat_male_login");// 微信登录性别是男性打点
                    final DialogInterface.OnClickListener listener0 = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            WeChatLogin.getInstance().cancel();
                            dialog.dismiss();
                            WXEntryActivity.this.finish();

                        }
                    };
                    final DialogInterface.OnClickListener listener1 = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            MobclickAgent.onEvent(TheLApp.context, "check_i_am_a_lady");// 点击我是女生按钮

                            // 发送微信登录/绑定成功的广播

                            if ("thel_login".equals(state)) {// 登录
                                WeChatLogin.getInstance().checkSex(openid, access_token, unionid, avatar, nickName, 0);
                            } else if ("thel_link".equals(state)) {// 绑定
                                Intent intent = new Intent();
                                intent.putExtra("sex", sex);
                                intent.putExtra("access_token", access_token);
                                intent.putExtra("openid", openid + "," + unionid);
                                intent.setAction(TheLConstants.BROADCAST_LINK_WX_SUCCEED);
                                TheLApp.context.sendBroadcast(intent);
                            }
                            WXEntryActivity.this.finish();
                        }
                    };

                    DialogUtil.showConfirmDialog(WXEntryActivity.this, "", TheLApp.context.getString(R.string.register_activity_test_hint), TheLApp.context.getString(R.string.i_am_a_lady), TheLApp.context.getString(R.string.info_quit), listener1, listener0);
                } else {
                    // 发送微信登录/绑定成功的广播

                    if ("thel_login".equals(state)) {// 登录
                        WeChatLogin.getInstance().checkSex(openid, access_token, unionid, avatar, nickName, 0);
                    } else if ("thel_link".equals(state)) {// 绑定
                        Intent intent = new Intent();
                        intent.putExtra("sex", 0);
                        intent.putExtra("access_token", access_token);
                        intent.putExtra("openid", openid + "," + unionid);
                        intent.setAction(TheLConstants.BROADCAST_LINK_WX_SUCCEED);
                        TheLApp.context.sendBroadcast(intent);

                    }
                    WXEntryActivity.this.finish();

                }
            }
        });

    }

    public String executeHttpGet(String str) {
        L.i(TAG, "granted:" + " str " + str);
        String result = null;
        try {
            URL url = new URL(str);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setConnectTimeout(3000);
            int code = conn.getResponseCode();
            if (code == 200) {
                InputStream is = conn.getInputStream();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                int len = 0;
                byte[] bys = new byte[1024];
                while ((len = is.read(bys)) != -1) {
                    baos.write(bys, 0, len);
                    result = new String(baos.toByteArray());
                    L.i(TAG, "granted:" + result);
                }
                baos.close();
                is.close();
            } else {
                L.i(TAG, "联网失败");
            }
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }

        return result;
    }
}
