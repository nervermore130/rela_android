package com.thel.wxapi;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.rela.pay.PayConstants;
import com.rela.pay.PayProxyImpl;
import com.rela.pay.R;
import com.rela.pay.wxpay.WXPayUtils;
import com.tencent.mm.opensdk.constants.ConstantsAPI;
import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

public class WXPayEntryActivity extends Activity implements IWXAPIEventHandler {

    private static final String TAG = "MicroMsg.SDKSample.WXPayEntryActivity";

    public static final String BROADCAST_PAY = "wx_broadcast_pay";

    private IWXAPI api;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wx_pay_result);

        Log.d("WXPayment", " WXPayEntryActivity onCreate : ");

        api = WXAPIFactory.createWXAPI(this, "wx3c1d8fb1e81ab49c");

        api.handleIntent(getIntent(), this);

        registerReceiver();

    }


    private PayResultReceiver payResultReceiver;

    // 注册广播接收器
    private void registerReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BROADCAST_PAY);
        payResultReceiver = new PayResultReceiver();
        registerReceiver(payResultReceiver, intentFilter);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        api.handleIntent(intent, this);
    }

    @Override
    public void onReq(BaseReq req) {
    }

    @Override
    public void onResp(BaseResp resp) {

        Log.d("WXPayment", " resp.getType(): " + resp.getType());

        Log.d("WXPayment", " resp.errCode(): " + resp.errCode);

        if (resp.getType() == ConstantsAPI.COMMAND_PAY_BY_WX) {
            switch (resp.errCode) {
                case 0:
                    if (PayProxyImpl.getInstance().mOnPayStatusListener != null) {
                        PayProxyImpl.getInstance().mOnPayStatusListener.onPayStatus(PayConstants.PAY_STATUS_SUCCESS, null);
                    }
                    break;
                case -1:
                    Toast.makeText(this, R.string.wx_pay_failed, Toast.LENGTH_LONG).show();
                    if (PayProxyImpl.getInstance().mOnPayStatusListener != null) {
                        PayProxyImpl.getInstance().mOnPayStatusListener.onPayStatus(PayConstants.PAY_STATUS_FAILED, "-1");
                    }
                    break;
                case -2:
                    Toast.makeText(this, R.string.wx_pay_canceled, Toast.LENGTH_LONG).show();
                    if (PayProxyImpl.getInstance().mOnPayStatusListener != null) {
                        PayProxyImpl.getInstance().mOnPayStatusListener.onPayStatus(PayConstants.PAY_STATUS_FAILED, "-2");
                    }
                    break;
                default:
                    break;
            }
            // 发送微信支付成功的广播
            Intent intent = new Intent();
            intent.setAction(BROADCAST_PAY);
            intent.putExtra(WXPayUtils.WXPAY_TAG_KEY, WXPayUtils.WXPAY_TAG_VALUE);
            if (resp.errCode == 0) {
                intent.putExtra("result", "succeed");
            } else {
                intent.putExtra("result", "fail");
                intent.putExtra("errCode", resp.errCode);
            }
            sendBroadcast(intent);
            WXPayUtils.WXPAY_TAG_VALUE = "";
            finish();
        }
    }

    // 微信支付成功接收器
    private class PayResultReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            Log.d("WXPayment", " PayResultReceiver : ");

            if (intent != null) {

                String result = intent.getStringExtra("result");

                int errCode = intent.getIntExtra("errCode", 0);

                Log.d("WXPayment", " result : " + result);

                if (TextUtils.equals(result, "succeed")) {
                    if (PayProxyImpl.getInstance().mOnPayStatusListener != null) {
                        PayProxyImpl.getInstance().mOnPayStatusListener.onPayStatus(PayConstants.PAY_STATUS_SUCCESS, null);
                    }
                }
                switch (errCode) {
                    case -1:
                        if (PayProxyImpl.getInstance().mOnPayStatusListener != null) {
                            PayProxyImpl.getInstance().mOnPayStatusListener.onPayStatus(PayConstants.PAY_STATUS_FAILED, "-1");
                        }
                        break;
                    case -2:
                        if (PayProxyImpl.getInstance().mOnPayStatusListener != null) {
                            PayProxyImpl.getInstance().mOnPayStatusListener.onPayStatus(PayConstants.PAY_STATUS_FAILED, "-2");
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }


    @Override protected void onDestroy() {
        super.onDestroy();
        if (payResultReceiver != null) {
            try {
                unregisterReceiver(payResultReceiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}