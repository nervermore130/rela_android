package com.thel.service.chat;

import android.content.Intent;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;

import com.thel.IMainProcessCallback;
import com.thel.app.TheLApp;
import com.thel.bean.AuthReportBean;
import com.thel.bean.ReceiptMsgBean;
import com.thel.bean.message.AuthTokenBean;
import com.thel.callback.IChatClient;
import com.thel.chat.netty.MsgPacket;
import com.thel.chat.netty.NettyConstants;
import com.thel.chat.netty.ResponseCallback;
import com.thel.constants.TheLConstants;
import com.thel.db.DBConstant;
import com.thel.db.DBManager;
import com.thel.db.DBUtils;
import com.thel.manager.ChatServiceManager;
import com.thel.modules.main.messages.bean.MsgBean;
import com.thel.modules.main.messages.utils.MsgUtils;
import com.thel.modules.main.messages.utils.ZIPUtils;
import com.thel.utils.AppInit;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;
import com.thel.utils.MD5Utils;
import com.thel.utils.PhoneUtils;
import com.thel.utils.ShareFileUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

import static com.thel.db.DBUtils.getOtherUserId;


/**
 * @author liuyun
 * @date 2018/3/19
 */

public class ChatClientImpl implements IChatClient {

    private static final String TAG = "ChatClientImpl";

    private boolean isAutoPing = false;

    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    private Disposable syncMsgDisposable;

    private Disposable autoPingDisposable;

    private Disposable cacheDisposable;

    private ChatService.ChatServiceBinder chatServiceBinder;

    private List<MsgBean> msgBeanList = new ArrayList<>();

    private List<MsgBean> msgCache = new Vector<>();

    private String userId = null;

    private IMainProcessCallback mIMainProcessCallback;

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public void setBinder(ChatService.ChatServiceBinder chatServiceBinder) {
        this.chatServiceBinder = chatServiceBinder;
        DBManager.getInstance().setBinder(chatServiceBinder);
    }

    public void setMainProcessCallback(IMainProcessCallback mIMainProcessCallback) {
        this.mIMainProcessCallback = mIMainProcessCallback;
    }

    @Override
    public synchronized void authMsg() {

        if (chatServiceBinder == null) {
            return;
        }

        if (chatServiceBinder.getNettyClient() == null) {
            chatServiceBinder.reconnect();
        }

        L.d("NettyClient", " -----------------authMsg--------------- ");

        isAutoPing = false;

        L.d("NettyClient", " authMsg authTokenBean.token : " + DBManager.getInstance().getToken());

        L.d("NettyClient", " authMsg authTokenBean.key : " + DBManager.getInstance().getKey());

        L.d("NettyClient", " authMsg authTokenBean.cursor : " + DBManager.getInstance().getMsgCursor());

        Map<String, String> map = MD5Utils.getMsgAuthMap(DBManager.getInstance().getToken(), DBManager.getInstance().getKey());

        final String body = GsonUtils.createJsonString(map);

        L.d("NettyClient", " authMsg body : " + body);

        try {
            if (mIMainProcessCallback != null) {
                mIMainProcessCallback.umengPushStatus(7, null, null);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        chatServiceBinder.getNettyClient().requestMsg(TheLConstants.MsgTypeConstant.MSG_TYPE_AUTH_V1, body, new com.thel.chat.netty.ResponseCallback() {

            @Override
            public void onError(Throwable throwable) {
                try {
                    if (mIMainProcessCallback != null && chatServiceBinder != null) {
                        mIMainProcessCallback.umengPushStatus(4, "auth_timeout", chatServiceBinder.getChatIp());
                    }
                } catch (RemoteException e) {
                    e.printStackTrace();
                }

                L.d("NettyClient", " authMsg onError : ");

                if (chatServiceBinder != null) {
                    chatServiceBinder.pushStatus(ChatService.ConnectStatus.ERROR);
                }

                if (chatServiceBinder != null) {

                    chatServiceBinder.pushStatus(ChatService.ConnectStatus.FAILED);

                    chatServiceBinder.reconnect();
                }

            }

            @Override
            public void onResponse(MsgPacket msgPacket) {

                L.d("NettyClient", " authMsg onResponse msgPacket code : " + msgPacket.code);

                if (TheLConstants.MsgTypeConstant.MSG_TYPE_OK.equals(msgPacket.code)) {
                    L.d(TAG, "消息服务连接成功");
                    try {
                        if (mIMainProcessCallback != null) {
                            mIMainProcessCallback.umengPushStatus(8, null, null);
                        }

                        if (isFirstReport) {

                            authReport();

                        }

                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }

                    if (chatServiceBinder != null) {
                        chatServiceBinder.pushStatus(ChatService.ConnectStatus.CONNECTED);
                        chatServiceBinder.onChatInfo();
                    }
                    startAutoPingTimer();

                } else {
                    if (TheLConstants.MsgTypeConstant.ERR_TOKEN.equals(msgPacket.code)) {

                        String payload = new String(msgPacket.payload);

                        AuthTokenBean authTokenBean = GsonUtils.getObject(payload, AuthTokenBean.class);

                        L.d(TAG, " authMsg onResponse token : " + authTokenBean.token);

                        if (authTokenBean.token != null && authTokenBean.token.length() > 0) {

                            DBManager.getInstance().setToken(authTokenBean.token);

                            authMsg();

                        }

                    } else {
                        Log.d(TAG, "createClient,request,onResponse,autoReconnect");
                        String t = new String(msgPacket.payload);
                        Log.d(TAG, "getPayload =" + t);

                        if (chatServiceBinder != null) {

                            chatServiceBinder.pushStatus(ChatService.ConnectStatus.FAILED);

                            chatServiceBinder.reconnect();
                        }

                    }

                    try {
                        if (mIMainProcessCallback != null && chatServiceBinder != null) {
                            mIMainProcessCallback.umengPushStatus(3, msgPacket.code, chatServiceBinder.getChatIp());
                        }
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }

                }
            }
        });

    }

    @Override
    public synchronized void syncMsg(int type) {

        if (chatServiceBinder == null) {
            return;
        }

        if (chatServiceBinder.getNettyClient() == null) {
            chatServiceBinder.reconnect();
        }

        L.d(TAG, " syncMsg logType " + type);

        String syncData;

        try {
            long cursor = Long.valueOf(DBManager.getInstance().getMsgCursor());

            syncData = new JSONObject().put(TheLConstants.MsgTypeConstant.MSG_TYPE_LAST_SYNC, cursor).toString();
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        L.d(TAG, " syncMsg data" + syncData);

        final String finalSyncData = syncData;

        final long syncStartTime = System.currentTimeMillis();

        chatServiceBinder.getNettyClient().requestMsg(TheLConstants.MsgTypeConstant.MSG_TYPE_SYNC, finalSyncData, new com.thel.chat.netty.ResponseCallback() {
            @Override
            public void onError(Throwable throwable) {
                L.d(TAG, " syncMsg onError ");

                if (chatServiceBinder != null) {

                    chatServiceBinder.pushStatus(ChatService.ConnectStatus.FAILED);

                    chatServiceBinder.reconnect();
                }

            }

            @Override
            public void onResponse(MsgPacket msgPacket) {

                L.d(TAG, " syncMsg onResponse " + msgPacket.code);

                processOnResponse(msgPacket, syncStartTime);

                if (msgPacket.code.equals(TheLConstants.MsgCodeConstants.MSG_CODE_NO_SUCH_SESSION)) {

                    if (chatServiceBinder != null) {

                        chatServiceBinder.pushStatus(ChatService.ConnectStatus.FAILED);

                        chatServiceBinder.reconnect();
                    }

                }

            }
        });
    }

    @Override
    public synchronized void pingMsg() {

        L.d("NettyClient", " --------- pingMsg ---------- ");

        if (chatServiceBinder == null) {
            return;
        }

        if (chatServiceBinder.getNettyClient() == null) {
            chatServiceBinder.reconnect();
        }

        chatServiceBinder.getNettyClient().requestMsg(TheLConstants.MsgTypeConstant.MSG_TYPE_PING, "", new com.thel.chat.netty.ResponseCallback() {

            @Override
            public void onError(Throwable throwable) {

                L.d("NettyClient", " pingMsg onError : ");
                try {
                    if (mIMainProcessCallback != null && chatServiceBinder != null) {
                        mIMainProcessCallback.umengPushStatus(1, null, chatServiceBinder.getChatIp());
                        mIMainProcessCallback.umengPushStatus(9, null, null);

                    }

                    if (chatServiceBinder != null) {

                        chatServiceBinder.pushStatus(ChatService.ConnectStatus.FAILED);

                        chatServiceBinder.reconnect();
                    }

                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onResponse(MsgPacket msgPacket) {

                if (!TheLConstants.MsgTypeConstant.MSG_TYPE_PONG.equals(msgPacket.code)) {
                    // ping不通，重连
                    L.d(TAG, "ping不通，重连,onResponse，code=" + msgPacket.code);
                    try {
                        if (mIMainProcessCallback != null && chatServiceBinder != null) {
                            mIMainProcessCallback.umengPushStatus(1, null, chatServiceBinder.getChatIp());
                            mIMainProcessCallback.umengPushStatus(10, null, null);

                        }
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }

                    if (chatServiceBinder != null) {
                        chatServiceBinder.reconnect();
                    }
                } else {
                    if (!TheLConstants.MsgTypeConstant.MSG_TYPE_PONG.equals(msgPacket.code)) {
                        // ping不通，重连
                        L.d(TAG, "ping不通，重连,onResponse，code=" + msgPacket.code);
                        //MobclickAgent.onEvent(TheLApp.context, "im_msg_fail", "msg_ping_ip_fail_android"); //  Android ping IP失败
                        try {
                            if (mIMainProcessCallback != null && chatServiceBinder != null) {
                                mIMainProcessCallback.umengPushStatus(1, null, chatServiceBinder.getChatIp());
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                        if (chatServiceBinder != null) {
                            chatServiceBinder.reconnect();
                        }
                    } else {
                        L.d(TAG, "ping成功,t=");
                        if (chatServiceBinder != null) {

                        }
                        ChatServiceManager.getInstance().pushStatus(ChatService.ConnectStatus.CONNECTED);
                    }
                }

                L.d("NettyClient", " pingMsg onResponse : " + msgPacket.code);

            }
        });

    }

    @Override
    public synchronized void sendMsg(final MsgBean msgBean) {


        if (chatServiceBinder == null) {
            return;
        }

        if (chatServiceBinder.getNettyClient() == null) {
            chatServiceBinder.reconnect();
        }

        String code = "sendto_v1:user:" + msgBean.toUserId + ":" + msgBean.packetId + ":" + MsgBean.TYPE_MSG;

        L.d("NettyClient", " sendMsg code : " + code);

        String content = GsonUtils.createJsonString(msgBean);

        L.d("NettyClient", " sendMsg content : " + content);

        chatServiceBinder.getNettyClient().requestMsg(code, content, new com.thel.chat.netty.ResponseCallback() {

            @Override
            public void onError(Throwable throwable) {

                L.d("NettyClient", " sendMsg onError : ");

                msgBean.msgStatus = TheLConstants.MsgSendingStatusConstants.MSG_FAILED;

                String userId = getOtherUserId(msgBean);

                if (userId != null && userId.length() > 0 && msgBean.packetId != null && msgBean.packetId.length() > 0) {

                    DBManager.getInstance().updateMsgSendingStatus(msgBean);
                }

                if (chatServiceBinder != null) {

                    chatServiceBinder.pushStatus(ChatService.ConnectStatus.FAILED);

                    chatServiceBinder.reconnect();
                }


            }

            @Override
            public void onResponse(MsgPacket msgPacket) {

                L.d("NettyClient", " sendMsg onResponse : " + msgPacket.code);

                String code = msgPacket.code;

                if (code.equals(TheLConstants.MsgCodeConstants.MSG_CODE_OK)) {

                    msgBean.msgStatus = TheLConstants.MsgSendingStatusConstants.MSG_SUCCESS;

                }

                if (code.equals(TheLConstants.MsgCodeConstants.MSG_CODE_ZERO)) {

                    msgBean.msgStatus = TheLConstants.MsgSendingStatusConstants.MSG_FAILED;

                }

                if (code.equals(TheLConstants.MsgCodeConstants.MSG_CODE_ERROR)) {

                    msgBean.msgStatus = TheLConstants.MsgSendingStatusConstants.MSG_ILLEGAL;

                }

                if (code.equals(TheLConstants.MsgCodeConstants.MSG_CODE_NO_SUCH_SESSION)) {

                    if (chatServiceBinder != null) {

                        chatServiceBinder.pushStatus(ChatService.ConnectStatus.FAILED);

                        chatServiceBinder.reconnect();
                    }

                }

                String userId = getOtherUserId(msgBean);

                L.d("ChatClientImpl", " sendMsg updateMsgSendingStatus userId : " + userId);

                L.d("ChatClientImpl", " sendMsg updateMsgSendingStatus msgBean.packetId : " + msgBean.packetId);

                DBManager.getInstance().updateMsgSendingStatus(msgBean);

                if (chatServiceBinder != null) {
                    chatServiceBinder.pushMsgSendStatus(msgBean);
                }

            }

        });

    }

    @Override
    public synchronized void sendReceiptMsg(final MsgBean msgBean) {

        if (chatServiceBinder == null) {
            return;
        }

        if (chatServiceBinder.getNettyClient() == null) {
            chatServiceBinder.reconnect();
        }

        String requestStr = "sendto:user:" + msgBean.fromUserId + ":" + msgBean.packetId + ":" + MsgBean.TYPE_RECEIVED;

        String receiptStr = msgBean.buildReceipt();

        chatServiceBinder.getNettyClient().requestMsg(requestStr, receiptStr, new ResponseCallback() {
            @Override
            public void onError(Throwable throwable) {

                if (chatServiceBinder != null) {

                    chatServiceBinder.pushStatus(ChatService.ConnectStatus.FAILED);

                    chatServiceBinder.reconnect();
                }

            }

            @Override
            public void onResponse(MsgPacket msgPacket) {

                if ("OK".equals(msgPacket.code)) {

                    msgBean.msgStatus = TheLConstants.MsgSendingStatusConstants.MSG_READ;

                    msgBean.hadRead = 1;

                    DBManager.getInstance().updateMsgSendingStatus(msgBean);

                    if (chatServiceBinder != null) {
                        chatServiceBinder.pushMsgSendStatus(msgBean);
                    }

                }
            }
        });

    }


    @Override
    public synchronized void sendPendingMsg(final String toUserId, final String content) {

        if (chatServiceBinder == null) {
            return;
        }

        if (chatServiceBinder.getNettyClient() == null) {
            chatServiceBinder.reconnect();
        }

        chatServiceBinder.getNettyClient().requestMsg(TheLConstants.MsgTypeConstant.MSG_TYPE_DIRECT_TO + toUserId, content, new ResponseCallback() {
            @Override
            public void onError(Throwable throwable) {

                L.d(TAG, " sendReceiptMsg onError : ");

            }

            @Override
            public void onResponse(MsgPacket msgPacket) {
                if ("OK".equals(msgPacket.code)) {

                }
            }
        });
    }

    public void clear() {
        if (mCompositeDisposable != null) {
            if (syncMsgDisposable != null) {
                mCompositeDisposable.remove(syncMsgDisposable);
            }
            if (autoPingDisposable != null) {
                mCompositeDisposable.remove(autoPingDisposable);
            }

            if (cacheDisposable != null) {
                mCompositeDisposable.remove(cacheDisposable);
            }
            mCompositeDisposable.clear();
        }
    }

    private void startAutoPingTimer() {

        L.d("NettyClient", " startAutoPingTimer " + isAutoPing);

        if (isAutoPing || PhoneUtils.getNetWorkType() == PhoneUtils.TYPE_NO) {
            return;
        }

        mCompositeDisposable.clear();

        L.d("NettyClient", " startAutoPingTimer 2 ");

        isAutoPing = true;

        autoPingDisposable = Flowable.interval(0, 25, TimeUnit.SECONDS).onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Long>() {
            @Override
            public void accept(Long aLong) {

                L.d("NettyClient", " auto ping ");

                pingMsg();
            }
        });

        syncMsgDisposable = Flowable.interval(0, TheLConstants.SyncMsgType.SYNC_MSG_DELAY_TIME, TimeUnit.SECONDS).onBackpressureDrop().subscribeOn(Schedulers.io()).subscribe(new Consumer<Long>() {
            @Override
            public void accept(Long aLong) {

                syncMsg(TheLConstants.SyncMsgType.SYNC_MSG_LOOP);

            }
        });

        cacheDisposable = Flowable.interval(0, 200, TimeUnit.MILLISECONDS).onBackpressureDrop().subscribeOn(Schedulers.io()).subscribe(new Consumer<Long>() {
            @Override
            public void accept(Long aLong) {
                try {

                    saveCacheMsgToDB();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        mCompositeDisposable.add(syncMsgDisposable);

        mCompositeDisposable.add(autoPingDisposable);

        mCompositeDisposable.add(cacheDisposable);


    }

    private synchronized void saveCacheMsgToDB() {

        if (msgCache.size() == 0) {
            return;
        }

        ListIterator<MsgBean> iterable = msgCache.listIterator();

        L.d(TAG, " msgCache : " + msgCache.size());

        while (iterable.hasNext()) {

            L.d(TAG, " msgCache chatServiceBinder : " + chatServiceBinder);

            L.d(TAG, " msgCache : " + 1);

            MsgBean msgBean = iterable.next();

            MsgBean mb = DBManager.getInstance().getMsgByUserIdAndPacketId(DBUtils.getOtherUserId(msgBean), msgBean.packetId);

            L.d(TAG, " msgCache mb : " + mb);

            if (mb == null) {

                msgBean.msgStatus = TheLConstants.MsgSendingStatusConstants.MSG_SENDING;

                L.d(TAG, " msgCache msgBean.msgStatus : " + msgBean.msgStatus);

                //// 是否读过: 0新消息, 1读过了, 2读过并发了回执 默认未读
                int hasRead;

                String otherUserId = DBUtils.getOtherUserId(msgBean);

                L.d(TAG, " msgCache otherUserId : " + otherUserId);

                L.d(TAG, " msgCache this.userId : " + this.userId);

                if (otherUserId.equals(this.userId)) {
                    hasRead = 1;
                } else {
                    hasRead = 0;
                }

                msgBean.msgStatus = TheLConstants.MsgSendingStatusConstants.MSG_SUCCESS;

                L.d(TAG, " msgCache hasRead : " + hasRead);

                DBManager.getInstance().insertNewMsg(msgBean, DBUtils.getChatTableName(msgBean), hasRead);

                if (chatServiceBinder != null) {
                    if (hasRead == 1) {
                        chatServiceBinder.pushNewMsgComing(msgBean);
                    }
                }
            }

            iterable.remove();
        }
    }

    private synchronized void clearMsgCache() {

        if (msgCache != null) {
            msgCache.clear();
        }
    }

    private synchronized void processOnResponse(MsgPacket packet, long syncStartTime) {

        L.d(TAG, " processOnResponse packet code : " + packet.code);

        if (packet.code != null && packet.code.startsWith(TheLConstants.MsgTypeConstant.MSG_TYPE_OK)) {

            //OK,10000071,nomore,75%,1
            String[] stringArr = packet.code.split(",");

            boolean isHasData = stringArr.length > 0 && TheLConstants.MsgTypeConstant.MSG_TYPE_OK.equals(stringArr[0]);

            if (isHasData) {

                String data = ZIPUtils.unZipByteToString(packet.payload, NettyConstants.MSG_BUFFER_SIZE);

                L.d(TAG, " processOnResponse data : " + data);

                if (data != null) {
                    JSONArray jsonArray = null;
                    try {
                        jsonArray = new JSONArray(data);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (jsonArray != null && jsonArray.length() > 0) {

                        syncReport(syncStartTime);

                        msgBeanList.clear();

                        StringBuilder stringBuilder = new StringBuilder();

                        for (int i = 0; i < jsonArray.length(); i++) {
                            try {
                                String str = jsonArray.getString(i);

                                MsgBean msgBean = GsonUtils.getObject(str, MsgBean.class);

                                stringBuilder.append(msgBean.packetId);

                                if (i != jsonArray.length() - 1) {
                                    stringBuilder.append(",");
                                }

                                L.d(TAG, " processOnResponse msgText : " + msgBean.msgText);

                                L.d(TAG, " processOnResponse msgType : " + msgBean.msgType);

                                /**
                                 * 配对喜欢消息、check消息、密钥消息已经被取消，兼容老版本
                                 */
                                if (MsgBean.MSG_TYPE_SECRET_KEY.equals(msgBean.msgType) || MsgBean.MSG_TYPE_LIKE.equals(msgBean.msgType) || MsgBean.MSG_TYPE_CHECK.equals(msgBean.msgType) || MsgBean.MSG_TYPE_REQUEST.equals(msgBean.msgType)) {
                                    continue;
                                }

                                if (MsgBean.MSG_TYPE_REMOVE_FROM_BLACK_LIST.equals(msgBean.msgType) || MsgBean.MSG_TYPE_ADD_TO_BLACK_LIST.equals(msgBean.msgType)) {

                                    if (chatServiceBinder != null) {
                                        chatServiceBinder.pushNewMsgComing(msgBean);
                                    }

                                    continue;

                                }

                                // 合法消息
                                if (!TextUtils.isEmpty(msgBean.packetId) && !TextUtils.isEmpty(msgBean.fromUserId)) {

                                    onReceiveMsg(msgBean);

                                    // 日志通知、密友消息，只存packetId，不存整个消息
                                    if (MsgBean.MSG_TYPE_EMOJI.equals(msgBean.msgType) || MsgBean.MSG_TYPE_MOMENTS_COMMENT.equals(msgBean.msgType) || MsgBean.MSG_TYPE_MENTION.equals(msgBean.msgType) || MsgBean.MSG_TYPE_REPLY.equals(msgBean.msgType) || MsgBean.MSG_TYPE_REQUEST.equals(msgBean.msgType)) {

                                    } else if (!TextUtils.isEmpty(msgBean.msgType)) { // received消息是没有消息类型的，不存储

                                        msgCache.add(msgBean);

                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            String msgIds = stringBuilder.toString();

                            if (msgIds.length() > 0) {
                                receiptMsg(msgIds);
                            }

                        }

                        if (stringArr.length > 1) {
                            try {
                                String cursor = stringArr[1];
                                DBManager.getInstance().setMsgCursor(cursor);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            if (stringArr.length > 2) {
                                if (TheLConstants.MsgTypeConstant.MSG_TYPE_MORE.equals(stringArr[2])) {
                                    syncMsg(TheLConstants.SyncMsgType.SYNC_MSG_LOOP);
                                }
                            }
                        }
                    }
                }

            }

        }

    }

    private void onReceiveMsg(MsgBean msgBean) {

        L.d(TAG, " onReceiveMsg msgBean.logType " + msgBean.type);

        // 如果是消息回执
        if (MsgBean.TYPE_RECEIVED.equals(msgBean.type)) {

            msgBean.hadRead = 1;

            msgBean.msgStatus = TheLConstants.MsgSendingStatusConstants.MSG_READ;

            long isSuccess = DBManager.getInstance().updateMsgSendingStatus(msgBean);

            L.d(TAG, " onReceiveMsg isSuccess " + isSuccess);

            if (chatServiceBinder != null) {

                chatServiceBinder.pushMsgSendStatus(msgBean);
            }
        }

        // 是收到的消息
        msgBean.msgDirection = "1";
        // 是否读过，没读过
        msgBean.hadRead = 0;

        if (TextUtils.isEmpty(msgBean.msgType)) {
            return;
        }

        // 1.消息处理（文件存到sd，多语言处理）
        // 如果是图片或者语音，需要把base64字符串转成文件保存在sd卡上
        switch (msgBean.msgType) {
            case MsgBean.MSG_TYPE_IMAGE:
                msgBean.filePath = msgBean.msgText;
                msgBean.isSystem = MsgBean.IS_NORMAL_MSG;

                break;
            case MsgBean.MSG_TYPE_VOICE:

                msgBean.filePath = msgBean.msgText;
                msgBean.hadPlayed = 0;
                msgBean.isSystem = MsgBean.IS_NORMAL_MSG;

                break;
            case MsgBean.MSG_TYPE_WINK:

                msgBean.isSystem = MsgBean.IS_WINK_MSG;
                if (TextUtils.isEmpty(msgBean.packetId)) {
                    msgBean.packetId = MsgUtils.getMsgId();
                }

                break;
            case MsgBean.MSG_TYPE_FOLLOW:

                msgBean.isSystem = MsgBean.IS_FOLLOW_MSG;
                if (TextUtils.isEmpty(msgBean.packetId)) {
                    msgBean.packetId = MsgUtils.getMsgId();
                }

                break;
            case MsgBean.MSG_TYPE_UNFOLLOW:

                msgBean.isSystem = MsgBean.IS_FOLLOW_MSG;
                break;
            case MsgBean.MSG_TYPE_ADD_TO_BLACK_LIST: {
                // 有人屏蔽了我，则保存她的userid，之后我不能向她发信息了
                String blockMeList = ShareFileUtils.getString(ShareFileUtils.BLOCK_ME_LIST, "");
                String userId = "[" + msgBean.fromUserId + "]";
                if (!blockMeList.contains(userId)) {
                    blockMeList += userId;
                    ShareFileUtils.setString(ShareFileUtils.BLOCK_ME_LIST, blockMeList);
                }
                return;
            }
            case MsgBean.MSG_TYPE_REMOVE_FROM_BLACK_LIST: {
                // 有人取消屏蔽我，则删除她的userid，之后我可以向她发信息了
                String blockMeList = ShareFileUtils.getString(ShareFileUtils.BLOCK_ME_LIST, "");
                String userId = "[" + msgBean.fromUserId + "]";
                if (blockMeList.contains(userId)) {
                    blockMeList = blockMeList.replace(userId, "");
                    ShareFileUtils.setString(ShareFileUtils.BLOCK_ME_LIST, blockMeList);
                }
                return;
            }
            case MsgBean.MSG_TYPE_REQUEST:
                // 密友请求
                if (!TextUtils.isEmpty(msgBean.msgText)) {
                    MsgBean msgBean1 = new MsgBean();
                    msgBean1.msgType = MsgBean.MSG_TYPE_REQUEST;
                    msgBean1.userId = DBConstant.Message.CIRCLE_REQUEST_CHAT_USER_ID;
                    msgBean1.msgTime = msgBean.msgTime;
                    msgBean1.messageTo = "100";
                    msgBean1.msgText = MsgBean.SHOW_CIRCLE_REQUEST_CHAT;
                }
                return;
            case MsgBean.MSG_TYPE_GIFT:
                //4.1.0 赠送会员消息,由于发送端没有此消息，所以，随便加一个packetId(其实在解析的时候，自动把id解析成了PackedId,这里为了容错)
                if (!TextUtils.isEmpty(msgBean.msgText) && TextUtils.isEmpty(msgBean.packetId)) {
                    msgBean.packetId = MsgUtils.getMsgId();
                }
                break;
            case MsgBean.MSG_TYPE_EMOJI:
            case MsgBean.MSG_TYPE_MOMENTS_COMMENT:
            case MsgBean.MSG_TYPE_MENTION:
            case MsgBean.MSG_TYPE_REPLY: // 日志相关的消息

                /**
                 * 2.20.0版本开始，由服务器发日志通知的push，客户端不再对这些消息进行处理(但是但是为了兼容老版本发来的动态，保留代码)
                 * 2.20.0版本新加了通知开关
                 */
                if ((MsgBean.MSG_TYPE_MOMENTS_COMMENT.equals(msgBean.msgType) || MsgBean.MSG_TYPE_MENTION.equals(msgBean.msgType)) && ShareFileUtils.getInt(ShareFileUtils.commentTextPush, 1) == 0) {
                    return;
                }
                if (MsgBean.MSG_TYPE_REPLY.equals(msgBean.msgType) && ShareFileUtils.getInt(ShareFileUtils.commentReplyPush, 1) == 0) {
                    return;
                }
                if (MsgBean.MSG_TYPE_EMOJI.equals(msgBean.msgType) && ShareFileUtils.getInt(ShareFileUtils.commentWinkPush, 1) == 0) {
                    return;
                }
                return;
            default:
                if (!TextUtils.isEmpty(msgBean.msgType)) {
                    msgBean.isSystem = MsgBean.IS_NORMAL_MSG;
                }
                break;
        }

    }

    @Override
    public void receivePendingMsg(MsgPacket msgPacket) {
        try {

            String[] arr = msgPacket.code.split(":");
            if (arr.length == 3) {
                String payload = new String(msgPacket.payload);

                L.d("receivePendingMsg", " payload : " + payload);

                String type = new JSONObject(payload).getString("type");

                L.d("receivePendingMsg", " logType : " + type);

                Intent intent = new Intent();
                if (TheLConstants.MsgTypeConstant.MSG_TYPE_TEXT.equals(type)) {
                    intent.setAction(TheLConstants.BROADCAST_PENDING_MSG_TYPING);
                } else if (TheLConstants.MsgTypeConstant.MSG_TYPE_VOICE.equals(type)) {
                    intent.setAction(TheLConstants.BROADCAST_PENDING_MSG_SPEAKING);
                } else {
                    return;
                }
                intent.putExtra("userId", arr[2]);
                TheLApp.getContext().sendBroadcast(intent);

                L.d(TAG, " sendBroadcast : ");

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override public void receiptMsg(String msgId) {
        if (chatServiceBinder != null) {
            String userId = DBManager.getInstance().getMyUserId();

            if (userId != null) {

                long uid = Long.valueOf(userId);

                ReceiptMsgBean authReportBean = new ReceiptMsgBean();

                authReportBean.userId = uid;

                authReportBean.msgId = msgId;

                authReportBean.client = "android";

                String body = GsonUtils.createJsonString(authReportBean);

                L.d(TAG, " receiptMsg body : " + body);

                chatServiceBinder.getNettyClient().requestMsg("receipt", body, new ResponseCallback() {
                    @Override
                    public void onError(Throwable throwable) {

                        L.d(TAG, " receiptMsg onError : " + throwable.getMessage());

                    }

                    @Override
                    public void onResponse(MsgPacket msgPacket) {

                        L.d(TAG, " receiptMsg onResponse : " + msgPacket.code);

                        if ("OK".equals(msgPacket.code)) {

                        }
                    }
                });
            }
        }
    }

    boolean isFirstReport = true;

    @Override public void authReport() {

        isFirstReport = false;

        if (chatServiceBinder != null) {

            if (AppInit.openMainActivityTime != -1) {

                String userId = DBManager.getInstance().getMyUserId();

                if (userId != null) {

                    long time = System.currentTimeMillis() - AppInit.openMainActivityTime;

                    AuthReportBean authReportBean = new AuthReportBean();

                    authReportBean.userId = Long.valueOf(userId);

                    authReportBean.client = "android";

                    authReportBean.duration = time;

                    String body = GsonUtils.createJsonString(authReportBean);

                    L.d(TAG, " authReport body : " + body);

                    chatServiceBinder.getNettyClient().requestMsg("auth_report", body, new ResponseCallback() {
                        @Override
                        public void onError(Throwable throwable) {

                            L.d(TAG, " authReport onError : " + throwable.getMessage());

                        }

                        @Override
                        public void onResponse(MsgPacket msgPacket) {

                            L.d(TAG, " authReport msgPacket.code : " + msgPacket.code);

                            if ("OK".equals(msgPacket.code)) {

                            }
                        }
                    });
                }

            }

        }
    }

    @Override public void syncReport(long startTime) {

        if (chatServiceBinder != null) {

            if (startTime != -1) {

                String userId = DBManager.getInstance().getMyUserId();

                if (userId != null) {

                    long time = System.currentTimeMillis() - startTime;

                    AuthReportBean authReportBean = new AuthReportBean();

                    authReportBean.userId = Long.valueOf(userId);

                    authReportBean.client = "android";

                    authReportBean.duration = time;

                    String body = GsonUtils.createJsonString(authReportBean);

                    L.d(TAG, " authReport body : " + body);

                    chatServiceBinder.getNettyClient().requestMsg("sync_report", body, new ResponseCallback() {
                        @Override
                        public void onError(Throwable throwable) {

                            L.d(TAG, " authReport onError : " + throwable.getMessage());

                        }

                        @Override
                        public void onResponse(MsgPacket msgPacket) {

                            L.d(TAG, " authReport msgPacket.code : " + msgPacket.code);

                            if ("OK".equals(msgPacket.code)) {

                            }
                        }
                    });
                }

            }

        }

    }

}
