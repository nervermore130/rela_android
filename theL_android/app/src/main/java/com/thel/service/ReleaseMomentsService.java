package com.thel.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import androidx.annotation.Nullable;

import android.text.TextUtils;

import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UploadManager;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.bean.LiveInfoLogBean;
import com.thel.bean.MentionedUserBean;
import com.thel.bean.MomentSongBean;
import com.thel.bean.ReleaseMomentListBean;
import com.thel.bean.ReleaseMomentSucceedBean;
import com.thel.bean.ReleaseMomentSucceedBean2;
import com.thel.bean.moments.MomentsBean;
import com.thel.bean.user.UploadTokenBean;
import com.thel.constants.TheLConstants;
import com.thel.modules.live.utils.LiveUtils;
import com.thel.network.InterceptorSubscribe;
import com.thel.network.RequestBusiness;
import com.thel.network.RequestConstants;
import com.thel.utils.DialogUtil;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;
import com.thel.utils.MD5Utils;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.Utils;

import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by chad
 * Time 17/10/13
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */

public class ReleaseMomentsService extends Service {

    public static final String TAG = "ReleaseMomentsService";

    //当前要发布的日志是发布日志列表中的哪一个
    public static final String MOMENT_INDEX = "moment index";

    private int releaseMomentIndex;
    //qiniu上传任务数
    private int qiniuUploadNumber;

    private ReleaseMomentListBean releaseMomentListBean;

    private Map<String, String> mUploadImgsUrlsMap = new HashMap<>();

    private InterceptorSubscribe<ReleaseMomentSucceedBean2> releaseMomentConsumer = new InterceptorSubscribe<ReleaseMomentSucceedBean2>() {

        @Override
        public void onNext(ReleaseMomentSucceedBean2 result) {
            super.onNext(result);

            L.d(TAG, " onNext ");

            try {

                LiveInfoLogBean.getInstance().getReleaseMomentFailedAnalytics().reason += "\n 发布日志失败 hasErrorCode=" + result.data.errcode;

                LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getReleaseMomentFailedAnalytics());

                if (hasErrorCode) {
                    Intent intent = new Intent();
                    intent.setAction(TheLConstants.BROADCAST_RELEASE_MOMENT_FAIL);
                    TheLApp.getContext().sendBroadcast(intent);
                    return;
                }
                if (releaseMomentListBean.list.get(releaseMomentIndex).shareTo != MomentsBean.SHARE_TO_ONLY_ME) {// 只有不是自己可见，才@用户
                    // at用户iOS推送
                /*for (MentionedUserBean bean : momentToRelease.atUserList) {
                    SendMsgUtils.newInstance().sendMessage(MsgUtils.newInstance().createMsgBean(MsgBean.MSG_TYPE_MENTION, "", 1, bean.userId + "", bean.nickname, bean.avatar));
                }*/
                }
                DialogUtil.showToastShort(TheLApp.getContext(), getString(R.string.post_moment_posted));
                final MomentsBean releaseMoment = releaseMomentListBean.list.get(releaseMomentIndex);
                releaseMomentListBean.list.remove(releaseMomentIndex);
                String toJson = GsonUtils.createJsonString(releaseMomentListBean);
                ShareFileUtils.setString(TheLConstants.RELEASE_CONTENT_KEY, toJson);

                // 发送发布日志成功的广播
                Intent intent = new Intent();
                intent.setAction(TheLConstants.BROADCAST_RELEASE_MOMENT_SUCCEED);
                intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_ID, result.data.momentsId + "");
                intent.putExtra(TheLConstants.BUNDLE_KEY_MOMENT_SEND_ID, releaseMoment.momentsId);
                intent.putExtra(TheLConstants.BUNDLE_KEY_VIDEO_URL, releaseMoment.videoUrl);
                intent.putExtra(TheLConstants.BUNDLE_KEY_VIDEO_WEBP, releaseMoment.videoWebp);
                intent.putExtra(TheLConstants.BUNDLE_KEY_IMAGE_URL, releaseMoment.imageUrl);
                //            intent.putExtra(TheLConstants.BUNDLE_KEY_RELEASE_TIME, momentToRelease.releaseTime);
                TheLApp.getContext().sendBroadcast(intent);


                // 发刷新tab界面的广播
                Intent intent1 = new Intent();
                intent1.setAction(TheLConstants.BROADCAST_FAILED_MOMENTS_CHECK_ACTION);
                TheLApp.getContext().sendBroadcast(intent1);
                if (releaseMomentListBean.list.size() == 0) {
                    stopSelf();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onError(Throwable t) {
            super.onError(t);
            Intent intent = new Intent();
            intent.setAction(TheLConstants.BROADCAST_RELEASE_MOMENT_FAIL);
            TheLApp.getContext().sendBroadcast(intent);

            try {

                LiveInfoLogBean.getInstance().getReleaseMomentFailedAnalytics().reason += "\n 发布日志失败 onError message=" + (t != null ? t.getMessage() : "未获取到Throwable");

                LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getReleaseMomentFailedAnalytics());

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    };

    public class MyConsumer extends InterceptorSubscribe<UploadTokenBean> {

        String picUploadPath;

        String picLocalPath;

        public MyConsumer(String picUploadPath, String picLocalPath) {
            this.picUploadPath = picUploadPath;
            this.picLocalPath = picLocalPath;
        }

        @Override
        public void onNext(UploadTokenBean uploadTokenBean) {
            super.onNext(uploadTokenBean);
            if (!TextUtils.isEmpty(uploadTokenBean.data.uploadToken)) {
                if (!TextUtils.isEmpty(picLocalPath) && !TextUtils.isEmpty(picUploadPath)) {

                    UploadManager uploadManager = new UploadManager();
                    uploadManager.put(new File(picLocalPath), picUploadPath, uploadTokenBean.data.uploadToken, new UpCompletionHandler() {
                        @Override
                        public void complete(String key, ResponseInfo info, JSONObject response) {
                            try {
                                if (info != null && info.statusCode == 200 && picUploadPath.equals(key)) {

                                    qiniuUploadNumber--;
                                    //todo Fatal Exception: java.lang.IndexOutOfBoundsException
                                    MomentsBean momentsBean = releaseMomentListBean.list.get(releaseMomentIndex);
                                    if (MomentsBean.MOMENT_TYPE_VIDEO.equals(momentsBean.momentsType)) {
                                        if (key.endsWith(".mp4")) {
                                            momentsBean.videoUrl = RequestConstants.VIDEO_BUCKET + key;
                                            final String webpUrl = RequestConstants.FILE_BUCKET + Utils.getWebpPathByVideoPath(key);
                                            momentsBean.videoWebp = webpUrl;
                                            L.d(TAG, "上传视频 momentsBean.videoUrl :" + momentsBean.videoUrl);
                                        } else {
                                            momentsBean.imageUrl = RequestConstants.FILE_BUCKET + key;
                                            L.d(TAG, "上传视频 momentsBean.imageUrl :" + momentsBean.imageUrl);
                                        }
                                    } else {
                                        L.d(TAG, "上传图片 " + "picLocalPath :" + picLocalPath
                                                + "\nuploadPath :" + RequestConstants.FILE_BUCKET + key);
                                        mUploadImgsUrlsMap.put(picLocalPath, RequestConstants.FILE_BUCKET + key);
                                    }
                                    if (qiniuUploadNumber == 0) {
                                        releaseImageAndVideoMoment();
                                    }
                                } else {
                                    try {

                                        String infoString = info != null ? "info statusCode=" + info.statusCode : "info消息为空";

                                        LiveInfoLogBean.getInstance().getReleaseMomentFailedAnalytics().reason += "\n 发布日志失败" + (infoString + " picUploadPath=" + picUploadPath + " , key=" + key);

                                        LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getReleaseMomentFailedAnalytics());

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    releaseFailed();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, null);
                } else {

                    try {

                        LiveInfoLogBean.getInstance().getReleaseMomentFailedAnalytics().reason += "\n 发布日志失败,本地图片地址或上传七牛的地址为空" + (" picLocalPath=" + picLocalPath + " picUploadPath=" + picUploadPath);

                        LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getReleaseMomentFailedAnalytics());

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    releaseFailed();
                }
            } else {

                try {

                    LiveInfoLogBean.getInstance().getReleaseMomentFailedAnalytics().reason += "\n 没有获取到七牛的token";

                    LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getReleaseMomentFailedAnalytics());

                } catch (Exception e) {
                    e.printStackTrace();
                }

                releaseFailed();
            }
        }

        @Override
        public void onError(Throwable t) {
            super.onError(t);
            Intent intent = new Intent();
            intent.setAction(TheLConstants.BROADCAST_RELEASE_MOMENT_FAIL);
            TheLApp.getContext().sendBroadcast(intent);

            try {

                LiveInfoLogBean.getInstance().getReleaseMomentFailedAnalytics().reason += "\n 发布日志失败 上传图片失败 " + (t != null ? t.getMessage() : "未获取到Throwable");

                LiveUtils.pushLivePointLog(LiveInfoLogBean.getInstance().getReleaseMomentFailedAnalytics());

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
//        startForeground(1,new Notification());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null) {
            stopSelf();
            return super.onStartCommand(intent, flags, startId);
        }

        String releaseJson = ShareFileUtils.getString(TheLConstants.RELEASE_CONTENT_KEY, "");
        releaseMomentListBean = GsonUtils.getObject(releaseJson, ReleaseMomentListBean.class);
        L.d(TAG, "releaseMomentListBean :" + releaseMomentListBean);

        // 发送提交发布日志的广播
        Intent intent1 = new Intent();
        intent1.setAction(TheLConstants.BROADCAST_RELEASE_MOMENT_SUBMIT);
        //        intent1.putExtra(TheLConstants.BUNDLE_KEY_RELEASE_TIME, releaseTime);
        TheLApp.getContext().sendBroadcast(intent1);

        releaseMomentIndex = intent.getIntExtra(MOMENT_INDEX, 0);
        //todo releaseMomentListBean.list releaseMomentIndex Caused by java.lang.IndexOutOfBoundsException
        try {
            MomentsBean momentsBean = releaseMomentListBean.list.get(releaseMomentIndex);
            L.d(TAG, "onStartCommand :" + momentsBean.toString());
            if (MomentsBean.MOMENT_TYPE_TEXT.equals(momentsBean.momentsType) || (MomentsBean.MOMENT_TYPE_THEME.equals(momentsBean.momentsType) && TextUtils.isEmpty(momentsBean.imageUrl)) || MomentsBean.MOMENT_TYPE_RECOMMEND.equals(momentsBean.momentsType)// 发布文字日志,4.0.0推荐日志按照文本日志处理
                    || MomentsBean.MOMENT_TYPE_WEB.equals(momentsBean.momentsType)//推荐网页，内容都放到momentsText里面，所以当普通文本日志处理
                    || MomentsBean.MOMENT_TYPE_LIVE_USER.equals(momentsBean.momentsType)) {//推荐直播用户
                RequestBusiness.getInstance()
                        .releaseMoment(momentsBean.momentsType, momentsBean.momentsText, null, momentsBean.shareTo, momentsBean.secret, buildMentionUserIds(momentsBean), "", "", momentsBean.themeTagName, "", 0, 0, 0, 0, "", momentsBean.themeClass, momentsBean.parentId, momentsBean.userId + "", momentsBean.videoWebp, momentsBean.tagList)
                        .onBackpressureDrop()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(releaseMomentConsumer);
            } else if (MomentsBean.MOMENT_TYPE_IMAGE.equals(momentsBean.momentsType) || MomentsBean.MOMENT_TYPE_TEXT_IMAGE.equals(momentsBean.momentsType) || (MomentsBean.MOMENT_TYPE_THEME.equals(momentsBean.momentsType) && !TextUtils.isEmpty(momentsBean.imageUrl)) || MomentsBean.MOMENT_TYPE_VIDEO.equals(momentsBean.momentsType)) {// 发布图片日志，或带图的话题，或视频
                if (!TextUtils.isEmpty(momentsBean.imageUrl)) {
                    // 上传视频的话，将视频地址和图片地址一起放在imgsToUpload里，用逗号分隔
                    String[] pics = momentsBean.imageUrl.split(",");
                    qiniuUploadNumber = pics.length;
                    for (int i = 0; i < pics.length; i++) {
                        String fileType = pics[i].substring(pics[i].lastIndexOf("."));
                        // 把文件地址作为uuid，等到上传成功返回结果后根据这个字符串来更新数据库未上传的图片列表
                        String uploadPath = RequestConstants.UPLOAD_FILE_ROOT_PATH_TIMELINE + ShareFileUtils.getString(ShareFileUtils.ID, "") + "/" + MD5Utils.calculateMD5(new File(pics[i])) + fileType;
                        // 如果是上传视频的话要穿『桶』名
                        RequestBusiness.getInstance()
                                .getUploadToken(System.currentTimeMillis() + "", ".mp4".equals(fileType) ? RequestConstants.QINIU_BUCKET_VIDEO : "", uploadPath, ".mp4".equals(fileType))
                                .onBackpressureDrop().subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(new MyConsumer(uploadPath, pics[i]));
                    }
                } else {
                    releaseImageAndVideoMoment();
                }
            } else if (MomentsBean.MOMENT_TYPE_VOICE.equals(momentsBean.momentsType) || MomentsBean.MOMENT_TYPE_TEXT_VOICE.equals(momentsBean.momentsType)) {// 发布音乐日志
                MomentSongBean song = new MomentSongBean();
                song.songId = momentsBean.songId;
                song.songName = momentsBean.songName;
                song.artistName = momentsBean.artistName;
                song.albumName = momentsBean.albumName;
                song.albumLogo100 = momentsBean.albumLogo100;
                song.albumLogo444 = momentsBean.albumLogo444;
                song.songLocation = momentsBean.songLocation;
                song.toURL = momentsBean.toURL;
                RequestBusiness.getInstance()
                        .releaseMoment(momentsBean.momentsType, momentsBean.momentsText, song, momentsBean.shareTo, momentsBean.secret, buildMentionUserIds(momentsBean), "", "", momentsBean.themeTagName, "", 0, 0, 0, 0, "", momentsBean.themeClass, momentsBean.parentId, "", momentsBean.videoWebp, momentsBean.tagList)
                        .onBackpressureDrop()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(releaseMomentConsumer);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void releaseFailed() {
        DialogUtil.showToastShort(TheLApp.getContext(), TheLApp.getContext().getString(R.string.post_moment_failed));
        // 发送发布日志失败的广播
        Intent intent = new Intent();
        intent.setAction(TheLConstants.BROADCAST_RELEASE_MOMENT_FAIL);
        TheLApp.getContext().sendBroadcast(intent);
        // 发刷新tab界面的广播
        Intent intent1 = new Intent();
        intent1.setAction(TheLConstants.BROADCAST_FAILED_MOMENTS_CHECK_ACTION);
        TheLApp.getContext().sendBroadcast(intent1);
        // 清除所有
        //        RequestServerThreadInstance.newInstance().removeAllReleaseMomentObjs();
        stopSelf();
    }

    private String buildMentionUserIds(MomentsBean momentsBean) {
        StringBuilder atUserList = new StringBuilder();
        if (momentsBean != null && momentsBean.atUserList != null && !momentsBean.atUserList.isEmpty()) {
            for (MentionedUserBean contact : momentsBean.atUserList) {
                atUserList.append(contact.userId);
                atUserList.append(",");
            }
            if (atUserList.length() > 0) {
                atUserList.deleteCharAt(atUserList.length() - 1);
            }
        }
        return atUserList.toString();
    }

    private void releaseImageAndVideoMoment() {
        MomentsBean momentsBean = releaseMomentListBean.list.get(releaseMomentIndex);
        try {
            if (MomentsBean.MOMENT_TYPE_VIDEO.equals(momentsBean.momentsType)) {// 视频
                L.d(TAG, "发布视频 " + "momentsBean.imageUrl :" + momentsBean.imageUrl
                        + "\nmomentsBean.videoUrl :" + momentsBean.videoUrl);
                RequestBusiness.getInstance()
                        .releaseMoment(momentsBean.momentsType
                                , momentsBean.momentsText
                                , null
                                , momentsBean.shareTo
                                , momentsBean.secret
                                , buildMentionUserIds(momentsBean)
                                , momentsBean.imageUrl
                                , ""
                                , momentsBean.themeTagName
                                , momentsBean.videoUrl
                                , momentsBean.mediaSize
                                , momentsBean.playTime
                                , momentsBean.pixelWidth
                                , momentsBean.pixelHeight
                                , momentsBean.videoColor
                                , momentsBean.themeClass
                                , momentsBean.parentId
                                , ""
                                , momentsBean.videoWebp
                                , momentsBean.tagList)
                        .onBackpressureDrop()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(releaseMomentConsumer);
            } else {
                // 根据imageUrl属性的图片顺序来确定最终发布日志的图片顺序
                String[] imgs = momentsBean.imageUrl.split(",");
                StringBuilder imgUrls = new StringBuilder();
                for (String str : imgs) {
                    imgUrls.append(mUploadImgsUrlsMap.get(str)).append(",");
                }
                if (imgUrls.length() > 0)
                    imgUrls.deleteCharAt(imgUrls.length() - 1);
                L.d(TAG, "发布图片 " + "imgUrls :" + imgUrls.toString());
                RequestBusiness.getInstance()
                        .releaseMoment(momentsBean.momentsType
                                , momentsBean.momentsText
                                , null
                                , momentsBean.shareTo
                                , momentsBean.secret
                                , buildMentionUserIds(momentsBean)
                                , imgUrls.toString()
                                , ""
                                , momentsBean.themeTagName
                                , ""
                                , 0, 0, 0, 0, ""
                                , momentsBean.themeClass
                                , momentsBean.parentId
                                , ""
                                , momentsBean.videoWebp
                                , momentsBean.tagList)
                        .onBackpressureDrop()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(releaseMomentConsumer);
            }
        } catch (Exception e) {
            releaseFailed();
        }
    }

}
