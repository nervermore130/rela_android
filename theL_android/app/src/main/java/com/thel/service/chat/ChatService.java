package com.thel.service.chat;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import android.os.RemoteException;

import androidx.annotation.Nullable;

import android.text.TextUtils;

import com.google.gson.reflect.TypeToken;
import com.thel.IChatAidlInterface;
import com.thel.IMainProcessCallback;
import com.thel.IMsgStatusCallback;
import com.thel.chat.netty.MsgPacket;
import com.thel.chat.netty.NettyClient;
import com.thel.chat.netty.NettyConstants;
import com.thel.chat.netty.NettyListener;
import com.thel.constants.TheLConstants;
import com.thel.db.DBManager;
import com.thel.db.table.message.MsgTable;
import com.thel.db.table.message.UserInfoTable;
import com.thel.db.table.message.WinkMsgTable;
import com.thel.manager.ChatServiceManager;
import com.thel.modules.main.messages.bean.MsgBean;
import com.thel.network.RequestConstants;
import com.thel.utils.GsonUtils;
import com.thel.utils.L;
import com.thel.utils.ShareFileUtils;
import com.thel.utils.UrlConstants;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.reactivex.Flowable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * @author liuyun
 * @date 2018/3/19
 */

public class ChatService extends Service {

    private static final String TAG = "ChatService";

    private static final int LOOP_DELAY_TIME = 25;

    private ChatClientImpl mChatClientImpl;

    private int mConnectStatus = ConnectStatus.STOP;

    private MyChatReceiver networkChangedReceiver;

    private Map<String, IMsgStatusCallback> mCallbacks;

    private IMainProcessCallback mIMainProcessCallback;

    private ChatServiceBinder mChatServiceBinder;

    private CompositeDisposable mCompositeDisposable;

    private Disposable reConnectDisposable;

    private NettyClient mNettyClient;

    private boolean isInit = false;

    private String userId = null;

    private String myUserId = null;

    private String key = null;

    private List<String> userList = new ArrayList<>();

    private List<String> ipList;

    private int ipIndex = 0;

    private String chatIp = null;

    public static class ConnectStatus {

        /**
         * 停止连接
         */
        public static final int STOP = 0x01;

        /**
         * 聊天连接中
         */
        public static final int CONNECTING = 0x02;

        /**
         * 连接成功
         */
        public static final int CONNECTED = 0x03;

        /**
         * 重连
         */
        public static final int RECONNECT = 0x04;

        /**
         * 连接失败
         */
        public static final int FAILED = 0x05;

        /**
         * 连接报错
         */
        public static final int ERROR = 0x06;

        /**
         * 数据迁移开始
         */
        public static final int DATA_TRANSFER_BEGIN = 0x07;

    }

    @Override
    public void onCreate() {
        super.onCreate();

        L.d(TAG, " service onCreate");

        mCompositeDisposable = new CompositeDisposable();
        mChatClientImpl = new ChatClientImpl();
        mCallbacks = new HashMap<>();
        L.d(TAG, " pushNewUpdateMsg onCreate mCallbacks : " + System.identityHashCode(mCallbacks));

        registerReceiver();
        myUserId = null;
        key = null;
    }

    @Override
    public void onDestroy() {
        unregisterReceiver();

        if (mCompositeDisposable != null) {
            mCompositeDisposable.clear();
        }

        if (mIMainProcessCallback != null) {
            try {
                mIMainProcessCallback.serviceKilledBySystem();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        chatDisconnect();

        myUserId = null;

        if (userList != null) {
            userList.clear();
        }

        if (mChatClientImpl != null) {
            mChatClientImpl.clear();
        }

        if (mCallbacks != null) {
            try {
                for (Map.Entry<String, IMsgStatusCallback> entry : mCallbacks.entrySet()) {
                    mCallbacks.remove(entry.getKey());
                }
                mCallbacks.clear();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        DBManager.getInstance().disconnectDB();

        ShareFileUtils.setString(ShareFileUtils.MSG_AUTH_TOKEN, "{}");

        L.d(TAG, " service onDestroy");

        super.onDestroy();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        L.d(TAG, " service onStartCommand");

        return Service.START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {

        mChatServiceBinder = new ChatServiceBinder();

        if (mChatClientImpl != null) {
            try {
                mChatClientImpl.setBinder(mChatServiceBinder);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        return mChatServiceBinder;
    }

    public class ChatServiceBinder extends IChatAidlInterface.Stub {

        public ChatService getService() {
            return ChatService.this;
        }

        @Override public void init() {

            L.e(TAG, " iMsgClient init : ");

            if (mNettyClient != null) {
                mNettyClient.disconnect();
            }

            mNettyClient = new NettyClient();

            mNettyClient.setListener(mNettyListener);

            try {
                if (mIMainProcessCallback != null) {
                    mIMainProcessCallback.umengPushStatus(5, null, null);
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }

            String ips = DBManager.getInstance().getIPs();

            Type typeOfDest = new TypeToken<List<String>>() {
            }.getType();

            ipList = GsonUtils.getObjects(ips, typeOfDest);

            changeIPs();

            ChatService.this.getChatIp();
        }

        @Override public int getConnectStatus() {

            L.e(TAG, " iMsgClient mConnectStatus : " + mConnectStatus);

            return mConnectStatus;
        }

        @Override public void setConnectStatus(int status) {
            mConnectStatus = status;
        }

        @Override public boolean isConnected() {

            return mConnectStatus == ConnectStatus.CONNECTED;
        }

        @Override public void reconnect() {

            L.e(TAG, " iMsgClient autoReconnect : ");

            ChatServiceManager.getInstance().pushStatus(ConnectStatus.RECONNECT);

            chatDisconnect();

            init();
        }

        @Override
        public void disconnect() {

            L.e(TAG, " iMsgClient disconnect : ");

            chatDisconnect();
        }

        @Override
        public void auth() {
            if (mChatClientImpl != null) {
                mChatClientImpl.authMsg();
            }
        }

        @Override
        public void sendMsg(MsgBean msgBean) {
            if (mChatClientImpl != null) {
                mChatClientImpl.sendMsg(msgBean);
            }
        }

        @Override
        public void sendReceiptMsg(MsgBean msgBean) {
            if (mChatClientImpl != null) {
                mChatClientImpl.sendReceiptMsg(msgBean);
            }
        }

        @Override
        public void sendPendingMsg(String toUserId, String content) {
            if (mChatClientImpl != null) {
                mChatClientImpl.sendPendingMsg(toUserId, content);
            }
        }

        @Override
        public void registerCallBack(String key, IMsgStatusCallback cb) {

            if (mCallbacks != null && cb != null) {
                mCallbacks.put(key, cb);

            }


        }

        @Override
        public void unregisterCallBack(String key) {


            if (mCallbacks != null && key != null) {
                mCallbacks.remove(key);
            }

        }

        @Override
        public void pushStatus(int status) {

            L.e(TAG, " iMsgClient pushStatus status : " + status);

            L.e(TAG, " iMsgClient reConnectDelayFiveSeconds pushStatus : " + status);

            mConnectStatus = status;

            Flowable.just(status).onBackpressureDrop().subscribeOn(Schedulers.io()).subscribe(new Consumer<Integer>() {
                @Override
                public void accept(Integer status) {
                    pushConnectStatus(status);

                }
            });

            if (status == ConnectStatus.ERROR || status == ConnectStatus.FAILED) {
                reConnectDelayFiveSeconds();
            }

            if (status == ConnectStatus.CONNECTED) {
                if (mCompositeDisposable != null && reConnectDisposable != null) {
                    mCompositeDisposable.remove(reConnectDisposable);
                }
            }

        }

        @Override
        public void clearCallback() {
            if (mCallbacks != null) {
                mCallbacks.clear();
            }

        }

        @Override
        public synchronized void pushMsgSendStatus(MsgBean msgBean) {
            Flowable.just(msgBean).onBackpressureDrop().subscribeOn(Schedulers.io()).subscribe(new Consumer<MsgBean>() {
                @Override
                public void accept(MsgBean msgBean) {
                    pushMsgStatus(msgBean);
                }
            });
        }

        @Override
        public void pushNewMsgComing(MsgBean msgBean) {

            L.d(TAG, " msgCache pushNewMsgComing 0 : ");

            Flowable.just(msgBean).onBackpressureDrop().subscribeOn(Schedulers.io()).subscribe(new Consumer<MsgBean>() {
                @Override
                public void accept(MsgBean msgBean) {

                    L.d(TAG, " msgCache pushNewMsgComing accept : ");

                    pushNewMsg(msgBean);
                }
            });
        }

        @Override
        public void pushUpdateMsg(MsgTable msgTable) {
            Flowable.just(msgTable).onBackpressureDrop().subscribeOn(Schedulers.io()).subscribe(new Consumer<MsgTable>() {
                @Override
                public void accept(MsgTable msgTable) {

                    L.d(TAG, " pushUpdateMsg subscribe : ");

                    pushNewUpdateMsg(msgTable);
                }
            });
        }

        @Override
        public void pushDeleteMsgTable(String userId) {
            Flowable.just(userId).onBackpressureDrop().onBackpressureDrop().subscribeOn(Schedulers.io()).subscribe(new Consumer<String>() {
                @Override
                public void accept(String userId) {

                    L.d(TAG, " pushDeleteMsgTable subscribe : ");

                    ChatService.this.pushDeleteMsgTable(userId);
                }
            });
        }

        @Override
        public List<String> getUserList() {
            return ChatService.this.userList;
        }

        @Override
        public void setUserList(List<String> userList) {

            if (ChatService.this.userList != null && userList != null) {
                ChatService.this.userList.clear();
                ChatService.this.userList.addAll(userList);
                DBManager.getInstance().setUserList(userList);

            }

        }

        @Override
        public void setUserId(String userId) {
            L.e(TAG, " iMsgClient setCurrentChatUserId userId : " + userId);
            L.e(TAG, " iMsgClient setCurrentChatUserId mChatClientImpl : " + mChatClientImpl);

            if (mChatClientImpl != null) {
                mChatClientImpl.setUserId(userId);
            }

            DBManager.getInstance();

        }

        @Override
        public String getUserId() {
            return ChatService.this.userId;
        }

        @Override
        public void setMyUserId(String userId) {
            ChatService.this.myUserId = userId;
            if (userId != null) {
                L.e(TAG, " iMsgClient setCurrentChatUserId setMyUserId : " + userId);
                DBManager.getInstance().setUserId(userId);
            }
        }

        @Override
        public String getMyUserId() {
            return ChatService.this.myUserId;
        }

        @Override
        public void setKey(String key) {
            ChatService.this.key = key;
        }

        @Override
        public String getKey() {
            return ChatService.this.key;
        }

        @Override
        public void registerMainProcessCallback(IMainProcessCallback impcb) {
            mIMainProcessCallback = impcb;
            if (mChatClientImpl != null) {
                mChatClientImpl.setMainProcessCallback(mIMainProcessCallback);
            }
        }

        @Override
        public void unregisterMainProcessCallback() {
            mIMainProcessCallback = null;
        }

        @Override
        public void checkFriendsRelationship(String userId, MsgBean msgBean) {
            if (mIMainProcessCallback != null) {
                try {
                    mIMainProcessCallback.checkFriendsRelationship(userId, msgBean);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onChatInfo() {
            Flowable.just("").onBackpressureDrop().subscribeOn(Schedulers.io()).subscribe(new Consumer<String>() {
                @Override
                public void accept(String msgTable) {

                    L.d(TAG, " pushUpdateMsg subscribe : ");

                    pushChatInfo();
                }
            });
        }

        @Override
        public void onTransferPercents(int percents) {
            Flowable.just(percents).onBackpressureDrop().subscribeOn(Schedulers.io()).subscribe(new Consumer<Integer>() {
                @Override
                public void accept(Integer percents) {

                    L.d(TAG, " pushUpdateMsg subscribe : ");

                    pushDBTransferPercent(percents);
                }
            });
        }

        @Override
        public List<MsgTable> getStrangerMsg() {
            return DBManager.getInstance().getStrangerMsg();
        }

        @Override
        public void insertNewMsg(MsgBean msgBean, String tableName, int hasRead) {
            DBManager.getInstance().insertNewMsg(msgBean, tableName, hasRead);
        }

        @Override
        public void insertChatTable(MsgBean msgBean, String tableName) {
            DBManager.getInstance().insertChatTable(msgBean, tableName);
        }

        @Override
        public void insertMsgTable(MsgTable msgTable) {
            DBManager.getInstance().insertMsgTable(msgTable);
        }

        @Override
        public MsgTable getMsgTableByUserId(String userId) {
            return DBManager.getInstance().getMsgTableByUserId(userId);
        }

        @Override
        public List<MsgBean> findMsgsByUserIdAndPage(String toUserId, String limit, String offset) {
            return DBManager.getInstance().findMsgsByUserIdAndPage(toUserId, limit, offset);
        }

        @Override
        public List<MsgBean> getMsgListByTableName(String tableName) {
            return DBManager.getInstance().getMsgListByTableName(tableName);
        }

        @Override
        public List<MsgBean> getMsgListByTableNameAsync(String tableName) {
            return DBManager.getInstance().getMsgListByTableNameAsync(tableName);
        }

        @Override
        public MsgBean getMsgByUserIdAndPacketId(String userId, String packetId) {
            return DBManager.getInstance().getMsgByUserIdAndPacketId(userId, packetId);
        }

        @Override
        public void updateMsgTableWithWink(MsgTable msgTable) {
            DBManager.getInstance().updateMsgTableWithWink(msgTable);
        }

        @Override
        public void updateMsgUnread(MsgTable msgTable) {
            DBManager.getInstance().updateMsgUnread(msgTable);
        }

        @Override
        public long deleteMsgById(String tableName, String packetId) {
            return DBManager.getInstance().deleteMsgById(tableName, packetId);
        }

        @Override
        public MsgBean getLastMsg(String tableName) {
            return DBManager.getInstance().getLastMsg(tableName);
        }

        @Override
        public MsgTable findMsgTableByOtherUserId(String otherUserId) {
            return DBManager.getInstance().findMsgTableByOtherUserId(otherUserId);
        }

        @Override
        public List<MsgTable> getMsgList(int isStranger) {
            return DBManager.getInstance().getMsgList(isStranger);
        }

        @Override
        public MsgTable insertRequestDataToMsgTable(MsgBean msgBean, int unReadCount) {
            return DBManager.getInstance().insertRequestDataToMsgTable(msgBean, unReadCount);
        }

        @Override
        public int findRequestCountToMsgTable() {
            return DBManager.getInstance().findRequestCountToMsgTable();
        }

        @Override
        public void changeRelationship(String userId, MsgBean msgBean, int isStranger) {
            DBManager.getInstance().changeRelationship(userId, msgBean, isStranger);
        }

        @Override
        public void deleteStranger() {
            DBManager.getInstance().deleteStranger();
        }

        @Override
        public boolean isStranger(String userId) {
            return DBManager.getInstance().isStranger(userId);
        }

        @Override
        public long getLastMsgTime(String userId) {
            return DBManager.getInstance().getLastMsgTime(userId);
        }

        @Override
        public long stickTop(MsgTable msgTable) {
            return DBManager.getInstance().stickTop(msgTable);
        }

        @Override
        public long resetStickTop(MsgTable msgTable) {
            return DBManager.getInstance().resetStickTop(msgTable);
        }

        @Override
        public long getStickTopCount() {
            return DBManager.getInstance().getStickTopCount();
        }

        @Override
        public void removeMsgTable(String userId) {
            DBManager.getInstance().removeMsgTable(userId);
        }

        @Override
        public void updateMsgTable(MsgBean msgBean) {
            DBManager.getInstance().updateMsgTable(msgBean);
        }

        @Override
        public void deleteMsgTable(String userId) {
            DBManager.getInstance().deleteMsgTable(userId);
        }

        @Override
        public void insertUserInfo(UserInfoTable userInfoTable) {
            DBManager.getInstance().insertUserInfo(userInfoTable);
        }

        @Override
        public UserInfoTable getUserInfo() {
            return DBManager.getInstance().getUserInfo();
        }

        @Override
        public String getMsgCursor() {
            return DBManager.getInstance().getMsgCursor();
        }

        @Override
        public void setMsgCursor(String cursor) {
            DBManager.getInstance().setMsgCursor(cursor);
        }

        @Override
        public void updateStrangerUnreadCount(int count, int isWinked) {
            DBManager.getInstance().updateStrangerUnreadCount(count, isWinked);
        }

        @Override
        public void winkMsgSaveToDB(MsgBean msgBean) {
            DBManager.getInstance().winkMsgSaveToDB(msgBean);
        }

        @Override
        public long getStrangerWinkOrUnWinkCount(int isWinked) {
            return DBManager.getInstance().getStrangerWinkOrUnWinkCount(isWinked);
        }

        @Override
        public void insertMsg(MsgBean msgBean, String tableName) {
            DBManager.getInstance().insertMsg(msgBean, tableName);
        }

        @Override
        public void createTable(String tableName) {
            DBManager.getInstance().createTable(tableName);
        }

        @Override
        public void attachToOldDB(String path, String name) {
            DBManager.getInstance().attachToOldDB(path, name);
        }

        @Override
        public void detachToOldDB(String name) {
            DBManager.getInstance().detachToOldDB(name);
        }

        @Override
        public void tableTransfer(String newDBName, String newTableName, String oldDBName, String oldTableName) {
            DBManager.getInstance().tableTransfer(newDBName, newTableName, oldDBName, oldTableName);
        }

        @Override
        public void dropTable(String tableName) {
            DBManager.getInstance().dropTable(tableName);
        }

        @Override
        public void deleteWinkMsg(WinkMsgTable winkMsgTable) {
            DBManager.getInstance().deleteWinkMsg(winkMsgTable);
        }

        @Override
        public List<WinkMsgTable> getWinkList(int limit, int offset) {
            return DBManager.getInstance().getWinkList(limit, offset);
        }

        @Override
        public void updateWinkMsg(WinkMsgTable winkMsgTable) {
            DBManager.getInstance().updateWinkMsg(winkMsgTable);
        }

        @Override
        public void insertWinkMsg(WinkMsgTable winkMsgTable) {
            DBManager.getInstance().insertWinkMsg(winkMsgTable);
        }

        @Override
        public void clearWinksTable() {
            DBManager.getInstance().clearWinksTable();
        }

        @Override public long getWinkMsgCount() {
            return 0;
        }

        @Override public boolean isMsgCreated(MsgBean msgBean) {
            return DBManager.getInstance().isMsgCreated(msgBean);
        }

        @Override public int getUnreadMsgCount() throws RemoteException {
            return DBManager.getInstance().getUnreadMsgCount();
        }

        @Override
        public List<MsgTable> getAllMsgTables() {
            return DBManager.getInstance().getAllMsgTables();
        }

        public void changeIPs() {
            if (ipList != null && ipList.size() > 0) {
                if (ipIndex == ipList.size()) {
                    ipIndex = 0;
                }
                chatIp = ipList.get(ipIndex);
                ipIndex++;
            } else {
                chatIp = null;
            }

            L.d(TAG, " chatIp : " + chatIp);
        }

        public String getChatIp() {
            return chatIp;
        }

        public NettyClient getNettyClient() {
            return mNettyClient;
        }

    }

    private void chatDisconnect() {

        if (mNettyClient != null) {
            mNettyClient.disconnect();
        }
    }

    private synchronized void pushConnectStatus(int status) {

        L.d(TAG, " status : " + status);

        mConnectStatus = status;

        if (mCallbacks != null && mCallbacks.size() > 0) {
            try {

                for (Map.Entry<String, IMsgStatusCallback> entry : mCallbacks.entrySet()) {

                    IMsgStatusCallback iMsgStatusCallback = entry.getValue();

                    iMsgStatusCallback.onConnectStatus(status);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private synchronized void pushMsgStatus(MsgBean msgBean) {

        if (mCallbacks != null && mCallbacks.size() > 0) {
            try {

                for (Map.Entry<String, IMsgStatusCallback> entry : mCallbacks.entrySet()) {

                    IMsgStatusCallback iMsgStatusCallback = entry.getValue();

                    iMsgStatusCallback.onMsgSendStatus(msgBean);

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private synchronized void pushNewMsg(MsgBean msgBean) {

        if (mCallbacks != null && mCallbacks.size() > 0) {
            try {

                for (Map.Entry<String, IMsgStatusCallback> entry : mCallbacks.entrySet()) {

                    IMsgStatusCallback iMsgStatusCallback = entry.getValue();

                    iMsgStatusCallback.onNewMsgComing(msgBean);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private synchronized void pushNewUpdateMsg(MsgTable msgTable) {

        if (mCallbacks != null && mCallbacks.size() > 0) {

            try {

                for (Map.Entry<String, IMsgStatusCallback> entry : mCallbacks.entrySet()) {

                    IMsgStatusCallback iMsgStatusCallback = entry.getValue();

                    iMsgStatusCallback.onUpdateMsg(msgTable);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private void pushChatInfo() {
        if (mCallbacks != null && mCallbacks.size() > 0) {
            try {

                for (Map.Entry<String, IMsgStatusCallback> entry : mCallbacks.entrySet()) {

                    IMsgStatusCallback iMsgStatusCallback = entry.getValue();

                    iMsgStatusCallback.onChatInfo();

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private void pushDBTransferPercent(int percent) {
        if (mCallbacks != null && mCallbacks.size() > 0) {
            try {

                for (Map.Entry<String, IMsgStatusCallback> entry : mCallbacks.entrySet()) {

                    IMsgStatusCallback iMsgStatusCallback = entry.getValue();

                    iMsgStatusCallback.onTransferPercents(percent);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private void pushDeleteMsgTable(String userId) {

        if (mCallbacks != null && mCallbacks.size() > 0) {
            try {

                for (Map.Entry<String, IMsgStatusCallback> entry : mCallbacks.entrySet()) {

                    IMsgStatusCallback iMsgStatusCallback = entry.getValue();

                    iMsgStatusCallback.onDeleteMsgTable(userId);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * ConnectivityManager.CONNECTIVITY_ACTION  网络连接广播
     * Intent.ACTION_SCREEN_OFF 息屏广播
     * Intent.ACTION_SCREEN_ON 亮屏广播
     */
    private void registerReceiver() {
        networkChangedReceiver = new MyChatReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        intentFilter.addAction(Intent.ACTION_SCREEN_OFF);
        intentFilter.addAction(Intent.ACTION_SCREEN_ON);
        this.registerReceiver(networkChangedReceiver, intentFilter);
    }

    private void unregisterReceiver() {
        if (networkChangedReceiver != null) {
            this.unregisterReceiver(networkChangedReceiver);
        }
    }

    public class MyChatReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent == null) {
                return;
            }

            String action = intent.getAction();

            if (action == null) {
                return;
            }

            switch (action) {
                case ConnectivityManager.CONNECTIVITY_ACTION:
                    networkChanged(context);
                    break;
                case Intent.ACTION_SCREEN_OFF:

                    L.e(TAG, " MyChatReceiver ACTION_SCREEN_OFF : ");

                    chatDisconnect();

                    if (mCompositeDisposable != null) {
                        mCompositeDisposable.clear();
                    }

                    break;
                case Intent.ACTION_SCREEN_ON:

                    L.e(TAG, " MyChatReceiver ACTION_SCREEN_ON : " + isInit);

                    getChatIp();

                    break;

                default:
                    break;
            }


        }
    }

    private void networkChanged(Context context) {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        L.e(TAG, " networkChanged manager : " + manager);

        assert manager != null;

        NetworkInfo activeInfo = manager.getActiveNetworkInfo();

        L.e(TAG, " networkChanged activeInfo : " + activeInfo);

        if (activeInfo == null) {
            if (mChatClientImpl != null) {
                mChatClientImpl.clear();
            }
            pushConnectStatus(ConnectStatus.FAILED);
            chatDisconnect();
        } else {

            L.e(TAG, " networkChanged : " + isInit);

            if (mConnectStatus != ConnectStatus.RECONNECT) {
                getChatIp();
            }
            pushConnectStatus(ConnectStatus.RECONNECT);
        }
    }

    private void reConnectDelayFiveSeconds() {

        L.e(TAG, " iMsgClient reConnectDelayFiveSeconds : ");

        mCompositeDisposable.clear();

        reConnectDisposable = Flowable.timer(5, TimeUnit.SECONDS).onBackpressureDrop().subscribe(new Consumer<Long>() {
            @Override
            public void accept(Long aLong) {
                L.e(TAG, " iMsgClient reConnect : ");
                chatDisconnect();
                getChatIp();
            }
        });
        mCompositeDisposable.add(reConnectDisposable);
    }

    private NettyListener mNettyListener = new NettyListener() {

        @Override
        public void onMessageResponse(MsgPacket msgPacket) {

            L.d(TAG, " onMessageResponse " + msgPacket.code);

            L.d(TAG, " onMessageResponse " + msgPacket.code);

            if (mNettyClient != null) {
                mNettyClient.responseMsg(TheLConstants.MsgTypeConstant.MSG_TYPE_OK, "");
            }

            if (!TextUtils.isEmpty(msgPacket.code) && !msgPacket.isResponse) {
                if (TheLConstants.MsgTypeConstant.MSG_TYPE_INCOMING.equals(msgPacket.code)) {
                    mChatClientImpl.syncMsg(TheLConstants.SyncMsgType.SYNC_MSG_COMING);
                } else if (msgPacket.code.startsWith(TheLConstants.MsgTypeConstant.MSG_TYPE_DIRECT_FROM)) {
                    mChatClientImpl.receivePendingMsg(msgPacket);
                }
            }

        }

        @Override
        public void onServiceStatusConnectChanged(int statusCode) {

            L.d("NettyClient", " onServiceStatusConnectChanged statusCode " + statusCode);

            switch (statusCode) {
                case NettyConstants.ConnectConstants.STATUS_CONNECT_SUCCESS:
                    try {
                        if (mIMainProcessCallback != null) {
                            mIMainProcessCallback.umengPushStatus(6, null, null);
                        }
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }

                    if (mNettyClient != null) {
                        mNettyClient.setConnectStatus(true);
                    }

                    L.d("NettyClient", " mChatServiceBinder " + mChatServiceBinder);

                    L.d("NettyClient", " onServiceStatusConnectChanged userId " + userId);

                    L.d("NettyClient", " onServiceStatusConnectChanged DBManager.getInstance().getMyUserId() " + DBManager.getInstance().getMyUserId());

                    if (mChatServiceBinder != null) {
                        mChatServiceBinder.auth();
                    }

                    break;
                case NettyConstants.ConnectConstants.STATUS_CONNECT_CLOSED:
                    if (mNettyClient != null) {
                        mNettyClient.setConnectStatus(false);
                    }
                    break;
                case NettyConstants.ConnectConstants.STATUS_CONNECT_ERROR:

                    L.e(TAG, " iMsgClient reConnectDelayFiveSeconds ConnectConstants : " + NettyConstants.ConnectConstants.STATUS_CONNECT_ERROR);

                    try {
                        if (mIMainProcessCallback != null && mChatServiceBinder != null) {

                            mIMainProcessCallback.umengPushStatus(2, null, mChatServiceBinder.getChatIp());

//                            chatDisconnect();
//
//                            getChatIp();

                        }
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                    break;
                default:
                    break;
            }


        }
    };

    private void getChatIp() {
        String userId = DBManager.getInstance().getMyUserId();
        if (userId == null || userId.length() <= 0) {
            return;
        }

        String host;

        int port;

        if (chatIp != null && chatIp.length() > 0) {
            host = chatIp;
            port = RequestConstants.msgServerPort;

        } else {
            host = ShareFileUtils.getString(ShareFileUtils.IM_HOST, UrlConstants.IM_HOST);
            port = RequestConstants.msgServerPort;

        }

        chatDisconnect();

        if (mNettyClient != null) {
            mNettyClient.connect(host, port);
        }
    }

}
