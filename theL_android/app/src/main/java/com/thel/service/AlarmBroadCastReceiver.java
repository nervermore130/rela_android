package com.thel.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import androidx.core.app.NotificationCompat;

import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.constants.TheLConstants;
import com.thel.modules.main.MainActivity;
import com.thel.utils.ActivityUtils;

/**
 * Created by setsail on 15/11/18.
 */
public class AlarmBroadCastReceiver extends BroadcastReceiver {

    public static String ACTION_MATCH = "com.thel.alarm.match";

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            showNotification();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 弹通知
     */
    public void showNotification() {
        // 获取头像
        Intent intent = new Intent();
        if (ActivityUtils.getBaseActivity(TheLApp.context).equals("com.thel.ui.activity.WelcomeActivity")) {
            // 如果当前在app中，点通知栏进入到聊天页
            intent.setClass(TheLApp.getContext(), MainActivity.class);
        } else {
            // 当前在lancher中，点通知栏进入到欢迎页，再到ThelTabActivity中
            // 相当于从通知栏启动app
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            intent.setClass(TheLApp.getContext(), MainActivity.class);
        }

        intent.putExtra(TheLConstants.BUNDLE_KEY_WHERE_TO_GO, "Match");
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        String ns = Context.NOTIFICATION_SERVICE;

        NotificationManager mNotificationManager = (NotificationManager) TheLApp.getContext().getSystemService(ns);

        PendingIntent pendingIntent = PendingIntent.getActivity(TheLApp.getContext(), R.string.app_name, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mBuilder = new NotificationCompat.Builder(TheLApp.getContext(), TheLConstants.NotificationChannelConstants.MSG_ALARM_CHANNEL_ID);
        } else {
            mBuilder = new NotificationCompat.Builder(TheLApp.getContext());
        }
        mBuilder.setContentTitle("Rela");
        mBuilder.setContentText(TheLApp.getContext().getString(R.string.match_alarm));
        mBuilder.setContentIntent(pendingIntent);
        mBuilder.setAutoCancel(true);
        mBuilder.setSmallIcon(R.drawable.ic_launcher);
        mBuilder.setLights(3000, 3000, 3000);
        mBuilder.setSound(Uri.parse("android.resource://com.thel/" + R.raw.sound_msg));

        // 把Notification传递给NotificationManager
        mNotificationManager.notify(TheLConstants.NOTIFICATION_ID_MATCH_ALARM, mBuilder.build());
    }

}
