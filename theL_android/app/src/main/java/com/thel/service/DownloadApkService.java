package com.thel.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.IBinder;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.liulishuo.filedownloader.BaseDownloadTask;
import com.liulishuo.filedownloader.FileDownloadSampleListener;
import com.liulishuo.filedownloader.FileDownloader;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.constants.TheLConstants;
import com.thel.utils.InstallUtils;
import com.thel.utils.L;

import java.io.File;
import java.text.NumberFormat;

/**
 * desc   : 后台下载应用的service
 * author : liuyun
 * date   : 2019-04-29 12:18
 * email  : poikl369@qq.com
 */
public class DownloadApkService extends Service {

    public static final String DOWNLOAD_START = "download_start";

    public static final String DOWNLOAD_RELOAD = "download_reload";

    public static final String DOWNLOAD_SUCCESS = "download_success";

    private static final String TAG = "DownloadApkService";

    private final static int NOTIFICATION_CHANNEL_ID = 1;

    private static final String NOTIFICATION_CHANNEL_NAME = "Download";

    private NotificationManager mNotificationManager;

    private NotificationCompat.Builder mBuilder;

    private String path = TheLConstants.F_DOWNLOAD_ROOTPATH + "Rela.apk";

    private String url;

    private String apkVersion;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        L.d(TAG, " Download onCreate : ");

        mNotificationManager = (NotificationManager) getApplicationContext().getSystemService(NOTIFICATION_SERVICE);

        initNotificationBuild();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        String action = intent.getAction();

        L.d(TAG, " Download onStartCommand action : " + action);

        if (action != null) {

            switch (action) {
                case DOWNLOAD_START:

                    url = intent.getStringExtra("download_url");

                    apkVersion = intent.getStringExtra("apk_version");

                    L.d(TAG, " Download onStartCommand url : " + url);

                    path = TheLConstants.F_DOWNLOAD_ROOTPATH + apkVersion + "_" + "Rela.apk";

                    File apkLocalFile = new File(path);

                    if (apkLocalFile.exists()) {
                        InstallUtils.installApk(getApplicationContext(), path);
                    } else {
                        updateNotification("0");
                        BaseDownloadTask downloadTask = FileDownloader.getImpl().create(url).setPath(path).setListener(fileDownloadSampleListener);
                        downloadTask.start();
                    }

                    break;
                case DOWNLOAD_RELOAD:

                    startDownload();

                    break;
                default:
                    break;
            }

        }

        return START_NOT_STICKY;
    }

    private void startDownload() {
        updateNotification("0");
        BaseDownloadTask downloadTask = FileDownloader.getImpl().create(url).setPath(path).setListener(fileDownloadSampleListener);
        downloadTask.start();
    }

    private FileDownloadSampleListener fileDownloadSampleListener = new FileDownloadSampleListener() {
        @Override
        protected void progress(BaseDownloadTask task, int soFarBytes, int totalBytes) {
            super.progress(task, soFarBytes, totalBytes);
            updateNotification(getPercent(soFarBytes, totalBytes));
        }

        @Override
        protected void error(BaseDownloadTask task, Throwable e) {
            super.error(task, e);

            L.d(TAG, " Download error : " + e.getMessage());

            updateNotification("-1");

        }

        @Override
        protected void paused(BaseDownloadTask task, int soFarBytes, int totalBytes) {
            super.paused(task, soFarBytes, totalBytes);

            L.d(TAG, " Download paused : ");

        }

        @Override
        protected void completed(BaseDownloadTask task) {
            super.completed(task);

            cancelNotification();

            stopService(new Intent(DownloadApkService.this, DownloadApkService.class));

            InstallUtils.installApk(getApplicationContext(), path);

            L.d(TAG, " Download completed : ");

        }

        @Override
        protected void warn(BaseDownloadTask task) {
            super.warn(task);
        }
    };

    private void updateNotification(String percent) {

        Intent intent;

        String progress_str;

        String content_str;

        if (percent.equals("-1")) {

            intent = new Intent(getBaseContext(), DownloadApkService.class);

            intent.setAction(DownloadApkService.DOWNLOAD_RELOAD);

            progress_str = "";

            content_str = "更新失败,点击重试";

        } else {
            intent = new Intent();

            progress_str = TheLApp.context.getString(R.string.progress) + " : " + percent + "%";

            content_str = TheLApp.context.getString(R.string.rela_updating);
        }

        PendingIntent pendingIntent = PendingIntent.getService(TheLApp.context, 0, intent,  0);

        mBuilder.setContentTitle(content_str)
                .setContentText(progress_str)
                .setSound(null)
                .setVibrate(new long[]{0, 0, 0, 0, 0, 0})
                .setPriority(Integer.MAX_VALUE)
                .setWhen(System.currentTimeMillis())
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.mipmap.ic_launcher);
        Notification notify = mBuilder.build();
        notify.flags = Notification.FLAG_ONGOING_EVENT | notify.flags;
        notify.flags = Notification.FLAG_NO_CLEAR | notify.flags;
        mNotificationManager.notify(NOTIFICATION_CHANNEL_ID, notify);

    }

    private void initNotificationBuild() {
        //判断是否是8.0Android.O
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_NAME,
                    "Rela APK Download", NotificationManager.IMPORTANCE_LOW);
            channel.setLightColor(Color.GREEN);
            channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            mNotificationManager.createNotificationChannel(channel);
            mBuilder = new NotificationCompat.Builder(getApplicationContext(), NOTIFICATION_CHANNEL_NAME);
        } else {
            mBuilder = new NotificationCompat.Builder(getApplicationContext());
        }
    }

    private synchronized String getPercent(int soFarBytes, int totalBytes) {
        NumberFormat numberFormat = NumberFormat.getInstance();

        numberFormat.setMaximumFractionDigits(0);

        return numberFormat.format((float) soFarBytes / (float) totalBytes * 100);
    }

    private void cancelNotification() {
        if (mNotificationManager != null) {
            mNotificationManager.cancel(NOTIFICATION_CHANNEL_ID);
        }
    }

}
