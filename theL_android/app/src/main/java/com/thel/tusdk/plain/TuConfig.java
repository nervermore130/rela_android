package com.thel.tusdk.plain;

/**
 * @author xujie
 * @Date 2018/11/26
 */

public class TuConfig {
    // 录制滤镜 code 列表, 每个 code 代表一种滤镜效果, 具体 code 可在 lsq_tusdk_configs.json 查看 (例如:lsq_filter_SkinNature02 滤镜的 code 为 SkinNature02)
    public static final String[] CAMERA_FILTER_CODES = new String[]{"None", "SkinNature20","SkinPink20", "SkinJelly20", "SkinNoir20", "SkinRuddy20",
            "SkinSugar20", "SkinPowder20", "SkinWheat20","SkinSoft20","SkinPure20","SkinMoving20","SkinPast20","SkinCookies20",
            "SkinRose20"};

}
