package com.thel.tusdk.plain;

import com.qiniu.pili.droid.streaming.AVCodecType;
import com.qiniu.pili.droid.streaming.StreamingProfile;
import com.qiniu.pili.droid.streaming.WatermarkSetting;

import java.io.Serializable;

import static com.qiniu.pili.droid.streaming.StreamingProfile.AUDIO_QUALITY_HIGH1;
import static com.qiniu.pili.droid.streaming.StreamingProfile.AUDIO_QUALITY_HIGH2;
import static com.qiniu.pili.droid.streaming.StreamingProfile.AUDIO_QUALITY_LOW1;
import static com.qiniu.pili.droid.streaming.StreamingProfile.AUDIO_QUALITY_LOW2;
import static com.qiniu.pili.droid.streaming.StreamingProfile.AUDIO_QUALITY_MEDIUM1;
import static com.qiniu.pili.droid.streaming.StreamingProfile.AUDIO_QUALITY_MEDIUM2;
import static com.qiniu.pili.droid.streaming.StreamingProfile.VIDEO_QUALITY_HIGH1;
import static com.qiniu.pili.droid.streaming.StreamingProfile.VIDEO_QUALITY_HIGH2;
import static com.qiniu.pili.droid.streaming.StreamingProfile.VIDEO_QUALITY_HIGH3;
import static com.qiniu.pili.droid.streaming.StreamingProfile.VIDEO_QUALITY_LOW1;
import static com.qiniu.pili.droid.streaming.StreamingProfile.VIDEO_QUALITY_LOW2;
import static com.qiniu.pili.droid.streaming.StreamingProfile.VIDEO_QUALITY_LOW3;
import static com.qiniu.pili.droid.streaming.StreamingProfile.VIDEO_QUALITY_MEDIUM1;
import static com.qiniu.pili.droid.streaming.StreamingProfile.VIDEO_QUALITY_MEDIUM2;
import static com.qiniu.pili.droid.streaming.StreamingProfile.VIDEO_QUALITY_MEDIUM3;

public class EncodingConfig implements Serializable {
    public AVCodecType mCodecType = CODEC_TYPE[0];
    public boolean mIsAudioOnly = false;

    public boolean mIsVideoQualityPreset = true;
    public int mVideoQualityPreset = VIDEO_QUALITY_PRESETS_MAPPING[4];
    public int mVideoQualityCustomFPS = 20;
    public int mVideoQualityCustomBitrate = 1000;
    public int mVideoQualityCustomMaxKeyFrameInterval = 60;
    public StreamingProfile.H264Profile mVideoQualityCustomProfile = VIDEO_QUALITY_PROFILES_MAPPING[0];

    public boolean mIsVideoSizePreset = true;
    public int mVideoSizePreset = VIDEO_SIZE_PRESETS[1];

    public int mVideoSizeCustomWidth = 480;
    public int mVideoSizeCustomHeight = 848;

    public boolean mVideoOrientationPortrait = true;

    public boolean mVideoRateControlQuality = true;

    public StreamingProfile.BitrateAdjustMode mBitrateAdjustMode = BITRATE_ADJUST_MODE[0];
    public int mAdaptiveBitrateMin = 150;
    public int mAdaptiveBitrateMax = 800;

    public boolean mVideoFPSControl = true;

    public boolean mIsWatermarkEnabled = false;
    public int mWatermarkAlpha = 100;
    public WatermarkSetting.WATERMARK_SIZE mWatermarkSize = WATERMARK_SIZE_PRESETS_MAPPING[0];
    public boolean mIsWatermarkLocationPreset;
    public WatermarkSetting.WATERMARK_LOCATION mWatermarkLocationPreset = WATERMARK_LOCATION_PRESETS_MAPPING[0];
    public float mWatermarkLocationCustomX;
    public float mWatermarkLocationCustomY;

    public boolean mIsPictureStreamingEnabled = false;
    public String mPictureStreamingFilePath;

    public boolean mIsAudioQualityPreset = true;
    public int mAudioQualityPreset = AUDIO_QUALITY_PRESETS_MAPPING[3];
    public int mAudioQualityCustomSampleRate = 44100;
    public int mAudioQualityCustomBitrate = 96;

    public StreamingProfile.YuvFilterMode mYuvFilterMode = StreamingProfile.YuvFilterMode.values()[1];

    public boolean mAudioStereo = false;//立体音响

    public static final AVCodecType[] CODEC_TYPE = {
            AVCodecType.SW_VIDEO_WITH_SW_AUDIO_CODEC,
            AVCodecType.HW_VIDEO_SURFACE_AS_INPUT_WITH_HW_AUDIO_CODEC,
            AVCodecType.HW_VIDEO_YUV_AS_INPUT_WITH_HW_AUDIO_CODEC
    };

    public static final int[] VIDEO_QUALITY_PRESETS_MAPPING = {
            VIDEO_QUALITY_LOW1,
            VIDEO_QUALITY_LOW2,
            VIDEO_QUALITY_LOW3,
            VIDEO_QUALITY_MEDIUM1,
            VIDEO_QUALITY_MEDIUM2,
            VIDEO_QUALITY_MEDIUM3,
            VIDEO_QUALITY_HIGH1,
            VIDEO_QUALITY_HIGH2,
            VIDEO_QUALITY_HIGH3
    };

    public static final StreamingProfile.H264Profile[] VIDEO_QUALITY_PROFILES_MAPPING = {
            StreamingProfile.H264Profile.HIGH,
            StreamingProfile.H264Profile.MAIN,
            StreamingProfile.H264Profile.BASELINE
    };

    public static final int[] AUDIO_QUALITY_PRESETS_MAPPING = {
            AUDIO_QUALITY_LOW1,
            AUDIO_QUALITY_LOW2,
            AUDIO_QUALITY_MEDIUM1,
            AUDIO_QUALITY_MEDIUM2,
            AUDIO_QUALITY_HIGH1,
            AUDIO_QUALITY_HIGH2
    };

    public static final WatermarkSetting.WATERMARK_SIZE[] WATERMARK_SIZE_PRESETS_MAPPING = {
            WatermarkSetting.WATERMARK_SIZE.SMALL,
            WatermarkSetting.WATERMARK_SIZE.MEDIUM,
            WatermarkSetting.WATERMARK_SIZE.LARGE
    };

    public static final WatermarkSetting.WATERMARK_LOCATION[] WATERMARK_LOCATION_PRESETS_MAPPING = {
            WatermarkSetting.WATERMARK_LOCATION.NORTH_WEST,
            WatermarkSetting.WATERMARK_LOCATION.NORTH_EAST,
            WatermarkSetting.WATERMARK_LOCATION.SOUTH_EAST,
            WatermarkSetting.WATERMARK_LOCATION.SOUTH_WEST
    };

    public static final StreamingProfile.BitrateAdjustMode[] BITRATE_ADJUST_MODE = {
            StreamingProfile.BitrateAdjustMode.Auto,
            StreamingProfile.BitrateAdjustMode.Manual,
            StreamingProfile.BitrateAdjustMode.Disable
    };

    public static final int[] VIDEO_SIZE_PRESETS = {
            0,//"240p(320x240 (4:3), 424x240 (16:9))",
            1,//"480p(640x480 (4:3), 848x480 (16:9))",
            2,//"544p(720x544 (4:3), 960x544 (16:9))",
            3,//"720p(960x720 (4:3), 1280x720 (16:9))",
            4//"1080p(1440x1080 (4:3), 1920x1080 (16:9))"
    };
}
