package com.thel.tusdk.plain;

import android.hardware.Camera;

import com.qiniu.pili.droid.streaming.CameraStreamingSetting;

import java.io.Serializable;

public class CameraConfig implements Serializable {

    public static final String KEY_EYE_SIZE = "eyeSize";
    public static final String KEY_CHIN_SIZE = "chinSize";
    public static final String KEY_WHITENING = "whitening";
    public static final String KEY_SMOOTHING = "smoothing";

    public boolean mFrontFacing = true;
    public CameraStreamingSetting.PREVIEW_SIZE_LEVEL mSizeLevel = PREVIEW_SIZE_LEVEL_PRESETS_MAPPING[1];
    public CameraStreamingSetting.PREVIEW_SIZE_RATIO mSizeRatio = PREVIEW_SIZE_RATIO_PRESETS_MAPPING[1];
    public String mFocusMode = FOCUS_MODE_PRESETS_MAPPING[1];
    public boolean mIsFaceBeautyEnabled = true;
    public boolean mIsCustomFaceBeauty = false;
    public boolean mContinuousAutoFocus = true;
    public boolean mPreviewMirror = false;
    public boolean mEncodingMirror = false;
    public float mWhitening = 0.3f;
    public float mSmoothing = 0.3f;
    public float mEyeSize = 0.3f;
    public float mChinSize = 0.3f;

    public static final CameraStreamingSetting.PREVIEW_SIZE_LEVEL[] PREVIEW_SIZE_LEVEL_PRESETS_MAPPING = {
            CameraStreamingSetting.PREVIEW_SIZE_LEVEL.SMALL,
            CameraStreamingSetting.PREVIEW_SIZE_LEVEL.MEDIUM,
            CameraStreamingSetting.PREVIEW_SIZE_LEVEL.LARGE
    };

    public static final CameraStreamingSetting.PREVIEW_SIZE_RATIO[] PREVIEW_SIZE_RATIO_PRESETS_MAPPING = {
            CameraStreamingSetting.PREVIEW_SIZE_RATIO.RATIO_4_3,
            CameraStreamingSetting.PREVIEW_SIZE_RATIO.RATIO_16_9
    };

    public static final String[] FOCUS_MODE_PRESETS_MAPPING = {
            Camera.Parameters.FOCUS_MODE_AUTO,
            Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE,
            Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO
    };
}
