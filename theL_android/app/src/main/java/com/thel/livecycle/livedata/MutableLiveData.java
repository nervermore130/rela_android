package com.thel.livecycle.livedata;

import androidx.lifecycle.LiveData;

/**
 * @author liuyun
 */
public class MutableLiveData<D> extends LiveData<D> {
    @Override public void postValue(D value) {
        super.postValue(value);
    }

    @Override public void setValue(D value) {
        super.setValue(value);
    }
}
