package com.thel.base;

import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;

public abstract class IntentFilterActivity extends BaseFlutterActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Uri uri = getIntent().getData();

        if (uri != null) {

            final String fromPage = uri.getQueryParameter("fromPage");

            final String pageId = uri.getQueryParameter("pageId");

            pushLog(fromPage, pageId);

        }

    }

    private void pushLog(String fromPage, String pageId) {

        String page = "share_page";

    }

}
