package com.thel.base;

import android.graphics.Rect;
import android.view.View;

import com.thel.bean.ListItem;


/**
 * @author liuyun
 * @date 2017/9/19
 */

public class BaseDataBean implements ListItem {
    /**
     * 错误标识
     */
    public String errcode = "";

    /**
     * 错误描述
     */
    public String errdesc = "";

    /**
     * 错误描述英文国际化
     */
    public String errdesc_en = "";

    /**
     * 是否成功 (成功为1，失败为0)
     */
    public String result = "";

    public int code;

    public String message;

    private final Rect mCurrentViewRect = new Rect();

    @Override
    public int getVisibilityPercents(View currentView) {

        int percents = 100;

        currentView.getLocalVisibleRect(mCurrentViewRect);

        int height = currentView.getHeight();

        if (viewIsPartiallyHiddenTop()) {
            // view is partially hidden behind the top edge
            percents = (height - mCurrentViewRect.top) * 100 / height;
        } else if (viewIsPartiallyHiddenBottom(height)) {
            percents = mCurrentViewRect.bottom * 100 / height;
        }

        return percents;
    }

    private boolean viewIsPartiallyHiddenBottom(int height) {
        return mCurrentViewRect.bottom > 0 && mCurrentViewRect.bottom < height;
    }

    private boolean viewIsPartiallyHiddenTop() {
        return mCurrentViewRect.top > 0;
    }

    @Override
    public void setActive(View newActiveView, int newActiveViewPosition) {

    }

    @Override
    public void deactivate(View currentView, int position) {

    }

    @Override
    public String toString() {
        return "BaseDataBean{" +
                "errcode='" + errcode + '\'' +
                ", errdesc='" + errdesc + '\'' +
                ", errdesc_en='" + errdesc_en + '\'' +
                ", result='" + result + '\'' +
                '}';
    }
}
