package com.thel.base;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by liuyun on 17/3/20.
 */

public class BaseAdapter<T extends ViewHolder, D> extends RecyclerView.Adapter<T> {

    public List<D> data = new ArrayList<>();

    public OnItemClickListener<D> mOnItemClickListener;

    public OnItemLongClickListener mOnItemLongClickListener;

    public OnDragItemClickListener mOnDragItemClickListener;

    public LayoutInflater mLayoutInflater;

    public Context mContext;

    public boolean hasHeader = false;

    public BaseAdapter(Context context) {
        this.mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
    }

    public void setOnItemClickListener(OnItemClickListener<D> mOnItemClickListener) {
        this.mOnItemClickListener = mOnItemClickListener;
    }

    public void setOnItemLongClickListener(OnItemLongClickListener mOnItemLongClickListener) {
        this.mOnItemLongClickListener = mOnItemLongClickListener;
    }

    public void setOnDragItemClickListener(OnDragItemClickListener mOnDragItemClickListener) {
        this.mOnDragItemClickListener = mOnDragItemClickListener;
    }

    public void clear() {
        data.clear();
        notifyDataSetChanged();
    }

    public void setData(List<D> data) {
        this.data.clear();
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    public void setData(int position, List<D> data) {
        this.data.clear();
        this.data.addAll(position, data);
        notifyDataSetChanged();
    }

    public void setDataNoCLear(int position, List<D> data) {
        this.data.addAll(position, data);
        notifyDataSetChanged();
    }

    public void addData(List<D> data) {
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    public List<D> getData() {
        return data;
    }

    public void setItem(D item) {
        this.data.clear();
        this.data.add(item);
        notifyDataSetChanged();
    }

    public void removeItem(D item) {
        this.data.remove(item);
    }

    @Override
    public T onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(T holder, int position) {

    }

    @Override public int getItemCount() {
        return data.size();
    }

    public interface OnItemClickListener<T> {
        void onItemClick(View view, T item, int position);
    }

    public interface OnItemLongClickListener<T> {
        void onLongItemClick(View view, T item, int position);
    }

    public interface OnDragItemClickListener {
        void OnDragItemClick(ViewHolder viewHolder);
    }

}
