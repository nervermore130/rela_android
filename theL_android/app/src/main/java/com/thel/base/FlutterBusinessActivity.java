package com.thel.base;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import com.thel.imp.follow.FollowStatusChangedImpl;
import com.thel.imp.follow.FollowStatusChangedListener;
import com.thel.ui.LoadingDialogImpl;

abstract public class FlutterBusinessActivity extends  BaseFlutterActivity implements FollowStatusChangedListener {

    private LoadingDialogImpl loadingDialog;

    protected FollowStatusChangedImpl followStatusChangedImpl;

    @Override protected void onCreate(Bundle savedInstanceState) {

        loadingDialog = LoadingDialogImpl.LoadingDialogInstance.getInstance(this);

        followStatusChangedImpl = new FollowStatusChangedImpl();

        followStatusChangedImpl.registerReceiver(this);

        super.onCreate(savedInstanceState);

        setContentView(initLayout());

        initFragment();

    }

    public void setTranslucentBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //5.0 以上直接设置状态栏颜色
            View decorView = getWindow().getDecorView();
            int option = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
            decorView.setSystemUiVisibility(option);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
    }

    abstract void initFragment();

    abstract int initLayout();

    @Override public void showLoading() {
        if (loadingDialog != null)
            loadingDialog.showLoading();
    }

    @Override public void showLoadingNoBack() {
        if (loadingDialog != null)
            loadingDialog.showLoadingNoBack();
    }

    @Override public void closeLoading() {
        if (loadingDialog != null)
            loadingDialog.closeDialog();
    }

    @Override
    public void onFollowStatusChanged(int followStatus, String userId, String nickName, String avatar) {
        closeLoading();
    }

}
