package com.thel.base;

/**
 * Created by liuyun on 2017/9/13.
 */

public interface BaseView<T> {

    void setPresenter(T presenter);
}
