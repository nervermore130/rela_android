package com.thel.base;

import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.thel.ui.imageviewer.widget.ImageViewer;


/**
 * Created by chad
 * Time 18/12/17
 * Email: wuxianchuang@foxmail.com
 * Description: TODO
 */
public class BaseImageViewerActivity extends BaseActivity {


    public ImageViewer imageViewer;

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);

        imageViewer = new ImageViewer(this);
        imageViewer.setBackgroundColor(Color.BLACK);
        FrameLayout contentView = findViewById(android.R.id.content);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        contentView.addView(imageViewer, layoutParams);
        imageViewer.setVisibility(View.GONE);
    }

    public ImageViewer getImageViewer() {
        return imageViewer;
    }

    @Override public boolean isStatusBarTranslucent() {
        return false;
    }
}
