package com.thel.base;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.text.TextUtils;
import android.view.View;
import android.view.Window;

import com.gyf.immersionbar.ImmersionBar;
import com.thel.R;
import com.thel.app.TheLApp;
import com.thel.flutter.OnActivityResultCallback;
import com.thel.imp.follow.FollowStatusChangedImpl;
import com.thel.imp.follow.FollowStatusChangedListener;
import com.thel.manager.ActivityManager;
import com.thel.manager.ChatServiceManager;
import com.thel.ui.LoadingDialogImpl;
import com.thel.utils.L;
import com.thel.utils.NotchUtils;
import com.thel.utils.UserUtils;


/**
 * Created by liuyun on 2017/9/13.
 */

abstract public class BaseActivity extends AppCompatActivity implements FollowStatusChangedListener {

    private LoadingDialogImpl loadingDialog;

    protected FollowStatusChangedImpl followStatusChangedImpl;

    protected boolean isShowPopup = false;

    private int safeInsetTop = 0;

    protected OnActivityResultCallback onActivityResultCallback;

    public void setOnActivityResultCallback(OnActivityResultCallback onActivityResultCallback) {
        this.onActivityResultCallback = onActivityResultCallback;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (onActivityResultCallback != null) {
            onActivityResultCallback.onActivityResult(requestCode, resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);

        showStatusBar();

        ActivityManager.getInstance().addActivity(this);

        loadingDialog = LoadingDialogImpl.LoadingDialogInstance.getInstance(this);

        followStatusChangedImpl = new FollowStatusChangedImpl();

        followStatusChangedImpl.registerReceiver(this);

        if (safeInsetTop <= 0) {
            safeInsetTop = NotchUtils.getNotchHeight(this);
        }

        L.d("BaseActivity", " Router onCreate : " + ActivityManager.getInstance().getActivitysStr());
    }

    @Override
    protected void onResume() {
        try {
            if (TheLApp.context != null && !TextUtils.isEmpty(UserUtils.getMyUserId())) {
                ChatServiceManager.getInstance().startService(TheLApp.context);
            }
            super.onResume();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onStop() { // todo #2202 java.lang.IllegalStateException Fragment has not been attached yet. 暂时加了try cache
        try {
            super.onStop();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //让其不再保存Fragment的状态，达到fragment随Activity一起销毁的目的。
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDetachedFromWindow() {

        super.onDetachedFromWindow();
    }

    @Override
    public void finish() {
        super.finish();
    }

    @Override
    protected void onDestroy() {

        if (loadingDialog != null) {
            loadingDialog.destroyDialog();
        }
        loadingDialog = null;

        ActivityManager.getInstance().removeActivity(this);

        followStatusChangedImpl.unRegisterReceiver(this);

        super.onDestroy();

        L.d("BaseActivity", " Router onDestroy : " + ActivityManager.getInstance().getActivitysStr());

    }

    public void showLoading() {
        if (loadingDialog != null)
            loadingDialog.showLoading();
    }

    public void showLoadingNoBack() {
        if (loadingDialog != null) {
            loadingDialog.showLoadingNoBack();
        }
    }

    public void closeLoading() {
        if (loadingDialog != null) {
            loadingDialog.closeDialog();
        }
    }

    public void showStatusBar() {

        if (isStatusBarTranslucent()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                //5.0 以上直接设置状态栏颜色
                View decorView = getWindow().getDecorView();
                int option = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
                decorView.setSystemUiVisibility(option);
                getWindow().setStatusBarColor(Color.TRANSPARENT);
            }
        } else {

            ImmersionBar.with(this)
                    .statusBarColor(R.color.white)
                    .statusBarDarkFont(true, 0.2f)
                    .navigationBarDarkIcon(true)
                    .navigationBarColor(R.color.white)
                    .init();
        }
    }

    /**
     * 利用反射获取状态栏高度
     *
     * @return
     */
    public int getStatusBarHeight() {
        try {
            int result = 0;
            //获取状态栏高度的资源id
            int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
            if (resourceId > 0) {
                result = getResources().getDimensionPixelSize(resourceId);
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return 24;
        }
    }

    protected <V> V findView(int id) {
        //noinspection unchecked
        return (V) findViewById(id);
    }

    @Override
    public void onFollowStatusChanged(int followStatus, String userId, String nickName, String avatar) {
        closeLoading();
    }

    public String getThelString(int resId) {
        return TheLApp.context.getResources().getString(resId);
    }

    public void fullScreenImmersive(Window window) {
        if (window != null && isShowPopup) {
            fullScreenPopupWindow(window.getDecorView());
        }
    }

    private void fullScreenPopupWindow(View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            int uiOptions = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;

            view.setSystemUiVisibility(uiOptions);
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            fullScreenImmersive(getWindow());
        }
    }

    public void setShowPopup(boolean isShowPopup) {
        this.isShowPopup = isShowPopup;
    }

    public abstract boolean isStatusBarTranslucent();

}
