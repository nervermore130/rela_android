package com.thel.player;

import android.content.Context;

import com.ksyun.media.player.KSYMediaPlayer;

/**
 * @author liuyun
 * @date 2017/3/14
 */

public class KSYFloatingPlayer {

    private KSYMediaPlayer mKsyMediaPlayer;

    private static KSYFloatingPlayer INSTANCE;

    private KSYFloatingPlayer() {
    }

    public static KSYFloatingPlayer getInstance() {
        if (INSTANCE == null) {
            synchronized (KSYFloatingPlayer.class) {
                if (INSTANCE == null)
                    INSTANCE = new KSYFloatingPlayer();
            }
        }

        return INSTANCE;
    }

    public KSYMediaPlayer init(Context context) {
        if (mKsyMediaPlayer != null) {
            mKsyMediaPlayer.release();
            mKsyMediaPlayer = null;
        }

        return mKsyMediaPlayer = new KSYMediaPlayer.Builder(context).build();
    }

    public KSYMediaPlayer getKSYMediaPlayer() {
        return mKsyMediaPlayer;
    }

    public void destroy() {
        if (mKsyMediaPlayer != null)
            mKsyMediaPlayer.release();

        mKsyMediaPlayer = null;
    }
}
