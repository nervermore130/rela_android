package com.thel.player.video;

/**
 * @author liuyun
 * @date 2018/3/2
 */

public interface VideoPlayerCallback {

    /**
     * 播放视频
     *
     * @param builder
     */
    void play(VideoInfoBuilder builder);

    /**
     * 停止播放
     */
    void stopPlay();

    /**
     * 恢复播放
     */
    void resume();

    /**
     * 暂停
     */
    void pause();

    /**
     * 重置播放器
     */
    void reset();

    /**
     * 获取播放地址
     *
     * @return String
     */
    String getUrl();

    /**
     * 视频是否播放
     *
     * @return boolean
     */
    boolean isPlaying();

    /**
     * 获取构造视频的builder
     *
     * @return VideoInfoBuilder
     */
    VideoInfoBuilder getBuilder();

}
