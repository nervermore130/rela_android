package com.thel.player.video;

import android.content.Intent;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.view.Surface;
import android.view.TextureView;

import com.thel.app.TheLApp;
import com.thel.constants.ActionConstants;
import com.thel.constants.BundleConstants;
import com.thel.ui.widget.SquareTextureView;
import com.thel.utils.L;

import java.io.IOException;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

/**
 *
 * @author liuyun
 * @date 2017/10/11
 */

public class VideoPlayer {

    private static final String TAG = "VideoPlayer";

    private static VideoPlayer ourInstance = null;

    private Surface mSurface;

    private MediaPlayer mMediaPlayer;

    private LocalBroadcastManager mLocalBroadcastManager = LocalBroadcastManager.getInstance(TheLApp.context);

    private String momentsId;

    public static VideoPlayer getInstance() {
        if (ourInstance == null) {
            synchronized (VideoPlayer.class) {
                ourInstance = new VideoPlayer();
            }
        }
        return ourInstance;
    }

    private VideoPlayer() {

    }

    public void startPlayer(SquareTextureView textureView, String url, String momentsId) {

        this.momentsId = momentsId;

        textureView.setSurfaceTextureListener(mSurfaceTextureListener);

        if (mMediaPlayer == null) {
            mMediaPlayer = new MediaPlayer();
        } else {
            mMediaPlayer.reset();
        }
        mMediaPlayer.setOnErrorListener(mOnErrorListener);
        mMediaPlayer.setOnPreparedListener(mOnPreparedListener);
        mMediaPlayer.setOnCompletionListener(mOnCompletionListener);
        mMediaPlayer.setOnInfoListener(mOnInfoListener);
        mMediaPlayer.setLooping(true);
        try {
            mMediaPlayer.setDataSource(url);
            mMediaPlayer.prepareAsync();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private TextureView.SurfaceTextureListener mSurfaceTextureListener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i1) {

            L.d(TAG, "-----onSurfaceTextureAvailable------");

            if (mSurface != null)
                mSurface.release();

            mSurface = new Surface(surfaceTexture);
            if (mMediaPlayer != null)
                mMediaPlayer.setSurface(mSurface);
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i1) {

        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
            if (mSurface != null) {
                mSurface.release();
                mSurface = null;
            }

            if (mMediaPlayer != null)
                mMediaPlayer.setSurface(null);

            return false;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {

        }
    };

    public void startPlayer(String url, String momentsId, Surface surface) {

        this.momentsId = momentsId;

        if (mMediaPlayer == null) {
            mMediaPlayer = new MediaPlayer();
        } else {
            mMediaPlayer.reset();
        }
        mMediaPlayer.setOnErrorListener(mOnErrorListener);
        mMediaPlayer.setOnPreparedListener(mOnPreparedListener);
        mMediaPlayer.setOnCompletionListener(mOnCompletionListener);
        mMediaPlayer.setOnInfoListener(mOnInfoListener);
        mMediaPlayer.setLooping(true);
        if (surface != null) {
            mMediaPlayer.setSurface(surface);
        }
        try {
            mMediaPlayer.setDataSource(url);
            mMediaPlayer.prepareAsync();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private MediaPlayer.OnPreparedListener mOnPreparedListener = new MediaPlayer.OnPreparedListener() {
        @Override
        public void onPrepared(MediaPlayer mediaPlayer) {
            if (mMediaPlayer != null) {
                L.d(TAG, " onPrepared : ");
                mMediaPlayer.start();
            }
        }
    };

    private MediaPlayer.OnErrorListener mOnErrorListener = new MediaPlayer.OnErrorListener() {
        @Override
        public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
            L.d(TAG, " onError : ");
            return false;
        }
    };

    private MediaPlayer.OnCompletionListener mOnCompletionListener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mediaPlayer) {
            sendBroadcast(ActionConstants.VIDEO_STOP);

            L.d(TAG, " onCompletion : ");
        }
    };

    private MediaPlayer.OnInfoListener mOnInfoListener = new MediaPlayer.OnInfoListener() {
        @Override public boolean onInfo(MediaPlayer mediaPlayer, int what, int i1) {
            switch (what) {
                case MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START:
                    sendBroadcast(ActionConstants.VIDEO_START);
                    L.d(TAG, " 视频开始渲染 ");
                    break;
            }
            return false;
        }
    };

    public void release() {
        if (mMediaPlayer != null) {
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }

    public void stop() {
        if (mMediaPlayer != null) {
            mMediaPlayer.stop();
        }
    }

    public MediaPlayer getMediaPlayer() {
        return mMediaPlayer;
    }

    private void sendBroadcast(String action) {
        if (momentsId != null) {
            Intent intent = new Intent();
            intent.setAction(action);
            intent.putExtra(BundleConstants.MOMENTS_ID, momentsId);
            mLocalBroadcastManager.sendBroadcast(intent);
        }
    }

}
