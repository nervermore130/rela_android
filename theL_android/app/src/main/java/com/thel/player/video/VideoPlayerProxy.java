package com.thel.player.video;

import android.graphics.SurfaceTexture;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.FrameLayout;

import com.kingsoft.media.httpcache.KSYProxyService;
import com.ksyun.media.player.IMediaPlayer;
import com.ksyun.media.player.KSYMediaPlayer;
import com.thel.player.KSYFloatingPlayer;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.L;
import com.thel.utils.VideoUtils;

import java.io.IOException;

import video.com.relavideolibrary.RelaVideoSDK;

/**
 * @author liuyun
 * @date 2017/10/16
 */

public class VideoPlayerProxy implements VideoPlayerCallback {

    private TextureView mTextureView;

    private String url = null;

    private String momentId;

    private String ksyUrl = null;

    private KSYMediaPlayer mKSYMediaPlayer;

    private Surface mSurface;

    private FrameLayout video_container;

    private VideoInfoBuilder builder;

    private VideoStatus mVideoStatus = VideoStatus.NONE;

    private KSYProxyService ksyProxyService;

    private VideoInfoBuilder.OnVideoRenderingStartListener mOnVideoRenderingStartListener;

    private static final VideoPlayerProxy ourInstance = new VideoPlayerProxy();

    public static VideoPlayerProxy getInstance() {
        return ourInstance;
    }

    private VideoPlayerProxy() {

        ksyProxyService = RelaVideoSDK.getKSYProxy();

        ksyProxyService.startServer();
    }

    @Override
    public void play(VideoInfoBuilder builder) {

        mVideoStatus = VideoStatus.PLAY;

        this.builder = builder;

        this.momentId = builder.momentId;

        this.mTextureView = builder.textureView;

        this.video_container = builder.video_container;

        this.url = builder.url;

        this.mOnVideoRenderingStartListener = builder.onVideoRenderingStartListener;

        initPlayer();

        mKSYMediaPlayer.setSurface(null);

        mKSYMediaPlayer.softReset();

        ksyUrl = ksyProxyService.getProxyUrl(builder.url);

        if (builder.surface != null) {
            startToPlay(ksyUrl, builder.surface);
        } else {
            if (mTextureView.isAvailable()) {
                startToPlay(ksyUrl, new Surface(mTextureView.getSurfaceTexture()));
            } else {
                mTextureView.setSurfaceTextureListener(mSurfaceTextureListener);
            }
        }

    }

    private void initPlayer() {

        mKSYMediaPlayer = KSYFloatingPlayer.getInstance().getKSYMediaPlayer();

        if (mKSYMediaPlayer == null) {

            mKSYMediaPlayer = KSYFloatingPlayer.getInstance().init(mTextureView.getContext());

        }
    }

    @Override
    public void stopPlay() {

        L.d("Fragment", " stopPlay : ");

        if (mKSYMediaPlayer != null && mVideoStatus == VideoStatus.PLAY) {
            mKSYMediaPlayer.reset();
            mKSYMediaPlayer.stop();
            mKSYMediaPlayer = null;
        }

        url = null;

        ksyUrl = null;

        mVideoStatus = VideoStatus.NONE;

    }

    @Override
    public void resume() {

        mVideoStatus = VideoStatus.PLAY;

        if (mKSYMediaPlayer != null) {
            mKSYMediaPlayer.start();
        }
    }

    @Override
    public void pause() {
        L.d("Fragment", " pause : ");
        mVideoStatus = VideoStatus.PAUSE;

        if (mKSYMediaPlayer != null) {
            mKSYMediaPlayer.pause();
        }
    }

    @Override public void reset() {
        mVideoStatus = VideoStatus.PAUSE;
        L.d("Fragment", " reset : ");
        if (mKSYMediaPlayer != null) {
            mKSYMediaPlayer.stop();
            mKSYMediaPlayer.reset();
        }
    }

    @Override
    public String getUrl() {
        return url == null ? "" : url;
    }

    @Override
    public boolean isPlaying() {
        return mVideoStatus == VideoStatus.PLAY;
    }

    @Override
    public VideoInfoBuilder getBuilder() {
        return builder;
    }


    private void startToPlay(String url, Surface mSurface) {

        L.d("Fragment", " startToPlay : ");


        initPlayer();

        if (builder != null && builder.onPreparedListener != null) {
            mKSYMediaPlayer.setOnPreparedListener(builder.onPreparedListener);
        } else {
            mKSYMediaPlayer.setOnPreparedListener(mOnPreparedListener);
        }

        if (builder != null && builder.onErrorListener != null) {
            mKSYMediaPlayer.setOnErrorListener(builder.onErrorListener);

        } else {
            mKSYMediaPlayer.setOnErrorListener(mOnErrorListener);
        }

        if (builder != null && builder.onCompletionListener != null) {
            mKSYMediaPlayer.setOnCompletionListener(builder.onCompletionListener);

        } else {
            mKSYMediaPlayer.setOnCompletionListener(mOnCompletionListener);

        }

        if (builder != null && builder.onInfoListener != null) {
            mKSYMediaPlayer.setOnInfoListener(builder.onInfoListener);

        } else {
            mKSYMediaPlayer.setOnInfoListener(mOnInfoListener);

        }

        mKSYMediaPlayer.setBufferTimeMax(2);

        mKSYMediaPlayer.setBufferSize(15);

        mKSYMediaPlayer.setDecodeMode(KSYMediaPlayer.KSYDecodeMode.KSY_DECODE_MODE_SOFTWARE);

        float volume = 0;

        if (builder != null) {
            volume = builder.volume;
        }

        mKSYMediaPlayer.setVolume(volume, volume);

        try {
            mKSYMediaPlayer.setDataSource(url);
            mKSYMediaPlayer.setSurface(mSurface);
            mKSYMediaPlayer.prepareAsync();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private IMediaPlayer.OnPreparedListener mOnPreparedListener = new IMediaPlayer.OnPreparedListener() {
        @Override public void onPrepared(IMediaPlayer iMediaPlayer) {

            startTime = System.currentTimeMillis();

            if (mKSYMediaPlayer != null) {

                mKSYMediaPlayer.setVideoScalingMode(KSYMediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);

                mKSYMediaPlayer.start();

            }

        }
    };

    private IMediaPlayer.OnInfoListener mOnInfoListener = new IMediaPlayer.OnInfoListener() {
        @Override public boolean onInfo(IMediaPlayer iMediaPlayer, int i, int i1) {

            switch (i) {

                case KSYMediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START:
                    if (mOnVideoRenderingStartListener != null) {
                        mOnVideoRenderingStartListener.onVideoRenderingStart();
                    }
                    break;

                default:
                    break;
            }

            return false;
        }
    };

    private IMediaPlayer.OnCompletionListener mOnCompletionListener = new IMediaPlayer.OnCompletionListener() {

        @Override public void onCompletion(IMediaPlayer iMediaPlayer) {

            if (mKSYMediaPlayer != null) {
                playCount++;
                mKSYMediaPlayer.seekTo(0);
                mKSYMediaPlayer.start();
            }

        }
    };

    private IMediaPlayer.OnErrorListener mOnErrorListener = new IMediaPlayer.OnErrorListener() {
        @Override public boolean onError(IMediaPlayer iMediaPlayer, int i, int i1) {
            return false;
        }
    };

    private int playCount = 1;

    private long playTime = 0;

    private long startTime = 0;

    private long endTime = 0;

    private void uploadVideoInfo() {

        if (builder != null) {

            endTime = System.currentTimeMillis();

            if (startTime > 0 && endTime - startTime > 0) {
                playTime = (endTime - startTime) / 1000;
            } else {
                playTime = 1;
            }

            GrowingIOUtil.uploadCommonVideoDuration(playTime);

            VideoUtils.postPlayVideoInfo(builder.entry, builder.momentId, playCount, playTime);

        }
    }

    public TextureView.SurfaceTextureListener mSurfaceTextureListener = new TextureView.SurfaceTextureListener() {

        @Override public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i1) {
            L.d("Fragment", " onSurfaceTextureAvailable : ");

            startToPlay(ksyUrl, new Surface(surfaceTexture));
        }

        @Override public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i1) {

        }

        @Override public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
            L.d("Fragment", " onSurfaceTextureDestroyed : ");

            releaseSurface();
            return true;
        }

        @Override public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {

        }
    };

    public enum VideoStatus {
        NONE, PLAY, PAUSE
    }

    private void releaseSurface() {

        if (this.mTextureView != null) {
            this.mTextureView.setVisibility(View.GONE);
        }

        if (mKSYMediaPlayer != null) {
            mKSYMediaPlayer.setSurface(null);
            stopPlay();
        }

        if (mSurface != null) {
            mSurface.release();
            mSurface = null;
        }

        uploadVideoInfo();

    }

}
