package com.thel.player.video;

import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.ksyun.media.player.IMediaPlayer;

/**
 * @author liuyun
 * @date 2018/3/1
 */

public class VideoInfoBuilder {

    public TextureView textureView;

    public Surface surface;

    public String url;

    public String momentId;

    public float volume;

    public int position;

    public String entry;

    public FrameLayout video_container;

    public IMediaPlayer.OnInfoListener onInfoListener;

    public IMediaPlayer.OnPreparedListener onPreparedListener;

    public IMediaPlayer.OnCompletionListener onCompletionListener;

    public IMediaPlayer.OnErrorListener onErrorListener;

    public OnVideoRenderingStartListener onVideoRenderingStartListener;

    public ViewGroup surfaceViewContainer;
    public SurfaceView surfaceView;
    public SurfaceHolder surfaceHolder;


    public VideoInfoBuilder(Builder builder) {
        this.url = builder.url;
        this.textureView = builder.textureView;
        this.video_container = builder.video_container;
        this.surface = builder.surface;
        this.momentId = builder.momentId;
        this.volume = builder.volume;
        this.position = builder.position;
        this.entry = builder.entry;
        this.onInfoListener = builder.onInfoListener;
        this.onPreparedListener = builder.onPreparedListener;
        this.onCompletionListener = builder.onCompletionListener;
        this.onErrorListener = builder.onErrorListener;
        this.onVideoRenderingStartListener = builder.onVideoRenderingStartListener;
        this.surfaceViewContainer = builder.surfaceViewContainer;
        this.surfaceView = builder.surfaceView;
        this.surfaceHolder = builder.surfaceHolder;
    }

    public static class Builder {

        private TextureView textureView;

        private FrameLayout video_container;

        private Surface surface;

        private String url;

        private String momentId;

        private float volume = 0;

        private int position;

        private String entry;

        private IMediaPlayer.OnInfoListener onInfoListener;

        private IMediaPlayer.OnPreparedListener onPreparedListener;

        private IMediaPlayer.OnCompletionListener onCompletionListener;

        private IMediaPlayer.OnErrorListener onErrorListener;

        private OnVideoRenderingStartListener onVideoRenderingStartListener;
        private ViewGroup surfaceViewContainer;
        private SurfaceView surfaceView;
        private SurfaceHolder surfaceHolder;

        /**
         * @param textureView 播放视频的TextureView
         * @return
         */
        public Builder setTextureView(TextureView textureView) {
            this.textureView = textureView;
            return this;
        }

        public Builder setVideoContainer(FrameLayout video_container) {
            this.video_container = video_container;
            if (video_container.getChildCount() > 0) {
                video_container.removeAllViews();
            }
            this.textureView = new TextureView(video_container.getContext());
            video_container.addView(textureView);
            return this;
        }

        /**
         * @param surface 设置TextureView 或 SurfaceView的surface
         * @return
         */
        public Builder setSurface(Surface surface) {
            this.surface = surface;
            return this;
        }

        /**
         * @param url 视频地址
         * @return
         */
        public Builder setUrl(String url) {
            this.url = url;
            return this;
        }

        /**
         * @param momentId 日志Id
         * @return
         */
        public Builder setMomentId(String momentId) {
            this.momentId = momentId;
            return this;
        }

        /**
         * @param volume 设置音量大小
         * @return
         */
        public Builder setVolume(float volume) {
            this.volume = volume;
            return this;
        }

        /**
         * @param position 设置视频位于列表中的position
         * @return
         */

        public Builder setPosition(int position) {
            this.position = position;
            return this;
        }

        /**
         * @param entry 播放视频的界面
         * @return
         */
        public Builder setEntry(String entry) {
            this.entry = entry;
            return this;
        }

        /**
         * @param onInfoListener 设置流媒体播放过程的监听
         * @return
         */
        public Builder setOnInfoListener(IMediaPlayer.OnInfoListener onInfoListener) {
            this.onInfoListener = onInfoListener;
            return this;
        }

        /**
         * @param onPreparedListener 设置流媒体的加载完成后的监听
         * @return
         */
        public Builder setOnPreparedListener(IMediaPlayer.OnPreparedListener onPreparedListener) {
            this.onPreparedListener = onPreparedListener;
            return this;
        }

        /**
         * @param onCompletionListener 设置流媒体的完成播放监听
         * @return
         */
        public Builder setOnCompletionListener(IMediaPlayer.OnCompletionListener onCompletionListener) {
            this.onCompletionListener = onCompletionListener;
            return this;
        }

        /**
         * @param onErrorListener 设置流媒体的错误监听
         * @return
         */
        public Builder setOnErrorListener(IMediaPlayer.OnErrorListener onErrorListener) {
            this.onErrorListener = onErrorListener;
            return this;
        }

        /**
         * 视频开始渲染
         *
         * @param onVideoRenderingStartListener
         * @return
         */
        public Builder setOnVideoRenderingStart(OnVideoRenderingStartListener onVideoRenderingStartListener) {
            this.onVideoRenderingStartListener = onVideoRenderingStartListener;
            return this;
        }

        public VideoInfoBuilder build() {
            return new VideoInfoBuilder(this);
        }

        /**
         * surfaceView 父容器
         *
         * @return
         */
        public Builder setSurfaceViewContainer(ViewGroup container) {
            this.surfaceViewContainer = container;
            if (surfaceViewContainer.getChildCount() > 0) {
                surfaceViewContainer.removeAllViews();
            }
            this.surfaceView = new SurfaceView(surfaceViewContainer.getContext());
            surfaceViewContainer.addView(surfaceView);
            return this;
        }

        public Builder setSurfaceHolder(SurfaceHolder surfaceHolder) {
            this.surfaceHolder = surfaceHolder;
            return this;
        }

    }

    public interface OnVideoRenderingStartListener {
        void onVideoRenderingStart();
    }


}
