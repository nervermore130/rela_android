package com.thel.player.video;

import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;

import com.kingsoft.media.httpcache.KSYProxyService;
import com.ksyun.media.player.IMediaPlayer;
import com.ksyun.media.player.KSYMediaPlayer;
import com.thel.player.KSYFloatingPlayer;
import com.thel.utils.GrowingIOUtil;
import com.thel.utils.L;
import com.thel.utils.VideoUtils;

import java.io.IOException;

import video.com.relavideolibrary.RelaVideoSDK;

/**
 * @author liuyun
 * @date 2017/10/16
 */

public class VideoPlayerProxy3 implements VideoPlayerCallback {


    private String url = null;

    private String momentId;

    private String ksyUrl = null;

    private KSYMediaPlayer mKSYMediaPlayer;

    private VideoInfoBuilder builder;

    private VideoStatus mVideoStatus = VideoStatus.NONE;

    private KSYProxyService ksyProxyService;

    private VideoInfoBuilder.OnVideoRenderingStartListener mOnVideoRenderingStartListener;

    private static final VideoPlayerProxy3 ourInstance = new VideoPlayerProxy3();
    private SurfaceView mSurfaceView;
    private ViewGroup surfaceViewContainer;

    public static VideoPlayerProxy3 getInstance() {
        return ourInstance;
    }

    private VideoPlayerProxy3() {

        ksyProxyService = RelaVideoSDK.getKSYProxy();

        ksyProxyService.startServer();
    }

    @Override
    public void play(VideoInfoBuilder builder) {

        mVideoStatus = VideoStatus.PLAY;

        this.builder = builder;

        this.momentId = builder.momentId;

        this.mSurfaceView = builder.surfaceView;

        this.surfaceViewContainer = builder.surfaceViewContainer;

        this.url = builder.url;

        this.mOnVideoRenderingStartListener = builder.onVideoRenderingStartListener;

        initPlayer();

        mKSYMediaPlayer.setDisplay(null);

        mKSYMediaPlayer.softReset();

        ksyUrl = ksyProxyService.getProxyUrl(builder.url);

        if (builder.surfaceHolder != null) {
            startToPlay(ksyUrl, builder.surfaceHolder);
        } else {
            if (mSurfaceView.getHolder().getSurface().isValid()) {
                startToPlay(ksyUrl, mSurfaceView.getHolder());
            } else {
                mSurfaceView.getHolder().addCallback(mSurfaceViewCallback);
            }
        }

    }

    private void initPlayer() {

        mKSYMediaPlayer = KSYFloatingPlayer.getInstance().getKSYMediaPlayer();

        if (mKSYMediaPlayer == null) {

            mKSYMediaPlayer = KSYFloatingPlayer.getInstance().init(mSurfaceView.getContext());

        }
    }

    @Override
    public void stopPlay() {

        L.d("Fragment", " stopPlay : ");

        if (mKSYMediaPlayer != null && mVideoStatus == VideoStatus.PLAY) {
            mKSYMediaPlayer.reset();
            mKSYMediaPlayer.stop();
            mKSYMediaPlayer = null;
        }

        url = null;

        ksyUrl = null;

        mVideoStatus = VideoStatus.NONE;

    }

    @Override
    public void resume() {

        mVideoStatus = VideoStatus.PLAY;

        if (mKSYMediaPlayer != null) {
            mKSYMediaPlayer.start();
        }
    }

    @Override
    public void pause() {
        L.d("Fragment", " pause : ");
        mVideoStatus = VideoStatus.PAUSE;

        if (mKSYMediaPlayer != null) {
            mKSYMediaPlayer.pause();
        }
    }

    @Override
    public void reset() {
        mVideoStatus = VideoStatus.PAUSE;
        L.d("Fragment", " reset : ");
        if (mKSYMediaPlayer != null) {
            mKSYMediaPlayer.stop();
            mKSYMediaPlayer.reset();
        }
    }

    @Override
    public String getUrl() {
        return url == null ? "" : url;
    }

    @Override
    public boolean isPlaying() {
        return mVideoStatus == VideoStatus.PLAY;
    }

    @Override
    public VideoInfoBuilder getBuilder() {
        return builder;
    }


    private void startToPlay(String url, SurfaceHolder mholder) {

        L.d("Fragment", " startToPlay : ");

        initPlayer();

        if (builder != null && builder.onPreparedListener != null) {
            mKSYMediaPlayer.setOnPreparedListener(builder.onPreparedListener);
        } else {
            mKSYMediaPlayer.setOnPreparedListener(mOnPreparedListener);
        }

        if (builder != null && builder.onErrorListener != null) {
            mKSYMediaPlayer.setOnErrorListener(builder.onErrorListener);

        } else {
            mKSYMediaPlayer.setOnErrorListener(mOnErrorListener);
        }

        if (builder != null && builder.onCompletionListener != null) {
            mKSYMediaPlayer.setOnCompletionListener(builder.onCompletionListener);

        } else {
            mKSYMediaPlayer.setOnCompletionListener(mOnCompletionListener);

        }

        if (builder != null && builder.onInfoListener != null) {
            mKSYMediaPlayer.setOnInfoListener(builder.onInfoListener);

        } else {
            mKSYMediaPlayer.setOnInfoListener(mOnInfoListener);

        }

        mKSYMediaPlayer.setBufferTimeMax(2);

        mKSYMediaPlayer.setBufferSize(15);

        mKSYMediaPlayer.setDecodeMode(KSYMediaPlayer.KSYDecodeMode.KSY_DECODE_MODE_SOFTWARE);

        try {
            mKSYMediaPlayer.setDataSource(url);
            mKSYMediaPlayer.setDisplay(mholder);
            mKSYMediaPlayer.prepareAsync();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private IMediaPlayer.OnPreparedListener mOnPreparedListener = new IMediaPlayer.OnPreparedListener() {
        @Override
        public void onPrepared(IMediaPlayer iMediaPlayer) {

            startTime = System.currentTimeMillis();

            float volume = 0;

            if (builder != null) {
                volume = builder.volume;
            }

            if (mKSYMediaPlayer != null) {

                mKSYMediaPlayer.setVolume(volume, volume);

                mKSYMediaPlayer.setVideoScalingMode(KSYMediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);

                mKSYMediaPlayer.start();

            }

            VideoUtils.uploadVideoCount(momentId);

        }
    };

    private IMediaPlayer.OnInfoListener mOnInfoListener = new IMediaPlayer.OnInfoListener() {
        @Override
        public boolean onInfo(IMediaPlayer iMediaPlayer, int i, int i1) {

            switch (i) {

                case KSYMediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START:
                    if (mOnVideoRenderingStartListener != null) {
                        mOnVideoRenderingStartListener.onVideoRenderingStart();
                    }
                    break;

                default:
                    break;
            }


            return false;
        }
    };

    private IMediaPlayer.OnCompletionListener mOnCompletionListener = new IMediaPlayer.OnCompletionListener() {

        @Override
        public void onCompletion(IMediaPlayer iMediaPlayer) {

            if (mKSYMediaPlayer != null) {
                playCount++;
                mKSYMediaPlayer.seekTo(0);
                mKSYMediaPlayer.start();
            }

        }
    };

    private IMediaPlayer.OnErrorListener mOnErrorListener = new IMediaPlayer.OnErrorListener() {
        @Override
        public boolean onError(IMediaPlayer iMediaPlayer, int i, int i1) {
            uploadVideoInfo();
            return false;
        }
    };

    private int playCount = 1;

    private long playTime = 0;

    private long startTime = 0;

    private long endTime = 0;

    private void uploadVideoInfo() {

        if (builder != null) {

            endTime = System.currentTimeMillis();

            if (startTime > 0 && endTime - startTime > 0) {
                playTime = (endTime - startTime) / 1000;
            } else {
                playTime = 1;
            }

            GrowingIOUtil.uploadCommonVideoDuration(playTime);

            VideoUtils.postPlayVideoInfo(builder.entry, builder.momentId, playCount, playTime);

        }
    }


    public SurfaceHolder.Callback mSurfaceViewCallback = new SurfaceHolder.Callback() {
        public SurfaceHolder holder;

        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            this.holder = holder;
            startToPlay(ksyUrl, holder);
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            if (mSurfaceView != null && mSurfaceView.getHolder() == holder) {
                releaseSurfaceHolder();
            }
        }
    };

    private void releaseSurfaceHolder() {
        if (this.mSurfaceView != null) {
            this.mSurfaceView.setVisibility(View.GONE);
        }
        if (mKSYMediaPlayer != null) {
            mKSYMediaPlayer.setDisplay(null);
            VideoPlayerProxy3.getInstance().stopPlay();
        }
        uploadVideoInfo();
    }

    public enum VideoStatus {
        NONE, PLAY, PAUSE
    }


}
