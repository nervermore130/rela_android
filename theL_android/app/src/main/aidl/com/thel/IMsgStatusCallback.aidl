// IMsgStatusCallback.aidl
package com.thel;

import com.thel.modules.main.messages.bean.MsgBean;
import com.thel.db.table.message.MsgTable;

// Declare any non-default types here with import statements

interface IMsgStatusCallback {

   //消息的连接状态
   void onConnectStatus(int status);

   //消息发送状态
   void onMsgSendStatus(in MsgBean msgBean);

   //新消息
   void onNewMsgComing(in MsgBean msgList);

   //新消息
   void onUpdateMsg(in MsgTable msgTable);

   //删除
   void onDeleteMsgTable(String userId);

   //初始化聊天成功
   void onChatInfo();

   //数据迁移百分比
   void onTransferPercents(int percents);

}
