// IMainProcessCallback.aidl
package com.thel;

import com.thel.modules.main.messages.bean.MsgBean;

// Declare any non-default types here with import statements

interface IMainProcessCallback {

   //检测好友关系
   void checkFriendsRelationship(String userId, in MsgBean msgBean);

   //友盟上报的回调
   void umengPushStatus(int status, String msg,String ip);

   //聊天服务被系统杀了
   void serviceKilledBySystem();

}
