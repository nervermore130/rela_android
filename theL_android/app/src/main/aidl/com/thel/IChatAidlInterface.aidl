// IChatAidlInterface.aidl
package com.thel;

import com.thel.IMsgStatusCallback;
import com.thel.modules.main.messages.bean.MsgBean;
import com.thel.db.table.message.MsgTable;
import com.thel.db.table.message.WinkMsgTable;
import com.thel.db.table.message.UserInfoTable;
import com.thel.IMainProcessCallback;

// Declare any non-default types here with import statements

interface IChatAidlInterface {

    //================ chat begin ===============

    //初始化聊天连接
    void init();

    //聊天连接的状态
    int getConnectStatus();

    //设置聊天状态
    void setConnectStatus(int status);

    //是否已连接
    boolean isConnected();

    //重连
    void reconnect();

    //断开连接
    void disconnect();

    //验证
    void auth();

    //发送消息
    void sendMsg(in MsgBean msgBean);

    //发送回执消息
    void sendReceiptMsg(in MsgBean msgBean);

    //发送正在聊天的消息
    void sendPendingMsg(String toUserId, String content);

    //注册连接状态的回调
    void registerCallBack(String key,IMsgStatusCallback cb);

    //取消连接状态的回调
    void unregisterCallBack(String key);

    //推送连接状态
    void pushStatus(int status);

    //推送消息状态改变的消息
    void pushMsgSendStatus(in MsgBean msgBean);

    //推送新消息
    void pushNewMsgComing(in MsgBean msgBean);

    //推送MessageFragment更新数据
    void pushUpdateMsg(in MsgTable msgTable);

    //推送删除MsgTable的消息
    void pushDeleteMsgTable(in String userId);

    //清理所有连接回调
    void clearCallback();

    //获取当前好友列表的userId
    List<String> getUserList();

    //设置好友列表
    void setUserList(in List<String> userList);

    //设置当前聊天界面的userId用于区分界面 当userId为null时,不在ChatActivity,
    //当userId于当前ChatActivity的toUserId相等时为当前正在聊天的界面，否则为不在聊天的界面
    void setUserId(String userId);

    //获取当前的userId
    String getUserId();

    //设置我的ID
    void setMyUserId(String userId);

    //获取我的ID
    String getMyUserId();

    //设置登录后shkey
    void setKey(String key);

    //获取
    String getKey();

    //注册聊天进程调用主进程的http请求接口
    void registerMainProcessCallback(IMainProcessCallback impcb);

    //取消聊天进程调用主进程的http请求接口
    void unregisterMainProcessCallback();

    //检测好友关系
    void checkFriendsRelationship(String userId, in MsgBean msgBean);

    //初始化聊天成功
    void onChatInfo();

    //数据迁移百分比
    void onTransferPercents(int percents);

    //================ chat end ===============

    //================ db begin ===============

     List<MsgTable> getStrangerMsg();

     void insertNewMsg(in MsgBean msgBean, String tableName, int hasRead);

     void insertChatTable(in MsgBean msgBean, String tableName);

     void insertMsgTable(in MsgTable msgTable);

     MsgTable getMsgTableByUserId(String userId);

     List<MsgBean> findMsgsByUserIdAndPage(String toUserId, String limit, String offset);

     List<MsgBean> getMsgListByTableName(String tableName);

     List<MsgBean> getMsgListByTableNameAsync(String tableName);

     MsgBean getMsgByUserIdAndPacketId(String userId, String packetId);

     void updateMsgTableWithWink(in MsgTable msgTable);

     void updateMsgUnread(in MsgTable msgTable);

     long deleteMsgById(String tableName, String packetId);

     MsgBean getLastMsg(String tableName);

     MsgTable findMsgTableByOtherUserId(String otherUserId);

     List<MsgTable> getMsgList(int isStranger);

     MsgTable insertRequestDataToMsgTable(in MsgBean msgBean, int unReadCount);

     int findRequestCountToMsgTable();

     void changeRelationship( String userId, in MsgBean msgBean, int isStranger);

     void deleteStranger();

     boolean isStranger(String userId);

     long getLastMsgTime(String userId);

     long stickTop(in MsgTable msgTable);

     long resetStickTop(in MsgTable msgTable);

     long getStickTopCount();

     void removeMsgTable(String userId);

     void updateMsgTable(in MsgBean msgBean);

     void deleteMsgTable(String userId);

     List<MsgTable> getAllMsgTables();

     void insertUserInfo(in UserInfoTable userInfoTable);

     UserInfoTable getUserInfo();

     String getMsgCursor();

     void setMsgCursor(String cursor);

     void updateStrangerUnreadCount(int count, int isWinked);

     void winkMsgSaveToDB(in MsgBean msgBean);

     long getStrangerWinkOrUnWinkCount(int isWinked);

     void insertMsg(in MsgBean msgBean, String tableName);

     void createTable(String tableName);

     void attachToOldDB(String path, String name);

     void detachToOldDB(String name);

     void tableTransfer(String newDBName, String newTableName, String oldDBName, String oldTableName);

     void dropTable(String tableName);

     List<WinkMsgTable> getWinkList(int limit, int offset);

     void deleteWinkMsg(in WinkMsgTable winkMsgTable);

     void updateWinkMsg(in WinkMsgTable winkMsgTable);

     void insertWinkMsg(in WinkMsgTable winkMsgTable);

     void clearWinksTable();

     long getWinkMsgCount();

     boolean isMsgCreated(in MsgBean msgBean);

     int getUnreadMsgCount();

    //================ db  end  ===============

}
