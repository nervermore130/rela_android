#include <jni.h>
#include <string>
#include <map>


char* reverse(const unsigned char xs[], size_t length) {
    char *result = (char *)malloc(length);
    for (size_t i = 0; i < length; i++) {
        char x = xs[i] ^ (unsigned char)(0xFF);
        result[i] = x;
    }
    return result;
}

const unsigned char wechatAppIdForLogin[19] = {136, 135, 204, 156, 206, 155, 199, 153, 157, 206, 154, 199, 206, 158, 157, 203, 198, 156, 255};
const unsigned char wechatSecretForLogin[33] = {153, 204, 204, 207, 204, 203, 155, 157, 157, 199, 207, 206, 206, 202, 205, 204, 202, 200, 206, 201, 201, 157, 158, 155, 205, 199, 156, 198, 154, 203, 206, 157, 255};
const unsigned char wechatAppIdForGlobal[19] = {136, 135, 199, 203, 205, 157, 207, 198, 202, 205, 157, 203, 206, 205, 158, 157, 207, 157, 255};
const unsigned char wechatSecretForGlobal[33] = {153, 202, 202, 199, 202, 204, 203, 199, 158, 201, 156, 205, 203, 202, 153, 201, 202, 204, 201, 206, 207, 155, 200, 199, 153, 205, 156, 207, 157, 199, 199, 207, 255};
const unsigned char wechatAppIdForPay[19] = {136, 135, 154, 207, 204, 198, 154, 202, 158, 156, 207, 207, 206, 207, 207, 154, 203, 199, 255};
const unsigned char wechatApiKeyForPay[33] = {185, 188, 207, 158, 205, 152, 181, 149, 188, 182, 145, 172, 203, 207, 180, 173, 152, 183, 165, 199, 181, 168, 148, 183, 166, 157, 139, 186, 149, 173, 174, 141, 255};
const unsigned char udeskAppId[17] = {157, 201, 206, 155, 207, 202, 155, 198, 157, 154, 154, 200, 202, 199, 202, 157, 255};
const unsigned char udeskAppKey[33] = {207, 156, 199, 153, 156, 202, 198, 157, 198, 153, 206, 153, 205, 203, 206, 198, 203, 158, 200, 207, 155, 203, 154, 199, 155, 157, 155, 153, 199, 158, 205, 155, 255};
const unsigned char qqAppId[11] = {206, 206, 207, 206, 201, 205, 199, 201, 201, 199, 255};
const unsigned char qqAppKey[17] = {183, 186, 134, 133, 140, 204, 149, 135, 166, 173, 170, 198, 181, 166, 200, 138, 255};
const unsigned char tingyunKey[33] = {203, 156, 207, 199, 201, 206, 203, 158, 203, 155, 198, 158, 203, 204, 198, 200, 198, 206, 204, 153, 158, 201, 206, 199, 156, 157, 156, 156, 207, 158, 153, 199, 255};
const unsigned char tingyunKeyForGlobal[33] = {154, 205, 205, 207, 157, 202, 204, 157, 156, 207, 200, 199, 203, 198, 198, 155, 158, 153, 203, 202, 155, 202, 201, 157, 200, 155, 205, 202, 202, 207, 204, 155, 255};
const unsigned char sinaAppKey[11] = {205, 207, 207, 202, 203, 204, 206, 205, 205, 199, 255};
const unsigned char sinaAppSecret[33] = {154, 205, 156, 155, 199, 157, 156, 207, 154, 154, 156, 206, 202, 154, 205, 157, 200, 205, 157, 207, 206, 207, 206, 205, 205, 206, 207, 200, 200, 204, 204, 200, 255};
const unsigned char agroaAppId[33] = {158, 205, 203, 202, 157, 206, 154, 206, 202, 199, 158, 199, 203, 201, 199, 156, 199, 201, 207, 205, 200, 154, 203, 157, 206, 202, 201, 199, 156, 205, 202, 155, 255};
const unsigned char ksKmcToken[33] = {154, 207, 199, 154, 155, 157, 154, 200, 157, 203, 200, 154, 205, 158, 158, 155, 157, 207, 205, 154, 156, 154, 156, 204, 157, 204, 206, 199, 201, 206, 153, 202, 255};
const unsigned char salt[33] = {199, 200, 198, 153, 204, 207, 156, 203, 157, 206, 201, 203, 206, 206, 203, 205, 156, 201, 206, 198, 205, 158, 156, 156, 205, 204, 156, 153, 157, 200, 204, 204, 255};


std::map<std::string, std::string> info() {


    std::map<std::string, std::string> m;

    m.insert(std::pair<std::string, std::string>("WX_APP_ID", reverse(wechatAppIdForLogin, sizeof(salt))));
    m.insert(std::pair<std::string, std::string>("WX_SECRET", reverse(wechatSecretForLogin, sizeof(salt))));
    m.insert(std::pair<std::string, std::string>("WX_APP_ID_GLOBAL", reverse(wechatAppIdForGlobal, sizeof(salt))));
    m.insert(std::pair<std::string, std::string>("WX_SECRET_GLOBAL", reverse(wechatSecretForGlobal, sizeof(salt))));
    m.insert(std::pair<std::string, std::string>("WX_APP_ID_PAY", reverse(wechatAppIdForPay, sizeof(salt))));
    m.insert(std::pair<std::string, std::string>("WX_API_KEY", reverse(wechatApiKeyForPay, sizeof(salt))));
    m.insert(std::pair<std::string, std::string>("UDESK_APP_ID", reverse(udeskAppId, sizeof(salt))));
    m.insert(std::pair<std::string, std::string>("UDESK_APP_KEY", reverse(udeskAppKey, sizeof(salt))));
    m.insert(std::pair<std::string, std::string>("QQ_APP_ID", reverse(qqAppId, sizeof(salt))));
    m.insert(std::pair<std::string, std::string>("QQ_APP_KEY", reverse(qqAppKey, sizeof(salt))));
    m.insert(std::pair<std::string, std::string>("TINGYUN_KEY", reverse(tingyunKey, sizeof(salt))));
    m.insert(std::pair<std::string, std::string>("TINGYUN_KEY_GLOBAL", reverse(tingyunKeyForGlobal, sizeof(salt))));
    m.insert(std::pair<std::string, std::string>("SINA_APP_KEY", reverse(sinaAppKey, sizeof(salt))));
    m.insert(std::pair<std::string, std::string>("SINA_APP_SECRET", reverse(sinaAppSecret, sizeof(salt))));
    m.insert(std::pair<std::string, std::string>("AGORA_APP_ID", reverse(agroaAppId, sizeof(salt))));
    m.insert(std::pair<std::string, std::string>("KS_KMC_TOKEN", reverse(ksKmcToken, sizeof(salt))));
    m.insert(std::pair<std::string, std::string>("REQUEST_SALT", reverse(salt, sizeof(salt))));

    return m;
}



extern "C" JNIEXPORT jobject JNICALL
Java_com_thel_constants_Configurations_configurationMap(
        JNIEnv* env,
        jobject /* this */) {

    auto configurationMap = info();

    jclass hashMapClass= env->FindClass("java/util/HashMap");
    jmethodID hashMapInit = env->GetMethodID(hashMapClass, "<init>", "(I)V");
    jobject hashMapObj = env->NewObject(hashMapClass, hashMapInit, configurationMap.size());
    jmethodID hashMapPut = env->GetMethodID(hashMapClass, "put", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;");

    for (auto it : configurationMap) {
        env->CallObjectMethod(hashMapObj, hashMapPut,
                              env->NewStringUTF(it.first.c_str()),
                              env->NewStringUTF(it.second.c_str()));
    }

    return hashMapObj;

}
