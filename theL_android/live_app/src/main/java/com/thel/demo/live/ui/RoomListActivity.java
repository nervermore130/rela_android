package com.thel.demo.live.ui;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.thel.demo.live.R;
import com.thel.demo.live.network.DefaultRequestService;
import com.thel.demo.live.network.beans.LiveRoomBean;
import com.thel.demo.live.network.beans.LiveRoomsNetBean;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

@SuppressLint("Registered")
public class RoomListActivity extends AppCompatActivity {

    private final String TAG = this.getClass().getSimpleName();
    private TextView emptyView;
    private TextView errorView;
    private RecyclerView recyclerView;
    private RoomListAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_roomlist);

        emptyView = findViewById(R.id.empty_view);
        errorView = findViewById(R.id.error_view);

        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));

        getHotRoomListData(0, "hot");
    }


    private void getHotRoomListData(final int cursor, final String type) {
        final Flowable<LiveRoomsNetBean> flowable = DefaultRequestService.createLiveRequestService().getLiveRoomsListBean(cursor + "", type);
        flowable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<LiveRoomsNetBean>() {
            @Override
            public void accept(LiveRoomsNetBean liveRoomsNetBean) throws Exception {
                Log.d(TAG, "getHotRoomListData accept:" + liveRoomsNetBean);
                List<LiveRoomBean> list = liveRoomsNetBean.data.list;
                if (list != null && list.size() > 0) {
                    adapter = new RoomListAdapter(list);
                    recyclerView.setAdapter(adapter);
                } else {
                    emptyView.setVisibility(View.VISIBLE);
                }
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                throwable.printStackTrace();
                Log.d(TAG, "getHotRoomListData error:" + throwable);
                errorView.setVisibility(View.VISIBLE);
            }
        });
    }


}
