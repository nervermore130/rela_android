package com.thel.demo.live.ui;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;

import com.tbruyelle.rxpermissions2.RxPermissions;
import com.thel.demo.live.R;
import com.thel.demo.live.util.DemoSpUtils;

import io.reactivex.functions.Consumer;

public class TestLiveEntryActivity extends AppCompatActivity {


    private TextView keyTextView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_live);
        keyTextView = findViewById(R.id.current_user_key);
        keyTextView.setText("当前key:\n" + DemoSpUtils.getString(DemoSpUtils.DemoKey.USER_KEY, ""));
        findViewById(R.id.user_token).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showKeyDialog();
            }
        });

        findViewById(R.id.room_list).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TestLiveEntryActivity.this, RoomListActivity.class));
            }
        });

        findViewById(R.id.show_live).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        requestPermissions();
    }

    private void showKeyDialog() {
        final View view = LayoutInflater.from(this).inflate(R.layout.dialog_user_key, null);
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("请输入用户key");
//        alertDialog.setIcon("Icon id here");
        alertDialog.setCancelable(false);
//        Constant.alertDialog.setMessage("Your Message Here");
        final EditText editText = view.findViewById(R.id.etComments);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String key = editText.getText().toString().trim();
                keyTextView.setText("当前key:" + key);
                DemoSpUtils.setString(DemoSpUtils.DemoKey.USER_KEY, key);
                dialog.dismiss();
            }
        });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialog.setView(view);
        alertDialog.show();
    }

    private void requestPermissions() {
        String[] permissions = new String[]{Manifest.permission.INTERNET};
        new RxPermissions(this).request(permissions).subscribe(new Consumer<Boolean>() {
            @Override
            public void accept(Boolean aBoolean) throws Exception {

            }
        });
    }


}
