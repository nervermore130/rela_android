package com.thel.demo.live.network.beans;

import java.io.Serializable;

/**
 * Created by the L on 2016/5/25.
 * 礼物，用软妹币可以够买的用来打赏的礼物
 */
public class SoftGiftBean implements Serializable {
    /**
     * 礼物id
     */
    public int id;
    /**
     * 礼物名称
     */
    public String title;
    /**
     * 礼物图片
     */
    public String icon;
    /**
     * 特效图
     */
    public String img;
    /**
     * 是否最新
     */
    public int isNew;
    /**
     * Gold虚拟币，价格
     */
    public int gold;
    /**
     * 动作描述，赠送时候用
     */
    public String action;
    /**
     * 是否热门
     */
    public int isHot;
    /**
     * 是否是大礼物标识（播放大动画）
     */
    public String resource;

    /**
     * 是否可以连击 1：可以连击，2：不可以连击 （通常，大礼物不可以连击，超过20软妹豆的不可以连击）
     */
    public int canCombo;
    public int playTime;
    /**
     * 0普通礼物,1AR礼物
     */
    public int isArGift;
    /**
     * AR礼物Id
     */
    public int arGiftId;

    public String arResource;

    public int rank;

    public String updateTime;//更新时间

    public String createTime;//创建时间
    //大礼物视频地址
    public String videoUrl;

    public int hearts;

    public int effectsThreshold = 0;

    //是否是快捷礼物
    public int canFasterGift = 0;

    /**
     * 是否等级特权礼物
     */
    public int isPrivilegeGift = 0;
    /**
     * 等级特权礼物级别
     */
    public int privilegeGrade = 0;
    /**
     * 是否是会员礼物
     */
    public int isMemberGift = 0;

    @Override
    public boolean equals(Object obj) {
        if (obj != null && obj instanceof SoftGiftBean) {
            return ((SoftGiftBean) obj).id == id;
        }
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return "SoftGiftBean{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", icon='" + icon + '\'' +
                ", img='" + img + '\'' +
                ", isNew=" + isNew +
                ", gold=" + gold +
                ", action='" + action + '\'' +
                ", isHot=" + isHot +
                ", resource='" + resource + '\'' +
                ", canCombo=" + canCombo +
                ", playTime=" + playTime +
                ", rank=" + rank +
                ", updateTime='" + updateTime + '\'' +
                ", createTime='" + createTime + '\'' +
                ", videoUrl='" + videoUrl + '\'' +
                ", hearts=" + hearts +
                '}';
    }
}
