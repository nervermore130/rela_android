package com.thel.demo.live.util;

import com.thel.demo.live.LiveTheLApp;

public class BusinessUtils {

    public static String generateUA() {
        return "theL/" +
                DeviceUtils.getVersionName(LiveTheLApp.getContext()) +
                " (" +
                DeviceUtils.getModel() +
                "; android " +
                DeviceUtils.getReleaseVersion() +
                "; " +
                "zh_CN" +
                "; " +
                DeviceUtils.getCurrentNetType(LiveTheLApp.getContext()) +
                "; " +
                "test" + ")";
    }

}
