package com.thel.demo.live.network;

/**
 * 请求接口的常量
 *
 * @author zhangwenjia
 */
public class RequestConstants {

    /*------------------------------------------ 接口服务器接口地址 -------------------------------------**/
    /**
     * 接口地址 (正式环境) *
     */
    // public static String FILE_BUCKET = "http://static.thel.co/"; 4.0.1 更换图片域名
    public static String FILE_BUCKET = "http://static.rela.me/";

    // public static String VIDEO_BUCKET = "http://video.thel.co/"; 4.0.1 更换视频域名
    public static String VIDEO_BUCKET = "http://video.rela.me/";
    /**
     * 图片接口地址 (正式环境)
     */
    public static String IMAGE_SERVER_IP = "http://google-location.rela.me:8080";

    // 消息服务器地址和端口
    public static int msgServerPort = 3456;

    /**
     * 国内版和国际版applicationId
     */
    public static final String APPLICATION_ID_LOCAL = "com.thel";
    public static final String APPLICATION_ID_GLOBAL = "com.thel.global";
    /**
     * 接口地址
     */
    public static String HTTP_DNS_SERVICE = "http://119.29.29.29/d?ttl=1&dn=";

    // 网络类型
    public static int NET_TYPE = 0;

    public static String QINIU_BUCKET_VIDEO = "thel-videos";

    public static String UPLOAD_FILE_ROOT_PATH_ALBUM = "app/album/";
    public static String UPLOAD_FILE_ROOT_PATH_ALBUM_GIF = "app/album-gif/";
    public static String UPLOAD_FILE_ROOT_PATH_AVATAR = "app/avatar/";
    public static String UPLOAD_FILE_ROOT_PATH_TIMELINE = "app/timeline/";
    public static String UPLOAD_FILE_ROOT_PATH_REPORT = "app/report/";
    public static String UPLOAD_FILE_ROOT_PATH_CHAT = "app/message/";
    public static String UPLOAD_FILE_ROOT_PATH_VOICE = "app/voice/";
    public static String UPLOAD_FILE_ROOT_PATH_BG_IMG = "app/bgimg/";
    public static String UPLOAD_FILE_ROOT_PATH_VIDEO = "app/video/";
    public static String UPLOAD_FILE_ROOT_PATH_LOG = "rela-client-log/";

    /*--------------------------------------------接口地址----------------------------------------------**/
    /**
     * 登录接口地址 *
     */
    public static final String SIGN_IN = "/v1/auth/signin";
    /**
     * 获取一些基本的必要信息接口，如'我'是否是会员、我的直播是否异常中断需要重新开启一个直播，2.18.0之前是firstNeed接口，2.18.0之后用initparams接口
     */
    public static final String GET_BASIC_INFO = "/v2/params/init";
    /**
     * 获取主播权限
     */
    public static final String GET_LIVE_PERMIT = "/v1/live/perm";
    /**
     * 上传设备id，主要是个推的getid
     */
    public static final String UPLOAD_DEVICE_TOKEN = "/v2/push/device/upload";
    /**
     * 显示欢迎消息
     */
    public static final String SHOW_WELCOME_MSG = "/v1/msg/request-welcome";
    /**
     * 通过服务器获取短信验证码 *
     */
    public static final String SEND_CODE = "/v1/sms/sendcode";
    /**
     * 获取上传文件到七牛的token *
     */
    public static final String GET_UPLOAD_TOKEN = "/v1/qiniu/upload-token";
    /**
     * 检查手机是否已经被注册地址 *
     */
    public static final String CHECK_PHONE_NUMBER = "/v1/auth/check-cell";
    /**
     * 获取用户的黑名单信息：我屏蔽的人、屏蔽我的人、屏蔽其日志的人
     */
    // public static final String BLACK_LIST = "/v1/blacklist";
    public static final String BLACK_LIST = "/v2/users/blacklist";
    /**
     * 验证用户名是否重复地址 *
     */
    public static final String CHECK_USERNAME = "/v1/auth/check-username";
    /**
     * 账号绑定 *
     */
    public static final String REBIND = "/v1/auth/rebind";
    /**
     * 找回密码接口地址 *
     */
    public static final String FIND_BACK_PASSWORD = "/interface/stay/appSystem!getBackPassword.do";
    /**
     * 注销账户接口地址 *
     */
    public static final String USER_LOGOUT = "/interface/stay/appUser!logout.do";
    /**
     * 读取某个点附近的用户(需登录) *
     */
    public static final String NEAR_BY_LIST = "/interface/stay/appUser!nearbyList.do";
    /**
     * 获取附近用户 *
     */
    public static final String NEAR_BY_SIMPLE_LIST = "/interface/stay/appUser!nearbySimpleList.do";
    /**
     * 获取活跃用户 *
     */
    public static final String GET_ACTIVE_USERS = "/v2/users/active/list";
    /**
     * 复合筛选 *
     */
    public static final String FILTER_USER = "/interface/stay/appUserFilter!filterUser.do";
    /**
     * 世界 *
     */
    public static final String WORLD_LIST = "/interface/stay/appUser!worldList.do";
    /**
     * 获取地址名 *
     */
    public static final String GET_LOC_NAME = "/image/loc";
    /**
     * 首页用户列表(不用登录)接口地址 *
     */
    public static final String GET_NEAR_USER_LIST = "/interface/stay/appUser!getNearUserList.do";
    /**
     * 读取(其他)用户个人信息 *
     */
    public static final String USER_INFO = "/interface/stay/appUser!newUserInfoForFriend.do";
    /**
     * 暗恋 *
     */
    public static final String SEND_HIDDEN_LOVE = "/interface/stay/appUser!sendHiddenLove.do";
    /**
     * 屏蔽用户 *
     */
    public static final String PULL_USER_INTO_BLACKLIST = "/interface/stay/appUser!pullUserIntoBlackList.do";
    /**
     * 传情 *
     */
    public static final String SEND_WINK = "/v1/wink/send";
    /**
     * 关注 *
     */
    public static final String FOLLOW_USER = "/interface/stay/appUserFollower!followUser.do";
    /**
     * 批量关注用户 *
     */
    public static final String FOLLOW_USER_BATCH = "/interface/stay/appUserFollower!followManyUser.do";
    /**
     * 获取朋友列表（简易） *
     */
    public static final String GET__CONTACTS = "/interface/stay/appUserFollower!getAtFriendUser.do";
    /**
     * 获取朋友列表 *
     */
    public static final String GET__FRIENDS = "/interface/stay/appUserFollower!getNewFriendUser.do";
    /**
     * 获取关注列表 *
     */
    public static final String GET__FOLLOW = "/interface/stay/appUserFollower!getNewFollowUser.do";

    /**
     * 获取关注的主播列表
     */
    public static final String GET_FOLLOW_ANCHORS = "/v2/users/follow/anchors";
    /**
     * 获取粉丝列表 *
     */
    public static final String GET__FANS = "/interface/stay/appUserFollower!getNewFansUser.do";

    /**
     * 获取悄悄查看列表 *
     */
    public static final String GET_SECRET = "/interface/stay/appUserFollowerSecretly";

    /**
     * 获取期待主播离列表*
     */
    public static final String GET_AWAIT = "/v2/live/await_list";

    /**
     * 获取liked列表 *
     */
    public static final String GET_LIKED = "/interface/stay/match/likeList";
    /**
     * 获取用户收到的like数 *
     */
    public static final String GET_LIKE_ME_COUNT = "/interface/stay/match/likeMeCount";
    /**
     * 获取匹配列表，最近喜欢我的人
     */
    public static final String GET_LIKE_ME_LIST = "/v2/mate/newLikeMeList";
    /**
     * 搜索我的朋友 *
     */
    public static final String SEARCH_MY_FRIENDS = "/interface/stay/search!searchFollowUser.do";
    /**
     * 我的信息 *
     */
    public static final String MY_INFO = "/interface/stay/appUser!myInfoForFriend.do";
    /**
     * 收到传情列表(我的) *
     */
    public static final String GET_MY_WINK_LIST = "/interface/stay/appUser!getUserWinkList.do";
    /**
     * 收到传情列表 (3.0改为人气列表)*
     */
    public static final String GET_USER_WINK_LIST = "/v1/wink/popularity";
    /**
     * 获取用户的粉丝列表 *
     */
    public static final String GET_USER_FOLLOWERS_LIST = "/interface/stay/appUserFollower!getNewFansUser.do";
    /**
     * 我的照片 *
     */
    public static final String MY_IMAGES_LIST = "/interface/stay/appUserImage!myImagesList.do";
    /**
     * 设为隐私照片 *
     */
    public static final String SET_PRIVATE_STATUS = "/interface/stay/appUserImage!setPrivateStatusPlus.do";
    /**
     * 设为封面照片 *
     */
    public static final String SET_COVER = "/interface/stay/appUserImage!setCoverImage.do";
    /**
     * 删除照片 *
     */
    public static final String DELETE_IMAGE = "/interface/stay/appUserImage!deleteImagePlus.do";
    /**
     * 解除绑定 *
     */
    public static final String UNBIND = "/v1/auth/unbind";
    /**
     * 我的黑名单列表 *
     */
    public static final String GET_USER_BLACK_LIST = "/interface/stay/appUser!getUserBlackList.do";
    /**
     * 收回密匙 *
     */
    public static final String TERMINATE_AUTHORIZATION = "/interface/stay/appUserImage!terminateAuthorization.do";
    /**
     * 修改密码 *
     */
    public static final String MODIFY_PASSWORD = "/interface/stay/appUser!modifyPassword.do";
    /**
     * 上传照片 *
     */
    public static final String UPLOAD_IMAGE = "/interface/stay/appUserImage!setUserPhotos.do";
    /**
     * 更新个人信息 *
     */
    public static final String UPDATE_USER_INFO = "/interface/stay/appUser!updateUserInfo.do";
    /**
     * 初始化个人信息 *
     * 4.5.0替换
     */
    //public static final String INIT_USER_INFO = "/interface/stay/appUser!initUserInfo.do";
    public static final String INIT_USER_INFO = "/v2/users/userinfo/init";

    /**
     * 解除黑名单 *
     */
    public static final String REMOVE_FROM_BLACKLIST = "/interface/stay/appUser!removeFromBlackList.do";
    /**
     * 谁来看过我 *
     */
    public static final String CHEACK_VIEW = "/interface/stay/appUserView!cheackView.do";
    /**
     * 我看过谁 *
     */
    public static final String I_VIEW = "/interface/stay/appUserView!iView.do";
    /**
     * thel广告 *
     */
    public static final String GET_MAIN_ADVERT = "/interface/stay/appSystem!getMainAdvert.do";
    /**
     * message push interface on Clie/v2/apps/android/updatent *
     */
    public static final String APP_PUSH = "/interface/stay/appPush!push.do";
    /**
     * 上传语音留言 *
     */
    public static final String MODIFY_AAC_VOICE = "/interface/stay/appUser!modifyAACVoice.do";
    /**
     * 获取我的密友圈列表 *
     */
    public static final String GET_MY_CIRCLE_DATA = "/interface/stay/binding/getBindingList";
    /**
     * 移除密友圈朋友 *
     */
    public static final String REMOVE_CIRCLE_FRIEND = "/interface/stay/binding/removeBinding";
    /**
     * 获取我的密友请求列表 *
     */
    public static final String GET_MY_CIRCLE_REQUESTS = "/interface/stay/request/getUserRequest";
    /**
     * 发送一个密友请求 *
     */
    public static final String SEND_CIRCLE_REQUEST = "/interface/stay/request/sendRequest";
    /**
     * 取消我发出去的密友请求 *
     */
    public static final String CANCEL_CIRCLE_REQUEST = "/interface/stay/request/cancelRequest";
    /**
     * 接受请求 *
     */
    public static final String ACCEPT_REQUEST = "/interface/stay/request/agreeRequest";
    /**
     * 拒绝请求 *
     */
    public static final String REFUSE_REQUEST = "/interface/stay/request/rejectRequest";
    /**
     * 推荐的匹配用户列表 *
     */
    public static final String MATCH_LIST = "/interface/stay/match/matchList";
    /**
     * 匹配用户反悔（清除对某人的like状态） *
     */
    public static final String MATCH_ROLLBACK = "/interface/stay/match/rollback";
    /**
     * 获取我的配对问题 *
     */
    public static final String MY_QUESTIONS = "/interface/stay/match/userQuestion";
    /**
     * 设置配对问题 *
     */
    public static final String SET_QUESTIONS = "/interface/stay/match/setQuestion";

    /**
     * 退出匹配 *
     */
    public static final String EXIT_MATCH = "/v2/mate/exitMatch";
    /**
     * 匹配设置
     */
    public static final String MATCH_PROFILE = "/v2/mate/updateMatchProfile";
    /**
     * 获取匹配设置的数据
     */
    public static final String GET_MATCH_PROFILE = "/v2/mate/matchProfile";
    /**
     * 喜欢或不喜欢对方 *
     */
    public static final String LIKE_OR_NOT = "/interface/stay/match/like";
    public static final String SUPERLIKE = "/v2/mate/superlike";
    public static final String SUPERLIKE_LIST = "/v1/superlike/list";
    /**
     * 表情商店首页-广告和推荐表情包接口
     */
    public static final String STICKER_STORE_RECOMMENDED = "/v1/stickerpack/store";
    /**
     * 表情商店首页-更多表情包接口
     */
    public static final String STICKER_STORE_MORE = "/v1/stickerpack/list";
    /**
     * 表情包详情接口
     */
    public static final String STICKER_PACK_DETAIL = "/v1/stickerpack/detail";
    /**
     * 我已购买的表情包接口
     */
    public static final String PURCHAESED_STICKER_PACKS = "/v1/stickerpack/purchased";
    /**
     * 生成一张购买表情包订单，从服务端获取一些需要的字段，再去调第三方支付
     */
    public static final String CREATE_STICKER_PACK_ORDER = "/v1/payment/order/create";
    /**
     * google wallet iap验证
     */
    public static final String IAP_NOTIFY = "/v1/payment/google-pay-notify";
    /**
     * 获取购买会员列表
     */
    public static final String BUY_VIP_LIST = "/v1/vip/list";
    /**
     * 获取会员功能开关信息
     *//*
    public static final String GET_VIP_CONFIG = "/interface/stay/vip/getVipSet";*/
    /**
     * 获取会员功能开关信息
     */
    public static final String GET_VIP_CONFIG = "/v2/users/vip/detail";
    /**
     * 配置vip功能
     * @deprecated
     *//*
    public static final String VIP_CONFIG = "/interface/stay/vip/vipSet";*/
    /**
     * 配置vip功能,4,2.0
     */
    public static final String VIP_CONFIG = "/v2/users/vip/update";
    /**
     * 推送开关
     */
    //public static final String PUSH_CONFIG = "/interface/stay/appUser!setPushType.do";
    public static final String PUSH_CONFIG = "/v2/push/switch/update";
    /**
     * 开关列表
     */
    public static final String PUSH_SWITCH = "/v2/push/switch/list";
    /**
     * 4.17.0小红点开关列表
     */
    public static final String GET_REDPOINT_SWITCH = "/v2/redpoint/list";
    /**
     * 上传视频
     */
    public static final String UPLOAD_ALBUM_VIDEO = "/interface/stay/appUserImage!setNewUserPhotos.do";

    /**
     * 移除粉丝
     */
    public static final String POST_REMOVEFANS = "/v2/users/fans/remove";

    /**
     * 获取芝麻认证的url地址
     */
    public static final String POST_ZMXY_AUTH = "/v1/ali/zmxy/auth-url";

    /**
     * 验证芝麻信用回调数据
     */
    public static final String POST_CHECK_CALLBACK = "/v1/ali/zmxy/check-callback";
    /*---------------------------------------- 朋友圈 -----------------------------------------------------**/
    /**
     * 举报用户 *
     */
    // public static final String REPORT_USER = "/friend/stay/moments/reportUserPlus";
    public static final String REPORT_USER = "/v2/users/report";

    /**
     * bug反馈 *
     */
    public static final String BUG_FEEDBACK = "/friend/stay/assist/feedback";
    /**
     * 搜索好友 *
     */
    public static final String SEARCH_NICKNAME = "/friend/stay/topic/searchNickname";
    /**
     * 上传用户详情的背景图片 *
     */
    public static final String UPLOAD_BG_IMAGE = "/v2/bgimg/update";
    /**
     * 检查moments是否有新的moments或是是否有新消息 *
     */
    public static final String MOMENTS_CHECK = "/friend/stay/moments/notRead";
    /**
     * 读取moments列表(有音乐功能) *
     */
    public static final String MOMENTS_LIST = "/friend/stay/moments/listWithMusicMultiImg";
    /**
     * 读取moment提到的所有人列表 *
     */
    public static final String MOMENTS_MENTIONED_LIST = "/friend/stay/moments/atUserList";
    /**
     * moments的广告 *
     */
    public static final String MOMENTS_ADVS_LIST = "/friend/stay/moments/advList";
    /**
     * 发布一条moment *
     */
    public static final String MOMENTS_RELEASE_MOMENT = "/v3/friend/stay/moments/add";
    /**
     * 删除一条moment *
     */
    public static final String MOMENTS_DELETE_MOMENT = "/friend/stay/moments/delete";
    /**
     * 举报一条moment *
     */
    public static final String MOMENTS_REPORT_MOMENT = "/friend/stay/moments/reportMomentsPlus";
    /**
     * 撤销点赞 *
     */
    public static final String MOMENTS_UNWINK_MOMENT = "/friend/stay/moments/unwink";
    /**
     * 点赞一条moment *
     */
    public static final String MOMENTS_WINK_MOMENT = "/friend/stay/moments/wink";
    /**
     * 获取评论列表 *
     */
    public static final String MOMENTS_GET_COMMENTS = "/friend/stay/moments/simpleCommentList_v2";
    /**
     * 获取消息 *
     */
    public static final String MOMENTS_GET_MSGS = "/friend/stay/moments/activityList_v1";
    /**
     * 获取评论列表(带moment信息) *
     */
    public static final String MOMENTS_GET_MOMENT_INFO = "/friend/stay/moments/commentList_v1";
    /**
     * 获取话题详情
     */
    public static final String MOMENTS_GET_THEME_INFO = "/v2/themes/detail";

    /**
     * 发布一条评论 *
     */
    public static final String MOMENTS_RELEASE_COMMENT = "/friend/stay/moments/addComment_v1";
    /**
     * 删除一条评论 *
     */
    public static final String MOMENTS_DELETE_COMMENT = "/v2/moments/comment/delete";
    /**
     * 举报一条评论 *
     */
    public static final String MOMENTS_REPORT_COMMENT = "/v2/moments/comment/report";
    /**
     * 获取点赞列表 *
     */
    public static final String MOMENTS_GET_LIKES = "v2/moments/like/list";
    /**
     * 回复一条日志（这是日志的回复）
     */
    public static final String REPLY_MOMENT = "/v2/moments/comment/create";
    /**
     * 回复一条评论（这是回复的回复）
     */
    public static final String REPLY_COMMENT = "/v2/moments/commentReply/create";
    /**
     * 获取日志的评论列表
     */
    public static final String GET_COMMENT_LIST = "/v2/moments/comment/list";
    /**
     * 获取日志详情
     */
    public static final String GET_MOMENT_DETAIL = "/v2/moments/detail";
    /**
     * 日志评论的回复列表
     */
    public static final String GET_COMMENT_REPLY_LIST = "/v2/moments/commentReply/list";
    /**
     * 删除一条评论的回复
     */
    public static final String DELETE_COMMENT_REPLY = "/v2/moments/commentReply/delete";
    /**
     * 最近5个参与的话题，加上 15个热门的话题 *
     */
    public static final String MOMENTS_GET_RECENT_AND_HOT_TOPICS = "/friend/stay/topic/recentAndHot";
    /**
     * 模糊检索话题列表 *
     */
    public static final String MOMENTS_SEARCH_TOPICS = "/friend/stay/topic/suggest";
    /**
     * 话题的主页(新日志) *
     */
    public static final String MOMENTS_TOPIC_MAINPAGE = "/friend/stay/topic/topicMainTab_v1";
    /**
     * 话题的主页(热门日志) *
     */
    public static final String MOMENTS_TOPIC_MAINPAGE_HOT = "/friend/stay/topic/topicMainPageHotMultiImg";
    /**
     * 屏蔽某人的日志(加入到屏蔽黑名单) *
     */
    public static final String BLOCK_USER_MOMENTS = "/friend/stay/moments/addToBlackList";
    /**
     * 屏蔽这条日志 *
     */
    public static final String BLOCK_THIS_MOMENT = "/friend/stay/moments/hideMomentsPlus";
    /**
     * 取消屏蔽某人的日志(从屏蔽黑名单中移除) *
     */
    public static final String CANCEL_BLOCK_USER_MOMENTS = "/friend/stay/moments/deleteFromBlackList";
    /**
     * 获取屏蔽其日志的用户列表 *
     */
    public static final String GET_BLOCK_USER_LIST_FOR_MOMENTS = "/friend/stay/moments/blackList";
    /**
     * 获取某个标签分类下的标签列表 *
     */
    public static final String GET_CATEGORY_TAGS = "/friend/stay/topic/cate";
    /**
     * 获取最热门的标签 *
     */
    public static final String GET_FIRST_HOT_TOPIC = "/friend/stay/topic/firstHotTopic";
    /**
     * 获取推荐用户 *
     */
    public static final String GET_RECOMMEND_USERS = "/friend/stay/topic/recommendUser";
    /**
     * 获取搜索模块随机推荐用户
     */
    public static final String GET_RECOMMEND_USERS_RANDOM = "v2/users/recommend/list_random";
    /**
     * 获取所有我关注的标签3.2.0取消最新话题标签2017.6.5
     */
    public static final String GET_HOT_THEMES = "/friend/stay/explore/discover_v22";
    /**
     * 3.2.0最新热门话题列表
     */
    public static final String GET_HOTTHEME_LIST = "/v2/themes/hotTheme/list";
    /**
     * 4.8.0获取最新话题列表
     */
    public static final String GET_NEWTHEMES_LIST = "/v2/themes/new/list";
    /**
     * 获取人气标签
     */
    public static final String GET_TRENDING_TAGS = "/friend/stay/tags/randomHotTags";
    /**
     * 获取附近日志列表
     */
    public static final String NEARBY_MOMENTS = "/friend/stay/explore/nearByMoment_v1";
    /**
     * 视频统计数据上传
     */
    public static final String SUBMIT_VIDEO_REPORT = "/friend/stay/assist/videoPlayReport";
    /**
     * 置顶我的日志
     */
    public static final String STICK_MY_MOMENT = "/friend/stay/moments/setUserBoardTopMoment";
    /**
     * 取消置顶我的日志
     */
    public static final String UNSTICK_MY_MOMENT = "/friend/stay/moments/cancelUserBoardTopMoment";
    /**
     * 将我的日志设为隐私
     */
    public static final String SET_MY_MOMENT_AS_PRIVATE = "/friend/stay/moments/hideMoment";
    /**
     * 公开我的日志
     */
    public static final String SET_MY_MOMENT_AS_PUBLIC = "/friend/stay/moments/cancelHideMoment";
    /**
     * 新注册用户提供一批推荐关注用户
     */
    public static final String GET_INIT_RECOMMEND_USERS = "/friend/stay/moments/recommendUserList/v1/blacklist";
    /**
     * 检查更新
     */
    public static final String CHECK_UPDATE = "/v2/apps/android/update";

    public static final String GET_RECOMMEND_LIST = "/v2/users/recommend/list_v2";
    /**
     * 4.5.0获取用户等级信息
     */
    public static final String GET_USER_LEVEL = "/v1/live/user-level";
    /***
     * 4.7.5修改等级设置
     * */
    public static final String POST_USER_LEVEL_SETTING = "/v1/live/user-level-setting";

    /*-------------------------------------- 直播相关接口 ---------------------------------------------*/
    /**
     * 获取直播列表
     */
    public static final String GET_LIVE_ROOMS = "/v1/live/list";
    /**
     * 获取直播室详情
     */
    public static final String GET_LIVE_ROOM_DETAIL = "/v1/live/detail";
    /**
     * 获取直播室观众用户信息
     */
    public static final String GET_LIVE_ROOM_USER_CARD = "/v2/users/card";
    /**
     * 上传用户头像
     */
    public static final String UPLOAD_AVATAR = "/v2/users/avatar/upload";
    /**
     * 创建直播间
     */
    public static final String CREATE_LIVE = "/v1/live/create";
    /**
     * 结束直播
     */
    public static final String STOP_LIVE = "/v1/live/stop";
    /**
     * 直播间礼物排行榜
     */
    public static final String GET_LIVE_GIFT_RANKING_LIST = "/v1/live/topfans";
    /**
     * 获取当前有多少位我关注的主播正在直播
     */
    public static final String GET_LIVE_META = "/v1/live/meta";
    /**
     * 话题列表
     */
    public static final String GET_THEMES_LIST = "/v2/themes/list";
    /**
     * 检查公告栏是否合法
     */
    public static final String CHECK_ANNOUNEMENT = "/v1/live/check-announcement-available";
    /*-------------------------------------- 直播相关接口 end ---------------------------------------------*/

    /*-------------------------------------------打赏--------------------------------------------*/
    /**
     * 充值列表
     */
    public static final String GET_GOLD_LIST = "/v1/gold/list";
    /**
     * 我的钱包
     */
    public static final String GET_WALLET_DATA = "/v1/wallet";
    /**
     * 礼物列表
     */
    public static final String GET_GIFT_LIST = "/v1/gift/list";
    /**
     * 充值和消费记录
     */
    public static final String GET_RECHARGE_RECORD = "/v1/wallet/gold/history";
    /**
     * 收益或提现记录
     */
    public static final String GET_INCOME_OR_WITHDRAW_RECORD = "/v1/wallet/gem/history";
    /**
     * Gold购买／赠送商品（会员）
     */
    public static final String BUY_WITH_GOLD = "/v1/payment/buy-with-gold";
    /***
     * 悄悄关注
     */
    public static final String SECRETLY_FOLLOW = "/interface/stay/appUserFollower!secretlyFollow.do";
    /**
     * 2.17,关注主播列表
     */
    public static final String GET_FOLLOWING_ROOM_LIST = "/v1/live/following";

    /**
     * 2.17 收藏列表
     */
    public static final String GET_COLLECTION_LIST = "/v2/favorite/list";
    /**
     * 2.17 收藏日志
     */
    public static final String POST_COLLECTION_CREATE = "/v2/favorite/create";
    /**
     * 2.17 删除收藏的某篇日志
     */
    public static final String POST_COLLECTION_DELETE = "/v2/favorite/delete";

    /**
     * 测试日志
     */
    public static final String TEST_LOG = "/v1/clientlog/";
    /**
     * 举报主播/用户
     */
    //  public static final String POST_REPORT_LIVESHOW_USER = "/v1/report/user";
    public static final String POST_REPORT_LIVESHOW_USER = "/v2/users/report";
    /**
     * 举报用户头像或者图片
     */
    public static final String POST_REPORT_USER_ORIMAGE = "/v2/users/picture/report";
    /**
     * 正在关注的直播列表
     */
    @Deprecated
    public static final String GET_FOLLOWING_USERS = "/v1/live/followingUsers";

    /**
     * 正在关注的直播列表
     */
    public static final String GET_FOLLOWING_USERS_V2 = "/v2/mainpage/subscribe";
    /**
     * 用户昵称修改时间验证（30天内只能修改一次昵称）
     */
    public static final String GET_NICKNAME_TIMEVERIFY = "/v2/users/nickname/timeVerify";

    /*--------------------------------------管理员专属功能--------------------------------------------**/
    /**
     * 最新话题
     */
    public static final String LATEST_THEMES = "/friend/stay/explore/discover_forAdminUse";
    /**
     * 推荐日志
     */
    public static final String RECOMMEND_MOMENT = "/friend/stay/assist/admin/topMom";
    /**
     * 取消推荐日志
     */
    public static final String CANCEL_RECOMMEND = "/friend/stay/assist/admin/cancelRecommendMom";
    /**
     * 关注日志列表的直播入口
     */
    public static final String BROADCASTERS_ON_LIVE = "/v1/live/banner";

    /**
     * 2.21.0
     * 获取话题推荐列表接口
     */
    public static final String GET_THEMES_RECOMMEND = "/v2/themes/recommend";

    /**
     * 3.0.0
     * 查看指定用户的视频列表
     */
    public static final String GET_MOMENTS_VIDEO_USER_LIST = "/v2/moments/video/user/list";

    /**
     * 上报视频播放次数
     */
    public static final String POST_VIDEO_PLAY_COUNT_UPLOAD = "/v2/moments/video/playCount/upload";
    /**
     * 3.0.3 获取软妹豆数量接口
     */
    public static final String GET_CREDITS_NUMBER = "/v1/user/goldCount";

    /**
     * 3.0.3 获取礼物挤眼列表
     */
    public static final String GET_WINK_ITEMS = "/v1/wink/items";

    /**
     * 3.1.0 获取直播间用户信息
     */
    //todo 获取用户信息 新增参数名confine
    public static final String GET_LIVE_USERS_CARD = "/v2/users/card";
    /**
     * 3.1.0 获取日志汇总最新的一些带图片的日志
     */
    public static final String GET_NEARBY_MOMENT_NEW_LIST = "/v2/moments/nearby/new";
    /**
     * 3.2.0 获取新人直播列表
     */
    public static final String GET_NEW_ANCHORS = "/v1/live/newAnchors";
    /**
     * 4.0.0 用户关系信息列表(朋友：friend,关注：concern)
     */
    public static final String GET_USERS_RELATION = "/v2/users/relation";
    public static final String GET_SINGLE_USER_REALTION = "/v2/users/relation/single";
    /**
     * 4.0.0 朋友推荐的日志详情
     */
    public static final String GET_FRIEND_RECOMMEND_DETAIL = "/v2/moments/friendRecommend/detail";
    public static final String GET_MOMENTS_FRIEND_RECOMMEND_LIST = "/v2/moments/friendRecommend/list";
    /**
     * 4.1.0
     * 获取评论列表 *
     */
    public static final String THEMES_COMMENT_LIST = "/v2/themes/comment/list";
    /**
     * 4.1.0 添加话题回复
     */
    public static final String THEMES_COMMENT_ADD = "/v2/themes/comment/add";
    /**
     * 4.1.0挤眼，又服务器发送消息*
     */
    public static final String SEND_WINK_CREATE = "/v1/wink/create";
    /***
     *4.4.0获取直播类型
     */
    public static final String GET_LIVE_TYPE = "/v1/live/logType-list";
    /**
     * 获取附近直播
     */
    public static final String GET_NEARBY_LIVE = "/v1/live/neighbours";
    /**
     * 推荐关注主播类型
     */
    public static final String GET_FOLLOW_RECOMMEND_TYPE = "/v1/live/recommend-follow-with-logType";
    /**
     * 推荐关注新人列表
     */
    public static final String GET_RECOMMEND_ANCHORS_LIST = "/v1/live/recommend-follow-anchors";
    /**
     * 按类型获取直播
     */
    public static final String GET_TYPE_LIVE_LIST = "/v1/live/list-with-logType";
    /**
     * 获取正在直播的新人直播
     **/
    public static final String GET_NEW_LIVE_LIST = "/v1/live/new-anchor-live-list";
    /**
     * 获取直播间好友PK列表
     * 4.4.0
     **/
    public static final String GET_LIVE_PK_FRIEND_LIST = "/v1/live/living-friends";

    public static final String GET_LIVE_CLASSIFICATION = "v1/live/logType-list";


    public static final String POST_LIVE_PK_LOG_UPLOAD = "reports/log";

    public static final String FOLLOW_USERS = "v2/users/follow/create/bluk";
    /**
     * 4.7.0 视频瀑布流
     */
    public static final String GET_MOMENTS_VIDEO_LIST = "v2/moments/video/list";
    /**
     * 4.7.4 PGC 瀑布流数据
     */
    public static final String GET_PGC_MOMENTS_VIDEO_LIST = "v2/moments/pgc/list";

    public static final String POST_AD_INDIFFERENT = "moments/admoments/indifferent";

    /**
     * 4.9.0首冲优惠
     */
    public static final String GET_IS_FIRST_CHARGE = "v1/gold/is-first-charge";

    /**
     * 获取前三名粉丝信息
     */
    public static final String GET_TOP_FANS_TODAY = "v1/live/topFansToday";

    /**
     * 上传视频播放的信息
     */
    public static final String POST_PLAY_VIDEO_INFO = "v2/moments/video/play/upload";

    /**
     * 礼物详情
     */
    public static final String GET_LIVE_GIFT_DETAIL = "v1/live/gift/detail";

    /**
     * push上报接口
     */
    public static final String PUSH_ACTION_UPLOAD = "v2/users/action/upload";

    /**
     * 针对vivo vip做abtest
     */
    public static final String GET_VIVO_VIP_LIST = "v1/vip/vivo/list";

    /**
     * 针对vivo 软妹豆做abtest
     */
    public static final String GET_VIVO_GOLD_LIST = "v1/gold/vivo/list";

    /**
     * 图片排序接口
     */
    public static final String IMAGE_SORT = "v2/images/sort";

    /**
     * 获取历史删除的7天内日志列表
     */
    public static final String DELETE_MOMENT_LIST = "v2/moments/recyclebin";

    /**
     * 恢复被删除的日志
     */
    public static final String RECOVER_MOMENT = "v2/moments/recover";

    /**
     * 匹配用户详情里的日志列表
     */
    public static final String MATCH_USER_MOMENT = "v2/mate/moments";

    /**
     * 小红点开关更新
     */
    public static final String SWITCH_REDPOINT_UPDATE = "/v2/redpoint/update";

    /**
     * 直播日志上报
     */
    public static final String REPORTS_LOGS = "reports/v2/logs";

    /**
     * 匹配日志上报 test-report-api.rela.me/reports/ai/logs  report-api.rela.me/reports/ai/logs
     */
    public static final String MATCH_REPORTS_LOGS = "reports/ai/logs";

    /**
     * 期待直播
     */
    public static final String LOOKING_FORWARD_TO_LIVE = "/v2/live/await";

    /**
     * 获取推荐列表
     */
    public static final String FOLLOW_PAGE_RECOMMEND_LIST = "/v2/followpage/recommend/list";

    /*--------------------------------------管理员专属功能--------------------------------------------**/

    /*--------------------------------------接口请求参数字段--------------------------------------------**/

    /*--------------------------the l部分---------------------------------**/
    /**
     * 手机操作系统标识 eg.Android 4.4 *
     */
    public static final String I_MOBILE_OS = "mobileOS";
    /**
     * 客户端版本号
     */
    public static final String I_CLIENT_VERSION = "clientVersion";
    /**
     * 登录，注册获取用户标识 *
     */
    public static final String I_KEY = "key";
    /**
     * 直播类型id
     */
    public static final String I_LIVE_TYPE_ID = "liveTypeId";
    /**
     * 直播类型 0视频直播1音频直播
     */
    public static final String I_LIVE_TYPE = "audioType";
    /**
     * 直播公告
     */
    public static final String I_ANNOUNCEMENT = "announcement";
    /**
     * 经度 *
     */
    public static final String I_LNG = "lng";
    /**
     * 纬度 *
     */
    public static final String I_LAT = "lat";
    /**
     * 经度 *
     */
    public static final String I_WORLD_LNG = "toLng";
    /**
     * 纬度 *
     */
    public static final String I_WORLD_LAT = "toLat";
    /**
     * 经纬度字符串，逗号分隔 *
     */
    public static final String I_LOC = "loc_str";
    /**
     * 语言 *
     */
    public static final String I_LANGUAGE = "language";
    /**
     * 设备的唯一标识 *
     */
    public static final String I_DEVICE_ID = "deviceId";
    /**
     * app类型，国际版or国内版
     */
    public static final String I_APP_TYPE = "apptype";
    /**
     * 是否为调试模式，如果为1，那么将输出数组数据 *
     */
    public static final String I_DEBUG = "debug";
    /**
     * 登录类型：手机登录(cell)、密码登陆(password)、fb、wx
     */
    public static final String I_TYPE = "type";
    /**
     * 手机号
     */
    public static final String I_CELL = "cell";
    /**
     * 短信验证id
     */
    public static final String I_SMS_REQUEST_ID = "smsRequestId";
    /**
     * 区号
     */
    public static final String I_ZONE = "zone";
    /**
     * 验证码
     */
    public static final String I_CODE = "code";
    /**
     * 客户端时间
     */
    public static final String I_CLIENT_TIME = "clientTime";
    /**
     * 七牛桶
     */
    public static final String I_BUCKET = "bucket";
    /**
     * 文件路径
     */
    public static final String I_PATH = "path";
    /**
     * 社交APP的账户id
     */
    public static final String I_SNS_ID = "snsId";
    /**
     * token
     */
    public static final String I_SNS_TOKEN = "snsToken";
    /**
     * mobid标识是安卓还是iOS，我们传0，标识安卓
     */
    public static final String I_MOBID = "mobid";
    /**
     * 账号邮箱地址 *
     */
    public static final String I_EMAIL = "email";
    /**
     * 账号密码 *
     */
    public static final String I_PASSWORD = "password";
    /**
     * 单次读取的用户数 *
     */
    public static final String I_PAGESIZE = "pageSize";
    /**
     * 页数 (从1开始) *
     */
    public static final String I_CURPAGE = "curPage";
    /**
     * 话题类型
     */
    public static final String I_THEMECLASS = "themeClass";
    /**
     * 排序方式
     */
    public static final String I_SORT = "sort";
    /**
     * 排序方式
     */
    public static final String I_SORT_TYPE = "sortType";
    /**
     * 单页最大数据量
     */
    public static final String I_LIMIT = "limit";
    /**
     * 过滤条件
     */
    public static final String I_FILTER = "filter";
    /**
     * 页码游标
     */
    public static final String I_CURSOR = "cursor";
    /**
     * 喜欢自己的新的下标
     */
    public static final String I_NEW_CURSOR = "newCursor";
    /**
     * 用户id *
     */
    public static final String I_USERID = "userId";
    /**
     * 消息图片 *
     */
    public static final String I_MESSAGE_IMAGE = "messageImage";
    /**
     * 消息语音 *
     */
    public static final String I_MESSAGE_VOICE = "voiceMessage";
    /**
     * from *
     */
    public static final String I_MESSAGE_VOICE_FROM = "from";
    /**
     * to *
     */
    public static final String I_MESSAGE_VOICE_TO = "to";
    /**
     * 是否在线(0不是, 1 是) *
     */
    public static final String I_ONLINE = "online";
    /**
     * 是否收到"传情"最多(0不是, 1 是) *
     */
    public static final String I_SUPSTAR = "supStar";
    /**
     * 是否最新注册会员(0不是, 1 是) *
     */
    public static final String I_NEWREGIST = "newRegist";
    /**
     * 星座 *
     */
    public static final String I_HOROSCOPE = "horoscope";
    /**
     * 种族 *
     */
    public static final String I_ETHNICITY = "ethnicity";
    /**
     * 好友类型("following" 关注, "followers"粉丝) *
     */
    public static final String I_LISTTYPE = "listType";
    /**
     * 筛选条件("online"在线, "star"明星, "newPic"新照片, "distance"位置距离) *
     */
    public static final String I_SELECTCONDITION = "selectCondition";

    public static final String I_USERIMAGE = "userImage";
    public static final String I_BGIMAGE = "content";
    /**
     * 图片id *
     */
    public static final String I_PICID = "picId";
    /**
     * 搜索关键字 *
     */
    public static final String I_KEYWORD = "keyWord";
    public static final String I_KEY_WORD = "keyword";
    public static final String I_ID = "id";
    /**
     * 用户名 *
     */
    public static final String I_USER_NAME = "userName";
    /**
     * 上传头像
     */
    public static final String I_URL = "url";
    /**
     * 生日 *
     */
    public static final String I_BIRTHDAY = "birthday";
    /**
     * 寻找的角色 *
     */
    public static final String I_WANT_ROLE = "wantRole";
    /**
     * 职业类型 *
     */
    public static final String I_CAREER_TYPE = "professionalTypes";
    /**
     * 出柜程度 *
     */
    public static final String I_OUT_LEVEL = "outLevel";
    /**
     * 头像地址 *
     */
    public static final String I_AVATAR_URL = "avatarURL";
    /**
     * 昵称 *
     */
    public static final String I_NICKNAME = "nickName";
    /**
     * 头像 *
     */
    public static final String I_AVATAR = "avatar";
    /**
     * 年龄 *
     */
    public static final String I_AGE = "age";
    /**
     * 身高 *
     */
    public static final String I_HEIGHT = "height";
    /**
     * 体重 *
     */
    public static final String I_WEIGHT = "weight";
    /**
     * 角色 *
     */
    public static final String I_ROLENAME = "roleName";
    /**
     * 感情状态 *
     */
    public static final String I_AFFECTION = "affection";
    /**
     * 交友目的 *
     */
    public static final String I_PURPOSE = "purpose";
    /**
     * 职业 *
     */
    public static final String I_OCCUPATION = "occupation";
    /**
     * 居住地 *
     */
    public static final String I_LIVECITY = "livecity";
    /**
     * 旅行地 *
     */
    public static final String I_TRAVELCITY = "travelcity";
    /**
     * 活动范围 *
     */
    public static final String I_LOCATION = "location";
    /**
     * 影视剧 *
     */
    public static final String I_MOVIE = "movie";
    /**
     * 音乐 *
     */
    public static final String I_MUSIC = "music";
    /**
     * 书籍 *
     */
    public static final String I_BOOKS = "books";
    /**
     * 食物 *
     */
    public static final String I_FOOD = "food";
    /**
     * 其他爱好 *
     */
    // public static final String I_OTHERS = "others";
    /**
     * 其它爱好4.4.0
     */
    public static final String I_OTHERS = "interests";

    /**
     * 签名 *
     */
    public static final String I_INTRO = "intro";
    /**
     * 接收者id *
     */
    public static final String I_RECEIVEDID = "receivedId";
    /**
     * 密友圈请求的ID *
     */
    public static final String I_REQUEST_ID = "requestId";
    /**
     * 密友圈请求类型 *
     */
    public static final String I_REQUEST_TYPE = "requestType";
    /**
     * 结为伴侣的日期 *
     */
    public static final String I_ANNIVERSARY_DATE = "anniversaryDate";
    /**
     * 结为伴侣的日期 *
     */
    public static final String I_REQUEST_COMMENT = "requestComent";
    /**
     * 接收者id列表 *
     */
    public static final String I_RECEIVEDID_LIST = "receivedIdList";
    /**
     * 动作类型(0,取消，1 添加) *
     */
    public static final String I_ACTIONTYPE = "actionType";
    /**
     * 新浪微博UId *
     */
    public static final String I_SINAUID = "sinaUid";
    /**
     * 新浪微博Token *
     */
    public static final String I_SINATOKEN = "sinaToken";
    /**
     * 新浪微博TokenSecret *
     */
    public static final String I_SINATOKENSECRET = "sinaTokenSecret";
    /**
     * 用户登录的pass *
     */
    public static final String I_pass = "pass";
    /**
     * 原始密码 *
     */
    public static final String I_OLD_PASSWORD = "oldPassword";
    /**
     * 新密码 *
     */
    public static final String I_NEW_PASSWORD = "newPassword";
    /**
     * push接口消息类型 挤眼，图片，关注 *
     */
    public static final String I_MESSAGE = "message";
    /**
     * token *
     */
    public static final String I_TOKEN = "token";
    /**
     * client *
     */
    public static final String I_CLIENT = "client";
    /**
     * 语音信息base64 *
     */
    public static final String I_VOICEMESSAGE = "voiceMessage";
    /**
     * 语音信息时长 *
     */
    public static final String I_RECORDERTIMES = "recorderTimes";
    /**
     * 消息的id *
     */
    public static final String I_PACKET_ID = "packetId";
    /**
     * moments页的页面类型 *
     */
    public static final String I_MAIN_TYPE = "mainType";
    /**
     * moment类型 *
     */
    public static final String I_MOMENTS_TYPE = "momentsType";
    /**
     * moment文字内容 *
     */
    public static final String I_MOMENT_TEXT = "momentsText";
    /**
     * momen公开范围 *
     */
    public static final String I_SHARE_TO = "shareTo";
    /**
     * moment是否匿名 *
     */
    public static final String I_SECRET = "secret";
    /**
     * moment at的用户id列表 *
     */
    public static final String I_AT_USER_LIST = "atUserList";
    /**
     * moment id *
     */
    public static final String I_MOMENTS_ID = "momentsId";
    /**
     * moment id *
     */
    public static final String I_MOMENT_ID = "momentId";
    /**
     * 日志发布的图片地址列表，以逗号分隔
     */
    public static final String I_IMAGE_URLS = "imageUrlList";
    /**
     * 单张图片
     */
    public static final String I_IMAGE_URL = "imageUrl";
    /**
     * 日志发布的图片文件大小列表，以逗号分隔
     */
    public static final String I_IMAGE_LENGTHS = "imageLengthList";
    /**
     * wink类型 *
     */
    public static final String I_WINK_COMMENT_TYPE = "winkCommentType";
    /**
     * 评论内容 *
     */
    public static final String I_COMMENT_TEXT = "commentText";
    /**
     * 评论类型 *
     */
    public static final String I_COMMENT_TYPE = "commentType";
    /**
     * toUserId *
     */
    public static final String I_TO_USER_ID = "toUserId";
    /**
     * 评论的id *
     */
    public static final String I_COMMENT_ID = "commentId";
    /**
     * 快捷筛选的年龄段 *
     */
    public static final String I_AGE_RANGE = "ageRange";
    /**
     * 歌曲id *
     */
    public static final String I_SONG_ID = "songId";
    /**
     * 歌曲名 *
     */
    public static final String I_SONG_NAME = "songName";
    /**
     * 歌手名 *
     */
    public static final String I_ARTIST_NAME = "artistName";
    /**
     * 专辑名 *
     */
    public static final String I_ALBUM_NAME = "albumName";
    /**
     * 专辑封面小尺寸 *
     */
    public static final String I_ALBUM_LOGO_100 = "albumLogo100";
    /**
     * 专辑封面大尺寸 *
     */
    public static final String I_ALBUM_LOGO_444 = "albumLogo444";
    /**
     * 歌曲文件所在的地址 *
     */
    public static final String I_SONG_LOCATION = "songLocation";
    /**
     * 歌曲跳转页面url *
     */
    public static final String I_SONG_TO_URL = "toURL";
    /**
     * 话题名 *
     */
    public static final String I_TOPIC_NAME = "topicName";
    /**
     * 话题id *
     */
    public static final String I_TOPIC_ID = "topicId";
    /**
     * uuid *
     */
    public static final String I_UUID = "uuid";
    /**
     * 上传文件的文件类型 *
     */
    public static final String I_FILE_TYPE = "file_type";
    /**
     * 上传文件的文件数据 *
     */
    public static final String I_FILE_DATA = "file_data";
    /**
     * 标签分类的id *
     */
    public static final String I_CATEGORY_ID = "cateId";
    /**
     * 发现页面日志分页查询，已经看过的页面序号 *
     */
    public static final String I_HISTORY_PAGE_LIST = "historyPageList";
    /**
     * 举报类型 *
     */
    public static final String I_REASON_TYPE = "reasonType";
    /**
     * 举报理由（文字描述） *
     */
    public static final String I_REASON_CONTENT = "reasonContent";
    /**
     * bug反馈类型 *
     */
    public static final String I_BUG_FEEDBACK_TYPE = "feedBackType";
    /**
     * bug反馈理由（文字描述） *
     */
    public static final String I_BUG_FEEDBACK_CONTENT = "feedBackContent";
    /**
     * 下载表情包 *
     */
    public static final String I_DOWNLOAD = "download";
    /**
     * 购买商品类型 *
     */
    public static final String I_PRODUCT_TYPE = "productType";
    /**
     * 购买商品的id *
     */
    public static final String I_PRODUCT_ID = "productId";
    /**
     *
     */
    public static final String I_DATA = "data";
    /**
     * 验证，签名
     */
    public static final String I_IAP_SIGNATURE = "iapSignature";
    /**
     * 支付类型 *
     */
    public static final String I_PAY_TYPE = "payType";
    /**
     * 来源
     */
    public static final String I_PAY_SOURCE = "source";
    /**
     * 文本 *
     */
    public static final String I_TEXT = "text";
    /**
     * 赠送给她人的用户id
     */
    public static final String I_SEND_TOLD = "sendToId";
    /**
     * 赠送礼物留言
     */
    public static final String I_GIFT_CARD = "giftCard";
    /**
     * 悄悄关注的对象 userid
     */
    public static final String I_SECRET_FOLLOW_RECEIVEID = "receivedId";
    /**
     * [可选］如果加了该参数，将自动将视频文件截取5s转换为gif并保存在thel-images桶中的gifPath, 域名固定为 static.tel.co
     */
    public static final String I_GIFPATH = "gifPath";
    /**
     * [可选］相册传 album
     */
    public static final String I_GIFTYPE = "gifType";
    /**
     * [可选］传相册id
     */
    public static final String I_GIFKEY = "gifKey";

    public static final String I_SIGNATURE = "signature";

    public static final String I_NEW_SIGNATURE = "Signature";

    public static final String I_VIDEO = "video";

    /**
     * 2.17 收藏日志  收藏类型中的 mom
     */
    public static final String FAVORITE_TYPE_MOM = "mom";
    /**
     * 2.17 收藏  日志 id
     */
    public static final String FAVORITE_COUNT = "content";
    /**
     * 2.17收藏日志类型
     */
    public static final String FAVORITE_TYPE = "type";
    /**
     * 2.17 收藏的Id
     */
    public static final String FAVORITE_ID = "id";
    /**
     * 2.21.0 关闭并删除直播日志 字段
     */
    public static final String DEL_LIVE_LOG = "delLiveLog";
    /**
     * 2.20 直播举报 用户或者主播
     */
    public static final String REPORT_LIVESHOW_RECEIVED_ID = "userId";//被举报的用户ID
    public static final String REPORT_LIVESHOW_REASON_TYPE = "reasonType";//原因类型
    public static final String REPORT_LIVESHOW_REASON_CONTENT = "reasonContent";//原因详情内容
    public static final String REPORT_LIVESHOW_IMAGE_URL = "url";//举报的截图地址，多个可用逗号分隔
    public static final String REPORT_LIVESHOW_REPORT_TYPE = "type";//举报类型，0 为普通，1 为举报主播, 2 为直播观众, 默认为0
    public static final String REPORT_LIVESHOW_REPORT_CONTENT_WORD = "commentWord";//举报直播评论下的话的内容
    /**
     * 3.0
     */
    public static final String VIDEO_PLAY_COUNT = "count";//视频播放次数上报
    /***
     * 3.0.3软妹豆余额
     */
    public static final String CREDITS_BALANCE = "gold";
    /**
     * 3.2.0用户头像图片举报
     */
    public static final String REPORT_LIVEUSER_RECEIVED_ID = "userId";//被举报的用户ID
    public static final String REPORT_LIVEUSER_REPORT_TYPE = "type";//举报类型，0用户头像:avatar ,1用户空间图片:userimg
    public static final String REPORT_LIVEUSER_REPORT_IMGID = "id";//type为userimg id参数有效)
    public static final String SELECT_MOMENT_THEME = "themeclass";//话题分类
    /**
     * 4.0.0 用户关系列表，
     */
    public static final String RELATION_TYPE_FRIEND = "friend";//朋友
    public static final String RELATION_TYPE_CONCERN = "concern";//关注

    /**
     * rela 短视频录制API
     */

    public static final String GET_MUSIC_BY_CATEGORY = "/v1/moment/music/getByCategory";
    public static final String GET_MUSIC_CATEGORIES = "/v1/moment/music/getCategories";
    public static final String GET_HOT_MUSIC = "/v1/moment/music/getHot";
    public static final String GET_NEWEST_MUSIC = "/v1/moment/music/getNewest";
    /**
     * 4.5.0发布直播新增当前定位的城市
     */
    public static final String I_CITY = "city";

    /**
     * 4.16.0附近列表过滤字段
     */
    public static final String HER_ROLE_NAME = "her_role_name";//T,P,H,双性，不限 不限为空 多选 int[]
    public static final String AGE = "age";//int[] [18,24]
    public static final String AFFECTION = "affection";//0,1,2
    public static final String ACTIVE = "active";//0关闭 1打开
    public static final String VIP = "vip";//0关闭 1打开
    public static final String HAVE_PHOTO = "have_photo";//0关闭 1打开
    public static final String HOROSCOPE = "horoscope";//int[]
    public static final String SECRCH = "search";//1表示打开搜索

    public static final String AK = "321Ybrcdy461NOx5nvwF28I9QA_3NLdVCM22xhoQ";
    public static final String SK = "BYmCTId36H0qO5hmmRnwgBnZL55nUWKULE0_U0N2";

    /*--------------------------------------------------=--------------------------------------------**/

    /*-----------------------------接口响应状态码，根据状态码回调做不同的处理-------------------------------**/
    /**
     * 服务器连接 返回正确数据 *
     */
    public final static int RESPONSE_SUCCESS = 1;
    /**
     * 服务器连接 返回有误数据 *
     */
    public final static int RESPONSE_ERRORDATA = 2;
    /**
     * 服务器连接 请求超时 *
     */
    public final static int RESPONSE_TIMEOUT = 3;
    /**
     * 服务器连接 没有网络 *
     */
    public final static int RESPONSE_NONETWORK = 4;
    /**
     * 服务器连接 网络连接异常 *
     */
    public final static int RESPONSE_EXCEPTION = 5;
    /**
     * 服务器连接 报文解析异常 *
     */
    public final static int RESPONSE_PARSE_EXCEPTION = 6; // 目前没用到
    /*------------------------------------------------------------------------------------------------**/

    /**
     * 如果返回的response_data对象中exception_flag为0时表示 忽略这次请求异常 *
     */
    public final static int EXCEPTION_FLAG_IGNORE = 0;
    /**
     * 如果返回的response_data对象中exception_flag为1时表示 处理这次请求异常 *
     */
    public final static int EXCEPTION_FLAG_PROCESS = 1;
    /*------------------------------------------------------------------------------------------------**/

    /*----------------------------------------代理-----------------------------------------------------**/
    /**
     * 代理地址 *
     */
    public final static String PROXY_HOSTNAME = "10.0.0.172";
    /**
     * 代理端口号 *
     */
    public final static int PROXY_PORT = 80;


    /*-------------------------------------------------------------------------------------------------**/


    public static class ChatApiUrlConstants {

        public static final String CREATE_CHAT = "rela/api/chatroom/room/create";

        public static final String JOIN_CHAT = "rela/api/chatroom/room/user/join";

        public static final String DESTROY_CHAT = "rela/api/chatroom/room/destroy?key";

        public static final String LEAVE_CHAT = "rela/api/chatroom/room/user/leave";

        public static final String SEND_GIFT = "rela/api/chatroom/room/send/gift";

    }

}
