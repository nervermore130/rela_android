package com.thel.demo.live.network.beans;

import java.io.Serializable;

public class AgoraBean implements Serializable {

    /**
     * agora频道id
     */
    public String channelId;
    /**
     * agoraToken, 为null时，表示用户未登录或者不是一个agora多人视频; 为空字符串时，表示未启用token，此时用appId进入频道
     */
    public String token;
    /**
     * 声网appId
     */
    public String appId;

    @Override
    public String toString() {
        return "AgoraBean{" +
                "token='" + token + '\'' +
                ", appId='" + appId + '\'' +
                ", channelId='" + channelId + '\'' +
                '}';
    }
}
