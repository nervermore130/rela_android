//package com.thel.demo.live.util;
//
//import android.os.Build;
//
//import java.security.Provider;
//import java.security.SecureRandom;
//
//import javax.crypto.Cipher;
//import javax.crypto.KeyGenerator;
//import javax.crypto.SecretKey;
//import javax.crypto.spec.SecretKeySpec;
//
///**
// * Created by setsail on 15/6/30.
// */
//public class AESEncryptor {
//
//    public static final String SEED = "cuhdes31EDI";
//
//    /**
//     * AES加密
//     */
//    public static String encrypt(String seed, String cleartext) throws Exception {
//        byte[] rawKey = getRawKey(seed.getBytes());
//        byte[] result = encrypt(rawKey, cleartext.getBytes());
//        return toHex(result);
//    }
//
//    /**
//     * AES解密
//     */
//    public static String decrypt(String seed, String encrypted) throws Exception {
//        byte[] rawKey = getRawKey(seed.getBytes());
//        byte[] enc = toByte(encrypted);
//        byte[] result = decrypt(rawKey, enc);
//        return new String(result);
//    }
//
//    /**
//     * AES加密，有签名
//     */
//    public static String encryptSigned(String seed, String cleartext) throws Exception {
//        byte[] rawKey = getRawKey(seed.getBytes());
//        // 在前面加上用户id，作为签名
//        byte[] result = encrypt(rawKey, (ShareFileUtils.getString(ShareFileUtils.ID, "") + cleartext).getBytes());
//        return toHex(result);
//    }
//
//    /**
//     * AES解密，有签名
//     */
//    public static String decryptSigned(String seed, String encrypted) throws Exception {
//        byte[] rawKey = getRawKey(seed.getBytes());
//        byte[] enc = toByte(encrypted);
//        byte[] result = decrypt(rawKey, enc);
//        // 验证签名
//        String str = new String(result);
//        String id = ShareFileUtils.getString(ShareFileUtils.ID, "");
//        if (str.startsWith(id)) {
//            return str.replaceFirst(id, "");
//        } else {
//            return "";
//        }
//    }
//
//    private static byte[] getRawKey(byte[] seed) throws Exception {
//
//        /* get the SecretKey as before Pie */
//
//        if (Build.VERSION.SDK_INT >= 28) {
//            return InsecureSHA1PRNGKeyDerivator.deriveInsecureKey(seed, 32);
//        } else {
//            KeyGenerator kgen = KeyGenerator.getInstance("AES");
//            SecureRandom sr = SecureRandom.getInstance("SHA1PRNG", new CryptoProvider());
//            sr.setSeed(seed);
//            kgen.init(128, sr); // 192 and 256 bits may not be available
//            SecretKey secretKey = kgen.generateKey();
//            return secretKey.getEncoded();
//        }
//
//    }
//
//
//    private static byte[] encrypt(byte[] raw, byte[] clear) throws Exception {
//        SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
//        Cipher cipher = Cipher.getInstance("AES");
//        cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
//        return cipher.doFinal(clear);
//    }
//
//    private static byte[] decrypt(byte[] raw, byte[] encrypted) throws Exception {
//        SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
//        Cipher cipher = Cipher.getInstance("AES");
//        cipher.init(Cipher.DECRYPT_MODE, skeySpec);
//        byte[] decrypted = cipher.doFinal(encrypted);
//        return decrypted;
//    }
//
//    public static String toHex(String txt) {
//        return toHex(txt.getBytes());
//    }
//
//    public static String fromHex(String hex) {
//        return new String(toByte(hex));
//    }
//
//    public static byte[] toByte(String hexString) {
//        int len = hexString.length() / 2;
//        byte[] result = new byte[len];
//        for (int i = 0; i < len; i++)
//            result[i] = Integer.valueOf(hexString.substring(2 * i, 2 * i + 2), 16).byteValue();
//        return result;
//    }
//
//    public static String toHex(byte[] buf) {
//        if (buf == null)
//            return "";
//        StringBuffer result = new StringBuffer(2 * buf.length);
//        for (int i = 0; i < buf.length; i++) {
//            appendHex(result, buf[i]);
//        }
//        return result.toString();
//    }
//
//    private final static String HEX = "0123456789ABCDEF";
//
//    private static void appendHex(StringBuffer sb, byte b) {
//        sb.append(HEX.charAt((b >> 4) & 0x0f)).append(HEX.charAt(b & 0x0f));
//    }
//
//    private static final class CryptoProvider extends Provider {
//
//        /**
//         * Constructs a provider with the specified name, version number,
//         * and information.
//         */
//        protected CryptoProvider() {
//            super("Crypto", 1.0, "HARMONY (SHA1 digest; SecureRandom; SHA1withDSA signature)");
//            put("SecureRandom.SHA1PRNG",
//                    "org.apache.harmony.security.provider.crypto.SHA1PRNG_SecureRandomImpl");
//            put("SecureRandom.SHA1PRNG ImplementedIn", "Software");
//        }
//    }
//}