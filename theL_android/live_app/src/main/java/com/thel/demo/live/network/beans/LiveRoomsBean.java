package com.thel.demo.live.network.beans;

import com.thel.demo.live.network.beans.BaseDataBean;

import java.util.ArrayList;

public class LiveRoomsBean extends BaseDataBean {

    public ArrayList<LiveRoomBean> list = new ArrayList<>();
    public String label;
    public String imageUrl;
    public int cursor;
}
