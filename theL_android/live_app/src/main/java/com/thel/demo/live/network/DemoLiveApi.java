package com.thel.demo.live.network;

import com.thel.demo.live.network.beans.LiveRoomsNetBean;

import io.reactivex.Flowable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface DemoLiveApi {

    /**
     * 获取直播列表
     *
     * @param cursor 请求cursor，第一页是1，没有时候为0
     * @param sort   类型 hot
     *               GET_LIVE_ROOMS
     * @return
     */
    @GET("v1/live/list")
    Flowable<LiveRoomsNetBean> getLiveRoomsListBean(@Query(RequestConstants.I_CURSOR) String cursor, @Query(RequestConstants.I_SORT) String sort);




}
