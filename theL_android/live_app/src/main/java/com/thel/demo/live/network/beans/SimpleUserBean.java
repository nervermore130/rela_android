package com.thel.demo.live.network.beans;

import java.io.Serializable;

public class SimpleUserBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户id(在某些接口中用，如直播列表)
     */
    public long id;

    /**
     * 软妹豆
     */

    public long gold = 0;

    /**
     * 用户id(目前只在挤眼功能中使用)
     */
    public long userId;

    /**
     * 是否在线( 0 不在,1 在,2忙)
     */
    public String online;

    /**
     * 用户名
     */
    public String nickName;

    /**
     * 头像
     */
    public String avatar;

    /**
     * 距离
     */
    public String distance;

    public String intro;

    public String roleName;

    /**
     * 认证状态，是否是加V用户 0:否 1:个人V 2:企业V
     */
    public String verifyType;
    /**
     * 认证说明
     */
    public String verifyIntro;
    /**
     * 年龄
     */
    public int age;
    /**
     * 是否开启了隐身
     */
    public int hiding;
    /**
     * 会员等级
     */
    public int level;
    /**
     * 感情状态
     */
    public int affection;
    /**
     * 我是否关注她
     */
    public int isFollow;
    /**
     * 粉丝数
     */
    public int fansNum;

    /**
     * 用户等级
     */
    public int userLevel;

    /**
     * 判断是否被禁用
     */
    public String confine;

    /**
     * 是否显示用户在排行榜中
     */
    public int isCloaking;


    @Override
    public String toString() {
        return "SimpleUserBean{" +
                "id=" + id +
                ", userId=" + userId +
                ", online='" + online + '\'' +
                ", nickName='" + nickName + '\'' +
                ", avatar='" + avatar + '\'' +
                ", distance='" + distance + '\'' +
                ", intro='" + intro + '\'' +
                ", roleName='" + roleName + '\'' +
                ", verifyType='" + verifyType + '\'' +
                ", verifyIntro='" + verifyIntro + '\'' +
                ", age=" + age +
                ", hiding=" + hiding +
                ", level=" + level +
                ", affection=" + affection +
                ", isFollow=" + isFollow +
                '}';
    }
}
