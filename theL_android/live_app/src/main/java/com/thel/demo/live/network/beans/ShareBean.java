package com.thel.demo.live.network.beans;

import java.io.Serializable;

public class ShareBean extends BaseDataBean implements Serializable {
    public String title;

    public String text;

    public String link;

    @Override
    public String toString() {
        return "ShareBean{" + "title='" + title + '\'' + ", text='" + text + '\'' + ", link='" + link + '\'' + '}';
    }
}
