package com.thel.demo.live.ui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.facebook.drawee.view.SimpleDraweeView;
import com.thel.demo.live.R;
import com.thel.demo.live.network.beans.LiveRoomBean;

import java.util.ArrayList;
import java.util.List;

public class RoomListAdapter extends RecyclerView.Adapter<RoomListAdapter.ViewHolder> {

    private List<LiveRoomBean> mList = new ArrayList<>();

    public RoomListAdapter(List<LiveRoomBean> list) {
        mList.clear();
        mList.addAll(list);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item_room, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        LiveRoomBean roomBean = mList.get(position);
//        ImageUtils.buildNetPictureUrl(url, width, height)
        String url = roomBean.imageUrl;
//        holder.draweeView.setController(Fresco.newDraweeControllerBuilder()
//                .setImageRequest(ImageRequestBuilder.newBuilderWithSource(Uri.parse(url)).build()).setAutoPlayAnimations(true).build());
        Glide.with(holder.itemView.getContext()).load(url).into(holder.draweeView);
        holder.titleView.setText(roomBean.text);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {

        SimpleDraweeView draweeView;
        TextView titleView;

        public ViewHolder(@NonNull View view) {
            super(view);
            draweeView = view.findViewById(R.id.preview);
            titleView = view.findViewById(R.id.text);
        }

//        public void bind(CircleBean item) {
//            mBinding.setCircle(item);
//            mBinding.executePendingBindings();
//        }
    }
}
