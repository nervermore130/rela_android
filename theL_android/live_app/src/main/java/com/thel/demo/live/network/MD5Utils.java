package com.thel.demo.live.network;

import android.os.Build;
import android.util.Log;

import com.thel.demo.live.BuildConfig;
import com.thel.demo.live.util.BusinessUtils;
import com.thel.demo.live.util.DemoSpUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * MD5加密工具类
 *
 * @author zhangwenjia
 */
public class MD5Utils {

    private static final String TAG = "MD5Utils";

    private static final char[] HEX_DIGITS = {'0',
            '1',
            '2',
            '3',
            '4',
            '5',
            '6',
            '7',
            '8',
            '9',
            'a',
            'b',
            'c',
            'd',
            'e',
            'f'};

    public static String SALT = "879f30c4b1641142c6192acc23cfb733";//"";

    private static StringBuffer stringBuffer = new StringBuffer();

    private final static String SALT1 = "izTeYDfLhBogw";
    private final static String SALT2 = "RKcb1SEQ3n4xw";

    /**
     * 获取签名字符串
     *
     * @return String 返回签名字符串
     */
    public static String getSign(Map<String, String> data, String appsecertValue) {
        // 获取map长度
        int length = data.size();
        String[] array = new String[length];
        StringBuffer stringBuffer = new StringBuffer();

        int x = 0;// 数组下标标识
        for (Map.Entry<String, String> m : data.entrySet()) {
            array[x] = m.getKey() + m.getValue();
            x++;
        }
        // 升序排序
        Arrays.sort(array);

        stringBuffer.append(appsecertValue);
        for (int i = 0; i < length; i++) {
            stringBuffer.append(array[i]);
        }
        return md5(stringBuffer.toString());
    }

    public static String md5(String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            byte[] messageDigest = digest.digest();
            return toHexString(messageDigest);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return s;
    }

    public static String md5(byte[] bytes) {
        String result = null;
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(bytes);
            byte[] messageDigest = digest.digest();
            result = toHexString(messageDigest);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    private static String toHexString(byte[] b) {// String to byte
        StringBuilder sb = new StringBuilder(b.length * 2);
        for (int i = 0; i < b.length; i++) {
            sb.append(HEX_DIGITS[(b[i] & 0xf0) >>> 4]);
            sb.append(HEX_DIGITS[b[i] & 0x0f]);
        }
        return sb.toString();
    }

    //    public static String generateSignature(List<BasicNameValuePair> requestData) {
    //        String signature = "";
    //        try {
    //            Set<String> params = new TreeSet<String>();
    //            for (BasicNameValuePair param : requestData) {
    //                params.add(param.toString());
    //            }
    //            StringBuilder sb = new StringBuilder();
    //            for (String s : params) {
    //                sb.append(s);
    //                sb.append("&");
    //            }
    //            if (sb.length() > 0) {
    //                sb.deleteCharAt(sb.length() - 1);
    //            }
    //            signature = MD5Utils.md5(sb.toString() + SALT);
    //        } catch (Exception e) {
    //        }
    //
    //        return signature;
    //    }

    public static synchronized String generateSignature(Map<String, String> params) {

        stringBuffer.setLength(0);

        params.putAll(getDefaultHashMap());

        for (Map.Entry<String, String> entry : params.entrySet()) {

            String key = entry.getKey();

            String value = entry.getValue();

            stringBuffer.append(key);
            stringBuffer.append("=");
            stringBuffer.append(value);
            stringBuffer.append("&");

        }

        String str = stringBuffer.toString();

        String result = generateSignature(str);

        stringBuffer.append("&");
        stringBuffer.append(RequestConstants.I_SIGNATURE);
        stringBuffer.append("=");
        stringBuffer.append(result);

        return stringBuffer.toString();
    }

    public static synchronized Map<String, String> generateSignatureForMap(Map<String, String> params) {

        stringBuffer.setLength(0);

        params.putAll(getDefaultHashMap());

        List<String> arrays = new ArrayList<>();

        for (Map.Entry<String, String> entry : params.entrySet()) {

            String key = entry.getKey();

            String value = entry.getValue();

            arrays.add(key + "=" + value);

        }

        Collections.sort(arrays);

        for (int i = 0; i < arrays.size(); i++) {
            String str = arrays.get(i);
            stringBuffer.append(str);
            if (i != arrays.size() - 1) {
                stringBuffer.append("&");
            }
        }

        String str = stringBuffer.toString();


        String result = MD5Utils.md5(str + SALT);

//
//        params.put(RequestConstants.I_SIGNATURE, result);

        return params;
    }

    public static String generateSignature(String params) {
        String signature = "";

        try {

            String currentParams = sort(params.trim());
            Log.d("rela_http", " currentParams :" + currentParams);

            signature = MD5Utils.md5(currentParams + SALT);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return signature;
    }

    public static String generateSignature(String params, String body) {
        String signature = "";

        try {
            String currentParams = sort(params.trim());
            final String result = currentParams + body + getSlat();
            signature = MD5Utils.md5(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return signature;
    }


    public static synchronized String sort(String params) {

        String[] paramsArray = params.split("&");

        List<String> arrays = Arrays.asList(paramsArray);

        Collections.sort(arrays);

        stringBuffer.setLength(0);

        for (int i = 0; i < arrays.size(); i++) {
            String str = arrays.get(i);
            stringBuffer.append(str);
            if (i != arrays.size() - 1) {
                stringBuffer.append("&");
            }
        }
        return stringBuffer.toString();
    }

    /**
     * 生成文件名
     *
     * @param file
     * @return
     */
    public static String calculateMD5(File file) {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            return null;
        }

        InputStream is;
        try {
            is = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            return null;
        }

        byte[] buffer = new byte[8192];
        int read;
        try {
            while ((read = is.read(buffer)) > 0) {
                digest.update(buffer, 0, read);
            }
            byte[] md5sum = digest.digest();
            BigInteger bigInt = new BigInteger(1, md5sum);
            String output = bigInt.toString(16);
            // Fill to 32 chars
            output = String.format("%32s", output).replace(' ', '0');
            return output;
        } catch (IOException e) {
            return null;
        } finally {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
    }

    public final static String getMessageDigest(byte[] buffer) {
        try {
            MessageDigest mdTemp = MessageDigest.getInstance("MD5");
            mdTemp.update(buffer);
            byte[] md = mdTemp.digest();
            int j = md.length;
            char[] str = new char[j * 2];
            int k = 0;
            for (int i = 0; i < j; i++) {
                byte byte0 = md[i];
                str[k++] = HEX_DIGITS[byte0 >>> 4 & 0xf];
                str[k++] = HEX_DIGITS[byte0 & 0xf];
            }
            return new String(str);
        } catch (Exception e) {
            return null;
        }
    }

    public static Map<String, String> getDefaultHashMap() {
        Map<String, String> data = new HashMap<>();
        data.put(RequestConstants.I_KEY, DemoSpUtils.getString(DemoSpUtils.DemoKey.USER_KEY,""));
        data.put(RequestConstants.I_DEBUG, "0");
        data.put(RequestConstants.I_LAT, "0.0");
        data.put(RequestConstants.I_LNG, "0.0");
        data.put(RequestConstants.I_MOBILE_OS, "Android " + Build.VERSION.RELEASE);
        data.put(RequestConstants.I_CLIENT_VERSION, BuildConfig.VERSION_CODE + "");
        data.put(RequestConstants.I_LANGUAGE, "zh_CN");
        data.put(RequestConstants.I_DEVICE_ID, DemoSpUtils.getString(DemoSpUtils.DemoKey.DEVICE_ID, UUID.randomUUID().toString()));
//        data.put(RequestConstants.I_APP_TYPE, "global");
        return data;
    }


    private static final String ALGORITHM = "RSA";

    private static final String SIGN_ALGORITHMS = "SHA1WithRSA";

    private static final String DEFAULT_CHARSET = "UTF-8";

    /**
     * 获取Slat
     *
     * @return
     */
    public static String getSlat() {
        return getMd5Slat(SALT1, SALT2);
    }

    /**
     * 获取Md5 Sloat
     *
     * @param salt1
     * @param salt2
     * @return
     */
    public static String getMd5Slat(String salt1, String salt2) {
        return md5(getBytes(getCharSolt(salt1, salt2)));
    }

    private static char[] getCharSolt(String a, String b) {
        final char[] arr1 = a.toCharArray();
        final char[] arr2 = b.toCharArray();
        final int length = Math.min(arr1.length, arr2.length);
        final char[] result = new char[length];
        for (int i = 0; i < length; i++) {
            int re = (arr1[i] + 1) ^ (arr2[i] - 1);
            result[i] = (char) re;
        }
        return result;
    }

    private static byte[] getBytes(char[] re) {
        final int length = re.length;
        final byte[] bt = new byte[length];
        for (int i = 0; i < length; i++) {
            bt[i] = (byte) re[i];
        }
        return bt;
    }

    public static String getUserAgent() {
        return BusinessUtils.generateUA();
    }
}
