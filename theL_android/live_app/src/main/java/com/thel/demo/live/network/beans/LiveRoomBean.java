package com.thel.demo.live.network.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class LiveRoomBean extends BaseDataBean implements Serializable {
    /**
     * 直播室id
     */
    public String id = "";

    /*
     * 直播间id号 4.11.1
     * **/
    public String liveId = "";

    /**
     * 直播用户信息
     */
    public SimpleUserBean user = new SimpleUserBean();
    /**
     * 标签
     */
    public String badge;
    /**
     * 是否在直播
     */
    public int active;

    /**
     * 观众数
     */
    public int liveUsersCount;
    /**
     * 直播描述
     */
    public String text;
    /**
     * 直播缩略图
     */
    public String imageUrl;

    public LiveChatBean livechat;

    public ShareBean share;
    /**
     * 新增粉丝数
     */
    public int fans;
    /**
     * 直播时长
     */
    public String livetime;
    /***
     * 最佳名次
     */
    public int bestrank;

    /**
     * 拉流地址
     */
    public String liveUrl;
    /**
     * 推流地址
     */
    public String pubUrl;

    /**
     * 提到的用户对象
     */
    // public List<SimpleUserBean> atUserList = new ArrayList<SimpleUserBean>();
    //  public String atUserList;
    /**
     * 直播排名
     */
    public int rank;
    /**
     * 主播的软妹币
     */
    public String gem;
    /**
     * 打赏的礼物列表对象
     */
    public SoftEnjoyBean softEnjoyBean = new SoftEnjoyBean();
    /**
     * 弹幕价格（软妹豆）
     */
    public int barrageCost;

    /**
     * 2.20新增  推荐标签
     */
    public String label;
    /**
     * ui旋转角度, 0为不旋转, 可选值为0、1、2、3, 分别*90为其旋转角度
     */
    public int uiRotate;
    /**
     * 流旋转角度, 值范围同上
     */
    public int streamRotate;
    /**
     * 热拉女孩排行榜按钮图片地址，如果不为空则显示按钮
     */
    public String rankListImage;
    /**
     * 热拉女孩排行榜地址
     */
    public String rankListUrl;

    int page;
    public List<LiveTopFansBean> topFans = new ArrayList<>();

    public List<SoftGiftBean> giftList = new ArrayList<>();

    public long gold = 0;
    /**
     * 距离
     */
    public String distance;
    /***本次软妹币增量***/
    public String nowGem;

    public String momentId;

    public StreamBean stream;

    public String activityInfoUrl;
    /**
     * 音频类型 0为视频直播，1是声音直播
     */
    public int audioType = -1;
    /**
     * 音频类型 0为单人直播，1为多人直播
     */
    public int isMulti;

    public int userLevel; //主播等级

    public int selfLevel;//自己等级
    /**
     * 视频直播
     */
    public static final int TYPE_VIDEO = 0;
    /**
     * 声音直播
     */
    public static final int TYPE_VOICE = 1;
    /**
     * 0不是多人直播
     */
    public static final int SINGLE_LIVE = 0;
    /**
     * 1为多人直播
     */
    public static final int MULTI_LIVE = 1;

    /**
     * 声网数据
     */
    public AgoraBean agora;

    /**
     * 是否指定播放地址
     */
    public int forceServerLiveUrl;

    /**
     * 直播状态
     */
    public int liveStatus;

    /**
     * 横屏 1 竖屏 0
     */
    public int isLandscape;

    /**
     * 直播公告
     */
    public String announcement;

    public String rank_id;

    public void setSoftEnjoyBean() {
        softEnjoyBean = new SoftEnjoyBean();
        softEnjoyBean.gold = gold;
        softEnjoyBean.list = giftList;
    }

    public void setSoftEnjoyBean(SoftEnjoyBean softEnjoyBean) {
        this.softEnjoyBean.list = softEnjoyBean.list;
    }

    public class StreamBean implements Serializable {
        public String rtmpLiveUrl;

        @Override
        public String toString() {
            return "StreamBean{" + "rtmpLiveUrl='" + rtmpLiveUrl + '\'' + '}';
        }
    }

    @Override
    public String toString() {
        return "LiveRoomBean{" + "id='" + id + '\'' + ", user=" + user + ", badge='" + badge + '\'' + ", active=" + active + ", liveUsersCount=" + liveUsersCount + ", text='" + text + '\'' + ", imageUrl='" + imageUrl + '\'' + ", livechat=" + livechat + ", share=" + share + ", fans=" + fans + ", livetime='" + livetime + '\'' + ", bestrank=" + bestrank + ", liveUrl='" + liveUrl + '\'' + ", pubUrl='" + pubUrl + '\'' + ", rank=" + rank + ", gem='" + gem + '\'' + ", softEnjoyBean=" + softEnjoyBean + ", barrageCost=" + barrageCost + ", label='" + label + '\'' + ", uiRotate=" + uiRotate + ", streamRotate=" + streamRotate + ", rankListImage='" + rankListImage + '\'' + ", rankListUrl='" + rankListUrl + '\'' + ", page=" + page + ", topFans=" + topFans + ", giftList=" + giftList + ", gold=" + gold + ", distance='" + distance + '\'' + ", nowGem=" + nowGem + ", momentId='" + momentId + '\'' + ", stream=" + stream + ", activityInfoUrl='" + activityInfoUrl + '\'' + ", liveStatus=" + liveStatus + '}';
    }
}
