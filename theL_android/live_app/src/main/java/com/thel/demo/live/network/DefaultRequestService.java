package com.thel.demo.live.network;

import android.net.Uri;
import android.os.Build;
import android.util.Log;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.thel.demo.live.BuildConfig;
import com.thel.demo.live.LiveTheLApp;
import com.thel.demo.live.util.DemoSpUtils;
import com.thel.demo.live.util.DeviceUtils;
import com.thel.utils.GsonUtils;

import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import okio.Buffer;
import okio.BufferedSink;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @author liuyun
 * @date 2017/9/13
 */

public class DefaultRequestService {

    public static final String TAG = "rela_http";

    private final static String POST = "POST";

    private final static String GET = "GET";

    private final static int CONNECT_TIME_OUT = 15 * 1000;

    private static Retrofit.Builder mBuilder;

    private static Retrofit.Builder mStringBuilder;

    private static Retrofit.Builder mTestReportBuilder;

    private static Retrofit.Builder mReportBuilder;

    private static OkHttpClient mClient;

    private static OkHttpClient mReportClient;

    private final static String HEADER_CONTENT_TYPE = "Content-Type";

    private static final String DEFAULT_PARAMS_ENCODING = "UTF-8";

    static {

        String url = BuildConfig.API_URL;

        mBuilder = new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(GsonUtils.getGson()))
                .baseUrl(url);


        //设置代理
//        Proxy proxy = Proxy.NO_PROXY;
//        String proxyStr = ShareFileUtils.getString(ShareFileUtils.FLUTTER_PROXY, "");
//        String[] strings = proxyStr.split(":");
//        if (BuildConfig.DEBUG && ShareFileUtils.getBoolean(ShareFileUtils.FLUTTER_PROXY_OPEN, false) && !proxyStr.isEmpty() && strings.length > 1) {
//            proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(strings[0], Integer.parseInt(strings[1])));
//        }
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                Log.d(TAG, message);
            }
        });
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        mClient = new OkHttpClient.
                Builder().
                retryOnConnectionFailure(true)
                .addInterceptor(loggingInterceptor).
                addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {

                        Request request = chain.request();

                        Response response;

                        if (!request.method().equals(POST)) {

                            HttpUrl.Builder urlBuilder = request.url().newBuilder();

                            urlBuilder.addQueryParameter(RequestConstants.I_KEY, DemoSpUtils.getString(DemoSpUtils.DemoKey.USER_KEY, ""));
                            urlBuilder.addQueryParameter(RequestConstants.I_DEBUG, "0");
                            urlBuilder.addQueryParameter(RequestConstants.I_LAT, "31.245889");
                            urlBuilder.addQueryParameter(RequestConstants.I_LNG, "121.486073");
                            urlBuilder.addQueryParameter(RequestConstants.I_MOBILE_OS, "Android " + Build.VERSION.RELEASE);
                            urlBuilder.addQueryParameter(RequestConstants.I_CLIENT_VERSION, DeviceUtils.getVersionCode(LiveTheLApp.getContext()) + "");
                            urlBuilder.addQueryParameter(RequestConstants.I_LANGUAGE, "zh_CN");
                            urlBuilder.addQueryParameter(RequestConstants.I_DEVICE_ID, DemoSpUtils.getString(DemoSpUtils.DemoKey.DEVICE_ID, UUID.randomUUID().toString()));
//                            urlBuilder.addQueryParameter(RequestConstants.I_APP_TYPE, "universal");
//                            if (ShareFileUtils.isGlobalVersion()) {
//                                urlBuilder.addQueryParameter(RequestConstants.I_APP_TYPE, "global");
//                            }

                            HttpUrl httpUrl = urlBuilder.build();

                            String params = urlBuilder.build().url().getQuery();

                            String decodeParams = Uri.decode(params);

//                            LiveLogger.subLog("DefaultRequestService", " decodeParams :" + decodeParams);
                            Log.d(TAG, " httpUrl decodeParams :" + decodeParams);
                            String signature = MD5Utils.generateSignature(decodeParams);

//                            Log.subLog("DefaultRequestService", " params signature :" + signature);
                            Log.d(TAG, " httpUrl signature :" + signature);
                            Request newRequest = request.newBuilder().url(httpUrl).build();

                            Log.d(TAG, " httpUrl.url :" + newRequest.url());

                            response = chain.proceed(getRequest(newRequest, newRequest.url(), signature));

                        } else {

                            BufferedSink bufferedSink = new Buffer();

                            String signature = "";

                            if (request.body() != null) {

                                Log.d(TAG, " httpUrl.url :" + request.url());

                                request.body().writeTo(bufferedSink);

                                String body = bufferedSink.buffer().readUtf8();

                                String decodeParams = Uri.decode(body);

                                Log.d(TAG, " RequestPost decodeParams :" + decodeParams);

                                signature = MD5Utils.generateSignature(decodeParams);

                            }

                            Request newRequest = getRequest(request, request.url(), signature);

                            response = chain.proceed(newRequest);

                        }

                        String body = response.body().string();

                        Log.d(TAG, " body :" + body);

                        return response.newBuilder().body(ResponseBody.create(MediaType.parse("application/json; charset=utf-8"), body)).build();
                    }
                })//设置超时
                .connectTimeout(CONNECT_TIME_OUT, TimeUnit.MILLISECONDS)
                .readTimeout(CONNECT_TIME_OUT, TimeUnit.MILLISECONDS)
                .writeTimeout(CONNECT_TIME_OUT, TimeUnit.MILLISECONDS)
//                .proxy(proxy)
                .build();

    }

    private static Request getRequest(Request request, HttpUrl httpUrl, String signature) {
        return request.newBuilder()
                .header(HEADER_CONTENT_TYPE, getBodyContentType())
                .header("User-Agent", MD5Utils.getUserAgent())
                .header("Host", BuildConfig.HTTP_HOST)
                .header("Connection", "close")
                .header(RequestConstants.I_SIGNATURE, signature == null ? "" : signature)
                .url(httpUrl).build();
    }

    private DefaultRequestService() {

    }

    public static DemoLiveApi createLiveRequestService() {
        return mBuilder.client(mClient).build().create(DemoLiveApi.class);
    }

    public static Retrofit.Builder getBuilder(String baseUrl) {
        return new Retrofit.Builder().
                addCallAdapterFactory(RxJava2CallAdapterFactory.create()).
                addConverterFactory(GsonConverterFactory.create(GsonUtils.getGson())).
                baseUrl(baseUrl);
    }

    public static Retrofit createUrlRetrofit(String baseUrl) {
        return getBuilder(baseUrl).client(mClient).build();
    }


    public static String getBodyContentType() {
        return "application/x-www-form-urlencoded; charset=" + getParamsEncoding();
    }

    protected static String getParamsEncoding() {
        return DEFAULT_PARAMS_ENCODING;
    }


}
