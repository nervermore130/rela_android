package com.thel.demo.live;

import android.app.Activity;
import android.app.Application;
import android.net.Uri;
import android.text.TextUtils;
import android.widget.ImageView;

import androidx.multidex.MultiDexApplication;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipelineConfig;
import com.thel.demo.live.util.DemoSpUtils;


import java.util.UUID;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

public class LiveApplication extends MultiDexApplication {//implements LiveInitialize.AppInterface {

    @Override
    public void onCreate() {
        super.onCreate();
        LiveTheLApp.onCreate(this);
        initFresco();


//        LiveInitialize.init(this, this);


//        DemoSpUtils.setString(DemoSpUtils.DemoKey.DEVICE_ID, UUID.randomUUID().toString());
        DemoSpUtils.setString(DemoSpUtils.DemoKey.DEVICE_ID, "sm_201911061446235346b30bc4ebd992527f55a987a510af010bc650cae6a192");
//        DemoSpUtils.setString(DemoSpUtils.DemoKey.USER_KEY, "pjvqa836I4gbPPbcimwcqax6ddKcOZrI-104153993");
    }



    private void initFresco() {
        ImagePipelineConfig imagePipelineConfig = ImagePipelineConfig.newBuilder(this).build();
        Fresco.initialize(this, imagePipelineConfig);
    }
    //

//    @Override
//    public Retrofit getRetrofit() {
//        return null;
//    }
//
//    @Override
//    public void loadImageViewUrl(ImageView imageView, String url, int width, int height) {
//        Glide.with(imageView).load(getImageUri(url, width, height)).centerCrop().dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL).into(imageView);
//    }
//
//    @Override
//    public void loadCircleImageViewUrl(ImageView imageView, int defaultImage, String url, int width, int height) {
//        final Uri uri = getImageUri(url, width, height);
//        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
//        Glide.with(imageView).load(uri).placeholder(defaultImage).dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL)
//                .skipMemoryCache(true).into(imageView);//transform(new GlideCircleTransform())
//    }
//
//    @Override
//    public Class<? extends Activity> getWebviewActivity() {
//        return null;
//    }
//
//    @Override
//    public Class<? extends Activity> getUserInfoActivity() {
//        return null;
//    }
//
//    @Override
//    public void doShare() {
//
//    }
//
//    @Override
//    public void toPay() {
//
//    }
//
//    @Override
//    public void toTipOff() {
//
//    }
//
//    @Override
//    public void getBlackList() {
//
//    }
//
//    @Override
//    public String getUserId() {
//        return null;
//    }
//
//    @Override
//    public String getUserToken() {
//        return null;
//    }

    //-----------
    public static Uri getImageUri(String url, int width, int height) {
        final Uri uri = Uri.parse(buildNetPictureUrl(url, width, height));
        return uri;
    }


    /**
     * 利用七牛的云服务剪裁图片尺寸
     *
     * @param url
     * @param width
     * @param height
     * @return
     */
    public static String buildNetPictureUrl(String url, float width, float height) {
        if (TextUtils.isEmpty(url)) {
            return "";
        }
        if (url.contains("file://") || url.contains(".gif") || url.contains(".webp")) {// 本地图片，返回原来的url
            return url;
        }
        try {
            url = url.replaceAll("-wh?\\d+($|\\?)", "$1");
            if (url.contains("?imageView2")) {
                return url.substring(0, url.indexOf("?imageView2")) + "?imageView2/1/w/" + (int) width + "/h/" + (int) height + (url.contains(".gif") ? "" : "/format/webp");// 七牛gif图后面不能加webp
            } else {
                return url + "?imageView2/1/w/" + (int) width + "/h/" + (int) height + (url.contains(".gif") ? "" : "/format/webp");// 七牛gif图后面不能加webp
            }
        } catch (Exception e) {
            return url;
        }
    }

}
