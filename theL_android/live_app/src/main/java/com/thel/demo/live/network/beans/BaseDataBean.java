package com.thel.demo.live.network.beans;

public class BaseDataBean {

    /**
     * 错误标识
     */
    public String errcode = "";

    /**
     * 错误描述
     */
    public String errdesc = "";

    /**
     * 错误描述英文国际化
     */
    public String errdesc_en = "";

    /**
     * 是否成功 (成功为1，失败为0)
     */
    public String result = "";

    public int code;

    public String message;

}
