package com.thel.demo.live;

import android.content.Context;

public class LiveTheLApp {
    public static Context context;

    public static Context getContext() {
        return context;
    }

    public static int enablePubLive = 1;

    public static void onCreate(Context context) {
        LiveTheLApp.context = context;
    }
}
