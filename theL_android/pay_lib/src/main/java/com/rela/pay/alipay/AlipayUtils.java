package com.rela.pay.alipay;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;

import com.alipay.sdk.app.PayTask;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * 支付宝相关工具类
 * Created by setsail on 15/9/9.
 */
public class AlipayUtils {

    private static AlipayUtils alipayUtils;

    // 商户PID
    private final String PARTNER = "2088021522675684";
    // 商户收款账号
    private final String SELLER = "anson@thel.co";
    // 商户私钥，pkcs8格式
    private final String RSA_PRIVATE = "MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBANDLq8LxrDdiZM+geIt/p2Y6yuw/9+QbtqtbciEbWI2ji69P9cuUg5U/yxot/DM9xvqK+uw9mEESK4YHnuVZlcuAZ3rTsVSm2aRNRTbxfyl53KFgsMfJpoqj5RHRdaIH5wXFgVZqW7QviPVj15tNuAS9v1X6Pjeo5OoCYIsc3JbTAgMBAAECgYBUBOFFPQLT9qAifKyFVgUCuj3V+5fBDvXe1pmCgQKNXHZlQYPjPAwHDvinvEF0TTO4thxq3A5cgPdKo95o5DfGRb8SPSFOinSYP6IWnu9OKh9lCG0aNONsWi7bMztpxrkTsh2IG1dLBQM90/4Sf2ILxHPUELE5FRXzLKpwAlXGAQJBAPOPfZb8W6il9jptsKmtOdP2sah8xRAPyukjjXmyfo19yIzt7VDVnvqZlz5/HuMBBF8c7xvNvdUH8kHyFVcGAsECQQDbdaKloKEgB8biFrdjrj3V/gTEQh5t+Q1VTaM7AN30EtweCy03gXBzvQ554KKmPAm99rHXv42vh07vtap+w4KTAkA8Wa7F7cwMcusbOTjQImM02WjWFeyCeVdPA4c3w3nGN4etG/t0zynwcIRgcQFHQGZD5xh8bDOAiNKHcqPFB3FBAkApZwwES0WakV0IElkOyTSJRp8Hl6G+BZpqWXdODl/RSC2WzWz+Z8EbVLOCcsq8qd78o6nxlgXY86IEvzUnLSNZAkAJgrDjNqdonutCGt6FONiZywMNhBGvr9Tr9YEM0hnq+Z+MxR56dQEDk6P9K8Ggp44wMjeVn7UbG0DJEX0k8kJd";

    public static final int SDK_PAY_FLAG = 1;

    private AlipayUtils() {
    }

    public static AlipayUtils getInstance() {
        if (alipayUtils == null) {
            alipayUtils = new AlipayUtils();
        }
        return alipayUtils;
    }

    /**
     * create the order info. 创建订单信息
     */
    public String getOrderInfo(String outTradeNo, String subject, String description, double total_fee, String alipayNotifyUrl, String expire) {

        // 签约合作者身份ID
        String orderInfo = "partner=" + "\"" + PARTNER + "\"";

        // 签约卖家支付宝账号
        orderInfo += "&seller_id=" + "\"" + SELLER + "\"";

        // 商户网站唯一订单号
        orderInfo += "&out_trade_no=" + "\"" + outTradeNo + "\"";

        // 商品名称
        orderInfo += "&subject=" + "\"" + subject + "\"";

        // 商品详情
        orderInfo += "&body=" + "\"" + description + "\"";

        // 商品金额
        orderInfo += "&total_fee=" + "\"" + total_fee + "\"";

        // 服务器异步通知页面路径
        orderInfo += "&notify_url=" + "\"" + alipayNotifyUrl + "\"";

        // 服务接口名称， 固定值
        orderInfo += "&service=\"mobile.securitypay.pay\"";

        // 支付类型， 固定值
        orderInfo += "&payment_type=\"1\"";

        // 参数编码， 固定值
        orderInfo += "&_input_charset=\"utf-8\"";

        // 设置未付款交易的超时时间
        // 默认30分钟，一旦超时，该笔交易就会自动被关闭。
        // 取值范围：1m～15d。
        // m-分钟，h-小时，d-天，1c-当天（无论交易何时创建，都在0点关闭）。
        // 该参数数值不接受小数点，如1.5h，可转换为90m。
        orderInfo += "&it_b_pay=\"" + expire + "\"";

        // extern_token为经过快登授权获取到的alipay_open_id,带上此参数用户将使用授权的账户进行支付
        // orderInfo += "&extern_token=" + "\"" + extern_token + "\"";

        // 支付宝处理完请求后，当前页面跳转到商户指定页面的路径，可空
        //        orderInfo += "&return_url=\"m.alipay.com\"";

        // 调用银行卡支付，需配置此参数，参与签名， 固定值 （需要签约《无线银行卡快捷支付》才能使用）
        // orderInfo += "&paymethod=\"expressGateway\"";

        return orderInfo;
    }

    /**
     * call alipay sdk pay. 调用SDK支付
     */
    public void pay(final Activity context, String orderInfo, final Handler mHandler) {

        // 对订单做RSA 签名
        String sign = sign(orderInfo);
        try {
            // 仅需对sign 做URL编码
            sign = URLEncoder.encode(sign, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        // 完整的符合支付宝参数规范的订单信息
        final String payInfo = orderInfo + "&sign=\"" + sign + "\"&" + getSignType();

        Runnable payRunnable = new Runnable() {

            @Override
            public void run() {
                // 构造PayTask 对象
                PayTask alipay = new PayTask(context);
                // 调用支付接口，获取支付结果
                String result = alipay.pay(payInfo, true);

                Message msg = new Message();
                msg.what = SDK_PAY_FLAG;
                msg.obj = result;
                mHandler.sendMessage(msg);
            }
        };

        // 必须异步调用
        Thread payThread = new Thread(payRunnable);
        payThread.start();
    }

    /**
     * sign the order info. 对订单信息进行签名
     *
     * @param content 待签名订单信息
     */
    public String sign(String content) {
        return MD5.sign(content, RSA_PRIVATE);
    }

    /**
     * get the sign logType we use. 获取签名方式
     */
    public String getSignType() {
        return "sign_type=\"RSA\"";
    }
}

