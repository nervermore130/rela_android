package com.rela.pay;

public class PayConstants {

    public final static int PAY_STATUS_SUCCESS = 1;

    public final static int PAY_STATUS_FAILED = 0;

    public static final String PAY_TYPE_ALIPAY = "alipay";

    public static final String PAY_TYPE_WXPAY = "wxpay";

    public static final String ALIPAY_SOURCE = "android-alipay";

    public static final String WXPAY_SOURCE = "android-wx";

}
