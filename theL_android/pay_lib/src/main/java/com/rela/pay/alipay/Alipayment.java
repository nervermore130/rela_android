package com.rela.pay.alipay;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.rela.pay.IPayment;
import com.rela.pay.OnPayStatusListener;
import com.rela.pay.PayConstants;
import com.rela.pay.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;

public class Alipayment implements IPayment {

    private static final String TAG = "Alipayment";

    private OnPayStatusListener mOnPayStatusListener;

    private WeakReference<Activity> activityWeakReference;

    @Override
    public void pay(Activity activity, String result, OnPayStatusListener mOnPayStatusListener) {

        Log.d(TAG, " result : " + result);

        activityWeakReference = new WeakReference<>(activity);

        this.mOnPayStatusListener = mOnPayStatusListener;

        try {
            JSONObject resultJson = new JSONObject(result);

            String outTradeNo = resultJson.getString("outTradeNo");

            String subject = resultJson.getString("subject");

            String description = resultJson.getString("description");

            double totalFee = resultJson.getDouble("totalFee");

            String alipayNotifyUrl = resultJson.getString("alipayNotifyUrl");

            String expire = resultJson.getString("expire");

            AlipayUtils.getInstance().pay(activityWeakReference.get(), AlipayUtils.getInstance().getOrderInfo(outTradeNo, subject, description, totalFee, alipayNotifyUrl, expire), mHandler);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override public void destroy() {

    }

    private Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case AlipayUtils.SDK_PAY_FLAG: {

                    PayResult payResult = new PayResult((String) msg.obj);

                    Log.d("AliPayment", " payResult getMemo : " + payResult.getMemo());

                    Log.d("AliPayment", " payResult getResult : " + payResult.getResult());

                    Log.d("AliPayment", " payResult getResultStatus : " + payResult.getResultStatus());

                    String resultStatus = payResult.getResultStatus();

                    // 判断resultStatus 为“9000”则代表支付成功，具体状态码代表含义可参考接口文档
                    if (TextUtils.equals(resultStatus, "9000")) {
                        if (mOnPayStatusListener != null) {
                            mOnPayStatusListener.onPayStatus(PayConstants.PAY_STATUS_SUCCESS, null);
                        }
                    } else {
                        // 判断resultStatus 为非“9000”则代表可能支付失败
                        // “8000”代表支付结果因为支付渠道原因或者系统原因还在等待支付结果确认，最终交易是否成功以服务端异步通知为准（小概率状态）

                        if (TextUtils.equals(resultStatus, "8000")) {
                            if (mOnPayStatusListener != null) {
                                mOnPayStatusListener.onPayStatus(PayConstants.PAY_STATUS_FAILED, "8000");
                            }

                        } else {
                            // 其他值就可以判断为支付失败，包括用户主动取消支付，或者系统返回的错误
                            if (TextUtils.equals(resultStatus, "6001")) {
                                Toast.makeText(activityWeakReference.get(), R.string.wx_pay_canceled, Toast.LENGTH_LONG).show();
                                if (mOnPayStatusListener != null) {
                                    mOnPayStatusListener.onPayStatus(PayConstants.PAY_STATUS_FAILED, "6001");
                                }

                            } else {
                                Toast.makeText(activityWeakReference.get(), R.string.wx_pay_failed, Toast.LENGTH_LONG).show();

                                if (mOnPayStatusListener != null) {
                                    mOnPayStatusListener.onPayStatus(PayConstants.PAY_STATUS_FAILED, "0");
                                }

                            }
                        }
                    }
                    break;
                }
                default:
                    break;
            }
            return false;
        }
    });

}
