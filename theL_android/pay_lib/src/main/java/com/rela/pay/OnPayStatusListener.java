package com.rela.pay;

public interface OnPayStatusListener {

    /**
     * @param payStatus 1成功 0失败
     * @param errorInfo 失败信息
     */
    void onPayStatus(int payStatus, String errorInfo);

}
