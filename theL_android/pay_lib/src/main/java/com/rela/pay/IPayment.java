package com.rela.pay;

import android.app.Activity;

public interface IPayment {

    void pay(Activity activity, String result, OnPayStatusListener mOnPayStatusListener);

    void destroy();

}
