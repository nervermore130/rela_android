package com.rela.pay;

import android.app.Activity;

import com.rela.pay.alipay.Alipayment;
import com.rela.pay.wxpay.WXPayment;

public class PayProxyImpl implements IPayProxy {

    private final static PayProxyImpl INSTANCE = new PayProxyImpl();

    private IPayment payment;

    public OnPayStatusListener mOnPayStatusListener;

    private PayProxyImpl() {

    }

    public static PayProxyImpl getInstance() {
        return INSTANCE;
    }


    @Override
    public void init() {

    }

    @Override
    public void pay(String payType, Activity activity, String result, OnPayStatusListener mOnPayStatusListener) {
        this.mOnPayStatusListener = mOnPayStatusListener;

        switch (payType) {
            case PayConstants.PAY_TYPE_ALIPAY:
                payment = new Alipayment();
                payment.pay(activity, result, mOnPayStatusListener);
                break;
            case PayConstants.PAY_TYPE_WXPAY:
                payment = new WXPayment();
                payment.pay(activity, result, mOnPayStatusListener);
                break;
            default:
                break;
        }

    }

    @Override public void destroy() {
        if (payment != null) {
            payment.destroy();
        }
    }
}
