package com.rela.pay.wxpay;

import android.content.Context;
import android.widget.Toast;

import com.rela.pay.alipay.MD5;
import com.tencent.mm.opensdk.constants.Build;
import com.tencent.mm.opensdk.modelpay.PayReq;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 微信支付相关工具类
 * Created by setsail on 15/9/9.
 */
public class WXPayUtils {

    private static final String WX_APP_ID_PAY = "wxe039e5ac00100e48";

    private static final String WX_API_KEY = "FC0a2gJjCInS40KRgHZ8JWkHYbtEjRQr";

    private static WXPayUtils wxPayUtils;

    private IWXAPI msgApi;
    /**
     * 微信支付请求来源页面，value通常为类名字
     */
    public static final String WXPAY_TAG_KEY = "WXPay_TAG_KEY";//key

    public static String WXPAY_TAG_VALUE = "";//value

    private WXPayUtils() {
    }

    public static WXPayUtils getInstance() {
        if (wxPayUtils == null) {
            wxPayUtils = new WXPayUtils();
        }
        return wxPayUtils;
    }

    /**
     * 调用微信SDK支付
     *
     * @param result
     */
    public void pay(String result, Context context) {

        if (msgApi == null) {
            msgApi = WXAPIFactory.createWXAPI(context, null);
        }

        msgApi.registerApp(WX_APP_ID_PAY);
        //没安装微信
        if (!msgApi.isWXAppInstalled()) {
            Toast.makeText(context, "请先安装微信", Toast.LENGTH_LONG).show();
            return;
            //当前微信版本低于支持微信支付的版本
        } else if (msgApi.getWXAppSupportAPI() < Build.PAY_SUPPORTED_SDK_INT) {
            Toast.makeText(context, "你的微信版本过低，不支持微信支付", Toast.LENGTH_LONG).show();
            return;
        }

        PayReq payReq = genPayReq(result);

        if (payReq != null) {

            msgApi.sendReq(payReq);
        }
    }

    private PayReq genPayReq(String result) {

        JSONObject jsonObject;

        PayReq req = new PayReq();

        try {
            jsonObject = new JSONObject(result);

            JSONObject wxpayResult = jsonObject.getJSONObject("wxOrder");

            String mch_id = wxpayResult.getString("mch_id");

            String prepay_id = wxpayResult.getString("prepay_id");

            String nonce_str = wxpayResult.getString("nonce_str");


            req.appId = WX_APP_ID_PAY;
            req.partnerId = mch_id;
            req.prepayId = prepay_id;
            req.packageValue = "Sign=WXPay";
            req.nonceStr = nonce_str;
            req.timeStamp = String.valueOf(genTimeStamp());

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

        List<WXModel> signParams = new ArrayList<>();

        signParams.add(new WXModel("appid", req.appId));
        signParams.add(new WXModel("noncestr", req.nonceStr));
        signParams.add(new WXModel("package", req.packageValue));
        signParams.add(new WXModel("partnerid", req.partnerId));
        signParams.add(new WXModel("prepayid", req.prepayId));
        signParams.add(new WXModel("timestamp", req.timeStamp));

        req.sign = genAppSign(signParams);

        return req;
    }

    private String genAppSign(List<WXModel> signParams) {

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < signParams.size(); i++) {
            sb.append(signParams.get(i).key);
            sb.append('=');
            sb.append(signParams.get(i).value);
            sb.append('&');

        }

        sb.append("key=");

        sb.append(WX_API_KEY);

        return MD5.getMessageDigest(sb.toString().getBytes()).toUpperCase();
    }

    /**
     * 微信支付请求来源页面
     *
     * @param tag 来源界面的标签
     * @return
     */
    public WXPayUtils setTag(String tag) {
        WXPAY_TAG_VALUE = tag;
        return getInstance();
    }

    private long genTimeStamp() {
        return System.currentTimeMillis() / 1000;
    }
}
