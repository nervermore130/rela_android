package com.rela.pay.wxpay;

import android.app.Activity;

import com.rela.pay.IPayment;

import com.rela.pay.OnPayStatusListener;

import java.lang.ref.WeakReference;

public class WXPayment implements IPayment {

    private WeakReference<Activity> activity;

    private OnPayStatusListener mOnPayStatusListener;

    @Override
    public void pay(Activity activity, String result, OnPayStatusListener mOnPayStatusListener) {

        if (activity != null && (activity.getClass().getSimpleName().contains("StickerPackDetailActivity")
                || activity.getClass().getSimpleName().contains("StickerStoreActivity"))) {

            WXPayUtils.WXPAY_TAG_VALUE = activity.getClass().getSimpleName();

        }


        this.activity = new WeakReference<>(activity);

        this.mOnPayStatusListener = mOnPayStatusListener;

        WXPayUtils.getInstance().pay(result, activity);
    }

    @Override
    public void destroy() {

    }




}

