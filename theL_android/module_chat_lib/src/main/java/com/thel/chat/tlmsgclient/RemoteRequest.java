package com.thel.chat.tlmsgclient;

import java.io.IOException;

/**
 * Created by gl on 15/11/20.
 */
public class RemoteRequest {
    AbstractMsgClient client;
    MsgPacket packet;

    protected RemoteRequest(AbstractMsgClient msgClient, MsgPacket msgPacket) {
        this.client = msgClient;
        this.packet = msgPacket;
    }

    public MsgPacket getPacket() {
        return packet;
    }

    public String getCode() {
        return getPacket().getCode();
    }

    public String getPayload() {
        return new String(getPacket().getPayload());
    }

    /**
     * 回复消息给远端。
     *
     * @param code
     * @param payload
     * @throws IOException
     */
    public void response(String code, String payload) throws IOException {
        MsgPacket msgPacket = new MsgPacket();
        msgPacket.setCode(code);
        msgPacket.setIsResponse(true);
        msgPacket.setSeq(packet.getSeq());
        msgPacket.setPayload(payload.getBytes());
        client.sendPacket(msgPacket);
    }
}
