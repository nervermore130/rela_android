package com.thel.chat.live.interfaces

interface ITencentLiveChat : ILiveChat {


    /**
     * 删除聊天成员
     * 主播
     */
    fun deleteGroupMember()

    /**
     * 解散聊天
     */
    fun deleteGroup()

    /**
     * 退出聊天
     * 观众
     */
    fun quitChatRoom()
}