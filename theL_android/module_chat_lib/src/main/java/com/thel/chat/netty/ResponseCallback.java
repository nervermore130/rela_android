package com.thel.chat.netty;

public interface ResponseCallback {

    void onError(Throwable throwable);

    void onResponse(MsgPacket msgPacket);
}
