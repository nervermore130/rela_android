package com.thel.chat.tlmsgclient;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * Created by gl on 16/1/27.
 */
public class SocketMsgClient extends AbstractMsgClient implements Runnable {
    public static final int CLOSED=0;
    public static final int CONNECTING=1;
    public static final int CONNECTED=2;

    private int state;
    private Socket socket;
    private String host;
    private int port;
    private InputStream inputStream;
    private OutputStream outputStream;

    public SocketMsgClient(String host, int port) throws IOException {
        this.host = host;
        this.port = port;
    }

    @Override
    public void connect() throws IOException {
        synchronized (this) {
            if(state==CLOSED) {
                state = CONNECTING;
                new Thread(this).start();
            }
        }
    }

    @Override
    public void run() {
        try {
            socket = new Socket(host, port);
            inputStream = socket.getInputStream();
            outputStream = socket.getOutputStream();
            state = CONNECTED;
            msgListener.onConnect(this);
            inputStream = socket.getInputStream();
            outputStream = socket.getOutputStream();
            while (true) {
                MsgPacket packet = readPacket();
                if (packet == null) {
                    close();
                    break;
                }
                onReceivePacket(packet);
            }
        } catch (IOException e) {
            msgListener.onError(e);
            try {
                close();
            } catch (IOException e1) {
            }
        }
    }

    @Override
    public void close() throws IOException {
        synchronized (this) {
            state = CLOSED;
            if (socket != null) {
                try{
                    msgListener.onClosed(this);
                    socket.close();
                }catch (Exception e){
                }
                socket = null;
                inputStream = null;
                outputStream = null;
            }
        }
    }

    @Override
    public boolean isConnected() {
        return state == CONNECTED;
    }

    private MsgPacket readPacket() throws IOException {
        byte[] lenBuff = new byte[2];
        inputStream.read(lenBuff);
        int len = Utils.getShort(lenBuff);
        byte[] buff = new byte[len];
        inputStream.read(buff);
        MsgPacket msgPacket = new MsgPacket();
        msgPacket.fromData(buff, len);
        return msgPacket;
    }

    @Override
    protected void sendBytePacket(byte[] data) throws IOException {
        int len = data.length;
        outputStream.write(Utils.shortToByteArray(len));
        outputStream.write(data);
        outputStream.flush();
    }
}
