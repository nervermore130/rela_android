package com.thel.chat.netty;

public class NettyConstants {

    /**
     * 解码时，处理每个帧数据的最大长度
     */
    public static final int MAX_FRAME_LENGTH = 1024 * 1024;

    /**
     * 该帧数据中，存放该帧数据的长度的数据的起始位置
     */
    public static final int LENGTH_FIELD_OFFSET = 0;

    /**
     * 记录该帧数据长度的字段本身的长度
     */
    public static final int LENGTH_FIELD_LENGTH = 2;

    /**
     * 修改帧数据长度字段中定义的值，可以为负数
     */
    public static final int LENGTH_ADJUSTMENT = 0;

    /**
     * 解析的时候需要跳过的字节数
     */
    public static final int INITIAL_BYTES_TO_STRIP = 0;

    /**
     * 为true，当frame长度超过maxFrameLength时立即报TooLongFrameException异常，为false，读取完整个帧再报异常
     */
    public static final boolean FAIL_FAST = false;

    public static final int MSG_BUFFER_SIZE = 65535;

    public static class ConnectConstants {

        /**
         * 连接聊天成功
         */
        public final static int STATUS_CONNECT_SUCCESS = 1;

        /**
         * 连接断开
         */
        public final static int STATUS_CONNECT_CLOSED = 0;

        /**
         * 连接出错
         */
        public final static int STATUS_CONNECT_ERROR = -1;

    }

}
