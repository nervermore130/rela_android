package com.thel.chat.live

import com.thel.chat.live.interfaces.ILiveChat
import com.thel.chat.live.tencent.TencentChatImpl
import com.thel.chat.live.tlmsg.TLMsgChatImpl

class LiveChatClientImpl{

    companion object{

        fun createChatRoom(liveChatBuilder: LiveChatBuilder): ILiveChat {
            return if (liveChatBuilder.type == 0) {
                TLMsgChatImpl(liveChatBuilder)
            } else {
                TencentChatImpl(liveChatBuilder)
            }
        }
    }


}