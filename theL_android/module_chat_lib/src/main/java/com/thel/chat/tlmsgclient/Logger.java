package com.thel.chat.tlmsgclient;

/**
 * Created by gl on 15/12/22.
 */
public interface Logger {
    void debug(String message);
    void info(String message);
    void warn(String message);
    void error(String message);
}