package com.thel.chat.tlmsgclient;

/**
 * Created by gl on 15/11/20.
 */
public interface MsgListener {
    /**
     * 当连接建立时
     * @param client
     */
    void onConnect(IMsgClient client);

    /**
     * 当收到远程的请求时
     * @param request
     */
    void onRemoteRequest(RemoteRequest request);

    /**
     * 当连接断开时
     * @param client
     */
    void onClosed(IMsgClient client);

    /**
     * 当连接发生异常时
     * @param ex
     */
    void onError(Exception ex);
}
