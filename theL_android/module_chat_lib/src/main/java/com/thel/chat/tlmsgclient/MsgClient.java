package com.thel.chat.tlmsgclient;

import android.text.TextUtils;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by setsail on 15/11/10.
 */
public class MsgClient extends AbstractMsgClient implements Runnable {

    // NIO 相关

    private String host;
    private int port;

    /**
     * 连接通道
     */
    private SocketChannel mSocketChannel;

    /**
     * 接收缓冲区
     */
    private final ByteBuffer mReceiveBuf;

    private final ByteBuffer mMsgBuff;

    /**
     * 端口选择器
     */
    private Selector mSelector;

    /**
     * 线程是否结束的标志
     */
    private final AtomicBoolean mShutdown;

    private int remainLength;

    private final ArrayList<Byte> msgLengthArr = new ArrayList<>();

    public MsgClient(String host, int port) throws IOException {
        this.host = host;
        this.port = port;

        // 初始化缓冲区
        mReceiveBuf = ByteBuffer.allocateDirect(Constants.PACKET_BUFFER_SIZE);
        mMsgBuff = ByteBuffer.allocate(Constants.MSG_BUFFER_SIZE);
        mShutdown = new AtomicBoolean(false);
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    @Override
    public void run() {
        try {
            if (mSocketChannel == null)
                mSocketChannel = SocketChannel.open();
            // 绑定到本地端口
            mSocketChannel.configureBlocking(false);
            if (mSocketChannel.connect(new InetSocketAddress(getHttpDNS(host), port))) {
                logger.info("开始建立连接");
                //                Vlog.writeLogToFile("开始建立连接：" + System.currentTimeMillis());
            }
            if (mSelector == null) {
                // 创建新的Selector
                mSelector = Selector.open();
            }
            mSocketChannel.register(mSelector, SelectionKey.OP_CONNECT | SelectionKey.OP_READ, this);
            if (mShutdown != null)
                mShutdown.set(false);
        } catch (Exception e) {
            e.printStackTrace();
            shutdown();
            msgListener.onError(e);
            return;
        }
        // 启动主循环流程
        while (!mShutdown.get()) {
            try {
                // do select
                select();
                try {
                    Thread.sleep(1000);
                } catch (final Exception e) {
                    e.printStackTrace();
                }
            } catch (final Exception e) {
                e.printStackTrace();
            }
        }
        shutdown();
    }

    private ExecutorService connectEcutorService = Executors.newSingleThreadExecutor();

    /**
     * 打开通道
     */
    public void connect() {
        if (isConnected())
            return;
        connectEcutorService.execute(this);
    }

    private void onConnected() throws IOException {
        if (!mSocketChannel.isConnectionPending()) {
            return;
        }
        logger.info("端口打开成功");
        // 完成SocketChannel的连接
        mSocketChannel.finishConnect();
        while (!mSocketChannel.isConnected()) {
            try {
                Thread.sleep(300);
            } catch (final InterruptedException e) {
                e.printStackTrace();
            }
            mSocketChannel.finishConnect();
        }
        msgListener.onConnect(this);
    }

    private void shutdown() {
        if (isConnected()) {
            try {
                mSocketChannel.close();
                while (mSocketChannel.isOpen()) {
                    try {
                        Thread.sleep(300);
                    } catch (final InterruptedException e) {
                        e.printStackTrace();
                    }
                    mSocketChannel.close();
                }
                logger.info("端口关闭成功");
            } catch (final IOException e) {
                logger.error("端口关闭错误:");
                e.printStackTrace();
            } finally {
                mSocketChannel = null;
            }
        } else {
            mSocketChannel = null;
        }
        // 关闭端口选择器
        if (mSelector != null) {
            try {
                mSelector.close();
                logger.info("端口选择器关闭成功");
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                mSelector = null;
            }
        }
    }

    private void select() {
        int nums = 0;
        try {
            if (mSelector == null) {
                return;
            }
            nums = mSelector.select(1000);
        } catch (final Exception e) {
            e.printStackTrace();
        }

        // 如果select返回大于0，处理事件
        if (nums > 0) {
            Iterator<SelectionKey> iterator = mSelector.selectedKeys().iterator();
            while (iterator.hasNext()) {
                // 得到下一个Key
                final SelectionKey key = iterator.next();
                iterator.remove();
                // 检查其是否还有效
                if (!key.isValid()) {
                    continue;
                }
                // 处理事件
                try {
                    if (key.isConnectable()) {
                        onConnected();
                    } else if (key.isReadable()) {
                        receive();
                    }
                } catch (final Exception e) {
                    e.printStackTrace();
                    key.cancel();
                    onDisConnect();
                }
            }
        }
    }

    private void receive() throws IOException {
        if (isConnected()) {
            int len = 0;
            int readBytes = 0;

            synchronized (mReceiveBuf) {
                mReceiveBuf.clear();
                try {
                    while ((len = mSocketChannel.read(mReceiveBuf)) > 0) {
                        readBytes += len;
                        if (!mReceiveBuf.hasRemaining())
                            break;
                    }
                } finally {
                    mReceiveBuf.flip();
                }
                if (readBytes > 0) {
                    final byte[] tmp = new byte[readBytes];
                    mReceiveBuf.get(tmp);
                    int offset = 0;
                    while (offset < tmp.length) {
                        if (remainLength > 0) {// 如果上一条包还没有读完整
                            if (remainLength > readBytes - offset) {
                                mMsgBuff.put(tmp, offset, readBytes - offset);
                                remainLength -= (readBytes - offset);
                                offset = readBytes;
                            } else {// 消息读取完毕
                                logger.info("一条消息读取完毕");
                                mMsgBuff.put(tmp, offset, remainLength);
                                offset += remainLength;
                                remainLength = 0;
                                msgLengthArr.clear();
                                int msgLen = mMsgBuff.position();
                                mMsgBuff.flip();
                                MsgPacket msgPacket = new MsgPacket();
                                msgPacket.fromData(mMsgBuff, msgLen);
                                mMsgBuff.clear();
                                this.onReceivePacket(msgPacket);
                            }
                        } else {
                            int lenRemain = 2 - msgLengthArr.size();
                            if (lenRemain > 0) {// 如果一个包的头两个字节都没有读取完
                                int lenTmp;
                                if (lenRemain <= readBytes - offset) {
                                    lenTmp = lenRemain;
                                } else {
                                    lenTmp = readBytes - offset;
                                }
                                for (int i = 0; i < lenTmp; i++) {
                                    msgLengthArr.add(tmp[offset]);
                                    offset += 1;
                                }
                            } else {// 如果头两个字节的读取完毕，则开始根据这两个字节所存储的长度来读取消息
                                byte[] lenArr = new byte[2];
                                lenArr[0] = msgLengthArr.get(0);
                                lenArr[1] = msgLengthArr.get(1);
                                remainLength = Utils.getShort(lenArr);
                            }
                        }
                    }
                    return;
                } else {
                    logger.error("接收到数据为空,重新启动连接:" + readBytes);
                    shutdown();
                    //                    run();
                    if (msgListener != null)
                        msgListener.onClosed(this);
                    return;
                }
            }
        } else {
            logger.error("端口没有连接");
        }
    }

    public boolean isConnected() {
        return mSocketChannel != null && mSocketChannel.isConnected();
    }

    private void onDisConnect() {
        logger.error("断开连接");
        mShutdown.set(true);
    }

    public void close() throws IOException {
        mShutdown.set(true);
        if (mSocketChannel != null)
            mSocketChannel.close();
    }

    @Override
    protected void sendBytePacket(byte[] data) throws IOException {
        int len = data.length;
        final ByteBuffer sendByteBuffer = ByteBuffer.allocateDirect(len + 2);
        sendByteBuffer.put(Utils.shortToByteArray(len));
        sendByteBuffer.put(data, 0, data.length);
        sendByteBuffer.flip();
        if (mSocketChannel != null) {//4.0.2容错，修复
            mSocketChannel.write(sendByteBuffer);
        }
    }

    private String getHttpDNS(String host) {
        final DefaultHttpClient client = new DefaultHttpClient();
        try {
            HttpGet getRequest = new HttpGet("http://119.29.29.29/d?ttl=1&dn=" + host);
            HttpResponse response = client.execute(getRequest);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                String result = EntityUtils.toString(response.getEntity());
                if (!TextUtils.isEmpty(result)) {
                    String[] arr = result.split(",");
                    if (arr != null && arr.length == 2) {
                        return arr[0];
                    }
                } else {
                    // 如果请求httpdns失败
                    return host;
                }
            } else {
                // 如果请求httpdns失败
                return host;
            }
        } catch (Exception ex) {
            // 如果请求httpdns失败
            if (ex != null)
                ex.printStackTrace();
            return host;
        }

        return host;
    }
}
