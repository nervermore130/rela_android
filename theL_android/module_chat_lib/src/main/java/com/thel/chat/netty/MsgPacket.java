package com.thel.chat.netty;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @author liuyun
 */
public class MsgPacket implements Parcelable {

    public boolean isResponse;

    public String code;

    public int seq;

    public byte[] payload;


    @Override public int describeContents() {
        return 0;
    }

    @Override public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.isResponse ? (byte) 1 : (byte) 0);
        dest.writeString(this.code);
        dest.writeInt(this.seq);
        dest.writeByteArray(this.payload);
    }

    public MsgPacket() {
    }

    protected MsgPacket(Parcel in) {
        this.isResponse = in.readByte() != 0;
        this.code = in.readString();
        this.seq = in.readInt();
        this.payload = in.createByteArray();
    }

    public static final Parcelable.Creator<MsgPacket> CREATOR = new Parcelable.Creator<MsgPacket>() {
        @Override public MsgPacket createFromParcel(Parcel source) {
            return new MsgPacket(source);
        }

        @Override public MsgPacket[] newArray(int size) {
            return new MsgPacket[size];
        }
    };
}
