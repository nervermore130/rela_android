package com.thel.chat.live.interfaces

import com.thel.chat.live.LiveChatBuilder

interface IChatClient {

    fun createChatRoom(liveChatBuilder: LiveChatBuilder): ILiveChat

}