package com.thel.chat.tlmsgclient;

import android.os.Environment;

import java.io.FileWriter;

/**
 * 日志打印类
 *
 * @version 1.0.0
 */
public class Vlog {

    public static void writeLogToFile(String str) {
        try {
            FileWriter fw = new FileWriter(Environment.getExternalStorageDirectory().getPath() + "/thel/log.txt", true);
            fw.flush();
            fw.write(str + "\n");
            fw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
