package com.thel.chat.tlmsgclient;

/**
 * Created by setsail on 15/11/10.
 */
public class Utils {

    /**
     * 通过byte数组取到short
     *
     * @param b
     * @return
     */
    public static int getShort(byte[] b) {
        return ((b[0] & 0xff) << 8) | b[1] & 0xff;
    }

    /**
     * 将16位的short转换成byte数组
     *
     * @param s short
     * @return byte[] 长度为2
     */
    public static byte[] shortToByteArray(int s) {
        byte[] targets = new byte[2];
        for (int i = 0; i < 2; i++) {
            int offset = (targets.length - 1 - i) * 8;
            targets[i] = (byte) ((s >>> offset) & 0xff);
        }
        return targets;
    }
}