package com.thel.chat.netty;

/**
 * @author liuyun
 */
public interface NettyListener {

    /**
     * 当接收到系统消息
     *
     * @param msgPacket
     */
    void onMessageResponse(MsgPacket msgPacket);

    /**
     * 当服务状态发生变化时触发
     *
     * @param statusCode
     */
    void onServiceStatusConnectChanged(int statusCode);
}
