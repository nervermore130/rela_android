package com.thel.chat.tlmsgclient;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Created by gl on 15/11/20.
 */
public class Request implements ResponseCallback {
    private CountDownLatch loginLatch;
    private AbstractMsgClient client;
    private String code;
    private String payload;

    private boolean responsed;
    private MsgPacket requestPacket;
    private MsgPacket responsePacket;
    private long timeout;
    private ResponseCallback responseCallback;

    protected Request(AbstractMsgClient msgClient, String code, String payload, long timeout) {
        this.client = msgClient;
        this.code = code;
        this.payload = payload;
        this.timeout = timeout;
        this.requestPacket = new MsgPacket();
        this.requestPacket.setCode(code);
        this.requestPacket.setPayload(payload.getBytes());
        this.requestPacket.setIsResponse(false);
    }

    /**
     * 同步请求
     * @return
     * @throws IOException
     */
    public MsgPacket execute() throws IOException{
        loginLatch = new CountDownLatch (1);
        try {
            send();
            loginLatch.await(timeout, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return responsePacket;
    }

    /**
     * 异步请求
     * @param callback
     */
    public void enqueue(ResponseCallback callback) {
        responseCallback = callback;
        try {
            send();
        } catch (IOException e) {
            onError(e);
        }
    }

    private void send() throws IOException {
        client.sendPacket(requestPacket);
        client.setResponseCallback(requestPacket.getSeq(), timeout, this);
    }

    @Override
    public void onError(Throwable e) {
        if(responseCallback != null) {
            responseCallback.onError(e);
        }
    }

    @Override
    public void onResponse(MsgPacket packet) {
        responsePacket = packet;
        responsed = true;
        if(responseCallback != null) {
            responseCallback.onResponse(packet);
        }
        if(loginLatch != null && loginLatch.getCount()==1) {
            loginLatch.countDown();
        }
    }
}
