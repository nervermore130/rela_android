package com.thel.chat.netty;

import java.nio.ByteBuffer;
import java.util.ArrayList;

import io.netty.buffer.ByteBuf;

/**
 * @author liuyun
 */
public class ByteUtils {

    public static byte[] toData(MsgPacket msgPacket) {

        ArrayList<Byte> bytes = new ArrayList<>();

        int _seq = msgPacket.seq;

        if (msgPacket.isResponse) {
            _seq |= 0x8000;
        }

        for (byte b : shortToByteArray(_seq)) {
            bytes.add(b);
        }

        for (byte b : msgPacket.code.getBytes()) {
            bytes.add(b);
        }

        bytes.add(zeroData());

        for (byte b : msgPacket.payload) {

            bytes.add(b);

        }

        byte[] result = new byte[bytes.size()];

        for (int i = 0; i < bytes.size(); i++) {
            result[i] = bytes.get(i);
        }

        int len = result.length;

        byte[] lengthArrays = ByteUtils.shortToByteArray(len);

        return byteMerger(lengthArrays, result);
    }

    public static byte[] byteMerger(byte[] lengthArrays, byte[] bodyArrays) {
        byte[] resultArrays = new byte[lengthArrays.length + bodyArrays.length];
        System.arraycopy(lengthArrays, 0, resultArrays, 0, lengthArrays.length);
        System.arraycopy(bodyArrays, 0, resultArrays, lengthArrays.length, bodyArrays.length);
        return resultArrays;
    }

    public static byte zeroData() {
        return Byte.valueOf("0");
    }

    /**
     * 将16位的short转换成byte数组
     *
     * @param s short
     * @return byte[] 长度为2
     */
    public static byte[] shortToByteArray(int s) {
        byte[] targets = new byte[2];
        for (int i = 0; i < 2; i++) {
            int offset = (targets.length - 1 - i) * 8;
            targets[i] = (byte) ((s >>> offset) & 0xff);
        }
        return targets;
    }

    public static MsgPacket makeMsgPacket(String code, String payload, boolean isResponse) {
        MsgPacket msgPacket = new MsgPacket();
        msgPacket.isResponse = isResponse;
        msgPacket.code = code;
        msgPacket.payload = payload.getBytes();

        if (!msgPacket.isResponse) {
            msgPacket.seq = ++nextSeq;
            if (msgPacket.seq > 0x7fff) {
                msgPacket.seq = nextSeq = 1;
            }
        }
        return msgPacket;
    }

    private static int nextSeq = 0;

    public static byte[] getRequestBytes(MsgPacket msgPacket) {

        byte[] data = ByteUtils.toData(msgPacket);

        if (data == null) {
            return null;
        }
        int len = data.length;

        if (len > 0xffff) {
            return null;
        }

        return data;

    }

    public static int getShort(byte[] b) {
        return (b[0] & 255) << 8 | b[1] & 255;
    }


    public static ByteBuffer toNioBuffer(ByteBuf buffer) {
        if (buffer.isDirect()) {
            return buffer.nioBuffer();
        }
        final byte[] bytes = new byte[buffer.readableBytes()];
        buffer.getBytes(buffer.readerIndex(), bytes);
        return ByteBuffer.wrap(bytes);
    }


    public static MsgPacket fromData(ByteBuffer data, int length) {
        try {

            MsgPacket msgPacket = new MsgPacket();

            byte[] seqArr = new byte[2];
            data.get(seqArr, 0, 2);
            int offset = 2;
            msgPacket.seq = ByteUtils.getShort(seqArr);
            msgPacket.isResponse = (msgPacket.seq & '耀') == 32768;

            for (msgPacket.seq &= 32767; offset < length && data.get(offset) != ByteUtils.zeroData(); ++offset) {
            }

            byte[] codeByteArr = new byte[offset - 2];
            data.get(codeByteArr, 0, offset - 2);
            msgPacket.code = new String(codeByteArr);
            ++offset;
            data.get();
            msgPacket.payload = new byte[length - offset];
            data.get(msgPacket.payload, 0, length - offset);

            return msgPacket;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

}
