package com.thel.chat.tlmsgclient;

/**
 * Created by gl on 15/11/20.
 */
public interface ResponseCallback {
    /**
     * 错误及超时
     * @param error
     * @see TimeoutException
     */
    void onError(Throwable error);

    /**
     * 正常返回
     * @param packet
     */
    void onResponse(MsgPacket packet);
}
