package com.thel.chat.tlmsgclient;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by gl on 16/1/27.
 */
public abstract class AbstractMsgClient implements IMsgClient {

    protected Logger logger = new StdLogger();

    // Msg 相关
    protected MsgListener msgListener;

    private int nextSeq = 0;
    private static HashMap<Integer, ResponseCallback> callbackMap = new HashMap<>();
    private Timer timer = new Timer();

    @Override
    public void setMsgListener(MsgListener listener) {
        this.msgListener = listener;
    }

    protected void setResponseCallback(final int seq, long timeout, final ResponseCallback callback) {
        logger.info("setResponseCallback:" + seq + " timeout:" + timeout + " callback:" + callback);
        callbackMap.put(seq, callback);
        if (timeout > 0) {
            timer.schedule(new AbstractMsgTimerTask(seq), timeout);
        }
    }

    @Override
    public Request request(String code, String payload) {
        return request(code, payload, 10000);
    }

    @Override
    public Request request(String code, String payload, long timeout) {
        return new Request(this, code, payload, timeout);
    }

    public void sendRequest(String code, String payload) throws IOException {
        sendRequest(code, payload.getBytes());
    }

    private void sendRequest(String code, byte[] payload) throws IOException {
        MsgPacket msgPacket = new MsgPacket();
        msgPacket.isResponse = false;
        msgPacket.code = code;
        msgPacket.payload = payload;
        sendPacket(msgPacket);
    }

    public void sendPacket(MsgPacket msgPacket) throws IOException {
        if (!isConnected()) {
            return;
        }
        if (!msgPacket.isResponse) {
            msgPacket.seq = ++nextSeq;
            if (msgPacket.seq > 0x7fff)
                msgPacket.seq = nextSeq = 1;
        }
        byte[] data = msgPacket.toData();
        if (data == null) {
            return;
        }
        int len = data.length;
        if (len > 0xffff) {
            logger.error("data too long");
            return;
        }
        logger.debug("send Request Seq:" + msgPacket.seq + " data:" + Arrays.toString(data));
        sendBytePacket(data);
    }

    /**
     * 子类在读取到消息后必须要调用此方法
     *
     * @param msgPacket
     */
    protected void onReceivePacket(MsgPacket msgPacket) {
        logger.info("recevied seq:" + msgPacket.getSeq() + " code:" + msgPacket.getCode() + ":" + msgPacket.getPayload() + " res:" + msgPacket.isResponse());
        if (!msgPacket.isResponse() && msgListener != null) {
            msgListener.onRemoteRequest(new RemoteRequest(this, msgPacket));
        } else {
            // response map
            ResponseCallback callback = callbackMap.get(msgPacket.getSeq());
            logger.info("callback:" + callback);
            if (callback != null) {
                callback.onResponse(msgPacket);
                callbackMap.remove(msgPacket.getSeq());
            }
        }
    }

    public abstract boolean isConnected();

    protected abstract void sendBytePacket(byte[] data) throws IOException;

    private static class AbstractMsgTimerTask extends TimerTask {

        private int mSeq;

        public AbstractMsgTimerTask(int seq) {
            mSeq = seq;
        }

        @Override
        public void run() {
            ResponseCallback _callback = callbackMap.get(mSeq);
            if (_callback != null) {
                _callback.onError(new TimeoutException("Request Timeout Seq:" + mSeq));
                callbackMap.remove(mSeq);
            }
        }
    }

}
