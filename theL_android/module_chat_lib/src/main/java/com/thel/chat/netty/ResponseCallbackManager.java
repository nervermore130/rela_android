package com.thel.chat.netty;

import java.util.LinkedHashMap;
import java.util.Map;

public class ResponseCallbackManager {

    private Map<Integer, ResponseCallback> callbacks = new LinkedHashMap<>();

    private static final ResponseCallbackManager ourInstance = new ResponseCallbackManager();

    public static ResponseCallbackManager getInstance() {
        return ourInstance;
    }

    private ResponseCallbackManager() {

    }

    public void setResponseCallback(int seq, ResponseCallback responseCallback) {
        if (callbacks != null) {
            callbacks.put(seq, responseCallback);
        }
    }

    public ResponseCallback getResponseCallback(int seq) {
        if (callbacks != null) {
            return callbacks.get(seq);
        }
        return null;
    }

    public void removeResponseCallback(int seq) {
        if (callbacks != null) {
            callbacks.remove(seq);
        }
    }

    public void clear() {
        if (callbacks != null) {
            callbacks.clear();
        }
    }
}
