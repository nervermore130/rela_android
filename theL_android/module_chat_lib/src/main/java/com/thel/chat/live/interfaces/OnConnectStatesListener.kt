package com.thel.chat.live.interfaces

interface OnConnectStatesListener {

    fun onConnectSuccess()

    fun onClose()

    fun onError(exDes: String?)

}