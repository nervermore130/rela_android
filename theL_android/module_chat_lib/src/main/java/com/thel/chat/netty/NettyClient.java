package com.thel.chat.netty;

import android.text.TextUtils;
import android.util.Log;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

/**
 * @author liuyun
 */
public class NettyClient {

    private static final String TAG = "NettyClient";

    private EventLoopGroup mEventLoopGroup;

    private NettyListener mNettyListener;

    private Channel mChannel;

    private boolean isConnect = false;

    private int reconnectNum = Integer.MAX_VALUE;

    private long reconnectIntervalTime = 5000;

    private String host = null;

    private int port = 3456;

    public synchronized NettyClient connect(final String host, final int port) {

        Log.d(TAG, " connect host : " + host);

        Log.d(TAG, " connect port : " + port);

        this.host = host;

        this.port = port;

        if (!isConnect && mNettyListener != null) {

            try {

                if (mEventLoopGroup != null) {
                    mEventLoopGroup.shutdownGracefully();
                }
                mEventLoopGroup = new NioEventLoopGroup();

                Bootstrap bootstrap = new Bootstrap().group(mEventLoopGroup)
                        .option(ChannelOption.SO_KEEPALIVE, true)
                        .channel(NioSocketChannel.class)
                        .handler(new NettyClientInitializer(mNettyListener));

                bootstrap.connect(host, port).addListener(new ChannelFutureListener() {
                    @Override
                    public void operationComplete(ChannelFuture channelFuture) throws Exception {

                        if (channelFuture.isSuccess()) {

                            Log.d(TAG, " operationComplete 连接成功 ");

                            isConnect = true;

                            mChannel = channelFuture.channel();
                        } else {

                            Log.d(TAG, " operationComplete 连接失败 ");

                            isConnect = false;

                            mNettyListener.onServiceStatusConnectChanged(NettyConstants.ConnectConstants.STATUS_CONNECT_ERROR);


                        }
                    }


                }).sync();

            } catch (Exception e) {

                Log.d(TAG, " 连接报错 " + e.getMessage());

                e.printStackTrace();

                if (mNettyListener != null) {

                    mNettyListener.onServiceStatusConnectChanged(NettyConstants.ConnectConstants.STATUS_CONNECT_ERROR);

                }

            }
        }
        return this;
    }

    public void disconnect() {
        if (mEventLoopGroup != null) {
            mEventLoopGroup.shutdownGracefully();
        }
    }

    public void autoReconnect() {
        if (reconnectNum > 0 && !isConnect) {
            reconnectNum--;
            try {
                Thread.sleep(reconnectIntervalTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Log.e(TAG, "重新连接");
            disconnect();
            if (host != null) {
                connect(host, port);

            }
        } else {
            disconnect();
        }
    }

    private boolean sendMsgToServer(byte[] data, ChannelFutureListener listener) {

        boolean flag = mChannel != null && isConnect;

        Log.d(TAG, " flag ：" + flag);

        if (flag) {
            ByteBuf buf = Unpooled.copiedBuffer(data);

            Log.d(TAG, " buf ：" + buf);

            mChannel.writeAndFlush(buf).addListener(listener);

        } else {
            if (mNettyListener != null) {
                mNettyListener.onServiceStatusConnectChanged(NettyConstants.ConnectConstants.STATUS_CONNECT_ERROR);
            }
        }
        return flag;
    }

    public void requestMsg(final String code, String payload, ResponseCallback responseCallback) {

        MsgPacket msgPacket = ByteUtils.makeMsgPacket(code, payload, false);

        Log.d(TAG, " requestMsg code : " + msgPacket.code);

        Log.d(TAG, " requestMsg seq : " + msgPacket.seq);

        ResponseCallbackManager.getInstance().setResponseCallback(msgPacket.seq, responseCallback);

        byte[] data = ByteUtils.getRequestBytes(msgPacket);

        if (data == null) {
            return;
        }

        sendMsgToServer(data, new ChannelFutureListener() {
            @Override public void operationComplete(ChannelFuture channelFuture) {

                if (channelFuture.isSuccess()) {

                    Log.d(TAG, " 发送消息成功 " + code);

                } else {

                    Log.d(TAG, " 发送消息失败 " + code);

                }

            }
        });
    }

    public void responseMsg(String code, String payload) {

        MsgPacket msgPacket = ByteUtils.makeMsgPacket(code, payload, true);

        byte[] data = ByteUtils.getRequestBytes(msgPacket);

        if (data == null) {
            return;
        }

        sendMsgToServer(data, new ChannelFutureListener() {
            @Override public void operationComplete(ChannelFuture channelFuture) {

                if (channelFuture.isSuccess()) {

                    Log.d(TAG, " 响应消息成功 ");

                } else {

                    Log.d(TAG, " 响应发送消息失败 ");

                }

            }
        });

    }

    public void setReconnectNum(int reconnectNum) {
        this.reconnectNum = reconnectNum;
    }

    public void setReconnectIntervalTime(long reconnectIntervalTime) {
        this.reconnectIntervalTime = reconnectIntervalTime;
    }

    public boolean getConnectStatus() {
        return isConnect;
    }

    public void setConnectStatus(boolean status) {
        this.isConnect = status;
    }

    public void setListener(NettyListener mNettyListener) {
        this.mNettyListener = mNettyListener;
    }


}
