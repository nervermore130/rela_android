package com.thel.chat.live.interfaces

interface ILiveChat {

    /**
     * 创建群组
     */
    fun createChatGroup()

    /**
     * 观众加入聊天
     */
    fun joinChatRoom()

    /**
     * 发送聊天消息
     * @param code 对应消息的code
     * @param content 消息的内容
     */
    fun sendMsg(code: String, content: String, onMessageRequestListener: OnMessageRequestListener)

    /**
     * 关闭聊天室
     */
    fun closeChatRoom()

}