package com.thel.chat.tlmsgclient;

import java.nio.ByteBuffer;
import java.util.ArrayList;

/**
 * Created by setsail on 15/11/10.
 */
public class MsgPacket extends Object {
    public boolean isResponse() {
        return isResponse;
    }

    public void setIsResponse(boolean isResponse) {
        this.isResponse = isResponse;
    }

    public long getLength() {
        return length;
    }

    public void setLength(long length) {
        this.length = length;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public byte[] getPayload() {
        return payload;
    }

    public void setPayload(byte[] payload) {
        this.payload = payload;
    }

    boolean isResponse;
    long length;
    String code;
    int seq;
    byte[] payload;

    // TODO 重寫此方法，使用byte[]代替ByteBuffer
    public void fromData(ByteBuffer data, int length) {
        try {
            byte[] seqArr = new byte[2];
            data.get(seqArr, 0, 2);
            int offset = 2;
            seq = Utils.getShort(seqArr);
            isResponse = (seq & 0x8000) == 0x8000;
            seq = seq & 0x7fff;
            for (; offset < length; offset++) {
                if (data.get(offset) == zeroData())
                    break;
            }
            byte[] codeByteArr = new byte[offset - 2];
            data.get(codeByteArr, 0, offset - 2);
            code = new String(codeByteArr);
            // 跳过\0字节
            offset++;
            data.get();
            payload = new byte[length - offset];
            data.get(payload, 0, length - offset);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // TODO 重寫此方法，不再依賴 fromData(ByteBuffer, int)
    public void fromData(byte[] data, int length) {
        ByteBuffer buffer = ByteBuffer.allocate(data.length);
        buffer.put(data);
        buffer.flip();
        fromData(buffer, length);
    }

    public byte[] toData() {
        ArrayList<Byte> bytes = new ArrayList<>();
        int _seq = seq;
        if (isResponse) {
            _seq |= 0x8000;
        }

        for (byte b : Utils.shortToByteArray(_seq)) {
            bytes.add(b);
        }
        for (byte b : code.getBytes()) {
            bytes.add(b);
        }
        bytes.add(zeroData());
        for (byte b : payload)
            bytes.add(b);
        byte[] result = new byte[bytes.size()];
        for (int i = 0; i < bytes.size(); i++) {
            result[i] = bytes.get(i);
        }

        return result;
    }

    public byte zeroData() {
        return new Byte("0");
    }

}
