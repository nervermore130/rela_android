package com.thel.chat.live.tencent

import android.text.TextUtils
import android.util.Log
import android.util.SparseArray
import com.tencent.imsdk.ext.group.TIMGroupMemberResult
import java.util.*
import com.tencent.imsdk.*
import com.thel.chat.live.LiveChatBuilder
import com.thel.chat.live.interfaces.ITencentLiveChat
import com.thel.chat.live.interfaces.OnMessageRequestListener
import org.json.JSONObject
import android.R.attr.tag
import com.tencent.imsdk.TIMValueCallBack
import com.tencent.imsdk.TIMGroupManager
import com.tencent.imsdk.TIMGroupMemberInfo
import com.thel.chat.live.tencent.signature.GenerateUserSig


class TencentChatImpl(private val liveChatBuilder: LiveChatBuilder) : ITencentLiveChat {

    private val tagName: String = "TencentChatImpl"

    private var conversation: TIMConversation? = null

    private val messageDecodeImpl = MessageDecodeImpl()

    private val messageCallbackMap = SparseArray<OnMessageRequestListener>()

    private var callbackSeq = 0

    override fun createChatGroup() {

        Log.e(tagName, "createChatGroup liveChatBuilder.userSigId : ${liveChatBuilder.userSigId}")

        TIMManager.getInstance().addMessageListener(myTIMMessageListener)

        TIMManager.getInstance().login(liveChatBuilder.userId, liveChatBuilder.userSigId, object : TIMCallBack {
            override fun onSuccess() {
                Log.d(tagName, "用户登录成功 liveChatBuilder.groupId : ${liveChatBuilder.groupId} ")

                liveChatBuilder.groupId?.let {

                    //创建公开群，且不自定义群 ID
                    val param = TIMGroupManager.CreateGroupParam("AVChatRoom", it)

                    param.groupId = it

                    //创建群组
                    TIMGroupManager.getInstance().createGroup(param, object : TIMValueCallBack<String> {
                        override fun onError(code: Int, desc: String) {

                            Log.e(tagName, "createGroup err groupId = $it ,code = $code, desc = $desc")

                            ///code 9518 网络异常

                            if (code == 10025 || 10016 == code) {

                                getConversation(it)

                                liveChatBuilder.onConnectStatesListener?.onConnectSuccess()

                            } else {
                                val errorInfo = "createGroup_${code}_$desc"
                                liveChatBuilder.onConnectStatesListener?.onError(errorInfo)
                            }
                        }

                        override fun onSuccess(s: String) {

                            Log.i(tagName, "createGroup success it : $it")

                            getConversation(it)

                            liveChatBuilder.onConnectStatesListener?.onConnectSuccess()
                            //会话类型：群组

                            Log.d(tagName, " liveChatBuilder.groupId : $it")

                        }
                    })

                }
            }

            override fun onError(code: Int, desc: String?) {
                Log.d(tagName, "用户登录失败 code : $code , des : $desc")
                val errorInfo = "login_${code}_$desc"
                liveChatBuilder.onConnectStatesListener?.onError(errorInfo)
                if (code == 6208) {
                    createChatGroup()
                }
            }

        })


    }


    override fun joinChatRoom() {

        Log.e(tagName, "joinChatRoom liveChatBuilder.userSigId : " + liveChatBuilder.userSigId)

        TIMManager.getInstance().addMessageListener(myTIMMessageListener)

        liveChatBuilder.groupId?.let {
            TIMManager.getInstance().login(liveChatBuilder.userId, liveChatBuilder.userSigId, object : MyTIMCallBack(it) {

                override fun onError(code: Int, desc: String) {
                    super.onError(code, desc)
                    Log.d(tagName, "用户登录失败 code : $code , des : $desc")
                    val errorInfo = "login_${code}_$desc"
                    liveChatBuilder.onConnectStatesListener?.onError(errorInfo)
                    if (code == 6208) {
                        joinChatRoom()
                    }
                }

                override fun onSuccess() {
                    Log.d("TencentChat", "用户登录成功" + liveChatBuilder.isAudience)

                    if (liveChatBuilder.isAudience!!) {
                        TIMGroupManager.getInstance().applyJoinGroup(it, "", object : TIMCallBack {
                            override fun onError(code: Int, desc: String) {
                                //接口返回了错误码 code 和错误描述 desc，可用于原因
                                //错误码 code 列表请参见错误码表
                                Log.e(tagName, "applyJoinGroup err groupId = $it ,code = $code, desc = $desc")
                                if (roomId == liveChatBuilder.groupId) {
                                    ///code 9518 网络异常
                                    if (code == 10013 || 10016 == code) {
                                        Log.i(tagName, " MyTIMCallBack onError 10013")
                                        getConversation(it)
                                        liveChatBuilder.onConnectStatesListener?.onConnectSuccess()
                                    } else {
                                        val errorInfo = "joinGroup_${code}_$desc"
                                        liveChatBuilder.onConnectStatesListener?.onError(errorInfo)
                                    }
                                }

                            }

                            override fun onSuccess() {
                                if (roomId == liveChatBuilder.groupId) {
                                    Log.i(tagName, " MyTIMCallBack applyJoinGroup success ${liveChatBuilder.groupId}")
                                    getConversation(it)
                                    liveChatBuilder.onConnectStatesListener?.onConnectSuccess()
                                    //会话类型：群组
                                }
                                Log.d(tagName, " liveChatBuilder.groupId : $it")

                            }
                        })
                    } else {
                        if (roomId == liveChatBuilder.groupId) {
                            getConversation(it)
                        }
                    }
                }

            })
        }

    }

    @ExperimentalStdlibApi
    override fun sendMsg(code: String, content: String, onMessageRequestListener: OnMessageRequestListener) {

        callbackSeq++

        messageCallbackMap.put(callbackSeq, onMessageRequestListener)

        //构造一条消息
        val msg = TIMMessage()

        //添加文本内容
        val elem = TIMCustomElem()

        Log.d(tagName, " old content : $content")

        val contextObj = if (TextUtils.isEmpty(content)) {
            JSONObject("{}")
        } else {
            JSONObject(content)
        }

        contextObj.put("reqId", callbackSeq)

        Log.d(tagName, " new content : $contextObj")

        val obj = JSONObject()

        obj.put("code", code)

        obj.put("body", contextObj.toString())

        val sendMsgContent = obj.toString()

        Log.d(tagName, " sendMsgContent : $sendMsgContent")

        elem.ext = sendMsgContent.toByteArray(Charsets.UTF_8)

        //将elem添加到消息
        if (msg.addElement(elem) != 0) {
            Log.d(tagName, "addElement failed")
            return
        }

        //发送消息
        conversation?.sendMessage(msg, object : TIMValueCallBack<TIMMessage> {
            //发送消息回调
            override fun onError(code: Int, desc: String) {//发送消息失败
                //错误码 code 和错误描述 desc，可用于定位请求失败原因
                //错误码 code 含义请参见错误码表
                Log.d(tagName, "send message failed. code: $code errmsg: $desc")

                val errorInfo = "sendMessage_${code}_$desc"

                onMessageRequestListener.onError(Throwable(errorInfo))
            }

            override fun onSuccess(msg: TIMMessage) {//发送消息成功
                Log.e(tagName, "SendMsg ok")

            }
        })

    }


    override fun quitChatRoom() {

        liveChatBuilder.groupId?.let {
            TIMGroupManager.getInstance().quitGroup(it, object : TIMCallBack {
                override fun onSuccess() {
                    Log.e(tagName, "quitChatRoom success")

                }

                override fun onError(code: Int, desc: String?) {
                    Log.e(tagName, "quitChatRoom err code = $code, desc = $desc")

                }

            })
        }


    }

    override fun deleteGroupMember() {

        liveChatBuilder.groupId?.let {

            //创建待踢出群组的用户列表
            val list = ArrayList<String>()

            list.add("user_id")

            val param = TIMGroupManager.DeleteMemberParam(it, list)
            param.setReason("some reason")

            TIMGroupManager.getInstance().deleteGroupMember(param, object : TIMValueCallBack<List<TIMGroupMemberResult>> {
                override fun onError(code: Int, desc: String) {
                    Log.e(tagName, "deleteGroupMember onErr. code: $code errmsg: $desc")
                }

                override fun onSuccess(results: List<TIMGroupMemberResult>) { //群组成员操作结果
                    for (r in results) {
                        Log.d(tagName, "result: " + r.result  //操作结果:  0：删除失败；1：删除成功；2：不是群组成员

                                + " user: " + r.user)    //用户帐号
                    }
                }
            })


        }


    }

    override fun deleteGroup() {

        liveChatBuilder.groupId?.let {
            //解散群组
            TIMGroupManager.getInstance().deleteGroup(it, object : TIMCallBack {
                override fun onError(code: Int, desc: String) {

                    //错误码 code 和错误描述 desc，可用于定位请求失败原因
                    //错误码 code 列表请参见错误码表
                    Log.d(tagName, "deleteGroup failed. code: $code errmsg: $desc")
                }

                override fun onSuccess() {
                    //解散群组成功

                    Log.d(tagName, "deleteGroup success. ")

                }
            })
        }
    }

    private fun getConversation(groupId: String) {

        conversation = TIMManager.getInstance().getConversation(TIMConversationType.Group, groupId)

    }

    override fun closeChatRoom() {

        Log.d(tagName, " closeChatRoom. ")

        conversation = null

        if (liveChatBuilder.isAudience!!) {
            quitChatRoom()
        }

        messageCallbackMap.clear()

        TIMManager.getInstance().removeMessageListener(myTIMMessageListener)
    }


    private val myTIMMessageListener = TIMMessageListener { messageList ->

        try {
            messageList?.forEach {

                for (i in 0 until it.elementCount) {

                    val elem = it.getElement(i.toInt())

                    if (elem is TIMCustomElem) {

                        Log.d(tagName, "=============new message=============")

                        val result = messageDecodeImpl.decode(elem)

                        Log.d(tagName, " result : $result")

                        val objectJson = JSONObject(result)

                        val hasRoomId = objectJson.has("roomId")

                        if (hasRoomId && objectJson.getString("roomId") == liveChatBuilder.groupId) {

                            val hasCode = objectJson.has("code")

                            val hasPayload = objectJson.has("data")

                            if (hasCode && hasPayload) {

                                var code = objectJson.getString("code")

                                Log.d(tagName, " result code : $code")

                                var payload = objectJson.getString("data")


                                if (code == "req") {

                                    val payloadJson = JSONObject(payload)

                                    if (payloadJson.has("data")) {

                                        payload = payloadJson.getString("data")

                                        code = payloadJson.getString("message")

                                    }

                                }

                                Log.d(tagName, " result payload : $payload")

                                val hasUserId = objectJson.has("toUserId")

                                val hasReqId = objectJson.has("reqId")

                                if (hasReqId) {

                                    val reqId = objectJson.getInt("reqId")

                                    if (reqId > 0) {

                                        val messageCallback = messageCallbackMap.get(reqId)

                                        if (messageCallback != null) {

                                            messageCallback.onSuccess(code, payload)

                                            messageCallbackMap.remove(reqId)
                                        }
                                    }
                                } else {

                                    if (hasUserId) {

                                        val toUserId = objectJson.getString("toUserId")

                                        if (toUserId == liveChatBuilder.userId || toUserId == "0") {

                                            liveChatBuilder.onMessageListener?.onSuccess(code, payload)
                                        }
                                    } else {
                                        liveChatBuilder.onMessageListener?.onSuccess(code, payload)
                                    }

                                    Log.d(tagName, " liveChatBuilder.onMessageListener ${liveChatBuilder.onMessageListener}")


                                }
                            }

                        } else {
//                            liveChatBuilder.onMessageListener?.onFailed()
                            Log.d(tagName, " 返回的数据类型不匹配")

                        }
                    }

                }

            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        true
    }


    private fun loginTencentChat() {

        TIMManager.getInstance().login(liveChatBuilder.userId, liveChatBuilder.userSigId, object : TIMCallBack {
            override fun onSuccess() {
                Log.d("TencentChat", "用户登录成功")
                joinChatRoom()
            }

            override fun onError(p0: Int, p1: String?) {
                Log.d("TencentChat", "用户登录失败")

            }

        })

    }
}

open class MyTIMCallBack(val roomId: String) : TIMCallBack {

    override fun onError(code: Int, desc: String) {

    }

    override fun onSuccess() {


    }

}