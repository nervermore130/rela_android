package com.thel.chat.live.tlmsg

import android.util.Log
import com.tencent.imsdk.TIMCallBack
import com.tencent.imsdk.TIMManager
import com.thel.chat.live.LiveChatBuilder
import com.thel.chat.live.interfaces.ILiveChat
import com.thel.chat.live.interfaces.OnMessageRequestListener
import com.thel.chat.live.tencent.signature.GenerateUserSig
import com.thel.chat.tlmsgclient.*
import kotlinx.coroutines.*
import java.lang.Exception

class TLMsgChatImpl(private val liveChatBuilder: LiveChatBuilder?) : ILiveChat {


    private val tagName: String = "TLMsgChatImpl"

    private var client: IMsgClient? = null

    private var connectState: LiveChatConnectState = LiveChatConnectState.CONNECTING

    private var job: Job? = null

    override fun createChatGroup() {
        joinChatRoom()
    }

    override fun joinChatRoom() {

        Log.d(tagName, " ------joinChatRoom-----")

        Log.d(tagName, " liveChatBuilder?.port : ${liveChatBuilder?.port}")

        Log.d(tagName, " liveChatBuilder?.host : ${liveChatBuilder?.host}")

        client = liveChatBuilder?.port?.let { MsgClient(liveChatBuilder.host, it) }

        Log.d(tagName, " client : $client")

        client?.let {
            it.setMsgListener(object : MsgListener {
                override fun onConnect(client: IMsgClient?) {

                    Log.d(tagName, " ------onConnect-----")

                    liveChatBuilder?.onConnectStatesListener?.onConnectSuccess()

                    connectState = LiveChatConnectState.CONNECTED_SUCCESS

                    job?.cancel()

                    job = null

                }

                override fun onRemoteRequest(request: RemoteRequest?) {

                    if (request != null) {

                        Log.d(tagName, " request.code : ${request.code}")

                        Log.d(tagName, " request.payload : ${request.payload}")

                        liveChatBuilder?.onMessageListener?.onSuccess(request.code, request.payload)

                    }
                }

                override fun onClosed(client: IMsgClient?) {

                    Log.d(tagName, " ------onClosed-----")

                    liveChatBuilder?.onConnectStatesListener?.onClose()

                    connectState = LiveChatConnectState.CONNECTED_FAILED

                    createCoroutine()
                }

                override fun onError(ex: Exception?) {

                    Log.d(tagName, " ------onError----- ex : ${ex?.message}")

                    ex?.let { liveChatBuilder?.onConnectStatesListener?.onError(ex.message) }

                    connectState = LiveChatConnectState.CONNECTED_FAILED

                    createCoroutine()

                }

            })
            it.connect()
        }


    }

    override fun sendMsg(code: String, content: String, onMessageRequestListener: OnMessageRequestListener) {
        GlobalScope.launch(Dispatchers.IO) {
            client?.request(code, content)?.enqueue(object : ResponseCallback {
                override fun onError(error: Throwable?) {

                    Log.d(tagName, "sendMsg onError : ${error?.message}")

                    onMessageRequestListener.onError(error)

                    if (code == "init") {

                        sendMsg(code, content, onMessageRequestListener)

                    }

                }

                override fun onResponse(packet: MsgPacket?) {

                    if (packet != null) {
                        onMessageRequestListener.onSuccess(packet.code, String(packet.payload))
                    }
                }

            })
        }
    }

    override fun closeChatRoom() {

        Log.d(tagName, "closeChatRoom")

        client?.close()
        job?.cancel()
    }

    private fun createCoroutine() {

        Log.d(tagName, "启动协程")

        closeChatRoom()

        job = GlobalScope.launch(start = CoroutineStart.LAZY) {

            if (connectState == LiveChatConnectState.CONNECTED_FAILED) {

                Log.d(tagName, "开始重连")

                joinChatRoom()

            }
        }

        Thread.sleep(5000L)
        // 手动启动协程
        job?.start()

    }

}

enum class LiveChatConnectState {
    CONNECTING,
    CONNECTED_SUCCESS,
    CONNECTED_FAILED,
}