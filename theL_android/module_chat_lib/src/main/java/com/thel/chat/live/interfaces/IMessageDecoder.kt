package com.thel.chat.live.interfaces

interface IMessageDecoder<T> {
    fun decode(t: T) : String
}