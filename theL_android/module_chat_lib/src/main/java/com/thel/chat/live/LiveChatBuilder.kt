package com.thel.chat.live

import com.thel.chat.live.interfaces.OnConnectStatesListener
import com.thel.chat.live.interfaces.OnMessageListener

class LiveChatBuilder private constructor(builder: Builder) {

    val type: Int?

    val host: String?

    val port: Int?

    val groupId: String?

    val isAudience: Boolean?

    val onMessageListener: OnMessageListener?

    val onConnectStatesListener: OnConnectStatesListener?

    val userId: String?

    val userSigId : String?

    init {
        this.type = builder.type
        this.host = builder.host
        this.port = builder.port
        this.groupId = builder.groupId
        this.isAudience = builder.isAudience
        this.onMessageListener = builder.onMessageListener
        this.onConnectStatesListener = builder.onConnectStatesListener
        this.userId = builder.userId
        this.userSigId = builder.userSigId
    }

    class Builder {

        var type: Int? = -1
            private set

        var host: String? = null
            private set

        var port: Int? = -1
            private set

        var groupId: String? = null
            private set

        var isAudience: Boolean? = true
            private set

        var onMessageListener: OnMessageListener? = null
            private set

        var onConnectStatesListener: OnConnectStatesListener? = null
            private set

        var userId: String? = null
            private set

        var userSigId: String? = null
            private set

        fun type(type: Int) = apply { this.type = type }
        fun host(host: String) = apply { this.host = host }
        fun port(port: Int) = apply { this.port = port }
        fun groupId(groupId: String) = apply { this.groupId = groupId }
        fun isAudience(isAudience: Boolean) = apply { this.isAudience = isAudience }
        fun onMessageListener(onMessageListener: OnMessageListener) = apply { this.onMessageListener = onMessageListener }
        fun onConnectStatesListener(onConnectStatesListener: OnConnectStatesListener) = apply { this.onConnectStatesListener = onConnectStatesListener }
        fun userId(userId: String) = apply { this.userId = userId }
        fun userSigId(userSigId: String) = apply { this.userSigId = userSigId }

        fun build() = LiveChatBuilder(this)

    }

}