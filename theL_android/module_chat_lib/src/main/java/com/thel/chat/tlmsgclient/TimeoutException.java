package com.thel.chat.tlmsgclient;

/**
 * Created by gl on 15/12/30.
 */
public class TimeoutException extends Exception{
    public TimeoutException(String message) {
        super((message));
    }
}
