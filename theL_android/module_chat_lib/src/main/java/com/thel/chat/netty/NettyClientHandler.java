package com.thel.chat.netty;

import android.util.Log;

import java.nio.ByteBuffer;

import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * @author liuyun
 */
public class NettyClientHandler extends SimpleChannelInboundHandler<ByteBuf> {

    private NettyListener mNettyListener;

    public NettyClientHandler(NettyListener mNettyListener) {
        this.mNettyListener = mNettyListener;
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) {

        Log.d("NettyClient", " channelActive : ");

        if (mNettyListener != null) {
            mNettyListener.onServiceStatusConnectChanged(NettyConstants.ConnectConstants.STATUS_CONNECT_SUCCESS);
        }
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) {

        Log.d("NettyClient", " channelInactive : ");

        if (mNettyListener != null) {
            mNettyListener.onServiceStatusConnectChanged(NettyConstants.ConnectConstants.STATUS_CONNECT_CLOSED);
        }
    }

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf) {

        short length = byteBuf.readShort();

        ByteBuffer byteBuffer = ByteUtils.toNioBuffer(byteBuf);

        MsgPacket msgPacket = ByteUtils.fromData(byteBuffer, length);

        if (msgPacket != null) {

            ResponseCallback responseCallback = ResponseCallbackManager.getInstance().getResponseCallback(msgPacket.seq);

            if (responseCallback == null) {

                if (mNettyListener != null) {

                    mNettyListener.onMessageResponse(msgPacket);

                }


            } else {
                responseCallback.onResponse(msgPacket);

                ResponseCallbackManager.getInstance().removeResponseCallback(msgPacket.seq);
            }

        }


    }

    @Override public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        super.exceptionCaught(ctx, cause);
        Channel channel = ctx.channel();
        if (channel.isActive()) {
            ctx.close();
        }
    }
}
