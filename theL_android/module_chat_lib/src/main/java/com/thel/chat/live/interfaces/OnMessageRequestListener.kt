package com.thel.chat.live.interfaces

interface OnMessageRequestListener {

    fun onError(error: Throwable?)

    fun onSuccess(code: String, payload: String)

}