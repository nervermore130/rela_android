package com.thel.chat.tlmsgclient;

/**
 * Created by gl on 15/12/22.
 */
public class StdLogger implements Logger {

    private int level = 0;
    private static int DEBUG=1;
    private static int INFO=2;
    private static int WARN=3;
    private static int ERROR=4;

    public StdLogger() {
        this(INFO);
    }

    public StdLogger(int level) {
        this.setLevel(level);
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getLevel() {
        return level;
    }

    @Override
    public void debug(String message) {
        if(level <= DEBUG) System.out.println("[DEBUG] MsgClient:"+message);
    }

    @Override
    public void info(String message) {
        if(level <= INFO) System.out.println("[INFO] MsgClient:"+message);
    }

    @Override
    public void warn(String message) {
        if(level <= WARN) System.out.println("[WARN] MsgClient:"+message);
    }

    @Override
    public void error(String message) {
        if(level <= ERROR) System.out.println("[ERROR] MsgClient:"+message);
    }
}
