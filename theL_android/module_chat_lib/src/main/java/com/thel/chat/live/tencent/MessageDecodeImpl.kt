package com.thel.chat.live.tencent

import android.util.Log
import com.tencent.imsdk.TIMCustomElem
import com.tencent.imsdk.TIMElemType
import com.thel.chat.live.interfaces.IMessageDecoder

class MessageDecodeImpl : IMessageDecoder<TIMCustomElem> {

    private val tagName: String = "MessageDecodeImpl"

    override fun decode(t: TIMCustomElem): String {

        Log.d(tagName, " message decode :")

        var body = ""

        //获取当前元素的类型
        val elemType = t.type

        if (elemType == TIMElemType.Custom) {

            body = String(t.ext)

            Log.d(tagName, " tencent msg body : $body")
        }

        return body
    }

}