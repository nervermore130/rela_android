package com.thel.chat.live

import android.content.Context
import android.os.Environment
import android.util.Log
import com.tencent.imsdk.*
import com.tencent.imsdk.TIMManager
import com.tencent.imsdk.session.SessionWrapper
import com.thel.chat.live.tencent.signature.GenerateUserSig

class TIMManager {


    companion object {

        fun init(context: Context) {
            if (SessionWrapper.isMainProcess(context.applicationContext)) {
                val config = TIMSdkConfig(GenerateUserSig.SDKAPPID)
                        .enableLogPrint(true)
                        .setLogLevel(TIMLogLevel.DEBUG)
                        .setLogPath(Environment.getExternalStorageDirectory().path + "/justfortest/")
                        .setLogListener { level, tag, msg -> Log.d("TencentChat", " level : $level , tag : $tag , msg : $msg") }
                //初始化 SDK
                TIMManager.getInstance().init(context.applicationContext, config)

                val userConfig = TIMUserConfig()

                userConfig.disableStorage()

                TIMManager.getInstance().userConfig = userConfig
            }
        }
    }
}