package com.thel.chat.tlmsgclient;

import java.io.IOException;

/**
 * Created by gl on 15/11/20.
 *
 * 客户端
 */
public interface IMsgClient {

    /**
     *
     * 建立连接
     * @throws IOException
     */
    void connect() throws IOException;

//    void sendPacket(MsgPacket msgPacket) throws IOException;

//    void setResponseCallback(int seq, long timeout, ResponseCallback callback);

    /**
     *
     * 设置监听器
     * @param listener
     */
    void setMsgListener(MsgListener listener);

    /**
     * 发起请求
     * @param code
     * @param payload
     * @return
     */
    Request request(String code, String payload);

    /**
     * 发起请求
     * @param code
     * @param payload
     * @param timeout
     * @return
     */
    Request request(String code, String payload, long timeout);

    /**
     * 关闭连接
     * @throws IOException
     */
    void close() throws IOException;
}
