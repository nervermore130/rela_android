package com.thel.chat.tlmsgclient;

/**
 * Created by setsail on 15/11/10.
 */
public interface Constants {
    int PUSH_MSG = 100;
    int PACKET_BUFFER_SIZE = 65537;
    int MSG_BUFFER_SIZE = 65535;
}
