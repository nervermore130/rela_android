package com.thel.chat.netty;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

/**
 * @author liuyun
 */
public class NettyClientInitializer extends ChannelInitializer<SocketChannel> {

    private NettyListener mNettyListener;

    public NettyClientInitializer(NettyListener mNettyListener) {
        this.mNettyListener = mNettyListener;
    }

    @Override
    protected void initChannel(SocketChannel ch) {

        ChannelPipeline pipeline = ch.pipeline();

        pipeline.addLast(new LoggingHandler(LogLevel.INFO));

        pipeline.addLast(new LengthFieldBasedFrameDecoder(NettyConstants.MAX_FRAME_LENGTH, NettyConstants.LENGTH_FIELD_OFFSET, NettyConstants.LENGTH_FIELD_LENGTH));

        pipeline.addLast(new NettyClientHandler(mNettyListener));
    }
}

