package com.thel.chat.live.interfaces

interface OnMessageListener {

    fun onFailed()

    fun onSuccess(code: String, result: String)

}