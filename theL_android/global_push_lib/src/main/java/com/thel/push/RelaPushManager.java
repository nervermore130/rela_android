package com.thel.push;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import androidx.annotation.NonNull;
import cn.jpush.android.api.JPushInterface;

public class RelaPushManager {

    private final static String TAG = "RelaPushManager";

    private static RelaPushManager INSTANCE = new RelaPushManager();

    private RelaPushManager() {

    }

    public static RelaPushManager getInstance() {
        return INSTANCE;
    }

    public void initJPush(Context context,OnPushListener onPushListener) {

        // 设置开启日志,发布时请关闭日志
        JPushInterface.setDebugMode(true);
        // 初始化 JPush
        JPushInterface.init(context);

        fireBaseTest(context);

        String registrationId = JPushInterface.getRegistrationID(context);

        Log.d(TAG, " registrationId : " + registrationId);

    }


    public OnPushListener mOnPushListener;

    public void setOnPushTokenListener(OnPushListener mOnPushListener) {
        this.mOnPushListener = mOnPushListener;
    }

    private void fireBaseTest(Context context) {

        FirebaseApp.initializeApp(context);

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.d(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();

                        Log.d(TAG, " fcm token token : " + token);

                    }
                });
    }

}
