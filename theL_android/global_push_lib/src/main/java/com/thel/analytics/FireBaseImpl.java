package com.thel.analytics;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;

public class FireBaseImpl implements IGoogleAnalytics {
    private static FireBaseImpl INSTANCE = new FireBaseImpl();

    private FireBaseImpl() {
    }

    public static FireBaseImpl getInstance() {
        return INSTANCE;
    }

    /**
     * 上传埋点
     */
    public static void uploadGoogle(String event, Context context) {
        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);

        if (mFirebaseAnalytics != null) {
            Bundle bundle = new Bundle();
            mFirebaseAnalytics.logEvent(event, bundle);

        }
    }

    @Override
    public void uploadGoogleDatas(String event, Activity context) {
        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);

        if (mFirebaseAnalytics != null) {
            Bundle bundle = new Bundle();
            mFirebaseAnalytics.logEvent(event, bundle);

        }
    }
}
