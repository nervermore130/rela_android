package com.thel.utils;

import android.content.Context;
import android.media.MediaPlayer;

/**
 * Created by kevin on 2017/9/26.
 */

public class SoundUtils {

    public static void playSound(Context context, int resid) {
        final MediaPlayer mMediaPlayer = MediaPlayer.create(context, resid);
        if (mMediaPlayer != null) {
            if (!mMediaPlayer.isPlaying()) {
                mMediaPlayer.start();
            } else {
                mMediaPlayer.stop();
            }
            mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mMediaPlayer.release();
                }
            });
        }
    }

}
