package com.thel.utils;

import android.content.Context;
import android.util.Log;

import java.lang.reflect.Method;

public class NotchUtils {

    private static final String TAG = "NotchUtils";

    public static int getNotchHeight(Context context) {

        int xmNotchHeight = getXiaoMiNotchHeight(context);

        if (xmNotchHeight > 0) {
            return xmNotchHeight;
        }

        int oppoNotchHeight = getOppoNotchHeight(context);

        if (oppoNotchHeight > 0) {
            return oppoNotchHeight;
        }

        if (hasNotchInScreen(context)) {
            return getHWNotchHeight(context);
        }

        if (hasNotchInScreenAtVoio(context)) {
            return context.getResources().getDimensionPixelSize(R.dimen.vivo_notch_height);
        }

        return 0;

    }

    private static int getXiaoMiNotchHeight(Context context) {

        int notchHeight = 0;

        int resourceId = context.getResources().getIdentifier("notch_height", "dimen", "android");
        if (resourceId > 0) {
            notchHeight = context.getResources().getDimensionPixelSize(resourceId);
        }

        return notchHeight;

    }

    private static int getOppoNotchHeight(Context context) {

        boolean isOppoNotch = context.getPackageManager().hasSystemFeature("com.oppo.feature.screen.heteromorphism");

        if (isOppoNotch) {
            return 80;
        }

        return 0;

    }

    private static int getHWNotchHeight(Context context) {

        int height = 0;

        try {

            ClassLoader cl = context.getClassLoader();

            Class HwNotchSizeUtil = cl.loadClass("com.huawei.android.util.HwNotchSizeUtil");

            Method get = HwNotchSizeUtil.getMethod("getNotchSize");

            int[] ret = (int[]) get.invoke(HwNotchSizeUtil);

            height = ret[1];

        } catch (ClassNotFoundException e) {

            Log.e(TAG, "getNotchSize ClassNotFoundException");

        } catch (NoSuchMethodException e) {

            Log.e(TAG, "getNotchSize NoSuchMethodException");

        } catch (Exception e) {

            Log.e(TAG, "getNotchSize Exception");

        } finally {

            return height;

        }
    }

    private static boolean hasNotchInScreen(Context context) {

        boolean ret = false;

        try {

            ClassLoader cl = context.getClassLoader();

            Class HwNotchSizeUtil = cl.loadClass("com.huawei.android.util.HwNotchSizeUtil");

            Method get = HwNotchSizeUtil.getMethod("hasNotchInScreen");

            ret = (boolean) get.invoke(HwNotchSizeUtil);

        } catch (ClassNotFoundException e) {

            Log.e(TAG, "hasNotchInScreen ClassNotFoundException");

        } catch (NoSuchMethodException e) {

            Log.e(TAG, "hasNotchInScreen NoSuchMethodException");

        } catch (Exception e) {

            Log.e(TAG, "hasNotchInScreen Exception");

        } finally {

            return ret;

        }

    }

    private static final int NOTCH_IN_SCREEN_VOIO = 0x00000020;//是否有凹槽

    private static final int ROUNDED_IN_SCREEN_VOIO = 0x00000008;//是否有圆角

    private static boolean hasNotchInScreenAtVoio(Context context) {
        boolean ret = false;
        try {
            ClassLoader cl = context.getClassLoader();
            Class FtFeature = cl.loadClass("com.util.FtFeature");
            Method get = FtFeature.getMethod("isFeatureSupport", int.class);
            ret = (boolean) get.invoke(FtFeature, NOTCH_IN_SCREEN_VOIO);

        } catch (ClassNotFoundException e) {
            Log.e(TAG, "hasNotchInScreen ClassNotFoundException");
        } catch (NoSuchMethodException e) {
            Log.e(TAG, "hasNotchInScreen NoSuchMethodException");
        } catch (Exception e) {
            Log.e(TAG, "hasNotchInScreen Exception");
        }
        return ret;
    }

}
