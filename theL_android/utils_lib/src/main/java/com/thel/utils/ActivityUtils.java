package com.thel.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;

import java.util.List;

import static android.content.Context.ACTIVITY_SERVICE;

/**
 * Created by kevin on 2017/9/26.
 */

public class ActivityUtils {
    /**
     * 获取根Activity
     *
     * @return
     */
    public static String getBaseActivity(Context context) {
        String baseActivity = "";
        ActivityManager manager = (ActivityManager) context.getSystemService(ACTIVITY_SERVICE);
        baseActivity = manager.getRunningTasks(1).get(0).baseActivity.getClassName();
        return baseActivity;
    }

    public static boolean isAppOnForeground(Context context) {
        try {
            ActivityManager activityManager = (ActivityManager) context.getSystemService(ACTIVITY_SERVICE);
            List<RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
            String packageName = context.getPackageName();

            if (appProcesses == null) {
                return false;
            }
            for (RunningAppProcessInfo appProcess : appProcesses) {

                if (appProcess.processName.equals(packageName) && appProcess.importance == RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    return false;
                }
            }
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    public static String getTopActivity(Activity context)

    {

        ActivityManager manager = (ActivityManager) context.getSystemService(ACTIVITY_SERVICE);

        List<ActivityManager.RunningTaskInfo> runningTaskInfos = manager.getRunningTasks(1);


        if (runningTaskInfos != null)

            return (runningTaskInfos.get(0).topActivity).toString();

        else

            return null;

    }


}
