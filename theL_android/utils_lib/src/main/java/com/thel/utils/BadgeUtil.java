package com.thel.utils;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import me.leolin.shortcutbadger.ShortcutBadger;

/**
 * 应用桌面图标未读消息显示工具类
 * 只支持 小米，三星和索尼
 */
public class BadgeUtil {

    private static final String LUNCHER_CLASS = "com.thel.modules.welcome.WelcomeActivity";

    private static final String NOTIFICATION_CHANNEL = "com.thel.xiaomi.notification";

    private static NotificationManager mNotificationManager;

    private static int notificationId = 1;

    private static int oldCount = -1;

    public static void setBadgeCount(Context context, int count) {

        if (count <= 0) {
            count = 0;
        } else {
            count = Math.max(0, Math.min(count, 99));
        }

        Log.d("BadgeUtil", " Build.MANUFACTURER : " + Build.MANUFACTURER);

        Log.d("BadgeUtil", " count : " + count);

        if (Build.MANUFACTURER.equalsIgnoreCase("sony")) {
            sendToSony(context, count);
        } else if (Build.MANUFACTURER.toLowerCase().contains("samsung")) {
            sendToSamsumg(context, count);
        } else if (isMIUI()) {
            boolean isAppForeground = isAppForeground(context);

            Log.d("BadgeUtil", " isAppForeground : " + isAppForeground);

            if (isAppForeground) {
                sendToXiaoMi(context, count);
            }

        } else {
            ShortcutBadger.applyCount(context, count);
        }

    }

    public static void clearBadgeCount(Context context) {
        setBadgeCount(context, 0);
        ShortcutBadger.applyCount(context, 0);
    }

    private static void sendToXiaoMi(Context context, int count) {

        Log.d("BadgeUtil", " xiaomi count : " + count);

        if (count > 0 && oldCount == count) {
            return;
        }

        if (count == 0) {
            if (mNotificationManager != null) {
                mNotificationManager.cancel(notificationId);
            }
            return;
        }

        if (mNotificationManager == null) {
            mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        }

        String title = context.getResources().getString(R.string.notification_title);

        String content = context.getResources().getString(R.string.notification_content, count);

        Notification.Builder builder = new Notification.Builder(context)
                .setContentTitle(title)
                .setContentText(content)
                .setSmallIcon(R.mipmap.ic_launcher);

        Intent intent = new Intent();
        intent.setClassName("com.thel", LUNCHER_CLASS);
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        builder.setContentIntent(contentIntent);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL, "XiaoMi Notification",
                    NotificationManager.IMPORTANCE_DEFAULT);

            mNotificationManager.createNotificationChannel(channel);

            builder.setChannelId(NOTIFICATION_CHANNEL);
        }

        try {
            Notification notification = builder.getNotification();
            ShortcutBadger.applyNotification(context, notification, count);
            mNotificationManager.notify(notificationId, notification);
        } catch (Exception e) {
            Intent localIntent = new Intent("android.intent.action.APPLICATION_MESSAGE_UPDATE");
            localIntent.putExtra("android.intent.extra.update_application_component_name",
                    context.getPackageName() + "/" + LUNCHER_CLASS);
            localIntent.putExtra("android.intent.extra.update_application_message_text",
                    String.valueOf(count == 0 ? "" : count));
            context.sendBroadcast(localIntent);
        }

        oldCount = count;
    }

    private static void sendToSony(Context context, int count) {
        boolean isShow = true;
        if (count == 0) {
            isShow = false;
        }
        Intent localIntent = new Intent();
        localIntent.setAction("com.sonyericsson.home.action.UPDATE_BADGE");
        localIntent.putExtra("com.sonyericsson.home.intent.extra.badge.SHOW_MESSAGE", isShow);//是否显示
        localIntent.putExtra("com.sonyericsson.home.intent.extra.badge.ACTIVITY_NAME", LUNCHER_CLASS);//启动页
        localIntent.putExtra("com.sonyericsson.home.intent.extra.badge.MESSAGE", String.valueOf(count));//数字
        localIntent.putExtra("com.sonyericsson.home.intent.extra.badge.PACKAGE_NAME", context.getPackageName());//包名
        context.sendBroadcast(localIntent);
    }

    private static void sendToSamsumg(Context context, int count) {
        Intent intent = new Intent("android.intent.action.BADGE_COUNT_UPDATE");
        intent.putExtra("badge_count", count);
        intent.putExtra("badge_count_package_name", context.getPackageName());
        intent.putExtra("badge_count_class_name", LUNCHER_CLASS);
        context.sendBroadcast(intent);
    }

    private static boolean isMIUI() {
        String code = getSystemProperty("ro.miui.ui.version.code");
        String name = getSystemProperty("ro.miui.ui.version.name");
        return !TextUtils.isEmpty(code) && !TextUtils.isEmpty(name);
    }

    private static String getSystemProperty(String propName) {
        String line;
        BufferedReader input = null;
        try {
            Process p = Runtime.getRuntime().exec("getprop " + propName);
            input = new BufferedReader(new InputStreamReader(p.getInputStream()), 1024);
            line = input.readLine();
            input.close();
        } catch (IOException ex) {
            return null;
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return line;
    }


    private static boolean isAppForeground(Context context) {
        ActivityManager activityManager =
                (ActivityManager) context.getSystemService(Service.ACTIVITY_SERVICE);
        if (activityManager == null || activityManager.getRunningAppProcesses() == null) {
            return false;
        }

        List<ActivityManager.RunningAppProcessInfo> runningAppProcessInfoList =
                activityManager.getRunningAppProcesses();
        if (runningAppProcessInfoList == null) {
            return false;
        }

        for (ActivityManager.RunningAppProcessInfo processInfo : runningAppProcessInfoList) {
            if (processInfo.processName.equals(context.getPackageName())
                    && (processInfo.importance ==
                    ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND)) {
                return true;
            }
        }
        return false;


    }
}