package com.thel.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

/**
 * Created by liuyun on 17/3/26.
 */

public class GsonUtils {

    private static Gson gson = new GsonBuilder().disableHtmlEscaping().create();

    public static String createJsonString(Object value) {

        return gson.toJson(value);
    }

    /**
     * json 转换为对象
     *
     * @param jsonString
     * @param clazz
     * @return
     */
    public static <T> T getObject(String jsonString, Class<T> clazz) {
        T t = null;
        try {
            t = gson.fromJson(jsonString, clazz);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return t;
    }


    /**
     * json转换为List<Object>
     *
     * @param jsonString
     * @param type
     * @return
     */
    public static <T> List<T> getObjects(String jsonString, Type type) {
        List<T> list = null;
        try {
            list = gson.fromJson(jsonString, type);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     * json转换为List<Map<String, Object>>
     *
     * @param jsonString
     * @return
     */
    public static List<Map<String, Object>> listKeyMaps(String jsonString) {
        List<Map<String, Object>> list;
        try {
            list = gson.fromJson(jsonString, new TypeToken<List<Map<String, Object>>>() {
            }.getType());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return list;
    }

    /**
     * 将Map转化为Json
     *
     * @param map
     * @return String
     */
    public static <T> String mapToJson(Map<String, T> map) {
        return gson.toJson(map);
    }

    public static <T> T fromJsonX(String json, Class<T> t) { return gson.fromJson(json, t);}

    public static Gson getGson(){
        return gson;
    }
}
