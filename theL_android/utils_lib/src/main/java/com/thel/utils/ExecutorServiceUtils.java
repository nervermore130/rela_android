package com.thel.utils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public class ExecutorServiceUtils {

    private static ExecutorServiceUtils instance = new ExecutorServiceUtils();

    private static final ExecutorService singleThreadExecutor = Executors.newCachedThreadPool(new ThreadFactory() {
        private final AtomicInteger mCount = new AtomicInteger(1);
        @Override
        public Thread newThread(Runnable r) {
            return new Thread(r, "CACHE_POOL_UTIL#" + mCount.getAndIncrement());
        }
    });
    private static final ExecutorService fixedThreadExecutor = Executors.newFixedThreadPool(10, new ThreadFactory() {
        private final AtomicInteger mCount = new AtomicInteger(1);
        @Override
        public Thread newThread(Runnable r) {
            return new Thread(r, "FIXED_POOL_UTIL#" + mCount.getAndIncrement());
        }
    });

    private ExecutorServiceUtils() {

    }

    public static ExecutorServiceUtils getInstatnce() {
        return instance;
    }

    public void exec(Runnable runnable) {
        singleThreadExecutor.execute(runnable);
    }

    public ExecutorService getExecutorService() {
        return singleThreadExecutor;
    }

    public ExecutorService getFixedExecutorService() {
        return fixedThreadExecutor;
    }

}
