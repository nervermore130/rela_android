package com.thel.live.ui.widget.recyclerview.animation;

import android.animation.Animator;
import android.view.View;

/**
 * Created by setsail on 16/7/25.
 */
public interface BaseAnimation {

    Animator[] getAnimators(View view);

}
