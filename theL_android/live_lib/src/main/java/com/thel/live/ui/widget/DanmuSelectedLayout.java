//package com.thel.live.ui.widget;
//
//import android.content.Context;
//import android.content.res.TypedArray;
//import android.util.AttributeSet;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//
//import androidx.appcompat.app.AppCompatActivity;
//
//import com.thel.R;
//import com.thel.app.TheLApp;
//import com.thel.constants.TheLConstants;
//import com.thel.growingio.GrowingIoConstant;
//import com.thel.live.LiveInitialize;
//import com.thel.live.LiveLogger;
//import com.thel.live.R;
//import com.thel.modules.live.GiftFirstChargeBean;
//import com.thel.network.InterceptorSubscribe;
//import com.thel.network.service.DefaultRequestService;
//import com.thel.ui.dialog.RechargeDialogFragment;
//import com.thel.utils.GrowingIOUtil;
//import com.thel.utils.L;
//
//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
//import io.reactivex.Flowable;
//import io.reactivex.android.schedulers.AndroidSchedulers;
//import io.reactivex.schedulers.Schedulers;
//
///**
// * @author liuyun
// */
//public class DanmuSelectedLayout extends RelativeLayout {
//
//    private static final String TAG = "DanmuSelectedLayout";
//
//    public static final String TYPE_DANMU_CLOSE = "0";
//
//    public static final String TYPE_DANMU_LEVEL = "1";
//
//    public static final String TYPE_DANMU_BOTTLE = "2";
//
//    public static final String TYPE_DANMU_CAT = "3";
//
//    public static final String TYPE_DANMU_CUPID = "4";
//
//    @BindView(R.id.close_rl) SquareRelativeLayout close_rl;
//
//    @BindView(R.id.level_rl) SquareRelativeLayout level_rl;
//
//    @BindView(R.id.bottle_rl) SquareRelativeLayout bottle_rl;
//
//    @BindView(R.id.cat_rl) SquareRelativeLayout cat_rl;
//
//    @BindView(R.id.cupid_rl) SquareRelativeLayout cupid_rl;
//
//    @BindView(R.id.close_selected_iv) SquareImageView close_selected_iv;
//
//    @BindView(R.id.level_selected_iv) SquareImageView level_selected_iv;
//
//    @BindView(R.id.bottle_selected_iv) SquareImageView bottle_selected_iv;
//
//    @BindView(R.id.cat_selected_iv) SquareImageView cat_selected_iv;
//
//    @BindView(R.id.cupid_selected_iv) SquareImageView cupid_selected_iv;
//
//    @BindView(R.id.level_money_tv) TextView level_money_tv;
//
//    @BindView(R.id.bottle_money_tv) TextView bottle_money_tv;
//
//    @BindView(R.id.cat_money_tv) TextView cat_money_tv;
//
//    @BindView(R.id.cupid_money_tv) TextView cupid_money_tv;
//
//    @BindView(R.id.txt_soft_money) TextView txt_soft_money;
//
//    @BindView(R.id.lin_recharge) LinearLayout lin_recharge;
//
//    @BindView(R.id.txt_recharge) TextView txt_recharge;
//
//    /**
//     * 是否是免费弹幕
//     */
//    private boolean isFreeBarrage = false;
//
//
//    private OnDanmuSelectedListener mOnDanmuSelectedListener;
//    private int lines;
//
//
//    public DanmuSelectedLayout(Context context) {
//        super(context);
//        initView(context, null);
//    }
//
//    public DanmuSelectedLayout(Context context, AttributeSet attrs) {
//        super(context, attrs);
//        initView(context, attrs);
//    }
//
//    public DanmuSelectedLayout(Context context, AttributeSet attrs, int defStyleAttr) {
//        super(context, attrs, defStyleAttr);
//        initView(context, attrs);
//    }
//
//    private void initView(Context context, AttributeSet attrs) {
//
//        int resId = R.layout.live_layout_danmu_selected;
//
//        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.Live_DanmuSelectedLayout);
//        lines = typedArray.getInt(R.styleable.Live_DanmuSelectedLayout_lines, 2);
//        typedArray.recycle();
//
//        if (lines == 1) {
//            resId = R.layout.live_layout_danmu_selected_horizontal;
//        }
//
//        View view = LayoutInflater.from(context).inflate(resId, this, true);
//        ButterKnife.bind(this, view);
//        setMoney();
//        if (TheLConstants.IsFirstCharge != 0) {
//            getGiftIsFirstCharge();
//        }
//    }
//
//    public void setGold(long gold) {
//        txt_soft_money.setText(LiveInitialize.getContext().getResources().getString(R.string.soft_money, gold));
//    }
//
//    public void rechargeSuccess() {
//
//    }
//
//    @OnClick(R.id.close_rl) void choiceDanmu() {
//
//        itemSelected(close_selected_iv);
//
//        if (mOnDanmuSelectedListener != null) {
//            mOnDanmuSelectedListener.onDanmuSelected(TYPE_DANMU_CLOSE);
//        }
//    }
//
//    @OnClick(R.id.level_rl) void choiceLevelDanmu() {
//
//        itemSelected(level_selected_iv);
//
//        if (mOnDanmuSelectedListener != null) {
//            mOnDanmuSelectedListener.onDanmuSelected(TYPE_DANMU_LEVEL);
//        }
//
//    }
//
//    @OnClick(R.id.bottle_rl) void choiceBottleDanmu() {
//
//        itemSelected(bottle_selected_iv);
//
//        if (mOnDanmuSelectedListener != null) {
//            mOnDanmuSelectedListener.onDanmuSelected(TYPE_DANMU_BOTTLE);
//        }
//
//    }
//
//    @OnClick(R.id.cat_rl) void choiceCatDanmu() {
//
//        itemSelected(cat_selected_iv);
//
//        if (mOnDanmuSelectedListener != null) {
//            mOnDanmuSelectedListener.onDanmuSelected(TYPE_DANMU_CAT);
//        }
//
//    }
//
//    @OnClick(R.id.cupid_rl) void choiceCupidDanmu() {
//        itemSelected(cupid_selected_iv);
//
//        if (mOnDanmuSelectedListener != null) {
//            mOnDanmuSelectedListener.onDanmuSelected(TYPE_DANMU_CUPID);
//        }
//    }
//
//    @OnClick(R.id.lin_recharge) void goRecharge() {
//
//    }
//
//    private void itemSelected(SquareImageView squareImageView) {
//
//        close_selected_iv.setVisibility(INVISIBLE);
//
//        level_selected_iv.setVisibility(INVISIBLE);
//
//        bottle_selected_iv.setVisibility(INVISIBLE);
//
//        cat_selected_iv.setVisibility(INVISIBLE);
//
//        cupid_selected_iv.setVisibility(INVISIBLE);
//
//        squareImageView.setVisibility(VISIBLE);
//    }
//
//    public void setOnDanmuSelectedListener(OnDanmuSelectedListener mOnDanmuSelectedListener) {
//        this.mOnDanmuSelectedListener = mOnDanmuSelectedListener;
//    }
//
//    public interface OnDanmuSelectedListener {
//        void onDanmuSelected(String selectedType);
//    }
//
//    public void showFreeBarrage() {
//
//        LiveLogger.d("DanmuSelectedLayout", "-------showFreeBarrage-------");
//
//        isFreeBarrage = true;
//
//        level_money_tv.setText(LiveInitialize.getContext().getResources().getString(R.string.free_this_time));
//
//        bottle_money_tv.setText(LiveInitialize.getContext().getResources().getString(R.string.free_this_time));
//
//        cat_money_tv.setText(LiveInitialize.getContext().getResources().getString(R.string.free_this_time));
//
//        cupid_money_tv.setText(LiveInitialize.getContext().getResources().getString(R.string.free_this_time));
//
//
//    }
//
//    public void setMoney() {
//
//        LiveLogger.d("DanmuSelectedLayout", "-------setMoney-------");
//
//        isFreeBarrage = false;
//
//        level_money_tv.setText(LiveInitialize.getContext().getResources().getString(R.string.soft_money, 2));
//
//        bottle_money_tv.setText(LiveInitialize.getContext().getResources().getString(R.string.soft_money, 3));
//
//        cat_money_tv.setText(LiveInitialize.getContext().getResources().getString(R.string.soft_money, 3));
//
//        cupid_money_tv.setText(LiveInitialize.getContext().getResources().getString(R.string.soft_money, 3));
//
//        if (isShown()) {
//            if (TheLConstants.IsFirstCharge != 0) {
//                getGiftIsFirstCharge();
//            }
//        }
//
//    }
//
//    public boolean isFreeBarrage() {
//        return isFreeBarrage;
//    }
//
//    public void getGiftIsFirstCharge() {
//        Flowable<GiftFirstChargeBean> beanFlowable = DefaultRequestService.createAllRequestService().getIsFirstCharge();
//        beanFlowable.onBackpressureDrop().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new InterceptorSubscribe<GiftFirstChargeBean>() {
//            @Override
//            public void onNext(GiftFirstChargeBean data) {
//                super.onNext(data);
//
//                showIsFirstCharge(data);
//
//            }
//        });
//
//    }
//
//    public void showIsFirstCharge(GiftFirstChargeBean firstChargeBean) {
//        if (firstChargeBean != null && firstChargeBean.data != null) {
//            //缓存到本地
//            TheLConstants.IsFirstCharge = firstChargeBean.data.isFirstCharge;
//            if (firstChargeBean.data.isFirstCharge == 1) {
//                lin_recharge.setBackgroundResource(R.drawable.bg_first_charge_shape_pink);
//                txt_recharge.setText(TheLApp.context.getText(R.string.first_charge_discount));
//            }
//        }
//    }
//
//    @OnClick(R.id.txt_recharge)
//    void goToRecharge() {
//        AppCompatActivity activity = (AppCompatActivity) getContext();
//        GrowingIOUtil.payTrack(GrowingIoConstant.LIVE_PAGE_POPWIN);
//        GrowingIOUtil.payEvar(GrowingIoConstant.G_LivePagePopWin);
//        RechargeDialogFragment rechargeDialogFragment = new RechargeDialogFragment();
//        rechargeDialogFragment.setTips(null);
//        if (lines == 1) {
//            rechargeDialogFragment.setHorizontal(1);
//        }
//        rechargeDialogFragment.show(activity.getSupportFragmentManager(), RechargeDialogFragment.class.getName());
//    }
//
//}
