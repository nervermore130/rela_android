package com.thel.live.persistence;


import android.content.Context;
import android.content.SharedPreferences;

import com.thel.live.LiveInitialize;
import com.thel.live.utils.AESEncryptor;

public class LiveSpUtils {

    private static SharedPreferences pref; // 文件操作对象
    private static final String SHARED_FILE_NAME = "the_live"; // sharefile 文件名

    /**
     * 从共享文件中获取字符串
     *
     * @param key      表签名
     * @param defValue 值
     */
    public static String getString(String key, String defValue) {
        try {
            if (null == pref) {
                pref = LiveInitialize.getContext().getSharedPreferences(SHARED_FILE_NAME, Context.MODE_PRIVATE);
            }
            String result;
            try {
                result = AESEncryptor.decrypt(AESEncryptor.SEED, pref.getString(key, defValue));
            } catch (Exception e) {
                result = pref.getString(key, defValue);
            }
            return result;

        } catch (Exception e) {
            return defValue;
        }
    }

    /**
     * 保存字符串数据
     *
     * @param key   表签名
     * @param value 值
     */
    public static void setString(String key, String value) {
        if (null == pref) {
            pref = LiveInitialize.getContext().getSharedPreferences(SHARED_FILE_NAME, Context.MODE_PRIVATE);
        }
        try {
            pref.edit().putString(key, AESEncryptor.encrypt(AESEncryptor.SEED, value)).apply();
        } catch (Exception e) {
            pref.edit().putString(key, value).apply();
        }
    }

    /**
     * 从共享文件中获取整型数据
     *
     * @param key      表签名
     * @param defValue 值
     */
    public static int getInt(String key, int defValue) {
        if (null == pref) {
            pref = LiveInitialize.getContext().getSharedPreferences(SHARED_FILE_NAME, Context.MODE_PRIVATE);
        }
        return pref.getInt(key, defValue);
    }

    /**
     * 从共享文件中获取长整型数据
     *
     * @param key
     * @param defValue
     * @return
     */
    public static long getLong(String key, long defValue) {
        if (null == pref) {
            pref = LiveInitialize.getContext().getSharedPreferences(SHARED_FILE_NAME, Context.MODE_PRIVATE);
        }
        return pref.getLong(key, defValue);
    }

    /**
     * 从共享文件中获取boolean数据
     *
     * @param key      表签名
     * @param defValue 值
     */
    public static boolean getBoolean(String key, boolean defValue) {
        if (null == pref) {
            pref = LiveInitialize.getContext().getSharedPreferences(SHARED_FILE_NAME, Context.MODE_PRIVATE);
        }
        return pref.getBoolean(key, defValue);
    }

    /**
     * 保存整型数据
     *
     * @param key   表签名
     * @param value 值
     */
    public static void setInt(String key, int value) {
        if (null == pref) {
            pref = LiveInitialize.getContext().getSharedPreferences(SHARED_FILE_NAME, Context.MODE_PRIVATE);
        }
        pref.edit().putInt(key, value).apply();
    }

    /**
     * 保存长整型数据
     *
     * @param key
     * @param value
     */
    public static void setLong(String key, long value) {
        if (null == pref) {
            pref = LiveInitialize.getContext().getSharedPreferences(SHARED_FILE_NAME, Context.MODE_PRIVATE);
        }
        pref.edit().putLong(key, value).apply();
    }

    /**
     * 保存boolean数据
     *
     * @param key   表签名
     * @param value 值
     */
    public static void setBoolean(String key, boolean value) {
        if (null == pref) {
            pref = LiveInitialize.getContext().getSharedPreferences(SHARED_FILE_NAME, Context.MODE_PRIVATE);
        }
        pref.edit().putBoolean(key, value).apply();
    }
}
