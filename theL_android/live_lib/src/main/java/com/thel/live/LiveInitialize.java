package com.thel.live;

import android.app.Activity;
import android.content.Context;
import android.widget.ImageView;

import com.thel.live.persistence.LiveSpUtils;

import retrofit2.Retrofit;

//
public class LiveInitialize {


    private static Context sApplicationContext;
    private static AppInterface sAppInterface;

    public static void init(Context context, AppInterface appInterface) {
        sApplicationContext = context;
        if (appInterface == null) {
            throw new IllegalArgumentException("AppInterface can't null");
        }
        sAppInterface = appInterface;
    }

    public void setDebug(boolean debug) {
        LiveLogger.setDebug(debug);
    }

    public static Context getContext() {
        return sApplicationContext;
    }

    public static AppInterface getAppInterface() {
        return sAppInterface;
    }

    private LiveInitialize() {
    }

    public static String getUserId() {
        if (sAppInterface != null) {
            String userId = sAppInterface.getUserId();
            if (userId == null) {
                userId = "";
            }
            return userId;
        }
        return "";
    }


    public interface AppInterface {
        Retrofit getRetrofit();

        void loadImageViewUrl(ImageView imageView, String url, int width, int height);

        void loadCircleImageViewUrl(ImageView imageView, int defaultImage, String url, int width, int height);

        Class<? extends Activity> getWebviewActivity();

        Class<? extends Activity> getUserInfoActivity();

        void doShare();

        void toPay();

        void toTipOff();//举报

        void getBlackList();//黑名单
        //客服

        //ShareFileUtils.getString(ShareFileUtils.ID, "")
        String getUserId();

        String getUserToken();
    }

}
