package com.thel.live;

import android.util.Log;

public class LiveLogger {

    public static boolean DEBUG = true;

    private static final String TAG = "theL_live";

    public static void setDebug(boolean debug) {
        DEBUG = debug;
    }

    public static void d(Object o) {
        if (DEBUG) {
            Log.d(TAG, String.valueOf(o));
        }
    }

    public static void e(Object o) {
        if (DEBUG) {
            Log.e(TAG, String.valueOf(o));
        }

    }

    public static void d(String tag, Object o) {
        if (DEBUG) {
            Log.d(tag, String.valueOf(o));
        }

    }

    public static void e(String tag, Object o) {
        if (DEBUG || Log.isLoggable(tag, Log.ERROR)) {
            Log.e(tag, String.valueOf(o));
        }

    }

    public static void i(String tag, Object o) {
        if (DEBUG) {
            String xml = String.valueOf(o);
            Log.i(tag, xml);
        }
    }

    public static void subLog(String tag, String msg) {
        // tag = null;//打包
        if (tag == null || tag.length() == 0 || msg == null || msg.length() == 0)
            return;

        int segmentSize = 3 * 1024;
        long length = msg.length();
        if (length <= segmentSize) {// 长度小于限制直接打印
            LiveLogger.i(tag, msg);
        } else {
            while (msg.length() > segmentSize) {// 循环分段打印日志
                String logContent = msg.substring(0, segmentSize);
                msg = msg.replace(logContent, "");
                LiveLogger.i(tag, logContent);
            }
            LiveLogger.i(tag, msg);// 打印剩余日志
        }
    }
}
